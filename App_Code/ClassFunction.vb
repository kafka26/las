Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class ClassFunction
    Inherits System.Web.UI.Page

    Public Overloads Shared Function SetPrintingWithPOSPrinter(ByVal reportDoc As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal sPaper As CrystalDecisions.Shared.PaperSize) As String
        Dim sMsg As String = ""
        Try
            ' ======== Set Printer Paper Size
            reportDoc.PrintOptions.PaperSize = sPaper
            Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
            Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
            Dim xCmd As New SqlCommand("", conn)
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd.CommandText = "SELECT genother1 FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='PRINTER' AND gencode='DUMMY'"
            reportDoc.PrintOptions.PrinterName = xCmd.ExecuteScalar
            conn.Close()
        Catch ex As Exception
            sMsg = ex.ToString
        End Try
        Return sMsg
    End Function

    Public Overloads Shared Function CheckDataExists(ByVal sSQL As String) As Boolean
        ' Return true when data exists or false when no data found
        CheckDataExists = False
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSQL
        If xCmd.ExecuteScalar > 0 Then
            CheckDataExists = True
        End If
        conn.Close()
        Return CheckDataExists
    End Function

    Public Overloads Shared Function CheckDataExists(ByVal sKey As String, ByVal sColomnKeyName As String, ByVal sTable As String) As Boolean
        CheckDataExists = False
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        sSql = "SELECT count(-1) FROM " & sTable & " where cmpcode='" & CompnyCode & "' and " & sColomnKeyName & "='" & sKey & "'"
        xCmd.CommandText = sSql
        If xCmd.ExecuteScalar > 0 Then
            CheckDataExists = True
        End If
        conn.Close()
        Return CheckDataExists
    End Function

    Public Overloads Shared Function CheckDataExists(ByVal iKey As Int64, ByVal sColomnKeyName As String, ByVal sTable As String) As Boolean
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        sSql = "SELECT count(-1) FROM " & sTable & " where cmpcode='" & CompnyCode & "' and " & sColomnKeyName & "=" & iKey
        xCmd.CommandText = sSql
        If xCmd.ExecuteScalar > 0 Then
            CheckDataExists = True
        End If
        conn.Close()
        Return CheckDataExists
    End Function

    Public Overloads Shared Function CheckDataExists(ByVal iKey As Int64, ByVal sColomnKeyName() As String, ByVal sTable() As String, ByVal sOtherWhere() As String) As Boolean
        CheckDataExists = False
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        For c1 As Int16 = 0 To UBound(sTable)
            sSql = "SELECT count(-1) FROM " & sTable(c1) & " where cmpcode='" & CompnyCode & "' and " & sColomnKeyName(c1) & "=" & iKey & " " & sOtherWhere(c1)
            xCmd.CommandText = sSql
            If xCmd.ExecuteScalar > 0 Then
                CheckDataExists = True
                Exit For
            End If
        Next
        conn.Close()
        Return CheckDataExists
    End Function

    Public Overloads Shared Function CheckDataExists(ByVal iKey As Int64, ByVal sColomnKeyName() As String, ByVal sTable() As String) As Boolean
        CheckDataExists = False
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        For c1 As Int16 = 0 To UBound(sTable)
            sSql = "SELECT COUNT(-1) FROM " & sTable(c1) & " WHERE " & sColomnKeyName(c1) & "=" & iKey
            xCmd.CommandText = sSql
            If xCmd.ExecuteScalar > 0 Then
                CheckDataExists = True
                Exit For
            End If
        Next
        conn.Close()
        Return CheckDataExists
    End Function

    Public Shared Function FoundCode(ByVal sType As String, ByVal sTable As String, ByVal sColumnCode As String, ByVal sKode As String, ByVal sColomnOid As String, ByVal sOid As String, ByVal sWhereAdd As String) As Boolean
        FoundCode = False
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        If sType.ToUpper = "I" Then ' type insert
            sSql = "select count(-1) from " & sTable & " where " & sColumnCode & " = '" & sKode & "' " & sWhereAdd
        ElseIf sType.ToUpper = "U" Then
            sSql = "select count(-1) from " & sTable & "  where " & sColomnOid & " <> " & sOid & " and  " & sColumnCode & "='" & sKode & "' " & sWhereAdd
        End If
        xCmd.CommandText = sSql
        If xCmd.ExecuteScalar > 0 Then
            FoundCode = True
        End If
        conn.Close()
    End Function

    Public Overloads Shared Function GetDateToPeriodAcctg(ByVal dtDate As Date) As String
        Return Format(dtDate, "yyyyMM")
    End Function

    Public Overloads Shared Function checkPagePermission(ByVal PagePath As String, ByVal dtAccess As DataTable) As Boolean
        Dim sTemp As String
        Dim sFill() As String
        Dim sepp As Char = "\"
        sFill = PagePath.Split(sepp)
        sTemp = sFill(sFill.Length - 1)
        Dim dv As DataView = dtAccess.DefaultView
        dv.RowFilter = "FORMADDRESS LIKE '%" & sTemp & "%'"
        If dv.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Overloads Shared Function FillDDLGen(ByVal sGroup() As String, ByRef oDDLObject() As DropDownList) As Boolean
        ' To fill data in dropdown list, return true when all data exist
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim xreader As SqlDataReader
        Dim sSql As String = ""
        FillDDLGen = True
        sSql = ""
        For c1 As Int16 = 0 To UBound(sGroup)
            sSql &= " select genoid, gendesc, gengroup from QL_mstgen where gengroup = '" & sGroup(c1) & "' UNION ALL"
        Next
        sSql = sSql.Remove(sSql.Length - 9)
        For c2 As Int16 = 0 To UBound(oDDLObject)
            oDDLObject(c2).Items.Clear()
        Next
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        Dim iDDL As Int16 = -1
        Dim sGroupTemp As String = ""
        While xreader.Read
            If iDDL = -1 Then
                sGroupTemp = xreader.GetValue(2).ToString.Trim
                iDDL = 0
            End If
            For c3 As Int16 = 0 To UBound(sGroup)
                If xreader.GetValue(2).ToString.Trim = sGroup(c3) Then
                    If xreader.GetValue(2).ToString.Trim <> sGroupTemp Then
                        iDDL += 1
                        sGroupTemp = xreader.GetValue(2).ToString.Trim
                    End If
                    oDDLObject(iDDL).Items.Add(xreader.GetValue(1))
                    oDDLObject(iDDL).Items.Item(oDDLObject(iDDL).Items.Count - 1).Value = xreader.GetValue(0)
                    Exit For
                End If
            Next
        End While
        xreader.Close()
        conn.Close()
        ' Cek if all data exits
        For c4 As Int16 = 0 To UBound(oDDLObject)
            If oDDLObject(c4).Items.Count = 0 Then
                FillDDLGen = False
                Exit For
            End If
        Next
        Return FillDDLGen
    End Function

    Public Overloads Shared Function FillDDLGen(ByVal sGroup As String, ByRef oDDLObject As DropDownList) As Boolean
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim xreader As SqlDataReader
        Dim sSql As String = ""
        FillDDLGen = True
        sSql = "select genoid, gendesc, gengroup from QL_mstgen where gengroup IN ('" & sGroup.Replace(",", "','") & "')"
        oDDLObject.Items.Clear()
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            oDDLObject.Items.Add(xreader.GetValue(1))
            oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()
        ' Cek if all data exits
        If oDDLObject.Items.Count = 0 Then
            FillDDLGen = False
        End If
        Return FillDDLGen
    End Function

    Public Overloads Shared Function FillDDLGen(ByVal sGroup As String, ByRef oDDLObject As DropDownList, ByVal sWhere As String) As Boolean
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim xreader As SqlDataReader
        Dim sSql As String = ""
        FillDDLGen = True
        sSql = "select genoid, gendesc, gengroup from QL_mstgen where gengroup IN ('" & sGroup.Replace(",", "','") & "') and " & sWhere
        oDDLObject.Items.Clear()
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            oDDLObject.Items.Add(xreader.GetValue(1))
            oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()
        ' Cek if all data exits
        If oDDLObject.Items.Count = 0 Then
            FillDDLGen = False
        End If
        Return FillDDLGen
    End Function

    Public Overloads Shared Function FillDDLGen(ByVal sGroup() As String, ByRef oDDLObject() As DropDownList, ByVal companycode() As String) As Boolean
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim xreader As SqlDataReader
        Dim sSql As String = ""
        FillDDLGen = True
        sSql = ""
        For c1 As Int16 = 0 To UBound(sGroup)
            sSql &= " select genoid, gendesc, gengroup from QL_mstgen where cmpcode='" & companycode(c1) & "' and gengroup = '" & sGroup(c1) & "'  and cmpcode='" & CompnyCode & "'    UNION ALL"
        Next
        sSql = sSql.Remove(sSql.Length - 9)
        For c2 As Int16 = 0 To UBound(oDDLObject)
            oDDLObject(c2).Items.Clear()
        Next
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        Dim iDDL As Int16 = -1
        Dim sGroupTemp As String = ""
        While xreader.Read
            If iDDL = -1 Then
                sGroupTemp = xreader.GetValue(2).ToString.Trim
                iDDL = 0
            End If
            For c3 As Int16 = 0 To UBound(sGroup)
                If xreader.GetValue(2).ToString.Trim = sGroup(c3) Then
                    If xreader.GetValue(2).ToString.Trim <> sGroupTemp Then
                        iDDL += 1
                        sGroupTemp = xreader.GetValue(2).ToString.Trim
                    End If
                    oDDLObject(iDDL).Items.Add(xreader.GetValue(1))
                    oDDLObject(iDDL).Items.Item(oDDLObject(iDDL).Items.Count - 1).Value = xreader.GetValue(0)
                    Exit For
                End If
            Next
        End While
        xreader.Close()
        conn.Close()
        ' Cek if all data exits
        For c4 As Int16 = 0 To UBound(oDDLObject)
            If oDDLObject(c4).Items.Count = 0 Then
                FillDDLGen = False
                Exit For
            End If
        Next
        Return FillDDLGen
    End Function

    Public Overloads Shared Function FillDDL(ByRef oDDLObject As DropDownList, ByVal sSql As String) As Boolean
        ' To fill data in dropdown list, return true when all data exist
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim xreader As SqlDataReader
        FillDDL = True
        oDDLObject.Items.Clear()
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            oDDLObject.Items.Add(xreader.GetValue(1))
            oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()
        ' Cek if all data exits
        If oDDLObject.Items.Count = 0 Then
            FillDDL = False
        End If
        Return FillDDL
    End Function

    Public Overloads Shared Function FillDDLWithNone(ByRef oDDLObject As DropDownList, ByVal sSql As String) As Boolean
        ' To fill data in dropdown list, return true when all data exist
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim xreader As SqlDataReader
        FillDDLWithNone = True
        oDDLObject.Items.Clear()
        oDDLObject.Items.Add("None")
        oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = "00"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            oDDLObject.Items.Add(xreader.GetValue(1))
            oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()
        ' Cek if all data exits
        If oDDLObject.Items.Count = 0 Then
            FillDDLWithNone = False
        End If
        Return FillDDLWithNone
    End Function

    Public Overloads Shared Function FillDDLWithAdditionalText(ByRef oDDLObject As DropDownList, ByVal sSql As String, ByVal sText As String, ByVal sValue As String) As Boolean
        ' To fill data in dropdown list, return true when all data exist
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim xreader As SqlDataReader
        FillDDLWithAdditionalText = True
        oDDLObject.Items.Clear()
        oDDLObject.Items.Add(sText)
        oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = sValue
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            oDDLObject.Items.Add(xreader.GetValue(1))
            oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()
        ' Cek if all data exits
        If oDDLObject.Items.Count = 0 Then
            FillDDLWithAdditionalText = False
        End If
        Return FillDDLWithAdditionalText
    End Function

    Public Overloads Shared Function FillDDL(ByRef oDDLObject As DropDownList, ByVal sValue() As String, ByVal sText() As String) As Boolean
        ' To fill data in dropdown list, return true when all data exist
        FillDDL = True
        oDDLObject.Items.Clear()
        For C1 As Integer = 0 To sValue.Length - 1
            oDDLObject.Items.Add(sText(C1))
            oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = sValue(C1)
        Next
        ' Cek if all data exits
        If oDDLObject.Items.Count = 0 Then
            FillDDL = False
        End If
        Return FillDDL
    End Function

    Public Overloads Shared Function FillGV(ByRef oGVObject As GridView, ByVal sSql As String, ByVal sTableName As String) As Boolean
        ' Fill data in grid view 
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim dset As New DataSet
        Dim adapter As New SqlDataAdapter("", conn)
        FillGV = True
        oGVObject.DataSource = Nothing
        adapter.SelectCommand.CommandText = sSql
        adapter.FillSchema(dset, SchemaType.Source)
        adapter.Fill(dset, sTableName)
        oGVObject.DataSource = dset.Tables(sTableName)
        oGVObject.DataBind()
        ' Cek if all data exits
        If oGVObject.Rows.Count < 1 Then
            FillGV = False
        End If
        Return FillGV
    End Function

    Public Overloads Shared Function FillGV(ByRef oGVObject As GridView, ByRef objDT As DataTable, ByVal sSql As String, ByVal sTableName As String) As Boolean
        ' Fill data in grid view 
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim dset As New DataSet
        Dim adapter As New SqlDataAdapter("", conn)
        FillGV = True
        oGVObject.DataSource = Nothing
        adapter.SelectCommand.CommandText = sSql
        adapter.FillSchema(dset, SchemaType.Source)
        adapter.Fill(dset, sTableName)
        objDT = dset.Tables(sTableName)
        oGVObject.DataSource = dset.Tables(sTableName)
        oGVObject.DataBind()
        ' Cek if all data exits
        If oGVObject.Rows.Count < 1 Then
            FillGV = False
        End If
        Return FillGV
    End Function

    Public Overloads Shared Function FillRBL(ByRef oRBLObject As RadioButtonList, ByVal sSql As String) As Boolean
        ' To fill data in radio button list, return true when all data exist
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim xreader As SqlDataReader
        FillRBL = True
        oRBLObject.Items.Clear()
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            oRBLObject.Items.Add(xreader.GetValue(1))
            oRBLObject.Items.Item(oRBLObject.Items.Count - 1).Value = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()
        ' Cek if all data exits
        If oRBLObject.Items.Count = 0 Then
            FillRBL = False
        End If
        Return FillRBL
    End Function

    Public Shared Function GenerateID(ByVal sTableName As String, ByVal sCompanyCode As String) As Int64
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""
        xCmd.CommandTimeout = 60
        sSql = "select ISNULL(lastoid,0) from QL_mstoid where tablename = '" & sTableName & "' and cmpcode='" & sCompanyCode & "'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        GenerateID = xCmd.ExecuteScalar + 1
        conn.Close()
        Return GenerateID
    End Function

    Public Overloads Shared Function DeleteData(ByVal sTableName As String, ByVal sColomnName As String, ByVal iKey As Int64, ByVal sCompanyCode As String) As Boolean
        ' Return true if delete was successful
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            sSql = "delete from " & sTableName & " where  " & sColomnName & "=" & iKey & " and cmpcode='" & sCompanyCode & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            DeleteData = True
            conn.Close()
        Catch ex As Exception
            DeleteData = False
        End Try
        Return DeleteData
    End Function

    Public Overloads Shared Function DeleteData(ByVal sTableName As String, ByVal sColomnName As String, ByVal iKey As Int64) As Boolean
        ' Return true if delete was successful
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            sSql = "delete from " & sTableName & " where  " & sColomnName & "=" & iKey
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            DeleteData = True
            conn.Close()
        Catch ex As Exception
            DeleteData = False
        End Try
        Return DeleteData
    End Function

    Public Overloads Shared Function DeleteData(ByVal sTableName As String, ByVal sColomnName As String, ByVal sKey As String, ByVal sCompanyCode As String) As Boolean
        ' Return true if delete was successful
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            sSql = "delete from " & sTableName & " where  " & sColomnName & "='" & sKey & "' and cmpcode='" & sCompanyCode & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            DeleteData = True
            conn.Close()
        Catch ex As Exception
            DeleteData = False
        End Try
        Return DeleteData
    End Function

    Public Overloads Shared Function DeleteData(ByVal sTableName As String, ByVal sColomnName As String, ByVal sKey As String) As Boolean
        ' Return true if delete was successful
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            sSql = "delete from " & sTableName & " where  " & sColomnName & "='" & sKey & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            DeleteData = True
            conn.Close()
        Catch ex As Exception
            DeleteData = False
        End Try
        Return DeleteData
    End Function

    Public Overloads Shared Function DeleteMasterDetail(ByVal sTableName() As String, ByVal sColomnName As String, ByVal iKey As Int64, ByVal sCompanyCode As String) As Boolean
        ' Return true if delete was successful
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        Try
            xCmd.Transaction = objTrans
            For C1 As Int16 = 0 To UBound(sTableName)
                sSql = "delete from " & sTableName(C1) & " where  " & sColomnName & "=" & iKey & " and cmpcode='" & sCompanyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            Next
            DeleteMasterDetail = True
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            DeleteMasterDetail = False
        End Try
        Return DeleteMasterDetail
    End Function

    Public Overloads Shared Function DeleteMasterDetail(ByVal sTableName() As String, ByVal sColomnName() As String, ByVal iKey As Int64, ByVal sCompanyCode As String) As Boolean
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        Try
            xCmd.Transaction = objTrans
            For C1 As Int16 = 0 To UBound(sTableName)
                sSql = "delete from " & sTableName(C1) & " where  " & sColomnName(C1) & "=" & iKey & " and cmpcode='" & sCompanyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            Next
            DeleteMasterDetail = True
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            DeleteMasterDetail = False
        End Try
        Return DeleteMasterDetail
    End Function

    Public Shared Function Tchar(ByVal sVar As String) As String
        Return (sVar.Trim.Replace("'", "''"))
    End Function

    Public Shared Function Tnumber(ByVal sVar As String) As String
        Try
            Tnumber = (sVar.Trim.Replace(",", "").Replace("_", ""))
        Catch ex As Exception
            Tnumber = "0"
        End Try
    End Function

    Public Shared Function ToDecimal(ByVal sVar As String) As Decimal
        Dim sTemp As String = "" : sTemp = sVar : ToDecimal = 0
        If sTemp <> "" Then
            Decimal.TryParse((sTemp.Trim.Replace(",", "").Replace("_", "")), ToDecimal)
        End If
    End Function

    Public Shared Function ToDouble(ByVal sVar As String) As Double
        Try
            ToDouble = (sVar.Trim.Replace(",", "").Replace("_", ""))
        Catch ex As Exception
            ToDouble = 0
        End Try
    End Function

    Public Shared Function ToInteger(ByVal sVar As String) As Integer
        Try
            ToInteger = Integer.Parse((sVar.Trim.Replace(",", "").Replace("_", "")))
        Catch ex As Exception
            ToInteger = 0
        End Try
    End Function

    Public Shared Function ToMaskEdit(ByVal dMoney As Double, ByVal iType As Byte) As String
        ToMaskEdit = "0"
        Try
            Dim sStr1 As String = CStr(dMoney)
            Dim sStr2 As String
            Dim sFormat As String = ""
            If sStr1.Contains(".") Then
                sStr2 = Left(sStr1, sStr1.IndexOf("."))
            Else
                sStr2 = sStr1
            End If
            For i As Integer = 1 To sStr2.Length
                sFormat = "#" & sFormat
                If i Mod 3 = 0 And i <> sStr2.Length Then
                    sFormat = "," & sFormat
                End If
            Next
            If iType = 0 Then ' 0 digit belakang koma
                ToMaskEdit = Format(dMoney, sFormat)
                If ToMaskEdit.Trim = "" Then
                    ToMaskEdit = "0"
                End If
            ElseIf iType = 1 Then ' 1 digit belakang koma
                ToMaskEdit = Format(dMoney, sFormat & ".#")
                If ToMaskEdit.Trim = "" Then
                    ToMaskEdit = "0.0"
                ElseIf ToMaskEdit.IndexOf(".") = -1 Then
                    ToMaskEdit &= ".0"
                End If
            Else ' digit belakang koma > 1
                sFormat &= "."
                Dim sNol As String = ""
                For i As Integer = 1 To iType
                    sFormat &= "#"
                    sNol &= "0"
                Next
                ToMaskEdit = Format(dMoney, sFormat)
                If ToMaskEdit.Trim = "" Then
                    ToMaskEdit = "0." & sNol
                ElseIf ToMaskEdit.IndexOf(".") = -1 Then
                    ToMaskEdit &= "." & sNol
                Else
                    For j As Integer = 2 To iType
                        If ToMaskEdit.Length - ToMaskEdit.IndexOf(".") = j Then
                            ToMaskEdit &= Left(sNol, iType - j + 1)
                            Exit For
                        End If
                    Next
                End If
            End If
            If dMoney > 0 And dMoney < 1 Then
                ToMaskEdit = "0" & ToMaskEdit
            End If
            Dim sSplit() As String = ToMaskEdit.Split(".")
            If sSplit.Length > 1 Then
                Dim sKoma As String = ""
                Dim sTmp As String = sSplit(1)
                For i As Integer = sSplit(1).Length To 1 Step -1
                    If ToInteger(Strings.Mid(sSplit(1), i, 1)) <> 0 Then
                        sKoma = Left(sSplit(1), i)
                        Exit For
                    End If
                Next
                If sKoma <> "" Then
                    ToMaskEdit = sSplit(0) & "." & sKoma
                Else
                    ToMaskEdit = sSplit(0)
                End If
            End If
        Catch ex As Exception
        End Try
        Return ToMaskEdit
    End Function

    Public Shared Function GenNumberString(ByVal sPrefix As String, ByVal sMiddle As String, ByVal lNo As Long, ByVal iDefaultCounter As Int16) As String
        Dim iAdd As Int16 = lNo.ToString.Length - iDefaultCounter
        If iAdd > 0 Then
            iDefaultCounter += iAdd
        End If
        Dim sFormat As String = ""
        For c1 As Int16 = 1 To iDefaultCounter
            sFormat = sFormat & "0"
        Next
        Return (sPrefix & sMiddle & Format(lNo, sFormat))
    End Function

    Public Shared Function GenNumberString(ByVal sPrefix As String, ByVal lNo As Long, ByVal iDefaultCounter As Int16) As String
        Dim iAdd As Int16 = lNo.ToString.Length - iDefaultCounter
        If iAdd > 0 Then
            iDefaultCounter += iAdd
        End If
        Dim sFormat As String = ""
        For c1 As Int16 = 1 To iDefaultCounter
            sFormat = sFormat & "0"
        Next
        Return (Format(lNo, sFormat) & sPrefix)
    End Function

    Public Shared Function toDate(ByVal sTgl As String) As String
        Dim aData() As String = sTgl.Split("/")
        Try
            toDate = aData(1) & "/" & aData(0) & "/" & aData(2)
        Catch ex As Exception
            toDate = ""
        End Try
        Return toDate
    End Function

    Public Shared Function GetServerTime() As Date
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""
        Dim dateValue As Date
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        sSql = "SELECT getdate() FROM ql_mstoid"
        xCmd.CommandText = sSql
        Try
            dateValue = xCmd.ExecuteScalar
        Catch ex As Exception
            dateValue = Now
        End Try
        conn.Close()
        Return dateValue
    End Function

    Public Overloads Shared Function GetStrData(ByVal sSql As String) As String
        ' To get 1 data from query , return '?' if no data was returned
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim xreader As SqlDataReader
        GetStrData = ""
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd.CommandText = sSql
            xreader = xCmd.ExecuteReader
            While xreader.Read
                GetStrData = xreader.GetValue(0)
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            GetStrData = ""
        End Try
        Return GetStrData
    End Function

    Public Shared Function GetVarInterface(ByVal sVar As String, ByVal sCmpCode As String) As String
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        GetVarInterface = GetStrData("SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='" & sVar & "' AND cmpcode='" & CompnyCode & "' AND interfaceres1='" & sCmpCode & "'")
        If GetVarInterface = "" Then
            GetVarInterface = GetStrData("SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='" & sVar & "' AND cmpcode='" & CompnyCode & "'")
        End If
        Return GetVarInterface
    End Function

    Public Shared Function GetAcctgOID(ByVal sVar As String, ByVal Scmpcode As String) As Integer
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("SELECT TOP 1 acctgoid FROM QL_mstacctg WHERE acctgcode='" & sVar & "' and cmpcode='" & Scmpcode & "'", conn)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        GetAcctgOID = xCmd.ExecuteScalar
        conn.Close()
        Return GetAcctgOID
    End Function

    Public Shared Function GetAcctgCodeDesc(ByVal iAcctgoid As Int32, ByVal Scmpcode As String) As String
        GetAcctgCodeDesc = GetStrData("SELECT acctgcode + '-' + acctgdesc  FROM QL_mstacctg  WHERE acctgoid=" & iAcctgoid & "  and cmpcode='" & Scmpcode & "'")
    End Function

    Public Shared Function ATrim(ByVal sStr As String) As String
        ' Remove all space
        Return sStr.Replace(" ", "")
    End Function

    Public Overloads Shared Function NewMaskEdit(ByVal sDec As String) As String
        NewMaskEdit = Left(ToDouble(sDec), IIf(sDec.ToString.IndexOf(".") > 0, sDec.ToString.IndexOf("."), sDec.ToString.Length))
        NewMaskEdit = ToMaskEdit(NewMaskEdit, 1) & ToDouble(sDec).ToString.Substring(IIf(ToDouble(sDec).ToString.IndexOf(".") > 0, ToDouble(sDec).ToString.IndexOf("."), ToDouble(sDec).ToString.Length))
        Return NewMaskEdit
    End Function

    Public Overloads Shared Function SetTableDetail(ByVal sName As String, ByVal sColumnName() As String, ByVal sColumnType() As String) As DataTable
        Dim nuTable As New DataTable(sName)
        For C1 As Integer = 0 To sColumnName.Length - 1
            nuTable.Columns.Add(sColumnName(C1), Type.GetType(sColumnType(C1)))
        Next
        Return nuTable
    End Function

    'Tambahan Fungsi Untuk Cek Tanggal Sudah Valid atau Belum (FamZ-16 Jan 12)
    Public Shared Function IsValidDate(ByVal sDate As String, ByVal sFormat As String, ByRef sError As String) As Boolean
        If sFormat.Trim <> "MM/dd/yyyy" And sFormat.Trim <> "dd/MM/yyyy" Then
            sError = "Date format should be '<STRONG>MM/dd/yyyy</STRONG>' or '<STRONG>dd/MM/yyyy</STRONG>'."
            Return False
        End If
        If sFormat = "dd/MM/yyyy" Then
            sDate = toDate(sDate)
        End If
        Dim splitdate() As String = sDate.Split("/")
        If splitdate.Length < 3 Then
            sError = "Date is incomplete. Date component must be consisted of day, month and year value."
            Return False
        End If
        If splitdate.Length > 3 Then
            sError = "Date is out of range. Date component must be consisted of day, month and year value only."
            Return False
        End If
        If ToDouble(splitdate(2)) >= 1900 And ToDouble(splitdate(2)) <= 9999 Then
            If ToDouble(splitdate(0)) > 0 And ToDouble(splitdate(0)) < 13 Then
                Dim iNDay As Integer = Date.DaysInMonth(CInt(splitdate(2)), CInt(splitdate(0)))
                If ToDouble(splitdate(1)) < 1 Or ToDouble(splitdate(1)) > CDbl(iNDay) Then
                    sError = "Day value is invalid. Day value must be between 1 and " & iNDay.ToString & "."
                    Return False
                End If
            Else
                sError = "Month value is invalid. Month value must be between 1 and 12."
                Return False
            End If
        Else
            sError = "Year value is invalid. Year value must be between 1900 and 9999."
            Return False
        End If
        Return True
    End Function

    Public Overloads Shared Function IsStockAvailable(ByVal sCmpCode As String, ByVal sPeriod As String, ByVal iMatOid As Integer, ByVal iWhoid As Integer, ByVal dQty As Double, Optional ByVal sType As String = "") As Boolean
        IsStockAvailable = False
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim sPeriodBefore As String = Left(sPeriod, 4) & Format((ToInteger(Right(sPeriod, 2)) - 1), "00")
        If ToInteger(Right(sPeriod, 2)) - 1 = 0 Then
            sPeriodBefore = Format((ToInteger(Left(sPeriod, 4)) - 1), "0000") & "12"
        End If
        ' sSql = "SELECT COUNT(refoid) FROM QL_crdstock WHERE cmpcode='" & sCmpCode & "' AND periodacctg IN ('" & sPeriod & "', '" & sPeriodBefore & "') AND refoid=" & iMatOid & " AND mtrlocoid=" & iWhoid & " AND closingdate='01/01/1900' GROUP BY refoid HAVING SUM(saldoakhir)>=" & dQty
		sSql = "SELECT ISNULL(SUM(qtyin - qtyout), 0.0) FROM QL_constock WHERE refoid=" & iMatOid & " AND mtrlocoid=" & iWhoid
		
        xCmd.CommandText = sSql
        ' If xCmd.ExecuteScalar > 0 Then
            ' IsStockAvailable = True
        ' End If
		If ToDouble(xCmd.ExecuteScalar) >= dQty Then
            IsStockAvailable = True
        End If
        conn.Close()
        Return IsStockAvailable
    End Function

    Public Overloads Shared Function FillDDL2(ByRef oDDLObject As DropDownList, ByVal sSql As String, ByVal first As String) As Boolean ' to fill data in dropdown list, return true when all data exist
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_PANVERTA_Conn"))
        Dim xCmd As New SqlCommand("", conn)
        Dim xreader As SqlDataReader

        FillDDL2 = True
        oDDLObject.Items.Clear()
        oDDLObject.Items.Add(first)
        oDDLObject.Items(0).Value = "NULL"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            oDDLObject.Items.Add(xreader.GetValue(1))
            oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()

        'cek if all data exits
        If oDDLObject.Items.Count = 0 Then
            FillDDL2 = False
        End If
        Return FillDDL2
    End Function

    Public Shared Function GetSortDirection(ByVal column As String, ByRef sExp As String, ByRef sDir As String) As String
        ' By default, set the sort direction to ascending.
        Dim sortDirection = "ASC"
        ' Retrieve the last column that was sorted.
        Dim sortExpression = TryCast(sExp, String)
        If sortExpression IsNot Nothing Then
            ' Check if the same column is being sorted.
            ' Otherwise, the default value can be returned.
            If sortExpression = column Then
                Dim lastDirection = TryCast(sDir, String)
                If lastDirection IsNot Nothing AndAlso lastDirection = "ASC" Then
                    sortDirection = "DESC"
                End If
            End If
        End If
        ' Save new values in ViewState.
        sDir = sortDirection
        sExp = column
        Return sortDirection
    End Function

    Public Shared Function isPeriodClosed(ByVal sCmpCode As String, ByVal sDate As String) As Boolean
        isPeriodClosed = False
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim sPeriod As String = ""
        Dim sErr As String = ""
        If sDate = "" Then
            sPeriod = GetDateToPeriodAcctg(GetServerTime())
        Else
            If Not IsValidDate(sDate, "MM/dd/yyyy", sErr) Then
                sPeriod = GetDateToPeriodAcctg(GetServerTime())
            Else
                sPeriod = GetDateToPeriodAcctg(CDate(sDate))
            End If
        End If
        sSql = "SELECT COUNT(*) FROM QL_crdstock WHERE cmpcode='" & sCmpCode & "' AND CONVERT(VARCHAR(10), closingdate, 101)<>'01/01/1900' AND periodacctg='" & sPeriod & "'"
        xCmd.CommandText = sSql
        If xCmd.ExecuteScalar > 0 Then
            isPeriodClosed = True
        End If
        conn.Close()
        Return isPeriodClosed
    End Function

    Public Shared Sub ShowCOAPosting(ByVal sNoRef As String, ByVal sCmpCode As String, ByRef gvData As GridView, Optional ByVal sRateType As String = "Default", Optional ByVal sOther As String = "")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        gvData.DataSource = Nothing
        Dim sFieldAmt As String = "glamt"
        If sRateType = "IDR" Then
            sFieldAmt = "glamtidr"
        ElseIf sRateType = "USD" Then
            sFieldAmt = "glamtusd"
        End If
        Dim sQuery As String = "SELECT a.acctgcode, a.acctgdesc, (CASE d.gldbcr WHEN 'D' THEN d." & sFieldAmt & " ELSE 0 END) AS acctgdebet, (CASE d.gldbcr WHEN 'C' THEN d." & sFieldAmt & " ELSE 0 END) AS acctgcredit FROM QL_trngldtl d INNER JOIN QL_mstacctg a ON a.acctgoid=d.acctgoid WHERE d.noref='" & sNoRef & "' AND d.cmpcode='" & sCmpCode & "' AND d." & sFieldAmt & ">0 AND ISNULL(d.glother1, '')='" & sOther & "' ORDER BY d.glseq"
        Dim daCOAPosting As New SqlDataAdapter(sQuery, conn)
        Dim dtCOAPosting As New DataTable
        daCOAPosting.Fill(dtCOAPosting)
        If dtCOAPosting.Rows.Count = 0 Then
            sQuery = "SELECT a.acctgcode, a.acctgdesc, (CASE d.gldbcr WHEN 'D' THEN d." & sFieldAmt & " ELSE 0 END) AS acctgdebet, (CASE d.gldbcr WHEN 'C' THEN d." & sFieldAmt & " ELSE 0 END) AS accgtcredit FROM QL_trngldtl d INNER JOIN QL_mstacctg a ON a.acctgoid=d.acctgoid WHERE d.noref LIKE '%" & sNoRef & "' AND d.cmpcode='" & sCmpCode & "' AND d." & sFieldAmt & ">0 AND ISNULL(d.glother1, '')='" & sOther & "' ORDER BY d.glseq"
            dtCOAPosting.Rows.Clear()
            Dim daCOAPosting2 As New SqlDataAdapter(sQuery, conn)
            daCOAPosting2.Fill(dtCOAPosting)
        End If
        gvData.DataSource = dtCOAPosting
        gvData.DataBind()
    End Sub

    Public Overloads Shared Function IsInterfaceExists(ByVal sVar As String, Optional ByVal sCmpCode As String = "All") As Boolean
        IsInterfaceExists = False
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        sSql = "SELECT COUNT(-1) FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND interfacevar='" & sVar & "' AND interfaceres1='" & sCmpCode & "'"
        xCmd.CommandText = sSql
        If xCmd.ExecuteScalar > 0 Then
            IsInterfaceExists = True
        Else
            sSql = "SELECT COUNT(-1) FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND interfacevar='" & sVar & "' AND interfaceres1='All'"
            xCmd.CommandText = sSql
            If xCmd.ExecuteScalar > 0 Then
                IsInterfaceExists = True
            End If
        End If
        conn.Close()
        Return IsInterfaceExists
    End Function

    Public Shared Function GetInterfaceWarning(ByVal sVar As String, Optional ByVal aAction As String = "using") As String
        Return "Please Define some COA for Variable " & sVar & " in Accounting -> Master -> Interface before continue " & aAction & " this form!"
    End Function

    Public Shared Function GetMultiUserStatus(ByVal sTable As String, ByVal sOidField As String, ByVal sOidValue As String, ByVal sStatusField As String, ByVal sUpdTime As String, ByVal sType As String) As String
        GetMultiUserStatus = ""
        Dim sSql As String = "SELECT COUNT(*) FROM " & sTable & " WHERE " & sOidField & "=" & sOidValue & " AND " & sStatusField & "='In Process' AND (CONVERT(VARCHAR(10), updtime, 101) + ' ' + CONVERT(VARCHAR(10), updtime, 108))<>'" & Format(CDate(sUpdTime), "MM/dd/yyyy HH:mm:ss") & "'"
        If CheckDataExists(sSql) Then
            GetMultiUserStatus="This data has been updated by another user. Please CANCEL this transaction and try again!"
        Else
            sSql = "SELECT COUNT(*) FROM " & sTable & " WHERE " & sOidField & "=" & sOidValue & " AND " & sStatusField & "='" & IIf(sType = "Post", sType, "In Approval") & "'"
            If CheckDataExists(sSql) Then
                GetMultiUserStatus = "This data has been " & IIf(sType = "Post", "posted", "sent for approval") & " by another user. Please CANCEL this transaction!"
            Else
                sSql = "SELECT COUNT(*) FROM " & sTable & " WHERE " & sOidField & "=" & sOidValue
                If ToDouble(GetStrData(sSql).ToString) = 0 Then
                    GetMultiUserStatus="This data has been deleted by another user. Please CANCEL this transaction!"
                End If
            End If
        End If
        Return GetMultiUserStatus
    End Function

    Public Shared Function GetRoundValue(ByVal sVar As String) As Integer
        GetRoundValue = 0
        Dim dValue As Double = ToDouble(sVar)
        If dValue.ToString.Contains(".") Then
            Dim sSplit() As String = dValue.ToString.Split(".")
            If sSplit.Length > 0 Then
                GetRoundValue = sSplit(sSplit.Length - 1).Length
            End If
        End If
        Return GetRoundValue
    End Function

    Public Overloads Shared Function IsQtyRounded(ByVal dQty As Double, ByVal dRound As Double) As Boolean
        Dim iRes As Long
        If Long.TryParse(dQty / dRound, iRes) Then
            IsQtyRounded = True
        Else
            IsQtyRounded = False
        End If
    End Function

    Public Overloads Shared Function IsQtyRounded(ByVal dvData As DataView, ByVal iIndexColQty As Integer, ByVal iIndexColRound As Integer) As Boolean
        IsQtyRounded = True
        Dim iRes As Long
        For C1 As Integer = 0 To dvData.Count - 1
            If Not Long.TryParse(ToDouble(dvData(C1)(iIndexColQty).ToString) / ToDouble(dvData(C1)(iIndexColRound).ToString), iRes) Then
                IsQtyRounded = False
                Exit For
            End If
        Next
    End Function

    Public Overloads Shared Function IsQtyRounded(ByVal dvData As DataView, ByVal sNameColQty As String, ByVal sNameColRound As String) As Boolean
        IsQtyRounded = True
        Dim iRes As Long
        For C1 As Integer = 0 To dvData.Count - 1
            If Not Long.TryParse(ToDouble(dvData(C1)(sNameColQty).ToString) / ToDouble(dvData(C1)(sNameColRound).ToString), iRes) Then
                IsQtyRounded = False
                Exit For
            End If
        Next
    End Function

    Public Overloads Shared Function IsQtyRounded(ByVal dtData As DataTable, ByVal iIndexColQty As Integer, ByVal iIndexColRound As Integer) As Boolean
        IsQtyRounded = True
        Dim iRes As Long
        For C1 As Integer = 0 To dtData.Rows.Count - 1
            If Not Long.TryParse(ToDouble(dtData.Rows(C1)(iIndexColQty).ToString) / ToDouble(dtData.Rows(C1)(iIndexColRound).ToString), iRes) Then
                IsQtyRounded = False
                Exit For
            End If
        Next
    End Function

    Public Overloads Shared Function IsQtyRounded(ByVal dtData As DataTable, ByVal sNameColQty As String, ByVal sNameColRound As String) As Boolean
        IsQtyRounded = True
        Dim iRes As Long
        For C1 As Integer = 0 To dtData.Rows.Count - 1
            If Not Long.TryParse(ToDouble(dtData.Rows(C1)(sNameColQty).ToString) / ToDouble(dtData.Rows(C1)(sNameColRound).ToString), iRes) Then
                IsQtyRounded = False
                Exit For
            End If
        Next
    End Function

    Public Overloads Shared Function FillDDLAcctg(ByRef oDDLObject As DropDownList, ByVal sVar As String, ByVal sCmpCode As String, Optional ByVal sNoneValue As String = "", Optional ByVal sNoneText As String = "", Optional ByVal sFilter As String = "") As Boolean
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        FillDDLAcctg = True
        oDDLObject.Items.Clear()
        Dim sSql As String = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & sVar & "' AND interfaceres1='" & sCmpCode & "'"
        Dim sCode As String = GetStrData(sSql)
        If sCode = "" Then
            sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & sVar & "' AND interfaceres1='All'"
            sCode = GetStrData(sSql)
        End If
        If sCode <> "" Then
            sSql = "SELECT DISTINCT a.acctgoid, ('(' + a.acctgcode + ') ' + a.acctgdesc) AS acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='" & CompnyCode & "' AND a.activeflag='ACTIVE' " & sFilter & " AND ("
            Dim sSplitCode() As String = sCode.Split(",")
            For C1 As Integer = 0 To sSplitCode.Length - 1
                sSql &= "a.acctgcode LIKE '" & LTrim(sSplitCode(C1)) & "%'"
                If C1 < sSplitCode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) ORDER BY acctgdesc"
            If sNoneText <> "" And sNoneValue <> "" Then
                FillDDLWithAdditionalText(oDDLObject, sSql, sNoneText, sNoneValue)
            Else
                FillDDL(oDDLObject, sSql)
            End If
        End If
        If oDDLObject.Items.Count = 0 Then
            FillDDLAcctg = False
        End If
        Return FillDDLAcctg
    End Function

    Public Overloads Shared Function FillDDLAcctg(ByRef oDDLObject As DropDownList, ByVal sVar() As String, ByVal sCmpCode As String) As Boolean
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim xreader As SqlDataReader
        FillDDLAcctg = True
        oDDLObject.Items.Clear()
        For C1 As Integer = 0 To sVar.Length - 1
            Dim sSql As String = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & sVar(C1) & "' AND interfaceres1='" & sCmpCode & "'"
            Dim sCode As String = GetStrData(sSql)
            If sCode = "" Then
                sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & sVar(C1) & "' AND interfaceres1='All'"
                sCode = GetStrData(sSql)
            End If
            If sCode <> "" Then
                sSql = "SELECT DISTINCT a.acctgoid, ('(' + a.acctgcode + ') ' + a.acctgdesc) AS acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='" & CompnyCode & "' AND a.activeflag='ACTIVE' AND ("
                Dim sSplitCode() As String = sCode.Split(",")
                For C2 As Integer = 0 To sSplitCode.Length - 1
                    sSql &= "a.acctgcode LIKE '" & LTrim(sSplitCode(C2)) & "%'"
                    If C2 < sSplitCode.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) ORDER BY acctgdesc"
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If
                xCmd.CommandText = sSql
                xreader = xCmd.ExecuteReader
                While xreader.Read
                    oDDLObject.Items.Add(xreader.GetValue(1))
                    oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
                End While
                xreader.Close()
                conn.Close()
            End If
        Next
        If oDDLObject.Items.Count = 0 Then
            FillDDLAcctg = False
        End If
        Return FillDDLAcctg
    End Function

    Public Overloads Shared Function FillGVAcctg(ByRef oGVObject As GridView, ByVal sVar As String, ByVal sCmpCode As String, Optional ByVal sFilter As String = "") As Boolean
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        FillGVAcctg = True
        oGVObject.DataSource = Nothing : oGVObject.DataBind()
        Dim sSql As String = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & sVar & "' AND interfaceres1='" & sCmpCode & "'"
        Dim sCode As String = GetStrData(sSql)
        If sCode = "" Then
            sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & sVar & "' AND interfaceres1='All'"
            sCode = GetStrData(sSql)
        End If
        If sCode <> "" Then
            sSql = "SELECT DISTINCT a.acctgoid, a.acctgcode, a.acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='" & CompnyCode & "' AND a.activeflag='ACTIVE' " & sFilter & " AND ("
            Dim sSplitCode() As String = sCode.Split(",")
            For C1 As Integer = 0 To sSplitCode.Length - 1
                sSql &= "a.acctgcode LIKE '" & LTrim(sSplitCode(C1)) & "%'"
                If C1 < sSplitCode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) ORDER BY a.acctgcode"
            FillGV(oGVObject, sSql, "QL_mstacctg")
        End If
        If oGVObject.Rows.Count < 1 Then
            FillGVAcctg = False
        End If
        Return FillGVAcctg
    End Function

    Public Overloads Shared Function FillGVAcctg(ByRef oGVObject As GridView, ByVal sVar() As String, ByVal sCmpCode As String, Optional ByVal sFilter As String = "") As Boolean
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        FillGVAcctg = True
        oGVObject.DataSource = Nothing : oGVObject.DataBind()
        Dim sCode As String = ""
        Dim sSql As String = ""
        For C1 As Integer = 0 To sVar.Length - 1
            sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & sVar(C1) & "' AND interfaceres1='" & sCmpCode & "'"
            Dim sTmp As String = GetStrData(sSql)
            If sTmp = "" Then
                sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & sVar(C1) & "' AND interfaceres1='All'"
                sTmp = GetStrData(sSql)
            End If
            If sCode = "" Then
                sCode = sTmp
            Else
                sCode &= ", " & sTmp
            End If
        Next
        If sCode <> "" Then
            sSql = "SELECT DISTINCT a.acctgoid, a.acctgcode, a.acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='" & CompnyCode & "' AND a.activeflag='ACTIVE' " & sFilter & " AND ("
            Dim sSplitCode() As String = sCode.Split(",")
            For C2 As Integer = 0 To sSplitCode.Length - 1
                sSql &= "a.acctgcode LIKE '" & LTrim(sSplitCode(C2)) & "%'"
                If C2 < sSplitCode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) ORDER BY a.acctgcode"
            FillGV(oGVObject, sSql, "QL_mstacctg")
        End If
        If oGVObject.Rows.Count < 1 Then
            FillGVAcctg = False
        End If
        Return FillGVAcctg
    End Function

    Public Overloads Shared Function isLengthAccepted(ByVal sField As String, ByVal sTable As String, ByVal sFieldLabel As String, ByVal dValue As Double, ByRef sErrReply As String) As Boolean
        isLengthAccepted = True
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim sSql As String = "SELECT col.precision, col.scale FROM sys.tables ta INNER JOIN sys.columns col ON col.object_id=ta.object_id WHERE col.name='" & sField & "' AND ta.name='" & sTable & "'"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim xCmd As New SqlCommand(sSql, conn)
        Dim iPrec, iScale As Integer
        Try
            Dim xRdr As SqlDataReader = xCmd.ExecuteReader
            While xRdr.Read
                iPrec = CInt(xRdr.Item("precision").ToString)
                iScale = CInt(xRdr.Item("scale").ToString)
            End While
            xRdr.Close()
        Catch ex As Exception
            iPrec = 0 : iScale = 0
            sErrReply = "- Can't check the result. Please check that the parameter has been assigned correctly!"
            isLengthAccepted = False
        End Try
        conn.Close()
        If iPrec > 0 And iScale >= 0 Then
            Dim sTextValue As String = ""
            For C1 As Integer = 0 To iPrec - iScale - 1
                sTextValue &= "9"
            Next
            If iScale > 0 Then
                sTextValue &= "."
                For C1 As Integer = 0 To iScale - 1
                    sTextValue &= "9"
                Next
            End If
            If dValue > ToDouble(sTextValue) Then
                sErrReply = ToMaskEdit(ToDouble(sTextValue), iScale)
                isLengthAccepted = False
            End If
        End If
        Return isLengthAccepted
    End Function

    Public Overloads Shared Function isLengthAccepted(ByVal sField As String, ByVal sTable As String, ByVal dValue As Double, ByRef sErrReply As String) As Boolean
        isLengthAccepted = True
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim sSql As String = "SELECT col.precision, col.scale FROM sys.tables ta INNER JOIN sys.columns col ON col.object_id=ta.object_id WHERE col.name='" & sField & "' AND ta.name='" & sTable & "'"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim xCmd As New SqlCommand(sSql, conn)
        Dim iPrec, iScale As Integer
        Try
            Dim xRdr As SqlDataReader = xCmd.ExecuteReader
            While xRdr.Read
                iPrec = CInt(xRdr.Item("precision").ToString)
                iScale = CInt(xRdr.Item("scale").ToString)
            End While
            xRdr.Close()
        Catch ex As Exception
            iPrec = 0 : iScale = 0
            sErrReply = "- Can't check the result. Please check that the parameter has been assigned correctly!"
            isLengthAccepted = False
        End Try
        conn.Close()
        If iPrec > 0 And iScale >= 0 Then
            Dim sTextValue As String = ""
            For C1 As Integer = 0 To iPrec - iScale - 1
                sTextValue &= "9"
            Next
            If iScale > 0 Then
                sTextValue &= "."
                For C1 As Integer = 0 To iScale - 1
                    sTextValue &= "9"
                Next
            End If
            If dValue > ToDouble(sTextValue) Then
                sErrReply = ToMaskEdit(ToDouble(sTextValue), iScale)
                isLengthAccepted = False
            End If
        End If
        Return isLengthAccepted
    End Function

    Public Shared Function GetLastPeriod(ByVal sPeriod As String) As String
        GetLastPeriod = ""
        If sPeriod <> "" Then
            If sPeriod.Trim.Length = 6 Then
                If ToInteger(Right(sPeriod, 2)) = 1 Then
                    GetLastPeriod = Format(ToInteger(Left(sPeriod, 4)) - 1, "0000") & "12"
                Else
                    GetLastPeriod = Left(sPeriod, 4) & Format(ToInteger(Right(sPeriod, 2)) - 1, "00")
                End If
            End If
        End If
    End Function

    Public Shared Function SortingDataTable(ByVal dtObject As DataTable, ByVal sSortCmd As String) As DataTable
        dtObject.DefaultView.Sort = sSortCmd
        SortingDataTable = dtObject.DefaultView.ToTable()
    End Function

    Public Overloads Shared Function GetActivePeriode() As String
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand(" SELECT TOP 1 periodacctg FROM QL_crdstock WHERE CONVERT(CHAR(10),closingdate,103) ='01/01/1900'", conn)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        GetActivePeriode = xCmd.ExecuteScalar
        conn.Close()

        Return GetActivePeriode
    End Function

    Public Overloads Shared Function CheckYearIsValid(ByVal ddate As Date) As Boolean
        Dim min As Integer = 1990
        Dim max As Integer = 2072

        CheckYearIsValid = False
        If ddate.Year < min Then
            CheckYearIsValid = True
        End If
        If ddate.Year > max Then
            CheckYearIsValid = True
        End If

        Return CheckYearIsValid
    End Function

    Public Shared Function isPeriodAcctgClosed(ByVal sCmpCode As String, ByVal sDate As String) As Boolean
        isPeriodAcctgClosed = False
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim sPeriod As String = ""
        Dim sErr As String = ""
        If sDate = "" Then
            sPeriod = GetDateToPeriodAcctg(GetServerTime())
        Else
            If Not IsValidDate(sDate, "MM/dd/yyyy", sErr) Then
                sPeriod = GetDateToPeriodAcctg(GetServerTime())
            Else
                sPeriod = GetDateToPeriodAcctg(CDate(sDate))
            End If
        End If
        sSql = "SELECT COUNT(*) FROM QL_crdgl WHERE cmpcode='" & sCmpCode & "' AND crdglflag='CLOSED' AND periodacctg='" & sPeriod & "'"
        xCmd.CommandText = sSql
        If xCmd.ExecuteScalar > 0 Then
            isPeriodAcctgClosed = True
        End If
        conn.Close()
        Return isPeriodAcctgClosed
    End Function

    Public Shared Function GetQueryUpdateStockValue(ByVal dQty As Double, ByVal dQty_unitbesar As Double, ByVal dValIDR As Double, ByVal dValUSD As Double, ByVal sFormType As String, ByVal dLastDate As Date, ByVal sLastUpdUser As String, ByVal sCmpCode As String, ByVal sPeriod As String, ByVal iRefOid As Integer) As String
        Return "UPDATE QL_stockvalue SET stockqty=stockqty + " & dQty & ", stockvalueidr=ISNULL((((stockvalueidr * stockqty) + " & dQty * dValIDR & ") / NULLIF((stockqty + " & dQty & "), 0)), 0), stockvalueusd=ISNULL((((stockvalueusd * stockqty) + " & dQty * dValUSD & ") / NULLIF((stockqty + " & dQty & "), 0)), 0), stockqty_unitbesar=stockqty_unitbesar + " & dQty_unitbesar & ", lasttranstype='" & sFormType & "', lasttransdate='" & dLastDate & "', upduser='" & sLastUpdUser & "', updtime=CURRENT_TIMESTAMP, backupqty=stockqty, backupqty_unitbesar=stockqty_unitbesar, backupvalueidr=stockvalueidr, backupvalueusd=stockvalueusd WHERE cmpcode='" & sCmpCode & "' AND periodacctg='" & sPeriod & "' AND refoid=" & iRefOid & " "
    End Function

    Public Shared Function GetQueryInsertStockValue(ByVal dQty As Double, ByVal dQty_unitbesar As Double, ByVal dValIDR As Double, ByVal dValUSD As Double, ByVal sFormType As String, ByVal dLastDate As Date, ByVal sLastUpdUser As String, ByVal sCmpCode As String, ByVal sPeriod As String, ByVal iRefOid As Integer, ByVal iOid As Integer) As String
        Return "INSERT INTO QL_stockvalue (cmpcode, stockvalueoid, periodacctg, refoid, stockqty, stockqty_unitbesar, stockvalueidr, stockvalueusd, lasttranstype, lasttransdate, note, upduser, updtime, backupqty, backupqty_unitbesar, backupvalueidr, backupvalueusd, closeflag) VALUES ('" & sCmpCode & "', " & iOid & ", '" & sPeriod & "', " & iRefOid & ", " & dQty & ", " & dQty_unitbesar & ", " & dValIDR & ", " & dValUSD & ", '" & sFormType & "', '" & dLastDate & "', '', '" & sLastUpdUser & "', CURRENT_TIMESTAMP, 0, 0, 0, 0, '')"
    End Function

    Public Shared Function GetUnitConverter(ByVal iRefOid As Integer, ByVal iUnitOid As Integer, ByVal dQty As Double, ByRef dQty_unitkecil As Double, ByRef dQty_unitbesar As Double) As Double
        If iUnitOid = GetStrData("SELECT itemunit1 FROM QL_mstitem WHERE itemoid='" & iRefOid & "'") Then
            dQty_unitkecil = dQty
            dQty_unitbesar = ToMaskEdit(ToDouble(dQty) / ToDouble(GetStrData("SELECT unit3unit1conversion FROM QL_mstitem WHERE itemoid=" & iRefOid & "")), 4)
        ElseIf iUnitOid = GetStrData("SELECT itemunit2 FROM QL_mstitem WHERE itemoid='" & iRefOid & "'") Then
            dQty_unitkecil = ToMaskEdit(ToDouble(dQty) * ToDouble(GetStrData("SELECT unit2unit1conversion FROM QL_mstitem WHERE itemoid=" & iRefOid & "")), 4)
            dQty_unitbesar = ToMaskEdit(ToDouble(dQty_unitkecil) / ToDouble(GetStrData("SELECT unit3unit1conversion FROM QL_mstitem WHERE itemoid=" & iRefOid & "")), 4)
        Else
            dQty_unitkecil = ToMaskEdit(ToDouble(dQty) * ToDouble(GetStrData("SELECT unit3unit1conversion FROM QL_mstitem WHERE itemoid=" & iRefOid & "")), 4)
            dQty_unitbesar = ToDouble(dQty)
        End If
    End Function

    Public Shared Function SetDefaultAppUser(ByVal sTable As String, ByVal sCmpCode As String, ByVal sUser As String, ByRef oDDLObject As DropDownList, Optional ByVal sExcludeUser As String = "''") As String
        Dim sSql As String = "SELECT TOP 1 app.approvaluser FROM QL_approval app INNER JOIN " & sTable & " tbl ON tbl.cmpcode=app.cmpcode AND tbl." & sTable.Replace("QL_", "").Replace("trn", "").Replace("brt", "") & "oid=app.oid AND tbl.approvalcode=app.approvaluser WHERE app.cmpcode='" & sCmpCode & "' AND tablename='" & sTable & "' AND requestuser='" & sUser & "' AND app.approvaluser NOT IN (" & sExcludeUser & ") ORDER BY app.approvaldate DESC"
        Dim sAppUser As String = GetStrData(sSql)
        If sAppUser <> "" Then
            Dim isFound As Boolean = False
            For C1 As Integer = 0 To oDDLObject.Items.Count - 1
                If oDDLObject.Items(C1).Value = sAppUser Then
                    isFound = True : Exit For
                End If
            Next
            If Not isFound Then
                sExcludeUser &= ", '" & sAppUser & "'"
                Return SetDefaultAppUser(sTable, sCmpCode, sUser, oDDLObject, sExcludeUser)
            Else
                Return sAppUser
            End If
        Else
            Return "-"
        End If
    End Function

    Public Shared Function GetNewTaxValue(ByVal trn_date As String) As Decimal
        Dim result As Decimal = 10D
        If CDate(trn_date) >= CDate(New Date(2022, 4, 1)) Then result = 11D

        Return result
    End Function

    Public Shared Function GetNewTaxValueInclude(ByVal trn_date As String) As Decimal
        Dim result As Decimal = 1.1D
        If CDate(trn_date) >= CDate(New Date(2022, 4, 1)) Then result = 1.11D

        Return result
    End Function
End Class
