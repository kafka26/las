Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Public Class ClassProcedure
    Inherits System.Web.UI.Page

    Public Sub SetFocusToControl(ByVal pPage As System.Web.UI.Page, ByVal cControl As System.Web.UI.Control)
        Dim cs As ScriptManager = ScriptManager.GetCurrent(pPage)
        cs.SetFocus(cControl.ClientID)
    End Sub

    Public Sub SetModalPopUpExtender(ByVal btnTargetButton As System.Web.UI.WebControls.Button, ByVal pnlTargetPanel As System.Web.UI.WebControls.Panel, ByVal mpeTargetModalPopUp As AjaxControlToolkit.ModalPopupExtender, ByVal bModalPopUpState As Boolean)
        btnTargetButton.Visible = bModalPopUpState
        pnlTargetPanel.Visible = bModalPopUpState
        If bModalPopUpState Then
            mpeTargetModalPopUp.Show()
        Else
            mpeTargetModalPopUp.Hide()
        End If
    End Sub

    Public Sub CheckRegionalSetting()
        If (Now.ToLongTimeString.IndexOf("AM") = -1 Or Now.ToLongTimeString.IndexOf("PM") = -1) = False Then
            Response.Redirect("~\Other\login.aspx")
        End If
    End Sub

    Public Sub DisposeGridView(ByVal objDisposedGridView As GridView)
        objDisposedGridView.SelectedIndex = -1
        objDisposedGridView.DataSource = Nothing
        objDisposedGridView.DataBind()
    End Sub

    Public Sub SetDBLogonForReport(ByVal reportDoc As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal serverName As String, ByVal dbName As String)
        Dim connection, connection2 As CrystalDecisions.Shared.IConnectionInfo
        ' Set Database Logon to main report
        For Each connection In reportDoc.DataSourceConnections
            connection.SetConnection(serverName, dbName, True)
            ' Set Database Logon to subreport
            Dim subreport As CrystalDecisions.CrystalReports.Engine.ReportDocument
            For Each subreport In reportDoc.Subreports
                For Each connection2 In subreport.DataSourceConnections
                    connection2.SetConnection(serverName, dbName, True)
                Next
            Next
        Next
    End Sub

    Public Sub SetDBLogonForReport(ByVal reportDoc As CrystalDecisions.CrystalReports.Engine.ReportDocument)
        Dim myTables As CrystalDecisions.CrystalReports.Engine.Tables = reportDoc.Database.Tables
        Dim crConnInfo As New CrystalDecisions.Shared.ConnectionInfo()
        With crConnInfo
            .ServerName = System.Configuration.ConfigurationManager.AppSettings("DB-Server")
            .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("DB-Name")
            .IntegratedSecurity = True
        End With
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As CrystalDecisions.Shared.TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = crConnInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Public Sub ResetSessionOnForm()
        '--> simpan session k variabel spy tidak hilang
        Dim sUserID As String = Session("UserID")
        Dim dtSpecAccess As DataTable = Session("SpecialAccess")
        Dim dtRole As DataTable = Session("Role")
        Dim sCmpcode As String = Session("CompnyCode")
        Session.Clear()  ' -->>  clear all session 
        '--> insert lagi session yg disimpan dan create session 
        Session("UserID") = sUserID
        Session("SpecialAccess") = dtSpecAccess
        Session("Role") = dtRole
        Session("CompnyCode") = sCmpcode
    End Sub

    Public Sub CheckSessionAndRole(ByVal sCurrentPagePath As String, ByVal res As HttpResponse, Optional ByVal sAlsoCheckRole As Boolean = True)
        If Session("UserID") Is Nothing Or Session("UserID") = "" Then res.Redirect("~/Other/login.aspx")
        If Session("CompnyCode") Is Nothing Or Session("CompnyCode") = "" Then res.Redirect("~/Other/login.aspx")
        If Session("Role") Is Nothing Then res.Redirect("~/Other/login.aspx")
        If sAlsoCheckRole Then
            If Not (ClassFunction.checkPagePermission(sCurrentPagePath, Session("Role"))) Then
                res.Redirect("~\other\NotAuthorize.aspx")
            End If
        End If
    End Sub

End Class