<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="frmExpense.aspx.vb" Inherits="ReportForm_ExpenseReport" title="" EnableEventValidation="false" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Expense Report" CssClass="Title" ForeColor="SaddleBrown" Width="224px"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="center" style="height: 300px">
                <asp:UpdatePanel id="upReportForm" runat="server">
                    <contenttemplate>
<asp:Panel id="pnlReportForm" runat="server" Width="500px" DefaultButton="btnViewReport"><TABLE><TBODY><TR><TD class="Label" align=left runat="server" Visible="false"><asp:Label id="Label5" runat="server" Text="Bussiness Unit"></asp:Label></TD><TD class="Label" align=center runat="server" Visible="false"></TD><TD class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="FilterDDLDiv" runat="server" CssClass="inpTextDisabled" Enabled="False"></asp:DropDownList></TD></TR><TR><TD id="TD4" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label7" runat="server" Text="Type"></asp:Label></TD><TD id="TD3" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD2" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="DDLType" runat="server" CssClass="inpText"><asp:ListItem>Summary</asp:ListItem>
<asp:ListItem>Detail</asp:ListItem>
</asp:DropDownList>&nbsp; </TD></TR><TR><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text="Period"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="FilterPeriod1" runat="server" Width="60px" CssClass="inpText" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="CalPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="lblTo" runat="server" Text="to"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" Width="60px" CssClass="inpText" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="CalPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label4" runat="server" ForeColor="Red" Text="(MM/dd/yyyy)"></asp:Label></TD></TR><TR><TD class="Label" align=left>PIC</TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="PicDdl" runat="server" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD id="TD7" class="Label" align=left Visible="true"><asp:Label id="Label2" runat="server" Text="No. Bukti"></asp:Label></TD><TD id="TD6" class="Label" align=center Visible="true">:</TD><TD id="TD5" class="Label" align=left Visible="true"><asp:TextBox id="sono" runat="server" Width="178px" CssClass="inpText" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindSO" onclick="imbFindSO_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseSO" onclick="imbEraseSO_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;</TD></TR><TR><TD class="Label" align=left><asp:Label id="Label6" runat="server" Text="COA"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="shipmentno" runat="server" Width="178px" CssClass="inpText" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindShipment" onclick="imbFindShipment_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseShipment" onclick="imbEraseShipment_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="CBsTatus" runat="server" Width="80px" Text="Status"></asp:CheckBox></TD><TD class="Label" align=center><asp:Label id="lbSeptCurr" runat="server" Text=":"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="sTatusDDL" runat="server" CssClass="inpText" OnSelectedIndexChanged="sTatusDDL_SelectedIndexChanged"><asp:ListItem Value="Post">POST</asp:ListItem>
<asp:ListItem Value="In Process">IN PROCESS</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lbCurr" runat="server" Text="Currency Value"></asp:Label></TD><TD class="Label" align=center><asp:Label id="Label8" runat="server" Text=":"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="FilterCurrency" runat="server" CssClass="inpText"><asp:ListItem Value="IDR">INDONESIAN RUPIAH</asp:ListItem>
<asp:ListItem Value="USD">US DOLLAR</asp:ListItem>
<asp:ListItem Enabled="False">VALAS</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" Format="MM/dd/yyyy" PopupButtonID="CalPeriod1" TargetControlID="FilterPeriod1">
                                                                            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="FilterPeriod1" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear">
                                                                            </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" Format="MM/dd/yyyy" PopupButtonID="CalPeriod2" TargetControlID="FilterPeriod2">
                                                                            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" TargetControlID="FilterPeriod2" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear">
                                                                            </ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnExportToPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnExportToExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3></TD></TR><TR><TD style="HEIGHT: 77px" class="Label" vAlign=top align=center colSpan=3><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upReportForm"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgLoading" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR></TBODY></TABLE></asp:Panel> <CR:CrystalReportViewer id="crvReportForm" runat="server" Width="350px" AutoDataBind="True" HasViewList="False" HasCrystalLogo="False" HasPrintButton="False" HasExportButton="False" HasToggleGroupTreeButton="False" HasDrillUpButton="False" DisplayGroupTree="False" OnNavigate="crvReportForm_Navigate"></CR:CrystalReportViewer>&nbsp; 
</contenttemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="btnExportToPdf"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnExportToExcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnViewReport"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="FilterDDLDiv"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td align="center" style="height: 226px">
                <asp:UpdatePanel id="upListSO" runat="server">
                    <contenttemplate>
<asp:Panel id="PanelListSO" runat="server" Width="600px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSO" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of No. Bukti"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListSO" runat="server" Width="100%" DefaultButton="btnFindListSO"><asp:Label id="LblFilter" runat="server" Text="Filter :"></asp:Label> <asp:DropDownList id="DDLFilterListSO" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="sono">Cashbank No</asp:ListItem>
<asp:ListItem Value="somstnote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListSO" runat="server" Width="150px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListSO" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" OnClick="btnFindListSO_Click"></asp:ImageButton> <asp:ImageButton id="btnViewAllListSO" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" OnClick="btnViewAllListSO_Click"></asp:ImageButton> </asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAllSO" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" OnClick="btnSelectAllSO_Click"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneSO" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" OnClick="btnSelectNoneSO_Click"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedSO" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" OnClick="btnViewCheckedSO_Click"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3>&nbsp;<asp:GridView id="gvListSO" runat="server" BorderColor="Black" ForeColor="#333333" Width="98%" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" DataKeyNames="somstoid,sono,sodate,somstnote" OnPageIndexChanging="gvListSO_PageIndexChanging" OnRowDataBound="gvListSO_RowDataBound" PageSize="8" BorderStyle="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:CheckBox id="cbLMSO" runat="server" ToolTip='<%# eval("somstoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="somstoid" HeaderText="Draft No">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sono" HeaderText="Cashbank No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sodate" HeaderText="Cashbank Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="somstnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
&nbsp;
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListSO" runat="server" OnClick="lkbAddToListListSO_Click">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbListSO" runat="server" OnClick="lkbListSO_Click">[ Cancel & Close ]</asp:LinkButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSO" runat="server" TargetControlID="btnHiddenListSO" PopupDragHandleControlID="lblListSO" BackgroundCssClass="modalBackground" PopupControlID="PanelListSO">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListSO" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel><br />
                <asp:UpdatePanel id="upListShipment" runat="server">
                    <contenttemplate>
<asp:Panel id="PanelListShipment" runat="server" Width="600px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListShipment" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of COA"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListShipment" runat="server" Width="100%" DefaultButton="btnFindListShipment"><asp:Label id="Label245" runat="server" Text="Filter :"></asp:Label> <asp:DropDownList id="DDLFilterListShipment" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="shipmentno">Acctg Code</asp:ListItem>
<asp:ListItem Value="custname">Acctg Desc</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListShipment" runat="server" Width="150px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListShipment" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" OnClick="btnFindListShipment_Click"></asp:ImageButton> <asp:ImageButton id="btnViewAllListShipment" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" OnClick="btnViewAllListShipment_Click"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAllShipment" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" OnClick="btnSelectAllShipment_Click"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneShipment" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" OnClick="btnSelectNoneShipment_Click"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedShipment" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" OnClick="btnViewCheckedShipment_Click"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListShipment" runat="server" BorderColor="Black" ForeColor="#333333" Width="98%" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" DataKeyNames="shipmentmstoid,shipmentno,custname" OnPageIndexChanging="gvListShipment_PageIndexChanging" PageSize="8" BorderStyle="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:CheckBox id="cbLMSO" runat="server" ToolTip='<%# eval("shipmentmstoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="shipmentmstoid" HeaderText="Draft No" Visible="False">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="shipmentno" HeaderText="Acctg Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Acctg Desc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD style="HEIGHT: 15px" align=center colSpan=3><asp:LinkButton id="lkbAddToListListShipment" runat="server" OnClick="lkbAddToListListShipment_Click">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbListShipment" runat="server" OnClick="lkbListShipment_Click">[ Cancel & Close ]</asp:LinkButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListShipment" runat="server" TargetControlID="btnHiddenListShipment" PopupDragHandleControlID="lblListShipment" BackgroundCssClass="modalBackground" PopupControlID="PanelListShipment"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListShipment" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel><br />
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" DropShadow="True" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>

