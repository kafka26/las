<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="frmPI.aspx.vb" Inherits="ReportForm_frmPI" title="" EnableEventValidation="false" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Purchase Invoice Report" CssClass="Title" ForeColor="SaddleBrown"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="center">
            </td>
        </tr>
        <tr>
            <td align="center">
                                                    <asp:UpdatePanel ID="upReportForm" runat="server">
                                                        <ContentTemplate>
<asp:Panel id="pnlReportForm" runat="server" Width="100%" DefaultButton="btnSearchSupplier" __designer:wfdid="w47"><TABLE><TBODY><TR><TD id="TD7" align=left runat="server" Visible="false"><asp:Label id="Label5" runat="server" Text="Bussiness Unit" Width="88px" __designer:wfdid="w48"></asp:Label></TD><TD id="TD9" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD8" align=left runat="server" Visible="false"><asp:DropDownList id="FilterDDLDiv" runat="server" CssClass="inpText" __designer:wfdid="w49" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label7" runat="server" Text="Type" __designer:wfdid="w50"></asp:Label></TD><TD class="Label" align=center>:</TD><TD align=left><asp:DropDownList id="FilterType" runat="server" CssClass="inpText" Width="250px" __designer:wfdid="w51" AutoPostBack="True"><asp:ListItem>SUMMARY</asp:ListItem>
<asp:ListItem>DETAIL</asp:ListItem>
<asp:ListItem Value="PP">PEMBELIAN &amp; PEMBAYARAN</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label14" runat="server" Text="Tax Type" __designer:wfdid="w50"></asp:Label></TD><TD class="Label" align=center>:</TD><TD align=left><asp:DropDownList id="DDLTaxType" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w1"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>TAX</asp:ListItem>
<asp:ListItem>NON TAX</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label3" runat="server" Text="Period" __designer:wfdid="w52"></asp:Label> </TD><TD class="Label" align=center>:</TD><TD align=left><asp:TextBox id="txtStart" runat="server" CssClass="inpText" Width="60px" __designer:wfdid="w53"></asp:TextBox> <asp:ImageButton id="imbStart" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w54"></asp:ImageButton> <asp:Label id="lblTo" runat="server" Text="to" __designer:wfdid="w55"></asp:Label> <asp:TextBox id="txtFinish" runat="server" CssClass="inpText" Width="60px" __designer:wfdid="w56"></asp:TextBox> <asp:ImageButton id="imbFinish" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w57"></asp:ImageButton> <asp:Label id="lblMMDD" runat="server" CssClass="Important" Text="(MM/DD/yyyy)" __designer:wfdid="w58"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label6" runat="server" Text="Currency" __designer:wfdid="w67"></asp:Label></TD><TD class="Label" align=center>:</TD><TD align=left><asp:DropDownList id="FilterCurrency" runat="server" CssClass="inpText" __designer:wfdid="w364"></asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label9" runat="server" Text="No. PO" __designer:wfdid="w52"></asp:Label> <asp:Label id="PomstOid" runat="server" __designer:wfdid="w64" Visible="False"></asp:Label></TD><TD class="Label" align=center>:</TD><TD align=left><asp:TextBox id="pono" runat="server" CssClass="inpTextDisabled" Width="175px" __designer:wfdid="w90" Enabled="False"></asp:TextBox> <asp:ImageButton id="ImageButton2" onclick="ImageButton2_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w93"></asp:ImageButton> <asp:ImageButton id="ImbErasePO" onclick="ImbErasePO_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w95"></asp:ImageButton>&nbsp;</TD></TR><TR><TD align=left><asp:Label id="Label10" runat="server" Text="No. Nota" __designer:wfdid="w52"></asp:Label> <asp:Label id="BeliMstOid" runat="server" __designer:wfdid="w64" Visible="False"></asp:Label></TD><TD class="Label" align=center>:</TD><TD align=left><asp:TextBox id="BeliNo" runat="server" CssClass="inpTextDisabled" Width="175px" __designer:wfdid="w91" Enabled="False"></asp:TextBox> <asp:ImageButton id="BtnSearchPI" onclick="BtnSearchPI_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w92"></asp:ImageButton> <asp:ImageButton id="BtnClearPI" onclick="BtnClearPI_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w94"></asp:ImageButton> </TD></TR><TR><TD align=left><asp:Label id="Label2" runat="server" Text="Supplier" __designer:wfdid="w59"></asp:Label></TD><TD class="Label" align=center>:</TD><TD align=left><asp:TextBox id="FilterTextSupplier" runat="server" CssClass="inpTextDisabled" Width="175px" __designer:wfdid="w60" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchSupplier" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w61"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearSupplier" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w62"></asp:ImageButton></TD></TR><TR><TD id="TD3" align=left runat="server" Visible="false"><asp:CheckBox id="cbType" runat="server" Text="Material" __designer:wfdid="w1" Visible="False" AutoPostBack="True"></asp:CheckBox></TD><TD id="TD1" class="Label" align=center runat="server" Visible="false"></TD><TD id="TD2" align=left runat="server" Visible="false"><asp:CheckBox id="chkEmpty" runat="server" Text="Hide Supplier with 0 A/P amount" __designer:wfdid="w63" Checked="True"></asp:CheckBox></TD></TR><TR><TD align=left runat="server" Visible="true"><asp:Label id="Label11" runat="server" Text="Material" __designer:wfdid="w59"></asp:Label></TD><TD class="Label" align=center runat="server" Visible="true"><asp:Label id="LblMat" runat="server" Text=":" __designer:wfdid="w59"></asp:Label></TD><TD align=left runat="server" Visible="true"><asp:TextBox id="matcode" runat="server" CssClass="inpTextDisabled" Width="178px" __designer:wfdid="w3" Enabled="False" TextMode="MultiLine" Height="25px"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindMat" onclick="imbFindMat_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w4" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseMat" onclick="imbEraseMat_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w5" Visible="False"></asp:ImageButton></TD></TR><TR><TD align=left><asp:Label id="LblStatus" runat="server" __designer:wfdid="w64">Status</asp:Label> <asp:Label id="suppoid" runat="server" __designer:wfdid="w64" Visible="False"></asp:Label></TD><TD class="Label" align=center><asp:Label id="Label13" runat="server" Text=":" __designer:wfdid="w2"></asp:Label></TD><TD align=left><asp:DropDownList id="StatusDDL" runat="server" CssClass="inpText" __designer:wfdid="w3"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>Approved</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Closed</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="TD6" align=left runat="server" Visible="false"><asp:Label id="Label8" runat="server" Text="COA" __designer:wfdid="w69"></asp:Label></TD><TD id="TD5" class="Label" align=center runat="server" Visible="false"><asp:Label id="Label12" runat="server" __designer:wfdid="w1" Visible="False">:</asp:Label></TD><TD id="TD4" align=left runat="server" Visible="false"><asp:DropDownList id="FilterDDLCOA" runat="server" CssClass="inpText" __designer:wfdid="w70">
            </asp:DropDownList></TD></TR><TR><TD align=left colSpan=3><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w71"></asp:ImageButton> <asp:ImageButton id="btnExportToPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w72"></asp:ImageButton> <asp:ImageButton id="btnExportToExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w73"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w74"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=3><ajaxToolkit:CalendarExtender id="ceStart" runat="server" __designer:wfdid="w75" TargetControlID="txtStart" Format="MM/dd/yyyy" PopupButtonID="imbStart"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeStart" runat="server" __designer:wfdid="w76" TargetControlID="txtStart" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="ceFin" runat="server" __designer:wfdid="w77" TargetControlID="txtFinish" Format="MM/dd/yyyy" PopupButtonID="imbFinish"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeFin" runat="server" __designer:wfdid="w78" TargetControlID="txtFinish" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <asp:RadioButtonList id="rbSupplier" runat="server" __designer:wfdid="w79" Visible="False" AutoPostBack="True" RepeatDirection="Horizontal"><asp:ListItem Selected="True">ALL</asp:ListItem>
<asp:ListItem>SELECT</asp:ListItem>
</asp:RadioButtonList> <asp:DropDownList id="FilterDDLMonth" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w80" Visible="False"></asp:DropDownList> <asp:DropDownList id="FilterDDLYear" runat="server" CssClass="inpText" Width="75px" __designer:wfdid="w81" Visible="False"></asp:DropDownList> <TABLE cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="WIDTH: 1px" align=left></TD><TD align=left>&nbsp; </TD></TR></TBODY></TABLE><asp:Panel id="pnlSupplier" runat="server" __designer:wfdid="w87"></asp:Panel></TD></TR><TR><TD style="HEIGHT: 50px" vAlign=top align=center colSpan=3><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w82" AssociatedUpdatePanelID="upReportForm"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgLoading" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w83"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> <asp:GridView id="gvSupplier" runat="server" __designer:wfdid="w366" Visible="False" DataKeyNames="suppoid,suppname" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="1">
<PagerSettings Mode="NumericFirstLast"></PagerSettings>
<Columns>
<asp:TemplateField><EditItemTemplate>
            <asp:CheckBox ID="CheckBox1" runat="server" />
        
</EditItemTemplate>
<ItemTemplate>
            <asp:CheckBox ID="chkSelect" runat="server" Checked='<%# Eval("selected") %>' ToolTip='<%# Eval("suppoid") %>' />
        
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="suppcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="False" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label4" runat="server" ForeColor="Red" Text="No data found."></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White"></SelectedRowStyle>

<HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White"></HeaderStyle>
</asp:GridView> </TD></TR></TBODY></TABLE></asp:Panel> <CR:CrystalReportViewer id="crvReportForm" runat="server" __designer:dtid="1125899906842650" __designer:wfdid="w85" AutoDataBind="True" HasViewList="False" HasCrystalLogo="False" HasPrintButton="False" HasExportButton="False" HasToggleGroupTreeButton="False" HasDrillUpButton="False"></CR:CrystalReportViewer> 
</ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnViewReport" />
                                                            <asp:PostBackTrigger ControlID="btnExportToPdf" />
                                                            <asp:PostBackTrigger ControlID="btnExportToExcel" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                            &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center">
                <asp:UpdatePanel id="UpdatePanel3" runat="server">
                    <contenttemplate>
<asp:Panel id="pnlListReg" runat="server" CssClass="modalBox" Width="700px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListReg" runat="server" Font-Size="Medium" Font-Bold="True" Text="List Of Purchase Order" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListReg" runat="server" DefaultButton="btnFindListReg" Width="100%">Filter : <asp:DropDownList id="FilterDDLListReg" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="pom.pono">No. PO</asp:ListItem>
<asp:ListItem Value="s.suppname">Supp. Name</asp:ListItem>
<asp:ListItem Value="pom.potaxtype">PO Type</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListReg" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListReg" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" OnClick="btnFindListReg_Click"></asp:ImageButton> <asp:ImageButton id="btnAllListReg" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" OnClick="btnAllListReg_Click"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListReg" runat="server" ForeColor="#333333" Width="100%" PageSize="5" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" DataKeyNames="pomstoid,pono,podate,curroid,suppname,suppoid,poflag,pogroup,potype,potaxtype,potaxamt,povat" OnSelectedIndexChanged="gvListReg_SelectedIndexChanged" OnPageIndexChanging="gvListReg_PageIndexChanging" AllowSorting="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="pono" HeaderText="PO No." SortExpression="pono">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="podate" HeaderText="PO Date" SortExpression="podate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="potaxtype" HeaderText="Tax Type">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="pogroup" HeaderText="PO Type">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                                    <asp:Label ID="Label114" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListReg" onclick="lkbCloseListReg_Click" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpeListReg" runat="server" TargetControlID="btnHideListReg" PopupControlID="pnlListReg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListReg" Drag="True">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListReg" runat="server" ForeColor="Transparent" Visible="False"></asp:Button>
</contenttemplate>
                </asp:UpdatePanel><asp:UpdatePanel id="upListAP" runat="server"><contenttemplate>
<asp:Panel id="pnlListAP" runat="server" CssClass="modalBox" Width="600px" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListAP" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Purchase Invoice"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListAP" runat="server" DefaultButton="btnFindListAP" Width="100%">Filter : <asp:DropDownList id="FilterDDLListAP" runat="server" CssClass="inpText"><asp:ListItem Value="bm.trnbelino">No. Nota</asp:ListItem>
<asp:ListItem Value="bm.trnbelitype">Type Nota</asp:ListItem>
<asp:ListItem Value="bm.trnbelimststatus">Status</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListAP" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListAP" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" OnClick="btnFindListAP_Click"></asp:ImageButton> <asp:ImageButton id="btnAllListAP" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" OnClick="btnAllListAP_Click"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListAP" runat="server" Width="100%" PageSize="5" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" DataKeyNames="trnbelimstoid,trnbelino" OnSelectedIndexChanged="gvListAP_SelectedIndexChanged" OnPageIndexChanging="gvListAP_PageIndexChanging" Height="64px">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnbelino" HeaderText="No. Nota">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelidate" HeaderText="Tgl. Nota">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelimsttaxtype" HeaderText="Tax Type">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelitype" HeaderText="Type">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListAP" onclick="lkbCloseListAP_Click" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpeListAP" runat="server" TargetControlID="btnHideListAP" PopupControlID="pnlListAP" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListAP" Drag="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="btnHideListAP" runat="server" Visible="False"></asp:Button>
</contenttemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel id="upListSupp" runat="server">
                    <contenttemplate>
<asp:Panel id="pnlListSupp" runat="server" CssClass="modalBox" Width="600px" Visible="False"><TABLE width="100%"><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSupp" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Supplier"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListSupp" runat="server" DefaultButton="btnFindListSupp" Width="100%">Filter : <asp:DropDownList id="FilterDDLListSupp" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="suppcode">Code</asp:ListItem>
<asp:ListItem Value="suppname">Name</asp:ListItem>
<asp:ListItem Value="suppaddr">Address</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListSupp" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListSupp" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" OnClick="btnFindListSupp_Click"></asp:ImageButton> <asp:ImageButton id="btnAllListSupp" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" OnClick="btnAllListSupp_Click"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListSupp" runat="server" Width="99%" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" DataKeyNames="suppoid,suppcode,suppname" PageSize="5" OnPageIndexChanging="gvListSupp_PageIndexChanging" OnSelectedIndexChanged="gvListSupp_SelectedIndexChanged">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Supp. Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supp. Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListSupp" runat="server" OnClick="lkbCloseListSupp_Click">[ Cancel & Close ]</asp:LinkButton></TD></TR></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpeListSupp" runat="server" TargetControlID="btnHideListSupp" PopupControlID="pnlListSupp" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListSupp" Drag="True">
            </ajaxToolkit:ModalPopupExtender><asp:Button id="btnHideListSupp" runat="server" Visible="False"></asp:Button>
</contenttemplate>
                </asp:UpdatePanel><asp:UpdatePanel id="upListMat" runat="server"><contenttemplate>
<asp:Panel id="pnlListMat" runat="server" CssClass="modalBox" Width="900px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Material" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMat" runat="server" Width="100%" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="matcode">Code</asp:ListItem>
<asp:ListItem Value="oldcode">Old Code</asp:ListItem>
<asp:ListItem Value="matlongdesc">Description</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" CssClass="inpText" Width="200px"></asp:TextBox> <asp:ImageButton id="btnFindListMat" onclick="btnFindListMat_Click" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" onclick="btnAllListMat_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAll" onclick="btnSelectAll_Click" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNone" onclick="btnSelectNone_Click" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewChecked" onclick="btnViewChecked_Click" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:UpdateProgress id="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat"><ProgressTemplate>
<TABLE style="WIDTH: 200px"><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple">Load Data, Please Wait ...</TD></TR><TR><TD><asp:Image id="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif"></asp:Image></TD></TR></TABLE>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD class="Label" align=left colSpan=3>Show <asp:TextBox id="tbData" runat="server" CssClass="inpText" Width="25px"></asp:TextBox> Data Per Page <asp:LinkButton id="lbShowData" runat="server">>> View</asp:LinkButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat" runat="server" BorderColor="Black" ForeColor="#333333" Width="96%" DataKeyNames="matcode" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" PageSize="8" OnPageIndexChanging="gvListMat_PageIndexChanging" BorderStyle="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:CheckBox id="cbLM" runat="server" ToolTip='<%# eval("matoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="matcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="oldcode" HeaderText="Old Code">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="matlongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
&nbsp;
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListMat" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbCloseListMat" onclick="lkbCloseListMat_Click" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE><ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" Drag="True" PopupDragHandleControlID="lblTitleListMat" BackgroundCssClass="modalBackground" PopupControlID="pnlListMat"></ajaxToolkit:ModalPopupExtender></asp:Panel> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Width="600px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 40px; TEXT-ALIGN: center"><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>

