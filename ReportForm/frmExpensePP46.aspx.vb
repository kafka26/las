Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_AR
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function FillDDLWithALL(ByRef oDDLObject As DropDownList, ByVal sSql As String) As Boolean
        ' To fill data in dropdown list, return true when all data exist
        FillDDLWithALL = True
        oDDLObject.Items.Clear()
        oDDLObject.Items.Add("ALL")
        oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = "ALL"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            oDDLObject.Items.Add(xreader.GetValue(1))
            oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()
        ' Cek if all data exits
        If oDDLObject.Items.Count = 0 Then
            FillDDLWithALL = False
        End If
        Return FillDDLWithALL
    End Function

    Private Function UpdateCheckedCust() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblCust") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCust")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListCust.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListCust.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "custoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblCust") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedCust2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCust")
            Dim dtTbl2 As DataTable = Session("TblCustView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListCust.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListCust.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "custoid=" & cbOid
                                dtView2.RowFilter = "custoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblCust") = dtTbl
                Session("TblCustView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedSO() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSO") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSO")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListSO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "somstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblSO") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedSO2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSO")
            Dim dtTbl2 As DataTable = Session("TblSOView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListSO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "somstoid=" & cbOid
                                dtView2.RowFilter = "somstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblSO") = dtTbl
                Session("TblSOView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedShipment() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblShipment") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblShipment")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListShipment.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListShipment.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "shipmentmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblShipment") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedShipment2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblShipmentView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblShipment")
            Dim dtTbl2 As DataTable = Session("TblShipmentView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListShipment.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListShipment.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "shipmentmstoid=" & cbOid
                                dtView2.RowFilter = "shipmentmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblShipment") = dtTbl
                Session("TblShipmentView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedAR() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblAR") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblAR")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListAR.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListAR.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "armstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblAR") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedAR2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblARView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblAR")
            Dim dtTbl2 As DataTable = Session("TblARView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListAR.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListAR.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "armstoid=" & cbOid
                                dtView2.RowFilter = "armstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblAR") = dtTbl
                Session("TblARView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matoid=" & cbOid
                                dtView2.RowFilter = "matoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindListCust()
        sSql = "SELECT 'False' AS checkvalue, custgroupoid custoid, custgroupcode custcode, custgroupname custname, custgroupaddr custaddr FROM QL_mstcustgroup WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' "
        If DDLBusUnit.SelectedValue <> "ALL" Then
            sSql &= " AND custgroupoid IN (SELECT DISTINCT c.custgroupoid from QL_trnjualmst arm INNER JOIN QL_mstcust c ON c.custoid=arm.custoid LEFT JOIN QL_trnjualdtl ard ON arm.trnjualmstoid=ard.trnjualmstoid WHERE arm.cmpcode='" & DDLBusUnit.SelectedValue & "' "
        Else
            sSql &= " AND custgroupoid IN (SELECT DISTINCT c.custgroupoid from QL_trnjualmst arm INNER JOIN QL_mstcust c ON c.custoid=arm.custoid LEFT JOIN QL_trnjualdtl ard ON arm.trnjualmstoid=ard.trnjualmstoid WHERE arm.cmpcode LIKE '%' "
        End If

        If FilterDDLStatus.SelectedValue = "In Process" Then
            sSql &= " AND arm.trnjualstatus IN ('In Process','In Approval','Revised')"
        ElseIf FilterDDLStatus.SelectedValue = "Approved" Then
            sSql &= " AND arm.trnjualstatus IN ('Approved','Closed')"
        ElseIf FilterDDLStatus.SelectedValue = "Rejected" Then
            sSql &= " AND arm.trnjualstatus IN ('Rejected','Cancel')"
        End If
        If FilterDDLTax.SelectedValue <> "All" Then
            If FilterDDLTax.SelectedValue = "Non Tax" Then
                sSql &= " AND arm.trntaxtype='NT'"
            Else
                sSql &= " AND arm.trntaxtype<>'NT'"
            End If
        End If

        If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
            If IsValidPeriod() Then
                sSql &= " AND " & FilterDDLDate.SelectedValue & " >='" & FilterPeriod1.Text & " 00:00:00' AND " & FilterDDLDate.SelectedValue & " <='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        sSql &= ") ORDER BY custcode"
        Session("TblCust") = cKon.ambiltabel(sSql, "QL_mstcust")
    End Sub

    Private Sub BindListSO()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, som.somstoid somstoid, som.sono sono, som.sodate, CONVERT(VARCHAR(10), som.sodate, 101) AS sodate, s.custname AS custname, som.somststatus somststatus, som.somstnote somstnote FROM QL_trnsomst som INNER JOIN QL_trnsodtl sod ON som.somstoid=sod.somstoid INNER JOIN QL_mstcust s ON som.custoid=s.custoid AND s.activeflag='ACTIVE' "
        If DDLBusUnit.SelectedValue <> "ALL" Then
            sSql &= "WHERE som.somstoid IN (SELECT DISTINCT somstoid FROM QL_trnjualmst arm INNER JOIN QL_trnjualdtl ard ON arm.cmpcode=ard.cmpcode AND arm.trnjualmstoid=ard.trnjualmstoid INNER JOIN QL_trndomst shd ON shd.cmpcode=ard.cmpcode AND shd.domstoid=arm.domstoid INNER JOIN QL_mstcust c ON arm.custoid=c.custoid AND c.activeflag='ACTIVE' WHERE arm.cmpcode='" & DDLBusUnit.SelectedValue & "' "
        Else
            sSql &= "WHERE som.somstoid IN (SELECT DISTINCT somstoid FROM QL_trnjualmst arm INNER JOIN QL_trnjualdtl ard ON arm.cmpcode=ard.cmpcode AND arm.trnjualmstoid=ard.trnjualmstoid INNER JOIN QL_trndomst shd ON shd.cmpcode=ard.cmpcode AND shd.domstoid=arm.domstoid INNER JOIN QL_mstcust c ON arm.custoid=c.custoid AND c.activeflag='ACTIVE' WHERE arm.cmpcode LIKE '%' "
        End If

        If custcode.Text <> "" Then
            Dim scode() As String = Split(custcode.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " c.custcode = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If FilterDDLStatus.SelectedValue = "In Process" Then
            sSql &= " AND arm.trnjualstatus IN ('In Process','In Approval','Revised')"
        ElseIf FilterDDLStatus.SelectedValue = "Approved" Then
            sSql &= " AND arm.trnjualstatus IN ('Approved','Closed')"
        ElseIf FilterDDLStatus.SelectedValue = "Rejected" Then
            sSql &= " AND arm.trnjualstatus IN ('Rejected','Cancel')"
        End If
        If FilterDDLTax.SelectedValue <> "All" Then
            If FilterDDLTax.SelectedValue = "Non Tax" Then
                sSql &= " AND arm.trntaxtype='NT'"
            Else
                sSql &= " AND arm.trntaxtype<>'NT'"
            End If
        End If

        If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
            If IsValidPeriod() Then
                sSql &= " AND " & FilterDDLDate.SelectedValue & " >='" & FilterPeriod1.Text & " 00:00:00' AND " & FilterDDLDate.SelectedValue & " <='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If

        sSql &= ") ORDER BY som.sodate DESC, som.somstoid DESC"
        Session("TblSO") = cKon.ambiltabel(sSql, "QL_trnsomst")
    End Sub

    Private Sub BindListShipment()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, shm.domstoid shipmentmstoid, shm.dono shipmentno, shm.dodate, CONVERT(VARCHAR(10), shm.dodate, 101) AS shipmentdate, c.custname AS custname, shm.domststatus shipmentmststatus, shm.domstnote shipmentmstnote FROM QL_trndomst shm INNER JOIN QL_trndodtl shd ON shm.domstoid=shd.domstoid INNER JOIN QL_mstcust c ON shm.custoid=c.custoid AND c.activeflag='ACTIVE' "

        If DDLBusUnit.SelectedValue <> "ALL" Then
            sSql &= " WHERE shm.domstoid IN (SELECT DISTINCT arm.domstoid FROM QL_trnjualmst arm INNER JOIN QL_trndomst dod ON arm.cmpcode=dod.cmpcode AND dod.domstoid=arm.domstoid INNER JOIN QL_trnsomst som ON som.cmpcode=dod.cmpcode AND som.somstoid=dod.somstoid INNER JOIN QL_mstcust c ON arm.custoid=c.custoid AND c.activeflag='ACTIVE' WHERE arm.cmpcode='" & DDLBusUnit.SelectedValue & "' "
        Else
            sSql &= " WHERE shm.domstoid IN (SELECT DISTINCT arm.domstoid FROM QL_trnjualmst arm INNER JOIN QL_trndomst dod ON arm.cmpcode=dod.cmpcode AND dod.domstoid=arm.domstoid INNER JOIN QL_trnsomst som ON som.cmpcode=dod.cmpcode AND som.somstoid=dod.somstoid INNER JOIN QL_mstcust c ON arm.custoid=c.custoid AND c.activeflag='ACTIVE' WHERE arm.cmpcode LIKE '%' "
        End If

        If custcode.Text <> "" Then
            Dim scode() As String = Split(custcode.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " c.custcode = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        'Filter SO
        If FilterDDLType.SelectedValue = "Detail" Then
            If sono.Text <> "" Then
                Dim sSOno() As String = Split(sono.Text, ";")
                sSql &= "  AND ( "
                For c1 As Integer = 0 To sSOno.Length - 1
                    sSql &= " som.sono LIKE '%" & Tchar(sSOno(c1)) & "%'"
                    If c1 < sSOno.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            End If
        End If


        If FilterDDLStatus.SelectedValue = "In Process" Then
            sSql &= " AND arm.trnjualstatus IN ('In Process','In Approval','Revised')"
        ElseIf FilterDDLStatus.SelectedValue = "Approved" Then
            sSql &= " AND arm.trnjualstatus IN ('Approved','Closed')"
        ElseIf FilterDDLStatus.SelectedValue = "Rejected" Then
            sSql &= " AND arm.trnjualstatus IN ('Rejected','Cancel')"
        End If

        If FilterDDLTax.SelectedValue <> "All" Then
            If FilterDDLTax.SelectedValue = "Non Tax" Then
                sSql &= " AND arm.trntaxtype='NT'"
            Else
                sSql &= " AND arm.trntaxtype<>'NT'"
            End If
        End If

        If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
            If IsValidPeriod() Then
                sSql &= " AND " & FilterDDLDate.SelectedValue & " >='" & FilterPeriod1.Text & " 00:00:00' AND " & FilterDDLDate.SelectedValue & " <='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If

        sSql &= ") ORDER BY shm.dodate DESC, shm.domstoid DESC"
        Session("TblShipment") = cKon.ambiltabel(sSql, "QL_trndomst")
    End Sub

    Private Sub BindListAR()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, arm.trnjualmstoid armstoid, arm.trnjualno arno, arm.trnjualdate, CONVERT(VARCHAR(10), arm.trnjualdate, 101) AS ardate, c.custname AS custname, arm.trnjualstatus armststatus, arm.trnjualnote armstnote FROM QL_trnjualmst arm LEFT JOIN QL_trnjualdtl ard ON arm.trnjualmstoid=ard.trnjualmstoid INNER JOIN QL_mstcust c ON arm.custoid=c.custoid AND c.activeflag='ACTIVE' "
        If DDLBusUnit.SelectedValue <> "ALL" Then
            sSql &= "WHERE arm.cmpcode='" & DDLBusUnit.SelectedValue & "'"
        Else
            sSql &= "WHERE arm.cmpcode LIKE '%%'"
        End If

        If custcode.Text <> "" Then
            Dim scode() As String = Split(custcode.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " c.custcode = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If
        If FilterDDLTax.SelectedValue <> "All" Then
            If FilterDDLTax.SelectedValue = "Non Tax" Then
                sSql &= " AND arm.trntaxtype='NT'"
            Else
                sSql &= " AND arm.trntaxtype<>'NT'"
            End If
        End If
        If FilterDDLType.SelectedValue = "Detail" Then
            'Filter SO
            If sono.Text <> "" Then
                sSql &= " AND ard.dodtloid IN (SELECT shd.dodtloid FROM QL_trndodtl shd INNER JOIN QL_trnsomst som ON som.somstoid=dod.somstoid "
                Dim sSOno() As String = Split(sono.Text, ";")
                sSql &= " WHERE "
                For c1 As Integer = 0 To sSOno.Length - 1
                    sSql &= " som.sono LIKE '%" & Tchar(sSOno(c1)) & "%'"
                    If c1 < sSOno.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            End If
            'Filter Shipment
            If shipmentno.Text <> "" Then
                sSql &= " AND ard.domstoid IN (SELECT shm.domstoid FROM QL_trndomst shm "
                Dim sShipmentno() As String = Split(shipmentno.Text, ";")
                sSql &= " WHERE "
                For c1 As Integer = 0 To sShipmentno.Length - 1
                    sSql &= " shm.dono LIKE '%" & Tchar(sShipmentno(c1)) & "%'"
                    If c1 < sShipmentno.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            End If
        End If


        If FilterDDLStatus.SelectedValue = "In Process" Then
            sSql &= " AND arm.trnjualstatus IN ('In Process','In Approval','Revised')"
        ElseIf FilterDDLStatus.SelectedValue = "Approved" Then
            sSql &= " AND arm.trnjualstatus IN ('Approved','Closed')"
        ElseIf FilterDDLStatus.SelectedValue = "Rejected" Then
            sSql &= " AND arm.trnjualstatus IN ('Rejected','Cancel')"
        End If

        If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
            If IsValidPeriod() Then
                sSql &= " AND " & FilterDDLDate.SelectedValue & " >='" & FilterPeriod1.Text & " 00:00:00' AND " & FilterDDLDate.SelectedValue & " <='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If

        sSql &= " ORDER BY arm.trnjualdate DESC, arm.trnjualmstoid DESC"
        Session("TblAR") = cKon.ambiltabel(sSql, "QL_trnjualmst")

    End Sub

    Private Sub BindListMat()
        sSql = "SELECT DISTINCT'False' AS checkvalue, ard.itemoid matoid, m.itemoldcode oldcode, m.itemlongdescription matlongdesc , m.itemcode matcode, g2.gendesc unit FROM QL_trnjualdtl ard INNER JOIN QL_trnjualmst arm ON arm.cmpcode=ard.cmpcode AND arm.trnjualmstoid=ard.trnjualmstoid INNER JOIN QL_mstitem m ON m.itemoid=ard.itemoid INNER JOIN QL_mstgen g2 ON g2.genoid=ard.trnjualdtlunitoid INNER JOIN QL_mstcust c ON arm.custoid=c.custoid AND c.activeflag='ACTIVE' "

        If DDLBusUnit.SelectedValue <> "ALL" Then
            sSql &= "WHERE arm.cmpcode='" & DDLBusUnit.SelectedValue & "'"
        Else
            sSql &= "WHERE arm.cmpcode LIKE '%%'"
        End If

        If custcode.Text <> "" Then
            Dim scode() As String = Split(custcode.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " c.custcode = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If FilterDDLType.SelectedValue = "Detail" Then
            'Filter SO
            If sono.Text <> "" Then
                sSql &= " AND ard.dodtloid IN (SELECT shd.dodtloid FROM QL_trndodtl shd INNER JOIN QL_trnsomst som ON som.somstoid=shd.somstoid "
                Dim sSOno() As String = Split(sono.Text, ";")
                sSql &= " WHERE "
                For c1 As Integer = 0 To sSOno.Length - 1
                    sSql &= " som.sono LIKE '%" & Tchar(sSOno(c1)) & "%'"
                    If c1 < sSOno.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            End If
            'Filter Shipment
            If shipmentno.Text <> "" Then
                sSql &= " AND ard.domstoid IN (SELECT shm.domstoid FROM QL_trndomst shm "
                Dim sShipmentno() As String = Split(shipmentno.Text, ";")
                sSql &= " WHERE "
                For c1 As Integer = 0 To sShipmentno.Length - 1
                    sSql &= " shm.dono LIKE '%" & Tchar(sShipmentno(c1)) & "%'"
                    If c1 < sShipmentno.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            End If
        End If

        If arno.Text <> "" Then
            If DDLARNo.SelectedValue = "AR No." Then
                Dim sARno() As String = Split(arno.Text, ";")
                sSql &= " AND ("
                For c1 As Integer = 0 To sARno.Length - 1
                    sSql &= " arm.trnjualno LIKE '%" & Tchar(sARno(c1)) & "%'"
                    If c1 < sARno.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            Else
                Dim sARno() As String = Split(arno.Text, ";")
                sSql &= " AND ("
                For c1 As Integer = 0 To sARno.Length - 1
                    sSql &= " arm.trnjualmstoid = " & ToDouble(sARno(c1))
                    If c1 < sARno.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            End If
        End If

        If FilterDDLStatus.SelectedValue = "In Process" Then
            sSql &= " AND arm.trnjualstatus IN ('In Process','In Approval','Revised')"
        ElseIf FilterDDLStatus.SelectedValue = "Approved" Then
            sSql &= " AND arm.trnjualstatus IN ('Approved','Closed')"
        ElseIf FilterDDLStatus.SelectedValue = "Rejected" Then
            sSql &= " AND arm.trnjualstatus IN ('Rejected','Cancel')"
        End If

        If FilterDDLTax.SelectedValue <> "All" Then
            If FilterDDLTax.SelectedValue = "Non Tax" Then
                sSql &= " AND arm.trntaxtype='NT'"
            Else
                sSql &= " AND arm.trntaxtype<>'NT'"
            End If
        End If

        If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
            If IsValidPeriod() Then
                sSql &= " AND " & FilterDDLDate.SelectedValue & " >='" & FilterPeriod1.Text & " 00:00:00' AND " & FilterDDLDate.SelectedValue & " <='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If

        Session("TblMat") = cKon.ambiltabel(sSql, "QL_mstitem")
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        Dim sWhere As String = " WHERE 1=1 "
        Dim rptName As String = ""
        Try

            If FilterDDLType.SelectedValue = "Summary" Then
                If sType = "Print Excel" Then
                    report.Load(Server.MapPath(folderReport & "rptExpensePP46Xls.rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptExpensePP46Xls.rpt"))
                End If
                rptName = "ExpensePP46Report"
            End If

            If DDLBusUnit.SelectedValue <> "ALL" Then
                sWhere &= " AND jm.cmpcode='" & DDLBusUnit.SelectedValue & "'"
            Else
                sWhere &= " AND jm.cmpcode LIKE '%%'"
            End If

            If custcode.Text <> "" Then
                Dim scode() As String = Split(custcode.Text, ";")
                sWhere &= " AND ("
                For c1 As Integer = 0 To scode.Length - 1
                    sWhere &= " c.custcode = '" & Tchar(scode(c1)) & "'"
                    If c1 < scode.Length - 1 Then
                        sWhere &= " OR "
                    End If
                Next
                sWhere &= ")"
            End If

            If arno.Text <> "" Then
                If DDLARNo.SelectedValue = "AR No." Then
                    Dim sARno() As String = Split(arno.Text, ";")
                    sWhere &= " AND ("
                    For c1 As Integer = 0 To sARno.Length - 1
                        sWhere &= " jm.trnjualno LIKE '%" & Tchar(sARno(c1)) & "%'"
                        If c1 < sARno.Length - 1 Then
                            sWhere &= " OR "
                        End If
                    Next
                    sWhere &= ")"
                Else
                    Dim sARno() As String = Split(arno.Text, ";")
                    sWhere &= " AND ("
                    For c1 As Integer = 0 To sARno.Length - 1
                        sWhere &= " jm.trnjualmstoid = " & ToDouble(sARno(c1))
                        If c1 < sARno.Length - 1 Then
                            sWhere &= " OR "
                        End If
                    Next
                    sWhere &= ")"
                End If
            End If

            If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
                If IsValidPeriod() Then
                    sWhere &= " AND receivedate >='" & FilterPeriod1.Text & " 00:00:00' AND receivedate <='" & FilterPeriod2.Text & " 23:59:59'"
                Else
                    Exit Sub
                End If
            End If
            sWhere &= " AND jm.trntaxamt>0 AND nofakturpajak<>'' ORDER BY [Faktur No.]"
            Dim strHostName As String
            strHostName = ""

            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)

            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, rptName)
                report.Close()
                report.Dispose()
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, rptName)
                report.Close()
                report.Dispose()
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.ToString, 1)
        End Try
    End Sub

    Private Sub InitAllDDL()
        ' DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)
    End Sub

    Private Sub InitFilterDDLCat1()
        'Fill DDL Category1
        sSql = "SELECT cat1code, cat1code+' - '+cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat1res1='Raw' Order By cat1code"
        FillDDL(FilterDDLCat01, sSql)
        If FilterDDLCat01.Items.Count > 0 Then
            InitFilterDDLCat2()
        Else
            FilterDDLCat02.Items.Clear()
            FilterDDLCat03.Items.Clear()
            FilterDDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitFilterDDLCat2()
        'Fill DDL Category2
        sSql = "SELECT cat2code, cat2code+' - '+cat2shortdesc FROM QL_mstcat2 cat2 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat2.cmpcode AND cat1.cat1oid=cat2.cat1oid AND cat1.cat1code='" & FilterDDLCat01.SelectedValue & "' WHERE cat2.cmpcode='" & CompnyCode & "' AND cat2.activeflag='ACTIVE' AND cat2res1='Raw' Order By cat2code"
        FillDDL(FilterDDLCat02, sSql)
        If FilterDDLCat02.Items.Count > 0 Then
            InitFilterDDLCat3()
        Else
            FilterDDLCat03.Items.Clear()
            FilterDDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitFilterDDLCat3()
        'Fill DDL Category3
        sSql = "SELECT cat3code, cat3code+' - '+cat3shortdesc FROM QL_mstcat3 cat3 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat3.cmpcode AND cat1.cat1oid=cat3.cat1oid AND cat1.cat1code='" & FilterDDLCat01.SelectedValue & "' INNER JOIN QL_mstcat2 cat2 ON cat2.cmpcode=cat3.cmpcode AND cat2.cat2oid=cat3.cat2oid AND cat2.cat2code='" & FilterDDLCat02.SelectedValue & "' WHERE cat3.cmpcode='" & CompnyCode & "' AND cat3.activeflag='ACTIVE' AND cat3res1='Raw' Order By cat3code"
        FillDDL(FilterDDLCat03, sSql)
        'If FilterDDLCat03.Items.Count > 0 Then
        '    InitFilterDDLCat4()
        'Else
        '    FilterDDLCat04.Items.Clear()
        'End If
    End Sub

    Private Sub InitFilterDDLCat4()
        'Fill DDL Category4
        sSql = "SELECT cat4code, cat4code+' - '+cat4shortdesc FROM QL_mstcat4 cat4 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat4.cmpcode AND cat1.cat1oid=cat4.cat1oid AND cat1.cat1code='" & FilterDDLCat01.SelectedValue & "' INNER JOIN QL_mstcat2 cat2 ON cat2.cmpcode=cat4.cmpcode AND cat2.cat2oid=cat4.cat2oid AND cat2.cat2code='" & FilterDDLCat02.SelectedValue & "' INNER JOIN QL_mstcat3 cat3 ON cat3.cmpcode=cat4.cmpcode AND cat3.cat3oid=cat4.cat3oid AND cat3.cat3code='" & FilterDDLCat03.SelectedValue & "' WHERE cat4.cmpcode='" & CompnyCode & "' AND cat4.activeflag='ACTIVE' AND cat4res1='Raw' Order By cat4code"
        FillDDL(FilterDDLCat04, sSql)
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmExpensePP46.aspx")

        End If

        If checkPagePermission("~\ReportForm\frmExpensePP46.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        Page.Title = CompnyName & " - Expense PP 46 Report"
        If Not Page.IsPostBack Then
            InitAllDDL()
            InitFilterDDLCat1()
            FilterDDLType_SelectedIndexChanged(Nothing, Nothing)
            FilterDDLStatus_SelectedIndexChanged(Nothing, Nothing)
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListCust") Is Nothing And Session("EmptyListCust") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListCust") Then
                Session("EmptyListCust") = Nothing
                cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
            End If
        End If
        If Not Session("WarningListCust") Is Nothing And Session("WarningListCust") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListCust") Then
                Session("WarningListCust") = Nothing
                cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
            End If
        End If
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("EmptyListSO") Is Nothing And Session("EmptyListSO") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListSO") Then
                Session("EmptyListSO") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
            End If
        End If
        If Not Session("WarningListSO") Is Nothing And Session("WarningListSO") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListSO") Then
                Session("WarningListSO") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
            End If
        End If

        If Not Session("EmptyListShipment") Is Nothing And Session("EmptyListShipment") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListShipment") Then
                Session("EmptyListShipment") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListShipment, PanelListShipment, mpeListShipment, True)
            End If
        End If
        If Not Session("WarningListShipment") Is Nothing And Session("WarningListShipment") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListShipment") Then
                Session("WarningListShipment") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListShipment, PanelListShipment, mpeListShipment, True)
            End If
        End If

        If Not Session("EmptyListAR") Is Nothing And Session("EmptyListAR") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListAR") Then
                Session("EmptyListAR") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListAR, PanelListAR, mpeListAR, True)
            End If
        End If
        If Not Session("WarningListAR") Is Nothing And Session("WarningListAR") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListAR") Then
                Session("WarningListAR") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListAR, PanelListAR, mpeListAR, True)
            End If
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        btnClearCust_Click(Nothing, Nothing)
    End Sub

    Protected Sub FilterDDLStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLStatus.SelectedIndexChanged
        FilterDDLDate.Items.Clear()
        FilterDDLDate.Items.Add("SI Date")
        FilterDDLDate.Items.Item(FilterDDLDate.Items.Count - 1).Value = "jm.trnjualdate"
        FilterDDLDate.Items.Add("Faktur Date")
        FilterDDLDate.Items.Item(FilterDDLDate.Items.Count - 1).Value = "jm.receivedate"
    End Sub

    Protected Sub FilterDDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLType.SelectedIndexChanged
        If FilterDDLType.SelectedValue = "Summary" Then
            lblSONo.Visible = False
            lblSo.Visible = False
            sono.Visible = False
            imbFindSO.Visible = False
            imbEraseSO.Visible = False
            lblShipmentNo.Visible = False
            lblSH.Visible = False
            shipmentno.Visible = False
            imbFindShipment.Visible = False
            imbEraseShipment.Visible = False
            lblMat.Visible = False
            lblm.Visible = False
            matcode.Visible = False
            imbFindMat.Visible = False
            imbEraseMat.Visible = False
            lblGrouping.Visible = False
            lblg.Visible = False
            DDLGrouping.Visible = False
            'lblSorting.Visible = False
            'lbls.Visible = False
            'DDLSorting.Visible = False
            'DDLOrderby.Visible = False
            'lblmin.Visible = False
        Else
            lblSONo.Visible = True
            lblSo.Visible = True
            sono.Visible = True
            imbFindSO.Visible = True
            imbEraseSO.Visible = True
            lblShipmentNo.Visible = True
            lblSH.Visible = True
            shipmentno.Visible = True
            imbFindShipment.Visible = True
            imbEraseShipment.Visible = True
            lblMat.Visible = True
            lblm.Visible = True
            matcode.Visible = True
            imbFindMat.Visible = True
            imbEraseMat.Visible = True
            lblGrouping.Visible = False
            lblg.Visible = False
            DDLGrouping.Visible = False
            'lblSorting.Visible = True
            'lbls.Visible = True
            'DDLSorting.Visible = True
            'DDLOrderby.Visible = True
            'lblmin.Visible = True
        End If
        DDLGrouping_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub btnClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearCust.Click
        custcode.Text = ""
    End Sub

    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchCust.Click
        If IsValidPeriod() Then
            DDLFilterListCust.SelectedIndex = -1 : txtFilterListCust.Text = ""
            Session("TblCust") = Nothing : Session("TblCustView") = Nothing : gvListCust.DataSource = Nothing : gvListCust.DataBind()
            cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub btnFindListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListCust.Click
        If Session("TblCust") Is Nothing Then
            BindListCust()
            If Session("TblCust").Rows.Count <= 0 Then
                Session("EmptyListCust") = "Customer data can't be found!"
                showMessage(Session("EmptyListCust"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListCust.SelectedValue & " LIKE '%" & Tchar(txtFilterListCust.Text) & "%'"
        If UpdateCheckedCust() Then
            Dim dv As DataView = Session("TblCust").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblCustView") = dv.ToTable
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                dv.RowFilter = ""
                mpeListCust.Show()
            Else
                dv.RowFilter = ""
                Session("TblCustView") = Nothing
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                Session("WarningListCust") = "Customer data can't be found!"
                showMessage(Session("WarningListCust"), 2)
            End If
        Else
            mpeListCust.Show()
        End If
    End Sub

    Protected Sub btnViewAllListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListCust.Click
        DDLFilterListCust.SelectedIndex = -1 : txtFilterListCust.Text = ""
        If Session("TblCust") Is Nothing Then
            BindListCust()
            If Session("TblCust").Rows.Count <= 0 Then
                Session("EmptyListCust") = "Customer data can't be found!"
                showMessage(Session("EmptyListCust"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedCust() Then
            Dim dt As DataTable = Session("TblCust")
            Session("TblCustView") = dt
            gvListCust.DataSource = Session("TblCustView")
            gvListCust.DataBind()
        End If
        mpeListCust.Show()
    End Sub

    Protected Sub btnSelectAllCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllCust.Click
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCustView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblCust")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "custoid=" & dtTbl.Rows(C1)("custoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblCust") = objTbl
                Session("TblCustView") = dtTbl
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
            End If
            mpeListCust.Show()
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneCust.Click
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCustView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblCust")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "custoid=" & dtTbl.Rows(C1)("custoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblCust") = objTbl
                Session("TblCustView") = dtTbl
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
            End If
            mpeListCust.Show()
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedCust.Click
        If Session("TblCust") Is Nothing Then
            Session("WarningListCust") = "Selected Customer data can't be found!"
            showMessage(Session("WarningListCust"), 2)
            Exit Sub
        End If
        If UpdateCheckedCust() Then
            Dim dtTbl As DataTable = Session("TblCust")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListCust.SelectedIndex = -1 : txtFilterListCust.Text = ""
                Session("TblCustView") = dtView.ToTable
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                dtView.RowFilter = ""
                mpeListCust.Show()
            Else
                dtView.RowFilter = ""
                Session("TblCustView") = Nothing
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                Session("WarningListCust") = "Selected Customer data can't be found!"
                showMessage(Session("WarningListCust"), 2)
            End If
        Else
            mpeListCust.Show()
        End If
    End Sub

    Protected Sub gvListCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListCust.PageIndexChanging
        If UpdateCheckedCust2() Then
            gvListCust.PageIndex = e.NewPageIndex
            gvListCust.DataSource = Session("TblCustView")
            gvListCust.DataBind()
        End If
        mpeListCust.Show()
    End Sub 'OK

    Protected Sub lkbAddToListListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListCust.Click
        If Not Session("TblCust") Is Nothing Then
            If UpdateCheckedCust() Then
                Dim dtTbl As DataTable = Session("TblCust")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If custcode.Text <> "" Then
                            custcode.Text &= ";" + vbCrLf + dtView(C1)("custcode")
                        Else
                            custcode.Text = dtView(C1)("custcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
                Else
                    Session("WarningListCust") = "Please select Customer to add to list!"
                    showMessage(Session("WarningListCust"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbCloseListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListCust.Click
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub

    Protected Sub imbFindSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindSO.Click
        If IsValidPeriod() Then
            DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
            Session("TblSO") = Nothing : Session("TblSOView") = Nothing : gvListSO.DataSource = Nothing : gvListSO.DataBind()
            cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbEraseSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseSO.Click
        sono.Text = ""
        DDLBusUnit.Enabled = True
    End Sub

    Protected Sub btnFindListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSO.Click
        If Session("TblSO") Is Nothing Then
            BindListSO()
            If Session("TblSO").Rows.Count <= 0 Then
                Session("EmptyListSO") = "SO data can't be found!"
                showMessage(Session("EmptyListSO"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListSO.SelectedValue & " LIKE '%" & Tchar(txtFilterListSO.Text) & "%'"
        If UpdateCheckedSO() Then
            Dim dv As DataView = Session("TblSO").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblSOView") = dv.ToTable
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                dv.RowFilter = ""
                mpeListSO.Show()
            Else
                dv.RowFilter = ""
                Session("TblSOView") = Nothing
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                Session("WarningListSO") = "SO data can't be found!"
                showMessage(Session("WarningListSO"), 2)
            End If
        Else
            mpeListSO.Show()
        End If
    End Sub

    Protected Sub btnViewAllListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListSO.Click
        DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
        If Session("TblSO") Is Nothing Then
            BindListSO()
            If Session("TblSO").Rows.Count <= 0 Then
                Session("EmptyListSO") = "SO data can't be found!"
                showMessage(Session("EmptyListSO"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedSO() Then
            Dim dt As DataTable = Session("TblSO")
            Session("TblSOView") = dt
            gvListSO.DataSource = Session("TblSOView")
            gvListSO.DataBind()
        End If
        mpeListSO.Show()
    End Sub

    Protected Sub btnSelectAllSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllSO.Click
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "somstoid=" & dtTbl.Rows(C1)("somstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSO") = objTbl
                Session("TblSOView") = dtTbl
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
            End If
            mpeListSO.Show()
        Else
            Session("WarningListSO") = "Please show some SO data first!"
            showMessage(Session("WarningListSO"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneSO.Click
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "somstoid=" & dtTbl.Rows(C1)("somstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSO") = objTbl
                Session("TblSOView") = dtTbl
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
            End If
            mpeListSO.Show()
        Else
            Session("WarningListSO") = "Please show some SO data first!"
            showMessage(Session("WarningListSO"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedSO.Click
        If Session("TblSO") Is Nothing Then
            Session("WarningListSO") = "Selected SO data can't be found!"
            showMessage(Session("WarningListSO"), 2)
            Exit Sub
        End If
        If UpdateCheckedSO() Then
            Dim dtTbl As DataTable = Session("TblSO")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
                Session("TblSOView") = dtView.ToTable
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                dtView.RowFilter = ""
                mpeListSO.Show()
            Else
                dtView.RowFilter = ""
                Session("TblSOView") = Nothing
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                Session("WarningListSO") = "Selected SO data can't be found!"
                showMessage(Session("WarningListSO"), 2)
            End If
        Else
            mpeListSO.Show()
        End If
    End Sub

    Protected Sub gvListSO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSO.PageIndexChanging
        If UpdateCheckedSO2() Then
            gvListSO.PageIndex = e.NewPageIndex
            gvListSO.DataSource = Session("TblSOView")
            gvListSO.DataBind()
        End If
        mpeListSO.Show()
    End Sub

    Protected Sub lkbAddToListListSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListSO.Click
        If Not Session("TblSO") Is Nothing Then
            If UpdateCheckedSO() Then
                Dim dtTbl As DataTable = Session("TblSO")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If sono.Text <> "" Then
                            If dtView(C1)("sono") <> "" Then
                                sono.Text &= ";" + vbCrLf + dtView(C1)("sono")
                            End If
                        Else
                            If dtView(C1)("sono") <> "" Then
                                sono.Text &= dtView(C1)("sono")
                            End If
                        End If
                    Next
                    dtView.RowFilter = ""
                    'DDLBusUnit.Enabled = False
                    cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, False)
                Else
                    Session("WarningListSO") = "Please select SO to add to list!"
                    showMessage(Session("WarningListSO"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListSO") = "Please show some SO data first!"
            showMessage(Session("WarningListSO"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListSO.Click
        cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, False)
    End Sub

    Protected Sub imbFindShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindShipment.Click
        If IsValidPeriod() Then
            DDLFilterListShipment.SelectedIndex = -1 : txtFilterListShipment.Text = ""
            Session("TblShipment") = Nothing : Session("TblShipmentView") = Nothing : gvListShipment.DataSource = Nothing : gvListShipment.DataBind()
            cProc.SetModalPopUpExtender(btnHiddenListShipment, PanelListShipment, mpeListShipment, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbEraseShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseShipment.Click
        shipmentno.Text = ""
        DDLBusUnit.Enabled = True
    End Sub

    Protected Sub btnFindListShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListShipment.Click
        If Session("TblShipment") Is Nothing Then
            BindListShipment()
            If Session("TblShipment").Rows.Count <= 0 Then
                Session("EmptyListShipment") = "Shipment data can't be found!"
                showMessage(Session("EmptyListShipment"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListShipment.SelectedValue & " LIKE '%" & Tchar(txtFilterListShipment.Text) & "%'"
        If UpdateCheckedShipment() Then
            Dim dv As DataView = Session("TblShipment").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblShipmentView") = dv.ToTable
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
                dv.RowFilter = ""
                mpeListShipment.Show()
            Else
                dv.RowFilter = ""
                Session("TblShipmentView") = Nothing
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
                Session("WarningListShipment") = "Shipment data can't be found!"
                showMessage(Session("WarningListShipment"), 2)
            End If
        Else
            mpeListShipment.Show()
        End If
    End Sub

    Protected Sub btnViewAllListShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListShipment.Click
        DDLFilterListShipment.SelectedIndex = -1 : txtFilterListShipment.Text = ""
        If Session("TblShipment") Is Nothing Then
            BindListShipment()
            If Session("TblShipment").Rows.Count <= 0 Then
                Session("EmptyListShipment") = "Shipment data can't be found!"
                showMessage(Session("EmptyListShipment"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedShipment() Then
            Dim dt As DataTable = Session("TblShipment")
            Session("TblShipmentView") = dt
            gvListShipment.DataSource = Session("TblShipmentView")
            gvListShipment.DataBind()
        End If
        mpeListShipment.Show()
    End Sub

    Protected Sub btnSelectAllShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllShipment.Click
        If Not Session("TblShipmentView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblShipmentView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblShipment")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "shipmentmstoid=" & dtTbl.Rows(C1)("shipmentmstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblShipment") = objTbl
                Session("TblShipmentView") = dtTbl
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
            End If
            mpeListShipment.Show()
        Else
            Session("WarningListShipment") = "Please show some Shipment data first!"
            showMessage(Session("WarningListShipment"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneShipment.Click
        If Not Session("TblShipmentView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblShipmentView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblShipment")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "shipmentmstoid=" & dtTbl.Rows(C1)("shipmentmstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblShipment") = objTbl
                Session("TblShipmentView") = dtTbl
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
            End If
            mpeListShipment.Show()
        Else
            Session("WarningListShipment") = "Please show some Shipment data first!"
            showMessage(Session("WarningListShipment"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedShipment.Click
        If Session("TblShipment") Is Nothing Then
            Session("WarningListShipment") = "Selected Shipment data can't be found!"
            showMessage(Session("WarningListShipment"), 2)
            Exit Sub
        End If
        If UpdateCheckedShipment() Then
            Dim dtTbl As DataTable = Session("TblShipment")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListShipment.SelectedIndex = -1 : txtFilterListShipment.Text = ""
                Session("TblShipmentView") = dtView.ToTable
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
                dtView.RowFilter = ""
                mpeListShipment.Show()
            Else
                dtView.RowFilter = ""
                Session("TblShipmentView") = Nothing
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
                Session("WarningListShipment") = "Selected Shipment data can't be found!"
                showMessage(Session("WarningListShipment"), 2)
            End If
        Else
            mpeListShipment.Show()
        End If
    End Sub

    Protected Sub gvListShipment_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListShipment.PageIndexChanging
        If UpdateCheckedShipment2() Then
            gvListShipment.PageIndex = e.NewPageIndex
            gvListShipment.DataSource = Session("TblShipmentView")
            gvListShipment.DataBind()
        End If
        mpeListShipment.Show()
    End Sub

    Protected Sub lkbAddToListListShipment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListShipment.Click
        If Not Session("TblShipment") Is Nothing Then
            If UpdateCheckedShipment() Then
                Dim dtTbl As DataTable = Session("TblShipment")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If shipmentno.Text <> "" Then
                            If dtView(C1)("shipmentno") <> "" Then
                                shipmentno.Text &= ";" + vbCrLf + dtView(C1)("shipmentno")
                            End If
                        Else
                            If dtView(C1)("shipmentno") <> "" Then
                                shipmentno.Text &= dtView(C1)("shipmentno")
                            End If
                        End If
                    Next
                    dtView.RowFilter = ""
                    'DDLBusUnit.Enabled = False
                    cProc.SetModalPopUpExtender(btnHiddenListShipment, PanelListShipment, mpeListShipment, False)
                Else
                    Session("WarningListShipment") = "Please select Shipment to add to list!"
                    showMessage(Session("WarningListShipment"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListShipment") = "Please show some Shipment data first!"
            showMessage(Session("WarningListShipment"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListShipment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListShipment.Click
        cProc.SetModalPopUpExtender(btnHiddenListShipment, PanelListShipment, mpeListShipment, False)
    End Sub

    Protected Sub DDLARNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLARNo.SelectedIndexChanged
        arno.Text = ""
    End Sub

    Protected Sub imbFindAR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindAR.Click
        If IsValidPeriod() Then
            DDLFilterListAR.SelectedIndex = -1 : txtFilterListAR.Text = ""
            Session("TblAR") = Nothing : Session("TblARView") = Nothing : gvListAR.DataSource = Nothing : gvListAR.DataBind()
            cProc.SetModalPopUpExtender(btnHiddenListAR, PanelListAR, mpeListAR, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbEraseAR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseAR.Click
        arno.Text = ""
    End Sub

    Protected Sub btnFindListAR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListAR.Click
        If Session("TblAR") Is Nothing Then
            BindListAR()
            If Session("TblAR").Rows.Count <= 0 Then
                Session("EmptyListAR") = "AR data can't be found!"
                showMessage(Session("EmptyListAR"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListAR.SelectedValue & " LIKE '%" & Tchar(txtFilterListAR.Text) & "%'"
        If UpdateCheckedAR() Then
            Dim dv As DataView = Session("TblAR").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblARView") = dv.ToTable
                gvListAR.DataSource = Session("TblARView")
                gvListAR.DataBind()
                dv.RowFilter = ""
                mpeListAR.Show()
            Else
                dv.RowFilter = ""
                Session("TblARView") = Nothing
                gvListAR.DataSource = Session("TblARView")
                gvListAR.DataBind()
                Session("WarningListAR") = "AR data can't be found!"
                showMessage(Session("WarningListAR"), 2)
            End If
        Else
            mpeListAR.Show()
        End If
    End Sub

    Protected Sub btnViewAllListAR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListAR.Click
        DDLFilterListAR.SelectedIndex = -1 : txtFilterListAR.Text = ""
        If Session("TblAR") Is Nothing Then
            BindListAR()
            If Session("TblAR").Rows.Count <= 0 Then
                Session("EmptyListAR") = "AR data can't be found!"
                showMessage(Session("EmptyListAR"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedAR() Then
            Dim dt As DataTable = Session("TblAR")
            Session("TblARView") = dt
            gvListAR.DataSource = Session("TblARView")
            gvListAR.DataBind()
        End If
        mpeListAR.Show()
    End Sub

    Protected Sub btnSelectAllAR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllAR.Click
        If Not Session("TblARView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblARView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblAR")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "armstoid=" & dtTbl.Rows(C1)("armstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblAR") = objTbl
                Session("TblARView") = dtTbl
                gvListAR.DataSource = Session("TblARView")
                gvListAR.DataBind()
            End If
            mpeListAR.Show()
        Else
            Session("WarningListAR") = "Please show some AR data first!"
            showMessage(Session("WarningListAR"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneAR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneAR.Click
        If Not Session("TblARView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblARView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblAR")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "armstoid=" & dtTbl.Rows(C1)("armstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblAR") = objTbl
                Session("TblARView") = dtTbl
                gvListAR.DataSource = Session("TblARView")
                gvListAR.DataBind()
            End If
            mpeListAR.Show()
        Else
            Session("WarningListAR") = "Please show some AR data first!"
            showMessage(Session("WarningListAR"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedAR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedAR.Click
        If Session("TblAR") Is Nothing Then
            Session("WarningListAR") = "Selected AR data can't be found!"
            showMessage(Session("WarningListAR"), 2)
            Exit Sub
        End If
        If UpdateCheckedAR() Then
            Dim dtTbl As DataTable = Session("TblAR")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListAR.SelectedIndex = -1 : txtFilterListAR.Text = ""
                Session("TblARView") = dtView.ToTable
                gvListAR.DataSource = Session("TblARView")
                gvListAR.DataBind()
                dtView.RowFilter = ""
                mpeListAR.Show()
            Else
                dtView.RowFilter = ""
                Session("TblARView") = Nothing
                gvListAR.DataSource = Session("TblARView")
                gvListAR.DataBind()
                Session("WarningListAR") = "Selected AR data can't be found!"
                showMessage(Session("WarningListAR"), 2)
            End If
        Else
            mpeListAR.Show()
        End If
    End Sub

    Protected Sub gvListAR_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListAR.PageIndexChanging
        If UpdateCheckedAR2() Then
            gvListAR.PageIndex = e.NewPageIndex
            gvListAR.DataSource = Session("TblARView")
            gvListAR.DataBind()
        End If
        mpeListAR.Show()
    End Sub 'OK

    Protected Sub lkbAddToListListAR_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListAR.Click
        If Not Session("TblAR") Is Nothing Then
            If UpdateCheckedAR() Then
                Dim dtTbl As DataTable = Session("TblAR")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If arno.Text <> "" Then
                            If DDLARNo.SelectedValue = "AR No." Then
                                If dtView(C1)("arno") <> "" Then
                                    arno.Text &= ";" + vbCrLf + dtView(C1)("arno")
                                End If
                            Else
                                arno.Text &= ";" + vbCrLf + dtView(C1)("armstoid").ToString
                            End If
                        Else
                            If DDLARNo.SelectedValue = "AR No." Then
                                If dtView(C1)("arno") <> "" Then
                                    arno.Text &= dtView(C1)("arno")
                                End If
                            Else
                                arno.Text &= dtView(C1)("armstoid").ToString
                            End If
                        End If
                    Next
                    dtView.RowFilter = ""
                    'DDLBusUnit.Enabled = False
                    'DDLBusUnit.CssClass = "inpTextDisabled"
                    cProc.SetModalPopUpExtender(btnHiddenListAR, PanelListAR, mpeListAR, False)
                Else
                    Session("WarningListAR") = "Please select AR to add to list!"
                    showMessage(Session("WarningListAR"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListAR") = "Please show some AR data first!"
            showMessage(Session("WarningListAR"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListAR_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListAR.Click
        cProc.SetModalPopUpExtender(btnHiddenListAR, PanelListAR, mpeListAR, False)
    End Sub

    Protected Sub FilterDDLCat01_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat01.SelectedIndexChanged
        InitFilterDDLCat2()
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat02_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat02.SelectedIndexChanged
        InitFilterDDLCat3()
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat03_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat03.SelectedIndexChanged
        InitFilterDDLCat4()
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat04_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat04.SelectedIndexChanged
        mpeListMat.Show()
    End Sub

    Protected Sub imbFindMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindMat.Click
        If IsValidPeriod() Then
            FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
            InitFilterDDLCat1()
            cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
            Session("TblMat") = Nothing : Session("TblMatView") = Nothing : gvListMat.DataSource = Nothing : gvListMat.DataBind() : tbData.Text = "5" : gvListMat.PageSize = CInt(tbData.Text)
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            Exit Sub
        End If
    End Sub 'OK

    Protected Sub imbEraseMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseMat.Click
        matcode.Text = ""
        'If mrno.Text = "" And custname.Text = "" Then
        '    DDLBusUnit.Enabled = True
        '    DDLBusUnit.CssClass = "inpText"
        'End If
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblMat") Is Nothing Then
            BindListMat()

            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        If cbCat01.Checked Then
            If FilterDDLCat01.SelectedValue <> "" Then
                sPlus &= " AND SUBSTRING(matcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'"
            Else
                cbCat01.Checked = False
            End If
        End If
        If cbCat02.Checked Then
            If FilterDDLCat02.SelectedValue <> "" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND SUBSTRING(matcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'") & " AND SUBSTRING(matcode, 4, 3)='" & FilterDDLCat02.SelectedValue & "'"
            Else
                cbCat02.Checked = False
            End If
        End If
        If cbCat03.Checked Then
            If FilterDDLCat03.SelectedValue <> "" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND SUBSTRING(matcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'") & IIf(cbCat02.Checked, "", " AND SUBSTRING(matcode, 4, 3)='" & FilterDDLCat02.SelectedValue & "'") & " AND SUBSTRING(matcode, 8, 4)='" & FilterDDLCat03.SelectedValue & "'"
            Else
                cbCat03.Checked = False
            End If
        End If
        If cbCat04.Checked Then
            If FilterDDLCat04.SelectedValue <> "" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND SUBSTRING(matcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'") & IIf(cbCat02.Checked, "", " AND SUBSTRING(matcode, 4, 3)='" & FilterDDLCat02.SelectedValue & "'") & IIf(cbCat03.Checked, "", " AND SUBSTRING(matcode, 8, 4)='" & FilterDDLCat03.SelectedValue & "'") & " AND SUBSTRING(matcode, 13, 4)='" & FilterDDLCat04.SelectedValue & "'"
            Else
                cbCat04.Checked = False
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub 'OK

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        InitFilterDDLCat1()
        cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub 'OK

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If matcode.Text <> "" Then
                            matcode.Text &= ";" + vbCrLf + dtView(C1)("matcode")
                        Else
                            matcode.Text = dtView(C1)("matcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some Material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matoid=" & dtTbl.Rows(C1)("matoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matoid=" & dtTbl.Rows(C1)("matoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                InitFilterDDLCat1()
                cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Selected material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub lbShowData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbShowData.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Please find material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If tbData.Text = "" Then
            Session("WarningListMat") = "Please fill total data to be shown!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        Else
            If ToDouble(tbData.Text) <= 0 Then
                Session("WarningListMat") = "Total data to be shown must be more than 0!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat2() Then
            gvListMat.PageSize = Int(tbData.Text)
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub DDLGrouping_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLGrouping.SelectedIndexChanged
        If FilterDDLType.SelectedValue = "Summary" Then
            'Fill DDL Sorting
            DDLSorting.Items.Clear()
            DDLSorting.Items.Add("SI No.")
            DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "arm.trnjualno"
            'DDLSorting.Items.Add("Approval Date")
            'DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "arm.approvaldatetime"
            'DDLSorting.Items.Add("Date Take Giro")
            'DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "arm.arrawdatetakegiro"
        Else
            If DDLGrouping.SelectedValue = "arno" Then
                'Fill DDL Sorting
                DDLSorting.Items.Clear()
                'DDLSorting.Items.Add("Approval Date")
                'DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "arm.approvaldatetime"
                DDLSorting.Items.Add("DO No.")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "shm.dono"
                'DDLSorting.Items.Add("Date Take Giro")
                'DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "arm.arrawdatetakegiro"
            ElseIf DDLGrouping.SelectedValue = "shipmentno" Then
                'Fill DDL Sorting
                DDLSorting.Items.Clear()
                DDLSorting.Items.Add("SI No.")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "arm.trnjualno"
                'DDLSorting.Items.Add("Approval Date")
                'DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "arm.approvaldatetime"
                'DDLSorting.Items.Add("Date Take Giro")
                'DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "arm.arrawdatetakegiro"
            Else
                'Fill DDL Sorting
                DDLSorting.Items.Clear()
                DDLSorting.Items.Add("SI No.")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "arm.trnjualno"
                DDLSorting.Items.Add("DO No.")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "shm.dono"
                'DDLSorting.Items.Add("Approval Date")
                'DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "arm.approvaldatetime"
                'DDLSorting.Items.Add("Date Take Giro")
                'DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "arm.arrawdatetakegiro"
            End If
        End If
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub 'OK

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmExpensePP46.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
         report.Dispose()
	 report.Close()    
    End Sub

    Protected Sub gvListSO_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = CDate(e.Row.Cells(3).Text)
        End If
    End Sub
#End Region

End Class
