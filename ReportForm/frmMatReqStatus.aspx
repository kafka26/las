<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="frmMatReqStatus.aspx.vb" Inherits="ReportForm_MatReqStatus" title="" EnableEventValidation="false" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Material Request Non KIK Status" CssClass="Title" ForeColor="SaddleBrown"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="center" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="center">
                                                    <asp:UpdatePanel ID="upReportForm" runat="server">
                                                        <ContentTemplate>
<asp:Panel id="pnlReportForm" runat="server" Width="500px" DefaultButton="btnViewReport" __designer:wfdid="w151"><TABLE><TBODY><TR id="BusinessUnitA" runat="server" visible="true"><TD id="TD2" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label3" runat="server" Text="Business Unit" __designer:wfdid="w152"></asp:Label></TD><TD id="TD1" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD3" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpText" __designer:wfdid="w153" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label8" runat="server" Text="Type" __designer:wfdid="w154"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLType" runat="server" CssClass="inpText" Width="105px" __designer:wfdid="w155" AutoPostBack="True"><asp:ListItem>SUMMARY</asp:ListItem>
<asp:ListItem>DETAIL</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label2" runat="server" Text="Status" __designer:wfdid="w156"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="188px" __designer:wfdid="w157" AutoPostBack="True"><asp:ListItem>All</asp:ListItem>
<asp:ListItem Value="In Process">In Process</asp:ListItem>
<asp:ListItem Value="Post">Post/Closed</asp:ListItem>
<asp:ListItem Value="Cancel">Cancel</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left rowSpan=2><asp:DropDownList id="FilterDDLDate" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w158"></asp:DropDownList></TD><TD class="Label" align=center rowSpan=2>:</TD></TR><TR><TD class="Label" align=left><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="60px" __designer:wfdid="w159" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="CalPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w160"></asp:ImageButton> <asp:Label id="lblTo" runat="server" Text="to" __designer:wfdid="w161"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="60px" __designer:wfdid="w162" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="CalPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w163"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w164"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:RadioButton id="RDReqQty" runat="server" Text="Req Qty" __designer:wfdid="w165" AutoPostBack="True" Checked="True" GroupName="2"></asp:RadioButton></TD><TD class="Label" align=center rowSpan=2><asp:Label id="septQty" runat="server" Text=":" __designer:wfdid="w166"></asp:Label></TD><TD class="Label" align=left rowSpan=2><asp:DropDownList id="FilterDDLReqQty" runat="server" CssClass="inpText" Width="46px" __designer:wfdid="w167"><asp:ListItem>=</asp:ListItem>
<asp:ListItem>&lt;</asp:ListItem>
<asp:ListItem>&gt;</asp:ListItem>
<asp:ListItem>&lt;=</asp:ListItem>
<asp:ListItem>&gt;=</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="ReqQtyTxt" runat="server" CssClass="inpText" Width="56px" __designer:wfdid="w168">0</asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:RadioButton id="RDUsg" runat="server" Text="Balance" __designer:wfdid="w169" AutoPostBack="True" GroupName="2"></asp:RadioButton></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" __designer:wfdid="w170" TargetControlID="FilterPeriod1" PopupButtonID="CalPeriod1" Format="MM/dd/yyyy">
                                                                            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" __designer:wfdid="w171" TargetControlID="FilterPeriod1" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999">
                                                                            </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" __designer:wfdid="w172" TargetControlID="FilterPeriod2" PopupButtonID="CalPeriod2" Format="MM/dd/yyyy">
                                                                            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" __designer:wfdid="w173" TargetControlID="FilterPeriod2" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999">
                                                                            </ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD class="Label" align=left rowSpan=2><asp:Label id="Label5" runat="server" Text="Department" __designer:wfdid="w174"></asp:Label></TD><TD class="Label" align=center rowSpan=2>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLDept" runat="server" CssClass="inpText" Width="275px" __designer:wfdid="w175"></asp:DropDownList> <asp:ImageButton id="btnAddDept" runat="server" ImageUrl="~/Images/plus.png" ImageAlign="AbsMiddle" __designer:wfdid="w176"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:ListBox id="lbDept" runat="server" CssClass="inpTextDisabled" Width="275px" __designer:wfdid="w177" Rows="2"></asp:ListBox> <asp:ImageButton id="btnMinDept" runat="server" ImageUrl="~/Images/minus.png" ImageAlign="AbsMiddle" __designer:wfdid="w178"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:DropDownList id="DDLRequest" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w179"><asp:ListItem Value="reqm.matreqno">Request No.</asp:ListItem>
<asp:ListItem Value="reqm.matreqmstoid">Draft No.</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="FilterRequest" runat="server" CssClass="inpText" Width="255px" __designer:wfdid="w180" Rows="2" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchRequest" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w181"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearRequest" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w182"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left rowSpan=2><asp:Label id="lblMat" runat="server" Text="Material" __designer:wfdid="w183"></asp:Label></TD><TD class="Label" align=center rowSpan=2><asp:Label id="septMat" runat="server" Text=":" __designer:wfdid="w184"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="DDLMatType" runat="server" CssClass="inpText" __designer:wfdid="w185"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:TextBox id="FilterMaterial" runat="server" CssClass="inpText" Width="255px" __designer:wfdid="w186" Rows="2" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchMat" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w187"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearMat" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w188"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblGroupBy" runat="server" Text="Grouping" __designer:wfdid="w189"></asp:Label></TD><TD class="Label" align=center><asp:Label id="septGroupBy" runat="server" Text=":" __designer:wfdid="w190"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="DDLGroupBy" runat="server" CssClass="inpText" __designer:wfdid="w191" AutoPostBack="True"><asp:ListItem Value="[Oid], [Request No.]">Draft &amp; Request No.</asp:ListItem>
<asp:ListItem Value="[Code]">Mat. Code</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblSortBy" runat="server" Text="Sorting" __designer:wfdid="w192"></asp:Label></TD><TD class="Label" align=center><asp:Label id="septSortBy" runat="server" Text=":" __designer:wfdid="w193"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="DDLSortBy" runat="server" CssClass="inpText" __designer:wfdid="w194"><asp:ListItem Value="[Oid], [Request No.]">Draft &amp; Request No.</asp:ListItem>
<asp:ListItem Value="[Code]">Mat. Code</asp:ListItem>
</asp:DropDownList>&nbsp;- <asp:DropDownList id="DDLOrder" runat="server" CssClass="inpText" Width="105px" __designer:wfdid="w195"><asp:ListItem Value="ASC">Ascending</asp:ListItem>
<asp:ListItem Value="DESC">Descending</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w196"></asp:ImageButton> <asp:ImageButton id="btnExportToPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w197"></asp:ImageButton> <asp:ImageButton id="btnExportToExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w198"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w199"></asp:ImageButton></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w200" AssociatedUpdatePanelID="upReportForm"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgLoading" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w201"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR></TBODY></TABLE></asp:Panel> <CR:CrystalReportViewer id="crvReportForm" runat="server" __designer:dtid="2251799813685274" __designer:wfdid="w202" AutoDataBind="True" HasViewList="False" HasCrystalLogo="False" HasPrintButton="False" HasExportButton="False" HasToggleGroupTreeButton="False" HasDrillUpButton="False"></CR:CrystalReportViewer> 
</ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnViewReport" />
                                                            <asp:PostBackTrigger ControlID="btnExportToPdf" />
                                                            <asp:PostBackTrigger ControlID="btnExportToExcel" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                            &nbsp;
            </td>
        </tr>
    </table>
    &nbsp;<asp:UpdatePanel id="upListRequest" runat="server"><contenttemplate>
<asp:Panel id="pnlListRequest" runat="server" CssClass="modalBox" Width="750px" DefaultButton="btnFindListRequest" Visible="False"><TABLE style="WIDTH: 100%"><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListRequest" runat="server" Font-Size="Medium" Font-Bold="True" Font-Underline="False">List Of Material Request Non KIK</asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListRequest" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="matreqno">Request No.</asp:ListItem>
<asp:ListItem Value="draftno">Draft No.</asp:ListItem>
<asp:ListItem Value="matreqmstnote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListRequest" runat="server" CssClass="inpText" Width="200px"></asp:TextBox> <asp:ImageButton id="btnFindListRequest" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListRequest" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListRequest" runat="server" ForeColor="#333333" Width="99%" PageSize="5" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><HeaderTemplate>
<asp:CheckBox id="cbHdrLMRequest" runat="server" AutoPostBack="True" OnCheckedChanged="cbHdrLMRequest_CheckedChanged"></asp:CheckBox> 
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="cbLMRequest" runat="server" ToolTip='<%# eval("matreqmstoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="draftno" HeaderText="Draft No.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matreqno" HeaderText="Request No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matreqdate" HeaderText="Request Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matreqmstnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbAddToListRequest" runat="server">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lbSelectAllListRequest" runat="server">[ Select All To List ]</asp:LinkButton> <asp:LinkButton id="lbCloseListRequest" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListRequest" runat="server" TargetControlID="btnHideListRequest" BackgroundCssClass="modalBackground" PopupControlID="pnlListRequest" PopupDragHandleControlID="lblListRequest" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListRequest" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    &nbsp; &nbsp;<asp:UpdatePanel id="upListMat" runat="server"><contenttemplate>
<asp:Panel id="pnlListMat" runat="server" CssClass="modalBox" Width="750px" DefaultButton="btnFindListMat" Visible="False"><TABLE style="WIDTH: 100%"><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListMat" runat="server" Font-Size="Medium" Font-Bold="True" Font-Underline="False">List Of Raw Material</asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" CssClass="inpText" Width="100px">
                                                        <asp:ListItem Value="matcode">Code</asp:ListItem>
                                                        <asp:ListItem Value="matlongdesc">Description</asp:ListItem>
                                                    </asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" CssClass="inpText" Width="200px"></asp:TextBox> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat" runat="server" ForeColor="#333333" Width="99%" PageSize="5" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
                                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"  />
                                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333"  />
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="cbHdrLM" runat="server" AutoPostBack="True" OnCheckedChanged="cbHdrLM_CheckedChanged"  />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbLM" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("matoid") %>'  />
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center"  />
                                            <ItemStyle HorizontalAlign="Center"  />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="matcode" HeaderText="Code">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left"  />
                                            <ItemStyle HorizontalAlign="Left"  />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="matlongdesc" HeaderText="Description">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left"  />
                                            <ItemStyle HorizontalAlign="Left"  />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="matunit" HeaderText="Unit">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center"  />
                                            <ItemStyle HorizontalAlign="Center"  />
                                        </asp:BoundField>
                                    </Columns>
                                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"  />
                                    <PagerStyle CssClass="gvpopup" Font-Bold="True" ForeColor="White" HorizontalAlign="Right"  />
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"  />
                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"  />
                                    <AlternatingRowStyle BackColor="White"  />
                                </asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbAddToListMat" runat="server">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lbSelectAllListMat" runat="server">[ Select All To List ]</asp:LinkButton> <asp:LinkButton id="lbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" BackgroundCssClass="modalBackground" PopupControlID="pnlListMat" PopupDragHandleControlID="lblListMat" Drag="True">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

