<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="frmPORegister.aspx.vb" Inherits="frmPORegister" title="" EnableEventValidation="false" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Purchase Register Report" CssClass="Title" ForeColor="SaddleBrown"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <table style="width: 100%">
                                <tr>
                                    <td align="center" valign="top" char="Label">
                                        <table>
                                            <tr>
                                                <td align="center" class="Label" valign="top">
                                                    <asp:UpdatePanel ID="upReportForm" runat="server">
                                                        <ContentTemplate>
<asp:Panel id="pnlReportForm" runat="server" Width="500px" DefaultButton="btnViewReport"><TABLE><TBODY><TR><TD class="Label" align=left><asp:CheckBox id="chkPeriod" runat="server" CssClass="Label" Text="Periode LPB" __designer:wfdid="w5" Enabled="False" Checked="True"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="month1" runat="server" CssClass="inpText" Width="48px" __designer:wfdid="w6"><asp:ListItem Value="01">Jan</asp:ListItem>
<asp:ListItem Value="02">Feb</asp:ListItem>
<asp:ListItem Value="03">Mar</asp:ListItem>
<asp:ListItem Value="04">Apr</asp:ListItem>
<asp:ListItem Value="05">May</asp:ListItem>
<asp:ListItem Value="06">Jun</asp:ListItem>
<asp:ListItem Value="07">Jul</asp:ListItem>
<asp:ListItem Value="08">Aug</asp:ListItem>
<asp:ListItem Value="09">Sep</asp:ListItem>
<asp:ListItem Value="10">Oct</asp:ListItem>
<asp:ListItem Value="11">Nov</asp:ListItem>
<asp:ListItem Value="12">Dec</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:DropDownList id="year1" runat="server" CssClass="inpText" Width="53px" __designer:wfdid="w7"></asp:DropDownList>&nbsp;- <asp:DropDownList id="month2" runat="server" CssClass="inpText" Width="48px" __designer:wfdid="w8"><asp:ListItem Value="01">Jan</asp:ListItem>
<asp:ListItem Value="02">Feb</asp:ListItem>
<asp:ListItem Value="03">Mar</asp:ListItem>
<asp:ListItem Value="04">Apr</asp:ListItem>
<asp:ListItem Value="05">May</asp:ListItem>
<asp:ListItem Value="06">Jun</asp:ListItem>
<asp:ListItem Value="07">Jul</asp:ListItem>
<asp:ListItem Value="08">Aug</asp:ListItem>
<asp:ListItem Value="09">Sep</asp:ListItem>
<asp:ListItem Value="10">Oct</asp:ListItem>
<asp:ListItem Value="11">Nov</asp:ListItem>
<asp:ListItem Value="12">Dec</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:DropDownList id="year2" runat="server" CssClass="inpText" Width="53px" __designer:wfdid="w9"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="chkGroup" runat="server" CssClass="Label" Text="Group" __designer:wfdid="w4"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="TypeMat" runat="server" CssClass="inpText" Width="128px" __designer:wfdid="w3" OnSelectedIndexChanged="TypeMat_SelectedIndexChanged"><asp:ListItem>ALL</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label9" runat="server" Text="Currency" __designer:wfdid="w4"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLcurr" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w2" OnSelectedIndexChanged="TypeMat_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label2" runat="server" Text="Status"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="Post">Post</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Approved</asp:ListItem>
<asp:ListItem>Force  Close</asp:ListItem>
<asp:ListItem>Closed</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="TD16" class="Label" align=left rowSpan=2 runat="server" Visible="false"><asp:DropDownList id="FilterDDLDate" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w1" Enabled="False"><asp:ListItem>MR Date</asp:ListItem>
<asp:ListItem Enabled="False">Post Date</asp:ListItem>
</asp:DropDownList></TD><TD id="TD15" class="Label" align=center rowSpan=2 runat="server" Visible="false">:</TD></TR><TR><TD id="TD14" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpTextDisabled" Width="60px" Enabled="False" ToolTip="MM/01/yyyy"></asp:TextBox> <asp:ImageButton id="CalPeriod1" onclick="CalPeriod1_Click" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="lblTo" runat="server" Text="to"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpTextDisabled" Width="60px" Enabled="False" ToolTip="MM/31/yyyy"></asp:TextBox> <asp:ImageButton id="CalPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" TargetControlID="FilterPeriod1" PopupButtonID="CalPeriod1" Format="MM/01/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="FilterPeriod1" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999">
                                                                            </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" TargetControlID="FilterPeriod2" PopupButtonID="CalPeriod2" Format="MM/31/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" TargetControlID="FilterPeriod2" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999">
                                                                            </ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD class="Label" align=left><asp:Label id="lblGrouping" runat="server" Text="Grouping"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLGrouping" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True"><asp:ListItem Value="Periode">Periode</asp:ListItem>
<asp:ListItem Value="Supplier">Supplier</asp:ListItem>
<asp:ListItem>Item</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label5" runat="server" Text="Supplier" Visible="False"></asp:Label></TD><TD class="Label" align=center><asp:Label id="lblg" runat="server" Text=":" __designer:wfdid="w1" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="suppcode" runat="server" CssClass="inpText" Width="158px" __designer:wfdid="w2" Visible="False" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchSupp" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w3" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearSupp" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w4" Visible="False"></asp:ImageButton></TD></TR><TR><TD id="TD12" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="DDLMRNo" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True"><asp:ListItem>MR No.</asp:ListItem>
<asp:ListItem Value="Draft No.">Draft No.</asp:ListItem>
</asp:DropDownList></TD><TD id="TD11" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD10" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="mrno" runat="server" CssClass="inpText" Width="178px" TextMode="MultiLine"></asp:TextBox> <asp:ImageButton id="imbFindMR" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbEraseMR" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblMat" runat="server" Text="Item " Visible="False"></asp:Label></TD><TD class="Label" align=center><asp:Label id="lblm" runat="server" Text=":" __designer:wfdid="w2" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="matcode" runat="server" CssClass="inpText" Width="178px" Visible="False" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindMat" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseMat" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD></TR><TR><TD id="TD9" class="Label" align=left runat="server" Visible="true"><asp:Label id="lblSorting" runat="server" Text="Sorting" Visible="False"></asp:Label></TD><TD id="TD6" class="Label" align=center runat="server" Visible="true"><asp:Label id="lbls" runat="server" Text=":" __designer:wfdid="w2" Visible="False"></asp:Label></TD><TD id="TD5" class="Label" align=left runat="server" Visible="true"><asp:DropDownList id="DDLSorting" runat="server" CssClass="inpText" Width="100px" Visible="False"><asp:ListItem Value="mrm.mr">MR/LPB No.</asp:ListItem>
<asp:ListItem Value="pom.po">PO/SP No.</asp:ListItem>
<asp:ListItem Value="m.">Material Description</asp:ListItem>
<asp:ListItem Value="s.suppname">Supplier</asp:ListItem>
</asp:DropDownList><asp:Label id="lblmin" runat="server" Text="-" __designer:wfdid="w2" Visible="False"></asp:Label><asp:DropDownList id="DDLOrderby" runat="server" CssClass="inpTextDisabled" Width="100px" Visible="False" Enabled="False"><asp:ListItem Value="ASC">ASC</asp:ListItem>
<asp:ListItem Value="DESC">DESC</asp:ListItem>
</asp:DropDownList> <asp:DropDownList id="suppType" runat="server" CssClass="inpText" Width="114px" __designer:wfdid="w1" Visible="False"><asp:ListItem Value="All">All</asp:ListItem>
<asp:ListItem>Internal</asp:ListItem>
<asp:ListItem>External</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text="Division" __designer:wfdid="w1" Visible="False"></asp:Label> <asp:Label id="Label8" runat="server" Text="Type" __designer:wfdid="w2" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpTextDisabled" Width="191px" AutoPostBack="True" Visible="False" Enabled="False"></asp:DropDownList><asp:DropDownList id="FilterDDLType" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w3" AutoPostBack="True" Visible="False"><asp:ListItem>Detail</asp:ListItem>
<asp:ListItem Value="Summary">Summary</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnExportToPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnExportToExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upReportForm">
                                                                                <ProgressTemplate>
                                                                                    <span></span><span style="font-weight: bold; font-size: 10pt; color: purple">
                                                                                        <asp:Image ID="imgLoading" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />
                                                                                        Please Wait .....</span><br />
                                                                                </ProgressTemplate>
                                                                            </asp:UpdateProgress> </TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnViewReport" />
                                                            <asp:PostBackTrigger ControlID="btnExportToPdf" />
                                                            <asp:PostBackTrigger ControlID="btnExportToExcel" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Label" style="height: 10px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="Label" valign="top">
                                                    <CR:CrystalReportViewer ID="crvReportForm" runat="server" AutoDataBind="True" HasCrystalLogo="False" HasExportButton="False" HasPrintButton="False" HasToggleGroupTreeButton="False" HasRefreshButton="True" HasSearchButton="False" ShowAllPageIds="True" DisplayGroupTree="False" />
                                                </td>
                                            </tr>
                                        </table>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <HeaderTemplate>
                            &nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel id="upListSupp" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListSupp" runat="server" CssClass="modalBox" Width="650px" Visible="False"><TABLE width="100%"><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSupp" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Supplier"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListSupp" runat="server" Width="100%" DefaultButton="btnFindListSupp">Filter : <asp:DropDownList id="DDLFilterListSupp" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="suppcode">Code</asp:ListItem>
<asp:ListItem Value="suppname">Name</asp:ListItem>
<asp:ListItem Value="suppaddr">Address</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListSupp" runat="server" CssClass="inpText" Width="150px"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListSupp" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton><asp:ImageButton id="btnViewAllListSupp" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3><asp:ImageButton id="btnSelectAllSupp" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton><asp:ImageButton id="btnSelectNoneSupp" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton><asp:ImageButton id="btnViewCheckedSupp" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3>&nbsp;<asp:GridView id="gvListSupp" runat="server" BorderColor="Black" ForeColor="#333333" Width="98%" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="suppoid,suppcode,suppname" BorderStyle="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:CheckBox id="cbLMSupp" runat="server" ToolTip='<%# eval("suppoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="suppcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListSupp" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbCloseListSupp" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSupp" runat="server" TargetControlID="btnHideListSupp" BackgroundCssClass="modalBackground" PopupControlID="pnlListSupp" PopupDragHandleControlID="lblListSupp" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListSupp" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    &nbsp;<br />
    <asp:UpdatePanel id="upListMR" runat="server">
        <contenttemplate>
<asp:Panel id="PanelListMR" runat="server" CssClass="modalBox" Width="650px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListMR" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of RM Recieved"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMR" runat="server" Width="100%" DefaultButton="btnFindListMR"><asp:Label id="Label24" runat="server" Text="Filter :"></asp:Label> <asp:DropDownList id="DDLFilterListMR" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="mrrawno">MR No</asp:ListItem>
<asp:ListItem Value="suppname">Supplier</asp:ListItem>
<asp:ListItem Value="mrrawmstnote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListMR" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListMR" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewAllListMR" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAllMR" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneMR" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedMR" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMR" runat="server" BorderColor="Black" ForeColor="#333333" Width="98%" BorderStyle="None" DataKeyNames="mrrawmstoid,mrrawno,mrdate,suppname,mrrawmststatus,mrrawmstnote" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:CheckBox id="cbLMMR" runat="server" ToolTip='<%# eval("mrrawmstoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="mrrawmstoid" HeaderText="Draft No">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="mrrawno" HeaderText="MR No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="mrdate" HeaderText="MR Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="mrrawmstnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="mrrawmststatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListMR" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbListMR" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMR" runat="server" TargetControlID="btnHiddenListMR" PopupDragHandleControlID="lblListMR" PopupControlID="PanelListMR" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListMR" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    &nbsp;&nbsp;
    <asp:UpdatePanel id="upListMat" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListMat" runat="server" CssClass="modalBox" Width="900px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Medium" Font-Bold="True" Font-Underline="False">List of Item</asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMat" runat="server" Width="100%" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="itemCode">Code</asp:ListItem>
<asp:ListItem Value="itemShortDescription">Short Description</asp:ListItem>
<asp:ListItem Value="itemLongDescription">Long Description</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" CssClass="inpText" Width="200px"></asp:TextBox> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD id="TD13" class="Label" align=center colSpan=3 runat="server" Visible="false"><asp:CheckBox id="cbCat01" runat="server" Text="Cat 1 :"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat01" runat="server" CssClass="inpText" Width="150px" AutoPostBack="True"></asp:DropDownList> <asp:CheckBox id="cbCat02" runat="server" Text="Cat 2 :"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat02" runat="server" CssClass="inpText" Width="150px" AutoPostBack="True"></asp:DropDownList> <asp:CheckBox id="cbCat03" runat="server" Text="Cat 3 :"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat03" runat="server" CssClass="inpText" Width="150px" AutoPostBack="True"></asp:DropDownList> <asp:CheckBox id="cbCat04" runat="server" Text="Cat 4 :"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat04" runat="server" CssClass="inpText" Width="150px" AutoPostBack="True"></asp:DropDownList></TD></TR></TBODY></TABLE></asp:Panel></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNone" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewChecked" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:UpdateProgress id="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat"><ProgressTemplate>
<TABLE style="WIDTH: 200px"><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple">Load Data, Please Wait ...</TD></TR><TR><TD><asp:Image id="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif"></asp:Image></TD></TR></TABLE>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD class="Label" align=left colSpan=3>Show <asp:TextBox id="tbData" runat="server" CssClass="inpText" Width="25px"></asp:TextBox> Data Per Page <asp:LinkButton id="lbShowData" runat="server">>> View</asp:LinkButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat" runat="server" BorderColor="Black" ForeColor="#333333" Width="96%" BorderStyle="None" DataKeyNames="itemCode" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:CheckBox id="cbLM" runat="server" __designer:wfdid="w2" ToolTip='<%# eval("itemoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemCode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemShortDescription" HeaderText="Short Desc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemLongDescription" HeaderText="Long Desc">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
&nbsp;
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListMat" runat="server">[ Add To List ]</asp:LinkButton>&nbsp;- <asp:LinkButton id="lkbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE><ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" Drag="True" PopupDragHandleControlID="lblTitleListMat" PopupControlID="pnlListMat" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender></asp:Panel> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

