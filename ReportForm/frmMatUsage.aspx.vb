Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_MaterialUsageNonKIK
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If FilterPeriod1.Text = "" Or FilterPeriod2.Text = "" Then
            showMessage("Please fill both of Period 1 and Period 2 first!", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitAllDDL()
        ' DDL Division
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If FillDDL(DDLBusUnit, sSql) Then
            InitDDLDept()
        End If
        ' DDL Warehouse
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='WAREHOUSE' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDL(DDLWarehouse, sSql)
        sSql = "SELECT gencode,gendesc FROM QL_mstgen WHERE gengroup='GROUPITEM' AND cmpcode='" & CompnyCode & "'"
        FillDDL(DDLMatType, sSql)
    End Sub

    Private Sub InitDDLDept()
        ' DDL Department
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE activeflag='ACTIVE' AND cmpcode='" & DDLBusUnit.SelectedValue & "' ORDER BY deptname"
        FillDDL(DDLDept, sSql)
    End Sub

    Private Sub BindListUsage()
        sSql = "SELECT mum.matusagemstoid, CONVERT(VARCHAR(20), matusagemstoid) AS draftno,mum.matusageno, CONVERT(VARCHAR(10), mum.matusagedate, 101) AS matusagedate,mum.matusagemstnote, 'False' AS checkvalue FROM QL_trnmatusagemst mum WHERE mum.cmpcode='" & DDLBusUnit.SelectedValue & "' ORDER BY matusagemstoid"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnmatusagemst")
        If dt.Rows.Count > 0 Then
            Session("TblListUsage") = dt
            Session("TblListUsageView") = Session("TblListUsage")
            gvListUsage.DataSource = Session("TblListUsageView")
            gvListUsage.DataBind()
            cProc.SetModalPopUpExtender(btnHideListUsage, pnlListUsage, mpeListUsage, True)
        Else
            showMessage("No Material Usage Non KIK data available for this time.", 2)
        End If
    End Sub

    Private Sub UpdateCheckedListUsage()
        If Session("TblListUsage") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListUsage")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListUsage.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListUsage.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matusagemstoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListUsage") = dt
        End If
        If Session("TblListUsageView") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListUsageView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListUsage.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListUsage.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matusagemstoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListUsageView") = dt
        End If
    End Sub

    Private Sub BindListMat()
        sSql = "SELECT DISTINCT mud.matusagerefoid AS matoid,m.itemCode AS matcode, m.itemLongDescription AS matlongdesc, gendesc AS matunit, 'False' AS checkvalue FROm QL_trnmatusagedtl mud INNER JOIN QL_mstitem m ON itemoid=mud.matusagerefoid INNER JOIN QL_mstgen g ON genoid=mud.matusageunitoid WHERE mud.cmpcode='" & DDLBusUnit.SelectedValue & "' AND m.itemgroup='" & DDLMatType.SelectedValue & "' ORDER BY matcode"

        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnmatusagedtl")
        If dt.Rows.Count > 0 Then
            Session("TblListMat") = dt
            Session("TblListMatView") = Session("TblListMat")
            gvListMat.DataSource = Session("TblListMatView")
            gvListMat.DataBind()
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            showMessage("No Material data available for this time.", 2)
        End If
    End Sub

    Private Sub UpdateCheckedListMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMat") = dt
        End If
        If Session("TblListMatView") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMatView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMatView") = dt
        End If
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        If Not IsValidPeriod() Then
            Exit Sub
        End If
        Try
            Dim sWhere As String = " WHERE cmpcode='" & CompnyCode & "' AND " & DDLDate.SelectedValue & ">=CONVERT(DATETIME, '" & FilterPeriod1.Text & " 00:00:00') AND " & DDLDate.SelectedValue & "<=CONVERT(DATETIME, '" & FilterPeriod2.Text & " 23:59:59')"
            If FilterUsage.Text <> "" Then
                Dim sWhereCode As String = ""
                Dim sNo() As String = Split(FilterUsage.Text, ";")
                If sNo.Length > 0 Then
                    For C1 As Integer = 0 To sNo.Length - 1
                        If sNo(C1) <> "" Then
                            sWhereCode &= DDLUsage.SelectedValue & " LIKE '" & Tchar(sNo(C1)) & "' OR "
                        End If
                    Next
                    If sWhereCode <> "" Then
                        sWhereCode = Left(sWhereCode, sWhereCode.Length - 4)
                        sWhere &= " AND (" & sWhereCode & ")"
                    End If
                End If
            End If
            If DDLType.SelectedValue = "DETAIL" Then
                If FilterMaterial.Text <> "" Then
                    Dim sWhereCode As String = ""
                    Dim sNo() As String = Split(FilterMaterial.Text, ";")
                    If sNo.Length > 0 Then
                        For C1 As Integer = 0 To sNo.Length - 1
                            If sNo(C1) <> "" Then
                                sWhereCode &= "[Code] LIKE '" & Tchar(sNo(C1)) & "' OR "
                            End If
                        Next
                        If sWhereCode <> "" Then
                            sWhereCode = Left(sWhereCode, sWhereCode.Length - 4)
                            sWhere &= " AND (" & sWhereCode & ")"
                        End If
                    End If
                End If
            End If
            If lbDept.Items.Count > 0 Then
                Dim sOid As String = ""
                For C1 As Integer = 0 To lbDept.Items.Count - 1
                    sOid &= lbDept.Items(C1).Value & ","
                Next
                If sOid <> "" Then
                    sOid = Left(sOid, sOid.Length - 1)
                    sWhere &= " AND [Dept ID] IN (" & sOid & ")"
                End If
            End If
            If lbWarehouse.Items.Count > 0 Then
                Dim sOid As String = ""
                For C1 As Integer = 0 To lbWarehouse.Items.Count - 1
                    sOid &= lbWarehouse.Items(C1).Value & ","
                Next
                If sOid <> "" Then
                    sOid = Left(sOid, sOid.Length - 1)
                    sWhere &= " AND [WH ID] IN (" & sOid & ")"
                End If
            End If
            Dim sStatus As String = ""
            For C1 As Integer = 0 To cblStatus.Items.Count - 1
                If cblStatus.Items(C1).Selected Then
                    sStatus &= "'" & cblStatus.Items(C1).Value & "',"
                End If
            Next
            If sStatus <> "" Then
                sStatus = Left(sStatus, sStatus.Length - 1)
                sWhere &= " AND [Status] IN (" & sStatus & ")"
            End If
            Dim sSortBy As String = ""
            Dim sSplitSort() As String = DDLSortBy.SelectedValue.Split(",")
            If sSplitSort.Length > 0 Then
                For C1 As Integer = 0 To sSplitSort.Length - 1
                    If sSplitSort(C1) <> "" Then
                        sSortBy &= sSplitSort(C1) & " " & DDLOrder.SelectedValue & ", "
                    End If
                Next
            End If
            Dim sRptName As String = ""
            Dim dtReport As DataTable = Nothing
            If DDLType.SelectedValue = "SUMMARY" Then
                sRptName = "MatUsageNonKIKSummaryReport"
                If sType = "Print Excel" Then
                    report.Load(Server.MapPath(folderReport & "rptMatUsageSum.rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptMatUsageSum.rpt"))
                End If
                sSql = "SELECT * FROM ("
				sSql &= "SELECT mum.cmpcode, (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=mum.cmpcode) AS [Business Unit], CONVERT(VARCHAR(20), mum.matusagemstoid) AS [Draft No.], mum.matusagedate AS [Usage Date],mum.matusageno AS [Usage No.], mum.deptoid AS [Dept ID], (SELECT deptname FROM QL_mstdept de WHERE de.deptoid=mum.deptoid) AS [Department], mum.matusagemststatus AS [Status], mum.matusagemstnote AS [Header Note], UPPER(mum.createuser) AS [Create User], (CASE mum.matusagemststatus WHEN 'In Process' THEN '' ELSE UPPER(mum.upduser) END) [Posting User], (CASE mum.matusagemststatus WHEN 'In Process' THEN NULL ELSE mum.updtime END) AS [Posting Date],(SELECT SUM(matusagevalueidr) FROM QL_trnmatusagedtl sd WHERE mum.matusagemstoid=sd.matusagemstoid) AS ValueIdr,(SELECT SUM(matusagevalueusd) FROM QL_trnmatusagedtl sd WHERE mum.matusagemstoid=sd.matusagemstoid) AS ValueUsd FROM QL_trnmatusagemst mum"
                sSql &= ") AS QL_tblsummary " & sWhere & " ORDER BY [Usage No.], [Draft No.]"
                dtReport = cKon.ambiltabel(sSql, "QL_tblsummary")
            Else
                sRptName = "MatUsageNonKIKDetailReport"
                Dim sExt As String = "", sGroup As String = ""
                If sType = "Print Excel" Then
                    sExt = "" '"_Excel"
                End If
                If DDLGroupBy.SelectedIndex = 0 Then
                    sGroup = "PerNo"
                Else
                    sGroup = "PerCode"
                End If
                If sType = "Print Excel" Then
					report.Load(Server.MapPath(folderReport & "rptMatUsageDtl" & sGroup & ".rpt"))
                Else
					report.Load(Server.MapPath(folderReport & "rptMatUsageDtl" & sGroup & ".rpt"))
                End If

                sSql = "SELECT * FROM ("
				sSql &= "SELECT mum.cmpcode, (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=mum.cmpcode) [Business Unit], CONVERT(VARCHAR(20), mum.matusagemstoid) AS [Draft No.], mum.matusagedate AS [Usage Date], mum.matusageno AS [Usage No.], mum.deptoid AS [Dept ID], (SELECT deptname FROM QL_mstdept de WHERE de.deptoid=mum.deptoid) AS [Department], mum.matusagemststatus AS [Status], mum.matusagemstnote AS [Header Note], UPPER(mum.createuser) AS [Create User], (CASE mum.matusagemststatus WHEN 'In Process' THEN '' ELSE UPPER(mum.upduser) END) [Posting User], (CASE mum.matusagemststatus WHEN 'In Process' THEN NULL ELSE mum.updtime END) [Posting Date], ISNULL((SELECT reqno FROM QL_trnreqmst reqm WHERE reqm.cmpcode=mud.cmpcode AND reqm.reqmstoid=mud.matreqmstoid), '') AS [Request No.], mud.matusagewhoid [WH ID], g1.gendesc [Warehouse], m.itemgroup AS [Type], m.itemcode AS [Code],m.itemLongDescription AS [Material], matusageqty AS [Qty], g2.gendesc AS [Unit], mud.matusagedtlnote AS [Note],mud.matusagevalueidr AS ValueIdr,mud.matusagevalueusd AS ValueUsd" & _
				" FROM QL_trnmatusagemst mum" & _
				" INNER JOIN QL_trnmatusagedtl mud ON mud.cmpcode=mum.cmpcode" & _
				" AND mud.matusagemstoid=mum.matusagemstoid" & _
				" INNER JOIN QL_mstitem m ON m.itemoid=mud.matusagerefoid" & _
				" INNER JOIN QL_mstgen g1 ON g1.genoid=mud.matusagewhoid" & _
				" AND g1.gengroup='WAREHOUSE'" & _
				" INNER JOIN QL_mstgen g2 ON g2.genoid=mud.matusageunitoid" & _
				" AND g2.gengroup='UNIT'"
                sSql &= ") AS QL_tbldetail " & sWhere
                sSql &= " ORDER BY cmpcode, " & DDLGroupBy.SelectedValue
                If sSortBy <> "" Then
                    sSql &= ", " & Left(sSortBy, sSortBy.Length - 2)
                End If
                dtReport = cKon.ambiltabel(sSql, "QL_tbldetail")
            End If
            report.SetDataSource(dtReport)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            cProc.SetDBLogonForReport(report)
			'report.PrintOptions.PaperSize = PaperSize.PaperFolio
            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, sRptName)
                report.Close()
                report.Dispose()
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, sRptName)
                report.Close()
                report.Dispose()
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmMatUsage.aspx")
        End If
        If checkPagePermission("~\ReportForm\frmMatUsage.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Material Usage Non KIK Report"
        If Not Page.IsPostBack Then
            InitAllDDL()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            DDLType_SelectedIndexChanged(Nothing, Nothing)
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListUsage") Is Nothing And Session("WarningListUsage") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListUsage") Then
                Session("WarningListUsage") = Nothing
                mpeListUsage.Show()
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                mpeListMat.Show()
            End If
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLDept()
    End Sub

    Protected Sub DDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLType.SelectedIndexChanged
        Dim bVal As Boolean = True
        If DDLType.SelectedValue = "SUMMARY" Then
            bVal = False
        End If
        DDLGroupBy.SelectedIndex = -1
        lblMat.Visible = bVal : septMat.Visible = bVal : DDLMatType.Visible = bVal : FilterMaterial.Visible = bVal : btnSearchMat.Visible = bVal : btnClearMat.Visible = bVal
        lblWH.Visible = bVal : septWH.Visible = bVal : DDLWarehouse.Visible = bVal : btnAddWH.Visible = bVal : lbWarehouse.Visible = bVal : btnMinWH.Visible = bVal
        lblGroupBy.Visible = bVal : septGroupBy.Visible = bVal : DDLGroupBy.Visible = bVal
        lblSortBy.Visible = bVal : septSortBy.Visible = bVal : DDLSortBy.Visible = bVal : DDLOrder.Visible = bVal
        DDLGroupBy_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub btnSearchUsage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchUsage.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2) : Exit Sub
        End If
        FilterDDLListUsage.SelectedIndex = -1 : FilterTextListUsage.Text = ""
        Session("TblListUsage") = Nothing : Session("TblListUsageView") = Nothing
        gvListUsage.DataSource = Nothing : gvListUsage.DataBind()
        BindListUsage()
    End Sub

    Protected Sub btnClearUsage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearUsage.Click
        FilterUsage.Text = ""
    End Sub

    Protected Sub btnFindListUsage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListUsage.Click
        UpdateCheckedListUsage()
        Dim dt As DataTable = Session("TblListUsage")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = FilterDDLListUsage.SelectedValue & " LIKE '%" & Tchar(FilterTextListUsage.Text) & "%'"
        If dv.Count > 0 Then
            Session("TblListUsageView") = dv.ToTable
            gvListUsage.DataSource = Session("TblListUsageView")
            gvListUsage.DataBind()
            dv.RowFilter = ""
            mpeListUsage.Show()
        Else
            dv.RowFilter = ""
            Session("WarningListUsage") = "Material Usage Non KIK data can't be found!"
            showMessage(Session("WarningListUsage"), 2)
        End If
    End Sub

    Protected Sub btnAllListUsage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListUsage.Click
        UpdateCheckedListUsage()
        FilterDDLListUsage.SelectedIndex = -1 : FilterTextListUsage.Text = ""
        Session("TblListUsageView") = Session("TblListUsage")
        gvListUsage.DataSource = Session("TblListUsageView")
        gvListUsage.DataBind()
        mpeListUsage.Show()
    End Sub

    Protected Sub gvListUsage_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListUsage.PageIndexChanging
        UpdateCheckedListUsage()
        gvListUsage.PageIndex = e.NewPageIndex
        gvListUsage.DataSource = Session("TblListUsageView")
        gvListUsage.DataBind()
        mpeListUsage.Show()
    End Sub

    Protected Sub cbHdrLMUsage_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For C1 As Integer = 0 To gvListUsage.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListUsage.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        mpeListUsage.Show()
    End Sub

    Protected Sub lbAddToListUsage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListUsage.Click
        UpdateCheckedListUsage()
        Dim dt As DataTable = Session("TblListUsage")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = "checkvalue='True'"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                If DDLUsage.SelectedIndex = 0 Then
                    If dv(C1)("matusageno").ToString <> "" Then
                        FilterUsage.Text &= dv(C1)("matusageno").ToString & ";"
                    End If
                Else
                    FilterUsage.Text &= dv(C1)("matusagemstoid").ToString & ";"
                End If
            Next
            If FilterUsage.Text <> "" Then
                FilterUsage.Text = Left(FilterUsage.Text, FilterUsage.Text.Length - 1)
            End If
            dv.RowFilter = ""
            cProc.SetModalPopUpExtender(btnHideListUsage, pnlListUsage, mpeListUsage, False)
        Else
            dv.RowFilter = ""
            Session("WarningListUsage") = "Please select some Material Usage Non KIK data first!"
            showMessage(Session("WarningListUsage"), 2)
        End If
    End Sub

    Protected Sub lbSelectAllListUsage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSelectAllListUsage.Click
        UpdateCheckedListUsage()
        Dim dt As DataTable = Session("TblListUsageView")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            If DDLUsage.SelectedIndex = 0 Then
                If dt.Rows(C1)("matusageno").ToString <> "" Then
                    FilterUsage.Text &= dt.Rows(C1)("matusageno").ToString & ";"
                End If
            Else
                FilterUsage.Text &= dt.Rows(C1)("matusagemstoid").ToString & ";"
            End If
        Next
        If FilterUsage.Text <> "" Then
            FilterUsage.Text = Left(FilterUsage.Text, FilterUsage.Text.Length - 1)
        End If
        cProc.SetModalPopUpExtender(btnHideListUsage, pnlListUsage, mpeListUsage, False)
    End Sub

    Protected Sub lbCloseListUsage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListUsage.Click
        cProc.SetModalPopUpExtender(btnHideListUsage, pnlListUsage, mpeListUsage, False)
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2) : Exit Sub
        End If
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        Session("TblListMat") = Nothing : Session("TblListMatView") = Nothing
        gvListMat.DataSource = Session("TblListMat") : gvListMat.DataBind()
        lblListMat.Text = "List Of " & DDLMatType.SelectedItem.Text
        BindListMat()
    End Sub

    Protected Sub btnClearMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearMat.Click
        FilterMaterial.Text = ""
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        UpdateCheckedListMat()
        Dim dt As DataTable = Session("TblListMat")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        If dv.Count > 0 Then
            Session("TblListMatView") = dv.ToTable
            gvListMat.DataSource = Session("TblListMatView")
            gvListMat.DataBind()
            dv.RowFilter = ""
            mpeListMat.Show()
        Else
            dv.RowFilter = ""
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        UpdateCheckedListMat()
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        Session("TblListMatView") = Session("TblListMat")
        gvListMat.DataSource = Session("TblListMatView")
        gvListMat.DataBind()
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        UpdateCheckedListMat()
        gvListMat.PageIndex = e.NewPageIndex
        gvListMat.DataSource = Session("TblListMatView")
        gvListMat.DataBind()
        mpeListMat.Show()
    End Sub

    Protected Sub cbHdrLM_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For C1 As Integer = 0 To gvListMat.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        mpeListMat.Show()
    End Sub

    Protected Sub lbAddToListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListMat.Click
        UpdateCheckedListMat()
        Dim dt As DataTable = Session("TblListMat")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = "checkvalue='True'"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                FilterMaterial.Text &= DDLMatType.SelectedValue & "-" & dv(C1)("matcode").ToString & ";"
            Next
            If FilterMaterial.Text <> "" Then
                FilterMaterial.Text = Left(FilterMaterial.Text, FilterMaterial.Text.Length - 1)
            End If
            dv.RowFilter = ""
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
        Else
            dv.RowFilter = ""
            Session("WarningListMat") = "Please select some Material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub lbSelectAllListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSelectAllListMat.Click
        UpdateCheckedListMat()
        Dim dt As DataTable = Session("TblListMat")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            FilterMaterial.Text &= DDLMatType.SelectedValue & "-" & dt.Rows(C1)("matcode").ToString & ";"
        Next
        If FilterMaterial.Text <> "" Then
            FilterMaterial.Text = Left(FilterMaterial.Text, FilterMaterial.Text.Length - 1)
        End If
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub btnAddDept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddDept.Click
        If DDLDept.SelectedValue <> "" Then
            If lbDept.Items.Count > 0 Then
                If Not lbDept.Items.Contains(lbDept.Items.FindByValue(DDLDept.SelectedValue)) Then
                    Dim objList As New ListItem
                    objList.Text = DDLDept.SelectedItem.Text : objList.Value = DDLDept.SelectedValue
                    lbDept.Items.Add(objList)
                End If
            Else
                Dim objList As New ListItem
                objList.Text = DDLDept.SelectedItem.Text : objList.Value = DDLDept.SelectedValue
                lbDept.Items.Add(objList)
            End If
        End If
    End Sub

    Protected Sub btnMinDept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMinDept.Click
        If lbDept.Items.Count > 0 Then
            Dim objList As ListItem = lbDept.SelectedItem
            lbDept.Items.Remove(objList)
        End If
    End Sub

    Protected Sub btnAddWH_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddWH.Click
        If DDLWarehouse.SelectedValue <> "" Then
            If lbWarehouse.Items.Count > 0 Then
                If Not lbWarehouse.Items.Contains(lbWarehouse.Items.FindByValue(DDLWarehouse.SelectedValue)) Then
                    Dim objList As New ListItem
                    objList.Text = DDLWarehouse.SelectedItem.Text : objList.Value = DDLWarehouse.SelectedValue
                    lbWarehouse.Items.Add(objList)
                End If
            Else
                Dim objList As New ListItem
                objList.Text = DDLWarehouse.SelectedItem.Text : objList.Value = DDLWarehouse.SelectedValue
                lbWarehouse.Items.Add(objList)
            End If
        End If
    End Sub

    Protected Sub btnMinWH_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMinWH.Click
        If lbWarehouse.Items.Count > 0 Then
            Dim objList As ListItem = lbWarehouse.SelectedItem
            lbWarehouse.Items.Remove(objList)
        End If
    End Sub

    Protected Sub DDLGroupBy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLGroupBy.SelectedIndexChanged
        For C1 As Integer = 0 To DDLSortBy.Items.Count - 1
            DDLSortBy.Items(C1).Enabled = True
        Next
        DDLSortBy.Items(DDLGroupBy.SelectedIndex).Enabled = False
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmMatUsage.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not report Is Nothing Then
                If report.IsLoaded Then
                    report.Dispose()
                    report.Close()
                End If
            End If
        Catch ex As Exception
            report.Dispose()
            report.Close()
        End Try
    End Sub
#End Region

End Class
