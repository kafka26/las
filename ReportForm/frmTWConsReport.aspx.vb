Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_ReportTW
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function 'OK

    Private Function UpdateCheckedTrans() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblTrans") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblTrans")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListTrans.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListTrans.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "transmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblTrans") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedTrans2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblTransView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblTrans")
            Dim dtTbl2 As DataTable = Session("TblTransView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListTrans.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListTrans.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "transmstoid=" & cbOid
                                dtView2.RowFilter = "transmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblTrans") = dtTbl
                Session("TblTransView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matoid=" & cbOid
                                dtView2.RowFilter = "matoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyCode
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub 'OK

    Private Sub InitAllDDL()
        ' Init DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)
        InitDDLWH()
        'Init DDL Customer
        sSql = "SELECT custoid, custname FROM QL_mstcust WHERE cmpcode='" & CompnyCode & "' AND custoid IN (SELECT DISTINCT transitemmstres3 FROM QL_trntransitemmst WHERE iskonsinyasi='True') ORDER BY custname"
        FillDDL(DDLCust, sSql)
    End Sub 'OK

    Private Sub BindListTrans()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, mt.transitemmstoid AS transmstoid, mt.transitemno AS transno, CONVERT(VARCHAR(10), mt.transitemdate, 101) AS transdate,mt.transitemdate, mt.transitemmststatus AS transmststatus, mt.transitemmstnote AS transmstnote FROM QL_trntransitemmst mt "

        If DDLBusUnit.SelectedValue <> "ALL" Then
            sSql &= "WHERE mt.cmpcode='" & DDLBusUnit.SelectedValue & "'"
        Else
            sSql &= "WHERE mt.cmpcode LIKE '%%'"
        End If

        If cbPeriode.Checked Then
            If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
                If IsValidPeriod() Then
                    sSql &= " AND mt.transitemdate>='" & FilterPeriod1.Text & " 00:00:00' AND mt.transitemdate<='" & FilterPeriod2.Text & " 23:59:59'"
                Else
                    Exit Sub
                End If
            End If
        End If

        sSql &= "ORDER BY mt.transitemdate DESC, mt.transitemmstoid DESC"
        Session("TblTrans") = cKon.ambiltabel(sSql, "QL_trntransitemmst")
    End Sub

    Private Sub InitDDLWH()
        ' Init DDL Warehouse
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND activeflag='ACTIVE' AND gengroup='WAREHOUSE' AND genother1=''"
        FillDDL(DDLFromWarehouse, sSql)
        FillDDL(DDLToWarehouse, sSql)
    End Sub 'OK

    Private Sub InitFilterDDLCat1()
        'Fill DDL Category1
        sSql = "SELECT cat1code, cat1code+' - '+cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat1res1='Raw' Order By cat1code"
        FillDDL(FilterDDLCat01, sSql)
        If FilterDDLCat01.Items.Count > 0 Then
            InitFilterDDLCat2()
        Else
            FilterDDLCat02.Items.Clear()
            FilterDDLCat03.Items.Clear()
            FilterDDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitFilterDDLCat2()
        'Fill DDL Category2
        sSql = "SELECT cat2code, cat2code+' - '+cat2shortdesc FROM QL_mstcat2 cat2 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat2.cmpcode AND cat1.cat1oid=cat2.cat1oid AND cat1.cat1code='" & FilterDDLCat01.SelectedValue & "' WHERE cat2.cmpcode='" & CompnyCode & "' AND cat2.activeflag='ACTIVE' AND cat2res1='Raw' Order By cat2code"
        FillDDL(FilterDDLCat02, sSql)
        If FilterDDLCat02.Items.Count > 0 Then
            InitFilterDDLCat3()
        Else
            FilterDDLCat03.Items.Clear()
            FilterDDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitFilterDDLCat3()
        'Fill DDL Category3
        sSql = "SELECT cat3code, cat3code+' - '+cat3shortdesc FROM QL_mstcat3 cat3 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat3.cmpcode AND cat1.cat1oid=cat3.cat1oid AND cat1.cat1code='" & FilterDDLCat01.SelectedValue & "' INNER JOIN QL_mstcat2 cat2 ON cat2.cmpcode=cat3.cmpcode AND cat2.cat2oid=cat3.cat2oid AND cat2.cat2code='" & FilterDDLCat02.SelectedValue & "' WHERE cat3.cmpcode='" & CompnyCode & "' AND cat3.activeflag='ACTIVE' AND cat3res1='Raw' Order By cat3code"
        FillDDL(FilterDDLCat03, sSql)
        'If FilterDDLCat03.Items.Count > 0 Then
        '    InitFilterDDLCat4()
        'Else
        '    FilterDDLCat04.Items.Clear()
        'End If
    End Sub

    Private Sub InitFilterDDLCat4()
        'Fill DDL Category4
        'sSql = "SELECT cat4code, cat4code+' - '+cat4shortdesc FROM QL_mstcat4 cat4 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat4.cmpcode AND cat1.cat1oid=cat4.cat1oid AND cat1.cat1code='" & FilterDDLCat01.SelectedValue & "' INNER JOIN QL_mstcat2 cat2 ON cat2.cmpcode=cat4.cmpcode AND cat2.cat2oid=cat4.cat2oid AND cat2.cat2code='" & FilterDDLCat02.SelectedValue & "' INNER JOIN QL_mstcat3 cat3 ON cat3.cmpcode=cat4.cmpcode AND cat3.cat3oid=cat4.cat3oid AND cat3.cat3code='" & FilterDDLCat03.SelectedValue & "' WHERE cat4.cmpcode='" & CompnyCode & "' AND cat4.activeflag='ACTIVE' AND cat4res1='Raw' Order By cat4code"
        'FillDDL(FilterDDLCat04, sSql)
    End Sub

    Private Sub BindListMat()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, md.itemoid AS matoid, m.itemShortDescription AS matshortdesc, m.itemLongDescription AS matlongdesc, m.itemCode AS matcode, g2.gendesc Unit,mt.transitemno,mt.transitemdate,md.transitemfromwhoid,md.transitemtowhoid" & _
        " FROM QL_trntransitemdtl md" & _
        " INNER JOIN QL_trntransitemmst mt ON mt.cmpcode=md.cmpcode" & _
        " AND mt.transitemmstoid=md.transitemmstoid" & _
        " INNER JOIN QL_mstitem m ON m.itemoid=md.itemoid" & _
        " INNER JOIN QL_mstgen g2 ON g2.genoid=md.transitemunitoid"
        If DDLBusUnit.SelectedValue <> "ALL" Then
            sSql &= " WHERE mt.cmpcode='" & DDLBusUnit.SelectedValue & "'"
        Else
            sSql &= " WHERE mt.cmpcode LIKE '%%'"
        End If

        If cbTrans.Checked Then
            If transno.Text <> "" Then
                If DDLTransNo.SelectedValue = "Trans No." Then
                    Dim sTransno() As String = Split(transno.Text, ";")
                    sSql &= " AND ("
                    For c1 As Integer = 0 To sTransno.Length - 1
                        sSql &= " mt.transitemno LIKE '%" & Tchar(sTransno(c1)) & "%'"
                        If c1 < sTransno.Length - 1 Then
                            sSql &= " OR "
                        End If
                    Next
                    sSql &= ")"
                Else
                    Dim sTransno() As String = Split(transno.Text, ";")
                    sSql &= " AND ("
                    For c1 As Integer = 0 To sTransno.Length - 1
                        sSql &= " mt.transitemno = " & ToDouble(sTransno(c1))
                        If c1 < sTransno.Length - 1 Then
                            sSql &= " OR "
                        End If
                    Next
                    sSql &= ")"
                End If
            End If
        End If

        If cbPeriode.Checked Then
            If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
                If IsValidPeriod() Then
                    sSql &= " AND mt.transitemdate>='" & FilterPeriod1.Text & " 00:00:00' AND mt.transitemdate<='" & FilterPeriod2.Text & " 23:59:59'"
                Else
                    Exit Sub
                End If
            End If
        End If

        If cbWarehouse.Checked Then
            If DDLFromWarehouse.SelectedValue <> "" And DDLToWarehouse.SelectedValue <> "" Then
                sSql &= " AND md.transitemfromwhoid=" & DDLFromWarehouse.SelectedValue & " AND md.transitemtowhoid=" & DDLToWarehouse.SelectedValue
            Else
                showMessage("Please select Warehouse first!", 2)
                Exit Sub
            End If
        End If
        Session("TblMat") = cKon.ambiltabel(sSql, "QL_mstitem")
    End Sub 'OK

    Private Sub SetEnableMat(ByVal bValue As Boolean)
        cbWarehouse.Visible = bValue
        DDLFromWarehouse.Visible = bValue
        DDLToWarehouse.Visible = bValue
        lbWh.Visible = bValue
        lbTo.Visible = bValue
        cbType.Visible = bValue
        lbMat.Visible = bValue
        matcode.Visible = bValue
        imbFindMat.Visible = bValue
        imbEraseMat.Visible = bValue
        cbTrans_CheckedChanged(Nothing, Nothing)
        cbType.Checked = False
        cbType_CheckedChanged(Nothing, Nothing)
    End Sub 'OK

    Private Sub ShowReport(ByVal sType As String)
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select business unit first!", 2)
            Exit Sub
        End If
        If cbPeriode.Checked Then If Not IsValidPeriod() Then Exit Sub
        If transno.Text = "" And cbTrans.Checked Then
            showMessage("Please select material Transfer first!", 2)
            Exit Sub
        End If
        If cbWarehouse.Checked Then
            If DDLFromWarehouse.SelectedValue = "" Then
                showMessage("Please select from warehouse first!", 2)
                Exit Sub
            End If
            If DDLToWarehouse.SelectedValue = "" Then
                showMessage("Please select to warehouse first!", 2)
                Exit Sub
            End If
        End If
        If matcode.Text = "" And cbType.Checked Then
            showMessage("Please select material first!", 2)
            Exit Sub
        End If
        Dim sWhere As String = ""
        Dim rptName As String = ""
        Dim Dtl As String = ""
        Dim Join As String = ""
        Try
            If DDLType.SelectedValue = "SUMMARY" Then
                If sType = "Print Excel" Then
                    report.Load(Server.MapPath(folderReport & "rptTransCons_SumXls.rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptTransCons_Sum.rpt"))
                End If
                rptName = "TransItemSummary"
            Else
                If sType = "Print Excel" Then
                    report.Load(Server.MapPath(folderReport & "rptTransCons_DtlXls.rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptTransCons_Dtl.rpt"))
                End If
                rptName = "TransItemDetail"
            End If

            If DDLType.SelectedValue = "DETAIL" Then
                Dtl = " , fw.gendesc AS [From Warehouse], tw.gendesc AS [To Warehouse], m.itemCode AS [Material Code], m.itemLongDescription AS [Material], mtd.transitemqty [Qty], u.gendesc AS [Unit], mtd.transitemdtlnote AS [Detail Note], mtd.transitemdtloid [Dtloid], salesprice [Value IDR] "
                Join = "INNER JOIN QL_trntransitemdtl mtd ON mtd.transitemmstoid=mt.transitemmstoid INNER JOIN QL_mstgen fw ON fw.genoid=mtd.transitemfromwhoid INNER JOIN QL_mstgen tw ON tw.genoid=mtd.transitemtowhoid INNER JOIN QL_mstitem m ON m.itemoid=mtd.itemoid INNER JOIN QL_mstgen u ON u.genoid=mtd.transitemunitoid "
            Else
                Dtl = ", ISNULL((SELECT SUM(salesprice * transitemqty) FROM QL_trntransitemdtl md WHERE md.transitemmstoid=mt.transitemmstoid), 0.0) [Value IDR]"
            End If
            sSql = "SELECT mt.cmpcode, (SELECT div.divname FROM QL_mstdivision div WHERE mt.cmpcode = div.cmpcode) AS [Bussines Unit], mt.transitemmstoid AS [ID], transitemdate AS [TransferDate], transitemno AS [Transfer No.], CONVERT(VARCHAR(10), mt.transitemmstoid) AS [Draft No.], transitemdocrefno AS [Doc Ref No], transitemmstnote AS [Header Note], transitemmststatus AS [Status], mt.createuser [Create User], mt.createtime [CreateDate],(CASE mt.transitemmststatus WHEN 'In Process' THEN '' ELSE mt.upduser END) AS [Post User] , (CASE mt.transitemmststatus WHEN 'In Process' THEN CONVERT(DATETIME,'1/1/1900') ELSE mt.updtime END) AS [Post Date], transitemmstto AS [Transfer To], transitemmstaddr AS [Address], transitemmstcityOid , (SELECT gendesc FROM QL_mstgen WHERE genoid=transitemmstcityOid AND gengroup='CITY' AND activeflag='ACTIVE') [City], c.custoid, c.custname [Customer] " & Dtl & " FROM QL_trntransitemmst mt INNER JOIN QL_mstcust c ON c.custoid=transitemmstres3  " & Join

            If DDLBusUnit.SelectedValue <> "ALL" Then
                sSql &= "WHERE mt.cmpcode='" & DDLBusUnit.SelectedValue & "' AND iskonsinyasi='True'"
            Else
                sSql &= "WHERE mt.cmpcode LIKE '%%' AND iskonsinyasi='True'"
            End If

            If cbPeriode.Checked Then
                If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
                    If IsValidPeriod() Then
                        sSql &= " AND mt.transitemdate>='" & FilterPeriod1.Text & " 00:00:00' AND mt.transitemdate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
            End If

            If lbCust.Items.Count > 0 Then
                Dim sOid As String = ""
                For C1 As Integer = 0 To lbCust.Items.Count - 1
                    sOid &= lbCust.Items(C1).Value & ","
                Next
                If sOid <> "" Then
                    sOid = Left(sOid, sOid.Length - 1)
                    sSql &= " AND mt.transitemmstres3 IN (" & sOid & ")"
                End If
            End If

            If cbTrans.Checked Then
                If transno.Text <> "" Then
                    If DDLTransNo.SelectedValue = "Trans No." Then
                        Dim sTransno() As String = Split(transno.Text, ";")
                        sSql &= " AND ("
                        For c1 As Integer = 0 To sTransno.Length - 1
                            sSql &= " mt.transitemno LIKE '%" & Tchar(sTransno(c1)) & "%'"
                            If c1 < sTransno.Length - 1 Then
                                sSql &= " OR "
                            End If
                        Next
                        sSql &= ")"
                    Else
                        Dim sTransno() As String = Split(transno.Text, ";")
                        sSql &= " AND ("
                        For c1 As Integer = 0 To sTransno.Length - 1
                            sSql &= " mt.transitemmstoid = " & ToDouble(sTransno(c1))
                            If c1 < sTransno.Length - 1 Then
                                sSql &= " OR "
                            End If
                        Next
                        sSql &= ")"
                    End If
                End If
            End If

            If DDLType.SelectedValue = "DETAIL" Then
                If cbWarehouse.Checked Then
                    If DDLFromWarehouse.SelectedValue <> "" And DDLToWarehouse.SelectedValue <> "" Then
                        sSql &= " AND mtd.transitemfromwhoid=" & DDLFromWarehouse.SelectedValue & " AND mtd.transitemtowhoid=" & DDLToWarehouse.SelectedValue
                    Else
                        showMessage("Please select Warehouse first!", 2)
                        Exit Sub
                    End If
                End If

                If cbType.Checked Then
                    If matcode.Text <> "" Then
                        Dim sMatcode() As String = Split(matcode.Text, ";")
                        sSql &= " AND ("
                        For c1 As Integer = 0 To sMatcode.Length - 1
                            sSql &= " m.itemCode LIKE '%" & Tchar(sMatcode(c1)) & "%'"
                            If c1 < sMatcode.Length - 1 Then
                                sSql &= " OR"
                            End If
                        Next
                        sSql &= ")"
                    End If
                End If
            End If

            'If DDLType.SelectedValue = "SUMMARY" Then
            '    sSql &= " GROUP BY fw.gendesc,tw.gendesc,m.matrawoid,m.matrawcode,m.matrawlongdesc,u.gendesc "
            'End If

            'If DDLType.SelectedValue = "SUMMARY" Then
            '    sSql &= " ORDER BY m.matrawcode ASC "
            'Else
            '    sSql &= " ORDER BY m.matrawcode ASC "
            'End If
            sSql &= " ORDER BY custname, transitemno"
            Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trntransitemmst")
            Dim dvTbl As DataView = dtTbl.DefaultView
            report.SetDataSource(dvTbl.ToTable)
            report.PrintOptions.PaperSize = PaperSize.PaperA4
            cProc.SetDBLogonForReport(report)

            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, rptName)
                report.Close()
                report.Dispose()
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, rptName)
                report.Close()
                report.Dispose()
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmTWConsReport.aspx")
        End If
        If checkPagePermission("~\ReportForm\frmTWConsReport.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Transfer Warehouse Konsinyasi Report"
        If Not Page.IsPostBack Then
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitAllDDL()
            DDLType_SelectedIndexChanged(Nothing, Nothing)
            cbPeriode.Checked = True
        End If
    End Sub 'OK

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("EmptyListTrans") Is Nothing And Session("EmptyListTrans") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListTrans") Then
                Session("EmptyListTrans") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListTrans, PanelListTrans, mpeListTrans, True)
            End If
        End If
        If Not Session("WarningListTrans") Is Nothing And Session("WarningListTrans") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListTrans") Then
                Session("WarningListTrans") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListTrans, PanelListTrans, mpeListTrans, True)
            End If
        End If
    End Sub 'OK

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLWH()
    End Sub

    Protected Sub DDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLType.SelectedIndexChanged
        If DDLType.SelectedValue = "SUMMARY" Then
            SetEnableMat(False)
        Else
            SetEnableMat(True)
        End If
    End Sub

    Protected Sub cbTrans_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbTrans.CheckedChanged
        If cbTrans.Checked Then
            imbFindTrans.Visible = True
            imbEraseTrans.Visible = True
        Else
            imbFindTrans.Visible = False
            imbEraseTrans.Visible = False
        End If
    End Sub

    Protected Sub imbFindTrans_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindTrans.Click
        If IsValidPeriod() Then
            DDLFilterListTrans.SelectedIndex = -1 : txtFilterListTrans.Text = ""
            Session("TblTrans") = Nothing
            Session("TblTransView") = Nothing
            gvListTrans.DataSource = Nothing
            gvListTrans.DataBind()
            'BindListTrans()
            cProc.SetModalPopUpExtender(btnHiddenListTrans, PanelListTrans, mpeListTrans, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbEraseTrans_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseTrans.Click
        transno.Text = ""
        cbTrans.Checked = False
        cbTrans_CheckedChanged(Nothing, Nothing)
        DDLBusUnit.Enabled = True
    End Sub

    Protected Sub btnFindListTrans_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListTrans.Click
        If Session("TblTrans") Is Nothing Then
            BindListTrans()
            If Session("TblTrans").Rows.Count <= 0 Then
                Session("EmptyListTrans") = "Transfer data can't be found!"
                showMessage(Session("EmptyListTrans"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListTrans.SelectedValue & " LIKE '%" & Tchar(txtFilterListTrans.Text) & "%'"
        If UpdateCheckedTrans() Then
            Dim dv As DataView = Session("TblTrans").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblTransView") = dv.ToTable
                gvListTrans.DataSource = Session("TblTransView")
                gvListTrans.DataBind()
                dv.RowFilter = ""
                mpeListTrans.Show()
            Else
                dv.RowFilter = ""
                Session("TblTransView") = Nothing
                gvListTrans.DataSource = Session("TblTransView")
                gvListTrans.DataBind()
                Session("WarningListTrans") = "Transfer data can't be found!"
                showMessage(Session("WarningListTrans"), 2)
            End If
        Else
            mpeListTrans.Show()
        End If
    End Sub

    Protected Sub btnViewAllListTrans_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListTrans.Click
        DDLFilterListTrans.SelectedIndex = -1 : txtFilterListTrans.Text = ""
        If Session("TblTrans") Is Nothing Then
            BindListTrans()
            If Session("TblTrans").Rows.Count <= 0 Then
                Session("EmptyListTrans") = "Transfer data can't be found!"
                showMessage(Session("EmptyListTrans"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedTrans() Then
            Dim dt As DataTable = Session("TblTrans")
            Session("TblTransView") = dt
            gvListTrans.DataSource = Session("TblTransView")
            gvListTrans.DataBind()
        End If
        mpeListTrans.Show()
    End Sub

    Protected Sub btnSelectAllTrans_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllTrans.Click
        If Not Session("TblTransView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblTransView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblTrans")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "transmstoid=" & dtTbl.Rows(C1)("transmstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblTrans") = objTbl
                Session("TblTransView") = dtTbl
                gvListTrans.DataSource = Session("TblTransView")
                gvListTrans.DataBind()
            End If
            mpeListTrans.Show()
        Else
            Session("WarningListTrans") = "Please show some Transfer data first!"
            showMessage(Session("WarningListTrans"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneTrans_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneTrans.Click
        If Not Session("TblTransView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblTransView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblTrans")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "transmstoid=" & dtTbl.Rows(C1)("transmstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblTrans") = objTbl
                Session("TblTransView") = dtTbl
                gvListTrans.DataSource = Session("TblTransView")
                gvListTrans.DataBind()
            End If
            mpeListTrans.Show()
        Else
            Session("WarningListTrans") = "Please show some Transfer data first!"
            showMessage(Session("WarningListTrans"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedTrans_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedTrans.Click
        If Session("TblTrans") Is Nothing Then
            Session("WarningListTrans") = "Selected Transfer data can't be found!"
            showMessage(Session("WarningListTrans"), 2)
            Exit Sub
        End If
        If UpdateCheckedTrans() Then
            Dim dtTbl As DataTable = Session("TblTrans")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListTrans.SelectedIndex = -1 : txtFilterListTrans.Text = ""
                Session("TblTransView") = dtView.ToTable
                gvListTrans.DataSource = Session("TblTransView")
                gvListTrans.DataBind()
                dtView.RowFilter = ""
                mpeListTrans.Show()
            Else
                dtView.RowFilter = ""
                Session("TblTransView") = Nothing
                gvListTrans.DataSource = Session("TblTransView")
                gvListTrans.DataBind()
                Session("WarningListTrans") = "Selected Transfer data can't be found!"
                showMessage(Session("WarningListTrans"), 2)
            End If
        Else
            mpeListTrans.Show()
        End If
    End Sub

    Protected Sub gvListTrans_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListTrans.PageIndexChanging
        If UpdateCheckedTrans2() Then
            gvListTrans.PageIndex = e.NewPageIndex
            gvListTrans.DataSource = Session("TblTransView")
            gvListTrans.DataBind()
        End If
        mpeListTrans.Show()
    End Sub 'OK

    Protected Sub lkbAddToListListTrans_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListTrans.Click
        If Not Session("TblTrans") Is Nothing Then
            If UpdateCheckedTrans() Then
                Dim dtTbl As DataTable = Session("TblTrans")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If transno.Text <> "" Then
                            If DDLTransNo.SelectedValue = "Trans No." Then
                                If dtView(C1)("transno") <> "" Then
                                    transno.Text &= ";" + vbCrLf + dtView(C1)("transno")
                                End If
                            Else
                                transno.Text &= ";" + vbCrLf + dtView(C1)("transmstoid").ToString
                            End If
                        Else
                            If DDLTransNo.SelectedValue = "Trans No." Then
                                If dtView(C1)("transno") <> "" Then
                                    transno.Text &= dtView(C1)("transno")
                                End If
                            Else
                                transno.Text &= dtView(C1)("transmstoid").ToString
                            End If
                        End If
                    Next
                    dtView.RowFilter = ""
                    DDLBusUnit.Enabled = False
                    cProc.SetModalPopUpExtender(btnHiddenListTrans, PanelListTrans, mpeListTrans, False)
                Else
                    Session("WarningListTrans") = "Please select Transfer to add to list!"
                    showMessage(Session("WarningListTrans"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListTrans") = "Please show some Transfer data first!"
            showMessage(Session("WarningListTrans"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListTrans_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListTrans.Click
        cProc.SetModalPopUpExtender(btnHiddenListTrans, PanelListTrans, mpeListTrans, False)
    End Sub

    Protected Sub cbType_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbType.CheckedChanged
        If cbType.Checked Then
            imbFindMat.Visible = True
            imbEraseMat.Visible = True
        Else
            imbFindMat.Visible = False
            imbEraseMat.Visible = False
        End If
    End Sub 'OK

    Protected Sub FilterDDLCat01_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat01.SelectedIndexChanged
        InitFilterDDLCat2()
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat02_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat02.SelectedIndexChanged
        InitFilterDDLCat3()
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat03_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat03.SelectedIndexChanged
        InitFilterDDLCat4()
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat04_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat04.SelectedIndexChanged
        mpeListMat.Show()
    End Sub

    Protected Sub imbFindMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindMat.Click
        If IsValidPeriod() Then
            FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
            InitFilterDDLCat1()
            cbCat01.Checked = False : cbCat02.Checked = False
            cbCat03.Checked = False : cbCat04.Checked = False
            Session("TblMat") = Nothing
            Session("TblMatView") = Nothing
            gvListMat.DataSource = Nothing : gvListMat.DataBind()
            tbData.Text = "5" : gvListMat.PageSize = CInt(tbData.Text)
            'BindListMat()
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            Exit Sub
        End If
    End Sub 'OK

    Protected Sub imbEraseMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseMat.Click
        matcode.Text = ""
        cbType.Checked = False
        cbType_CheckedChanged(Nothing, Nothing)
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        If cbCat01.Checked Then
            If FilterDDLCat01.SelectedValue <> "" Then
                sPlus &= " AND SUBSTRING(matcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'"
            Else
                cbCat01.Checked = False
            End If
        End If
        If cbCat02.Checked Then
            If FilterDDLCat02.SelectedValue <> "" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND SUBSTRING(matcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'") & " AND SUBSTRING(matcode, 4, 3)='" & FilterDDLCat02.SelectedValue & "'"
            Else
                cbCat02.Checked = False
            End If
        End If
        If cbCat03.Checked Then
            If FilterDDLCat03.SelectedValue <> "" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND SUBSTRING(matcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'") & IIf(cbCat02.Checked, "", " AND SUBSTRING(matcode, 4, 3)='" & FilterDDLCat02.SelectedValue & "'") & " AND SUBSTRING(matcode, 8, 4)='" & FilterDDLCat03.SelectedValue & "'"
            Else
                cbCat03.Checked = False
            End If
        End If
        If cbCat04.Checked Then
            If FilterDDLCat04.SelectedValue <> "" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND SUBSTRING(matcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'") & IIf(cbCat02.Checked, "", " AND SUBSTRING(matcode, 4, 3)='" & FilterDDLCat02.SelectedValue & "'") & IIf(cbCat03.Checked, "", " AND SUBSTRING(matcode, 8, 4)='" & FilterDDLCat03.SelectedValue & "'") & " AND SUBSTRING(matcode, 13, 4)='" & FilterDDLCat04.SelectedValue & "'"
            Else
                cbCat04.Checked = False
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub 'OK

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        BindListMat()
        'InitFilterDDLCat1()
        cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
        If Session("TblMat") Is Nothing Then
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub 'OK

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If matcode.Text <> "" Then
                            matcode.Text &= ";" + vbCrLf + dtView(C1)("matcode")
                        Else
                            matcode.Text = dtView(C1)("matcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some Material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matoid=" & dtTbl.Rows(C1)("matoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matoid=" & dtTbl.Rows(C1)("matoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                InitFilterDDLCat1()
                cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Selected material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub lbShowData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbShowData.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Please find material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If tbData.Text = "" Then
            Session("WarningListMat") = "Please fill total data to be shown!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        Else
            If ToDouble(tbData.Text) <= 0 Then
                Session("WarningListMat") = "Total data to be shown must be more than 0!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat2() Then
            gvListMat.PageSize = Int(tbData.Text)
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub 'OK

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub 'OK

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub 'OK

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub 'OK

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmTWConsReport.aspx?awal=true")
    End Sub 'OK

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub 'OK

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub 'OK

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub 'OK

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        If Not report Is Nothing Then
            If report.IsLoaded Then
                report.Dispose()
                report.Close()
            End If
        End If
    End Sub 'OK
#End Region

    Protected Sub btnAddCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If DDLCust.SelectedValue <> "" Then
            If lbCust.Items.Count > 0 Then
                If Not lbCust.Items.Contains(lbCust.Items.FindByValue(DDLCust.SelectedValue)) Then
                    Dim objList As New ListItem
                    objList.Text = DDLCust.SelectedItem.Text : objList.Value = DDLCust.SelectedValue
                    lbCust.Items.Add(objList)
                End If
            Else
                Dim objList As New ListItem
                objList.Text = DDLCust.SelectedItem.Text : objList.Value = DDLCust.SelectedValue
                lbCust.Items.Add(objList)
            End If
        End If
    End Sub

    Protected Sub btnMinCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If lbCust.Items.Count > 0 Then
            Dim objList As ListItem = lbCust.SelectedItem
            lbCust.Items.Remove(objList)
        End If
    End Sub
End Class
