<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="frmGeneralLedger.aspx.vb" Inherits="ReportForm_frmGeneralLedger" title="" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="Table2" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" style="text-align: left" valign="center">
                <asp:Label ID="Label7" runat="server" Font-Bold="False" Text=".: General Ledger Report" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server">
                        <ContentTemplate>
    
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 10px" align=left></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><TABLE><TBODY><TR><TD align=left>Bussiness Unit</TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="FilterDDLDiv" runat="server" CssClass="inpText" Width="155px" AutoPostBack="True">
            </asp:DropDownList></TD><TD align=left></TD></TR><TR><TD align=left></TD><TD align=left></TD><TD align=left><asp:DropDownList id="FilterDDLGroup" runat="server" CssClass="inpText" Width="155px" Visible="False"></asp:DropDownList></TD><TD align=left></TD></TR><TR><TD align=left>Period</TD><TD align=left>:</TD><TD align=left><asp:TextBox id="date1" runat="server" CssClass="inpText" Width="75px"></asp:TextBox> <asp:ImageButton id="search1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label1" runat="server" Text="to"></asp:Label> <asp:TextBox id="date2" runat="server" CssClass="inpText" Width="75px"></asp:TextBox> <asp:ImageButton id="search2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label2" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD><TD align=left></TD></TR><TR><TD align=left>Report Type</TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="DDLType" runat="server" CssClass="inpText" AutoPostBack="True"><asp:ListItem Value="0">Summary</asp:ListItem>
<asp:ListItem Value="1">Detail</asp:ListItem>
<asp:ListItem Value="2">General Journal</asp:ListItem>
</asp:DropDownList> <asp:CheckBox id="cbShowMemo" runat="server" Text="Show Memorial Journal Only" Visible="False"></asp:CheckBox></TD><TD align=left></TD></TR><TR><TD align=left>Currency Value</TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="FilterCurrency" runat="server" CssClass="inpText">
            </asp:DropDownList></TD><TD align=left></TD></TR><TR><TD align=left><asp:Label id="lbAcc" runat="server" Text="Account"></asp:Label></TD><TD align=left>:</TD><TD align=left><TABLE><TBODY><TR><TD align=left><asp:RadioButtonList id="rblAccount" runat="server" AutoPostBack="True" RepeatDirection="Horizontal"><asp:ListItem Selected="True">All</asp:ListItem>
<asp:ListItem Value="Beberapa">Select</asp:ListItem>
</asp:RadioButtonList></TD><TD align=left><asp:Panel id="pnlFilterAcc" runat="server" DefaultButton="btnSearch"><asp:TextBox id="acctgdesc" runat="server" CssClass="inpText" Width="150px" Visible="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Width="16px" Visible="False" Height="16px"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></asp:Panel> </TD></TR></TBODY></TABLE></TD><TD align=left><asp:Label id="acctgoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD align=left></TD><TD align=left></TD><TD align=left colSpan=2><asp:GridView id="gvAccounting" runat="server" Visible="False" GridLines="None" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" BackColor="White">
<PagerSettings Mode="NumericFirstLast"></PagerSettings>

<RowStyle CssClass="gvrow" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer "></EmptyDataRowStyle>
<Columns>
<asp:TemplateField HeaderText="Acctgoid" Visible="False"><ItemTemplate>
<asp:Label id="lblacctgoid" runat="server" Text='<%# Eval("id") %>'></asp:Label> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Wrap="True" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbAccount" runat="server" Checked='<%# Eval("selected") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Wrap="True" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="code" HeaderText="Code" SortExpression="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="name" HeaderText="Name" SortExpression="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" ForeColor="Black"></FooterStyle>

<PagerStyle HorizontalAlign="Left" CssClass="gvfooter" Font-Size="Small" ForeColor="Black"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblstatusdatasupp" runat="server" Text="No data found." CssClass="Important"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow" Font-Bold="True"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="Navy"></HeaderStyle>

<AlternatingRowStyle CssClass="gvrowalternate "></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left><asp:Label id="lblNoRef" runat="server" Text="No. Ref." Visible="False"></asp:Label></TD><TD align=left><asp:Label id="lblSept" runat="server" Text=":" Visible="False"></asp:Label></TD><TD align=left colSpan=2><asp:TextBox id="tbNoRef" runat="server" CssClass="inpText" Width="150px" Visible="False"></asp:TextBox></TD></TR></TBODY></TABLE></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><TABLE><TBODY><TR><TD align=left><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" PopupButtonID="search1" Format="MM/dd/yyyy" TargetControlID="Date1"></ajaxToolkit:CalendarExtender></TD><TD align=left><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" PopupButtonID="search2" Format="MM/dd/yyyy" TargetControlID="Date2"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD align=left><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" TargetControlID="date1" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD><TD align=left><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" TargetControlID="date2" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE></TD></TR><TR><TD style="TEXT-ALIGN: center" align=center><asp:ImageButton id="imbView" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbPDF" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbXLS" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top; HEIGHT: 50px; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress2" runat="server">
        <ProgressTemplate>
            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/loadingbar.gif" />
            Please wait ..
        </ProgressTemplate>
    </asp:UpdateProgress> </TD></TR><TR><TD align=center><TABLE><TBODY><TR><TD align=left><CR:CrystalReportViewer id="crv" runat="server" Width="350px" HasPrintButton="False" HasExportButton="False" AutoDataBind="True" DisplayGroupTree="False" HasCrystalLogo="False" HasDrillUpButton="False" HasToggleGroupTreeButton="False" HasViewList="False" Height="50px"></CR:CrystalReportViewer> </TD></TR></TBODY></TABLE></TD></TR><TR><TD align=left><asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False" Width="700px"><TABLE width="100%"><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD style="vertical-align: top; width: 40px; text-align: center"><asp:Image id="imIcon" runat="server" Height="24px" Width="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label" align="left"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>
    <asp:ImageButton ID="imbOKPopUp" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/ok.png" /></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" DropShadow="True" PopupDragHandleControlID="lblCaption" PopupControlID="pnlPopUpMsg" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel> </TD></TR><TR><TD align=left></TD></TR></TBODY></TABLE>
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="imbPDF"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="imbXLS"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
</asp:Content>

