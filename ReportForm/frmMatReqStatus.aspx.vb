Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_MatReqStatus
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function FillDDLWithALL(ByRef oDDLObject As DropDownList, ByVal sSql As String) As Boolean
        ' To fill data in dropdown list, return true when all data exist
        FillDDLWithALL = True
        oDDLObject.Items.Clear()
        oDDLObject.Items.Add("ALL")
        oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = "ALL"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            oDDLObject.Items.Add(xreader.GetValue(1))
            oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()
        ' Cek if all data exits
        If oDDLObject.Items.Count = 0 Then
            FillDDLWithALL = False
        End If
        Return FillDDLWithALL
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matrawoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matrawoid=" & cbOid
                                dtView2.RowFilter = "matrawoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitAllDDL()
        ' DDL Division
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If FillDDL(DDLBusUnit, sSql) Then
            InitDDLDept()
        End If
    End Sub

    Private Sub DDLTypeMat()
        sSql = "SELECT gencode,gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='GROUPITEM'"
        FillDDL(DDLMatType, sSql)
    End Sub

    Private Sub InitDDLDept()
        ' DDL Department
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE activeflag='ACTIVE' AND cmpcode='" & DDLBusUnit.SelectedValue & "' ORDER BY deptname"
        FillDDL(DDLDept, sSql)
    End Sub

    Private Sub BindListRequest()
        sSql = "SELECT matreqmstoid, CONVERT(VARCHAR(20), matreqmstoid) AS draftno, matreqno, CONVERT(VARCHAR(10), matreqdate, 101) AS matreqdate, matreqmstnote, 'False' AS checkvalue FROM QL_trnmatreqmst reqm WHERE reqm.cmpcode='" & DDLBusUnit.SelectedValue & "' "


        If FilterDDLStatus.SelectedValue = "In Process" Then
            sSql &= " AND reqm.matreqmststatus IN ('In Process')"
        ElseIf FilterDDLStatus.SelectedValue = "Post" Then
            sSql &= " AND reqm.matreqmststatus IN ('Post','Closed')"
        ElseIf FilterDDLStatus.SelectedValue = "Cancel" Then
            sSql &= " AND reqm.matreqmststatus IN ('Cancel')"
        End If

        If lbDept.Items.Count > 0 Then
            Dim sOid As String = ""
            For C1 As Integer = 0 To lbDept.Items.Count - 1
                sOid &= lbDept.Items(C1).Value & ","
            Next
            If sOid <> "" Then
                sOid = Left(sOid, sOid.Length - 1)
                sSql &= " AND reqm.deptoid IN (" & sOid & ")"
            End If
        End If

        If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
            If IsValidPeriod() Then
                sSql &= " AND " & FilterDDLDate.SelectedValue & ">='" & FilterPeriod1.Text & " 00:00:00' AND " & FilterDDLDate.SelectedValue & "<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        sSql &= " ORDER BY matreqmstoid"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnmatreqmst")
        If dt.Rows.Count > 0 Then
            Session("TblListRequest") = dt
            Session("TblListRequestView") = Session("TblListRequest")
            gvListRequest.DataSource = Session("TblListRequestView")
            gvListRequest.DataBind()
            cProc.SetModalPopUpExtender(btnHideListRequest, pnlListRequest, mpeListRequest, True)
        Else
            showMessage("No Material Request Non KIK data available for this time.", 2)
        End If
    End Sub

    Private Sub UpdateCheckedListRequest()
        If Session("TblListRequest") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListRequest")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListRequest.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListRequest.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matreqmstoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListRequest") = dt
        End If
        If Session("TblListRequestView") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListRequestView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListRequest.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListRequest.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matreqmstoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListRequestView") = dt
        End If
    End Sub

    Private Sub BindListMat()
   
        sSql = "SELECT DISTINCT m.itemoid AS matoid, m.itemCode AS matcode, m.itemLongDescription AS matlongdesc, gendesc AS matunit, 'False' AS checkvalue FROM QL_trnmatreqdtl reqd INNER JOIN QL_trnmatreqmst reqm ON reqm.cmpcode=reqd.cmpcode AND reqm.matreqmstoid=reqd.matreqmstoid INNER JOIN QL_mstitem m ON m.itemoid=matreqrefoid INNER JOIN QL_mstgen g ON genoid=matrequnitoid WHERE reqd.cmpcode='PRKP' AND matreqreftype LIKE '%RAW%' "

        If FilterDDLStatus.SelectedValue = "In Process" Then
            sSql &= " AND reqm.matreqmststatus IN ('In Process')"
        ElseIf FilterDDLStatus.SelectedValue = "Post" Then
            sSql &= " AND reqm.matreqmststatus IN ('Post','Closed')"
        ElseIf FilterDDLStatus.SelectedValue = "Cancel" Then
            sSql &= " AND reqm.matreqmststatus IN ('Cancel')"
        End If

        If lbDept.Items.Count > 0 Then
            Dim sOid As String = ""
            For C1 As Integer = 0 To lbDept.Items.Count - 1
                sOid &= lbDept.Items(C1).Value & ","
            Next
            If sOid <> "" Then
                sOid = Left(sOid, sOid.Length - 1)
                sSql &= " AND reqm.deptoid IN (" & sOid & ")"
            End If
        End If

        If FilterRequest.Text <> "" Then
            If DDLRequest.SelectedValue = "reqm.matreqno" Then
                Dim sReqno() As String = Split(FilterRequest.Text, ";")
                sSql &= " AND ("
                For c1 As Integer = 0 To sReqno.Length - 1
                    sSql &= DDLRequest.SelectedValue & " LIKE '%" & Tchar(sReqno(c1)) & "%'"
                    If c1 < sReqno.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            Else
                Dim sReqno() As String = Split(FilterRequest.Text, ";")
                sSql &= " AND ("
                For c1 As Integer = 0 To sReqno.Length - 1
                    sSql &= DDLRequest.SelectedValue & " = " & ToDouble(sReqno(c1))
                    If c1 < sReqno.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            End If
        End If

        If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
            If IsValidPeriod() Then
                sSql &= " AND " & FilterDDLDate.SelectedValue & ">='" & FilterPeriod1.Text & " 00:00:00' AND " & FilterDDLDate.SelectedValue & "<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        sSql &= " ORDER BY matcode"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnmatreqdtl")
        If dt.Rows.Count > 0 Then
            Session("TblListMat") = dt
            Session("TblListMatView") = Session("TblListMat")
            gvListMat.DataSource = Session("TblListMatView")
            gvListMat.DataBind()
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            showMessage("No Material data available for this time.", 2)
        End If
    End Sub

    Private Sub UpdateCheckedListMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMat") = dt
        End If
        If Session("TblListMatView") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMatView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMatView") = dt
        End If
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        Dim sWhere As String = ""
        Dim sRptName As String = ""
        Dim Dtl As String = ""
        Dim Join As String = ""
        Dim Balance As String = ""
        Try
            If DDLType.SelectedValue = "SUMMARY" Then
                sRptName = "MatRequestNonKIKSummaryStatus"
                If sType = "Print Excel" Then
					report.Load(Server.MapPath(folderReport & "rptMatReqNonKIKStatusSum.rpt"))
                Else
					report.Load(Server.MapPath(folderReport & "rptMatReqNonKIKStatusSum.rpt"))
                End If
            Else
                sRptName = "MatRequestNonKIKDetailStatus"
                Dim sExt As String = "", sGroup As String = ""
               
                If DDLGroupBy.SelectedIndex = 0 Then
                    sGroup = "PerNo"
                Else
                    sGroup = "PerCode"
                End If
                If sType = "Print Excel" Then
					report.Load(Server.MapPath(folderReport & "rptMatReqNonKIKStatusDtl.rpt"))
                Else
					report.Load(Server.MapPath(folderReport & "rptMatReqNonKIKStatusDtl" & sGroup & sExt & ".rpt"))
                End If

            End If

            If DDLType.SelectedValue = "DETAIL" Then
				Dtl = " ,[Usage Date], ISNULL([Usage No.],'')[Usage No.] ,ISNULL([Usg Qty],0.0)[Usg Qty] ,ISNULL([Usage Status],'') AS [Usage Status] , ISNULL([TotalRetQty],0) [TotalRetQty], (ISNULL([Usg Qty],0.0)- ISNULL([TotalRetQty],0)) [BalanceUsageQty] , 0.0 AS [Balance]"
				Join = "  LEFT JOIN (SELECT usgd.cmpcode, (CASE WHEN matusagemststatus IN ('Post','Closed')  THEN usgm.matusageno ELSE ('Draft No. ' + CONVERT(VARCHAR(10), usgm.matusagemstoid)) END) [Usage No.], usgd.matusagedtloid, usgd.matreqdtloid,(SELECT ISNULL(usd.matusageqty,0.0) [Usg Qty] FROM QL_trnmatusagedtl usd INNER JOIN QL_trnmatusagemst usm ON usd.cmpcode=usm.cmpcode AND usm.matusagemstoid=usd.matusagemstoid WHERE usd.matusagedtloid=usgd.matusagedtloid AND usgm.matusagemststatus IN ('Post','Closed')) AS [Usg Qty], ISNULL(usgm.matusagemststatus,'') AS [Usage Status],usgm.matusagedate [Usage Date] FROM QL_trnmatusagedtl usgd INNER JOIN QL_trnmatusagemst usgm ON usgd.cmpcode=usgm.cmpcode AND usgm.matusagemstoid=usgd.matusagemstoid WHERE usgm.matusagemststatus<>'Cancel') AS Tbl_Usage ON reqm.cmpcode=Tbl_Usage.cmpcode AND Tbl_Usage.matreqdtloid=reqd.matreqdtloid "
                Join &= " LEFT JOIN (SELECT cmpcode,matusagedtloid, SUM([TotalRetQty]) [TotalRetQty] FROM (SELECT retd.cmpcode, retd.matusagedtloid, ISNULL(retd.matretqty,0.0) [TotalRetQty]  FROM QL_trnmatretdtl retd INNER JOIN QL_trnmatretmst retm ON retd.cmpcode=retm.cmpcode AND retm.matretmstoid=retd.matretmstoid WHERE retm.matretmststatus='Post') AS TblTemp GROUP BY cmpcode,matusagedtloid) AS Tbl_Ret ON Tbl_Usage.cmpcode=Tbl_Ret.cmpcode AND Tbl_Usage.matusagedtloid=Tbl_Ret.matusagedtloid  "
            Else
                Dtl = "  , ISNULL([TotalUsageQty],0)[TotalUsageQty], ISNULL([TotalRetQty],0) [TotalRetQty], (ISNULL([TotalUsageQty],0) -  ISNULL([TotalRetQty],0)) [BalanceUsageQty], (reqd.matreqqty-(ISNULL([TotalUsageQty],0) -  ISNULL([TotalRetQty],0))) [Balance] "
                Join &= "  LEFT JOIN (SELECT cmpcode,matreqdtloid, SUM([Usg Qty]) [TotalUsageQty] FROM (SELECT  usgd.cmpcode,usgd.matreqdtloid,(SELECT ISNULL(usd.matusageqty,0.0) [Usg Qty] FROM QL_trnmatusagedtl usd INNER JOIN QL_trnmatusagemst usm ON usd.cmpcode=usm.cmpcode AND usm.matusagemstoid=usd.matusagemstoid WHERE usd.matusagedtloid=usgd.matusagedtloid AND usgm.matusagemststatus IN ('Post','Closed')) AS [Usg Qty] FROM QL_trnmatusagedtl usgd INNER JOIN QL_trnmatusagemst usgm ON usgd.cmpcode=usgm.cmpcode AND usgm.matusagemstoid=usgd.matusagemstoid WHERE usgm.matusagemststatus<>'Cancel')AS TblTemp GROUP BY cmpcode,matreqdtloid) AS Tbl_Usage ON reqm.cmpcode=Tbl_Usage.cmpcode AND Tbl_Usage.matreqdtloid=reqd.matreqdtloid"
                Join &= " LEFT JOIN (SELECT cmpcode, matreqdtloid, SUM([TotalRetQty]) [TotalRetQty] FROM (SELECT retd.cmpcode, usgd.matreqdtloid, ISNULL(retd.matretqty,0.0) [TotalRetQty] FROM QL_trnmatusagedtl usgd INNER JOIN QL_trnmatusagemst usgm ON usgd.cmpcode=usgm.cmpcode AND usgm.matusagemstoid=usgd.matusagemstoid  INNER JOIN QL_trnmatretdtl retd ON usgd.matusagedtloid=retd.matusagedtloid INNER JOIN QL_trnmatretmst retm ON retd.cmpcode=retm.cmpcode AND retm.matretmstoid=retd.matretmstoid AND retm.matretmststatus='Post' WHERE usgm.matusagemststatus<>'Cancel' ) AS TblTemp GROUP BY cmpcode,matreqdtloid) AS Tbl_Ret ON Tbl_Usage.cmpcode=Tbl_Ret.cmpcode AND Tbl_Usage.matreqdtloid=Tbl_Ret.matreqdtloid"
            End If
            sSql = "SELECT  * FROM (SELECT reqm.cmpcode, (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=reqm.cmpcode) [Business Unit], reqm.matreqmstoid [Oid], 'Draft No. ' + (CONVERT(VARCHAR(20), reqm.matreqmstoid)) [Draft No.], matreqno [Request No.], matreqdate [Request Date], reqm.deptoid [Dept ID], (SELECT deptname FROM QL_mstdept de WHERE de.deptoid=reqm.deptoid) [Department], matreqwhoid [WH ID], g1.gendesc [Warehouse], matreqmststatus [Status], matreqmstnote [Header Note], UPPER(reqm.createuser) [Create User], (CASE matreqmststatus WHEN 'In Process' THEN '' ELSE reqm.upduser END) [Posting User], (CASE matreqmststatus WHEN 'In Process' THEN NULL ELSE reqm.updtime END) [Posting Date], matreqreftype [Type], (SELECT m.itemcode FROM QL_mstitem m WHERE m.itemoid=reqd.matreqrefoid AND m.cmpcode=reqd.cmpcode) AS [Code] , (SELECT m.itemLongDescription FROM QL_mstitem m WHERE m.itemoid=reqd.matreqrefoid AND m.cmpcode=reqd.cmpcode) AS  [Material], reqd.matreqdtloid[Dtloid], matreqqty [Qty], g2.gendesc [Unit], matreqdtlnote [Note] " & Dtl & "  FROM QL_trnmatreqmst reqm INNER JOIN QL_mstgen g1 ON g1.genoid=matreqwhoid INNER JOIN QL_trnmatreqdtl reqd ON reqd.cmpcode=reqm.cmpcode AND reqd.matreqmstoid=reqm.matreqmstoid INNER JOIN QL_mstgen g2 ON g2.genoid=matrequnitoid " & Join & " ) AS tbl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' "

            If FilterDDLStatus.SelectedValue = "In Process" Then
                sSql &= " AND [Status] IN ('In Process')"
            ElseIf FilterDDLStatus.SelectedValue = "Post" Then
                sSql &= " AND [Status] IN ('Post','Closed')"
            ElseIf FilterDDLStatus.SelectedValue = "Cancel" Then
                sSql &= " AND [Status] IN ('Cancel')"
            End If

            If lbDept.Items.Count > 0 Then
                Dim sOid As String = ""
                For C1 As Integer = 0 To lbDept.Items.Count - 1
                    sOid &= lbDept.Items(C1).Value & ","
                Next
                If sOid <> "" Then
                    sOid = Left(sOid, sOid.Length - 1)
                    sSql &= " AND [Dept ID] IN (" & sOid & ")"
                End If
            End If

            If FilterRequest.Text <> "" Then
                If DDLRequest.SelectedValue = "reqm.matreqno" Then
                    Dim sReqno() As String = Split(FilterRequest.Text, ";")
                    sSql &= " AND ("
                    For c1 As Integer = 0 To sReqno.Length - 1
                        sSql &= " [Request No.] LIKE '%" & Tchar(sReqno(c1)) & "%'"
                        If c1 < sReqno.Length - 1 Then
                            sSql &= " OR "
                        End If
                    Next
                    sSql &= ")"
                Else
                    Dim sReqno() As String = Split(FilterRequest.Text, ";")
                    sSql &= " AND ("
                    For c1 As Integer = 0 To sReqno.Length - 1
                        sSql &= " [Oid] = " & ToDouble(sReqno(c1))
                        If c1 < sReqno.Length - 1 Then
                            sSql &= " OR "
                        End If
                    Next
                    sSql &= ")"
                End If
            End If

            If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
                Dim FDate As String
                If FilterDDLDate.SelectedValue = "reqm.matreqdate" Then
                    FDate = " [Request Date]"
                Else
                    FDate = " [Posting Date]"
                End If

                If IsValidPeriod() Then
                    sSql &= " AND " & FDate & ">='" & FilterPeriod1.Text & " 00:00:00' AND " & FDate & "<='" & FilterPeriod2.Text & " 23:59:59'"
                Else
                    Exit Sub
                End If
            End If

            If FilterMaterial.Text <> "" Then
                Dim sWhereCode As String = ""
                Dim sNo() As String = Split(FilterMaterial.Text, ";")
                If sNo.Length > 0 Then
                    For C1 As Integer = 0 To sNo.Length - 1
                        If sNo(C1) <> "" Then
                            sWhereCode &= "[Code] LIKE '" & Tchar(sNo(C1)) & "' OR "
                        End If
                    Next
                    If sWhereCode <> "" Then
                        sWhereCode = Left(sWhereCode, sWhereCode.Length - 4)
                        sSql &= " AND (" & sWhereCode & ")"
                    End If
                End If
            End If

            If DDLType.SelectedValue = "DETAIL" Then
                sSql &= " ORDER BY cmpcode, " & DDLGroupBy.SelectedValue
            Else
                sSql &= " ORDER BY [Request No.], [Oid]"
            End If

            Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnmatreqmst")
            For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                If DDLType.SelectedValue = "DETAIL" Then
                    dtTbl.Rows(C1)("Balance") = ((ToDouble(dtTbl.Rows(C1)("Qty").ToString)) - (ToDouble(dtTbl.Compute("SUM(BalanceUsageQty)", "Dtloid=" & dtTbl.Rows(C1)("Dtloid").ToString).ToString)))
                End If
            Next
            ''For Status Req Closed (In Process Usg)
            For C2 As Integer = 0 To dtTbl.Rows.Count - 1
                If dtTbl.Rows(C2)("Status") = "Closed" And (ToDouble(dtTbl.Rows(C2)("Balance").ToString)) > 0 Then
                    dtTbl.Rows(C2)("Status") = "Post"
                End If
            Next
            '
            Dim dvTbl As DataView = dtTbl.DefaultView
            If RDUsg.Checked = True Then
                dvTbl.RowFilter = "Balance " & FilterDDLReqQty.SelectedValue & " " & ToDouble(ReqQtyTxt.Text)
            Else
                dvTbl.RowFilter = "Qty " & FilterDDLReqQty.SelectedValue & " " & ToDouble(ReqQtyTxt.Text)
            End If
            report.SetDataSource(dvTbl.ToTable)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            'report.PrintOptions.PaperSize = PaperSize.PaperFolio
            cProc.SetDBLogonForReport(report)

            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, sRptName)
                report.Close()
                report.Dispose()
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, sRptName)
                report.Close()
                report.Dispose()
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub

#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If

        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmMatReqStatus.aspx")

        End If

        If checkPagePermission("~\ReportForm\frmMatReqStatus.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        Page.Title = CompnyName & " - Material Request Non KIK Status"
        If Not Page.IsPostBack Then
            InitAllDDL()
            DDLTypeMat()
            FilterDDLReqQty.SelectedValue = ">"
            DDLType_SelectedIndexChanged(Nothing, Nothing)
            FilterDDLStatus_SelectedIndexChanged(Nothing, Nothing)
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListRequest") Is Nothing And Session("WarningListRequest") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListRequest") Then
                Session("WarningListRequest") = Nothing
                mpeListRequest.Show()
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                mpeListMat.Show()
            End If
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLDept()
    End Sub

    Protected Sub DDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLType.SelectedIndexChanged
        Dim bVal As Boolean = True
        If DDLType.SelectedValue = "SUMMARY" Then
            bVal = False
        End If
        DDLGroupBy.SelectedIndex = -1
        'lblMat.Visible = bVal : septMat.Visible = bVal : DDLMatType.Visible = bVal : FilterMaterial.Visible = bVal : btnSearchMat.Visible = bVal : btnClearMat.Visible = bVal
        lblGroupBy.Visible = bVal : septGroupBy.Visible = bVal : DDLGroupBy.Visible = bVal
        lblSortBy.Visible = bVal : septSortBy.Visible = bVal : DDLSortBy.Visible = bVal : DDLOrder.Visible = bVal
        'RDReqQty.Visible = bVal : RDUsg.Visible = bVal : septQty.Visible = bVal
        'FilterDDLReqQty.Visible = bVal : ReqQtyTxt.Visible = bVal
        DDLGroupBy_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub FilterDDLStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLStatus.SelectedIndexChanged
        If FilterDDLStatus.SelectedValue = "Post" Then
            'Fill DDL Date
            FilterDDLDate.Items.Clear()
            FilterDDLDate.Items.Add("Request Date")
            FilterDDLDate.Items.Item(FilterDDLDate.Items.Count - 1).Value = "reqm.matreqdate"
            FilterDDLDate.Items.Add("Posting Date")
            FilterDDLDate.Items.Item(FilterDDLDate.Items.Count - 1).Value = "reqm.updtime"
        Else
            'Fill DDL Date
            FilterDDLDate.Items.Clear()
            FilterDDLDate.Items.Add("Request Date")
            FilterDDLDate.Items.Item(FilterDDLDate.Items.Count - 1).Value = "reqm.matreqdate"
        End If
    End Sub

    Protected Sub btnSearchRequest_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchRequest.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2) : Exit Sub
        End If
        FilterDDLListRequest.SelectedIndex = -1 : FilterTextListRequest.Text = "" : Session("TblListRequest") = Nothing : Session("TblListRequestView") = Nothing : gvListRequest.DataSource = Nothing : gvListRequest.DataBind()
        BindListRequest()
    End Sub

    Protected Sub btnClearRequest_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearRequest.Click
        FilterRequest.Text = ""
    End Sub

    Protected Sub btnFindListRequest_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListRequest.Click
        UpdateCheckedListRequest()
        Dim dt As DataTable = Session("TblListRequest")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = FilterDDLListRequest.SelectedValue & " LIKE '%" & Tchar(FilterTextListRequest.Text) & "%'"
        If dv.Count > 0 Then
            Session("TblListRequestView") = dv.ToTable
            gvListRequest.DataSource = Session("TblListRequestView")
            gvListRequest.DataBind()
            dv.RowFilter = ""
            mpeListRequest.Show()
        Else
            dv.RowFilter = ""
            Session("WarningListRequest") = "Material Request Non KIK data can't be found!"
            showMessage(Session("WarningListRequest"), 2)
        End If
    End Sub

    Protected Sub btnAllListRequest_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListRequest.Click
        UpdateCheckedListRequest()
        FilterDDLListRequest.SelectedIndex = -1 : FilterTextListRequest.Text = ""
        Session("TblListRequestView") = Session("TblListRequest")
        gvListRequest.DataSource = Session("TblListRequestView")
        gvListRequest.DataBind()
        mpeListRequest.Show()
    End Sub

    Protected Sub gvListRequest_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListRequest.PageIndexChanging
        UpdateCheckedListRequest()
        gvListRequest.PageIndex = e.NewPageIndex
        gvListRequest.DataSource = Session("TblListRequestView")
        gvListRequest.DataBind()
        mpeListRequest.Show()
    End Sub

    Protected Sub cbHdrLMRequest_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For C1 As Integer = 0 To gvListRequest.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListRequest.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        mpeListRequest.Show()
    End Sub

    Protected Sub lbAddToListRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListRequest.Click
        UpdateCheckedListRequest()
        Dim dt As DataTable = Session("TblListRequest")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = "checkvalue='True'"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                If DDLRequest.SelectedIndex = 0 Then
                    If dv(C1)("matreqno").ToString <> "" Then
                        FilterRequest.Text &= dv(C1)("matreqno").ToString & ";"
                    End If
                Else
                    FilterRequest.Text &= dv(C1)("matreqmstoid").ToString & ";"
                End If
            Next
            If FilterRequest.Text <> "" Then
                FilterRequest.Text = Left(FilterRequest.Text, FilterRequest.Text.Length - 1)
            End If
            dv.RowFilter = ""
            cProc.SetModalPopUpExtender(btnHideListRequest, pnlListRequest, mpeListRequest, False)
        Else
            dv.RowFilter = ""
            Session("WarningListRequest") = "Please select some Material Request Non KIK data first!"
            showMessage(Session("WarningListRequest"), 2)
        End If
    End Sub

    Protected Sub lbSelectAllListRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSelectAllListRequest.Click
        UpdateCheckedListRequest()
        Dim dt As DataTable = Session("TblListRequestView")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            If DDLRequest.SelectedIndex = 0 Then
                If dt.Rows(C1)("matreqno").ToString <> "" Then
                    FilterRequest.Text &= dt.Rows(C1)("matreqno").ToString & ";"
                End If
            Else
                FilterRequest.Text &= dt.Rows(C1)("matreqmstoid").ToString & ";"
            End If
        Next
        If FilterRequest.Text <> "" Then
            FilterRequest.Text = Left(FilterRequest.Text, FilterRequest.Text.Length - 1)
        End If
        cProc.SetModalPopUpExtender(btnHideListRequest, pnlListRequest, mpeListRequest, False)
    End Sub

    Protected Sub lbCloseListRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListRequest.Click
        cProc.SetModalPopUpExtender(btnHideListRequest, pnlListRequest, mpeListRequest, False)
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2) : Exit Sub
        End If
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : Session("TblListMat") = Nothing : Session("TblListMatView") = Nothing : gvListMat.DataSource = Session("TblListMat") : gvListMat.DataBind()
        lblListMat.Text = "List Of " & DDLMatType.SelectedItem.Text
        BindListMat()
    End Sub

    Protected Sub btnClearMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearMat.Click
        FilterMaterial.Text = ""
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        UpdateCheckedListMat()
        Dim dt As DataTable = Session("TblListMat")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        If dv.Count > 0 Then
            Session("TblListMatView") = dv.ToTable
            gvListMat.DataSource = Session("TblListMatView")
            gvListMat.DataBind()
            dv.RowFilter = ""
            mpeListMat.Show()
        Else
            dv.RowFilter = ""
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        UpdateCheckedListMat()
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        Session("TblListMatView") = Session("TblListMat")
        gvListMat.DataSource = Session("TblListMatView")
        gvListMat.DataBind()
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        UpdateCheckedListMat()
        gvListMat.PageIndex = e.NewPageIndex
        gvListMat.DataSource = Session("TblListMatView")
        gvListMat.DataBind()
        mpeListMat.Show()
    End Sub

    Protected Sub cbHdrLM_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For C1 As Integer = 0 To gvListMat.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        mpeListMat.Show()
    End Sub

    Protected Sub lbAddToListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListMat.Click
        UpdateCheckedListMat()
        Dim dt As DataTable = Session("TblListMat")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = "checkvalue='True'"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                FilterMaterial.Text &= DDLMatType.SelectedValue & "-" & dv(C1)("matcode").ToString & ";"
            Next
            If FilterMaterial.Text <> "" Then
                FilterMaterial.Text = Left(FilterMaterial.Text, FilterMaterial.Text.Length - 1)
            End If
            dv.RowFilter = ""
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
        Else
            dv.RowFilter = ""
            Session("WarningListMat") = "Please select some Material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub lbSelectAllListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSelectAllListMat.Click
        UpdateCheckedListMat()
        Dim dt As DataTable = Session("TblListMat")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            FilterMaterial.Text &= DDLMatType.SelectedValue & "-" & dt.Rows(C1)("matcode").ToString & ";"
        Next
        If FilterMaterial.Text <> "" Then
            FilterMaterial.Text = Left(FilterMaterial.Text, FilterMaterial.Text.Length - 1)
        End If
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub btnAddDept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddDept.Click
        If DDLDept.SelectedValue <> "" Then
            If lbDept.Items.Count > 0 Then
                If Not lbDept.Items.Contains(lbDept.Items.FindByValue(DDLDept.SelectedValue)) Then
                    Dim objList As New ListItem
                    objList.Text = DDLDept.SelectedItem.Text : objList.Value = DDLDept.SelectedValue
                    lbDept.Items.Add(objList)
                End If
            Else
                Dim objList As New ListItem
                objList.Text = DDLDept.SelectedItem.Text : objList.Value = DDLDept.SelectedValue
                lbDept.Items.Add(objList)
            End If
        End If
    End Sub

    Protected Sub btnMinDept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMinDept.Click
        If lbDept.Items.Count > 0 Then
            Dim objList As ListItem = lbDept.SelectedItem
            lbDept.Items.Remove(objList)
        End If
    End Sub


    Protected Sub DDLGroupBy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLGroupBy.SelectedIndexChanged
        For C1 As Integer = 0 To DDLSortBy.Items.Count - 1
            DDLSortBy.Items(C1).Enabled = True
        Next
        DDLSortBy.Items(DDLGroupBy.SelectedIndex).Enabled = False
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmMatReqStatus.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        If Not report Is Nothing Then
            If report.IsLoaded Then
                report.Dispose()
                report.Close()
            End If
        End If
    End Sub
#End Region

End Class
