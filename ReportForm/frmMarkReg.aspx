<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="frmMarkReg.aspx.vb" Inherits="ReportForm_MarkReg" title="" EnableEventValidation="false" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Marketing Register Report" CssClass="Title" ForeColor="SaddleBrown"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <table style="width: 100%">
                                <tr>
                                    <td align="center" valign="top" char="Label">
                                        <table>
                                            <tr>
                                                <td align="center" class="Label" valign="top">
                                                    <asp:UpdatePanel ID="upReportForm" runat="server">
                                                        <ContentTemplate>
<asp:Panel id="pnlReportForm" runat="server" Width="500px" DefaultButton="btnViewReport"><TABLE><TBODY><TR id="BusinessUnitA" runat="server" visible="true"><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text="Divisi" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpTextDisabled" Width="192px" AutoPostBack="True" Enabled="False" Visible="False"></asp:DropDownList></TD></TR><TR><TD id="TD1" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label8" runat="server" Text="Type"></asp:Label> <asp:Label id="Label10" runat="server" Text="Type" Visible="False" __designer:wfdid="w1"></asp:Label></TD><TD id="TD3" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD2" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="FilterDDLType" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True"><asp:ListItem>Detail</asp:ListItem>
<asp:ListItem Value="Summary">Summary</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left runat="server" Visible="true"><asp:CheckBox id="CBtype" runat="server" Text="Type" __designer:wfdid="w1"></asp:CheckBox></TD><TD class="Label" align=center runat="server" Visible="true">:</TD><TD class="Label" align=left runat="server" Visible="true"><asp:DropDownList id="TypeMat" runat="server" CssClass="inpText" Width="128px" AutoPostBack="True" __designer:wfdid="w2" OnSelectedIndexChanged="TypeMat_SelectedIndexChanged"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left runat="server" Visible="true"><asp:Label id="Label12" runat="server" Text="Currency" __designer:wfdid="w1"></asp:Label></TD><TD class="Label" align=center runat="server" Visible="true">:</TD><TD class="Label" align=left runat="server" Visible="true"><asp:DropDownList id="DDLcurr" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" __designer:wfdid="w4" OnSelectedIndexChanged="TypeMat_SelectedIndexChanged"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label2" runat="server" Text="Status"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="188px" AutoPostBack="True" __designer:wfdid="w1"><asp:ListItem>All</asp:ListItem>
<asp:ListItem Value="Approved">Post</asp:ListItem>
<asp:ListItem Value="In Process">In Process</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label11" runat="server" Text="Period" __designer:wfdid="w11"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="month1" runat="server" CssClass="inpText" Width="48px" __designer:wfdid="w12"><asp:ListItem Value="01">Jan</asp:ListItem>
<asp:ListItem Value="02">Feb</asp:ListItem>
<asp:ListItem Value="03">Mar</asp:ListItem>
<asp:ListItem Value="04">Apr</asp:ListItem>
<asp:ListItem Value="05">May</asp:ListItem>
<asp:ListItem Value="06">Jun</asp:ListItem>
<asp:ListItem Value="07">Jul</asp:ListItem>
<asp:ListItem Value="08">Aug</asp:ListItem>
<asp:ListItem Value="09">Sep</asp:ListItem>
<asp:ListItem Value="10">Oct</asp:ListItem>
<asp:ListItem Value="11">Nov</asp:ListItem>
<asp:ListItem Value="12">Dec</asp:ListItem>
</asp:DropDownList> <asp:DropDownList id="year1" runat="server" CssClass="inpText" Width="53px" __designer:wfdid="w13"><asp:ListItem>2014</asp:ListItem>
<asp:ListItem>2015</asp:ListItem>
<asp:ListItem>2016</asp:ListItem>
<asp:ListItem>2017</asp:ListItem>
<asp:ListItem>2018</asp:ListItem>
<asp:ListItem>2019</asp:ListItem>
<asp:ListItem>2020</asp:ListItem>
</asp:DropDownList>&nbsp;- <asp:DropDownList id="month2" runat="server" CssClass="inpText" Width="48px" __designer:wfdid="w14"><asp:ListItem Value="01">Jan</asp:ListItem>
<asp:ListItem Value="02">Feb</asp:ListItem>
<asp:ListItem Value="03">Mar</asp:ListItem>
<asp:ListItem Value="04">Apr</asp:ListItem>
<asp:ListItem Value="05">May</asp:ListItem>
<asp:ListItem Value="06">Jun</asp:ListItem>
<asp:ListItem Value="07">Jul</asp:ListItem>
<asp:ListItem Value="08">Aug</asp:ListItem>
<asp:ListItem Value="09">Sep</asp:ListItem>
<asp:ListItem Value="10">Oct</asp:ListItem>
<asp:ListItem Value="11">Nov</asp:ListItem>
<asp:ListItem Value="12">Dec</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:DropDownList id="year2" runat="server" CssClass="inpText" Width="53px" __designer:wfdid="w15"><asp:ListItem>2014</asp:ListItem>
<asp:ListItem>2015</asp:ListItem>
<asp:ListItem>2016</asp:ListItem>
<asp:ListItem>2017</asp:ListItem>
<asp:ListItem>2018</asp:ListItem>
<asp:ListItem>2019</asp:ListItem>
<asp:ListItem>2020</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="TD28" class="Label" align=left rowSpan=2 runat="server" Visible="false"><asp:DropDownList id="FilterDDLDate" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="dom.crttime">Create Date</asp:ListItem>
<asp:ListItem Value="dom.updtime">Post Date</asp:ListItem>
</asp:DropDownList></TD><TD id="TD29" class="Label" align=center rowSpan=2 runat="server" Visible="false">:</TD></TR><TR><TD id="TD27" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="60px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="CalPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="lblTo" runat="server" Text="to"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="60px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="CalPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" TargetControlID="FilterPeriod1" PopupButtonID="CalPeriod1" Format="MM/dd/yyyy">
                                                                            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="FilterPeriod1" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999">
                                                                            </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" TargetControlID="FilterPeriod2" PopupButtonID="CalPeriod2" Format="MM/dd/yyyy">
                                                                            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" TargetControlID="FilterPeriod2" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999">
                                                                            </ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD class="Label" align=left><asp:Label id="Label5" runat="server" Text="Customer" __designer:wfdid="w2"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custcode" runat="server" CssClass="inpText" Width="158px" __designer:wfdid="w3" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchCust" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w4"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearCust" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w5"></asp:ImageButton></TD></TR><TR><TD id="TD18" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label9" runat="server" Text="Driver" __designer:wfdid="w2"></asp:Label></TD><TD id="TD16" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD25" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="drivcode" runat="server" CssClass="inpText" Width="158px" __designer:wfdid="w6" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchDriv" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w7"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearDriv" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w8"></asp:ImageButton></TD></TR><TR><TD id="TD17" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label6" runat="server" Text="Vehicle" __designer:wfdid="w2"></asp:Label></TD><TD id="TD20" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD13" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="vhccode" runat="server" CssClass="inpText" Width="158px" __designer:wfdid="w2" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchVhc" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w3"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearVhc" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w4"></asp:ImageButton></TD></TR><TR><TD id="TD23" class="Label" align=left runat="server" Visible="false"><asp:Label id="lblDO" runat="server" Text="DO No." __designer:wfdid="w2"></asp:Label></TD><TD id="TD14" class="Label" align=center runat="server" Visible="false"><asp:Label id="lbl3" runat="server" Text=":" __designer:wfdid="w2"></asp:Label></TD><TD id="TD22" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="dono" runat="server" CssClass="inpText" Width="158px" __designer:wfdid="w23" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindDO" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w24"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseDO" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w25"></asp:ImageButton></TD></TR><TR><TD id="TD24" class="Label" align=left rowSpan=2 runat="server" Visible="false"><asp:Label id="lblWh" runat="server" Text="Warehouse" __designer:wfdid="w136"></asp:Label></TD><TD id="TD15" class="Label" align=center rowSpan=2 runat="server" Visible="false"><asp:Label id="lblWh2" runat="server" Text=":" __designer:wfdid="w2"></asp:Label></TD><TD id="TD21" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="DDLWarehouse" runat="server" CssClass="inpText" Width="275px" __designer:wfdid="w139"></asp:DropDownList><asp:ImageButton id="btnAddWH" runat="server" ImageUrl="~/Images/plus.png" ImageAlign="AbsMiddle" __designer:wfdid="w140"></asp:ImageButton></TD></TR><TR><TD id="TD19" class="Label" align=left runat="server" Visible="false"><asp:ListBox id="lbWarehouse" runat="server" CssClass="inpTextDisabled" Width="275px" __designer:wfdid="w141" Rows="3"></asp:ListBox><asp:ImageButton id="btnMinWH" runat="server" ImageUrl="~/Images/minus.png" ImageAlign="AbsMiddle" __designer:wfdid="w142"></asp:ImageButton></TD></TR><TR><TD id="TD11" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="DDLShipmentNo" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True"><asp:ListItem>Shipment No.</asp:ListItem>
<asp:ListItem Value="Draft No.">Draft No.</asp:ListItem>
</asp:DropDownList></TD><TD id="TD10" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD12" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="shipmentno" runat="server" CssClass="inpText" Width="158px" TextMode="MultiLine"></asp:TextBox> <asp:ImageButton id="imbFindShipment" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbEraseShipment" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblMat" runat="server" Text="Material"></asp:Label></TD><TD class="Label" align=center><asp:Label id="lbl2" runat="server" Text=":" __designer:wfdid="w2"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="itemcode" runat="server" CssClass="inpText" Width="178px" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindMat" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseMat" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD id="TD4" class="Label" align=left runat="server" Visible="false"><asp:Label id="lblgroup" runat="server" Text="Grouping"></asp:Label></TD><TD id="TD9" class="Label" align=center runat="server" Visible="false"><asp:Label id="lbl" runat="server" Text=":" __designer:wfdid="w2"></asp:Label></TD><TD id="TD5" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="DDLGrouping" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True"><asp:ListItem Value="c.custname">Customer</asp:ListItem>
<asp:ListItem Value="shm.shipmentitemno">Shipment No.</asp:ListItem>
<asp:ListItem Value="i.itemcode">Material Code</asp:ListItem>
<asp:ListItem Value="dom.doitemno">DO No.</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="TD7" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label7" runat="server" Text="Sorting"></asp:Label></TD><TD id="TD8" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD6" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="DDLSorting" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w4"><asp:ListItem Value="shm.shipmentitemno">Shipment No.</asp:ListItem>
<asp:ListItem Value="shm.updtime">Post Date</asp:ListItem>
</asp:DropDownList>&nbsp;- <asp:DropDownList id="DDLOrderby" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"><asp:ListItem Value="ASC">ASC</asp:ListItem>
<asp:ListItem Value="DESC">DESC</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnExportToPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnExportToExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upReportForm">
                                                                                <ProgressTemplate>
                                                                                    <span></span><span style="font-weight: bold; font-size: 10pt; color: purple">
                                                                                        <asp:Image ID="imgLoading" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />
                                                                                        Please Wait .....</span><br />
                                                                                </ProgressTemplate>
                                                                            </asp:UpdateProgress> </TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnViewReport" />
                                                            <asp:PostBackTrigger ControlID="btnExportToPdf" />
                                                            <asp:PostBackTrigger ControlID="btnExportToExcel" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Label" style="height: 10px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="Label" valign="top">
                                                    <CR:CrystalReportViewer ID="crvReportForm" runat="server" AutoDataBind="True" HasCrystalLogo="False"
                                                        HasDrillUpButton="False" HasExportButton="False" HasPrintButton="False" HasToggleGroupTreeButton="False"
                                                        HasViewList="False" />
                                                </td>
                                            </tr>
                                        </table>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <HeaderTemplate>
                            &nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel id="upListCust" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListCust" runat="server" CssClass="modalBox" Width="650px" Visible="False"><TABLE width="100%"><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListCust" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Customer"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListCust" runat="server" Width="100%" DefaultButton="btnFindListCust">Filter : <asp:DropDownList id="DDLFilterListCust" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="Custcode">Code</asp:ListItem>
<asp:ListItem Value="custname">Name</asp:ListItem>
<asp:ListItem Value="custaddr">Address</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListCust" runat="server" CssClass="inpText" Width="150px"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListCust" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton><asp:ImageButton id="btnViewAllListCust" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3><asp:ImageButton id="btnSelectAllCust" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton><asp:ImageButton id="btnSelectNoneCust" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton><asp:ImageButton id="btnViewCheckedCust" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3>&nbsp;<asp:GridView id="gvListCust" runat="server" BorderColor="Black" ForeColor="#333333" Width="98%" DataKeyNames="custoid,custcode,custname" CellPadding="4" BorderStyle="None" AutoGenerateColumns="False" AllowPaging="True" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:CheckBox id="cbLMCust" runat="server" ToolTip='<%# eval("custoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="custcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListCust" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbCloseListCust" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListCust" runat="server" TargetControlID="btnHideListCust" PopupDragHandleControlID="lblListCust" PopupControlID="pnlListCust" BackgroundCssClass="modalBackground" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListCust" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    &nbsp;&nbsp;
    <asp:UpdatePanel id="UpListDriv" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListDriv" runat="server" CssClass="modalBox" Width="650px" __designer:wfdid="w40" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListDriv" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Driver" __designer:wfdid="w11"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListDriv" runat="server" Width="100%" DefaultButton="btnFindListDriv" __designer:wfdid="w42">Filter : <asp:DropDownList id="DDLFilterListDriv" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w43"><asp:ListItem Value="drivcode">Code</asp:ListItem>
<asp:ListItem Value="drivname">Name</asp:ListItem>
<asp:ListItem Value="drivaddr">Address</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListDriv" runat="server" CssClass="inpText" Width="150px" __designer:wfdid="w44"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListDriv" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w45"></asp:ImageButton><asp:ImageButton id="btnViewAllListDriv" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w46"></asp:ImageButton></asp:Panel></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3><asp:ImageButton id="btnSelectAllDriv" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" __designer:wfdid="w47"></asp:ImageButton><asp:ImageButton id="btnSelectNoneDriv" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" __designer:wfdid="w48"></asp:ImageButton><asp:ImageButton id="btnViewCheckedDriv" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" __designer:wfdid="w49"></asp:ImageButton></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3>&nbsp;<asp:GridView id="gvListDriv" runat="server" BorderColor="Black" ForeColor="#333333" Width="98%" __designer:wfdid="w20" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" BorderStyle="None" CellPadding="4" DataKeyNames="drivoid,drivcode,drivname">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:CheckBox id="cbLMDriv" runat="server" ToolTip='<%# eval("drivoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="drivcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="drivname" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="drivaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListDriv" runat="server" __designer:wfdid="w51">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbCloseListDriv" runat="server" __designer:wfdid="w52">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListDriv" runat="server" __designer:wfdid="w53" TargetControlID="btnHideListDriv" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlListDriv" PopupDragHandleControlID="lblListDriv"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListDriv" runat="server" __designer:wfdid="w54" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <br />
    <asp:UpdatePanel id="UpListVhc" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListVhc" runat="server" CssClass="modalBox" Width="650px" __designer:wfdid="w116" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListVhc" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Vehicle" __designer:wfdid="w117"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListVhc" runat="server" Width="100%" DefaultButton="btnFindListVhc" __designer:wfdid="w118">Filter : <asp:DropDownList id="DDLFilterListVhc" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w119"><asp:ListItem Value="vhccode">Code</asp:ListItem>
<asp:ListItem Value="vhcno">No</asp:ListItem>
<asp:ListItem Value="vhcdesc">Description</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListVhc" runat="server" CssClass="inpText" Width="150px" __designer:wfdid="w120"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListVhc" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w121"></asp:ImageButton><asp:ImageButton id="btnViewAllListVhc" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w122"></asp:ImageButton></asp:Panel></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3><asp:ImageButton id="btnSelectAllVhc" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" __designer:wfdid="w123"></asp:ImageButton><asp:ImageButton id="btnSelectNoneVhc" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" __designer:wfdid="w124"></asp:ImageButton><asp:ImageButton id="btnViewCheckedVhc" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" __designer:wfdid="w125"></asp:ImageButton></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3>&nbsp;<asp:GridView id="gvListVhc" runat="server" BorderColor="Black" ForeColor="#333333" Width="98%" __designer:wfdid="w126" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" BorderStyle="None" CellPadding="4" DataKeyNames="vhcoid,vhccode,vhcno,vhcdesc">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:CheckBox id="cbLMVhc" runat="server" ToolTip='<%# eval("vhcoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="vhccode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="vhcno" HeaderText="No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="vhcdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD style="HEIGHT: 18px" align=center colSpan=3><asp:LinkButton id="lkbAddToListListVhc" runat="server" __designer:wfdid="w127">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbCloseListVhc" runat="server" __designer:wfdid="w128">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListVhc" runat="server" __designer:wfdid="w129" TargetControlID="btnHideListVhc" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlListVhc" PopupDragHandleControlID="lblListVhc"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListVhc" runat="server" __designer:wfdid="w130" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upListDO" runat="server">
        <ContentTemplate>
<asp:Panel id="PanelListDO" runat="server" CssClass="modalBox" Width="600px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListDO" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Delivery Order"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListDO" runat="server" Width="100%" DefaultButton="btnFindListDO"><asp:Label id="Label24" runat="server" Text="Filter :"></asp:Label> <asp:DropDownList id="DDLFilterListDO" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="doitemno">DO No</asp:ListItem>
<asp:ListItem Value="doitemmstnote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListDO" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListDO" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewAllListDO" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> </asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAllDO" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneDO" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedDO" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3>&nbsp;<asp:GridView id="gvListDO" runat="server" BorderColor="Black" ForeColor="#333333" Width="98%" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" BorderStyle="None" CellPadding="4" DataKeyNames="doitemmstoid,doitemno,dodate,doitemmststatus,doitemmstnote">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:CheckBox id="cbLMDO" runat="server" ToolTip='<%# eval("doitemmstoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="doitemmstoid" HeaderText="Draft No">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="doitemno" HeaderText="DO No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dodate" HeaderText="DO Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="doitemcustpono" HeaderText="Cust PO No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="doitemmstnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="doitemmststatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
&nbsp;
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListDO" runat="server">[ Add To List ]</asp:LinkButton>&nbsp;- <asp:LinkButton id="lkbListDO" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListDO" runat="server" TargetControlID="btnHiddenListDO" BackgroundCssClass="modalBackground" PopupControlID="PanelListDO" PopupDragHandleControlID="lblListDO">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListDO" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upListShipment" runat="server">
        <contenttemplate>
<asp:Panel id="PanelListShipment" runat="server" CssClass="modalBox" Width="600px" __designer:wfdid="w67" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListShipment" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Shipment" __designer:wfdid="w68"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListShipment" runat="server" Width="100%" DefaultButton="btnFindListShipment" __designer:wfdid="w69"><asp:Label id="Label241" runat="server" Text="Filter :" __designer:wfdid="w70"></asp:Label> <asp:DropDownList id="DDLFilterListShipment" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w71"><asp:ListItem Value="shipmentitemno">Shipment No.</asp:ListItem>
<asp:ListItem Value="shipmentitemmstnote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListShipment" runat="server" CssClass="inpText" Width="150px" __designer:wfdid="w72"></asp:TextBox> <asp:ImageButton id="btnFindListShipment" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w73"></asp:ImageButton> <asp:ImageButton id="btnViewAllListShipment" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w74"></asp:ImageButton> </asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAllShipment" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" __designer:wfdid="w75"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneShipment" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" __designer:wfdid="w76"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedShipment" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" __designer:wfdid="w77"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3>&nbsp;<asp:GridView id="gvListShipment" runat="server" BorderColor="Black" ForeColor="#333333" Width="98%" __designer:wfdid="w46" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" BorderStyle="None" CellPadding="4" DataKeyNames="shipmentitemmstoid,shipmentitemno,shipmentdate,shipmentitemmststatus,shipmentitemmstnote">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:CheckBox id="cbLMShipment" runat="server" ToolTip='<%# eval("shipmentitemmstoid") %>' __designer:wfdid="w4" Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="shipmentitemmstoid" HeaderText="Draft No">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="shipmentitemno" HeaderText="Shipment No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="shipmentdate" HeaderText="Shipment Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="shipmentitemmstnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="shipmentitemmststatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListShipment" runat="server" __designer:wfdid="w79">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbListShipment" runat="server" __designer:wfdid="w80">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListShipment" runat="server" __designer:wfdid="w81" TargetControlID="btnHiddenListShipment" BackgroundCssClass="modalBackground" PopupControlID="PanelListShipment" PopupDragHandleControlID="lblListShipment">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListShipment" runat="server" __designer:wfdid="w82" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    &nbsp;&nbsp;
    <asp:UpdatePanel id="upListMat" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListMat" runat="server" CssClass="modalBox" Width="900px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Finish Good" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMat" runat="server" Width="100%" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="itemlongdescription">Description</asp:ListItem>
<asp:ListItem Value="itemcode">Code</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" CssClass="inpText" Width="200px"></asp:TextBox> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD id="TD26" class="Label" align=center colSpan=3 runat="server" Visible="false"><asp:CheckBox id="cbCat01" runat="server" Text="Cat 1 :"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat01" runat="server" CssClass="inpText" Width="150px" AutoPostBack="True"></asp:DropDownList> <asp:CheckBox id="cbCat02" runat="server" Text="Cat 2 :"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat02" runat="server" CssClass="inpText" Width="150px" AutoPostBack="True"></asp:DropDownList> <asp:CheckBox id="cbCat03" runat="server" Text="Cat 3 :"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat03" runat="server" CssClass="inpText" Width="150px" AutoPostBack="True"></asp:DropDownList> <asp:CheckBox id="cbCat04" runat="server" Text="Cat 4 :"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat04" runat="server" CssClass="inpText" Width="150px" AutoPostBack="True"></asp:DropDownList></TD></TR></TBODY></TABLE></asp:Panel></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNone" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewChecked" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:UpdateProgress id="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat"><ProgressTemplate>
<TABLE style="WIDTH: 200px"><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple">Load Data, Please Wait ...</TD></TR><TR><TD><asp:Image id="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif"></asp:Image></TD></TR></TABLE>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD class="Label" align=left colSpan=3>Show <asp:TextBox id="tbData" runat="server" CssClass="inpText" Width="25px"></asp:TextBox> Data Per Page <asp:LinkButton id="lbShowData" runat="server">>> View</asp:LinkButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat" runat="server" BorderColor="Black" ForeColor="#333333" Width="96%" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" BorderStyle="None" CellPadding="4" DataKeyNames="itemcode">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:CheckBox id="cbLM" runat="server" ToolTip='<%# eval("itemoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdescription" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
&nbsp;
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListMat" runat="server">[ Add To List ]</asp:LinkButton>&nbsp;- <asp:LinkButton id="lkbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE><ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlListMat" PopupDragHandleControlID="lblTitleListMat"></ajaxToolkit:ModalPopupExtender></asp:Panel> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

