<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="frmTrialBalance.aspx.vb" Inherits="ReportForm_frmTrialBalance" title="" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="Table4" align="center" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th class="header" colspan="3" style="text-align: center">
                <asp:Label ID="Label7" runat="server" Font-Bold="False" Text=".: TRIAL BALANCE REPORT :." CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="">
                        <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" align=left></TD></TR><TR><TD style="TEXT-ALIGN: center" align=center>
    <table>
        <tr>
            <td align="right" class="Label">
                <asp:Label ID="Label4" runat="server" Text="Bussines Unit :"></asp:Label></td>
            <td align="left" class="Label">
                <asp:DropDownList ID="FilterDDLDiv" runat="server" CssClass="inpText"
                    Width="155px">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="right" class="Label">
                <asp:Label ID="Label2" runat="server" Text="Date :"></asp:Label></td>
            <td align="left" class="Label">
                <asp:TextBox ID="txtDate1" runat="server" CssClass="inpText" Width="75px"></asp:TextBox>
                <asp:ImageButton ID="imbDate1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/oCalendar.gif" />
                <asp:Label ID="Label1" runat="server" Text="to"></asp:Label>
                <asp:TextBox ID="txtDate2" runat="server" CssClass="inpText" Width="75px"></asp:TextBox>
                <asp:ImageButton ID="imbDate2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/oCalendar.gif" />
                <asp:Label ID="Label5" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></td>
        </tr>
        <tr>
            <td align="right" class="Label">
                <asp:Label ID="Label3" runat="server" Text="Currency Value :"></asp:Label></td>
            <td align="left" class="Label">
                <asp:DropDownList ID="FilterCurrency" runat="server" CssClass="inpText">
                    <asp:ListItem Value="IDR">INDONESIAN RUPIAH</asp:ListItem>
                    <asp:ListItem Value="USD">US DOLLAR</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left" class="Label">
                <ajaxToolkit:CalendarExtender ID="ceDate1" runat="server" Format="MM/dd/yyyy" PopupButtonID="imbDate1"
                    TargetControlID="txtDate1">
                </ajaxToolkit:CalendarExtender>
            </td>
            <td align="left" class="Label">
                <ajaxToolkit:MaskedEditExtender ID="meeDate1" runat="server" Mask="99/99/9999" MaskType="Date"
                    TargetControlID="txtDate1">
                </ajaxToolkit:MaskedEditExtender>
            </td>
        </tr>
        <tr>
            <td align="left" class="Label">
                <ajaxToolkit:CalendarExtender ID="ceDate2" runat="server" Format="MM/dd/yyyy" PopupButtonID="imbDate2"
                    TargetControlID="txtDate2">
                </ajaxToolkit:CalendarExtender>
            </td>
            <td align="left" class="Label">
                <ajaxToolkit:MaskedEditExtender ID="meeDate2" runat="server" Mask="99/99/9999" MaskType="Date"
                    TargetControlID="txtDate2">
                </ajaxToolkit:MaskedEditExtender>
            </td>
        </tr>
    </table>
</TD></TR><TR><TD style="FONT-SIZE: x-small; TEXT-ALIGN: center" align=left><asp:Label id="lblWarning" runat="server" CssClass="Important"></asp:Label></TD></TR>
    <tr>
        <td align="center" style="text-align: center">
            <asp:ImageButton ID="imbView" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewreport.png" />
            <asp:ImageButton ID="imbPDF" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/topdf.png" />
            <asp:ImageButton ID="imbXLS" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/toexcel.png" />
            <asp:ImageButton ID="imbClear" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/clear.png" /></td>
    </tr>
    <TR><TD style="TEXT-ALIGN: center; height: 100px;" align=center valign="top"><asp:UpdateProgress id="UpdateProgress1" runat="server"><ProgressTemplate>
<asp:Image id="Image1" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle"></asp:Image>&nbsp;Please wait .. 
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left></TD></TR></TBODY></TABLE><TABLE style="WIDTH: 100%"><TBODY><TR><TD align=left><CR:CrystalReportViewer id="crvTB" runat="server" Height="50px" Width="350px" DisplayGroupTree="False" AutoDataBind="true" HasCrystalLogo="False" HasDrillUpButton="False" HasExportButton="False" HasPrintButton="False" HasToggleGroupTreeButton="False" HasViewList="False"></CR:CrystalReportViewer></TD></TR></TBODY></TABLE>
</ContentTemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="imbPDF"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="imbXLS"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="crvTB"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
</asp:Content>


