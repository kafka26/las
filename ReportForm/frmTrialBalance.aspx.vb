Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports ClassFunction

Partial Class ReportForm_frmTrialBalance
    Inherits System.Web.UI.Page

#Region "Variables"
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_CONN")
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public folderReport As String = "~/report/"
    Public cKon As New Koneksi
    Dim conn As New SqlConnection(ConnStr)
    Dim oReport As New ReportDocument
    Dim sSql As String = ""
    Dim CProc As New ClassProcedure
#End Region

#Region "Functions"
    Private Function ValidateInput() As String ' Return empty string if Success
        Dim sReturn As String = ""
        Dim sErrTemp As String = ""

        If FilterDDLDiv.Items.Count < 1 Then sReturn &= "- No Bussiness Unit availabale."
        If Not IsValidDate(txtDate1.Text, "MM/dd/yyyy", sErrTemp) Then sReturn &= "- Invalid Start Date format: " & sErrTemp & ".<BR>"
        If Not IsValidDate(txtDate2.Text, "MM/dd/yyyy", sErrTemp) Then sReturn &= "- Invalid End Date format: " & sErrTemp & ".<BR>"
        If IsValidDate(txtDate1.Text, "MM/dd/yyyy", sErrTemp) And IsValidDate(txtDate2.Text, "MM/dd/yyyy", sErrTemp) Then
            If DateDiff(DateInterval.Day, CDate(txtDate1.Text), CDate(txtDate2.Text)) < 0 Then
                sReturn &= "- End Date must be greater than Start Date.<BR>"
            End If
        End If

        Return sReturn
    End Function

    Private Function ProcessReport(ByVal sType As String) As String
        Dim sMsg As String = ""
        crvTB.ReportSource = Nothing
        Try
            If sType = "PDF" Then
                oReport.Load(Server.MapPath("~/report/crTrialBalance.rpt"))
            Else
                oReport.Load(Server.MapPath("~/report/crTrialBalanceXls.rpt"))
            End If

            CProc.SetDBLogonForReport(oReport)

            oReport.PrintOptions.PaperSize = PaperSize.PaperA4
            oReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait

            oReport.SetParameterValue("cmpcode", FilterDDLDiv.SelectedValue)
            oReport.SetParameterValue("startdate", CDate(txtDate1.Text))
            oReport.SetParameterValue("enddate", CDate(txtDate2.Text))
            oReport.SetParameterValue("speriodacctg", GetDateToPeriodAcctg(CDate(txtDate1.Text)))
            oReport.SetParameterValue("speriod", Format(CDate(txtDate1.Text), "dd MMMM yyyy") & " - " & Format(CDate(txtDate2.Text), "dd MMMM yyyy"))
            oReport.SetParameterValue("currency", FilterCurrency.SelectedValue)
        Catch ex As Exception
            sMsg = ex.ToString
        End Try
        Return sMsg
    End Function
#End Region

#Region "Procedures"
    Private Sub InitDDL()
        ' DDL Division
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE' "
        FillDDL(FilterDDLDiv, sSql)
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then Response.Redirect("~\Other\login.aspx")
        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            ' Response.Redirect("~\other\NotAuthorize.aspx")
        End If
        Session.Timeout = 60

        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("frmTrialBalance.aspx")
        End If

        Page.Title = CompnyName & " - Trial Balance Report"
        If Not IsPostBack Then
            InitDDL()
            txtDate1.Text = Format(GetServerTime(), "MM/01/yyyy")
            txtDate2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        oReport.Close() : oReport.Dispose()
    End Sub

    Protected Sub crvTB_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvTB.Navigate
        imbView_Click(Nothing, Nothing)
    End Sub

    Protected Sub imbView_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbView.Click
        crvTB.ReportSource = Nothing
        lblWarning.Text = ValidateInput()
        If lblWarning.Text <> "" Then Exit Sub
        If ProcessReport("PDF") = "" Then
            crvTB.DisplayGroupTree = False
            crvTB.ReportSource = oReport
        Else
            lblWarning.Text = ProcessReport("PDF")
        End If
    End Sub

    Protected Sub imbPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbPDF.Click
        crvTB.ReportSource = Nothing
        lblWarning.Text = ValidateInput()
        If lblWarning.Text <> "" Then Exit Sub
        If ProcessReport("PDF") = "" Then
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            Try
                oReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "TRIAL_BALANCE_" & FilterCurrency.SelectedValue & "_" & _
                    Format(CDate(txtDate1.Text), "yyMMdd") & "_" & Format(CDate(txtDate2.Text), "yyMMdd"))
            Catch ex As Exception
                oReport.Close() : oReport.Dispose()
            End Try
        Else
            lblWarning.Text = ProcessReport("PDF")
        End If
    End Sub

    Protected Sub imbXLS_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbXLS.Click
        crvTB.ReportSource = Nothing
        lblWarning.Text = ValidateInput()
        If lblWarning.Text <> "" Then Exit Sub
        If ProcessReport("XLS") = "" Then
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            Try
                oReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "TRIAL_BALANCE_" & FilterCurrency.SelectedValue & "_" & _
                    Format(CDate(txtDate1.Text), "yyMMdd") & "_" & Format(CDate(txtDate2.Text), "yyMMdd"))
            Catch ex As Exception
                oReport.Close() : oReport.Dispose()
            End Try
        Else
            lblWarning.Text = ProcessReport("XLS")
        End If
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClear.Click
        Response.Redirect("frmTrialBalance.aspx?awal=true")
    End Sub
#End Region

End Class

