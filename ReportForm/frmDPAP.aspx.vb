' Last Update By 4n7JuK On 130923
Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_frmDPAP
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function ValidateParam() As String
        Dim sReturn As String = ""
        Dim sErrTemp As String = ""

        If Not IsValidDate(txtStart.Text, "MM/dd/yyyy", sErrTemp) Then sReturn &= "- Invalid Start Date format: " & sErrTemp & ".<BR>"
        If Not IsValidDate(txtFinish.Text, "MM/dd/yyyy", sErrTemp) Then sReturn &= "- Invalid End Date format: " & sErrTemp & ".<BR>"
        If IsValidDate(txtStart.Text, "MM/dd/yyyy", sErrTemp) And IsValidDate(txtFinish.Text, "MM/dd/yyyy", sErrTemp) Then
            If DateDiff(DateInterval.Day, CDate(txtStart.Text), CDate(txtFinish.Text)) < 0 Then
                sReturn &= "- End Date must be greater than Start Date.<BR>"
            End If
        End If

        Return sReturn
    End Function

    Private Function GetIndoMonth(ByVal sVal As String) As String
        Select Case sVal
            Case "01"
                Return "JANUARI"
            Case "02"
                Return "FEBRUARI"
            Case "03"
                Return "MARET"
            Case "04"
                Return "APRIL"
            Case "05"
                Return "MEI"
            Case "06"
                Return "JUNI"
            Case "07"
                Return "JULI"
            Case "08"
                Return "AGUSTUS"
            Case "09"
                Return "SEPTEMBER"
            Case "10"
                Return "OKTOBER"
            Case "11"
                Return "NOVEMBER"
            Case "12"
                Return "DESEMBER"
            Case Else
                Return "Unknown"
        End Select
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindSupplierData()
        sSql = "SELECT 0 selected,suppoid,suppcode,suppname,suppaddr FROM QL_mstsupp WHERE activeflag='ACTIVE' AND (suppcode LIKE '%" & Tchar(FilterTextSupplier.Text) & "%' OR suppname LIKE '%" & Tchar(FilterTextSupplier.Text) & "%') ORDER BY suppname"
        Dim dtSupp As DataTable = cKon.ambiltabel(sSql, "QL_mstsupp")
        gvSupplier.DataSource = dtSupp : gvSupplier.DataBind()
        Session("QL_mstsupp") = dtSupp
        gvSupplier.Visible = True
        gvSupplier.SelectedIndex = -1
    End Sub

    Private Sub UpdateCheckedGV()
        If Not Session("QL_mstsupp") Is Nothing Then
            Dim dtab As DataTable = Session("QL_mstsupp")

            If dtab.Rows.Count > 0 Then
                Dim cb As System.Web.UI.WebControls.CheckBox
                Dim dView As DataView = dtab.DefaultView
                Dim drView As DataRowView

                For i As Integer = 0 To gvSupplier.Rows.Count - 1
                    cb = gvSupplier.Rows(i).FindControl("chkSelect")
                    dView.RowFilter = "suppoid = " & cb.ToolTip
                    drView = dView.Item(0)
                    drView.BeginEdit()
                    If cb.Checked = True Then
                        drView("selected") = 1
                    Else
                        drView("selected") = 0
                    End If
                    drView.EndEdit()
                    dView.RowFilter = ""
                Next
                dtab.AcceptChanges()
            End If
            Session("QL_mstsupp") = dtab
        End If
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        Try
            Dim sSuppOid As String = "", sSqlPlus_1 As String = "*", sSqlPlus_2 As String = " ORDER BY d.suppname,d.dpapoid,d.transdate, d.transtype", sFilterDate_1 As String = " a.transdate BETWEEN @start AND @finish", sFilterDate_2 As String = " b.transdate<@start"
            If rbSupplier.SelectedValue = "SELECT" Then
                Dim dtSupp As DataTable : dtSupp = Session("QL_mstsupp")
                Dim dvSupp As DataView = dtSupp.DefaultView
                dvSupp.RowFilter = "selected='1'"

                For R1 As Integer = 0 To dvSupp.Count - 1
                    sSuppOid &= dvSupp(R1)("suppoid").ToString & ","
                Next
            End If
            Dim sFilterCOA As String = ""
            If FilterDDLAcctg.SelectedValue <> "NONE" Then
                sFilterCOA = " AND dp.acctgoid=" & FilterDDLAcctg.SelectedValue
            End If
            Dim swhere As String = "" : Dim swheresupp As String = "" : Dim swherezero As String = ""
            If sSuppOid <> "" Then swhere = " WHERE suppoid IN (" & Left(sSuppOid, sSuppOid.Length - 1) & ")"
            If sSuppOid <> "" Then swheresupp = " WHERE s.suppoid IN (" & Left(sSuppOid, sSuppOid.Length - 1) & ")"
            If chkEmpty.Checked Then
                swherezero = " WHERE sawal>0 OR sawalidr>0 OR sawalusd>0 OR ISNULL(amtin,0)>0 OR ISNULL(amtinidr,0)>0 OR ISNULL(amtinusd,0)>0 OR ISNULL(amtout,0)>0 OR ISNULL(amtoutidr,0)>0 OR ISNULL(amtout,0)>0 "
            End If
            If FilterType.SelectedValue = "SUMMARY" Then
                report.Load(Server.MapPath(folderReport & "crKartuDPAPSum.rpt"))
                sSqlPlus_1 = "suppoid, suppname, (sawal) saidr, (sawal) sausd, SUM(amtin) beliidr, SUM(amtin) beliusd, SUM(amtout) paymentidr, SUM(amtout) paymentusd, 'IDR' currency, @cmpcode cmpcode, '" & FilterDDLMonth.SelectedItem.Text.ToUpper & " " & FilterDDLYear.SelectedItem.Text & "' periodreport, ISNULL((SELECT TOP 1 divname FROM QL_mstdivision WHERE divcode='" & FilterDDLDiv.SelectedValue & "'),'') company" : sSqlPlus_2 = " GROUP BY suppoid, suppname, sawal ORDER BY suppname" : sFilterDate_1 = "(RIGHT(CONVERT(VARCHAR(10), a.transdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), a.transdate, 101), 2))=@periodacctg" : sFilterDate_2 = "(RIGHT(CONVERT(VARCHAR(10), b.transdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), b.transdate, 101), 2))<@periodacctg"
            Else
                report.Load(Server.MapPath(folderReport & "crKartuDPAP.rpt"))
            End If
           
            sSql = "DECLARE @cmpcode AS VARCHAR(10);"
            sSql &= "DECLARE @start AS DATETIME; "
            sSql &= "DECLARE @finish AS DATETIME; "
            sSql &= "DECLARE @dateacuan AS DATETIME;"
            sSql &= "DECLARE @periodacctg AS VARCHAR(10);"
            sSql &= "SET @cmpcode='" & FilterDDLDiv.SelectedValue & "'; "
            sSql &= "SET @start='" & txtStart.Text & " 00:00:00'; "
            sSql &= "SET @finish='" & txtFinish.Text & " 23:59:59'; "
            sSql &= "SET @periodacctg='" & GetDateToPeriodAcctg(New Date(FilterDDLYear.SelectedValue, FilterDDLMonth.SelectedValue, 1)) & "';"
            sSql &= "SET @dateacuan=CAST('" & Format(New Date(FilterDDLYear.SelectedValue, FilterDDLMonth.SelectedValue, Date.DaysInMonth(FilterDDLYear.SelectedValue, FilterDDLMonth.SelectedValue)), "MM/dd/yyyy") & " 23:59:59' AS DATETIME);"
            sSql &= "SELECT " & sSqlPlus_1 & " FROM ( /* SUPPLIER & CURRENCY */ "
            sSql &= "SELECT ISNULL(a.cmpcode,@cmpcode) cmpcode,s.suppoid,s.suppname,ISNULL(a.transdate,@start) transdate,a.dpapoid,a.notrans,a.currcode,a.rate,ISNULL(a.amtin,0.0) AS amtin,ISNULL(a.amtinidr,0.0) AS amtinidr,ISNULL(a.amtinusd,0.0) AS amtinusd,ISNULL(a.amtout,0.0) AS amtout,ISNULL(a.amtoutidr,0.0) AS amtoutidr,ISNULL(a.amtoutusd,0.0) AS amtoutusd,SUM(ISNULL(b.amt,0.0)) sawal,SUM(ISNULL(b.amtidr,0.0)) sawalidr,SUM(ISNULL(b.amtusd,0.0)) sawalusd,'{?companyname}' companyname,'{?periodreport}' periodreport,'{?currvalue}' currvalue, ISNULL(a.transtype, 0) transtype, a.note "
            sSql &= "FROM QL_mstsupp s LEFT JOIN ( /* DOWN PAYMENT */ "
            sSql &= "SELECT dp.cmpcode,dp.dpapoid dpapoid,dp.dpapno notrans,dp.dpapdate transdate,s.suppoid,s.suppname,currcode currcode,1 rate,dp.dpapamt amtin,dp.dpapamt amtinidr,dp.dpapamt amtinusd,0.0 amtout,0.0 amtoutidr,0.0 amtoutusd, 1 transtype, dpapnote note FROM QL_trndpap dp INNER JOIN QL_mstsupp s ON s.suppoid=dp.suppoid LEFT JOIN QL_mstcurr cr ON cr.curroid=dp.curroid WHERE dp.cmpcode=@cmpcode AND dp.dpapstatus IN ('POST') " & sFilterCOA & " UNION ALL /* PAYMENT WITH DP */ "
            sSql &= "SELECT dp.cmpcode,dp.dpapoid,cb.cashbankno notrans,cb.cashbankdate transdate,s.suppoid,s.suppname,cr.currcode currcode,1 rate,0.0 amtin,0.0 amtinidr,0.0 amtinusd,SUM(cb.cashbankamt) amtout,SUM(cb.cashbankamt) amtoutidr,SUM(cb.cashbankamt) amtoutusd, 2 transtype, cashbanknote note FROM QL_trndpap dp INNER JOIN QL_mstsupp s ON s.suppoid=dp.suppoid LEFT JOIN QL_mstcurr cr ON cr.curroid=dp.curroid INNER JOIN QL_trncashbankmst cb ON cb.giroacctgoid=dp.dpapoid AND cb.cashbanktype='BLK' AND cb.cashbankstatus IN ('POST') AND cb.cashbankgroup<>'EXPENSE' WHERE dp.cmpcode=@cmpcode AND dp.dpapstatus IN ('POST') " & sFilterCOA & " GROUP BY dp.cmpcode,dp.dpapoid,cb.cashbankno,cb.cashbankdate,s.suppoid,s.suppname,cr.currcode, cashbanknote UNION ALL /* EXPENSE WITH DP */ "
            sSql &= "SELECT dp.cmpcode,dp.dpapoid dpapoid,cb.cashbankno notrans,cb.cashbankdate transdate,s.suppoid,s.suppname,cr.currcode currcode,1 rate,0.0 amtin,0.0 amtinidr,0.0 amtinusd,SUM(cb.cashbankamt) amtout,SUM(cb.cashbankamt) amtoutidr,SUM(cb.cashbankamt) amtoutusd, 2 transtype, cashbanknote note FROM QL_trndpap dp INNER JOIN QL_mstsupp s ON s.suppoid=dp.suppoid LEFT JOIN QL_mstcurr cr ON cr.curroid=dp.curroid INNER JOIN QL_trncashbankmst cb ON cb.giroacctgoid=dp.dpapoid AND cb.cashbanktype='BLK' AND cb.cashbankstatus IN ('POST') AND cb.cashbankgroup='EXPENSE' WHERE dp.cmpcode=@cmpcode AND dp.dpapstatus IN ('POST') " & sFilterCOA & " GROUP BY dp.cmpcode,dp.dpapoid,cb.cashbankno,cb.cashbankdate,s.suppoid,s.suppname,cr.currcode, cashbanknote "
            sSql &= ") AS a ON a.suppoid=s.suppoid AND " & sFilterDate_1
            sSql &= " LEFT JOIN ( /* DOWN PAYMENT */SELECT dp.cmpcode,dp.dpapoid dpapoid,dp.dpapno notrans,dp.dpapdate transdate,s.suppoid,s.suppname,cr.currcode currcode,1 rate,dp.dpapamt amt,dp.dpapamt amtidr,dp.dpapamt amtusd FROM QL_trndpap dp INNER JOIN QL_mstsupp s ON s.suppoid=dp.suppoid LEFT JOIN QL_mstcurr cr ON cr.curroid=dp.curroid WHERE dp.cmpcode=@cmpcode AND dp.dpapstatus IN ('POST') " & sFilterCOA & " UNION ALL /* PAYMENT WITH DP */ "
            sSql &= "SELECT dp.cmpcode,dp.dpapoid papoid,cb.cashbankno notrans,cb.cashbankdate transdate,s.suppoid,s.suppname,cr.currcode currcode,1 rate,(cb.cashbankamt*-1) amt,(cb.cashbankamt*-1) amtidr,(cb.cashbankamt*-1) amtusd FROM QL_trndpap dp INNER JOIN QL_mstsupp s ON s.suppoid=dp.suppoid LEFT JOIN QL_mstcurr cr ON cr.curroid=dp.curroid INNER JOIN QL_trncashbankmst cb ON cb.giroacctgoid=dp.dpapoid AND cb.cashbanktype='BLK' AND cb.cashbankstatus IN ('POST') AND cb.cashbankgroup<>'EXPENSE' WHERE dp.cmpcode=@cmpcode AND dp.dpapstatus IN ('POST') " & sFilterCOA & " UNION ALL /* EXPENSE WITH DP */ "
            sSql &= "SELECT dp.cmpcode,dp.dpapoid dpapoid,cb.cashbankno notrans,cb.cashbankdate transdate,s.suppoid,s.suppname,cr.currcode currcode,1 rate,(cb.cashbankamt*-1) amt,(cb.cashbankamt*-1) amtidr,(cb.cashbankamt*-1) amtusd FROM QL_trndpap dp INNER JOIN QL_mstsupp s ON s.suppoid=dp.suppoid LEFT JOIN QL_mstcurr cr ON cr.curroid=dp.curroid INNER JOIN QL_trncashbankmst cb ON cb.giroacctgoid=dp.dpapoid AND cb.cashbanktype='BLK' AND cb.cashbankstatus IN ('POST') " & sFilterCOA & " AND cb.cashbankgroup='EXPENSE' WHERE dp.cmpcode=@cmpcode AND dp.dpapstatus IN ('POST') "
            sSql &= ") AS b ON b.suppoid=s.suppoid AND " & sFilterDate_2 & " " & swheresupp & ""
            sSql &= "GROUP BY a.cmpcode,s.suppoid,s.suppname,a.transdate,a.dpapoid,a.notrans,a.currcode,a.rate,a.amtin,a.amtinidr,a.amtinusd,a.amtout,a.amtoutidr,a.amtoutusd, a.transtype, a.note "
            sSql &= ") AS d " & swherezero & sSqlPlus_2
            Dim dtData As DataTable = cKon.ambiltabel(sSql, "tblDtl")
            report.SetDataSource(dtData)

            cProc.SetDBLogonForReport(report)

            If FilterType.SelectedValue.ToUpper = "DETAIL" Then
                report.SetParameterValue("cmpcode", FilterDDLDiv.SelectedValue)
                report.SetParameterValue("currvalue", FilterCurrency.SelectedValue)
                report.SetParameterValue("companyname", FilterDDLDiv.SelectedItem.Text)
                report.SetParameterValue("periodreport", Format(CDate(txtStart.Text), "dd MMMM yyyy") & " - " & Format(CDate(txtFinish.Text), "dd MMMM yyyy"))

            Else
                report.SetParameterValue("PrintLastModified", "")
                report.SetParameterValue("PrintUserID", Session("UserID"))
            End If
            report.PrintOptions.PaperOrientation = PaperOrientation.Portrait
            report.PrintOptions.PaperSize = PaperSize.PaperFolio

            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "DPAPREPORT_" & Format(CDate(txtStart.Text), "yyyyMMdd" & "_" & Format(CDate(txtFinish.Text), "yyyyMMdd")))
                Catch ex As Exception
                    report.Close() : report.Dispose()
                End Try
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "DPAPREPORT_" & Format(CDate(txtStart.Text), "yyyyMMdd" & "_" & Format(CDate(txtFinish.Text), "yyyyMMdd")))
                Catch ex As Exception
                    report.Close() : report.Dispose()
                End Try
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.ToString, 1)
        End Try
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmDPAP.aspx")
        End If
        If checkPagePermission("~\ReportForm\frmDPAP.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Down Payment A/P Report"
        If Not Page.IsPostBack Then
            ' DDL Division
            sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE' "
            If Session("CompnyCode") <> CompnyCode Then
                sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
            End If
            FillDDL(FilterDDLDiv, sSql)

            FilterDDLMonth.Items.Clear()
            For C1 As Int16 = 1 To 12
                FilterDDLMonth.Items.Add(MonthName(C1))
                FilterDDLMonth.Items(FilterDDLMonth.Items.Count - 1).Value = Format(C1, "00")
            Next
            FilterDDLMonth.SelectedValue = Format(Now.Month, "00")
            FilterDDLYear.Items.Clear()
            For C1 As Int16 = Now.Year - 2 To Now.Year + 2
                FilterDDLYear.Items.Add(C1)
                FilterDDLYear.Items(FilterDDLYear.Items.Count - 1).Value = C1
            Next
            FilterDDLYear.SelectedValue = Now.Year

            FilterCurrency.Items.Clear()
            FilterCurrency.Items.Add(New ListItem("INDONESIAN RUPIAH", "IDR"))
            FilterDDLAcctg.Items.Clear()
            FillDDLAcctg(FilterDDLAcctg, "VAR_DPAP", Session("CompnyCode"), "NONE", "NONE")

            pnlSupplier.CssClass = "popupControl"
            txtStart.Text = Format(GetServerTime, "MM/01/yyyy")
            txtFinish.Text = Format(GetServerTime, "MM/dd/yyyy")

            FilterType_SelectedIndexChanged(Nothing, Nothing)
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub FilterDDLDiv_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLDiv.SelectedIndexChanged
        rbSupplier.SelectedIndex = 0 : rbSupplier_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub FilterType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterType.SelectedIndexChanged
        If (FilterType.SelectedIndex = 0) Then
            FilterDDLMonth.Visible = True : FilterDDLYear.Visible = True
            txtStart.Visible = False : imbStart.Visible = False : lblTo.Visible = False
            txtFinish.Visible = False : imbFinish.Visible = False : lblMMDD.Visible = False
        Else
            FilterDDLMonth.Visible = False : FilterDDLYear.Visible = False
            txtStart.Visible = True : imbStart.Visible = True : lblTo.Visible = True
            txtFinish.Visible = True : imbFinish.Visible = True : lblMMDD.Visible = True
        End If
        'FilterCurrency.Items(2).Enabled = (FilterType.SelectedIndex = 1)
    End Sub

    Protected Sub rbSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbSupplier.SelectedIndexChanged
        gvSupplier.DataSource = Nothing : gvSupplier.DataBind()
        Session("QL_mstsupp") = Nothing : gvSupplier.Visible = False
        If rbSupplier.SelectedValue = "ALL" Then
            FilterTextSupplier.Visible = False
            btnSearchSupplier.Visible = False
            btnClearSupplier.Visible = False
        Else
            FilterTextSupplier.Visible = True
            btnSearchSupplier.Visible = True
            btnClearSupplier.Visible = True
        End If
    End Sub

    Protected Sub btnSearchSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupplier.Click
        BindSupplierData()
    End Sub

    Protected Sub btnClearSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupplier.Click
        FilterTextSupplier.Text = ""
        suppoid.Text = ""
        gvSupplier.DataSource = Nothing
        gvSupplier.DataBind()
        gvSupplier.Visible = False
    End Sub

    Protected Sub gvSupplier_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupplier.PageIndexChanging
        UpdateCheckedGV()

        gvSupplier.PageIndex = e.NewPageIndex
        Dim dtSupp As DataTable = Session("QL_mstsupp")
        gvSupplier.DataSource = dtSupp
        gvSupplier.DataBind()
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        UpdateCheckedGV()
        Dim sMsg As String = ValidateParam()
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        Else
            ShowReport("View")
        End If
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        UpdateCheckedGV()
        Dim sMsg As String = ValidateParam()
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        Else
            ShowReport("Print PDF")
        End If
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        UpdateCheckedGV()
        Dim sMsg As String = ValidateParam()
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        Else
            ShowReport("Print Excel")
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmDPAP.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Dispose() : report.Close()
    End Sub
#End Region

End Class
