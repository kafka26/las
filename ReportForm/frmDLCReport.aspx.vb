Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_DLCReport
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function 'OK

    Private Function UpdateCheckedBOM() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblBOM") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblBOM")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListBOM.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListBOM.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "bomoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblBOM") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function 'OK

    Private Function UpdateCheckedBOM2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblBOMView2") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblBOM")
            Dim dtTbl2 As DataTable = Session("TblBOMView2")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListBOM.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListBOM.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "bomoid=" & cbOid
                                dtView2.RowFilter = "bomoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblBOM") = dtTbl
                Session("TblBOMView2") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function 'OK

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                dtView2.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub 'OK

    Private Sub InitAllDDL()
        ' DDL Division
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE' "
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If FillDDL(FilterDDLDiv, sSql) Then
            InitDept()
        End If
    End Sub 'OK

    Private Sub InitDept()
        ' Init DDL Department
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE cmpcode='" & FilterDDLDiv.SelectedValue & "' AND activeflag='ACTIVE'"
        FillDDL(FilterDDLDept, sSql)
        If DDLDept.SelectedValue.ToUpper = "TO" Then
            FilterDDLDept.Items.Add("END")
            FilterDDLDept.Items.Item(FilterDDLDept.Items.Count - 1).Value = "END"
        End If
    End Sub 'OK

    Private Sub BindListBOM()
        sSql = "SELECT 'False' AS Checkvalue , bom.bomoid, CONVERT(VARCHAR(30),bom.bomoid) AS bomdoid , bom.bomdesc, CONVERT(VARCHAR(10), bom.bomdate, 101) AS bomdate, bom.bomnote  FROM QL_mstdlc dlcm INNER JOIN QL_mstbom bom ON dlcm.bomoid=bom.bomoid WHERE bom.cmpcode='" & FilterDDLDiv.SelectedValue & "' "
        If cbPeriod.Checked Then
            If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
                If IsValidPeriod() Then
                    sSql &= " AND " & FilterDDLDate.SelectedValue & ">='" & FilterPeriod1.Text & " 00:00:00' AND " & FilterDDLDate.SelectedValue & "<='" & FilterPeriod2.Text & " 23:59:59'"
                Else
                    Exit Sub
                End If
            End If
        End If
        sSql &= " ORDER BY bom.bomdate DESC, bom.bomoid DESC"
        Session("TblBOM") = cKon.ambiltabel(sSql, "QL_mstbom")
    End Sub 'OK

    Private Sub BindListMat()
        sSql = "SELECT 'False' AS checkvalue, itemcat1, itemcat2, itemcat3, itemcat4, i.itemoid, itemcode, itemshortdescription AS itemshortdesc, itemlongdescription AS itemlongdesc, g.gendesc AS unit FROM QL_mstdlc dlcm INNER JOIN QL_mstbom bom ON bom.cmpcode=dlcm.cmpcode AND bom.bomoid=dlcm.bomoid INNER JOIN QL_mstitem i ON i.itemoid=bom.itemoid INNER JOIN QL_mstgen g ON g.genoid=i.itemunit1  WHERE dlcm.cmpcode='" & FilterDDLDiv.SelectedValue & "'"
        If bomoid.Text <> "" Then
            sSql &= " AND bom.bomoid IN (" & bomoid.Text & ")"
        End If

        If cbPeriod.Checked Then
            If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
                If IsValidPeriod() Then
                    sSql &= " AND " & FilterDDLDate.SelectedValue & ">='" & FilterPeriod1.Text & " 00:00:00' AND " & FilterDDLDate.SelectedValue & "<='" & FilterPeriod2.Text & " 23:59:59'"
                Else
                    Exit Sub
                End If
            End If
        End If


        sSql &= " ORDER BY itemcode"
        Session("TblMat") = cKon.ambiltabel(sSql, "QL_mstitem")
    End Sub 'OK

    Private Sub InitFilterDDLCat1()
        'Fill DDL Category1
        sSql = "SELECT cat1oid, cat1code+' - '+cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat1res1='FG' Order By cat1code"
        FillDDL(FilterDDLCat01, sSql)
        If FilterDDLCat01.Items.Count > 0 Then
            InitFilterDDLCat2()
        Else
            FilterDDLCat02.Items.Clear()
            FilterDDLCat03.Items.Clear()
            FilterDDLCat04.Items.Clear()
        End If
    End Sub 'OK

    Private Sub InitFilterDDLCat2()
        'Fill DDL Category2
        sSql = "SELECT cat2oid, cat2code+' - '+cat2shortdesc FROM QL_mstcat2 cat2 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat2.cmpcode AND cat1.cat1oid=cat2.cat1oid AND cat1.cat1oid='" & FilterDDLCat01.SelectedValue & "' WHERE cat2.cmpcode='" & CompnyCode & "' AND cat2.activeflag='ACTIVE' AND cat2res1='FG' Order By cat2code"
        FillDDL(FilterDDLCat02, sSql)
        If FilterDDLCat02.Items.Count > 0 Then
            InitFilterDDLCat3()
        Else
            FilterDDLCat03.Items.Clear()
            FilterDDLCat04.Items.Clear()
        End If
    End Sub 'OK

    Private Sub InitFilterDDLCat3()
        'Fill DDL Category3
        sSql = "SELECT cat3oid, cat3code+' - '+cat3shortdesc FROM QL_mstcat3 cat3 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat3.cmpcode AND cat1.cat1oid=cat3.cat1oid AND cat1.cat1oid='" & FilterDDLCat01.SelectedValue & "' INNER JOIN QL_mstcat2 cat2 ON cat2.cmpcode=cat3.cmpcode AND cat2.cat2oid=cat3.cat2oid AND cat2.cat2oid='" & FilterDDLCat02.SelectedValue & "' WHERE cat3.cmpcode='" & CompnyCode & "' AND cat3.activeflag='ACTIVE' AND cat3res1='FG' Order By cat3code"
        FillDDL(FilterDDLCat03, sSql)
        If FilterDDLCat03.Items.Count > 0 Then
            InitFilterDDLCat4()
        Else
            FilterDDLCat04.Items.Clear()
        End If
    End Sub 'OK

    Private Sub InitFilterDDLCat4()
        'Fill DDL Category4
        sSql = "SELECT genoid, gencode+' - '+gendesc FROM QL_mstgen WHERE gengroup='CAT4' AND activeflag='ACTIVE' ORDER BY gencode"
        FillDDL(FilterDDLCat04, sSql)

    End Sub 'OK

    Private Sub ShowReport(ByVal sType As String)
        Dim sWhere As String = ""
        Dim rptName As String = ""
        Dim Dtl As String = ""
        Dim Join As String = ""
        Try
            If FilterDDLType.SelectedValue = "Summary" Then
                If sType = "Print Excel" Then
                    report.Load(Server.MapPath(folderReport & "rptDLC_SumXls.rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptDLC_SumPdf.rpt"))
                End If
                rptName = "DirectLaborCostSummary"
            Else
                If sType = "Print Excel" Then
                    report.Load(Server.MapPath(folderReport & "rptDLC_DtlXls.rpt"))
                Else
                    If DDLGrouping.SelectedValue = "i.itemcode" Then
                        report.Load(Server.MapPath(folderReport & "rptDLC_DtlFGPdf.rpt"))
                    ElseIf DDLGrouping.SelectedValue = "dlcd.deptoid" Then
                        report.Load(Server.MapPath(folderReport & "rptDLC_DtlDeptFromPdf.rpt"))
                    Else
                        report.Load(Server.MapPath(folderReport & "rptDLC_DtlDeptToPdf.rpt"))
                    End If
                End If
                rptName = "DirectLaborCostDetail"
            End If

            If FilterDDLType.SelectedValue = "Detail" Then
                Dtl = " , dlcd.dlcdtlseq [DLC Seq] , dlcd.dlcpct [Cost %], dlcd.dlcdtlamount [DL Cost Amt.], dlcd.dlcohdamount [OHD Cost Amt.], dlcd.dlcdtlnote [Detail Note], dlcd.deptoid [Dept Oid From], (SELECT deptname FROM QL_mstdept df WHERE df.cmpcode=dlcd.cmpcode AND df.deptoid=dlcd.deptoid)  [Dept. From], bom.bomoid [BOM Oid], bomd1.bomdtl1todeptoid [Dept Oid To], (CASE bomd1.bomdtl1reftype WHEN 'FG' THEN 'END' ELSE (SELECT deptname FROM QL_mstdept dt WHERE dt.cmpcode=bomd1.cmpcode AND dt.deptoid=bomd1.bomdtl1todeptoid) END) [Dept. To], dlcohdamount [DLC FOH] "
                Join = " INNER JOIN QL_mstdlcdtl dlcd ON dlcm.cmpcode=dlcd.cmpcode AND dlcm.dlcoid=dlcd.dlcoid INNER JOIN QL_mstbomdtl1 bomd1 ON dlcm.cmpcode=bomd1.cmpcode AND bomd1.bomoid=dlcm.bomoid AND dlcd.deptoid=bomd1.bomdtl1deptoid"
            End If
            sSql = "SELECT (SELECT div.divname FROM QL_mstdivision div WHERE dlcm.cmpcode = div.cmpcode) AS [Business Unit], dlcm.cmpcode [CMPCODE], dlcm.dlcoid [Oid], dlcm.createtime [DLC Create Date], dlcm.dlcdesc [DLC Desc], dlcm.dlcamount [DLC Amount], dlcoverheadamt [DLC Overhead Amt], dlcm.dlctotalpct [Total Cost %], dlcm.dlcnote [Header Note],/*convert(datetime,dlcm.dlcres1,1) AS*/ CAST(dlcm.dlcres1 AS date) AS [Last BOM Update], 0.0 [Max DLC], 0.0 [Max OHD], itemcode AS [FG Code], itemShortDescription AS [FG Short Desc.], itemLongDescription AS [FG Long Desc.], 0.0 AS [FG Length], 0.0 AS [FG Width], 0.0 AS [FG Height], 0.0 AS [FG Diameter], 0.0 AS [FG Volume], 0.0 AS [FG Packing Length], 0.0 AS [FG Packing Width], 0.0 AS [FG Packing Height], 0.0 AS [FG Packing Diameter], 0.0 AS [FG Packing Volume], 0.0 AS [CBF], g.gendesc [Unit], dlcm.createuser [Create User], dlcm.upduser [Update User], dlcm.updtime [Update Time], dlcm.activeflag [Status], GETDATE() AS [Precosting Date], GETDATE() [Precosting Create Date], '' [Precosting Create User], GETDATE() [Precosting Update Time], '' [Precosting Update User] , bom.bomdate [BOM Date], bom.createtime [BOM Create Date], bom.createuser [BOM Create User], /*convert(datetime,dlcm.dlcres1,1)*/CAST(dlcm.dlcres1 AS date) AS [BOM Update Time], bom.upduser [BOM Update User], dlcoverheadamt AS  [Total DLC FOH]  " & Dtl & " FROM QL_mstdlc dlcm INNER JOIN QL_mstbom bom ON bom.cmpcode=dlcm.cmpcode AND bom.bomoid=dlcm.bomoid /*INNER JOIN QL_mstprecost pr ON pr.precostoid=bom.precostoid*/ INNER JOIN QL_mstitem i ON bom.itemoid=i.itemoid INNER JOIN QL_mstgen g ON g.genoid=bom.itemunitoid /*INNER JOIN QL_mstprecost pre ON pre.cmpcode=dlcm.cmpcode AND pre.precostoid=bom.precostoid*/ " & Join

            If FilterDDLDiv.SelectedValue <> "ALL" Then
                sSql &= " WHERE dlcm.cmpcode='" & FilterDDLDiv.SelectedValue & "'"
            Else
                sSql &= " WHERE dlcm.cmpcode LIKE '%%'"
            End If

            If bomoid.Text <> "" Then
                sSql &= " AND bom.bomoid IN (" & bomoid.Text & ")"
            End If
            If cbDept.Checked Then
                If DDLDept.SelectedValue.ToUpper = "FROM" Then
                    sSql &= " AND dlcd.deptoid=" & FilterDDLDept.SelectedValue & ""
                Else
                    If FilterDDLDept.SelectedValue.ToUpper = "END" Then
                        sSql &= " AND bomd1.bomdtl1reftype='FG'"
                    Else
                        sSql &= "'AND bomd1.bomdtl1todeptoid=" & FilterDDLDept.SelectedValue & " "
                    End If
                End If
            End If
            If cbPeriod.Checked Then
                If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
                    If IsValidPeriod() Then
                        sSql &= " AND " & FilterDDLDate.SelectedValue & ">='" & FilterPeriod1.Text & " 00:00:00' AND " & FilterDDLDate.SelectedValue & "<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
            End If

            If itemcode.Text <> "" Then
                Dim sMatcode() As String = Split(itemcode.Text, ";")
                sSql &= " AND ("
                For c1 As Integer = 0 To sMatcode.Length - 1
                    sSql &= " i.itemcode LIKE '%" & Tchar(sMatcode(c1)) & "%'"
                    If c1 < sMatcode.Length - 1 Then
                        sSql &= " OR"
                    End If
                Next
                sSql &= ")"
            End If

            If FilterDDLType.SelectedValue = "Summary" Then
                sSql &= " ORDER BY dlcm.cmpcode ASC, i.itemcode ASC "
            Else
                If DDLGrouping.SelectedIndex = 0 Then
                    sSql &= " ORDER BY dlcm.cmpcode ASC, i.itemcode ASC , dlcd.dlcdtlseq ASC "
                ElseIf DDLGrouping.SelectedIndex = 1 Then
                    sSql &= " ORDER BY dlcm.cmpcode ASC, [Dept. From] ASC , i.itemcode ASC, dlcd.dlcdtlseq ASC "
                Else
                    sSql &= " ORDER BY dlcm.cmpcode ASC, [Dept. To] ASC , i.itemcode ASC, dlcd.dlcdtlseq ASC "
                End If
            End If

            Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstdlc")
            Dim dvTbl As DataView = dtTbl.DefaultView
            Dim strHostName As String
            Dim client As System.Net.IPHostEntry
            client = System.Net.Dns.GetHostEntry(Request.ServerVariables.Item("REMOTE_HOST"))
            strHostName = client.HostName

            report.SetDataSource(dvTbl.ToTable)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            report.PrintOptions.PaperSize = PaperSize.PaperFolio
            cProc.SetDBLogonForReport(report)

            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, rptName)
                Catch ex As Exception
                    report.Close()
                    report.Dispose()
                End Try
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, rptName)
                Catch ex As Exception
                    report.Close()
                    report.Dispose()
                End Try
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        'Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmDLCReport.aspx")
        End If
        If checkPagePermission("~\ReportForm\frmDLCReport.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Direct Labor Cost Report"
        If Not Page.IsPostBack Then
            InitAllDDL()
            FilterDDLType_SelectedIndexChanged(Nothing, Nothing)
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        End If
    End Sub 'OK

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListBOM") Is Nothing And Session("EmptyListBOM") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListBOM") Then
                Session("EmptyListBOM") = Nothing
                cProc.SetModalPopUpExtender(btnHideListBOM, pnlListBOM, mpeListBOM, True)
            End If
        End If
        If Not Session("WarningListBOM") Is Nothing And Session("WarningListBOM") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListBOM") Then
                Session("WarningListBOM") = Nothing
                cProc.SetModalPopUpExtender(btnHideListBOM, pnlListBOM, mpeListBOM, True)
            End If
        End If
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
    End Sub 'OK

    Protected Sub FilterDDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLType.SelectedIndexChanged
        If FilterDDLType.SelectedValue = "Summary" Then
            'Fill DDL Grouping
            DDLGrouping.Items.Clear()
            DDLGrouping.Items.Add("FG Code")
            DDLGrouping.Items.Item(DDLGrouping.Items.Count - 1).Value = "i.itemcode"
            'Hide Dept
            cbDept.Visible = False
            DDLDept.Visible = False
            lblSepDept.Visible = False
            FilterDDLDept.Visible = False
        Else
            'Fill DDL Grouping
            DDLGrouping.Items.Clear()
            DDLGrouping.Items.Add("FG Code")
            DDLGrouping.Items.Item(DDLGrouping.Items.Count - 1).Value = "i.itemcode"
            DDLGrouping.Items.Add("Departement From")
            DDLGrouping.Items.Item(DDLGrouping.Items.Count - 1).Value = "dlcd.deptoid"
            DDLGrouping.Items.Add("Department To")
            DDLGrouping.Items.Item(DDLGrouping.Items.Count - 1).Value = "bomd1.bomdtl1todeptoid"
            'Show Dept
            cbDept.Visible = True
            DDLDept.Visible = True
            lblSepDept.Visible = True
            FilterDDLDept.Visible = True
        End If
    End Sub

    Protected Sub DDLDept_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitDept()
    End Sub

    Protected Sub imbEraseBom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseBom.Click
        bomoid.Text = ""
    End Sub 'OK

    Protected Sub imbFindBOM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindBOM.Click
        If IsValidPeriod() Then
            DDLFilterListBOM.SelectedIndex = -1 : txtFilterListBOM.Text = ""
            Session("TblBOM") = Nothing : Session("TblBOMView") = Nothing : gvListBOM.DataSource = Nothing : gvListBOM.DataBind()
            cProc.SetModalPopUpExtender(btnHideListBOM, pnlListBOM, mpeListBOM, True)
        Else
            Exit Sub
        End If
    End Sub 'OK

    Protected Sub btnFindListBOM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListBOM.Click
        If Session("TblBOM") Is Nothing Then
            BindListBOM()
            If Session("TblBOM").Rows.Count <= 0 Then
                Session("EmptyListBOM") = "BOM data can't be found!"
                showMessage(Session("EmptyListBOM"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListBOM.SelectedValue & " LIKE '%" & Tchar(txtFilterListBOM.Text) & "%'"
        If UpdateCheckedBOM() Then
            Dim dv As DataView = Session("TblBOM").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblBOMView") = dv.ToTable
                gvListBOM.DataSource = Session("TblBOMView")
                gvListBOM.DataBind()
                dv.RowFilter = ""
                mpeListBOM.Show()
            Else
                dv.RowFilter = ""
                Session("TblBOMView") = Nothing
                gvListBOM.DataSource = Session("TblBOMView")
                gvListBOM.DataBind()
                Session("WarningListBOM") = "BOM data can't be found!"
                showMessage(Session("WarningListBOM"), 2)
            End If
        Else
            Session("WarningListBOM") = "BOM data can't be found!"
            showMessage(Session("WarningListBOM"), 2)
        End If
    End Sub 'OK

    Protected Sub btnViewAllListBOM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListBOM.Click
        DDLFilterListBOM.SelectedIndex = -1 : txtFilterListBOM.Text = ""
        If Session("TblBOM") Is Nothing Then
            BindListBOM()
            If Session("TblBOM").Rows.Count <= 0 Then
                Session("EmptyListBOM") = "BOM data can't be found!"
                showMessage(Session("EmptyListBOM"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedBOM() Then
            Dim dt As DataTable = Session("TblBOM")
            Session("TblBOMView") = dt
            gvListBOM.DataSource = Session("TblBOMView")
            gvListBOM.DataBind()
            mpeListBOM.Show()
        Else
            Session("WarningListBOM") = "BOM data can't be found!"
            showMessage(Session("WarningListBOM"), 2)
        End If

    End Sub 'OK

    Protected Sub btnSelectAllBOM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllBOM.Click
        If Not Session("TblBOMView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblBOMView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblBOM")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "bomoid=" & dtTbl.Rows(C1)("bomoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblBOM") = objTbl
                Session("TblBOMView") = dtTbl
                gvListBOM.DataSource = Session("TblBOMView")
                gvListBOM.DataBind()
            End If
            mpeListBOM.Show()
        Else
            Session("WarningListBOM") = "Please show some BOM data first!"
            showMessage(Session("WarningListBOM"), 2)
        End If
    End Sub 'OK

    Protected Sub btnSelectNoneBOM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneBOM.Click
        If Not Session("TblBOMView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblBOMView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblBOM")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "bomoid=" & dtTbl.Rows(C1)("bomoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblBOM") = objTbl
                Session("TblBOMView") = dtTbl
                gvListBOM.DataSource = Session("TblBOMView")
                gvListBOM.DataBind()
            End If
            mpeListBOM.Show()
        Else
            Session("WarningListBOM") = "Please show some BOM data first!"
            showMessage(Session("WarningListBOM"), 2)
        End If
    End Sub 'OK

    Protected Sub btnViewCheckedBOM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedBOM.Click
        If Session("TblBOM") Is Nothing Then
            Session("WarningListBOM") = "Selected BOM data can't be found!"
            showMessage(Session("WarningListBOM"), 2)
            Exit Sub
        End If
        If UpdateCheckedBOM() Then
            Dim dtTbl As DataTable = Session("TblBOM")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListBOM.SelectedIndex = -1 : txtFilterListBOM.Text = ""
                Session("TblBOMView") = dtView.ToTable
                gvListBOM.DataSource = Session("TblBOMView")
                gvListBOM.DataBind()
                dtView.RowFilter = ""
                mpeListBOM.Show()
            Else
                dtView.RowFilter = ""
                Session("TblBOMView") = Nothing
                gvListBOM.DataSource = Session("TblBOMView")
                gvListBOM.DataBind()
                Session("WarningListBOM") = "Selected BOM data can't be found!"
                showMessage(Session("WarningListBOM"), 2)
            End If
        Else
            mpeListBOM.Show()
        End If
    End Sub 'OK

    Protected Sub gvListBOM_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListBOM.PageIndexChanging
        If UpdateCheckedBOM2() Then
            gvListBOM.PageIndex = e.NewPageIndex
            gvListBOM.DataSource = Session("TblBOMView")
            gvListBOM.DataBind()
        End If
        mpeListBOM.Show()
    End Sub 'OK

    Protected Sub lkbAddToListBOM_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListBOM.Click
        If Not Session("TblBOM") Is Nothing Then
            If UpdateCheckedBOM() Then
                Dim dtTbl As DataTable = Session("TblBOM")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If bomoid.Text <> "" Then
                            bomoid.Text &= "," + dtView(C1)("bomdoid")
                        Else
                            bomoid.Text = dtView(C1)("bomdoid")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListBOM, pnlListBOM, mpeListBOM, False)
                Else
                    Session("WarningListBOM") = "Please select BOM to add to list!"
                    showMessage(Session("WarningListBOM"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListBOM") = "Please show some BOM data first!"
            showMessage(Session("WarningListBOM"), 2)
            Exit Sub
        End If
    End Sub 'OK

    Protected Sub lkbCloseListBOM_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListBOM.Click
        cProc.SetModalPopUpExtender(btnHideListBOM, pnlListBOM, mpeListBOM, False)
    End Sub 'OK

    Protected Sub FilterDDLCat01_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat01.SelectedIndexChanged
        InitFilterDDLCat2()
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat02_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat02.SelectedIndexChanged
        InitFilterDDLCat3()
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat03_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat03.SelectedIndexChanged
        InitFilterDDLCat4()
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat04_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat04.SelectedIndexChanged
        mpeListMat.Show()
    End Sub

    Protected Sub imbFindMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindMat.Click
        If IsValidPeriod() Then
            FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
            InitFilterDDLCat1()
            cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
            Session("TblMat") = Nothing : Session("TblMatView") = Nothing : gvListMat.DataSource = Nothing : gvListMat.DataBind() : tbData.Text = "5" : gvListMat.PageSize = CInt(tbData.Text)
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            Exit Sub
        End If
    End Sub 'OK

    Protected Sub imbEraseMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseMat.Click
        itemcode.Text = ""
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        If cbCat01.Checked Then
            If FilterDDLCat01.SelectedValue <> "" Then
                sPlus &= " AND itemcat1='" & FilterDDLCat01.SelectedValue & "'"
            Else
                cbCat01.Checked = False
            End If
        End If
        If cbCat02.Checked Then
            If FilterDDLCat02.SelectedValue <> "" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND itemcat1='" & FilterDDLCat01.SelectedValue & "'") & " AND itemcat2='" & FilterDDLCat02.SelectedValue & "'"
            Else
                cbCat02.Checked = False
            End If
        End If
        If cbCat03.Checked Then
            If FilterDDLCat03.SelectedValue <> "" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND itemcat1='" & FilterDDLCat01.SelectedValue & "'") & IIf(cbCat02.Checked, "", " AND itemcat2='" & FilterDDLCat02.SelectedValue & "'") & " AND itemcat3='" & FilterDDLCat03.SelectedValue & "'"
            Else
                cbCat03.Checked = False
            End If
        End If
        If cbCat04.Checked Then
            If FilterDDLCat04.SelectedValue <> "" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND itemcat1='" & FilterDDLCat01.SelectedValue & "'") & IIf(cbCat02.Checked, "", " AND itemcat2='" & FilterDDLCat02.SelectedValue & "'") & IIf(cbCat03.Checked, "", " AND itemcat3='" & FilterDDLCat03.SelectedValue & "'") & " AND itemcat4='" & FilterDDLCat04.SelectedValue & "'"
            Else
                cbCat04.Checked = False
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub 'OK

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        InitFilterDDLCat1()
        cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub 'OK

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If itemcode.Text <> "" Then
                            itemcode.Text &= ";" + vbCrLf + dtView(C1)("itemcode")
                        Else
                            itemcode.Text = dtView(C1)("itemcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some Material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                InitFilterDDLCat1()
                cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Selected material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub lbShowData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbShowData.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Please find material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If tbData.Text = "" Then
            Session("WarningListMat") = "Please fill total data to be shown!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        Else
            If ToDouble(tbData.Text) <= 0 Then
                Session("WarningListMat") = "Total data to be shown must be more than 0!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat2() Then
            gvListMat.PageSize = Int(tbData.Text)
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub 'OK

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub 'OK

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub 'OK

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub 'OK

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmDLCReport.aspx?awal=true")
    End Sub 'OK

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub 'OK

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub 'OK

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub 'OK

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Dispose()
                report.Close()
    End Sub 'OK
#End Region

End Class
