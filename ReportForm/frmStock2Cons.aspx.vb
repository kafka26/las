Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_RawMaterialStockIncludeValue
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
    Const sTypeCat = "Raw"
    Const sTypeMat = "item"
    Const sRefName = ""
    Const sTypeIS = "MIS"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If FilterPeriod1.Text = "" Or FilterPeriod2.Text = "" Then
            showMessage("Please fill both of Period 1 and Period 2 first!", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitAllDDL()
        ' Init DDL Business Unit
        sSql = "SELECT divcode, UPPER(divname) AS divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        FillDDL(DDLBusUnit, sSql)
        ' Init DDL Warehouse
        sSql = "SELECT genoid, UPPER(gendesc) AS gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND gengroup='WAREHOUSE' AND genoid IN (SELECT DISTINCT transitemtowhoid FROM QL_trntransitemmst m INNER JOIN QL_trntransitemdtl d ON d.transitemmstoid=m.transitemmstoid WHERE m.iskonsinyasi='True' AND transitemmststatus<>'In Process') ORDER BY gendesc"
        FillDDL(DDLWarehouse, sSql)
        ' Init DDLMonth dan Year
        For R2 As Integer = 1 To 12
            DDLMonth.Items.Add(New ListItem(MonthName(R2), R2))
        Next
        DDLMonth.SelectedValue = GetServerTime.Month
        sSql = "SELECT ISNULL(MIN(periodacctg),190001) FROM QL_crdstock "
        Dim firstperiod As String = GetStrData(sSql)
        Dim iYear As Integer = Integer.Parse(Left(firstperiod, 4))
        For R1 As Integer = iYear To GetServerTime.Year
            DDLYear.Items.Add(New ListItem(R1, R1))
        Next
        DDLYear.SelectedValue = GetServerTime.Year

        'Fill DDL Type Mat
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND gengroup='GROUPITEM' AND gencode IN ('FG') ORDER BY gendesc"
        FillDDL(DDLTypeMat, sSql)
    End Sub

    Private Sub InitDDLCat1()
        'Fill DDL Category 1
        Dim sGroup As String = "RAW"
        If DDLTypeMat.SelectedItem.Text = "GENERAL MATERIAL" Then
            sGroup = "GEN"
        ElseIf DDLTypeMat.SelectedItem.Text = "FINISH GOOD" Then
            sGroup = "FG"
        ElseIf DDLTypeMat.SelectedItem.Text = "WIP" Then
            sGroup = "WIP"
        End If

        sSql = "SELECT cat1oid, cat1code + ' - ' + cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat1res1='" & sGroup & "' AND ISNULL(cat1res2, '') IN ('WIP', 'Non WIP', '') ORDER BY cat1code"
        If FillDDL(DDLCat01, sSql) Then
            InitDDLCat2()
        Else
            DDLCat02.Items.Clear()
            DDLCat03.Items.Clear()
            DDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat2()
        'Fill DDL Category 2
        Dim sGroup As String = "RAW"
        If DDLTypeMat.SelectedItem.Text = "GENERAL MATERIAL" Then
            sGroup = "GEN"
        ElseIf DDLTypeMat.SelectedItem.Text = "FINISH GOOD" Then
            sGroup = "FG"
        ElseIf DDLTypeMat.SelectedItem.Text = "WIP" Then
            sGroup = "WIP"
        End If

        sSql = "SELECT cat2oid, cat2code + ' - ' + cat2shortdesc FROM QL_mstcat2 c2 INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c1.cat1oid=c2.cat1oid WHERE c2.cmpcode='" & CompnyCode & "' AND c2.activeflag='ACTIVE' AND c1.cat1oid='" & DDLCat01.SelectedValue & "' AND cat2res1='" & sGroup & "' AND cat1res1='" & sGroup & "' AND ISNULL(cat1res2, '') IN ('WIP', 'Non WIP', '') ORDER BY cat2code"
        If FillDDL(DDLCat02, sSql) Then
            InitDDLCat3()
        Else
            DDLCat03.Items.Clear()
            DDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat3()
        'Fill DDL Category 3
        Dim sGroup As String = "RAW"
        If DDLTypeMat.SelectedItem.Text = "GENERAL MATERIAL" Then
            sGroup = "GEN"
        ElseIf DDLTypeMat.SelectedItem.Text = "FINISH GOOD" Then
            sGroup = "FG"
        ElseIf DDLTypeMat.SelectedItem.Text = "WIP" Then
            sGroup = "WIP"
        End If

        sSql = "SELECT cat3oid, cat3code + ' - ' + cat3shortdesc FROM QL_mstcat3 c3 INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c3.cmpcode AND c1.cat1oid=c3.cat1oid WHERE c3.cmpcode='" & CompnyCode & "' AND c3.activeflag='ACTIVE' AND c2.cat2oid='" & DDLCat02.SelectedValue & "' AND cat3res1='" & sGroup & "' AND cat2res1='" & sGroup & "' AND cat1res1='" & sGroup & "' AND ISNULL(cat1res2, '') IN ('WIP', 'Non WIP', '') ORDER BY cat3code"
        If FillDDL(DDLCat03, sSql) Then
            'InitDDLCat4()
        Else
            DDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat4()
        'Fill DDL Category 3
        sSql = "SELECT genoid, gencode+' - '+gendesc FROM QL_mstgen WHERE gengroup='CAT4' AND activeflag='ACTIVE' ORDER BY gencode"
        FillDDL(DDLCat04, sSql)
    End Sub

    Private Sub BindListMat()
        Dim sGroup As String = "RAW"
        If DDLTypeMat.SelectedItem.Text = "GENERAL MATERIAL" Then
            sGroup = "GEN"
        ElseIf DDLTypeMat.SelectedItem.Text = "FINISH GOOD" Then
            sGroup = "FG"
        ElseIf DDLTypeMat.SelectedItem.Text = "WIP" Then
            sGroup = "WIP"
        End If

        sSql = "SELECT " & sTypeMat & "oid AS matoid, " & sTypeMat & "code AS matcode, " & sTypeMat & "longdescription AS matlongdesc, gendesc AS matunit, 'False' AS checkvalue, itemoldcode, itemcat1 AS cat1, itemcat2 AS cat2, itemcat3 AS cat3, itemcat4 AS cat4 FROM QL_mst" & sTypeMat & " m INNER JOIN QL_mstgen g ON genoid=" & sTypeMat & "unit1 WHERE m.cmpcode='" & CompnyCode & "' AND m.itemrecordstatus='ACTIVE' AND m.itemgroupoid='" & DDLTypeMat.SelectedValue & "' ORDER BY " & sTypeMat & "code"
        Session("TblListMat") = cKon.ambiltabel(sSql, "QL_mstmat")
        Session("TblListMatView") = Session("TblListMat")
        gvListMat.DataSource = Session("TblListMatView")
        gvListMat.DataBind()
    End Sub

    Private Sub UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMat") = dt
        End If
        If Session("TblListMatView") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMatView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMatView") = dt
        End If
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select business unit first!", 2)
            Exit Sub
        End If
        If Not IsValidPeriod() Then
            Exit Sub
        End If
        Try
            Dim sRptName As String = "Laporan_Persediaan_Konsinyasi"
            Dim dtReport As DataTable = Nothing
            Dim sWhere As String = " AND cs.cmpcode='" & DDLBusUnit.SelectedValue & "'"
            If lbWarehouse.Items.Count > 0 Then
                Dim sOid As String = ""
                For C1 As Integer = 0 To lbWarehouse.Items.Count - 1
                    sOid &= lbWarehouse.Items(C1).Value & ","
                Next
                If sOid <> "" Then
                    sOid = Left(sOid, sOid.Length - 1)
                    sWhere &= " AND cs.mtrlocoid IN (" & sOid & ")"
                End If
            End If
            Dim sWhereCode As String = ""
            If FilterMaterial.Text <> "" Then
                Dim sMatcode() As String = Split(FilterMaterial.Text, ";")
                If sMatcode.Length > 0 Then
                    For C1 As Integer = 0 To sMatcode.Length - 1
                        If sMatcode(C1) <> "" Then
                            sWhereCode &= "i." & sTypeMat & "code LIKE '" & Tchar(sMatcode(C1)) & "' OR "
                        End If
                    Next
                    If sWhereCode <> "" Then
                        sWhereCode = Left(sWhereCode, sWhereCode.Length - 4)
                        sWhere &= " AND (" & sWhereCode & ")"
                    End If
                End If
            End If
            If cbCat3.Checked And FilterDDLCat3.SelectedValue <> "" Then
                sWhere &= " AND itemcat3='" & FilterDDLCat3.SelectedValue & "'"
            Else
                If cbCat2.Checked And FilterDDLCat2.SelectedValue <> "" Then
                    sWhere &= " AND itemcat2='" & FilterDDLCat2.SelectedValue & "'"
                Else
                    If cbCat1.Checked And FilterDDLCat1.SelectedValue <> "" Then
                        sWhere &= " AND itemcat1='" & FilterDDLCat1.SelectedValue & "'"
                    End If
                End If
            End If
            If DDLType.SelectedValue = "SUMMARY" Then
                report.Load(Server.MapPath(folderReport & "rptStockKonsinyasi_New.rpt"))

                sSql = "select wh_id, wh_name, item_code, item_name, sum(awal) awal, sum(masuk) masuk, sum(keluar) keluar, sum(total_amt) total_amt, isnull(sum(total_amt) / nullif(sum(awal + masuk - keluar), 0.0), 0.0) harga from (select cs.mtrlocoid wh_id, g.gendesc wh_name, i.itemCode item_code, i.itemLongDescription item_name, sum(qtyin - qtyout) awal, 0.0 masuk, 0.0 keluar, 0.0 harga, sum(hpp * (qtyin - qtyout)) total_amt "
                sSql += "from QL_constock cs inner join QL_mstitem i on i.itemoid=cs.refoid inner join QL_mstgen g on g.genoid=cs.mtrlocoid "
                sSql += "where cs.trndate < '" & Format(CDate(FilterPeriod1.Text), "MM/dd/yyyy") & "' /*and cs.mtrlocoid in (select distinct d.transitemtowhoid from QL_trntransitemdtl d inner join QL_trntransitemmst h on h.transitemmstoid=d.transitemmstoid where h.iskonsinyasi='true' and cons_type='TRANSFER')*/ and itemgroup='FG' " & sWhere & " group by cs.mtrlocoid, g.gendesc, i.itemCode, i.itemLongDescription union all "
                sSql += "select cs.mtrlocoid wh_id, g.gendesc wh_name, i.itemCode item_code, i.itemLongDescription item_name, 0.0 awal, (qtyin) masuk, (qtyout) keluar, 0.0 harga, (hpp * (qtyin - qtyout)) total_amt "
                sSql += "from QL_constock cs inner join QL_mstitem i on i.itemoid=cs.refoid inner join QL_mstgen g on g.genoid=cs.mtrlocoid "
                sSql += "where cs.trndate between '" & Format(CDate(FilterPeriod1.Text), "MM/dd/yyyy") & "' and '" & Format(CDate(FilterPeriod2.Text), "MM/dd/yyyy") & "' /*and cs.mtrlocoid in (select distinct d.transitemtowhoid from QL_trntransitemdtl d inner join QL_trntransitemmst h on h.transitemmstoid=d.transitemmstoid where h.iskonsinyasi='true' and cons_type='TRANSFER')*/ and itemgroup='FG' " & sWhere & ") t "
                sSql += "where t.wh_name not in ('GUDANG BARANG JADI', 'GUDANG BARANG RUSAK', 'GUDANG BARANG RETURN') group by wh_id, wh_name, item_code, item_name having (sum(awal)!=0 or sum(masuk) !=0 or sum(keluar)!=0) order by wh_name, item_code"
                dtReport = cKon.ambiltabel(sSql, "QL_stocksummary")
            ElseIf DDLType.SelectedValue = "DETAIL" Then
                report.Load(Server.MapPath(folderReport & "rptStockKonsinyasiDtl_New.rpt"))

                sSql = "select wh_id, wh_name, item_code, item_name, sum(awal) awal, sum(masuk) masuk, sum(keluar) keluar, sum(total_amt) total_amt, isnull(sum(total_amt) / nullif(sum(awal + masuk - keluar), 0.0), 0.0) harga, constockoid, refno, note, trndate from (select cs.mtrlocoid wh_id, g.gendesc wh_name, i.itemCode item_code, i.itemLongDescription item_name, sum(qtyin - qtyout) awal, 0.0 masuk, 0.0 keluar, 0.0 harga, sum(hpp * (qtyin - qtyout)) total_amt, 0 constockoid, '' refno, 'Saldo Awal' note, cast('" & Format(CDate(FilterPeriod1.Text), "MM/dd/yyyy") & "' as date) trndate "
                sSql += "from QL_constock cs inner join QL_mstitem i on i.itemoid=cs.refoid inner join QL_mstgen g on g.genoid=cs.mtrlocoid "
                sSql += "where cs.trndate < '" & Format(CDate(FilterPeriod1.Text), "MM/dd/yyyy") & "' and itemgroup='FG' " & sWhere & " group by cs.mtrlocoid, g.gendesc, i.itemCode, i.itemLongDescription union all "
                sSql += "select cs.mtrlocoid wh_id, g.gendesc wh_name, i.itemCode item_code, i.itemLongDescription item_name, 0.0 awal, (qtyin) masuk, (qtyout) keluar, 0.0 harga, (hpp * (qtyin - qtyout)) total_amt, constockoid, refno, note, trndate "
                sSql += "from QL_constock cs inner join QL_mstitem i on i.itemoid=cs.refoid inner join QL_mstgen g on g.genoid=cs.mtrlocoid "
                sSql += "where cs.trndate between '" & Format(CDate(FilterPeriod1.Text), "MM/dd/yyyy") & "' and '" & Format(CDate(FilterPeriod2.Text), "MM/dd/yyyy") & "' and itemgroup='FG' " & sWhere & ") t "
                sSql += "where t.wh_name not in ('GUDANG BARANG JADI', 'GUDANG BARANG RUSAK', 'GUDANG BARANG RETURN') group by wh_id, wh_name, item_code, item_name, constockoid, refno, note, trndate having (sum(awal)!=0 or sum(masuk) !=0 or sum(keluar)!=0) order by wh_name, item_code, constockoid"

                dtReport = cKon.ambiltabel(sSql, "QL_stockdetail")
            End If
            report.SetDataSource(dtReport)
            report.SetParameterValue("periode", Format(CDate(FilterPeriod1.Text), "dd MMM yyyy") & " - " & Format(CDate(FilterPeriod2.Text), "dd MMM yyyy"))
            cProc.SetDBLogonForReport(report)

            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, sRptName)
                report.Close()
                report.Dispose()
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, sRptName)
                report.Close()
                report.Dispose()
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.ToString, 1)
        End Try
    End Sub

    Private Sub InitFilterDDLCat1()
        'Fill DDL Category 1
        Dim sGroup As String = "RAW"
        If DDLTypeMat.SelectedItem.Text = "GENERAL MATERIAL" Then
            sGroup = "GEN"
        ElseIf DDLTypeMat.SelectedItem.Text = "FINISH GOOD" Then
            sGroup = "FG"
        ElseIf DDLTypeMat.SelectedItem.Text = "WIP" Then
            sGroup = "WIP"
        End If

        sSql = "SELECT cat1oid, cat1code + ' - ' + cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat1res1='" & sGroup & "' AND ISNULL(cat1res2, '') IN ('WIP', 'Non WIP', '') ORDER BY cat1code"
        If FillDDL(FilterDDLCat1, sSql) Then
            InitFilterDDLCat2()
        Else
            FilterDDLCat2.Items.Clear()
            FilterDDLCat3.Items.Clear()
        End If
    End Sub

    Private Sub InitFilterDDLCat2()
        'Fill DDL Category 2
        Dim sGroup As String = "RAW"
        If DDLTypeMat.SelectedItem.Text = "GENERAL MATERIAL" Then
            sGroup = "GEN"
        ElseIf DDLTypeMat.SelectedItem.Text = "FINISH GOOD" Then
            sGroup = "FG"
        ElseIf DDLTypeMat.SelectedItem.Text = "WIP" Then
            sGroup = "WIP"
        End If

        sSql = "SELECT cat2oid, cat2code + ' - ' + cat2shortdesc FROM QL_mstcat2 c2 INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c1.cat1oid=c2.cat1oid WHERE c2.cmpcode='" & CompnyCode & "' AND c2.activeflag='ACTIVE' AND c1.cat1oid='" & FilterDDLCat1.SelectedValue & "' AND cat2res1='" & sGroup & "' AND cat1res1='" & sGroup & "' AND ISNULL(cat1res2, '') IN ('WIP', 'Non WIP', '') ORDER BY cat2code"
        If FillDDL(FilterDDLCat2, sSql) Then
            InitFilterDDLCat3()
        Else
            FilterDDLCat3.Items.Clear()
        End If
    End Sub

    Private Sub InitFilterDDLCat3()
        'Fill DDL Category 3
        Dim sGroup As String = "RAW"
        If DDLTypeMat.SelectedItem.Text = "GENERAL MATERIAL" Then
            sGroup = "GEN"
        ElseIf DDLTypeMat.SelectedItem.Text = "FINISH GOOD" Then
            sGroup = "FG"
        ElseIf DDLTypeMat.SelectedItem.Text = "WIP" Then
            sGroup = "WIP"
        End If

        sSql = "SELECT cat3oid, cat3code + ' - ' + cat3shortdesc FROM QL_mstcat3 c3 INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c3.cmpcode AND c1.cat1oid=c3.cat1oid WHERE c3.cmpcode='" & CompnyCode & "' AND c3.activeflag='ACTIVE' AND c2.cat2oid='" & FilterDDLCat2.SelectedValue & "' AND cat3res1='" & sGroup & "' AND cat2res1='" & sGroup & "' AND cat1res1='" & sGroup & "' AND ISNULL(cat1res2, '') IN ('WIP', 'Non WIP', '') ORDER BY cat3code"
        FillDDL(FilterDDLCat3, sSql)
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmstock2Cons.aspx")
        End If
        If checkPagePermission("~\ReportForm\frmstock2Cons.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Material Stock Konsinyasi (Inc. Value)"
        If Not Page.IsPostBack Then
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitAllDDL()
            InitFilterDDLCat1() : cbCat1.Checked = False : cbCat2.Checked = False : cbCat3.Checked = False
            DDLTypeMat_SelectedIndexChanged(Nothing, Nothing)
            cbAllowNull.Checked = False
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                mpeListMat.Show()
            End If
        End If
    End Sub

    Protected Sub DDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLType.SelectedIndexChanged
        cbAllowNull.Visible = (DDLType.SelectedIndex = 0) : lblGroupBy.Visible = (DDLType.SelectedIndex = 0) : septGroupBy.Visible = (DDLType.SelectedIndex = 0) : DDLGroupBy.Visible = (DDLType.SelectedIndex = 0)
        lblCurrency.Visible = (DDLType.SelectedIndex <> 0) : lblneedcurr.Visible = (DDLType.SelectedIndex <> 0) : ddlcurrency.Visible = (DDLType.SelectedIndex <> 0)
        lblwh.Visible = (DDLType.SelectedIndex <> 1) : lblwhsep.Visible = (DDLType.SelectedIndex <> 1) : lblwh.Visible = (DDLType.SelectedIndex <> 1) : DDLWarehouse.Visible = (DDLType.SelectedIndex <> 1) : btnAddWH.Visible = (DDLType.SelectedIndex <> 1)
        lbWarehouse.Visible = (DDLType.SelectedIndex <> 1) : btnMinWH.Visible = (DDLType.SelectedIndex <> 1)
        FilterPeriod1.Visible = (DDLType.SelectedIndex <> 2) : imbDate1.Visible = (DDLType.SelectedIndex <> 2) : lblto.Visible = (DDLType.SelectedIndex <> 2)
        FilterPeriod2.Visible = (DDLType.SelectedIndex <> 2) : imbDate2.Visible = (DDLType.SelectedIndex <> 2) : lblDateFormat.Visible = (DDLType.SelectedIndex <> 2)
        DDLMonth.Visible = (DDLType.SelectedIndex = 2) : DDLYear.Visible = (DDLType.SelectedIndex = 2)
    End Sub

    Protected Sub btnAddWH_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddWH.Click
        If DDLWarehouse.SelectedValue <> "" Then
            If lbWarehouse.Items.Count > 0 Then
                If Not lbWarehouse.Items.Contains(lbWarehouse.Items.FindByValue(DDLWarehouse.SelectedValue)) Then
                    Dim objList As New ListItem
                    objList.Text = DDLWarehouse.SelectedItem.Text : objList.Value = DDLWarehouse.SelectedValue
                    lbWarehouse.Items.Add(objList)
                End If
            Else
                Dim objList As New ListItem
                objList.Text = DDLWarehouse.SelectedItem.Text : objList.Value = DDLWarehouse.SelectedValue
                lbWarehouse.Items.Add(objList)
            End If
        End If
    End Sub

    Protected Sub btnMinWH_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMinWH.Click
        If lbWarehouse.Items.Count > 0 Then
            Dim objList As ListItem = lbWarehouse.SelectedItem
            lbWarehouse.Items.Remove(objList)
        End If
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : InitDDLCat1()
        cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
        Session("TblListMat") = Nothing : Session("TblListMatView") = Nothing : gvListMat.DataSource = Session("TblListMat") : gvListMat.DataBind()
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Protected Sub btnClearMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearMat.Click
        FilterMaterial.Text = ""
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblListMat") Is Nothing Then
            BindListMat()
        End If
        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                Dim dv As DataView = dt.DefaultView
                Dim sFilter As String = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
                If cbCat04.Checked And DDLCat04.SelectedValue <> "" Then
                    sFilter &= " AND cat4='" & DDLCat04.SelectedValue & "'"
                Else
                    If cbCat03.Checked And DDLCat03.SelectedValue <> "" Then
                        sFilter &= " AND cat3='" & DDLCat03.SelectedValue & "'"
                    Else
                        If cbCat02.Checked And DDLCat02.SelectedValue <> "" Then
                            sFilter &= " AND cat2='" & DDLCat02.SelectedValue & "'"
                        Else
                            If cbCat01.Checked And DDLCat01.SelectedValue <> "" Then
                                sFilter &= " AND cat1='" & DDLCat01.SelectedValue & "'"
                            End If
                        End If
                    End If
                End If
                dv.RowFilter = sFilter
                Session("TblListMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblListMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        If Session("TblListMat") Is Nothing Then
            BindListMat()
        End If
        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : InitDDLCat1()
                cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
                Session("TblListMatView") = Session("TblListMat")
                gvListMat.DataSource = Session("TblListMatView")
                gvListMat.DataBind()
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub DDLCat01_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLCat01.SelectedIndexChanged
        InitDDLCat2()
        mpeListMat.Show()
    End Sub

    Protected Sub DDLCat02_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLCat02.SelectedIndexChanged
        InitDDLCat3()
        mpeListMat.Show()
    End Sub

    Protected Sub DDLCat03_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLCat03.SelectedIndexChanged
        InitDDLCat4()
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        UpdateCheckedMat()
        gvListMat.PageIndex = e.NewPageIndex
        gvListMat.DataSource = Session("TblListMatView")
        gvListMat.DataBind()
        mpeListMat.Show()
    End Sub

    Protected Sub cbHdrLM_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For C1 As Integer = 0 To gvListMat.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        mpeListMat.Show()
    End Sub

    Protected Sub lbAddToListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListMat.Click
        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "checkvalue='True'"
                If dv.Count > 0 Then
                    For C1 As Integer = 0 To dv.Count - 1
                        FilterMaterial.Text &= dv(C1)("matcode").ToString & ";"
                    Next
                    FilterMaterial.Text = Left(FilterMaterial.Text, FilterMaterial.Text.Length - 1)
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                End If
            Else
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmstock2Cons.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not report Is Nothing Then
                If report.IsLoaded Then
                    report.Dispose()
                    report.Close()
                End If
            End If
        Catch ex As Exception
            report.Dispose()
            report.Close()
        End Try
    End Sub

    Protected Sub DDLTypeMat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitFilterDDLCat1()
    End Sub


    Protected Sub FilterDDLCat1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitFilterDDLCat2()
    End Sub

    Protected Sub FilterDDLCat2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitFilterDDLCat3()
    End Sub
#End Region

End Class
