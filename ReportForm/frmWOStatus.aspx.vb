Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_WOStatus
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function UpdateCheckedKIK() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblKIK") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblKIK")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListKIK.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListKIK.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "womstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblKIK") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedKIK2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblKIKView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblKIK")
            Dim dtTbl2 As DataTable = Session("TblKIKView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListKIK.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListKIK.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "womstoid=" & cbOid
                                dtView2.RowFilter = "womstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblKIK") = dtTbl
                Session("TblKIKView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Function FillDDLWithALL(ByRef oDDLObject As DropDownList, ByVal sSql As String) As Boolean
        ' To fill data in dropdown list, return true when all data exist
        FillDDLWithALL = True
        oDDLObject.Items.Clear()
        oDDLObject.Items.Add("ALL")
        oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = "ALL"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            oDDLObject.Items.Add(xreader.GetValue(1))
            oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()
        ' Cek if all data exits
        If oDDLObject.Items.Count = 0 Then
            FillDDLWithALL = False
        End If
        Return FillDDLWithALL
    End Function

    Private Sub BindListKIK()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, wom.womstoid AS womstoid, wom.wono AS wono, CONVERT(VARCHAR(10), wom.wodate, 101) AS wodate, wom.womstnote AS womstnote, som.sono AS soitemno FROM QL_trnwomst wom INNER JOIN QL_trnwodtl1 wod1 ON wom.womstoid=wod1.womstoid INNER JOIN QL_mstitem m ON m.itemoid=wod1.itemoid INNER JOIN QL_trnsomst som ON som.somstoid=wod1.soitemmstoid "

        sSql &= " WHERE wom.cmpcode='" & DDLBusUnit.SelectedValue & "'"

        If DDLDivision.SelectedValue <> "ALL" Then
            sSql &= " AND som.groupoid=" & DDLDivision.SelectedValue
        End If

        'If IsValidPeriod() Then
        '    sSql &= " AND wom.wodate>='" & FilterPeriod1.Text & " 00:00:00' AND wom.wodate<='" & FilterPeriod2.Text & " 23:59:59'"
        'Else
        '    Exit Sub
        'End If
        sSql &= " AND wom.womststatus <> 'Cancel'"
        sSql &= " ORDER BY wodate DESC, womstoid DESC"
        Session("TblKIK") = cKon.ambiltabel(sSql, "QL_trnwomst")
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        Dim sWhere As String = ""
        Dim rptName As String = ""
        Dim Dtl As String = ""
        Dim Join As String = ""
        Dim sDate As String = ""
        Dim sDate2 As String = ""
        Try
            If sType = "Print Excel" Then
                report.Load(Server.MapPath(folderReport & "rptWOStatusXls.rpt"))
            Else
                report.Load(Server.MapPath(folderReport & "rptWOStatus.rpt"))
            End If
            rptName = "JobCostingMOStatus"

            If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
                If IsValidPeriod() Then
                    sDate = FilterPeriod1.Text
                    sDate2 = FilterPeriod2.Text
                Else
                    Exit Sub
                End If
            End If

            Dim arType() As String = {""}
            If FilterDDLType.SelectedValue = "Summary" Or FilterDDLType.SelectedValue = "Detail" Then
                sSql = "  SELECT wom.cmpcode,(SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=wom.cmpcode) [BUSSINESS UNIT],wom.womstoid AS [Oid], wom.wono AS [KIK No.], wom.womststatus [Status], [Ref No], GETDATE() [Ref Date], 0.0 [DM IDR], 0.0 [DM USD], 0.0 [DL IDR], 0.0 [DL USD], 0.0 [FOH IDR], 0.0 [FOH USD], 0.0 [Confirm Qty], 0.0 [Confirm IDR], 0.0 [Confirm USD], itemcode [Code], itemlongdesc [Finish Good], wodtl1qty [Finish Good Qty], gendesc [Unit], Division, groupoid, woclosingdate [Tgl Closed], '' [Code_Mat], '' [Description], 0.0 [Qty], 0.0 [Value IDR], 0.0 [Value USD], 0.0 [Amount IDR], 0.0 [Amount USD], '' [Unit_Mat] " & _
                " FROM QL_trnwomst wom INNER JOIN (SELECT DISTINCT wod1.cmpcode, wod1.womstoid, itemcode, itemLongDescription AS itemlongdesc, ISNULL((SELECT SUM(wodx.wodtl1qty) FROM QL_trnwodtl1 wodx WHERE wodx.womstoid=wod1.womstoid),0.0) AS wodtl1qty, wod1.wodtl1unitoid, g.gendesc, 0 AS groupoid, '' AS Division FROM QL_trnwodtl1 wod1 INNER JOIN QL_mstitem m ON wod1.itemoid=m.itemoid INNER JOIN QL_mstgen g ON g.genoid=wod1.wodtl1unitoid) AS tblItem ON tblItem.cmpcode=wom.cmpcode AND tblItem.womstoid=wom.womstoid " & _
                " LEFT JOIN (SELECT cmpcode, womstoid, [Ref No], [Ref Date], ISNULL(SUM([DM IDR]), 0.0) [DM IDR] , ISNULL(SUM([DM USD]), 0.0) [DM USD], ISNULL(SUM([DL IDR]), 0.0) [DL IDR] , ISNULL(SUM([DL USD]), 0.0) [DL USD], ISNULL(SUM([FOH IDR]), 0.0) [FOH IDR] , ISNULL(SUM([FOH USD]), 0.0) [FOH USD], ISNULL(SUM([Confirm Qty]),0.0) [Confirm Qty],ISNULL(SUM([Confirm IDR]), 0.0) [Confirm IDR],ISNULL(SUM([Confirm USD]), 0.0) [Confirm USD], [Code_Mat], [Description], [Qty], [Value IDR], [Value USD], [Amount IDR],[Amount USD], [Unit_Mat] FROM ("
                'MATERIAL REQUEST
                sSql &= " SELECT reqm.cmpcode, reqm.womstoid, (CASE WHEN ISNULL(reqno,'')='' THEN 'Request Draft No.'+(CAST (reqmstoid AS VARCHAR(10))) ELSE reqno END) [Ref No], reqm.updtime [Ref Date], 0.0 [DM IDR], 0.0 [DM USD] , 0.0 AS [DL IDR], 0.0 AS [DL USD], 0.0 AS [FOH IDR], 0.0 AS [FOH USD], 0.0 [Confirm Qty] ,0.0 [Confirm IDR],0.0 [Confirm USD], '' AS [Code_Mat], '' AS [Description], 0.0 AS [Qty], 0.0 AS [Value IDR], 0.0 AS [Value USD], 0.0 AS [Amount IDR], 0.0 AS [Amount USD], '' AS [Unit_Mat] FROM QL_trnreqmst reqm WHERE reqm.reqmstoid>0 GROUP BY reqm.cmpcode, reqm.womstoid, reqno , reqm.updtime, reqm.reqmstoid UNION ALL"
                'MATERIAL USAGE
                sSql &= " SELECT mud.cmpcode, reqm.womstoid, (CASE WHEN ISNULL(usageno,'')='' THEN 'Usage Draft No.'+(CAST (mum.usagemstoid AS VARCHAR(10))) ELSE usageno END) [Ref No], mum.updtime [Ref Date], 0.0 [DM IDR], 0.0 [DM USD] , 0.0 AS [DL IDR], 0.0 AS [DL USD], 0.0 AS [FOH IDR], 0.0 AS [FOH USD], 0.0 [Confirm Qty] ,0.0 [Confirm IDR],0.0 [Confirm USD], '' AS [Code_Mat], '' AS [Description], 0.0 AS [Qty], 0.0 AS [Value IDR], 0.0 AS [Value USD], 0.0 AS [Amount IDR], 0.0 AS [Amount USD], '' AS [Unit_Mat] FROM QL_trnusagedtl mud INNER JOIN QL_trnusagemst mum ON mum.cmpcode=mud.cmpcode AND mum.usagemstoid=mud.usagemstoid INNER JOIN QL_trnreqmst reqm ON reqm.cmpcode=mud.cmpcode AND reqm.reqmstoid=mud.reqmstoid WHERE mum.usagemstoid>0 GROUP BY mud.cmpcode, reqm.womstoid, usageno , mum.updtime, mum.usagemstoid UNION ALL"
                'BERITA ACARA USAGE
                sSql &= " SELECT bam.cmpcode, bam.womstoid, (CASE WHEN ISNULL(bam.acarano,'')='' THEN 'BA Draft No.'+(CAST (bam.acaramstoid AS VARCHAR(10))) ELSE acarano END) [Ref No], bam.approvaldatetime [Ref Date], 0.0 [DM IDR], 0.0 [DM USD] , 0.0 AS [DL IDR], 0.0 AS [DL USD], 0.0 AS [FOH IDR], 0.0 AS [FOH USD], 0.0 [Confirm Qty],0.0 [Confirm IDR],0.0 [Confirm USD], '' AS [Code_Mat], '' AS [Description], 0.0 AS [Qty], 0.0 AS [Value IDR], 0.0 AS [Value USD], 0.0 AS [Amount IDR], 0.0 AS [Amount USD], '' AS [Unit_Mat] FROM QL_trnbrtacaramst bam WHERE bam.acaramstoid>0 GROUP BY bam.cmpcode, bam.womstoid, bam.acarano, bam.approvaldatetime, bam.acaramstoid UNION ALL"
                'PRODUCTION RESULT
                sSql &= " SELECT resd.cmpcode, resd.womstoid, (CASE WHEN ISNULL(prodresno,'')='' THEN 'ProdRes Draft No.'+(CAST (resm.prodresmstoid AS VARCHAR(10))) ELSE prodresno END) [Ref No], resm.updtime [Ref Date], 0.0 AS [DM IDR], 0.0 AS [DM USD], 0.0 [DL IDR], 0.0 [DL USD], 0.0 AS [FOH IDR], 0.0 AS [FOH USD], 0.0 [Confirm Qty],0.0 [Confirm IDR],0.0 [Confirm USD], '' AS [Code_Mat], '' AS [Description], 0.0 AS [Qty], 0.0 AS [Value IDR], 0.0 AS [Value USD], 0.0 AS [Amount IDR], 0.0 AS [Amount USD], '' AS [Unit_Mat] FROM QL_trnprodresdtl resd INNER JOIN QL_trnprodresmst resm ON resm.cmpcode=resd.cmpcode AND resm.prodresmstoid=resd.prodresmstoid WHERE resm.prodresmstoid>0 GROUP BY resd.cmpcode, resd.womstoid, prodresno, resm.updtime, resm.prodresmstoid UNION ALL"
                'PRODUCTION RESULT CONF
                sSql &= " SELECT conf.cmpcode, conf.womstoid, 'CONFIRM:' + prodresno [Ref No], conf.updtime [Ref Date], 0.0 AS [DM IDR], 0.0 AS [DM USD], 0.0 AS [DL IDR], 0.0 AS [DL USD], 0.0 AS [FOH IDR], 0.0 AS [FOH USD], SUM(confirmqty) [Confirm Qty], SUM(confirmvalueidr * confirmqty) [Confirm IDR], SUM(confirmvalueusd * confirmqty) [Confirm USD], '' AS [Code_Mat], '' AS [Description], 0.0 AS [Qty], 0.0 AS [Value IDR], 0.0 AS [Value USD], 0.0 AS [Amount IDR], 0.0 AS [Amount USD], '' AS [Unit_Mat] FROM QL_trnprodresconfirm conf INNER JOIN QL_trnprodresmst resm ON resm.cmpcode=conf.cmpcode AND resm.prodresmstoid=conf.prodresmstoid GROUP BY conf.cmpcode, conf.womstoid,prodresno, conf.updtime "
                sSql &= " ) AS tbl_Tmp "
                sSql &= " GROUP BY cmpcode, womstoid,[Ref Date],[Ref No], [Code_Mat], [Description], [Qty], [Value IDR], [Value USD], [Amount IDR], [Amount USD], [Unit_Mat]) as tblALL ON tblALL.cmpcode=wom.cmpcode AND tblALL.womstoid=wom.womstoid "
                sSql &= " WHERE wom.cmpcode='" & DDLBusUnit.SelectedValue & "'"
                If wono.Text <> "" Then
                    Dim sResNo() As String = Split(wono.Text, ";")
                    sSql &= " AND ("
                    For C1 As Integer = 0 To sResNo.Length - 1
                        sSql &= " wono LIKE '" & Tchar(sResNo(C1)) & "'"
                        If C1 < sResNo.Length - 1 Then
                            sSql &= " OR"
                        End If
                    Next
                    sSql &= ")"
                End If
                If DDLDivision.SelectedValue <> "ALL" Then
                    sSql &= " AND groupoid=" & DDLDivision.SelectedValue
                End If
                sSql &= " AND womststatus<>'Cancel'"
                sSql &= " AND wom.wodate>=CAST('" & FilterPeriod1.Text & " 00:00:00' AS DATETIME)"
                sSql &= " AND wom.wodate<=CAST('" & FilterPeriod2.Text & " 23:59:59' AS DATETIME)"
                If Not cbAllowNull.Checked Then
                    sSql &= " AND (ISNULL([DM IDR],0)<>0 OR ISNULL([DM USD],0)<>0 OR ISNULL([DL IDR],0)<>0 OR ISNULL([DL USD],0)<>0 OR ISNULL([FOH IDR],0)<>0 OR ISNULL([FOH USD],0)<>0 OR ISNULL([Confirm IDR],0)<>0 OR ISNULL([Confirm USD],0)<>0) "
                End If
                sSql &= " ORDER BY [KIK No.],Oid,[Ref Date]"

            End If
            Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstKIK")
            Dim dvTbl As DataView = dtTbl.DefaultView

            report.SetDataSource(dvTbl.ToTable)

            report.SetParameterValue("StartPeriod", Format(CDate(FilterPeriod1.Text), "dd MMM yyyy"))
            report.SetParameterValue("EndPeriod", Format(CDate(FilterPeriod2.Text), "dd MMM yyyy"))
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            If sType = "Print Excel" Then
                report.PrintOptions.PaperSize = PaperSize.PaperA4
            Else
                'report.PrintOptions.PaperSize = PaperSize.PaperFolio
            End If
            cProc.SetDBLogonForReport(report)

            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, rptName)
                report.Close()
                report.Dispose()
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, rptName)
                report.Close()
                report.Dispose()
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message & sSql, 1)
        End Try
    End Sub

    Private Sub InitAllDDL()
        ' DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        FillDDL(DDLBusUnit, sSql)
        If DDLBusUnit.Items.Count > 0 Then
            InitDDLDivision(DDLBusUnit.SelectedValue)
        End If
    End Sub

    Private Sub InitDDLDivision(ByVal sDiv As String)
        ' Init DDL Division
        sSql = "SELECT groupoid, groupcode+' - '+groupdesc FROM QL_mstdeptgroup WHERE activeflag='ACTIVE'"
        If sDiv <> "ALL" Then
            sSql &= " AND cmpcode='" & sDiv & "'"
            FillDDLWithALL(DDLDivision, sSql)
        Else
            sSql = "SELECT groupoid, groupcode+' - '+groupdesc FROM QL_mstdeptgroup dept INNER JOIN QL_mstdivision div ON  div.cmpcode=dept.cmpcode WHERE div.activeflag='ACTIVE' AND dept.activeflag='ACTIVE'"
            FillDDLWithALL(DDLDivision, sSql)
        End If
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If

        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmWOStatus.aspx")

        End If

        If checkPagePermission("~\ReportForm\frmWOStatus.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        Page.Title = CompnyName & " - Job Costing MO Status"
        If Not Page.IsPostBack Then
            InitAllDDL()
            cbAllowNull.Checked = True : cbAllowNull.Visible = False
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListKIK") Is Nothing And Session("EmptyListKIK") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListKIK") Then
                Session("EmptyListKIK") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListKIK, PanelListKIK, mpeListKIK, True)
            End If
        End If
        If Not Session("WarningListKIK") Is Nothing And Session("WarningListKIK") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListKIK") Then
                Session("WarningListKIK") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListKIK, PanelListKIK, mpeListKIK, True)
            End If
        End If

    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        imbEraseKIK_Click(Nothing, Nothing)
        InitDDLDivision(DDLBusUnit.SelectedValue)
    End Sub

    Protected Sub FilterDDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLType.SelectedIndexChanged
        If FilterDDLType.SelectedValue = "Summary" Then
            lbldate.Text = "Production Period"
        Else
            lbldate.Text = "SPK Date"
        End If
    End Sub

    Protected Sub imbFindKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindKIK.Click
        If IsValidPeriod() Then
            DDLFilterListKIK.SelectedIndex = -1 : txtFilterListKIK.Text = ""
            Session("TblKIK") = Nothing : Session("TblKIKView") = Nothing : gvListKIK.DataSource = Nothing : gvListKIK.DataBind()
            cProc.SetModalPopUpExtender(btnHiddenListKIK, PanelListKIK, mpeListKIK, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbEraseKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseKIK.Click
        wono.Text = ""
    End Sub

    Protected Sub btnFindListKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListKIK.Click
        If Session("TblKIK") Is Nothing Then
            BindListKIK()
            If Session("TblKIK").Rows.Count <= 0 Then
                Session("EmptyListKIK") = "KIK data can't be found!"
                showMessage(Session("EmptyListKIK"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListKIK.SelectedValue & " LIKE '%" & Tchar(txtFilterListKIK.Text) & "%'"
        If UpdateCheckedKIK() Then
            Dim dv As DataView = Session("TblKIK").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblKIKView") = dv.ToTable
                gvListKIK.DataSource = Session("TblKIKView")
                gvListKIK.DataBind()
                dv.RowFilter = ""
                mpeListKIK.Show()
            Else
                dv.RowFilter = ""
                Session("TblKIKView") = Nothing
                gvListKIK.DataSource = Session("TblKIKView")
                gvListKIK.DataBind()
                Session("WarningListKIK") = "KIK data can't be found!"
                showMessage(Session("WarningListKIK"), 2)
            End If
        Else
            mpeListKIK.Show()
        End If
    End Sub

    Protected Sub btnViewAllListKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListKIK.Click
        DDLFilterListKIK.SelectedIndex = -1 : txtFilterListKIK.Text = ""
        If Session("TblKIK") Is Nothing Then
            BindListKIK()
            If Session("TblKIK").Rows.Count <= 0 Then
                Session("EmptyListKIK") = "KIK data can't be found!"
                showMessage(Session("EmptyListKIK"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedKIK() Then
            Dim dt As DataTable = Session("TblKIK")
            Session("TblKIKView") = dt
            gvListKIK.DataSource = Session("TblKIKView")
            gvListKIK.DataBind()
        End If
        mpeListKIK.Show()
    End Sub

    Protected Sub btnSelectAllKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllKIK.Click
        If Not Session("TblKIKView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblKIKView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblKIK")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "womstoid=" & dtTbl.Rows(C1)("womstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblKIK") = objTbl
                Session("TblKIKView") = dtTbl
                gvListKIK.DataSource = Session("TblKIKView")
                gvListKIK.DataBind()
            End If
            mpeListKIK.Show()
        Else
            Session("WarningListKIK") = "Please show some KIK data first!"
            showMessage(Session("WarningListKIK"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneKIK.Click
        If Not Session("TblKIKView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblKIKView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblKIK")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "womstoid=" & dtTbl.Rows(C1)("womstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblKIK") = objTbl
                Session("TblKIKView") = dtTbl
                gvListKIK.DataSource = Session("TblKIKView")
                gvListKIK.DataBind()
            End If
            mpeListKIK.Show()
        Else
            Session("WarningListKIK") = "Please show some KIK data first!"
            showMessage(Session("WarningListKIK"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedKIK.Click
        If Session("TblKIK") Is Nothing Then
            Session("WarningListKIK") = "Selected KIK data can't be found!"
            showMessage(Session("WarningListKIK"), 2)
            Exit Sub
        End If
        If UpdateCheckedKIK() Then
            Dim dtTbl As DataTable = Session("TblKIK")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListKIK.SelectedIndex = -1 : txtFilterListKIK.Text = ""
                Session("TblKIKView") = dtView.ToTable
                gvListKIK.DataSource = Session("TblKIKView")
                gvListKIK.DataBind()
                dtView.RowFilter = ""
                mpeListKIK.Show()
            Else
                dtView.RowFilter = ""
                Session("TblKIKView") = Nothing
                gvListKIK.DataSource = Session("TblKIKView")
                gvListKIK.DataBind()
                Session("WarningListKIK") = "Selected KIK data can't be found!"
                showMessage(Session("WarningListKIK"), 2)
            End If
        Else
            mpeListKIK.Show()
        End If
    End Sub

    Protected Sub gvListKIK_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListKIK.PageIndexChanging
        If UpdateCheckedKIK2() Then
            gvListKIK.PageIndex = e.NewPageIndex
            gvListKIK.DataSource = Session("TblKIKView")
            gvListKIK.DataBind()
        End If
        mpeListKIK.Show()
    End Sub

    Protected Sub lkbAddToListListKIK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListKIK.Click
        If Not Session("TblKIK") Is Nothing Then
            If UpdateCheckedKIK() Then
                Dim dtTbl As DataTable = Session("TblKIK")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If wono.Text <> "" Then
                            If dtView(C1)("wono") <> "" Then
                                wono.Text &= ";" + vbCrLf + dtView(C1)("wono")
                            End If
                        Else
                            If dtView(C1)("wono") <> "" Then
                                wono.Text &= dtView(C1)("wono")
                            End If
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHiddenListKIK, PanelListKIK, mpeListKIK, False)
                Else
                    Session("WarningListKIK") = "Please select KIK to add to list!"
                    showMessage(Session("WarningListKIK"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListKIK") = "Please show some KIK data first!"
            showMessage(Session("WarningListKIK"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListKIK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListKIK.Click
        cProc.SetModalPopUpExtender(btnHiddenListKIK, PanelListKIK, mpeListKIK, False)
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmWOStatus.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Dispose()
        report.Close()
    End Sub
#End Region

End Class
