Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_PaymentAR
	Inherits System.Web.UI.Page

#Region "Variables"
	Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
	Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
	Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
	Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
	Dim xCmd As New SqlCommand("", conn)
	Dim xreader As SqlDataReader
	Dim sSql As String = ""
	Dim cKon As New Koneksi
	Dim cProc As New ClassProcedure
	Dim report As New ReportDocument
	Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
	Private Function IsValidPeriod() As Boolean
		Dim sErr As String = ""
		If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
			showMessage("Your Period 1 is invalid. " & sErr, 2)
			Return False
		End If
		If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
			showMessage("Your Period 2 is invalid. " & sErr, 2)
			Return False
		End If
		If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
			showMessage("Period 2 must be more than Period 1 !", 2)
			Return False
		End If
		Return True
	End Function
#End Region

#Region "Procedures"
	Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
		Dim strCaption As String = CompnyName
		If iType = 1 Then ' Error
			imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
		ElseIf iType = 2 Then ' Warning
			imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
		ElseIf iType = 3 Then ' Information
			imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
		Else
			imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
		End If
		lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
		cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
	End Sub

    Private Sub InitDDLPayAccount()
        Dim sVar As String = "VAR_CASH"
        Dim bVal As Boolean = False
        Dim bVal2 As Boolean = True
        Dim bVal3 As Boolean = False
        Dim sCss As String = "inpText"
        If DDLPayType.SelectedValue = "BBM" Then
            sVar = "VAR_BANK" : bVal = True : bVal3 = True
        ElseIf DDLPayType.SelectedValue = "BGM" Then
            sVar = "VAR_BANK" : bVal = True : bVal3 = True
        ElseIf DDLPayType.SelectedValue = "BLM" Then
            sVar = "VAR_DP_AR"
        End If
        FillDDLAcctg(DDLPayAccount, sVar, DDLBusUnit.SelectedValue)
    End Sub

	Private Sub BindListNo()
        sSql = "SELECT 'False' AS checkvalue, cashbankoid, cashbankno, acctgdesc, CONVERT(VARCHAR(10), cbm.createtime, 101) AS createdate, cashbanknote FROM QL_trncashbankmst cbm INNER JOIN QL_mstacctg a ON a.acctgoid=cbm.acctgoid WHERE cbm.cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankgroup ='AR' AND cbm.cashbankoid > 0"
		If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
			sSql &= " AND " & FilterDDLDate.SelectedValue & " BETWEEN CAST('" & FilterPeriod1.Text & " 00:00:00' AS DATETIME) AND CAST('" & FilterPeriod2.Text & " 23:59:59' AS DATETIME)"
		End If
		If DDLPayType.SelectedIndex <> 0 Then
			sSql &= " AND cashbanktype='" & DDLPayType.SelectedValue & "'"
		End If
		If lbPayAccount.Items.Count > 0 Then
			Dim sOid As String = ""
			For C1 As Integer = 0 To lbPayAccount.Items.Count - 1
				sOid &= lbPayAccount.Items(C1).Value & ","
			Next
			If sOid <> "" Then
				sSql &= " AND cbm.acctgoid IN (" & Left(sOid, sOid.Length - 1) & ")"
			End If
		End If
		sSql &= " ORDER BY cashbankno"
		Session("TblListNo") = cKon.ambiltabel(sSql, "QL_trncashbankmst")
		gvListNo.DataSource = Session("TblListNo") : gvListNo.DataBind()
	End Sub

	Private Sub UpdateCheckedListNo(Optional ByVal sView As String = "")
		If Session("Tbl" & sView & "ListNo") IsNot Nothing Then
			For C1 As Integer = 0 To gvListNo.Rows.Count - 1
				Dim row As System.Web.UI.WebControls.GridViewRow = gvListNo.Rows(C1)
				If (row.RowType = DataControlRowType.DataRow) Then
					Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
					For Each myControl As System.Web.UI.Control In cc
						If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
							Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
							Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
							Session("Tbl" & sView & "ListNo").DefaultView.RowFilter = "cashbankoid=" & sOid
							If cbcheck = True Then
								If Session("Tbl" & sView & "ListNo").DefaultView.Count > 0 Then
									Session("Tbl" & sView & "ListNo").DefaultView(0)("checkvalue") = "True"
								End If
							Else
								If Session("Tbl" & sView & "ListNo").DefaultView.Count > 0 Then
									Session("Tbl" & sView & "ListNo").DefaultView(0)("checkvalue") = "False"
								End If
							End If
							Session("Tbl" & sView & "ListNo").DefaultView.RowFilter = ""
						End If
					Next
				End If
			Next
		End If
	End Sub

	Private Sub BindListSupp()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, s.custoid AS suppoid,s.custcode AS suppcode,s.custname as suppname,s.custaddr AS suppaddr FROM QL_trncashbankmst cbm INNER JOIN QL_mstcust s ON s.custoid=personoid WHERE cbm.cmpcode='" & CompnyCode & "' AND cashbankgroup LIKE 'AR%' AND cbm.cashbankoid > 0"
		If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
			sSql &= " AND " & FilterDDLDate.SelectedValue & " BETWEEN CAST('" & FilterPeriod1.Text & " 00:00:00' AS DATETIME) AND CAST('" & FilterPeriod2.Text & " 23:59:59' AS DATETIME)"
		End If
		If DDLPayType.SelectedIndex <> 0 Then
			sSql &= " AND cashbanktype='" & DDLPayType.SelectedValue & "'"
		End If
		If lbPayAccount.Items.Count > 0 Then
			Dim sOid As String = ""
			For C1 As Integer = 0 To lbPayAccount.Items.Count - 1
				sOid &= lbPayAccount.Items(C1).Value & ","
			Next
			If sOid <> "" Then
				sSql &= " AND cbm.acctgoid IN (" & Left(sOid, sOid.Length - 1) & ")"
			End If
		End If
		If FilterTextNo.Text <> "" Then
			sSql &= " AND ("
			Dim arNo() As String = FilterTextNo.Text.Split(";")
			Dim sNo As String = ""
			For C1 As Integer = 0 To arNo.Length - 1
				If arNo(C1) <> "" Then
					sNo &= "cashbankno LIKE '" & arNo(C1) & "' OR "
				End If
			Next
			sSql &= Left(sNo, sNo.Length - 4) & ")"
		End If
		sSql &= " ORDER BY suppcode"
		Session("TblListSupp") = cKon.ambiltabel(sSql, "QL_mstsupp")
		gvListSupp.DataSource = Session("TblListSupp") : gvListSupp.DataBind()
	End Sub

	Private Sub UpdateCheckedListSupp(Optional ByVal sView As String = "")
		If Session("Tbl" & sView & "ListSupp") IsNot Nothing Then
			For C1 As Integer = 0 To gvListSupp.Rows.Count - 1
				Dim row As System.Web.UI.WebControls.GridViewRow = gvListSupp.Rows(C1)
				If (row.RowType = DataControlRowType.DataRow) Then
					Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
					For Each myControl As System.Web.UI.Control In cc
						If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
							Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
							Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
							Session("Tbl" & sView & "ListSupp").DefaultView.RowFilter = "suppoid=" & sOid
							If cbcheck = True Then
								If Session("Tbl" & sView & "ListSupp").DefaultView.Count > 0 Then
									Session("Tbl" & sView & "ListSupp").DefaultView(0)("checkvalue") = "True"
								End If
							Else
								If Session("Tbl" & sView & "ListSupp").DefaultView.Count > 0 Then
									Session("Tbl" & sView & "ListSupp").DefaultView(0)("checkvalue") = "False"
								End If
							End If
							Session("Tbl" & sView & "ListSupp").DefaultView.RowFilter = ""
						End If
					Next
				End If
			Next
		End If
	End Sub

	Private Sub BindListAP()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, apm.trnjualmstoid AS apmstoid, apm.trnjualno AS apno, CONVERT(VARCHAR(10), apm.trnjualdate, 101) AS apdate, apm.trnjualnote AS apmstnote FROM QL_trncashbankmst cbm INNER JOIN QL_trnpayar par ON par.cmpcode=cbm.cmpcode AND par.cashbankoid=cbm.cashbankoid INNER JOIN QL_trnjualmst apm ON apm.cmpcode=par.cmpcode AND apm.trnjualmstoid=par.refoid INNER JOIN QL_mstcust s ON s.custoid=apm.custoid WHERE cbm.cmpcode='" & CompnyCode & "' AND cashbankgroup LIKE 'AR%' AND cbm.cashbankoid > 0 AND par.reftype='QL_trnjualmst'"

		If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
			sSql &= " AND " & FilterDDLDate.SelectedValue & " BETWEEN CAST('" & FilterPeriod1.Text & " 00:00:00' AS DATETIME) AND CAST('" & FilterPeriod2.Text & " 23:59:59' AS DATETIME)"
		End If
		If DDLPayType.SelectedIndex <> 0 Then
			sSql &= " AND cashbanktype='" & DDLPayType.SelectedValue & "'"
		End If
		If lbPayAccount.Items.Count > 0 Then
			Dim sOid As String = ""
			For C1 As Integer = 0 To lbPayAccount.Items.Count - 1
				sOid &= lbPayAccount.Items(C1).Value & ","
			Next
			If sOid <> "" Then
				sSql &= " AND cbm.acctgoid IN (" & Left(sOid, sOid.Length - 1) & ")"
			End If
		End If
		If FilterTextNo.Text <> "" Then
			sSql &= " AND ("
			Dim arNo() As String = FilterTextNo.Text.Split(";")
			Dim sNo As String = ""
			For C1 As Integer = 0 To arNo.Length - 1
				If arNo(C1) <> "" Then
					sNo &= "cashbankno LIKE '" & arNo(C1) & "' OR "
				End If
			Next
			sSql &= Left(sNo, sNo.Length - 4) & ")"
		End If
		If FilterTextSupp.Text <> "" Then
			sSql &= " AND ("
			Dim arSupp() As String = FilterTextSupp.Text.Split(";")
			Dim sSupp As String = ""
			For C1 As Integer = 0 To arSupp.Length - 1
				If arSupp(C1) <> "" Then
                    sSupp &= "s.custcode LIKE '" & arSupp(C1) & "' OR "
				End If
			Next
			sSql &= Left(sSupp, sSupp.Length - 4) & ")"
		End If
		sSql &= " ORDER BY apno"
		Session("TblListAP") = cKon.ambiltabel(sSql, "QL_mstsupp")
		gvListAP.DataSource = Session("TblListAP") : gvListAP.DataBind()
	End Sub

	Private Sub UpdateCheckedListAP(Optional ByVal sView As String = "")
		If Session("Tbl" & sView & "ListAP") IsNot Nothing Then
			For C1 As Integer = 0 To gvListAP.Rows.Count - 1
				Dim row As System.Web.UI.WebControls.GridViewRow = gvListAP.Rows(C1)
				If (row.RowType = DataControlRowType.DataRow) Then
					Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
					For Each myControl As System.Web.UI.Control In cc
						If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
							Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
							Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
							Session("Tbl" & sView & "ListAP").DefaultView.RowFilter = "apmstoid=" & sOid
							If cbcheck = True Then
								If Session("Tbl" & sView & "ListAP").DefaultView.Count > 0 Then
									Session("Tbl" & sView & "ListAP").DefaultView(0)("checkvalue") = "True"
								End If
							Else
								If Session("Tbl" & sView & "ListAP").DefaultView.Count > 0 Then
									Session("Tbl" & sView & "ListAP").DefaultView(0)("checkvalue") = "False"
								End If
							End If
							Session("Tbl" & sView & "ListAP").DefaultView.RowFilter = ""
						End If
					Next
				End If
			Next
		End If
	End Sub

	Private Sub ShowReport(ByVal sType As String, Optional ByVal sRptType As String = "Pdf")
        If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
            If Not IsValidPeriod() Then
                Exit Sub
            End If
        End If
		Try
			Dim sRptName As String = ""
			If DDLType.SelectedValue = "SUMMARY" Then
                report.Load(Server.MapPath(folderReport & "rptPayARSum.rpt"))
                sRptName = "PayARSumReport"
			Else
                report.Load(Server.MapPath(folderReport & "rptPayARDtl.rpt"))
                sRptName = "PayARDtlReport"
			End If
            sSql = "SELECT cbm.cmpcode, ( SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=cbm.cmpcode ) AS [Business Unit], cbm.cashbankoid AS [ID Hdr], cashbankno AS [Cash/Bank No.], cashbankdate AS [Payment Date], s.custname AS [Supplier], custcode AS [Supplier Code], currcode [Currency], currsymbol [Curr Symbol], (CASE cashbanktype WHEN 'BKM' THEN 'CASH' WHEN 'BBM' THEN 'TRANSFER' WHEN 'BGM' THEN 'GIRO/CHEQUE' ELSE 'DOWN PAYMENT' END) AS [Payment Type], ('(' + a.acctgcode + ') ' + a.acctgdesc) AS [Payment Account], cashbankrefno [Ref. No.], cashbankduedate [Due Date], cashbanknote [Header Note], cashbanktakegiro [Date Take Giro], cashbankstatus [Status], cbm.createuser [Create User], cbm.createtime [Create Datetime], cbm.upduser [Last Upd. User], cbm.updtime [Last Upd. Datetime], (CASE cashbankstatus WHEN 'In Process' THEN '' ELSE cbm.upduser END) AS [Posting User], (CASE cashbankstatus WHEN 'In Process' THEN CAST('01/01/1900' AS DATETIME) ELSE cbm.updtime END) AS [Posting Datetime], ISNULL((SELECT '(' + ax.acctgcode + ') ' + ax.acctgdesc FROM QL_mstacctg ax WHERE ax.acctgoid=addacctgoid1), '') AS [COA Add. Cost 1], ISNULL((SELECT '(' + ax.acctgcode + ') ' + ax.acctgdesc FROM QL_mstacctg ax WHERE ax.acctgoid=addacctgoid2), '') AS [COA Add. Cost 2], ISNULL((SELECT '(' + ax.acctgcode + ') ' + ax.acctgdesc FROM QL_mstacctg ax WHERE ax.acctgoid=addacctgoid3), '') AS [COA Add. Cost 3], (cashbankamt - addacctgamt1 - addacctgamt2 - addacctgamt3) AS [Total Payment], addacctgamt1 AS [Amount Add. Cost 1], addacctgamt2 AS [Amount Add. Cost 2], addacctgamt3 AS [Amount Add. Cost 3], cashbankamt [Total Payment + Cost], (CASE cashbanktype WHEN 'BGM' THEN (CASE ISNULL(cashbankres1, '') WHEN '' THEN 'Giro belum diambil' WHEN 'Post' THEN 'Giro sudah diambil tapi belum cair' ELSE 'Giro sudah diambil dan sudah cair' END) ELSE '' END) AS [Giro Status], cashbanktakegiroreal AS [Date Take Giro Real], (CASE WHEN cashbanktakegiroreal>cashbankduedate THEN cashbanktakegiroreal ELSE cashbankduedate END) AS [Date Giro Real] "
			If DDLType.SelectedValue = "DETAIL" Then
				Dim sRef As String = ""
				If FilterTextAP.Text <> "" Then sRef = DDLAPType.SelectedValue
                'sSql &= ", payaroid [ID Dtl], trnjualno AS [A/P No.], (SELECT apcurr.currcode FROM QL_mstcurr apcurr WHERE apcurr.curroid=tblAP.curroid) AS[Currency A/P], (SELECT apcurr.currsymbol FROM QL_mstcurr apcurr WHERE apcurr.curroid=tblAP.curroid) AS [Curr Symbol A/P], ('(' + ad.acctgcode + ') ' + ad.acctgdesc) AS [Account], trnjualamtnetto AS [A/P Amount], (CASE WHEN ISNULL(payarres1, '')='' THEN (ISNULL((SELECT SUM(ap.amtbayar) FROM QL_conar ap INNER JOIN QL_trnpayar pay2 ON ap.cmpcode=pay2.cmpcode AND ap.payrefoid=pay2.payaroid AND pay2.cashbankoid <> pay.cashbankoid WHERE ap.payrefoid <> 0 AND ap.cmpcode=pay.cmpcode AND ap.reftype=pay.reftype AND ap.refoid=pay.refoid AND ISNULL(pay2.payarres1, '')<>'Lebih Bayar'), 0.0) + ISNULL((SELECT SUM(ap2.amtbayar) FROM ql_conar ap2 WHERE ap2.payrefoid<>0 AND pay.cmpcode=ap2.cmpcode AND pay.reftype=ap2.reftype AND pay.refoid=ap2.refoid AND ap2.trnartype IN ('DNAR', 'CNAR')), 0.0)) ELSE 0.0 END) AS [Paid Amount], (CASE WHEN ISNULL(payarres1, '')='Kurang Bayar' THEN -payaramt WHEN ISNULL(payarres1, '')='Lebih Bayar' THEN payaramt ELSE (ISNULL((SELECT SUM(ap.amtbayar) FROM QL_conar ap INNER JOIN QL_trnpayar pay2 ON ap.cmpcode=pay2.cmpcode AND ap.payrefoid=pay2.payaroid AND pay2.cashbankoid=pay.cashbankoid WHERE ap.payrefoid <> 0 AND ap.cmpcode=pay.cmpcode AND ap.reftype=pay.reftype AND ap.refoid=pay.refoid AND ISNULL(pay2.payarres1, '')<>'Lebih Bayar'), 0.0)) END) AS [A/P Payment], payaramtother AS [Payment Amt.], ISNULL(payarres1, '') AS [Flag], payarnote AS [Detail Note]"
			End If
            sSql &= " FROM QL_trncashbankmst cbm INNER JOIN QL_mstcust s ON s.custoid=cbm.personoid INNER JOIN QL_mstcurr cu ON cu.curroid=cbm.curroid INNER JOIN QL_mstacctg a ON a.acctgoid=cbm.acctgoid"
			If DDLType.SelectedValue = "DETAIL" Then
                sSql &= " INNER JOIN QL_trnpayar pay ON pay.cmpcode=cbm.cmpcode AND pay.cashbankoid=cbm.cashbankoid INNER JOIN QL_mstacctg ad ON ad.acctgoid=pay.acctgoid "
				If FilterTextAP.Text <> "" Then
                    sSql &= " INNER JOIN QL_trnjualmst tblAP ON tblAP.cmpcode=pay.cmpcode AND tblAP.trnjualmstoid=pay.refoid"
				Else
                    sSql &= " INNER JOIN (SELECT cmpcode, curroid, trnjualno, trnjualamtnetto, 'QL_trnjualmst' artype, trnjualmstoid AS apmstoid FROM QL_trnjualmst) AS tblAP ON tblAP.cmpcode=pay.cmpcode AND tblAP.artype=pay.reftype AND tblAP.apmstoid=pay.refoid"
				End If
			End If
            sSql &= " WHERE cbm.cmpcode='" & CompnyCode & "' AND cashbankgroup LIKE 'AR%' AND cbm.cashbankoid > 0 AND cbm.cashbanktype='" & DDLPayType.SelectedValue & "'"
			If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
				sSql &= " AND " & FilterDDLDate.SelectedValue & " BETWEEN CAST('" & FilterPeriod1.Text & " 00:00:00' AS DATETIME) AND CAST('" & FilterPeriod2.Text & " 23:59:59' AS DATETIME)"
			End If
			If DDLPayType.SelectedIndex <> 0 Then
				sSql &= " AND cashbanktype='" & DDLPayType.SelectedValue & "'"
			End If
			If lbPayAccount.Items.Count > 0 Then
				Dim sOid As String = ""
				For C1 As Integer = 0 To lbPayAccount.Items.Count - 1
					sOid &= lbPayAccount.Items(C1).Value & ","
				Next
				If sOid <> "" Then
					sSql &= " AND cbm.acctgoid IN (" & Left(sOid, sOid.Length - 1) & ")"
				End If
			End If
			If FilterTextNo.Text <> "" Then
				sSql &= " AND ("
				Dim arNo() As String = FilterTextNo.Text.Split(";")
				Dim sNo As String = ""
				For C1 As Integer = 0 To arNo.Length - 1
					If arNo(C1) <> "" Then
						sNo &= "cashbankno LIKE '" & arNo(C1) & "' OR "
					End If
				Next
				sSql &= Left(sNo, sNo.Length - 4) & ")"
			End If
			If FilterTextSupp.Text <> "" Then
				sSql &= " AND ("
				Dim arSupp() As String = FilterTextSupp.Text.Split(";")
				Dim sSupp As String = ""
				For C1 As Integer = 0 To arSupp.Length - 1
					If arSupp(C1) <> "" Then
                        sSupp &= "custcode LIKE '" & arSupp(C1) & "' OR "
					End If
				Next
				sSql &= Left(sSupp, sSupp.Length - 4) & ")"
			End If
			If DDLType.SelectedValue = "DETAIL" Then
				If FilterTextAP.Text <> "" Then
                    sSql &= " AND pay.reftype='QL_trnjualmst'"
					sSql &= " AND ("
					Dim arAP() As String = FilterTextAP.Text.Split(";")
					Dim sAP As String = ""
					For C1 As Integer = 0 To arAP.Length - 1
						If arAP(C1) <> "" Then
                            sAP &= " trnjualno LIKE '" & arAP(C1) & "' OR "
						End If
					Next
					sSql &= Left(sAP, sAP.Length - 4) & ")"
				End If
			End If
            sSql &= " ORDER BY [ID Hdr]"
			If DDLType.SelectedValue = "DETAIL" Then
				sSql &= ", [ID Dtl]"
			End If
			Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trncashbankmst")
			report.SetDataSource(dtTbl)
			report.SetParameterValue("PrintUserID", Session("UserID"))
			report.SetParameterValue("PrintUserName", GetStrData("SELECT profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
			report.PrintOptions.PaperSize = PaperSize.PaperA4
			cProc.SetDBLogonForReport(report)
			If sType = "View" Then
				crvReportForm.DisplayGroupTree = False
				crvReportForm.ReportSource = report
				crvReportForm.SeparatePages = True
			ElseIf sType = "Print PDF" Then
				Response.Buffer = False
				Response.ClearContent()
				Response.ClearHeaders()
				report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, sRptName)
				report.Close()
				report.Dispose()
			Else
				Response.Buffer = False
				Response.ClearContent()
				Response.ClearHeaders()
				report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, sRptName)
				report.Close()
				report.Dispose()
			End If
		Catch ex As Exception
			report.Close() : report.Dispose()
			showMessage(ex.Message, 1)
		End Try
	End Sub
#End Region

#Region "Events"
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("UserID") = "" Then
			Response.Redirect("~/Other/login.aspx")
		End If
		If Request.QueryString("awal") = "true" Then
			' Simpan session ke variabel temporary supaya tidak hilang
			Dim userId As String = Session("UserID")
			Dim xsetAcc As DataTable = Session("SpecialAccess")
			Dim appLimit As Decimal = Session("ApprovalLimit")
			Dim xsetRole As DataTable = Session("Role")
			Dim cmpcode As String = Session("CompnyCode")
			' Clear all session
			Session.Clear()
			' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
			Session("UserID") = userId
			Session("SpecialAccess") = xsetAcc
			Session("ApprovalLimit") = appLimit
			Session("Role") = xsetRole
			Session("CompnyCode") = cmpcode
			' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
			Response.Redirect("~\ReportForm\frmPaymentAR.aspx")
		End If
		If checkPagePermission("~\ReportForm\frmPaymentAR.aspx", Session("Role")) = False Then
			Response.Redirect("~\Other\NotAuthorize.aspx")
		End If
		Page.Title = CompnyName & " - A/R Payment Report"
		If Not Page.IsPostBack Then
			FillDDL(DDLBusUnit, "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'" & IIf(Session("CompnyCode") <> CompnyCode, " AND cmpcode='" & Session("CompnyCode") & "'", ""))
			InitDDLPayAccount()
			FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
			FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
		End If
	End Sub

	Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
		cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
	End Sub

	Protected Sub DDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLType.SelectedIndexChanged
		Dim bVal As Boolean = True
		If DDLType.SelectedValue = "SUMMARY" Then
			bVal = False
		End If
		lblAPNo.Visible = bVal : lblSeptAPNo.Visible = bVal : DDLAPType.Visible = bVal : FilterTextAP.Visible = bVal : btnSearchAP.Visible = bVal : btnClearAP.Visible = bVal
	End Sub

	Protected Sub DDLPayType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLPayType.SelectedIndexChanged
		InitDDLPayAccount()
	End Sub

	Protected Sub btnAddPayAccount_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddPayAccount.Click
		If DDLPayAccount.SelectedValue <> "" Then
			If lbPayAccount.Items.Count > 0 Then
				If Not lbPayAccount.Items.Contains(lbPayAccount.Items.FindByValue(DDLPayAccount.SelectedValue)) Then
					Dim objList As New ListItem
					objList.Text = DDLPayAccount.SelectedItem.Text : objList.Value = DDLPayAccount.SelectedValue
					lbPayAccount.Items.Add(objList)
				End If
			Else
				Dim objList As New ListItem
				objList.Text = DDLPayAccount.SelectedItem.Text : objList.Value = DDLPayAccount.SelectedValue
				lbPayAccount.Items.Add(objList)
			End If
		End If
	End Sub

	Protected Sub btnMinPayAccount_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMinPayAccount.Click
		If lbPayAccount.Items.Count > 0 Then
			Dim objList As ListItem = lbPayAccount.SelectedItem
			lbPayAccount.Items.Remove(objList)
		End If
	End Sub

	Protected Sub btnSearchNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchNo.Click
		If DDLBusUnit.SelectedValue = "" Then
			showMessage("Please select Business Unit first!", 2) : Exit Sub
		End If
		If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
			If Not IsValidPeriod() Then
				Exit Sub
			End If
		End If
		FilterDDLListNo.SelectedIndex = -1 : FilterTextListNo.Text = "" : Session("TblListNo") = Nothing : Session("TblViewListNo") = Nothing : gvListNo.DataSource = Session("TblListNo") : gvListNo.DataBind()
		BindListNo()
		cProc.SetModalPopUpExtender(btnHideListNo, pnlListNo, mpeListNo, True)
	End Sub

	Protected Sub btnClearNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearNo.Click
		FilterTextNo.Text = ""
	End Sub

	Protected Sub btnFindListNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListNo.Click
		UpdateCheckedListNo()
		If Session("TblListNo") IsNot Nothing Then
			Session("TblListNo").DefaultView.RowFilter = FilterDDLListNo.SelectedValue & " LIKE '%" & Tchar(FilterTextListNo.Text) & "%'"
			Session("TblViewListNo") = Session("TblListNo").DefaultView.ToTable
			gvListNo.DataSource = Session("TblViewListNo") : gvListNo.DataBind()
			Session("TblListNo").DefaultView.RowFilter = ""
		End If
		mpeListNo.Show()
	End Sub

	Protected Sub btnAllListNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListNo.Click
		UpdateCheckedListNo()
		FilterDDLListNo.SelectedIndex = -1 : FilterTextListNo.Text = ""
		If Session("TblListNo") IsNot Nothing Then
			Session("TblViewListNo") = Session("TblListNo")
			gvListNo.DataSource = Session("TblViewListNo") : gvListNo.DataBind()
		End If
		mpeListNo.Show()
	End Sub

	Protected Sub gvListNo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListNo.PageIndexChanging
		If Session("TblViewListNo") Is Nothing Then
			Session("TblViewListNo") = Session("TblListNo")
		End If
		UpdateCheckedListNo() : UpdateCheckedListNo("View")
		gvListNo.PageIndex = e.NewPageIndex
		gvListNo.DataSource = Session("TblViewListNo") : gvListNo.DataBind()
		mpeListNo.Show()
	End Sub

	Protected Sub lbAddToListNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListNo.Click
		UpdateCheckedListNo()
		If Session("TblListNo") IsNot Nothing Then
			Session("TblListNo").DefaultView.RowFilter = "checkvalue='True'"
			If Session("TblListNo").DefaultView.Count > 0 Then
				Dim sTextNo As String = ""
				For C1 As Integer = 0 To Session("TblListNo").DefaultView.Count - 1
					If Not FilterTextNo.Text.Contains(Session("TblListNo").DefaultView(C1)("cashbankno").ToString) Then
						sTextNo &= Session("TblListNo").DefaultView(C1)("cashbankno").ToString & ";"
					End If
				Next
				If sTextNo <> "" Then
					If FilterTextNo.Text <> "" Then FilterTextNo.Text &= ";"
					FilterTextNo.Text &= Left(sTextNo, sTextNo.Length - 1)
				End If
			End If
			Session("TblListNo").DefaultView.RowFilter = ""
		End If
		cProc.SetModalPopUpExtender(btnHideListNo, pnlListNo, mpeListNo, False)
	End Sub

	Protected Sub lbCloseListNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListNo.Click
		cProc.SetModalPopUpExtender(btnHideListNo, pnlListNo, mpeListNo, False)
	End Sub

	Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupp.Click
		If DDLBusUnit.SelectedValue = "" Then
			showMessage("Please select Business Unit first!", 2) : Exit Sub
		End If
		If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
			If Not IsValidPeriod() Then
				Exit Sub
			End If
		End If
		FilterDDLListSupp.SelectedIndex = -1 : FilterTextListSupp.Text = "" : Session("TblListSupp") = Nothing : Session("TblViewListSupp") = Nothing : gvListSupp.DataSource = Session("TblListSupp") : gvListSupp.DataBind()
		BindListSupp()
		cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
	End Sub

	Protected Sub btnClearSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupp.Click
		FilterTextSupp.Text = ""
	End Sub

	Protected Sub btnFindListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSupp.Click
		UpdateCheckedListSupp()
		If Session("TblListSupp") IsNot Nothing Then
			Session("TblListSupp").DefaultView.RowFilter = FilterDDLListSupp.SelectedValue & " LIKE '%" & Tchar(FilterTextListSupp.Text) & "%'"
			Session("TblViewListSupp") = Session("TblListSupp").DefaultView.ToTable
			gvListSupp.DataSource = Session("TblViewListSupp") : gvListSupp.DataBind()
			Session("TblListSupp").DefaultView.RowFilter = ""
		End If
		mpeListSupp.Show()
	End Sub

	Protected Sub btnAllListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListSupp.Click
		UpdateCheckedListSupp()
		FilterDDLListSupp.SelectedIndex = -1 : FilterTextListSupp.Text = ""
		If Session("TblListSupp") IsNot Nothing Then
			Session("TblViewListSupp") = Session("TblListSupp")
			gvListSupp.DataSource = Session("TblViewListSupp") : gvListSupp.DataBind()
		End If
		mpeListSupp.Show()
	End Sub

	Protected Sub gvListSupp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSupp.PageIndexChanging
		If Session("TblViewListSupp") Is Nothing Then
			Session("TblViewListSupp") = Session("TblListSupp")
		End If
		UpdateCheckedListSupp() : UpdateCheckedListSupp("View")
		gvListSupp.PageIndex = e.NewPageIndex
		gvListSupp.DataSource = Session("TblViewListSupp") : gvListSupp.DataBind()
		mpeListSupp.Show()
	End Sub

	Protected Sub lbAddToListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListSupp.Click
		UpdateCheckedListSupp()
		If Session("TblListSupp") IsNot Nothing Then
			Session("TblListSupp").DefaultView.RowFilter = "checkvalue='True'"
			If Session("TblListSupp").DefaultView.Count > 0 Then
				Dim sTextSupp As String = ""
				For C1 As Integer = 0 To Session("TblListSupp").DefaultView.Count - 1
					If Not FilterTextSupp.Text.Contains(Session("TblListSupp").DefaultView(C1)("suppcode").ToString) Then
						sTextSupp &= Session("TblListSupp").DefaultView(C1)("suppcode").ToString & ";"
					End If
				Next
				If sTextSupp <> "" Then
					If FilterTextSupp.Text <> "" Then FilterTextSupp.Text &= ";"
					FilterTextSupp.Text &= Left(sTextSupp, sTextSupp.Length - 1)
				End If
			End If
			Session("TblListSupp").DefaultView.RowFilter = ""
		End If
		cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
	End Sub

	Protected Sub lbCloseListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListSupp.Click
		cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
	End Sub

	Protected Sub btnSearchAP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchAP.Click
		If DDLBusUnit.SelectedValue = "" Then
			showMessage("Please select Business Unit first!", 2) : Exit Sub
		End If
		If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
			If Not IsValidPeriod() Then
				Exit Sub
			End If
		End If
		FilterDDLListAP.SelectedIndex = -1 : FilterTextListAP.Text = "" : Session("TblListAP") = Nothing : Session("TblViewListAP") = Nothing : gvListAP.DataSource = Session("TblListAP") : gvListAP.DataBind()
		BindListAP()
		cProc.SetModalPopUpExtender(btnHideListAP, pnlListAP, mpeListAP, True)
	End Sub

	Protected Sub btnClearAP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearAP.Click
		FilterTextAP.Text = ""
	End Sub

	Protected Sub btnFindListAP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListAP.Click
		UpdateCheckedListAP()
		If Session("TblListAP") IsNot Nothing Then
			Session("TblListAP").DefaultView.RowFilter = FilterDDLListAP.SelectedValue & " LIKE '%" & Tchar(FilterTextListAP.Text) & "%'"
			Session("TblViewListAP") = Session("TblListAP").DefaultView.ToTable
			gvListAP.DataSource = Session("TblViewListAP") : gvListAP.DataBind()
			Session("TblListAP").DefaultView.RowFilter = ""
		End If
		mpeListAP.Show()
	End Sub

	Protected Sub btnAllListAP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListAP.Click
		UpdateCheckedListAP()
		FilterDDLListAP.SelectedIndex = -1 : FilterTextListAP.Text = ""
		If Session("TblListAP") IsNot Nothing Then
			Session("TblViewListAP") = Session("TblListAP")
			gvListAP.DataSource = Session("TblViewListAP") : gvListAP.DataBind()
		End If
		mpeListAP.Show()
	End Sub

	Protected Sub gvListAP_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListAP.PageIndexChanging
		If Session("TblViewListAP") Is Nothing Then
			Session("TblViewListAP") = Session("TblListAP")
		End If
		UpdateCheckedListAP() : UpdateCheckedListAP("View")
		gvListAP.PageIndex = e.NewPageIndex
		gvListAP.DataSource = Session("TblViewListAP") : gvListAP.DataBind()
		mpeListAP.Show()
	End Sub

	Protected Sub lbAddToListAP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListAP.Click
		UpdateCheckedListAP()
		If Session("TblListAP") IsNot Nothing Then
			Session("TblListAP").DefaultView.RowFilter = "checkvalue='True'"
			If Session("TblListAP").DefaultView.Count > 0 Then
				Dim sTextAP As String = ""
				For C1 As Integer = 0 To Session("TblListAP").DefaultView.Count - 1
					If Not FilterTextAP.Text.Contains(Session("TblListAP").DefaultView(C1)("apno").ToString) Then
						sTextAP &= Session("TblListAP").DefaultView(C1)("apno").ToString & ";"
					End If
				Next
				If sTextAP <> "" Then
					If FilterTextAP.Text <> "" Then FilterTextAP.Text &= ";"
					FilterTextAP.Text &= Left(sTextAP, sTextAP.Length - 1)
				End If
			End If
			Session("TblListAP").DefaultView.RowFilter = ""
		End If
		cProc.SetModalPopUpExtender(btnHideListAP, pnlListAP, mpeListAP, False)
	End Sub

	Protected Sub lbCloseListAP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListAP.Click
		cProc.SetModalPopUpExtender(btnHideListAP, pnlListAP, mpeListAP, False)
	End Sub

	Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
		ShowReport("View")
	End Sub

	Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
		ShowReport("Print PDF")
	End Sub

	Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
		ShowReport("")
	End Sub

	Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
		Response.Redirect("~\ReportForm\frmPaymentAR.aspx?awal=true")
	End Sub

	Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
		ShowReport("View")
	End Sub

	Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
		ShowReport("View")
	End Sub

	Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
		ShowReport("View")
	End Sub

	Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
		Try
			If Not report Is Nothing Then
				If report.IsLoaded Then
					report.Dispose()
					report.Close()
				End If
			End If
		Catch ex As Exception
			report.Dispose()
			report.Close()
		End Try
	End Sub
#End Region

End Class