Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_MaterialUsageKIK
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If FilterPeriod1.Text = "" Or FilterPeriod2.Text = "" Then
            showMessage("Please fill both of Period 1 and Period 2 first!", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitAllDDL()
        ' DDL Division
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If FillDDL(DDLBusUnit, sSql) Then
            InitDDLDept()
        End If
        ' DDL Warehouse
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='WAREHOUSE' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDL(DDLWarehouse, sSql)
    End Sub

    Private Sub InitDDLDept()
        ' DDL Department
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE activeflag='ACTIVE' AND cmpcode='" & DDLBusUnit.SelectedValue & "' ORDER BY deptname"
        FillDDL(DDLDept, sSql)
    End Sub

    Private Sub BindListKIK()
        sSql = "SELECT DISTINCT * FROM ("
        sSql &= "SELECT wom.womstoid,CONVERT(VARCHAR(20), wom.womstoid) AS draftno, wono, CONVERT(VARCHAR(10), wodate, 101) AS KIKDate, womstnote, sono, 'False' AS checkvalue FROM QL_trnwomst wom INNER JOIN QL_trnwodtl1 wod1 ON wom.cmpcode=wod1.cmpcode AND wom.womstoid=wod1.womstoid INNER JOIN QL_trnsomst som ON som.cmpcode=wod1.cmpcode AND som.somstoid=wod1.soitemmstoid WHERE wom.womstoid IN (SELECT womstoid FROM QL_trnusagedtl mud INNER JOIN QL_trnreqmst reqm ON reqm.cmpcode=mud.cmpcode AND reqm.reqmstoid=mud.reqmstoid WHERE mud.cmpcode='" & DDLBusUnit.SelectedValue & "') "
        'UNION ALL "
        '        sSql &= "SELECT wom.womstoid,CONVERT(VARCHAR(20), wom.womstoid) AS draftno, wono, CONVERT(VARCHAR(10), wodate, 101) AS KIKDate, womstnote, soitemno, 'False' AS checkvalue FROM QL_trnwomst wom INNER JOIN QL_trnwodtl1 wod1 ON wom.cmpcode=wod1.cmpcode AND wom.womstoid=wod1.womstoid INNER JOIN QL_trnsoitemmst som ON som.cmpcode=wod1.cmpcode AND som.soitemmstoid=wod1.soitemmstoid WHERE wom.womstoid IN (SELECT womstoid FROM QL_trnusagegendtl mud INNER JOIN QL_trnreqgenmst reqm ON reqm.cmpcode=mud.cmpcode AND reqm.reqgenmstoid=mud.reqgenmstoid WHERE mud.cmpcode='" & DDLBusUnit.SelectedValue & "') UNION ALL "
        '        sSql &= "SELECT wom.womstoid,CONVERT(VARCHAR(20), wom.womstoid) AS draftno, wono, CONVERT(VARCHAR(10), wodate, 101) AS KIKDate, womstnote, soitemno, 'False' AS checkvalue FROM QL_trnwomst wom INNER JOIN QL_trnwodtl1 wod1 ON wom.cmpcode=wod1.cmpcode AND wom.womstoid=wod1.womstoid INNER JOIN QL_trnsoitemmst som ON som.cmpcode=wod1.cmpcode AND som.soitemmstoid=wod1.soitemmstoid WHERE wom.womstoid IN (SELECT womstoid FROM QL_trnusagespdtl mud INNER JOIN QL_trnreqspmst reqm ON reqm.cmpcode=mud.cmpcode AND reqm.reqspmstoid=mud.reqspmstoid WHERE mud.cmpcode='" & DDLBusUnit.SelectedValue & "')"
        sSql &= ") AS QL_trnwomst ORDER BY wono,womstoid"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnwomst")
        If dt.Rows.Count > 0 Then
            Session("TblListKIK") = dt
            Session("TblListKIKView") = Session("TblListKIK")
            gvListKIK.DataSource = Session("TblListKIKView")
            gvListKIK.DataBind()
            cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, True)
        Else
            showMessage("No KIK data available for this time.", 2)
        End If
    End Sub

    Private Sub UpdateCheckedListKIK()
        If Session("TblListKIK") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListKIK")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListKIK.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListKIK.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "womstoid='" & sOid & "'"
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListKIK") = dt
        End If
        If Session("TblListKIKView") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListKIKView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListKIK.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListKIK.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "womstoid='" & sOid & "'"
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListKIKView") = dt
        End If
    End Sub

    Private Sub BindListUsage()
        sSql = "SELECT * FROM ("
        sSql &= "SELECT '' + CONVERT(VARCHAR(20), usagemstoid) AS matusagemstoid, CONVERT(VARCHAR(20), usagemstoid) AS draftno, usageno AS matusageno, CONVERT(VARCHAR(10), usagedate, 101) AS matusagedate, usagemstnote AS matusagemstnote, 'False' AS checkvalue FROM QL_trnusagemst mum WHERE mum.cmpcode='" & DDLBusUnit.SelectedValue & "' "
        'UNION ALL "
        '        sSql &= "SELECT '(GM)' + CONVERT(VARCHAR(20), usagegenmstoid) AS matusagemstoid, CONVERT(VARCHAR(20), usagegenmstoid) AS draftno, usagegenno AS matusageno, CONVERT(VARCHAR(10), usagegendate, 101) AS matusagedate, usagegenmstnote AS matusagemstnote, 'False' AS checkvalue FROM QL_trnusagegenmst mum WHERE mum.cmpcode='" & DDLBusUnit.SelectedValue & "' UNION ALL "
        '        sSql &= "SELECT '(SP)' + CONVERT(VARCHAR(20), usagespmstoid) AS matusagemstoid, CONVERT(VARCHAR(20), usagespmstoid) AS draftno, usagespno AS matusageno, CONVERT(VARCHAR(10), usagespdate, 101) AS matusagedate, usagespmstnote AS matusagemstnote, 'False' AS checkvalue FROM QL_trnusagespmst mum WHERE mum.cmpcode='" & DDLBusUnit.SelectedValue & "'"
        sSql &= ") AS QL_trnmatusagemst ORDER BY matusageno"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnmatusagemst")
        If dt.Rows.Count > 0 Then
            Session("TblListUsage") = dt
            Session("TblListUsageView") = Session("TblListUsage")
            gvListUsage.DataSource = Session("TblListUsageView")
            gvListUsage.DataBind()
            cProc.SetModalPopUpExtender(btnHideListUsage, pnlListUsage, mpeListUsage, True)
        Else
            showMessage("No Material Usage KIK data available for this time.", 2)
        End If
    End Sub

    Private Sub UpdateCheckedListUsage()
        If Session("TblListUsage") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListUsage")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListUsage.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListUsage.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matusagemstoid='" & sOid & "'"
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListUsage") = dt
        End If
        If Session("TblListUsageView") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListUsageView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListUsage.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListUsage.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matusagemstoid='" & sOid & "'"
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListUsageView") = dt
        End If
    End Sub

    Private Sub BindListMat()
        'Dim sTypeMat As String = "", sTypeMat2 As String = "", sType As String = ""
        'If DDLMatType.SelectedValue = "RM" Then
        '    sTypeMat = "matraw" : sType = "raw"
        'ElseIf DDLMatType.SelectedValue = "GM" Then
        '    sTypeMat = "matgen" : sType = "gen"
        'ElseIf DDLMatType.SelectedValue = "SP" Then
        '    sTypeMat = "sparepart" : sType = "sp"
        'ElseIf DDLMatType.SelectedValue = "LOG" Then
        '    sTypeMat2 = "log" : sType = "log"
        'Else
        '    sTypeMat2 = "pallet" : sType = "sawn"
        'End If
        'If sTypeMat = "" Then
        '    sSql = "SELECT DISTINCT mud." & sTypeMat2 & "oid AS matoid, matrawcode + ' (' + " & sTypeMat2 & "no + ')' AS matcode, matrawlongdesc AS matlongdesc, gendesc AS matunit, 'False' AS checkvalue FROM QL_trnusage" & sType & "dtl mud INNER JOIN QL_mst" & sTypeMat2 & " p ON p.cmpcode=mud.cmpcode AND p." & sTypeMat2 & "oid=mud." & sTypeMat2 & "oid INNER JOIN QL_mstmatraw m ON m.matrawoid=p.refoid INNER JOIN QL_mstgen g ON genoid=usage" & sType & "unitoid WHERE mud.cmpcode='" & DDLBusUnit.SelectedValue & "' ORDER BY matcode"
        'Else
        '    sSql = "SELECT DISTINCT mud." & sTypeMat & "oid AS matoid, " & sTypeMat & "code AS matcode, " & sTypeMat & "longdesc AS matlongdesc, gendesc AS matunit, 'False' AS checkvalue FROM QL_trnusage" & sType & "dtl mud INNER JOIN QL_mst" & sTypeMat & " m ON m." & sTypeMat & "oid=mud." & sTypeMat & "oid INNER JOIN QL_mstgen g ON genoid=usage" & sType & "unitoid WHERE mud.cmpcode='" & DDLBusUnit.SelectedValue & "' ORDER BY matcode"
        'End If
        sSql = "SELECT DISTINCT mud.usagedtloid AS matoid, m.itemcode AS matcode, m.itemLongDescription AS matlongdesc, gendesc AS matunit, 'False' AS checkvalue FROM QL_trnusagedtl mud INNER JOIN QL_mstitem m ON m.itemoid=mud.matoid INNER JOIN QL_mstgen g ON genoid=usageunitoid WHERE mud.cmpcode='" & DDLBusUnit.SelectedValue & "' ORDER BY matcode"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnusagedtl")
        If dt.Rows.Count > 0 Then
            Session("TblListMat") = dt
            Session("TblListMatView") = Session("TblListMat")
            gvListMat.DataSource = Session("TblListMatView")
            gvListMat.DataBind()
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            showMessage("No Material data available for this time.", 2)
        End If
    End Sub

    Private Sub UpdateCheckedListMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMat") = dt
        End If
        If Session("TblListMatView") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMatView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMatView") = dt
        End If
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        If Not IsValidPeriod() Then
            Exit Sub
        End If
        Try
            Dim sWhere As String = " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND " & DDLDate.SelectedValue & ">=CONVERT(DATETIME, '" & FilterPeriod1.Text & " 00:00:00') AND " & DDLDate.SelectedValue & "<=CONVERT(DATETIME, '" & FilterPeriod2.Text & " 23:59:59')"
            If FilterUsage.Text <> "" Then
                Dim sWhereCode As String = ""
                Dim sNo() As String = Split(FilterUsage.Text, ";")
                If sNo.Length > 0 Then
                    For C1 As Integer = 0 To sNo.Length - 1
                        If sNo(C1) <> "" Then
                            sWhereCode &= DDLUsage.SelectedValue & " LIKE '" & Tchar(sNo(C1)) & "' OR "
                        End If
                    Next
                    If sWhereCode <> "" Then
                        sWhereCode = Left(sWhereCode, sWhereCode.Length - 4)
                        sWhere &= " AND (" & sWhereCode & ")"
                    End If
                End If
            End If
            If DDLType.SelectedValue = "DETAIL" Then
                If FilterKIK.Text <> "" Then
                    Dim sWhereCode As String = ""
                    Dim sNo() As String = Split(FilterKIK.Text, ";")
                    If sNo.Length > 0 Then
                        For C1 As Integer = 0 To sNo.Length - 1
                            If sNo(C1) <> "" Then
                                sWhereCode &= " [KIK No.] LIKE '" & Tchar(sNo(C1)) & "' OR "
                            End If
                        Next
                        If sWhereCode <> "" Then
                            sWhereCode = Left(sWhereCode, sWhereCode.Length - 4)
                            sWhere &= " AND (" & sWhereCode & ")"
                        End If
                    End If
                End If
                If FilterMaterial.Text <> "" Then
                    Dim sWhereCode As String = ""
                    Dim sNo() As String = Split(FilterMaterial.Text, ";")
                    If sNo.Length > 0 Then
                        For C1 As Integer = 0 To sNo.Length - 1
                            If sNo(C1) <> "" Then
                                sWhereCode &= "[Code] LIKE '" & Tchar(sNo(C1)) & "' OR "
                            End If
                        Next
                        If sWhereCode <> "" Then
                            sWhereCode = Left(sWhereCode, sWhereCode.Length - 4)
                            sWhere &= " AND (" & sWhereCode & ")"
                        End If
                    End If
                End If
            End If
            If lbDept.Items.Count > 0 Then
                Dim sOid As String = ""
                For C1 As Integer = 0 To lbDept.Items.Count - 1
                    sOid &= lbDept.Items(C1).Value & ","
                Next
                If sOid <> "" Then
                    sOid = Left(sOid, sOid.Length - 1)
                    sWhere &= " AND [Dept ID] IN (" & sOid & ")"
                End If
            End If
            If lbWarehouse.Items.Count > 0 Then
                Dim sOid As String = ""
                For C1 As Integer = 0 To lbWarehouse.Items.Count - 1
                    sOid &= lbWarehouse.Items(C1).Value & ","
                Next
                If sOid <> "" Then
                    sOid = Left(sOid, sOid.Length - 1)
                    sWhere &= " AND [WH ID] IN (" & sOid & ")"
                End If
            End If
            Dim sStatus As String = ""
            For C1 As Integer = 0 To cblStatus.Items.Count - 1
                If cblStatus.Items(C1).Selected Then
                    sStatus &= "'" & cblStatus.Items(C1).Value & "',"
                End If
            Next
            If sStatus <> "" Then
                sStatus = Left(sStatus, sStatus.Length - 1)
                sWhere &= " AND [Status] IN (" & sStatus & ")"
            End If
            Dim sSortBy As String = ""
            Dim sSplitSort() As String = DDLSortBy.SelectedValue.Split(",")
            If sSplitSort.Length > 0 Then
                For C1 As Integer = 0 To sSplitSort.Length - 1
                    If sSplitSort(C1) <> "" Then
                        sSortBy &= sSplitSort(C1) & " " & DDLOrder.SelectedValue & ", "
                    End If
                Next
            End If
            Dim sRptName As String = ""
            Dim dtReport As DataTable = Nothing
            If DDLType.SelectedValue = "SUMMARY" Then
                sRptName = "MatUsageKIKSummaryReport"
                If sType = "Print Excel" Then
                    report.Load(Server.MapPath(folderReport & "rptMatUsageKIKSum.rpt")) '_Excel.rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptMatUsageKIKSum.rpt"))
                End If
                sSql = "SELECT * FROM ("
                sSql &= "SELECT mum.cmpcode, (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=mum.cmpcode) [Business Unit], CONVERT(VARCHAR(20), mum.usagemstoid) [Draft No.], usagedate [Usage Date], usageno [Usage No.], mum.deptoid [Dept ID], (SELECT deptname FROM QL_mstdept de WHERE de.deptoid=mum.deptoid) [Department], usagemststatus [Status], usagemstnote [Header Note], UPPER(mum.createuser) [Create User], (CASE usagemststatus WHEN 'In Process' THEN '' ELSE UPPER(mum.upduser) END) [Posting User], (CASE usagemststatus WHEN 'In Process' THEN NULL ELSE mum.updtime END) [Posting Date] FROM QL_trnusagemst mum "
                'UNION ALL " & _
                '                        "SELECT mum.cmpcode, (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=mum.cmpcode) [Business Unit], CONVERT(VARCHAR(20), mum.usagegenmstoid) [Draft No.], usagegendate [Usage Date], usagegenno [Usage No.], mum.deptoid [Dept ID], (SELECT deptname FROM QL_mstdept de WHERE de.deptoid=mum.deptoid) [Department], usagegenmststatus [Status], usagegenmstnote [Header Note], UPPER(mum.createuser) [Create User], (CASE usagegenmststatus WHEN 'In Process' THEN '' ELSE UPPER(mum.upduser) END) [Posting User], (CASE usagegenmststatus WHEN 'In Process' THEN NULL ELSE mum.updtime END) [Posting Date] FROM QL_trnusagegenmst mum UNION ALL " & _
                '                        "SELECT mum.cmpcode, (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=mum.cmpcode) [Business Unit], CONVERT(VARCHAR(20), mum.usagespmstoid) [Draft No.], usagespdate [Usage Date], usagespno [Usage No.], mum.deptoid [Dept ID], (SELECT deptname FROM QL_mstdept de WHERE de.deptoid=mum.deptoid) [Department], usagespmststatus [Status], usagespmstnote [Header Note], UPPER(mum.createuser) [Create User], (CASE usagespmststatus WHEN 'In Process' THEN '' ELSE UPPER(mum.upduser) END) [Posting User], (CASE usagespmststatus WHEN 'In Process' THEN NULL ELSE mum.updtime END) [Posting Date] FROM QL_trnusagespmst mum"
                sSql &= ") AS QL_tblsummary " & sWhere & " ORDER BY [Usage No.], [Draft No.]"
                dtReport = cKon.ambiltabel(sSql, "QL_tblsummary")
            Else
                sRptName = "MatUsageKIKDetailReport"
                Dim sExt As String = "", sGroup As String = ""
                If sType = "Print Excel" Then
                    sExt = "" '"_Excel"
                End If
                If DDLGroupBy.SelectedIndex = 0 Then
                    sGroup = "PerNo"
                Else
                    sGroup = "PerCode"
                End If
                If sType = "Print Excel" Then
                    report.Load(Server.MapPath(folderReport & "rptMatUsageKIKDtl" & sGroup & sExt & ".rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptMatUsageKIKDtl" & sGroup & sExt & ".rpt"))
                End If
                sSql = "SELECT * FROM ("
                sSql &= "SELECT mum.cmpcode, (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=mum.cmpcode) [Business Unit], CONVERT(VARCHAR(20), mum.usagemstoid) [Draft No.], usagedate [Usage Date], usageno [Usage No.], mum.deptoid [Dept ID], (SELECT deptname FROM QL_mstdept de WHERE de.deptoid=mum.deptoid) [Department], usagemststatus [Status], usagemstnote [Header Note], UPPER(mum.createuser) [Create User], (CASE usagemststatus WHEN 'In Process' THEN '' ELSE UPPER(mum.upduser) END) [Posting User], (CASE usagemststatus WHEN 'In Process' THEN NULL ELSE mum.updtime END) [Posting Date], ISNULL((SELECT wono FROM QL_trnreqmst reqm INNER JOIN QL_trnwomst wom ON wom.cmpcode=reqm.cmpcode AND wom.womstoid=reqm.womstoid WHERE reqm.cmpcode=mud.cmpcode AND reqm.reqmstoid=mud.reqmstoid), '') [KIK No.], ISNULL((SELECT itemcode FROM QL_trnreqmst reqm INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=reqm.cmpcode AND wod1.womstoid=reqm.womstoid INNER JOIN QL_mstitem i ON i.itemoid=wod1.itemoid WHERE reqm.cmpcode=mud.cmpcode AND reqm.reqmstoid=mud.reqmstoid), '') [Finish Good Code], ISNULL((SELECT itemLongDescription FROM QL_trnreqmst reqm INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=reqm.cmpcode AND wod1.womstoid=reqm.womstoid INNER JOIN QL_mstitem i ON i.itemoid=wod1.itemoid WHERE reqm.cmpcode=mud.cmpcode AND reqm.reqmstoid=mud.reqmstoid), '') [Finish Good], ISNULL((SELECT sono FROM QL_trnreqmst reqm INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=reqm.cmpcode AND wod1.womstoid=reqm.womstoid INNER JOIN QL_trnsomst som ON som.cmpcode=wod1.cmpcode AND som.somstoid=wod1.soitemmstoid WHERE reqm.cmpcode=mud.cmpcode AND reqm.reqmstoid=mud.reqmstoid), '') [SO No.], ISNULL((SELECT custcode FROM QL_trnreqmst reqm INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=reqm.cmpcode AND wod1.womstoid=reqm.womstoid INNER JOIN QL_trnsomst som ON som.cmpcode=wod1.cmpcode AND som.somstoid=wod1.soitemmstoid INNER JOIN QL_mstcust c ON c.custoid=som.custoid WHERE reqm.cmpcode=mud.cmpcode AND reqm.reqmstoid=mud.reqmstoid), '') [Customer Code], ISNULL((SELECT custname FROM QL_trnreqmst reqm INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=reqm.cmpcode AND wod1.womstoid=reqm.womstoid INNER JOIN QL_trnsomst som ON som.cmpcode=wod1.cmpcode AND som.somstoid=wod1.soitemmstoid INNER JOIN QL_mstcust c ON c.custoid=som.custoid WHERE reqm.cmpcode=mud.cmpcode AND reqm.reqmstoid=mud.reqmstoid), '') [Customer], ISNULL((SELECT wodtl3qty FROM QL_trnreqdtl reqd INNER JOIN  QL_trnreqmst reqm ON reqm.cmpcode=mud.cmpcode AND reqm.reqmstoid=mud.reqmstoid INNER JOIN QL_trnwodtl3 wod3 ON wod3.cmpcode=reqd.cmpcode AND wod3.womstoid=reqm.womstoid AND wod3.wodtl3oid=reqd.refoid WHERE reqd.cmpcode=mud.cmpcode AND reqd.reqdtloid=mud.reqdtloid), 0) [KIK Qty Mat], ISNULL((SELECT reqno FROM QL_trnreqmst reqm WHERE reqm.cmpcode=mud.cmpcode AND reqm.reqmstoid=mud.reqmstoid), '') [Request No.], ISNULL((SELECT reqqty FROM QL_trnreqdtl reqd INNER JOIN  QL_trnreqmst reqm ON reqm.cmpcode=mud.cmpcode AND reqm.reqmstoid=mud.reqmstoid WHERE reqd.cmpcode=mud.cmpcode AND reqd.reqdtloid=mud.reqdtloid), 0) [Req Qty], usagewhoid [WH ID], g1.gendesc [Warehouse], 'Raw' [Type], '' + itemCode [Code], itemLongDescription [Material], usageqty [Qty], g2.gendesc [Unit], usagedtlnote [Note] FROM QL_trnusagemst mum INNER JOIN QL_trnusagedtl mud ON mud.cmpcode=mum.cmpcode AND mud.usagemstoid=mum.usagemstoid INNER JOIN QL_mstgen g1 ON g1.genoid=usagewhoid AND g1.gengroup='WAREHOUSE' INNER JOIN QL_mstitem m ON m.itemoid=mud.matoid INNER JOIN QL_mstgen g2 ON g2.genoid=usageunitoid AND g2.gengroup='UNIT' "
                'UNION ALL " & _
                '                        "SELECT mum.cmpcode, (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=mum.cmpcode) [Business Unit], CONVERT(VARCHAR(20), mum.usagegenmstoid) [Draft No.], usagegendate [Usage Date], usagegenno [Usage No.], mum.deptoid [Dept ID], (SELECT deptname FROM QL_mstdept de WHERE de.deptoid=mum.deptoid) [Department], usagegenmststatus [Status], usagegenmstnote [Header Note], UPPER(mum.createuser) [Create User], (CASE usagegenmststatus WHEN 'In Process' THEN '' ELSE UPPER(mum.upduser) END) [Posting User], (CASE usagegenmststatus WHEN 'In Process' THEN NULL ELSE mum.updtime END) [Posting Date], ISNULL((SELECT wono FROM QL_trnreqgenmst reqm INNER JOIN QL_trnwomst wom ON wom.cmpcode=reqm.cmpcode AND wom.womstoid=reqm.womstoid WHERE reqm.cmpcode=mud.cmpcode AND reqm.reqgenmstoid=mud.reqgenmstoid), '') [KIK No.], ISNULL((SELECT itemcode FROM QL_trnreqgenmst reqm INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=reqm.cmpcode AND wod1.womstoid=reqm.womstoid INNER JOIN QL_mstitem i ON i.itemoid=wod1.itemoid WHERE reqm.cmpcode=mud.cmpcode AND reqm.reqgenmstoid=mud.reqgenmstoid), '') [Finish Good Code], ISNULL((SELECT itemlongdesc FROM QL_trnreqgenmst reqm INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=reqm.cmpcode AND wod1.womstoid=reqm.womstoid INNER JOIN QL_mstitem i ON i.itemoid=wod1.itemoid WHERE reqm.cmpcode=mud.cmpcode AND reqm.reqgenmstoid=mud.reqgenmstoid), '') [Finish Good], ISNULL((SELECT soitemno FROM QL_trnreqgenmst reqm INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=reqm.cmpcode AND wod1.womstoid=reqm.womstoid INNER JOIN QL_trnsoitemmst som ON som.cmpcode=wod1.cmpcode AND som.soitemmstoid=wod1.soitemmstoid WHERE reqm.cmpcode=mud.cmpcode AND reqm.reqgenmstoid=mud.reqgenmstoid), '') [SO No.], ISNULL((SELECT custcode FROM QL_trnreqgenmst reqm INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=reqm.cmpcode AND wod1.womstoid=reqm.womstoid INNER JOIN QL_trnsoitemmst som ON som.cmpcode=wod1.cmpcode AND som.soitemmstoid=wod1.soitemmstoid INNER JOIN QL_mstcust c ON c.custoid=som.custoid WHERE reqm.cmpcode=mud.cmpcode AND reqm.reqgenmstoid=mud.reqgenmstoid), '') [Customer Code], ISNULL((SELECT custname FROM QL_trnreqgenmst reqm INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=reqm.cmpcode AND wod1.womstoid=reqm.womstoid INNER JOIN QL_trnsoitemmst som ON som.cmpcode=wod1.cmpcode AND som.soitemmstoid=wod1.soitemmstoid INNER JOIN QL_mstcust c ON c.custoid=som.custoid WHERE reqm.cmpcode=mud.cmpcode AND reqm.reqgenmstoid=mud.reqgenmstoid), '') [Customer], ISNULL((SELECT wodtl3qty FROM QL_trnreqgendtl reqd INNER JOIN  QL_trnreqgenmst reqm ON reqm.cmpcode=mud.cmpcode AND reqm.reqgenmstoid=mud.reqgenmstoid INNER JOIN QL_trnwodtl3 wod3 ON wod3.cmpcode=reqd.cmpcode AND wod3.womstoid=reqm.womstoid AND wod3.wodtl3oid=reqd.refoid ), 0) [KIK Qty Mat], ISNULL((SELECT reqgenno FROM QL_trnreqgenmst reqm WHERE reqm.cmpcode=mud.cmpcode AND reqm.reqgenmstoid=mud.reqgenmstoid), '') [Request No.], ISNULL((SELECT reqgenqty FROM QL_trnreqgendtl reqd INNER JOIN  QL_trnreqgenmst reqm ON reqm.cmpcode=mud.cmpcode AND reqm.reqgenmstoid=mud.reqgenmstoid WHERE reqd.cmpcode=mud.cmpcode AND reqd.reqgendtloid=mud.reqgendtloid), 0) [Req Qty], usagegenwhoid [WH ID], g1.gendesc [Warehouse], 'General' [Type], 'GM-' + matgencode [Code], matgenlongdesc [Material], usagegenqty [Qty], g2.gendesc [Unit], usagegendtlnote [Note] FROM QL_trnusagegenmst mum INNER JOIN QL_trnusagegendtl mud ON mud.cmpcode=mum.cmpcode AND mud.usagegenmstoid=mum.usagegenmstoid INNER JOIN QL_mstgen g1 ON g1.genoid=usagegenwhoid AND g1.gengroup='MATERIAL LOCATION' INNER JOIN QL_mstmatgen m ON m.matgenoid=mud.matgenoid INNER JOIN QL_mstgen g2 ON g2.genoid=usagegenunitoid AND g2.gengroup='MATERIAL UNIT' UNION ALL " & _
                '                        "SELECT mum.cmpcode, (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=mum.cmpcode) [Business Unit], CONVERT(VARCHAR(20), mum.usagespmstoid) [Draft No.], usagespdate [Usage Date], usagespno [Usage No.], mum.deptoid [Dept ID], (SELECT deptname FROM QL_mstdept de WHERE de.deptoid=mum.deptoid) [Department], usagespmststatus [Status], usagespmstnote [Header Note], UPPER(mum.createuser) [Create User], (CASE usagespmststatus WHEN 'In Process' THEN '' ELSE UPPER(mum.upduser) END) [Posting User], (CASE usagespmststatus WHEN 'In Process' THEN NULL ELSE mum.updtime END) [Posting Date], ISNULL((SELECT wono FROM QL_trnreqspmst reqm INNER JOIN QL_trnwomst wom ON wom.cmpcode=reqm.cmpcode AND wom.womstoid=reqm.womstoid WHERE reqm.cmpcode=mud.cmpcode AND reqm.reqspmstoid=mud.reqspmstoid), '') [KIK No.], ISNULL((SELECT itemcode FROM QL_trnreqspmst reqm INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=reqm.cmpcode AND wod1.womstoid=reqm.womstoid INNER JOIN QL_mstitem i ON i.itemoid=wod1.itemoid WHERE reqm.cmpcode=mud.cmpcode AND reqm.reqspmstoid=mud.reqspmstoid), '') [Finish Good Code], ISNULL((SELECT itemlongdesc FROM QL_trnreqspmst reqm INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=reqm.cmpcode AND wod1.womstoid=reqm.womstoid INNER JOIN QL_mstitem i ON i.itemoid=wod1.itemoid WHERE reqm.cmpcode=mud.cmpcode AND reqm.reqspmstoid=mud.reqspmstoid), '') [Finish Good], ISNULL((SELECT soitemno FROM QL_trnreqspmst reqm INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=reqm.cmpcode AND wod1.womstoid=reqm.womstoid INNER JOIN QL_trnsoitemmst som ON som.cmpcode=wod1.cmpcode AND som.soitemmstoid=wod1.soitemmstoid WHERE reqm.cmpcode=mud.cmpcode AND reqm.reqspmstoid=mud.reqspmstoid), '') [SO No.], ISNULL((SELECT custcode FROM QL_trnreqspmst reqm INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=reqm.cmpcode AND wod1.womstoid=reqm.womstoid INNER JOIN QL_trnsoitemmst som ON som.cmpcode=wod1.cmpcode AND som.soitemmstoid=wod1.soitemmstoid INNER JOIN QL_mstcust c ON c.custoid=som.custoid WHERE reqm.cmpcode=mud.cmpcode AND reqm.reqspmstoid=mud.reqspmstoid), '') [Customer Code], ISNULL((SELECT custname FROM QL_trnreqspmst reqm INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=reqm.cmpcode AND wod1.womstoid=reqm.womstoid INNER JOIN QL_trnsoitemmst som ON som.cmpcode=wod1.cmpcode AND som.soitemmstoid=wod1.soitemmstoid INNER JOIN QL_mstcust c ON c.custoid=som.custoid WHERE reqm.cmpcode=mud.cmpcode AND reqm.reqspmstoid=mud.reqspmstoid), '') [Customer], ISNULL((SELECT wodtl3qty FROM QL_trnreqspdtl reqd INNER JOIN  QL_trnreqspmst reqm ON reqm.cmpcode=mud.cmpcode AND reqm.reqspmstoid=mud.reqspmstoid INNER JOIN QL_trnwodtl3 wod3 ON wod3.cmpcode=reqd.cmpcode AND wod3.womstoid=reqm.womstoid AND wod3.wodtl3oid=reqd.refoid ), 0) [KIK Qty Mat], ISNULL((SELECT reqspno FROM QL_trnreqspmst reqm WHERE reqm.cmpcode=mud.cmpcode AND reqm.reqspmstoid=mud.reqspmstoid), '') [Request No.], ISNULL((SELECT reqspqty FROM QL_trnreqspdtl reqd INNER JOIN  QL_trnreqspmst reqm ON reqm.cmpcode=mud.cmpcode AND reqm.reqspmstoid=mud.reqspmstoid WHERE reqd.cmpcode=mud.cmpcode AND reqd.reqspdtloid=mud.reqspdtloid), 0) [Req Qty], usagespwhoid [WH ID], g1.gendesc [Warehouse], 'Spare Part' [Type], 'SP-' + sparepartcode [Code], sparepartlongdesc [Material], usagespqty [Qty], g2.gendesc [Unit], usagespdtlnote [Note] FROM QL_trnusagespmst mum INNER JOIN QL_trnusagespdtl mud ON mud.cmpcode=mum.cmpcode AND mud.usagespmstoid=mum.usagespmstoid INNER JOIN QL_mstgen g1 ON g1.genoid=usagespwhoid AND g1.gengroup='MATERIAL LOCATION' INNER JOIN QL_mstsparepart m ON m.sparepartoid=mud.sparepartoid INNER JOIN QL_mstgen g2 ON g2.genoid=usagespunitoid AND g2.gengroup='MATERIAL UNIT'"
                sSql &= ") AS QL_tbldetail " & sWhere
                sSql &= " ORDER BY cmpcode, " & DDLGroupBy.SelectedValue
                If sSortBy <> "" Then
                    sSql &= ", " & Left(sSortBy, sSortBy.Length - 2)
                End If
                dtReport = cKon.ambiltabel(sSql, "QL_tbldetail")
            End If
            report.SetDataSource(dtReport)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            cProc.SetDBLogonForReport(report)
            report.PrintOptions.PaperSize = PaperSize.PaperFolio
            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, sRptName)
                report.Close()
                report.Dispose()
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, sRptName)
                report.Close()
                report.Dispose()
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmMatUsageKIK.aspx")
        End If
        If checkPagePermission("~\ReportForm\frmMatUsageKIK.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Material Usage KIK Report"
        If Not Page.IsPostBack Then
            InitAllDDL()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            DDLType_SelectedIndexChanged(Nothing, Nothing)
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListKIK") Is Nothing And Session("WarningListKIK") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListKIK") Then
                Session("WarningListKIK") = Nothing
                mpeListKIK.Show()
            End If
        End If
        If Not Session("WarningListUsage") Is Nothing And Session("WarningListUsage") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListUsage") Then
                Session("WarningListUsage") = Nothing
                mpeListUsage.Show()
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                mpeListMat.Show()
            End If
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLDept()
    End Sub

    Protected Sub DDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLType.SelectedIndexChanged
        Dim bVal As Boolean = True
        If DDLType.SelectedValue = "SUMMARY" Then
            bVal = False
        End If
        DDLGroupBy.SelectedIndex = -1
        lblKIK.Visible = bVal : septKIK.Visible = bVal : FilterKIK.Visible = bVal : btnSearchKIK.Visible = bVal : btnClearKIK.Visible = bVal
        lblMat.Visible = bVal : septMat.Visible = bVal : DDLMatType.Visible = bVal : FilterMaterial.Visible = bVal : btnSearchMat.Visible = bVal : btnClearMat.Visible = bVal
        lblWH.Visible = bVal : septWH.Visible = bVal : DDLWarehouse.Visible = bVal : btnAddWH.Visible = bVal : lbWarehouse.Visible = bVal : btnMinWH.Visible = bVal
        lblGroupBy.Visible = bVal : septGroupBy.Visible = bVal : DDLGroupBy.Visible = bVal
        lblSortBy.Visible = bVal : septSortBy.Visible = bVal : DDLSortBy.Visible = bVal : DDLOrder.Visible = bVal
        DDLGroupBy_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub btnSearchKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchKIK.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2) : Exit Sub
        End If
        FilterDDLListKIK.SelectedIndex = -1 : FilterTextListKIK.Text = "" : Session("TblListKIK") = Nothing : Session("TblListKIKView") = Nothing : gvListKIK.DataSource = Nothing : gvListKIK.DataBind()
        BindListKIK()
    End Sub

    Protected Sub btnClearKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearKIK.Click
        FilterKIK.Text = ""
    End Sub

    Protected Sub btnFindListKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListKIK.Click
        UpdateCheckedListKIK()
        Dim dt As DataTable = Session("TblListKIK")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = FilterDDLListKIK.SelectedValue & " LIKE '%" & Tchar(FilterTextListKIK.Text) & "%'"
        If dv.Count > 0 Then
            Session("TblListKIKView") = dv.ToTable
            gvListKIK.DataSource = Session("TblListKIKView")
            gvListKIK.DataBind()
            dv.RowFilter = ""
            mpeListKIK.Show()
        Else
            dv.RowFilter = ""
            Session("WarningListKIK") = "KIK data can't be found!"
            showMessage(Session("WarningListKIK"), 2)
        End If
    End Sub

    Protected Sub btnAllListKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListKIK.Click
        UpdateCheckedListKIK()
        FilterDDLListKIK.SelectedIndex = -1 : FilterTextListKIK.Text = ""
        Session("TblListKIKView") = Session("TblListKIK")
        gvListKIK.DataSource = Session("TblListKIKView")
        gvListKIK.DataBind()
        mpeListKIK.Show()
    End Sub

    Protected Sub gvListKIK_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListKIK.PageIndexChanging
        UpdateCheckedListKIK()
        gvListKIK.PageIndex = e.NewPageIndex
        gvListKIK.DataSource = Session("TblListKIKView")
        gvListKIK.DataBind()
        mpeListKIK.Show()
    End Sub

    Protected Sub cbHdrLMKIK_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For C1 As Integer = 0 To gvListKIK.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListKIK.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        mpeListKIK.Show()
    End Sub

    Protected Sub lbAddToListKIK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListKIK.Click
        UpdateCheckedListKIK()
        Dim dt As DataTable = Session("TblListKIK")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = "checkvalue='True'"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                If dv(C1)("wono").ToString <> "" Then
                    FilterKIK.Text &= dv(C1)("wono").ToString & ";"
                End If
            Next
            If FilterKIK.Text <> "" Then
                FilterKIK.Text = Left(FilterKIK.Text, FilterKIK.Text.Length - 1)
            End If
            dv.RowFilter = ""
            cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, False)
        Else
            dv.RowFilter = ""
            Session("WarningListKIK") = "Please select KIK data first!"
            showMessage(Session("WarningListKIK"), 2)
        End If
    End Sub

    Protected Sub lbSelectAllListKIK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSelectAllListKIK.Click
        UpdateCheckedListKIK()
        Dim dt As DataTable = Session("TblListKIKView")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            If dt.Rows(C1)("wono").ToString <> "" Then
                FilterKIK.Text &= dt.Rows(C1)("wono").ToString & ";"
            End If
        Next
        If FilterKIK.Text <> "" Then
            FilterKIK.Text = Left(FilterKIK.Text, FilterKIK.Text.Length - 1)
        End If
        cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, False)
    End Sub

    Protected Sub lbCloseListKIK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListKIK.Click
        cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, False)
    End Sub

    Protected Sub btnSearchUsage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchUsage.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2) : Exit Sub
        End If
        FilterDDLListUsage.SelectedIndex = -1 : FilterTextListUsage.Text = "" : Session("TblListUsage") = Nothing : Session("TblListUsageView") = Nothing : gvListUsage.DataSource = Nothing : gvListUsage.DataBind()
        BindListUsage()
    End Sub

    Protected Sub btnClearUsage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearUsage.Click
        FilterUsage.Text = ""
    End Sub

    Protected Sub btnFindListUsage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListUsage.Click
        UpdateCheckedListUsage()
        Dim dt As DataTable = Session("TblListUsage")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = FilterDDLListUsage.SelectedValue & " LIKE '%" & Tchar(FilterTextListUsage.Text) & "%'"
        If dv.Count > 0 Then
            Session("TblListUsageView") = dv.ToTable
            gvListUsage.DataSource = Session("TblListUsageView")
            gvListUsage.DataBind()
            dv.RowFilter = ""
            mpeListUsage.Show()
        Else
            dv.RowFilter = ""
            Session("WarningListUsage") = "Material Usage KIK data can't be found!"
            showMessage(Session("WarningListUsage"), 2)
        End If
    End Sub

    Protected Sub btnAllListUsage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListUsage.Click
        UpdateCheckedListUsage()
        FilterDDLListUsage.SelectedIndex = -1 : FilterTextListUsage.Text = ""
        Session("TblListUsageView") = Session("TblListUsage")
        gvListUsage.DataSource = Session("TblListUsageView")
        gvListUsage.DataBind()
        mpeListUsage.Show()
    End Sub

    Protected Sub gvListUsage_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListUsage.PageIndexChanging
        UpdateCheckedListUsage()
        gvListUsage.PageIndex = e.NewPageIndex
        gvListUsage.DataSource = Session("TblListUsageView")
        gvListUsage.DataBind()
        mpeListUsage.Show()
    End Sub

    Protected Sub cbHdrLMUsage_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For C1 As Integer = 0 To gvListUsage.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListUsage.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        mpeListUsage.Show()
    End Sub

    Protected Sub lbAddToListUsage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListUsage.Click
        UpdateCheckedListUsage()
        Dim dt As DataTable = Session("TblListUsage")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = "checkvalue='True'"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                If DDLUsage.SelectedIndex = 0 Then
                    If dv(C1)("matusageno").ToString <> "" Then
                        FilterUsage.Text &= dv(C1)("matusageno").ToString & ";"
                    End If
                Else
                    FilterUsage.Text &= dv(C1)("matusagemstoid").ToString & ";"
                End If
            Next
            If FilterUsage.Text <> "" Then
                FilterUsage.Text = Left(FilterUsage.Text, FilterUsage.Text.Length - 1)
            End If
            dv.RowFilter = ""
            cProc.SetModalPopUpExtender(btnHideListUsage, pnlListUsage, mpeListUsage, False)
        Else
            dv.RowFilter = ""
            Session("WarningListUsage") = "Please select some Material Usage KIK data first!"
            showMessage(Session("WarningListUsage"), 2)
        End If
    End Sub

    Protected Sub lbSelectAllListUsage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSelectAllListUsage.Click
        UpdateCheckedListUsage()
        Dim dt As DataTable = Session("TblListUsageView")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            If DDLUsage.SelectedIndex = 0 Then
                If dt.Rows(C1)("matusageno").ToString <> "" Then
                    FilterUsage.Text &= dt.Rows(C1)("matusageno").ToString & ";"
                End If
            Else
                FilterUsage.Text &= dt.Rows(C1)("matusagemstoid").ToString & ";"
            End If
        Next
        If FilterUsage.Text <> "" Then
            FilterUsage.Text = Left(FilterUsage.Text, FilterUsage.Text.Length - 1)
        End If
        cProc.SetModalPopUpExtender(btnHideListUsage, pnlListUsage, mpeListUsage, False)
    End Sub

    Protected Sub lbCloseListUsage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListUsage.Click
        cProc.SetModalPopUpExtender(btnHideListUsage, pnlListUsage, mpeListUsage, False)
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2) : Exit Sub
        End If
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : Session("TblListMat") = Nothing : Session("TblListMatView") = Nothing : gvListMat.DataSource = Session("TblListMat") : gvListMat.DataBind()
        lblListMat.Text = "List Of " & DDLMatType.SelectedItem.Text
        BindListMat()
    End Sub

    Protected Sub btnClearMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearMat.Click
        FilterMaterial.Text = ""
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        UpdateCheckedListMat()
        Dim dt As DataTable = Session("TblListMat")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        If dv.Count > 0 Then
            Session("TblListMatView") = dv.ToTable
            gvListMat.DataSource = Session("TblListMatView")
            gvListMat.DataBind()
            dv.RowFilter = ""
            mpeListMat.Show()
        Else
            dv.RowFilter = ""
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        UpdateCheckedListMat()
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        Session("TblListMatView") = Session("TblListMat")
        gvListMat.DataSource = Session("TblListMatView")
        gvListMat.DataBind()
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        UpdateCheckedListMat()
        gvListMat.PageIndex = e.NewPageIndex
        gvListMat.DataSource = Session("TblListMatView")
        gvListMat.DataBind()
        mpeListMat.Show()
    End Sub

    Protected Sub cbHdrLM_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For C1 As Integer = 0 To gvListMat.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        mpeListMat.Show()
    End Sub

    Protected Sub lbAddToListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListMat.Click
        UpdateCheckedListMat()
        Dim dt As DataTable = Session("TblListMat")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = "checkvalue='True'"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                FilterMaterial.Text &= "" & dv(C1)("matcode").ToString & ";"
            Next
            If FilterMaterial.Text <> "" Then
                FilterMaterial.Text = Left(FilterMaterial.Text, FilterMaterial.Text.Length - 1)
            End If
            dv.RowFilter = ""
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
        Else
            dv.RowFilter = ""
            Session("WarningListMat") = "Please select some Material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub lbSelectAllListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSelectAllListMat.Click
        UpdateCheckedListMat()
        Dim dt As DataTable = Session("TblListMat")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            FilterMaterial.Text &= DDLMatType.SelectedValue & "-" & dt.Rows(C1)("matcode").ToString & ";"
        Next
        If FilterMaterial.Text <> "" Then
            FilterMaterial.Text = Left(FilterMaterial.Text, FilterMaterial.Text.Length - 1)
        End If
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub btnAddDept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddDept.Click
        If DDLDept.SelectedValue <> "" Then
            If lbDept.Items.Count > 0 Then
                If Not lbDept.Items.Contains(lbDept.Items.FindByValue(DDLDept.SelectedValue)) Then
                    Dim objList As New ListItem
                    objList.Text = DDLDept.SelectedItem.Text : objList.Value = DDLDept.SelectedValue
                    lbDept.Items.Add(objList)
                End If
            Else
                Dim objList As New ListItem
                objList.Text = DDLDept.SelectedItem.Text : objList.Value = DDLDept.SelectedValue
                lbDept.Items.Add(objList)
            End If
        End If
    End Sub

    Protected Sub btnMinDept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMinDept.Click
        If lbDept.Items.Count > 0 Then
            Dim objList As ListItem = lbDept.SelectedItem
            lbDept.Items.Remove(objList)
        End If
    End Sub

    Protected Sub btnAddWH_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddWH.Click
        If DDLWarehouse.SelectedValue <> "" Then
            If lbWarehouse.Items.Count > 0 Then
                If Not lbWarehouse.Items.Contains(lbWarehouse.Items.FindByValue(DDLWarehouse.SelectedValue)) Then
                    Dim objList As New ListItem
                    objList.Text = DDLWarehouse.SelectedItem.Text : objList.Value = DDLWarehouse.SelectedValue
                    lbWarehouse.Items.Add(objList)
                End If
            Else
                Dim objList As New ListItem
                objList.Text = DDLWarehouse.SelectedItem.Text : objList.Value = DDLWarehouse.SelectedValue
                lbWarehouse.Items.Add(objList)
            End If
        End If
    End Sub

    Protected Sub btnMinWH_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMinWH.Click
        If lbWarehouse.Items.Count > 0 Then
            Dim objList As ListItem = lbWarehouse.SelectedItem
            lbWarehouse.Items.Remove(objList)
        End If
    End Sub

    Protected Sub DDLGroupBy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLGroupBy.SelectedIndexChanged
        For C1 As Integer = 0 To DDLSortBy.Items.Count - 1
            DDLSortBy.Items(C1).Enabled = True
        Next
        DDLSortBy.Items(DDLGroupBy.SelectedIndex).Enabled = False
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmMatUsageKIK.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not report Is Nothing Then
                If report.IsLoaded Then
                    report.Dispose()
                    report.Close()
                End If
            End If
        Catch ex As Exception
            report.Dispose()
            report.Close()
        End Try
    End Sub
#End Region

End Class
