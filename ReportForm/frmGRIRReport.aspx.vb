Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_GRIR
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        Return True
    End Function

    Private Function UpdateCheckedSupp() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSupp") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSupp")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListSupp.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSupp.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "suppoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblSupp") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function 'OK

    Private Function UpdateCheckedSuppView() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSuppView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSupp")
            Dim dtTbl2 As DataTable = Session("TblSuppView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListSupp.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSupp.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "suppoid=" & cbOid
                                dtView2.RowFilter = "suppoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblSupp") = dtTbl
                Session("TblSuppView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function 'OK

    Private Function UpdateCheckedMR() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMR") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMR")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMR.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMR.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "irmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMR") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function 'OK

    Private Function UpdateCheckedMRView() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMRView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMR")
            Dim dtTbl2 As DataTable = Session("TblMRView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMR.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMR.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "irmstoid=" & cbOid
                                dtView2.RowFilter = "irmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMR") = dtTbl
                Session("TblMRView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function 'OK
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub 'OK

    Private Sub BindListSupp()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, irm.suppoid, suppcode, suppname, suppaddr FROM QL_trnmrmst irm INNER JOIN QL_mstsupp s ON s.cmpcode=irm.cmpcode AND s.suppoid=irm.suppoid WHERE irm.cmpcode='" & CompnyCode & "' AND mrmststatus<>'In Process' ORDER BY suppcode"
        Session("TblSupp") = cKon.ambiltabel(sSql, "QL_mstsupp")
    End Sub 'OK

    Private Sub BindListMR()
        sSql = "SELECT 'False' AS checkvalue, mrmstoid irmstoid, mrdate AS sortdate, mrno irno, CONVERT(VARCHAR(10), mrdate, 101) irdate, mrmstnote irmstnote FROM QL_trnmrmst irm WHERE irm.cmpcode='" & CompnyCode & "' AND mrmststatus<>'In Process' ORDER BY sortdate DESC"
        Session("TblMR") = cKon.ambiltabel(sSql, "QL_trnirmst")
    End Sub 'OK

    Private Sub ShowReport(ByVal sType As String)
        If Not IsValidPeriod() Then
            Exit Sub
        End If
        Try
            If sType = "Print Excel" Then
                report.Load(Server.MapPath(folderReport & "rptGRIR.rpt"))
            Else
                report.Load(Server.MapPath(folderReport & "rptGRIR.rpt"))
            End If
            Dim sFilterSupplierIn As String = "", sFilterSupplierLike As String = ""
            If FilterSupplier.Text <> "" Then
                Dim sSupp() As String = FilterSupplier.Text.Split(";")
                For C1 As Integer = 0 To sSupp.Length - 1
                    If sSupp(C1) <> "" Then
                        If sSupp(C1).Contains("%") Then
                            sFilterSupplierLike &= "suppcode LIKE '" & sSupp(C1) & "' OR "
                        Else
                            sFilterSupplierIn &= "'" & sSupp(C1) & "', "
                        End If
                    End If
                Next
            End If
            
            Dim sFilterMRIn As String = "", sFilterMRLike As String = ""
            If FilterMR.Text <> "" Then
                Dim sMR() As String = FilterMR.Text.Split(";")
                For C1 As Integer = 0 To sMR.Length - 1
                    If sMR(C1) <> "" Then
                        If sMR(C1).Contains("%") Then
                            sFilterMRLike &= "irno LIKE '" & sMR(C1) & "' OR "
                        Else
                            sFilterMRIn &= "'" & sMR(C1) & "', "
                        End If
                    End If
                Next
            End If

            sSql = "DECLARE @dDate DATETIME; "
            sSql &= "SET @dDate = CAST('" & FilterPeriod2.Text & " 23:59:59' AS DATETIME); "
            sSql &= "SELECT * FROM ( "
            sSql &= "SELECT irm.cmpcode, irm.mrmstoid irmstoid, mrno irno, irm.updtime lastupdatetime, (CASE WHEN mrmststatus='Closed' THEN (CASE WHEN (iramount<>ISNULL(retamount, 0.0)+ISNULL(apamount, 0.0)) THEN 'Post' ELSE mrmststatus END) ELSE mrmststatus END) irmststatus, iramount, suppname, suppcode, ISNULL(retamount, 0.0) retamount, ISNULL(trnbelino, '') trnbelino, ISNULL(apamount, 0.0) apamount "
            sSql &= "FROM QL_trnmrmst irm INNER JOIN (SELECT ird.cmpcode, ird.mrmstoid, SUM(mrqty * mrvalueidr) iramount FROM QL_trnmrdtl ird GROUP BY ird.cmpcode, ird.mrmstoid) ird ON ird.cmpcode=irm.cmpcode AND ird.mrmstoid=irm.mrmstoid INNER JOIN QL_mstsupp s ON s.cmpcode=irm.cmpcode AND s.suppoid=irm.suppoid "
            sSql &= "LEFT JOIN (SELECT retm.cmpcode, retd.refoidmst, SUM(retqty * retvalueidr) retamount, retm.approvaldatetime updtime FROM QL_trnreturmst retm INNER JOIN QL_trnreturdtl retd ON retd.cmpcode=retm.cmpcode AND retd.retmstoid=retm.retmstoid WHERE retmststatus IN ('Approved', 'Closed') AND retmstres1='QL_trnmrmst' GROUP BY retm.cmpcode, retd.refoidmst, retm.approvaldatetime) ret ON ret.cmpcode=irm.cmpcode AND ret.refoidmst=irm.mrmstoid AND ret.updtime<=@dDate "
            sSql &= "LEFT JOIN (SELECT apm.cmpcode, apd.mrmstoid, SUM(trnbelidtlqty * (trnbelidtlamtnetto/trnbelidtlqty)) - trnbelidiscamt apamount, apm.trnbelidate updtime, trnbelino FROM QL_trnbelimst apm INNER JOIN QL_trnbelidtl apd ON apd.cmpcode=apm.cmpcode AND apd.trnbelimstoid=apm.trnbelimstoid WHERE trnbelimststatus IN ('Approved', 'Closed') GROUP BY apm.cmpcode, apd.mrmstoid, apm.trnbelidate, trnbelino, trnbelidiscamt) ap ON ap.cmpcode=irm.cmpcode AND ap.mrmstoid=irm.mrmstoid AND ap.updtime<=@dDate "
            sSql &= "WHERE irm.cmpcode='" & CompnyCode & "' AND irm.updtime<=@dDate AND mrmststatus<>'In Process' "
            sSql &= ") tblGRIR "
            sSql &= "WHERE irmststatus IN ('" & FilterDDLStatus.SelectedValue & "') "
            If sFilterSupplierIn <> "" Then
                sFilterSupplierIn = Left(sFilterSupplierIn, sFilterSupplierIn.Length - 2)
                sSql &= " AND suppcode IN (" & sFilterSupplierIn & ")"
            End If
            If sFilterSupplierLike <> "" Then
                sFilterSupplierLike = "(" & Left(sFilterSupplierLike, sFilterSupplierLike.Length - 4) & ")"
                sSql &= " AND " & sFilterSupplierLike
            End If
            If sFilterMRIn <> "" Then
                sFilterMRIn = Left(sFilterMRIn, sFilterMRIn.Length - 2)
                sSql &= " AND irno IN (" & sFilterMRIn & ")"
            End If
            If sFilterMRLike <> "" Then
                sFilterMRLike = "(" & Left(sFilterMRLike, sFilterMRLike.Length - 4) & ")"
                sSql &= " AND " & sFilterMRLike
            End If
            sSql &= "ORDER BY cmpcode, irno"

            Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_tblReport")
            report.SetDataSource(dtTbl)
            report.SetParameterValue("sCmpName", GetStrData("SELECT divname FROM QL_mstdivision WHERE divcode='" & Session("CompnyCode") & "'"))
            If sType <> "Print Excel" Then
                report.PrintOptions.PaperSize = PaperSize.PaperA4
            End If
            cProc.SetDBLogonForReport(report)

            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "rptName")
                report.Close()
                report.Dispose()
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "rptName")
                report.Close()
                report.Dispose()
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.ToString, 1)
        End Try
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmGRIRReport.aspx")
        End If
        If checkPagePermission("~\ReportForm\frmGRIRReport.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - GRIR Report"
        If Not Page.IsPostBack Then
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        End If
    End Sub 'OK

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListSupp") Is Nothing And Session("EmptyListSupp") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListSupp") Then
                Session("EmptyListSupp") = Nothing
                cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
            End If
        End If
        If Not Session("WarningListSupp") Is Nothing And Session("WarningListSupp") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListSupp") Then
                Session("WarningListSupp") = Nothing
                cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
            End If
        End If
        If Not Session("EmptyListMR") Is Nothing And Session("EmptyListMR") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMR") Then
                Session("EmptyListMR") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMR, pnlListMR, mpeListMR, True)
            End If
        End If
        If Not Session("WarningListMR") Is Nothing And Session("WarningListMR") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMR") Then
                Session("WarningListMR") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMR, pnlListMR, mpeListMR, True)
            End If
        End If
    End Sub 'OK

    Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupp.Click
        FilterDDLListSupp.SelectedIndex = -1 : FilterTextListSupp.Text = ""
        Session("TblSupp") = Nothing : Session("TblSuppView") = Nothing : gvListSupp.DataSource = Nothing : gvListSupp.DataBind()
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
    End Sub 'OK

    Protected Sub btnClearSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupp.Click
        FilterSupplier.Text = ""
    End Sub 'OK

    Protected Sub btnFindListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSupp.Click
        If Session("TblSupp") Is Nothing Then
            BindListSupp()
            If Session("TblSupp").Rows.Count <= 0 Then
                Session("EmptyListSupp") = "Supplier data can't be found!"
                showMessage(Session("EmptyListSupp"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListSupp.SelectedValue & " LIKE '%" & Tchar(FilterTextListSupp.Text) & "%'"
        If UpdateCheckedSupp() Then
            Dim dv As DataView = Session("TblSupp").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblSuppView") = dv.ToTable
                gvListSupp.DataSource = Session("TblSuppView")
                gvListSupp.DataBind()
                dv.RowFilter = ""
                mpeListSupp.Show()
            Else
                dv.RowFilter = ""
                Session("TblSuppView") = Nothing
                gvListSupp.DataSource = Session("TblSuppView")
                gvListSupp.DataBind()
                Session("WarningListSupp") = "Supplier data can't be found!"
                showMessage(Session("WarningListSupp"), 2)
            End If
        Else
            mpeListSupp.Show()
        End If
    End Sub 'OK

    Protected Sub btnViewAllListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListSupp.Click
        FilterDDLListSupp.SelectedIndex = -1 : FilterTextListSupp.Text = ""
        If Session("TblSupp") Is Nothing Then
            BindListSupp()
            If Session("TblSupp").Rows.Count <= 0 Then
                Session("EmptyListSupp") = "Supplier data can't be found!"
                showMessage(Session("EmptyListSupp"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedSupp() Then
            Dim dt As DataTable = Session("TblSupp")
            Session("TblSuppView") = dt
            gvListSupp.DataSource = Session("TblSuppView")
            gvListSupp.DataBind()
        End If
        mpeListSupp.Show()
    End Sub 'OK

    Protected Sub gvListSupp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSupp.PageIndexChanging
        If UpdateCheckedSuppView() Then
            gvListSupp.PageIndex = e.NewPageIndex
            gvListSupp.DataSource = Session("TblSuppView")
            gvListSupp.DataBind()
        End If
        mpeListSupp.Show()
    End Sub 'OK

    Protected Sub lkbAddToListListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListSupp.Click
        If Not Session("TblSupp") Is Nothing Then
            If UpdateCheckedSupp() Then
                Dim dtTbl As DataTable = Session("TblSupp")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If FilterSupplier.Text <> "" Then
                            FilterSupplier.Text &= ";" + vbCrLf + dtView(C1)("suppcode")
                        Else
                            FilterSupplier.Text = dtView(C1)("suppcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
                Else
                    Session("WarningListSupp") = "Please select Supplier to add to list!"
                    showMessage(Session("WarningListSupp"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListSupp") = "Please show some Supplier data first!"
            showMessage(Session("WarningListSupp"), 2)
            Exit Sub
        End If
    End Sub 'OK

    Protected Sub lkbCloseListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListSupp.Click
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub 'OK

    Protected Sub btnSearchMR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMR.Click
        FilterDDLListMR.SelectedIndex = -1 : FilterTextListMR.Text = ""
        Session("TblMR") = Nothing : Session("TblMRView") = Nothing : gvListMR.DataSource = Nothing : gvListMR.DataBind()
        cProc.SetModalPopUpExtender(btnHideListMR, pnlListMR, mpeListMR, True)
    End Sub 'OK

    Protected Sub btnClearMR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearMR.Click
        FilterMR.Text = ""
    End Sub 'OK

    Protected Sub btnFindListMR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMR.Click
        If Session("TblMR") Is Nothing Then
            BindListMR()
            If Session("TblMR").Rows.Count <= 0 Then
                Session("EmptyListMR") = "MR data can't be found!"
                showMessage(Session("EmptyListMR"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListMR.SelectedValue & " LIKE '%" & Tchar(FilterTextListMR.Text) & "%'"
        If UpdateCheckedMR() Then
            Dim dv As DataView = Session("TblMR").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMRView") = dv.ToTable
                gvListMR.DataSource = Session("TblMRView")
                gvListMR.DataBind()
                dv.RowFilter = ""
                mpeListMR.Show()
            Else
                dv.RowFilter = ""
                Session("TblMRView") = Nothing
                gvListMR.DataSource = Session("TblMRView")
                gvListMR.DataBind()
                Session("WarningListMR") = "MR data can't be found!"
                showMessage(Session("WarningListMR"), 2)
            End If
        Else
            mpeListMR.Show()
        End If
    End Sub 'OK

    Protected Sub btnViewAllListMR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListMR.Click
        FilterDDLListMR.SelectedIndex = -1 : FilterTextListMR.Text = ""
        If Session("TblMR") Is Nothing Then
            BindListMR()
            If Session("TblMR").Rows.Count <= 0 Then
                Session("EmptyListMR") = "MR data can't be found!"
                showMessage(Session("EmptyListMR"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMR() Then
            Dim dt As DataTable = Session("TblMR")
            Session("TblMRView") = dt
            gvListMR.DataSource = Session("TblMRView")
            gvListMR.DataBind()
        End If
        mpeListMR.Show()
    End Sub 'OK

    Protected Sub gvListMR_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMR.PageIndexChanging
        If UpdateCheckedMRView() Then
            gvListMR.PageIndex = e.NewPageIndex
            gvListMR.DataSource = Session("TblMRView")
            gvListMR.DataBind()
        End If
        mpeListMR.Show()
    End Sub 'OK

    Protected Sub lkbAddToListListMR_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMR.Click
        If Not Session("TblMR") Is Nothing Then
            If UpdateCheckedMR() Then
                Dim dtTbl As DataTable = Session("TblMR")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If FilterMR.Text <> "" Then
                            If dtView(C1)("irno") <> "" Then
                                FilterMR.Text &= ";" + vbCrLf + dtView(C1)("irno")
                            End If
                        Else
                            If dtView(C1)("irno") <> "" Then
                                FilterMR.Text &= dtView(C1)("irno")
                            End If
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListMR, pnlListMR, mpeListMR, False)
                Else
                    Session("WarningListMR") = "Please select MR to add to list!"
                    showMessage(Session("WarningListMR"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMR") = "Please show some MR data first!"
            showMessage(Session("WarningListMR"), 2)
            Exit Sub
        End If
    End Sub 'OK

    Protected Sub lkbCloseListMR_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMR.Click
        cProc.SetModalPopUpExtender(btnHideListMR, pnlListMR, mpeListMR, False)
    End Sub 'OK

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub 'OK

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub 'OK

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub 'OK

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmGRIRReport.aspx?awal=true")
    End Sub 'OK

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub 'OK

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub 'OK

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub 'OK

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not report Is Nothing Then
                If report.IsLoaded Then
                    report.Dispose()
                    report.Close()
                End If
            End If
        Catch ex As Exception
            report.Dispose()
            report.Close()
        End Try
    End Sub 'OK
#End Region

End Class
