Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_MarkReg
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Dim Typem1 As String = ""
    Dim Typem2 As String = ""
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function FillDDLWithALL(ByRef oDDLObject As DropDownList, ByVal sSql As String) As Boolean
        ' To fill data in dropdown list, return true when all data exist
        FillDDLWithALL = True
        oDDLObject.Items.Clear()
        oDDLObject.Items.Add("ALL")
        oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = "ALL"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            oDDLObject.Items.Add(xreader.GetValue(1))
            oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()
        ' Cek if all data exits
        If oDDLObject.Items.Count = 0 Then
            FillDDLWithALL = False
        End If
        Return FillDDLWithALL
    End Function

    Private Function CheckType()
        If TypeMat.SelectedValue = "raw" Then
            Typem1 = "raw"
            Typem2 = "matraw"
        ElseIf TypeMat.SelectedValue = "gen" Then
            Typem1 = "gen"
            Typem2 = "matgen"
        ElseIf TypeMat.SelectedValue = "sparepart" Then
            Typem1 = "sp"
            Typem2 = "sparepart"
        ElseIf TypeMat.SelectedValue = "item" Then
            Typem1 = "item"
            Typem2 = "item"
        End If
    End Function

    Private Function UpdateCheckedCust() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblCust") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCust")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListCust.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListCust.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "custoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblCust") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedCust2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCust")
            Dim dtTbl2 As DataTable = Session("TblCustView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListCust.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListCust.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "custoid=" & cbOid
                                dtView2.RowFilter = "custoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblCust") = dtTbl
                Session("TblCustView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedDriv() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblDriv") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDriv")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListDriv.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListDriv.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "drivoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblDriv") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedDriv2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblDrivView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDriv")
            Dim dtTbl2 As DataTable = Session("TblDrivView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListDriv.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListDriv.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "drivoid=" & cbOid
                                dtView2.RowFilter = "drivoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblDriv") = dtTbl
                Session("TblDrivView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedVhc() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblVhc") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblVhc")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListVhc.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListVhc.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "vhcoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblVhc") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedVhc2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblVhcView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblVhc")
            Dim dtTbl2 As DataTable = Session("TblVhcView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListVhc.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListVhc.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "vhcoid=" & cbOid
                                dtView2.RowFilter = "vhcoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblVhc") = dtTbl
                Session("TblVhcView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedDO() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblDO") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDO")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListDO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListDO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "doitemmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblDO") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedDO2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblDOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDO")
            Dim dtTbl2 As DataTable = Session("TblDOView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListDO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListDO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "doitemmstoid=" & cbOid
                                dtView2.RowFilter = "doitemmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblDO") = dtTbl
                Session("TblDOView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedShipment() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblShipment") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblShipment")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListShipment.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListShipment.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "shipmentitemmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblShipment") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedShipment2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblShipmentView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblShipment")
            Dim dtTbl2 As DataTable = Session("TblShipmentView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListShipment.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListShipment.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "shipmentitemmstoid=" & cbOid
                                dtView2.RowFilter = "shipmentitemmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblShipment") = dtTbl
                Session("TblShipmentView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                dtView2.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindListCust()
        'CheckType()
        sSql = "SELECT 'False' AS checkvalue, custoid, custcode, custname, custaddr FROM QL_mstcust WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' "
        If DDLBusUnit.SelectedValue <> "ALL" Then
            sSql &= " AND custoid IN (SELECT custoid from QL_trnjualmst shm WHERE shm.cmpcode='" & DDLBusUnit.SelectedValue & "' "
        Else
            sSql &= " AND custoid IN (SELECT custoid from QL_trnjualmst shm WHERE shm.cmpcode LIKE '%' "
        End If

        If FilterDDLStatus.SelectedValue = "In Process" Then
            sSql &= " AND shm.trnjualstatus IN ('In Process','In Approval','Revised')"
        ElseIf FilterDDLStatus.SelectedValue = "Approved" Then
            sSql &= " AND shm.trnjualstatus IN ('Approved','Closed')"
        ElseIf FilterDDLStatus.SelectedValue = "Rejected" Then
            sSql &= " AND shm.trnjualstatus IN ('Rejected','Cancel')"
        End If

        'If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
        '    If IsValidPeriod() Then
        '        sSql &= " AND " & FilterDDLDate.SelectedValue & ">='" & FilterPeriod1.Text & " 00:00:00' AND " & FilterDDLDate.SelectedValue & "<='" & FilterPeriod2.Text & " 23:59:59'"
        '    Else
        '        Exit Sub
        '    End If
        'End If

        sSql &= ") ORDER BY custcode"
        Session("TblCust") = cKon.ambiltabel(sSql, "QL_mstCust")
    End Sub

    Private Sub BindListDriver()
        sSql = "SELECT 'False' AS checkvalue, drivoid, drivcode, drivname, drivaddr FROM QL_mstdriver WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' "
        If DDLBusUnit.SelectedValue <> "ALL" Then
            sSql &= " AND drivoid IN (SELECT drivoid from QL_trnshipmentitemmst shm INNER JOIN QL_mstcust c ON shm.custoid=c.custoid AND c.activeflag='ACTIVE' WHERE shm.cmpcode='" & DDLBusUnit.SelectedValue & "' "
        Else
            sSql &= " AND drivoid IN (SELECT drivoid from QL_trnshipmentitemmst shm INNER JOIN QL_mstcust c ON shm.custoid=c.custoid AND c.activeflag='ACTIVE' WHERE shm.cmpcode LIKE '%' "
        End If

        If custcode.Text <> "" Then
            Dim scode() As String = Split(custcode.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " c.custcode = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If FilterDDLStatus.SelectedValue = "In Process" Then
            sSql &= " AND shm.shipmentitemmststatus IN ('In Process','In Approval','Revised')"
        ElseIf FilterDDLStatus.SelectedValue = "Approved" Then
            sSql &= " AND shm.shipmentitemmststatus IN ('Approved','Closed')"
        ElseIf FilterDDLStatus.SelectedValue = "Rejected" Then
            sSql &= " AND shm.shipmentitemmststatus IN ('Rejected','Cancel')"
        End If

        If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
            If IsValidPeriod() Then
                sSql &= " AND " & FilterDDLDate.SelectedValue & ">='" & FilterPeriod1.Text & " 00:00:00' AND " & FilterDDLDate.SelectedValue & "<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If

        sSql &= ") ORDER BY drivcode"
        Session("TblDriv") = cKon.ambiltabel(sSql, "QL_mstdriver")
    End Sub

    Private Sub BindListVehicle()
        sSql = "SELECT 'False' AS checkvalue, vhcoid, vhccode, vhcno, vhcdesc FROM QL_mstvehicle WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' "
        If DDLBusUnit.SelectedValue <> "ALL" Then
            sSql &= " AND vhcoid IN (SELECT vhcoid from QL_trnshipmentitemmst shm INNER JOIN QL_mstcust c ON shm.custoid=c.custoid AND c.activeflag='ACTIVE' WHERE shm.cmpcode='" & DDLBusUnit.SelectedValue & "' "
        Else
            sSql &= " AND vhcoid IN (SELECT vhcoid from QL_trnshipmentitemmst shm INNER JOIN QL_mstcust c ON shm.custoid=c.custoid AND c.activeflag='ACTIVE' WHERE shm.cmpcode LIKE '%' "
        End If

        If custcode.Text <> "" Then
            Dim scode() As String = Split(custcode.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " c.custcode = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If FilterDDLStatus.SelectedValue = "In Process" Then
            sSql &= " AND shm.shipmentitemmststatus IN ('In Process','In Approval','Revised')"
        ElseIf FilterDDLStatus.SelectedValue = "Approved" Then
            sSql &= " AND shm.shipmentitemmststatus IN ('Approved','Closed')"
        ElseIf FilterDDLStatus.SelectedValue = "Rejected" Then
            sSql &= " AND shm.shipmentitemmststatus IN ('Rejected','Cancel')"
        End If

        If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
            If IsValidPeriod() Then
                sSql &= " AND " & FilterDDLDate.SelectedValue & ">='" & FilterPeriod1.Text & " 00:00:00' AND " & FilterDDLDate.SelectedValue & "<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If

        sSql &= ") ORDER BY vhccode"
        Session("TblVhc") = cKon.ambiltabel(sSql, "QL_mstvehicle")
    End Sub

    Private Sub BindListDO()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, dom.doitemmstoid, dom.doitemno, dom.doitemdate, CONVERT(VARCHAR(10), dom.doitemdate, 101) AS dodate, dom.doitemmststatus, dom.doitemmstnote, dom.doitemcustpono FROM QL_trndoitemmst dom "
        If DDLBusUnit.SelectedValue <> "ALL" Then
            sSql &= " WHERE dom.doitemmstoid IN (SELECT shd.doitemmstoid from QL_trnshipmentitemmst shm INNER JOIN QL_trnshipmentitemdtl shd ON shm.shipmentitemmstoid=shd.shipmentitemmstoid INNER JOIN QL_mstcust c ON shm.custoid=c.custoid AND c.activeflag='ACTIVE' WHERE shm.cmpcode='" & DDLBusUnit.SelectedValue & "' "
        Else
            sSql &= " WHERE dom.doitemmstoid IN (SELECT shd.doitemmstoid from QL_trnshipmentitemmst shm INNER JOIN QL_trnshipmentitemdtl shd ON shm.shipmentitemmstoid=shd.shipmentitemmstoid INNER JOIN QL_mstcust c ON shm.custoid=c.custoid AND c.activeflag='ACTIVE' WHERE shm.cmpcode LIKE '%' "
        End If

        If custcode.Text <> "" Then
            Dim scode() As String = Split(custcode.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " c.custcode = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If drivcode.Text <> "" Then
            sSql &= " AND shm.shipmentitemmstoid IN (SELECT shm2.shipmentitemmstoid FROM QL_trnshipmentitemmst shm2 INNER JOIN QL_mstdriver driv ON shm2.drivoid=driv.drivoid AND driv.activeflag='ACTIVE' "
            Dim sDrivCode() As String = Split(drivcode.Text, ";")
            sSql &= " WHERE "
            For c1 As Integer = 0 To sDrivCode.Length - 1
                sSql &= " driv.drivcode = '" & Tchar(sDrivCode(c1)) & "'"
                If c1 < sDrivCode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If vhccode.Text <> "" Then
            sSql &= " AND shm.shipmentitemmstoid IN (SELECT shm2.shipmentitemmstoid FROM QL_trnshipmentitemmst shm2 INNER JOIN QL_mstvehicle vhc ON shm.vhcoid=vhc.vhcoid AND vhc.activeflag='ACTIVE' "
            Dim sVhcCode() As String = Split(vhccode.Text, ";")
            sSql &= " WHERE "
            For c1 As Integer = 0 To sVhcCode.Length - 1
                sSql &= " vhc.vhccode = '" & Tchar(sVhcCode(c1)) & "'"
                If c1 < sVhcCode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If FilterDDLStatus.SelectedValue = "In Process" Then
            sSql &= " AND shm.shipmentitemmststatus IN ('In Process','In Approval','Revised')"
        ElseIf FilterDDLStatus.SelectedValue = "Approved" Then
            sSql &= " AND shm.shipmentitemmststatus IN ('Approved','Closed')"
        ElseIf FilterDDLStatus.SelectedValue = "Rejected" Then
            sSql &= " AND shm.shipmentitemmststatus IN ('Rejected','Cancel')"
        End If

        If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
            If IsValidPeriod() Then
                sSql &= " AND " & FilterDDLDate.SelectedValue & ">='" & FilterPeriod1.Text & " 00:00:00' AND " & FilterDDLDate.SelectedValue & "<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If

        sSql &= ") ORDER BY dom.doitemdate DESC, dom.doitemmstoid DESC"
        Session("TblDO") = cKon.ambiltabel(sSql, "QL_doitemmst")
    End Sub

    Private Sub BindListShipment()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, shm.shipmentitemmstoid, shm.shipmentitemno, shm.shipmentitemdate, CONVERT(VARCHAR(10), shm.shipmentitemdate, 101) AS shipmentdate, c.custname AS custname, shm.shipmentitemmststatus, shm.shipmentitemmstnote FROM QL_trnshipmentitemmst shm INNER JOIN QL_trnshipmentitemdtl shd ON shm.shipmentitemmstoid=shd.shipmentitemmstoid INNER JOIN QL_mstcust c ON shm.custoid=c.custoid AND c.activeflag='ACTIVE' "

        If DDLBusUnit.SelectedValue <> "ALL" Then
            sSql &= "WHERE shm.cmpcode='" & DDLBusUnit.SelectedValue & "'"
        Else
            sSql &= "WHERE shm.cmpcode LIKE '%%'"
        End If

        If custcode.Text <> "" Then
            Dim scode() As String = Split(custcode.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " c.custcode = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If drivcode.Text <> "" Then
            sSql &= " AND shm.shipmentitemmstoid IN (SELECT shm2.shipmentitemmstoid FROM QL_trnshipmentitemmst shm2 INNER JOIN QL_mstdriver driv ON shm2.drivoid=driv.drivoid AND driv.activeflag='ACTIVE' "
            Dim sDrivCode() As String = Split(drivcode.Text, ";")
            sSql &= " WHERE "
            For c1 As Integer = 0 To sDrivCode.Length - 1
                sSql &= " driv.drivcode = '" & Tchar(sDrivCode(c1)) & "'"
                If c1 < sDrivCode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If vhccode.Text <> "" Then
            sSql &= " AND shm.shipmentitemmstoid IN (SELECT shm2.shipmentitemmstoid FROM QL_trnshipmentitemmst shm2 INNER JOIN QL_mstvehicle vhc ON shm.vhcoid=vhc.vhcoid AND vhc.activeflag='ACTIVE' "
            Dim sVhcCode() As String = Split(vhccode.Text, ";")
            sSql &= " WHERE "
            For c1 As Integer = 0 To sVhcCode.Length - 1
                sSql &= " vhc.vhccode = '" & Tchar(sVhcCode(c1)) & "'"
                If c1 < sVhcCode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If FilterDDLType.SelectedValue = "Detail" Then
            If dono.Text <> "" Then
                sSql &= " AND shd.doitemmstoid IN (SELECT shd2.doitemmstoid FROM QL_trnshipmentitemdtl shd2 INNER JOIN QL_trndoitemmst dom ON dom.doitemmstoid=shd2.doitemmstoid "
                Dim sDOno() As String = Split(dono.Text, ";")
                sSql &= " WHERE "
                For c1 As Integer = 0 To sDOno.Length - 1
                    sSql &= " dom.doitemno LIKE '%" & Tchar(sDOno(c1)) & "%'"
                    If c1 < sDOno.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            End If
            If lbWarehouse.Items.Count > 0 Then
                Dim sOid As String = ""
                For C1 As Integer = 0 To lbWarehouse.Items.Count - 1
                    sOid &= lbWarehouse.Items(C1).Value & ","
                Next
                If sOid <> "" Then
                    sOid = Left(sOid, sOid.Length - 1)
                    sSql &= " AND shd.shipmentitemwhoid IN (" & sOid & ")"
                End If
            End If
        End If

        If FilterDDLStatus.SelectedValue = "In Process" Then
            sSql &= " AND shm.shipmentitemmststatus IN ('In Process','In Approval','Revised')"
        ElseIf FilterDDLStatus.SelectedValue = "Approved" Then
            sSql &= " AND shm.shipmentitemmststatus IN ('Approved','Closed')"
        ElseIf FilterDDLStatus.SelectedValue = "Rejected" Then
            sSql &= " AND shm.shipmentitemmststatus IN ('Rejected','Cancel')"
        End If

        If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
            If IsValidPeriod() Then
                sSql &= " AND " & FilterDDLDate.SelectedValue & ">='" & FilterPeriod1.Text & " 00:00:00' AND " & FilterDDLDate.SelectedValue & "<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If

        sSql &= " ORDER BY shm.shipmentitemdate DESC, shm.shipmentitemmstoid DESC"
        Session("TblShipment") = cKon.ambiltabel(sSql, "QL_trnshipmentitemmst")
    End Sub

    Private Sub BindListMat()
        CheckType()

        sSql = "SELECT DISTINCT 'False' AS checkvalue, shd.itemoid, i.itemlongdescription, i.itemcode, g2.gendesc unit FROM QL_trnjualdtl shd INNER JOIN QL_trnjualmst shm ON shm.cmpcode=shd.cmpcode AND shm.trnjualmstoid=shd.trnjualmstoid INNER JOIN QL_mstitem i ON i.itemoid=shd.itemoid INNER JOIN QL_mstgen g2 ON g2.genoid=shd.trnjualdtlunitoid INNER JOIN QL_mstcust c ON shm.custoid=c.custoid AND c.activeflag='ACTIVE' "

        If DDLBusUnit.SelectedValue <> "ALL" Then
            sSql &= "WHERE shm.cmpcode='" & DDLBusUnit.SelectedValue & "'"
        Else
            sSql &= "WHERE shm.cmpcode LIKE '%%'"
        End If

        If custcode.Text <> "" Then
            Dim scode() As String = Split(custcode.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " c.custcode = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        'If drivcode.Text <> "" Then
        '    sSql &= " AND shm.shipment" & Typem1 & "mstoid IN (SELECT shm2.shipment" & Typem1 & "mstoid FROM QL_trnshipment" & Typem1 & "mst shm2 INNER JOIN QL_mstdriver driv ON shm2.drivoid=driv.drivoid AND driv.activeflag='ACTIVE' "
        '    Dim sDrivCode() As String = Split(drivcode.Text, ";")
        '    sSql &= " WHERE "
        '    For c1 As Integer = 0 To sDrivCode.Length - 1
        '        sSql &= " driv.drivcode = '" & Tchar(sDrivCode(c1)) & "'"
        '        If c1 < sDrivCode.Length - 1 Then
        '            sSql &= " OR "
        '        End If
        '    Next
        '    sSql &= ")"
        'End If

        'If vhccode.Text <> "" Then
        '    sSql &= " AND shm.shipment" & Typem1 & "mstoid IN (SELECT shm2.shipment" & Typem1 & "mstoid FROM QL_trnshipment" & Typem1 & "mst shm2 INNER JOIN QL_mstvehicle vhc ON shm.vhcoid=vhc.vhcoid AND vhc.activeflag='ACTIVE' "
        '    Dim sVhcCode() As String = Split(vhccode.Text, ";")
        '    sSql &= " WHERE "
        '    For c1 As Integer = 0 To sVhcCode.Length - 1
        '        sSql &= " vhc.vhccode = '" & Tchar(sVhcCode(c1)) & "'"
        '        If c1 < sVhcCode.Length - 1 Then
        '            sSql &= " OR "
        '        End If
        '    Next
        '    sSql &= ")"
        'End If

        'If FilterDDLType.SelectedValue = "Detail" Then
        '    If dono.Text <> "" Then
        '        sSql &= " AND shm.shipment" & Typem1 & "mstoid IN (SELECT shd2.shipment" & Typem1 & "mstoid FROM QL_trnshipment" & Typem1 & "dtl shd2 INNER JOIN QL_trndo" & Typem1 & "mst dom ON shd2.do" & Typem1 & "mstoid=dom.do" & Typem1 & "mstoid"
        '        Dim sDOno() As String = Split(dono.Text, ";")
        '        sSql &= " WHERE "
        '        For c1 As Integer = 0 To sDOno.Length - 1
        '            sSql &= " dom.do" & Typem1 & "no LIKE '%" & Tchar(sDOno(c1)) & "%'"
        '            If c1 < sDOno.Length - 1 Then
        '                sSql &= " OR "
        '            End If
        '        Next
        '        sSql &= ")"
        '    End If
        '    If lbWarehouse.Items.Count > 0 Then
        '        Dim sOid As String = ""
        '        For C1 As Integer = 0 To lbWarehouse.Items.Count - 1
        '            sOid &= lbWarehouse.Items(C1).Value & ","
        '        Next
        '        If sOid <> "" Then
        '            sOid = Left(sOid, sOid.Length - 1)
        '            sSql &= " AND shd.shipment" & Typem1 & "whoid IN (" & sOid & ")"
        '        End If
        '    End If
        'End If

        'If shipmentno.Text <> "" Then
        '    If DDLShipmentNo.SelectedValue = "Shipment No." Then
        '        Dim sShipmentno() As String = Split(shipmentno.Text, ";")
        '        sSql &= " AND ("
        '        For c1 As Integer = 0 To sShipmentno.Length - 1
        '            sSql &= " shm.shipment" & Typem1 & "no LIKE '%" & Tchar(sShipmentno(c1)) & "%'"
        '            If c1 < sShipmentno.Length - 1 Then
        '                sSql &= " OR "
        '            End If
        '        Next
        '        sSql &= ")"
        '    Else
        '        Dim sShipmentno() As String = Split(shipmentno.Text, ";")
        '        sSql &= " AND ("
        '        For c1 As Integer = 0 To sShipmentno.Length - 1
        '            sSql &= " shm.shipment" & Typem1 & "mstoid = " & ToDouble(sShipmentno(c1))
        '            If c1 < sShipmentno.Length - 1 Then
        '                sSql &= " OR "
        '            End If
        '        Next
        '        sSql &= ")"
        '    End If
        'End If

        If FilterDDLStatus.SelectedValue = "In Process" Then
            sSql &= " AND shm.trnjualstatus IN ('In Process','In Approval','Revised')"
        ElseIf FilterDDLStatus.SelectedValue = "Approved" Then
            sSql &= " AND shm.trnjualstatus IN ('Approved','Closed')"
        ElseIf FilterDDLStatus.SelectedValue = "Rejected" Then
            sSql &= " AND shm.trnjualstatus IN ('Rejected','Cancel')"
        End If

        'If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
        '    If IsValidPeriod() Then
        '        sSql &= " AND " & FilterDDLDate.SelectedValue & ">='" & FilterPeriod1.Text & " 00:00:00' AND " & FilterDDLDate.SelectedValue & "<='" & FilterPeriod2.Text & " 23:59:59'"
        '    Else
        '        Exit Sub
        '    End If
        'End If

        Session("TblMat") = cKon.ambiltabel(sSql, "QL_mstitem")
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        Dim sWhere As String = ""
        Dim rptName As String = ""
        Dim Dtl As String = ""
        Dim Join As String = ""
        Dim period1 As String
        Dim period2 As String
        Try
            If sType = "Print Excel" Then
                'report.Load(Server.MapPath(folderReport & "rptMarkRegisterXls.rpt"))
                report.Load(Server.MapPath(folderReport & "rptMarkRegister.rpt"))
            Else
                report.Load(Server.MapPath(folderReport & "rptMarkRegister.rpt"))
                'If DDLGrouping.SelectedValue = "v.vhccode" Then
                '    report.Load(Server.MapPath(folderReport & "rptShipment_DtlVhcPdf2.rpt"))
                'ElseIf DDLGrouping.SelectedValue = "c.custname" Then
                '    report.Load(Server.MapPath(folderReport & "rptShipment_DtlMatCodePdf2.rpt"))
                'ElseIf DDLGrouping.SelectedValue = "i.itemcode" Then
                '    report.Load(Server.MapPath(folderReport & "rptShipment_DtlMatCodePdf2.rpt"))
                'Else
                '    report.Load(Server.MapPath(folderReport & "rptShipment_DtlWhPdf2.rpt"))
                'End If
            End If
            rptName = "MarketingRegisterReport"
            'End If
            'CheckType()

            'If FilterDDLType.SelectedValue = "Detail" Then
            '    Dtl = " , shipment" & Typem1 & "dtlseq [No.], " & Typem2 & "code [Code], " & Typem2 & "longdesc [Material], " & Typem2 & "note [SKU], ISNULL([SO No.],'') AS [SO No.], ISNULL((dom.do" & Typem1 & "no),'') AS [DO No.], shipment" & Typem1 & "qty [Qty], g2.gendesc [Unit], g3.gendesc [Warehouse], shipment" & Typem1 & "dtlnote [Detail Note], 0 [Pack Volume FG] "
            '    'With Price
            '    Dtl &= " , (SELECT ISNULL(((so" & Typem1 & "dtlnetto+so" & Typem1 & "dtldiscamt)/so" & Typem1 & "qty)*shipment" & Typem1 & "qty,0.0) FROM QL_trndo" & Typem1 & "dtl dod2 INNER JOIN QL_trnso" & Typem1 & "dtl sod ON dod2.so" & Typem1 & "dtloid=sod.so" & Typem1 & "dtloid WHERE shd.do" & Typem1 & "dtloid=dod2.do" & Typem1 & "dtloid) AS [Value], (SELECT ISNULL((so" & Typem1 & "dtldiscamt),0.0) FROM QL_trndo" & Typem1 & "dtl dod2 INNER JOIN QL_trnso" & Typem1 & "dtl sod ON dod2.so" & Typem1 & "dtloid=sod.so" & Typem1 & "dtloid WHERE shd.do" & Typem1 & "dtloid=dod2.do" & Typem1 & "dtloid) AS [Discount], so" & Typem1 & "taxtype AS [Taxtype], shm.periodacctg AS [Period], fakturPajak "

            '    Join = " INNER JOIN QL_trnshipment" & Typem1 & "dtl shd ON shd.cmpcode=shm.cmpcode AND shd.shipment" & Typem1 & "mstoid=shm.shipment" & Typem1 & "mstoid INNER JOIN QL_trndo" & Typem1 & "mst dom ON dom.do" & Typem1 & "mstoid=shd.do" & Typem1 & "mstoid INNER JOIN QL_mst" & Typem2 & " i ON i." & Typem2 & "oid=shd." & Typem2 & "oid INNER JOIN QL_mstgen g2 ON g2.genoid=shipment" & Typem1 & "unitoid INNER JOIN QL_mstgen g3 ON g3.genoid=shipment" & Typem1 & "whoid "

            '    Join &= "INNER JOIN (SELECT dod.do" & Typem1 & "mstoid, dod.do" & Typem1 & "dtloid, som.so" & Typem1 & "mstoid, som.so" & Typem1 & "no [SO No.] FROM QL_trnso" & Typem1 & "mst som INNER JOIN QL_trndo" & Typem1 & "dtl dod ON dod.so" & Typem1 & "mstoid=som.so" & Typem1 & "mstoid) AS tblSO ON tblSO.do" & Typem1 & "mstoid=shd.do" & Typem1 & "mstoid AND shd.do" & Typem1 & "dtloid=tblSO.do" & Typem1 & "dtloid "

            '    Join &= "inner join QL_trndo" & Typem1 & "dtl dodtl on dodtl.do" & Typem1 & "mstoid=dom.do" & Typem1 & "mstoid inner join QL_trnso" & Typem1 & "mst somst on somst.so" & Typem1 & "mstoid=dodtl.so" & Typem1 & "mstoid LEFT JOIN ql_trnar" & Typem1 & "dtl ard on ard.shipment" & Typem1 & "mstoid= shm.shipment" & Typem1 & "mstoid LEFT JOIN QL_trnar" & Typem1 & "mst arm on arm.ar" & Typem1 & "mstoid = ard.ar" & Typem1 & "mstoid "

            'Else
            '    'Price
            '    Dtl &= " , (SELECT SUM(ISNULL((so" & Typem1 & "dtlnetto/so" & Typem1 & "qty)*shipment" & Typem1 & "qty,0.0)) FROM QL_trnshipment" & Typem1 & "dtl shd INNER JOIN QL_trndo" & Typem1 & "dtl dod ON dod.do" & Typem1 & "dtloid=shd.do" & Typem1 & "dtloid INNER JOIN QL_trnso" & Typem1 & "dtl sod ON sod.so" & Typem1 & "dtloid=dod.so" & Typem1 & "dtloid WHERE shd.shipment" & Typem1 & "mstoid=shm.shipment" & Typem1 & "mstoid) AS [Total Value] "
            'End If
            'sSql = " SELECT distinct (SELECT div.divname FROM QL_mstdivision div WHERE shm.cmpcode = div.cmpcode) AS [Business Unit], shm.cmpcode [CMPCODE], CONVERT(VARCHAR(20), shm.shipment" & Typem1 & "mstoid) [Draft No.], shm.shipment" & Typem1 & "mstoid [ID], shipment" & Typem1 & "date [Date], shipment" & Typem1 & "no [Shipment No.], custname [Customer], custcode [Customer Code], currcode [Currency], shipment" & Typem1 & "mstnote [Header Note], shipment" & Typem1 & "mststatus [Status], shm.approvaluser [Approval User], shm.approvaldatetime [Approval Date], shm.createuser [Create User], shm.createtime [Create Date], shipment" & Typem1 & "eta [ETA], shipment" & Typem1 & "etd [ETD], shipment" & Typem1 & "contno [Container No.], shipment" & Typem1 & "portship [Port Ship], shipment" & Typem1 & "portdischarge [Port Discharge], shipment" & Typem1 & "nopol [VehiclePolice No.], ISNULL(driv.drivname,'') [Driver Name], ISNULL(v.vhcdesc,'') [Vehicle], 'FG' AS [Shipment Type],0 AS [Grand Total CBF] " & Dtl & " FROM QL_trnshipment" & Typem1 & "mst shm INNER JOIN QL_mstcust c ON c.custoid=shm.custoid INNER JOIN QL_mstcurr curr ON curr.curroid=shm.curroid LEFT JOIN QL_mstdriver driv ON shm.drivoid=driv.drivoid LEFT JOIN QL_mstvehicle v ON shm.vhcoid=v.vhcoid " & Join & " "

            sSql = "SELECT distinct (SELECT div.divname FROM QL_mstdivision div WHERE shm.cmpcode = div.cmpcode) AS [Business Unit], shm.cmpcode [CMPCODE], CONVERT(VARCHAR(20), shm.trnjualmstoid) [Draft No.], shm.trnjualmstoid [ID], trnjualdate [Date], trnjualno [Shipment No.], c.custname [Customer], custcode [Customer Code], currcode [Currency], trnjualnote [Header Note], trnjualstatus [Status], '' [Approval User], '' [Approval Date], shm.crtuser [Create User], shm.crttime [Create Date], '' [ETA], shm.trnjualdateetd [ETD], '' [Container No.], '' [Port Ship], '' [Port Discharge],'' [VehiclePolice No.], g1.gendesc [Driver Name], '' [Vehicle], shm.trnjualtype AS [Shipment Type],0 AS [Grand Total CBF], trnjualdtlseq [No.], itemcode [Code], itemlongdescription [Material], trnjualdtlnote [SKU], ISNULL([SO No.],'') AS [SO No.], ISNULL((dom.dono),'') AS [DO No.], trnjualdtlqty [Qty], g2.gendesc [Unit], g3.gendesc [Warehouse], trnjualdtlnote [Detail Note], 0 [Pack Volume FG], (SELECT ISNULL(((sodtlnetto+(sodtldiscamt1+sodtldiscamt2+sodtldiscamt3))/sodtlqty)*doqty,0.0) FROM QL_trndodtl dod2 INNER JOIN QL_trnsodtl sod ON dod2.sodtloid=sod.sodtloid WHERE shd.dodtloid=dod2.dodtloid) AS [Value], (SELECT ISNULL((sodtldiscamt1+sodtldiscamt2+sodtldiscamt3),0.0) FROM QL_trndodtl dod2 INNER JOIN QL_trnsodtl sod ON dod2.sodtloid=sod.sodtloid WHERE shd.dodtloid=dod2.dodtloid) AS [Discount], sotaxtype AS [Taxtype], shm.periodacctg AS [Period], shm.nofakturpajak AS fakturPajak FROM QL_trnjualmst shm INNER JOIN QL_mstcust c ON c.custoid=shm.custoid INNER JOIN QL_mstcurr curr ON curr.curroid=shm.curroid INNER JOIN QL_trndomst dom ON dom.domstoid=shm.domstoid INNER JOIN QL_mstgen g1 ON g1.genoid=dom.expedisioid INNER JOIN QL_trnjualdtl shd ON shd.cmpcode=shm.cmpcode AND shd.trnjualmstoid=shm.trnjualmstoid INNER JOIN QL_mstitem i ON i.itemoid=shd.itemoid INNER JOIN QL_mstgen g2 ON g2.genoid=trnjualdtlunitoid INNER JOIN QL_mstgen g3 ON g3.genoid=dom.dowhoid INNER JOIN (SELECT dod.domstoid, dod.dodtloid, som.somstoid, som.sono [SO No.] FROM QL_trnsomst som INNER JOIN QL_trndodtl dod ON dod.somstoid=som.somstoid) AS tblSO ON tblSO.domstoid=shm.domstoid AND shd.dodtloid=tblSO.dodtloid inner join QL_trndodtl dodtl on dodtl.domstoid=dom.domstoid inner join QL_trnsomst somst on somst.somstoid=dodtl.somstoid "

            If DDLBusUnit.SelectedValue <> "ALL" Then
                sSql &= "WHERE shm.cmpcode='" & DDLBusUnit.SelectedValue & "'"
            Else
                sSql &= "WHERE shm.cmpcode LIKE '%%'"
            End If

            If DDLcurr.SelectedItem.Text <> "ALL" Then
                sSql &= " AND currcode='" & DDLcurr.SelectedItem.Text & "'"
            End If
            If CBtype.Checked = True Then
                sSql &= " AND shm.trnjualtype='" & TypeMat.SelectedItem.ToString & "'"
            End If

            If FilterDDLStatus.SelectedValue = "In Process" Then
                sSql &= " AND shm.trnjualstatus IN ('In Process')"
            ElseIf FilterDDLStatus.SelectedValue = "Post" Then
                sSql &= " AND shm.trnjualstatus IN ('Post')"
            ElseIf FilterDDLStatus.SelectedValue = "Rejected" Then
                sSql &= " AND shm.trnjualstatus IN ('Rejected','Cancel')"
            End If

            If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
                'If IsValidPeriod() Then
                '    sSql &= " AND " & FilterDDLDate.SelectedValue & ">='" & FilterPeriod1.Text & " 00:00:00' AND " & FilterDDLDate.SelectedValue & "<='" & FilterPeriod2.Text & " 23:59:59'"
                'Else
                '    Exit Sub
                'End If
                period1 = year1.SelectedValue
                period1 &= month1.SelectedValue
                period2 = year2.SelectedValue
                period2 &= month2.SelectedValue

                If period1 <= period2 Then
                    sSql &= " AND shm.periodacctg >=" & year1.SelectedValue & "" & month1.SelectedValue & " AND shm.periodacctg <=" & year2.SelectedValue & "" & month2.SelectedValue & ""
                Else
                    Session("WarningPeriod") = "Period 1 must be less or equal than period 2!"
                    showMessage(Session("WarningPeriod"), 2)
                    Exit Sub
                End If
            End If

            If custcode.Text <> "" Then
                Dim scode() As String = Split(custcode.Text, ";")
                sSql &= " AND ("
                For c1 As Integer = 0 To scode.Length - 1
                    sSql &= " c.custcode = '" & Tchar(scode(c1)) & "'"
                    If c1 < scode.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            End If

            'If drivcode.Text <> "" Then
            '    Dim scode() As String = Split(drivcode.Text, ";")
            '    sSql &= " AND ("
            '    For c1 As Integer = 0 To scode.Length - 1
            '        sSql &= " driv.drivcode = '" & Tchar(scode(c1)) & "'"
            '        If c1 < scode.Length - 1 Then
            '            sSql &= " OR "
            '        End If
            '    Next
            '    sSql &= ")"
            'End If

            'If vhccode.Text <> "" Then
            '    Dim scode() As String = Split(vhccode.Text, ";")
            '    sSql &= " AND ("
            '    For c1 As Integer = 0 To scode.Length - 1
            '        sSql &= " v.vhccode = '" & Tchar(scode(c1)) & "'"
            '        If c1 < scode.Length - 1 Then
            '            sSql &= " OR "
            '        End If
            '    Next
            '    sSql &= ")"
            'End If

            'If drivcode.Text <> "" Then
            '    sSql &= " AND shm.shipmentitemmstoid IN (SELECT shm2.shipmentitemmstoid FROM QL_trnshipmentitemmst shm2 INNER JOIN QL_mstdriver driv ON shm2.drivoid=driv.drivoid AND driv.activeflag='ACTIVE' "
            '    Dim sDrivCode() As String = Split(drivcode.Text, ";")
            '    sSql &= " WHERE "
            '    For c1 As Integer = 0 To sDrivCode.Length - 1
            '        sSql &= " driv.drivcode LIKE '%" & Tchar(sDrivCode(c1)) & "%'"
            '        If c1 < sDrivCode.Length - 1 Then
            '            sSql &= " OR "
            '        End If
            '    Next
            '    sSql &= ")"
            'End If

            'If vhccode.Text <> "" Then
            '    sSql &= " AND shm.shipmentitemmstoid IN (SELECT shm2.shipmentitemmstoid FROM QL_trnshipmentitemmst shm2 INNER JOIN QL_mstvehicle vhc ON shm.vhcoid=vhc.vhcoid AND vhc.activeflag='ACTIVE' "
            '    Dim sVhcCode() As String = Split(vhccode.Text, ";")
            '    sSql &= " WHERE "
            '    For c1 As Integer = 0 To sVhcCode.Length - 1
            '        sSql &= " vhc.vhccode LIKE '%" & Tchar(sVhcCode(c1)) & "%'"
            '        If c1 < sVhcCode.Length - 1 Then
            '            sSql &= " OR "
            '        End If
            '    Next
            '    sSql &= ")"
            'End If

            If shipmentno.Text <> "" Then
                If DDLShipmentNo.SelectedValue = "Shipment No." Then
                    Dim sShipmentno() As String = Split(shipmentno.Text, ";")
                    sSql &= " AND ("
                    For c1 As Integer = 0 To sShipmentno.Length - 1
                        sSql &= " shm.trnjualno LIKE '%" & Tchar(sShipmentno(c1)) & "%'"
                        If c1 < sShipmentno.Length - 1 Then
                            sSql &= " OR "
                        End If
                    Next
                    sSql &= ")"
                Else
                    Dim sShipmentno() As String = Split(shipmentno.Text, ";")
                    sSql &= " AND ("
                    For c1 As Integer = 0 To sShipmentno.Length - 1
                        sSql &= " shm.trnjualmstoid = " & ToDouble(sShipmentno(c1))
                        If c1 < sShipmentno.Length - 1 Then
                            sSql &= " OR "
                        End If
                    Next
                    sSql &= ")"
                End If
            End If

            If FilterDDLType.SelectedValue = "Detail" Then
                If dono.Text <> "" Then
                    sSql &= " AND shm.trnjualmstoid IN (SELECT shd2.trnjualmstoid FROM QL_trnjualmst shd2 INNER JOIN QL_trndomst dom ON shd2.domstoid=dom.domstoid "
                    Dim sDono() As String = Split(dono.Text, ";")
                    sSql &= " WHERE "
                    For c1 As Integer = 0 To sDono.Length - 1
                        sSql &= " dom.dono LIKE '%" & Tchar(sDono(c1)) & "%'"
                        If c1 < sDono.Length - 1 Then
                            sSql &= " OR "
                        End If
                    Next
                    sSql &= ")"
                End If
                If lbWarehouse.Items.Count > 0 Then
                    Dim sOid As String = ""
                    For C1 As Integer = 0 To lbWarehouse.Items.Count - 1
                        sOid &= lbWarehouse.Items(C1).Value & ","
                    Next
                    If sOid <> "" Then
                        sOid = Left(sOid, sOid.Length - 1)
                        sSql &= " AND dod.dowhoid IN (" & sOid & ")"
                    End If
                End If
                If itemcode.Text <> "" Then
                    Dim sMatcode() As String = Split(itemcode.Text, ";")
                    sSql &= " AND ("
                    For c1 As Integer = 0 To sMatcode.Length - 1
                        sSql &= " i.itemcode LIKE '%" & Tchar(sMatcode(c1)) & "%'"
                        If c1 < sMatcode.Length - 1 Then
                            sSql &= " OR"
                        End If
                    Next
                    sSql &= ")"
                End If
            End If
            sSql &= " ORDER BY shm.cmpcode ASC, c.custname " & DDLOrderby.SelectedValue & ""

            'If FilterDDLType.SelectedValue = "Summary" Then
            'If DDLGrouping.SelectedValue = "v.vhccode" Then
            '    If DDLSorting.SelectedValue = "shm.shipment" & Typem1 & "no" Then
            '        sSql &= " ORDER BY shm.cmpcode ASC, v.vhccode " & DDLOrderby.SelectedValue & ", " & DDLSorting.SelectedValue & " " & DDLOrderby.SelectedValue & " , shm.shipment" & Typem1 & "mstoid " & DDLOrderby.SelectedValue
            '    Else
            '        sSql &= " ORDER BY shm.cmpcode ASC, v.vhccode " & DDLOrderby.SelectedValue & ", " & DDLSorting.SelectedValue & " " & DDLOrderby.SelectedValue
            '    End If
            'ElseIf DDLGrouping.SelectedValue = "c.custname" Then
            '    If DDLSorting.SelectedValue = "shm.shipment" & Typem1 & "no" Then
            '        sSql &= " ORDER BY shm.cmpcode ASC, c.custname " & DDLOrderby.SelectedValue & ", " & DDLSorting.SelectedValue & " " & DDLOrderby.SelectedValue & " , shm.shipment" & Typem1 & "mstoid " & DDLOrderby.SelectedValue
            '    Else
            '        sSql &= " ORDER BY shm.cmpcode ASC, c.custname " & DDLOrderby.SelectedValue & ", " & DDLSorting.SelectedValue & " " & DDLOrderby.SelectedValue
            '    End If
            'Else
            '    'Driver
            '    If DDLSorting.SelectedValue = "shm.shipment" & Typem1 & "no" Then
            '        sSql &= " ORDER BY shm.cmpcode ASC, driv.drivname " & DDLOrderby.SelectedValue & ", " & DDLSorting.SelectedValue & " " & DDLOrderby.SelectedValue & " , shm.shipment" & Typem1 & "mstoid " & DDLOrderby.SelectedValue
            '    Else
            '        sSql &= " ORDER BY shm.cmpcode ASC, driv.drivname " & DDLOrderby.SelectedValue & ", " & DDLSorting.SelectedValue & " " & DDLOrderby.SelectedValue
            '    End If
            'End If
            'Else
            'If DDLGrouping.SelectedValue = "v.vhccode" Then
            '    If DDLSorting.SelectedValue = "shm.shipment" & Typem1 & "no" Then
            '        sSql &= " ORDER BY shm.cmpcode ASC, v.vhccode " & DDLOrderby.SelectedValue & ", " & DDLSorting.SelectedValue & " " & DDLOrderby.SelectedValue & " , shm.shipment" & Typem1 & "mstoid " & DDLOrderby.SelectedValue
            '    Else
            '        sSql &= " ORDER BY shm.cmpcode ASC, v.vhccode " & DDLOrderby.SelectedValue & ", " & DDLSorting.SelectedValue & " " & DDLOrderby.SelectedValue
            '    End If
            'ElseIf DDLGrouping.SelectedValue = "c.custname" Then
            'If DDLSorting.SelectedValue = "shm.shipment" & Typem1 & "no" Then
            '    sSql &= " ORDER BY shm.cmpcode ASC, c.custname " & DDLOrderby.SelectedValue & ", " & DDLSorting.SelectedValue & " " & DDLOrderby.SelectedValue & " , shm.shipment" & Typem1 & "mstoid " & DDLOrderby.SelectedValue
            'Else
            'End If
            '    ElseIf DDLGrouping.SelectedValue = "i.itemcode" Then
            '    If DDLSorting.SelectedValue = "shm.shipmentitemno" Then
            '        sSql &= " ORDER BY shm.cmpcode ASC, i.itemcode " & DDLOrderby.SelectedValue & ", " & DDLSorting.SelectedValue & " " & DDLOrderby.SelectedValue & " , shm.shipmentitemmstoid " & DDLOrderby.SelectedValue
            '    Else
            '        sSql &= " ORDER BY shm.cmpcode ASC, i.itemcode " & DDLOrderby.SelectedValue & ", " & DDLSorting.SelectedValue & " " & DDLOrderby.SelectedValue
            '    End If
            '    Else
            '    'Warehouse
            '    If DDLSorting.SelectedValue = "shm.shipmentitemno" Then
            '        sSql &= " ORDER BY shm.cmpcode ASC, g3.gendesc " & DDLOrderby.SelectedValue & ", " & DDLSorting.SelectedValue & " " & DDLOrderby.SelectedValue & " , shm.shipmentitemmstoid " & DDLOrderby.SelectedValue
            '    Else
            '        sSql &= " ORDER BY shm.cmpcode ASC, g3.gendesc " & DDLOrderby.SelectedValue & ", " & DDLSorting.SelectedValue & " " & DDLOrderby.SelectedValue
            '    End If
            'End If
            'End If

            Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnjualmst")
            Dim dvTbl As DataView = dtTbl.DefaultView
            report.SetDataSource(dvTbl.ToTable)
            report.PrintOptions.PaperSize = PaperSize.PaperFolio
            cProc.SetDBLogonForReport(report)

            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, rptName)
                report.Close()
                report.Dispose()
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, rptName)
                report.Close()
                report.Dispose()
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Private Sub InitAllDDL()
        ' DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)
        ' DDL Warehouse
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='WAREHOUSE' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDL(DDLWarehouse, sSql)
        ' DDL Group
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='GROUPITEM' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDL(TypeMat, sSql)
        ' DDL Currency
        sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLcurr, sSql)
    End Sub

    Private Sub InitFilterDDLCat1()
        'Fill DDL Category1
        sSql = "SELECT cat1code, cat1code+' - '+cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat1res1='Finish Good' Order By cat1code"
        FillDDL(FilterDDLCat01, sSql)
        If FilterDDLCat01.Items.Count > 0 Then
            InitFilterDDLCat2()
        Else
            FilterDDLCat02.Items.Clear()
            FilterDDLCat03.Items.Clear()
            FilterDDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitFilterDDLCat2()
        'Fill DDL Category2
        sSql = "SELECT cat2code, cat2code+' - '+cat2shortdesc FROM QL_mstcat2 cat2 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat2.cmpcode AND cat1.cat1oid=cat2.cat1oid AND cat1.cat1code='" & FilterDDLCat01.SelectedValue & "' WHERE cat2.cmpcode='" & CompnyCode & "' AND cat2.activeflag='ACTIVE' AND cat2res1='Finish Good' Order By cat2code"
        FillDDL(FilterDDLCat02, sSql)
        If FilterDDLCat02.Items.Count > 0 Then
            InitFilterDDLCat3()
        Else
            FilterDDLCat03.Items.Clear()
            FilterDDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitFilterDDLCat3()
        'Fill DDL Category3
        sSql = "SELECT cat3code, cat3code+' - '+cat3shortdesc FROM QL_mstcat3 cat3 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat3.cmpcode AND cat1.cat1oid=cat3.cat1oid AND cat1.cat1code='" & FilterDDLCat01.SelectedValue & "' INNER JOIN QL_mstcat2 cat2 ON cat2.cmpcode=cat3.cmpcode AND cat2.cat2oid=cat3.cat2oid AND cat2.cat2code='" & FilterDDLCat02.SelectedValue & "' WHERE cat3.cmpcode='" & CompnyCode & "' AND cat3.activeflag='ACTIVE' AND cat3res1='Finish Good' Order By cat3code"
        FillDDL(FilterDDLCat03, sSql)
        If FilterDDLCat03.Items.Count > 0 Then
            InitFilterDDLCat4()
        Else
            FilterDDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitFilterDDLCat4()
        'Fill DDL Category4
        sSql = "SELECT cat4code, cat4code+' - '+cat4shortdesc FROM QL_mstcat4 cat4 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat4.cmpcode AND cat1.cat1oid=cat4.cat1oid AND cat1.cat1code='" & FilterDDLCat01.SelectedValue & "' INNER JOIN QL_mstcat2 cat2 ON cat2.cmpcode=cat4.cmpcode AND cat2.cat2oid=cat4.cat2oid AND cat2.cat2code='" & FilterDDLCat02.SelectedValue & "' INNER JOIN QL_mstcat3 cat3 ON cat3.cmpcode=cat4.cmpcode AND cat3.cat3oid=cat4.cat3oid AND cat3.cat3code='" & FilterDDLCat03.SelectedValue & "' WHERE cat4.cmpcode='" & CompnyCode & "' AND cat4.activeflag='ACTIVE' AND cat4res1='Finish Good' Order By cat4code"
        FillDDL(FilterDDLCat04, sSql)
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If

        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmMarkReg.aspx")

        End If

        If checkPagePermission("~\ReportForm\frmMarkReg.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        Page.Title = CompnyName & " - Marketing Register Report"
        If Not Page.IsPostBack Then
            InitAllDDL()
            InitFilterDDLCat1()
            FilterDDLType.SelectedIndex = 0
            DDLGrouping.SelectedIndex = 0
            FilterDDLStatus_SelectedIndexChanged(Nothing, Nothing)
            FilterDDLType_SelectedIndexChanged(Nothing, Nothing)
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            month1.SelectedValue = Format(GetServerTime(), "MM")
            year1.SelectedValue = Format(GetServerTime(), "YYYY")
            month2.SelectedValue = Format(GetServerTime(), "MM")
            year2.SelectedValue = Format(GetServerTime(), "YYYY")
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListCust") Is Nothing And Session("EmptyListCust") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListCust") Then
                Session("EmptyListCust") = Nothing
                cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
            End If
        End If
        If Not Session("WarningListCust") Is Nothing And Session("WarningListCust") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListCust") Then
                Session("WarningListCust") = Nothing
                cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
            End If
        End If
        If Not Session("EmptyListDriv") Is Nothing And Session("EmptyListDriv") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListDriv") Then
                Session("EmptyListDriv") = Nothing
                cProc.SetModalPopUpExtender(btnHideListDriv, pnlListDriv, mpeListDriv, True)
            End If
        End If
        If Not Session("WarningListDriv") Is Nothing And Session("WarningListDriv") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListDriv") Then
                Session("WarningListDriv") = Nothing
                cProc.SetModalPopUpExtender(btnHideListDriv, pnlListDriv, mpeListDriv, True)
            End If
        End If
        If Not Session("EmptyListVhc") Is Nothing And Session("EmptyListVhc") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListVhc") Then
                Session("EmptyListVhc") = Nothing
                cProc.SetModalPopUpExtender(btnHideListVhc, pnlListVhc, mpeListVhc, True)
            End If
        End If
        If Not Session("WarningListVhc") Is Nothing And Session("WarningListVhc") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListVhc") Then
                Session("WarningListVhc") = Nothing
                cProc.SetModalPopUpExtender(btnHideListVhc, pnlListVhc, mpeListVhc, True)
            End If
        End If
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("EmptyListShipment") Is Nothing And Session("EmptyListShipment") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListShipment") Then
                Session("EmptyListShipment") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListShipment, PanelListShipment, mpeListShipment, True)
            End If
        End If
        If Not Session("WarningListShipment") Is Nothing And Session("WarningListShipment") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListShipment") Then
                Session("WarningListShipment") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListShipment, PanelListShipment, mpeListShipment, True)
            End If
        End If
        If Not Session("EmptyListDO") Is Nothing And Session("EmptyListDO") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListDO") Then
                Session("EmptyListDO") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListDO, PanelListDO, mpeListDO, True)
            End If
        End If
        If Not Session("WarningListDO") Is Nothing And Session("WarningListDO") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListDO") Then
                Session("WarningListDO") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListDO, PanelListDO, mpeListDO, True)
            End If
        End If
    End Sub

    Protected Sub FilterDDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLType.SelectedIndexChanged
        If FilterDDLType.SelectedValue = "Summary" Then
            'Fill DDL Sorting
            DDLGrouping.Items.Clear()
            DDLGrouping.Items.Add("Vehicle")
            DDLGrouping.Items.Item(DDLGrouping.Items.Count - 1).Value = "v.vhccode"
            DDLGrouping.Items.Add("Customer")
            DDLGrouping.Items.Item(DDLGrouping.Items.Count - 1).Value = "c.custname"
            DDLGrouping.Items.Add("Driver")
            DDLGrouping.Items.Item(DDLGrouping.Items.Count - 1).Value = "driv.drivname"
            lblgroup.Visible = True
            DDLGrouping.Visible = True
            DDLGrouping_SelectedIndexChanged(Nothing, Nothing)
            lbl.Visible = False
            lblMat.Visible = False
            lbl2.Visible = False
            itemcode.Visible = False
            imbFindMat.Visible = False
            imbEraseMat.Visible = False
            lblDO.Visible = False
            lbl3.Visible = False
            dono.Visible = False
            imbFindDO.Visible = False
            imbEraseDO.Visible = False
            lblWh.Visible = False
            lblWh2.Visible = False
            DDLWarehouse.Visible = False
            btnAddWH.Visible = False
            lbWarehouse.Visible = False
            btnMinWH.Visible = False
        Else
            DDLGrouping.Items.Clear()
            DDLGrouping.Items.Add("Vehicle")
            DDLGrouping.Items.Item(DDLGrouping.Items.Count - 1).Value = "v.vhccode"
            DDLGrouping.Items.Add("Customer")
            DDLGrouping.Items.Item(DDLGrouping.Items.Count - 1).Value = "c.custname"
            DDLGrouping.Items.Add("Material Code")
            DDLGrouping.Items.Item(DDLGrouping.Items.Count - 1).Value = "i.itemcode"
            DDLGrouping.Items.Add("Warehouse")
            DDLGrouping.Items.Item(DDLGrouping.Items.Count - 1).Value = "g3.gendesc"
            lblgroup.Visible = True
            DDLGrouping.Visible = True
            DDLGrouping_SelectedIndexChanged(Nothing, Nothing)
            lbl.Visible = True
            lblMat.Visible = True
            lbl2.Visible = True
            itemcode.Visible = True
            imbFindMat.Visible = True
            imbEraseMat.Visible = True
            lblDO.Visible = True
            lbl3.Visible = True
            dono.Visible = True
            imbFindDO.Visible = True
            imbEraseDO.Visible = True
            lblWh.Visible = True
            lblWh2.Visible = True
            DDLWarehouse.Visible = True
            btnAddWH.Visible = True
            lbWarehouse.Visible = True
            btnMinWH.Visible = True
        End If
    End Sub

    Protected Sub FilterDDLStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLStatus.SelectedIndexChanged
        'If FilterDDLStatus.SelectedValue = "Approved" Then
        '    FilterDDLDate.Items.Clear()
        '    FilterDDLDate.Items.Add("Create Date")
        '    FilterDDLDate.Items.Item(FilterDDLDate.Items.Count - 1).Value = "shm.createtime"
        '    FilterDDLDate.Items.Add("Approval Date")
        '    FilterDDLDate.Items.Item(FilterDDLDate.Items.Count - 1).Value = "shm.approvaldatetime"
        '    FilterDDLDate.Items.Add("ETD")
        '    FilterDDLDate.Items.Item(FilterDDLDate.Items.Count - 1).Value = "shm.shipmentitemetd"
        '    FilterDDLDate.Items.Add("ETA")
        '    FilterDDLDate.Items.Item(FilterDDLDate.Items.Count - 1).Value = "shm.shipmentitemeta"
        'Else
        '    FilterDDLDate.Items.Clear()
        '    FilterDDLDate.Items.Add("Create Date")
        '    FilterDDLDate.Items.Item(FilterDDLDate.Items.Count - 1).Value = "shm.createtime"
        '    FilterDDLDate.Items.Add("ETD")
        '    FilterDDLDate.Items.Item(FilterDDLDate.Items.Count - 1).Value = "shm.shipmentitemetd"
        '    FilterDDLDate.Items.Add("ETA")
        '    FilterDDLDate.Items.Item(FilterDDLDate.Items.Count - 1).Value = "shm.shipmentitemeta"
        'End If
    End Sub

    Protected Sub btnClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearCust.Click
        custcode.Text = ""
        'custoid.Text = ""
    End Sub

    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchCust.Click
        If IsValidPeriod() Then
            DDLFilterListCust.SelectedIndex = -1 : txtFilterListCust.Text = ""
            Session("TblCust") = Nothing : Session("TblCustView") = Nothing : gvListCust.DataSource = Nothing : gvListCust.DataBind()
            cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub btnFindListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListCust.Click
        If Session("TblCust") Is Nothing Then
            BindListCust()
            If Session("TblCust").Rows.Count <= 0 Then
                Session("EmptyListCust") = "Customer data can't be found!"
                showMessage(Session("EmptyListCust"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListCust.SelectedValue & " LIKE '%" & Tchar(txtFilterListCust.Text) & "%'"
        If UpdateCheckedCust() Then
            Dim dv As DataView = Session("TblCust").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblCustView") = dv.ToTable
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                dv.RowFilter = ""
                mpeListCust.Show()
            Else
                dv.RowFilter = ""
                Session("TblCustView") = Nothing
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                Session("WarningListCust") = "Customer data can't be found!"
                showMessage(Session("WarningListCust"), 2)
            End If
        Else
            Session("WarningListCust") = "Customer data can't be found!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub

    Protected Sub btnViewAllListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListCust.Click
        DDLFilterListCust.SelectedIndex = -1 : txtFilterListCust.Text = ""
        If Session("TblCust") Is Nothing Then
            BindListCust()
            If Session("TblCust").Rows.Count <= 0 Then
                Session("EmptyListCust") = "Customer data can't be found!"
                showMessage(Session("EmptyListCust"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedCust() Then
            Dim dt As DataTable = Session("TblCust")
            Session("TblCustView") = dt
            gvListCust.DataSource = Session("TblCustView")
            gvListCust.DataBind()
            mpeListCust.Show()
        Else
            Session("WarningListCust") = "Customer data can't be found!"
            showMessage(Session("WarningListCust"), 2)
        End If

    End Sub

    Protected Sub btnSelectAllCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllCust.Click
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCustView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblCust")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "custoid=" & dtTbl.Rows(C1)("custoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblCust") = objTbl
                Session("TblCustView") = dtTbl
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
            End If
            mpeListCust.Show()
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneCust.Click
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCustView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblCust")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "custoid=" & dtTbl.Rows(C1)("custoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblCust") = objTbl
                Session("TblCustView") = dtTbl
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
            End If
            mpeListCust.Show()
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedCust.Click
        If Session("TblCust") Is Nothing Then
            Session("WarningListCust") = "Selected Customer data can't be found!"
            showMessage(Session("WarningListCust"), 2)
            Exit Sub
        End If
        If UpdateCheckedCust() Then
            Dim dtTbl As DataTable = Session("TblCust")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListCust.SelectedIndex = -1 : txtFilterListCust.Text = ""
                Session("TblCustView") = dtView.ToTable
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                dtView.RowFilter = ""
                mpeListCust.Show()
            Else
                dtView.RowFilter = ""
                Session("TblCustView") = Nothing
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                Session("WarningListCust") = "Selected Customer data can't be found!"
                showMessage(Session("WarningListCust"), 2)
            End If
        Else
            mpeListCust.Show()
        End If
    End Sub

    Protected Sub gvListCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListCust.PageIndexChanging
        If UpdateCheckedCust2() Then
            gvListCust.PageIndex = e.NewPageIndex
            gvListCust.DataSource = Session("TblCustView")
            gvListCust.DataBind()
        End If
        mpeListCust.Show()
    End Sub 'OK

    Protected Sub lkbAddToListListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListCust.Click
        If Not Session("TblCust") Is Nothing Then
            If UpdateCheckedCust() Then
                Dim dtTbl As DataTable = Session("TblCust")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If custcode.Text <> "" Then
                            custcode.Text &= ";" + vbCrLf + dtView(C1)("custcode")
                        Else
                            custcode.Text = dtView(C1)("custcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
                Else
                    Session("WarningListCust") = "Please select Customer to add to list!"
                    showMessage(Session("WarningListCust"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbCloseListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListCust.Click
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub

    Protected Sub btnClearDriv_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearDriv.Click
        drivcode.Text = ""
        'drivoid.Text = ""
    End Sub

    Protected Sub btnSearchDriv_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchDriv.Click
        If IsValidPeriod() Then
            DDLFilterListDriv.SelectedIndex = -1 : txtFilterListDriv.Text = ""
            Session("TblDriv") = Nothing : Session("TblDrivView") = Nothing : gvListDriv.DataSource = Nothing : gvListDriv.DataBind()
            cProc.SetModalPopUpExtender(btnHideListDriv, pnlListDriv, mpeListDriv, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub btnFindListDriv_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListDriv.Click
        If Session("TblDriv") Is Nothing Then
            BindListDriver()
            If Session("TblDriv").Rows.Count <= 0 Then
                Session("EmptyListDriv") = "Driver data can't be found!"
                showMessage(Session("EmptyListDriv"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListDriv.SelectedValue & " LIKE '%" & Tchar(txtFilterListDriv.Text) & "%'"
        If UpdateCheckedDriv() Then
            Dim dv As DataView = Session("TblDriv").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblDrivView") = dv.ToTable
                gvListDriv.DataSource = Session("TblDrivView")
                gvListDriv.DataBind()
                dv.RowFilter = ""
                mpeListDriv.Show()
            Else
                dv.RowFilter = ""
                Session("TblDrivView") = Nothing
                gvListDriv.DataSource = Session("TblDrivView")
                gvListDriv.DataBind()
                Session("WarningListDriv") = "Driver data can't be found!"
                showMessage(Session("WarningListDriv"), 2)
            End If
        Else
            Session("WarningListDriv") = "Driver data can't be found!"
            showMessage(Session("WarningListDriv"), 2)
        End If
    End Sub

    Protected Sub btnViewAllListDriv_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListDriv.Click
        DDLFilterListDriv.SelectedIndex = -1 : txtFilterListDriv.Text = ""
        If Session("TblDriv") Is Nothing Then
            BindListDriver()
            If Session("TblDriv").Rows.Count <= 0 Then
                Session("EmptyListDriv") = "Driver data can't be found!"
                showMessage(Session("EmptyListDriv"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedDriv() Then
            Dim dt As DataTable = Session("TblDriv")
            Session("TblDrivView") = dt
            gvListDriv.DataSource = Session("TblDrivView")
            gvListDriv.DataBind()
            mpeListDriv.Show()
        Else
            Session("WarningListDriv") = "Driver data can't be found!"
            showMessage(Session("WarningListDriv"), 2)
        End If

    End Sub

    Protected Sub btnSelectAllDriv_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllDriv.Click
        If Not Session("TblDrivView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDrivView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblDriv")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "drivoid=" & dtTbl.Rows(C1)("drivoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblDriv") = objTbl
                Session("TblDrivView") = dtTbl
                gvListDriv.DataSource = Session("TblDrivView")
                gvListDriv.DataBind()
            End If
            mpeListDriv.Show()
        Else
            Session("WarningListDriv") = "Please show some Driver data first!"
            showMessage(Session("WarningListDriv"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneDriv_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneDriv.Click
        If Not Session("TblDrivView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDrivView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblDriv")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "drivoid=" & dtTbl.Rows(C1)("drivoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblDriv") = objTbl
                Session("TblDrivView") = dtTbl
                gvListDriv.DataSource = Session("TblDrivView")
                gvListDriv.DataBind()
            End If
            mpeListDriv.Show()
        Else
            Session("WarningListDriv") = "Please show some Driver data first!"
            showMessage(Session("WarningListDriv"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedDriv_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedDriv.Click
        If Session("TblDriv") Is Nothing Then
            Session("WarningListDriv") = "Selected Driver data can't be found!"
            showMessage(Session("WarningListDriv"), 2)
            Exit Sub
        End If
        If UpdateCheckedDriv() Then
            Dim dtTbl As DataTable = Session("TblDriv")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListDriv.SelectedIndex = -1 : txtFilterListDriv.Text = ""
                Session("TblDrivView") = dtView.ToTable
                gvListDriv.DataSource = Session("TblDrivView")
                gvListDriv.DataBind()
                dtView.RowFilter = ""
                mpeListDriv.Show()
            Else
                dtView.RowFilter = ""
                Session("TblDrivView") = Nothing
                gvListDriv.DataSource = Session("TblDrivView")
                gvListDriv.DataBind()
                Session("WarningListDriv") = "Selected Driver data can't be found!"
                showMessage(Session("WarningListDriv"), 2)
            End If
        Else
            mpeListDriv.Show()
        End If
    End Sub

    Protected Sub gvListDriv_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListDriv.PageIndexChanging
        If UpdateCheckedDriv2() Then
            gvListDriv.PageIndex = e.NewPageIndex
            gvListDriv.DataSource = Session("TblDrivView")
            gvListDriv.DataBind()
        End If
        mpeListDriv.Show()
    End Sub 'OK

    Protected Sub lkbAddToListListDriv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListDriv.Click
        If Not Session("TblDriv") Is Nothing Then
            If UpdateCheckedDriv() Then
                Dim dtTbl As DataTable = Session("TblDriv")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If drivcode.Text <> "" Then
                            drivcode.Text &= ";" + vbCrLf + dtView(C1)("drivcode")
                        Else
                            drivcode.Text = dtView(C1)("drivcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListDriv, pnlListDriv, mpeListDriv, False)
                Else
                    Session("WarningListDriv") = "Please select Driver to add to list!"
                    showMessage(Session("WarningListDriv"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListDriv") = "Please show some Driver data first!"
            showMessage(Session("WarningListDriv"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbCloseListDriv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListDriv.Click
        cProc.SetModalPopUpExtender(btnHideListDriv, pnlListDriv, mpeListDriv, False)
    End Sub

    Protected Sub btnClearVhc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearVhc.Click
        vhccode.Text = ""
        'vhcoid.Text = ""
    End Sub

    Protected Sub btnSearchVhc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchVhc.Click
        If IsValidPeriod() Then
            DDLFilterListVhc.SelectedIndex = -1 : txtFilterListVhc.Text = ""
            Session("TblVhc") = Nothing : Session("TblVhcView") = Nothing : gvListVhc.DataSource = Nothing : gvListVhc.DataBind()
            cProc.SetModalPopUpExtender(btnHideListVhc, pnlListVhc, mpeListVhc, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub btnFindListVhc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListVhc.Click
        If Session("TblVhc") Is Nothing Then
            BindListVehicle()
            If Session("TblVhc").Rows.Count <= 0 Then
                Session("EmptyListVhc") = "Vehicle data can't be found!"
                showMessage(Session("EmptyListVhc"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListVhc.SelectedValue & " LIKE '%" & Tchar(txtFilterListVhc.Text) & "%'"
        If UpdateCheckedVhc() Then
            Dim dv As DataView = Session("TblVhc").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblVhcView") = dv.ToTable
                gvListVhc.DataSource = Session("TblVhcView")
                gvListVhc.DataBind()
                dv.RowFilter = ""
                mpeListVhc.Show()
            Else
                dv.RowFilter = ""
                Session("TblVhcView") = Nothing
                gvListVhc.DataSource = Session("TblVhcView")
                gvListVhc.DataBind()
                Session("WarningListVhc") = "Vehicle data can't be found!"
                showMessage(Session("WarningListVhc"), 2)
            End If
        Else
            Session("WarningListVhc") = "Vehicle data can't be found!"
            showMessage(Session("WarningListVhc"), 2)
        End If
    End Sub

    Protected Sub btnViewAllListVhc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListVhc.Click
        DDLFilterListVhc.SelectedIndex = -1 : txtFilterListVhc.Text = ""
        If Session("TblVhc") Is Nothing Then
            BindListVehicle()
            If Session("TblVhc").Rows.Count <= 0 Then
                Session("EmptyListVhc") = "Vehicle data can't be found!"
                showMessage(Session("EmptyListVhc"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedVhc() Then
            Dim dt As DataTable = Session("TblVhc")
            Session("TblVhcView") = dt
            gvListVhc.DataSource = Session("TblVhcView")
            gvListVhc.DataBind()
            mpeListVhc.Show()
        Else
            Session("WarningListVhc") = "Vehicle data can't be found!"
            showMessage(Session("WarningListVhc"), 2)
        End If

    End Sub

    Protected Sub btnSelectAllVhc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllVhc.Click
        If Not Session("TblVhcView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblVhcView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblVhc")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "vhcoid=" & dtTbl.Rows(C1)("vhcoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblVhc") = objTbl
                Session("TblVhcView") = dtTbl
                gvListVhc.DataSource = Session("TblVhcView")
                gvListVhc.DataBind()
            End If
            mpeListVhc.Show()
        Else
            Session("WarningListVhc") = "Please show some Vehicle data first!"
            showMessage(Session("WarningListVhc"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneVhc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneVhc.Click
        If Not Session("TblVhcView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblVhcView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblVhc")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "vhcoid=" & dtTbl.Rows(C1)("vhcoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblVhc") = objTbl
                Session("TblVhcView") = dtTbl
                gvListVhc.DataSource = Session("TblVhcView")
                gvListVhc.DataBind()
            End If
            mpeListVhc.Show()
        Else
            Session("WarningListVhc") = "Please show some Vehicle data first!"
            showMessage(Session("WarningListVhc"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedVhc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedVhc.Click
        If Session("TblVhc") Is Nothing Then
            Session("WarningListVhc") = "Selected Vehicle data can't be found!"
            showMessage(Session("WarningListVhc"), 2)
            Exit Sub
        End If
        If UpdateCheckedVhc() Then
            Dim dtTbl As DataTable = Session("TblVhc")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListVhc.SelectedIndex = -1 : txtFilterListVhc.Text = ""
                Session("TblVhcView") = dtView.ToTable
                gvListVhc.DataSource = Session("TblVhcView")
                gvListVhc.DataBind()
                dtView.RowFilter = ""
                mpeListVhc.Show()
            Else
                dtView.RowFilter = ""
                Session("TblVhcView") = Nothing
                gvListVhc.DataSource = Session("TblVhcView")
                gvListVhc.DataBind()
                Session("WarningListVhc") = "Selected Vehicle data can't be found!"
                showMessage(Session("WarningListVhc"), 2)
            End If
        Else
            mpeListVhc.Show()
        End If
    End Sub

    Protected Sub gvListVhc_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListVhc.PageIndexChanging
        If UpdateCheckedVhc2() Then
            gvListVhc.PageIndex = e.NewPageIndex
            gvListVhc.DataSource = Session("TblVhcView")
            gvListVhc.DataBind()
        End If
        mpeListVhc.Show()
    End Sub 'OK

    Protected Sub lkbAddToListListVhc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListVhc.Click
        If Not Session("TblVhc") Is Nothing Then
            If UpdateCheckedVhc() Then
                Dim dtTbl As DataTable = Session("TblVhc")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If vhccode.Text <> "" Then
                            vhccode.Text &= ";" + vbCrLf + dtView(C1)("vhccode")
                        Else
                            vhccode.Text = dtView(C1)("vhccode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListVhc, pnlListVhc, mpeListVhc, False)
                Else
                    Session("WarningListVhc") = "Please select Vehicle to add to list!"
                    showMessage(Session("WarningListVhc"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListVhc") = "Please show some Vehicle data first!"
            showMessage(Session("WarningListVhc"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbCloseListVhc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListVhc.Click
        cProc.SetModalPopUpExtender(btnHideListVhc, pnlListVhc, mpeListVhc, False)
    End Sub

    Protected Sub imbFindDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindDO.Click
        If IsValidPeriod() Then
            DDLFilterListDO.SelectedIndex = -1 : txtFilterListDO.Text = ""
            Session("TblDO") = Nothing : Session("TblDOView") = Nothing : gvListDO.DataSource = Nothing : gvListDO.DataBind()
            cProc.SetModalPopUpExtender(btnHiddenListDO, PanelListDO, mpeListDO, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbEraseDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseDO.Click
        dono.Text = ""
        DDLBusUnit.Enabled = True
    End Sub

    Protected Sub btnFindListDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListDO.Click
        If Session("TblDO") Is Nothing Then
            BindListDO()
            If Session("TblDO").Rows.Count <= 0 Then
                Session("EmptyListDO") = "DO data can't be found!"
                showMessage(Session("EmptyListDO"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListDO.SelectedValue & " LIKE '%" & Tchar(txtFilterListDO.Text) & "%'"
        If UpdateCheckedDO() Then
            Dim dv As DataView = Session("TblDO").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblDOView") = dv.ToTable
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
                dv.RowFilter = ""
                mpeListDO.Show()
            Else
                dv.RowFilter = ""
                Session("TblDOView") = Nothing
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
                Session("WarningListDO") = "DO data can't be found!"
                showMessage(Session("WarningListDO"), 2)
            End If
        Else
            mpeListDO.Show()
        End If
    End Sub

    Protected Sub btnViewAllListDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListDO.Click
        DDLFilterListDO.SelectedIndex = -1 : txtFilterListDO.Text = ""
        If Session("TblDO") Is Nothing Then
            BindListDO()
            If Session("TblDO").Rows.Count <= 0 Then
                Session("EmptyListDO") = "DO data can't be found!"
                showMessage(Session("EmptyListDO"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedDO() Then
            Dim dt As DataTable = Session("TblDO")
            Session("TblDOView") = dt
            gvListDO.DataSource = Session("TblDOView")
            gvListDO.DataBind()
        End If
        mpeListDO.Show()
    End Sub

    Protected Sub btnSelectAllDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllDO.Click
        If Not Session("TblDOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblDO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "doitemmstoid=" & dtTbl.Rows(C1)("doitemmstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblDO") = objTbl
                Session("TblDOView") = dtTbl
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
            End If
            mpeListDO.Show()
        Else
            Session("WarningListDO") = "Please show some DO data first!"
            showMessage(Session("WarningListDO"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneDO.Click
        If Not Session("TblDOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblDO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "doitemmstoid=" & dtTbl.Rows(C1)("doitemmstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblDO") = objTbl
                Session("TblDOView") = dtTbl
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
            End If
            mpeListDO.Show()
        Else
            Session("WarningListDO") = "Please show some DO data first!"
            showMessage(Session("WarningListDO"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedDO.Click
        If Session("TblDO") Is Nothing Then
            Session("WarningListDO") = "Selected DO data can't be found!"
            showMessage(Session("WarningListDO"), 2)
            Exit Sub
        End If
        If UpdateCheckedDO() Then
            Dim dtTbl As DataTable = Session("TblDO")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListDO.SelectedIndex = -1 : txtFilterListDO.Text = ""
                Session("TblDOView") = dtView.ToTable
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
                dtView.RowFilter = ""
                mpeListDO.Show()
            Else
                dtView.RowFilter = ""
                Session("TblDOView") = Nothing
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
                Session("WarningListDO") = "Selected DO data can't be found!"
                showMessage(Session("WarningListDO"), 2)
            End If
        Else
            mpeListDO.Show()
        End If
    End Sub

    Protected Sub gvListDO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListDO.PageIndexChanging
        If UpdateCheckedDO2() Then
            gvListDO.PageIndex = e.NewPageIndex
            gvListDO.DataSource = Session("TblDOView")
            gvListDO.DataBind()
        End If
        mpeListDO.Show()
    End Sub 'OK

    Protected Sub lkbAddToListListDO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListDO.Click
        If Not Session("TblDO") Is Nothing Then
            If UpdateCheckedDO() Then
                Dim dtTbl As DataTable = Session("TblDO")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If dono.Text <> "" Then
                            If dtView(C1)("doitemno") <> "" Then
                                dono.Text &= ";" + vbCrLf + dtView(C1)("doitemno")
                            End If
                        Else
                            If dtView(C1)("doitemno") <> "" Then
                                dono.Text &= dtView(C1)("doitemno")
                            End If
                        End If
                    Next
                    dtView.RowFilter = ""
                    DDLBusUnit.Enabled = False
                    cProc.SetModalPopUpExtender(btnHiddenListDO, PanelListDO, mpeListDO, False)
                Else
                    Session("WarningListDO") = "Please select DO to add to list!"
                    showMessage(Session("WarningListDO"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListDO") = "Please show some DO data first!"
            showMessage(Session("WarningListDO"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListDO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListDO.Click
        cProc.SetModalPopUpExtender(btnHiddenListDO, PanelListDO, mpeListDO, False)
    End Sub

    Protected Sub btnAddWH_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddWH.Click
        If DDLWarehouse.SelectedValue <> "" Then
            If lbWarehouse.Items.Count > 0 Then
                If Not lbWarehouse.Items.Contains(lbWarehouse.Items.FindByValue(DDLWarehouse.SelectedValue)) Then
                    Dim objList As New ListItem
                    objList.Text = DDLWarehouse.SelectedItem.Text : objList.Value = DDLWarehouse.SelectedValue
                    lbWarehouse.Items.Add(objList)
                End If
            Else
                Dim objList As New ListItem
                objList.Text = DDLWarehouse.SelectedItem.Text : objList.Value = DDLWarehouse.SelectedValue
                lbWarehouse.Items.Add(objList)
            End If
        End If
    End Sub

    Protected Sub btnMinWH_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMinWH.Click
        If lbWarehouse.Items.Count > 0 Then
            Dim objList As ListItem = lbWarehouse.SelectedItem
            lbWarehouse.Items.Remove(objList)
        End If
    End Sub

    Protected Sub DDLShipmentNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLShipmentNo.SelectedIndexChanged
        shipmentno.Text = ""
    End Sub

    Protected Sub imbFindShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindShipment.Click
        If IsValidPeriod() Then
            DDLFilterListShipment.SelectedIndex = -1 : txtFilterListShipment.Text = ""
            Session("TblShipment") = Nothing : Session("TblShipmentView") = Nothing : gvListShipment.DataSource = Nothing : gvListShipment.DataBind()
            cProc.SetModalPopUpExtender(btnHiddenListShipment, PanelListShipment, mpeListShipment, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbEraseShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseShipment.Click
        shipmentno.Text = ""
        DDLBusUnit.Enabled = True
    End Sub

    Protected Sub btnFindListShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListShipment.Click
        If Session("TblShipment") Is Nothing Then
            BindListShipment()
            If Session("TblShipment").Rows.Count <= 0 Then
                Session("EmptyListShipment") = "Shipment data can't be found!"
                showMessage(Session("EmptyListShipment"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListShipment.SelectedValue & " LIKE '%" & Tchar(txtFilterListShipment.Text) & "%'"
        If UpdateCheckedShipment() Then
            Dim dv As DataView = Session("TblShipment").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblShipmentView") = dv.ToTable
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
                dv.RowFilter = ""
                mpeListShipment.Show()
            Else
                dv.RowFilter = ""
                Session("TblShipmentView") = Nothing
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
                Session("WarningListShipment") = "Shipment data can't be found!"
                showMessage(Session("WarningListShipment"), 2)
            End If
        Else
            mpeListShipment.Show()
        End If
    End Sub

    Protected Sub btnViewAllListShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListShipment.Click
        DDLFilterListShipment.SelectedIndex = -1 : txtFilterListShipment.Text = ""
        If Session("TblShipment") Is Nothing Then
            BindListShipment()
            If Session("TblShipment").Rows.Count <= 0 Then
                Session("EmptyListShipment") = "Shipment data can't be found!"
                showMessage(Session("EmptyListShipment"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedShipment() Then
            Dim dt As DataTable = Session("TblShipment")
            Session("TblShipmentView") = dt
            gvListShipment.DataSource = Session("TblShipmentView")
            gvListShipment.DataBind()
        End If
        mpeListShipment.Show()
    End Sub

    Protected Sub btnSelectAllShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllShipment.Click
        If Not Session("TblShipmentView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblShipmentView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblShipment")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "shipmentitemmstoid=" & dtTbl.Rows(C1)("shipmentitemmstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblShipment") = objTbl
                Session("TblShipmentView") = dtTbl
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
            End If
            mpeListShipment.Show()
        Else
            Session("WarningListShipment") = "Please show some Shipment data first!"
            showMessage(Session("WarningListShipment"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneShipment.Click
        If Not Session("TblShipmentView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblShipmentView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblShipment")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "shipmentitemmstoid=" & dtTbl.Rows(C1)("shipmentitemmstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblShipment") = objTbl
                Session("TblShipmentView") = dtTbl
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
            End If
            mpeListShipment.Show()
        Else
            Session("WarningListShipment") = "Please show some Shipment data first!"
            showMessage(Session("WarningListShipment"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedShipment.Click
        If Session("TblShipment") Is Nothing Then
            Session("WarningListShipment") = "Selected Shipment data can't be found!"
            showMessage(Session("WarningListShipment"), 2)
            Exit Sub
        End If
        If UpdateCheckedShipment() Then
            Dim dtTbl As DataTable = Session("TblShipment")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListShipment.SelectedIndex = -1 : txtFilterListShipment.Text = ""
                Session("TblShipmentView") = dtView.ToTable
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
                dtView.RowFilter = ""
                mpeListShipment.Show()
            Else
                dtView.RowFilter = ""
                Session("TblShipmentView") = Nothing
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
                Session("WarningListShipment") = "Selected Shipment data can't be found!"
                showMessage(Session("WarningListShipment"), 2)
            End If
        Else
            mpeListShipment.Show()
        End If
    End Sub

    Protected Sub gvListShipment_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListShipment.PageIndexChanging
        If UpdateCheckedShipment2() Then
            gvListShipment.PageIndex = e.NewPageIndex
            gvListShipment.DataSource = Session("TblShipmentView")
            gvListShipment.DataBind()
        End If
        mpeListShipment.Show()
    End Sub 'OK

    Protected Sub lkbAddToListListShipment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListShipment.Click
        If Not Session("TblShipment") Is Nothing Then
            If UpdateCheckedShipment() Then
                Dim dtTbl As DataTable = Session("TblShipment")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If shipmentno.Text <> "" Then
                            If DDLShipmentNo.SelectedValue = "Shipment No." Then
                                If dtView(C1)("shipmentitemno") <> "" Then
                                    shipmentno.Text &= ";" + vbCrLf + dtView(C1)("shipmentitemno")
                                End If
                            Else
                                shipmentno.Text &= ";" + vbCrLf + dtView(C1)("shipmentitemmstoid").ToString
                            End If
                        Else
                            If DDLShipmentNo.SelectedValue = "Shipment No." Then
                                If dtView(C1)("shipmentitemno") <> "" Then
                                    shipmentno.Text &= dtView(C1)("shipmentitemno")
                                End If
                            Else
                                shipmentno.Text &= dtView(C1)("shipmentitemmstoid").ToString
                            End If
                        End If
                    Next
                    dtView.RowFilter = ""
                    DDLBusUnit.Enabled = False
                    cProc.SetModalPopUpExtender(btnHiddenListShipment, PanelListShipment, mpeListShipment, False)
                Else
                    Session("WarningListShipment") = "Please select Shipment to add to list!"
                    showMessage(Session("WarningListShipment"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListShipment") = "Please show some Shipment data first!"
            showMessage(Session("WarningListShipment"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListShipment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListShipment.Click
        cProc.SetModalPopUpExtender(btnHiddenListShipment, PanelListShipment, mpeListShipment, False)
    End Sub

    Protected Sub FilterDDLCat01_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat01.SelectedIndexChanged
        InitFilterDDLCat2()
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat02_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat02.SelectedIndexChanged
        InitFilterDDLCat3()
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat03_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat03.SelectedIndexChanged
        InitFilterDDLCat4()
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat04_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat04.SelectedIndexChanged
        mpeListMat.Show()
    End Sub

    Protected Sub imbFindMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindMat.Click
        If IsValidPeriod() Then
            FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
            InitFilterDDLCat1()
            'If TypeMat.SelectedValue = "raw" Then
            '    lblTitleListMat.Text = "List of Raw Material"
            'ElseIf TypeMat.SelectedValue = "gen" Then
            '    lblTitleListMat.Text = "List of General Material"
            'ElseIf TypeMat.SelectedValue = "sparepart" Then
            '    lblTitleListMat.Text = "List of Spare Part"
            'ElseIf TypeMat.SelectedValue = "item" Then
            '    lblTitleListMat.Text = "List of Finish Good"
            'End If
            cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
            Session("TblMat") = Nothing : Session("TblMatView") = Nothing : gvListMat.DataSource = Nothing : gvListMat.DataBind() : tbData.Text = "5" : gvListMat.PageSize = CInt(tbData.Text)
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            Exit Sub
        End If
    End Sub 'OK

    Protected Sub imbEraseMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseMat.Click
        itemcode.Text = ""
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        If cbCat01.Checked Then
            If FilterDDLCat01.SelectedValue <> "" Then
                sPlus &= " AND SUBSTRING(itemcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'"
            Else
                cbCat01.Checked = False
            End If
        End If
        If cbCat02.Checked Then
            If FilterDDLCat02.SelectedValue <> "" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND SUBSTRING(itemcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'") & " AND SUBSTRING(itemcode, 4, 3)='" & FilterDDLCat02.SelectedValue & "'"
            Else
                cbCat02.Checked = False
            End If
        End If
        If cbCat03.Checked Then
            If FilterDDLCat03.SelectedValue <> "" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND SUBSTRING(itemcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'") & IIf(cbCat02.Checked, "", " AND SUBSTRING(itemcode, 4, 3)='" & FilterDDLCat02.SelectedValue & "'") & " AND SUBSTRING(itemcode, 8, 4)='" & FilterDDLCat03.SelectedValue & "'"
            Else
                cbCat03.Checked = False
            End If
        End If
        If cbCat04.Checked Then
            If FilterDDLCat04.SelectedValue <> "" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND SUBSTRING(itemcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'") & IIf(cbCat02.Checked, "", " AND SUBSTRING(itemcode, 4, 3)='" & FilterDDLCat02.SelectedValue & "'") & IIf(cbCat03.Checked, "", " AND SUBSTRING(itemcode, 8, 4)='" & FilterDDLCat03.SelectedValue & "'") & " AND SUBSTRING(itemcode, 13, 4)='" & FilterDDLCat04.SelectedValue & "'"
            Else
                cbCat04.Checked = False
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub 'OK

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        InitFilterDDLCat1()
        cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub 'OK

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If itemcode.Text <> "" Then
                            itemcode.Text &= ";" + vbCrLf + dtView(C1)("itemcode")
                        Else
                            itemcode.Text = dtView(C1)("itemcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some Material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                InitFilterDDLCat1()
                cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Selected material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub lbShowData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbShowData.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Please find material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If tbData.Text = "" Then
            Session("WarningListMat") = "Please fill total data to be shown!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        Else
            If ToDouble(tbData.Text) <= 0 Then
                Session("WarningListMat") = "Total data to be shown must be more than 0!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat2() Then
            gvListMat.PageSize = Int(tbData.Text)
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub 'OK


    Protected Sub DDLGrouping_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLGrouping.SelectedIndexChanged
        If FilterDDLType.SelectedValue = "Summary" Then
            If DDLGrouping.SelectedIndex = 0 Then
                DDLSorting.Items.Clear()
                DDLSorting.Items.Add("Customer")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "c.custname"
                DDLSorting.Items.Add("Driver")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "driv.drivname"
                DDLSorting.Items.Add("ETA")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "shm.shipmentitemeta"
                DDLSorting.Items.Add("ETD")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "shm.shipmentitemetd"
                DDLSorting.Items.Add("Shipment No.")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "shm.shipmentitemno"
            ElseIf DDLGrouping.SelectedIndex = 1 Then
                DDLSorting.Items.Clear()
                DDLSorting.Items.Add("Shipment No.")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "shm.shipmentitemno"
                DDLSorting.Items.Add("Vehicle")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "v.vhccode"
                DDLSorting.Items.Add("ETA")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "shm.shipmentitemeta"
                DDLSorting.Items.Add("ETD")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "shm.shipmentitemetd"
            ElseIf DDLGrouping.SelectedIndex = 2 Then
                DDLSorting.Items.Clear()
                DDLSorting.Items.Add("Customer")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "c.custname"
                DDLSorting.Items.Add("Shipment No.")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "shm.shipmentitemno"
                DDLSorting.Items.Add("Vehicle")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "v.vhccode"
                DDLSorting.Items.Add("ETA")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "shm.shipmentitemeta"
                DDLSorting.Items.Add("ETD")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "shm.shipmentitemetd"
            End If
        Else
            If DDLGrouping.SelectedIndex = 0 Then
                DDLSorting.Items.Clear()
                DDLSorting.Items.Add("Customer")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "c.custname"
                DDLSorting.Items.Add("Driver")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "driv.drivname"
                DDLSorting.Items.Add("ETA")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "shm.shipmentitemeta"
                DDLSorting.Items.Add("ETD")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "shm.shipmentitemetd"
                DDLSorting.Items.Add("Shipment No.")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "shm.shipmentitemno"
                DDLSorting.Items.Add("Material Code")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "i.itemcode"
                DDLSorting.Items.Add("Warehouse")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "g3.gendesc"
            ElseIf DDLGrouping.SelectedIndex = 1 Then
                DDLSorting.Items.Clear()
                DDLSorting.Items.Add("Shipment No.")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "shm.shipmentitemno"
                DDLSorting.Items.Add("Vehicle")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "v.vhccode"
                DDLSorting.Items.Add("ETA")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "shm.shipmentitemeta"
                DDLSorting.Items.Add("ETD")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "shm.shipmentitemetd"
                DDLSorting.Items.Add("Material Code")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "i.itemcode"
                DDLSorting.Items.Add("Warehouse")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "g3.gendesc"
            ElseIf DDLGrouping.SelectedIndex = 2 Then
                DDLSorting.Items.Clear()
                DDLSorting.Items.Add("Vehicle")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "v.vhccode"
                DDLSorting.Items.Add("Customer ")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "c.custname"
                DDLSorting.Items.Add("Shipment No.")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "shm.shipmentitemno"
                DDLSorting.Items.Add("ETA")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "shm.shipmentitemeta"
                DDLSorting.Items.Add("ETD")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "shm.shipmentitemetd"
                DDLSorting.Items.Add("Driver")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "driv.drivname"
                DDLSorting.Items.Add("Warehouse")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "g3.gendesc"
            Else
                DDLSorting.Items.Clear()
                DDLSorting.Items.Add("Driver")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "driv.drivname"
                DDLSorting.Items.Add("Vehicle")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "v.vhccode"
                DDLSorting.Items.Add("Shipment No.")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "shm.shipmentitemno"
                DDLSorting.Items.Add("Material Code")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "i.itemcode"
                DDLSorting.Items.Add("ETA")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "shm.shipmentitemeta"
                DDLSorting.Items.Add("ETD")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "shm.shipmentitemetd"
                DDLSorting.Items.Add("Customer ")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "c.custname"
            End If
        End If
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmMarkReg.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        If Not report Is Nothing Then
            If report.IsLoaded Then
                report.Dispose()
                report.Close()
            End If
        End If
    End Sub
#End Region

    Protected Sub TypeMat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        custcode.Text = ""
        itemcode.Text = ""
    End Sub
End Class
