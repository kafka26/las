Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions


Partial Class rptPOStatus
    Inherits System.Web.UI.Page

#Region "Variable"
    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Procedure"
    Public Sub initddl()
        'sSql = "select distinct periodacctg,periodacctg from ql_pomst order by periodacctg desc"
        'FillDDL(period, sSql)
        'initGroup()
        'initSubGroup()
        sSql = "select gencode,gendesc from QL_mstgen where gengroup='GROUPITEM' order by gendesc"
        FillDDL(FilterDDLGrup, sSql)
    End Sub

    Public Sub initGroup()
        'sSql = "select genoid, gendesc from ql_mstgen where gengroup='ITEMGROUP'  ORDER BY gendesc"
        'FillDDL(FilterDDLGrup, sSql)
        'FilterDDLGrup.Items.Add(New ListItem("ALL GROUP", "ALL GROUP"))
        'FilterDDLGrup.SelectedValue = "ALL GROUP"
    End Sub

    Public Sub initSubGroup()
        'sSql = "select genoid, gendesc from ql_mstgen where gengroup='ITEMSUBGROUP'  ORDER BY gendesc"
        'FillDDL(FilterDDLSubGrup, sSql)
        'FilterDDLSubGrup.Items.Add(New ListItem("ALL SUB GROUP", "ALL SUB GROUP"))
        'FilterDDLSubGrup.SelectedValue = "ALL SUB GROUP"
    End Sub

    Public Sub showPrint(ByVal tipe As String)
        lblkonfirmasi.Text = ""
        Dim swhere As String = ""

        Try
            Dim dat1 As Date = CDate(dateAwal.Text)
            Dim dat2 As Date = CDate(dateAkhir.Text)
        Catch ex As Exception
            lblkonfirmasi.Text = "Period report is invalid!"
            Exit Sub
        End Try
        If CDate(dateAwal.Text.Trim) > CDate(dateAkhir.Text.Trim) Then
            lblkonfirmasi.Text = "Period 2 must be more than Period 1!"
            Exit Sub
        End If

        Try
            Dim namaPDF As String = ""
            swhere &= "WHERE pom.podate between '" & Format(CDate(dateAwal.Text), "yyyy-MM-dd") & "' and '" & Format(CDate(dateAkhir.Text), "yyyy-MM-dd") & "'"
            If chkGroup.Checked Then
                swhere &= IIf(FilterDDLGrup.SelectedValue = "ALL", "  ", "AND i.itemGroup='" & FilterDDLGrup.SelectedValue & "'")
            End If
            swhere &= IIf(ToDecimal(oid.Text) > 0, "AND pom.pomstoid=" & oid.Text & "", " ")
            swhere &= IIf(ToDecimal(itemoid.Text) > 0, "AND i.itemoid=" & itemoid.Text & "", " ")
            swhere &= IIf(ToDecimal(suppoid.Text) > 0, "AND supp.suppoid = " & suppoid.Text & "", " ")
            swhere &= IIf(Tchar(taxable.SelectedValue) = "ALL", " ", "AND pom.potaxtype='" & Tchar(taxable.SelectedValue) & "'")
            If Tchar(statusDelivery.SelectedValue) = "COMPLETE" Then
                swhere &= "AND pod.podtlstatus='COMPLETE'"
            ElseIf Tchar(statusDelivery.SelectedValue) = "IN COMPLETE" Then
                swhere &= "AND pod.podtlstatus<>'COMPLETE'"
            Else
                swhere &= IIf(Tchar(statusDelivery.SelectedValue) = "ALL", " ", " ")
            End If
            swhere &= IIf(ddlstatusPO.SelectedValue = "ALL", " ", "AND pom.pomststatus = '" & ddlstatusPO.SelectedValue & "' ")
            vReport = New ReportDocument
            If Tchar(itemname.Text.Trim) <> "" Or ToDecimal(itemoid.Text) > 0 Then
                vReport.Load(Server.MapPath("~\Report\rptPOStatus1.rpt"))
                namaPDF = "POStatus_"
            Else
                vReport.Load(Server.MapPath("~\Report\rptPOStatus.rpt"))
                namaPDF = "POStatus_"
            End If

            cProc.SetDBLogonForReport(vReport)

            vReport.SetParameterValue("sWhere", swhere)
            vReport.SetParameterValue("startperiod", dateAwal.Text)
            vReport.SetParameterValue("endperiod", dateAkhir.Text)
            vReport.SetParameterValue("filterPO", IIf(oid.Text.Trim = "", "ALL", nota.Text))
            vReport.SetParameterValue("filterGroup", IIf(FilterDDLGrup.SelectedValue = "ALL", "ALL", FilterDDLGrup.SelectedItem.Text))
            vReport.SetParameterValue("filterItem", IIf(itemoid.Text.Trim = "", "ALL", itemname.Text))
            vReport.SetParameterValue("filterSupplier", IIf(suppoid.Text.Trim = "", "ALL", suppname.Text))
            vReport.SetParameterValue("filterTaxable", IIf(taxable.SelectedValue = "ALL", "ALL", taxable.SelectedValue))
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))

            If tipe = "" Then
                crvMutasiStock.DisplayGroupTree = False
                crvMutasiStock.ReportSource = vReport
            ElseIf tipe = "pdf" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf tipe = "excel" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            End If
        Catch ex As Exception
            vReport.Close() : vReport.Dispose()
            lblkonfirmasi.Text = ex.ToString
        End Try
    End Sub

    Public Sub BindDataListItem()
        Dim sWhere As String = ""
        'If FilterDDLGrup.SelectedItem.Text <> "ALL" Then
        '    sWhere &= " and i.itemGroup=" & FilterDDLGrup.SelectedValue & ""
        'End If
        'If FilterDDLSubGrup.SelectedItem.Text <> "ALL SUB GROUP" Then
        '    sWhere &= " and itemsubgroupoid=" & FilterDDLSubGrup.SelectedValue & ""
        'End If
        sSql = "select TOP 1000 i.itemoid, i.itemCode, i.itemShortDescription from ql_mstitem i inner join ql_mstgen g on g.genoid=i.itemUnit1 WHERE (itemShortDescription like '%" & Tchar(itemname.Text) & "%' or i.itemCode like '%" & Tchar(itemname.Text) & "%')"
        'FillGV(gvItem, sSql, "ql_mstitem")
        Session("Tblitem") = ckon.ambiltabel(sSql, "QL_mstitem")
        gvItem.DataSource = Session("Tblitem")
        gvItem.DataBind()
        gvItem.Visible = True
    End Sub

    Public Sub bindDataListSupplier()
        sSql = "select suppoid, suppcode, suppname, supptype from ql_mstsupp where suppname like '%" & Tchar(suppname.Text) & "%' or suppcode like '%" & Tchar(suppname.Text) & "%'"
        FillGV(gvSupp, sSql, "ql_mstsupp")
        gvSupp.Visible = True
    End Sub

    Public Sub bindDataListPO()
        'Try
        '    Dim dat1 As Date = CDate(toDate(dateAwal.Text))
        '    Dim dat2 As Date = CDate(toDate(dateAkhir.Text))
        'Catch ex As Exception
        '    lblkonfirmasi.Text = "Period report is invalid!"
        '    Exit Sub
        'End Try
        If CDate(dateAwal.Text.Trim) > CDate(dateAkhir.Text.Trim) Then
            lblkonfirmasi.Text = "Period 2 must be more than Period 1!"
            Exit Sub
        End If

        sSql = "select TOP 1000 pomstoid, pono, s.suppoid, suppname from ql_trnpomst po inner join ql_mstsupp s on s.suppoid=po.suppoid where podate between '" & Format(CDate(dateAwal.Text), "yyyy-MM-dd") & "' and '" & Format(CDate(dateAkhir.Text), "yyyy-MM-dd") & "' and pono like '%" & Tchar(nota.Text.Trim) & "%'"
        'If po_spb.SelectedValue <> "SPB/PO" Then
        '    sSql &= " and trnbelipono like '" & po_spb.SelectedValue & "%'"
        'End If
        'FillGV(GVNota, sSql, "ql_trnpomst")
        Session("Tblpo") = ckon.ambiltabel(sSql, "QL_trnpomst")
        GVNota.DataSource = Session("Tblpo")
        GVNota.DataBind()
        GVNota.Visible = True
    End Sub
#End Region

#Region "Event"
    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex
        BindDataListItem()
    End Sub

    Protected Sub gvItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItem.SelectedIndexChanged
        crvMutasiStock.ReportSource = Nothing
        Session("diprint") = "False"
        itemname.Text = gvItem.SelectedDataKey.Item("itemCode")
        itemoid.Text = gvItem.SelectedDataKey.Item("itemoid")
        'merk.Text = gvItem.SelectedDataKey.Item("merk")
        gvItem.Visible = False
    End Sub

    Protected Sub btnEraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseItem.Click
        itemname.Text = ""
        itemoid.Text = ""
        merk.Text = ""
        gvItem.Visible = False
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindDataListItem()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\rptPOStatus.aspx")
        End If
        If checkPagePermission("~\ReportForm\rptPOStatus.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        Page.Title = CompnyName & " - Laporan Status PO"

        If Not IsPostBack Then
            initddl()
            dateAwal.Text = Format(GetServerTime(), "MM/01/yyyy")
            dateAkhir.Text = Format(GetServerTime(), "MM/dd/yyyy")
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        showPrint("")
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("rptPOStatus.aspx?awal=true")
    End Sub

    Protected Sub ibexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        showPrint("excel")
    End Sub

    Protected Sub btnSearchSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindDataListSupplier()
    End Sub

    Protected Sub btnSearchNota_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindDataListPO()
    End Sub

    Protected Sub btnEraseSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        suppname.Text = ""
        suppoid.Text = ""
        gvSupp.Visible = False
    End Sub

    Protected Sub gvSupp_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        crvMutasiStock.ReportSource = Nothing
        Session("diprint") = "False"
        suppname.Text = gvSupp.SelectedDataKey.Item("suppname")
        suppoid.Text = gvSupp.SelectedDataKey.Item("suppoid")
        gvSupp.Visible = False
    End Sub

    Protected Sub gvSupp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupp.PageIndexChanging
        gvSupp.PageIndex = e.NewPageIndex
        bindDataListSupplier()
    End Sub

    Protected Sub gvNota_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVNota.PageIndexChanging
        GVNota.PageIndex = e.NewPageIndex
        bindDataListPO()
    End Sub

    Protected Sub EraseNota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        nota.Text = ""
        oid.Text = ""
        GVNota.Visible = False
    End Sub

    Protected Sub GVNota_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        crvMutasiStock.ReportSource = Nothing
        Session("diprint") = "False"
        nota.Text = GVNota.SelectedDataKey.Item("pono")
        oid.Text = GVNota.SelectedDataKey.Item("pomstoid")
        suppname.Text = GVNota.SelectedDataKey.Item("suppname")
        suppoid.Text = GVNota.SelectedDataKey.Item("suppoid")
        GVNota.Visible = False
    End Sub

    Protected Sub btnPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPdf.Click
        showPrint("pdf")
    End Sub

    Protected Sub rbPO_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbPO.CheckedChanged, rbPOLPB.CheckedChanged
        If rbPO.Checked = True Then
            tdTipeLap1.Visible = True
            tdTipeLap2.Visible = True
        Else
            tdTipeLap1.Visible = False
            tdTipeLap2.Visible = False
        End If
        Session("diprint") = "False"
    End Sub

#End Region

End Class
