Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_frmPAYAR
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function ValidateParam() As String
        Dim sReturn As String = ""
        Dim sErrTemp As String = ""

        If Not IsValidDate(txtStart.Text, "MM/dd/yyyy", sErrTemp) Then sReturn &= "- Invalid Start Date format: " & sErrTemp & ".<BR>"
        If Not IsValidDate(txtFinish.Text, "MM/dd/yyyy", sErrTemp) Then sReturn &= "- Invalid End Date format: " & sErrTemp & ".<BR>"
        If IsValidDate(txtStart.Text, "MM/dd/yyyy", sErrTemp) And IsValidDate(txtFinish.Text, "MM/dd/yyyy", sErrTemp) Then
            If DateDiff(DateInterval.Day, CDate(txtStart.Text), CDate(txtFinish.Text)) < 0 Then
                sReturn &= "- End Date must be greater than Start Date.<BR>"
            End If
        End If

        Return sReturn
    End Function

    Private Function GetIndoMonth(ByVal sVal As String) As String
        Select Case sVal
            Case "01"
                Return "JANUARI"
            Case "02"
                Return "FEBRUARI"
            Case "03"
                Return "MARET"
            Case "04"
                Return "APRIL"
            Case "05"
                Return "MEI"
            Case "06"
                Return "JUNI"
            Case "07"
                Return "JULI"
            Case "08"
                Return "AGUSTUS"
            Case "09"
                Return "SEPTEMBER"
            Case "10"
                Return "OKTOBER"
            Case "11"
                Return "NOVEMBER"
            Case "12"
                Return "DESEMBER"
            Case Else
                Return "Unknown"
        End Select
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindSupplierData()
        If DDLCustomer.SelectedValue = "CUSTOMER" Then
            sSql = "SELECT 0 selected,custoid,custcode,custname,custaddr FROM QL_mstcust WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' /*AND (custcode LIKE '%" & Tchar(FilterTextSupplier.Text) & "%' OR custname LIKE '%" & Tchar(FilterTextSupplier.Text) & "%')*/ ORDER BY custname"
        Else
            sSql = "SELECT 0 selected,custgroupoid custoid,custgroupcode custcode,custgroupname custname,custgroupaddr custaddr FROM QL_mstcustgroup WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' /*AND (custgroupcode LIKE '%" & Tchar(FilterTextSupplier.Text) & "%' OR custgroupname LIKE '%" & Tchar(FilterTextSupplier.Text) & "%')*/ ORDER BY custgroupname"
        End If
        Dim dtSupp As DataTable = cKon.ambiltabel(sSql, "QL_mstcust")
        'gvSupplier.DataSource = dtSupp : gvSupplier.DataBind()
        Session("TblCust") = dtSupp
        gvSupplier.Visible = True
        gvSupplier.SelectedIndex = -1
    End Sub

    Private Function UpdateCheckedGV2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblCustView") Is Nothing Then
            Dim dtab As DataTable = Session("TblCust")
            Dim dtab2 As DataTable = Session("TblCustView")
            If dtab.Rows.Count > 0 Then
                Dim cb As System.Web.UI.WebControls.CheckBox
                Dim dView As DataView = dtab.DefaultView
                Dim dView2 As DataView = dtab2.DefaultView
                Dim drView As DataRowView
                Dim drView2 As DataRowView
                For i As Integer = 0 To gvSupplier.Rows.Count - 1
                    cb = gvSupplier.Rows(i).FindControl("chkSelect")
                    dView.RowFilter = "custoid = " & cb.ToolTip
                    dView2.RowFilter = "custoid = " & cb.ToolTip
                    drView = dView.Item(0)
                    drView2 = dView2.Item(0)
                    drView.BeginEdit()
                    drView2.BeginEdit()
                    If cb.Checked = True Then
                        drView("selected") = 1
                        drView2("selected") = 1
                    Else
                        drView("selected") = 0
                        drView2("selected") = 0
                    End If
                    drView.EndEdit()
                    drView2.EndEdit()
                    dView.RowFilter = ""
                    dView2.RowFilter = ""
                Next
                dtab.AcceptChanges()
                dtab2.AcceptChanges()
                bReturn = True
            End If
            Session("TblCust") = dtab
            Session("TblCustView") = dtab2
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedGV() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblCust") Is Nothing Then
            Dim dtab As DataTable = Session("TblCust")
            If dtab.Rows.Count > 0 Then
                Dim cb As System.Web.UI.WebControls.CheckBox
                Dim dView As DataView = dtab.DefaultView
                Dim drView As DataRowView
                For i As Integer = 0 To gvSupplier.Rows.Count - 1
                    cb = gvSupplier.Rows(i).FindControl("chkSelect")
                    dView.RowFilter = "custoid = " & cb.ToolTip
                    drView = dView.Item(0)
                    drView.BeginEdit()
                    If cb.Checked = True Then
                        drView("selected") = 1
                    Else
                        drView("selected") = 0
                    End If
                    drView.EndEdit()
                    dView.RowFilter = ""
                Next
                dtab.AcceptChanges()
                bReturn = True
            End If
            Session("TblCust") = dtab
        End If
        Return bReturn
    End Function

    Private Sub ShowReport(ByVal sType As String)
        Try
            Dim sSuppOid As String = ""
            If rbSupplier.SelectedValue = "SELECT" Then
                Dim dtSupp As DataTable : dtSupp = Session("TblCust")
                Dim dvSupp As DataView = dtSupp.DefaultView
                dvSupp.RowFilter = "selected='1'"
                If Session("TblCust") Is Nothing Then
                    showMessage("Please Show Some Customer Data First!", 2)
                    Exit Sub
                Else
                    If dvSupp.Count <= 0 Then
                        showMessage("Please Select Customer!", 2)
                        Exit Sub
                    Else
                        For R1 As Integer = 0 To dvSupp.Count - 1
                            sSuppOid &= dvSupp(R1)("custoid").ToString & ","
                        Next
                        sSuppOid &= "0"
                    End If
                End If
            End If
            Dim tgl_1 As Date = CDate(txtStart.Text.Trim)
            Dim tgl_2 As Date = CDate(txtFinish.Text.Trim)
            Dim periode As String = "between '" & tgl_1 & "' and '" & tgl_2 & "'"

            sSql = "select * from (select custoid, custcode, custname, credit_limit, cast('" & tgl_1 & "' as date) transitemdate, '' transitemno, 'Saldo Awal' transitemmstnote, sum(amt_awal) amt_awal, 0.0 amt_masuk, 0.0 amt_keluar, personname from (select t3.custoid, custcode, custname, credit_limit, (sum(t2.salesprice * t2.transitemqty) * case when t1.cons_type='transfer' then 1 else -1 end) amt_awal, personname from QL_trntransitemmst t1 inner join QL_trntransitemdtl t2 on t2.transitemmstoid=t1.transitemmstoid inner join QL_mstcust t3 on t3.custoid=t1.transitemmstres3 left join QL_mstperson pr on pr.personoid=t1.person_id where transitemno like 'sjk%' and transitemdate<'" & tgl_1 & "' group by t3.custoid, custcode, custname, credit_limit, cons_type, personname union all select t3.custoid, custcode, custname, credit_limit, sum(t5.salesprice * t2.sodtlqty) * -1 amt_awal, personname from QL_trnsomst t1 inner join QL_trnsodtl t2 on t2.somstoid=t1.somstoid inner join QL_mstcust t3 on t3.custoid=t1.custoid inner join QL_trntransitemmst t4 on t4.transitemmstoid=t2.transitemmstoid inner join QL_trntransitemdtl t5 on t5.transitemmstoid=t2.transitemmstoid and t5.itemoid=t2.itemoid left join QL_mstperson pr on pr.personoid=t4.person_id where t1.iskonsinyasi='true' and sodate<'" & tgl_1 & "' group by t3.custoid, custcode, custname, credit_limit, personname) t " & IIf(sSuppOid <> "", " where custoid in (" & sSuppOid & ")", "") & " group by custoid, custcode, custname, credit_limit, personname union all "
            sSql &= "select t3.custoid, custcode, custname, credit_limit, transitemdate, (case when t1.cons_type='transfer' then '' else '  � ' end + transitemno) transitemno, transitemmstnote, 0.0 amt_awal, case when t1.cons_type='transfer' then sum(t2.salesprice * t2.transitemqty) else 0.0 end amt_masuk, case when t1.cons_type='return' then sum(t2.salesprice * t2.transitemqty) else 0.0 end amt_keluar, pr.personname from QL_trntransitemmst t1 inner join QL_trntransitemdtl t2 on t2.transitemmstoid=t1.transitemmstoid inner join QL_mstcust t3 on t3.custoid=t1.transitemmstres3 left join QL_mstperson pr on pr.personoid=t1.person_id where transitemno like 'sjk%' and transitemdate " & periode & " " & IIf(sSuppOid <> "", " and t3.custoid in (" & sSuppOid & ")", "") & " group by t3.custoid, custcode, custname, transitemdate, transitemno, transitemmstnote, cons_type, credit_limit, pr.personname union all "
            sSql &= "select t3.custoid, custcode, custname, credit_limit, sodate transitemdate, ('  � ' + sono) transitemno, somstnote transitemmstnote, 0.0 amt_awal, 0.0 amt_masuk, sum(t5.salesprice * t2.sodtlqty) amt_keluar, pr.personname from QL_trnsomst t1 inner join QL_trnsodtl t2 on t2.somstoid=t1.somstoid inner join QL_mstcust t3 on t3.custoid=t1.custoid inner join QL_trntransitemmst t4 on t4.transitemmstoid=t2.transitemmstoid inner join QL_trntransitemdtl t5 on t5.transitemmstoid=t2.transitemmstoid and t5.itemoid=t2.itemoid left join QL_mstperson pr on pr.personoid=t4.person_id where t1.iskonsinyasi='true' and sodate " & periode & " " & IIf(sSuppOid <> "", " and t3.custoid in (" & sSuppOid & ")", "") & " group by t3.custoid, custcode, custname, sodate, sono, somstnote, credit_limit, pr.personname) t where 1=1"
            If cbSales.Checked Then
                sSql &= " and personname='" & DDLSales.SelectedItem.Text & "'"
            End If
            sSql &= "order by custname, transitemdate, transitemno"
            Dim dt As DataTable = cKon.ambiltabel(sSql, "tbldata")

            Dim nFile As String = ""
            If sType = "Print Excel" Then
                nFile = Server.MapPath(folderReport & "laporan_kartu_konsinyasi" & IIf(FilterType.SelectedValue.ToLower() = "summary", "_sum", "") & ".rpt")
            Else
                nFile = Server.MapPath(folderReport & "laporan_kartu_konsinyasi" & IIf(FilterType.SelectedValue.ToLower() = "summary", "_sum", "") & ".rpt")
            End If
            report.Load(nFile)
            report.SetDataSource(dt)
            cProc.SetDBLogonForReport(report)
            report.SetParameterValue("periode", Format(CDate(txtStart.Text), "dd MMMM yyyy") & " - " & Format(CDate(txtFinish.Text), "dd MMMM yyyy"))

            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "ARREPORT_" & FilterDDLMonth.SelectedItem.Text.ToUpper & FilterDDLYear.SelectedItem.Text)
                Catch ex As Exception
                    report.Close() : report.Dispose()
                End Try
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "ARREPORT_" & FilterDDLMonth.SelectedItem.Text.ToUpper & FilterDDLYear.SelectedItem.Text)
                Catch ex As Exception
                    report.Close() : report.Dispose()
                End Try
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.ToString, 1)
        End Try
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmKartuKonsinyasi.aspx")
        End If
        If checkPagePermission("~\ReportForm\frmKartuKonsinyasi.aspx", Session("Role")) = False Then
            ' Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - kartu Konsinyasi"
        If Not Page.IsPostBack Then
            FilterDDLMonth.Items.Clear()
            For C1 As Int16 = 1 To 12
                FilterDDLMonth.Items.Add(MonthName(C1))
                FilterDDLMonth.Items(FilterDDLMonth.Items.Count - 1).Value = Format(C1, "00")
            Next
            FilterDDLMonth.SelectedValue = Format(Now.Month, "00")
            FilterDDLYear.Items.Clear()
            For C1 As Int16 = Now.Year - 2 To Now.Year + 2
                FilterDDLYear.Items.Add(C1)
                FilterDDLYear.Items(FilterDDLYear.Items.Count - 1).Value = C1
            Next
            FilterDDLYear.SelectedValue = Now.Year

            ' DDL Division
            sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE' "
            FillDDL(FilterDDLDiv, sSql)
            ' DDL Sales
            sSql = "SELECT personoid, UPPER(personname) personname FROM QL_mstperson WHERE leveloid=1 AND activeflag='ACTIVE' ORDER BY personname"
            FillDDL(DDLSales, sSql)

            FilterCurrency.Items.Clear()
            FilterCurrency.Items.Add(New ListItem("INDONESIAN RUPIAH", "IDR"))
            FilterCurrency.Items.Add(New ListItem("US DOLLAR", "USD"))
            FilterCurrency.Items.Add(New ListItem("TRANSACTION RATE", ""))
            FilterCurrency.Items(2).Enabled = False

            pnlSupplier.CssClass = "popupControl"
            txtStart.Text = Format(GetServerTime, "MM/01/yyyy")
            txtFinish.Text = Format(GetServerTime, "MM/dd/yyyy")

            'DDLGroupBy_SelectedIndexChanged(Nothing, Nothing)
            DDLCustomer_SelectedIndexChanged(Nothing, Nothing)
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub FilterDDLDiv_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLDiv.SelectedIndexChanged
        rbSupplier.SelectedIndex = 0 : rbSupplier_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub FilterType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If (FilterType.SelectedIndex = 0) Then
            FilterDDLMonth.Visible = True : FilterDDLYear.Visible = True
            txtStart.Visible = False : imbStart.Visible = False : lblTo.Visible = False
            txtFinish.Visible = False : imbFinish.Visible = False : lblMMDD.Visible = False
            lblGroupBy.Visible = True : DDLGroupBy.Visible = True
            'DDLGroupBy_SelectedIndexChanged(Nothing, Nothing)
        Else
            FilterDDLMonth.Visible = False : FilterDDLYear.Visible = False
            txtStart.Visible = True : imbStart.Visible = True : lblTo.Visible = True
            txtFinish.Visible = True : imbFinish.Visible = True : lblMMDD.Visible = True
            lblGroupBy.Visible = True : DDLGroupBy.Visible = True
        End If
        FilterDDLMonth.Visible = False : FilterDDLYear.Visible = False
        txtStart.Visible = True : imbStart.Visible = True : lblTo.Visible = True
        txtFinish.Visible = True : imbFinish.Visible = True : lblMMDD.Visible = True
        DDLGroupBy_SelectedIndexChanged(Nothing, Nothing)
        FilterCurrency.Items(2).Enabled = (FilterType.SelectedIndex = 1)
    End Sub

    Protected Sub rbSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbSupplier.SelectedIndexChanged
        gvSupplier.DataSource = Nothing : gvSupplier.DataBind()
        Session("TblCust") = Nothing : gvSupplier.Visible = False
        Session("TblCustView") = Nothing
        If rbSupplier.SelectedValue = "ALL" Then
            FilterTextSupplier.Visible = False
            btnSearchSupplier.Visible = False
            btnClearSupplier.Visible = False
            TDFilterCust.Visible = False
        Else
            FilterTextSupplier.Visible = True
            btnSearchSupplier.Visible = True
            btnClearSupplier.Visible = True
        End If
    End Sub

    Protected Sub btnSearchSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupplier.Click
        If Session("TblCust") Is Nothing Then
            BindSupplierData()
            If Session("TblCust").Rows.Count <= 0 Then
                Session("EmptyListCust") = "Customer data can't be found!"
                showMessage(Session("EmptyListCust"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = "custcode LIKE '%" & Tchar(FilterTextSupplier.Text) & "%' OR custname LIKE '%" & Tchar(FilterTextSupplier.Text) & "%'"
        If UpdateCheckedGV() Then
            Dim dv As DataView = Session("TblCust").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblCustView") = dv.ToTable
                gvSupplier.DataSource = Session("TblCustView")
                gvSupplier.DataBind()
                dv.RowFilter = ""
            Else
                dv.RowFilter = ""
                Session("TblCustView") = Nothing
                gvSupplier.DataSource = Session("TblCustView")
                gvSupplier.DataBind()
            End If
        End If
        TDFilterCust.Visible = True
    End Sub

    Protected Sub btnClearSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupplier.Click
        Session("TblCust") = Nothing : Session("TblCustView") = Nothing
        FilterTextSupplier.Text = ""
        custoid.Text = ""
        TDFilterCust.Visible = False
        gvSupplier.DataSource = Nothing
        gvSupplier.DataBind()
        gvSupplier.Visible = False
    End Sub

    Protected Sub gvSupplier_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupplier.PageIndexChanging
        If UpdateCheckedGV2() Then
            gvSupplier.PageIndex = e.NewPageIndex
            Dim dtSupp As DataTable = Session("TblCustView")
            gvSupplier.DataSource = dtSupp
            gvSupplier.DataBind()
        End If
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        If UpdateCheckedGV() Then
            Dim sMsg As String = ValidateParam()
            If sMsg <> "" Then
                showMessage(sMsg, 2) : Exit Sub
            Else
                ShowReport("View")
            End If
        Else
            Dim sMsg As String = ValidateParam()
            If sMsg <> "" Then
                showMessage(sMsg, 2) : Exit Sub
            Else
                ShowReport("View")
            End If
        End If
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        If UpdateCheckedGV() Then
            Dim sMsg As String = ValidateParam()
            If sMsg <> "" Then
                showMessage(sMsg, 2) : Exit Sub
            Else
                ShowReport("Print PDF")
            End If
        Else
            Dim sMsg As String = ValidateParam()
            If sMsg <> "" Then
                showMessage(sMsg, 2) : Exit Sub
            Else
                ShowReport("Print PDF")
            End If
        End If
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        If UpdateCheckedGV() Then
            Dim sMsg As String = ValidateParam()
            If sMsg <> "" Then
                showMessage(sMsg, 2) : Exit Sub
            Else
                ShowReport("Print Excel")
            End If
        Else
            Dim sMsg As String = ValidateParam()
            If sMsg <> "" Then
                showMessage(sMsg, 2) : Exit Sub
            Else
                ShowReport("Print Excel")
            End If
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmKartuKonsinyasi.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Dispose() : report.Close()
    End Sub

    Protected Sub DDLGroupBy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim bVal As Boolean = True
        If DDLGroupBy.SelectedValue = "SALES" Then
            'bVal = True
        End If
        cbSales.Visible = bVal : DDLSales.Visible = bVal
    End Sub

    Protected Sub DDLCustomer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        DDLGroupBy.Items(2).Enabled = False
        DDLGroupBy.Items(1).Enabled = True
        DDLGroupBy.Items(0).Enabled = True
        If DDLCustomer.SelectedValue <> "CUSTOMER" Then
            DDLGroupBy.Items(0).Enabled = False : DDLGroupBy.Items(1).Enabled = False : DDLGroupBy.Items(2).Enabled = True
        End If
        gvSupplier.DataSource = Nothing : gvSupplier.DataBind()
        gvSupplier.Visible = False
        rbSupplier.SelectedValue = "ALL"

        If rbSupplier.SelectedValue = "ALL" Then
            FilterTextSupplier.Visible = False
            btnSearchSupplier.Visible = False
            btnClearSupplier.Visible = False
        Else
            FilterTextSupplier.Visible = True
            btnSearchSupplier.Visible = True
            btnClearSupplier.Visible = True
        End If
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCustView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblCust")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "custoid=" & dtTbl.Rows(C1)("custoid")
                    objView(0)("selected") = 1
                    dtTbl.Rows(C1)("selected") = 1
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblCust") = objTbl
                Session("TblCustView") = dtTbl
                gvSupplier.DataSource = Session("TblCustView")
                gvSupplier.DataBind()
            End If
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCustView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblCust")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "custoid=" & dtTbl.Rows(C1)("custoid")
                    objView(0)("selected") = 0
                    dtTbl.Rows(C1)("selected") = 0
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblCust") = objTbl
                Session("TblCustView") = dtTbl
                gvSupplier.DataSource = Session("TblCustView")
                gvSupplier.DataBind()
            End If
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not Session("TblCustView") Is Nothing Then
            If UpdateCheckedGV() Then
                Dim dtTbl As DataTable = Session("TblCust")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "selected=1"
                If dtView.Count > 0 Then
                    Session("TblCustView") = dtView.ToTable
                    gvSupplier.DataSource = Session("TblCustView")
                    gvSupplier.DataBind()
                    dtView.RowFilter = ""
                Else
                    dtView.RowFilter = ""
                    Session("TblCustView") = Nothing
                    gvSupplier.DataSource = Session("TblCustView")
                    gvSupplier.DataBind()
                End If
            End If
        End If

    End Sub
#End Region

End Class
