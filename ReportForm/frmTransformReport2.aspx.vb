Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_Transformation2
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function FillDDLWithALL(ByRef oDDLObject As DropDownList, ByVal sSql As String) As Boolean
        ' To fill data in dropdown list, return true when all data exist
        FillDDLWithALL = True
        oDDLObject.Items.Clear()
        oDDLObject.Items.Add("ALL")
        oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = "ALL"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            oDDLObject.Items.Add(xreader.GetValue(1))
            oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()
        ' Cek if all data exits
        If oDDLObject.Items.Count = 0 Then
            FillDDLWithALL = False
        End If
        Return FillDDLWithALL
    End Function

    Private Function UpdateCheckedTrans() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblTrans") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblTrans")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListTrans.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListTrans.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "transformmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblTrans") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedTrans2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblTransView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblTrans")
            Dim dtTbl2 As DataTable = Session("TblTransView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListTrans.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListTrans.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "transformmstoid=" & cbOid
                                dtView2.RowFilter = "transformmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblTrans") = dtTbl
                Session("TblTransView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    'Private Function UpdateCheckedPallet() As Boolean
    '    Dim bReturn As Boolean = False
    '    If Not Session("TblPallet") Is Nothing Then
    '        Dim dtTbl As DataTable = Session("TblPallet")
    '        If dtTbl.Rows.Count > 0 Then
    '            Dim dtView As DataView = dtTbl.DefaultView
    '            dtView.RowFilter = ""
    '            dtView.AllowEdit = True
    '            For C1 As Integer = 0 To gvListPallet.Rows.Count - 1
    '                Dim row As System.Web.UI.WebControls.GridViewRow = gvListPallet.Rows(C1)
    '                If (row.RowType = DataControlRowType.DataRow) Then
    '                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
    '                    For Each myControl As System.Web.UI.Control In cc
    '                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
    '                            Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
    '                            Dim cbCheckValue As String = "False"
    '                            Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
    '                            dtView.RowFilter = "palletoid=" & cbOid
    '                            If cbCheck Then
    '                                cbCheckValue = "True"
    '                            End If
    '                            dtView(0)("CheckValue") = cbCheckValue
    '                        End If
    '                    Next
    '                End If
    '                dtView.RowFilter = ""
    '            Next
    '            dtTbl.AcceptChanges()
    '            dtTbl = dtView.ToTable
    '            Session("TblPallet") = dtTbl
    '            bReturn = True
    '        End If
    '    End If
    '    Return bReturn
    'End Function

    'Private Function UpdateCheckedPallet2() As Boolean
    '    Dim bReturn As Boolean = False
    '    If Not Session("TblPalletView") Is Nothing Then
    '        Dim dtTbl As DataTable = Session("TblPallet")
    '        Dim dtTbl2 As DataTable = Session("TblPalletView")
    '        If dtTbl2.Rows.Count > 0 Then
    '            Dim dtView As DataView = dtTbl.DefaultView
    '            Dim dtView2 As DataView = dtTbl2.DefaultView
    '            dtView.RowFilter = ""
    '            dtView2.RowFilter = ""
    '            dtView.AllowEdit = True
    '            dtView2.AllowEdit = True
    '            For C1 As Integer = 0 To gvListPallet.Rows.Count - 1
    '                Dim row As System.Web.UI.WebControls.GridViewRow = gvListPallet.Rows(C1)
    '                If (row.RowType = DataControlRowType.DataRow) Then
    '                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
    '                    For Each myControl As System.Web.UI.Control In cc
    '                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
    '                            Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
    '                            Dim cbCheckValue As String = "False"
    '                            Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
    '                            dtView.RowFilter = "palletoid=" & cbOid
    '                            dtView2.RowFilter = "palletoid=" & cbOid
    '                            If cbCheck Then
    '                                cbCheckValue = "True"
    '                            End If
    '                            dtView(0)("CheckValue") = cbCheckValue
    '                            If dtView2.Count > 0 Then
    '                                dtView2(0)("CheckValue") = cbCheckValue
    '                            End If
    '                        End If
    '                    Next
    '                End If
    '                dtView.RowFilter = ""
    '                dtView2.RowFilter = ""
    '            Next
    '            dtTbl.AcceptChanges()
    '            dtTbl2.AcceptChanges()
    '            Session("TblPallet") = dtTbl
    '            Session("TblPalletView") = dtTbl2
    '            bReturn = True
    '        End If
    '    End If
    '    Return bReturn
    'End Function

#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindListTrans()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, transm.transformmstoid, transformno, CONVERT(VARCHAR(10), transformdate, 101) AS transformdate, dept.deptname, transformtype, fwh.gendesc AS fromwh, (CASE transm.suppoid WHEN 0 then 'None' ELSE (SELECT s.suppname FROM QL_mstsupp s where s.suppoid=transm.suppoid) END ) as suppname, transformmststatus, transformmstnote FROM QL_trntransformmst transm INNER JOIN QL_mstdept dept ON dept.deptoid=transm.deptoid INNER JOIN QL_mstgen fwh ON fwh.genoid=transm.transformfromwhoid AND fwh.gengroup='MATERIAL LOCATION' WHERE transm.cmpcode='" & DDLBusUnit.SelectedValue & "' "

        If FilterDDLStatus.SelectedValue = "In Process" Then
            sSql &= " AND transm.transformmststatus IN ('In Process')"
        ElseIf FilterDDLStatus.SelectedValue = "Post" Then
            sSql &= " AND transm.transformmststatus IN ('Post','Closed')"
        End If

        If lbDept.Items.Count > 0 Then
            Dim sOid As String = ""
            For C1 As Integer = 0 To lbDept.Items.Count - 1
                sOid &= lbDept.Items(C1).Value & ","
            Next
            If sOid <> "" Then
                sOid = Left(sOid, sOid.Length - 1)
                sSql &= " AND transm.deptoid IN (" & sOid & ")"
            End If
        End If

        'Filter Type
        If transformtype.SelectedValue <> "All" Then
            sSql &= " AND transm.transformtype='" & transformtype.SelectedValue & "' "
        End If

        If lbFrWh.Items.Count > 0 Then
            Dim sOid As String = ""
            For C1 As Integer = 0 To lbFrWh.Items.Count - 1
                sOid &= lbFrWh.Items(C1).Value & ","
            Next
            If sOid <> "" Then
                sOid = Left(sOid, sOid.Length - 1)
                sSql &= " AND transm.transformfromwhoid IN (" & sOid & ")"
            End If
        End If

        'Flter Group Name
        If cbsupp.Checked Then
            sSql &= " AND transm.suppoid= " & suppoid.SelectedValue & " "
        End If

        If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
            If IsValidPeriod() Then
                sSql &= " AND " & FilterDDLDate.SelectedValue & " >='" & FilterPeriod1.Text & " 00:00:00' AND " & FilterDDLDate.SelectedValue & " <='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If

        sSql &= " ORDER BY transformno,transm.transformmstoid"
        Session("TblTrans") = cKon.ambiltabel(sSql, "QL_trntransformmst")
    End Sub

    'Private Sub BindListPallet()
    '    sSql = "SELECT DISTINCT 'False' AS checkvalue, pland.palletoid, palletno, matrawcode AS palletcode, palletlongdesc, palletres1 AS palletrefno, palletunitoid, gendesc AS palletunit, palletvolume FROM QL_trnkdplanmst planm INNER JOIN QL_trnkdplandtl pland ON pland.cmpcode=planm.cmpcode AND pland.kdplanmstoid=planm.kdplanmstoid INNER JOIN QL_mstpallet p ON pland.palletoid=p.palletoid INNER JOIN QL_mstmatraw m ON matrawoid=p.refoid INNER JOIN QL_mstgen g ON genoid=palletunitoid LEFT JOIN QL_trnkdprocmst procm ON planm.cmpcode=procm.cmpcode AND planm.kdplanmstoid=procm.kdplanmstoid WHERE planm.cmpcode='" & DDLBusUnit.SelectedValue & "' "

    '    If FilterDDLStatus.SelectedValue = "In Process" Then
    '        sSql &= " AND planm.kdplanmststatus IN ('In Process')"
    '    ElseIf FilterDDLStatus.SelectedValue = "Post" Then
    '        sSql &= " AND planm.kdplanmststatus IN ('Post','Closed')"
    '    ElseIf FilterDDLStatus.SelectedValue = "Closed" Then
    '        sSql &= " AND planm.kdplanmststatus IN ('Closed')"
    '    End If

    '    If lbRoom.Items.Count > 0 Then
    '        Dim sOid As String = ""
    '        For C1 As Integer = 0 To lbRoom.Items.Count - 1
    '            sOid &= lbRoom.Items(C1).Value & ","
    '        Next
    '        If sOid <> "" Then
    '            sOid = Left(sOid, sOid.Length - 1)
    '            sSql &= " AND planm.kdplanroomoid IN (" & sOid & ")"
    '        End If
    '    End If

    '    If FilterDDLStatus.SelectedValue = "Closed" Then
    '        If lbToWh.Items.Count > 0 Then
    '            Dim sOid As String = ""
    '            For C1 As Integer = 0 To lbToWh.Items.Count - 1
    '                sOid &= lbToWh.Items(C1).Value & ","
    '            Next
    '            If sOid <> "" Then
    '                sOid = Left(sOid, sOid.Length - 1)
    '                sSql &= " AND procm.kdprocwhoid IN (" & sOid & ")"
    '            End If
    '        End If
    '    End If

    '    If kdplanno.Text <> "" Then
    '        If DDLKDNo.SelectedValue = "KD Plan No." Then
    '            Dim sKDno() As String = Split(kdplanno.Text, ";")
    '            sSql &= " AND ("
    '            For c1 As Integer = 0 To sKDno.Length - 1
    '                sSql &= " kdplanno LIKE '%" & Tchar(sKDno(c1)) & "%'"
    '                If c1 < sKDno.Length - 1 Then
    '                    sSql &= " OR "
    '                End If
    '            Next
    '            sSql &= ")"
    '        Else
    '            Dim sKDno() As String = Split(kdplanno.Text, ";")
    '            sSql &= " AND ("
    '            For c1 As Integer = 0 To sKDno.Length - 1
    '                sSql &= " procm.kdplanmstoid = " & ToDouble(sKDno(c1))
    '                If c1 < sKDno.Length - 1 Then
    '                    sSql &= " OR "
    '                End If
    '            Next
    '            sSql &= ")"
    '        End If
    '    End If

    '    If lbFrWh.Items.Count > 0 Then
    '        Dim sOid As String = ""
    '        For C1 As Integer = 0 To lbFrWh.Items.Count - 1
    '            sOid &= lbFrWh.Items(C1).Value & ","
    '        Next
    '        If sOid <> "" Then
    '            sOid = Left(sOid, sOid.Length - 1)
    '            sSql &= " AND planm.kdplanwhoid IN (" & sOid & ")"
    '        End If
    '    End If

    '    If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
    '        If IsValidPeriod() Then
    '            sSql &= " AND " & FilterDDLDate.SelectedValue & " >='" & FilterPeriod1.Text & " 00:00:00' AND " & FilterDDLDate.SelectedValue & " <='" & FilterPeriod2.Text & " 23:59:59'"
    '        Else
    '            Exit Sub
    '        End If
    '    End If

    '    sSql &= " ORDER BY palletno"
    '    Session("TblPallet") = cKon.ambiltabel(sSql, "QL_mstpallet")
    'End Sub

    Private Sub ShowReport(ByVal sType As String)
        Dim sWhere As String = ""
        Dim rptName As String = ""
        Dim Dtl As String = ""
        Dim Slct As String = ""
        Dim Join As String = ""
        Try
            If FilterDDLType.SelectedValue = "Summary" Then
                If sType = "Print Excel" Then
                    report.Load(Server.MapPath(folderReport & "rptTransform_ReportXls2.rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptTransform_Report2.rpt"))
                End If
                rptName = "MatTransformationReportSummary"
            ElseIf FilterDDLType.SelectedValue = "Detail LV5" Then
                report.Load(Server.MapPath(folderReport & "rptTransform_ReportDtlLv5TrnsNo2.rpt"))
            Else
                If sType = "Print Excel" Then
                    report.Load(Server.MapPath(folderReport & "rptTransform_ReportDtlXls2.rpt"))
                Else
                    If DDLGrouping.SelectedValue = "transformno" Then
                        report.Load(Server.MapPath(folderReport & "rptTransform_ReportDtlTrnsNo2.rpt"))
                    ElseIf DDLGrouping.SelectedValue = "deptname" Then
                        report.Load(Server.MapPath(folderReport & "rptTransform_ReportDtlDeptName2.rpt"))
                    ElseIf DDLGrouping.SelectedValue = "fwh.gendesc" Then
                        report.Load(Server.MapPath(folderReport & "rptTransform_ReportDtlFromWH2.rpt"))
                    Else
                        report.Load(Server.MapPath(folderReport & "rptTransform_ReportDtlGrpName2.rpt"))
                    End If
                End If
                rptName = "MatTransformationReportDetail"
            End If


            If FilterDDLType.SelectedValue = "Detail" Then
                'Detail Input
                Dtl = " , [Detail Reference], [Seq],  [Mat. Type], [Code], [Description], [Qty], [Detail Note], [Value IDR], [Value USD], [Warehouse], [Total Qty], [Unit], [Cost], [Total Cost] "
                Join = "  LEFT JOIN ("
                'Detail Input
                Join &= " SELECT trd1.cmpcode,trd1.transformmstoid, transformdtl1oid [DtlOID], 'Detail Input' [Detail Reference], transformdtl1seq [Seq], transformdtl1reftype [Mat. Type], (SELECT itemCode From QL_mstitem lg WHERE lg.cmpcode=trd1.cmpcode AND itemoid=transformdtl1refoid) AS [Code], (SELECT itemLongDescription From QL_mstitem lg WHERE lg.cmpcode=trd1.cmpcode AND itemoid=transformdtl1refoid) AS [Description], transformdtl1qty [Qty], trd1.transformdtl1note [Detail Note], trd1.transformdtl1valueidr [Value IDR], trd1.transformdtl1valueusd [Value USD], fwh.gendesc [Warehouse], transformtotalqtyin [Total Qty], uin.gendesc [Unit],transformcostin [Cost], transformtotalcostin [Total Cost] FROM QL_trntransformdtl1 trd1 INNER JOIN QL_trntransformmst trx ON trd1.cmpcode=trx.cmpcode AND trd1.transformmstoid=trx.transformmstoid INNER JOIN QL_mstgen fwh ON fwh.genoid=trx.transformfromwhoid AND fwh.gengroup='WAREHOUSE' INNER JOIN QL_mstgen uin ON uin.genoid=trx.transformunitoid AND uin.gengroup='UNIT' "
                Join &= " UNION ALL "
                'Detail Output
                Join &= " SELECT trd2.cmpcode,trd2.transformmstoid, transformdtl2oid [DtlOID], 'Detail Output' [Detail Reference], transformdtl2seq [Seq], transformdtl2reftype [Mat. Type], (SELECT itemCode From QL_mstitem lg WHERE lg.cmpcode=trd2.cmpcode AND itemoid=transformdtl2refoid) AS [Code], (SELECT itemLongDescription From QL_mstitem lg WHERE lg.cmpcode=trd2.cmpcode AND itemoid=transformdtl2refoid) AS [Description], transformdtl2qty [Qty], trd2.transformdtl2note [Detail Note], trd2.transformdtl2valueidr [Value IDR], trd2.transformdtl2valueusd [Value USD], twh.gendesc [Warehouse], transformtotalqtyout [Total Qty], uout.gendesc [Unit], transformcostout [Cost], transformtotalcostout [Total Cost] FROM QL_trntransformdtl2 trd2 INNER JOIN QL_trntransformmst trx ON trd2.cmpcode=trx.cmpcode AND trd2.transformmstoid=trx.transformmstoid INNER JOIN QL_mstgen twh ON twh.genoid=trx.transformtowhoid AND twh.gengroup='WAREHOUSE' INNER JOIN QL_mstgen uout ON uout.genoid=trx.transformtounitoid AND uout.gengroup='UNIT' "
                Join &= " ) AS tbltmp ON tbltmp.cmpcode=transm.cmpcode AND tbltmp.transformmstoid=transm.transformmstoid"
            ElseIf FilterDDLType.SelectedValue = "Detail LV5" Then
                Dtl = " ,[Detail Reference], [Code], [Description], [Qty], [Warehouse], [Total Qty], [Unit], [Cost], [Total Cost]  ,[Amount IDR],  [Amount USD] "
                Join &= " LEFT JOIN (SELECT [CMP Code],[ID],[Detail Reference],[Code], [Description],SUM([Qty]) [Qty],[Warehouse], [Total Qty], SUM([Amount IDR]) [Amount IDR], SUM([Amount USD]) [Amount USD], [Unit], [Cost], [Total Cost] FROM ("
                'Detail Input
                Join &= " SELECT trd1.cmpcode [CMP Code], trd1.transformmstoid [ID], 'Detail Input' [Detail Reference], (SELECT itemCode From QL_mstitem lg WHERE lg.cmpcode=trd1.cmpcode AND itemoid=transformdtl1refoid) AS [Code], (SELECT itemLongDescription From QL_mstitem lg WHERE lg.cmpcode=trd1.cmpcode AND itemoid=transformdtl1refoid) AS [Description], transformdtl1qty [Qty], (transformdtl1qty*transformdtl1valueidr) [Amount IDR],(transformdtl1qty*transformdtl1valueusd) [Amount USD], fwh.gendesc [Warehouse], transformtotalqtyin [Total Qty], uin.gendesc [Unit],transformcostin [Cost], transformtotalcostin [Total Cost] FROM QL_trntransformdtl1 trd1 INNER JOIN QL_trntransformmst trx ON trd1.cmpcode=trx.cmpcode AND trd1.transformmstoid=trx.transformmstoid INNER JOIN QL_mstgen fwh ON fwh.genoid=trx.transformfromwhoid AND fwh.gengroup='WAREHOUSE' INNER JOIN QL_mstgen uin ON uin.genoid=trx.transformunitoid AND uin.gengroup='UNIT' UNION ALL"
                'Detail Output
                Join &= " SELECT trd2.cmpcode [CMP Code],trd2.transformmstoid [ID], 'Detail Output' [Detail Reference], (SELECT itemCode From QL_mstitem lg WHERE lg.cmpcode=trd2.cmpcode AND itemoid=transformdtl2refoid) AS [Code], (SELECT itemLongDescription From QL_mstitem lg WHERE lg.cmpcode=trd2.cmpcode AND itemoid=transformdtl2refoid) AS [Description], (transformdtl2qty) [Qty], (transformdtl2qty*transformdtl2valueidr) [Amount IDR],(transformdtl2qty*transformdtl2valueusd) [Amount USD], twh.gendesc [Warehouse], transformtotalqtyout [Total Qty], uout.gendesc [Unit], transformcostout [Cost], transformtotalcostout [Total Cost] FROM QL_trntransformdtl2 trd2 INNER JOIN QL_trntransformmst trx ON trd2.cmpcode=trx.cmpcode AND trd2.transformmstoid=trx.transformmstoid INNER JOIN QL_mstgen twh ON twh.genoid=trx.transformtowhoid AND twh.gengroup='WAREHOUSE' INNER JOIN QL_mstgen uout ON uout.genoid=trx.transformtounitoid AND uout.gengroup='UNIT' "
                Join &= " ) AS tbl GROUP BY [CMP Code],[ID],[Detail Reference],[Code],[Description],[Warehouse], [Total Qty], [Unit], [Cost], [Total Cost]"
                Join &= " ) AS tblTmp ON tbltmp.[CMP Code]=transm.cmpcode AND tbltmp.[ID]=transm.transformmstoid"
            End If
            sSql = " SELECT  transm.cmpcode [CMPCODE], (SELECT divname FROM QL_mstdivision div WHERE transm.cmpcode=div.cmpcode) [BU],transm.transformmstoid [OID], transformno [Transform No.], transformdate [Transform Date], dept.deptname [Department], transformtype [Type], fwh.gendesc [From Warehouse], twh.gendesc [To Warehouse], transformtotalqtyin [Total Qty In], transformtotalqtyout [Total Qty Out], uin.gendesc [Unit In], uout.gendesc [Unit Out], (CASE transm.suppoid WHEN 0 then 'None' ELSE (SELECT s.suppname FROM QL_mstsupp s where s.suppoid=transm.suppoid) END ) as [Group Name], transformcostin [Cost In], transformcostout [Cost Out], transformtotalcostin [Total Cost In], transformtotalcostout [Total Cost Out] ,(SELECT SUM(transformdtl1valueidr* transformdtl1qty) AS TotalValueIDR FROM QL_trntransformdtl1 dtl1 WHERE dtl1.cmpcode=transm.cmpcode AND dtl1.transformmstoid=transm.transformmstoid GROUP BY cmpcode,transformmstoid) [TotalValue IN IDR],(SELECT SUM(transformdtl1valueusd* transformdtl1qty) AS TotalValueUSD FROM QL_trntransformdtl1 dtl1 WHERE dtl1.cmpcode=transm.cmpcode AND dtl1.transformmstoid=transm.transformmstoid GROUP BY cmpcode,transformmstoid) [TotalValue IN USD] ,(SELECT SUM(transformdtl2valueidr* transformdtl2qty) AS TotalValueIDR FROM QL_trntransformdtl2 dtl2 WHERE dtl2.cmpcode=transm.cmpcode AND dtl2.transformmstoid=transm.transformmstoid GROUP BY cmpcode,transformmstoid) [TotalValue Out IDR],(SELECT SUM(transformdtl2valueusd* transformdtl2qty) AS TotalValueUSD FROM QL_trntransformdtl2 dtl2 WHERE dtl2.cmpcode=transm.cmpcode AND dtl2.transformmstoid=transm.transformmstoid GROUP BY cmpcode,transformmstoid) [TotalValue Out USD] , c.currcode [Currency],transformmststatus [Status], transformmstnote [Note], transm.createtime [Create Date], transm.createuser [Create User], (CASE transformmststatus WHEN 'In Process' THEN '' ELSE transm.upduser END) AS [PostUser], (CASE transformmststatus WHEN 'In Process' THEN CONVERT(DATETIME,'1/1/1900') ELSE transm.updtime END) AS [PostDate] " & Dtl & " FROM QL_trntransformmst transm INNER JOIN QL_mstdept dept ON dept.deptoid=transm.deptoid INNER JOIN QL_mstgen fwh ON fwh.genoid=transm.transformfromwhoid AND fwh.gengroup='WAREHOUSE' INNER JOIN QL_mstgen twh ON twh.genoid=transm.transformtowhoid AND twh.gengroup='WAREHOUSE' INNER JOIN QL_mstgen uin ON uin.genoid=transm.transformunitoid AND uin.gengroup='UNIT' INNER JOIN QL_mstgen uout ON uout.genoid=transm.transformtounitoid AND uout.gengroup='UNIT' LEFT JOIN QL_mstcurr c ON c.curroid=transm.curroid " & Join & " WHERE transm.cmpcode='" & DDLBusUnit.SelectedValue & "' "

            If FilterDDLStatus.SelectedValue = "In Process" Then
                sSql &= " AND transm.transformmststatus IN ('In Process')"
            ElseIf FilterDDLStatus.SelectedValue = "Post" Then
                sSql &= " AND transm.transformmststatus IN ('Post','Closed')"
            End If

            If lbDept.Items.Count > 0 Then
                Dim sOid As String = ""
                For C1 As Integer = 0 To lbDept.Items.Count - 1
                    sOid &= lbDept.Items(C1).Value & ","
                Next
                If sOid <> "" Then
                    sOid = Left(sOid, sOid.Length - 1)
                    sSql &= " AND transm.deptoid IN (" & sOid & ")"
                End If
            End If

            'Filter Type
            If transformtype.SelectedValue <> "All" Then
                sSql &= " AND transm.transformtype='" & transformtype.SelectedValue & "' "
            End If

            If lbFrWh.Items.Count > 0 Then
                Dim sOid As String = ""
                For C1 As Integer = 0 To lbFrWh.Items.Count - 1
                    sOid &= lbFrWh.Items(C1).Value & ","
                Next
                If sOid <> "" Then
                    sOid = Left(sOid, sOid.Length - 1)
                    sSql &= " AND transm.transformfromwhoid IN (" & sOid & ")"
                End If
            End If

            'Flter Group Name
            If cbsupp.Checked Then
                sSql &= " AND transm.suppoid= " & suppoid.SelectedValue & " "
            End If

            If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
                If IsValidPeriod() Then
                    sSql &= " AND " & FilterDDLDate.SelectedValue & " >='" & FilterPeriod1.Text & " 00:00:00' AND " & FilterDDLDate.SelectedValue & " <='" & FilterPeriod2.Text & " 23:59:59'"
                Else
                    Exit Sub
                End If
            End If

            If transformno.Text <> "" Then
                'Dim trns As String

                If DDLTransNo.SelectedValue = "Transform No." Then
                    transformno.Text = transformno.Text.Replace(",", "','")
                    sSql &= " AND (transformno IN ('" & transformno.Text & "'))"
                Else
                    sSql &= " AND (transm.transformmstoid IN (" & transformno.Text & "))"
                End If

                'If DDLTransNo.SelectedValue = "Transform No." Then
                '    Dim sTransno() As String = Split(transformno.Text, ";")
                '    sSql &= " AND ("
                '    For c1 As Integer = 0 To sTransno.Length - 1
                '        sSql &= " transformno LIKE '%" & Tchar(sTransno(c1)) & "%'"
                '        If c1 < sTransno.Length - 1 Then
                '            sSql &= " OR "
                '        End If
                '    Next
                '    sSql &= ")"
                'Else
                '    Dim sTransno() As String = Split(transformno.Text, ";")
                '    sSql &= " AND ("
                '    For c1 As Integer = 0 To sTransno.Length - 1
                '        sSql &= " transm.transformmstoid = " & ToDouble(sTransno(c1))
                '        If c1 < sTransno.Length - 1 Then
                '            sSql &= " OR "
                '        End If
                '    Next
                '    sSql &= ")"
                'End If
            End If


            If FilterDDLType.SelectedValue = "Summary" Then
                sSql &= " ORDER BY [CMPCODE] ASC, [Transform No.] ASC, [OID] ASC, " & DDLSorting.SelectedValue & " " & DDLOrderby.SelectedValue & ""
            ElseIf FilterDDLType.SelectedValue = "Detail LV5" Then
                sSql &= " ORDER BY [CMPCODE] ASC, [Transform No.] ASC, [OID] ASC, " & DDLSorting.SelectedValue & " " & DDLOrderby.SelectedValue & " , [Detail Reference] DESC "
            Else
                If DDLGrouping.SelectedValue = "transformno" Then
                    sSql &= " ORDER BY [CMPCODE] ASC, [Transform No.] ASC, [OID] ASC, " & DDLSorting.SelectedValue & " " & DDLOrderby.SelectedValue & " , [Detail Reference] DESC, [Seq] ASC"
                ElseIf DDLGrouping.SelectedValue = "deptname" Then
                    If DDLSorting.SelectedValue = "[Transform No.]" Then
                        sSql &= " ORDER BY [CMPCODE] ASC, [Department] ASC, [Transform No.] " & DDLOrderby.SelectedValue & ", [OID] " & DDLOrderby.SelectedValue & ", [Detail Reference] DESC, [Seq] ASC"
                    Else
                        sSql &= " ORDER BY [CMPCODE] ASC, [Department] ASC, " & DDLSorting.SelectedValue & " " & DDLOrderby.SelectedValue & ", [Detail Reference] DESC, [Seq] ASC"
                    End If
                ElseIf DDLGrouping.SelectedValue = "fwh.gendesc" Then
                    If DDLSorting.SelectedValue = "[Transform No.]" Then
                        sSql &= " ORDER BY [CMPCODE] ASC, [From Warehouse] ASC, [Transform No.] " & DDLOrderby.SelectedValue & ", [OID] " & DDLOrderby.SelectedValue & ", [Detail Reference] DESC, [Seq] ASC"
                    Else
                        sSql &= " ORDER BY [CMPCODE] ASC, [From Warehouse] ASC, " & DDLSorting.SelectedValue & " " & DDLOrderby.SelectedValue & ", [Detail Reference] DESC, [Seq] ASC"
                    End If
                Else
                    If DDLSorting.SelectedValue = "[Transform No.]" Then
                        sSql &= " ORDER BY [CMPCODE] ASC, [Group Name] ASC, [Transform No.] " & DDLOrderby.SelectedValue & ", [OID] " & DDLOrderby.SelectedValue & ", [Detail Reference] DESC, [Seq] ASC"
                    Else
                        sSql &= " ORDER BY [CMPCODE] ASC, [Group Name] ASC, " & DDLSorting.SelectedValue & " " & DDLOrderby.SelectedValue & ", [Detail Reference] DESC, [Seq] ASC"
                    End If
                End If
            End If

            Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trntransformmst")
            transformno.Text = transformno.Text.Replace("','", ",")
            Dim dvTbl As DataView = dtTbl.DefaultView

            report.SetDataSource(dvTbl.ToTable)
            report.PrintOptions.PaperSize = PaperSize.PaperFolio

            cProc.SetDBLogonForReport(report)

            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, rptName)
                report.Close()
                report.Dispose()
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, rptName)
                report.Close()
                report.Dispose()
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Private Sub InitAllDDL()
        ' DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)
        If FillDDL(DDLBusUnit, sSql) Then
            InitDDLDept()
        End If
        'DDL Warehouse
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='MATERIAL LOCATION'"
        FillDDL(DDLFrWh, sSql)
        ' Fill DDL Group Name
        sSql = "SELECT suppoid, suppname FROM QL_mstsupp WHERE cmpcode='" & CompnyCode & "' AND suppres1='PEMBORONG' AND activeflag='ACTIVE' ORDER BY suppname"
        FillDDLWithAdditionalText(suppoid, sSql, "None", "0")
    End Sub

    Private Sub InitDDLDept()
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND activeflag='ACTIVE' ORDER BY deptname"
        FillDDL(DDLDept, sSql)
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmTransformReport2.aspx")

        End If

        If checkPagePermission("~\ReportForm\frmTransformReport2.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        Page.Title = CompnyName & " - Material Transformation Report"
        If Not Page.IsPostBack Then
            InitAllDDL()
            FilterDDLType_SelectedIndexChanged(Nothing, Nothing)
            FilterDDLStatus_SelectedIndexChanged(Nothing, Nothing)
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListTrans") Is Nothing And Session("EmptyListTrans") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListTrans") Then
                Session("EmptyListTrans") = Nothing
                cProc.SetModalPopUpExtender(btnHideListTrans, pnlListTrans, mpeListTrans, True)
            End If
        End If

        If Not Session("WarningListTrans") Is Nothing And Session("WarningListTrans") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListTrans") Then
                Session("WarningListTrans") = Nothing
                cProc.SetModalPopUpExtender(btnHideListTrans, pnlListTrans, mpeListTrans, True)
            End If
        End If

        'If Not Session("EmptyListPallet") Is Nothing And Session("EmptyListPallet") <> "" Then
        '    If lblPopUpMsg.Text = Session("EmptyListPallet") Then
        '        Session("EmptyListPallet") = Nothing
        '        cProc.SetModalPopUpExtender(btnHideListPallet, pnlListPallet, mpeListPallet, True)
        '    End If
        'End If
        'If Not Session("WarningListPallet") Is Nothing And Session("WarningListPallet") <> "" Then
        '    If lblPopUpMsg.Text = Session("WarningListPallet") Then
        '        Session("WarningListPallet") = Nothing
        '        cProc.SetModalPopUpExtender(btnHideListPallet, pnlListPallet, mpeListPallet, True)
        '    End If
        'End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        'btnClearSupp_Click(Nothing, Nothing)
    End Sub

    Protected Sub FilterDDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLType.SelectedIndexChanged
        If FilterDDLType.SelectedValue = "Summary" Then
            'Fill DDL Grouping
            DDLGrouping.Items.Clear()
            DDLGrouping.Items.Add("Transform No.")
            DDLGrouping.Items.Item(DDLGrouping.Items.Count - 1).Value = "transformno"
        ElseIf FilterDDLType.SelectedValue = "Detail LV5" Then
            DDLGrouping.Items.Clear()
            DDLGrouping.Items.Add("Transform No.")
            DDLGrouping.Items.Item(DDLGrouping.Items.Count - 1).Value = "transformno"
        Else
            DDLGrouping.Items.Clear()
            DDLGrouping.Items.Add("Transform No.")
            DDLGrouping.Items.Item(DDLGrouping.Items.Count - 1).Value = "transformno"
            DDLGrouping.Items.Add("Department")
            DDLGrouping.Items.Item(DDLGrouping.Items.Count - 1).Value = "deptname"
            DDLGrouping.Items.Add("From Warehouse")
            DDLGrouping.Items.Item(DDLGrouping.Items.Count - 1).Value = "fwh.gendesc"
            DDLGrouping.Items.Add("Group Name")
            DDLGrouping.Items.Item(DDLGrouping.Items.Count - 1).Value = "suppname"
        End If
        DDLGrouping_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub FilterDDLStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLStatus.SelectedIndexChanged
        If FilterDDLStatus.SelectedValue = "Post" Then
            'Fill DDL Date
            FilterDDLDate.Items.Clear()
            FilterDDLDate.Items.Add("Transform Date")
            FilterDDLDate.Items.Item(FilterDDLDate.Items.Count - 1).Value = "transm.transformdate"
            FilterDDLDate.Items.Add("Post Date")
            FilterDDLDate.Items.Item(FilterDDLDate.Items.Count - 1).Value = "transm.updtime"
        Else
            'Fill DDL Date
            FilterDDLDate.Items.Clear()
            FilterDDLDate.Items.Add("Transform Date")
            FilterDDLDate.Items.Item(FilterDDLDate.Items.Count - 1).Value = "transm.transformdate"
        End If
    End Sub

    Protected Sub btnAddDept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddDept.Click
        If DDLDept.SelectedValue <> "" Then
            If lbDept.Items.Count > 0 Then
                If Not lbDept.Items.Contains(lbDept.Items.FindByValue(DDLDept.SelectedValue)) Then
                    Dim objList As New ListItem
                    objList.Text = DDLDept.SelectedItem.Text : objList.Value = DDLDept.SelectedValue
                    lbDept.Items.Add(objList)
                End If
            Else
                Dim objList As New ListItem
                objList.Text = DDLDept.SelectedItem.Text : objList.Value = DDLDept.SelectedValue
                lbDept.Items.Add(objList)
            End If
        End If
    End Sub

    Protected Sub btnMinDept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMinDept.Click
        If lbDept.Items.Count > 0 Then
            Dim objList As ListItem = lbDept.SelectedItem
            lbDept.Items.Remove(objList)
        End If
    End Sub

    Protected Sub btnAddFrWh_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddFrWh.Click
        If DDLFrWh.SelectedValue <> "" Then
            If lbFrWh.Items.Count > 0 Then
                If Not lbFrWh.Items.Contains(lbFrWh.Items.FindByValue(DDLFrWh.SelectedValue)) Then
                    Dim objList As New ListItem
                    objList.Text = DDLFrWh.SelectedItem.Text : objList.Value = DDLFrWh.SelectedValue
                    lbFrWh.Items.Add(objList)
                End If
            Else
                Dim objList As New ListItem
                objList.Text = DDLFrWh.SelectedItem.Text : objList.Value = DDLFrWh.SelectedValue
                lbFrWh.Items.Add(objList)
            End If
        End If
    End Sub

    Protected Sub btnMinFrWh_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMinFrWh.Click
        If lbFrWh.Items.Count > 0 Then
            Dim objList As ListItem = lbFrWh.SelectedItem
            lbFrWh.Items.Remove(objList)
        End If
    End Sub

    Protected Sub DDLTransNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLTransNo.SelectedIndexChanged
        transformno.Text = ""
    End Sub

    Protected Sub imbFindTrans_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindTrans.Click
        If IsValidPeriod() Then
            FilterDDLListTrans.SelectedIndex = -1 : FilterTextListTrans.Text = ""
            Session("TblTrans") = Nothing : Session("TblTransView") = Nothing : gvListTrans.DataSource = Nothing : gvListTrans.DataBind()
            cProc.SetModalPopUpExtender(btnHideListTrans, pnlListTrans, mpeListTrans, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbEraseTrans_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseTrans.Click
        transformno.Text = ""
    End Sub

    Protected Sub btnFindListTrans_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListTrans.Click
        If Session("TblTrans") Is Nothing Then
            BindListTrans()
            If Session("TblTrans").Rows.Count <= 0 Then
                Session("EmptyListTrans") = "Transformation data can't be found!"
                showMessage(Session("EmptyListTrans"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListTrans.SelectedValue & " LIKE '%" & Tchar(FilterTextListTrans.Text) & "%'"
        If UpdateCheckedTrans() Then
            Dim dv As DataView = Session("TblTrans").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblTransView") = dv.ToTable
                gvListTrans.DataSource = Session("TblTransView")
                gvListTrans.DataBind()
                dv.RowFilter = ""
                mpeListTrans.Show()
            Else
                dv.RowFilter = ""
                Session("TblTransView") = Nothing
                gvListTrans.DataSource = Session("TblTransView")
                gvListTrans.DataBind()
                Session("WarningListTrans") = "Transformation data can't be found!"
                showMessage(Session("WarningListTrans"), 2)
            End If
        Else
            mpeListTrans.Show()
        End If
    End Sub

    Protected Sub btnAllListTrans_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListTrans.Click
        FilterDDLListTrans.SelectedIndex = -1 : FilterTextListTrans.Text = ""
        If Session("TblTrans") Is Nothing Then
            BindListTrans()
            If Session("TblTrans").Rows.Count <= 0 Then
                Session("EmptyListTrans") = "Transformation data can't be found!"
                showMessage(Session("EmptyListTrans"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedTrans() Then
            Dim dt As DataTable = Session("TblTrans")
            Session("TblTransView") = dt
            gvListTrans.DataSource = Session("TblTransView")
            gvListTrans.DataBind()
        End If
        mpeListTrans.Show()
    End Sub

    Protected Sub btnSelectAllTrans_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllTrans.Click
        If Not Session("TblTransView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblTransView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblTrans")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "transformmstoid=" & dtTbl.Rows(C1)("transformmstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblTrans") = objTbl
                Session("TblTransView") = dtTbl
                gvListTrans.DataSource = Session("TblTransView")
                gvListTrans.DataBind()
            End If
            mpeListTrans.Show()
        Else
            Session("WarningListTrans") = "Please show some Kiln Dry Plan data first!"
            showMessage(Session("WarningListTrans"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneTrans_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneTrans.Click
        If Not Session("TblTransView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblTransView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblTrans")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "transformmstoid=" & dtTbl.Rows(C1)("transformmstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblTrans") = objTbl
                Session("TblTransView") = dtTbl
                gvListTrans.DataSource = Session("TblTransView")
                gvListTrans.DataBind()
            End If
            mpeListTrans.Show()
        Else
            Session("WarningListTrans") = "Please show some Transformation data first!"
            showMessage(Session("WarningListTrans"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedTrans_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedTrans.Click
        If Session("TblTrans") Is Nothing Then
            Session("WarningListTrans") = "Selected Transformation data can't be found!"
            showMessage(Session("WarningListTrans"), 2)
            Exit Sub
        End If
        If UpdateCheckedTrans() Then
            Dim dtTbl As DataTable = Session("TblTrans")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListTrans.SelectedIndex = -1 : FilterTextListTrans.Text = ""
                Session("TblTransView") = dtView.ToTable
                gvListTrans.DataSource = Session("TblTransView")
                gvListTrans.DataBind()
                dtView.RowFilter = ""
                mpeListTrans.Show()
            Else
                dtView.RowFilter = ""
                Session("TblTransView") = Nothing
                gvListTrans.DataSource = Session("TblTransView")
                gvListTrans.DataBind()
                Session("WarningListTrans") = "Selected Transformation data can't be found!"
                showMessage(Session("WarningListTrans"), 2)
            End If
        Else
            mpeListTrans.Show()
        End If
    End Sub

    Protected Sub gvListTrans_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListTrans.PageIndexChanging
        If UpdateCheckedTrans2() Then
            gvListTrans.PageIndex = e.NewPageIndex
            gvListTrans.DataSource = Session("TblTransView")
            gvListTrans.DataBind()
        End If
        mpeListTrans.Show()
    End Sub 'OK

    Protected Sub lkbAddToListListTrans_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListTrans.Click
        If Not Session("TblTrans") Is Nothing Then
            If UpdateCheckedTrans() Then
                Dim dtTbl As DataTable = Session("TblTrans")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If transformno.Text <> "" Then
                            If DDLTransNo.SelectedValue = "Transform No." Then
                                If dtView(C1)("transformno") <> "" Then
                                    'transformno.Text &= ";" + vbCrLf + dtView(C1)("transformno")
                                    transformno.Text &= "," + dtView(C1)("transformno")
                                End If
                            Else
                                'transformno.Text &= ";" + vbCrLf + dtView(C1)("transformmstoid").ToString
                                transformno.Text &= "," + dtView(C1)("transformmstoid").ToString
                            End If
                        Else
                            If DDLTransNo.SelectedValue = "Transform No." Then
                                If dtView(C1)("transformno") <> "" Then
                                    transformno.Text &= dtView(C1)("transformno")
                                End If
                            Else
                                transformno.Text &= dtView(C1)("transformmstoid").ToString
                            End If
                        End If
                    Next
                    dtView.RowFilter = ""
                    'DDLBusUnit.Enabled = False
                    'DDLBusUnit.CssClass = "inpTextDisabled"
                    cProc.SetModalPopUpExtender(btnHideListTrans, pnlListTrans, mpeListTrans, False)
                Else
                    Session("WarningListTrans") = "Please select Kiln Dry Plan to add to list!"
                    showMessage(Session("WarningListTrans"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListTrans") = "Please show some Kiln Dry Plan data first!"
            showMessage(Session("WarningListTrans"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbCloseListTrans_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListTrans.Click
        cProc.SetModalPopUpExtender(btnHideListTrans, pnlListTrans, mpeListTrans, False)
    End Sub

    'Protected Sub imbFindPallet_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindPallet.Click
    '    If IsValidPeriod() Then
    '        FilterDDLListPallet.SelectedIndex = -1 : FilterTextListPallet.Text = ""
    '        Session("TblPallet") = Nothing : Session("TblPalletView") = Nothing : gvListPallet.DataSource = Nothing : gvListPallet.DataBind()
    '        cProc.SetModalPopUpExtender(btnHideListPallet, pnlListPallet, mpeListPallet, True)
    '    Else
    '        Exit Sub
    '    End If
    'End Sub

    'Protected Sub imbErasePallet_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbErasePallet.Click
    '    palletno.Text = ""
    'End Sub

    'Protected Sub btnFindListPallet_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListPallet.Click
    '    If Session("TblPallet") Is Nothing Then
    '        BindListPallet()
    '        If Session("TblPallet").Rows.Count <= 0 Then
    '            Session("EmptyListPallet") = "Pallet data can't be found!"
    '            showMessage(Session("EmptyListPallet"), 2)
    '            Exit Sub
    '        End If
    '    End If
    '    Dim sPlus As String = FilterDDLListPallet.SelectedValue & " LIKE '%" & Tchar(FilterTextListPallet.Text) & "%'"
    '    If UpdateCheckedPallet() Then
    '        Dim dv As DataView = Session("TblPallet").DefaultView
    '        dv.RowFilter = sPlus
    '        If dv.Count > 0 Then
    '            Session("TblPalletView") = dv.ToTable
    '            gvListPallet.DataSource = Session("TblPalletView")
    '            gvListPallet.DataBind()
    '            dv.RowFilter = ""
    '            mpeListPallet.Show()
    '        Else
    '            dv.RowFilter = ""
    '            Session("TblPalletView") = Nothing
    '            gvListPallet.DataSource = Session("TblPalletView")
    '            gvListPallet.DataBind()
    '            Session("WarningListPallet") = "Pallet data can't be found!"
    '            showMessage(Session("WarningListPallet"), 2)
    '        End If
    '    Else
    '        mpeListPallet.Show()
    '    End If
    'End Sub

    'Protected Sub btnAllListPallet_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListPallet.Click
    '    FilterDDLListPallet.SelectedIndex = -1 : FilterTextListPallet.Text = ""
    '    If Session("TblPallet") Is Nothing Then
    '        BindListPallet()
    '        If Session("TblPallet").Rows.Count <= 0 Then
    '            Session("EmptyListPallet") = "Pallet data can't be found!"
    '            showMessage(Session("EmptyListPallet"), 2)
    '            Exit Sub
    '        End If
    '    End If
    '    If UpdateCheckedPallet() Then
    '        Dim dt As DataTable = Session("TblPallet")
    '        Session("TblPalletView") = dt
    '        gvListPallet.DataSource = Session("TblPalletView")
    '        gvListPallet.DataBind()
    '    End If
    '    mpeListPallet.Show()
    'End Sub

    'Protected Sub btnSelectAllPallet_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllPallet.Click
    '    If Not Session("TblPalletView") Is Nothing Then
    '        Dim dtTbl As DataTable = Session("TblPalletView")
    '        If dtTbl.Rows.Count > 0 Then
    '            Dim objTbl As DataTable = Session("TblPallet")
    '            Dim objView As DataView = objTbl.DefaultView
    '            objView.AllowEdit = True
    '            objView.RowFilter = ""
    '            For C1 As Integer = 0 To dtTbl.Rows.Count - 1
    '                objView.RowFilter = "palletoid=" & dtTbl.Rows(C1)("palletoid")
    '                objView(0)("checkvalue") = "True"
    '                dtTbl.Rows(C1)("checkvalue") = "True"
    '                objView.RowFilter = ""
    '            Next
    '            objTbl.AcceptChanges()
    '            Session("TblPallet") = objTbl
    '            Session("TblPalletView") = dtTbl
    '            gvListPallet.DataSource = Session("TblPalletView")
    '            gvListPallet.DataBind()
    '        End If
    '        mpeListPallet.Show()
    '    Else
    '        Session("WarningListPallet") = "Please show some Pallet data first!"
    '        showMessage(Session("WarningListPallet"), 2)
    '    End If
    'End Sub

    'Protected Sub btnSelectNonePallet_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNonePallet.Click
    '    If Not Session("TblPalletView") Is Nothing Then
    '        Dim dtTbl As DataTable = Session("TblPalletView")
    '        If dtTbl.Rows.Count > 0 Then
    '            Dim objTbl As DataTable = Session("TblPallet")
    '            Dim objView As DataView = objTbl.DefaultView
    '            objView.AllowEdit = True
    '            objView.RowFilter = ""
    '            For C1 As Integer = 0 To dtTbl.Rows.Count - 1
    '                objView.RowFilter = "palletoid=" & dtTbl.Rows(C1)("palletoid")
    '                objView(0)("checkvalue") = "False"
    '                dtTbl.Rows(C1)("checkvalue") = "False"
    '                objView.RowFilter = ""
    '            Next
    '            objTbl.AcceptChanges()
    '            Session("TblPallet") = objTbl
    '            Session("TblPalletView") = dtTbl
    '            gvListPallet.DataSource = Session("TblPalletView")
    '            gvListPallet.DataBind()
    '        End If
    '        mpeListPallet.Show()
    '    Else
    '        Session("WarningListPallet") = "Please show some Pallet data first!"
    '        showMessage(Session("WarningListPallet"), 2)
    '    End If
    'End Sub

    'Protected Sub btnViewCheckedPallet_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedPallet.Click
    '    If Session("TblPallet") Is Nothing Then
    '        Session("WarningListPallet") = "Selected Pallet data can't be found!"
    '        showMessage(Session("WarningListPallet"), 2)
    '        Exit Sub
    '    End If
    '    If UpdateCheckedPallet() Then
    '        Dim dtTbl As DataTable = Session("TblPallet")
    '        Dim dtView As DataView = dtTbl.DefaultView
    '        dtView.RowFilter = "checkvalue='True'"
    '        If dtView.Count > 0 Then
    '            FilterDDLListPallet.SelectedIndex = -1 : FilterTextListPallet.Text = ""
    '            Session("TblPalletView") = dtView.ToTable
    '            gvListPallet.DataSource = Session("TblPalletView")
    '            gvListPallet.DataBind()
    '            dtView.RowFilter = ""
    '            mpeListPallet.Show()
    '        Else
    '            dtView.RowFilter = ""
    '            Session("TblPalletView") = Nothing
    '            gvListPallet.DataSource = Session("TblPalletView")
    '            gvListPallet.DataBind()
    '            Session("WarningListPallet") = "Selected Pallet data can't be found!"
    '            showMessage(Session("WarningListPallet"), 2)
    '        End If
    '    Else
    '        mpeListPallet.Show()
    '    End If
    'End Sub

    'Protected Sub gvListPallet_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListPallet.PageIndexChanging
    '    If UpdateCheckedPallet2() Then
    '        gvListPallet.PageIndex = e.NewPageIndex
    '        gvListPallet.DataSource = Session("TblPalletView")
    '        gvListPallet.DataBind()
    '    End If
    '    mpeListPallet.Show()
    'End Sub

    'Protected Sub lbAddToListListPallet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListListPallet.Click
    '    If Not Session("TblPallet") Is Nothing Then
    '        If UpdateCheckedPallet() Then
    '            Dim dtTbl As DataTable = Session("TblPallet")
    '            Dim dtView As DataView = dtTbl.DefaultView
    '            dtView.RowFilter = "checkvalue='True'"
    '            Dim iCheck As Integer = dtView.Count
    '            If iCheck > 0 Then
    '                For C1 As Integer = 0 To dtView.Count - 1
    '                    If palletno.Text <> "" Then
    '                        If dtView(C1)("palletno") <> "" Then
    '                            palletno.Text &= ";" + vbCrLf + dtView(C1)("palletno")
    '                        End If
    '                    Else
    '                        If dtView(C1)("palletno") <> "" Then
    '                            palletno.Text &= dtView(C1)("palletno")
    '                        End If
    '                    End If
    '                Next
    '                dtView.RowFilter = ""
    '                'DDLBusUnit.Enabled = False
    '                cProc.SetModalPopUpExtender(btnHideListPallet, pnlListPallet, mpeListPallet, False)
    '            Else
    '                Session("WarningListPallet") = "Please select Pallet to add to list!"
    '                showMessage(Session("WarningListPallet"), 2)
    '                Exit Sub
    '            End If
    '        End If
    '    Else
    '        Session("WarningListPallet") = "Please show some Pallet data first!"
    '        showMessage(Session("WarningListPallet"), 2)
    '        Exit Sub
    '    End If
    'End Sub

    'Protected Sub lkbCloseListPallet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListPallet.Click
    '    cProc.SetModalPopUpExtender(btnHideListPallet, pnlListPallet, mpeListPallet, False)
    'End Sub

    Protected Sub DDLGrouping_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLGrouping.SelectedIndexChanged
        If FilterDDLType.SelectedValue = "Summary" Then
            'Fill DDL Sorting
            DDLSorting.Items.Clear()
            DDLSorting.Items.Add("Department")
            DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "[Department]"
            DDLSorting.Items.Add("From Warehouse")
            DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "[From Warehouse]"
            DDLSorting.Items.Add("Group Name")
            DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "[Group Name]"
        Else
            If DDLGrouping.SelectedValue = "transformno" Then
                'Fill DDL Sorting
                DDLSorting.Items.Clear()
                DDLSorting.Items.Add("Department")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "[Department]"
                DDLSorting.Items.Add("From Warehouse")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "[From Warehouse]"
                DDLSorting.Items.Add("Group Name")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "[Group Name]"
            ElseIf DDLGrouping.SelectedValue = "deptname" Then
                'Fill DDL Sorting
                DDLSorting.Items.Clear()
                DDLSorting.Items.Add("Transform No.")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "[Transform No.]"
                DDLSorting.Items.Add("From Warehouse")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "[From Warehouse]"
                DDLSorting.Items.Add("Group Name")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "[Group Name]"
            ElseIf DDLGrouping.SelectedValue = "fwh.gendesc" Then
                'Fill DDL Sorting
                DDLSorting.Items.Clear()
                DDLSorting.Items.Add("Transform No.")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "[Transform No.]"
                DDLSorting.Items.Add("Department")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "[Department]"
                DDLSorting.Items.Add("Group Name")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "[Group Name]"
            Else
                'Fill DDL Sorting
                DDLSorting.Items.Clear()
                DDLSorting.Items.Add("Transform No.")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "[Transform No.]"
                DDLSorting.Items.Add("Department")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "[Department]"
                DDLSorting.Items.Add("From Warehouse")
                DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "[From Warehouse]"
            End If
        End If
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmTransformReport2.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        If Not report Is Nothing Then
            If report.IsLoaded Then
                report.Dispose()
                report.Close()
            End If
        End If
    End Sub
#End Region

End Class
