Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_DailyCashReport
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyCode
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        If FilterDDLAccount.SelectedValue = "" Then
            showMessage("Please select Account first!", 2)
            Exit Sub
        End If
        crvReportForm.ReportSource = Nothing
        Try
            If DDLType.SelectedValue = "Summary" Then
                report.Load(Server.MapPath(folderReport & "rptDailyCash.rpt"))

                Dim sPeriod As String = ""
                Dim sWhere As String = "WHERE CompCode='" & FilterDDLDiv.SelectedValue & "' AND Oid=" & FilterDDLAccount.SelectedValue
                Dim iAcctgOid As Integer = CInt(FilterDDLAccount.SelectedValue)
                Dim sAnd As String = ""

                If IsValidPeriod() Then
                    sPeriod = Format(CDate(FilterPeriod1.Text), "dd MMMM yyyy") & " s/d " & Format(CDate(FilterPeriod2.Text), "dd MMMM yyyy")
                    sWhere &= " AND Tanggal BETWEEN '" & FilterPeriod1.Text & "' AND '" & FilterPeriod2.Text & "'"
                    sAnd &= " AND gm.gldate<'" & FilterPeriod1.Text & "' "
                Else
                    Exit Sub
                End If
                report.SetParameterValue("sCompnyName", FilterDDLDiv.SelectedItem.Text)
                report.SetParameterValue("sPeriod", sPeriod)
                report.SetParameterValue("sWhere", sWhere)
                'report.SetParameterValue("sWhere2", "WHERE b.acctgoid IN (" & sAcctgOid & ")")
                report.SetParameterValue("iAcctgOid", iAcctgOid)
                report.SetParameterValue("sAnd", sAnd)
                report.SetParameterValue("sCurrency", FilterCurrency.SelectedValue)
                report.SetParameterValue("cmpcode", FilterDDLDiv.SelectedValue)

                cProc.SetDBLogonForReport(report)

                report.PrintOptions.PaperSize = PaperSize.PaperA4
                If FilterCurrency.SelectedValue.ToUpper = "VALAS" Then
                    report.PrintOptions.PaperOrientation = PaperOrientation.Landscape
                Else
                    report.PrintOptions.PaperOrientation = PaperOrientation.Portrait
                End If
            ElseIf DDLType.SelectedValue = "Detail" Then
                If sType = "Print Excel" Then
                    report.Load(Server.MapPath(folderReport & "rptDailyKasBankDtlXls.rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptDailyKasBankDtl.rpt"))
                End If

                Dim sPeriod As String = ""
                Dim sWhere As String = " WHERE cmpcode='" & FilterDDLDiv.SelectedValue & "' AND acctgoid=" & FilterDDLAccount.SelectedValue

                If IsValidPeriod() Then
                    sPeriod = Format(CDate(FilterPeriod1.Text), "dd MMMM yyyy") & " s/d " & Format(CDate(FilterPeriod2.Text), "dd MMMM yyyy")
                    sWhere &= " AND [Tanggal]>=CAST('" & FilterPeriod1.Text & " 00:00' AS DATETIME) AND [Tanggal]<=CAST('" & FilterPeriod2.Text & " 23:59' AS DATETIME)"
                Else
                    Exit Sub
                End If
                report.SetParameterValue("sPeriod", sPeriod)
                report.SetParameterValue("sWhere", sWhere)
                report.SetParameterValue("iAcctgOid", ToInteger(FilterDDLAccount.SelectedValue))
                report.SetParameterValue("cmpcode", FilterDDLDiv.SelectedValue)
                report.SetParameterValue("PrintUserID", Session("UserID"))
                report.SetParameterValue("PrintUserName", GetStrData("SELECT profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
                report.SetParameterValue("sTypeCB", "K")

                cProc.SetDBLogonForReport(report)

                report.PrintOptions.PaperSize = PaperSize.PaperFolio
                'ElseIf DDLType.SelectedValue = "Detail2" Then
                '    If sType = "Print Excel" Then
                '        report.Load(Server.MapPath(folderReport & "rptDailyKasBankDtlXls.rpt"))
                '    Else
                '        report.Load(Server.MapPath(folderReport & "rptDailyKasBankDtl.rpt"))
                '    End If

                '    Dim sPeriod As String = ""
                '    Dim sWhere As String = " WHERE cmpcode='" & FilterDDLDiv.SelectedValue & "' AND acctgoid=" & FilterDDLAccount.SelectedValue

                '    If IsValidPeriod() Then
                '        sPeriod = Format(CDate(FilterPeriod1.Text), "dd MMMM yyyy") & " s/d " & Format(CDate(FilterPeriod2.Text), "dd MMMM yyyy")
                '        sWhere &= " AND [Tanggal]>=CAST('" & FilterPeriod1.Text & " 00:00' AS DATETIME) AND [Tanggal]<=CAST('" & FilterPeriod2.Text & " 23:59' AS DATETIME)"
                '    Else
                '        Exit Sub
                '    End If
                '    report.SetParameterValue("sPeriod", sPeriod)
                '    report.SetParameterValue("sWhere", sWhere)
                '    report.SetParameterValue("iAcctgOid", ToInteger(FilterDDLAccount.SelectedValue))
                '    report.SetParameterValue("cmpcode", FilterDDLDiv.SelectedValue)
                '    report.SetParameterValue("PrintUserID", Session("UserID"))
                '    report.SetParameterValue("PrintUserName", GetStrData("SELECT profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
                '    report.SetParameterValue("sTypeCB", "K")

                '    cProc.SetDBLogonForReport(report)

                '    report.PrintOptions.PaperSize = PaperSize.PaperFolio
                End If

                If sType = "View" Then
                    Session("ViewReport") = "True"
                    crvReportForm.DisplayGroupTree = False
                    crvReportForm.ReportSource = report
                    crvReportForm.SeparatePages = True
                ElseIf sType = "Print PDF" Then
                    Response.Buffer = False
                    Response.ClearContent()
                    Response.ClearHeaders()
                    Try
                        report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "DAILYCASHREPORT_" & Format(CDate(FilterPeriod1.Text), "ddMMMyy").ToUpper & "_" & Format(CDate(FilterPeriod2.Text), "ddMMMyy").ToUpper)
                    Catch ex As Exception
                        report.Close() : report.Dispose()
                    End Try
                Else
                    Response.Buffer = False
                    Response.ClearContent()
                    Response.ClearHeaders()
                    Try
                        report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "DAILYCASHREPORT_" & Format(CDate(FilterPeriod1.Text), "ddMMMyy").ToUpper & "_" & Format(CDate(FilterPeriod2.Text), "ddMMMyy").ToUpper)
                    Catch ex As Exception
                        report.Close() : report.Dispose()
                    End Try
                End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmDailyCash.aspx")
        End If
        If checkPagePermission("~\ReportForm\frmDailyCash.aspx", Session("Role")) = False Then
            ' Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Daily Cash Report"
        If Not Page.IsPostBack Then
            ' DDL Division
            sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE' "
            If Session("CompnyCode") <> CompnyCode Then
                sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
            End If
            FillDDL(FilterDDLDiv, sSql)
            FilterDDLDiv_SelectedIndexChanged(Nothing, Nothing)
            DDLType_SelectedIndexChanged(Nothing, Nothing)
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        Else
            If Session("ViewReport") = "True" Then
                ShowReport("View")
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub FilterDDLDiv_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLDiv.SelectedIndexChanged
        FillDDLAcctg(FilterDDLAccount, "VAR_CASH", FilterDDLDiv.SelectedValue)
        Session("ViewReport") = "False" : crvReportForm.ReportSource = Nothing : crvReportForm.DataBind()
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Session("ViewReport") = "False"
        Response.Redirect("~\ReportForm\frmDailyCash.aspx?awal=true")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Dispose() : report.Close()
    End Sub
#End Region

    Protected Sub DDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLType.SelectedIndexChanged
        If DDLType.SelectedValue = "Summary" Then
            lbCurr.Visible = True : lbSeptCurr.Visible = True : FilterCurrency.Visible = True
        Else
            lbCurr.Visible = False : lbSeptCurr.Visible = False : FilterCurrency.Visible = False
        End If
        lbCurr.Visible = False : lbSeptCurr.Visible = False : FilterCurrency.Visible = False
    End Sub
End Class
