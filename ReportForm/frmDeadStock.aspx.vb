Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_frmDeadStock
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindItemData()
        sSql = "SELECT 0 selected,itemoid,itemcode,itemlongdescription itemdesc FROM QL_mstitem WHERE itemrecordstatus='ACTIVE' AND (itemcode LIKE '%" & Tchar(FilterTextItem.Text) & "%' OR itemlongdescription LIKE '%" & Tchar(FilterTextItem.Text) & "%') ORDER BY itemlongdescription"
        Dim dtItem As DataTable = cKon.ambiltabel(sSql, "QL_mstitem")
        GVitem.DataSource = dtItem
        GVitem.DataBind()
        Session("GVitem") = dtItem
        GVitem.Visible = True
        GVitem.SelectedIndex = -1
    End Sub

    Private Sub UpdateCheckedGV()
        If Not Session("GVitem") Is Nothing Then
            Dim dtab As DataTable = Session("GVitem")

            If dtab.Rows.Count > 0 Then
                Dim cb As System.Web.UI.WebControls.CheckBox
                Dim dView As DataView = dtab.DefaultView
                Dim drView As DataRowView

                For i As Integer = 0 To GVitem.Rows.Count - 1
                    cb = GVitem.Rows(i).FindControl("chkSelect")
                    dView.RowFilter = "itemoid = " & cb.ToolTip
                    drView = dView.Item(0)
                    drView.BeginEdit()
                    If cb.Checked = True Then
                        drView("selected") = 1
                    Else
                        drView("selected") = 0
                    End If
                    drView.EndEdit()
                    dView.RowFilter = ""
                Next
                dtab.AcceptChanges()
            End If
            Session("GVitem") = dtab
        End If
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        Try
            Dim sitemoid As String = ""
            If rbItem.SelectedValue = "SELECT" Then
                Dim dtItem As DataTable
                dtItem = Session("GVitem")
                Dim dvItem As DataView = dtItem.DefaultView
                dvItem.RowFilter = "selected='1'"
                For R1 As Integer = 0 To dvItem.Count - 1
                    sitemoid &= dvItem(R1)("itemoid").ToString & ","
                Next
            End If
            Dim swhere As String = ""
            If sitemoid <> "" Then swhere = " and i.itemoid IN (" & Left(sitemoid, sitemoid.Length - 1) & ")"

            If FilterType.SelectedValue.ToUpper = "SUMMARY" Then
                sSql = "select itemcode,itemdesc,case when age<=1 then qty else 0 end age1,case when age>1 and age<=3 then qty else 0 end age3,case when age>3 and age<=6 then qty else 0 end age6,case when age>6 and age<=12 then qty else 0 end age12,case when age>12 then qty else 0 end age12up,unit from (select dt.itemcode,dt.itemdesc,dt.qty,dt.unit,case when dt.agesales<dt.agepurchase then dt.agesales when dt.agepurchase<dt.agesales then dt.agepurchase else 1 end age from (select i.itemcode,i.itemlongdescription itemdesc,c.saldoakhir qty,g.gendesc unit,isnull((select top 1 j.trnjualdate from QL_trnjualmst j inner join QL_trnjualdtl d on j.trnjualmstoid=d.trnjualmstoid and d.itemoid=i.itemoid order by j.trnjualdate desc),'1900-01-01') lastsales,isnull((select top 1 j.trnbelidate from QL_trnbelimst j inner join QL_trnbelidtl d on j.trnbelimstoid=d.trnbelimstoid and d.itemoid=i.itemoid order by j.trnbelidate desc),'1900-01-01') lastpurch,datediff(month,isnull((select top 1 j.trnjualdate from QL_trnjualmst j inner join QL_trnjualdtl d on j.trnjualmstoid=d.trnjualmstoid and d.itemoid=i.itemoid order by j.trnjualdate desc),getdate()),getdate()) agesales,datediff(month,isnull((select top 1 j.trnbelidate from QL_trnbelimst j inner join QL_trnbelidtl d on j.trnbelimstoid=d.trnbelimstoid and d.itemoid=i.itemoid order by j.trnbelidate desc),getdate()),getdate()) agepurchase from QL_crdstock c inner join QL_mstitem i on i.itemoid=c.refoid and c.saldoakhir>0 and closeuser='' inner join QL_mstgen g on g.genoid=i.itemunit1 INNER JOIN QL_mstgen gx ON gx.genoid=i.itemgroupoid where 1=1" & swhere & " AND gx.gendesc='FINISH GOOD') dt) tdt"
            Else
                sSql = "select itemcode,itemdesc,saldoakhir,unit,lastdatepenjualan,lastdatepembelian from (select i.itemcode,i.itemlongdescription itemdesc,c.saldoakhir saldoakhir,g.gendesc unit,isnull((select top 1 j.trnjualdate from QL_trnjualmst j inner join QL_trnjualdtl d on j.trnjualmstoid=d.trnjualmstoid and d.itemoid=i.itemoid order by j.trnjualdate desc),getdate()) lastdatepenjualan,isnull((select top 1 j.trnbelidate from QL_trnbelimst j inner join QL_trnbelidtl d on j.trnbelimstoid=d.trnbelimstoid and d.itemoid=i.itemoid order by j.trnbelidate desc),getdate()) lastdatepembelian from QL_crdstock c inner join QL_mstitem i on i.itemoid=c.refoid and c.saldoakhir>0 and closeuser='' inner join QL_mstgen g on g.genoid=i.itemunit1  INNER JOIN QL_mstgen gx ON gx.genoid=i.itemgroupoid where 1=1" & swhere & "  AND gx.gendesc='FINISH GOOD') dt where datediff(month,dt.lastdatepenjualan,current_timestamp) >= " & FilterDDLMonth.SelectedValue & " and datediff(month,dt.lastdatepembelian,current_timestamp) >= " & FilterDDLMonth.SelectedValue & ""
            End If
            sSql &= " order by itemdesc"

            Dim dt As DataTable
            Dim nFile As String = ""

            If FilterType.SelectedValue.ToUpper = "SUMMARY" Then
                If sType = "Print Excel" Then
                    nFile = Server.MapPath(folderReport & "rptDSXls.rpt")
                Else
                    nFile = Server.MapPath(folderReport & "rptDS.rpt")
                End If
                report.Load(nFile)
                dt = cKon.ambiltabel(sSql, "tbldata")
                report.SetDataSource(dt)
            Else
                If sType = "Print Excel" Then
                    nFile = Server.MapPath(folderReport & "rptDSDtlXls.rpt")
                Else
                    nFile = Server.MapPath(folderReport & "rptDSDtl.rpt")
                End If
                report.Load(nFile)
            End If

            Dim strLastModified As String = System.IO.File.GetLastWriteTime(nFile.ToString())
            cProc.SetDBLogonForReport(report)

            If FilterType.SelectedValue.ToUpper = "DETAIL" Then
                report.SetParameterValue("period", FilterDDLMonth.SelectedItem.ToString)
            End If

            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "DSReport")
                Catch ex As Exception
                    report.Close()
                    report.Dispose()
                End Try
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "DSReport")
                Catch ex As Exception
                    report.Close()
                    report.Dispose()
                End Try
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.ToString, 1)
        End Try
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("frmDeadStock.aspx")
        End If
        If checkPagePermission("frmDeadStock.aspx", Session("Role")) = False Then
            ' Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Dead Stock Report"
        If Not Page.IsPostBack Then
            ' DDL Division
            sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE' "
            If Session("CompnyCode") <> CompnyCode Then
                sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
            End If
            FillDDL(FilterDDLDiv, sSql)
            pnlItem.CssClass = "popupControl"
            FilterType_SelectedIndexChanged(Nothing, Nothing)
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub FilterDDLDiv_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLDiv.SelectedIndexChanged
        rbItem.SelectedIndex = 0
        rbItem_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub FilterType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterType.SelectedIndexChanged
        If FilterType.SelectedIndex = 0 Then
            trtype.visible = False
        Else
            trtype.visible = True
        End If
    End Sub

    Protected Sub rbItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbItem.SelectedIndexChanged
        GVitem.DataSource = Nothing
        GVitem.DataBind()
        Session("GVitem") = Nothing
        GVitem.Visible = False
        If rbItem.SelectedValue = "ALL" Then
            FilterTextItem.Visible = False
            btnSearchItem.Visible = False
            btnClearItem.Visible = False
        Else
            FilterTextItem.Visible = True
            btnSearchItem.Visible = True
            btnClearItem.Visible = True
        End If
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchItem.Click
        BindItemData()
    End Sub

    Protected Sub btnClearItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearItem.Click
        FilterTextItem.Text = ""
        itemoid.Text = ""
        GVitem.DataSource = Nothing
        GVitem.DataBind()
        GVitem.Visible = False
    End Sub

    Protected Sub GVitem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVitem.PageIndexChanging
        UpdateCheckedGV()
        GVitem.PageIndex = e.NewPageIndex
        Dim dtItem As DataTable = Session("GVitem")
        GVitem.DataSource = dtItem
        GVitem.DataBind()
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        UpdateCheckedGV()
        ShowReport("View")
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        UpdateCheckedGV()
        ShowReport("Print PDF")
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        UpdateCheckedGV()
        ShowReport("Print Excel")
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("frmDeadStock.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Dispose()
        report.Close()
    End Sub
#End Region

End Class
