Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_ProductionResult
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function 'OK

    Private Function IsValidPeriodTo() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period is invalid. " & sErr, 2)
            Return False
        End If
        Return True
    End Function 'OK
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub 'OK

    Private Sub InitAllDDL()
        ' Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If FillDDL(FilterDDLBusUnit, sSql) Then
            InitDept()
        End If
        ' Fill DDL Group Name
        sSql = "SELECT suppoid, suppname FROM QL_mstsupp WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND suppres1='Pemborong' ORDER BY suppname"
        FillDDLWithAdditionalText(FilterDDLGroupName, sSql, "None", "0")
    End Sub 'OK

    Private Sub InitDept()
        '' DDL Department
        'sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE activeflag='ACTIVE' AND cmpcode='" & FilterDDLBusUnit.SelectedValue & "' AND deptoid IN (SELECT deptoid FROM QL_trnprodresmst) ORDER BY deptname"
        'FillDDLWithAdditionalText(FilterDDLDept, sSql, "All", "All")
        ' Init DDL Department
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE cmpcode='" & FilterDDLBusUnit.SelectedValue & "' AND activeflag='ACTIVE'"
        FillDDL(FilterDDLDept, sSql)
        If DDLDept.SelectedValue.ToUpper = "TO" Then
            FilterDDLDept.Items.Add("END")
            FilterDDLDept.Items.Item(FilterDDLDept.Items.Count - 1).Value = "END"
        End If
    End Sub 'OK

    Private Sub BindResultData()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, resm.prodresmstoid, prodresno, CONVERT(VARCHAR(10), prodresdate, 101) AS prodresdate, de.deptname FROMDEPT, dx.deptname TODEPT, prodresmstnote FROM QL_trnprodresmst resm INNER JOIN QL_trnprodresdtl resd ON resd.prodresmstoid=resm.prodresmstoid INNER JOIN QL_mstdept dx ON dx.cmpcode=resm.cmpcode AND dx.deptoid=resd.todeptoid INNER JOIN QL_mstdept de ON de.cmpcode=resm.cmpcode AND de.deptoid=resm.deptoid WHERE resm.cmpcode='" & FilterDDLBusUnit.SelectedValue & "'"
        If FilterDDLStatus.SelectedValue <> "ALL" Then
            'Filter Status
            If FilterDDLStatus.SelectedValue.ToUpper = "IN PROCESS" Then
                sSql &= " AND resm.prodresmststatus='" & FilterDDLStatus.SelectedValue & "'"
            ElseIf FilterDDLStatus.SelectedValue.ToUpper = "POST" Then
                sSql &= " AND resm.prodresmststatus IN ('Post','Closed') "
            End If
        End If
        If cbDept.Checked = True Then
            If DDLDept.SelectedValue.ToUpper = "FROM" Then
                sSql &= " AND resm.deptoid=" & FilterDDLDept.SelectedValue & ""
            Else
                If FilterDDLDept.SelectedValue.ToUpper = "END" Then
                    sSql &= " AND resd.prodrestype='FG' "
                Else
                    sSql &= " AND resd.todeptoid=" & FilterDDLDept.SelectedValue & " "
                End If
            End If
        End If
        If FilterDDLType.SelectedValue <> "ALL" Then
            If FilterDDLDept.SelectedValue.ToUpper <> "END" Then
                sSql &= " AND resd.prodrestype='" & FilterDDLType.SelectedValue & "'"
            End If
        End If

        If cbPeriod.Checked Then
            If IsValidPeriod() Then
                sSql &= " AND " & DDLDate.SelectedValue & ">='" & FilterPeriod1.Text & " 00:00:00' AND " & DDLDate.SelectedValue & "<='" & FilterPeriod2.Text & " 23:59:59'"
                'sWhere &= " AND prodresdate>='" & FilterPeriod1.Text & " 00:00:00' AND prodresdate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If

        sSql &= " ORDER BY resm.prodresmstoid"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnprodresmst")
        If dt.Rows.Count > 0 Then
            Session("TblListResult") = dt
            Session("TblListResultView") = dt
            gvListResult.DataSource = dt
            gvListResult.DataBind()
            cProc.SetModalPopUpExtender(btnHideListResult, pnlListResult, mpeListResult, True)
        Else
            showMessage("No Production Result data available for this time!", 2)
        End If
    End Sub 'OK

    Private Sub UpdateCheckedResult()
        If Session("TblListResult") IsNot Nothing Then
            For C1 As Integer = 0 To gvListResult.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListResult.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            Session("TblListResult").DefaultView.RowFilter = "prodresmstoid=" & CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                            If Session("TblListResult").DefaultView.Count > 0 Then
                                If CType(myControl, System.Web.UI.WebControls.CheckBox).Checked Then
                                    Session("TblListResult").DefaultView(0)("checkvalue") = "True"
                                Else
                                    Session("TblListResult").DefaultView(0)("checkvalue") = "False"
                                End If
                            End If
                            Session("TblListResult").DefaultView.RowFilter = ""
                        End If
                    Next
                End If
            Next
        End If
        If Session("TblListResultView") IsNot Nothing Then
            For C1 As Integer = 0 To gvListResult.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListResult.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            Session("TblListResultView").DefaultView.RowFilter = "prodresmstoid=" & CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                            If Session("TblListResultView").DefaultView.Count > 0 Then
                                If CType(myControl, System.Web.UI.WebControls.CheckBox).Checked Then
                                    Session("TblListResultView").DefaultView(0)("checkvalue") = "True"
                                Else
                                    Session("TblListResultView").DefaultView(0)("checkvalue") = "False"
                                End If
                            End If
                            Session("TblListResultView").DefaultView.RowFilter = ""
                        End If
                    Next
                End If
            Next
        End If
    End Sub 'OK

    Private Sub ShowReport(ByVal sType As String)
        If FilterDDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        Try
            If FilterDDLTypeS.SelectedValue = "Summary" Then
                If sType = "Print Excel" Then
                    report.Load(Server.MapPath(folderReport & "rptProductionResultSumXls.rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptProductionResultSum.rpt"))
                End If
            Else
                If sType = "Print Excel" Then
                    report.Load(Server.MapPath(folderReport & "rptProductionResultXls.rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptProductionResult.rpt"))
                End If
            End If
            Dim sPeriodStart As String = "", sPeriodEnd As String = ""
            Dim sWhere As String = " WHERE resm.cmpcode='" & FilterDDLBusUnit.SelectedValue & "'"
            Dim sWhere2 As String = " WHERE 1=1"
            If DDLTextStatus.SelectedIndex = 0 Then
                If FilterDDLStatus.SelectedValue <> "ALL" Then
                    'Filter Status
                    If FilterDDLStatus.SelectedValue.ToUpper = "IN PROCESS" Then
                        sWhere &= " AND resm.prodresmststatus='" & FilterDDLStatus.SelectedValue & "'"
                    ElseIf FilterDDLStatus.SelectedValue.ToUpper = "POST" Then
                        sWhere &= " AND resm.prodresmststatus IN ('Post','Closed') "
                    End If
                End If
                If cbPeriod.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND " & DDLDate.SelectedValue & ">='" & FilterPeriod1.Text & " 00:00:00' AND " & DDLDate.SelectedValue & "<='" & FilterPeriod2.Text & " 23:59:59'"
                        'sWhere &= " AND prodresdate>='" & FilterPeriod1.Text & " 00:00:00' AND prodresdate<='" & FilterPeriod2.Text & " 23:59:59'"
                        sPeriodStart = Format(CDate(FilterPeriod1.Text), "dd MMM yyyy")
                        sPeriodEnd = Format(CDate(FilterPeriod2.Text), "dd MMM yyyy")
                    Else
                        Exit Sub
                    End If
                End If
            Else
                If FilterDDLStatus2.SelectedValue <> "ALL" Then
                    sWhere2 &= " AND [DLA Status]='" & FilterDDLStatus2.SelectedValue & "'"
                Else
                    sWhere2 &= " AND [DLA Status] IN ('POST', 'CLOSED')"
                End If
                If cbPeriod.Checked Then
                    If IsValidPeriodTo() Then
                        sWhere2 &= " AND [Post Date]<='" & FilterPeriod2.Text & " 23:59:59'"
                        sPeriodStart = ""
                        sPeriodEnd = Format(CDate(FilterPeriod2.Text), "dd MMM yyyy")
                    Else
                        Exit Sub
                    End If
                End If
            End If
            'If FilterDDLDept.SelectedValue <> "All" Then
            '    sWhere &= " AND resm.deptoid=" & FilterDDLDept.SelectedValue
            'End If
            'Filter From To Department
            If cbDept.Checked = True Then
                If DDLDept.SelectedValue.ToUpper = "FROM" Then
                    sWhere &= " AND resm.deptoid=" & FilterDDLDept.SelectedValue & ""
                Else
                    If FilterDDLDept.SelectedValue.ToUpper = "END" Then
                        sWhere &= " AND prodrestype='FG' "
                    Else
                        sWhere &= " AND resd.todeptoid=" & FilterDDLDept.SelectedValue & " "
                    End If
                End If
            End If
            If cbGroupName.Checked Then
                sWhere &= " AND resm.suppoid=" & FilterDDLGroupName.SelectedValue & " "
            End If
            If FilterDDLType.SelectedValue <> "ALL" Then
                If FilterDDLDept.SelectedValue.ToUpper <> "END" Then
                    sWhere &= " AND prodrestype='" & FilterDDLType.SelectedValue & "'"
                End If
            End If

            If prodresno.Text <> "" Then
                Dim sSplit() As String = prodresno.Text.Split(";")
                If sSplit.Length > 0 Then
                    sWhere &= " AND resm." & DDLNo.SelectedValue & " IN ("
                    For C1 As Integer = 0 To sSplit.Length - 1
                        sWhere &= "'" & sSplit(C1) & "'"
                        If C1 < sSplit.Length - 1 Then
                            sWhere &= ","
                        End If
                    Next
                    sWhere &= ")"
                End If
            End If
            Dim sOrderBy As String
            If DDLSorting.SelectedValue = "resm.prodresno" Then
                sOrderBy = " ORDER BY [From Dept. Code] ASC, [Result No.] ASC, [Oid] ASC,  [Result Date] ASC, [KIK No.] ASC "
            ElseIf DDLSorting.SelectedValue = "resm.prodresdate" Then
                sOrderBy = " ORDER BY [From Dept. Code] ASC,  [Result Date] ASC, [Oid] ASC, [Result No.] ASC, [KIK No.] ASC "
            Else
                'Sorting by KIK
                sOrderBy = " ORDER BY [From Dept. Code] ASC, [KIK No.] ASC,  [Result Date] ASC, [Oid] ASC, [Result No.] ASC "
            End If
            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("sWhere2", sWhere2)
            report.SetParameterValue("sDateTo", FilterPeriod2.Text & " 23:59:59")
            report.SetParameterValue("sOrderBy", sOrderBy)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            report.SetParameterValue("sDate", DDLDate.SelectedItem.Text)
            report.SetParameterValue("sPeriodStart", sPeriodStart)
            report.SetParameterValue("sPeriodEnd", sPeriodEnd)
            'report.PrintOptions.PaperSize = PaperSize.PaperFolio
            cProc.SetDBLogonForReport(report)
            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "ProductionResultReport")
                report.Close()
                report.Dispose()
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "ProductionResultReport")
                report.Close()
                report.Dispose()
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub 'OK
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmProdRes.aspx")
        End If
        If checkPagePermission("~\ReportForm\frmProdRes.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Production Result Report"
        If Not Page.IsPostBack Then
            InitAllDDL()
            FilterDDLStatus_SelectedIndexChanged(Nothing, Nothing)
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        End If
    End Sub 'OK

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListResult") Is Nothing And Session("WarningListResult") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListResult") Then
                Session("WarningListResult") = Nothing
                cProc.SetModalPopUpExtender(btnHideListResult, pnlListResult, mpeListResult, True)
            End If
        End If
    End Sub 'OK

    Protected Sub FilterDDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLBusUnit.SelectedIndexChanged
        InitDept()
    End Sub 'OK

    Protected Sub btnSearchResult_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchResult.Click
        If FilterDDLBusUnit.Text = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        FilterDDLListResult.SelectedIndex = -1 : FilterTextListResult.Text = "" : Session("TblListResult") = Nothing : Session("TblListResultView") = Nothing : gvListResult.DataSource = Nothing : gvListResult.DataBind()
        BindResultData()
    End Sub 'OK

    Protected Sub btnClearResult_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearResult.Click
        prodresmstoid.Text = "" : prodresno.Text = ""
    End Sub 'OK

    Protected Sub btnFindListResult_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListResult.Click
        UpdateCheckedResult()
        If Session("TblListResult") IsNot Nothing Then
            Session("TblListResult").DefaultView.RowFilter = FilterDDLListResult.SelectedValue & " LIKE '%" & Tchar(FilterTextListResult.Text) & "%'"
            If Session("TblListResult").DefaultView.Count > 0 Then
                Session("TblListResultView") = Session("TblListResult").DefaultView.ToTable
                gvListResult.DataSource = Session("TblListResultView")
                gvListResult.DataBind()
                Session("TblListResult").DefaultView.RowFilter = ""
                mpeListResult.Show()
            Else
                Session("WarningListResult") = "Production Result data can't be found!"
                showMessage(Session("WarningListResult"), 2)
                Session("TblListResult").DefaultView.RowFilter = ""
            End If
        Else
            Session("WarningListResult") = "No Production Result data available for this time!"
            showMessage(Session("WarningListResult"), 2)
        End If
    End Sub 'OK

    Protected Sub btnAllListResult_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListResult.Click
        UpdateCheckedResult()
        If Session("TblListResult") IsNot Nothing Then
            FilterDDLListResult.SelectedIndex = -1 : FilterTextListResult.Text = "" : gvListResult.SelectedIndex = -1
            Session("TblListResultView") = Session("TblListResult")
            gvListResult.DataSource = Session("TblListResultView")
            gvListResult.DataBind()
            mpeListResult.Show()
        Else
            Session("WarningListResult") = "No Production Result data available for this time!"
            showMessage(Session("WarningListResult"), 2)
        End If
    End Sub 'OK

    Protected Sub gvListResult_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListResult.PageIndexChanging
        UpdateCheckedResult()
        gvListResult.PageIndex = e.NewPageIndex
        gvListResult.DataSource = Session("TblListResultView")
        gvListResult.DataBind()
        mpeListResult.Show()
    End Sub 'OK

    Protected Sub cbListResultHdr_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim bVal As Boolean = sender.Checked
        For C1 As Integer = 0 To gvListResult.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListResult.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = bVal
                    End If
                Next
            End If
        Next
        mpeListResult.Show()
    End Sub 'OK

    Protected Sub lkbAddToListResult_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListResult.Click
        UpdateCheckedResult()
        If Session("TblListResult") IsNot Nothing Then
            Session("TblListResult").DefaultView.RowFilter = "checkvalue='True'"
            If Session("TblListResult").DefaultView.Count > 0 Then
                Dim sOid As String = "", sNo As String = ""
                For C1 As Integer = 0 To Session("TblListResult").DefaultView.Count - 1
                    sOid &= Session("TblListResult").DefaultView(C1)("prodresmstoid").ToString & ","
                    If DDLNo.SelectedValue = "prodresmstoid" Then
                        sNo &= Session("TblListResult").DefaultView(C1)("prodresmstoid").ToString & ";"
                    Else
                        sNo &= Session("TblListResult").DefaultView(C1)("prodresno").ToString & ";"
                    End If
                Next
                prodresmstoid.Text = Left(sOid, sOid.Length - 1)
                prodresno.Text = Left(sNo, sNo.Length - 1)
                cProc.SetModalPopUpExtender(btnHideListResult, pnlListResult, mpeListResult, False)
            Else
                Session("WarningListResult") = "Please select some Production Result data!"
                showMessage(Session("WarningListResult"), 2)
                Session("TblListResult").DefaultView.RowFilter = ""
            End If
        Else
            Session("WarningListResult") = "No Production Result data available for this time!"
            showMessage(Session("WarningListResult"), 2)
        End If
    End Sub 'OK

    Protected Sub lkbCloseListResult_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListResult.Click
        cProc.SetModalPopUpExtender(btnHideListResult, pnlListResult, mpeListResult, False)
    End Sub 'OK

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub 'OK

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub 'OK

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub 'OK

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmProdRes.aspx?awal=true")
    End Sub 'OK

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub 'OK

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub 'OK

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub 'OK

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not report Is Nothing Then
                If report.IsLoaded Then
                    report.Dispose()
                    report.Close()
                End If
            End If
        Catch ex As Exception
            report.Dispose()
            report.Close()
        End Try
    End Sub 'OK
#End Region

    Protected Sub DDLDept_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitDept()
    End Sub

    Protected Sub FilterDDLStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLStatus.SelectedIndexChanged
        If FilterDDLStatus.SelectedValue = "POST" Then
            'Fill DDL Date
            DDLDate.Items.Clear()
            DDLDate.Items.Add("Result Date")
            DDLDate.Items.Item(DDLDate.Items.Count - 1).Value = "resm.prodresdate"
            DDLDate.Items.Add("Post Date")
            DDLDate.Items.Item(DDLDate.Items.Count - 1).Value = "resm.updtime"
        Else
            'Fill DDL Date
            DDLDate.Items.Clear()
            DDLDate.Items.Add("Result Date")
            DDLDate.Items.Item(DDLDate.Items.Count - 1).Value = "resm.prodresdate"
        End If
    End Sub

    Protected Sub DDLTextStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLTextStatus.SelectedIndexChanged
        Dim bVal As Boolean = False
        If DDLTextStatus.SelectedIndex = 0 Then
            bVal = True
        End If
        FilterPeriod1.Visible = bVal : CalPeriod1.Visible = bVal : FilterDDLStatus.Visible = bVal : FilterDDLStatus2.Visible = Not bVal : DDLDate.Visible = bVal : DDLDate2.Visible = Not bVal
    End Sub

End Class
