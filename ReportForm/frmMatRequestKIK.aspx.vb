Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_MaterialRequestKIK
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If FilterPeriod1.Text = "" Or FilterPeriod2.Text = "" Then
            showMessage("Please fill both of Period 1 and Period 2 first!", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitAllDDL()
        ' DDL Division
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If FillDDL(DDLBusUnit, sSql) Then
            InitDDLDept()
        End If
        ' DDL Warehouse
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='WAREHOUSE' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDL(DDLWarehouse, sSql)

        sSql = "SELECT gencode, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='GROUPITEM' AND activeflag='ACTIVE'"

        FillDDL(DDLMatType, sSql)
    End Sub

    Private Sub InitDDLDept()
        ' DDL Department
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE activeflag='ACTIVE' AND cmpcode='" & DDLBusUnit.SelectedValue & "' ORDER BY deptname"
        FillDDL(DDLDept, sSql)
    End Sub

    Private Sub BindListRequest()
        'sSql = "SELECT * FROM ("
        'sSql &= "SELECT '(RM)' + CONVERT(VARCHAR(20), reqrawmstoid) AS matreqmstoid, CONVERT(VARCHAR(20), reqrawmstoid) AS draftno, reqrawno AS matreqno, CONVERT(VARCHAR(10), reqrawdate, 101) AS matreqdate, reqrawmstnote AS matreqmstnote, 'False' AS checkvalue FROM QL_trnreqrawmst reqm WHERE reqm.cmpcode='" & DDLBusUnit.SelectedValue & "' UNION ALL "
        'sSql &= "SELECT '(GM)' + CONVERT(VARCHAR(20), reqgenmstoid) AS matreqmstoid, CONVERT(VARCHAR(20), reqgenmstoid) AS draftno, reqgenno AS matreqno, CONVERT(VARCHAR(10), reqgendate, 101) AS matreqdate, reqgenmstnote AS matreqmstnote, 'False' AS checkvalue FROM QL_trnreqgenmst reqm WHERE reqm.cmpcode='" & DDLBusUnit.SelectedValue & "' UNION ALL "
        'sSql &= "SELECT '(SP)' + CONVERT(VARCHAR(20), reqspmstoid) AS matreqmstoid, CONVERT(VARCHAR(20), reqspmstoid) AS draftno, reqspno AS matreqno, CONVERT(VARCHAR(10), reqspdate, 101) AS matreqdate, reqspmstnote AS matreqmstnote, 'False' AS checkvalue FROM QL_trnreqspmst reqm WHERE reqm.cmpcode='" & DDLBusUnit.SelectedValue & "'"
        sSql = "SELECT reqmstoid AS matreqmstoid, CONVERT(VARCHAR(20), reqmstoid) AS draftno, reqno AS matreqno, CONVERT(VARCHAR(10), reqdate, 101) AS matreqdate, reqmstnote AS matreqmstnote, 'False' AS checkvalue FROM QL_trnreqmst reqm WHERE reqm.cmpcode='" & DDLBusUnit.SelectedValue & "'"
        'sSql &= ") AS QL_trnmatreqmst ORDER BY matreqno"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnmatreqmst")
        If dt.Rows.Count > 0 Then
            Session("TblListRequest") = dt
            Session("TblListRequestView") = Session("TblListRequest")
            gvListRequest.DataSource = Session("TblListRequestView")
            gvListRequest.DataBind()
            cProc.SetModalPopUpExtender(btnHideListRequest, pnlListRequest, mpeListRequest, True)
        Else
            showMessage("No Material Request KIK data available for this time.", 2)
        End If
    End Sub

    Private Sub UpdateCheckedListRequest()
        If Session("TblListRequest") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListRequest")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListRequest.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListRequest.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matreqmstoid='" & sOid & "'"
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListRequest") = dt
        End If
        If Session("TblListRequestView") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListRequestView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListRequest.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListRequest.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matreqmstoid='" & sOid & "'"
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListRequestView") = dt
        End If
    End Sub

    Private Sub BindListMat()
        'Dim sTypeMat As String = "", sTypeMat2 As String = "", sType As String = ""
        'If DDLMatType.SelectedValue = "RM" Then
        '    sTypeMat = "matraw" : sType = "raw"
        'ElseIf DDLMatType.SelectedValue = "GM" Then
        '    sTypeMat = "matgen" : sType = "gen"
        'ElseIf DDLMatType.SelectedValue = "SP" Then
        '    sTypeMat = "sparepart" : sType = "sp"
        'ElseIf DDLMatType.SelectedValue = "LOG" Then
        '    sTypeMat2 = "log" : sType = "log"
        'Else
        '    sTypeMat2 = "pallet" : sType = "sawn"
        'End If
        'If sTypeMat = "" Then
        '    sSql = "SELECT DISTINCT reqd." & sTypeMat2 & "oid AS matoid, matrawcode + ' (' + " & sTypeMat2 & "no + ')' AS matcode, matrawlongdesc AS matlongdesc, gendesc AS matunit, 'False' AS checkvalue FROM QL_trnreq" & sType & "dtl reqd INNER JOIN QL_mst" & sTypeMat2 & " p ON p.cmpcode=reqd.cmpcode AND p." & sTypeMat2 & "oid=reqd." & sTypeMat2 & "oid INNER JOIN QL_mstmatraw m ON m.matrawoid=p.refoid INNER JOIN QL_mstgen g ON genoid=req" & sType & "unitoid WHERE reqd.cmpcode='" & DDLBusUnit.SelectedValue & "' ORDER BY matcode"
        'Else
        '    sSql = "SELECT DISTINCT reqd." & sTypeMat & "oid AS matoid, " & sTypeMat & "code AS matcode, " & sTypeMat & "longdesc AS matlongdesc, gendesc AS matunit, 'False' AS checkvalue FROM QL_trnreq" & sType & "dtl reqd INNER JOIN QL_mst" & sTypeMat & " m ON m." & sTypeMat & "oid=reqd." & sTypeMat & "oid INNER JOIN QL_mstgen g ON genoid=req" & sType & "unitoid WHERE reqd.cmpcode='" & DDLBusUnit.SelectedValue & "' ORDER BY matcode"
        'End If]
        sSql = "SELECT DISTINCT reqd.matoid AS matoid, itemCode AS matcode, itemLongDescription AS matlongdesc, gendesc AS matunit, 'False' AS checkvalue FROM QL_trnreqdtl reqd INNER JOIN QL_mstitem m ON m.itemoid=reqd.matoid INNER JOIN QL_mstgen g ON genoid=requnitoid WHERE reqd.cmpcode='" & DDLBusUnit.SelectedValue & "' ORDER BY itemcode"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnmatreqdtl")
        If dt.Rows.Count > 0 Then
            Session("TblListMat") = dt
            Session("TblListMatView") = Session("TblListMat")
            gvListMat.DataSource = Session("TblListMatView")
            gvListMat.DataBind()
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            showMessage("No Material data available for this time.", 2)
        End If
    End Sub

    Private Sub UpdateCheckedListMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMat") = dt
        End If
        If Session("TblListMatView") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMatView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMatView") = dt
        End If
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        If Not IsValidPeriod() Then
            Exit Sub
        End If
        Try
            Dim sWhere As String = " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND " & DDLDate.SelectedValue & ">=CONVERT(DATETIME, '" & FilterPeriod1.Text & " 00:00:00') AND " & DDLDate.SelectedValue & "<=CONVERT(DATETIME, '" & FilterPeriod2.Text & " 23:59:59')"
            If FilterRequest.Text <> "" Then
                Dim sWhereCode As String = ""
                Dim sNo() As String = Split(FilterRequest.Text, ";")
                If sNo.Length > 0 Then
                    For C1 As Integer = 0 To sNo.Length - 1
                        If sNo(C1) <> "" Then
                            sWhereCode &= DDLRequest.SelectedValue & " LIKE '" & Tchar(sNo(C1)) & "' OR "
                        End If
                    Next
                    If sWhereCode <> "" Then
                        sWhereCode = Left(sWhereCode, sWhereCode.Length - 4)
                        sWhere &= " AND (" & sWhereCode & ")"
                    End If
                End If
            End If
            If DDLType.SelectedValue = "DETAIL" Then
                If FilterMaterial.Text <> "" Then
                    Dim sWhereCode As String = ""
                    Dim sNo() As String = Split(FilterMaterial.Text, ";")
                    If sNo.Length > 0 Then
                        For C1 As Integer = 0 To sNo.Length - 1
                            If sNo(C1) <> "" Then
                                sWhereCode &= "[Code] LIKE '" & Tchar(sNo(C1)) & "' OR "
                            End If
                        Next
                        If sWhereCode <> "" Then
                            sWhereCode = Left(sWhereCode, sWhereCode.Length - 4)
                            sWhere &= " AND (" & sWhereCode & ")"
                        End If
                    End If
                End If
            End If
            If lbDept.Items.Count > 0 Then
                Dim sOid As String = ""
                For C1 As Integer = 0 To lbDept.Items.Count - 1
                    sOid &= lbDept.Items(C1).Value & ","
                Next
                If sOid <> "" Then
                    sOid = Left(sOid, sOid.Length - 1)
                    sWhere &= " AND [Dept ID] IN (" & sOid & ")"
                End If
            End If
            If lbWarehouse.Items.Count > 0 Then
                Dim sOid As String = ""
                For C1 As Integer = 0 To lbWarehouse.Items.Count - 1
                    sOid &= lbWarehouse.Items(C1).Value & ","
                Next
                If sOid <> "" Then
                    sOid = Left(sOid, sOid.Length - 1)
                    sWhere &= " AND [WH ID] IN (" & sOid & ")"
                End If
            End If
            Dim sStatus As String = ""
            For C1 As Integer = 0 To cblStatus.Items.Count - 1
                If cblStatus.Items(C1).Selected Then
                    sStatus &= "'" & cblStatus.Items(C1).Value & "',"
                End If
            Next
            If sStatus <> "" Then
                sStatus = Left(sStatus, sStatus.Length - 1)
                sWhere &= " AND [Status] IN (" & sStatus & ")"
            End If
            Dim sSortBy As String = ""
            Dim sSplitSort() As String = DDLSortBy.SelectedValue.Split(",")
            If sSplitSort.Length > 0 Then
                For C1 As Integer = 0 To sSplitSort.Length - 1
                    If sSplitSort(C1) <> "" Then
                        sSortBy &= sSplitSort(C1) & " " & DDLOrder.SelectedValue & ", "
                    End If
                Next
            End If
            Dim sRptName As String = ""
            Dim dtReport As DataTable = Nothing
            If DDLType.SelectedValue = "SUMMARY" Then
                sRptName = "MatRequestKIKSummaryReport"
                If sType = "Print Excel" Then
                    report.Load(Server.MapPath(folderReport & "rptMatReqKIKSum.rpt")) '_Excel.rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptMatReqKIKSum.rpt"))
                End If
                sSql = "SELECT * FROM (SELECT reqm.cmpcode, (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=reqm.cmpcode) [Business Unit], CONVERT(VARCHAR(20), reqm.reqmstoid) [Draft No.], reqno [Request No.], reqdate [Request Date], (SELECT wono FROM QL_trnwomst wom WHERE wom.cmpcode=reqm.cmpcode AND wom.womstoid=reqm.womstoid) [KIK No.], reqm.deptoid [Dept ID], (SELECT deptname FROM QL_mstdept de WHERE de.deptoid=reqm.deptoid) [Department], reqwhoid [WH ID], g1.gendesc [Warehouse], reqmststatus [Status], reqmstnote [Header Note], UPPER(reqm.createuser) [Create User], (CASE reqmststatus WHEN 'In Process' THEN '' ELSE reqm.upduser END) [Posting User], (CASE reqmststatus WHEN 'In Process' THEN NULL ELSE reqm.updtime END) [Posting Date] FROM QL_trnreqmst reqm INNER JOIN QL_mstgen g1 ON g1.genoid=reqwhoid "
                'sSql &= "SELECT reqm.cmpcode, (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=reqm.cmpcode) [Business Unit], CONVERT(VARCHAR(20), reqm.reqrawmstoid) [Draft No.], reqrawno [Request No.], reqrawdate [Request Date], (SELECT wono FROM QL_trnwomst wom WHERE wom.cmpcode=reqm.cmpcode AND wom.womstoid=reqm.womstoid) [KIK No.], reqm.deptoid [Dept ID], (SELECT deptname FROM QL_mstdept de WHERE de.deptoid=reqm.deptoid) [Department], reqrawwhoid [WH ID], g1.gendesc [Warehouse], reqrawmststatus [Status], reqrawmstnote [Header Note], UPPER(reqm.createuser) [Create User], (CASE reqrawmststatus WHEN 'In Process' THEN '' ELSE reqm.upduser END) [Posting User], (CASE reqrawmststatus WHEN 'In Process' THEN NULL ELSE reqm.updtime END) [Posting Date] FROM QL_trnreqrawmst reqm INNER JOIN QL_mstgen g1 ON g1.genoid=reqrawwhoid UNION ALL " & _
                '        "SELECT reqm.cmpcode, (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=reqm.cmpcode) [Business Unit], CONVERT(VARCHAR(20), reqm.reqgenmstoid) [Draft No.], reqgenno [Request No.], reqgendate [Request Date], (SELECT wono FROM QL_trnwomst wom WHERE wom.cmpcode=reqm.cmpcode AND wom.womstoid=reqm.womstoid) [KIK No.], reqm.deptoid [Dept ID], (SELECT deptname FROM QL_mstdept de WHERE de.deptoid=reqm.deptoid) [Department], reqgenwhoid [WH ID], g1.gendesc [Warehouse], reqgenmststatus [Status], reqgenmstnote [Header Note], UPPER(reqm.createuser) [Create User], (CASE reqgenmststatus WHEN 'In Process' THEN '' ELSE reqm.upduser END) [Posting User], (CASE reqgenmststatus WHEN 'In Process' THEN NULL ELSE reqm.updtime END) [Posting Date] FROM QL_trnreqgenmst reqm INNER JOIN QL_mstgen g1 ON g1.genoid=reqgenwhoid UNION ALL " & _
                '        "SELECT reqm.cmpcode, (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=reqm.cmpcode) [Business Unit], CONVERT(VARCHAR(20), reqm.reqspmstoid) [Draft No.], reqspno [Request No.], reqspdate [Request Date], (SELECT wono FROM QL_trnwomst wom WHERE wom.cmpcode=reqm.cmpcode AND wom.womstoid=reqm.womstoid) [KIK No.], reqm.deptoid [Dept ID], (SELECT deptname FROM QL_mstdept de WHERE de.deptoid=reqm.deptoid) [Department], reqspwhoid [WH ID], g1.gendesc [Warehouse], reqspmststatus [Status], reqspmstnote [Header Note], UPPER(reqm.createuser) [Create User], (CASE reqspmststatus WHEN 'In Process' THEN '' ELSE reqm.upduser END) [Posting User], (CASE reqspmststatus WHEN 'In Process' THEN NULL ELSE reqm.updtime END) [Posting Date] FROM QL_trnreqspmst reqm INNER JOIN QL_mstgen g1 ON g1.genoid=reqspwhoid"
                sSql &= ") AS QL_tblsummary " & sWhere & " ORDER BY [Request No.], [Draft No.]"
                dtReport = cKon.ambiltabel(sSql, "QL_tblsummary")
            Else
                sRptName = "MatRequestKIKDetailReport"
                Dim sExt As String = "", sGroup As String = ""
                If sType = "Print Excel" Then
                    sExt = "" '"_Excel"
                End If
                If DDLGroupBy.SelectedIndex = 0 Then
                    sGroup = "PerNo"
                Else
                    sGroup = "PerCode"
                End If
                report.Load(Server.MapPath(folderReport & "rptMatReqKIKDtl" & sGroup & ".rpt"))
                sSql = "SELECT * FROM ( SELECT reqm.cmpcode, (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=reqm.cmpcode) [Business Unit], CONVERT(VARCHAR(20), reqm.reqmstoid) [Draft No.], reqno [Request No.], reqdate [Request Date], (SELECT wono FROM QL_trnwomst wom WHERE wom.cmpcode=reqm.cmpcode AND wom.womstoid=reqm.womstoid) [KIK No.], reqm.deptoid [Dept ID], (SELECT deptname FROM QL_mstdept de WHERE de.deptoid=reqm.deptoid) [Department], reqwhoid [WH ID], g1.gendesc [Warehouse], reqmststatus [Status], reqmstnote [Header Note], UPPER(reqm.createuser) [Create User], (CASE reqmststatus WHEN 'In Process' THEN '' ELSE reqm.upduser END) [Posting User], (CASE reqmststatus WHEN 'In Process' THEN NULL ELSE reqm.updtime END) [Posting Date], itemGroup [Type],M.itemCode [Code], itemLongDescription [Material], reqqty [Qty], g2.gendesc [Unit], reqdtlnote [Note] FROM QL_trnreqmst reqm INNER JOIN QL_mstgen g1 ON g1.genoid=reqwhoid INNER JOIN QL_trnreqdtl reqd ON reqd.cmpcode=reqm.cmpcode AND reqd.reqmstoid=reqm.reqmstoid INNER JOIN QL_mstgen g2 ON g2.genoid=requnitoid INNER JOIN QL_mstitem m ON m.itemoid=reqd.matoid "
                'sSql &= "SELECT reqm.cmpcode, (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=reqm.cmpcode) [Business Unit], CONVERT(VARCHAR(20), reqm.reqrawmstoid) [Draft No.], reqrawno [Request No.], reqrawdate [Request Date], (SELECT wono FROM QL_trnwomst wom WHERE wom.cmpcode=reqm.cmpcode AND wom.womstoid=reqm.womstoid) [KIK No.], reqm.deptoid [Dept ID], (SELECT deptname FROM QL_mstdept de WHERE de.deptoid=reqm.deptoid) [Department], reqrawwhoid [WH ID], g1.gendesc [Warehouse], reqrawmststatus [Status], reqrawmstnote [Header Note], UPPER(reqm.createuser) [Create User], (CASE reqrawmststatus WHEN 'In Process' THEN '' ELSE reqm.upduser END) [Posting User], (CASE reqrawmststatus WHEN 'In Process' THEN NULL ELSE reqm.updtime END) [Posting Date], 'Raw' [Type], 'RM-' + matrawcode [Code], matrawlongdesc [Material], reqrawqty [Qty], g2.gendesc [Unit], reqrawdtlnote [Note] FROM QL_trnreqrawmst reqm INNER JOIN QL_mstgen g1 ON g1.genoid=reqrawwhoid INNER JOIN QL_trnreqrawdtl reqd ON reqd.cmpcode=reqm.cmpcode AND reqd.reqrawmstoid=reqm.reqrawmstoid INNER JOIN QL_mstgen g2 ON g2.genoid=reqrawunitoid INNER JOIN QL_mstmatraw m ON m.matrawoid=reqd.matrawoid UNION ALL " & _
                '        "SELECT reqm.cmpcode, (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=reqm.cmpcode) [Business Unit], CONVERT(VARCHAR(20), reqm.reqgenmstoid) [Draft No.], reqgenno [Request No.], reqgendate [Request Date], (SELECT wono FROM QL_trnwomst wom WHERE wom.cmpcode=reqm.cmpcode AND wom.womstoid=reqm.womstoid) [KIK No.], reqm.deptoid [Dept ID], (SELECT deptname FROM QL_mstdept de WHERE de.deptoid=reqm.deptoid) [Department], reqgenwhoid [WH ID], g1.gendesc [Warehouse], reqgenmststatus [Status], reqgenmstnote [Header Note], UPPER(reqm.createuser) [Create User], (CASE reqgenmststatus WHEN 'In Process' THEN '' ELSE reqm.upduser END) [Posting User], (CASE reqgenmststatus WHEN 'In Process' THEN NULL ELSE reqm.updtime END) [Posting Date], 'General' [Type], 'GM-' + matgencode [Code], matgenlongdesc [Material], reqgenqty [Qty], g2.gendesc [Unit], reqgendtlnote [Note] FROM QL_trnreqgenmst reqm INNER JOIN QL_mstgen g1 ON g1.genoid=reqgenwhoid INNER JOIN QL_trnreqgendtl reqd ON reqd.cmpcode=reqm.cmpcode AND reqd.reqgenmstoid=reqm.reqgenmstoid INNER JOIN QL_mstgen g2 ON g2.genoid=reqgenunitoid INNER JOIN QL_mstmatgen m ON m.matgenoid=reqd.matgenoid UNION ALL " & _
                '        "SELECT reqm.cmpcode, (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=reqm.cmpcode) [Business Unit], CONVERT(VARCHAR(20), reqm.reqspmstoid) [Draft No.], reqspno [Request No.], reqspdate [Request Date], (SELECT wono FROM QL_trnwomst wom WHERE wom.cmpcode=reqm.cmpcode AND wom.womstoid=reqm.womstoid) [KIK No.], reqm.deptoid [Dept ID], (SELECT deptname FROM QL_mstdept de WHERE de.deptoid=reqm.deptoid) [Department], reqspwhoid [WH ID], g1.gendesc [Warehouse], reqspmststatus [Status], reqspmstnote [Header Note], UPPER(reqm.createuser) [Create User], (CASE reqspmststatus WHEN 'In Process' THEN '' ELSE reqm.upduser END) [Posting User], (CASE reqspmststatus WHEN 'In Process' THEN NULL ELSE reqm.updtime END) [Posting Date], 'Spare Part' [Type], 'SP-' + sparepartcode [Code], sparepartlongdesc [Material], reqspqty [Qty], g2.gendesc [Unit], reqspdtlnote [Note] FROM QL_trnreqspmst reqm INNER JOIN QL_mstgen g1 ON g1.genoid=reqspwhoid INNER JOIN QL_trnreqspdtl reqd ON reqd.cmpcode=reqm.cmpcode AND reqd.reqspmstoid=reqm.reqspmstoid INNER JOIN QL_mstgen g2 ON g2.genoid=reqspunitoid INNER JOIN QL_mstsparepart m ON m.sparepartoid=reqd.sparepartoid"
                sSql &= ") AS QL_tbldetail " & sWhere
                sSql &= " ORDER BY cmpcode, " & DDLGroupBy.SelectedValue
                If sSortBy <> "" Then
                    sSql &= ", " & Left(sSortBy, sSortBy.Length - 2)
                End If
                dtReport = cKon.ambiltabel(sSql, "QL_tbldetail")
            End If
            report.SetDataSource(dtReport)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            cProc.SetDBLogonForReport(report)
            report.PrintOptions.PaperSize = PaperSize.PaperA4
            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, sRptName)
                report.Close()
                report.Dispose()
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, sRptName)
                report.Close()
                report.Dispose()
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmMatRequestKIK.aspx")
        End If
        If checkPagePermission("~\ReportForm\frmMatRequestKIK.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Material Request KIK Report"
        If Not Page.IsPostBack Then
            InitAllDDL()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            DDLType_SelectedIndexChanged(Nothing, Nothing)
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListRequest") Is Nothing And Session("WarningListRequest") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListRequest") Then
                Session("WarningListRequest") = Nothing
                mpeListRequest.Show()
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                mpeListMat.Show()
            End If
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLDept()
    End Sub

    Protected Sub DDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLType.SelectedIndexChanged
        Dim bVal As Boolean = True
        If DDLType.SelectedValue = "SUMMARY" Then
            bVal = False
        End If
        DDLGroupBy.SelectedIndex = -1
        lblMat.Visible = bVal : septMat.Visible = bVal : DDLMatType.Visible = bVal : FilterMaterial.Visible = bVal : btnSearchMat.Visible = bVal : btnClearMat.Visible = bVal
        lblWH.Visible = bVal : septWH.Visible = bVal : DDLWarehouse.Visible = bVal : btnAddWH.Visible = bVal : lbWarehouse.Visible = bVal : btnMinWH.Visible = bVal
        lblGroupBy.Visible = bVal : septGroupBy.Visible = bVal : DDLGroupBy.Visible = bVal
        lblSortBy.Visible = bVal : septSortBy.Visible = bVal : DDLSortBy.Visible = bVal : DDLOrder.Visible = bVal
        DDLGroupBy_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub btnSearchRequest_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchRequest.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2) : Exit Sub
        End If
        FilterDDLListRequest.SelectedIndex = -1 : FilterTextListRequest.Text = "" : Session("TblListRequest") = Nothing : Session("TblListRequestView") = Nothing : gvListRequest.DataSource = Nothing : gvListRequest.DataBind()
        BindListRequest()
    End Sub

    Protected Sub btnClearRequest_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearRequest.Click
        FilterRequest.Text = ""
    End Sub

    Protected Sub btnFindListRequest_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListRequest.Click
        UpdateCheckedListRequest()
        Dim dt As DataTable = Session("TblListRequest")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = FilterDDLListRequest.SelectedValue & " LIKE '%" & Tchar(FilterTextListRequest.Text) & "%'"
        If dv.Count > 0 Then
            Session("TblListRequestView") = dv.ToTable
            gvListRequest.DataSource = Session("TblListRequestView")
            gvListRequest.DataBind()
            dv.RowFilter = ""
            mpeListRequest.Show()
        Else
            dv.RowFilter = ""
            Session("WarningListRequest") = "Material Request KIK data can't be found!"
            showMessage(Session("WarningListRequest"), 2)
        End If
    End Sub

    Protected Sub btnAllListRequest_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListRequest.Click
        UpdateCheckedListRequest()
        FilterDDLListRequest.SelectedIndex = -1 : FilterTextListRequest.Text = ""
        Session("TblListRequestView") = Session("TblListRequest")
        gvListRequest.DataSource = Session("TblListRequestView")
        gvListRequest.DataBind()
        mpeListRequest.Show()
    End Sub

    Protected Sub gvListRequest_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListRequest.PageIndexChanging
        UpdateCheckedListRequest()
        gvListRequest.PageIndex = e.NewPageIndex
        gvListRequest.DataSource = Session("TblListRequestView")
        gvListRequest.DataBind()
        mpeListRequest.Show()
    End Sub

    Protected Sub cbHdrLMRequest_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For C1 As Integer = 0 To gvListRequest.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListRequest.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        mpeListRequest.Show()
    End Sub

    Protected Sub lbAddToListRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListRequest.Click
        UpdateCheckedListRequest()
        Dim dt As DataTable = Session("TblListRequest")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = "checkvalue='True'"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                If DDLRequest.SelectedIndex = 0 Then
                    If dv(C1)("matreqno").ToString <> "" Then
                        FilterRequest.Text &= dv(C1)("matreqno").ToString & ";"
                    End If
                Else
                    FilterRequest.Text &= dv(C1)("matreqmstoid").ToString & ";"
                End If
            Next
            If FilterRequest.Text <> "" Then
                FilterRequest.Text = Left(FilterRequest.Text, FilterRequest.Text.Length - 1)
            End If
            dv.RowFilter = ""
            cProc.SetModalPopUpExtender(btnHideListRequest, pnlListRequest, mpeListRequest, False)
        Else
            dv.RowFilter = ""
            Session("WarningListRequest") = "Please select some Material Request KIK data first!"
            showMessage(Session("WarningListRequest"), 2)
        End If
    End Sub

    Protected Sub lbSelectAllListRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSelectAllListRequest.Click
        UpdateCheckedListRequest()
        Dim dt As DataTable = Session("TblListRequestView")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            If DDLRequest.SelectedIndex = 0 Then
                If dt.Rows(C1)("matreqno").ToString <> "" Then
                    FilterRequest.Text &= dt.Rows(C1)("matreqno").ToString & ";"
                End If
            Else
                FilterRequest.Text &= dt.Rows(C1)("matreqmstoid").ToString & ";"
            End If
        Next
        If FilterRequest.Text <> "" Then
            FilterRequest.Text = Left(FilterRequest.Text, FilterRequest.Text.Length - 1)
        End If
        cProc.SetModalPopUpExtender(btnHideListRequest, pnlListRequest, mpeListRequest, False)
    End Sub

    Protected Sub lbCloseListRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListRequest.Click
        cProc.SetModalPopUpExtender(btnHideListRequest, pnlListRequest, mpeListRequest, False)
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2) : Exit Sub
        End If
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : Session("TblListMat") = Nothing : Session("TblListMatView") = Nothing : gvListMat.DataSource = Session("TblListMat") : gvListMat.DataBind()
        lblListMat.Text = "List Of " & DDLMatType.SelectedItem.Text
        BindListMat()
    End Sub

    Protected Sub btnClearMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearMat.Click
        FilterMaterial.Text = ""
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        UpdateCheckedListMat()
        Dim dt As DataTable = Session("TblListMat")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        If dv.Count > 0 Then
            Session("TblListMatView") = dv.ToTable
            gvListMat.DataSource = Session("TblListMatView")
            gvListMat.DataBind()
            dv.RowFilter = ""
            mpeListMat.Show()
        Else
            dv.RowFilter = ""
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        UpdateCheckedListMat()
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        Session("TblListMatView") = Session("TblListMat")
        gvListMat.DataSource = Session("TblListMatView")
        gvListMat.DataBind()
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        UpdateCheckedListMat()
        gvListMat.PageIndex = e.NewPageIndex
        gvListMat.DataSource = Session("TblListMatView")
        gvListMat.DataBind()
        mpeListMat.Show()
    End Sub

    Protected Sub cbHdrLM_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For C1 As Integer = 0 To gvListMat.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        mpeListMat.Show()
    End Sub

    Protected Sub lbAddToListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListMat.Click
        UpdateCheckedListMat()
        Dim dt As DataTable = Session("TblListMat")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = "checkvalue='True'"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                FilterMaterial.Text &= DDLMatType.SelectedValue & "-" & dv(C1)("matcode").ToString & ";"
            Next
            If FilterMaterial.Text <> "" Then
                FilterMaterial.Text = Left(FilterMaterial.Text, FilterMaterial.Text.Length - 1)
            End If
            dv.RowFilter = ""
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
        Else
            dv.RowFilter = ""
            Session("WarningListMat") = "Please select some Material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub lbSelectAllListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSelectAllListMat.Click
        UpdateCheckedListMat()
        Dim dt As DataTable = Session("TblListMat")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            FilterMaterial.Text &= DDLMatType.SelectedValue & "-" & dt.Rows(C1)("matcode").ToString & ";"
        Next
        If FilterMaterial.Text <> "" Then
            FilterMaterial.Text = Left(FilterMaterial.Text, FilterMaterial.Text.Length - 1)
        End If
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub btnAddDept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddDept.Click
        If DDLDept.SelectedValue <> "" Then
            If lbDept.Items.Count > 0 Then
                If Not lbDept.Items.Contains(lbDept.Items.FindByValue(DDLDept.SelectedValue)) Then
                    Dim objList As New ListItem
                    objList.Text = DDLDept.SelectedItem.Text : objList.Value = DDLDept.SelectedValue
                    lbDept.Items.Add(objList)
                End If
            Else
                Dim objList As New ListItem
                objList.Text = DDLDept.SelectedItem.Text : objList.Value = DDLDept.SelectedValue
                lbDept.Items.Add(objList)
            End If
        End If
    End Sub

    Protected Sub btnMinDept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMinDept.Click
        If lbDept.Items.Count > 0 Then
            Dim objList As ListItem = lbDept.SelectedItem
            lbDept.Items.Remove(objList)
        End If
    End Sub

    Protected Sub btnAddWH_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddWH.Click
        If DDLWarehouse.SelectedValue <> "" Then
            If lbWarehouse.Items.Count > 0 Then
                If Not lbWarehouse.Items.Contains(lbWarehouse.Items.FindByValue(DDLWarehouse.SelectedValue)) Then
                    Dim objList As New ListItem
                    objList.Text = DDLWarehouse.SelectedItem.Text : objList.Value = DDLWarehouse.SelectedValue
                    lbWarehouse.Items.Add(objList)
                End If
            Else
                Dim objList As New ListItem
                objList.Text = DDLWarehouse.SelectedItem.Text : objList.Value = DDLWarehouse.SelectedValue
                lbWarehouse.Items.Add(objList)
            End If
        End If
    End Sub

    Protected Sub btnMinWH_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMinWH.Click
        If lbWarehouse.Items.Count > 0 Then
            Dim objList As ListItem = lbWarehouse.SelectedItem
            lbWarehouse.Items.Remove(objList)
        End If
    End Sub

    Protected Sub DDLGroupBy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLGroupBy.SelectedIndexChanged
        For C1 As Integer = 0 To DDLSortBy.Items.Count - 1
            DDLSortBy.Items(C1).Enabled = True
        Next
        DDLSortBy.Items(DDLGroupBy.SelectedIndex).Enabled = False
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmMatRequestKIK.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not report Is Nothing Then
                If report.IsLoaded Then
                    report.Dispose()
                    report.Close()
                End If
            End If
        Catch ex As Exception
            report.Dispose()
            report.Close()
        End Try
    End Sub
#End Region

End Class
