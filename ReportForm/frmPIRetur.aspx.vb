Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_Pret
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function FillDDLWithALL(ByRef oDDLObject As DropDownList, ByVal sSql As String) As Boolean
        ' To fill data in dropdown list, return true when all data exist
        FillDDLWithALL = True
        oDDLObject.Items.Clear()
        oDDLObject.Items.Add("ALL")
        oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = "ALL"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            oDDLObject.Items.Add(xreader.GetValue(1))
            oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()
        ' Cek if all data exits
        If oDDLObject.Items.Count = 0 Then
            FillDDLWithALL = False
        End If
        Return FillDDLWithALL
    End Function

    Private Function UpdateCheckedCust() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblCust") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCust")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListCust.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListCust.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "custoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblCust") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedCust2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCust")
            Dim dtTbl2 As DataTable = Session("TblCustView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListCust.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListCust.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "custoid=" & cbOid
                                dtView2.RowFilter = "custoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblCust") = dtTbl
                Session("TblCustView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedSret() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSret") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSret")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListSret.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSret.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "sretmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblSret") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedSret2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSretView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSret")
            Dim dtTbl2 As DataTable = Session("TblSretView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListSret.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSret.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "sretmstoid=" & cbOid
                                dtView2.RowFilter = "sretmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblSret") = dtTbl
                Session("TblSretView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedShipment() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblShipment") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblShipment")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListShipment.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListShipment.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "trnjualmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblShipment") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedShipment2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblShipmentView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblShipment")
            Dim dtTbl2 As DataTable = Session("TblShipmentView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListShipment.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListShipment.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "ShipmentRawmstoid=" & cbOid
                                dtView2.RowFilter = "ShipmentRawmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblShipment") = dtTbl
                Session("TblShipmentView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matoid=" & cbOid
                                dtView2.RowFilter = "matoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindListCust()
        If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
            If IsValidPeriod() Then
                sSql = "SELECT 'False' AS checkvalue,suppoid as custoid,suppcode as custcode,suppname as custname,suppaddr as custaddr FROM QL_mstsupp WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'  AND suppoid IN (SELECT suppoid from QL_trnreturmst sretm INNER JOIN QL_trnreturdtl sretd ON sretm.retmstoid=sretd.retmstoid WHERE sretm.cmpcode='" & CompnyCode & "' AND " & FilterDDLDate.SelectedValue & ">='" & FilterPeriod1.Text & " 00:00:00' AND " & FilterDDLDate.SelectedValue & "<='" & FilterPeriod2.Text & " 23:59:59' ) ORDER BY custcode"
            Else
                Exit Sub
            End If
        End If
        Session("TblCust") = cKon.ambiltabel(sSql, "QL_mstcust")
    End Sub

    Private Sub BindListShipment()
        Dim Cust As String = ""
        If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
            If IsValidPeriod() Then
                sSql = "SELECT DISTINCT 'False' AS checkvalue, shm.trnbelimstoid AS trnjualmstoid, shm.trnbelino as trnjualno, shm.trnbelidate as trnjualdate, CONVERT(VARCHAR(10), shm.trnbelidate, 101) AS trnjualdate1,c.suppcode AS CustCode, c.suppname AS custname, shm.trnbelimststatus AS trnjualstatus, shm.trnbelimstnote as trnjualnote FROM QL_trnbelimst shm INNER JOIN QL_trnbelidtl shd ON shm.trnbelimstoid=shd.trnbelimstoid INNER JOIN QL_mstsupp c ON shm.suppoid=c.suppoid AND c.activeflag='ACTIVE' WHERE shm.trnbelimstoid IN (SELECT sretm.refoidmst from QL_trnreturdtl sretm INNER JOIN QL_trnreturmst rm ON rm.retmstoid=sretm.retmstoid AND sretm.refoidmst=shm.trnbelimstoid AND c.suppoid=shm.suppoid AND c.activeflag='ACTIVE' AND sretm.cmpcode='" & CompnyCode & "' AND shm.createtime >='" & FilterPeriod1.Text & " 00:00:00' AND shm.createtime <='" & FilterPeriod2.Text & " 23:59:59' AND sretm.retdtlres1='" & TypeRetDDL.SelectedValue & "')" & _
                "  UNION ALL " & _
                "  SELECT DISTINCT 'False' AS checkvalue, shm.mrmstoid AS trnjualmstoid, shm.mrno as trnjualno, shm.mrdate as trnjualdate, CONVERT(VARCHAR(10), shm.mrdate, 101) AS trnjualdate1,c.suppcode AS CustCode, c.suppname AS custname, shm.mrmststatus AS trnjualstatus, shm.mrmstnote as trnjualnote FROM QL_trnmrmst shm INNER JOIN ql_trnmrdtl md ON md.mrmstoid=shm.mrmstoid INNER JOIN QL_mstsupp c ON shm.suppoid=c.suppoid WHERE shm.cmpcode='" & CompnyCode & "' AND shm.mrmstoid IN (SELECT refoidmst rd FROM QL_trnreturdtl rd INNER JOIN QL_trnreturmst rm ON rm.retmstoid=rd.retmstoid AND rd.refoidmst=shm.mrmstoid AND c.suppoid=shm.suppoid AND shm.cmpcode=rd.cmpcode AND shm.createtime >='" & FilterPeriod1.Text & " 00:00:00' AND shm.createtime <='" & FilterPeriod2.Text & " 23:59:59' AND rd.retdtlres1='" & TypeRetDDL.SelectedValue & "')"

                If custcode.Text <> "" Then
                    Dim scode() As String = Split(custcode.Text, ";")
                    Cust = " AND ("
                    For c1 As Integer = 0 To scode.Length - 1
                        Cust = " c.suppcode = '" & Tchar(scode(c1)) & "'"
                        If c1 < scode.Length - 1 Then
                            Cust = " OR "
                        End If
                    Next
                    Cust = ")"
                End If
            Else
                Exit Sub
            End If
        End If

        sSql &= " ORDER BY trnjualdate DESC,trnjualmstoid DESC"
        Session("TblShipment") = cKon.ambiltabel(sSql, "QL_trnjualmst")
    End Sub

    Private Sub BindListSret()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, sretm.Retmstoid as sretmstoid, sretm.Retno as sretno, sretm.retdate as SRetdate, CONVERT(VARCHAR(10), sretm.Retdate, 101) AS sretdate1, c.suppname AS custname, sretm.Retmststatus as sretmststatus, sretm.Retmstnote as sretmstnote FROM QL_trnreturmst sretm INNER JOIN QL_trnreturdtl sretd ON sretm.Retmstoid=sretd.Retmstoid INNER JOIN QL_mstsupp c ON sretm.suppoid=c.suppoid AND c.activeflag='ACTIVE' WHERE sretm.cmpcode='" & CompnyCode & "' AND sretm.rettype='" & TypeRetDDL.SelectedItem.Text & "' "

        If custcode.Text <> "" Then
            Dim scode() As String = Split(custcode.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " c.suppcode = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        'Filter Shipment
        If FilterDDLType.SelectedValue = "Detail" Then
            If shipmentno.Text <> "" Then
                If TypeRetDDL.SelectedValue = "QL_trnbelimst" Then
                    sSql = "AND sretd.refoidmst IN (SELECT bm.trnbelimstoid FROM QL_trnbelimst bm WHERE bm.trnbelimstoid=sretd.refoidmst"
                    Dim sShipmentno() As String = Split(shipmentno.Text, ";")
                    sSql &= " AND "
                    For c1 As Integer = 0 To sShipmentno.Length - 1
                        sSql &= " bm.trnbelino LIKE '%" & Tchar(sShipmentno(c1)) & "%'"
                        If c1 < sShipmentno.Length - 1 Then
                            sSql &= " OR "
                        End If
                    Next
                    sSql &= ")"
                Else
                    sSql = "AND sretd.refoidmst IN (SELECT bm.mrmstoid FROM QL_trnmrmst bm WHERE bm.mrmstoid=sretd.refoidmst)"
                    Dim sShipmentno() As String = Split(shipmentno.Text, ";")
                    sSql &= " AND "
                    For c1 As Integer = 0 To sShipmentno.Length - 1
                        sSql &= "bm.mrno LIKE '%" & Tchar(sShipmentno(c1)) & "%'"
                        If c1 < sShipmentno.Length - 1 Then
                            sSql &= " OR "
                        End If
                    Next
                    sSql &= ")"
                End If
            End If
        End If

        If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
            If IsValidPeriod() Then
                sSql &= "AND " & FilterDDLDate.SelectedValue & " >='" & FilterPeriod1.Text & " 00:00:00' AND " & FilterDDLDate.SelectedValue & " <='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If

        sSql &= " ORDER BY sretm.Retdate DESC, sretm.Retmstoid DESC"
        Session("TblSret") = cKon.ambiltabel(sSql, "QL_trnSalesreturmst")
    End Sub

    Private Sub BindListMat()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, sretd.matoid matoid, m.itemlongdescription matlongdesc, m.itemcode matcode, g2.gendesc unit FROM QL_trnreturdtl sretd INNER JOIN QL_trnreturmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.Retmstoid=sretd.Retmstoid INNER JOIN QL_mstitem m ON m.itemoid=sretd.matoid INNER JOIN QL_mstgen g2 ON g2.genoid=sretd.retunitoid INNER JOIN QL_mstsupp c ON sretm.suppoid=c.suppoid AND c.activeflag='ACTIVE' INNER JOIN QL_trnbelimst bm ON bm.trnbelimstoid=sretd.refoidmst INNER JOIN QL_trnmrmst mr ON mr.mrmstoid=sretd.refoidmst WHERE sretm.cmpcode='" & CompnyCode & "'"

        If custcode.Text <> "" Then
            Dim scode() As String = Split(custcode.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " c.suppcode = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        'Filter Sales Return
        If sretno.Text <> "" Then
            If DDLSretNo.SelectedValue = "Return No." Then
                Dim sSretno() As String = Split(sretno.Text, ";")
                sSql &= " AND ("
                For c1 As Integer = 0 To sSretno.Length - 1
                    sSql &= " sretm.Retno LIKE '%" & Tchar(sSretno(c1)) & "%'"
                    If c1 < sSretno.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            Else
                Dim sSretno() As String = Split(sretno.Text, ";")
                sSql &= " AND ("
                For c1 As Integer = 0 To sSretno.Length - 1
                    sSql &= " sretm.Retmstoid = " & ToDouble(sSretno(c1))
                    If c1 < sSretno.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            End If
        End If

        'Filter Shipment
        If FilterDDLType.SelectedValue = "Detail" Then
            If TypeRetDDL.SelectedValue = "QL_trnbelimst" Then
                If shipmentno.Text <> "" Then
                    sSql &= " AND sretm.Retmstoid IN (SELECT sretm2.retmstoid FROM QL_trnreturdtl sretm2 INNER JOIN QL_trnbelimst shm ON shm.trnbelimstoid=sretm2.refoidmst "
                    Dim sShipmentno() As String = Split(shipmentno.Text, ";")
                    sSql &= " WHERE "
                    For c1 As Integer = 0 To sShipmentno.Length - 1
                        sSql &= " shm.trnbelino LIKE '%" & Tchar(sShipmentno(c1)) & "%' AND sretm2.retdtlres1='" & TypeRetDDL.SelectedValue & "'"
                        If c1 < sShipmentno.Length - 1 Then
                            sSql &= " OR "
                        End If
                    Next
                    sSql &= ")"
                End If
            Else
                If shipmentno.Text <> "" Then
                    sSql &= " AND sretm.Retmstoid IN (SELECT sretm2.retmstoid FROM QL_trnreturdtl sretm2 INNER JOIN QL_trnmrmst shm ON shm.mrmstoid=sretm2.refoidmst "
                    Dim sShipmentno() As String = Split(shipmentno.Text, ";")
                    sSql &= " WHERE "
                    For c1 As Integer = 0 To sShipmentno.Length - 1
                        sSql &= " shm.trnbelino LIKE '%" & Tchar(sShipmentno(c1)) & "%' AND sretm2.retdtlres1='" & TypeRetDDL.SelectedValue & "'"
                        If c1 < sShipmentno.Length - 1 Then
                            sSql &= " OR "
                        End If
                    Next
                    sSql &= ")"
                End If
            End If
        End If

        If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
            If IsValidPeriod() Then
                sSql &= "AND sretm.createtime >='" & FilterPeriod1.Text & " 00:00:00' AND sretm.createtime <='" & FilterPeriod2.Text & " 23:59:59' "
            Else
                Exit Sub
            End If
        End If

        Session("TblMat") = cKon.ambiltabel(sSql, "QL_mstitem")
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        Dim sWhere As String = ""
        Dim rptName As String = ""
        Dim Dtl As String = ""
        Dim Join As String = ""
        Try
            If FilterDDLType.SelectedValue = "Summary" Then
                If sType = "Print Excel" Then
                    'report.Load(Server.MapPath(folderReport & "rptSret_SumXls.rpt"))
                    report.Load(Server.MapPath(folderReport & "rptPIRetSum.rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptPIRetSum.rpt"))
                End If
                rptName = "PurchaseReturnSummary"
            Else
                If sType = "Print Excel" Then
                    report.Load(Server.MapPath(folderReport & "rptPIRetDtl.rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptPIRetDtl.rpt"))
                End If
                rptName = "PurchaseReturnDetail"
            End If

            If FilterDDLType.SelectedValue = "Detail" Then
                'If TypeRetDDL.SelectedValue = "QL_trnbelimst" Then
                sSql = "SELECT rm.cmpcode,d.divname,rm.retmstoid AS pomstoid,Case rm.rettype When 'Return PI' Then bm.trnbelino Else mr.mrno End [No. PI],rm.retno AS [No. PO],mr.mrno AS [No. LPB],s.suppoid AS OidSupp,s.suppname AS [Nama Supplier],i.itemoid AS OidItem,i.itemCode AS [Kode Barang],i.itemoldcode AS OldKodeItem,rm.rettype,i.itemLongDescription AS [Nama Barang],u.gendesc AS [Satuan],rd.retqty AS [Qty],rd.retdtlamt AS [SubtotalNet],rm.retmstnote AS [Note],rm.retmstamt AS Netto,rm.retmststatus AS FlagMst,rd.retdtlstatus AS Flagdtl,rd.retvalue AS [hrg Item],rd.retdtlamt AS NetDtl FROM QL_trnreturmst rm INNER JOIN QL_mstsupp s ON s.suppoid=rm.suppoid INNER JOIN QL_mstdivision d ON rm.cmpcode=d.divcode INNER JOIN QL_trnreturdtl rd ON rd.retmstoid=rm.retmstoid INNER JOIN QL_mstitem i ON i.itemoid=rd.matoid INNER JOIN QL_mstgen u ON u.genoid=rd.retunitoid INNER JOIN QL_mstgen r ON r.genoid=rd.reasonoid LEFT JOIN QL_trnmrmst mr ON mr.mrmstoid=rd.refoidmst LEFT JOIN QL_trnbelimst bm ON rd.refoidmst=bm.trnbelimstoid"
                'End If
            Else
                sSql = "SELECT rm.cmpcode,d.divname,rm.retmstoid AS pomstoid,rm.retdate AS [tgl PO],rm.retno AS [No Po],rm.rettype,rm.suppoid,s.suppname AS [Supp. Name],GETDATE() AS [Tgl Nota],0 AS trnbelimstoid,'-' AS [No. Nota],0 AS curroid,'' AS Currency,s.suppcode,0.00 AS Diskon,0.00 AS PPn,rm.retmstamt AS DPP,rm.retmstamt  AS Netto,rm.retmstnote AS [Hdr Note] ,rm.retmststatus  AS Flag, 0.0 AS TotalNet FROM QL_trnreturmst rm INNER JOIN QL_mstsupp s ON s.suppoid=rm.suppoid INNER JOIN QL_mstdivision d ON rm.cmpcode=d.divcode"
            End If
            sSql &= " WHERE rm.cmpcode='" & CompnyCode & "' AND rm.rettype='" & TypeRetDDL.SelectedItem.Text & "' "
            If FilterDDLStatus.SelectedValue <> "All" Then
                sSql &= "AND rm.retmststatus='" & FilterDDLStatus.SelectedValue & "'"
            Else
                sSql &= ""
            End If
            If custcode.Text <> "" Then
                Dim scode() As String = Split(custcode.Text, ";")
                sSql &= " AND ("
                For c1 As Integer = 0 To scode.Length - 1
                    sSql &= " s.suppcode = '" & Tchar(scode(c1)) & "'"
                    If c1 < scode.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            End If

            'Filter Sales Return
            If sretno.Text <> "" Then
                If DDLSretNo.SelectedValue = "Return No." Then
                    Dim sSretno() As String = Split(sretno.Text, ";")
                    sSql &= " AND ("
                    For c1 As Integer = 0 To sSretno.Length - 1
                        sSql &= " rm.retno  LIKE '%" & Tchar(sSretno(c1)) & "%'"
                        If c1 < sSretno.Length - 1 Then
                            sSql &= " OR "
                        End If
                    Next
                    sSql &= ")"
                Else
                    Dim sSretno() As String = Split(sretno.Text, ";")
                    sSql &= " AND ("
                    For c1 As Integer = 0 To sSretno.Length - 1
                        sSql &= " rm.retmstoid = " & ToDouble(sSretno(c1))
                        If c1 < sSretno.Length - 1 Then
                            sSql &= " OR "
                        End If
                    Next
                    sSql &= ")"
                End If
            End If

            If FilterDDLType.SelectedValue = "Detail" Then
                'Filter Shipment
                If TypeRetDDL.SelectedValue = "QL_trnbelimst" Then
                    If shipmentno.Text <> "" Then
                        sSql &= " "
                        Dim sShipmentno() As String = Split(shipmentno.Text, ";")
                        sSql &= " AND ("
                        For c1 As Integer = 0 To sShipmentno.Length - 1
                            sSql &= "bm.trnbelino LIKE '%" & Tchar(sShipmentno(c1)) & "%'"
                            If c1 < sShipmentno.Length - 1 Then
                                sSql &= " OR "
                            End If
                        Next
                        sSql &= ")"
                    End If
                Else
                    If shipmentno.Text <> "" Then
                        sSql &= " "
                        Dim sShipmentno() As String = Split(shipmentno.Text, ";")
                        sSql &= " AND ("
                        For c1 As Integer = 0 To sShipmentno.Length - 1
                            sSql &= "mr.mrno LIKE '%" & Tchar(sShipmentno(c1)) & "%'"
                            If c1 < sShipmentno.Length - 1 Then
                                sSql &= " OR "
                            End If
                        Next
                        sSql &= ")"
                    End If

                End If
                'Filter Material
                If matcode.Text <> "" Then
                    Dim sMatcode() As String = Split(matcode.Text, ";")
                    sSql &= " AND ("
                    For c1 As Integer = 0 To sMatcode.Length - 1
                        sSql &= " m.itemcode LIKE '%" & Tchar(sMatcode(c1)) & "%'"
                        If c1 < sMatcode.Length - 1 Then
                            sSql &= " OR "
                        End If
                    Next
                    sSql &= ")"
                End If
            End If

            If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
                If IsValidPeriod() Then
                    sSql &= " AND rm.retdate >='" & FilterPeriod1.Text & " 00:00:00' AND  rm.retdate <='" & FilterPeriod2.Text & " 23:59:59'"
                Else
                    Exit Sub
                End If
            End If

            

            Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnreturmst")
            Dim dvTbl As DataView = dtTbl.DefaultView
            report.SetDataSource(dvTbl.ToTable)
            'report.PrintOptions.PaperSize = PaperSize.PaperFolio
            cProc.SetDBLogonForReport(report)
            report.SetParameterValue("sPeriod", Format(CDate(FilterPeriod1.Text), "MM/dd/yyyy") & " - " & Format(CDate(FilterPeriod1.Text), "MM/dd/yyyy"))

            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, rptName)
                report.Close()
                report.Dispose()
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, rptName)
                report.Close()
                report.Dispose()
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Private Sub InitAllDDL()
        ' DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)
    End Sub

    Private Sub InitFilterDDLCat1()
        'Fill DDL Category1
        sSql = "SELECT cat1code, cat1code+' - '+cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat1res1='Raw' Order By cat1code"
        FillDDL(FilterDDLCat01, sSql)
        If FilterDDLCat01.Items.Count > 0 Then
            InitFilterDDLCat2()
        Else
            FilterDDLCat02.Items.Clear()
            FilterDDLCat03.Items.Clear()
            FilterDDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitFilterDDLCat2()
        'Fill DDL Category2
        sSql = "SELECT cat2code, cat2code+' - '+cat2shortdesc FROM QL_mstcat2 cat2 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat2.cmpcode AND cat1.cat1oid=cat2.cat1oid AND cat1.cat1code='" & FilterDDLCat01.SelectedValue & "' WHERE cat2.cmpcode='" & CompnyCode & "' AND cat2.activeflag='ACTIVE' AND cat2res1='Raw' Order By cat2code"
        FillDDL(FilterDDLCat02, sSql)
        If FilterDDLCat02.Items.Count > 0 Then
            InitFilterDDLCat3()
        Else
            FilterDDLCat03.Items.Clear()
            FilterDDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitFilterDDLCat3()
        'Fill DDL Category3
        sSql = "SELECT cat3code, cat3code+' - '+cat3shortdesc FROM QL_mstcat3 cat3 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat3.cmpcode AND cat1.cat1oid=cat3.cat1oid AND cat1.cat1code='" & FilterDDLCat01.SelectedValue & "' INNER JOIN QL_mstcat2 cat2 ON cat2.cmpcode=cat3.cmpcode AND cat2.cat2oid=cat3.cat2oid AND cat2.cat2code='" & FilterDDLCat02.SelectedValue & "' WHERE cat3.cmpcode='" & CompnyCode & "' AND cat3.activeflag='ACTIVE' AND cat3res1='Raw' Order By cat3code"
        FillDDL(FilterDDLCat03, sSql)
    End Sub

    Private Sub InitFilterDDLCat4()
        'Fill DDL Category4
        sSql = "SELECT cat4code, cat4code+' - '+cat4shortdesc FROM QL_mstcat4 cat4 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat4.cmpcode AND cat1.cat1oid=cat4.cat1oid AND cat1.cat1code='" & FilterDDLCat01.SelectedValue & "' INNER JOIN QL_mstcat2 cat2 ON cat2.cmpcode=cat4.cmpcode AND cat2.cat2oid=cat4.cat2oid AND cat2.cat2code='" & FilterDDLCat02.SelectedValue & "' INNER JOIN QL_mstcat3 cat3 ON cat3.cmpcode=cat4.cmpcode AND cat3.cat3oid=cat4.cat3oid AND cat3.cat3code='" & FilterDDLCat03.SelectedValue & "' WHERE cat4.cmpcode='" & CompnyCode & "' AND cat4.activeflag='ACTIVE' AND cat4res1='Raw' Order By cat4code"
        FillDDL(FilterDDLCat04, sSql)
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If

        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            'Clear all session
            Session.Clear()
            'Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            '    ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmPIRetur.aspx")

        End If

        If checkPagePermission("~\ReportForm\frmPIRetur.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        Page.Title = CompnyName & " - Purchase Return Report"
        If Not Page.IsPostBack Then
            InitAllDDL()
            InitFilterDDLCat1()
            FilterDDLType.SelectedIndex = 0
            TypeRetDDL_SelectedIndexChanged(Nothing, Nothing)
            FilterDDLType_SelectedIndexChanged(Nothing, Nothing)
            FilterDDLStatus_SelectedIndexChanged(Nothing, Nothing)
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListCust") Is Nothing And Session("EmptyListCust") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListCust") Then
                Session("EmptyListCust") = Nothing
                cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
            End If
        End If
        If Not Session("WarningListCust") Is Nothing And Session("WarningListCust") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListCust") Then
                Session("WarningListCust") = Nothing
                cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
            End If
        End If
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If

        If Not Session("EmptyListSret") Is Nothing And Session("EmptyListSret") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListSret") Then
                Session("EmptyListSret") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListSret, PanelListSret, mpeListSret, True)
            End If
        End If
        If Not Session("WarningListSret") Is Nothing And Session("WarningListSret") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListSret") Then
                Session("WarningListSret") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListSret, PanelListSret, mpeListSret, True)
            End If
        End If

        If Not Session("EmptyListShipment") Is Nothing And Session("EmptyListShipment") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListShipment") Then
                Session("EmptyListShipment") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListShipment, PanelListShipment, mpeListShipment, True)
            End If
        End If
        If Not Session("WarningListShipment") Is Nothing And Session("WarningListShipment") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListShipment") Then
                Session("WarningListShipment") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListShipment, PanelListShipment, mpeListShipment, True)
            End If
        End If
    End Sub

    Protected Sub FilterDDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLType.SelectedIndexChanged
        If FilterDDLType.SelectedValue = "Summary" Then
            'Fill DDL Sorting
            lblMat.Visible = False : lbl2.Visible = False
            matcode.Visible = False : imbFindMat.Visible = False
            imbEraseMat.Visible = False : lblShipment.Visible = False
            lbl3.Visible = False : shipmentno.Visible = False
            imbFindShipment.Visible = False : imbEraseShipment.Visible = False
        Else
            lblMat.Visible = True : lbl2.Visible = True
            matcode.Visible = True : imbFindMat.Visible = True
            imbEraseMat.Visible = True : lblShipment.Visible = True
            lbl3.Visible = True : shipmentno.Visible = True
            imbFindShipment.Visible = True : imbEraseShipment.Visible = True
        End If
    End Sub

    Protected Sub FilterDDLStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLStatus.SelectedIndexChanged
        If FilterDDLStatus.SelectedValue = "Approved" Then
            FilterDDLDate.Items.Clear()
            FilterDDLDate.Items.Add("Create Date")
            FilterDDLDate.Items.Item(FilterDDLDate.Items.Count - 1).Value = "sretm.createtime"
            FilterDDLDate.Items.Add("Approval Date")
            FilterDDLDate.Items.Item(FilterDDLDate.Items.Count - 1).Value = "sretm.approvaldatetime"
        Else
            FilterDDLDate.Items.Clear()
            FilterDDLDate.Items.Add("Create Date")
            FilterDDLDate.Items.Item(FilterDDLDate.Items.Count - 1).Value = "sretm.createtime"
        End If
    End Sub

    Protected Sub btnClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearCust.Click
        custcode.Text = ""
    End Sub

    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchCust.Click
        If IsValidPeriod() Then
            DDLFilterListCust.SelectedIndex = -1 : txtFilterListCust.Text = ""
            Session("TblCust") = Nothing : Session("TblCustView") = Nothing : gvListCust.DataSource = Nothing : gvListCust.DataBind()
            cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub btnFindListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListCust.Click
        If Session("TblCust") Is Nothing Then
            BindListCust()
            If Session("TblCust").Rows.Count <= 0 Then
                Session("EmptyListCust") = "Supplier data can't be found!"
                showMessage(Session("EmptyListCust"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListCust.SelectedValue & " LIKE '%" & Tchar(txtFilterListCust.Text) & "%'"
        If UpdateCheckedCust() Then
            Dim dv As DataView = Session("TblCust").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblCustView") = dv.ToTable
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                dv.RowFilter = ""
                mpeListCust.Show()
            Else
                dv.RowFilter = ""
                Session("TblCustView") = Nothing
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                Session("WarningListCust") = "Customer data can't be found!"
                showMessage(Session("WarningListCust"), 2)
            End If
        Else
            Session("WarningListCust") = "Customer data can't be found!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub

    Protected Sub btnViewAllListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListCust.Click
        DDLFilterListCust.SelectedIndex = -1 : txtFilterListCust.Text = ""
        If Session("TblCust") Is Nothing Then
            BindListCust()
            If Session("TblCust").Rows.Count <= 0 Then
                Session("EmptyListCust") = "Customer data can't be found!"
                showMessage(Session("EmptyListCust"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedCust() Then
            Dim dt As DataTable = Session("TblCust")
            Session("TblCustView") = dt
            gvListCust.DataSource = Session("TblCustView")
            gvListCust.DataBind()
            mpeListCust.Show()
        Else
            Session("WarningListCust") = "Customer data can't be found!"
            showMessage(Session("WarningListCust"), 2)
        End If

    End Sub

    Protected Sub btnSelectAllCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllCust.Click
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCustView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblCust")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "custoid=" & dtTbl.Rows(C1)("custoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblCust") = objTbl
                Session("TblCustView") = dtTbl
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
            End If
            mpeListCust.Show()
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneCust.Click
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCustView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblCust")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "custoid=" & dtTbl.Rows(C1)("custoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblCust") = objTbl
                Session("TblCustView") = dtTbl
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
            End If
            mpeListCust.Show()
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedCust.Click
        If Session("TblCust") Is Nothing Then
            Session("WarningListCust") = "Selected Customer data can't be found!"
            showMessage(Session("WarningListCust"), 2)
            Exit Sub
        End If
        If UpdateCheckedCust() Then
            Dim dtTbl As DataTable = Session("TblCust")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListCust.SelectedIndex = -1 : txtFilterListCust.Text = ""
                Session("TblCustView") = dtView.ToTable
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                dtView.RowFilter = ""
                mpeListCust.Show()
            Else
                dtView.RowFilter = ""
                Session("TblCustView") = Nothing
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                Session("WarningListCust") = "Selected Customer data can't be found!"
                showMessage(Session("WarningListCust"), 2)
            End If
        Else
            mpeListCust.Show()
        End If
    End Sub

    Protected Sub gvListCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListCust.PageIndexChanging
        If UpdateCheckedCust2() Then
            gvListCust.PageIndex = e.NewPageIndex
            gvListCust.DataSource = Session("TblCustView")
            gvListCust.DataBind()
        End If
        mpeListCust.Show()
    End Sub

    Protected Sub lkbAddToListListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListCust.Click
        If Not Session("TblCust") Is Nothing Then
            If UpdateCheckedCust() Then
                Dim dtTbl As DataTable = Session("TblCust")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If custcode.Text <> "" Then
                            custcode.Text &= ";" + vbCrLf + dtView(C1)("custcode")
                        Else
                            custcode.Text = dtView(C1)("custcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
                Else
                    Session("WarningListCust") = "Please select Customer to add to list!"
                    showMessage(Session("WarningListCust"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbCloseListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListCust.Click
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub

    Protected Sub imbFindShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindShipment.Click
        If IsValidPeriod() Then
            DDLFilterListShipment.SelectedIndex = -1 : txtFilterListShipment.Text = ""
            Session("TblShipment") = Nothing : Session("TblShipmentView") = Nothing : gvListShipment.DataSource = Nothing : gvListShipment.DataBind()
            cProc.SetModalPopUpExtender(btnHiddenListShipment, PanelListShipment, mpeListShipment, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbEraseShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseShipment.Click
        shipmentno.Text = ""
        DDLBusUnit.Enabled = True
    End Sub

    Protected Sub btnFindListShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListShipment.Click
        If Session("TblShipment") Is Nothing Then
            BindListShipment()
            If Session("TblShipment").Rows.Count <= 0 Then
                Session("EmptyListShipment") = "Shipment data can't be found!"
                showMessage(Session("EmptyListShipment"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListShipment.SelectedValue & " LIKE '%" & Tchar(txtFilterListShipment.Text) & "%'"
        If UpdateCheckedShipment() Then
            Dim dv As DataView = Session("TblShipment").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblShipmentView") = dv.ToTable
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
                dv.RowFilter = ""
                mpeListShipment.Show()
            Else
                dv.RowFilter = ""
                Session("TblShipmentView") = Nothing
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
                Session("WarningListShipment") = "Shipment data can't be found!"
                showMessage(Session("WarningListShipment"), 2)
            End If
        Else
            mpeListShipment.Show()
        End If
    End Sub

    Protected Sub btnViewAllListShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListShipment.Click
        DDLFilterListShipment.SelectedIndex = -1 : txtFilterListShipment.Text = ""
        If Session("TblShipment") Is Nothing Then
            BindListShipment()
            If Session("TblShipment").Rows.Count <= 0 Then
                Session("EmptyListShipment") = "Shipment data can't be found!"
                showMessage(Session("EmptyListShipment"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedShipment() Then
            Dim dt As DataTable = Session("TblShipment")
            Session("TblShipmentView") = dt
            gvListShipment.DataSource = Session("TblShipmentView")
            gvListShipment.DataBind()
        End If
        mpeListShipment.Show()
    End Sub

    Protected Sub btnSelectAllShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllShipment.Click
        If Not Session("TblShipmentView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblShipmentView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblShipment")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "trnjualmstoid=" & dtTbl.Rows(C1)("trnjualmstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblShipment") = objTbl
                Session("TblShipmentView") = dtTbl
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
            End If
            mpeListShipment.Show()
        Else
            Session("WarningListShipment") = "Please show some Shipment data first!"
            showMessage(Session("WarningListShipment"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneShipment.Click
        If Not Session("TblShipmentView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblShipmentView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblShipment")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "trnjualmstoid=" & dtTbl.Rows(C1)("trnjualmstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblShipment") = objTbl
                Session("TblShipmentView") = dtTbl
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
            End If
            mpeListShipment.Show()
        Else
            Session("WarningListShipment") = "Please show some Shipment data first!"
            showMessage(Session("WarningListShipment"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedShipment.Click
        If Session("TblShipment") Is Nothing Then
            Session("WarningListShipment") = "Selected Shipment data can't be found!"
            showMessage(Session("WarningListShipment"), 2)
            Exit Sub
        End If
        If UpdateCheckedShipment() Then
            Dim dtTbl As DataTable = Session("TblShipment")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListShipment.SelectedIndex = -1 : txtFilterListShipment.Text = ""
                Session("TblShipmentView") = dtView.ToTable
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
                dtView.RowFilter = ""
                mpeListShipment.Show()
            Else
                dtView.RowFilter = ""
                Session("TblShipmentView") = Nothing
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
                Session("WarningListShipment") = "Selected Shipment data can't be found!"
                showMessage(Session("WarningListShipment"), 2)
            End If
        Else
            mpeListShipment.Show()
        End If
    End Sub

    Protected Sub gvListShipment_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListShipment.PageIndexChanging
        If UpdateCheckedShipment2() Then
            gvListShipment.PageIndex = e.NewPageIndex
            gvListShipment.DataSource = Session("TblShipmentView")
            gvListShipment.DataBind()
        End If
        mpeListShipment.Show()
    End Sub

    Protected Sub lkbAddToListListShipment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListShipment.Click
        If Not Session("TblShipment") Is Nothing Then
            If UpdateCheckedShipment() Then
                Dim dtTbl As DataTable = Session("TblShipment")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If shipmentno.Text <> "" Then
                            If dtView(C1)("trnjualno") <> "" Then
                                shipmentno.Text &= ";" + vbCrLf + dtView(C1)("trnjualno")
                            End If
                        Else
                            If dtView(C1)("trnjualno") <> "" Then
                                shipmentno.Text &= dtView(C1)("trnjualno")
                            End If
                        End If
                    Next
                    dtView.RowFilter = ""
                    DDLBusUnit.Enabled = False
                    cProc.SetModalPopUpExtender(btnHiddenListShipment, PanelListShipment, mpeListShipment, False)
                Else
                    Session("WarningListShipment") = "Please select Shipment to add to list!"
                    showMessage(Session("WarningListShipment"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListShipment") = "Please show some Shipment data first!"
            showMessage(Session("WarningListShipment"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListShipment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListShipment.Click
        cProc.SetModalPopUpExtender(btnHiddenListShipment, PanelListShipment, mpeListShipment, False)
    End Sub

    Protected Sub DDLSretNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLSretNo.SelectedIndexChanged
        sretno.Text = ""
    End Sub

    Protected Sub imbFindSret_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindSret.Click
        If IsValidPeriod() Then
            DDLFilterListSret.SelectedIndex = -1 : txtFilterListSret.Text = ""
            Session("TblSret") = Nothing : Session("TblSretView") = Nothing : gvListSret.DataSource = Nothing : gvListSret.DataBind()
            cProc.SetModalPopUpExtender(btnHiddenListSret, PanelListSret, mpeListSret, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbEraseSret_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseSret.Click
        sretno.Text = ""
        DDLBusUnit.Enabled = True
    End Sub

    Protected Sub btnFindListSret_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSret.Click
        If Session("TblSret") Is Nothing Then
            BindListSret()
            If Session("TblSret").Rows.Count <= 0 Then
                Session("EmptyListSret") = "Sales Return data can't be found!"
                showMessage(Session("EmptyListSret"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListSret.SelectedValue & " LIKE '%" & Tchar(txtFilterListSret.Text) & "%'"
        If UpdateCheckedSret() Then
            Dim dv As DataView = Session("TblSret").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblSretView") = dv.ToTable
                gvListSret.DataSource = Session("TblSretView")
                gvListSret.DataBind()
                dv.RowFilter = ""
                mpeListSret.Show()
            Else
                dv.RowFilter = ""
                Session("TblSretView") = Nothing
                gvListSret.DataSource = Session("TblSretView")
                gvListSret.DataBind()
                Session("WarningListSret") = "Sales Return data can't be found!"
                showMessage(Session("WarningListSret"), 2)
            End If
        Else
            mpeListSret.Show()
        End If
    End Sub

    Protected Sub btnViewAllListSret_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListSret.Click
        DDLFilterListSret.SelectedIndex = -1 : txtFilterListSret.Text = ""
        If Session("TblSret") Is Nothing Then
            BindListSret()
            If Session("TblSret").Rows.Count <= 0 Then
                Session("EmptyListSret") = "Sales Return data can't be found!"
                showMessage(Session("EmptyListSret"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedSret() Then
            Dim dt As DataTable = Session("TblSret")
            Session("TblSretView") = dt
            gvListSret.DataSource = Session("TblSretView")
            gvListSret.DataBind()
        End If
        mpeListSret.Show()
    End Sub

    Protected Sub btnSelectAllSret_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllSret.Click
        If Not Session("TblSretView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSretView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSret")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "sretmstoid=" & dtTbl.Rows(C1)("sretmstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSret") = objTbl
                Session("TblSretView") = dtTbl
                gvListSret.DataSource = Session("TblSretView")
                gvListSret.DataBind()
            End If
            mpeListSret.Show()
        Else
            Session("WarningListSret") = "Please show some Sales Return data first!"
            showMessage(Session("WarningListSret"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneSret_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneSret.Click
        If Not Session("TblSretView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSretView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSret")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "sretmstoid=" & dtTbl.Rows(C1)("sretmstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSret") = objTbl
                Session("TblSretView") = dtTbl
                gvListSret.DataSource = Session("TblSretView")
                gvListSret.DataBind()
            End If
            mpeListSret.Show()
        Else
            Session("WarningListSret") = "Please show some Sales Return data first!"
            showMessage(Session("WarningListSret"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedSret_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedSret.Click
        If Session("TblSret") Is Nothing Then
            Session("WarningListSret") = "Selected Sales Return data can't be found!"
            showMessage(Session("WarningListSret"), 2)
            Exit Sub
        End If
        If UpdateCheckedSret() Then
            Dim dtTbl As DataTable = Session("TblSret")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListSret.SelectedIndex = -1 : txtFilterListSret.Text = ""
                Session("TblSretView") = dtView.ToTable
                gvListSret.DataSource = Session("TblSretView")
                gvListSret.DataBind()
                dtView.RowFilter = ""
                mpeListSret.Show()
            Else
                dtView.RowFilter = ""
                Session("TblSretView") = Nothing
                gvListSret.DataSource = Session("TblSretView")
                gvListSret.DataBind()
                Session("WarningListSret") = "Selected Sales Return data can't be found!"
                showMessage(Session("WarningListSret"), 2)
            End If
        Else
            mpeListSret.Show()
        End If
    End Sub

    Protected Sub gvListSret_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSret.PageIndexChanging
        If UpdateCheckedSret2() Then
            gvListSret.PageIndex = e.NewPageIndex
            gvListSret.DataSource = Session("TblSretView")
            gvListSret.DataBind()
        End If
        mpeListSret.Show()
    End Sub

    Protected Sub lkbAddToListListSret_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListSret.Click
        If Not Session("TblSret") Is Nothing Then
            If UpdateCheckedSret() Then
                Dim dtTbl As DataTable = Session("TblSret")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If sretno.Text <> "" Then
                            If DDLSretNo.SelectedValue = "Return No." Then
                                If dtView(C1)("sretno") <> "" Then
                                    sretno.Text &= ";" + vbCrLf + dtView(C1)("sretno")
                                End If
                            Else
                                sretno.Text &= ";" + vbCrLf + dtView(C1)("sretmstoid").ToString
                            End If
                        Else
                            If DDLSretNo.SelectedValue = "Return No." Then
                                If dtView(C1)("sretno") <> "" Then
                                    sretno.Text &= dtView(C1)("sretno")
                                End If
                            Else
                                sretno.Text &= dtView(C1)("sretmstoid").ToString
                            End If
                        End If
                    Next
                    dtView.RowFilter = ""
                    DDLBusUnit.Enabled = False
                    cProc.SetModalPopUpExtender(btnHiddenListSret, PanelListSret, mpeListSret, False)
                Else
                    Session("WarningListSret") = "Please select Sales Return to add to list!"
                    showMessage(Session("WarningListSret"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListSret") = "Please show some Sales Return data first!"
            showMessage(Session("WarningListSret"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListSret_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListSret.Click
        cProc.SetModalPopUpExtender(btnHiddenListSret, PanelListSret, mpeListSret, False)
    End Sub

    Protected Sub FilterDDLCat01_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat01.SelectedIndexChanged
        InitFilterDDLCat2()
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat02_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat02.SelectedIndexChanged
        InitFilterDDLCat3()
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat03_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat03.SelectedIndexChanged
        InitFilterDDLCat4()
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat04_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat04.SelectedIndexChanged
        mpeListMat.Show()
    End Sub

    Protected Sub imbFindMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindMat.Click
        If IsValidPeriod() Then
            FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
            cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
            Session("TblMat") = Nothing : Session("TblMatView") = Nothing : gvListMat.DataSource = Nothing : gvListMat.DataBind() : tbData.Text = "5" : gvListMat.PageSize = CInt(tbData.Text)
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbEraseMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseMat.Click
        matcode.Text = ""
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        If cbCat01.Checked Then
            If FilterDDLCat01.SelectedValue <> "" Then
                sPlus &= " AND SUBSTRING(matcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'"
            Else
                cbCat01.Checked = False
            End If
        End If
        If cbCat02.Checked Then
            If FilterDDLCat02.SelectedValue <> "" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND SUBSTRING(matcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'") & " AND SUBSTRING(matcode, 4, 3)='" & FilterDDLCat02.SelectedValue & "'"
            Else
                cbCat02.Checked = False
            End If
        End If
        If cbCat03.Checked Then
            If FilterDDLCat03.SelectedValue <> "" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND SUBSTRING(matcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'") & IIf(cbCat02.Checked, "", " AND SUBSTRING(matcode, 4, 3)='" & FilterDDLCat02.SelectedValue & "'") & " AND SUBSTRING(matcode, 8, 4)='" & FilterDDLCat03.SelectedValue & "'"
            Else
                cbCat03.Checked = False
            End If
        End If
        If cbCat04.Checked Then
            If FilterDDLCat04.SelectedValue <> "" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND SUBSTRING(matcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'") & IIf(cbCat02.Checked, "", " AND SUBSTRING(matcode, 4, 3)='" & FilterDDLCat02.SelectedValue & "'") & IIf(cbCat03.Checked, "", " AND SUBSTRING(matcode, 8, 4)='" & FilterDDLCat03.SelectedValue & "'") & " AND SUBSTRING(matcode, 13, 4)='" & FilterDDLCat04.SelectedValue & "'"
            Else
                cbCat04.Checked = False
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        InitFilterDDLCat1()
        cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If matcode.Text <> "" Then
                            matcode.Text &= ";" + vbCrLf + dtView(C1)("matcode")
                        Else
                            matcode.Text = dtView(C1)("matcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some Material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matoid=" & dtTbl.Rows(C1)("matoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matoid=" & dtTbl.Rows(C1)("matoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                InitFilterDDLCat1()
                cbCat01.Checked = False : cbCat02.Checked = False
                cbCat03.Checked = False : cbCat04.Checked = False
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Selected material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub lbShowData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbShowData.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Please find material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If tbData.Text = "" Then
            Session("WarningListMat") = "Please fill total data to be shown!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        Else
            If ToDouble(tbData.Text) <= 0 Then
                Session("WarningListMat") = "Total data to be shown must be more than 0!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat2() Then
            gvListMat.PageSize = Int(tbData.Text)
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmPIRetur.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        If Not report Is Nothing Then
            If report.IsLoaded Then
                report.Dispose()
                report.Close()
            End If
        End If
    End Sub

    Protected Sub TypeRetDDL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If TypeRetDDL.SelectedValue = "QL_trnbelimst" Then
            lblListShipment.Text = "List of Purchase Invoice"
            lblShipment.Text = "PI. No"
            DDLFilterListShipment.Items(0).Enabled = True
            DDLFilterListShipment.Items(1).Enabled = False
        Else
            DDLFilterListShipment.Items(0).Enabled = False
            DDLFilterListShipment.Items(1).Enabled = True
            lblListShipment.Text = "List of Material Receive"
            lblShipment.Text = "MR. No"
        End If
    End Sub
#End Region
End Class
