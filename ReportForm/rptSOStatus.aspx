<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptSOStatus.aspx.vb" Inherits="rptSOStatus" title="Laporan Order Pembelian" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias">
        <tr>
            <th align="left" class="header" valign="center" style="width: 975px;">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="21px" ForeColor="Maroon" Text=".: Laporan Status SO"></asp:Label></th>
        </tr>
    </table>
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
<TABLE><TBODY><TR><TD align=center>&nbsp;<TABLE><TBODY><TR><TD style="WIDTH: 103px; TEXT-ALIGN: right" align=right></TD><TD align=left colSpan=3><asp:RadioButton id="rbPO" runat="server" Text="Status PO" Visible="False" GroupName="rbStatus" Checked="True" AutoPostBack="True" __designer:wfdid="w62"></asp:RadioButton> <asp:RadioButton id="rbPOLPB" runat="server" Text="Status PO-Purchase DO" Visible="False" GroupName="rbStatus" AutoPostBack="True" __designer:wfdid="w63"></asp:RadioButton> <asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpText" Visible="False" AutoPostBack="True" __designer:wfdid="w64"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 103px; TEXT-ALIGN: right" id="tdTipeLap1" align=right runat="server"><asp:Label id="Label2" runat="server" Text="Tipe Laporan :" Visible="False" __designer:wfdid="w65" Width="100px"></asp:Label></TD><TD id="tdTipeLap2" align=left colSpan=3 runat="server"><asp:DropDownList id="type" runat="server" CssClass="inpText" Visible="False" __designer:wfdid="w66" Width="100px"><asp:ListItem>Summary</asp:ListItem>
<asp:ListItem>Detail</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 103px; TEXT-ALIGN: right" id="tdPeriod1" align=right runat="server" Visible="true"><asp:Label id="Label6" runat="server" Text="Periode : " __designer:wfdid="w67"></asp:Label></TD><TD id="tdperiod2" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="dateAwal" runat="server" CssClass="inpText" __designer:wfdid="w68" Width="75px"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w69"></asp:ImageButton>&nbsp; -&nbsp; <asp:TextBox id="dateAkhir" runat="server" CssClass="inpText" __designer:wfdid="w70" Width="75px"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w71"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" ForeColor="Red" __designer:wfdid="w72">(mm/dd/yyyy)</asp:Label><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w73" PopupButtonID="imageButton1" TargetControlID="dateAwal" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w74" PopupButtonID="imageButton2" TargetControlID="dateAkhir" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" __designer:wfdid="w75" TargetControlID="dateAwal" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" __designer:wfdid="w76" TargetControlID="dateAkhir" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 103px; TEXT-ALIGN: right" id="Td3" align=right runat="server" Visible="true"><asp:Label id="Label5" runat="server" Text="Customer :" __designer:wfdid="w77"></asp:Label></TD><TD id="Td4" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="custcode" runat="server" CssClass="inpText" __designer:wfdid="w78" Width="158px" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchCust" onclick="btnSearchCust_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w79"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearCust" onclick="btnClearCust_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w80"></asp:ImageButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 103px; TEXT-ALIGN: right" align=right><asp:DropDownList id="DDLSONo" runat="server" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w81" Width="100px" OnSelectedIndexChanged="DDLSONo_SelectedIndexChanged"><asp:ListItem>SO No.</asp:ListItem>
<asp:ListItem Value="Draft No.">Draft No.</asp:ListItem>
</asp:DropDownList></TD><TD align=left colSpan=3><asp:TextBox id="sono" runat="server" CssClass="inpText" __designer:wfdid="w82" Width="158px" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindSO" onclick="imbFindSO_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w83"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseSO" onclick="imbEraseSO_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w84"></asp:ImageButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 103px; TEXT-ALIGN: right" align=right><asp:Label id="Label3" runat="server" Text="Item / Barang :" __designer:wfdid="w85"></asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="matrawcode" runat="server" CssClass="inpText" __designer:wfdid="w86" Width="178px" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindMat" onclick="imbFindMat_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w87"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseMat" onclick="imbEraseMat_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w88"></asp:ImageButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 103px; TEXT-ALIGN: right" align=right>Taxable :</TD><TD align=left colSpan=3><asp:DropDownList id="taxable" runat="server" CssClass="inpText" __designer:wfdid="w89" Width="100px"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>TAX</asp:ListItem>
<asp:ListItem>NON TAX</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="merk" runat="server" CssClass="inpTextDisabled" Visible="False" __designer:wfdid="w90" Width="250px" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 103px; TEXT-ALIGN: right" align=right>Status&nbsp;Item SO&nbsp;:</TD><TD align=left colSpan=3><asp:DropDownList id="statusDelivery" runat="server" CssClass="inpText" __designer:wfdid="w91" Width="100px"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>COMPLETE</asp:ListItem>
<asp:ListItem>IN COMPLETE</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 103px; TEXT-ALIGN: right" align=right>Status SO :</TD><TD align=left colSpan=3><asp:DropDownList id="ddlstatusPO" runat="server" CssClass="inpText" __designer:wfdid="w92" Width="100px"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>In Approval</asp:ListItem>
<asp:ListItem>Approved</asp:ListItem>
<asp:ListItem>Cancel</asp:ListItem>
<asp:ListItem>Closed</asp:ListItem>
</asp:DropDownList> </TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 103px; TEXT-ALIGN: right" align=right><asp:CheckBox id="chkGroup" runat="server" Text="Group :" CssClass="Label" __designer:wfdid="w93"></asp:CheckBox></TD><TD align=left colSpan=3><asp:DropDownList id="FilterDDLGrup" runat="server" CssClass="inpText" __designer:wfdid="w94" Width="150px"></asp:DropDownList> <asp:Label id="labelEx" runat="server" __designer:wfdid="w95" Width="98px"></asp:Label></TD></TR></TBODY></TABLE></TD></TR><TR><TD style="WIDTH: 979px"></TD></TR><TR><TD style="WIDTH: 979px; HEIGHT: 32px" align=center><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w96"></asp:ImageButton> <asp:ImageButton id="btnPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w97"></asp:ImageButton> <asp:ImageButton id="ibexcel" onclick="ibexcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w98"></asp:ImageButton> <asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w99"></asp:ImageButton><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w100"><ProgressTemplate>
<STRONG><SPAN style="FONT-SIZE: 14pt; COLOR: #800080">Please Wait....</SPAN></STRONG><BR /><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w101"></asp:Image> 
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD style="WIDTH: 979px" align=center><asp:Label id="lblkonfirmasi" runat="server" ForeColor="Red" __designer:wfdid="w102"></asp:Label></TD></TR><TR><TD style="WIDTH: 979px" align=center><TABLE><TBODY><TR><TD align=center><CR:CrystalReportViewer id="crvMutasiStock" runat="server" __designer:wfdid="w103" Width="350px" HasToggleGroupTreeButton="False" HasSearchButton="False" HasRefreshButton="True" HasPrintButton="False" HasExportButton="False" HasCrystalLogo="False" DisplayGroupTree="False" AutoDataBind="true" ShowAllPageIds="True" Height="50px"></CR:CrystalReportViewer></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="btnViewReport"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnPdf"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>
    <br />
    <asp:UpdatePanel id="upListCust" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListCust" runat="server" CssClass="modalBox" Visible="False" Width="650px"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListCust" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Customer"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListCust" runat="server" Width="100%" DefaultButton="btnFindListCust">Filter : <asp:DropDownList id="DDLFilterListCust" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="custcode">Code</asp:ListItem>
<asp:ListItem Value="custname">Name</asp:ListItem>
<asp:ListItem Value="custaddr">Address</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListCust" runat="server" CssClass="inpText" Width="150px"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListCust" onclick="btnFindListCust_Click" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton><asp:ImageButton id="btnViewAllListCust" onclick="btnViewAllListCust_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3><asp:ImageButton id="btnSelectAllCust" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" OnClick="btnSelectAllCust_Click"></asp:ImageButton><asp:ImageButton id="btnSelectNoneCust" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" OnClick="btnSelectNoneCust_Click"></asp:ImageButton><asp:ImageButton id="btnViewCheckedCust" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" OnClick="btnViewCheckedCust_Click"></asp:ImageButton></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3>&nbsp;<asp:GridView id="gvListCust" runat="server" BorderColor="Black" ForeColor="#333333" Width="98%" PageSize="8" CellPadding="4" BorderStyle="None" DataKeyNames="custoid,custcode,custname" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="gvListCust_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:CheckBox id="cbLMCust" runat="server" ToolTip='<%# eval("custoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="custcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListCust" runat="server" OnClick="lkbAddToListListCust_Click">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbCloseListCust" runat="server" OnClick="lkbCloseListCust_Click">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListCust" runat="server" TargetControlID="btnHideListCust" PopupDragHandleControlID="lblListCust" PopupControlID="pnlListCust" BackgroundCssClass="modalBackground" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListCust" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <br />
    <asp:UpdatePanel id="upListSO" runat="server">
        <contenttemplate>
<asp:Panel id="PanelListSO" runat="server" CssClass="modalBox" Visible="False" Width="600px"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSO" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Sales Order"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListSO" runat="server" Width="100%" DefaultButton="btnFindListSO"><asp:Label id="Label24" runat="server" Text="Filter :"></asp:Label> <asp:DropDownList id="DDLFilterListSO" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="sono">SO No</asp:ListItem>
<asp:ListItem Value="sorawmstnote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListSO" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListSO" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" OnClick="btnFindListSO_Click"></asp:ImageButton> <asp:ImageButton id="btnViewAllListSO" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" OnClick="btnViewAllListSO_Click"></asp:ImageButton> </asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAllSO" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" OnClick="btnSelectAllSO_Click"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneSO" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" OnClick="btnSelectNoneSO_Click"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedSO" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" OnClick="btnViewCheckedSO_Click"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3>&nbsp;<asp:GridView id="gvListSO" runat="server" BorderColor="Black" ForeColor="#333333" Width="98%" PageSize="8" CellPadding="4" BorderStyle="None" DataKeyNames="somstoid,sono,sodate,sorawmststatus,sorawmstnote" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="gvListSO_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:CheckBox id="cbLMSO" runat="server" ToolTip='<%# eval("somstoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="somstoid" HeaderText="Draft No">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sono" HeaderText="SO No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sodate" HeaderText="SO Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sorawmstnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sorawmststatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
&nbsp;
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListSO" runat="server" OnClick="lkbAddToListListSO_Click">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbListSO" runat="server" OnClick="lkbListSO_Click">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSO" runat="server" TargetControlID="btnHiddenListSO" PopupDragHandleControlID="lblListSO" PopupControlID="PanelListSO" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListSO" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <br />
    <asp:UpdatePanel id="upListMat" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListMat" runat="server" CssClass="modalBox" Visible="False" Width="900px"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Material" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMat" runat="server" Width="100%" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="itemcode">Code</asp:ListItem>
<asp:ListItem Value="matrawlongdesc">Description</asp:ListItem>
<asp:ListItem Value="itemoldcode">Old Code</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" CssClass="inpText" Width="200px"></asp:TextBox> <asp:ImageButton id="btnFindListMat" onclick="btnFindListMat_Click" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" onclick="btnAllListMat_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:CheckBox id="cbCat01" runat="server" Text="Cat 1 :" Visible="False"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat01" runat="server" CssClass="inpText" AutoPostBack="True" Visible="False" Width="150px"></asp:DropDownList> <asp:CheckBox id="cbCat02" runat="server" Text="Cat 2 :" Visible="False"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat02" runat="server" CssClass="inpText" AutoPostBack="True" Width="150px"></asp:DropDownList> <asp:CheckBox id="cbCat03" runat="server" Text="Cat 3 :" Visible="False"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat03" runat="server" CssClass="inpText" AutoPostBack="True" Visible="False" Width="150px"></asp:DropDownList> <asp:CheckBox id="cbCat04" runat="server" Text="Cat 4 :" Visible="False"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat04" runat="server" CssClass="inpText" AutoPostBack="True" Visible="False" Width="150px"></asp:DropDownList></TD></TR></TBODY></TABLE></asp:Panel></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAll" onclick="btnSelectAll_Click" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNone" onclick="btnSelectNone_Click" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewChecked" onclick="btnViewChecked_Click" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:UpdateProgress id="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat"><ProgressTemplate>
<TABLE style="WIDTH: 200px"><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple">Load Data, Please Wait ...</TD></TR><TR><TD><asp:Image id="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif"></asp:Image></TD></TR></TABLE>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD class="Label" align=left colSpan=3>Show <asp:TextBox id="tbData" runat="server" CssClass="inpText" Width="25px"></asp:TextBox> Data Per Page <asp:LinkButton id="lbShowData" runat="server">>> View</asp:LinkButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat" runat="server" BorderColor="Black" ForeColor="#333333" Width="96%" OnPageIndexChanging="gvListMat_PageIndexChanging" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="itemCode" BorderStyle="None" CellPadding="4" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:CheckBox id="cbLM" runat="server" ToolTip='<%# eval("itemoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemCode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemoldcode" HeaderText="Old Code">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="matrawlongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
&nbsp;
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListMat" onclick="lkbAddToListListMat_Click" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbCloseListMat" onclick="lkbCloseListMat_Click" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE><ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlListMat" PopupDragHandleControlID="lblTitleListMat"></ajaxToolkit:ModalPopupExtender></asp:Panel> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <br />
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

