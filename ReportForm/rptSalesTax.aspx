<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptSalesTax.aspx.vb" Inherits="reportForm_rptSalesTaxReport" title="Untitled Page" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias">
        <tr>
            <th align="left" class="header" style="width: 975px" valign="center">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="False" Text=".: Report TAX"></asp:Label></th>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
<TABLE style="WIDTH: 533px"><TBODY><TR><TD style="TEXT-ALIGN: left" vAlign=top colSpan=1><asp:Label id="Periode" runat="server" Text="Periode" __designer:wfdid="w44"></asp:Label></TD><TD vAlign=top colSpan=1><asp:Label id="Label5" runat="server" Text=":" __designer:wfdid="w45" Width="10px"></asp:Label></TD><TD style="TEXT-ALIGN: left" vAlign=top align=left colSpan=2><asp:TextBox id="invdate1" runat="server" CssClass="inpText" __designer:wfdid="w46" Width="75px"></asp:TextBox>&nbsp;<asp:ImageButton id="ibcal1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w47"></asp:ImageButton>&nbsp;To <asp:TextBox id="invdate2" runat="server" CssClass="inpText" __designer:wfdid="w48" Width="75px"></asp:TextBox> <asp:ImageButton id="ibcal2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w49"></asp:ImageButton> <asp:Label id="Label3" runat="server" Text="(MM/dd/yyyy)" CssClass="Important" __designer:wfdid="w50"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: left" vAlign=top colSpan=1><asp:Label id="jenisinv" runat="server" Text="Type Pajak" __designer:wfdid="w51" Width="69px"></asp:Label></TD><TD vAlign=top colSpan=1><asp:Label id="Label4" runat="server" Text=":" __designer:wfdid="w52" Width="10px"></asp:Label></TD><TD vAlign=top align=left colSpan=2><asp:DropDownList id="DDLppn" runat="server" CssClass="inpText" __designer:wfdid="w53" Width="125px" AutoPostBack="True" OnSelectedIndexChanged="DDLppn_SelectedIndexChanged"><asp:ListItem Value="IN">Masukan</asp:ListItem>
<asp:ListItem Value="OUT">Keluaran</asp:ListItem>
<asp:ListItem Enabled="False" Value="ALL">Masukan/Keluaran</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="TEXT-ALIGN: left" vAlign=top colSpan=1><asp:Label id="Label1" runat="server" Text="Ref Type" __designer:wfdid="w54" Width="64px"></asp:Label></TD><TD vAlign=top colSpan=1><asp:Label id="Label2" runat="server" Text=":" __designer:wfdid="w55" Width="10px"></asp:Label></TD><TD vAlign=top align=left colSpan=2><asp:DropDownList id="DDLRefType" runat="server" CssClass="inpText" __designer:wfdid="w56" Width="80px"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="SI">S. Invoice</asp:ListItem>
<asp:ListItem Value="SR">S. Return</asp:ListItem>
<asp:ListItem Value="PI">P. Invoice</asp:ListItem>
<asp:ListItem Value="PR">P. Return</asp:ListItem>
<asp:ListItem Value="FAP">Fix Asset</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="TEXT-ALIGN: left" vAlign=top colSpan=1><asp:Label id="Nota" runat="server" Text="No. Ref" __designer:wfdid="w57"></asp:Label></TD><TD vAlign=top colSpan=1><asp:Label id="Label7" runat="server" Text=":" __designer:wfdid="w58" Width="10px"></asp:Label></TD><TD style="TEXT-ALIGN: left" vAlign=top colSpan=2><asp:TextBox id="FilterRef" runat="server" CssClass="inpText" __designer:wfdid="w59" Width="162px"></asp:TextBox>&nbsp;<asp:ImageButton id="imbsallist" onclick="imbsallist_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w60" Width="16px" Height="16px"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbsaldel" onclick="imbsaldel_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w61"></asp:ImageButton>&nbsp;<asp:Label id="NoReff" runat="server" __designer:wfdid="w62" Visible="False"></asp:Label>&nbsp;<BR /><asp:GridView id="GvRefNo" runat="server" __designer:wfdid="w63" Width="450px" OnSelectedIndexChanged="GvRefNo_SelectedIndexChanged" Visible="False" OnRowDataBound="GvRefNo_RowDataBound" GridLines="None" OnPageIndexChanging="GvRefNo_PageIndexChanging" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="Noref,oid,Void">
<PagerSettings Mode="NumericFirstLast"></PagerSettings>

<RowStyle CssClass="gvrow"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="Noref" HeaderText="No. Ref">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="name" HeaderText="Name">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="tgl" HeaderText="Date">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="typeTax" HeaderText="Tax Type" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<PagerStyle CssClass="gvfooter" Font-Bold="True"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow"></SelectedRowStyle>

<HeaderStyle BackColor="WhiteSmoke" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="TEXT-ALIGN: left" vAlign=top colSpan=1><asp:Label id="Label6" runat="server" Text="Supplier" __designer:wfdid="w64"></asp:Label> <asp:Label id="Label11" runat="server" Text="Cust.Name" __designer:wfdid="w65" Width="61px" Visible="False"></asp:Label></TD><TD vAlign=top colSpan=1><asp:Label id="Label9" runat="server" Text=":" __designer:wfdid="w66" Width="10px"></asp:Label></TD><TD vAlign=top align=left colSpan=2><asp:TextBox id="SuppFilter" runat="server" CssClass="inpText" __designer:wfdid="w67" Width="162px"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" onclick="imbso_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w68" Width="16px" Height="16px"></asp:ImageButton>&nbsp;<asp:ImageButton id="ImageButton2" onclick="imbsodel_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w69"></asp:ImageButton>&nbsp;<asp:Label id="suppoid" runat="server" __designer:wfdid="w70" Visible="False"></asp:Label> <asp:GridView id="GvSupp" runat="server" __designer:wfdid="w71" Width="450px" OnSelectedIndexChanged="gvSupp_SelectedIndexChanged" Visible="False" GridLines="None" OnPageIndexChanging="gvSupp_PageIndexChanging" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="oid,code,name,npwp">
<PagerSettings Mode="NumericFirstLast"></PagerSettings>

<RowStyle CssClass="gvrow"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="code" HeaderText="Code">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="name" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="npwp" HeaderText="Npwp">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="addres" HeaderText="Addres">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<PagerStyle CssClass="gvfooter" Font-Bold="True"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow"></SelectedRowStyle>

<HeaderStyle BackColor="WhiteSmoke" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="TEXT-ALIGN: left" id="TD2" vAlign=bottom colSpan=4 runat="server" Visible="true"><asp:ImageButton id="ViewReport" onclick="ViewReport_Click" runat="server" ImageUrl="~/Images/viewreport.png" __designer:wfdid="w72"></asp:ImageButton> <asp:ImageButton id="ExPdfBtn" onclick="ExPdfBtn_Click" runat="server" ImageUrl="~/Images/topdf.png" __designer:wfdid="w73"></asp:ImageButton> <asp:ImageButton id="ExpExcelBtn" onclick="ExpExcelBtn_Click" runat="server" ImageUrl="~/Images/toexcel.png" __designer:wfdid="w74"></asp:ImageButton>&nbsp;<asp:ImageButton id="ClearBn" onclick="ClearBn_Click" runat="server" ImageUrl="~/Images/clear.png" __designer:wfdid="w75"></asp:ImageButton></TD></TR><TR><TD style="TEXT-ALIGN: left" id="TD1" vAlign=bottom colSpan=4 runat="server" Visible="false"><asp:Button id="btnViewPrint" runat="server" ImageAlign="AbsMiddle" Text="ViewReport" CssClass="blue" __designer:wfdid="w76" Width="100" Visible="False"></asp:Button> <asp:Button id="btnExportToPdf" runat="server" ImageAlign="AbsMiddle" Text="Export To PDF" CssClass="red" __designer:wfdid="w77" Width="100" Visible="False"></asp:Button>&nbsp;<asp:Button id="ExportToXls" runat="server" Text="Export To Excel" CssClass="green" __designer:wfdid="w78" Width="106px" Visible="False"></asp:Button> <asp:Button id="ibClear" runat="server" ImageAlign="AbsMiddle" Text="Clear" CssClass="gray" __designer:wfdid="w79" Width="94px" UseSubmitBehavior="False"></asp:Button></TD></TR><TR><TD style="HEIGHT: 60px; TEXT-ALIGN: center" vAlign=top colSpan=4><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w80"><ProgressTemplate>
<asp:Image id="Image2" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w81"></asp:Image><BR />Loading report ... 
</ProgressTemplate>
</asp:UpdateProgress><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w82" PopupButtonID="ibcal1" Format="MM/dd/yyyy" TargetControlID="invdate1"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w83" PopupButtonID="ibcal2" Format="MM/dd/yyyy" TargetControlID="invdate2"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" __designer:wfdid="w84" TargetControlID="invdate1" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" __designer:wfdid="w85" TargetControlID="invdate2" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE><cr:crystalreportviewer id="CrystalReportViewer1" runat="server" __designer:wfdid="w86" autodatabind="true" DisplayGroupTree="False"></cr:crystalreportviewer> 
</ContentTemplate>
        <triggers>
<asp:PostBackTrigger ControlID="ExpExcelBtn"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ExPdfBtn"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnViewPrint"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ibClear"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>
	<asp:UpdatePanel id="upListSupp" runat="server">
		<contenttemplate>
<asp:Panel id="pnlListSupp" runat="server" CssClass="modalBox" Width="650px" Visible="False"><TABLE width="100%"><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSupp" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Supplier"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListSupp" runat="server" Width="100%" DefaultButton="btnFindListSupp">Filter : <asp:DropDownList id="FilterDDLListSupp" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="suppname">Name</asp:ListItem>
<asp:ListItem Value="suppcode">Code</asp:ListItem>
<asp:ListItem Value="suppaddr">Address</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListSupp" runat="server" CssClass="inpText" Width="150px"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListSupp" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListSupp" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListSupp" runat="server" ForeColor="#333333" Width="98%" GridLines="None" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="suppoid,suppcode,suppname,supppaymentoid,supptaxable,suppres3,supptype" AllowSorting="True" PageSize="5">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Code" SortExpression="suppcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Name" SortExpression="suppname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Width="40%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="40%"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListSupp" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSupp" runat="server" TargetControlID="btnHideListSupp" PopupControlID="pnlListSupp" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListSupp" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListSupp" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
	</asp:UpdatePanel>
	<asp:UpdatePanel id="upPopUpMsg" runat="server">
		<contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Width="700px" Visible="False"><TABLE style="WIDTH: 100%"><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 40px; TEXT-ALIGN: center"><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" DropShadow="True" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
	</asp:UpdatePanel>
</asp:Content>
