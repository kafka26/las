Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_OutgoingGiroReal
	Inherits System.Web.UI.Page

#Region "Variables"
	Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
	Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
	Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
	Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
	Dim xCmd As New SqlCommand("", conn)
	Dim xreader As SqlDataReader
	Dim sSql As String = ""
	Dim cKon As New Koneksi
	Dim cProc As New ClassProcedure
	Dim report As New ReportDocument
	Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
	Private Function IsValidPeriod() As Boolean
		Dim sErr As String = ""
		If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
			showMessage("Your Period 1 is invalid. " & sErr, 2)
			Return False
		End If
		If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
			showMessage("Your Period 2 is invalid. " & sErr, 2)
			Return False
		End If
		If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
			showMessage("Period 2 must be more than Period 1 !", 2)
			Return False
		End If
		Return True
	End Function

	Private Function FillDDLWithALL(ByRef oDDLObject As DropDownList, ByVal sSql As String) As Boolean
		' To fill data in dropdown list, return true when all data exist
		FillDDLWithALL = True
		oDDLObject.Items.Clear()
		oDDLObject.Items.Add("ALL")
		oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = "ALL"
		If conn.State = ConnectionState.Closed Then
			conn.Open()
		End If
		xCmd.CommandText = sSql
		xreader = xCmd.ExecuteReader
		While xreader.Read
			oDDLObject.Items.Add(xreader.GetValue(1))
			oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
		End While
		xreader.Close()
		conn.Close()
		' Cek if all data exits
		If oDDLObject.Items.Count = 0 Then
			FillDDLWithALL = False
		End If
		Return FillDDLWithALL
    End Function

    Private Sub UpdateCheckedListSupp()
        If Not Session("TblListSupp") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblListSupp")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                For C1 As Integer = 0 To gvListSupp.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSupp.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheckValue As String = "False"
                                If CType(myControl, System.Web.UI.WebControls.CheckBox).Checked Then
                                    cbCheckValue = "True"
                                End If
                                dtView.RowFilter = "suppoid=" & CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                If dtView.Count > 0 Then
                                    dtView(0)("checkvalue") = cbCheckValue
                                End If
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblListSupp") = dtTbl
            End If
        End If
        If Not Session("TblListSuppView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblListSuppView")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                For C1 As Integer = 0 To gvListSupp.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSupp.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheckValue As String = "False"
                                If CType(myControl, System.Web.UI.WebControls.CheckBox).Checked Then
                                    cbCheckValue = "True"
                                End If
                                dtView.RowFilter = "suppoid=" & CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                If dtView.Count > 0 Then
                                    dtView(0)("checkvalue") = cbCheckValue
                                End If
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblListSuppView") = dtTbl
            End If
        End If
    End Sub
#End Region

#Region "Procedures"
	Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
		Dim strCaption As String = CompnyName
		If iType = 1 Then ' Error
			imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
		ElseIf iType = 2 Then ' Warning
			imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
		ElseIf iType = 3 Then ' Information
			imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
		Else
			imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
		End If
		lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
		cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
	End Sub

	'Private Sub InitCBL()
	'    sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='VAR_BANK' AND interfaceres1='" & DDLBusUnit.SelectedValue & "'"
	'    sSql = GetStrData(sSql)
	'    sSql = "SELECT acctgcode, ('(' + acctgcode + ') ' + acctgdesc) AS acctgdesc FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' ORDER BY acctgcode "
	'    cbldata.Items.Clear()
	'    cbldata.Items.Add("All")
	'    cbldata.Items(cbldata.Items.Count - 1).Value = "All"
	'    If conn.State = ConnectionState.Closed Then
	'        conn.Open()
	'    End If
	'    xCmd = New SqlCommand(sSql, conn)
	'    xreader = xCmd.ExecuteReader
	'    While xreader.Read
	'        cbldata.Items.Add(xreader.GetValue(1).ToString)
	'        cbldata.Items(cbldata.Items.Count - 1).Value = xreader.GetValue(0).ToString
	'    End While
	'    xreader.Close()
	'    conn.Close()
	'    Session("LastAllCheck") = False
    'End Sub

    Private Sub BindListSupp()
        sSql = "SELECT DISTINCT 'False' AS checkvalue,suppoid AS suppoid, suppcode AS [Supplier Code], suppname AS [Supplier],suppaddr AS suppaddr FROM QL_mstsupp c WHERE suppoid IN (SELECT g.suppoid FROM QL_trnpayapgiro g INNER JOIN QL_trncashbankmst cb ON g.cashbankoid=cb.cashbankoid AND g.cmpcode=cb.cmpcode AND g.suppoid=c.suppoid AND c.cmpcode=g.cmpcode) AND c.cmpcode='" & CompnyCode & "' ORDER BY [Supplier Code]"
        Dim dtSupp As DataTable = cKon.ambiltabel(sSql, "QL_mstsupp")
        If dtSupp.Rows.Count > 0 Then
            Session("TblListSupp") = dtSupp
            gvListSupp.DataSource = Session("TblListSupp")
            gvListSupp.DataBind()
            cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
        Else
            showMessage("There is no Supplier data available for this time!", 2)
        End If
    End Sub

	Private Sub ShowReport(ByVal sType As String)
		Dim sWhere As String = ""
		Dim rptName As String = ""
		Dim Dtl As String = ""
		Dim Join As String = ""
        Try
            report.Load(Server.MapPath(folderReport & "rptGiroOutReal.rpt"))
            rptName = "GiroRealizationReport"

            If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
                If IsValidPeriod() Then
                    sWhere &= " WHERE cb.cashbankdate >= '" & FilterPeriod1.Text & " 00:00:00' AND cb.cashbankdate <= '" & FilterPeriod2.Text & " 23:59:59'"
                Else
                    Exit Sub
                End If
            End If
            sWhere &= " AND cb.cmpcode = '" & DDLBusUnit.SelectedValue & "' /*AND cb.cashbanktype='BGK'*/"
            If checkPagePermission("~\ReportForm\frmGiroOutReal.aspx", Session("SpecialAccess")) = False Then
                sWhere &= " AND cb.createuser='" & Session("UserID") & "'"
            End If
            If TextSupplier.Text <> "" Then
                Dim sFilter As String = ""
                Dim sSplit() As String = TextSupplier.Text.Split(";")
                For C1 As Integer = 0 To sSplit.Length - 1
                    If sSplit(C1) <> "" Then
                        sFilter &= "s.suppname LIKE '" & sSplit(C1) & "' OR "
                    End If
                Next
                If sFilter <> "" Then
                    sWhere &= " AND (" & Left(sFilter, sFilter.Length - 4) & ")"
                End If
            End If
            If cashbankno.Text.Trim <> "" Then
                If DDLCashBankNo.SelectedValue = "Cash/Bank No." Then
                    Dim sCashBankno() As String = Split(cashbankno.Text, ";")
                    sWhere &= " AND ("
                    For c1 As Integer = 0 To sCashBankno.Length - 1
                        sWhere &= " cb.cashbankno LIKE '%" & Tchar(sCashBankno(c1)) & "%'"
                        If c1 < sCashBankno.Length - 1 Then
                            sWhere &= " OR"
                        End If
                    Next
                    sWhere &= ")"
                Else
                    Dim sCashBankno() As String = Split(cashbankno.Text, ";")
                    sWhere &= " AND ("
                    For c1 As Integer = 0 To sCashBankno.Length - 1
                        sWhere &= " cbref.cashbankno LIKE '%" & Tchar(sCashBankno(c1)) & "%'"
                        If c1 < sCashBankno.Length - 1 Then
                            sWhere &= " OR"
                        End If
                    Next
                    sWhere &= ")"
                End If
            End If

            If cbCOA.Checked Then
                If ddlCOA.Items.Count > 0 Then
                    sWhere &= " AND cb.acctgoid = " & ddlCOA.SelectedValue
                End If
            End If

            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", Session("UserID"))
            cProc.SetDBLogonForReport(report)

            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, rptName)
                report.Close()
                report.Dispose()
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, rptName)
                report.Close()
                report.Dispose()
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
	End Sub

	Private Sub InitAllDDL()
		' DDL Business Unit
		sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
		If Session("CompnyCode") <> CompnyCode Then
			sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
		End If
		FillDDL(DDLBusUnit, sSql)

	End Sub

	Private Sub InitCOA()
		' Fill DDL Negotiating Bank
		FillDDLAcctg(ddlCOA, "VAR_BANK", DDLBusUnit.SelectedValue)
	End Sub
#End Region

#Region "Events"
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("UserID") = "" Then
			Response.Redirect("~/Other/login.aspx")
		End If
		Session.Timeout = 60
		If Request.QueryString("awal") = "true" Then
			' Simpan session ke variabel temporary supaya tidak hilang
			Dim userId As String = Session("UserID")
			Dim xsetAcc As DataTable = Session("SpecialAccess")
			Dim appLimit As Decimal = Session("ApprovalLimit")
			Dim xsetRole As DataTable = Session("Role")
			Dim cmpcode As String = Session("CompnyCode")
			' Clear all session
			Session.Clear()
			' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
			Session("UserID") = userId
			Session("SpecialAccess") = xsetAcc
			Session("ApprovalLimit") = appLimit
			Session("Role") = xsetRole
			Session("CompnyCode") = cmpcode
			' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
			Response.Redirect("~\ReportForm\frmGiroOutReal.aspx")

		End If

		If checkPagePermission("~\ReportForm\frmGiroOutReal.aspx", Session("Role")) = False Then
			Response.Redirect("~\Other\NotAuthorize.aspx")
		End If

		Page.Title = CompnyName & " - Outgoing Giro Realization Report"
		If Not Page.IsPostBack Then
			InitAllDDL()
			DDLBusUnit_SelectedIndexChanged(Nothing, Nothing)
			FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
			FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
		End If
	End Sub

	Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
		cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
	End Sub

	Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
		InitCOA()
	End Sub

	Protected Sub DDLCashBankNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLCashBankNo.SelectedIndexChanged
		cashbankno.Text = ""
	End Sub

	Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
		ShowReport("View")
	End Sub

	Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
		ShowReport("Print PDF")
	End Sub

	Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
		ShowReport("Print Excel")
	End Sub

	Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
		Response.Redirect("~\ReportForm\frmGiroOutReal.aspx?awal=true")
	End Sub

	Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
		ShowReport("View")
	End Sub

	Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
		ShowReport("View")
	End Sub

	Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
		ShowReport("View")
	End Sub

	Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
		If Not report Is Nothing Then
			If report.IsLoaded Then
				report.Dispose()
				report.Close()
			End If
		End If
    End Sub

    Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupp.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2) : Exit Sub
        End If
        FilterDDLListSupp.SelectedIndex = -1 : FilterTextListSupp.Text = "" : Session("TblListSupp") = Nothing : Session("TblListSuppView") = Nothing : gvListSupp.DataSource = Nothing : gvListSupp.DataBind()
        BindListSupp()
    End Sub

    Protected Sub lbAddToListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListSupp.Click
        UpdateCheckedListSupp()
        Dim dtTbl As DataTable = Session("TblListSupp")
        If dtTbl.Rows.Count > 0 Then
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            Dim iCheck As Integer = dtView.Count
            If dtView.Count > 0 Then
                For C1 As Integer = 0 To dtView.Count - 1
                    If TextSupplier.Text <> "" Then
                        TextSupplier.Text &= ";" + vbCrLf + dtView(C1)("Supplier").ToString
                    Else
                        TextSupplier.Text = dtView(C1)("Supplier").ToString
                    End If
                Next
                dtView.RowFilter = ""
                cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
            Else
                dtView.RowFilter = ""
                Session("WarningListSupp") = "Please select some Supplier data to add to list!"
                showMessage(Session("WarningListSupp"), 2) : Exit Sub
            End If
        Else
            Session("WarningListSupp") = "Please select some Supplier data to add to list!"
            showMessage(Session("WarningListSupp"), 2) : Exit Sub
        End If
    End Sub

    Protected Sub lbCloseListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListSupp.Click
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub btnClearSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        TextSupplier.Text = ""
    End Sub

    Protected Sub btnFindListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSupp.Click
        UpdateCheckedListSupp()
        Dim dt As DataTable = Session("TblListSupp")
        If dt.Rows.Count > 0 Then
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = FilterDDLListSupp.SelectedValue & " LIKE '%" & Tchar(FilterTextListSupp.Text) & "%'"
            If dv.Count > 0 Then
                Session("TblListSuppView") = dv.ToTable
                gvListSupp.DataSource = Session("TblListSuppView")
                gvListSupp.DataBind()
                dv.RowFilter = ""
                mpeListSupp.Show()
            Else
                dv.RowFilter = ""
                Session("WarningListSupp") = "Customer data can't be found!"
                showMessage(Session("WarningListSupp"), 2) : Exit Sub
            End If
        Else
            Session("WarningListSupp") = "Supplier data can't be found!"
            showMessage(Session("WarningListSupp"), 2) : Exit Sub
        End If
    End Sub

    Protected Sub btnAllListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListSupp.Click
        UpdateCheckedListSupp()
        FilterDDLListSupp.SelectedIndex = -1 : FilterTextListSupp.Text = ""
        Dim dt As DataTable = Session("TblListSupp")
        If dt.Rows.Count > 0 Then
            Session("TblListSuppView") = dt
            gvListSupp.DataSource = Session("TblListSuppView")
            gvListSupp.DataBind()
            mpeListSupp.Show()
        Else
            Session("WarningListSupp") = "Supplier data can't be found!"
            showMessage(Session("WarningListSupp"), 2) : Exit Sub
        End If
    End Sub

    Protected Sub gvListSupp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSupp.PageIndexChanging
        UpdateCheckedListSupp()
        gvListSupp.PageIndex = e.NewPageIndex
        gvListSupp.DataSource = Session("TblListSupp")
        gvListSupp.DataBind()
        mpeListSupp.Show()
    End Sub

#End Region
End Class
