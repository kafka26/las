Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_frmMat
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Const sTypeCat = "Raw"
    Const sTypeMat = "item"
    Const sRefName = "RAW MATERIAL"
    Const sTypeIS = "MIS"
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsReportFilterValid() As Boolean
        Dim sError As String = ""

        If sError <> "" Then
            showMessage(sError, 2) : Return False
        Else
            Return True
        End If
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        If IsReportFilterValid() Then
            Try
                Dim sRptName As String = "MaterialReport"
                Dim sRptFile As String = "rptPriceList.rpt"
                report.Load(Server.MapPath(folderReport & sRptFile))

                ' Use Outside Data Source
                Dim dtReport As DataTable = Nothing
                sSql = "select c1.cat1shortdesc, br.gendesc merek, itemCode, itemLongDescription, itemPriceList harga_agen, itemPriceList + (itemPriceList * 25.00/100.00) harga_ecer, stok, age stok_1_30, case when age between 31 and 60 then stok else 0.0 end stok_31_60, case when age between 61 and 90 then stok else 0.0 end stok_61_90, case when age between 91 and 120 then stok else 0.0 end stok_91_120, case when age > 120 then stok else 0.0 end lebih_120, g.gendesc satuan from QL_mstitem i inner join QL_mstcat1 c1 on c1.cat1oid=i.itemCat1 inner join QL_mstgen g on g.genoid=i.itemUnit1 left join QL_mstgen br on br.genoid=i.itembrand cross apply ((select sum(stok) stok, sum(agesales) age from (select sum(qtyin - qtyout) stok, case formaction when 'QL_trnprodresconfirm' then datediff(day, (select top 1 j.trndate from QL_constock j where j.refoid=cs.refoid and formaction='QL_trnprodresconfirm' order by j.trndate),getdate()) when 'QL_trnmrdtl' then datediff(day, (select top 1 j.trndate from QL_constock j where j.refoid=cs.refoid and formaction='QL_trnmrdtl' order by j.trndate),getdate()) when 'Material Init Stock' then datediff(day, (select top 1 j.trndate from QL_constock j where j.refoid=cs.refoid and formaction='Material Init Stock' order by j.trndate),getdate()) else 0 end agesales, datediff(day,isnull((select top 1 j.trndate from QL_constock j where j.refoid=cs.refoid and formaction='QL_trndodtl' order by j.trndate desc),getdate()),getdate()) agepurchase from QL_constock cs where refoid=itemoid and mtrlocoid=147 group by refoid, formaction) yy)) st where i.itemGroup='fg' and i.itemRecordStatus='active' and stok>0"

                'Filter Text
                sSql &= " and 1=1 "
                Dim sWhereCode As String = ""
                If FilterMaterial.Text <> "" Then
                    Dim sMatcode() As String = Split(FilterMaterial.Text, ";")
                    If sMatcode.Length > 0 Then
                        For C1 As Integer = 0 To sMatcode.Length - 1
                            If sMatcode(C1) <> "" Then
                                sWhereCode &= "itemCode LIKE '" & Tchar(sMatcode(C1)) & "' OR "
                            End If
                        Next
                        If sWhereCode <> "" Then
                            sWhereCode = Left(sWhereCode, sWhereCode.Length - 4)
                            sSql &= " AND (" & sWhereCode & ")"
                        End If
                    End If
                End If
                If DDLStatus.SelectedValue <> "ALL" Then
                    sSql &= "AND itemrecordstatus='" & DDLStatus.SelectedValue & "'"
                End If
                If cbCat.Checked Then
                    sSql &= " and c1.cat1oid=" & DDLCat.SelectedValue
                End If
                sSql &= " order by c1.cat1shortdesc, age desc, merek, itemCode"
                dtReport = cKon.ambiltabel(sSql, "tbl_Report")
                report.SetDataSource(dtReport)
                cProc.SetDBLogonForReport(report)

                If sType = "View" Then
                    crvReportForm.DisplayGroupTree = False
                    crvReportForm.ReportSource = report
                    crvReportForm.SeparatePages = True
                ElseIf sType = "Print PDF" Then
                    Response.Buffer = False
                    Response.ClearContent()
                    Response.ClearHeaders()
                    report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, sRptName)
                    report.Close()
                    report.Dispose()
                Else
                    Response.Buffer = False
                    Response.ClearContent()
                    Response.ClearHeaders()
                    report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, sRptName)
                    report.Close()
                    report.Dispose()
                End If
            Catch ex As Exception
                report.Close()
                report.Dispose()
                showMessage(ex.Message, 1)
            End Try
        End If
    End Sub

    Private Sub InitAllDDL()
        sSql = "select cat1oid, cat1code + ' - ' + cat1shortdesc cat1shortdesc from QL_mstcat1 where cat1res1='FG' order by cat1code"
        FillDDL(DDLCat, sSql)
    End Sub

    Private Sub InitDDLCat1()
        'Fill DDL Category 1
        Dim sGroup As String = "RAW"
        If DDLTypeMat.SelectedValue = "GENERAL MATERIAL" Then
            sGroup = "GEN"
        ElseIf DDLTypeMat.SelectedValue = "FINISH GOOD" Then
            sGroup = "FG"
        ElseIf DDLTypeMat.SelectedValue = "WIP" Then
            sGroup = "WIP"
        End If

        sSql = "SELECT cat1oid, cat1code + ' - ' + cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat1res1='" & sGroup & "' AND ISNULL(cat1res2, '') IN ('WIP', 'Non WIP', '') ORDER BY cat1code"
        If FillDDL(DDLCat01, sSql) Then
            InitDDLCat2()
        Else
            DDLCat02.Items.Clear()
            DDLCat03.Items.Clear()
            DDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat2()
        'Fill DDL Category 2
        Dim sGroup As String = "RAW"
        If DDLTypeMat.SelectedValue = "GENERAL MATERIAL" Then
            sGroup = "GEN"
        ElseIf DDLTypeMat.SelectedValue = "FINISH GOOD" Then
            sGroup = "FG"
        ElseIf DDLTypeMat.SelectedValue = "WIP" Then
            sGroup = "WIP"
        End If

        sSql = "SELECT cat2oid, cat2code + ' - ' + cat2shortdesc FROM QL_mstcat2 c2 INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c1.cat1oid=c2.cat1oid WHERE c2.cmpcode='" & CompnyCode & "' AND c2.activeflag='ACTIVE' AND c1.cat1oid='" & DDLCat01.SelectedValue & "' AND cat2res1='" & sGroup & "' AND cat1res1='" & sGroup & "' AND ISNULL(cat1res2, '') IN ('WIP', 'Non WIP', '') ORDER BY cat2code"
        If FillDDL(DDLCat02, sSql) Then
            InitDDLCat3()
        Else
            DDLCat03.Items.Clear()
            DDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat3()
        'Fill DDL Category 3
        Dim sGroup As String = "RAW"
        If DDLTypeMat.SelectedValue = "GENERAL MATERIAL" Then
            sGroup = "GEN"
        ElseIf DDLTypeMat.SelectedValue = "FINISH GOOD" Then
            sGroup = "FG"
        ElseIf DDLTypeMat.SelectedValue = "WIP" Then
            sGroup = "WIP"
        End If

        sSql = "SELECT cat3oid, cat3code + ' - ' + cat3shortdesc FROM QL_mstcat3 c3 INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c3.cmpcode AND c1.cat1oid=c3.cat1oid WHERE c3.cmpcode='" & CompnyCode & "' AND c3.activeflag='ACTIVE' AND c2.cat2oid='" & DDLCat02.SelectedValue & "' AND cat3res1='" & sGroup & "' AND cat2res1='" & sGroup & "' AND cat1res1='" & sGroup & "' AND ISNULL(cat1res2, '') IN ('WIP', 'Non WIP', '') ORDER BY cat3code"
        If FillDDL(DDLCat03, sSql) Then
            InitDDLCat4()
        Else
            DDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat4()
        'Fill DDL Category 3
        sSql = "SELECT genoid, gencode+' - '+gendesc FROM QL_mstgen WHERE gengroup='CAT4' AND activeflag='ACTIVE' ORDER BY gencode"
        FillDDL(DDLCat04, sSql)
    End Sub

    Private Sub BindListMat()
        Dim sGroup As String = "RAW"
        If DDLTypeMat.SelectedValue = "GENERAL MATERIAL" Then
            sGroup = "GEN"
        ElseIf DDLTypeMat.SelectedValue = "FINISH GOOD" Then
            sGroup = "FG"
        ElseIf DDLTypeMat.SelectedValue = "WIP" Then
            sGroup = "WIP"
        End If

        sSql = "SELECT " & sTypeMat & "oid AS matoid, " & sTypeMat & "code AS matcode, " & sTypeMat & "longdescription AS matlongdesc, itemoldcode AS oldcode, gendesc AS matunit, 'False' AS checkvalue, itemcat1 AS cat1, itemcat2 AS cat2, itemcat3 AS cat3, itemcat4 AS cat4 FROM QL_mst" & sTypeMat & " m INNER JOIN QL_mstgen g ON genoid=" & sTypeMat & "unit1 WHERE m.cmpcode='" & CompnyCode & "' AND m.itemrecordstatus='ACTIVE' AND m.itemgroup='FG' "
        If cbCat.Checked Then
            sSql &= " and itemcat1=" & DDLCat.SelectedValue
        End If
        sSql &= " ORDER BY " & sTypeMat & "code"
        Session("TblListMat") = cKon.ambiltabel(sSql, "QL_mstmat")
        Session("TblListMatView") = Session("TblListMat")
        gvListMat.DataSource = Session("TblListMatView")
        gvListMat.DataBind()
    End Sub

    Private Sub UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMat") = dt
        End If
        If Session("TblListMatView") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMatView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMatView") = dt
        End If
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmMat.aspx")
        End If
        If checkPagePermission("~\ReportForm\frmMat.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Material Report"
        If Not Page.IsPostBack Then
            InitAllDDL()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                mpeListMat.Show()
            End If
        End If
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmMat.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not report Is Nothing Then
                If report.IsLoaded Then
                    report.Dispose()
                    report.Close()
                End If
            End If
        Catch ex As Exception
            report.Dispose()
            report.Close()
        End Try
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" 'InitDDLCat1()
        cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
        Session("TblListMat") = Nothing : Session("TblListMatView") = Nothing : gvListMat.DataSource = Session("TblListMat") : gvListMat.DataBind()
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Protected Sub btnClearMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterMaterial.Text = ""
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("TblListMat") Is Nothing Then
            BindListMat()
        End If
        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                Dim dv As DataView = dt.DefaultView
                Dim sFilter As String = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
                If cbCat04.Checked And DDLCat04.SelectedValue <> "" Then
                    sFilter &= " AND cat4='" & DDLCat04.SelectedValue & "'"
                Else
                    If cbCat03.Checked And DDLCat03.SelectedValue <> "" Then
                        sFilter &= " AND cat3='" & DDLCat03.SelectedValue & "'"
                    Else
                        If cbCat02.Checked And DDLCat02.SelectedValue <> "" Then
                            sFilter &= " AND cat2='" & DDLCat02.SelectedValue & "'"
                        Else
                            If cbCat01.Checked And DDLCat01.SelectedValue <> "" Then
                                sFilter &= " AND cat1='" & DDLCat01.SelectedValue & "'"
                            End If
                        End If
                    End If
                End If
                dv.RowFilter = sFilter
                Session("TblListMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblListMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("TblListMat") Is Nothing Then
            BindListMat()
        End If
        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : InitDDLCat1()
                cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
                Session("TblListMatView") = Session("TblListMat")
                gvListMat.DataSource = Session("TblListMatView")
                gvListMat.DataBind()
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub lbAddToListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "checkvalue='True'"
                If dv.Count > 0 Then
                    For C1 As Integer = 0 To dv.Count - 1
                        FilterMaterial.Text &= dv(C1)("matcode").ToString & ";"
                    Next
                    FilterMaterial.Text = Left(FilterMaterial.Text, FilterMaterial.Text.Length - 1)
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                End If
            Else
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub cbLM_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub cbHdrLM_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For C1 As Integer = 0 To gvListMat.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        mpeListMat.Show()
    End Sub

    Protected Sub DDLCat01_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitDDLCat2()
        mpeListMat.Show()
    End Sub

    Protected Sub DDLCat02_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitDDLCat3()
        mpeListMat.Show()
    End Sub

    Protected Sub DDLCat03_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'InitDDLCat4()
        'mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        UpdateCheckedMat()
        gvListMat.PageIndex = e.NewPageIndex
        gvListMat.DataSource = Session("TblListMatView")
        gvListMat.DataBind()
        mpeListMat.Show()
    End Sub

#End Region

End Class
