<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="frmStockAdj.aspx.vb" Inherits="ReportForm_StockAdjustment" title="" EnableEventValidation="false" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Stock Adjusment Report" CssClass="Title" ForeColor="SaddleBrown"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="center">
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:UpdatePanel id="upReportForm" runat="server"><contenttemplate>
<asp:Panel id="pnlReportForm" runat="server" DefaultButton="btnViewReport" Width="500px"><TABLE><TBODY><TR id="BusinessUnit" runat="server" visible="true"><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text="Business Unit"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpText" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:DropDownList id="DDLDate" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="adj.stockadjdate">Adj. Date</asp:ListItem>
<asp:ListItem Value="adj.approvaldatetime">Approve Date</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="70px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="CalPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> - <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="70px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="CalPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label4" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:DropDownList id="DDLStockAdj" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="adj.stockadjno">Adj. No.</asp:ListItem>
<asp:ListItem Value="adj.resfield1">Draft No.</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="FilterStockAdj" runat="server" CssClass="inpText" Width="255px" TextMode="MultiLine" Rows="3"></asp:TextBox> <asp:ImageButton id="btnSearchStockAdj" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClearStockAdj" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left rowSpan=2><asp:Label id="lblMat" runat="server" Text="Code / No."></asp:Label></TD><TD class="Label" align=center rowSpan=2><asp:Label id="septMat" runat="server" Text=":"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="DDLMatType" runat="server" CssClass="inpText" Width="105px"><asp:ListItem Value="FG">Finish Good</asp:ListItem>
<asp:ListItem Value="RM">Raw Material</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:TextBox id="FilterMaterial" runat="server" CssClass="inpText" Width="255px" TextMode="MultiLine" Rows="3"></asp:TextBox> <asp:ImageButton id="btnSearchMat" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClearMat" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left rowSpan=2><asp:Label id="lblWH" runat="server" Text="Warehouse"></asp:Label></TD><TD class="Label" align=center rowSpan=2><asp:Label id="septWH" runat="server" Text=":"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="DDLWarehouse" runat="server" CssClass="inpText" Width="275px"></asp:DropDownList> <asp:ImageButton id="btnAddWH" runat="server" ImageUrl="~/Images/plus.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:ListBox id="lbWarehouse" runat="server" CssClass="inpTextDisabled" Width="275px" Rows="3"></asp:ListBox> <asp:ImageButton id="btnMinWH" runat="server" ImageUrl="~/Images/minus.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label9" runat="server" Text="Status"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:CheckBoxList id="cblStatus" runat="server" CssClass="inpText" Width="300px" RepeatColumns="3"><asp:ListItem>In Process</asp:ListItem>
        <asp:ListItem>In Approval</asp:ListItem>
        <asp:ListItem>Approved</asp:ListItem>
        <asp:ListItem>Rejected</asp:ListItem>
        <asp:ListItem>Revised</asp:ListItem>
</asp:CheckBoxList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblGroupBy" runat="server" Text="Group By"></asp:Label></TD><TD class="Label" align=center><asp:Label id="septGroupBy" runat="server" Text=":"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="DDLGroupBy" runat="server" CssClass="inpText" Width="105px" AutoPostBack="True"><asp:ListItem Value="adj.resfield1, adj.stockadjno">Draft &amp; Adj. No.</asp:ListItem>
<asp:ListItem Value="i.itemCode">Mat. Code</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblSortBy" runat="server" Text="Sort By"></asp:Label></TD><TD class="Label" align=center><asp:Label id="septSortBy" runat="server" Text=":"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="DDLSortBy" runat="server" CssClass="inpText" Width="105px"><asp:ListItem Value="adj.resfield1, adj.stockadjno">Draft &amp; Adj. No.</asp:ListItem>
<asp:ListItem Value=" i.itemCode ">Mat. Code</asp:ListItem>
<asp:ListItem Value="adj.stockadjdate">Adj. Date</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:DropDownList id="DDLOrder" runat="server" CssClass="inpText" Width="105px"><asp:ListItem Value="ASC">Ascending</asp:ListItem>
<asp:ListItem Value="DESC">Descending</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" PopupButtonID="CalPeriod1" Format="MM/dd/yyyy" TargetControlID="FilterPeriod1">
                                                                            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="FilterPeriod1" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear">
                                                                            </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" PopupButtonID="CalPeriod2" Format="MM/dd/yyyy" TargetControlID="FilterPeriod2">
                                                                            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" TargetControlID="FilterPeriod2" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear">
                                                                            </ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD class="Label" align=left colSpan=3><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnExportToPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnExportToExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="upReportForm"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></DIV></TD></TR></TBODY></TABLE></asp:Panel> <CR:CrystalReportViewer id="crvReportForm" runat="server" AutoDataBind="True" HasViewList="False" HasCrystalLogo="False" HasPrintButton="False" HasExportButton="False" HasToggleGroupTreeButton="False" HasDrillUpButton="False"></CR:CrystalReportViewer> 
</contenttemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="btnViewReport"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnExportToPdf"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnExportToExcel"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel>
                            &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center">
                <asp:UpdatePanel id="upListStockAdj" runat="server">
                    <contenttemplate>
<asp:Panel id="pnlListStockAdj" runat="server" CssClass="modalBox" DefaultButton="btnFindListStockAdj" Width="750px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListStockAdj" runat="server" Font-Size="Medium" Font-Bold="True" Font-Underline="False">List Of Stock Adjustment</asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListStockAdj" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="stockadjno">Adj. No.</asp:ListItem>
<asp:ListItem Value="resfield1">Draft No.</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListStockAdj" runat="server" CssClass="inpText" Width="200px"></asp:TextBox> <asp:ImageButton id="btnFindListStockAdj" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListStockAdj" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListStockAdj" runat="server" ForeColor="#333333" Width="99%" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="5">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><HeaderTemplate>
<asp:CheckBox id="cbHdrLMStockAdj" runat="server" AutoPostBack="True" OnCheckedChanged="cbHdrLMStockAdj_CheckedChanged"></asp:CheckBox> 
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="cbLMStockAdj" runat="server" ToolTip='<%# eval("resfield1") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="resfield1" HeaderText="Draft No.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stockadjno" HeaderText="Adj. No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stockadjdate" HeaderText="Adj. Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbAddToListStockAdj" runat="server">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lbSelectAllListStockAdj" runat="server">[ Select All To List ]</asp:LinkButton> <asp:LinkButton id="lbCloseListStockAdj" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListStockAdj" runat="server" TargetControlID="btnHideListStockAdj" PopupControlID="pnlListStockAdj" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListStockAdj" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListStockAdj" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel id="upListMat" runat="server">
                    <contenttemplate>
<asp:Panel id="pnlListMat" runat="server" CssClass="modalBox" Width="750px" DefaultButton="btnFindListMat" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListMat" runat="server" Font-Size="Medium" Font-Bold="True" Font-Underline="False">List Of Material</asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" CssClass="inpText" Width="100px">
                                                        <asp:ListItem Value="matcode">Code</asp:ListItem>
                                                        <asp:ListItem Value="matlongdesc">Description</asp:ListItem>
                                                    </asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" CssClass="inpText" Width="200px"></asp:TextBox> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat" runat="server" ForeColor="#333333" Width="99%" PageSize="5" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
                                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"  />
                                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333"  />
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="cbHdrLM" runat="server" AutoPostBack="True" OnCheckedChanged="cbHdrLM_CheckedChanged"  />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbLM" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("matoid") %>'  />
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center"  />
                                            <ItemStyle HorizontalAlign="Center"  />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="matcode" HeaderText="Code">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left"  />
                                            <ItemStyle HorizontalAlign="Left"  />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="matlongdesc" HeaderText="Description">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left"  />
                                            <ItemStyle HorizontalAlign="Left"  />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="matunit" HeaderText="Unit">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center"  />
                                            <ItemStyle HorizontalAlign="Center"  />
                                        </asp:BoundField>
                                    </Columns>
                                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"  />
                                    <PagerStyle CssClass="gvpopup" Font-Bold="True" ForeColor="White" HorizontalAlign="Right"  />
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"  />
                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"  />
                                    <AlternatingRowStyle BackColor="White"  />
                                </asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbAddToListMat" runat="server">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lbSelectAllListMat" runat="server">[ Select All To List ]</asp:LinkButton> <asp:LinkButton id="lbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" Drag="True" PopupDragHandleControlID="lblListMat" BackgroundCssClass="modalBackground" PopupControlID="pnlListMat">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel id="upPopUpMsg" runat="server">
                    <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>

