Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_frmPAYAR
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function ValidateParam() As String
        Dim sReturn As String = ""
        Dim sErrTemp As String = ""

        If Not IsValidDate(txtStart.Text, "MM/dd/yyyy", sErrTemp) Then sReturn &= "- Invalid Start Date format: " & sErrTemp & ".<BR>"
        If Not IsValidDate(txtFinish.Text, "MM/dd/yyyy", sErrTemp) Then sReturn &= "- Invalid End Date format: " & sErrTemp & ".<BR>"
        If IsValidDate(txtStart.Text, "MM/dd/yyyy", sErrTemp) And IsValidDate(txtFinish.Text, "MM/dd/yyyy", sErrTemp) Then
            If DateDiff(DateInterval.Day, CDate(txtStart.Text), CDate(txtFinish.Text)) < 0 Then
                sReturn &= "- End Date must be greater than Start Date.<BR>"
            End If
        End If

        Return sReturn
    End Function

    Private Function GetIndoMonth(ByVal sVal As String) As String
        Select Case sVal
            Case "01"
                Return "JANUARI"
            Case "02"
                Return "FEBRUARI"
            Case "03"
                Return "MARET"
            Case "04"
                Return "APRIL"
            Case "05"
                Return "MEI"
            Case "06"
                Return "JUNI"
            Case "07"
                Return "JULI"
            Case "08"
                Return "AGUSTUS"
            Case "09"
                Return "SEPTEMBER"
            Case "10"
                Return "OKTOBER"
            Case "11"
                Return "NOVEMBER"
            Case "12"
                Return "DESEMBER"
            Case Else
                Return "Unknown"
        End Select
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindSupplierData()
        If DDLCustomer.SelectedValue = "CUSTOMER" Then
            sSql = "SELECT 0 selected,custoid,custcode,custname,custaddr FROM QL_mstcust WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' /*AND (custcode LIKE '%" & Tchar(FilterTextSupplier.Text) & "%' OR custname LIKE '%" & Tchar(FilterTextSupplier.Text) & "%')*/ ORDER BY custname"
        Else
            sSql = "SELECT 0 selected,custgroupoid custoid,custgroupcode custcode,custgroupname custname,custgroupaddr custaddr FROM QL_mstcustgroup WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' /*AND (custgroupcode LIKE '%" & Tchar(FilterTextSupplier.Text) & "%' OR custgroupname LIKE '%" & Tchar(FilterTextSupplier.Text) & "%')*/ ORDER BY custgroupname"
        End If
        Dim dtSupp As DataTable = cKon.ambiltabel(sSql, "QL_mstcust")
        'gvSupplier.DataSource = dtSupp : gvSupplier.DataBind()
        Session("TblCust") = dtSupp
        gvSupplier.Visible = True
        gvSupplier.SelectedIndex = -1
    End Sub

    Private Function UpdateCheckedGV2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblCustView") Is Nothing Then
            Dim dtab As DataTable = Session("TblCust")
            Dim dtab2 As DataTable = Session("TblCustView")
            If dtab.Rows.Count > 0 Then
                Dim cb As System.Web.UI.WebControls.CheckBox
                Dim dView As DataView = dtab.DefaultView
                Dim dView2 As DataView = dtab2.DefaultView
                Dim drView As DataRowView
                Dim drView2 As DataRowView
                For i As Integer = 0 To gvSupplier.Rows.Count - 1
                    cb = gvSupplier.Rows(i).FindControl("chkSelect")
                    dView.RowFilter = "custoid = " & cb.ToolTip
                    dView2.RowFilter = "custoid = " & cb.ToolTip
                    drView = dView.Item(0)
                    drView2 = dView2.Item(0)
                    drView.BeginEdit()
                    drView2.BeginEdit()
                    If cb.Checked = True Then
                        drView("selected") = 1
                        drView2("selected") = 1
                    Else
                        drView("selected") = 0
                        drView2("selected") = 0
                    End If
                    drView.EndEdit()
                    drView2.EndEdit()
                    dView.RowFilter = ""
                    dView2.RowFilter = ""
                Next
                dtab.AcceptChanges()
                dtab2.AcceptChanges()
                bReturn = True
            End If
            Session("TblCust") = dtab
            Session("TblCustView") = dtab2
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedGV() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblCust") Is Nothing Then
            Dim dtab As DataTable = Session("TblCust")
            If dtab.Rows.Count > 0 Then
                Dim cb As System.Web.UI.WebControls.CheckBox
                Dim dView As DataView = dtab.DefaultView
                Dim drView As DataRowView
                For i As Integer = 0 To gvSupplier.Rows.Count - 1
                    cb = gvSupplier.Rows(i).FindControl("chkSelect")
                    dView.RowFilter = "custoid = " & cb.ToolTip
                    drView = dView.Item(0)
                    drView.BeginEdit()
                    If cb.Checked = True Then
                        drView("selected") = 1
                    Else
                        drView("selected") = 0
                    End If
                    drView.EndEdit()
                    dView.RowFilter = ""
                Next
                dtab.AcceptChanges()
                bReturn = True
            End If
            Session("TblCust") = dtab
        End If
        Return bReturn
    End Function

    Private Sub ShowReport(ByVal sType As String)
        Try
            Dim sSuppOid As String = "", sColCustOid As String = "custoid", sColCustOid2 As String = "s.custoid"
            If DDLCustomer.SelectedValue = "CUSTOMER GROUP" Then
                sColCustOid = "custgroupoid" : sColCustOid2 = "cg.custgroupoid"
            End If
            If rbSupplier.SelectedValue = "SELECT" Then
                Dim dtSupp As DataTable : dtSupp = Session("TblCust")
                Dim dvSupp As DataView = dtSupp.DefaultView
                dvSupp.RowFilter = "selected='1'"
                If Session("TblCust") Is Nothing Then
                    showMessage("Please Show Some Customer Data First!", 2)
                    Exit Sub
                Else
                    If dvSupp.Count <= 0 Then
                        showMessage("Please Select Customer!", 2)
                        Exit Sub
                    Else
                        For R1 As Integer = 0 To dvSupp.Count - 1
                            sSuppOid &= dvSupp(R1)("custoid").ToString & ","
                        Next
                    End If
                End If
            End If
            Dim swhere As String = "WHERE 1=1 " : Dim swheresupp As String = "" : Dim swheresupp2 As String = ""
            If sSuppOid <> "" Then swhere &= " AND " & sColCustOid & " IN (" & Left(sSuppOid, sSuppOid.Length - 1) & ")"
            If sSuppOid <> "" Then swheresupp = " WHERE  " & sColCustOid2 & " IN (" & Left(sSuppOid, sSuppOid.Length - 1) & ")"
            If sSuppOid <> "" Then swheresupp2 = " AND c.custoid IN (" & Left(sSuppOid, sSuppOid.Length - 1) & ")"
            If DDLGroupBy.SelectedValue = "SALES" And cbSales.Checked Then
                swhere &= " AND salescode='" & DDLSales.SelectedValue & "'"
            End If
            Dim sFilterDueDate As String = "('BB')"
            If cbDueDateGiro.Checked Then
                sFilterDueDate = "('BB', 'BG', 'BL')"
            End If
            Dim tgl_1 As Date = CDate(txtStart.Text.Trim)
            Dim tgl_2 As Date = CDate(txtFinish.Text.Trim)
            Dim periode As String = "between '" & tgl_1 & "' and '" & tgl_2 & "'"

            sSql = "DECLARE @cmpcode AS VARCHAR(10);DECLARE @periodacctg AS VARCHAR(10);DECLARE @currency AS VARCHAR(10);DECLARE @dateacuan AS DATETIME;SET @cmpcode='" & FilterDDLDiv.SelectedValue & "';SET @periodacctg='" & GetDateToPeriodAcctg(New Date(FilterDDLYear.SelectedValue, FilterDDLMonth.SelectedValue, 1)) & "';SET @dateacuan=CAST('" & Format(New Date(FilterDDLYear.SelectedValue, FilterDDLMonth.SelectedValue, Date.DaysInMonth(FilterDDLYear.SelectedValue, FilterDDLMonth.SelectedValue)), "MM/dd/yyyy") & " 23:59:59' AS DATETIME);SET @currency='" & FilterCurrency.SelectedValue & "';/*UNION ALL QUERY*/"
            If DDLGroupBy.SelectedValue = "Customer" Then
                sSql &= "/*UNION ALL QUERY*/" & _
                            "SELECT custoid, custname, SUM(saidr) saidr, SUM(sausd) sausd, SUM(beliidr) beliidr, " & _
                            "SUM(beliusd) beliusd, SUM(paymentidr) paymentidr, SUM(paymentusd) paymentusd, " & _
                            "SUM(ageblmjtidr) ageblmjtidr, SUM(ageblmjtusd) ageblmjtusd, " & _
                            "SUM(age1idr) age1idr, SUM(age1usd) age1usd, SUM(age2idr) age2idr, SUM(age2usd) age2usd, " & _
                            "SUM(age3idr) age3idr, SUM(age3usd) age3usd, @currency currency, @cmpcode cmpcode, " & _
                            "'" & Format(CDate(txtStart.Text), "dd MMMM yyyy") & " - " & Format(CDate(txtFinish.Text), "dd MMMM yyyy") & "' periodreport, '" & FilterDDLDiv.SelectedItem.Text.ToUpper & "' company " & _
                            "FROM (" & _
                            "/*SALDO AWAL PERIODE*/" & _
                            "SELECT custoid, custname, " & _
                            "(SUM(beliidr) - SUM(paymentidr)) saidr, " & _
                            "(SUM(beliusd) - SUM(paymentusd)) sausd, " & _
                            "0.0 beliidr, 0.0 beliusd, 0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, " & _
                            "0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd FROM (" & _
                            "SELECT custoid, custname, " & _
                            "SUM(ISNULL(beliidr, 0.0)) beliidr, SUM(ISNULL(beliusd, 0.0)) beliusd, " & _
                            "0.0 paymentidr, 0.0 paymentusd " & _
                            "FROM (" & _
                            "SELECT s.custoid, s.custname, " & _
                            "ISNULL(c.amttransidr, 0.0) beliidr, ISNULL(c.amttransusd, 0.0) beliusd " & _
                            "FROM QL_mstcust s " & _
                            "LEFT JOIN QL_conar c ON s.custoid=c.custoid " & _
                            "AND c.cmpcode=@cmpcode AND c.trnardate < '" & tgl_1 & "' AND c.conaroid>0 " & _
                            "AND payrefoid=0 AND trnarstatus='Post' " & _
                            "UNION ALL " & _
                            "SELECT s.custoid, s.custname, " & _
                            "ISNULL(c.amtbayaridr * -1, 0.0) beliidr, ISNULL(c.amtbayarusd * -1, 0.0) beliusd " & _
                            "FROM QL_mstcust s " & _
                            "LEFT JOIN QL_conar c ON s.custoid=c.custoid " & _
                            "AND c.cmpcode=@cmpcode AND c.paymentdate < '" & tgl_1 & "' AND trnarstatus='Post' AND trnartype IN ('DNAR', 'GCAR') " & _
                            ") AS tblBeli GROUP BY custoid, custname " & _
                            "UNION ALL " & _
                            "SELECT custoid, custname, 0.0 beliidr, 0.0 beliusd," & _
                            "SUM(paymentidr) paymentidr, SUM(paymentusd) paymentusd " & _
                            "FROM (" & _
                            "SELECT s.custoid, s.custname, " & _
                            "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd " & _
                            "FROM QL_mstcust s " & _
                            "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.custoid, con.cmpcode FROM QL_trncashbankmst cb INNER JOIN QL_trnpayar pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conar con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payaroid INNER JOIN QL_conar c ON c.cmpcode=con.cmpcode AND c.reftype=con.reftype AND c.refoid=con.refoid AND c.payrefoid=0 WHERE cb.cmpcode=@cmpcode AND ISNULL(payarres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnartype NOT IN ('CNAR', 'DNAR', 'SRET', 'GCAR') AND (CASE WHEN LEFT(cashbanktype, 2) IN " & sFilterDueDate & " THEN cashbankduedate ELSE cashbankdate END) < '" & tgl_1 & "' GROUP BY con.custoid, con.cmpcode) AS c ON c.custoid=s.custoid " & _
                            "UNION ALL " & _
                            "SELECT s.custoid, s.custname," & _
                            "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd " & _
                            "FROM QL_mstcust s " & _
                            "LEFT JOIN QL_conar c ON s.custoid=c.custoid " & _
                            "AND c.cmpcode=@cmpcode AND c.paymentdate < '" & tgl_1 & "' AND trnarstatus='Post' AND c.trnartype IN ('CNAR', 'SRET') " & _
                            ") AS tblpayment GROUP BY custoid, custname " & _
                            ") AS tblSaldoAwalPeriod GROUP BY custoid, custname " & _
                            "UNION ALL /*SALDO AWAL CUT OFF*/" & _
                            "SELECT s.custoid, s.custname, " & _
                            "(SUM(ISNULL(c.amttransidr, 0.0)) - SUM(ISNULL(c.amtbayaridr, 0.0))) AS saidr, " & _
                            "(SUM(ISNULL(c.amttransusd, 0.0)) - SUM(ISNULL(c.amtbayarusd, 0.0))) AS sausd, " & _
                            "0.0 beliidr, 0.0 beliusd, 0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, " & _
                            "0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd " & _
                            "FROM QL_mstcust s " & _
                            "LEFT JOIN QL_conar c ON s.custoid=c.custoid " & _
                            "AND c.cmpcode=@cmpcode AND c.conaroid<0 " & _
                            "GROUP BY s.custoid, s.custname " & _
                            "UNION ALL /*PENJUALAN*/" & _
                            "SELECT custoid, custname, 0.0 saidr, 0.0 sausd, " & _
                            "SUM(ISNULL(beliidr, 0.0)) beliidr, SUM(ISNULL(beliusd, 0.0)) beliusd, " & _
                            "0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, 0.0 age1idr, 0.0 age1usd, " & _
                            "0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd " & _
                            "FROM (" & _
                            "SELECT s.custoid, s.custname, " & _
                            "ISNULL(c.amttransidr, 0.0) beliidr, ISNULL(c.amttransusd, 0.0) beliusd " & _
                            "FROM QL_mstcust s " & _
                            "LEFT JOIN QL_conar c ON s.custoid=c.custoid " & _
                            "AND c.cmpcode=@cmpcode AND c.trnardate " & periode & " AND c.conaroid>0 " & _
                            "AND payrefoid=0 AND trnarstatus='Post' " & _
                            "UNION ALL " & _
                            "SELECT s.custoid, s.custname, " & _
                            "ISNULL(c.amtbayaridr * -1, 0.0) beliidr, ISNULL(c.amtbayarusd * -1, 0.0) beliusd " & _
                            "FROM QL_mstcust s " & _
                            "LEFT JOIN QL_conar c ON s.custoid=c.custoid " & _
                            "AND c.cmpcode=@cmpcode AND c.paymentdate " & periode & " AND trnarstatus='Post' AND trnartype IN ('DNAR', 'GCAR') " & _
                            ") AS tblBeli GROUP BY custoid, custname " & _
                            "UNION ALL /*PAYMENT*/" & _
                            "SELECT custoid, custname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd," & _
                            "SUM(paymentidr) paymentidr, SUM(paymentusd) paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, " & _
                            "0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd " & _
                            "FROM (" & _
                            "SELECT s.custoid, s.custname, " & _
                            "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd " & _
                            "FROM QL_mstcust s " & _
                            "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.custoid, con.cmpcode FROM QL_trncashbankmst cb INNER JOIN QL_trnpayar pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conar con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payaroid INNER JOIN QL_conar c ON c.cmpcode=con.cmpcode AND c.reftype=con.reftype AND c.refoid=con.refoid AND c.payrefoid=0 WHERE cb.cmpcode=@cmpcode AND ISNULL(payarres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnartype NOT IN ('CNAR', 'DNAR', 'SRET', 'GCAR') AND (CASE WHEN LEFT(cashbanktype, 2) IN " & sFilterDueDate & " THEN cashbankduedate ELSE cashbankdate END) " & periode & " GROUP BY con.custoid, con.cmpcode) AS c ON c.custoid=s.custoid " & _
                            "UNION ALL " & _
                            "SELECT s.custoid, s.custname," & _
                            "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd " & _
                            "FROM QL_mstcust s " & _
                            "LEFT JOIN QL_conar c ON s.custoid=c.custoid " & _
                            "AND c.cmpcode=@cmpcode AND c.paymentdate " & periode & " AND trnarstatus='Post' AND c.trnartype IN ('CNAR', 'SRET') " & _
                            ") AS tblpayment GROUP BY custoid, custname " & _
                            "UNION ALL /*BELUM JATUH TEMPO*/" & _
                            "SELECT ag.custoid, ag.custname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, " & _
                            "0.0 paymentidr, 0.0 paymentusd, " & _
                            "SUM(ag.amttransidr - ag.amtbayaridr) ageblmjtidr, SUM(ag.amttransusd - ag.amtbayarusd) ageblmjtusd, " & _
                            "0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd " & _
                            "FROM (" & _
                            "SELECT s.custoid, s.custname, ISNULL(c.reftype, '') reftype, ISNULL(c.refoid, 0) refoid, " & _
                            "ISNULL(c.amttransidr, 0.0) amttransidr, SUM(ISNULL(c1.amtbayaridr, 0.0)) amtbayaridr, " & _
                            "ISNULL(c.amttransusd, 0.0) amttransusd, SUM(ISNULL(c1.amtbayarusd, 0.0)) amtbayarusd " & _
                            "FROM QL_mstcust s " & _
                            "LEFT JOIN QL_conar c ON s.custoid=c.custoid AND c.cmpcode=@cmpcode " & _
                            "AND datediff(d, c.payduedate, '" & tgl_2 & "') < 0 AND c.payrefoid=0 " & _
                            "AND c.trnardate <= '" & tgl_2 & "' " & _
                            "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.custoid, con.cmpcode, con.reftype, con.refoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayar pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conar con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payaroid WHERE cb.cmpcode=@cmpcode AND ISNULL(payarres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnartype NOT IN ('CNAR', 'DNAR', 'GCAR', 'SRET') AND (CASE WHEN LEFT(cashbanktype, 2) IN " & sFilterDueDate & " THEN cashbankduedate ELSE cashbankdate END) <= '" & tgl_2 & "' GROUP BY con.custoid, con.cmpcode, con.reftype, con.refoid UNION ALL SELECT SUM(amtbayaridr) amtbayaridr, SUM(amtbayarusd) amtbayarusd, con.custoid, con.cmpcode, con.reftype, con.refoid FROM QL_conar con WHERE con.cmpcode=@cmpcode AND trnarstatus='post' AND trnartype IN ('CNAR', 'DNAR', 'SRET', 'GCAR') AND con.paymentdate <= '" & tgl_2 & "' GROUP BY con.custoid, con.cmpcode, con.reftype, con.refoid) c1 ON c.cmpcode=c1.cmpcode AND c.custoid=c1.custoid AND c.reftype=c1.reftype AND c.refoid=c1.refoid " & _
                            "GROUP BY s.custoid, s.custname, c.amttransidr, c.amttransusd, c.reftype, c.refoid " & _
                            ") AS ag GROUP BY ag.custoid, ag.custname " & _
                            "UNION ALL /*0-30 DAYS*/" & _
                            "SELECT ag.custoid, ag.custname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, " & _
                            "0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, " & _
                            "SUM(ag.amttransidr - ag.amtbayaridr) age1idr, SUM(ag.amttransusd - ag.amtbayarusd) age1usd, " & _
                            "0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd " & _
                            "FROM (" & _
                            "SELECT s.custoid, s.custname, ISNULL(c.reftype, '') reftype, ISNULL(c.refoid, 0) refoid, " & _
                            "ISNULL(c.amttransidr, 0.0) amttransidr, SUM(ISNULL(c1.amtbayaridr, 0.0)) amtbayaridr, " & _
                            "ISNULL(c.amttransusd, 0.0) amttransusd, SUM(ISNULL(c1.amtbayarusd, 0.0)) amtbayarusd " & _
                            "FROM QL_mstcust s " & _
                            "LEFT JOIN QL_conar c ON s.custoid=c.custoid AND c.cmpcode=@cmpcode " & _
                            "AND datediff(d, c.payduedate, '" & tgl_2 & "') between 0 and 30 AND c.payrefoid=0 " & _
                            "AND c.trnardate <= '" & tgl_2 & "' " & _
                            "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.custoid, con.cmpcode, con.reftype, con.refoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayar pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conar con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payaroid WHERE cb.cmpcode=@cmpcode AND ISNULL(payarres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnartype NOT IN ('CNAR', 'DNAR', 'SRET', 'GCAR') AND (CASE WHEN LEFT(cashbanktype, 2) IN " & sFilterDueDate & " THEN cashbankduedate ELSE cashbankdate END)<='" & tgl_2 & "' GROUP BY con.custoid, con.cmpcode, con.reftype, con.refoid UNION ALL SELECT SUM(amtbayaridr) amtbayaridr, SUM(amtbayarusd) amtbayarusd, con.custoid, con.cmpcode, con.reftype, con.refoid FROM QL_conar con WHERE con.cmpcode=@cmpcode AND trnarstatus='post' AND trnartype IN ('CNAR', 'DNAR', 'SRET', 'GCAR') AND con.paymentdate<='" & tgl_2 & "' GROUP BY con.custoid, con.cmpcode, con.reftype, con.refoid) c1 ON c.cmpcode=c1.cmpcode AND c.custoid=c1.custoid AND c.reftype=c1.reftype AND c.refoid=c1.refoid " & _
                            "GROUP BY s.custoid, s.custname, c.amttransidr, c.amttransusd, c.reftype, c.refoid " & _
                            ") AS ag GROUP BY ag.custoid, ag.custname " & _
                            "UNION ALL /*31-60 DAYS*/" & _
                            "SELECT ag.custoid, ag.custname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, " & _
                            "0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, 0.0 age1idr, 0.0 age1usd, " & _
                            "SUM(ag.amttransidr - ag.amtbayaridr) age2idr, SUM(ag.amttransusd - ag.amtbayarusd) age2usd, " & _
                            "0.0 age3idr, 0.0 age3usd " & _
                            "FROM (" & _
                            "SELECT s.custoid, s.custname, ISNULL(c.reftype, '') reftype, ISNULL(c.refoid, 0) refoid, " & _
                            "ISNULL(c.amttransidr, 0.0) amttransidr, SUM(ISNULL(c1.amtbayaridr, 0.0)) amtbayaridr, " & _
                            "ISNULL(c.amttransusd, 0.0) amttransusd, SUM(ISNULL(c1.amtbayarusd, 0.0)) amtbayarusd " & _
                            "FROM QL_mstcust s " & _
                            "LEFT JOIN QL_conar c ON s.custoid=c.custoid AND c.cmpcode=@cmpcode " & _
                            "AND datediff(d, c.payduedate, '" & tgl_2 & "') between 31 and 60 AND c.payrefoid=0 " & _
                            "AND c.trnardate <= '" & tgl_2 & "' " & _
                            "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.custoid, con.cmpcode, con.reftype, con.refoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayar pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conar con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payaroid WHERE cb.cmpcode=@cmpcode AND ISNULL(payarres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnartype NOT IN ('CNAR', 'DNAR', 'SRET', 'GCAR') AND (CASE WHEN LEFT(cashbanktype, 2) IN " & sFilterDueDate & " THEN cashbankduedate ELSE cashbankdate END)<='" & tgl_2 & "' GROUP BY con.custoid, con.cmpcode, con.reftype, con.refoid UNION ALL SELECT SUM(amtbayaridr) amtbayaridr, SUM(amtbayarusd) amtbayarusd, con.custoid, con.cmpcode, con.reftype, con.refoid FROM QL_conar con WHERE con.cmpcode=@cmpcode AND trnarstatus='post' AND trnartype IN ('CNAR', 'DNAR', 'SRET', 'GCAR') AND con.paymentdate<='" & tgl_2 & "' GROUP BY con.custoid, con.cmpcode, con.reftype, con.refoid) c1 ON c.cmpcode=c1.cmpcode AND c.custoid=c1.custoid AND c.reftype=c1.reftype AND c.refoid=c1.refoid " & _
                            "GROUP BY s.custoid, s.custname, c.amttransidr, c.amttransusd, c.reftype, c.refoid " & _
                            ") AS ag GROUP BY ag.custoid, ag.custname " & _
                            "UNION ALL /*>60 DAYS*/" & _
                            "SELECT ag.custoid, ag.custname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, " & _
                            "0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, 0.0 age1idr, 0.0 age1usd, " & _
                            "0.0 age2idr, 0.0 age2usd, " & _
                            "SUM(ag.amttransidr - ag.amtbayaridr) age3idr, SUM(ag.amttransusd - ag.amtbayarusd) age3usd " & _
                            "FROM (" & _
                            "SELECT s.custoid, s.custname, ISNULL(c.reftype, '') reftype, ISNULL(c.refoid, 0) refoid, " & _
                            "ISNULL(c.amttransidr, 0.0) amttransidr, SUM(ISNULL(c1.amtbayaridr, 0.0)) amtbayaridr, " & _
                            "ISNULL(c.amttransusd, 0.0) amttransusd, SUM(ISNULL(c1.amtbayarusd, 0.0)) amtbayarusd " & _
                            "FROM QL_mstcust s " & _
                            "LEFT JOIN QL_conar c ON s.custoid=c.custoid AND c.cmpcode=@cmpcode " & _
                            "AND datediff(d, c.payduedate, '" & tgl_2 & "') > 60 AND c.payrefoid=0 " & _
                            "AND c.trnardate <= '" & tgl_2 & "' " & _
                            "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.custoid, con.cmpcode, con.reftype, con.refoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayar pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conar con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payaroid WHERE cb.cmpcode=@cmpcode AND ISNULL(payarres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnartype NOT IN ('CNAR', 'DNAR', 'SRET', 'GCAR') AND (CASE WHEN LEFT(cashbanktype, 2) IN " & sFilterDueDate & " THEN cashbankduedate ELSE cashbankdate END)<='" & tgl_2 & "' GROUP BY con.custoid, con.cmpcode, con.reftype, con.refoid UNION ALL SELECT SUM(amtbayaridr) amtbayaridr, SUM(amtbayarusd) amtbayarusd, con.custoid, con.cmpcode, con.reftype, con.refoid FROM QL_conar con WHERE con.cmpcode=@cmpcode AND trnarstatus='post' AND trnartype IN ('CNAR', 'DNAR', 'SRET', 'GCAR') AND con.paymentdate<='" & tgl_2 & "' GROUP BY con.custoid, con.cmpcode, con.reftype, con.refoid) c1 ON c.cmpcode=c1.cmpcode AND c.custoid=c1.custoid AND c.reftype=c1.reftype AND c.refoid=c1.refoid " & _
                            "GROUP BY s.custoid, s.custname, c.amttransidr, c.amttransusd, c.reftype, c.refoid " & _
                            ") AS ag GROUP BY ag.custoid, ag.custname " & _
                            ") AS tbldata " & _
                            swhere & _
                            "GROUP BY custoid, custname "
            ElseIf DDLGroupBy.SelectedValue = "SALES" Then
                sSql &= "SELECT custoid, custname, salescode, personname, SUM(saidr) saidr, SUM(sausd) sausd, SUM(beliidr) beliidr, SUM(beliusd) beliusd, SUM(paymentidr) paymentidr, SUM(paymentusd) paymentusd, SUM(ageblmjtidr) ageblmjtidr, SUM(ageblmjtusd) ageblmjtusd, SUM(age1idr) age1idr, SUM(age1usd) age1usd, SUM(age2idr) age2idr, SUM(age2usd) age2usd, SUM(age3idr) age3idr, SUM(age3usd) age3usd, @currency currency, @cmpcode cmpcode, '" & Format(CDate(txtStart.Text), "dd MMMM yyyy") & " - " & Format(CDate(txtFinish.Text), "dd MMMM yyyy") & "' periodreport, '" & FilterDDLDiv.SelectedItem.Text.ToUpper & "' company FROM (/*SALDO AWAL PERIODE*/ "
                sSql &= "SELECT custoid, custname, salescode, personname, (SUM(beliidr) - SUM(paymentidr)) saidr, (SUM(beliusd) - SUM(paymentusd)) sausd, 0.0 beliidr, 0.0 beliusd, 0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, 0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd FROM (SELECT custoid, custname, salescode, personname, SUM(ISNULL(beliidr, 0.0)) beliidr, SUM(ISNULL(beliusd, 0.0)) beliusd, 0.0 paymentidr, 0.0 paymentusd FROM (SELECT s.custoid, s.custname, p.salescode, personname, ISNULL(c.amttransidr, 0.0) beliidr, ISNULL(c.amttransusd, 0.0) beliusd FROM QL_mstcust s LEFT JOIN QL_conar c ON s.custoid=c.custoid AND c.cmpcode=@cmpcode AND c.trnardate<'" & tgl_1 & "' AND c.conaroid>0 AND payrefoid=0 AND trnarstatus='Post' INNER JOIN QL_trnjualmst jm ON jm.cmpcode=c.cmpcode AND jm.trnjualmstoid=c.refoid INNER JOIN QL_mstperson p ON p.salescode=jm.salescode AND p.leveloid=1 UNION ALL SELECT s.custoid, s.custname, p.salescode, personname,ISNULL(c.amtbayaridr * -1, 0.0) beliidr, ISNULL(c.amtbayarusd * -1, 0.0) beliusd FROM QL_mstcust s LEFT JOIN QL_conar c ON s.custoid=c.custoid AND c.cmpcode=@cmpcode AND c.paymentdate<'" & tgl_1 & "' AND trnarstatus='Post' AND trnartype IN ('DNAR', 'GCAR') INNER JOIN QL_trnjualmst jm ON jm.cmpcode=c.cmpcode AND jm.trnjualmstoid=c.refoid INNER JOIN QL_mstperson p ON p.salescode=jm.salescode AND p.leveloid=1) AS tblBeli GROUP BY custoid, custname, salescode, personname UNION ALL "
                sSql &= "SELECT custoid, custname, salescode, personname, 0.0 beliidr, 0.0 beliusd, SUM(paymentidr) paymentidr, SUM(paymentusd) paymentusd FROM (SELECT s.custoid, s.custname, salescode, personname, c.amtbayaridr paymentidr, c.amtbayarusd paymentusd FROM QL_mstcust s LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.custoid, con.cmpcode, p.salescode, personname FROM QL_trncashbankmst cb INNER JOIN QL_trnpayar pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conar con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payaroid INNER JOIN QL_conar c ON c.cmpcode=con.cmpcode AND c.reftype=con.reftype AND c.refoid=con.refoid AND c.payrefoid=0 INNER JOIN QL_trnjualmst jm ON jm.cmpcode=c.cmpcode AND jm.trnjualmstoid=c.refoid INNER JOIN QL_mstperson p ON p.salescode=jm.salescode AND p.leveloid=1 WHERE cb.cmpcode=@cmpcode AND ISNULL(payarres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnartype NOT IN ('CNAR', 'DNAR', 'SRET', 'GCAR') AND (CASE WHEN LEFT(cashbanktype, 2) IN " & sFilterDueDate & " THEN cashbankduedate ELSE cashbankdate END)<'" & tgl_1 & "' GROUP BY con.custoid, con.cmpcode, p.salescode, personname) AS c ON c.custoid=s.custoid UNION ALL "
                sSql &= "SELECT s.custoid, s.custname, p.salescode, personname,c.amtbayaridr paymentidr, c.amtbayarusd paymentusd FROM QL_mstcust s LEFT JOIN QL_conar c ON s.custoid=c.custoid AND c.cmpcode=@cmpcode AND c.paymentdate<'" & tgl_1 & "' AND trnarstatus='Post' AND c.trnartype IN ('CNAR', 'SRET') INNER JOIN QL_trnjualmst jm ON jm.cmpcode=c.cmpcode AND jm.trnjualmstoid=c.refoid INNER JOIN QL_mstperson p ON p.salescode=jm.salescode AND p.leveloid=1) AS tblpayment GROUP BY custoid, custname, salescode, personname) AS tblSaldoAwalPeriod GROUP BY custoid, custname, salescode, personname UNION ALL /*SALDO AWAL CUT OFF*/ "
                sSql &= "SELECT s.custoid, s.custname, p.salescode, personname, (SUM(ISNULL(c.amttransidr, 0.0)) - SUM(ISNULL(c.amtbayaridr, 0.0))) AS saidr, (SUM(ISNULL(c.amttransusd, 0.0)) - SUM(ISNULL(c.amtbayarusd, 0.0))) AS sausd, 0.0 beliidr, 0.0 beliusd, 0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, 0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd FROM QL_mstcust s LEFT JOIN QL_conar c ON s.custoid=c.custoid AND c.cmpcode=@cmpcode AND c.conaroid<0 INNER JOIN QL_trnjualmst jm ON jm.cmpcode=c.cmpcode AND jm.trnjualmstoid=c.refoid INNER JOIN QL_mstperson p ON p.salescode=jm.salescode AND p.leveloid=1 GROUP BY s.custoid, s.custname, p.salescode, personname UNION ALL /*PENJUALAN*/ "
                sSql &= "SELECT custoid, custname, salescode, personname, 0.0 saidr, 0.0 sausd, SUM(ISNULL(beliidr, 0.0)) beliidr, SUM(ISNULL(beliusd, 0.0)) beliusd, 0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, 0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd FROM (SELECT s.custoid, s.custname, p.salescode, personname, ISNULL(c.amttransidr, 0.0) beliidr, ISNULL(c.amttransusd, 0.0) beliusd FROM QL_mstcust s LEFT JOIN QL_conar c ON s.custoid=c.custoid AND c.cmpcode=@cmpcode AND c.trnardate " & periode & " AND c.conaroid>0 INNER JOIN QL_trnjualmst jm ON jm.cmpcode=c.cmpcode AND jm.trnjualmstoid=c.refoid AND payrefoid=0 AND trnarstatus='Post' INNER JOIN QL_mstperson p ON p.salescode=jm.salescode AND p.leveloid=1 UNION ALL SELECT s.custoid, s.custname, p.salescode, personname, ISNULL(c.amtbayaridr * -1, 0.0) beliidr, ISNULL(c.amtbayarusd * -1, 0.0) beliusd FROM QL_mstcust s LEFT JOIN QL_conar c ON s.custoid=c.custoid AND c.cmpcode=@cmpcode AND c.paymentdate " & periode & " AND trnarstatus='Post' AND trnartype IN ('DNAR', 'GCAR') INNER JOIN QL_trnjualmst jm ON jm.cmpcode=c.cmpcode AND jm.trnjualmstoid=c.refoid INNER JOIN QL_mstperson p ON p.salescode=jm.salescode AND p.leveloid=1) AS tblBeli GROUP BY custoid, custname, salescode, personname UNION ALL /*PAYMENT*/ "
                sSql &= "SELECT custoid, custname, salescode, personname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, SUM(paymentidr) paymentidr, SUM(paymentusd) paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, 0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd FROM (SELECT s.custoid, s.custname, salescode, personname, c.amtbayaridr paymentidr, c.amtbayarusd paymentusd FROM QL_mstcust s LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.custoid, con.cmpcode, p.salescode, personname FROM QL_trncashbankmst cb INNER JOIN QL_trnpayar pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conar con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payaroid INNER JOIN QL_conar c ON c.cmpcode=con.cmpcode AND c.reftype=con.reftype AND c.refoid=con.refoid AND c.payrefoid=0 INNER JOIN QL_trnjualmst jm ON jm.cmpcode=c.cmpcode AND jm.trnjualmstoid=c.refoid INNER JOIN QL_mstperson p ON p.salescode=jm.salescode AND p.leveloid=1 WHERE cb.cmpcode=@cmpcode AND ISNULL(payarres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnartype NOT IN ('CNAR', 'DNAR', 'SRET', 'GCAR') AND (CASE WHEN LEFT(cashbanktype, 2) IN " & sFilterDueDate & " THEN cashbankduedate ELSE cashbankdate END) " & periode & " GROUP BY con.custoid, con.cmpcode, p.salescode, personname) AS c ON c.custoid=s.custoid UNION ALL SELECT s.custoid, s.custname, p.salescode, personname, c.amtbayaridr paymentidr, c.amtbayarusd paymentusd FROM QL_mstcust s LEFT JOIN QL_conar c ON s.custoid=c.custoid AND c.cmpcode=@cmpcode AND c.paymentdate " & periode & " AND trnarstatus='Post' AND c.trnartype IN ('CNAR', 'SRET') INNER JOIN QL_trnjualmst jm ON jm.cmpcode=c.cmpcode AND jm.trnjualmstoid=c.refoid INNER JOIN QL_mstperson p ON p.salescode=jm.salescode AND p.leveloid=1) AS tblpayment GROUP BY custoid, custname, salescode, personname UNION ALL /*BELUM JATUH TEMPO*/ "
                sSql &= "SELECT ag.custoid, ag.custname, salescode, personname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, 0.0 paymentidr, 0.0 paymentusd, SUM(ag.amttransidr - ag.amtbayaridr) ageblmjtidr, SUM(ag.amttransusd - ag.amtbayarusd) ageblmjtusd, 0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd FROM (SELECT s.custoid, s.custname, p.salescode, personname, ISNULL(c.reftype, '') reftype, ISNULL(c.refoid, 0) refoid, ISNULL(c.amttransidr, 0.0) amttransidr, SUM(ISNULL(c1.amtbayaridr, 0.0)) amtbayaridr, ISNULL(c.amttransusd, 0.0) amttransusd, SUM(ISNULL(c1.amtbayarusd, 0.0)) amtbayarusd FROM QL_mstcust s LEFT JOIN QL_conar c ON s.custoid=c.custoid AND c.cmpcode=@cmpcode AND datediff(d, c.payduedate, '" & tgl_2 & "') < 0 AND c.payrefoid=0 AND c.trnardate<='" & tgl_2 & "' INNER JOIN QL_trnjualmst jm ON jm.cmpcode=c.cmpcode AND jm.trnjualmstoid=c.refoid INNER JOIN QL_mstperson p ON p.salescode=jm.salescode AND p.leveloid=1 LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.custoid, con.cmpcode, con.reftype, con.refoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayar pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conar con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payaroid INNER JOIN QL_trnjualmst jm ON jm.cmpcode=con.cmpcode AND jm.trnjualmstoid=con.refoid INNER JOIN QL_mstperson p ON p.salescode=jm.salescode AND p.leveloid=1 WHERE cb.cmpcode=@cmpcode AND ISNULL(payarres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnartype NOT IN ('CNAR', 'DNAR', 'GCAR', 'SRET') AND (CASE WHEN LEFT(cashbanktype, 2) IN " & sFilterDueDate & " THEN cashbankduedate ELSE cashbankdate END)<='" & tgl_2 & "' GROUP BY con.custoid, con.cmpcode, con.reftype, con.refoid, p.salescode, personname UNION ALL SELECT SUM(amtbayaridr) amtbayaridr, SUM(amtbayarusd) amtbayarusd, con.custoid, con.cmpcode, con.reftype, con.refoid FROM QL_conar con INNER JOIN QL_trnjualmst jm ON jm.cmpcode=con.cmpcode AND jm.trnjualmstoid=con.refoid INNER JOIN QL_mstperson p ON p.salescode=jm.salescode AND p.leveloid=1 WHERE con.cmpcode=@cmpcode AND trnarstatus='post' AND trnartype IN ('CNAR', 'DNAR', 'SRET', 'GCAR') AND con.paymentdate<='" & tgl_2 & "' GROUP BY con.custoid, con.cmpcode, con.reftype, con.refoid, p.salescode, personname) c1 ON c.cmpcode=c1.cmpcode AND c.custoid=c1.custoid AND c.reftype=c1.reftype AND c.refoid=c1.refoid GROUP BY s.custoid, s.custname, c.amttransidr, c.amttransusd, c.reftype, c.refoid, p.salescode, personname) AS ag GROUP BY ag.custoid, ag.custname, salescode, personname UNION ALL /*0-30 DAYS*/ "
                sSql &= "SELECT ag.custoid, ag.custname, salescode, personname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, 0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, SUM(ag.amttransidr - ag.amtbayaridr) age1idr, SUM(ag.amttransusd - ag.amtbayarusd) age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd FROM (SELECT s.custoid, s.custname, p.salescode, personname, ISNULL(c.reftype, '') reftype, ISNULL(c.refoid, 0) refoid, ISNULL(c.amttransidr, 0.0) amttransidr, SUM(ISNULL(c1.amtbayaridr, 0.0)) amtbayaridr, ISNULL(c.amttransusd, 0.0) amttransusd, SUM(ISNULL(c1.amtbayarusd, 0.0)) amtbayarusd FROM QL_mstcust s LEFT JOIN QL_conar c ON s.custoid=c.custoid AND c.cmpcode=@cmpcode AND datediff(d, c.payduedate, '" & tgl_2 & "') between 0 and 30 AND c.payrefoid=0 AND c.trnardate<='" & tgl_2 & "' INNER JOIN QL_trnjualmst jm ON jm.cmpcode=c.cmpcode AND jm.trnjualmstoid=c.refoid INNER JOIN QL_mstperson p ON p.salescode=jm.salescode AND p.leveloid=1 LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.custoid, con.cmpcode, con.reftype, con.refoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayar pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conar con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payaroid INNER JOIN QL_trnjualmst jm ON jm.cmpcode=con.cmpcode AND jm.trnjualmstoid=con.refoid INNER JOIN QL_mstperson p ON p.salescode=jm.salescode AND p.leveloid=1 WHERE cb.cmpcode=@cmpcode AND ISNULL(payarres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnartype NOT IN ('CNAR', 'DNAR', 'SRET', 'GCAR') AND (CASE WHEN LEFT(cashbanktype, 2) IN " & sFilterDueDate & " THEN cashbankduedate ELSE cashbankdate END)<='" & tgl_2 & "' GROUP BY con.custoid, con.cmpcode, con.reftype, con.refoid, p.salescode, p.personname UNION ALL SELECT SUM(amtbayaridr) amtbayaridr, SUM(amtbayarusd) amtbayarusd, con.custoid, con.cmpcode, con.reftype, con.refoid FROM QL_conar con INNER JOIN QL_trnjualmst jm ON jm.cmpcode=con.cmpcode AND jm.trnjualmstoid=con.refoid INNER JOIN QL_mstperson p ON p.salescode=jm.salescode AND p.leveloid=1 WHERE con.cmpcode=@cmpcode AND trnarstatus='post' AND trnartype IN ('CNAR', 'DNAR', 'SRET', 'GCAR') AND con.paymentdate<='" & tgl_2 & "' GROUP BY con.custoid, con.cmpcode, con.reftype, con.refoid, p.salescode, personname) c1 ON c.cmpcode=c1.cmpcode AND c.custoid=c1.custoid AND c.reftype=c1.reftype AND c.refoid=c1.refoid GROUP BY s.custoid, s.custname, c.amttransidr, c.amttransusd, c.reftype, c.refoid, p.salescode, personname) AS ag GROUP BY ag.custoid, ag.custname, salescode, personname UNION ALL /*31-60 DAYS*/ "
                sSql &= "SELECT ag.custoid, ag.custname, salescode, personname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, 0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, 0.0 age1idr, 0.0 age1usd, SUM(ag.amttransidr - ag.amtbayaridr) age2idr, SUM(ag.amttransusd - ag.amtbayarusd) age2usd, 0.0 age3idr, 0.0 age3usd FROM (SELECT s.custoid, s.custname, p.salescode, personname, ISNULL(c.reftype, '') reftype, ISNULL(c.refoid, 0) refoid, ISNULL(c.amttransidr, 0.0) amttransidr, SUM(ISNULL(c1.amtbayaridr, 0.0)) amtbayaridr, ISNULL(c.amttransusd, 0.0) amttransusd, SUM(ISNULL(c1.amtbayarusd, 0.0)) amtbayarusd FROM QL_mstcust s LEFT JOIN QL_conar c ON s.custoid=c.custoid AND c.cmpcode=@cmpcode AND datediff(d, c.payduedate, '" & tgl_2 & "') between 31 and 60 AND c.payrefoid=0 AND c.trnardate<='" & tgl_2 & "' INNER JOIN QL_trnjualmst jm ON jm.cmpcode=c.cmpcode AND jm.trnjualmstoid=c.refoid INNER JOIN QL_mstperson p ON p.salescode=jm.salescode AND p.leveloid=1 LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.custoid, con.cmpcode, con.reftype, con.refoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayar pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conar con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payaroid INNER JOIN QL_trnjualmst jm ON jm.cmpcode=con.cmpcode AND jm.trnjualmstoid=con.refoid INNER JOIN QL_mstperson p ON p.salescode=jm.salescode AND p.leveloid=1 WHERE cb.cmpcode=@cmpcode AND ISNULL(payarres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnartype NOT IN ('CNAR', 'DNAR', 'SRET', 'GCAR') AND (CASE WHEN LEFT(cashbanktype, 2) IN " & sFilterDueDate & " THEN cashbankduedate ELSE cashbankdate END)<='" & tgl_2 & "' GROUP BY con.custoid, con.cmpcode, con.reftype, con.refoid, p.salescode, personname UNION ALL SELECT SUM(amtbayaridr) amtbayaridr, SUM(amtbayarusd) amtbayarusd, con.custoid, con.cmpcode, con.reftype, con.refoid FROM QL_conar con INNER JOIN QL_trnjualmst jm ON jm.cmpcode=con.cmpcode AND jm.trnjualmstoid=con.refoid INNER JOIN QL_mstperson p ON p.salescode=jm.salescode AND p.leveloid=1 WHERE con.cmpcode=@cmpcode AND trnarstatus='post' AND trnartype IN ('CNAR', 'DNAR', 'SRET', 'GCAR') AND con.paymentdate<='" & tgl_2 & "' GROUP BY con.custoid, con.cmpcode, con.reftype, con.refoid, p.salescode, personname) c1 ON c.cmpcode=c1.cmpcode AND c.custoid=c1.custoid AND c.reftype=c1.reftype AND c.refoid=c1.refoid GROUP BY s.custoid, s.custname, c.amttransidr, c.amttransusd, c.reftype, c.refoid, p.salescode, personname) AS ag GROUP BY ag.custoid, ag.custname, salescode, personname UNION ALL /*>60 DAYS*/ "
                sSql &= "SELECT ag.custoid, ag.custname, salescode, personname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, 0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, 0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, SUM(ag.amttransidr - ag.amtbayaridr) age3idr, SUM(ag.amttransusd - ag.amtbayarusd) age3usd FROM (SELECT s.custoid, s.custname, p.salescode, personname, ISNULL(c.reftype, '') reftype, ISNULL(c.refoid, 0) refoid, ISNULL(c.amttransidr, 0.0) amttransidr, SUM(ISNULL(c1.amtbayaridr, 0.0)) amtbayaridr, ISNULL(c.amttransusd, 0.0) amttransusd, SUM(ISNULL(c1.amtbayarusd, 0.0)) amtbayarusd FROM QL_mstcust s LEFT JOIN QL_conar c ON s.custoid=c.custoid AND c.cmpcode=@cmpcode AND datediff(d, c.payduedate, '" & tgl_2 & "') > 60 AND c.payrefoid=0 AND c.trnardate<='" & tgl_2 & "' INNER JOIN QL_trnjualmst jm ON jm.cmpcode=c.cmpcode AND jm.trnjualmstoid=c.refoid INNER JOIN QL_mstperson p ON p.salescode=jm.salescode AND p.leveloid=1 LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.custoid, con.cmpcode, con.reftype, con.refoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayar pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conar con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payaroid INNER JOIN QL_trnjualmst jm ON jm.cmpcode=con.cmpcode AND jm.trnjualmstoid=con.refoid INNER JOIN QL_mstperson p ON p.salescode=jm.salescode AND p.leveloid=1 WHERE cb.cmpcode=@cmpcode AND ISNULL(payarres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnartype NOT IN ('CNAR', 'DNAR', 'SRET', 'GCAR') AND (CASE WHEN LEFT(cashbanktype, 2) IN " & sFilterDueDate & " THEN cashbankduedate ELSE cashbankdate END)<='" & tgl_2 & "' GROUP BY con.custoid, con.cmpcode, con.reftype, con.refoid, p.salescode, personname UNION ALL SELECT SUM(amtbayaridr) amtbayaridr, SUM(amtbayarusd) amtbayarusd, con.custoid, con.cmpcode, con.reftype, con.refoid FROM QL_conar con INNER JOIN QL_trnjualmst jm ON jm.cmpcode=con.cmpcode AND jm.trnjualmstoid=con.refoid INNER JOIN QL_mstperson p ON p.salescode=jm.salescode AND p.leveloid=1 WHERE con.cmpcode=@cmpcode AND trnarstatus='post' AND trnartype IN ('CNAR', 'DNAR', 'SRET', 'GCAR') AND con.paymentdate<='" & tgl_2 & "' GROUP BY con.custoid, con.cmpcode, con.reftype, con.refoid, p.salescode, personname) c1 ON c.cmpcode=c1.cmpcode AND c.custoid=c1.custoid AND c.reftype=c1.reftype AND c.refoid=c1.refoid GROUP BY s.custoid, s.custname, c.amttransidr, c.amttransusd, c.reftype, c.refoid, p.salescode, personname) AS ag GROUP BY ag.custoid, ag.custname, salescode, personname) AS tbldata "
                sSql &= swhere & " GROUP BY custoid, custname, salescode, personname "
            Else
                sSql &= "SELECT custoid, custname, custgroupoid, custgroupname personname, SUM(saidr) saidr, SUM(sausd) sausd, SUM(beliidr) beliidr, " & _
                            "SUM(beliusd) beliusd, SUM(paymentidr) paymentidr, SUM(paymentusd) paymentusd, " & _
                            "SUM(ageblmjtidr) ageblmjtidr, SUM(ageblmjtusd) ageblmjtusd, " & _
                            "SUM(age1idr) age1idr, SUM(age1usd) age1usd, SUM(age2idr) age2idr, SUM(age2usd) age2usd, " & _
                            "SUM(age3idr) age3idr, SUM(age3usd) age3usd, @currency currency, @cmpcode cmpcode, " & _
                            "'" & Format(CDate(txtStart.Text), "dd MMMM yyyy") & " - " & Format(CDate(txtFinish.Text), "dd MMMM yyyy") & "' periodreport, '" & FilterDDLDiv.SelectedItem.Text.ToUpper & "' company " & _
                            "FROM (" & _
                            "/*SALDO AWAL PERIODE*/" & _
                            "SELECT custoid, custname, custgroupoid, custgroupname, " & _
                            "(SUM(beliidr) - SUM(paymentidr)) saidr, " & _
                            "(SUM(beliusd) - SUM(paymentusd)) sausd, " & _
                            "0.0 beliidr, 0.0 beliusd, 0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, " & _
                            "0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd FROM (" & _
                            "SELECT custoid, custname, custgroupoid, custgroupname, " & _
                            "SUM(ISNULL(beliidr, 0.0)) beliidr, SUM(ISNULL(beliusd, 0.0)) beliusd, " & _
                            "0.0 paymentidr, 0.0 paymentusd " & _
                            "FROM (" & _
                            "SELECT s.custoid, s.custname, cg.custgroupoid, cg.custgroupname, " & _
                            "ISNULL(c.amttransidr, 0.0) beliidr, ISNULL(c.amttransusd, 0.0) beliusd " & _
                            "FROM QL_mstcust s INNER JOIN QL_mstcustgroup cg ON cg.custgroupoid=s.custgroupoid " & _
                            "LEFT JOIN QL_conar c ON s.custoid=c.custoid " & _
                            "AND c.cmpcode=@cmpcode AND c.trnardate<'" & tgl_1 & "' AND c.conaroid>0 " & _
                            "AND payrefoid=0 AND trnarstatus='Post' " & _
                            "UNION ALL " & _
                            "SELECT s.custoid, s.custname, cg.custgroupoid, cg.custgroupname, " & _
                            "ISNULL(c.amtbayaridr * -1, 0.0) beliidr, ISNULL(c.amtbayarusd * -1, 0.0) beliusd " & _
                            "FROM QL_mstcust s INNER JOIN QL_mstcustgroup cg ON cg.custgroupoid=s.custgroupoid " & _
                            "LEFT JOIN QL_conar c ON s.custoid=c.custoid " & _
                            "AND c.cmpcode=@cmpcode AND c.paymentdate<'" & tgl_1 & "' AND trnarstatus='Post' AND trnartype IN ('DNAR', 'GCAR') " & _
                            ") AS tblBeli GROUP BY custoid, custname, custgroupoid, custgroupname " & _
                            "UNION ALL " & _
                            "SELECT custoid, custname, custgroupoid, custgroupname, 0.0 beliidr, 0.0 beliusd," & _
                            "SUM(paymentidr) paymentidr, SUM(paymentusd) paymentusd " & _
                            "FROM (" & _
                            "SELECT s.custoid, s.custname, cg.custgroupoid, cg.custgroupname, " & _
                            "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd " & _
                            "FROM QL_mstcust s INNER JOIN QL_mstcustgroup cg ON cg.custgroupoid=s.custgroupoid " & _
                            "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.custoid, con.cmpcode FROM QL_trncashbankmst cb INNER JOIN QL_trnpayar pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conar con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payaroid INNER JOIN QL_conar c ON c.cmpcode=con.cmpcode AND c.reftype=con.reftype AND c.refoid=con.refoid AND c.payrefoid=0 WHERE cb.cmpcode=@cmpcode AND ISNULL(payarres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnartype NOT IN ('CNAR', 'DNAR', 'SRET', 'GCAR') AND (CASE WHEN LEFT(cashbanktype, 2) IN " & sFilterDueDate & " THEN cashbankduedate ELSE cashbankdate END)<'" & tgl_1 & "' GROUP BY con.custoid, con.cmpcode) AS c ON c.custoid=s.custoid " & _
                            "UNION ALL " & _
                            "SELECT s.custoid, s.custname, cg.custgroupoid, cg.custgroupname," & _
                            "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd " & _
                            "FROM QL_mstcust s INNER JOIN QL_mstcustgroup cg ON cg.custgroupoid=s.custgroupoid " & _
                            "LEFT JOIN QL_conar c ON s.custoid=c.custoid " & _
                            "AND c.cmpcode=@cmpcode AND c.paymentdate<'" & tgl_1 & "' AND trnarstatus='Post' AND c.trnartype IN ('CNAR', 'SRET') " & _
                            ") AS tblpayment GROUP BY custoid, custname, custgroupoid, custgroupname " & _
                            ") AS tblSaldoAwalPeriod GROUP BY custoid, custname, custgroupoid, custgroupname " & _
                            "UNION ALL /*SALDO AWAL CUT OFF*/" & _
                            "SELECT s.custoid, s.custname, cg.custgroupoid, cg.custgroupname, " & _
                            "(SUM(ISNULL(c.amttransidr, 0.0)) - SUM(ISNULL(c.amtbayaridr, 0.0))) AS saidr, " & _
                            "(SUM(ISNULL(c.amttransusd, 0.0)) - SUM(ISNULL(c.amtbayarusd, 0.0))) AS sausd, " & _
                            "0.0 beliidr, 0.0 beliusd, 0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, " & _
                            "0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd " & _
                            "FROM QL_mstcust s INNER JOIN QL_mstcustgroup cg ON cg.custgroupoid=s.custgroupoid " & _
                            "LEFT JOIN QL_conar c ON s.custoid=c.custoid " & _
                            "AND c.cmpcode=@cmpcode AND c.conaroid<0 " & _
                            "GROUP BY s.custoid, s.custname, cg.custgroupoid, cg.custgroupname " & _
                            "UNION ALL /*PENJUALAN*/" & _
                            "SELECT custoid, custname, custgroupoid, custgroupname, 0.0 saidr, 0.0 sausd, " & _
                            "SUM(ISNULL(beliidr, 0.0)) beliidr, SUM(ISNULL(beliusd, 0.0)) beliusd, " & _
                            "0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, 0.0 age1idr, 0.0 age1usd, " & _
                            "0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd " & _
                            "FROM (" & _
                            "SELECT s.custoid, s.custname, cg.custgroupoid, cg.custgroupname, " & _
                            "ISNULL(c.amttransidr, 0.0) beliidr, ISNULL(c.amttransusd, 0.0) beliusd " & _
                            "FROM QL_mstcust s INNER JOIN QL_mstcustgroup cg ON cg.custgroupoid=s.custgroupoid " & _
                            "LEFT JOIN QL_conar c ON s.custoid=c.custoid " & _
                            "AND c.cmpcode=@cmpcode AND c.trnardate " & periode & " AND c.conaroid>0 " & _
                            "AND payrefoid=0 AND trnarstatus='Post' " & _
                            "UNION ALL " & _
                            "SELECT s.custoid, s.custname, cg.custgroupoid, cg.custgroupname, " & _
                            "ISNULL(c.amtbayaridr * -1, 0.0) beliidr, ISNULL(c.amtbayarusd * -1, 0.0) beliusd " & _
                            "FROM QL_mstcust s INNER JOIN QL_mstcustgroup cg ON cg.custgroupoid=s.custgroupoid " & _
                            "LEFT JOIN QL_conar c ON s.custoid=c.custoid " & _
                            "AND c.cmpcode=@cmpcode AND c.paymentdate " & periode & " AND trnarstatus='Post' AND trnartype IN ('DNAR', 'GCAR') " & _
                            ") AS tblBeli GROUP BY custoid, custname, custgroupoid, custgroupname " & _
                            "UNION ALL /*PAYMENT*/" & _
                            "SELECT custoid, custname, custgroupoid, custgroupname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd," & _
                            "SUM(paymentidr) paymentidr, SUM(paymentusd) paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, " & _
                            "0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd " & _
                            "FROM (" & _
                            "SELECT s.custoid, s.custname, cg.custgroupoid, cg.custgroupname, " & _
                            "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd " & _
                            "FROM QL_mstcust s INNER JOIN QL_mstcustgroup cg ON cg.custgroupoid=s.custgroupoid " & _
                            "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.custoid, con.cmpcode FROM QL_trncashbankmst cb INNER JOIN QL_trnpayar pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conar con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payaroid INNER JOIN QL_conar c ON c.cmpcode=con.cmpcode AND c.reftype=con.reftype AND c.refoid=con.refoid AND c.payrefoid=0 WHERE cb.cmpcode=@cmpcode AND ISNULL(payarres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnartype NOT IN ('CNAR', 'DNAR', 'SRET', 'GCAR') AND (CASE WHEN LEFT(cashbanktype, 2) IN " & sFilterDueDate & " THEN cashbankduedate ELSE cashbankdate END) " & periode & " GROUP BY con.custoid, con.cmpcode) AS c ON c.custoid=s.custoid " & _
                            "UNION ALL " & _
                            "SELECT s.custoid, s.custname, cg.custgroupoid, cg.custgroupname," & _
                            "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd " & _
                            "FROM QL_mstcust s INNER JOIN QL_mstcustgroup cg ON cg.custgroupoid=s.custgroupoid " & _
                            "LEFT JOIN QL_conar c ON s.custoid=c.custoid " & _
                            "AND c.cmpcode=@cmpcode AND c.paymentdate " & periode & " AND trnarstatus='Post' AND c.trnartype IN ('CNAR', 'SRET') " & _
                            ") AS tblpayment GROUP BY custoid, custname, custgroupoid, custgroupname " & _
                            "UNION ALL /*BELUM JATUH TEMPO*/" & _
                            "SELECT ag.custoid, ag.custname, ag.custgroupoid, ag.custgroupname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, " & _
                            "0.0 paymentidr, 0.0 paymentusd, " & _
                            "SUM(ag.amttransidr - ag.amtbayaridr) ageblmjtidr, SUM(ag.amttransusd - ag.amtbayarusd) ageblmjtusd, " & _
                            "0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd " & _
                            "FROM (" & _
                            "SELECT s.custoid, s.custname, cg.custgroupoid, cg.custgroupname, ISNULL(c.reftype, '') reftype, ISNULL(c.refoid, 0) refoid, " & _
                            "ISNULL(c.amttransidr, 0.0) amttransidr, SUM(ISNULL(c1.amtbayaridr, 0.0)) amtbayaridr, " & _
                            "ISNULL(c.amttransusd, 0.0) amttransusd, SUM(ISNULL(c1.amtbayarusd, 0.0)) amtbayarusd " & _
                            "FROM QL_mstcust s INNER JOIN QL_mstcustgroup cg ON cg.custgroupoid=s.custgroupoid " & _
                            "LEFT JOIN QL_conar c ON s.custoid=c.custoid AND c.cmpcode=@cmpcode " & _
                            "AND datediff(d, c.payduedate, '" & tgl_2 & "') < 0 AND c.payrefoid=0 " & _
                            "AND c.trnardate<='" & tgl_2 & "' " & _
                            "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.custoid, con.cmpcode, con.reftype, con.refoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayar pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conar con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payaroid WHERE cb.cmpcode=@cmpcode AND ISNULL(payarres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnartype NOT IN ('CNAR', 'DNAR', 'GCAR', 'SRET') AND (CASE WHEN LEFT(cashbanktype, 2) IN " & sFilterDueDate & " THEN cashbankduedate ELSE cashbankdate END)<='" & tgl_2 & "' GROUP BY con.custoid, con.cmpcode, con.reftype, con.refoid UNION ALL SELECT SUM(amtbayaridr) amtbayaridr, SUM(amtbayarusd) amtbayarusd, con.custoid, con.cmpcode, con.reftype, con.refoid FROM QL_conar con WHERE con.cmpcode=@cmpcode AND trnarstatus='post' AND trnartype IN ('CNAR', 'DNAR', 'SRET', 'GCAR') AND con.paymentdate<='" & tgl_2 & "' GROUP BY con.custoid, con.cmpcode, con.reftype, con.refoid) c1 ON c.cmpcode=c1.cmpcode AND c.custoid=c1.custoid AND c.reftype=c1.reftype AND c.refoid=c1.refoid " & _
                            "GROUP BY s.custoid, s.custname, c.amttransidr, c.amttransusd, c.reftype, c.refoid, cg.custgroupoid, cg.custgroupname " & _
                            ") AS ag GROUP BY ag.custoid, ag.custname, ag.custgroupoid, ag.custgroupname " & _
                            "UNION ALL /*0-30 DAYS*/" & _
                            "SELECT ag.custoid, ag.custname, ag.custgroupoid, ag.custgroupname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, " & _
                            "0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, " & _
                            "SUM(ag.amttransidr - ag.amtbayaridr) age1idr, SUM(ag.amttransusd - ag.amtbayarusd) age1usd, " & _
                            "0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd " & _
                            "FROM (" & _
                            "SELECT s.custoid, s.custname, cg.custgroupoid, cg.custgroupname, ISNULL(c.reftype, '') reftype, ISNULL(c.refoid, 0) refoid, " & _
                            "ISNULL(c.amttransidr, 0.0) amttransidr, SUM(ISNULL(c1.amtbayaridr, 0.0)) amtbayaridr, " & _
                            "ISNULL(c.amttransusd, 0.0) amttransusd, SUM(ISNULL(c1.amtbayarusd, 0.0)) amtbayarusd " & _
                            "FROM QL_mstcust s INNER JOIN QL_mstcustgroup cg ON cg.custgroupoid=s.custgroupoid " & _
                            "LEFT JOIN QL_conar c ON s.custoid=c.custoid AND c.cmpcode=@cmpcode " & _
                            "AND datediff(d, c.payduedate, '" & tgl_2 & "') between 0 and 30 AND c.payrefoid=0 " & _
                            "AND c.trnardate <= '" & tgl_2 & "' " & _
                            "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.custoid, con.cmpcode, con.reftype, con.refoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayar pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conar con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payaroid WHERE cb.cmpcode=@cmpcode AND ISNULL(payarres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnartype NOT IN ('CNAR', 'DNAR', 'SRET', 'GCAR') AND (CASE WHEN LEFT(cashbanktype, 2) IN " & sFilterDueDate & " THEN cashbankduedate ELSE cashbankdate END)<='" & tgl_2 & "' GROUP BY con.custoid, con.cmpcode, con.reftype, con.refoid UNION ALL SELECT SUM(amtbayaridr) amtbayaridr, SUM(amtbayarusd) amtbayarusd, con.custoid, con.cmpcode, con.reftype, con.refoid FROM QL_conar con WHERE con.cmpcode=@cmpcode AND trnarstatus='post' AND trnartype IN ('CNAR', 'DNAR', 'SRET', 'GCAR') AND con.paymentdate<='" & tgl_2 & "' GROUP BY con.custoid, con.cmpcode, con.reftype, con.refoid) c1 ON c.cmpcode=c1.cmpcode AND c.custoid=c1.custoid AND c.reftype=c1.reftype AND c.refoid=c1.refoid " & _
                            "GROUP BY s.custoid, s.custname, c.amttransidr, c.amttransusd, c.reftype, c.refoid, cg.custgroupoid, cg.custgroupname " & _
                            ") AS ag GROUP BY ag.custoid, ag.custname, ag.custgroupoid, ag.custgroupname " & _
                            "UNION ALL /*31-60 DAYS*/" & _
                            "SELECT ag.custoid, ag.custname, ag.custgroupoid, ag.custgroupname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, " & _
                            "0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, 0.0 age1idr, 0.0 age1usd, " & _
                            "SUM(ag.amttransidr - ag.amtbayaridr) age2idr, SUM(ag.amttransusd - ag.amtbayarusd) age2usd, " & _
                            "0.0 age3idr, 0.0 age3usd " & _
                            "FROM (" & _
                            "SELECT s.custoid, s.custname, cg.custgroupoid, cg.custgroupname, ISNULL(c.reftype, '') reftype, ISNULL(c.refoid, 0) refoid, " & _
                            "ISNULL(c.amttransidr, 0.0) amttransidr, SUM(ISNULL(c1.amtbayaridr, 0.0)) amtbayaridr, " & _
                            "ISNULL(c.amttransusd, 0.0) amttransusd, SUM(ISNULL(c1.amtbayarusd, 0.0)) amtbayarusd " & _
                            "FROM QL_mstcust s INNER JOIN QL_mstcustgroup cg ON cg.custgroupoid=s.custgroupoid " & _
                            "LEFT JOIN QL_conar c ON s.custoid=c.custoid AND c.cmpcode=@cmpcode " & _
                            "AND datediff(d, c.payduedate, '" & tgl_2 & "') between 31 and 60 AND c.payrefoid=0 " & _
                            "AND c.trnardate<='" & tgl_2 & "' " & _
                            "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.custoid, con.cmpcode, con.reftype, con.refoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayar pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conar con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payaroid WHERE cb.cmpcode=@cmpcode AND ISNULL(payarres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnartype NOT IN ('CNAR', 'DNAR', 'SRET', 'GCAR') AND (CASE WHEN LEFT(cashbanktype, 2) IN " & sFilterDueDate & " THEN cashbankduedate ELSE cashbankdate END)<='" & tgl_2 & "' GROUP BY con.custoid, con.cmpcode, con.reftype, con.refoid UNION ALL SELECT SUM(amtbayaridr) amtbayaridr, SUM(amtbayarusd) amtbayarusd, con.custoid, con.cmpcode, con.reftype, con.refoid FROM QL_conar con WHERE con.cmpcode=@cmpcode AND trnarstatus='post' AND trnartype IN ('CNAR', 'DNAR', 'SRET', 'GCAR') AND con.paymentdate<='" & tgl_2 & "' GROUP BY con.custoid, con.cmpcode, con.reftype, con.refoid) c1 ON c.cmpcode=c1.cmpcode AND c.custoid=c1.custoid AND c.reftype=c1.reftype AND c.refoid=c1.refoid " & _
                            "GROUP BY s.custoid, s.custname, c.amttransidr, c.amttransusd, c.reftype, c.refoid, cg.custgroupoid, cg.custgroupname " & _
                            ") AS ag GROUP BY ag.custoid, ag.custname, ag.custgroupoid, ag.custgroupname " & _
                            "UNION ALL /*>60 DAYS*/" & _
                            "SELECT ag.custoid, ag.custname, ag.custgroupoid, ag.custgroupname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, " & _
                            "0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, 0.0 age1idr, 0.0 age1usd, " & _
                            "0.0 age2idr, 0.0 age2usd, " & _
                            "SUM(ag.amttransidr - ag.amtbayaridr) age3idr, SUM(ag.amttransusd - ag.amtbayarusd) age3usd " & _
                            "FROM (" & _
                            "SELECT s.custoid, s.custname, cg.custgroupoid, cg.custgroupname, ISNULL(c.reftype, '') reftype, ISNULL(c.refoid, 0) refoid, " & _
                            "ISNULL(c.amttransidr, 0.0) amttransidr, SUM(ISNULL(c1.amtbayaridr, 0.0)) amtbayaridr, " & _
                            "ISNULL(c.amttransusd, 0.0) amttransusd, SUM(ISNULL(c1.amtbayarusd, 0.0)) amtbayarusd " & _
                            "FROM QL_mstcust s INNER JOIN QL_mstcustgroup cg ON cg.custgroupoid=s.custgroupoid " & _
                            "LEFT JOIN QL_conar c ON s.custoid=c.custoid AND c.cmpcode=@cmpcode " & _
                            "AND datediff(d, c.payduedate, '" & tgl_2 & "') > 60 AND c.payrefoid=0 " & _
                            "AND c.trnardate<='" & tgl_2 & "' " & _
                            "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.custoid, con.cmpcode, con.reftype, con.refoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayar pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conar con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payaroid WHERE cb.cmpcode=@cmpcode AND ISNULL(payarres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnartype NOT IN ('CNAR', 'DNAR', 'SRET', 'GCAR') AND (CASE WHEN LEFT(cashbanktype, 2) IN " & sFilterDueDate & " THEN cashbankduedate ELSE cashbankdate END)<='" & tgl_2 & "' GROUP BY con.custoid, con.cmpcode, con.reftype, con.refoid UNION ALL SELECT SUM(amtbayaridr) amtbayaridr, SUM(amtbayarusd) amtbayarusd, con.custoid, con.cmpcode, con.reftype, con.refoid FROM QL_conar con WHERE con.cmpcode=@cmpcode AND trnarstatus='post' AND trnartype IN ('CNAR', 'DNAR', 'SRET', 'GCAR') AND con.paymentdate<='" & tgl_2 & "' GROUP BY con.custoid, con.cmpcode, con.reftype, con.refoid) c1 ON c.cmpcode=c1.cmpcode AND c.custoid=c1.custoid AND c.reftype=c1.reftype AND c.refoid=c1.refoid " & _
                            "GROUP BY s.custoid, s.custname, c.amttransidr, c.amttransusd, c.reftype, c.refoid, cg.custgroupoid, cg.custgroupname " & _
                            ") AS ag GROUP BY ag.custoid, ag.custname, ag.custgroupoid, ag.custgroupname " & _
                            ") AS tbldata " & _
                            swhere & _
                            "GROUP BY custoid, custname, custgroupoid, custgroupname "
            End If
            If chkEmpty.Checked Then
                sSql &= "HAVING SUM(saidr)<>0 OR SUM(beliidr)<>0 OR " & _
                "SUM(paymentidr)<>0 OR SUM(age1idr)<>0 OR " & _
                "SUM(age2idr)<>0 OR SUM(age3idr)<>0 "
            End If
            sSql &= "ORDER BY "
            If DDLGroupBy.SelectedValue = "Customer" Then
                sSql &= "custname"
            ElseIf DDLGroupBy.SelectedValue = "SALES" Then
                sSql &= "personname, custname"
            Else
                sSql &= "custgroupname"
            End If
            Dim dt As DataTable
            Dim nFile As String = ""
            Dim sGroup As String = ""
            If DDLGroupBy.SelectedValue = "SALES" Then
                sGroup = "Sales"
            ElseIf DDLGroupBy.SelectedValue = "CUSTOMER GROUP" Then
                sGroup = "CustGroup"
            End If
            If FilterType.SelectedValue.ToUpper = "SUMMARY" Then
                If sType = "Print Excel" Then
                    nFile = Server.MapPath(folderReport & "rptAR" & sGroup & "Xls.rpt")
                Else
                    nFile = Server.MapPath(folderReport & "rptAR" & sGroup & ".rpt")
                End If
                report.Load(nFile)
                dt = cKon.ambiltabel(sSql, "tbldata")
                report.SetDataSource(dt)
            ElseIf FilterType.SelectedValue.ToUpper = "DETAIL" Then
                If sType = "Print Excel" Then
                    If FilterCurrency.SelectedIndex <> "2" Then
                        nFile = Server.MapPath(folderReport & "crKartuARDtlXls.rpt")
                        If DDLGroupBy.SelectedValue.ToUpper = "CUSTOMER" Then
                            nFile = Server.MapPath(folderReport & "crKartuARDtlXls.rpt")
                        ElseIf DDLGroupBy.SelectedValue.ToUpper = "SALES" Then
                            nFile = Server.MapPath(folderReport & "crKartuARPerSalesDtl.rpt")
                        Else
                            nFile = Server.MapPath(folderReport & "crKartuARDtlCustGroupXls.rpt")
                        End If
                    Else
                        nFile = Server.MapPath(folderReport & "crKartuARDtlRateXls.rpt")
                    End If
                Else
                    If FilterCurrency.SelectedIndex <> "2" Then
                        If DDLGroupBy.SelectedValue.ToUpper = "CUSTOMER" Then
                            nFile = Server.MapPath(folderReport & "crKartuARDtl.rpt")
                        ElseIf DDLGroupBy.SelectedValue.ToUpper = "SALES" Then
                            nFile = Server.MapPath(folderReport & "crKartuARPerSalesDtl.rpt")
                        Else
                            nFile = Server.MapPath(folderReport & "crKartuARDtlCustGroup.rpt")
                        End If
                    Else
                        nFile = Server.MapPath(folderReport & "crKartuARDtlRate.rpt")
                    End If
                End If
                report.Load(nFile)
            Else
                If sType = "Print Excel" Then
                    nFile = Server.MapPath(folderReport & "crKartuARDtlXls2.rpt")
                Else
                    nFile = Server.MapPath(folderReport & "crKartuARDtl2.rpt")
                End If
                report.Load(nFile)
            End If

            Dim strLastModified As String = System.IO.File.GetLastWriteTime(nFile.ToString())
            'Dim data As DataTable = cKon.ambiltabel(sSql, "data")
            'Dim dv As DataView = data.DefaultView()
            'report.SetDataSource(dv.ToTable())
            cProc.SetDBLogonForReport(report)

            If FilterType.SelectedValue.ToUpper = "DETAIL" Or FilterType.SelectedValue.ToUpper = "EKSTERNAL" Then
                report.SetParameterValue("cmpcode", FilterDDLDiv.SelectedValue)
                report.SetParameterValue("start", CDate(txtStart.Text.Trim))
                report.SetParameterValue("finish", CDate(txtFinish.Text.Trim))
                'If FilterType.SelectedValue.ToUpper = "EKSTERNAL" Then
                '    report.SetParameterValue("swhere", swheresupp2)
                'Else
                '    report.SetParameterValue("swhere", swheresupp)
                'End If
                report.SetParameterValue("swhere", swheresupp)
                Dim sAdd As String = ""
                If DDLGroupBy.SelectedValue = "SALES" And cbSales.Checked Then
                    sAdd &= " AND salescode='" & DDLSales.SelectedValue & "'"
                End If
                If chkEmpty.Checked Then
                    report.SetParameterValue("swherezero", " WHERE (ISNULL(sawal,0)>0 OR ISNULL(sawalidr,0)>0 OR ISNULL(sawalusd,0)>0 OR " & _
                        "ISNULL(amttrans,0)>0 OR ISNULL(amttransidr,0)>0 OR ISNULL(amttransusd,0)>0 " & _
                        "OR ISNULL(amtbayar,0)>0 OR ISNULL(amtbayaridr,0)>0 OR ISNULL(amtbayarusd,0)>0) " & sAdd)
                Else
                    report.SetParameterValue("swherezero", " WHERE 1=1 " & sAdd)
                End If
                report.SetParameterValue("currvalue", FilterCurrency.SelectedValue)
                report.SetParameterValue("companyname", FilterDDLDiv.SelectedItem.Text)
                report.SetParameterValue("periodreport", Format(CDate(txtStart.Text), "dd MMMM yyyy") & " - " & Format(CDate(txtFinish.Text), "dd MMMM yyyy"))
                report.SetParameterValue("PrintUserID", Session("UserID"))
                report.SetParameterValue("PrintLastModified", strLastModified)
                report.SetParameterValue("sFilterDueDate", sFilterDueDate)

                If FilterCurrency.SelectedIndex <> "2" Then
                    report.PrintOptions.PaperOrientation = PaperOrientation.Portrait
                Else
                    report.PrintOptions.PaperOrientation = PaperOrientation.Landscape
                End If
            Else
                report.SetParameterValue("PrintLastModified", strLastModified)
                report.SetParameterValue("PrintUserID", Session("UserID"))
                report.PrintOptions.PaperOrientation = PaperOrientation.Landscape
                report.PrintOptions.PaperSize = PaperSize.PaperFolio
            End If

            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "ARREPORT_" & FilterDDLMonth.SelectedItem.Text.ToUpper & FilterDDLYear.SelectedItem.Text)
                Catch ex As Exception
                    report.Close() : report.Dispose()
                End Try
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "ARREPORT_" & FilterDDLMonth.SelectedItem.Text.ToUpper & FilterDDLYear.SelectedItem.Text)
                Catch ex As Exception
                    report.Close() : report.Dispose()
                End Try
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.ToString, 1)
        End Try
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmPAYAR.aspx")
        End If
        If checkPagePermission("~\ReportForm\frmPAYAR.aspx", Session("Role")) = False Then
            ' Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Account Receiveable Report"
        If Not Page.IsPostBack Then
            FilterDDLMonth.Items.Clear()
            For C1 As Int16 = 1 To 12
                FilterDDLMonth.Items.Add(MonthName(C1))
                FilterDDLMonth.Items(FilterDDLMonth.Items.Count - 1).Value = Format(C1, "00")
            Next
            FilterDDLMonth.SelectedValue = Format(Now.Month, "00")
            FilterDDLYear.Items.Clear()
            For C1 As Int16 = Now.Year - 2 To Now.Year + 2
                FilterDDLYear.Items.Add(C1)
                FilterDDLYear.Items(FilterDDLYear.Items.Count - 1).Value = C1
            Next
            FilterDDLYear.SelectedValue = Now.Year

            ' DDL Division
            sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE' "
            FillDDL(FilterDDLDiv, sSql)
            ' DDL Sales
            sSql = "SELECT salescode, UPPER(personname) personname FROM QL_mstperson WHERE cmpcode='" & FilterDDLDiv.SelectedValue & "' AND leveloid=1 AND activeflag='ACTIVE' ORDER BY personname"
            FillDDL(DDLSales, sSql)

            FilterCurrency.Items.Clear()
            FilterCurrency.Items.Add(New ListItem("INDONESIAN RUPIAH", "IDR"))
            FilterCurrency.Items.Add(New ListItem("US DOLLAR", "USD"))
            FilterCurrency.Items.Add(New ListItem("TRANSACTION RATE", ""))
            FilterCurrency.Items(2).Enabled = False

            pnlSupplier.CssClass = "popupControl"
            txtStart.Text = Format(GetServerTime, "MM/01/yyyy")
            txtFinish.Text = Format(GetServerTime, "MM/dd/yyyy")

            DDLGroupBy_SelectedIndexChanged(Nothing, Nothing)
            DDLCustomer_SelectedIndexChanged(Nothing, Nothing)
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub FilterDDLDiv_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLDiv.SelectedIndexChanged
        rbSupplier.SelectedIndex = 0 : rbSupplier_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub FilterType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterType.SelectedIndexChanged
        If (FilterType.SelectedIndex = 0) Then
            FilterDDLMonth.Visible = True : FilterDDLYear.Visible = True
            txtStart.Visible = False : imbStart.Visible = False : lblTo.Visible = False
            txtFinish.Visible = False : imbFinish.Visible = False : lblMMDD.Visible = False
            lblGroupBy.Visible = True : lblSepGroupBy.Visible = True : DDLGroupBy.Visible = True
            'DDLGroupBy_SelectedIndexChanged(Nothing, Nothing)
        Else
            FilterDDLMonth.Visible = False : FilterDDLYear.Visible = False
            txtStart.Visible = True : imbStart.Visible = True : lblTo.Visible = True
            txtFinish.Visible = True : imbFinish.Visible = True : lblMMDD.Visible = True
            lblGroupBy.Visible = True : lblSepGroupBy.Visible = True : DDLGroupBy.Visible = True
        End If
        FilterDDLMonth.Visible = False : FilterDDLYear.Visible = False
        txtStart.Visible = True : imbStart.Visible = True : lblTo.Visible = True
        txtFinish.Visible = True : imbFinish.Visible = True : lblMMDD.Visible = True
        DDLGroupBy_SelectedIndexChanged(Nothing, Nothing)
        FilterCurrency.Items(2).Enabled = (FilterType.SelectedIndex = 1)
    End Sub

    Protected Sub rbSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbSupplier.SelectedIndexChanged
        gvSupplier.DataSource = Nothing : gvSupplier.DataBind()
        Session("TblCust") = Nothing : gvSupplier.Visible = False
        Session("TblCustView") = Nothing
        If rbSupplier.SelectedValue = "ALL" Then
            FilterTextSupplier.Visible = False
            btnSearchSupplier.Visible = False
            btnClearSupplier.Visible = False
            TDFilterCust.Visible = False
        Else
            FilterTextSupplier.Visible = True
            btnSearchSupplier.Visible = True
            btnClearSupplier.Visible = True
        End If
    End Sub

    Protected Sub btnSearchSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupplier.Click
        If Session("TblCust") Is Nothing Then
            BindSupplierData()
            If Session("TblCust").Rows.Count <= 0 Then
                Session("EmptyListCust") = "Customer data can't be found!"
                showMessage(Session("EmptyListCust"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = "custcode LIKE '%" & Tchar(FilterTextSupplier.Text) & "%' OR custname LIKE '%" & Tchar(FilterTextSupplier.Text) & "%'"
        If UpdateCheckedGV() Then
            Dim dv As DataView = Session("TblCust").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblCustView") = dv.ToTable
                gvSupplier.DataSource = Session("TblCustView")
                gvSupplier.DataBind()
                dv.RowFilter = ""
            Else
                dv.RowFilter = ""
                Session("TblCustView") = Nothing
                gvSupplier.DataSource = Session("TblCustView")
                gvSupplier.DataBind()
            End If
        End If
        TDFilterCust.Visible = True
    End Sub

    Protected Sub btnClearSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupplier.Click
        Session("TblCust") = Nothing : Session("TblCustView") = Nothing
        FilterTextSupplier.Text = ""
        custoid.Text = ""
        TDFilterCust.Visible = False
        gvSupplier.DataSource = Nothing
        gvSupplier.DataBind()
        gvSupplier.Visible = False
    End Sub

    Protected Sub gvSupplier_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupplier.PageIndexChanging
        If UpdateCheckedGV2() Then
            gvSupplier.PageIndex = e.NewPageIndex
            Dim dtSupp As DataTable = Session("TblCustView")
            gvSupplier.DataSource = dtSupp
            gvSupplier.DataBind()
        End If
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        If UpdateCheckedGV() Then
            Dim sMsg As String = ValidateParam()
            If sMsg <> "" Then
                showMessage(sMsg, 2) : Exit Sub
            Else
                ShowReport("View")
            End If
        Else
            Dim sMsg As String = ValidateParam()
            If sMsg <> "" Then
                showMessage(sMsg, 2) : Exit Sub
            Else
                ShowReport("View")
            End If
        End If
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        If UpdateCheckedGV() Then
            Dim sMsg As String = ValidateParam()
            If sMsg <> "" Then
                showMessage(sMsg, 2) : Exit Sub
            Else
                ShowReport("Print PDF")
            End If
        Else
            Dim sMsg As String = ValidateParam()
            If sMsg <> "" Then
                showMessage(sMsg, 2) : Exit Sub
            Else
                ShowReport("Print PDF")
            End If
        End If
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        If UpdateCheckedGV() Then
            Dim sMsg As String = ValidateParam()
            If sMsg <> "" Then
                showMessage(sMsg, 2) : Exit Sub
            Else
                ShowReport("Print Excel")
            End If
        Else
            Dim sMsg As String = ValidateParam()
            If sMsg <> "" Then
                showMessage(sMsg, 2) : Exit Sub
            Else
                ShowReport("Print Excel")
            End If
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmPAYAR.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Dispose() : report.Close()
    End Sub

    Protected Sub DDLGroupBy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim bVal As Boolean = False
        If DDLGroupBy.SelectedValue = "SALES" Then
            bVal = True
        End If
        cbSales.Visible = bVal : lblSepSales.Visible = bVal : DDLSales.Visible = bVal
    End Sub

    Protected Sub DDLCustomer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        DDLGroupBy.Items(2).Enabled = False
        DDLGroupBy.Items(1).Enabled = True
        DDLGroupBy.Items(0).Enabled = True
        If DDLCustomer.SelectedValue <> "CUSTOMER" Then
            DDLGroupBy.Items(0).Enabled = False : DDLGroupBy.Items(1).Enabled = False : DDLGroupBy.Items(2).Enabled = True
        End If
        gvSupplier.DataSource = Nothing : gvSupplier.DataBind()
        gvSupplier.Visible = False
        rbSupplier.SelectedValue = "ALL"

        If rbSupplier.SelectedValue = "ALL" Then
            FilterTextSupplier.Visible = False
            btnSearchSupplier.Visible = False
            btnClearSupplier.Visible = False
        Else
            FilterTextSupplier.Visible = True
            btnSearchSupplier.Visible = True
            btnClearSupplier.Visible = True
        End If
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCustView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblCust")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "custoid=" & dtTbl.Rows(C1)("custoid")
                    objView(0)("selected") = 1
                    dtTbl.Rows(C1)("selected") = 1
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblCust") = objTbl
                Session("TblCustView") = dtTbl
                gvSupplier.DataSource = Session("TblCustView")
                gvSupplier.DataBind()
            End If
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCustView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblCust")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "custoid=" & dtTbl.Rows(C1)("custoid")
                    objView(0)("selected") = 0
                    dtTbl.Rows(C1)("selected") = 0
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblCust") = objTbl
                Session("TblCustView") = dtTbl
                gvSupplier.DataSource = Session("TblCustView")
                gvSupplier.DataBind()
            End If
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not Session("TblCustView") Is Nothing Then
            If UpdateCheckedGV() Then
                Dim dtTbl As DataTable = Session("TblCust")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "selected=1"
                If dtView.Count > 0 Then
                    Session("TblCustView") = dtView.ToTable
                    gvSupplier.DataSource = Session("TblCustView")
                    gvSupplier.DataBind()
                    dtView.RowFilter = ""
                Else
                    dtView.RowFilter = ""
                    Session("TblCustView") = Nothing
                    gvSupplier.DataSource = Session("TblCustView")
                    gvSupplier.DataBind()
                End If
            End If
        End If

    End Sub
#End Region

End Class
