<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="frmDPAR.aspx.vb" Inherits="ReportForm_frmDPAR" title="" EnableEventValidation="false" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Down Payment A/R Report" CssClass="Title" ForeColor="SaddleBrown" Width="358px"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="center">
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:UpdatePanel id="upReportForm" runat="server">
                    <contenttemplate>
<asp:Panel id="pnlReportForm" runat="server" Width="100%" __designer:wfdid="w258" DefaultButton="btnSearchSupplier"><TABLE><TBODY><TR><TD id="TD2" align=left runat="server" Visible="false"><asp:Label id="Label5" runat="server" Text="Bussiness Unit" __designer:wfdid="w259"></asp:Label></TD><TD id="TD3" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD1" align=left runat="server" Visible="false"><asp:DropDownList id="FilterDDLDiv" runat="server" CssClass="inpText" __designer:wfdid="w260" AutoPostBack="True">
            </asp:DropDownList> <asp:DropDownList id="FilterType" runat="server" CssClass="inpText" __designer:wfdid="w261" AutoPostBack="True" Visible="False"><asp:ListItem>SUMMARY</asp:ListItem>
<asp:ListItem Selected="True">DETAIL</asp:ListItem>
</asp:DropDownList> <asp:Label id="Label7" runat="server" Text="Type" __designer:wfdid="w262" Visible="False"></asp:Label> <asp:DropDownList id="FilterDDLMonth" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w263"></asp:DropDownList> <asp:DropDownList id="FilterDDLYear" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w264"></asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label3" runat="server" Text="Period" __designer:wfdid="w265"></asp:Label></TD><TD class="Label" align=center>:</TD><TD align=left><asp:TextBox id="txtStart" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w266" Visible="False"></asp:TextBox> <asp:ImageButton id="imbStart" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w267" Visible="False"></asp:ImageButton> <asp:Label id="lblTo" runat="server" Text="to" __designer:wfdid="w268" Visible="False"></asp:Label> <asp:TextBox id="txtFinish" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w269" Visible="False"></asp:TextBox> <asp:ImageButton id="imbFinish" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w270" Visible="False"></asp:ImageButton> <asp:Label id="lblMMDD" runat="server" CssClass="Important" Text="(MM/DD/yyyy)" __designer:wfdid="w271" Visible="False"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label2" runat="server" Text="Customer" __designer:wfdid="w272"></asp:Label></TD><TD class="Label" align=center>:</TD><TD align=left><TABLE cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD align=left><asp:RadioButtonList id="rbSupplier" runat="server" __designer:wfdid="w273" AutoPostBack="True" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True">ALL</asp:ListItem>
                                <asp:ListItem>SELECT</asp:ListItem>
                            </asp:RadioButtonList></TD><TD align=left><asp:TextBox id="FilterTextSupplier" runat="server" Width="175px" CssClass="inpText" __designer:wfdid="w274" Visible="False"></asp:TextBox> <asp:ImageButton id="btnSearchSupplier" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w275" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnClearSupplier" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w276" Visible="False"></asp:ImageButton></TD></TR></TBODY></TABLE></TD></TR><TR><TD align=left></TD><TD class="Label" align=center></TD><TD align=left><asp:CheckBox id="chkEmpty" runat="server" Text="Hide Customer with 0 DP A/R amount" __designer:wfdid="w277" Checked="True"></asp:CheckBox></TD></TR><TR><TD align=left><asp:Label id="custoid" runat="server" __designer:wfdid="w278" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD align=left><asp:Panel id="pnlSupplier" runat="server" __designer:wfdid="w279"><asp:GridView id="gvCustomer" runat="server" Visible="False" DataKeyNames="custoid,custname" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" __designer:wfdid="w280">
<PagerSettings Mode="NumericFirstLast"></PagerSettings>
<Columns>
<asp:TemplateField><EditItemTemplate>
<asp:CheckBox id="CheckBox1" runat="server"></asp:CheckBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:CheckBox id="chkSelect" runat="server" Checked='<%# Eval("selected") %>' ToolTip='<%# Eval("custoid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="custcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="False" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label4" runat="server" ForeColor="Red" Text="No data found."></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White"></SelectedRowStyle>

<HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White"></HeaderStyle>
</asp:GridView></asp:Panel></TD></TR><TR><TD align=left><asp:Label id="Label6" runat="server" Width="96px" Text="Currency Value" __designer:wfdid="w281"></asp:Label></TD><TD class="Label" align=center>:</TD><TD align=left><asp:DropDownList id="FilterCurrency" runat="server" CssClass="inpText" __designer:wfdid="w282"></asp:DropDownList></TD></TR><TR><TD align=left colSpan=3><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w283"></asp:ImageButton> <asp:ImageButton id="btnExportToPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w284"></asp:ImageButton> <asp:ImageButton id="btnExportToExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w285"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w286"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" align=center colSpan=3><ajaxToolkit:CalendarExtender id="ceStart" runat="server" __designer:wfdid="w287" Format="MM/dd/yyyy" PopupButtonID="imbStart" TargetControlID="txtStart"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeStart" runat="server" __designer:wfdid="w288" TargetControlID="txtStart" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="ceFin" runat="server" __designer:wfdid="w289" Format="MM/dd/yyyy" PopupButtonID="imbFinish" TargetControlID="txtFinish"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeFin" runat="server" __designer:wfdid="w290" TargetControlID="txtFinish" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="HEIGHT: 60px" vAlign=top align=center colSpan=3><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w291" AssociatedUpdatePanelID="upReportForm"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgLoading" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w292"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR></TBODY></TABLE></asp:Panel> <CR:CrystalReportViewer id="crvReportForm" runat="server" __designer:wfdid="w293" AutoDataBind="True" HasViewList="False" HasCrystalLogo="False" HasPrintButton="False" HasExportButton="False" HasToggleGroupTreeButton="False" HasDrillUpButton="False"></CR:CrystalReportViewer> 
</contenttemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="btnViewReport"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnExportToPdf"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnExportToExcel"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="center" style="height: 226px">
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" DropShadow="True" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>

