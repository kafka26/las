Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_StockAdjustment
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If FilterPeriod1.Text = "" Or FilterPeriod2.Text = "" Then
            showMessage("Please fill both of Period 1 and Period 2 first!", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function 'OK
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub 'OK

    Private Sub InitAllDDL()
        ' DDL Division
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If FillDDL(DDLBusUnit, sSql) Then
            InitDDLWarehouse()
        End If
    End Sub 'OK

    Private Sub InitDDLWarehouse()
        ' DDL Warehouse
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode = '" & CompnyCode & "' AND gengroup IN ('WAREHOUSE', '') AND activeflag='ACTIVE' AND genother1='' ORDER BY gendesc"
        FillDDL(DDLWarehouse, sSql)
    End Sub 'OK

    Private Sub BindListStockAdj()
        sSql = "SELECT DISTINCT resfield1, stockadjno, CONVERT(VARCHAR(10), stockadjdate, 101) AS stockadjdate, 'False' AS checkvalue /*stockadjtype*/ FROM QL_trnstockadj adj WHERE adj.cmpcode='" & DDLBusUnit.SelectedValue & "' ORDER BY resfield1"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnmatusagemst")
        If dt.Rows.Count > 0 Then
            Session("TblListStockAdj") = dt
            Session("TblListStockAdjView") = Session("TblListStockAdj")
            gvListStockAdj.DataSource = Session("TblListStockAdjView")
            gvListStockAdj.DataBind()
            cProc.SetModalPopUpExtender(btnHideListStockAdj, pnlListStockAdj, mpeListStockAdj, True)
        Else
            showMessage("No Stock Adjustment data available for this time.", 2)
        End If
    End Sub 'OK

    Private Sub UpdateCheckedListStockAdj()
        If Session("TblListStockAdj") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListStockAdj")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListStockAdj.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListStockAdj.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "resfield1='" & sOid & "'"
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListStockAdj") = dt
        End If
        If Session("TblListStockAdjView") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListStockAdjView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListStockAdj.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListStockAdj.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "resfield1='" & sOid & "'"
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListStockAdjView") = dt
        End If
    End Sub 'OK

    Private Sub BindListMat()
        Dim sTypeMat As String = "", sTypeMat2 As String = "", sType As String = ""
        If DDLMatType.SelectedValue = "RM" Then
            sType = "RAW MATERIAL"
        ElseIf DDLMatType.SelectedValue = "GM" Then
            sType = "GENERAL MATERIAL"
        ElseIf DDLMatType.SelectedValue = "WIP" Then
            sType = "WIP"
        ElseIf DDLMatType.SelectedValue = "FG" Then
            sType = "FINISH GOOD"
        End If
        sSql = "SELECT DISTINCT m.itemoid AS matoid, m.itemCode AS matcode, m.itemLongDescription AS matlongdesc, g.gendesc AS matunit, 'False' AS checkvalue FROM QL_trnstockadj adj INNER JOIN QL_mstitem m ON m.itemoid=adj.refoid INNER JOIN QL_mstgen g ON genoid=stockadjunit INNER JOIN QL_mstgen gx ON gx.genoid=m.itemgroupoid WHERE adj.cmpcode='" & DDLBusUnit.SelectedValue & "' AND gx.gendesc='" & sType & "' "
        If IsValidPeriod() Then
            sSql &= " AND " & DDLDate.SelectedValue & " >='" & FilterPeriod1.Text & " 00:00:00' AND " & DDLDate.SelectedValue & " <='" & FilterPeriod2.Text & " 23:59:59' "
        End If
        sSql &= "ORDER BY matcode"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnstockadj")
        If dt.Rows.Count > 0 Then
            Session("TblListMat") = dt
            Session("TblListMatView") = Session("TblListMat")
            gvListMat.DataSource = Session("TblListMatView")
            gvListMat.DataBind()
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            showMessage("No Material data available for this time.", 2)
        End If
    End Sub 'OK

    Private Sub UpdateCheckedListMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMat") = dt
        End If
        If Session("TblListMatView") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMatView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMatView") = dt
        End If
    End Sub 'OK

    Private Sub ShowReport(ByVal sType As String)
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        If Not IsValidPeriod() Then
            Exit Sub
        End If
        Try
            Dim sWhere As String = " WHERE adj.cmpcode='" & DDLBusUnit.SelectedValue & "' AND " & DDLDate.SelectedValue & ">=CONVERT(DATETIME, '" & FilterPeriod1.Text & " 00:00:00') AND " & DDLDate.SelectedValue & "<=CONVERT(DATETIME, '" & FilterPeriod2.Text & " 23:59:59')"
            If FilterStockAdj.Text <> "" Then
                Dim sWhereCode As String = ""
                Dim sNo() As String = Split(FilterStockAdj.Text, ";")
                If sNo.Length > 0 Then
                    For C1 As Integer = 0 To sNo.Length - 1
                        If sNo(C1) <> "" Then
                            sWhereCode &= DDLStockAdj.SelectedValue & " LIKE '" & Tchar(sNo(C1)) & "' OR "
                        End If
                    Next
                    If sWhereCode <> "" Then
                        sWhereCode = Left(sWhereCode, sWhereCode.Length - 4)
                        sWhere &= " AND (" & sWhereCode & ")"
                    End If
                End If
            End If
            If FilterMaterial.Text <> "" Then
                Dim sWhereCode As String = ""
                Dim sNo() As String = Split(FilterMaterial.Text, ";")
                If sNo.Length > 0 Then
                    For C1 As Integer = 0 To sNo.Length - 1
                        If sNo(C1) <> "" Then
                            sWhereCode &= "itemcode LIKE '" & Tchar(sNo(C1)) & "%' OR "
                        End If
                    Next
                    If sWhereCode <> "" Then
                        sWhereCode = Left(sWhereCode, sWhereCode.Length - 4)
                        sWhere &= " AND (" & sWhereCode & ")"
                    End If
                End If
            End If
            If lbWarehouse.Items.Count > 0 Then
                Dim sOid As String = ""
                For C1 As Integer = 0 To lbWarehouse.Items.Count - 1
                    sOid &= lbWarehouse.Items(C1).Value & ","
                Next
                If sOid <> "" Then
                    sOid = Left(sOid, sOid.Length - 1)
                    sWhere &= " AND adj.mtrwhoid IN (" & sOid & ")"
                End If
            End If
            Dim sStatus As String = ""
            For C1 As Integer = 0 To cblStatus.Items.Count - 1
                If cblStatus.Items(C1).Selected Then
                    sStatus &= "'" & cblStatus.Items(C1).Value & "',"
                End If
            Next
            If sStatus <> "" Then
                sStatus = Left(sStatus, sStatus.Length - 1)
                sWhere &= " AND adj.stockadjstatus IN (" & sStatus & ")"
            End If
            Dim sSortBy As String = ""
            Dim sSplitSort() As String = DDLSortBy.SelectedValue.Split(",")
            If sSplitSort.Length > 0 Then
                For C1 As Integer = 0 To sSplitSort.Length - 1
                    If sSplitSort(C1) <> "" Then
                        sSortBy &= sSplitSort(C1) & " " & DDLOrder.SelectedValue & ", "
                    End If
                Next
            End If
            Dim sRptName As String = "StockAdjustmentReport"
            Dim dtReport As DataTable = Nothing
            Dim sExt As String = "", sGroup As String = ""
            If DDLGroupBy.SelectedIndex = 0 Then
                sGroup = "PerNo"
            ElseIf DDLGroupBy.SelectedIndex = 1 Then
                sGroup = "PerCode"
            End If
            If sType = "Print Excel" Then
                sExt = "Xls" : sGroup = ""
            End If
            report.Load(Server.MapPath(folderReport & "rptStockAdj" & sGroup & sExt & ".rpt"))

            'sSql = "SELECT * FROM ("
            sSql &= "SELECT adj.cmpcode,(SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=adj.cmpcode) [Business Unit], resfield1 [Draft No.], adj.stockadjdate [Adj. Date], stockadjno [Adj. No.], stockadjstatus [Status], UPPER(adj.createuser) [Create User], adj.createdate [Create Datetime], UPPER(ISNULL(adj.approvaluser, '')) [Approval User], ISNULL(adj.approvaldatetime, CAST('01/01/1900' AS DATETIME)) [Approval Date], adj.mtrwhoid [WH ID], g1.gendesc [Warehouse], adj.refname [Type], i.itemCode AS [Code], i.itemLongDescription AS [Material] , stockadjqty [Qty], stockadjqtybefore [Qty Before Adj], g2.gendesc [Unit], adj.stockadjnote [Reason], stockadjamtidr [Price], (stockadjqty - stockadjqtybefore) AS [Adj. Qty], itemoldcode AS [Old Code] FROM QL_trnstockadj adj INNER JOIN QL_mstgen g1 ON g1.genoid=adj.mtrwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=stockadjunit INNER JOIN QL_mstgen g3 ON g3.genoid=reasonoid INNER JOIN QL_mstitem i ON i.itemoid=adj.refoid " & sWhere & ""
            'sSql &= ") AS QL_tbldetail " & sWhere
            sSql &= " ORDER BY cmpcode, " & DDLGroupBy.SelectedValue
            If sSortBy <> "" Then
                sSql &= ", " & Left(sSortBy, sSortBy.Length - 2)
            End If
            dtReport = cKon.ambiltabel(sSql, "QL_tbldetail")
            report.SetDataSource(dtReport)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            cProc.SetDBLogonForReport(report)
            'report.PrintOptions.PaperSize = PaperSize.PaperFolio
            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, sRptName)
                report.Close()
                report.Dispose()
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, sRptName)
                report.Close()
                report.Dispose()
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.ToString, 1)
        End Try
    End Sub 'OK
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmStockAdj.aspx")
        End If
        If checkPagePermission("~\ReportForm\frmStockAdj.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Stock Adjustment Report"
        If Not Page.IsPostBack Then
            InitAllDDL()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            DDLGroupBy_SelectedIndexChanged(Nothing, Nothing)
        End If
    End Sub 'OK

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListStockAdj") Is Nothing And Session("WarningListStockAdj") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListStockAdj") Then
                Session("WarningListStockAdj") = Nothing
                mpeListStockAdj.Show()
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                mpeListMat.Show()
            End If
        End If
    End Sub 'OK

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLWarehouse()
    End Sub 'OK

    Protected Sub btnSearchStockAdj_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchStockAdj.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2) : Exit Sub
        End If
        FilterDDLListStockAdj.SelectedIndex = -1 : FilterTextListStockAdj.Text = "" : Session("TblListStockAdj") = Nothing : Session("TblListStockAdjView") = Nothing : gvListStockAdj.DataSource = Nothing : gvListStockAdj.DataBind()
        BindListStockAdj()
    End Sub 'OK

    Protected Sub btnClearStockAdj_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearStockAdj.Click
        FilterStockAdj.Text = ""
    End Sub 'OK

    Protected Sub btnFindListStockAdj_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListStockAdj.Click
        UpdateCheckedListStockAdj()
        Dim dt As DataTable = Session("TblListStockAdj")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = FilterDDLListStockAdj.SelectedValue & " LIKE '%" & Tchar(FilterTextListStockAdj.Text) & "%'"
        If dv.Count > 0 Then
            Session("TblListStockAdjView") = dv.ToTable
            gvListStockAdj.DataSource = Session("TblListStockAdjView")
            gvListStockAdj.DataBind()
            dv.RowFilter = ""
            mpeListStockAdj.Show()
        Else
            dv.RowFilter = ""
            Session("WarningListStockAdj") = "Stock Adjustment data can't be found!"
            showMessage(Session("WarningListStockAdj"), 2)
        End If
    End Sub 'OK

    Protected Sub btnAllListStockAdj_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListStockAdj.Click
        UpdateCheckedListStockAdj()
        FilterDDLListStockAdj.SelectedIndex = -1 : FilterTextListStockAdj.Text = ""
        Session("TblListStockAdjView") = Session("TblListStockAdj")
        gvListStockAdj.DataSource = Session("TblListStockAdjView")
        gvListStockAdj.DataBind()
        mpeListStockAdj.Show()
    End Sub 'OK

    Protected Sub gvListStockAdj_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListStockAdj.PageIndexChanging
        UpdateCheckedListStockAdj()
        gvListStockAdj.PageIndex = e.NewPageIndex
        gvListStockAdj.DataSource = Session("TblListStockAdjView")
        gvListStockAdj.DataBind()
        mpeListStockAdj.Show()
    End Sub 'OK

    Protected Sub cbHdrLMStockAdj_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For C1 As Integer = 0 To gvListStockAdj.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListStockAdj.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        mpeListStockAdj.Show()
    End Sub 'OK

    Protected Sub lbAddToListStockAdj_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListStockAdj.Click
        UpdateCheckedListStockAdj()
        Dim dt As DataTable = Session("TblListStockAdj")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = "checkvalue='True'"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                If DDLStockAdj.SelectedIndex = 0 Then
                    If dv(C1)("stockadjno").ToString <> "" Then
                        FilterStockAdj.Text &= dv(C1)("stockadjno").ToString & ";"
                    End If
                Else
                    FilterStockAdj.Text &= dv(C1)("resfield1").ToString & ";"
                End If
            Next
            If FilterStockAdj.Text <> "" Then
                FilterStockAdj.Text = Left(FilterStockAdj.Text, FilterStockAdj.Text.Length - 1)
            End If
            dv.RowFilter = ""
            cProc.SetModalPopUpExtender(btnHideListStockAdj, pnlListStockAdj, mpeListStockAdj, False)
        Else
            dv.RowFilter = ""
            Session("WarningListStockAdj") = "Please select some Stock Adjustment data first!"
            showMessage(Session("WarningListStockAdj"), 2)
        End If
    End Sub 'OK

    Protected Sub lbSelectAllListStockAdj_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSelectAllListStockAdj.Click
        UpdateCheckedListStockAdj()
        Dim dt As DataTable = Session("TblListStockAdjView")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            If DDLStockAdj.SelectedIndex = 0 Then
                If dt.Rows(C1)("stockadjno").ToString <> "" Then
                    FilterStockAdj.Text &= dt.Rows(C1)("stockadjno").ToString & ";"
                End If
            Else
                FilterStockAdj.Text &= dt.Rows(C1)("resfield1").ToString & ";"
            End If
        Next
        If FilterStockAdj.Text <> "" Then
            FilterStockAdj.Text = Left(FilterStockAdj.Text, FilterStockAdj.Text.Length - 1)
        End If
        cProc.SetModalPopUpExtender(btnHideListStockAdj, pnlListStockAdj, mpeListStockAdj, False)
    End Sub 'OK

    Protected Sub lbCloseListStockAdj_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListStockAdj.Click
        cProc.SetModalPopUpExtender(btnHideListStockAdj, pnlListStockAdj, mpeListStockAdj, False)
    End Sub 'OK

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2) : Exit Sub
        End If
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : Session("TblListMat") = Nothing : Session("TblListMatView") = Nothing : gvListMat.DataSource = Session("TblListMat") : gvListMat.DataBind()
        lblListMat.Text = "List Of " & DDLMatType.SelectedItem.Text
        BindListMat()
    End Sub 'OK

    Protected Sub btnClearMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearMat.Click
        FilterMaterial.Text = ""
    End Sub 'OK

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        UpdateCheckedListMat()
        Dim dt As DataTable = Session("TblListMat")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        If dv.Count > 0 Then
            Session("TblListMatView") = dv.ToTable
            gvListMat.DataSource = Session("TblListMatView")
            gvListMat.DataBind()
            dv.RowFilter = ""
            mpeListMat.Show()
        Else
            dv.RowFilter = ""
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub 'OK

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        UpdateCheckedListMat()
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        Session("TblListMatView") = Session("TblListMat")
        gvListMat.DataSource = Session("TblListMatView")
        gvListMat.DataBind()
        mpeListMat.Show()
    End Sub 'OK

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        UpdateCheckedListMat()
        gvListMat.PageIndex = e.NewPageIndex
        gvListMat.DataSource = Session("TblListMatView")
        gvListMat.DataBind()
        mpeListMat.Show()
    End Sub 'OK

    Protected Sub cbHdrLM_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For C1 As Integer = 0 To gvListMat.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        mpeListMat.Show()
    End Sub 'OK

    Protected Sub lbAddToListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListMat.Click
        UpdateCheckedListMat()
        Dim dt As DataTable = Session("TblListMat")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = "checkvalue='True'"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                FilterMaterial.Text &= dv(C1)("matcode").ToString & ","
            Next
            If FilterMaterial.Text <> "" Then
                FilterMaterial.Text = Left(FilterMaterial.Text, FilterMaterial.Text.Length - 1)
            End If
            dv.RowFilter = ""
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
        Else
            dv.RowFilter = ""
            Session("WarningListMat") = "Please select some Material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub 'OK

    Protected Sub lbSelectAllListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSelectAllListMat.Click
        UpdateCheckedListMat()
        Dim dt As DataTable = Session("TblListMat")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            FilterMaterial.Text &= dt.Rows(C1)("matcode").ToString & ";"
            'DDLMatType.SelectedValue & "-" &
        Next
        If FilterMaterial.Text <> "" Then
            FilterMaterial.Text = Left(FilterMaterial.Text, FilterMaterial.Text.Length - 1)
        End If
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub 'OK

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub 'OK

    Protected Sub btnAddWH_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddWH.Click
        If DDLWarehouse.SelectedValue <> "" Then
            If lbWarehouse.Items.Count > 0 Then
                If Not lbWarehouse.Items.Contains(lbWarehouse.Items.FindByValue(DDLWarehouse.SelectedValue)) Then
                    Dim objList As New ListItem
                    objList.Text = DDLWarehouse.SelectedItem.Text : objList.Value = DDLWarehouse.SelectedValue
                    lbWarehouse.Items.Add(objList)
                End If
            Else
                Dim objList As New ListItem
                objList.Text = DDLWarehouse.SelectedItem.Text : objList.Value = DDLWarehouse.SelectedValue
                lbWarehouse.Items.Add(objList)
            End If
        End If
    End Sub 'OK

    Protected Sub btnMinWH_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMinWH.Click
        If lbWarehouse.Items.Count > 0 Then
            Dim objList As ListItem = lbWarehouse.SelectedItem
            lbWarehouse.Items.Remove(objList)
        End If
    End Sub 'OK

    Protected Sub DDLGroupBy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLGroupBy.SelectedIndexChanged
        For C1 As Integer = 0 To DDLSortBy.Items.Count - 1
            DDLSortBy.Items(C1).Enabled = True
        Next
        DDLSortBy.Items(DDLGroupBy.SelectedIndex).Enabled = False
    End Sub 'OK

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub 'OK

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub 'OK

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub 'OK

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmStockAdj.aspx?awal=true")
    End Sub 'OK

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub 'OK

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub 'OK

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub 'OK

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not report Is Nothing Then
                If report.IsLoaded Then
                    report.Dispose()
                    report.Close()
                End If
            End If
        Catch ex As Exception
            report.Dispose()
            report.Close()
        End Try
    End Sub 'OK
#End Region

End Class
