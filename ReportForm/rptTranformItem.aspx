<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptTranformItem.aspx.vb" Inherits="ReportForm_rptTItem"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<table align="left" border="0" style="width: 976px; height: 168px">
        <tr>
            <td colspan="3" style="width: 100%; vertical-align: top; text-align: center;">                
                <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
                    width="100%">
                    <tr>
                        <th align="left" class="header" valign="center">
                            <asp:Label ID="LabelJudul" runat="server" CssClass="Title" Font-Bold="True" ForeColor="Maroon"
                                Text=".: Laporan Transformation Item" Font-Names="Verdana" Font-Size="21px"></asp:Label></th>
                    </tr>
                </table>
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left"></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right>Bussiness Unit&nbsp;:</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left"><asp:DropDownList id="DDLbranch" runat="server" CssClass="inpText" __designer:wfdid="w1" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right>Periode :</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left"><asp:TextBox id="range1" runat="server" CssClass="inpText" __designer:wfdid="w1" Width="75px"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w2"></asp:ImageButton>&nbsp;to <asp:TextBox id="range2" runat="server" CssClass="inpText" __designer:wfdid="w3" Width="75px"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w4"></asp:ImageButton>&nbsp;<asp:Label id="Label1" runat="server" Text="(MM/dd/yyyy)" CssClass="Important" __designer:wfdid="w5"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right>From Warehouse&nbsp;:</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left"><asp:DropDownList id="ddllocawal" runat="server" CssClass="inpText" __designer:wfdid="w2" Width="200px" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right>To Warehouse&nbsp;:</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left"><asp:DropDownList id="ddllocakhir" runat="server" CssClass="inpText" __designer:wfdid="w3" Width="200px">
    </asp:DropDownList> </TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right>Item Name&nbsp;:</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left" align=left><asp:TextBox id="barang" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w19" Width="169px" Enabled="False"></asp:TextBox> &nbsp;<asp:ImageButton id="barangsearch" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsBottom" __designer:wfdid="w20" Width="17px" Height="17px"></asp:ImageButton> &nbsp;<asp:ImageButton id="barangerase" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom" __designer:wfdid="w21"></asp:ImageButton> &nbsp;<asp:Label id="barangoid" runat="server" __designer:wfdid="w22" Visible="False"></asp:Label> </TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" id="TD4" align=right runat="server" Visible="false">Merk&nbsp;:</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left" id="TD3" align=left runat="server" Visible="false"><asp:TextBox id="merk" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w24" Width="169px" Enabled="False"></asp:TextBox> </TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" id="TD2" align=right runat="server" Visible="false">Tipe Transformasi :</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 50%; WHITE-SPACE: nowrap; TEXT-ALIGN: left" id="TD1" align=left runat="server" Visible="false"><asp:DropDownList id="ddltype" runat="server" CssClass="inpText" __designer:wfdid="w2" Width="110px"><asp:ListItem>Create</asp:ListItem>
<asp:ListItem>Release</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 100px; TEXT-ALIGN: center" align=center colSpan=2><asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnshowprint" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w36"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnpdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w37"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 5px" id="ibexcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w38"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 5px" id="ibClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w39"></asp:ImageButton> <BR /><BR /><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w40">
            <progresstemplate>
                <strong><span style="FONT-SIZE: 14pt; COLOR: #800080">Please Wait ....</span></strong>
                <BR />
                <asp:Image ID="Image4" runat="server" __designer:wfdid="w41" ImageAlign="AbsBottom" ImageUrl="~/Images/loadingbar.gif" />
            </progresstemplate>
        </asp:UpdateProgress> </TD></TR><TR><TD colSpan=2><ajaxToolkit:CalendarExtender id="CLE1" runat="server" __designer:wfdid="w201" TargetControlID="range1" PopupButtonID="ImageButton1" Format="MM/dd/yyyy" __designer:errorcontrol="'dd/MM/yyyy' could not be set on property 'Format'."></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CLE2" runat="server" __designer:wfdid="w202" TargetControlID="range2" PopupButtonID="ImageButton2" Format="MM/dd/yyyy" __designer:errorcontrol="'dd/MM/yyyy' could not be set on property 'Format'."></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MKE1" runat="server" __designer:wfdid="w203" TargetControlID="range1" __designer:errorcontrol="'id-ID' could not be set on property 'CultureName'." MaskType="Date" Mask="99/99/9999" ErrorTooltipEnabled="True" CultureName="id-ID">
        </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MKE2" runat="server" __designer:wfdid="w204" TargetControlID="range2" __designer:errorcontrol="'id-ID' could not be set on property 'CultureName'." MaskType="Date" Mask="99/99/9999" ErrorTooltipEnabled="True" CultureName="id-ID">
        </ajaxToolkit:MaskedEditExtender> <asp:Label id="Label2" runat="server" ForeColor="Red" __designer:wfdid="w42"></asp:Label> </TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2><TABLE style="MARGIN: auto"><TBODY><TR><TD style="TEXT-ALIGN: left"><CR:CrystalReportViewer id="CrystalReportViewer1" runat="server" __designer:wfdid="w270" HasZoomFactorList="False" HasViewList="False" HasToggleGroupTreeButton="False" HasSearchButton="False" HasPrintButton="False" HasGotoPageButton="False" HasExportButton="False" HasDrillUpButton="False" HasCrystalLogo="False" DisplayGroupTree="False" AutoDataBind="true"></CR:CrystalReportViewer> </TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
</contenttemplate>
<triggers>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnpdf"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnshowprint"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel><asp:UpdatePanel id="upListMat" runat="server"><contenttemplate>
<asp:Panel id="pnlListMat" runat="server" CssClass="modalBox" Width="900px" Visible="False"><TABLE style="WIDTH: 100%"><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Material" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMat" runat="server" Width="100%" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="matcode">Code</asp:ListItem>
<asp:ListItem Value="matlongdesc">Description</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" CssClass="inpText" Width="200px"></asp:TextBox> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:CheckBox id="cbCat01" runat="server" Text="Cat 1 :" Visible="False"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat01" runat="server" CssClass="inpText" Width="150px" AutoPostBack="True" Visible="False"></asp:DropDownList> <asp:CheckBox id="cbCat02" runat="server" Text="Cat 2 :" Visible="False"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat02" runat="server" CssClass="inpText" Width="150px" AutoPostBack="True" Visible="False"></asp:DropDownList> <asp:CheckBox id="cbCat03" runat="server" Text="Cat 3 :" Visible="False"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat03" runat="server" CssClass="inpText" Width="150px" AutoPostBack="True" Visible="False"></asp:DropDownList> <asp:CheckBox id="cbCat04" runat="server" Text="Cat 4 :" Visible="False"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat04" runat="server" CssClass="inpText" Width="150px" AutoPostBack="True" Visible="False"></asp:DropDownList></TD></TR></TABLE></asp:Panel></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNone" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewChecked" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:UpdateProgress id="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat"><ProgressTemplate>
<TABLE style="WIDTH: 200px"><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple">Load Data, Please Wait ...</TD></TR><TR><TD><asp:Image id="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif"></asp:Image></TD></TR></TABLE>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD class="Label" align=left colSpan=3>Show <asp:TextBox id="tbData" runat="server" CssClass="inpText" Width="25px"></asp:TextBox> Data Per Page <asp:LinkButton id="lbShowData" runat="server">>> View</asp:LinkButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat" runat="server" BorderColor="Black" ForeColor="#333333" Width="96%" DataKeyNames="matcode" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" PageSize="8" BorderStyle="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:CheckBox id="cbLM" runat="server" ToolTip='<%# eval("matoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="matcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matlongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
&nbsp;
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListMat" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TABLE><ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" Drag="True" PopupControlID="pnlListMat" PopupDragHandleControlID="lblTitleListMat" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender></asp:Panel> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel id="upPopUpMsg" runat="server">
                    <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" DropShadow="True" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    <tr>
        <td colspan="3" style="vertical-align: top; width: 100%">
            </td>
    </tr>
    </table>
</asp:Content>

