Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_COGM
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function UpdateCheckedKIK() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblKIK") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblKIK")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListKIK.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListKIK.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "womstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblKIK") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedKIK2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblKIKView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblKIK")
            Dim dtTbl2 As DataTable = Session("TblKIKView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListKIK.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListKIK.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "womstoid=" & cbOid
                                dtView2.RowFilter = "womstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblKIK") = dtTbl
                Session("TblKIKView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Function FillDDLWithALL(ByRef oDDLObject As DropDownList, ByVal sSql As String) As Boolean
        ' To fill data in dropdown list, return true when all data exist
        FillDDLWithALL = True
        oDDLObject.Items.Clear()
        oDDLObject.Items.Add("ALL")
        oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = "ALL"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            oDDLObject.Items.Add(xreader.GetValue(1))
            oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()
        ' Cek if all data exits
        If oDDLObject.Items.Count = 0 Then
            FillDDLWithALL = False
        End If
        Return FillDDLWithALL
    End Function

	Private Sub BindListKIK()
		If DDLSource.SelectedValue = "PRODUCTION" Then
			sSql = "SELECT DISTINCT 'False' AS checkvalue, wom.womstoid AS womstoid, wom.wono AS wono, CONVERT(VARCHAR(10), wom.wodate, 101) AS wodate, wom.womstnote AS womstnote, som.sono AS soitemno FROM QL_trnwomst wom INNER JOIN QL_trnwodtl1 wod1 ON wom.womstoid=wod1.womstoid INNER JOIN QL_mstitem m ON m.itemoid=wod1.itemoid INNER JOIN QL_trnsomst som ON som.somstoid=wod1.soitemmstoid "
			sSql &= " WHERE wom.cmpcode='" & DDLBusUnit.SelectedValue & "'"

			If DDLDivision.SelectedValue <> "ALL" Then
				sSql &= " AND som.groupoid=" & DDLDivision.SelectedValue
			End If

			If IsValidPeriod() Then
				sSql &= " AND wom.wodate>='" & FilterPeriod1.Text & " 00:00:00' AND wom.wodate<='" & FilterPeriod2.Text & " 23:59:59'"
			Else
				Exit Sub
			End If
			sSql &= " AND wom.womststatus <> 'Cancel'"
			sSql &= " ORDER BY wodate DESC, womstoid DESC"
		Else

			sSql = "SELECT DISTINCT 'False' AS checkvalue, wom.transformmstoid AS womstoid, wom.transformno AS wono, CONVERT(VARCHAR(10), wom.transformdate, 101) AS wodate, wom.transformmstnote AS womstnote, '' AS soitemno FROM QL_trntransformmst wom INNER JOIN QL_trntransformdtl1 wod1 ON wom.transformmstoid=wod1.transformmstoid INNER JOIN QL_mstitem m ON m.itemoid=wod1.transformdtl1refoid "
			sSql &= " WHERE wom.cmpcode='" & DDLBusUnit.SelectedValue & "'"
			If IsValidPeriod() Then
				sSql &= " AND wom.transformdate > ='" & FilterPeriod1.Text & " 00:00:00' AND wom.transformdate < ='" & FilterPeriod2.Text & " 23:59:59'"
			Else
				Exit Sub
			End If
			sSql &= " AND wom.transformmststatus <> 'Cancel'"
			sSql &= " ORDER BY wodate DESC, womstoid DESC"
		End If

		Session("TblKIK") = cKon.ambiltabel(sSql, "QL_trnwomst")
	End Sub

    Private Sub ShowReport(ByVal sType As String)
        Dim sWhere As String = ""
        Dim rptName As String = ""
        Dim Dtl As String = ""
        Dim Join As String = ""
        Dim sDate As String = ""
        Dim sDate2 As String = ""
        Try
            If FilterDDLType.SelectedValue = "Summary" Then
                If DDLSource.SelectedValue = "PRODUCTION" Then
                    If sType = "Print Excel" Then
                        report.Load(Server.MapPath(folderReport & "rptCOGM.rpt"))
                    Else
                        report.Load(Server.MapPath(folderReport & "rptCOGM.rpt"))
                    End If
                Else
                    If sType = "Print Excel" Then
                        report.Load(Server.MapPath(folderReport & "rptCOGMTransformXls.rpt"))
                    Else
                        report.Load(Server.MapPath(folderReport & "rptCOGMTransform.rpt"))
                    End If
                End If
            Else
                If DDLSource.SelectedValue = "PRODUCTION" Then
                    If sType = "Print Excel" Then
                        report.Load(Server.MapPath(folderReport & "rptCOGM_DtlXls.rpt"))
                    Else
                        report.Load(Server.MapPath(folderReport & "rptCOGM_Dtl.rpt"))
                    End If
                Else
                    If sType = "Print Excel" Then
                        report.Load(Server.MapPath(folderReport & "rptCOGMTransform_DtlXls.rpt"))
                    Else
                        report.Load(Server.MapPath(folderReport & "rptCOGMTransform_Dtl.rpt"))
                    End If
                End If
            End If
            rptName = "CostofGoodManufacture"

            If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
                If IsValidPeriod() Then
                    sDate = FilterPeriod1.Text
                    sDate2 = FilterPeriod2.Text
                Else
                    Exit Sub
                End If
            End If

            Dim arType() As String = {""}
            If DDLSource.SelectedValue = "PRODUCTION" Then
                If FilterDDLType.SelectedValue = "Summary" Then
                    sSql = " SELECT [Oid], [KIK No.], [Status], ((DMAwal + DLAwal + FOHAwal + KANCINGAwal + KAINAwal + LABELAwal + OTHERAwal) - ConfirmAwal) [Saldo Awal], DM, DL, FOH, KANCING, KAIN, LABEL, OTHER, Confirm, ConfirmQty, (((DMAwal + DLAwal + FOHAwal + KANCINGAwal + KAINAwal + LABELAwal + OTHERAwal) - ConfirmAwal) + ((DM + DL + FOH + KANCING + KAIN + LABEL + OTHER) - Confirm)) AS [Saldo Akhir], [Code], [Finish Good], [Finish Good Qty], [Unit], [Tgl Closed] FROM (" & _
                           "SELECT wom.womstoid AS [Oid], wom.wono AS [KIK No.], wom.womststatus [Status], (ISNULL(SUM(DMAwal), 0.0) + cutoffamt) DMAwal, ISNULL(SUM(DM), 0.0) DM, ISNULL(SUM(DLAwal), 0.0) DLAwal, ISNULL(SUM(DL), 0.0) DL, ISNULL(SUM(FOHAwal), 0.0) FOHAwal, ISNULL(SUM(FOH),0.0) FOH, ISNULL(SUM(KANCINGAwal), 0.0) KANCINGAwal, ISNULL(SUM(KANCING),0.0) KANCING, ISNULL(SUM(KAINAwal), 0.0) KAINAwal, ISNULL(SUM(KAIN),0.0) KAIN, ISNULL(SUM(LABELAwal), 0.0) LABELAwal, ISNULL(SUM(LABEL),0.0) LABEL, ISNULL(SUM(OTHERAwal), 0.0) OTHERAwal, ISNULL(SUM(OTHER),0.0) OTHER, ISNULL(SUM(ConfirmAwal),0.0) ConfirmAwal, ISNULL(SUM(Confirm),0.0) Confirm, ISNULL(SUM(ConfirmQty), 0.0) ConfirmQty, itemcode [Code], itemlongdesc [Finish Good], wodtl1qty [Finish Good Qty], gendesc [Unit], Division, groupoid, woclosingdate [Tgl Closed] FROM QL_trnwomst wom "
                    sSql &= "  INNER JOIN (SELECT DISTINCT wod1.cmpcode, wod1.womstoid, itemcode, itemLongDescription AS itemlongdesc, ISNULL((SELECT SUM(wodx.wodtl1qty) FROM QL_trnwodtl1 wodx WHERE wodx.womstoid=wod1.womstoid),0.0) AS wodtl1qty, wod1.wodtl1unitoid, g.gendesc, 0 AS groupoid, /*(groupcode + ' - ' + groupdesc)*/ '' AS Division FROM QL_trnwodtl1 wod1 /*INNER JOIN QL_trnsomst som ON som.cmpcode=wod1.cmpcode AND som.somstoid=wod1.soitemmstoid*/ INNER JOIN QL_mstitem m ON wod1.itemoid=m.itemoid INNER JOIN QL_mstgen g ON g.genoid=wod1.wodtl1unitoid /*LEFT JOIN QL_mstdeptgroup dept ON som.cmpcode=dept.cmpcode AND dept.groupoid=som.groupoid*/) AS tblItem ON tblItem.cmpcode=wom.cmpcode AND tblItem.womstoid=wom.womstoid "

                    ' SALDO AWAL DM - START
                    sSql &= " LEFT JOIN (SELECT cmpcode, womstoid, ISNULL(SUM(DMAwal), 0.0) DMAwal FROM ("
                    ' AMBIL DARI MATERIAL USAGE (RAW, GEN, SP, KAYU)
                    For C1 As Integer = 0 To arType.Length - 1
                        sSql &= "SELECT mud.cmpcode, mum.womstoid, ISNULL(SUM(usageqty * usagevalueidr), 0.0) DMAwal FROM QL_trnusagedtl mud INNER JOIN QL_trnusagemst mum ON mum.cmpcode=mud.cmpcode AND mum.usagemstoid=mud.usagemstoid WHERE mum.usagemststatus<>'In Process' AND mum.updtime<CAST('" & sDate & " 00:00:00' AS DATETIME) AND mum.usagemstoid>0 GROUP BY mud.cmpcode, mum.womstoid UNION ALL "
                    Next
                    ' AMBIL DARI BERITA ACARA USAGE - NEW BERITA ACARA RETURN
                    sSql &= " SELECT cmpcode, womstoid, SUM(DMAwal) DMAwal FROM (SELECT bad.cmpcode, bam.womstoid, ISNULL(((acaraqty - ISNULL((SELECT SUM(retbaqty) FROM QL_trnreturnbadtl retd INNER JOIN QL_trnreturnbamst retm ON retm.cmpcode=retd.cmpcode AND retm.retbamstoid=retd.retbamstoid WHERE retd.cmpcode=bad.cmpcode AND retbamststatus<>'In Process' AND retd.acaradtloid=bad.acaradtloid AND retd.acaramstoid=bad.acaramstoid), 0.0)) * acaravalueidr), 0.0) DMAwal FROM QL_trnbrtacaradtl bad INNER JOIN QL_trnbrtacaramst bam ON bam.cmpcode=bad.cmpcode AND bad.acaramstoid=bam.acaramstoid WHERE acaramststatus IN ('Approved', 'Closed') AND acaratype<>'Material Return' AND bam.updtime<CAST('" & sDate & " 00:00:00' AS DATETIME) AND bam.acaramstoid>0) AS QL_trnbrtacaradtl GROUP BY cmpcode, womstoid UNION ALL "
                    ' AMBIL DARI BERITA ACARA RETUR
                    sSql &= " SELECT bad.cmpcode, bam.womstoid, (ISNULL(SUM(acaraqty * acaravalueidr), 0.0) * -1) DMAwal FROM QL_trnbrtacaradtl bad INNER JOIN QL_trnbrtacaramst bam ON bam.cmpcode=bad.cmpcode AND bad.acaramstoid=bam.acaramstoid WHERE acaramststatus IN ('Approved', 'Closed') AND acaratype='Material Return' AND bam.updtime<CAST('" & sDate & " 00:00:00' AS DATETIME) AND bam.acaramstoid>0 GROUP BY bad.cmpcode, bam.womstoid "
                    sSql &= ") AS tblDMAwal_Tmp GROUP BY cmpcode, womstoid"
                    sSql &= ") AS tblDMAwal ON tblDMAwal.cmpcode=wom.cmpcode AND tblDMAwal.womstoid=wom.womstoid"
                    ' SALDO AWAL DM - END

                    ' DM - START
                    sSql &= " LEFT JOIN (SELECT cmpcode, womstoid, ISNULL(SUM(DM), 0.0) DM FROM ("
                    ' AMBIL DARI MATERIAL USAGE (RAW, GEN, SP,KAYU)
                    For C1 As Integer = 0 To arType.Length - 1
                        sSql &= "SELECT mud.cmpcode, mum.womstoid, ISNULL(SUM(usageqty * usagevalueidr), 0.0) DM FROM QL_trnusagedtl mud INNER JOIN QL_trnusagemst mum ON mum.cmpcode=mud.cmpcode AND mum.usagemstoid=mud.usagemstoid WHERE mum.usagemststatus<>'In Process' AND mum.updtime>=CAST('" & sDate & " 00:00:00' AS DATETIME) AND mum.updtime<=CAST('" & sDate2 & " 23:59:59' AS DATETIME) AND mum.usagemstoid>0 GROUP BY mud.cmpcode, mum.womstoid UNION ALL "
                    Next
                    ' AMBIL DARI BERITA ACARA USAGE - NEW BERITA ACARA RETURN
                    sSql &= " SELECT cmpcode, womstoid, SUM(DMAwal) DMAwal FROM (SELECT bad.cmpcode, bam.womstoid, ISNULL(((acaraqty - ISNULL((SELECT SUM(retbaqty) FROM QL_trnreturnbadtl retd INNER JOIN QL_trnreturnbamst retm ON retm.cmpcode=retd.cmpcode AND retm.retbamstoid=retd.retbamstoid WHERE retd.cmpcode=bad.cmpcode AND retbamststatus<>'In Process' AND retd.acaradtloid=bad.acaradtloid AND retd.acaramstoid=bad.acaramstoid), 0.0)) * acaravalueidr), 0.0) DMAwal FROM QL_trnbrtacaradtl bad INNER JOIN QL_trnbrtacaramst bam ON bam.cmpcode=bad.cmpcode AND bad.acaramstoid=bam.acaramstoid WHERE acaramststatus IN ('Approved', 'Closed') AND acaratype<>'Material Return' AND bam.updtime>=CAST('" & sDate & " 00:00:00' AS DATETIME) AND bam.updtime<=CAST('" & sDate2 & " 23:59:59' AS DATETIME) AND bam.acaramstoid>0) AS QL_trnbrtacaradtl GROUP BY cmpcode, womstoid UNION ALL "
                    ' AMBIL DARI BERITA ACARA RETUR
                    sSql &= " SELECT bad.cmpcode, bam.womstoid, (ISNULL(SUM(acaraqty * acaravalueidr), 0.0) * -1) DM FROM QL_trnbrtacaradtl bad INNER JOIN QL_trnbrtacaramst bam ON bam.cmpcode=bad.cmpcode AND bad.acaramstoid=bam.acaramstoid WHERE acaramststatus IN ('Approved', 'Closed') AND acaratype='Material Return' AND bam.updtime>=CAST('" & sDate & " 00:00:00' AS DATETIME) AND bam.updtime<=CAST('" & sDate2 & " 23:59:59' AS DATETIME) AND bam.acaramstoid>0 GROUP BY bad.cmpcode, bam.womstoid "
                    sSql &= ") AS tblDM_Tmp GROUP BY cmpcode, womstoid"
                    sSql &= ") AS tblDM ON tblDM.cmpcode=wom.cmpcode AND tblDM.womstoid=wom.womstoid"
                    ' DM - END

                    ' SALDO AWAL DL - START
                    sSql &= " LEFT JOIN (SELECT cmpcode, womstoid, ISNULL(SUM(DLAwal), 0.0) DLAwal, ISNULL(SUM(KANCINGAwal), 0.0) KANCINGAwal, ISNULL(SUM(KAINAwal), 0.0) KAINAwal, ISNULL(SUM(LABELAwal), 0.0) LABELAwal, ISNULL(SUM(OTHERAwal), 0.0) OTHERAwal FROM ("
                    ' AMBIL DARI PRODUCTION RESULT
                    sSql &= " SELECT resd.cmpcode, resd.womstoid, ISNULL(SUM(prodresdlcamt), 0.0) DLAwal, ISNULL(SUM(prodreskancingamt), 0.0) KANCINGAwal, ISNULL(SUM(prodreskainamt), 0.0) KAINAwal, ISNULL(SUM(prodreslabelamt), 0.0) LABELAwal, ISNULL(SUM(prodresotheramt), 0.0) OTHERAwal FROM QL_trnprodresdtl resd INNER JOIN QL_trnprodresmst resm ON resm.cmpcode=resd.cmpcode AND resm.prodresmstoid=resd.prodresmstoid WHERE prodresmststatus<>'In Process' AND resm.updtime<CAST('" & sDate & " 00:00:00' AS DATETIME) AND resm.prodresmstoid>0 GROUP BY resd.cmpcode, resd.womstoid "
                    sSql &= ") AS tblDLAwal_Tmp GROUP BY cmpcode, womstoid"
                    sSql &= ") AS tblDLAwal ON tblDLAwal.cmpcode=wom.cmpcode AND tblDLAwal.womstoid=wom.womstoid"
                    ' SALDO AWAL DL - END

                    ' DL - START
                    sSql &= " LEFT JOIN (SELECT cmpcode, womstoid, ISNULL(SUM(DL), 0.0) DL, ISNULL(SUM(KANCING), 0.0) KANCING, ISNULL(SUM(KAIN), 0.0) KAIN, ISNULL(SUM(LABEL), 0.0) LABEL, ISNULL(SUM(OTHER), 0.0) OTHER FROM ("
                    ' AMBIL DARI PRODUCTION RESULT
                    sSql &= " SELECT resd.cmpcode, resd.womstoid, ISNULL(SUM(prodresdlcamt), 0.0) DL, ISNULL(SUM(prodreskancingamt), 0.0) KANCING, ISNULL(SUM(prodreskainamt), 0.0) KAIN, ISNULL(SUM(prodreslabelamt), 0.0) LABEL, ISNULL(SUM(prodresotheramt), 0.0) OTHER  FROM QL_trnprodresdtl resd INNER JOIN QL_trnprodresmst resm ON resm.cmpcode=resd.cmpcode AND resm.prodresmstoid=resd.prodresmstoid WHERE prodresmststatus<>'In Process' AND resm.updtime>=CAST('" & sDate & " 00:00:00' AS DATETIME) AND resm.updtime<=CAST('" & sDate2 & " 23:59:59' AS DATETIME) AND resm.prodresmstoid>0 GROUP BY resd.cmpcode, resd.womstoid "
                    sSql &= ") AS tblDL_Tmp GROUP BY cmpcode, womstoid"
                    sSql &= ") AS tblDL ON tblDL.cmpcode=wom.cmpcode AND tblDL.womstoid=wom.womstoid"
                    ' DL - END

                    ' SALDO AWAL FOH - START
                    sSql &= " LEFT JOIN (SELECT cmpcode, womstoid, ISNULL(SUM(FOHAwal), 0.0) FOHAwal FROM ("
                    ' AMBIL DARI PRODUCTION RESULT
                    sSql &= " SELECT resd.cmpcode, resd.womstoid, ISNULL(SUM(bordiramt), 0.0) FOHAwal FROM QL_trnprodresdtl resd INNER JOIN QL_trnprodresmst resm ON resm.cmpcode=resd.cmpcode AND resm.prodresmstoid=resd.prodresmstoid WHERE prodresmststatus<>'In Process' AND resm.updtime<CAST('" & sDate & " 00:00:00' AS DATETIME) AND resm.prodresmstoid>0 GROUP BY resd.cmpcode, resd.womstoid "
                    sSql &= ") AS tblFOHAwal_Tmp GROUP BY cmpcode, womstoid"
                    sSql &= ") AS tblFOHAwal ON tblFOHAwal.cmpcode=wom.cmpcode AND tblFOHAwal.womstoid=wom.womstoid"
                    ' SALDO AWAL FOH - END

                    ' FOH - START
                    sSql &= " LEFT JOIN (SELECT cmpcode, womstoid, ISNULL(SUM(FOH), 0.0) FOH FROM ("
                    ' AMBIL DARI PRODUCTION RESULT
                    sSql &= " SELECT resd.cmpcode, resd.womstoid, ISNULL(SUM(bordiramt), 0.0) FOH FROM QL_trnprodresdtl resd INNER JOIN QL_trnprodresmst resm ON resm.cmpcode=resd.cmpcode AND resm.prodresmstoid=resd.prodresmstoid WHERE prodresmststatus<>'In Process' AND resm.updtime>=CAST('" & sDate & " 00:00:00' AS DATETIME) AND resm.updtime<=CAST('" & sDate2 & " 23:59:59' AS DATETIME) AND resm.prodresmstoid>0 GROUP BY resd.cmpcode, resd.womstoid "
                    sSql &= ") AS tblFOH_Tmp GROUP BY cmpcode, womstoid"
                    sSql &= ") AS tblFOH ON tblFOH.cmpcode=wom.cmpcode AND tblFOH.womstoid=wom.womstoid"
                    ' FOH - END

                    ' SALDO AWAL CONFIRM - START
                    sSql &= " LEFT JOIN (SELECT conf.cmpcode, conf.womstoid, SUM(confirmvalueidr * confirmqty) ConfirmAwal FROM QL_trnprodresconfirm conf WHERE conf.updtime<CAST('" & sDate & " 00:00:00' AS DATETIME) GROUP BY conf.cmpcode, conf.womstoid"
                    sSql &= ") AS tblConfirmAwal ON tblConfirmAwal.womstoid=wom.womstoid AND tblConfirmAwal.cmpcode=wom.cmpcode"
                    ' SALDO AWAL CONFIRM - END

                    ' CONFIRM - START
                    sSql &= " LEFT JOIN (SELECT conf.cmpcode, conf.womstoid, SUM(confirmvalueidr * (confirmqty+confirmqty_sisa)) Confirm, SUM(confirmqty) AS ConfirmQty FROM QL_trnprodresconfirm conf WHERE conf.updtime>=CAST('" & sDate & " 00:00:00' AS DATETIME) AND conf.updtime<=CAST('" & sDate2 & " 23:59:59' AS DATETIME) GROUP BY conf.cmpcode, conf.womstoid, confirmdm, confirmdl, confirmohd"
                    sSql &= ") AS tblConfirm ON tblConfirm.womstoid=wom.womstoid AND tblConfirm.cmpcode=wom.cmpcode"
                    ' CONFIRM - END

                    sSql &= " WHERE wom.cmpcode='" & DDLBusUnit.SelectedValue & "'"
                    If wono.Text <> "" Then
                        Dim sResNo() As String = Split(wono.Text, ";")
                        sSql &= " AND ("
                        For C1 As Integer = 0 To sResNo.Length - 1
                            sSql &= " wono LIKE '" & Tchar(sResNo(C1)) & "'"
                            If C1 < sResNo.Length - 1 Then
                                sSql &= " OR"
                            End If
                        Next
                        sSql &= ")"
                    End If
                    If FilterMaterial.Text <> "" Then
                        Dim sWhereCode As String = ""
                        Dim sNo() As String = Split(FilterMaterial.Text, ";")
                        If sNo.Length > 0 Then
                            For C1 As Integer = 0 To sNo.Length - 1
                                If sNo(C1) <> "" Then
                                    sWhereCode &= "itemcode LIKE '" & Tchar(sNo(C1)) & "' OR "
                                End If
                            Next
                            If sWhereCode <> "" Then
                                sWhereCode = Left(sWhereCode, sWhereCode.Length - 4)
                                sSql &= " AND (" & sWhereCode & ")"
                            End If
                        End If
                    End If
                    If DDLDivision.SelectedValue <> "ALL" Then
                        sSql &= " AND groupoid=" & DDLDivision.SelectedValue
                    End If
                    sSql &= " AND womststatus<>'Cancel'"
                    sSql &= " AND ((woclosingdate>=CAST('" & FilterPeriod1.Text & " 00:00:00' AS DATETIME)) OR (woclosingdate=CAST('01/01/1900' AS DATETIME)))"
                    sSql &= " AND wom.updtime<=CAST('" & FilterPeriod2.Text & " 23:59:59' AS DATETIME)"
                    If Not cbAllowNull.Checked Then
                        sSql &= " AND (ISNULL(DM,0)<>0 OR ISNULL(DL,0)<>0 OR ISNULL(FOH,0)<>0 OR ISNULL(Confirm,0)<>0) "
                    End If
                    sSql &= " GROUP BY wom.womstoid, wono, womststatus, cutoffamt, itemcode, itemlongdesc, wodtl1qty, gendesc, Division, groupoid, woclosingdate) AS tblKIK ORDER BY [KIK No.] ASC, [Oid] ASC"

                Else
                    sSql = "  SELECT wom.cmpcode,(SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=wom.cmpcode) [BUSSINESS UNIT],wom.womstoid AS [Oid], wom.wono AS [KIK No.], wom.womststatus [Status], [Ref No], [Ref Date], [DM IDR], [DM USD], [DL IDR], [DL USD], [FOH IDR], [FOH USD], [Confirm Qty], [Confirm IDR], [Confirm USD], itemcode [Code], itemlongdesc [Finish Good], wodtl1qty [Finish Good Qty], gendesc [Unit], Division, groupoid, woclosingdate [Tgl Closed], [Code_Mat], [Description], [Qty], [Value IDR], [Value USD], [Amount IDR],[Amount USD], [Unit_Mat], [KANCING IDR], [KAIN IDR], [LABEL IDR], [OTHER IDR] " & _
                    " FROM QL_trnwomst wom INNER JOIN (SELECT DISTINCT wod1.cmpcode, wod1.womstoid, itemcode, itemLongDescription AS itemlongdesc, ISNULL((SELECT SUM(wodx.wodtl1qty) FROM QL_trnwodtl1 wodx WHERE wodx.womstoid=wod1.womstoid),0.0) AS wodtl1qty, wod1.wodtl1unitoid, g.gendesc, 0 AS groupoid, '' AS Division FROM QL_trnwodtl1 wod1 INNER JOIN QL_mstitem m ON wod1.itemoid=m.itemoid INNER JOIN QL_mstgen g ON g.genoid=wod1.wodtl1unitoid) AS tblItem ON tblItem.cmpcode=wom.cmpcode AND tblItem.womstoid=wom.womstoid " & _
                    " LEFT JOIN (SELECT cmpcode, womstoid, [Ref No], [Ref Date], ISNULL(SUM([DM IDR]), 0.0) [DM IDR] , ISNULL(SUM([DM USD]), 0.0) [DM USD], ISNULL(SUM([DL IDR]), 0.0) [DL IDR] , ISNULL(SUM([DL USD]), 0.0) [DL USD], ISNULL(SUM([FOH IDR]), 0.0) [FOH IDR] , ISNULL(SUM([FOH USD]), 0.0) [FOH USD], ISNULL(SUM([Confirm Qty]),0.0) [Confirm Qty],ISNULL(SUM([Confirm IDR]), 0.0) [Confirm IDR],ISNULL(SUM([Confirm USD]), 0.0) [Confirm USD], [Code_Mat], [Description], [Qty], [Value IDR], [Value USD], [Amount IDR],[Amount USD], [Unit_Mat], ISNULL(SUM([KANCING IDR]), 0.0) [KANCING IDR], ISNULL(SUM([KAIN IDR]), 0.0) [KAIN IDR], ISNULL(SUM([LABEL IDR]), 0.0) [LABEL IDR], ISNULL(SUM([OTHER IDR]), 0.0) [OTHER IDR] FROM ("
                    'MATERIAL USAGE RAW,GEN,SP,KAYU
                    For C1 As Integer = 0 To arType.Length - 1
                        sSql &= " SELECT mud.cmpcode, mum.womstoid, usageno [Ref No], mum.updtime [Ref Date], ISNULL(SUM(usageqty * usagevalueidr), 0.0) [DM IDR], ISNULL(SUM(usageqty * usagevalueusd), 0.0) [DM USD] , 0.0 AS  [DL IDR], 0.0 AS  [DL USD], 0.0 AS [FOH IDR], 0.0 AS [FOH USD], 0.0 [Confirm Qty] ,0.0 [Confirm IDR],0.0 [Confirm USD], itemcode AS [Code_Mat], itemlongdescription AS [Description], usageqty AS [Qty], (CASE WHEN usageunitoid=itemunit1 THEN usagevalueidr WHEN usageunitoid=itemunit2 THEN (usagevalueidr*unit2unit1conversion) ELSE (usagevalueidr*unit3unit1conversion) END) AS [Value IDR], (CASE WHEN usageunitoid=itemunit1 THEN usagevalueusd WHEN usageunitoid=itemunit2 THEN (usagevalueusd*unit2unit1conversion) ELSE (usagevalueusd*unit3unit1conversion) END) AS [Value USD], (usagevalueidr*usageqty) AS [Amount IDR], (usagevalueusd*usageqty) AS [Amount USD], gx.gendesc AS [Unit_Mat], 0.0 AS [KANCING IDR], 0.0 AS [KAIN IDR], 0.0 AS [LABEL IDR], 0.0 AS [OTHER IDR] FROM QL_trnusagedtl mud INNER JOIN QL_trnusagemst mum ON mum.cmpcode=mud.cmpcode AND mum.usagemstoid=mud.usagemstoid INNER JOIN QL_mstitem m ON m.itemoid=mud.matoid INNER JOIN QL_mstgen gx ON gx.genoid=mud.usageunitoid WHERE mum.usagemststatus<>'In Process' AND mum.usagemstoid>0 GROUP BY mud.cmpcode, mum.womstoid, usageno , mum.updtime, itemcode, itemlongdescription, usageqty, usagevalueidr, usagevalueusd, gx.gendesc, usageunitoid, itemunit1, itemunit2, unit2unit1conversion, unit3unit1conversion UNION ALL "
                    Next
                    'BERITA ACARA USAGE
                    sSql &= " SELECT bad.cmpcode, bam.womstoid, bam.acarano [Ref No], bam.approvaldatetime [Ref Date], ISNULL(SUM(acaraqty * acaravalueidr), 0.0) [DM IDR], ISNULL(SUM(acaraqty * acaravalueusd), 0.0) [DM USD] , 0.0 AS  [DL IDR], 0.0 AS  [DL USD], 0.0 AS [FOH IDR], 0.0 AS [FOH USD], 0.0 [Confirm Qty],0.0 [Confirm IDR],0.0 [Confirm USD], itemcode AS [Code_Mat], itemlongdescription AS [Description], acaraqty AS [Qty], (CASE WHEN acaraunitoid=itemunit1 THEN acaravalueidr WHEN acaraunitoid=itemunit2 THEN (acaravalueidr*unit2unit1conversion) ELSE (acaravalueidr*unit3unit1conversion) END) AS [Value IDR], (CASE WHEN acaraunitoid=itemunit1 THEN acaravalueusd WHEN acaraunitoid=itemunit2 THEN (acaravalueusd*unit2unit1conversion) ELSE (acaravalueusd*unit3unit1conversion) END) AS [Value USD], (acaravalueidr*acaraqty) AS [Amount IDR], (acaravalueusd*acaraqty) AS [Amount USD], gx.gendesc AS [Unit_Mat], 0.0 AS [KANCING IDR], 0.0 AS [KAIN IDR], 0.0 AS [LABEL IDR], 0.0 AS [OTHER IDR] FROM QL_trnbrtacaradtl bad INNER JOIN QL_trnbrtacaramst bam ON bam.cmpcode=bad.cmpcode AND bad.acaramstoid=bam.acaramstoid INNER JOIN QL_mstitem m ON m.itemoid=bad.acararefoid INNER JOIN QL_mstgen gx ON gx.genoid=bad.acaraunitoid WHERE acaramststatus IN ('Approved', 'Closed') AND acaratype<>'Material Return' AND bam.acaramstoid>0 GROUP BY bad.cmpcode, bam.womstoid, bam.acarano, bam.approvaldatetime, itemcode, itemlongdescription, acaraqty, acaravalueidr, acaravalueusd, gx.gendesc, acaraunitoid, itemunit1, itemunit2, unit2unit1conversion, unit3unit1conversion UNION ALL"
                    'BERITA ACARA RETUR
                    sSql &= " SELECT bad.cmpcode, bam.womstoid, bam.acarano [Ref No], bam.approvaldatetime [Ref Date], (ISNULL(SUM(acaraqty * acaravalueidr), 0.0) * -1) [DM IDR], (ISNULL(SUM(acaraqty * acaravalueusd), 0.0) * -1) [DM USD] , 0.0 AS  [DL IDR], 0.0 AS  [DL USD], 0.0 AS [FOH IDR], 0.0 AS [FOH USD], 0.0 [Confirm Qty],0.0 [Confirm IDR],0.0 [Confirm USD], itemcode AS [Code_Mat], itemlongdescription AS [Description], acaraqty AS [Qty], (CASE WHEN acaraunitoid=itemunit1 THEN acaravalueidr WHEN acaraunitoid=itemunit2 THEN (acaravalueidr*unit2unit1conversion) ELSE (acaravalueidr*unit3unit1conversion) END) AS [Value IDR], (CASE WHEN acaraunitoid=itemunit1 THEN acaravalueusd WHEN acaraunitoid=itemunit2 THEN (acaravalueusd*unit2unit1conversion) ELSE (acaravalueusd*unit3unit1conversion) END) AS [Value USD], (acaravalueidr*acaraqty)*-1 AS [Amount IDR], (acaravalueusd*acaraqty)*-1 AS [Amount USD], gx.gendesc AS [Unit_Mat], 0.0 AS [KANCING IDR], 0.0 AS [KAIN IDR], 0.0 AS [LABEL IDR], 0.0 AS [OTHER IDR] FROM QL_trnbrtacaradtl bad INNER JOIN QL_trnbrtacaramst bam ON bam.cmpcode=bad.cmpcode AND bad.acaramstoid=bam.acaramstoid INNER JOIN QL_mstitem m ON m.itemoid=bad.acararefoid INNER JOIN QL_mstgen gx ON gx.genoid=bad.acaraunitoid WHERE acaramststatus IN ('Approved', 'Closed') AND acaratype='Material Return' AND bam.acaramstoid>0 GROUP BY bad.cmpcode, bam.womstoid, bam.acarano, bam.approvaldatetime, itemcode, itemlongdescription, acaraqty, acaravalueidr, acaravalueusd, gx.gendesc, acaraunitoid, itemunit1, itemunit2, unit2unit1conversion, unit3unit1conversion UNION ALL"
                    'RETURN BERITA ACARA
                    sSql &= " SELECT bad.cmpcode, bm.womstoid, bam.retbano [Ref No], bam.updtime [Ref Date], (ISNULL(SUM(retbaqty * retbavalueidr), 0.0) * -1) [DM IDR], (ISNULL(SUM(retbaqty * retbavalueusd), 0.0) * -1) [DM USD] , 0.0 AS  [DL IDR], 0.0 AS  [DL USD], 0.0 AS [FOH IDR], 0.0 AS [FOH USD], 0.0 [Confirm Qty],0.0 [Confirm IDR],0.0 [Confirm USD], itemcode AS [Code_Mat], itemlongdescription AS [Description], retbaqty AS [Qty], (CASE WHEN retbaunitoid=itemunit1 THEN retbavalueidr WHEN retbaunitoid=itemunit2 THEN (retbavalueidr*unit2unit1conversion) ELSE (retbavalueidr*unit3unit1conversion) END) AS [Value IDR], (CASE WHEN retbaunitoid=itemunit1 THEN retbavalueusd WHEN retbaunitoid=itemunit2 THEN (retbavalueusd*unit2unit1conversion) ELSE (retbavalueusd*unit3unit1conversion) END) AS [Value USD], (retbavalueidr*retbaqty)*-1 AS [Amount IDR], (retbavalueusd*retbaqty)*-1 AS [Amount USD], gx.gendesc AS [Unit_Mat], 0.0 AS [KANCING IDR], 0.0 AS [KAIN IDR], 0.0 AS [LABEL IDR], 0.0 AS [OTHER IDR] FROM QL_trnreturnbadtl bad INNER JOIN QL_trnreturnbamst bam ON bam.cmpcode=bad.cmpcode AND bad.retbamstoid=bam.retbamstoid INNER JOIN QL_trnbrtacaramst bm ON bam.cmpcode=bad.cmpcode AND bm.acaramstoid=bad.acaramstoid INNER JOIN QL_mstitem m ON m.itemoid=bad.retbarefoid INNER JOIN QL_mstgen gx ON gx.genoid=bad.retbaunitoid WHERE retbamststatus <>'In Process' AND bam.retbamstoid>0 GROUP BY bad.cmpcode, bm.womstoid, bam.retbano, bam.updtime, itemcode, itemlongdescription, retbaqty, retbavalueidr, retbavalueusd, gx.gendesc, retbaunitoid, itemunit1, itemunit2, unit2unit1conversion, unit3unit1conversion UNION ALL "
                    'PRODUCTION RESULT DLC
                    sSql &= " SELECT resd.cmpcode, resd.womstoid, prodresno [Ref No], resm.updtime [Ref Date], 0.0 AS  [DM IDR], 0.0 AS  [DM USD], ISNULL(SUM(prodresdlcamt), 0.0) [DL IDR], ISNULL(SUM(prodresdlcamtusd), 0.0) [DL USD], 0.0 AS [FOH IDR], 0.0 AS [FOH USD], 0.0 [Confirm Qty],0.0 [Confirm IDR],0.0 [Confirm USD], '' AS [Code_Mat], '' AS [Description], 0.0 AS [Qty], 0.0 AS [Value IDR], 0.0 AS [Value USD], 0.0 AS [Amount IDR], 0.0 AS [Amount USD], '' AS [Unit_Mat], 0.0 AS [KANCING IDR], 0.0 AS [KAIN IDR], 0.0 AS [LABEL IDR], 0.0 AS [OTHER IDR] FROM QL_trnprodresdtl resd INNER JOIN QL_trnprodresmst resm ON resm.cmpcode=resd.cmpcode AND resm.prodresmstoid=resd.prodresmstoid WHERE prodresmststatus<>'In Process' AND resm.prodresmstoid>0 GROUP BY resd.cmpcode, resd.womstoid, prodresno, resm.updtime UNION ALL"
                    'PRODUCTION RESULT FOH
                    sSql &= "  SELECT resd.cmpcode, resd.womstoid, prodresno [Ref No], resm.updtime [Ref Date], 0.0 AS  [DM IDR], 0.0 AS [DM USD], 0.0 AS [DL IDR], 0.0 AS [DL USD], ISNULL(SUM(bordiramt), 0.0) [FOH IDR], ISNULL(SUM(prodresohdamtusd), 0.0) [FOH USD], 0.0 [Confirm Qty],0.0 [Confirm IDR],0.0 [Confirm USD], '' AS [Code], '' AS [Description], 0.0 AS [Qty], 0.0 AS [Value IDR], 0.0 AS [Value USD], 0.0 AS [Amount IDR], 0.0 AS [Amount USD], '' AS [Unit_Mat], 0.0 AS [KANCING IDR], 0.0 AS [KAIN IDR], 0.0 AS [LABEL IDR], 0.0 AS [OTHER IDR] FROM QL_trnprodresdtl resd INNER JOIN QL_trnprodresmst resm ON resm.cmpcode=resd.cmpcode AND resm.prodresmstoid=resd.prodresmstoid WHERE prodresmststatus<>'In Process' AND resm.prodresmstoid>0 GROUP BY resd.cmpcode, resd.womstoid, prodresno, resm.updtime UNION ALL"
                    'PRODUCTION RESULT KANCING
                    sSql &= "  SELECT resd.cmpcode, resd.womstoid, prodresno [Ref No], resm.updtime [Ref Date], 0.0 AS  [DM IDR], 0.0 AS [DM USD], 0.0 AS [DL IDR], 0.0 AS [DL USD], 0.0 AS [FOH IDR], 0.0 AS [FOH USD], 0.0 [Confirm Qty],0.0 [Confirm IDR],0.0 [Confirm USD], '' AS [Code], '' AS [Description], 0.0 AS [Qty], 0.0 AS [Value IDR], 0.0 AS [Value USD], 0.0 AS [Amount IDR], 0.0 AS [Amount USD], '' AS [Unit_Mat], ISNULL(SUM(prodreskancingamt), 0.0) [KANCING IDR], 0.0 AS [KAIN IDR], 0.0 [LABEL IDR], 0.0 AS [OTHER IDR] FROM QL_trnprodresdtl resd INNER JOIN QL_trnprodresmst resm ON resm.cmpcode=resd.cmpcode AND resm.prodresmstoid=resd.prodresmstoid WHERE prodresmststatus<>'In Process' AND resm.prodresmstoid>0 GROUP BY resd.cmpcode, resd.womstoid, prodresno, resm.updtime UNION ALL"
                    'PRODUCTION RESULT KAIN
                    sSql &= "  SELECT resd.cmpcode, resd.womstoid, prodresno [Ref No], resm.updtime [Ref Date], 0.0 AS  [DM IDR], 0.0 AS [DM USD], 0.0 AS [DL IDR], 0.0 AS [DL USD], 0.0 AS [FOH IDR], 0.0 AS [FOH USD], 0.0 [Confirm Qty],0.0 [Confirm IDR],0.0 [Confirm USD], '' AS [Code], '' AS [Description], 0.0 AS [Qty], 0.0 AS [Value IDR], 0.0 AS [Value USD], 0.0 AS [Amount IDR], 0.0 AS [Amount USD], '' AS [Unit_Mat], 0.0 AS [KANCING IDR], ISNULL(SUM(prodreskainamt), 0.0) AS [KAIN IDR], 0.0 AS [LABEL IDR], 0.0 AS [OTHER IDR] FROM QL_trnprodresdtl resd INNER JOIN QL_trnprodresmst resm ON resm.cmpcode=resd.cmpcode AND resm.prodresmstoid=resd.prodresmstoid WHERE prodresmststatus<>'In Process' AND resm.prodresmstoid>0 GROUP BY resd.cmpcode, resd.womstoid, prodresno, resm.updtime UNION ALL"
                    'PRODUCTION RESULT LABEL
                    sSql &= "  SELECT resd.cmpcode, resd.womstoid, prodresno [Ref No], resm.updtime [Ref Date], 0.0 AS  [DM IDR], 0.0 AS [DM USD], 0.0 AS [DL IDR], 0.0 AS [DL USD], 0.0 AS [FOH IDR], 0.0 AS [FOH USD], 0.0 [Confirm Qty],0.0 [Confirm IDR],0.0 [Confirm USD], '' AS [Code], '' AS [Description], 0.0 AS [Qty], 0.0 AS [Value IDR], 0.0 AS [Value USD], 0.0 AS [Amount IDR], 0.0 AS [Amount USD], '' AS [Unit_Mat], 0.0 AS [KANCING IDR], 0.0 AS [KAIN IDR], ISNULL(SUM(prodreslabelamt), 0.0) AS [LABEL IDR], 0.0 AS [OTHER IDR] FROM QL_trnprodresdtl resd INNER JOIN QL_trnprodresmst resm ON resm.cmpcode=resd.cmpcode AND resm.prodresmstoid=resd.prodresmstoid WHERE prodresmststatus<>'In Process' AND resm.prodresmstoid>0 GROUP BY resd.cmpcode, resd.womstoid, prodresno, resm.updtime UNION ALL"
                    'PRODUCTION RESULT OTHER
                    sSql &= "  SELECT resd.cmpcode, resd.womstoid, prodresno [Ref No], resm.updtime [Ref Date], 0.0 AS  [DM IDR], 0.0 AS [DM USD], 0.0 AS [DL IDR], 0.0 AS [DL USD], 0.0 AS [FOH IDR], 0.0 AS [FOH USD], 0.0 [Confirm Qty],0.0 [Confirm IDR],0.0 [Confirm USD], '' AS [Code], '' AS [Description], 0.0 AS [Qty], 0.0 AS [Value IDR], 0.0 AS [Value USD], 0.0 AS [Amount IDR], 0.0 AS [Amount USD], '' AS [Unit_Mat], 0.0 AS [KANCING IDR], 0.0 AS [KAIN IDR], 0.0 AS [LABEL IDR], ISNULL(SUM(prodresotheramt), 0.0)AS [OTHER IDR] FROM QL_trnprodresdtl resd INNER JOIN QL_trnprodresmst resm ON resm.cmpcode=resd.cmpcode AND resm.prodresmstoid=resd.prodresmstoid WHERE prodresmststatus<>'In Process' AND resm.prodresmstoid>0 GROUP BY resd.cmpcode, resd.womstoid, prodresno, resm.updtime UNION ALL"
                    'PRODUCTION RESULT CONF
                    sSql &= " SELECT conf.cmpcode, conf.womstoid, 'CONFIRM:' + prodresno [Ref No], conf.updtime [Ref Date], 0.0 AS  [DM IDR], 0.0 AS [DM USD], 0.0 AS [DL IDR], 0.0 AS [DL USD], 0.0 AS [FOH IDR], 0.0 AS [FOH USD], SUM(confirmqty) [Confirm Qty], SUM(confirmvalueidr * (confirmqty)) [Confirm IDR], SUM(confirmvalueusd * (confirmqty)) [Confirm USD], '' AS [Code_Mat], '' AS [Description], 0.0 AS [Qty], 0.0 AS [Value IDR], 0.0 AS [Value USD], 0.0 AS [Amount IDR], 0.0 AS [Amount USD], '' AS [Unit_Mat], 0.0 AS [KANCING IDR], 0.0 AS [KAIN IDR], 0.0 AS [LABEL IDR], 0.0 AS [OTHER IDR] FROM QL_trnprodresconfirm conf INNER JOIN QL_trnprodresmst resm ON resm.cmpcode=conf.cmpcode AND resm.prodresmstoid=conf.prodresmstoid GROUP BY conf.cmpcode, conf.womstoid,prodresno, conf.updtime"
                    sSql &= " ) AS tbl_Tmp "
                    sSql &= " GROUP BY cmpcode, womstoid,[Ref Date],[Ref No], [Code_Mat], [Description], [Qty], [Value IDR], [Value USD], [Amount IDR], [Amount USD], [Unit_Mat]) as tblALL ON tblALL.cmpcode=wom.cmpcode AND tblALL.womstoid=wom.womstoid"

                    sSql &= " WHERE wom.cmpcode='" & DDLBusUnit.SelectedValue & "'"
                    If wono.Text <> "" Then
                        Dim sResNo() As String = Split(wono.Text, ";")
                        sSql &= " AND ("
                        For C1 As Integer = 0 To sResNo.Length - 1
                            sSql &= " wono LIKE '" & Tchar(sResNo(C1)) & "'"
                            If C1 < sResNo.Length - 1 Then
                                sSql &= " OR"
                            End If
                        Next
                        sSql &= ")"
                    End If

                    If FilterMaterial.Text <> "" Then
                        Dim sWhereCode As String = ""
                        Dim sNo() As String = Split(FilterMaterial.Text, ";")
                        If sNo.Length > 0 Then
                            For C1 As Integer = 0 To sNo.Length - 1
                                If sNo(C1) <> "" Then
                                    sWhereCode &= "itemcode LIKE '" & Tchar(sNo(C1)) & "' OR "
                                End If
                            Next
                            If sWhereCode <> "" Then
                                sWhereCode = Left(sWhereCode, sWhereCode.Length - 4)
                                sSql &= " AND (" & sWhereCode & ")"
                            End If
                        End If
                    End If

                    If DDLDivision.SelectedValue <> "ALL" Then
                        sSql &= " AND groupoid=" & DDLDivision.SelectedValue
                    End If
                    sSql &= " AND womststatus<>'Cancel'"
                    sSql &= " AND wom.wodate>=CAST('" & FilterPeriod1.Text & " 00:00:00' AS DATETIME)"
                    sSql &= " AND wom.wodate<=CAST('" & FilterPeriod2.Text & " 23:59:59' AS DATETIME)"
                    If Not cbAllowNull.Checked Then
                        sSql &= " AND (ISNULL([DM IDR],0)<>0 OR ISNULL([DM USD],0)<>0 OR ISNULL([DL IDR],0)<>0 OR ISNULL([DL USD],0)<>0 OR ISNULL([FOH IDR],0)<>0 OR ISNULL([FOH USD],0)<>0 OR ISNULL([KAIN IDR],0)<>0 OR ISNULL([KANCING IDR],0)<>0 OR ISNULL([LABEL IDR],0)<>0 OR ISNULL([Confirm IDR],0)<>0 OR ISNULL([Confirm USD],0)<>0) "
                    End If
                    sSql &= " ORDER BY [KIK No.],Oid,[Ref Date]"

                End If
                Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstKIK")
                Dim dvTbl As DataView = dtTbl.DefaultView

                report.SetDataSource(dvTbl.ToTable)
            Else
                '===================Query For Transform===============================
                '===================Query For Transform===============================
                '===================Query For Transform===============================

                If FilterDDLType.SelectedValue = "Summary" Then
                    sSql = " SELECT [Oid], [KIK No.], [Status], ((DMAwal + DLAwal + FOHAwal) - ConfirmAwal) [Saldo Awal], DM, DL, FOH, Confirm, ConfirmQty, (((DMAwal + DLAwal + FOHAwal) - ConfirmAwal) + ((DM + DL + FOH) - Confirm)) AS [Saldo Akhir], [Code], [Finish Good], [Finish Good Qty], [Unit], [Tgl Closed] FROM (" & _
                           "SELECT wom.transformmstoid AS [Oid], wom.transformno AS [KIK No.], wom.transformmststatus [Status], 0.0 DMAwal, ISNULL(SUM(DM), 0.0) DM, 0.0 DLAwal, ISNULL(SUM(transformtotalcostin), 0.0) DL, 0.0 FOHAwal, ISNULL(SUM(transformtotalcostout),0.0) FOH, 0.0 ConfirmAwal, 0.0 Confirm, ISNULL(SUM(wodtl1qty), 0.0) ConfirmQty, itemcode [Code], itemlongdesc [Finish Good], wodtl1qty [Finish Good Qty], gendesc [Unit], Division, groupoid, '1/1/1900' [Tgl Closed] FROM QL_trntransformmst wom "
                    sSql &= "  INNER JOIN (SELECT DISTINCT wod1.cmpcode, wod1.transformmstoid AS womstoid, itemcode, itemLongDescription AS itemlongdesc, ISNULL((SELECT SUM(wodx.transformdtl2qty) FROM QL_trntransformdtl2 wodx WHERE wodx.transformmstoid=wod1.transformmstoid),0.0) AS wodtl1qty, wod1.transformdtl2res2 AS wodtl1unitoid, g.gendesc, 0 AS groupoid, /*(groupcode + ' - ' + groupdesc)*/ '' AS Division FROM QL_trntransformdtl2 wod1 /*INNER JOIN QL_trnsomst som ON som.cmpcode=wod1.cmpcode AND som.somstoid=wod1.soitemmstoid*/ INNER JOIN QL_mstitem m ON wod1.transformdtl2refoid=m.itemoid INNER JOIN QL_mstgen g ON g.genoid=wod1.transformdtl2res2 /*LEFT JOIN QL_mstdeptgroup dept ON som.cmpcode=dept.cmpcode AND dept.groupoid=som.groupoid*/) AS tblItem ON tblItem.cmpcode=wom.cmpcode AND tblItem.womstoid=wom.transformmstoid "

                    ' SALDO AWAL DM
                    sSql &= " LEFT JOIN (SELECT cmpcode, womstoid, ISNULL(SUM(DM), 0.0) DM FROM ("
                    ' AMBIL DARI MATERIAL USAGE (RAW, GEN, SP, KAYU)

                    sSql &= "SELECT mud.cmpcode, mum.transformmstoid AS womstoid, ISNULL(SUM(transformdtl1qty * transformdtl1valueidr), 0.0) DM FROM QL_trntransformdtl1 mud INNER JOIN QL_trntransformmst mum ON mum.cmpcode=mud.cmpcode AND mum.transformmstoid=mud.transformmstoid WHERE mum.transformmststatus<>'In Process' /*AND mum.updtime<CAST('" & sDate & " 00:00:00' AS DATETIME)*/ GROUP BY mud.cmpcode, mum.transformmstoid "
                    sSql &= ") AS tblDM_Tmp GROUP BY cmpcode, womstoid"
                    sSql &= ") AS tblDM ON tblDM.cmpcode=wom.cmpcode AND tblDM.womstoid=wom.transformmstoid"
                    ' SALDO DM - END

                    sSql &= " WHERE wom.cmpcode='" & DDLBusUnit.SelectedValue & "'"
                    If wono.Text <> "" Then
                        Dim sResNo() As String = Split(wono.Text, ";")
                        sSql &= " AND ("
                        For C1 As Integer = 0 To sResNo.Length - 1
                            sSql &= " wom.transformno LIKE '" & Tchar(sResNo(C1)) & "'"
                            If C1 < sResNo.Length - 1 Then
                                sSql &= " OR"
                            End If
                        Next
                        sSql &= ")"
                    End If
                    If FilterMaterial.Text <> "" Then
                        Dim sWhereCode As String = ""
                        Dim sNo() As String = Split(FilterMaterial.Text, ";")
                        If sNo.Length > 0 Then
                            For C1 As Integer = 0 To sNo.Length - 1
                                If sNo(C1) <> "" Then
                                    sWhereCode &= "itemcode LIKE '" & Tchar(sNo(C1)) & "' OR "
                                End If
                            Next
                            If sWhereCode <> "" Then
                                sWhereCode = Left(sWhereCode, sWhereCode.Length - 4)
                                sSql &= " AND (" & sWhereCode & ")"
                            End If
                        End If
                    End If
                    If DDLDivision.SelectedValue <> "ALL" Then
                        sSql &= " AND groupoid=" & DDLDivision.SelectedValue
                    End If
                    sSql &= " AND transformmststatus<>'Cancel'"
                    sSql &= " AND wom.updtime<=CAST('" & FilterPeriod2.Text & " 23:59:59' AS DATETIME)"
                    If Not cbAllowNull.Checked Then
                        sSql &= " AND (ISNULL(DM,0)<>0) "
                    End If
                    sSql &= " GROUP BY wom.transformmstoid , transformno, transformmststatus, itemcode, itemlongdesc, wodtl1qty, gendesc, Division, groupoid) AS tblKIK ORDER BY [KIK No.] ASC, [Oid] ASC"

                Else
                    sSql = " SELECT wom.cmpcode,(SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=wom.cmpcode) [BUSSINESS UNIT],wom.transformmstoid AS [Oid], wom.transformno AS [KIK No.], wom.transformmststatus [Status], [Ref No], [Ref Date], [DM IDR], [DM USD], [DL IDR], [DL USD], [FOH IDR], [FOH USD], [Confirm Qty], [Confirm IDR], [Confirm USD], itemcode [Code], itemlongdesc [Finish Good], wodtl1qty [Finish Good Qty], gendesc [Unit], Division, groupoid, wom.updtime AS [Tgl Closed], [Code_Mat], [Description], [Qty], [Value IDR], [Value USD], [Amount IDR],[Amount USD], [Unit_Mat] " & _
                    "  FROM QL_trntransformmst wom INNER JOIN (SELECT DISTINCT wod1.cmpcode, wod1.transformmstoid AS womstoid, itemcode, itemLongDescription AS itemlongdesc, ISNULL((SELECT SUM(wodx.transformdtl2qty) FROM QL_trntransformdtl2 wodx WHERE wodx.transformmstoid=wod1.transformmstoid),0.0) AS wodtl1qty, wod1.transformdtl2res2 AS wodtl1unitoid, g.gendesc, 0 AS groupoid, /*(groupcode + ' - ' + groupdesc)*/ '' AS Division FROM QL_trntransformdtl2 wod1 /*INNER JOIN QL_trnsomst som ON som.cmpcode=wod1.cmpcode AND som.somstoid=wod1.soitemmstoid*/ INNER JOIN QL_mstitem m ON wod1.transformdtl2refoid=m.itemoid INNER JOIN QL_mstgen g ON g.genoid=wod1.transformdtl2res2 /*LEFT JOIN QL_mstdeptgroup dept ON som.cmpcode=dept.cmpcode AND dept.groupoid=som.groupoid*/) AS tblItem ON tblItem.cmpcode=wom.cmpcode AND tblItem.womstoid=wom.transformmstoid " & _
                    "  LEFT JOIN (SELECT cmpcode, womstoid, [Ref No], [Ref Date], ISNULL(SUM([DM IDR]), 0.0) [DM IDR] , ISNULL(SUM([DM USD]), 0.0) [DM USD], ISNULL(SUM([DL IDR]), 0.0) [DL IDR] , ISNULL(SUM([DL USD]), 0.0) [DL USD], ISNULL(SUM([FOH IDR]), 0.0) [FOH IDR] , ISNULL(SUM([FOH USD]), 0.0) [FOH USD], ISNULL(SUM([Confirm Qty]),0.0) [Confirm Qty],ISNULL(SUM([Confirm IDR]), 0.0) [Confirm IDR],ISNULL(SUM([Confirm USD]), 0.0) [Confirm USD], [Code_Mat], [Description], [Qty], [Value IDR], [Value USD], [Amount IDR],[Amount USD], [Unit_Mat] FROM ("
                    'MATERIAL USAGE RAW,GEN,SP,KAYU
                    For C1 As Integer = 0 To arType.Length - 1
                        sSql &= " SELECT mud.cmpcode, mum.transformmstoid AS womstoid, 'Detail Material' [Ref No], mum.updtime [Ref Date], ISNULL(SUM(transformdtl1qty * transformdtl1valueidr), 0.0) [DM IDR], ISNULL(SUM(transformdtl1qty * transformdtl1valueusd), 0.0) [DM USD] , 0.0 AS  [DL IDR], 0.0 AS  [DL USD], 0.0 AS [FOH IDR], 0.0 AS [FOH USD], 0.0 [Confirm Qty] ,0.0 [Confirm IDR],0.0 [Confirm USD], itemcode AS [Code_Mat], itemlongdescription AS [Description], transformdtl1qty AS [Qty], (transformdtl1valueidr*(CASE WHEN transformdtl1res1='itemunit3' THEN unit3unit1conversion ELSE 1 END)) AS [Value IDR], transformdtl1valueusd AS [Value USD], (transformdtl1valueidr*transformdtl1qty) AS [Amount IDR], (transformdtl1valueusd*transformdtl1qty) AS [Amount USD], gx.gendesc AS [Unit_Mat] FROM QL_trntransformdtl1 mud INNER JOIN QL_trntransformmst mum ON mum.cmpcode=mud.cmpcode AND mum.transformmstoid=mud.transformmstoid INNER JOIN QL_mstitem m ON m.itemoid=mud.transformdtl1refoid INNER JOIN QL_mstgen gx ON gx.genoid=mud.transformdtl1res2 WHERE mum.transformmststatus<>'In Process' AND mum.transformmstoid>0 GROUP BY mud.cmpcode, transformno , mum.updtime, itemcode, itemlongdescription, transformdtl1qty, transformdtl1valueidr, transformdtl1valueusd, transformdtl1qty, gx.gendesc, mum.transformmstoid, unit3unit1conversion, transformdtl1res1 UNION ALL "
                    Next
                    'PRODUCTION RESULT DLC
                    sSql &= "  SELECT resm.cmpcode, resm.transformmstoid AS womstoid, 'Material In' [Ref No], resm.updtime [Ref Date], 0.0 AS  [DM IDR], 0.0 AS  [DM USD], ISNULL(SUM(transformtotalcostin), 0.0) [DL IDR], 0.0 [DL USD], 0.0 AS [FOH IDR], 0.0 AS [FOH USD], 0.0 [Confirm Qty],0.0 [Confirm IDR],0.0 [Confirm USD], '' AS [Code_Mat], '' AS [Description], 0.0 AS [Qty], 0.0 AS [Value IDR], 0.0 AS [Value USD], 0.0 AS [Amount IDR], 0.0 AS [Amount USD], '' AS [Unit_Mat] FROM QL_trntransformmst resm WHERE transformmststatus<>'In Process' AND resm.transformmstoid>0 GROUP BY resm.cmpcode, resm.transformmstoid, transformno, resm.updtime UNION ALL"
                    'PRODUCTION RESULT FOH
                    sSql &= "  SELECT resm.cmpcode, resm.transformmstoid AS womstoid, 'Material Out' [Ref No], resm.updtime [Ref Date], 0.0 AS  [DM IDR], 0.0 AS  [DM USD], 0.0 [DL IDR], 0.0 [DL USD], ISNULL(SUM(transformtotalcostout), 0.0) AS [FOH IDR], 0.0 AS [FOH USD], 0.0 [Confirm Qty],0.0 [Confirm IDR],0.0 [Confirm USD], '' AS [Code_Mat], '' AS [Description], 0.0 AS [Qty], 0.0 AS [Value IDR], 0.0 AS [Value USD], 0.0 AS [Amount IDR], 0.0 AS [Amount USD], '' AS [Unit_Mat] FROM QL_trntransformmst resm WHERE transformmststatus<>'In Process' AND resm.transformmstoid>0 GROUP BY resm.cmpcode, resm.transformmstoid, transformno, resm.updtime UNION ALL"
                    sSql &= " SELECT conf.cmpcode, conf.transformmstoid AS womstoid, 'Confirm' [Ref No], conf.updtime [Ref Date], 0.0 AS  [DM IDR], 0.0 AS [DM USD], 0.0 AS [DL IDR], 0.0 AS [DL USD], 0.0 AS [FOH IDR], 0.0 AS [FOH USD], SUM(transformdtl2qty) [Confirm Qty], SUM(transformdtl2valueidr * transformdtl2qty) [Confirm IDR], SUM(transformdtl2valueusd * transformdtl2qty) [Confirm USD], '' AS [Code_Mat], '' AS [Description], 0.0 AS [Qty], 0.0 AS [Value IDR], 0.0 AS [Value USD], 0.0 AS [Amount IDR], 0.0 AS [Amount USD], '' AS [Unit_Mat] FROM QL_trntransformdtl2 conf INNER JOIN QL_trntransformmst resm ON resm.cmpcode=conf.cmpcode AND resm.transformmstoid=conf.transformmstoid GROUP BY conf.cmpcode, conf.transformmstoid, transformno, conf.updtime"
                    sSql &= " ) AS tbl_Tmp "
                    sSql &= " GROUP BY cmpcode, womstoid,[Ref Date],[Ref No], [Code_Mat], [Description], [Qty], [Value IDR], [Value USD], [Amount IDR], [Amount USD], [Unit_Mat]) as tblALL ON tblALL.cmpcode=wom.cmpcode AND tblALL.womstoid=wom.transformmstoid"

                    sSql &= " WHERE wom.cmpcode='" & DDLBusUnit.SelectedValue & "'"
                    If wono.Text <> "" Then
                        Dim sResNo() As String = Split(wono.Text, ";")
                        sSql &= " AND ("
                        For C1 As Integer = 0 To sResNo.Length - 1
                            sSql &= " wom.transformno LIKE '" & Tchar(sResNo(C1)) & "'"
                            If C1 < sResNo.Length - 1 Then
                                sSql &= " OR"
                            End If
                        Next
                        sSql &= ")"
                    End If
                    If FilterMaterial.Text <> "" Then
                        Dim sWhereCode As String = ""
                        Dim sNo() As String = Split(FilterMaterial.Text, ";")
                        If sNo.Length > 0 Then
                            For C1 As Integer = 0 To sNo.Length - 1
                                If sNo(C1) <> "" Then
                                    sWhereCode &= "itemcode LIKE '" & Tchar(sNo(C1)) & "' OR "
                                End If
                            Next
                            If sWhereCode <> "" Then
                                sWhereCode = Left(sWhereCode, sWhereCode.Length - 4)
                                sSql &= " AND (" & sWhereCode & ")"
                            End If
                        End If
                    End If
                    If DDLDivision.SelectedValue <> "ALL" Then
                        sSql &= " AND groupoid=" & DDLDivision.SelectedValue
                    End If
                    sSql &= " AND transformmststatus<>'Cancel'"
                    sSql &= " AND wom.transformdate>=CAST('" & FilterPeriod1.Text & " 00:00:00' AS DATETIME)"
                    sSql &= " AND wom.transformdate<=CAST('" & FilterPeriod2.Text & " 23:59:59' AS DATETIME)"
                    If Not cbAllowNull.Checked Then
                        sSql &= " AND (ISNULL([DM IDR],0)<>0 OR ISNULL([DM USD],0)<>0 OR ISNULL([DL IDR],0)<>0 OR ISNULL([DL USD],0)<>0 OR ISNULL([FOH IDR],0)<>0 OR ISNULL([FOH USD],0)<>0 OR ISNULL([Confirm IDR],0)<>0 OR ISNULL([Confirm USD],0)<>0) "
                    End If
                    sSql &= " ORDER BY [KIK No.],Oid,[Ref Date]"

                End If
                Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstKIK")
                Dim dvTbl As DataView = dtTbl.DefaultView

                report.SetDataSource(dvTbl.ToTable)
            End If
            
            report.SetParameterValue("StartPeriod", Format(CDate(FilterPeriod1.Text), "dd MMM yyyy"))
            report.SetParameterValue("EndPeriod", Format(CDate(FilterPeriod2.Text), "dd MMM yyyy"))
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            If sType = "Print Excel" Then
                report.PrintOptions.PaperSize = PaperSize.PaperA4
            Else
                'report.PrintOptions.PaperSize = PaperSize.PaperFolio
            End If
            cProc.SetDBLogonForReport(report)

            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, rptName)
                report.Close()
                report.Dispose()
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, rptName)
                report.Close()
                report.Dispose()
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Private Sub InitAllDDL()
        ' DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        FillDDL(DDLBusUnit, sSql)
        If DDLBusUnit.Items.Count > 0 Then
            InitDDLDivision(DDLBusUnit.SelectedValue)
        End If
    End Sub

    Private Sub InitDDLDivision(ByVal sDiv As String)
        ' Init DDL Division
        sSql = "SELECT groupoid, groupcode+' - '+groupdesc FROM QL_mstdeptgroup WHERE activeflag='ACTIVE'"
        If sDiv <> "ALL" Then
            sSql &= " AND cmpcode='" & sDiv & "'"
            FillDDLWithALL(DDLDivision, sSql)
        Else
            sSql = "SELECT groupoid, groupcode+' - '+groupdesc FROM QL_mstdeptgroup dept INNER JOIN QL_mstdivision div ON  div.cmpcode=dept.cmpcode WHERE div.activeflag='ACTIVE' AND dept.activeflag='ACTIVE'"
            FillDDLWithALL(DDLDivision, sSql)
        End If
    End Sub

    Private Sub BindListMat()
        sSql = "SELECT DISTINCT mud.itemoid AS matoid,m.itemCode AS matcode, m.itemLongDescription AS matlongdesc, gendesc AS matunit, 'False' AS checkvalue FROm QL_trnwodtl1 mud INNER JOIN QL_mstitem m ON m.itemoid=mud.itemoid INNER JOIN QL_mstgen g ON genoid=mud.wodtl1unitoid WHERE mud.cmpcode='" & DDLBusUnit.SelectedValue & "' "
        If IsValidPeriod() Then
            sSql &= " AND mud.updtime>='" & FilterPeriod1.Text & " 00:00:00' AND mud.updtime<='" & FilterPeriod2.Text & " 23:59:59'"
        Else
            Exit Sub
        End If
     
        sSql &= " ORDER BY matcode"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnmatusagedtl")
        If dt.Rows.Count > 0 Then
            Session("TblListMat") = dt
            Session("TblListMatView") = Session("TblListMat")
            gvListMat.DataSource = Session("TblListMatView")
            gvListMat.DataBind()
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            showMessage("No Material data available for this time.", 2)
        End If
    End Sub

    Private Sub BindListMat2()
        sSql = "SELECT DISTINCT mud.transformdtl2refoid AS matoid,m.itemCode AS matcode, m.itemLongDescription AS matlongdesc, gendesc AS matunit, 'False' AS checkvalue FROm QL_trntransformdtl2 mud INNER JOIN QL_mstitem m ON m.itemoid=mud.transformdtl2refoid INNER JOIN QL_mstgen g ON genoid=mud.transformdtl2res2 WHERE mud.cmpcode='" & DDLBusUnit.SelectedValue & "' "
        If IsValidPeriod() Then
            sSql &= " AND mud.updtime>='" & FilterPeriod1.Text & " 00:00:00' AND mud.updtime<='" & FilterPeriod2.Text & " 23:59:59'"
        Else
            Exit Sub
        End If

        sSql &= " ORDER BY matcode"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnmatusagedtl")
        If dt.Rows.Count > 0 Then
            Session("TblListMat") = dt
            Session("TblListMatView") = Session("TblListMat")
            gvListMat.DataSource = Session("TblListMatView")
            gvListMat.DataBind()
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            showMessage("No Material data available for this time.", 2)
        End If
    End Sub

    Private Sub UpdateCheckedListMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMat") = dt
        End If
        If Session("TblListMatView") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMatView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMatView") = dt
        End If
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If

        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmCOGM.aspx")

        End If

        If checkPagePermission("~\ReportForm\frmCOGM.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        Page.Title = CompnyName & " - Cost of Good Manufactured"
        If Not Page.IsPostBack Then
            InitAllDDL()
            DDLSource_SelectedIndexChanged(Nothing, Nothing)
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListKIK") Is Nothing And Session("EmptyListKIK") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListKIK") Then
                Session("EmptyListKIK") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListKIK, PanelListKIK, mpeListKIK, True)
            End If
        End If
        If Not Session("WarningListKIK") Is Nothing And Session("WarningListKIK") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListKIK") Then
                Session("WarningListKIK") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListKIK, PanelListKIK, mpeListKIK, True)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                mpeListMat.Show()
            End If
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        imbEraseKIK_Click(Nothing, Nothing)
        InitDDLDivision(DDLBusUnit.SelectedValue)
    End Sub

    Protected Sub FilterDDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLType.SelectedIndexChanged
        Dim bVal As Boolean = True
        If FilterDDLType.SelectedValue = "Summary" Then
            lbldate.Text = "Production Period"
            bVal = False
        Else
            lbldate.Text = "SPK Date"
        End If

        'lblMat.Visible = bVal : septMat.Visible = bVal : FilterMaterial.Visible = bVal : btnSearchMat.Visible = bVal : btnClearMat.Visible = bVal
    End Sub

	Protected Sub imbFindKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindKIK.Click
		If DDLSource.SelectedValue = "PRODUCTION" Then
			lblListKIK.Text = "List of SPK"
			DDLFilterListKIK.Items(0).Text = "SPK No"
			DDLFilterListKIK.Items(1).Text = "SPK Note"
			DDLFilterListKIK.Items(2).Enabled = True
		Else
			lblListKIK.Text = "List of Transform"
			DDLFilterListKIK.Items(0).Text = "Transform No"
			DDLFilterListKIK.Items(1).Text = "Transform Note"
			DDLFilterListKIK.Items(2).Enabled = False
		End If
		If IsValidPeriod() Then
			DDLFilterListKIK.SelectedIndex = -1 : txtFilterListKIK.Text = ""
			Session("TblKIK") = Nothing : Session("TblKIKView") = Nothing : gvListKIK.DataSource = Nothing : gvListKIK.DataBind()
			cProc.SetModalPopUpExtender(btnHiddenListKIK, PanelListKIK, mpeListKIK, True)
		Else
			Exit Sub
		End If
	End Sub

    Protected Sub imbEraseKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseKIK.Click
        wono.Text = ""
    End Sub

    Protected Sub btnFindListKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListKIK.Click
        If Session("TblKIK") Is Nothing Then
            BindListKIK()
            If Session("TblKIK").Rows.Count <= 0 Then
                Session("EmptyListKIK") = "KIK data can't be found!"
                showMessage(Session("EmptyListKIK"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListKIK.SelectedValue & " LIKE '%" & Tchar(txtFilterListKIK.Text) & "%'"
        If UpdateCheckedKIK() Then
            Dim dv As DataView = Session("TblKIK").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblKIKView") = dv.ToTable
                gvListKIK.DataSource = Session("TblKIKView")
                gvListKIK.DataBind()
                dv.RowFilter = ""
                mpeListKIK.Show()
            Else
                dv.RowFilter = ""
                Session("TblKIKView") = Nothing
                gvListKIK.DataSource = Session("TblKIKView")
                gvListKIK.DataBind()
                Session("WarningListKIK") = "KIK data can't be found!"
                showMessage(Session("WarningListKIK"), 2)
            End If
        Else
            mpeListKIK.Show()
        End If
    End Sub

    Protected Sub btnViewAllListKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListKIK.Click
        DDLFilterListKIK.SelectedIndex = -1 : txtFilterListKIK.Text = ""
        If Session("TblKIK") Is Nothing Then
            BindListKIK()
            If Session("TblKIK").Rows.Count <= 0 Then
                Session("EmptyListKIK") = "KIK data can't be found!"
                showMessage(Session("EmptyListKIK"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedKIK() Then
            Dim dt As DataTable = Session("TblKIK")
            Session("TblKIKView") = dt
            gvListKIK.DataSource = Session("TblKIKView")
            gvListKIK.DataBind()
        End If
        mpeListKIK.Show()
    End Sub

    Protected Sub btnSelectAllKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllKIK.Click
        If Not Session("TblKIKView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblKIKView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblKIK")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "womstoid=" & dtTbl.Rows(C1)("womstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblKIK") = objTbl
                Session("TblKIKView") = dtTbl
                gvListKIK.DataSource = Session("TblKIKView")
                gvListKIK.DataBind()
            End If
            mpeListKIK.Show()
        Else
            Session("WarningListKIK") = "Please show some KIK data first!"
            showMessage(Session("WarningListKIK"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneKIK.Click
        If Not Session("TblKIKView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblKIKView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblKIK")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "womstoid=" & dtTbl.Rows(C1)("womstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblKIK") = objTbl
                Session("TblKIKView") = dtTbl
                gvListKIK.DataSource = Session("TblKIKView")
                gvListKIK.DataBind()
            End If
            mpeListKIK.Show()
        Else
            Session("WarningListKIK") = "Please show some KIK data first!"
            showMessage(Session("WarningListKIK"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedKIK.Click
        If Session("TblKIK") Is Nothing Then
            Session("WarningListKIK") = "Selected KIK data can't be found!"
            showMessage(Session("WarningListKIK"), 2)
            Exit Sub
        End If
        If UpdateCheckedKIK() Then
            Dim dtTbl As DataTable = Session("TblKIK")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListKIK.SelectedIndex = -1 : txtFilterListKIK.Text = ""
                Session("TblKIKView") = dtView.ToTable
                gvListKIK.DataSource = Session("TblKIKView")
                gvListKIK.DataBind()
                dtView.RowFilter = ""
                mpeListKIK.Show()
            Else
                dtView.RowFilter = ""
                Session("TblKIKView") = Nothing
                gvListKIK.DataSource = Session("TblKIKView")
                gvListKIK.DataBind()
                Session("WarningListKIK") = "Selected KIK data can't be found!"
                showMessage(Session("WarningListKIK"), 2)
            End If
        Else
            mpeListKIK.Show()
        End If
    End Sub

    Protected Sub gvListKIK_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListKIK.PageIndexChanging
        If UpdateCheckedKIK2() Then
            gvListKIK.PageIndex = e.NewPageIndex
            gvListKIK.DataSource = Session("TblKIKView")
            gvListKIK.DataBind()
        End If
        mpeListKIK.Show()
    End Sub

    Protected Sub lkbAddToListListKIK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListKIK.Click
        If Not Session("TblKIK") Is Nothing Then
            If UpdateCheckedKIK() Then
                Dim dtTbl As DataTable = Session("TblKIK")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If wono.Text <> "" Then
                            If dtView(C1)("wono") <> "" Then
                                wono.Text &= ";" + vbCrLf + dtView(C1)("wono")
                            End If
                        Else
                            If dtView(C1)("wono") <> "" Then
                                wono.Text &= dtView(C1)("wono")
                            End If
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHiddenListKIK, PanelListKIK, mpeListKIK, False)
                Else
                    Session("WarningListKIK") = "Please select KIK to add to list!"
                    showMessage(Session("WarningListKIK"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListKIK") = "Please show some KIK data first!"
            showMessage(Session("WarningListKIK"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListKIK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListKIK.Click
        cProc.SetModalPopUpExtender(btnHiddenListKIK, PanelListKIK, mpeListKIK, False)
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmCOGM.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Dispose()
        report.Close()
    End Sub

    Protected Sub DDLSource_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If DDLSource.SelectedIndex = 0 Then
            Label7.Text = "SPK No."
            FilterDDLType_SelectedIndexChanged(Nothing, Nothing)
        Else
            Label7.Text = "Transform No."
            lbldate.Text = "Transform Period"
        End If
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2) : Exit Sub
        End If
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        Session("TblListMat") = Nothing : Session("TblListMatView") = Nothing
        gvListMat.DataSource = Session("TblListMat") : gvListMat.DataBind()
        lblListMat.Text = "List Of Material"
        If DDLSource.SelectedValue.ToUpper = "PRODUCTION" Then
            BindListMat()
        Else
            BindListMat2()
        End If
    End Sub

    Protected Sub btnClearMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterMaterial.Text = ""
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        UpdateCheckedListMat()
        Dim dt As DataTable = Session("TblListMat")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        If dv.Count > 0 Then
            Session("TblListMatView") = dv.ToTable
            gvListMat.DataSource = Session("TblListMatView")
            gvListMat.DataBind()
            dv.RowFilter = ""
            mpeListMat.Show()
        Else
            dv.RowFilter = ""
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        UpdateCheckedListMat()
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        Session("TblListMatView") = Session("TblListMat")
        gvListMat.DataSource = Session("TblListMatView")
        gvListMat.DataBind()
        mpeListMat.Show()
    End Sub

    Protected Sub lbAddToListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        UpdateCheckedListMat()
        Dim dt As DataTable = Session("TblListMat")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = "checkvalue='True'"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                FilterMaterial.Text &= dv(C1)("matcode").ToString & ";"
            Next
            If FilterMaterial.Text <> "" Then
                FilterMaterial.Text = Left(FilterMaterial.Text, FilterMaterial.Text.Length - 1)
            End If
            dv.RowFilter = ""
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
        Else
            dv.RowFilter = ""
            Session("WarningListMat") = "Please select some Material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub lbSelectAllListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        UpdateCheckedListMat()
        Dim dt As DataTable = Session("TblListMat")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            FilterMaterial.Text &= dt.Rows(C1)("matcode").ToString & ";"
        Next
        If FilterMaterial.Text <> "" Then
            FilterMaterial.Text = Left(FilterMaterial.Text, FilterMaterial.Text.Length - 1)
        End If
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub cbHdrLM_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For C1 As Integer = 0 To gvListMat.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        mpeListMat.Show()
    End Sub
#End Region

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        UpdateCheckedListMat()
        gvListMat.PageIndex = e.NewPageIndex
        gvListMat.DataSource = Session("TblListMatView")
        gvListMat.DataBind()
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub
End Class
