Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class frmPORegister
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Dim Typem1 As String = ""
    Dim Typem2 As String = ""
    Dim thisMonth As New DateTime(DateTime.Today.Year, DateTime.Today.Month, 1)
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function FillDDLWithALL(ByRef oDDLObject As DropDownList, ByVal sSql As String) As Boolean
        ' To fill data in dropdown list, return true when all data exist
        FillDDLWithALL = True
        oDDLObject.Items.Clear()
        oDDLObject.Items.Add("ALL")
        oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = "ALL"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            oDDLObject.Items.Add(xreader.GetValue(1))
            oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()
        ' Cek if all data exits
        If oDDLObject.Items.Count = 0 Then
            FillDDLWithALL = False
        End If
        Return FillDDLWithALL
    End Function

    Private Function CheckType()
        If TypeMat.SelectedValue = "raw" Then
            Typem1 = "raw"
            Typem2 = "matraw"
        ElseIf TypeMat.SelectedValue = "gen" Then
            Typem1 = "gen"
            Typem2 = "matgen"
        ElseIf TypeMat.SelectedValue = "sparepart" Then
            Typem1 = "sp"
            Typem2 = "sparepart"
        ElseIf TypeMat.SelectedValue = "item" Then
            Typem1 = "item"
            Typem2 = "item"
        ElseIf TypeMat.SelectedValue = "service" Then
            Typem1 = "service"
            Typem2 = "service"
        ElseIf TypeMat.SelectedValue = "Asset" Then
            Typem1 = "asset"
            Typem2 = "asset"
        End If
    End Function

    Private Function UpdateCheckedSupp() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSupp") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSupp")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListSupp.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSupp.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "suppoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblSupp") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedSupp2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSuppView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSupp")
            Dim dtTbl2 As DataTable = Session("TblSuppView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListSupp.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSupp.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "suppoid=" & cbOid
                                dtView2.RowFilter = "suppoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblSupp") = dtTbl
                Session("TblSuppView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMR() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMR") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMR")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMR.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMR.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "mrrawmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMR") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMR2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMRView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMR")
            Dim dtTbl2 As DataTable = Session("TblMRView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMR.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMR.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "mrrawmstoid=" & cbOid
                                dtView2.RowFilter = "mrrawmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMR") = dtTbl
                Session("TblMRView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                dtView2.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindListSupp()
        'CheckType()
        sSql = "SELECT 'False' AS checkvalue, suppoid, suppcode, suppname, suppaddr FROM QL_mstsupp WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"

        'If TypeMat.SelectedValue = "ekspedisi" Then
        'Else
        '    If DDLBusUnit.SelectedValue <> "ALL" Then
        '        sSql &= " AND suppoid IN (SELECT suppoid from QL_trnmr" & Typem1 & "mst mrm INNER JOIN QL_trnmr" & Typem1 & "dtl mrd ON mrm.mr" & Typem1 & "mstoid=mrd.mr" & Typem1 & "mstoid WHERE mrm.cmpcode='" & DDLBusUnit.SelectedValue & "' "
        '    Else
        '        sSql &= " AND suppoid IN (SELECT suppoid from QL_trnmr" & Typem1 & "mst mrm INNER JOIN QL_trnmr" & Typem1 & "dtl mrd ON mrm.mr" & Typem1 & "mstoid=mrd.mr" & Typem1 & "mstoid WHERE mrm.cmpcode LIKE '%' "
        '    End If
        'If FilterDDLStatus.SelectedValue = "In Process" Then
        '    sSql &= " AND mrm.mr" & Typem1 & "mststatus IN ('In Process')"
        'ElseIf FilterDDLStatus.SelectedValue = "Post" Then
        '    sSql &= " AND mrm.mr" & Typem1 & "mststatus IN ('Post','Closed')"
        'End If
        sSql &= "ORDER BY suppcode"
        'End If
        Session("TblSupp") = cKon.ambiltabel(sSql, "QL_mstsupp")
    End Sub

    Private Sub BindListMR()
        CheckType()

        sSql = "SELECT DISTINCT 'False' AS checkvalue, mrm.mr" & Typem1 & "mstoid, mrm.mr" & Typem1 & "no, mrm.mr" & Typem1 & "date, CONVERT(VARCHAR(10), mrm.mr" & Typem1 & "date, 101) AS mrdate, s.suppname AS suppname, mrm.mr" & Typem1 & "mststatus, mrm.mr" & Typem1 & "mstnote FROM QL_trnmr" & Typem1 & "mst mrm INNER JOIN QL_trnmr" & Typem1 & "dtl mrd ON mrm.mr" & Typem1 & "mstoid=mrd.mr" & Typem1 & "mstoid INNER JOIN QL_mstsupp s ON mrm.suppoid=s.suppoid /*AND s.activeflag='ACTIVE'*/ "
        If DDLBusUnit.SelectedValue <> "ALL" Then
            sSql &= "WHERE mrm.cmpcode='" & DDLBusUnit.SelectedValue & "'"
        Else
            sSql &= "WHERE mrm.cmpcode LIKE '%%'"
        End If

        If suppcode.Text <> "" Then
            Dim scode() As String = Split(suppcode.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " s.suppcode = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If FilterDDLStatus.SelectedValue = "In Process" Then
            sSql &= " AND mrm.mr" & Typem1 & "mststatus IN ('In Process')"
        ElseIf FilterDDLStatus.SelectedValue = "Post" Then
            sSql &= " AND mrm.mr" & Typem1 & "mststatus IN ('Post','Closed')"
        End If

        If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
            If IsValidPeriod() Then
                sSql &= " AND mrm.mr" & Typem1 & "date >='" & FilterPeriod1.Text & " 00:00:00' AND mrm.mr" & Typem1 & "date <='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If

        'If checkPagePermission("~\ReportForm\frmRec" & Typem1 & "Report.aspx", Session("SpecialAccess")) = False Then
        '    sSql &= " AND mrm.createuser='" & Session("UserID") & "'"
        'End If

        sSql &= " ORDER BY mrm.mr" & Typem1 & "date DESC, mrm.mr" & Typem1 & "mstoid DESC"
        Session("TblMR") = cKon.ambiltabel(sSql, "QL_trnmr" & Typem1 & "mst")

    End Sub

    Private Sub BindListMat()
        sSql = "select distinct 'False' AS checkvalue,i.itemoid,i.itemCode,i.itemShortDescription,i.itemLongDescription,cat1.cat1longdesc AS cat1,cat2.cat2longdesc AS cat2,cat3.cat3longdesc AS cat3,cat4.gendesc AS cat4,g1.gendesc AS itemgroup from ql_mstitem i inner join QL_mstcat1 cat1 ON cat1.cat1oid=i.itemCat1 inner join QL_mstcat2 cat2 ON cat2.cat2oid=i.itemCat2 inner join QL_mstcat3 cat3 ON cat3.cat3oid=i.itemCat3 inner join QL_mstgen cat4 ON cat4.genoid=i.itemCat4 inner join QL_mstgen g1 ON g1.genoid=i.itemgroupoid WHERE i.itemRecordStatus='ACTIVE'"
        If FilterTextListMat.Text.Trim <> "" Then
            sSql &= IIf(FilterDDLListMat.SelectedValue = "ALL", "", " AND i." & FilterDDLListMat.SelectedValue & " like '%" & Tchar(FilterTextListMat.Text.Trim) & "%'")
        End If
        If cbCat01.Checked Then
            sSql &= IIf(DDLBusUnit.SelectedValue = "ALL", "", " AND i.itemCat1= " & FilterDDLCat01.SelectedValue & "")
        End If
        If cbCat02.Checked Then
            sSql &= IIf(DDLBusUnit.SelectedValue = "ALL", "", " AND i.itemCat2= " & FilterDDLCat02.SelectedValue & "")
        End If
        If cbCat03.Checked Then
            sSql &= IIf(DDLBusUnit.SelectedValue = "ALL", "", " AND i.itemCat3= " & FilterDDLCat03.SelectedValue & "")
        End If
        If cbCat04.Checked Then
            sSql &= IIf(DDLBusUnit.SelectedValue = "ALL", "", " AND i.itemCat4= " & FilterDDLCat04.SelectedValue & "")
        End If

        'CheckType()
        'If TypeMat.SelectedValue = "ekspedisi" Then
        'sSql = "select distinct 'False' AS checkvalue, case porefname when 'QL_MSTITEM' then (select itemoid from ql_mstitem i where i.itemoid=pod.porefoid) when 'QL_MSTMATRAW' then (select matrawoid from ql_mstmatraw where matrawoid=pod.porefoid) when 'QL_MSTMATGEN' then (select matgenoid from ql_mstmatgen where matgenoid=pod.porefoid) when 'QL_MSTSPAREPART' then (select sparepartoid from ql_mstsparepart where sparepartoid=pod.porefoid) end matoid, case porefname when 'QL_MSTITEM' then (select itemlongdesc from ql_mstitem i where i.itemoid=pod.porefoid) when 'QL_MSTMATRAW' then (select matrawlongdesc from ql_mstmatraw where matrawoid=pod.porefoid) when 'QL_MSTMATGEN' then (select matgenlongdesc from ql_mstmatgen where matgenoid=pod.porefoid) when 'QL_MSTSPAREPART' then (select sparepartlongdesc from ql_mstsparepart where sparepartoid=pod.porefoid) end matlongdesc, case porefname when 'QL_MSTITEM' then (select itemcode from ql_mstitem i where i.itemoid=pod.porefoid) when 'QL_MSTMATRAW' then (select matrawcode from ql_mstmatraw where matrawoid=pod.porefoid) when 'QL_MSTMATGEN' then (select matgencode from ql_mstmatgen where matgenoid=pod.porefoid) when 'QL_MSTSPAREPART' then (select sparepartcode from ql_mstsparepart where sparepartoid=pod.porefoid) end matcode, g.gendesc unit from QL_trnpoekspedisidtl pod inner join QL_trnpoekspedisimst pom on pom.poekspedisimstoid=pod.poekspedisimstoid inner join QL_mstgen g on g.genoid=pod.poekspedisiunitoid "
        'Else
        'sSql = "SELECT DISTINCT'False' AS checkvalue, mrd." & Typem2 & "oid as matoid, m." & Typem2 & "longdesc AS matlongdesc , m." & Typem2 & "code AS matcode , g2.gendesc unit FROM QL_trnmr" & Typem1 & "dtl mrd INNER JOIN QL_trnmr" & Typem1 & "mst mrm ON mrm.cmpcode=mrd.cmpcode AND mrm.mr" & Typem1 & "mstoid=mrd.mr" & Typem1 & "mstoid INNER JOIN QL_mst" & Typem2 & " m ON m." & Typem2 & "oid=mrd." & Typem2 & "oid INNER JOIN QL_mstgen g2 ON g2.genoid=mrd.mr" & Typem1 & "unitoid INNER JOIN QL_mstsupp s ON mrm.suppoid=s.suppoid /*AND s.activeflag='ACTIVE'*/ "
        'End If

        'If TypeMat.SelectedValue = "ekspedisi" Then
        'Else
        '    If DDLBusUnit.SelectedValue <> "ALL" Then
        '        sSql &= "WHERE mrm.cmpcode='" & DDLBusUnit.SelectedValue & "'"
        '    Else
        '        sSql &= "WHERE mrm.cmpcode LIKE '%%'"
        '    End If
        'End If



        'If suppcode.Text <> "" Then
        '    Dim scode() As String = Split(suppcode.Text, ";")
        '    sSql &= " AND ("
        '    For c1 As Integer = 0 To scode.Length - 1
        '        sSql &= " s.suppcode = '" & Tchar(scode(c1)) & "'"
        '        If c1 < scode.Length - 1 Then
        '            sSql &= " OR "
        '        End If
        '    Next
        '    sSql &= ")"
        'End If

        'If mrno.Text <> "" Then
        '    If DDLMRNo.SelectedValue = "MR No." Then
        '        Dim sMRno() As String = Split(mrno.Text, ";")
        '        sSql &= " AND ("
        '        For c1 As Integer = 0 To sMRno.Length - 1
        '            sSql &= " mrm.mr" & Typem1 & "no LIKE '%" & Tchar(sMRno(c1)) & "%'"
        '            If c1 < sMRno.Length - 1 Then
        '                sSql &= " OR "
        '            End If
        '        Next
        '        sSql &= ")"
        '    Else
        '        Dim sMRno() As String = Split(mrno.Text, ";")
        '        sSql &= " AND ("
        '        For c1 As Integer = 0 To sMRno.Length - 1
        '            sSql &= " mrm.mr" & Typem1 & "mstoid = " & ToDouble(sMRno(c1))
        '            If c1 < sMRno.Length - 1 Then
        '                sSql &= " OR "
        '            End If
        '        Next
        '        sSql &= ")"
        '    End If
        'End If

        'If TypeMat.SelectedValue = "ekspedisi" Then
        '    If FilterDDLStatus.SelectedValue = "In Process" Then
        '        sSql &= " AND pom.poekspedisimststatus IN ('In Process')"
        '    ElseIf FilterDDLStatus.SelectedValue = "Post" Then
        '        sSql &= " AND pom.poekspedisimststatus IN ('Post','Closed')"
        '    End If
        'Else
        '    If FilterDDLStatus.SelectedValue = "In Process" Then
        '        sSql &= " AND mrm.mr" & Typem1 & "mststatus IN ('In Process')"
        '    ElseIf FilterDDLStatus.SelectedValue = "Post" Then
        '        sSql &= " AND mrm.mr" & Typem1 & "mststatus IN ('Post','Closed')"
        '    End If
        'End If


        'If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
        '    If IsValidPeriod() Then
        '        If TypeMat.SelectedValue = "ekspedisi" Then
        '            sSql &= " AND pom.poekspedisidate >='" & FilterPeriod1.Text & " 00:00:00' AND pom.poekspedisidate <='" & FilterPeriod2.Text & " 23:59:59'"
        '        Else
        '            sSql &= " AND mrm.mr" & Typem1 & "date >='" & FilterPeriod1.Text & " 00:00:00' AND mrm.mr" & Typem1 & "date <='" & FilterPeriod2.Text & " 23:59:59'"
        '        End If
        '    Else
        '        Exit Sub
        '    End If
        'End If

        'If checkPagePermission("~\ReportForm\frmRec" & Typem1 & "Report.aspx", Session("SpecialAccess")) = False Then
        '    sSql &= " AND mrm.createuser='" & Session("UserID") & "'"
        'End If

        Session("TblMat") = cKon.ambiltabel(sSql, "QL_mstitem")
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        Dim sWhere As String = "WHERE pom.cmpcode='" & CompnyCode & "'"
        Dim rptName As String = ""
        Dim sGroup As String = TypeMat.SelectedItem.ToString
        'Dim Slct As String = ""
        'Dim Join As String = ""
        Dim period1 As String = year1.SelectedValue & "" & month1.SelectedValue
        Dim period2 As String = year2.SelectedValue & "" & month2.SelectedValue

        'CheckType()
        Try
            'If DDLBusUnit.SelectedValue <> "ALL" Then
            '    If TypeMat.SelectedValue = "ekspedisi" Then
            '        sSql &= "WHERE pod.cmpcode='" & DDLBusUnit.SelectedValue & "'"
            '    Else
            '        sSql &= "WHERE mrm.cmpcode='" & DDLBusUnit.SelectedValue & "'"
            '    End If
            'Else
            If chkGroup.Checked Then
                sWhere &= "AND pom.pogroup='" & TypeMat.SelectedValue & "'"
            Else
                sGroup = "ALL"
            End If
            'End If

            If DDLcurr.SelectedItem.Text <> "ALL" Then               
                sWhere &= " AND pom.curroid=" & DDLcurr.SelectedValue & ""
            End If

            If suppcode.Text <> "" Then
                Dim scode() As String = Split(suppcode.Text, ";")
                sWhere &= " AND ("
                For c1 As Integer = 0 To scode.Length - 1
                    sWhere &= " sp.suppcode = '" & Tchar(scode(c1)) & "'"
                    If c1 < scode.Length - 1 Then
                        sWhere &= " OR "
                    End If
                Next
                sWhere &= ")"
            End If

            'If TypeMat.SelectedValue = "ekspedisi" Then
            If FilterDDLStatus.SelectedValue <> "ALL" Then
                sWhere &= " AND pom.pomststatus='" & FilterDDLStatus.SelectedValue & "'"
            End If
            'Else
            'If FilterDDLStatus.SelectedValue = "In Process" Then
            '    sSql &= " AND mrm.mr" & Typem1 & "mststatus IN ('In Process')"
            'ElseIf FilterDDLStatus.SelectedValue = "Post" Then
            '    sSql &= " AND mrm.mr" & Typem1 & "mststatus IN ('Post')"
            'ElseIf FilterDDLStatus.SelectedValue = "Closed" Then
            '    sSql &= " AND mrm.mr" & Typem1 & "mststatus IN ('Closed')"
            'End If
            'End If

            'Filter Type Supplier
            'If suppType.SelectedValue = "All" Then
            'Else
            '    sSql &= " AND s.suppres1='" & suppType.SelectedValue & "'"
            'End If
            '==================================================================================


            If chkPeriod.Checked Then
                If period1 <= period2 Then
                    sWhere &= " AND pom.periodacctg >=" & period1 & " AND pom.periodacctg <=" & period2 & ""
                Else
                    Session("WarningPeriod") = "Period 1 must be less or equal than period 2!"
                    showMessage(Session("WarningPeriod"), 2)
                    Exit Sub
                End If
            End If

            'If mrno.Text <> "" Then
            '    If DDLMRNo.SelectedValue = "MR No." Then
            '        Dim sMRno() As String = Split(mrno.Text, ";")
            '        sWhere &= " AND ("
            '        For c1 As Integer = 0 To sMRno.Length - 1
            '            sWhere &= " mrm.mr" & Typem1 & "no LIKE '%" & Tchar(sMRno(c1)) & "%'"
            '            If c1 < sMRno.Length - 1 Then
            '                sWhere &= " OR"
            '            End If
            '        Next
            '        sWhere &= ")"
            '    Else
            '        Dim sMRno() As String = Split(mrno.Text, ";")
            '        sWhere &= " AND ("
            '        For c1 As Integer = 0 To sMRno.Length - 1
            '            sWhere &= " mrm.mr" & Typem1 & "mstoid = " & ToDouble(sMRno(c1))
            '            If c1 < sMRno.Length - 1 Then
            '                sWhere &= " OR"
            '            End If
            '        Next
            '        sWhere &= ")"
            '    End If
            'End If

            'If FilterDDLType.SelectedValue = "Detail" Then
            If matcode.Text <> "" Then
                Dim sMatcode() As String = Split(matcode.Text, ";")
                sWhere &= " AND ("
                For c1 As Integer = 0 To sMatcode.Length - 1
                    'If TypeMat.SelectedValue = "ekspedisi" Then
                    '    sSql &= "case porefname when 'QL_MSTITEM' then (select itemcode from ql_mstitem i where i.itemoid=pod.porefoid) when 'QL_MSTMATRAW' then (select matrawcode from ql_mstmatraw where matrawoid=pod.porefoid) when 'QL_MSTMATGEN' then (select matgencode from ql_mstmatgen where matgenoid=pod.porefoid) when 'QL_MSTSPAREPART' then (select sparepartcode from ql_mstsparepart where sparepartoid=pod.porefoid) end LIKE '%" & Tchar(sMatcode(c1)) & "%'"
                    'Else
                    sWhere &= " i.itemCode = '" & Tchar(sMatcode(c1)) & "'"
                    'End If
                    If c1 < sMatcode.Length - 1 Then
                        sWhere &= " OR"
                    End If
                Next
                sWhere &= ")"
            End If
            'End If

            'Dim orderTemp As String = ""
            'If TypeMat.SelectedValue = "ekspedisi" Then
            '    If DDLSorting.SelectedValue = "mrm.mr" Then
            '        orderTemp = "pom.poekspedisimstoid"
            '    ElseIf DDLSorting.SelectedValue = "pom.po" Then
            '        orderTemp = "poekspedisino"
            '    ElseIf DDLSorting.SelectedValue = "m." Then
            '        orderTemp = "matcode"
            '    Else
            '        orderTemp = "suppname"
            '    End If
            'Else
            '    If DDLSorting.SelectedValue = "mrm.mr" Then
            '        orderTemp = DDLSorting.SelectedValue & Typem1 & "no"
            '    ElseIf DDLSorting.SelectedValue = "pom.po" Then
            '        orderTemp = DDLSorting.SelectedValue & Typem1 & "no"
            '    ElseIf DDLSorting.SelectedValue = "m." Then
            '        orderTemp = DDLSorting.SelectedValue & Typem2 & "longdesc"
            '    Else
            '        orderTemp = DDLSorting.SelectedValue
            '    End If
            'End If

            report = New ReportDocument
            If DDLGrouping.SelectedValue = "Periode" Then
                report.Load(Server.MapPath(folderReport & "rptPORegister.rpt"))
                rptName = "PORegisterPeriode"
            ElseIf DDLGrouping.SelectedValue = "Supplier" Then
                report.Load(Server.MapPath(folderReport & "rptPORegSupp.rpt"))
                rptName = "PORegisterSupp"
            ElseIf DDLGrouping.SelectedValue = "Item" Then
                report.Load(Server.MapPath(folderReport & "rptPORegItem.rpt"))
                rptName = "PORegisterItem"
            End If

            cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("sType", sGroup)
            'vReport.SetParameterValue("startperiod", dateAwal.Text)
            'vReport.SetParameterValue("endperiod", dateAkhir.Text)
            'vReport.SetParameterValue("filterPO", IIf(oid.Text.Trim = "", "ALL", nota.Text))
            'vReport.SetParameterValue("filterGroup", IIf(FilterDDLGrup.SelectedValue = "ALL", "ALL", FilterDDLGrup.SelectedItem.Text))
            'vReport.SetParameterValue("filterSubGroup", IIf(FilterDDLSubGrup.SelectedValue = "ALL SUB GROUP", "ALL SUB GROUP", FilterDDLSubGrup.SelectedItem.Text))
            'vReport.SetParameterValue("filterItem", IIf(itemoid.Text.Trim = "", "ALL", itemname.Text))
            'vReport.SetParameterValue("filterMerk", IIf(merk.Text.Trim = "", "ALL", merk.Text))
            'vReport.SetParameterValue("filterSupplier", IIf(suppoid.Text.Trim = "", "ALL", suppname.Text))
            'vReport.SetParameterValue("filterTaxable", IIf(taxable.SelectedValue = "ALL", "ALL", taxable.SelectedValue))
            'report.SetParameterValue("reportName", report.FileName.Substring(report.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))

            'Session("diprint") = "True"

            If sType = "" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
            ElseIf sType = "pdf" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, rptName & Format(GetServerTime, "dd_MM_yy"))
                report.Close() : report.Dispose()
            ElseIf sType = "excel" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, rptName & Format(GetServerTime, "dd_MM_yy"))
                report.Close() : report.Dispose()
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Private Sub InitAllDDL()
        ' DDL Type item
        sSql = "select gencode,gendesc from QL_mstgen where gengroup='GROUPITEM' order by gendesc"
        FillDDL(TypeMat, sSql)
        ' DDL Currency
        sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE activeflag='ACTIVE'"
        FillDDL(DDLcurr, sSql)
        'ddl Cat04
        sSql = "select genoid,gendesc from QL_mstgen where gengroup='CAT4' order by gendesc"
        FillDDL(FilterDDLCat04, sSql)
    End Sub

    Private Sub InitFilterDDLCat1()
        'Fill DDL Category1
        sSql = "SELECT cat1code, cat1code+' - '+cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat1res1='Raw' Order By cat1code"
        FillDDL(FilterDDLCat01, sSql)
        If FilterDDLCat01.Items.Count > 0 Then
            InitFilterDDLCat2()
        Else
            FilterDDLCat02.Items.Clear()
            FilterDDLCat03.Items.Clear()
            FilterDDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitFilterDDLCat2()
        'Fill DDL Category2
        sSql = "SELECT cat2code, cat2code+' - '+cat2shortdesc FROM QL_mstcat2 cat2 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat2.cmpcode AND cat1.cat1oid=cat2.cat1oid AND cat1.cat1code='" & FilterDDLCat01.SelectedValue & "' WHERE cat2.cmpcode='" & CompnyCode & "' AND cat2.activeflag='ACTIVE' AND cat2res1='Raw' Order By cat2code"
        FillDDL(FilterDDLCat02, sSql)
        If FilterDDLCat02.Items.Count > 0 Then
            InitFilterDDLCat3()
        Else
            FilterDDLCat03.Items.Clear()
            FilterDDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitFilterDDLCat3()
        'Fill DDL Category3
        sSql = "SELECT cat3code, cat3code+' - '+cat3shortdesc FROM QL_mstcat3 cat3 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat3.cmpcode AND cat1.cat1oid=cat3.cat1oid AND cat1.cat1code='" & FilterDDLCat01.SelectedValue & "' INNER JOIN QL_mstcat2 cat2 ON cat2.cmpcode=cat3.cmpcode AND cat2.cat2oid=cat3.cat2oid AND cat2.cat2code='" & FilterDDLCat02.SelectedValue & "' WHERE cat3.cmpcode='" & CompnyCode & "' AND cat3.activeflag='ACTIVE' AND cat3res1='Raw' Order By cat3code"
        FillDDL(FilterDDLCat03, sSql)
        'If FilterDDLCat03.Items.Count > 0 Then
        '    InitFilterDDLCat4()
        'Else
        '    FilterDDLCat04.Items.Clear()
        'End If
    End Sub

    Private Sub InitFilterDDLCat4()
        'Fill DDL Category4
        'sSql = "SELECT cat4code, cat4code+' - '+cat4shortdesc FROM QL_mstcat4 cat4 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat4.cmpcode AND cat1.cat1oid=cat4.cat1oid AND cat1.cat1code='" & FilterDDLCat01.SelectedValue & "' INNER JOIN QL_mstcat2 cat2 ON cat2.cmpcode=cat4.cmpcode AND cat2.cat2oid=cat4.cat2oid AND cat2.cat2code='" & FilterDDLCat02.SelectedValue & "' INNER JOIN QL_mstcat3 cat3 ON cat3.cmpcode=cat4.cmpcode AND cat3.cat3oid=cat4.cat3oid AND cat3.cat3code='" & FilterDDLCat03.SelectedValue & "' WHERE cat4.cmpcode='" & CompnyCode & "' AND cat4.activeflag='ACTIVE' AND cat4res1='Raw' Order By cat4code"
        'FillDDL(FilterDDLCat04, sSql)
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmPORegister.aspx")

        End If
        If checkPagePermission("~\ReportForm\frmPORegister.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        Page.Title = CompnyName & " - Purchase Register Report"
        If Not Page.IsPostBack Then
            InitAllDDL()
            'InitFilterDDLCat1()
            'FilterDDLType_SelectedIndexChanged(Nothing, Nothing)
            'FilterDDLStatus_SelectedIndexChanged(Nothing, Nothing)
            DDLGrouping_SelectedIndexChanged(Nothing, Nothing)
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            'FilterPeriod2.Text = thisMonth.AddMonths(1).AddDays(-1).ToString("MM/dd/yyyy")
            year1.Items.Clear()
            year2.Items.Clear()
            Dim thn As String = GetServerTime().Year + 2
            For i As Integer = GetServerTime().Year - 1 To thn
                year1.Items.Add(i)
                year2.Items.Add(i)
            Next
            year1.SelectedValue = GetServerTime().Year
            year2.SelectedValue = GetServerTime().Year

            month1.SelectedValue = GetServerTime().Month
            month2.SelectedValue = GetServerTime().Month
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListSupp") Is Nothing And Session("EmptyListSupp") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListSupp") Then
                Session("EmptyListSupp") = Nothing
                cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
            End If
        End If
        If Not Session("WarningListSupp") Is Nothing And Session("WarningListSupp") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListSupp") Then
                Session("WarningListSupp") = Nothing
                cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
            End If
        End If
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If

        If Not Session("EmptyListMR") Is Nothing And Session("EmptyListMR") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMR") Then
                Session("EmptyListMR") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListMR, PanelListMR, mpeListMR, True)
            End If
        End If
        If Not Session("WarningListMR") Is Nothing And Session("WarningListMR") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMR") Then
                Session("WarningListMR") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListMR, PanelListMR, mpeListMR, True)
            End If
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        btnClearSupp_Click(Nothing, Nothing)
    End Sub

    Protected Sub FilterDDLStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLStatus.SelectedIndexChanged
        CheckType()
        If FilterDDLStatus.SelectedValue = "Post" Then
            'Fill DDL Date
            FilterDDLDate.Items.Clear()
            FilterDDLDate.Items.Add("MR Date")
            FilterDDLDate.Items.Item(FilterDDLDate.Items.Count - 1).Value = "mrm.mr" & Typem1 & "date"
            FilterDDLDate.Items.Add("Post Date")
            FilterDDLDate.Items.Item(FilterDDLDate.Items.Count - 1).Value = "mrm.updtime"
        Else
            'Fill DDL Date
            FilterDDLDate.Items.Clear()
            FilterDDLDate.Items.Add("MR Date")
            FilterDDLDate.Items.Item(FilterDDLDate.Items.Count - 1).Value = "mrm.mr" & Typem1 & "date"
        End If
    End Sub

    Protected Sub FilterDDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLType.SelectedIndexChanged
        If FilterDDLType.SelectedValue = "Summary" Then
            lblMat.Visible = False
            lblm.Visible = False
            matcode.Visible = False
            imbFindMat.Visible = False
            imbEraseMat.Visible = False
            lblGrouping.Visible = False
            lblg.Visible = False
            DDLGrouping.Visible = False
            lblSorting.Visible = False
            lbls.Visible = False
            DDLSorting.Visible = False
            DDLOrderby.Visible = False
            lblmin.Visible = False
        Else
            lblMat.Visible = True
            lblm.Visible = True
            matcode.Visible = True
            imbFindMat.Visible = True
            imbEraseMat.Visible = True
            lblGrouping.Visible = True
            lblg.Visible = True
            DDLGrouping.Visible = True
            lblSorting.Visible = True
            lbls.Visible = True
            DDLSorting.Visible = True
            DDLOrderby.Visible = True
            lblmin.Visible = True
            DDLGrouping_SelectedIndexChanged(Nothing, Nothing)
            ''Fill DDL Sorting
            'DDLSorting.Items.Clear()
            'DDLSorting.Items.Add("MR No.")
            'DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "mrm.mrrawno"
            'DDLSorting.Items.Add("Material Code")
            'DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "matrawcode"
        End If
    End Sub

    Protected Sub btnClearSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupp.Click
        'suppname.Text = ""
        'suppoid.Text = ""
        'If mrrawno.Text = "" And matcode.Text = "" Then
        '    DDLBusUnit.Enabled = True
        '    DDLBusUnit.CssClass = "inpText"
        'End If
        suppcode.Text = ""
    End Sub

    Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupp.Click
        If IsValidPeriod() Then
            DDLFilterListSupp.SelectedIndex = -1 : txtFilterListSupp.Text = ""
            Session("TblSupp") = Nothing : Session("TblSuppView") = Nothing : gvListSupp.DataSource = Nothing
            gvListSupp.DataBind()
            cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub btnFindListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSupp.Click
        If Session("TblSupp") Is Nothing Then
            BindListSupp()
            If Session("TblSupp").Rows.Count <= 0 Then
                Session("EmptyListSupp") = "Supplier data can't be found!"
                showMessage(Session("EmptyListSupp"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListSupp.SelectedValue & " LIKE '%" & Tchar(txtFilterListSupp.Text) & "%'"
        If UpdateCheckedSupp() Then
            Dim dv As DataView = Session("TblSupp").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblSuppView") = dv.ToTable
                gvListSupp.DataSource = Session("TblSuppView")
                gvListSupp.DataBind()
                dv.RowFilter = ""
                mpeListSupp.Show()
            Else
                dv.RowFilter = ""
                Session("TblSuppView") = Nothing
                gvListSupp.DataSource = Session("TblSuppView")
                gvListSupp.DataBind()
                Session("WarningListSupp") = "Supplier data can't be found!"
                showMessage(Session("WarningListSupp"), 2)
            End If
        Else
            mpeListSupp.Show()
        End If
    End Sub

    Protected Sub btnViewAllListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListSupp.Click
        DDLFilterListSupp.SelectedIndex = -1 : txtFilterListSupp.Text = ""
        If Session("TblSupp") Is Nothing Then
            BindListSupp()
            If Session("TblSupp").Rows.Count <= 0 Then
                Session("EmptyListSupp") = "Supplier data can't be found!"
                showMessage(Session("EmptyListSupp"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedSupp() Then
            Dim dt As DataTable = Session("TblSupp")
            Session("TblSuppView") = dt
            gvListSupp.DataSource = Session("TblSuppView")
            gvListSupp.DataBind()
        End If
        mpeListSupp.Show()
    End Sub

    Protected Sub btnSelectAllSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllSupp.Click
        If Not Session("TblSuppView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSuppView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSupp")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "suppoid=" & dtTbl.Rows(C1)("suppoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSupp") = objTbl
                Session("TblSuppView") = dtTbl
                gvListSupp.DataSource = Session("TblSuppView")
                gvListSupp.DataBind()
            End If
            mpeListSupp.Show()
        Else
            Session("WarningListSupp") = "Please show some Supplier data first!"
            showMessage(Session("WarningListSupp"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneSupp.Click
        If Not Session("TblSuppView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSuppView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSupp")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "suppoid=" & dtTbl.Rows(C1)("suppoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSupp") = objTbl
                Session("TblSuppView") = dtTbl
                gvListSupp.DataSource = Session("TblSuppView")
                gvListSupp.DataBind()
            End If
            mpeListSupp.Show()
        Else
            Session("WarningListSupp") = "Please show some Supplier data first!"
            showMessage(Session("WarningListSupp"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedSupp.Click
        If Session("TblSupp") Is Nothing Then
            Session("WarningListSupp") = "Selected Supplier data can't be found!"
            showMessage(Session("WarningListSupp"), 2)
            Exit Sub
        End If
        If UpdateCheckedSupp() Then
            Dim dtTbl As DataTable = Session("TblSupp")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListSupp.SelectedIndex = -1 : txtFilterListSupp.Text = ""
                Session("TblSuppView") = dtView.ToTable
                gvListSupp.DataSource = Session("TblSuppView")
                gvListSupp.DataBind()
                dtView.RowFilter = ""
                mpeListSupp.Show()
            Else
                dtView.RowFilter = ""
                Session("TblSuppView") = Nothing
                gvListSupp.DataSource = Session("TblSuppView")
                gvListSupp.DataBind()
                Session("WarningListSupp") = "Selected Supplier data can't be found!"
                showMessage(Session("WarningListSupp"), 2)
            End If
        Else
            mpeListSupp.Show()
        End If
    End Sub

    Protected Sub gvListSupp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSupp.PageIndexChanging
        If UpdateCheckedSupp2() Then
            gvListSupp.PageIndex = e.NewPageIndex
            gvListSupp.DataSource = Session("TblSuppView")
            gvListSupp.DataBind()
        End If
        mpeListSupp.Show()
    End Sub 'OK

    Protected Sub lkbAddToListListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListSupp.Click
        If Not Session("TblSupp") Is Nothing Then
            If UpdateCheckedSupp() Then
                Dim dtTbl As DataTable = Session("TblSupp")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If suppcode.Text <> "" Then
                            suppcode.Text &= ";" + vbCrLf + dtView(C1)("suppcode")
                        Else
                            suppcode.Text = dtView(C1)("suppcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
                Else
                    Session("WarningListSupp") = "Please select Supplier to add to list!"
                    showMessage(Session("WarningListSupp"), 2)
                    Exit Sub
                End If
            End If
        Else

            Exit Sub
        End If
    End Sub

    Protected Sub lkbCloseListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListSupp.Click
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub DDLMRNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLMRNo.SelectedIndexChanged
        mrno.Text = ""
    End Sub

    Protected Sub imbFindMR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindMR.Click
        If IsValidPeriod() Then
            DDLFilterListMR.SelectedIndex = -1 : txtFilterListMR.Text = ""
            Session("TblMR") = Nothing : Session("TblMRView") = Nothing : gvListMR.DataSource = Nothing : gvListMR.DataBind()
            cProc.SetModalPopUpExtender(btnHiddenListMR, PanelListMR, mpeListMR, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbEraseMR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseMR.Click
        mrno.Text = ""
        'If suppname.Text = "" And matcode.Text = "" Then
        '    DDLBusUnit.Enabled = True
        '    DDLBusUnit.CssClass = "inpText"
        'End If
    End Sub

    Protected Sub btnFindListMR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMR.Click
        If Session("TblMR") Is Nothing Then
            BindListMR()
            If Session("TblMR").Rows.Count <= 0 Then
                Session("EmptyListMR") = "MR data can't be found!"
                showMessage(Session("EmptyListMR"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListMR.SelectedValue & " LIKE '%" & Tchar(txtFilterListMR.Text) & "%'"
        If UpdateCheckedMR() Then
            Dim dv As DataView = Session("TblMR").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMRView") = dv.ToTable
                gvListMR.DataSource = Session("TblMRView")
                gvListMR.DataBind()
                dv.RowFilter = ""
                mpeListMR.Show()
            Else
                dv.RowFilter = ""
                Session("TblMRView") = Nothing
                gvListMR.DataSource = Session("TblMRView")
                gvListMR.DataBind()
                Session("WarningListMR") = "MR data can't be found!"
                showMessage(Session("WarningListMR"), 2)
            End If
        Else
            mpeListMR.Show()
        End If
    End Sub

    Protected Sub btnViewAllListMR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListMR.Click
        DDLFilterListMR.SelectedIndex = -1 : txtFilterListMR.Text = ""
        If Session("TblMR") Is Nothing Then
            BindListMR()
            If Session("TblMR").Rows.Count <= 0 Then
                Session("EmptyListMR") = "MR data can't be found!"
                showMessage(Session("EmptyListMR"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMR() Then
            Dim dt As DataTable = Session("TblMR")
            Session("TblMRView") = dt
            gvListMR.DataSource = Session("TblMRView")
            gvListMR.DataBind()
        End If
        mpeListMR.Show()
    End Sub

    Protected Sub btnSelectAllMR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllMR.Click
        If Not Session("TblMRView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMRView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMR")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "mrrawmstoid=" & dtTbl.Rows(C1)("mrrawmstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMR") = objTbl
                Session("TblMRView") = dtTbl
                gvListMR.DataSource = Session("TblMRView")
                gvListMR.DataBind()
            End If
            mpeListMR.Show()
        Else
            Session("WarningListMR") = "Please show some MR data first!"
            showMessage(Session("WarningListMR"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneMR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneMR.Click
        If Not Session("TblMRView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMRView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMR")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "mrrawmstoid=" & dtTbl.Rows(C1)("mrrawmstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMR") = objTbl
                Session("TblMRView") = dtTbl
                gvListMR.DataSource = Session("TblMRView")
                gvListMR.DataBind()
            End If
            mpeListMR.Show()
        Else
            Session("WarningListMR") = "Please show some MR data first!"
            showMessage(Session("WarningListMR"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedMR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedMR.Click
        If Session("TblMR") Is Nothing Then
            Session("WarningListMR") = "Selected MR data can't be found!"
            showMessage(Session("WarningListMR"), 2)
            Exit Sub
        End If
        If UpdateCheckedMR() Then
            Dim dtTbl As DataTable = Session("TblMR")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListMR.SelectedIndex = -1 : txtFilterListMR.Text = ""
                Session("TblMRView") = dtView.ToTable
                gvListMR.DataSource = Session("TblMRView")
                gvListMR.DataBind()
                dtView.RowFilter = ""
                mpeListMR.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMRView") = Nothing
                gvListMR.DataSource = Session("TblMRView")
                gvListMR.DataBind()
                Session("WarningListMR") = "Selected MR data can't be found!"
                showMessage(Session("WarningListMR"), 2)
            End If
        Else
            mpeListMR.Show()
        End If
    End Sub

    Protected Sub gvListMR_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMR.PageIndexChanging
        If UpdateCheckedMR2() Then
            gvListMR.PageIndex = e.NewPageIndex
            gvListMR.DataSource = Session("TblMRView")
            gvListMR.DataBind()
        End If
        mpeListMR.Show()
    End Sub 'OK

    Protected Sub lkbAddToListListMR_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMR.Click
        If Not Session("TblMR") Is Nothing Then
            If UpdateCheckedMR() Then
                Dim dtTbl As DataTable = Session("TblMR")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If mrno.Text <> "" Then
                            If DDLMRNo.SelectedValue = "MR No." Then
                                If dtView(C1)("mrrawno") <> "" Then
                                    mrno.Text &= ";" + vbCrLf + dtView(C1)("mrrawno")
                                End If
                            Else
                                mrno.Text &= ";" + vbCrLf + dtView(C1)("mrrawmstoid").ToString
                            End If
                        Else
                            If DDLMRNo.SelectedValue = "MR No." Then
                                If dtView(C1)("mrrawno") <> "" Then
                                    mrno.Text &= dtView(C1)("mrrawno")
                                End If
                            Else
                                mrno.Text &= dtView(C1)("mrrawmstoid").ToString
                            End If
                        End If
                    Next
                    dtView.RowFilter = ""
                    DDLBusUnit.Enabled = False
                    DDLBusUnit.CssClass = "inpTextDisabled"
                    cProc.SetModalPopUpExtender(btnHiddenListMR, PanelListMR, mpeListMR, False)
                Else
                    Session("WarningListMR") = "Please select MR to add to list!"
                    showMessage(Session("WarningListMR"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMR") = "Please show some MR data first!"
            showMessage(Session("WarningListMR"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListMR_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListMR.Click
        cProc.SetModalPopUpExtender(btnHiddenListMR, PanelListMR, mpeListMR, False)
    End Sub

    Protected Sub FilterDDLCat01_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat01.SelectedIndexChanged
        InitFilterDDLCat2()
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat02_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat02.SelectedIndexChanged
        InitFilterDDLCat3()
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat03_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat03.SelectedIndexChanged
        InitFilterDDLCat4()
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat04_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat04.SelectedIndexChanged
        mpeListMat.Show()
    End Sub

    Protected Sub imbFindMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindMat.Click
        If IsValidPeriod() Then
            'If TypeMat.SelectedValue = "service" Then
            '    lblTitleListMat.Text = "List of Service"
            'Else
            '    lblTitleListMat.Text = "List of Material"
            'End If
            FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
            InitFilterDDLCat1()
            cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
            Session("TblMat") = Nothing : Session("TblMatView") = Nothing : gvListMat.DataSource = Nothing
            gvListMat.DataBind() : tbData.Text = "5" : gvListMat.PageSize = CInt(tbData.Text)
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            Exit Sub
        End If
    End Sub 'OK

    Protected Sub imbEraseMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseMat.Click
        matcode.Text = ""
        'If mrno.Text = "" And suppname.Text = "" Then
        '    DDLBusUnit.Enabled = True
        '    DDLBusUnit.CssClass = "inpText"
        'End If
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblMat") Is Nothing Then
            BindListMat()

            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        If cbCat01.Checked Then
            If FilterDDLCat01.SelectedValue <> "" Then
                sPlus &= " AND SUBSTRING(matrawcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'"
            Else
                cbCat01.Checked = False
            End If
        End If
        If cbCat02.Checked Then
            If FilterDDLCat02.SelectedValue <> "" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND SUBSTRING(matrawcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'") & " AND SUBSTRING(matrawcode, 4, 3)='" & FilterDDLCat02.SelectedValue & "'"
            Else
                cbCat02.Checked = False
            End If
        End If
        If cbCat03.Checked Then
            If FilterDDLCat03.SelectedValue <> "" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND SUBSTRING(matrawcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'") & IIf(cbCat02.Checked, "", " AND SUBSTRING(matrawcode, 4, 3)='" & FilterDDLCat02.SelectedValue & "'") & " AND SUBSTRING(matrawcode, 8, 4)='" & FilterDDLCat03.SelectedValue & "'"
            Else
                cbCat03.Checked = False
            End If
        End If
        If cbCat04.Checked Then
            If FilterDDLCat04.SelectedValue <> "" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND SUBSTRING(matrawcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'") & IIf(cbCat02.Checked, "", " AND SUBSTRING(matrawcode, 4, 3)='" & FilterDDLCat02.SelectedValue & "'") & IIf(cbCat03.Checked, "", " AND SUBSTRING(matrawcode, 8, 4)='" & FilterDDLCat03.SelectedValue & "'") & " AND SUBSTRING(matrawcode, 13, 4)='" & FilterDDLCat04.SelectedValue & "'"
            Else
                cbCat04.Checked = False
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub 'OK

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        InitFilterDDLCat1()
        cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub 'OK

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If matcode.Text <> "" Then
                            matcode.Text &= ";" + vbCrLf + dtView(C1)("itemCode")
                        Else
                            matcode.Text = dtView(C1)("itemCode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some Material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                InitFilterDDLCat1()
                cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Selected material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub lbShowData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbShowData.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Please find material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If tbData.Text = "" Then
            Session("WarningListMat") = "Please fill total data to be shown!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        Else
            If ToDouble(tbData.Text) <= 0 Then
                Session("WarningListMat") = "Total data to be shown must be more than 0!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat2() Then
            gvListMat.PageSize = Int(tbData.Text)
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub DDLGrouping_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLGrouping.SelectedIndexChanged
        If DDLGrouping.SelectedValue = "Periode" Then
            Label5.Visible = False
            lblg.Visible = False
            suppcode.Visible = False
            btnSearchSupp.Visible = False
            btnClearSupp.Visible = False
            lblMat.Visible = False
            lblm.Visible = False
            matcode.Visible = False
            imbFindMat.Visible = False
            imbEraseMat.Visible = False
        ElseIf DDLGrouping.SelectedValue = "Supplier" Then
            Label5.Visible = True
            lblg.Visible = True
            suppcode.Visible = True
            btnSearchSupp.Visible = True
            btnClearSupp.Visible = True
            lblMat.Visible = False
            lblm.Visible = False
            matcode.Visible = False
            imbFindMat.Visible = False
            imbEraseMat.Visible = False
        ElseIf DDLGrouping.SelectedValue = "Item" Then
            Label5.Visible = False
            lblg.Visible = False
            suppcode.Visible = False
            btnSearchSupp.Visible = False
            btnClearSupp.Visible = False
            lblMat.Visible = True
            lblm.Visible = True
            matcode.Visible = True
            imbFindMat.Visible = True
            imbEraseMat.Visible = True
        End If
        'If DDLGrouping.SelectedValue = "mrm.mrrawno" Then
        '    'Fill DDL Sorting
        '    DDLSorting.Items.Clear()
        '    DDLSorting.Items.Add("Material Code")
        '    DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "matrawcode"
        'ElseIf DDLGrouping.SelectedValue = "matrawcode" Then
        '    'Fill DDL Sorting
        '    DDLSorting.Items.Clear()
        '    DDLSorting.Items.Add("Supplier")
        '    DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "s.suppname"
        '    DDLSorting.Items.Add("MR No.")
        '    DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "mrm.mrrawno"
        'Else
        '    'Fill DDL Sorting
        '    DDLSorting.Items.Clear()
        '    DDLSorting.Items.Add("MR No.")
        '    DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "mrm.mrrawno"
        '    DDLSorting.Items.Add("Material Code")
        '    DDLSorting.Items.Item(DDLSorting.Items.Count - 1).Value = "matrawcode"
        'End If
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub 'OK

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("")
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("pdf")
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("excel")
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmPORegister.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not report Is Nothing Then
                If report.IsLoaded Then
                    report.Dispose()
                    report.Close()
                End If
            End If
        Catch ex As Exception
            report.Dispose()
            report.Close()
        End Try
    End Sub
#End Region

    Protected Sub TypeMat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        suppcode.Text = ""
        matcode.Text = ""
    End Sub

    Protected Sub CalPeriod1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub
End Class
