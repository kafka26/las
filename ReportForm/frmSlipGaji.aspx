<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="frmSlipGaji.aspx.vb" Inherits="ReportForm_frmSlipGaji" title="Untitled Page" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="Table2" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" style="text-align: left" valign="center">
                <asp:Label ID="Label7" runat="server" Font-Bold="False" Text=".: Print Out Slip Gaji" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="center">
    
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD align=center><TABLE><TBODY><TR><TD align=left></TD><TD align=left></TD><TD align=left><asp:DropDownList id="FilterDDLDiv" runat="server" CssClass="inpText" AutoPostBack="True" Width="200px" Visible="False">
            </asp:DropDownList></TD></TR>
    <tr>
        <td align="left">
            <asp:Label ID="Label6" runat="server" Text="Tanggal Print"></asp:Label></td>
        <td align="left">
            :</td>
        <td align="left">
            <asp:TextBox ID="tbDate" runat="server" CssClass="inpText" ToolTip="(MM/dd/yyyy)"
                Width="60px"></asp:TextBox>&nbsp;<asp:ImageButton ID="imbDate" runat="server" ImageAlign="AbsMiddle"
                    ImageUrl="~/Images/oCalendar.gif" /></td>
    </tr>
    <tr>
        <td align="left">
            User Priint</td>
        <td align="left">
            :</td>
        <td align="left">
            <asp:TextBox ID="tbUser" runat="server" CssClass="inpText" Width="150px"></asp:TextBox></td>
    </tr>
    <tr>
        <td align="left">
        </td>
        <td align="left">
        </td>
        <td align="left">
            <ajaxToolkit:CalendarExtender ID="ceDate" runat="server" Format="MM/dd/yyyy" PopupButtonID="imbDate"
                TargetControlID="tbDate">
            </ajaxToolkit:CalendarExtender>
            <ajaxToolkit:MaskedEditExtender ID="meeDate" runat="server" Mask="99/99/9999" MaskType="Date"
                TargetControlID="tbDate" UserDateFormat="MonthDayYear">
            </ajaxToolkit:MaskedEditExtender>
        </td>
    </tr>
    <tr>
        <td align="left">
            Bulan</td>
        <td align="left">
            :</td>
        <td align="left">
            <asp:DropDownList ID="ddlperiodtype" runat="server" AutoPostBack="True" CssClass="inpText"
                Width="100px">
                <asp:ListItem Value="MINGGU 1">MINGGU 1</asp:ListItem>
                <asp:ListItem Value="MINGGU 2">MINGGU 2</asp:ListItem>
            </asp:DropDownList></td>
    </tr>
    <TR><TD align=left>
        Periode</TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="ddlmonth" runat="server" CssClass="inpText" AutoPostBack="True" Width="100px">
            </asp:DropDownList> - <asp:DropDownList id="ddlyear" runat="server" CssClass="inpText" AutoPostBack="True" Width="75px">
            </asp:DropDownList></TD></TR><TR><TD align=left>Employee</TD><TD align=left>:</TD><TD align=left>
                <asp:TextBox ID="TxtPerson" runat="server" CssClass="inpTextDisabled" ReadOnly="True"
                    Width="145px"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchLP" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="Middle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseLP" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="Middle"></asp:ImageButton>
                <asp:Label ID="personoid" runat="server" Text="0" Visible="False"></asp:Label></TD></TR></TBODY></TABLE></TD></TR><TR><TD align=center><asp:ImageButton id="imbView" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbPDF" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbXLS" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD align=center><asp:UpdateProgress id="UpdateProgress2" runat="server">
        <ProgressTemplate>
            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/loadingbar.gif" />
            Please wait ..
        </ProgressTemplate>
    </asp:UpdateProgress> </TD></TR><TR><TD align=center><TABLE><TBODY><TR><TD align=left><CR:CrystalReportViewer id="CrvReport" runat="server" Width="350px" Height="50px" HasViewList="False" HasToggleGroupTreeButton="False" HasDrillUpButton="False" HasCrystalLogo="False" DisplayGroupTree="False" AutoDataBind="True" HasExportButton="False" HasPrintButton="False"></CR:CrystalReportViewer> </TD></TR></TBODY></TABLE></TD></TR><TR><TD align=center><asp:UpdatePanel id="upLP" runat="server"><ContentTemplate>
<asp:Panel id="pnlLP" runat="server" CssClass="modalBox" Width="600px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center><asp:Label id="lblTitleLP" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Employee"></asp:Label></TD></TR><TR><TD class="Label" align=center></TD></TR><TR><TD class="Label" align=center><asp:Panel id="pnlFindLP" runat="server" Width="100%" DefaultButton="btnFindLP">
                                    <table style="width: 70%">
                                        <tr>
                                            <td align="left" class="Label" style="width: 23%">
                                                <asp:Label ID="lblFilterLP" runat="server" Text="Filter"></asp:Label></td>
                                            <td align="center" class="Label" style="width: 2%">
                                                :</td>
                                            <td align="left" class="Label">
                                                <asp:DropDownList ID="FilterDDLLP" runat="server" CssClass="inpText" Width="100px">
                                                    <asp:ListItem Value="nip">NIP</asp:ListItem>
                                                    <asp:ListItem Value="personname">Name</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:TextBox ID="FilterTextLP" runat="server" CssClass="inpText" Width="170px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="Label" style="width: 23%">
                                            </td>
                                            <td align="center" class="Label" style="width: 2%">
                                            </td>
                                            <td align="left" class="Label">
                                                <asp:ImageButton ID="btnFindLP" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/btnfind.bmp" />
                                                <asp:ImageButton ID="btnAllLP" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewall.png" /></td>
                                        </tr>
                                    </table>
                                </asp:Panel> </TD></TR><TR><TD class="Label" align=center><asp:ImageButton id="btnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNone" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewChecked" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center></TD></TR><TR><TD class="Label" vAlign=top align=center><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset4"><DIV id="Div4"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="gvLP" runat="server" ForeColor="#333333" Width="97%" GridLines="None" EmptyDataRowStyle-ForeColor="Red" CellPadding="4" AutoGenerateColumns="False">
                                            <RowStyle BackColor="#EFF3FB" />
                                            <EmptyDataRowStyle ForeColor="Red" /> 

                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label19" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label>
                                            </EmptyDataTemplate>
                                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                            <HeaderStyle BackColor="#507CD1" BorderColor="White" CssClass="gvhdr" Font-Bold="True"
                                                ForeColor="White" />
                                            <EditRowStyle BackColor="#2461BF" />
                                            <AlternatingRowStyle BackColor="White" />
                                        </asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center></TD></TR><TR><TD class="Label" align=center><asp:ImageButton id="btnAddToListLP" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnCloseLP" runat="server" ImageUrl="~/Images/Close.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeLP" runat="server" TargetControlID="btnHideLP" PopupDragHandleControlID="lblTitleLP" PopupControlID="pnlLP" Drag="True" BackgroundCssClass="modalBackground">
                </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideLP" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False" Width="700px"><TABLE width="100%"><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD style="vertical-align: top; width: 40px; text-align: center"><asp:Image id="imIcon" runat="server" Height="24px" Width="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label" align="left"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>
    <asp:ImageButton ID="imbOKPopUp" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/ok.png" /></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" DropShadow="True" PopupDragHandleControlID="lblCaption" PopupControlID="pnlPopUpMsg" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel> </TD></TR><TR><TD align=left></TD></TR></TBODY></TABLE>
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="imbPDF"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="imbXLS"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td align="center">
                </td>
        </tr>
    </table>
</asp:Content>

