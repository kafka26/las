Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_frmAP
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function ValidateParam() As String
        Dim sReturn As String = ""
        Dim sErrTemp As String = ""

        If Not IsValidDate(txtStart.Text, "MM/dd/yyyy", sErrTemp) Then sReturn &= "- Invalid Start Date format: " & sErrTemp & ".<BR>"
        If Not IsValidDate(txtFinish.Text, "MM/dd/yyyy", sErrTemp) Then sReturn &= "- Invalid End Date format: " & sErrTemp & ".<BR>"
        If IsValidDate(txtStart.Text, "MM/dd/yyyy", sErrTemp) And IsValidDate(txtFinish.Text, "MM/dd/yyyy", sErrTemp) Then
            If DateDiff(DateInterval.Day, CDate(txtStart.Text), CDate(txtFinish.Text)) < 0 Then
                sReturn &= "- End Date must be greater than Start Date.<BR>"
            End If
        End If

        Return sReturn
    End Function

    Private Function GetIndoMonth(ByVal sVal As String) As String
        Select Case sVal
            Case "01"
                Return "JANUARI"
            Case "02"
                Return "FEBRUARI"
            Case "03"
                Return "MARET"
            Case "04"
                Return "APRIL"
            Case "05"
                Return "MEI"
            Case "06"
                Return "JUNI"
            Case "07"
                Return "JULI"
            Case "08"
                Return "AGUSTUS"
            Case "09"
                Return "SEPTEMBER"
            Case "10"
                Return "OKTOBER"
            Case "11"
                Return "NOVEMBER"
            Case "12"
                Return "DESEMBER"
            Case Else
                Return "Unknown"
        End Select
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindSupplierData()
        sSql = "SELECT 0 selected,suppoid,suppcode,suppname,suppaddr FROM QL_mstsupp WHERE activeflag='ACTIVE' AND (suppcode LIKE '%" & Tchar(FilterTextSupplier.Text) & "%' OR suppname LIKE '%" & Tchar(FilterTextSupplier.Text) & "%') ORDER BY suppname"
        Dim dtSupp As DataTable = cKon.ambiltabel(sSql, "QL_mstsupp")
        gvSupplier.DataSource = dtSupp : gvSupplier.DataBind()
        Session("QL_mstsupp") = dtSupp
        gvSupplier.Visible = True
        gvSupplier.SelectedIndex = -1
    End Sub

    Private Sub UpdateCheckedGV()
        If Not Session("QL_mstsupp") Is Nothing Then
            Dim dtab As DataTable = Session("QL_mstsupp")

            If dtab.Rows.Count > 0 Then
                Dim cb As System.Web.UI.WebControls.CheckBox
                Dim dView As DataView = dtab.DefaultView
                Dim drView As DataRowView

                For i As Integer = 0 To gvSupplier.Rows.Count - 1
                    cb = gvSupplier.Rows(i).FindControl("chkSelect")
                    dView.RowFilter = "suppoid = " & cb.ToolTip
                    drView = dView.Item(0)
                    drView.BeginEdit()
                    If cb.Checked = True Then
                        drView("selected") = 1
                    Else
                        drView("selected") = 0
                    End If
                    drView.EndEdit()
                    dView.RowFilter = ""
                Next
                dtab.AcceptChanges()
            End If
            Session("QL_mstsupp") = dtab
        End If
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        Try
            Dim sSuppOid As String = ""
            If rbSupplier.SelectedValue = "SELECT" Then
                Dim dtSupp As DataTable : dtSupp = Session("QL_mstsupp")
                Dim dvSupp As DataView = dtSupp.DefaultView
                dvSupp.RowFilter = "selected='1'"

                For R1 As Integer = 0 To dvSupp.Count - 1
                    sSuppOid &= dvSupp(R1)("suppoid").ToString & ","
                Next
            End If
            Dim swhere As String = "" : Dim swheresupp As String = ""
            If sSuppOid <> "" Then swhere = " WHERE suppoid IN (" & Left(sSuppOid, sSuppOid.Length - 1) & ")"
            If sSuppOid <> "" Then swheresupp = " WHERE s.suppoid IN (" & Left(sSuppOid, sSuppOid.Length - 1) & ")"
            Dim sFilterCOA As String = "", sFilterCOAPay As String = ""
            If FilterDDLCOA.SelectedValue <> "0" Then
                sFilterCOA = " AND c.acctgoid=" & FilterDDLCOA.SelectedValue
            End If
            If FilterDDLCOA.SelectedValue <> "0" Then
                sSql = "SELECT DISTINCT acctgoid FROM QL_conap con WHERE cmpcode='" & FilterDDLDiv.SelectedValue & "' AND payrefoid<>0 AND acctgoid NOT IN (SELECT DISTINCT conx.acctgoid FROM QL_conap conx WHERE conx.cmpcode=con.cmpcode AND conx.payrefoid=0 AND conx.suppoid=con.suppoid)"
                If sSuppOid <> "" Then sSql &= " AND suppoid IN (" & Left(sSuppOid, sSuppOid.Length - 1) & ")"
                Dim dtTmp As DataTable = cKon.ambiltabel(sSql, "QL_conapacctg")
                Dim sTmp As String = FilterDDLCOA.SelectedValue & ","
                For C1 As Integer = 0 To dtTmp.Rows.Count - 1
                    sTmp &= dtTmp.Rows(C1)("acctgoid").ToString & ","
                Next
                sFilterCOAPay = " AND c.acctgoid IN (" & Left(sTmp, sTmp.Length - 1) & ")"
            End If

            Dim queryGatel As String = "case suppoid when 243 then -137000 else 0.0 end"

            sSql = "DECLARE @cmpcode AS VARCHAR(10);" & _
                "DECLARE @periodacctg AS VARCHAR(10);" & _
                "DECLARE @currency AS VARCHAR(10);" & _
                "DECLARE @dateacuan AS DATETIME;" & _
                "SET @cmpcode='" & FilterDDLDiv.SelectedValue & "';" & _
                "SET @periodacctg='" & GetDateToPeriodAcctg(New Date(FilterDDLYear.SelectedValue, FilterDDLMonth.SelectedValue, 1)) & "';" & _
                "SET @dateacuan=CAST('" & Format(New Date(FilterDDLYear.SelectedValue, FilterDDLMonth.SelectedValue, Date.DaysInMonth(FilterDDLYear.SelectedValue, FilterDDLMonth.SelectedValue)), "MM/dd/yyyy") & " 23:59:59' AS DATETIME);" & _
                "SET @currency='" & FilterCurrency.SelectedValue & "';" & _
                "/*UNION ALL QUERY*/" & _
                "SELECT suppoid, suppname, SUM(saidr) + " & queryGatel & " saidr, SUM(sausd) + " & queryGatel & " sausd, SUM(beliidr) beliidr, " & _
                "SUM(beliusd) beliusd, SUM(paymentidr) paymentidr, SUM(paymentusd) paymentusd, " & _
                "SUM(ageblmjtidr) ageblmjtidr, SUM(ageblmjtusd) ageblmjtusd, " & _
                "SUM(age1idr) age1idr, SUM(age1usd) age1usd, SUM(age2idr) age2idr, SUM(age2usd) age2usd, " & _
                "SUM(age3idr) + " & queryGatel & " age3idr, SUM(age3usd) + " & queryGatel & " age3usd, @currency currency, @cmpcode cmpcode, " & _
                "'" & FilterDDLMonth.SelectedItem.Text.ToUpper & " " & FilterDDLYear.SelectedItem.Text & "' periodreport, '" & FilterDDLDiv.SelectedItem.Text.ToUpper & "' company " & _
                "FROM (" & _
                "/*SALDO AWAL PERIODE*/" & _
                "SELECT suppoid, suppname, " & _
                "(SUM(beliidr) - SUM(paymentidr)) saidr, " & _
                "(SUM(beliusd) - SUM(paymentusd)) sausd, " & _
                "0.0 beliidr, 0.0 beliusd, 0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, " & _
                "0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd FROM (" & _
                "SELECT suppoid, suppname, " & _
                "SUM(ISNULL(beliidr, 0.0)) beliidr, SUM(ISNULL(beliusd, 0.0)) beliusd, " & _
                "0.0 paymentidr, 0.0 paymentusd " & _
                "FROM (" & _
                "SELECT s.suppoid, s.suppname, " & _
                "ISNULL(c.amttransidr, 0.0) beliidr, ISNULL(c.amttransusd, 0.0) beliusd " & _
                "FROM QL_mstsupp s " & _
                "LEFT JOIN QL_conap c ON s.suppoid=c.suppoid " & sFilterCOA & " " & _
                "AND c.cmpcode=@cmpcode AND c.periodacctg<@periodacctg AND c.conapoid>0 " & _
                "AND payrefoid=0 AND trnapstatus='Post' " & _
                "UNION ALL " & _
                "SELECT s.suppoid, s.suppname, " & _
                "ISNULL(c.amtbayaridr * -1, 0.0) beliidr, ISNULL(c.amtbayarusd * -1, 0.0) beliusd " & _
                "FROM QL_mstsupp s " & _
                "LEFT JOIN QL_conap c ON s.suppoid=c.suppoid " & sFilterCOA & " " & _
                "AND c.cmpcode=@cmpcode AND (RIGHT(CONVERT(VARCHAR(10), c.paymentdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), c.paymentdate, 101), 2))<@periodacctg AND trnapstatus='Post' AND trnaptype IN ('CNAP', 'GCAP') " & _
                ") AS tblBeli GROUP BY suppoid, suppname " & _
                "UNION ALL " & _
                "SELECT suppoid, suppname, 0.0 beliidr, 0.0 beliusd," & _
                "SUM(paymentidr) paymentidr, SUM(paymentusd) paymentusd " & _
                "FROM (" & _
                "SELECT s.suppoid, s.suppname, " & _
                "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd " & _
                "FROM QL_mstsupp s " & _
                "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conap con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payapoid INNER JOIN QL_conap c ON c.cmpcode=con.cmpcode AND c.reftype=con.reftype AND c.refoid=con.refoid AND c.payrefoid=0 WHERE cb.cmpcode=@cmpcode AND ISNULL(payapres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnaptype NOT IN ('DNAP', 'CNAP', 'RETPI', 'GCAP') AND (CASE WHEN LEFT(cashbanktype, 2)='BB' THEN (RIGHT(CONVERT(VARCHAR(10), cashbankduedate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankduedate, 101), 2)) ELSE (RIGHT(CONVERT(VARCHAR(10), cashbankdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankdate, 101), 2)) END)<@periodacctg " & sFilterCOA & " GROUP BY con.suppoid, con.cmpcode) AS c ON c.suppoid=s.suppoid " & _
                "UNION ALL " & _
                "SELECT s.suppoid, s.suppname," & _
                "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd " & _
                "FROM QL_mstsupp s " & _
                "LEFT JOIN QL_conap c ON s.suppoid=c.suppoid " & sFilterCOA & " " & _
                "AND c.cmpcode=@cmpcode AND (RIGHT(CONVERT(VARCHAR(10), c.paymentdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), c.paymentdate, 101), 2))<@periodacctg AND trnapstatus='Post' AND c.trnaptype IN ('DNAP', 'RETPI') " & _
                ") AS tblpayment GROUP BY suppoid, suppname " & _
                ") AS tblSaldoAwalPeriod GROUP BY suppoid, suppname " & _
                "UNION ALL /*SALDO AWAL CUT OFF*/" & _
                "SELECT s.suppoid, s.suppname, " & _
                "(SUM(ISNULL(c.amttransidr, 0.0)) - SUM(ISNULL(c.amtbayaridr, 0.0))) AS saidr, " & _
                "(SUM(ISNULL(c.amttransusd, 0.0)) - SUM(ISNULL(c.amtbayarusd, 0.0))) AS sausd, " & _
                "0.0 beliidr, 0.0 beliusd, 0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, " & _
                "0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd " & _
                "FROM QL_mstsupp s " & _
                "LEFT JOIN QL_conap c ON s.suppoid=c.suppoid " & sFilterCOA & " " & _
                "AND c.cmpcode=@cmpcode AND c.conapoid<0 " & _
                "GROUP BY s.suppoid, s.suppname " & _
                "UNION ALL /*PEMBELIAN*/" & _
                "SELECT suppoid, suppname, 0.0 saidr, 0.0 sausd, " & _
                "SUM(ISNULL(beliidr, 0.0)) beliidr, SUM(ISNULL(beliusd, 0.0)) beliusd, " & _
                "0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, 0.0 age1idr, 0.0 age1usd, " & _
                "0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd " & _
                "FROM (" & _
                "SELECT s.suppoid, s.suppname, " & _
                "ISNULL(c.amttransidr, 0.0) beliidr, ISNULL(c.amttransusd, 0.0) beliusd " & _
                "FROM QL_mstsupp s " & _
                "LEFT JOIN QL_conap c ON s.suppoid=c.suppoid " & sFilterCOA & " " & _
                "AND c.cmpcode=@cmpcode AND c.periodacctg=@periodacctg AND c.conapoid>0 " & _
                "AND payrefoid=0 AND trnapstatus='Post' " & _
                "UNION ALL " & _
                "SELECT s.suppoid, s.suppname, " & _
                "ISNULL(c.amtbayaridr * -1, 0.0) beliidr, ISNULL(c.amtbayarusd * -1, 0.0) beliusd " & _
                "FROM QL_mstsupp s " & _
                "LEFT JOIN QL_conap c ON s.suppoid=c.suppoid " & sFilterCOA & " " & _
                "AND c.cmpcode=@cmpcode AND (RIGHT(CONVERT(VARCHAR(10), c.paymentdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), c.paymentdate, 101), 2))=@periodacctg AND trnapstatus='Post' AND trnaptype IN ('CNAP', 'GCAP') " & _
                ") AS tblBeli GROUP BY suppoid, suppname " & _
                "UNION ALL /*PAYMENT*/" & _
                "SELECT suppoid, suppname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd," & _
                "SUM(paymentidr) paymentidr, SUM(paymentusd) paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, " & _
                "0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd " & _
                "FROM (" & _
                "SELECT s.suppoid, s.suppname, " & _
                "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd " & _
                "FROM QL_mstsupp s " & _
                "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conap con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payapoid INNER JOIN QL_conap c ON c.cmpcode=con.cmpcode AND c.reftype=con.reftype AND c.refoid=con.refoid AND c.payrefoid=0 WHERE cb.cmpcode=@cmpcode AND ISNULL(payapres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnaptype NOT IN ('DNAP', 'CNAP', 'RETPI', 'GCAP') AND (CASE WHEN LEFT(cashbanktype, 2)='BB' THEN (RIGHT(CONVERT(VARCHAR(10), cashbankduedate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankduedate, 101), 2)) ELSE (RIGHT(CONVERT(VARCHAR(10), cashbankdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankdate, 101), 2)) END)=@periodacctg " & sFilterCOA & " GROUP BY con.suppoid, con.cmpcode) AS c ON c.suppoid=s.suppoid " & _
                "UNION ALL " & _
                "SELECT s.suppoid, s.suppname," & _
                "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd " & _
                "FROM QL_mstsupp s " & _
                "LEFT JOIN QL_conap c ON s.suppoid=c.suppoid " & sFilterCOA & " " & _
                "AND c.cmpcode=@cmpcode AND (RIGHT(CONVERT(VARCHAR(10), c.paymentdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), c.paymentdate, 101), 2))=@periodacctg AND trnapstatus='Post' AND c.trnaptype IN ('DNAP', 'RETPI') " & _
                ") AS tblpayment GROUP BY suppoid, suppname " & _
                "UNION ALL /*BELUM JATUH TEMPO*/" & _
                "SELECT ag.suppoid, ag.suppname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, " & _
                "0.0 paymentidr, 0.0 paymentusd, " & _
                "SUM(ag.amttransidr - ag.amtbayaridr) ageblmjtidr, SUM(ag.amttransusd - ag.amtbayarusd) ageblmjtusd, " & _
                "0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd " & _
                "FROM (" & _
                "SELECT s.suppoid, s.suppname, ISNULL(c.reftype, '') reftype, ISNULL(c.refoid, 0) refoid, " & _
                "ISNULL(c.amttransidr, 0.0) amttransidr, SUM(ISNULL(c1.amtbayaridr, 0.0)) amtbayaridr, " & _
                "ISNULL(c.amttransusd, 0.0) amttransusd, SUM(ISNULL(c1.amtbayarusd, 0.0)) amtbayarusd " & _
                "FROM QL_mstsupp s " & _
                "LEFT JOIN QL_conap c ON s.suppoid=c.suppoid AND c.cmpcode=@cmpcode " & sFilterCOA & " " & _
                "AND c.payduedate>@dateacuan AND c.payrefoid=0 " & _
                "AND (RIGHT(CONVERT(VARCHAR(10), c.trnapdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), c.trnapdate, 101), 2))<=@periodacctg " & _
                "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conap con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payapoid WHERE cb.cmpcode=@cmpcode AND ISNULL(payapres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnaptype NOT IN ('DNAP', 'CNAP', 'RETPI', 'GCAP') AND (CASE WHEN LEFT(cashbanktype, 2)='BB' THEN (RIGHT(CONVERT(VARCHAR(10), cashbankduedate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankduedate, 101), 2)) ELSE (RIGHT(CONVERT(VARCHAR(10), cashbankdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankdate, 101), 2)) END)<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid UNION ALL SELECT SUM(amtbayaridr) amtbayaridr, SUM(amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM QL_conap con WHERE con.cmpcode=@cmpcode AND trnapstatus='post' AND trnaptype IN ('DNAP', 'CNAP', 'RETPI', 'GCAP') AND (RIGHT(CONVERT(VARCHAR(10), con.paymentdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), con.paymentdate, 101), 2))<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid) c1 ON c.cmpcode=c1.cmpcode AND c.suppoid=c1.suppoid AND c.reftype=c1.reftype AND c.refoid=c1.refoid " & _
                "GROUP BY s.suppoid, s.suppname, c.amttransidr, c.amttransusd, c.reftype, c.refoid " & _
                ") AS ag GROUP BY ag.suppoid, ag.suppname " & _
                "UNION ALL /*0-30 DAYS*/" & _
                "SELECT ag.suppoid, ag.suppname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, " & _
                "0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, " & _
                "SUM(ag.amttransidr - ag.amtbayaridr) age1idr, SUM(ag.amttransusd - ag.amtbayarusd) age1usd, " & _
                "0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd " & _
                "FROM (" & _
                "SELECT s.suppoid, s.suppname, ISNULL(c.reftype, '') reftype, ISNULL(c.refoid, 0) refoid, " & _
                "ISNULL(c.amttransidr, 0.0) amttransidr, SUM(ISNULL(c1.amtbayaridr, 0.0)) amtbayaridr, " & _
                "ISNULL(c.amttransusd, 0.0) amttransusd, SUM(ISNULL(c1.amtbayarusd, 0.0)) amtbayarusd " & _
                "FROM QL_mstsupp s " & _
                "LEFT JOIN QL_conap c ON s.suppoid=c.suppoid AND c.cmpcode=@cmpcode " & sFilterCOA & " " & _
                "AND c.payduedate<=@dateacuan AND c.payrefoid=0 " & _
                "AND (RIGHT(CONVERT(VARCHAR(10), c.trnapdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), c.trnapdate, 101), 2))=@periodacctg " & _
                "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conap con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payapoid WHERE cb.cmpcode=@cmpcode AND ISNULL(payapres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnaptype NOT IN ('DNAP', 'CNAP', 'RETPI', 'GCAP') AND (CASE WHEN LEFT(cashbanktype, 2)='BB' THEN (RIGHT(CONVERT(VARCHAR(10), cashbankduedate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankduedate, 101), 2)) ELSE (RIGHT(CONVERT(VARCHAR(10), cashbankdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankdate, 101), 2)) END)<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid UNION ALL SELECT SUM(amtbayaridr) amtbayaridr, SUM(amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM QL_conap con WHERE con.cmpcode=@cmpcode AND trnapstatus='post' AND trnaptype IN ('DNAP', 'CNAP', 'RETPI', 'GCAP') AND (RIGHT(CONVERT(VARCHAR(10), con.paymentdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), con.paymentdate, 101), 2))<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid) c1 ON c.cmpcode=c1.cmpcode AND c.suppoid=c1.suppoid AND c.reftype=c1.reftype AND c.refoid=c1.refoid " & _
                "GROUP BY s.suppoid, s.suppname, c.amttransidr, c.amttransusd, c.reftype, c.refoid " & _
                ") AS ag GROUP BY ag.suppoid, ag.suppname " & _
                "UNION ALL /*31-60 DAYS*/" & _
                "SELECT ag.suppoid, ag.suppname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, " & _
                "0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, 0.0 age1idr, 0.0 age1usd, " & _
                "SUM(ag.amttransidr - ag.amtbayaridr) age2idr, SUM(ag.amttransusd - ag.amtbayarusd) age2usd, " & _
                "0.0 age3idr, 0.0 age3usd " & _
                "FROM (" & _
                "SELECT s.suppoid, s.suppname, ISNULL(c.reftype, '') reftype, ISNULL(c.refoid, 0) refoid, " & _
                "ISNULL(c.amttransidr, 0.0) amttransidr, SUM(ISNULL(c1.amtbayaridr, 0.0)) amtbayaridr, " & _
                "ISNULL(c.amttransusd, 0.0) amttransusd, SUM(ISNULL(c1.amtbayarusd, 0.0)) amtbayarusd " & _
                "FROM QL_mstsupp s " & _
                "LEFT JOIN QL_conap c ON s.suppoid=c.suppoid AND c.cmpcode=@cmpcode " & sFilterCOA & " " & _
                "AND c.payduedate<=@dateacuan AND c.payrefoid=0 " & _
                "AND (RIGHT(CONVERT(VARCHAR(10), c.trnapdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), c.trnapdate, 101), 2))='" & GetLastPeriod(GetDateToPeriodAcctg(New Date(FilterDDLYear.SelectedValue, FilterDDLMonth.SelectedValue, 1))) & "' " & _
                "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conap con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payapoid WHERE cb.cmpcode=@cmpcode AND ISNULL(payapres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnaptype NOT IN ('DNAP', 'CNAP', 'RETPI', 'GCAP') AND (CASE WHEN LEFT(cashbanktype, 2)='BB' THEN (RIGHT(CONVERT(VARCHAR(10), cashbankduedate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankduedate, 101), 2)) ELSE (RIGHT(CONVERT(VARCHAR(10), cashbankdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankdate, 101), 2)) END)<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid UNION ALL SELECT SUM(amtbayaridr) amtbayaridr, SUM(amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM QL_conap con WHERE con.cmpcode=@cmpcode AND trnapstatus='post' AND trnaptype IN ('DNAP', 'CNAP', 'RETPI', 'GCAP') AND (RIGHT(CONVERT(VARCHAR(10), con.paymentdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), con.paymentdate, 101), 2))<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid) c1 ON c.cmpcode=c1.cmpcode AND c.suppoid=c1.suppoid AND c.reftype=c1.reftype AND c.refoid=c1.refoid " & _
                "GROUP BY s.suppoid, s.suppname, c.amttransidr, c.amttransusd, c.reftype, c.refoid " & _
                ") AS ag GROUP BY ag.suppoid, ag.suppname " & _
                "UNION ALL /*>60 DAYS*/" & _
                "SELECT ag.suppoid, ag.suppname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, " & _
                "0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, 0.0 age1idr, 0.0 age1usd, " & _
                "0.0 age2idr, 0.0 age2usd, " & _
                "SUM(ag.amttransidr - ag.amtbayaridr) age3idr, SUM(ag.amttransusd - ag.amtbayarusd) age3usd " & _
                "FROM (" & _
                "SELECT s.suppoid, s.suppname, ISNULL(c.reftype, '') reftype, ISNULL(c.refoid, 0) refoid, " & _
                "ISNULL(c.amttransidr, 0.0) amttransidr, SUM(ISNULL(c1.amtbayaridr, 0.0)) amtbayaridr, " & _
                "ISNULL(c.amttransusd, 0.0) amttransusd, SUM(ISNULL(c1.amtbayarusd, 0.0)) amtbayarusd " & _
                "FROM QL_mstsupp s " & _
                "LEFT JOIN QL_conap c ON s.suppoid=c.suppoid AND c.cmpcode=@cmpcode " & sFilterCOA & " " & _
                "AND c.payduedate<=@dateacuan AND c.payrefoid=0 " & _
                "AND (RIGHT(CONVERT(VARCHAR(10), c.trnapdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), c.trnapdate, 101), 2))<'" & GetLastPeriod(GetDateToPeriodAcctg(New Date(FilterDDLYear.SelectedValue, FilterDDLMonth.SelectedValue, 1))) & "' " & _
                "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conap con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payapoid WHERE cb.cmpcode=@cmpcode AND ISNULL(payapres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnaptype NOT IN ('DNAP', 'CNAP', 'RETPI', 'GCAP') AND (CASE WHEN LEFT(cashbanktype, 2)='BB' THEN (RIGHT(CONVERT(VARCHAR(10), cashbankduedate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankduedate, 101), 2)) ELSE (RIGHT(CONVERT(VARCHAR(10), cashbankdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankdate, 101), 2)) END)<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid UNION ALL SELECT SUM(amtbayaridr) amtbayaridr, SUM(amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM QL_conap con WHERE con.cmpcode=@cmpcode AND trnapstatus='post' AND trnaptype IN ('DNAP', 'CNAP', 'RETPI', 'GCAP') AND (RIGHT(CONVERT(VARCHAR(10), con.updtime, 101), 4) + LEFT(CONVERT(VARCHAR(10), con.updtime, 101), 2))<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid) c1 ON c.cmpcode=c1.cmpcode AND c.suppoid=c1.suppoid AND c.reftype=c1.reftype AND c.refoid=c1.refoid " & _
                "GROUP BY s.suppoid, s.suppname, c.amttransidr, c.amttransusd, c.reftype, c.refoid " & _
                ") AS ag GROUP BY ag.suppoid, ag.suppname " & _
                ") AS tbldata " & _
                swhere & _
                "GROUP BY suppoid, suppname "
            If chkEmpty.Checked Then
                sSql &= "HAVING SUM(saidr)<>0 OR SUM(sausd)<>0 OR SUM(beliidr)<>0 OR SUM(beliusd)<>0 OR " & _
                "SUM(paymentidr)<>0 OR SUM(paymentusd)<>0 OR SUM(age1idr)<>0 OR SUM(age1usd)<>0 OR " & _
                "SUM(age2idr)<>0 OR SUM(age2usd)<>0 OR SUM(age3idr)<>0 OR SUM(age3usd)<>0 "
            End If
            sSql &= "ORDER BY suppname"

            Dim dt As DataTable
            Dim nFile As String = ""

            If FilterType.SelectedValue.ToUpper = "SUMMARY" Then
                If sType = "Print Excel" Then
                    nFile = Server.MapPath(folderReport & "rptAPXls.rpt")
                Else
                    nFile = Server.MapPath(folderReport & "rptAP.rpt")
                End If
                report.Load(nFile)
                dt = cKon.ambiltabel(sSql, "tbldata")
                report.SetDataSource(dt)
            Else
                If sType = "Print Excel" Then
                    If FilterCurrency.SelectedIndex <> "2" Then
                        nFile = Server.MapPath(folderReport & "crKartuAPDtlXls.rpt")
                    Else
                        nFile = Server.MapPath(folderReport & "crKartuAPDtlRateXls.rpt")
                    End If
                Else
                    If FilterCurrency.SelectedIndex <> "2" Then
                        nFile = Server.MapPath(folderReport & "crKartuAPDtl.rpt")
                    Else
                        nFile = Server.MapPath(folderReport & "crKartuAPDtlRate.rpt")
                    End If
                End If
                report.Load(nFile)
            End If

            Dim strLastModified As String = System.IO.File.GetLastWriteTime(nFile.ToString())
            cProc.SetDBLogonForReport(report)

            If FilterType.SelectedValue.ToUpper = "DETAIL" Then
                report.SetParameterValue("cmpcode", FilterDDLDiv.SelectedValue)
                report.SetParameterValue("start", CDate(txtStart.Text.Trim))
                report.SetParameterValue("finish", CDate(txtFinish.Text.Trim))
                report.SetParameterValue("swhere", swheresupp)
                If chkEmpty.Checked Then
                    report.SetParameterValue("swherezero", " WHERE ISNULL(sawal,0)>0 OR ISNULL(sawalidr,0)>0 OR ISNULL(sawalusd,0)>0 OR " & _
                        "ISNULL(amttrans,0)>0 OR ISNULL(amttransidr,0)>0 OR ISNULL(amttransusd,0)>0 " & _
                        "OR ISNULL(amtbayar,0)>0 OR ISNULL(amtbayaridr,0)>0 OR ISNULL(amtbayarusd,0)>0 ")
                Else
                    report.SetParameterValue("swherezero", "")
                End If
                report.SetParameterValue("currvalue", FilterCurrency.SelectedValue)
                report.SetParameterValue("companyname", FilterDDLDiv.SelectedItem.Text)
                report.SetParameterValue("periodreport", Format(CDate(txtStart.Text), "dd MMMM yyyy") & " - " & Format(CDate(txtFinish.Text), "dd MMMM yyyy"))
                report.SetParameterValue("PrintUserID", Session("UserID"))
                report.SetParameterValue("PrintLastModified", strLastModified)

                If FilterCurrency.SelectedIndex <> "2" Then
                    report.PrintOptions.PaperOrientation = PaperOrientation.Portrait
                Else
                    report.PrintOptions.PaperOrientation = PaperOrientation.Landscape
                End If
            Else
                report.SetParameterValue("PrintLastModified", strLastModified)
                report.SetParameterValue("PrintUserID", Session("UserID"))
                report.PrintOptions.PaperOrientation = PaperOrientation.Landscape
                report.PrintOptions.PaperSize = PaperSize.PaperFolio
            End If

            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "APReport_" & FilterDDLMonth.SelectedItem.Text.ToUpper & FilterDDLYear.SelectedItem.Text)
                Catch ex As Exception
                    report.Close() : report.Dispose()
                End Try
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "APReport_" & FilterDDLMonth.SelectedItem.Text.ToUpper & FilterDDLYear.SelectedItem.Text)
                Catch ex As Exception
                    report.Close() : report.Dispose()
                End Try
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.ToString, 1)
        End Try
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmAP.aspx")
        End If
        If checkPagePermission("~\ReportForm\frmAP.aspx", Session("Role")) = False Then
            ' Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Account Payable Report"
        If Not Page.IsPostBack Then
            ' DDL Division
            sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE' "
            If Session("CompnyCode") <> CompnyCode Then
                sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
            End If
            FillDDL(FilterDDLDiv, sSql)

            FilterDDLMonth.Items.Clear()
            For C1 As Int16 = 1 To 12
                FilterDDLMonth.Items.Add(MonthName(C1))
                FilterDDLMonth.Items(FilterDDLMonth.Items.Count - 1).Value = Format(C1, "00")
            Next
            FilterDDLMonth.SelectedValue = Format(Now.Month, "00")
            FilterDDLYear.Items.Clear()
            For C1 As Int16 = Now.Year - 2 To Now.Year + 2
                FilterDDLYear.Items.Add(C1)
                FilterDDLYear.Items(FilterDDLYear.Items.Count - 1).Value = C1
            Next
            FilterDDLYear.SelectedValue = Now.Year

            FilterCurrency.Items.Clear()
            FilterCurrency.Items.Add(New ListItem("INDONESIAN RUPIAH", "IDR"))
            FilterCurrency.Items.Add(New ListItem("US DOLLAR", "USD"))
            FilterCurrency.Items.Add(New ListItem("TRANSACTION RATE", ""))
            FilterCurrency.Items(2).Enabled = False

            FilterDDLCOA.Items.Clear()
            sSql = "SELECT DISTINCT a.acctgoid, acctgdesc FROM QL_conap con INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE payrefoid=0"
            FillDDLWithAdditionalText(FilterDDLCOA, sSql, "None", "0")

            pnlSupplier.CssClass = "popupControl"
            txtStart.Text = Format(GetServerTime, "MM/01/yyyy")
            txtFinish.Text = Format(GetServerTime, "MM/dd/yyyy")
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub FilterDDLDiv_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLDiv.SelectedIndexChanged
        rbSupplier.SelectedIndex = 0 : rbSupplier_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub FilterType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterType.SelectedIndexChanged
        If (FilterType.SelectedIndex = 0) Then
            FilterDDLMonth.Visible = True : FilterDDLYear.Visible = True
            txtStart.Visible = False : imbStart.Visible = False : lblTo.Visible = False
            txtFinish.Visible = False : imbFinish.Visible = False : lblMMDD.Visible = False
        Else
            FilterDDLMonth.Visible = False : FilterDDLYear.Visible = False
            txtStart.Visible = True : imbStart.Visible = True : lblTo.Visible = True
            txtFinish.Visible = True : imbFinish.Visible = True : lblMMDD.Visible = True
        End If
        FilterCurrency.Items(2).Enabled = (FilterType.SelectedIndex = 1)
    End Sub

    Protected Sub rbSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbSupplier.SelectedIndexChanged
        gvSupplier.DataSource = Nothing : gvSupplier.DataBind()
        Session("QL_mstsupp") = Nothing : gvSupplier.Visible = False
        If rbSupplier.SelectedValue = "ALL" Then
            FilterTextSupplier.Visible = False
            btnSearchSupplier.Visible = False
            btnClearSupplier.Visible = False
        Else
            FilterTextSupplier.Visible = True
            btnSearchSupplier.Visible = True
            btnClearSupplier.Visible = True
        End If
    End Sub

    Protected Sub btnSearchSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupplier.Click
        BindSupplierData()
    End Sub

    Protected Sub btnClearSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupplier.Click
        FilterTextSupplier.Text = ""
        suppoid.Text = ""
        gvSupplier.DataSource = Nothing
        gvSupplier.DataBind()
        gvSupplier.Visible = False
    End Sub

    Protected Sub gvSupplier_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupplier.PageIndexChanging
        UpdateCheckedGV()

        gvSupplier.PageIndex = e.NewPageIndex
        Dim dtSupp As DataTable = Session("QL_mstsupp")
        gvSupplier.DataSource = dtSupp
        gvSupplier.DataBind()
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        UpdateCheckedGV()
        Dim sMsg As String = ValidateParam()
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        Else
            ShowReport("View")
        End If
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        UpdateCheckedGV()
        Dim sMsg As String = ValidateParam()
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        Else
            ShowReport("Print PDF")
        End If
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        UpdateCheckedGV()
        Dim sMsg As String = ValidateParam()
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        Else
            ShowReport("Print Excel")
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmAP.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Dispose() : report.Close()
    End Sub
#End Region

End Class
