<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="frmStockCompare.aspx.vb" Inherits="ReportForm_frmStockCompare" title="Untitled Page" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" style="text-align: left" valign="center">
                <asp:Label ID="Label1" runat="server" CssClass="Title" Font-Bold="True" nowrap="nowrap"
                    Text=".: Stock Opname Comparation Report" Font-Size="16pt"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left" style="text-align: center; vertical-align: top;">
                <asp:UpdatePanel id="UpdatePanel2" runat="server">
                    <contenttemplate>
<asp:Panel id="Panel1" runat="server" __designer:wfdid="w447"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left colSpan=5></TD></TR><TR><TD class="Label" align=center colSpan=5><TABLE cellSpacing=0 cellPadding=2 border=0><TBODY><TR><TD id="TD13" align=left runat="server" Visible="false"><asp:Label id="Label21" runat="server" Text="Business Unit" __designer:wfdid="w448"></asp:Label></TD><TD id="TD12" align=left runat="server" Visible="false">:</TD><TD id="TD14" align=left runat="server" Visible="false"><asp:DropDownList id="busunit" runat="server" CssClass="inpText" __designer:wfdid="w449" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label3" runat="server" Text="Item Type " __designer:wfdid="w450"></asp:Label></TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="ddlType" runat="server" CssClass="inpText" __designer:wfdid="w451" AutoPostBack="True" OnSelectedIndexChanged="ddlType_SelectedIndexChanged"></asp:DropDownList></TD></TR><TR><TD id="TD8" align=left runat="server" visible="true"><asp:Label id="Label2" runat="server" Text="Periode " __designer:wfdid="w452"></asp:Label></TD><TD align=left runat="server" visible="true">:</TD><TD id="TD7" align=left runat="server" visible="true"><asp:TextBox id="tbPeriod1" runat="server" CssClass="inpText" __designer:wfdid="w453" Width="72px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="btnPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w454"></asp:ImageButton> <asp:Label id="Label5" runat="server" Text="to" __designer:wfdid="w455"></asp:Label> <asp:TextBox id="tbPeriod2" runat="server" CssClass="inpText" __designer:wfdid="w456" Width="72px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="btnPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w457"></asp:ImageButton> <asp:Label id="Label13" runat="server" ForeColor="Red" Text="(MM/dd/yyyy)" __designer:wfdid="w458"></asp:Label></TD></TR><TR><TD id="TD11" align=left runat="server" Visible="false"><asp:Label id="Label7" runat="server" Text="Item Group " __designer:wfdid="w459" Visible="False"></asp:Label></TD><TD id="TD10" align=left runat="server" Visible="false"></TD><TD id="TD9" align=left runat="server" Visible="false"><asp:DropDownList id="itemgroup" runat="server" CssClass="inpText" __designer:wfdid="w460" Width="155px" Visible="False"><asp:ListItem Value="ALL">All</asp:ListItem>
<asp:ListItem>MATERIAL</asp:ListItem>
<asp:ListItem>FG</asp:ListItem>
<asp:ListItem>WIP</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="TD4" align=left runat="server" Visible="false"><asp:Label id="Label8" runat="server" Text="Area " __designer:wfdid="w461"></asp:Label></TD><TD align=left runat="server" Visible="false"></TD><TD id="TD2" align=left runat="server" Visible="false"><asp:DropDownList id="areaoid" runat="server" CssClass="inpText" __designer:wfdid="w462" OnSelectedIndexChanged="areaoid_SelectedIndexChanged1" Width="155px"></asp:DropDownList></TD></TR><TR><TD id="TD5" align=left runat="server" Visible="true"><asp:CheckBox id="cbWH" runat="server" Text="Warehouse" __designer:wfdid="w463"></asp:CheckBox> <asp:Label id="Label6" runat="server" Text="Warehouse" __designer:wfdid="w464" Visible="False"></asp:Label> </TD><TD align=left runat="server" Visible="true">:</TD><TD id="TD3" align=left runat="server" Visible="true"><asp:DropDownList id="warehouseoid" runat="server" CssClass="inpText" __designer:wfdid="w465" OnSelectedIndexChanged="warehouseoid_SelectedIndexChanged1" Width="155px"></asp:DropDownList></TD></TR><TR><TD id="TD6" align=left runat="server" Visible="false"><asp:CheckBox id="cbLoc" runat="server" Text="Location" __designer:wfdid="w466"></asp:CheckBox></TD><TD align=left runat="server" Visible="false">:</TD><TD id="TD1" align=left runat="server" Visible="false"><asp:DropDownList id="locationoid" runat="server" CssClass="inpText" __designer:wfdid="w467" Width="155px"></asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label4" runat="server" Text="Item " __designer:wfdid="w468"></asp:Label></TD><TD align=left>:</TD><TD align=left><asp:TextBox id="itemcode" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w469" Width="150px" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchMat" onclick="btnSearchMat_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w470"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearMat" onclick="btnClearMat_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w471"></asp:ImageButton>&nbsp;<asp:Label id="itemoid" runat="server" __designer:wfdid="w472" Visible="False"></asp:Label></TD></TR><TR><TD align=right></TD><TD align=right></TD><TD align=left></TD></TR><TR><TD align=left colSpan=3><asp:ImageButton id="ViewReport" onclick="ViewReport_Click" runat="server" ImageUrl="~/Images/viewreport.png" __designer:wfdid="w473"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnPdf" onclick="BtnPdf_Click" runat="server" ImageUrl="~/Images/topdf.png" __designer:wfdid="w474"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnExcel" onclick="BtnExcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" __designer:wfdid="w475"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" __designer:wfdid="w476"></asp:ImageButton></TD></TR></TBODY></TABLE></TD></TR><TR><TD class="Label" align=left colSpan=5><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w477" TargetControlID="tbPeriod1" PopupButtonID="btnPeriod1" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w478" TargetControlID="tbPeriod2" PopupButtonID="btnPeriod2" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" __designer:wfdid="w479" TargetControlID="tbPeriod1" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" __designer:wfdid="w480" TargetControlID="tbPeriod2" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="VERTICAL-ALIGN: top; HEIGHT: 100px; TEXT-ALIGN: center" class="Label" vAlign=top align=center colSpan=5><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w481"><ProgressTemplate>
<asp:Image id="Image1" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w482"></asp:Image> Please wait .. 
</ProgressTemplate>
</asp:UpdateProgress> <asp:Button id="btnViewRPT" runat="server" Font-Bold="True" Text="View Report" CssClass="blue" __designer:wfdid="w483" Visible="False"></asp:Button> <asp:Button id="btnViewPDF" runat="server" Font-Bold="True" Text="Export PDF" CssClass="red" __designer:wfdid="w484" Visible="False"></asp:Button> <asp:Button id="btnViewExcel" runat="server" Font-Bold="True" Text="Export XLS" CssClass="green" __designer:wfdid="w485" Visible="False"></asp:Button> <asp:Button id="btnClearRPT" runat="server" Font-Bold="True" Text="Clear" CssClass="gray" __designer:wfdid="w486" Width="75px" Visible="False"></asp:Button></TD></TR><TR><TD class="Label" align=center colSpan=5><CR:CrystalReportViewer id="crvStock" runat="server" __designer:wfdid="w487" AutoDataBind="true" DisplayGroupTree="False" HasPrintButton="False" HasZoomFactorList="False" HasSearchButton="False" HasGotoPageButton="False" HasExportButton="False" HasDrillUpButton="False" HasViewList="False" HasToggleGroupTreeButton="False" HasCrystalLogo="False"></CR:CrystalReportViewer></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="BtnPdf"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="BtnExcel"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel></td>
        </tr>
    </table>
    <asp:UpdatePanel id="UpdatePanel3" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListMat" runat="server" CssClass="modalBox" Width="600px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListMat" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Item" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMat" runat="server" Width="100%" DefaultButton="btnFindListMat">Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="i.itemcode">Code</asp:ListItem>
<asp:ListItem Value="i.itemLongDescription">Description</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" CssClass="inpText" Width="200px"></asp:TextBox> <asp:ImageButton id="btnFindListMat" onclick="btnFindListMat_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" onclick="btnAllListMat_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat" runat="server" ForeColor="#333333" Width="99%" OnSelectedIndexChanged="gvListMat_SelectedIndexChanged" PageSize="5" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="itemoid,itemcode" CellPadding="4" GridLines="None">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Item Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlocaldesc" HeaderText="Item Desc.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="Transparent" CssClass="gvpopup" Font-Bold="True" ForeColor="Black"></PagerStyle>
<EmptyDataTemplate>
                                        <asp:Label ID="Label114" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbCloseListMat" onclick="lbCloseListMat_Click" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" PopupControlID="pnlListMat" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListMat" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

