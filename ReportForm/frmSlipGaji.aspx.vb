Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_frmSlipGaji
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Function"
    Private Function UpdateCheckedPerson() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("DataPerson") Is Nothing Then
            Dim dtTbl As DataTable = Session("DataPerson")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvLP.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvLP.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(3).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "personoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl = dtView.ToTable
                Session("DataPerson") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function IndoMonth(ByVal sValue As String) As String
        Select Case sValue
            Case "1"
                Return "Januari"
            Case "2"
                Return "Februari"
            Case "3"
                Return "Maret"
            Case "4"
                Return "April"
            Case "5"
                Return "Mei"
            Case "6"
                Return "Juni"
            Case "7"
                Return "Juli"
            Case "8"
                Return "Agustus"
            Case "9"
                Return "September"
            Case "10"
                Return "Oktober"
            Case "11"
                Return "November"
            Case "12"
                Return "Desember"
            Case Else
                Return "Unknown"
        End Select
    End Function
#End Region

#Region "Procedure"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitDDL()
        ' DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        FillDDL(FilterDDLDiv, sSql)
    End Sub

    Private Sub BindPersonData()
        sSql = "select ROW_NUMBER() OVER(ORDER BY personname ASC) AS Nomer, personoid, nip, personname, deptname, leveldesc from tb_v_slip_gaji where r_salary_month=" & ddlmonth.SelectedValue & " AND r_salary_year=" & ddlyear.SelectedValue & " Order By ROW_NUMBER() OVER(ORDER BY personname ASC)"
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "tb_v_slip_gaji")
        If dtTbl.Rows.Count > 0 Then
            Dim HdrText() As String = {"No", "NIK", "Nama Pegawai", "Departement", "Level"}
            Dim DtField() As String = {"Nomer", "nip", "personname", "deptname", "leveldesc"}
            Dim arKey() As String = {"personoid", "personname"}
            gvLP.Columns.Clear()
            For C1 As Int16 = 0 To HdrText.Length - 1
                Dim sfield As New BoundField
                sfield.HeaderStyle.CssClass = "gvhdr"
                sfield.HeaderText = HdrText(C1)
                If DtField(C1) = "nip" Or DtField(C1) = "personname" Then
                    sfield.HeaderStyle.HorizontalAlign = HorizontalAlign.Left
                    sfield.ItemStyle.HorizontalAlign = HorizontalAlign.Left
                ElseIf DtField(C1) = "Nomer" Then
                    sfield.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                    sfield.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                Else
                    sfield.HeaderStyle.HorizontalAlign = HorizontalAlign.Right
                    sfield.ItemStyle.HorizontalAlign = HorizontalAlign.Right
                End If
                sfield.DataField = DtField(C1)
                sfield.HeaderStyle.Font.Size = FontUnit.XSmall
                sfield.ItemStyle.Font.Size = FontUnit.XSmall
                gvLP.Columns.Add(sfield)
            Next
            gvLP.DataKeyNames = arKey
            Dim cField As New CommandField
            cField.ShowSelectButton = True
            cField.SelectText = "Select"
            cField.HeaderStyle.CssClass = "gvhdr"
            cField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            gvLP.Columns.Add(cField)
            gvLP.DataSource = dtTbl
            Session("DataPerson") = dtTbl
            Session("DataPersonView") = dtTbl
            gvLP.DataBind()
        End If
    End Sub
#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmSlipGaji.aspx")
        End If
        If checkPagePermission("~\ReportForm\frmSlipGaji.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Laporan Perhitungan Gaji"
        If Not Page.IsPostBack Then
            InitDDL()
            ddlmonth.Items.Clear()
            For iMonth As Integer = 1 To 12
                Dim li As New ListItem(MonthName(iMonth).ToUpper, iMonth)
                ddlmonth.Items.Add(li)
            Next
            ddlmonth.SelectedValue = Now.Month
            ddlyear.Items.Clear()
            For iYear As Integer = Now.Year + 2 To Now.Year - 2 Step -1
                ddlyear.Items.Add(iYear)
            Next
            ddlyear.SelectedValue = Now.Year
        End If
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        Dim sWhere As String = "" : Dim rptName As String = ""
        Dim rptfile As String = Replace(sType, "PDF", "") : Dim sErr As String = ""
        If Not IsValidDate(tbDate.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Tanggal Print is invalid. " & sErr, 2)
            Exit Sub
        End If
        Try
            report.Load(Server.MapPath(folderReport & "rptSlipSalary" & rptfile & ".rpt"))
            rptName = "Print_Out_Slip_Gaji"
            sSql = "SELECT * FROM tb_v_slip_gaji Where divcode='" & FilterDDLDiv.SelectedValue & "'"
            sSql &= " AND r_salary_month = " & ddlmonth.SelectedValue & " AND r_salary_year=" & ddlyear.SelectedValue
            sSql &= " AND salary_type = '" & ddlperiodtype.SelectedValue & "'"
            If personoid.Text <> "0" Then
                sSql &= " AND personoid=" & personoid.Text
            End If
            sSql &= " ORDER BY personname"

            Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "tb_v_slip_gaji")
            Dim dvTbl As DataView = dtTbl.DefaultView
            report.SetDataSource(dvTbl.ToTable)
            report.SetParameterValue("sBulan", IndoMonth(ddlmonth.SelectedValue))
            report.SetParameterValue("sTahun", ddlyear.SelectedValue)
            report.SetParameterValue("sPenggaji", tbUser.Text)
            report.SetParameterValue("sTanggalGaji", DatePart(DateInterval.Day, CDate(tbDate.Text)) & " " & IndoMonth(DatePart(DateInterval.Month, CDate(tbDate.Text))) & " " & DatePart(DateInterval.Year, CDate(tbDate.Text)))

            If sType = "Print Excel" Then
                report.PrintOptions.PaperSize = PaperSize.PaperA4
            End If
            cProc.SetDBLogonForReport(report)

            If sType = "XLS" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, rptName)
                report.Close()
                report.Dispose()
            ElseIf sType = "PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, rptName)
                report.Close()
                report.Dispose()
            Else
                'CrvReport.DisplayGroupTree = False
                CrvReport.ReportSource = report
                CrvReport.SeparatePages = True
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Close() : report.Dispose()
    End Sub

    Protected Sub imbOKPopUp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUp.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnSearchLP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchLP.Click
        Session("DataPerson") = Nothing
        Session("DataPersonView") = Nothing
        FilterDDLLP.SelectedIndex = -1
        FilterTextLP.Text = ""
        BindPersonData()
        cProc.SetModalPopUpExtender(btnHideLP, pnlLP, mpeLP, True)
    End Sub

    Protected Sub btnEraseLP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseLP.Click
        TxtPerson.Text = "" : personoid.Text = "0"
    End Sub

    Protected Sub btnFindLP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindLP.Click
        If UpdateCheckedPerson() Then
            Dim dtTbl As DataTable = Session("DataPerson")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim sFilter As String = FilterDDLLP.SelectedValue & " LIKE '%" & Tchar(FilterTextLP.Text) & "%'"
            dtView.RowFilter = sFilter
            Session("DataPersonView") = dtView.ToTable
            gvLP.DataSource = Session("DataPersonView")
            gvLP.DataBind()
            dtView.RowFilter = ""
        End If
        mpeLP.Show()
    End Sub

    Protected Sub btnAllLP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllLP.Click
        If UpdateCheckedPerson() Then
            FilterDDLLP.SelectedIndex = -1
            FilterTextLP.Text = ""
            Session("DataPersonView") = Session("DataPerson")
            gvLP.DataSource = Session("DataPersonView")
            gvLP.DataBind()
        End If
        mpeLP.Show()
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("DataPersonView") Is Nothing Then
            Dim dtTbl As DataTable = Session("DataPersonView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("DataPerson")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "personoid=" & dtTbl.Rows(C1)("personoid")
                    objView(0)("CheckValue") = "True"
                    dtTbl.Rows(C1)("CheckValue") = "True"
                    objView.RowFilter = ""
                Next
                Session("DataPerson") = objView.ToTable
                Session("DataPersonView") = dtTbl
                gvLP.DataSource = Session("DataPersonView")
                gvLP.DataBind()
            End If
        End If
        mpeLP.Show()
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("DataPersonView") Is Nothing Then
            Dim dtTbl As DataTable = Session("DataPersonView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("DataPerson")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "personoid=" & dtTbl.Rows(C1)("personoid")
                    objView(0)("CheckValue") = "False"
                    dtTbl.Rows(C1)("CheckValue") = "False"
                    objView.RowFilter = ""
                Next
                Session("DataPerson") = objView.ToTable
                Session("DataPersonView") = dtTbl
                gvLP.DataSource = Session("DataPersonView")
                gvLP.DataBind()
            End If
        End If
        mpeLP.Show()
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If UpdateCheckedPerson() Then
            Dim dtTbl As DataTable = Session("DataPerson")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "CheckValue='True'"
            Session("DataPersonView") = dtView.ToTable
            gvLP.DataSource = Session("DataPersonView")
            gvLP.DataBind()
            dtView.RowFilter = ""
        End If
        mpeLP.Show()
    End Sub

    Protected Sub btnAddToListLP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToListLP.Click
        If UpdateCheckedPerson() Then
            Dim dtTbl As DataTable = Session("DataPerson")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "CheckValue='True'"
            'lbPersonOid.Items.Clear()
            'For C1 As Integer = 0 To dtView.Count - 1
            '    Dim li As New ListItem(dtView(C1)("nip").ToString, dtView(C1)("personoid"))
            '    lbPersonOid.Items.Add(li)
            'Next
        End If
        cProc.SetModalPopUpExtender(btnHideLP, pnlLP, mpeLP, False)
    End Sub

    Protected Sub btnCloseLP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCloseLP.Click
        cProc.SetModalPopUpExtender(btnHideLP, pnlLP, mpeLP, False)
    End Sub

    Protected Sub imbView_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbView.Click
        ShowReport("")
    End Sub

    Protected Sub imbPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbPDF.Click
        ShowReport("PDF")
    End Sub

    Protected Sub imbXLS_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbXLS.Click
        ShowReport("XLS")
    End Sub

    Protected Sub gvLP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvLP.SelectedIndexChanged
        personoid.Text = gvLP.SelectedDataKey.Item("personoid")
        TxtPerson.Text = gvLP.SelectedDataKey.Item("personname")
        cProc.SetModalPopUpExtender(btnHideLP, pnlLP, mpeLP, False)
    End Sub

    Protected Sub CrvReport_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles CrvReport.Navigate
        ShowReport("")
    End Sub
#End Region

End Class
