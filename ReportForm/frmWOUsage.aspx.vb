Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_WOUsage
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function 'OK

    Private Function UpdateCheckedSO() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSO") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSO")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListSO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "soitemmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblSO") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedSO2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSO")
            Dim dtTbl2 As DataTable = Session("TblSOView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListSO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "soitemmstoid=" & cbOid
                                dtView2.RowFilter = "soitemmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblSO") = dtTbl
                Session("TblSOView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedKIK() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblKIK") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblKIK")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListResult.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListResult.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "womstoid =" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblKIK") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedKIK2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblKIKView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblKIK")
            Dim dtTbl2 As DataTable = Session("TblKIKView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListResult.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListResult.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "womstoid =" & cbOid
                                dtView2.RowFilter = "womstoid =" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblKIK") = dtTbl
                Session("TblKIKView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matrefoid =" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matrefoid =" & cbOid
                                dtView2.RowFilter = "matrefoid =" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub 'OK

    Private Sub BindListSO()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, som.somstoid AS soitemmstoid, som.sono AS soitemno, som.sodate AS soitemdate, CONVERT(VARCHAR(10), som.sodate, 101) AS sodate, som.somststatus AS soitemmststatus, som.somstnote AS soitemmstnote FROM QL_trnsomst som "
        If FilterDDLDiv.SelectedValue <> "ALL" Then
            sSql &= " WHERE som.somstoid IN (SELECT wod1.soitemmstoid from QL_trnwodtl1 wod1 INNER JOIN QL_trnwomst wom ON wom.womstoid=wod1.womstoid WHERE wom.cmpcode='" & FilterDDLDiv.SelectedValue & "' "
        Else
            sSql &= " WHERE som.somstoid IN (SELECT wod1.soitemmstoid from QL_trnwodtl1 wod1 INNER JOIN QL_trnwomst wom ON wom.womstoid=wod1.womstoid WHERE wom.cmpcode LIKE '%' "
        End If

        If DDLDivision.SelectedValue <> "ALL" Then
            sSql &= " AND som.groupoid=" & DDLDivision.SelectedValue
        End If

        If FilterDDLDiv.SelectedValue <> "ALL" Then
            sSql &= "AND wom.cmpcode='" & FilterDDLDiv.SelectedValue & "'"
        Else
            sSql &= "AND wom.cmpcode LIKE '%%'"
        End If

        If FilterDDLStatus.SelectedValue.ToUpper <> "ALL" Then
            sSql &= " AND wom.womststatus LIKE '%" & FilterDDLStatus.SelectedValue & "%'"
        End If

        If IsValidPeriod() Then
            sSql &= " AND wom.wodate>='" & FilterPeriod1.Text & " 00:00:00' AND wom.wodate<='" & FilterPeriod2.Text & " 23:59:59'"
        Else
            Exit Sub
        End If

        sSql &= ") ORDER BY soitemdate DESC, soitemmstoid DESC"
        Session("TblSO") = cKon.ambiltabel(sSql, "QL_soitemmst")
    End Sub

    Private Sub BindListResult()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, pr.womstoid AS womstoid, pr.wono AS wono, CONVERT(VARCHAR(10), pr.wodate, 101) AS wodate, pr.womstnote AS womstnote FROM QL_trnwomst pr INNER JOIN QL_trnwodtl1 wod1 ON pr.womstoid=wod1.womstoid INNER JOIN QL_trnsomst som ON som.somstoid=wod1.soitemmstoid "

        sSql &= " WHERE " & DDLFilterListResult.SelectedValue & " LIKE '%" & Tchar(txtFilterListResult.Text) & "%'"

        'If FilterDDLDiv.SelectedValue.ToUpper = "ALL" Then
        'sSql &= ""
        'Else
        sSql &= " AND pr.cmpcode='" & FilterDDLDiv.SelectedValue & "'"
        'End If

        If sono.Text <> "" Then
            Dim scode() As String = Split(sono.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " som.soitemno = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If DDLDivision.SelectedValue <> "ALL" Then
            sSql &= " AND som.groupoid=" & DDLDivision.SelectedValue
        End If

        If IsValidPeriod() Then
            sSql &= " AND pr.wodate>='" & FilterPeriod1.Text & " 00:00:00' AND pr.wodate<='" & FilterPeriod2.Text & " 23:59:59'"
        Else
            Exit Sub
        End If

        If FilterDDLStatus.SelectedValue <> "All" Then
            sSql &= " AND pr.womststatus='" & FilterDDLStatus.SelectedValue & "'"
        End If

        sSql &= " ORDER BY wodate DESC, womstoid DESC"
        'FillGV(gvListResult, sSql, "QL_trnwomst")
        Session("TblKIK") = cKon.ambiltabel(sSql, "QL_trnwomst")
    End Sub 'OK

    Private Sub BindListMat()
        If ddlType.SelectedValue = "Raw" Then
            sSql = "SELECT DISTINCT 'False' AS checkvalue, ISNULL(pod.wodtl3refoid,0) AS matrefoid, m.matrawlongdesc AS matreflongdesc, m.matrawcode AS matrefcode, g2.gendesc unit FROM QL_trnwodtl3 pod INNER JOIN QL_trnwomst pom ON pom.cmpcode=pod.cmpcode AND pom.womstoid=pod.womstoid INNER JOIN QL_mstmatraw m ON m.matrawoid=pod.wodtl3refoid INNER JOIN QL_mstgen g2 ON g2.genoid=pod.wodtl3unitoid "
            sSql &= " Union All "
            sSql &= "SELECT DISTINCT 'False' AS checkvalue, ISNULL(pod.wodtl4refoid,0) AS matrefoid, (cat1.cat1longdesc + '.' + cat2.cat2longdesc) AS matreflongdesc, (cat1.cat1code + '.' + cat2.cat2code) AS matrefcode, g2.gendesc unit FROM QL_trnwodtl4 pod INNER JOIN QL_trnwomst pom ON pom.cmpcode=pod.cmpcode AND pom.womstoid=pod.womstoid INNER JOIN QL_mstcat1 cat1 ON cat1.cat1oid = pod.wodtl4refoid INNER JOIN QL_mstcat2 cat2 ON cat2.cat1oid = cat2.cat1oid INNER JOIN QL_mstgen g2 ON g2.genoid=pod.wodtl4unitoid "
            sSql &= " Union All "
            sSql &= " SELECT DISTINCT 'False' AS checkvalue, ISNULL(bad.acararefoid,0) AS matrefoid, m.matrawlongdesc AS matreflongdesc, m.matrawcode AS matrefcode, g2.gendesc unit FROM QL_trnbrtacaramst bam  INNER JOIN QL_trnbrtacaradtl bad ON bad.cmpcode=bam.cmpcode AND bad.acaramstoid=bam.acaramstoid AND bad.acarareftype='Raw' AND bad.acararefoid NOT IN (SELECT wodtl3refoid FROM QL_trnwodtl3) INNER JOIN QL_trnwomst pom ON pom.cmpcode=bam.cmpcode AND pom.womstoid=bam.womstoid INNER JOIN QL_mstmatraw m ON m.matrawoid=bad.acararefoid INNER JOIN QL_mstgen g2 ON g2.genoid=bad.acaraunitoid "
        ElseIf ddlType.SelectedValue = "General" Then
            sSql = "SELECT DISTINCT 'False' AS checkvalue, ISNULL(pod.wodtl3refoid,0) AS matrefoid, m.matgenlongdesc AS matreflongdesc, m.matgencode AS matrefcode, g2.gendesc unit FROM QL_trnwodtl3 pod INNER JOIN QL_trnwomst pom ON pom.cmpcode=pod.cmpcode AND pom.womstoid=pod.womstoid INNER JOIN QL_mstmatgen m ON m.matgenoid=pod.wodtl3refoid INNER JOIN QL_mstgen g2 ON g2.genoid=pod.wodtl3unitoid "
            sSql &= " Union All "
            sSql &= " SELECT DISTINCT 'False' AS checkvalue, ISNULL(bad.acararefoid,0) AS matrefoid, m.matgenlongdesc AS matreflongdesc, m.matgencode AS matrefcode, g2.gendesc unit FROM QL_trnbrtacaramst bam  INNER JOIN QL_trnbrtacaradtl bad ON bad.cmpcode=bam.cmpcode AND bad.acaramstoid=bam.acaramstoid AND bad.acarareftype='General' AND bad.acararefoid NOT IN (SELECT wodtl3refoid FROM QL_trnwodtl3) INNER JOIN QL_trnwomst pom ON pom.cmpcode=bam.cmpcode AND pom.womstoid=bam.womstoid INNER JOIN QL_mstmatgen m ON m.matgenoid=bad.acararefoid INNER JOIN QL_mstgen g2 ON g2.genoid=bad.acaraunitoid "
        ElseIf ddlType.SelectedValue = "Spare Part" Then
            sSql = "SELECT DISTINCT 'False' AS checkvalue, ISNULL(pod.wodtl3refoid,0) AS matrefoid, m.sparepartlongdesc AS matreflongdesc, m.sparepartcode AS matrefcode, g2.gendesc unit FROM QL_trnwodtl3 pod INNER JOIN QL_trnwomst pom ON pom.cmpcode=pod.cmpcode AND pom.womstoid=pod.womstoid INNER JOIN QL_mstsparepart m ON m.sparepartoid=pod.wodtl3refoid INNER JOIN QL_mstgen g2 ON g2.genoid=pod.wodtl3unitoid "
            sSql &= " Union All "
            sSql &= " SELECT DISTINCT 'False' AS checkvalue, ISNULL(bad.acararefoid,0) AS matrefoid, m.sparepartlongdesc AS matreflongdesc, m.sparepartcode AS matrefcode, g2.gendesc unit FROM QL_trnbrtacaramst bam  INNER JOIN QL_trnbrtacaradtl bad ON bad.cmpcode=bam.cmpcode AND bad.acaramstoid=bam.acaramstoid AND bad.acarareftype='Spare Part' AND bad.acararefoid NOT IN (SELECT wodtl3refoid FROM QL_trnwodtl3) INNER JOIN QL_trnwomst pom ON pom.cmpcode=bam.cmpcode AND pom.womstoid=bam.womstoid INNER JOIN QL_mstsparepart m ON m.sparepartoid=bad.acararefoid INNER JOIN QL_mstgen g2 ON g2.genoid=bad.acaraunitoid "
        End If

        sSql &= "WHERE pom.cmpcode='" & FilterDDLDiv.SelectedValue & "'"

        If FilterDDLStatus.SelectedValue <> "All" Then
            sSql &= " AND pom.womststatus='" & FilterDDLStatus.SelectedValue & "'"
        End If

        If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then

            If IsValidPeriod() Then
                sSql &= " AND pom.wodate>='" & FilterPeriod1.Text & " 00:00:00' AND pom.wodate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If

        End If
        'If RDMRReg.Checked = False Then
        '    sSql &= " AND (SELECT COUNT(*) FROM QL_trnporawdtl pod2 WHERE pom.porawmstoid=pod2.porawmstoid AND pod.porawdtloid=pod2.porawdtloid AND(CASE WHEN ISNULL(CONVERT(decimal(18,2), pod.porawdtlres1),0)=0 then ISNULL(pod.porawqty,0) ELSE CONVERT(decimal(18,2), pod.porawdtlres1) END) " & FilterDDLPOQty.SelectedValue & " ISNULL((SELECT SUM(regd.registerqty) FROM QL_trnregisterdtl regd INNER JOIN QL_trnregistermst regm ON regd.registermstoid=regm.registermstoid WHERE regd.porefmstoid=pom.porawmstoid AND regd.porefdtloid=pod2.porawdtloid AND regm.registertype='Raw'),0)) > 0 "
        'Else
        'Qty Awal
        'sSql &= " AND ISNULL(pod.porawqty,0) " & FilterDDLPOQty.SelectedValue & " " & ToDouble(POQtyTxt.Text)
        'Minus Closing
        'sSql &= " AND (CASE WHEN ISNULL(CONVERT(decimal(18,2), pod.porawdtlres1),0)=0 then ISNULL(pod.porawqty,0) ELSE CONVERT(decimal(18,2), pod.porawdtlres1) END) " & FilterDDLPOQty.SelectedValue & " " & ToDouble(POQtyTxt.Text)
        'End If

        If checkPagePermission("~\ReportForm\frmWOUsage.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND pom.createuser='" & Session("UserID") & "'"
        End If

        Session("TblMat") = cKon.ambiltabel(sSql, "QL_mstref")
    End Sub

    Private Sub InitFilterDDLCat1()
        'Fill DDL Category1
        If ddlType.SelectedValue = "Raw" Then
            sSql = "SELECT cat1code, cat1code+' - '+cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat1res1='Raw' Order By cat1code"
        ElseIf ddlType.SelectedValue = "General" Then
            sSql = "SELECT cat1code, cat1code+' - '+cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat1res1='General' Order By cat1code"
        ElseIf ddlType.SelectedValue = "Spare Part" Then
            sSql = "SELECT cat1code, cat1code+' - '+cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat1res1='Spare Part' Order By cat1code"
        End If

        FillDDL(FilterDDLCat01, sSql)
        If FilterDDLCat01.Items.Count > 0 Then
            InitFilterDDLCat2()
        Else
            FilterDDLCat02.Items.Clear()
            FilterDDLCat03.Items.Clear()
            FilterDDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitFilterDDLCat2()
        'Fill DDL Category2
        If ddlType.SelectedValue = "Raw" Then
            sSql = "SELECT cat2code, cat2code+' - '+cat2shortdesc FROM QL_mstcat2 cat2 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat2.cmpcode AND cat1.cat1oid=cat2.cat1oid AND cat1.cat1code='" & FilterDDLCat01.SelectedValue & "' WHERE cat2.cmpcode='" & CompnyCode & "' AND cat2.activeflag='ACTIVE' AND cat2res1='Raw' Order By cat2code"
        ElseIf ddlType.SelectedValue = "General" Then
            sSql = "SELECT cat2code, cat2code+' - '+cat2shortdesc FROM QL_mstcat2 cat2 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat2.cmpcode AND cat1.cat1oid=cat2.cat1oid AND cat1.cat1code='" & FilterDDLCat01.SelectedValue & "' WHERE cat2.cmpcode='" & CompnyCode & "' AND cat2.activeflag='ACTIVE' AND cat2res1='General' Order By cat2code"
        ElseIf ddlType.SelectedValue = "Spare Part" Then
            sSql = "SELECT cat2code, cat2code+' - '+cat2shortdesc FROM QL_mstcat2 cat2 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat2.cmpcode AND cat1.cat1oid=cat2.cat1oid AND cat1.cat1code='" & FilterDDLCat01.SelectedValue & "' WHERE cat2.cmpcode='" & CompnyCode & "' AND cat2.activeflag='ACTIVE' AND cat2res1='Spare Part' Order By cat2code"
        End If

        FillDDL(FilterDDLCat02, sSql)
        If FilterDDLCat02.Items.Count > 0 Then
            InitFilterDDLCat3()
        Else
            FilterDDLCat03.Items.Clear()
            FilterDDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitFilterDDLCat3()
        'Fill DDL Category3
        If ddlType.SelectedValue = "Raw" Then
            sSql = "SELECT cat3code, cat3code+' - '+cat3shortdesc FROM QL_mstcat3 cat3 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat3.cmpcode AND cat1.cat1oid=cat3.cat1oid AND cat1.cat1code='" & FilterDDLCat01.SelectedValue & "' INNER JOIN QL_mstcat2 cat2 ON cat2.cmpcode=cat3.cmpcode AND cat2.cat2oid=cat3.cat2oid AND cat2.cat2code='" & FilterDDLCat02.SelectedValue & "' WHERE cat3.cmpcode='" & CompnyCode & "' AND cat3.activeflag='ACTIVE' AND cat3res1='Raw' Order By cat3code"
        ElseIf ddlType.SelectedValue = "General" Then
            sSql = "SELECT cat3code, cat3code+' - '+cat3shortdesc FROM QL_mstcat3 cat3 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat3.cmpcode AND cat1.cat1oid=cat3.cat1oid AND cat1.cat1code='" & FilterDDLCat01.SelectedValue & "' INNER JOIN QL_mstcat2 cat2 ON cat2.cmpcode=cat3.cmpcode AND cat2.cat2oid=cat3.cat2oid AND cat2.cat2code='" & FilterDDLCat02.SelectedValue & "' WHERE cat3.cmpcode='" & CompnyCode & "' AND cat3.activeflag='ACTIVE' AND cat3res1='General' Order By cat3code"
        ElseIf ddlType.SelectedValue = "Spare Part" Then
            sSql = "SELECT cat3code, cat3code+' - '+cat3shortdesc FROM QL_mstcat3 cat3 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat3.cmpcode AND cat1.cat1oid=cat3.cat1oid AND cat1.cat1code='" & FilterDDLCat01.SelectedValue & "' INNER JOIN QL_mstcat2 cat2 ON cat2.cmpcode=cat3.cmpcode AND cat2.cat2oid=cat3.cat2oid AND cat2.cat2code='" & FilterDDLCat02.SelectedValue & "' WHERE cat3.cmpcode='" & CompnyCode & "' AND cat3.activeflag='ACTIVE' AND cat3res1='Spare Part' Order By cat3code"
        End If
        FillDDL(FilterDDLCat03, sSql)
        If FilterDDLCat03.Items.Count > 0 Then
            InitFilterDDLCat4()
        Else
            FilterDDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitFilterDDLCat4()
        'Fill DDL Category4
        If ddlType.SelectedValue = "Raw" Then
            sSql = "SELECT cat4code, cat4code+' - '+cat4shortdesc FROM QL_mstcat4 cat4 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat4.cmpcode AND cat1.cat1oid=cat4.cat1oid AND cat1.cat1code='" & FilterDDLCat01.SelectedValue & "' INNER JOIN QL_mstcat2 cat2 ON cat2.cmpcode=cat4.cmpcode AND cat2.cat2oid=cat4.cat2oid AND cat2.cat2code='" & FilterDDLCat02.SelectedValue & "' INNER JOIN QL_mstcat3 cat3 ON cat3.cmpcode=cat4.cmpcode AND cat3.cat3oid=cat4.cat3oid AND cat3.cat3code='" & FilterDDLCat03.SelectedValue & "' WHERE cat4.cmpcode='" & CompnyCode & "' AND cat4.activeflag='ACTIVE' AND cat4res1='Raw' Order By cat4code"
        ElseIf ddlType.SelectedValue = "General" Then
            sSql = "SELECT cat4code, cat4code+' - '+cat4shortdesc FROM QL_mstcat4 cat4 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat4.cmpcode AND cat1.cat1oid=cat4.cat1oid AND cat1.cat1code='" & FilterDDLCat01.SelectedValue & "' INNER JOIN QL_mstcat2 cat2 ON cat2.cmpcode=cat4.cmpcode AND cat2.cat2oid=cat4.cat2oid AND cat2.cat2code='" & FilterDDLCat02.SelectedValue & "' INNER JOIN QL_mstcat3 cat3 ON cat3.cmpcode=cat4.cmpcode AND cat3.cat3oid=cat4.cat3oid AND cat3.cat3code='" & FilterDDLCat03.SelectedValue & "' WHERE cat4.cmpcode='" & CompnyCode & "' AND cat4.activeflag='ACTIVE' AND cat4res1='General' Order By cat4code"
        ElseIf ddlType.SelectedValue = "Spare Part" Then
            sSql = "SELECT cat4code, cat4code+' - '+cat4shortdesc FROM QL_mstcat4 cat4 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat4.cmpcode AND cat1.cat1oid=cat4.cat1oid AND cat1.cat1code='" & FilterDDLCat01.SelectedValue & "' INNER JOIN QL_mstcat2 cat2 ON cat2.cmpcode=cat4.cmpcode AND cat2.cat2oid=cat4.cat2oid AND cat2.cat2code='" & FilterDDLCat02.SelectedValue & "' INNER JOIN QL_mstcat3 cat3 ON cat3.cmpcode=cat4.cmpcode AND cat3.cat3oid=cat4.cat3oid AND cat3.cat3code='" & FilterDDLCat03.SelectedValue & "' WHERE cat4.cmpcode='" & CompnyCode & "' AND cat4.activeflag='ACTIVE' AND cat4res1='Spare Part' Order By cat4code"
        End If

        FillDDL(FilterDDLCat04, sSql)
    End Sub

    Private Function FillDDLWithALL(ByRef oDDLObject As DropDownList, ByVal sSql As String) As Boolean
        ' To fill data in dropdown list, return true when all data exist
        FillDDLWithALL = True
        oDDLObject.Items.Clear()
        oDDLObject.Items.Add("ALL")
        oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = "ALL"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            oDDLObject.Items.Add(xreader.GetValue(1))
            oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()
        ' Cek if all data exits
        If oDDLObject.Items.Count = 0 Then
            FillDDLWithALL = False
        End If
        Return FillDDLWithALL
    End Function

    Private Sub InitAllDDL()
        ' DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(FilterDDLDiv, sSql)
        If FilterDDLDiv.Items.Count > 0 Then
            InitDDLDept(FilterDDLDiv.SelectedValue)
            InitDDLDivision(FilterDDLDiv.SelectedValue)
        End If
    End Sub

    Private Sub InitDDLDivision(ByVal sDiv As String)
        ' Init DDL Division
        sSql = "SELECT groupoid, groupcode+' - '+groupdesc FROM QL_mstdeptgroup WHERE activeflag='ACTIVE'"
        If sDiv <> "ALL" Then
            sSql &= " AND cmpcode='" & sDiv & "'"
            FillDDLWithALL(DDLDivision, sSql)
        End If
    End Sub

    Private Sub InitDDLDept(ByVal sDiv As String)
        ' Init DDL Department Filter By Division
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE activeflag='ACTIVE'"
        If sDiv <> "ALL" Then
            sSql &= " AND cmpcode='" & sDiv & "'"
            FillDDLWithALL(deptoid, sSql)
        Else
            sSql = "SELECT dept.deptoid, dept.deptname+' - '+div.divname FROM QL_mstdept dept INNER JOIN QL_mstdivision div ON  div.cmpcode=dept.cmpcode WHERE div.activeflag='ACTIVE' AND dept.activeflag='ACTIVE'"
            FillDDLWithALL(deptoid, sSql)
        End If
    End Sub 'OK

    Private Sub ShowReport(ByVal sType As String)
        If FilterDDLDiv.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        Try
            Dim sWhere As String = ""
            Dim sWhere2 As String = ""

            Dim rptName As String = ""
            If ddlTipe.SelectedValue = "Summary" Then
                If sType = "Print Excel" Then
                    report.Load(Server.MapPath(folderReport & "rptJobCostMOUsgSumXls.rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptJobCostMOUsgSum.rpt"))
                End If
                rptName = "JobCostingMOUsageSumStatus_"
            Else
                If sType = "Print Excel" Then
                    report.Load(Server.MapPath(folderReport & "rptJobCostMOUsgDtlXls.rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptJobCostMOUsgDtl.rpt"))
                End If

                rptName = "JobCostingMOUsageDtlStatus_"
            End If

            sWhere &= " WHERE tbl.[CMP Code]='" & FilterDDLDiv.SelectedValue & "'"
            If DDLDivision.SelectedValue <> "ALL" Then
                sWhere &= " AND tbl.[Group Oid]=" & DDLDivision.SelectedValue
            End If

            If sono.Text <> "" Then
                Dim sSOno As String = ""
                Dim scode() As String = Split(sono.Text, ";")
                sSOno &= " AND ("
                For c1 As Integer = 0 To scode.Length - 1
                    sSOno &= " tbl.[SO No.] = '" & Tchar(scode(c1)) & "'"
                    If c1 < scode.Length - 1 Then
                        sSOno &= " OR "
                    End If
                Next
                sSOno &= ")"
                sWhere &= sSOno & " "
            End If

            If IsValidPeriod() Then
                sWhere &= " AND tbl.[KIK Date]>='" & FilterPeriod1.Text & " 00:00:00' AND tbl.[KIK Date]<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If

            If wono.Text <> "" Then
                Dim sResNo1() As String = Split(wono.Text, ";")
                sWhere &= " AND ("
                For c1 As Integer = 0 To sResNo1.Length - 1
                    sWhere &= "  tbl.[KIK_No] = '" & Tchar(sResNo1(c1)) & "'"
                    If c1 < sResNo1.Length - 1 Then
                        sWhere &= " OR"
                    End If
                Next
                sWhere &= ")"
            End If

            If FilterDDLStatus.SelectedValue.ToUpper <> "ALL" Then
                sWhere &= " AND tbl.KIK_Status LIKE '%" & FilterDDLStatus.SelectedValue & "%'"
            End If

            If deptoid.SelectedValue.ToUpper <> "ALL" Then
                sWhere &= " AND [Deptoid]=" & deptoid.SelectedValue
            End If

            If ddlType.SelectedValue.ToUpper <> "ALL" Then
                sWhere2 &= " AND bad.acarareftype='" & ddlType.SelectedValue.ToUpper & "' "
                sWhere &= " AND [Tipe_Mat]='" & ddlType.SelectedValue.ToUpper & "' "
                If matrefcode.Text <> "" Then
                    Dim sResNo2() As String = Split(matrefcode.Text, ";")
                    sWhere &= " AND ("
                    For c1 As Integer = 0 To sResNo2.Length - 1
                        sWhere &= " [Mat_Code] = '" & Tchar(sResNo2(c1)) & "'"
                        If c1 < sResNo2.Length - 1 Then
                            sWhere &= " OR"
                        End If
                    Next
                    sWhere &= ")"
                End If
            End If

            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("sWhere2", sWhere2)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            report.PrintOptions.PaperSize = PaperSize.PaperFolio
            cProc.SetDBLogonForReport(report)
            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, rptName & Format(GetServerTime(), "yyyyMMddHHmmss"))
                report.Close()
                report.Dispose()
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, rptName & Format(GetServerTime(), "yyyyMMddHHmmss"))
                report.Close()
                report.Dispose()
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub 'OK
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        'Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmWOUsage.aspx")
        End If
        If checkPagePermission("~\ReportForm\frmWOUsage.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Job Costing MO Usage Status"
        If Not Page.IsPostBack Then
            InitAllDDL()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        End If
    End Sub 'OK

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If

        If Not Session("EmptyListSO") Is Nothing And Session("EmptyListSO") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListSO") Then
                Session("EmptyListSO") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
            End If
        End If
        If Not Session("WarningListSO") Is Nothing And Session("WarningListSO") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListSO") Then
                Session("WarningListSO") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
            End If
        End If

        If Not Session("EmptyListKIK") Is Nothing And Session("EmptyListKIK") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListKIK") Then
                Session("EmptyListKIK") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListResult, PanelListResult, mpeListResult, True)
            End If
        End If
        If Not Session("WarningListKIK") Is Nothing And Session("WarningListKIK") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListKIK") Then
                Session("WarningListKIK") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListResult, PanelListResult, mpeListResult, True)
            End If
        End If
    End Sub 'OK

    Protected Sub FilterDDLDiv_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLDiv.SelectedIndexChanged
        InitDDLDept(FilterDDLDiv.SelectedValue)
        InitDDLDivision(FilterDDLDiv.SelectedValue)
    End Sub 'OK

    Protected Sub imbFindSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindSO.Click
        If IsValidPeriod() Then
            DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
            Session("TblSO") = Nothing : Session("TblSOView") = Nothing : gvListSO.DataSource = Nothing : gvListSO.DataBind()
            cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbEraseSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseSO.Click
        sono.Text = ""
        FilterDDLDiv.Enabled = True
    End Sub

    Protected Sub btnFindListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSO.Click
        If Session("TblSO") Is Nothing Then
            BindListSO()
            If Session("TblSO").Rows.Count <= 0 Then
                Session("EmptyListSO") = "SO data can't be found!"
                showMessage(Session("EmptyListSO"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListSO.SelectedValue & " LIKE '%" & Tchar(txtFilterListSO.Text) & "%'"
        If UpdateCheckedSO() Then
            Dim dv As DataView = Session("TblSO").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblSOView") = dv.ToTable
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                dv.RowFilter = ""
                mpeListSO.Show()
            Else
                dv.RowFilter = ""
                Session("TblSOView") = Nothing
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                Session("WarningListSO") = "SO data can't be found!"
                showMessage(Session("WarningListSO"), 2)
            End If
        Else
            mpeListSO.Show()
        End If
    End Sub

    Protected Sub btnViewAllListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListSO.Click
        DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
        If Session("TblSO") Is Nothing Then
            BindListSO()
            If Session("TblSO").Rows.Count <= 0 Then
                Session("EmptyListSO") = "SO data can't be found!"
                showMessage(Session("EmptyListSO"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedSO() Then
            Dim dt As DataTable = Session("TblSO")
            Session("TblSOView") = dt
            gvListSO.DataSource = Session("TblSOView")
            gvListSO.DataBind()
        End If
        mpeListSO.Show()
    End Sub

    Protected Sub btnSelectAllSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllSO.Click
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "soitemmstoid=" & dtTbl.Rows(C1)("soitemmstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSO") = objTbl
                Session("TblSOView") = dtTbl
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
            End If
            mpeListSO.Show()
        Else
            Session("WarningListSO") = "Please show some SO data first!"
            showMessage(Session("WarningListSO"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneSO.Click
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "soitemmstoid=" & dtTbl.Rows(C1)("soitemmstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSO") = objTbl
                Session("TblSOView") = dtTbl
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
            End If
            mpeListSO.Show()
        Else
            Session("WarningListSO") = "Please show some SO data first!"
            showMessage(Session("WarningListSO"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedSO.Click
        If Session("TblSO") Is Nothing Then
            Session("WarningListSO") = "Selected SO data can't be found!"
            showMessage(Session("WarningListSO"), 2)
            Exit Sub
        End If
        If UpdateCheckedSO() Then
            Dim dtTbl As DataTable = Session("TblSO")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
                Session("TblSOView") = dtView.ToTable
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                dtView.RowFilter = ""
                mpeListSO.Show()
            Else
                dtView.RowFilter = ""
                Session("TblSOView") = Nothing
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                Session("WarningListSO") = "Selected SO data can't be found!"
                showMessage(Session("WarningListSO"), 2)
            End If
        Else
            mpeListSO.Show()
        End If
    End Sub

    Protected Sub gvListSO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSO.PageIndexChanging
        If UpdateCheckedSO2() Then
            gvListSO.PageIndex = e.NewPageIndex
            gvListSO.DataSource = Session("TblSOView")
            gvListSO.DataBind()
        End If
        mpeListSO.Show()
    End Sub 'OK

    Protected Sub lkbAddToListListSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListSO.Click
        If Not Session("TblSO") Is Nothing Then
            If UpdateCheckedSO() Then
                Dim dtTbl As DataTable = Session("TblSO")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If sono.Text <> "" Then
                            If dtView(C1)("soitemno") <> "" Then
                                sono.Text &= ";" + vbCrLf + dtView(C1)("soitemno")
                            End If
                        Else
                            If dtView(C1)("soitemno") <> "" Then
                                sono.Text &= dtView(C1)("soitemno")
                            End If
                        End If
                    Next
                    dtView.RowFilter = ""
                    FilterDDLDiv.Enabled = False
                    cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, False)
                Else
                    Session("WarningListSO") = "Please select SO to add to list!"
                    showMessage(Session("WarningListSO"), 2)
                    Exit Sub
                End If
            Else
                Session("WarningListSO") = "Please show some SO data first!"
                showMessage(Session("WarningListSO"), 2)
                Exit Sub
            End If
        Else
            Session("WarningListSO") = "Please show some SO data first!"
            showMessage(Session("WarningListSO"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListSO.Click
        cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, False)
    End Sub

    Protected Sub imbFindResult_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindResult.Click
        If IsValidPeriod() Then
            DDLFilterListResult.SelectedIndex = -1 : txtFilterListResult.Text = ""
            Session("TblKIK") = Nothing : Session("TblKIKView") = Nothing : gvListResult.DataSource = Nothing : gvListResult.DataBind()
            cProc.SetModalPopUpExtender(btnHiddenListResult, PanelListResult, mpeListResult, True)
        Else
            Exit Sub
        End If
    End Sub 'OK

    Protected Sub imbEraseResult_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseResult.Click
        wono.Text = ""
    End Sub 'OK

    Protected Sub btnFindListResult_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListResult.Click
        If Session("TblKIK") Is Nothing Then
            BindListResult()
            If Session("TblKIK").Rows.Count <= 0 Then
                Session("EmptyListKIK") = "KIK data can't be found!"
                showMessage(Session("EmptyListKIK"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListResult.SelectedValue & " LIKE '%" & Tchar(txtFilterListResult.Text) & "%'"
        If UpdateCheckedKIK() Then
            Dim dv As DataView = Session("TblKIK").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblKIKView") = dv.ToTable
                gvListResult.DataSource = Session("TblKIKView")
                gvListResult.DataBind()
                dv.RowFilter = ""
                mpeListResult.Show()
            Else
                dv.RowFilter = ""
                Session("TblKIKView") = Nothing
                gvListResult.DataSource = Session("TblKIKView")
                gvListResult.DataBind()
                Session("WarningListKIK") = "KIK data can't be found!"
                showMessage(Session("WarningListKIK"), 2)
            End If
        Else
            mpeListResult.Show()
        End If
    End Sub 'OK

    Protected Sub btnViewAllListResult_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListResult.Click
        DDLFilterListResult.SelectedIndex = -1 : txtFilterListResult.Text = ""
        If Session("TblKIK") Is Nothing Then
            BindListResult()
            If Session("TblKIK").Rows.Count <= 0 Then
                Session("EmptyListKIK") = "KIK data can't be found!"
                showMessage(Session("EmptyListKIK"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedKIK() Then
            Dim dt As DataTable = Session("TblKIK")
            Session("TblKIKView") = dt
            gvListResult.DataSource = Session("TblKIKView")
            gvListResult.DataBind()
        End If
        mpeListResult.Show()
    End Sub

    Protected Sub gvListResult_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListResult.PageIndexChanging
        If UpdateCheckedKIK2() Then
            gvListResult.PageIndex = e.NewPageIndex
            gvListResult.DataSource = Session("TblKIKView")
            gvListResult.DataBind()
        End If
        mpeListResult.Show()
    End Sub 'OK

    Protected Sub lkbListResult_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListResult.Click
        cProc.SetModalPopUpExtender(btnHiddenListResult, PanelListResult, mpeListResult, False)
    End Sub 'OK

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Session("TblKIK") Is Nothing Then
            If UpdateCheckedKIK() Then
                Dim dtTbl As DataTable = Session("TblKIK")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If wono.Text <> "" Then
                            wono.Text &= ";" + vbCrLf + dtView(C1)("wono")
                        Else
                            wono.Text = dtView(C1)("wono")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHiddenListResult, PanelListResult, mpeListResult, False)
                Else
                    Session("WarningListKIK") = "Please select KIK to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListKIK") = "Please show some KIK data first!"
            showMessage(Session("WarningListKIK"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        If ddlType.SelectedValue.ToUpper <> "ALL" Then
            LabelMat6.Visible = True
            separate.Visible = True
            matrefcode.Visible = True
            btnSearchMat.Visible = True
            btnEraseMap.Visible = True
            lblTitleListMat.Text = "List of " & ddlType.SelectedValue & " Material"
            LabelMat6.Text = ddlType.SelectedValue & " Material"
        Else
            LabelMat6.Visible = False
            separate.Visible = False
            matrefcode.Visible = False
            btnSearchMat.Visible = False
            btnEraseMap.Visible = False
        End If
    End Sub

    Protected Sub lkbAddToListListMat2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If matrefcode.Text <> "" Then
                            matrefcode.Text &= ";" + vbCrLf + dtView(C1)("matrefcode")
                        Else
                            matrefcode.Text = dtView(C1)("matrefcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some Material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListMaterial_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedKIK2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        If cbCat01.Checked Then
            If FilterDDLCat01.SelectedValue <> "" Then
                'If ddlType.SelectedValue = "Raw" Then
                sPlus &= " AND SUBSTRING(matrefcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'"
                'ElseIf ddlType.SelectedValue = "General" Then
                'sPlus &= " AND SUBSTRING(matgencode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'"
                'ElseIf ddlType.SelectedValue = "Spare Part" Then
                'sPlus &= " AND SUBSTRING(sparepartcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'"
                'End If
            Else
                cbCat01.Checked = False
            End If
        End If
        If cbCat02.Checked Then
            If FilterDDLCat02.SelectedValue <> "" Then
                'If ddlType.SelectedValue = "Raw" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND SUBSTRING(matrefcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'") & " AND SUBSTRING(matrefcode, 4, 3)='" & FilterDDLCat02.SelectedValue & "'"
                'ElseIf ddlType.SelectedValue = "General" Then
                'sPlus &= IIf(cbCat01.Checked, "", " AND SUBSTRING(matgencode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'") & " AND SUBSTRING(matgencode, 4, 3)='" & FilterDDLCat02.SelectedValue & "'"
                'ElseIf ddlType.SelectedValue = "Spare Part" Then
                'sPlus &= IIf(cbCat01.Checked, "", " AND SUBSTRING(sparepartcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'") & " AND SUBSTRING(sparepartcode, 4, 3)='" & FilterDDLCat02.SelectedValue & "'"
                'End If

            Else
                cbCat02.Checked = False
            End If
        End If
        If cbCat03.Checked Then
            If FilterDDLCat03.SelectedValue <> "" Then
                'If ddlType.SelectedValue = "Raw" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND SUBSTRING(matrefcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'") & IIf(cbCat02.Checked, "", " AND SUBSTRING(matrefcode, 4, 3)='" & FilterDDLCat02.SelectedValue & "'") & " AND SUBSTRING(matrefcode, 8, 4)='" & FilterDDLCat03.SelectedValue & "'"
                'ElseIf ddlType.SelectedValue = "General" Then
                'sPlus &= IIf(cbCat01.Checked, "", " AND SUBSTRING(matgencode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'") & IIf(cbCat02.Checked, "", " AND SUBSTRING(matgencode, 4, 3)='" & FilterDDLCat02.SelectedValue & "'") & " AND SUBSTRING(matgencode, 8, 4)='" & FilterDDLCat03.SelectedValue & "'"
                'ElseIf ddlType.SelectedValue = "Spare Part" Then
                'sPlus &= IIf(cbCat01.Checked, "", " AND SUBSTRING(sparepartcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'") & IIf(cbCat02.Checked, "", " AND SUBSTRING(sparepartcode, 4, 3)='" & FilterDDLCat02.SelectedValue & "'") & " AND SUBSTRING(sparepartcode, 8, 4)='" & FilterDDLCat03.SelectedValue & "'"
                'End If
            Else
                cbCat03.Checked = False
            End If
        End If
        If cbCat04.Checked Then
            If FilterDDLCat04.SelectedValue <> "" Then
                'If ddlType.SelectedValue = "Raw" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND SUBSTRING(matrefcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'") & IIf(cbCat02.Checked, "", " AND SUBSTRING(matrefcode, 4, 3)='" & FilterDDLCat02.SelectedValue & "'") & IIf(cbCat03.Checked, "", " AND SUBSTRING(matrefcode, 8, 4)='" & FilterDDLCat03.SelectedValue & "'") & " AND SUBSTRING(matrefcode, 13, 4)='" & FilterDDLCat04.SelectedValue & "'"
                'ElseIf ddlType.SelectedValue = "General" Then
                'sPlus &= IIf(cbCat01.Checked, "", " AND SUBSTRING(matgencode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'") & IIf(cbCat02.Checked, "", " AND SUBSTRING(matgencode, 4, 3)='" & FilterDDLCat02.SelectedValue & "'") & IIf(cbCat03.Checked, "", " AND SUBSTRING(matgencode, 8, 4)='" & FilterDDLCat03.SelectedValue & "'") & " AND SUBSTRING(matgencode, 13, 4)='" & FilterDDLCat04.SelectedValue & "'"
                'ElseIf ddlType.SelectedValue = "Spare Part" Then
                'sPlus &= IIf(cbCat01.Checked, "", " AND SUBSTRING(sparepartcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'") & IIf(cbCat02.Checked, "", " AND SUBSTRING(sparepartcode, 4, 3)='" & FilterDDLCat02.SelectedValue & "'") & IIf(cbCat03.Checked, "", " AND SUBSTRING(sparepartcode, 8, 4)='" & FilterDDLCat03.SelectedValue & "'") & " AND SUBSTRING(sparepartcode, 13, 4)='" & FilterDDLCat04.SelectedValue & "'"
                'End If
            Else
                cbCat04.Checked = False
            End If
        End If

        sSql = sSql & sPlus
        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        InitFilterDDLCat1()
        cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedKIK2() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat01_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat01.SelectedIndexChanged
        InitFilterDDLCat2()
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat02_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat02.SelectedIndexChanged
        InitFilterDDLCat3()
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat03_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat03.SelectedIndexChanged
        InitFilterDDLCat4()
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat04_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat04.SelectedIndexChanged
        mpeListMat.Show()
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsValidPeriod() Then
            InitFilterDDLCat1()
            FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
            Session("TblMat") = Nothing : Session("TblMatView") = Nothing : gvListMat.DataSource = Nothing : gvListMat.DataBind()
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub btnEraseMap_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        matrefcode.Text = ""
    End Sub

    Protected Sub cbKIK_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbKIK.CheckedChanged
        If cbKIK.Checked = True Then
            imbFindResult.Visible = True
            imbEraseResult.Visible = True
        Else
            imbFindResult.Visible = False
            imbEraseResult.Visible = False
        End If
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matrefoid=" & dtTbl.Rows(C1)("matrefoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matrefoid=" & dtTbl.Rows(C1)("matrefoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedKIK2() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                InitFilterDDLCat1()
                cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Selected material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub lbShowData_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Please find material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If tbData.Text = "" Then
            Session("WarningListMat") = "Please fill total data to be shown!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        Else
            If ToDouble(tbData.Text) <= 0 Then
                Session("WarningListMat") = "Total data to be shown must be more than 0!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedKIK2() Then
            gvListMat.PageSize = Int(tbData.Text)
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub


    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub 'OK

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub 'OK

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub 'OK

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmWOUsage.aspx?awal=true")
    End Sub 'OK

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub 'OK

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub 'OK

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub 'OK

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        If Not report Is Nothing Then
            If report.IsLoaded Then
                report.Dispose()
                report.Close()
            End If
        End If
    End Sub 'OK
#End Region

End Class
