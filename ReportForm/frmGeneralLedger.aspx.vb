Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_frmGeneralLedger
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function ValidateParam() As String
        Dim sReturn As String = ""
        Dim sErrTemp As String = ""

        If Not IsValidDate(date1.Text, "MM/dd/yyyy", sErrTemp) Then sReturn &= "- Invalid Start Date format: " & sErrTemp & ".<BR>"
        If Not IsValidDate(date2.Text, "MM/dd/yyyy", sErrTemp) Then sReturn &= "- Invalid End Date format: " & sErrTemp & ".<BR>"
        If IsValidDate(date1.Text, "MM/dd/yyyy", sErrTemp) And IsValidDate(date2.Text, "MM/dd/yyyy", sErrTemp) Then
            If DateDiff(DateInterval.Day, CDate(date1.Text), CDate(date2.Text)) < 0 Then
                sReturn &= "- End Date must be greater than Start Date.<BR>"
            End If
        End If

        If rblAccount.SelectedValue = "Beberapa" Then
            If Session("account") Is Nothing Then
                sReturn &= "- No Account selected.<BR>"
            Else
                Dim dtab As DataTable = Session("account")
                If dtab.Select("selected='1'", "").Length < 1 Then
                    sReturn &= "- No Account selected.<BR>"
                End If
            End If
        End If

        Return sReturn
    End Function
#End Region

#Region "Procedure"
    Private Sub UpdateCheckedGV()
        If Not Session("account") Is Nothing Then
            Dim dtab As DataTable = Session("account")

            If dtab.Rows.Count > 0 Then
                Dim cb As System.Web.UI.WebControls.CheckBox
                Dim lbl As System.Web.UI.WebControls.Label
                Dim acctgoid As Integer
                Dim dView As DataView = dtab.DefaultView
                Dim drView As DataRowView

                For i As Integer = 0 To gvAccounting.Rows.Count - 1
                    cb = gvAccounting.Rows(i).FindControl("cbAccount")
                    If cb.Checked = True Then
                        lbl = gvAccounting.Rows(i).FindControl("lblacctgoid")
                        acctgoid = Integer.Parse(lbl.Text)
                        dView.RowFilter = "id = " & acctgoid
                        drView = dView.Item(0)
                        drView.BeginEdit()
                        drView("selected") = 1
                        drView.EndEdit()
                        dView.RowFilter = ""
                    Else
                        lbl = gvAccounting.Rows(i).FindControl("lblacctgoid")
                        acctgoid = Integer.Parse(lbl.Text)
                        dView.RowFilter = "id = " & acctgoid
                        drView = dView.Item(0)
                        drView.BeginEdit()
                        drView("selected") = 0
                        drView.EndEdit()
                        dView.RowFilter = ""
                    End If
                Next
                dtab.AcceptChanges()
            End If

            Session("account") = dtab
        End If
    End Sub

    Private Sub BindAccounting(ByVal filter As String)
        Try
            Dim dtab As New DataTable
            sSql = "SELECT 0 as selected, acctgoid as id,acctgcode as code,acctgdesc as name FROM QL_mstacctg WHERE acctgoid not in " & _
                "(select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null ) " & _
                "AND (acctgcode LIKE '%" & Tchar(filter.Trim) & "%' OR acctgdesc LIKE '%" & Tchar(filter.Trim) & "%') " & _
                "ORDER BY acctgcode "
            dtab = cKon.ambiltabel(sSql, "QL_mstacctg")

            gvAccounting.DataSource = dtab
            gvAccounting.DataBind()
            Session("account") = dtab
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            conn.Close() : Exit Sub
        End Try
    End Sub

    Private Function ProcessReport(Optional ByVal isXls As Boolean = False) As String ' Return Error/Warning message if any
        Dim sMsg As String = ""
        crv.ReportSource = Nothing
        Session("showReport") = "false"
        Try
            report = New ReportDocument

            Dim sWhere As String = ""
            Dim sWhereAwal As String = ""
            Dim sWhereTrans As String = ""
            Dim period As String = ""

            period = Format(CDate(date1.Text), "dd MMMM yyyy").ToString & " - " & Format(CDate(date2.Text), "dd MMMM yyyy").ToString
            report = New ReportDocument

            Dim sAcctgOid As String = ""
            If rblAccount.SelectedValue = "Beberapa" Then
                Dim dtAcctg As DataTable : dtAcctg = Session("account")
                Dim dvAcctg As DataView = dtAcctg.DefaultView
                dvAcctg.RowFilter = "selected=1"

                For R1 As Integer = 0 To dvAcctg.Count - 1
                    sAcctgOid &= dvAcctg(R1)("id").ToString & ","
                Next
            End If
            Dim sXls As String = ""
            If isXls Then
                sXls = "Xls"
            End If

            If DDLType.SelectedValue = "0" Or DDLType.SelectedValue = "1" Then
                If rblAccount.SelectedValue = "Beberapa" Then
                    sWhereAwal = " WHERE acctgoid IN (" & Left(sAcctgOid, sAcctgOid.Length - 1) & ")"
                    If FilterDDLGroup.SelectedValue <> "All" Then
                        sWhereAwal &= " AND groupoid=" & FilterDDLGroup.SelectedValue & ""
                    End If
                    sWhereTrans = " AND a.acctgoid IN (" & Left(sAcctgOid, sAcctgOid.Length - 1) & ")"
                    If FilterDDLGroup.SelectedValue <> "All" Then
                        sWhereTrans &= " AND d.groupoid=" & FilterDDLGroup.SelectedValue & ""
                    End If
                Else
                    If FilterDDLGroup.SelectedValue <> "All" Then
                        sWhereAwal = " WHERE groupoid=" & FilterDDLGroup.SelectedValue & ""
                    End If
                    If FilterDDLGroup.SelectedValue <> "All" Then
                        sWhereTrans = " AND d.groupoid=" & FilterDDLGroup.SelectedValue & ""
                    End If
                End If
                If DDLType.SelectedValue = "0" Then
                    report.Load(Server.MapPath(folderReport & "crGLSummary" & sXls & ".rpt"))
                ElseIf DDLType.SelectedValue = "1" Then
                    report.Load(Server.MapPath(folderReport & "crGLDetail" & sXls & ".rpt"))
                End If

            ElseIf DDLType.SelectedValue = "2" Then
                sWhere = " WHERE d.cmpcode = '" & FilterDDLDiv.SelectedValue & "' AND m.gldate BETWEEN '" & CDate(date1.Text) & "' AND '" & CDate(date2.Text) & "' AND m.glflag='POST' AND m.glmstoid>0 AND d.noref LIKE '%" & Tchar(tbNoRef.Text) & "%'"
                If FilterDDLGroup.SelectedValue <> "All" Then
                    sWhere &= " AND d.groupoid=" & FilterDDLGroup.SelectedValue & ""
                End If
                If rblAccount.SelectedValue = "Beberapa" Then
                    sWhere &= "  AND m.glmstoid IN (SELECT a.glmstoid FROM QL_trnglmst a INNER JOIN QL_trngldtl b ON a.glmstoid = b.glmstoid AND a.cmpcode = b.cmpcode WHERE a.cmpcode = d.cmpcode AND a.gldate BETWEEN '" & CDate(date1.Text) & "' AND '" & CDate(date2.Text) & "' AND b.acctgoid IN (" & Left(sAcctgOid, sAcctgOid.Length - 1) & "))"
                End If
                If cbShowMemo.Checked Then
                    sWhere &= " AND m.glnote LIKE 'Memo|%'"
                End If

                report.Load(Server.MapPath(folderReport & "crGenJournal" & sXls & ".rpt"))
            End If

            cProc.SetDBLogonForReport(report)

            If DDLType.SelectedValue = "0" Or DDLType.SelectedValue = "1" Then
                report.SetParameterValue("awal", CDate(date1.Text.Trim))
                report.SetParameterValue("akhir", CDate(date2.Text.Trim))
                report.SetParameterValue("periodacctg", GetDateToPeriodAcctg(CDate(date1.Text.Trim)))

                report.SetParameterValue("cmpcode", FilterDDLDiv.SelectedValue)
                report.SetParameterValue("swhereawal", sWhereAwal)
                report.SetParameterValue("swheretrans", sWhereTrans)
                report.SetParameterValue("currency", FilterCurrency.SelectedValue)
            ElseIf DDLType.SelectedValue = "2" Then
                report.SetParameterValue("period", period)
                report.SetParameterValue("sWhere", sWhere)
                report.SetParameterValue("sCurrency", FilterCurrency.SelectedValue)
                report.SetParameterValue("sCompany", FilterDDLDiv.SelectedItem.Text)
            End If

            'report.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            'report.PrintOptions.PaperSize = PaperSize.PaperA4
        Catch ex As Exception
            sMsg = ex.ToString
        End Try
        Return sMsg
    End Function

    Private Sub InitDDL()
        ' DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE' "
        If FillDDL(FilterDDLDiv, sSql) Then
            InitDDLGroup()
        End If
    End Sub

    Private Sub InitDDLGroup()
        ' DDL Division
        sSql = "SELECT groupoid, groupcode + ' - ' + groupdesc FROM QL_mstdeptgroup WHERE cmpcode='" & FilterDDLDiv.SelectedValue & "' AND activeflag='ACTIVE'"
        FillDDLWithAdditionalText(FilterDDLGroup, sSql, "All", "All")
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then Response.Redirect("~/OTHER/login.aspx")
        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            ' Response.Redirect("~/other/NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("frmGeneralLedger.aspx")
        End If

        Page.Title = CompnyName & " - General Ledger Report"

        If Not Page.IsPostBack Then
            InitDDL()
            FilterCurrency.Items.Clear()
            FilterCurrency.Items.Add(New ListItem("INDONESIAN RUPIAH", "IDR"))
            FilterCurrency.Items.Add(New ListItem("US DOLLAR", "USD"))
            FilterCurrency.Items.Add(New ListItem("TRANSACTION RATE", ""))
            FilterCurrency.Items(2).Enabled = False
            date1.Text = "" & GetServerTime.Month & "/01/" & GetServerTime.Year & ""
            date2.Text = Format(GetServerTime, "MM/dd/yyyy")
        End If
    End Sub

    Protected Sub imbOKPopUp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUp.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Close() : report.Dispose()
    End Sub

    Protected Sub rblAccount_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblAccount.SelectedIndexChanged
        Session("account") = Nothing
        crv.ReportSource = Nothing : Session("showReport") = "false"
        If rblAccount.SelectedValue = "All" Then
            gvAccounting.Visible = False
            gvAccounting.DataSource = Nothing
            gvAccounting.DataBind()
            acctgdesc.Visible = False
            btnSearch.Visible = False
            btnDelete.Visible = False
            acctgdesc.Text = ""
            acctgoid.Text = ""
        Else
            acctgdesc.Visible = True
            btnSearch.Visible = True
            btnDelete.Visible = True
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sDate As String = ""
        BindAccounting(acctgdesc.Text.Trim)
        gvAccounting.Visible = True
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        acctgdesc.Text = ""
        acctgoid.Text = ""
        gvAccounting.Visible = False
        gvAccounting.DataSource = Nothing
        gvAccounting.DataBind()
        Session("account") = Nothing
    End Sub

    Protected Sub gvAccounting_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAccounting.PageIndexChanging
        UpdateCheckedGV()
        Dim dtab As DataTable = Session("account")
        gvAccounting.PageIndex = e.NewPageIndex
        gvAccounting.DataSource = dtab
        gvAccounting.DataBind()
        Session("account") = dtab
    End Sub

    Protected Sub DDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLType.SelectedIndexChanged
        Session("account") = Nothing
        acctgdesc.Text = ""
        acctgoid.Text = ""
        acctgdesc.Visible = False
        btnSearch.Visible = False
        btnDelete.Visible = False
        gvAccounting.Visible = False
        rblAccount.SelectedValue = "All"
        crv.ReportSource = Nothing : Session("showReport") = "false"
        FilterCurrency.Items(2).Enabled = (DDLType.SelectedIndex = 2)
        cbShowMemo.Visible = (DDLType.SelectedIndex = 2) : cbShowMemo.Checked = False
        lblNoRef.Visible = (DDLType.SelectedIndex = 2) : lblSept.Visible = (DDLType.SelectedIndex = 2) : tbNoRef.Visible = (DDLType.SelectedIndex = 2) : tbNoRef.Text = ""
    End Sub

    Protected Sub crv_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crv.Navigate
        imbView_Click(Nothing, Nothing)
    End Sub

    Protected Sub imbView_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbView.Click
        UpdateCheckedGV()
        Dim sMsg As String = ValidateParam()
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        Else
            sMsg = "" : sMsg = ProcessReport()
            If sMsg <> "" Then
                showMessage(sMsg, 1)
            Else
                crv.ReportSource = report
                Session("showReport") = "true"
            End If
        End If
    End Sub

    Protected Sub imbPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbPDF.Click
        UpdateCheckedGV()
        Dim sMsg As String = ValidateParam()
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        Else
            sMsg = "" : sMsg = ProcessReport()
            If sMsg <> "" Then
                showMessage(sMsg, 1)
            Else
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                Try
                    report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "GLReport_" & _
                        DDLType.SelectedItem.Text.Replace(" ", "") & "_" & IIf(FilterCurrency.SelectedIndex = 2, "", FilterCurrency.SelectedValue & "_") & _
                        Format(CDate(date1.Text), "yyMMdd") & "_" & Format(CDate(date2.Text), "yyMMdd"))
                Catch ex As Exception
                    report.Close() : report.Dispose()
                End Try
            End If
        End If
    End Sub

    Protected Sub imbXLS_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbXLS.Click
        UpdateCheckedGV()
        Dim sMsg As String = ValidateParam()
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        Else
            sMsg = "" : sMsg = ProcessReport(True)
            If sMsg <> "" Then
                showMessage(sMsg, 1)
            Else
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                Try
                    report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "GLReport_" & _
                        DDLType.SelectedItem.Text.Replace(" ", "") & "_" & IIf(FilterCurrency.SelectedIndex = 2, "", FilterCurrency.SelectedValue & "_") & _
                        Format(CDate(date1.Text), "yyMMdd") & "_" & Format(CDate(date2.Text), "yyMMdd"))
                Catch ex As Exception
                    report.Close() : report.Dispose()
                End Try
            End If
        End If
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClear.Click
        Response.Redirect("frmGeneralLedger.aspx?awal=true")
    End Sub
#End Region

    Protected Sub FilterDDLDiv_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLDiv.SelectedIndexChanged
        InitDDLGroup()
    End Sub
End Class
