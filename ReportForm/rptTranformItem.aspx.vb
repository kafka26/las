Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class ReportForm_rptTItem
    Inherits System.Web.UI.Page

#Region "Variables"
    Dim Report As New ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Dim CProc As New ClassProcedure
    Dim CKon As New Koneksi
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim sSql As String = ""
    Public folderReport As String = "~/Report/"
#End Region

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyCode
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matoid=" & cbOid
                                dtView2.RowFilter = "matoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Sub BindListMat()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, md.transformdtl1refoid AS matoid, m.itemShortDescription AS matshortdesc, m.itemLongDescription AS matlongdesc, m.itemCode AS matcode, g2.gendesc Unit/*,mt.transitemno,mt.transitemdate,md.transitemfromwhoid,md.transitemtowhoid */ FROM QL_trntransformdtl1 md INNER JOIN QL_trntransformmst mt ON mt.cmpcode=md.cmpcode AND mt.transformmstoid=md.transformmstoid INNER JOIN QL_mstitem m ON m.itemoid=md.transformdtl1refoid INNER JOIN QL_trntransformdtl2 md2 On mt.transformmstoid=md2.transformmstoid INNER JOIN QL_mstgen g2 ON g2.genoid=m.itemUnit1 WHERE mt.cmpcode='" & DDLbranch.SelectedValue & "' AND mt.transformdate BETWEEN '" & CDate(range1.Text) & "' AND '" & CDate(range2.Text) & "'"
        Session("TblMat") = CKon.ambiltabel(sSql, "QL_mstitem")
    End Sub 'OK

    Sub showPrint(ByVal sType As String)
        Try
            Dim sWhere As String = ""
            Dim rptName As String = ""

            sWhere &= " WHERE transm.cmpcode='" & DDLbranch.SelectedValue & "' AND transm.transformdate BETWEEN '" & CDate(range1.Text) & "' and '" & CDate(range2.Text) & "'"
          
            If ddllocawal.SelectedItem.Text <> "ALL" Then
                sWhere &= " AND transm.transformfromwhoid=" & ddllocawal.SelectedValue & ""
            Else
                sWhere &= ""
            End If

            If ddllocakhir.SelectedItem.Text <> "ALL" Then
                sWhere &= " AND transm.transformtowhoid=" & ddllocakhir.SelectedValue & ""
            Else
                sWhere &= ""
            End If
            If barang.Text.Trim <> "" Then
                Dim sMatcode() As String = Split(barang.Text, ";")
                sSql &= " AND ("
                For c1 As Integer = 0 To sMatcode.Length - 1
                    sSql &= " m.itemCode LIKE '%" & Tchar(sMatcode(c1)) & "%'"
                    If c1 < sMatcode.Length - 1 Then
                        sSql &= " OR"
                    End If
                Next
                sSql &= ")"
            Else
                sSql &= ""
            End If

            Report = New ReportDocument
            If sType = "xls" Then
                Report.Load(Server.MapPath("~\Report\rptTransformItem.rpt"))
                rptName = "Fixed_Assets_Report_(Sum)_"
            Else
                Report.Load(Server.MapPath("~\Report\rptTransformItem.rpt"))
                rptName = "Fixed_Assets_Report_(Sum)_"
            End If

            Report.SetParameterValue("sSql", sSql)
            Report.SetParameterValue("sWhere", sWhere)
            Report.SetParameterValue("sPeriode", range1.Text & " - " & range2.Text)
            Report.SetParameterValue("reportName", Report.FileName.Substring(Report.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))

            CProc.SetDBLogonForReport(Report)

            If sType = "crv" Then
                CrystalReportViewer1.DisplayGroupTree = False
                CrystalReportViewer1.ReportSource = Report
            ElseIf sType = "xls" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "Fixed_Asset_Report" & Format(GetServerTime(), "MM_dd_yy"))
                Report.Close() : Report.Dispose()
                Response.Redirect("~\ReportForm\rptFixedAsset.aspx")
            ElseIf sType = "pdf" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "Fixed_Asset_Report" & Format(GetServerTime(), "MM_dd_yy"))
                Report.Close()
                Report.Dispose()
                Response.Redirect("~\ReportForm\rptTranformItem.aspx")
            Else
            End If

        Catch ex As Exception
            Report.Close()
            Report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Private Sub DDLUnit()
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLbranch, sSql)
    End Sub

    Sub DDLFromWh()
        sSql = "SELECT g.genoid,g.gendesc FROM QL_mstgen g INNER JOIN QL_trntransformmst tr ON tr.cmpcode=g.cmpcode AND tr.transformfromwhoid=g.genoid AND g.gengroup='WAREHOUSE' ORDER BY g.genoid"
        FillDDL(ddllocawal, sSql)
        ddllocawal.Items.Add("ALL")
        'ddllocawal.Items(ddllocawal.Items.Count - 1).Value = "ALL"
        ddllocawal.SelectedValue = "ALL"
    End Sub

    Sub DDLToWh(ByVal WhOId As String)
        sSql = "SELECT g.genoid,g.gendesc FROM QL_mstgen g INNER JOIN QL_trntransformmst tr ON tr.cmpcode=g.cmpcode AND tr.transformtowhoid=g.genoid AND g.gengroup='WAREHOUSE' " & WhOId & ""
        If ddllocawal.SelectedItem.Text <> "ALL" Then
            FillDDL(ddllocakhir, sSql)
        Else
            ddllocakhir.Items.Add("ALL")
            ddllocakhir.Items(ddllocakhir.Items.Count - 1).Value = "ALL"
            ddllocakhir.SelectedValue = "ALL"
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\rptTranformItem.aspx")
        End If
        If checkPagePermission("~\ReportForm\rptTranformItem.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Transformation Item Report"
        Label2.Text = ""

        If Not Page.IsPostBack Then
            DDLFromWh()
            DDLToWh("")
            DDLUnit()
            range1.Text = Format(New Date(GetServerTime.Year, Now.Month, 1), "MM/dd/yyyy")
            range2.Text = Format(GetServerTime, "MM/dd/yyyy")
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not Report Is Nothing Then
                If Report.IsLoaded Then
                    Report.Dispose()
                    Report.Close()
                End If
            End If
        Catch ex As Exception
            Report.Dispose()
            Report.Close()
        End Try
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnshowprint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnshowprint.Click
        If range1.Text.Trim = "" Then
            Label2.Text = "First period cannot empty!"
            Exit Sub
        End If
        If range2.Text.Trim = "" Then
            Label2.Text = "Second period cannot empty!"
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "MM/dd/yyyy", Nothing, Nothing, date1) = False Then
            Label2.Text = "Date format for first period is invalid!"
            Exit Sub
        End If
        If Date.TryParseExact(range2.Text, "MM/dd/yyyy", Nothing, Nothing, date2) = False Then
            Label2.Text = "Date format for second period is invalid!"
            Exit Sub
        End If
        If date2 < date1 Then
            Label2.Text = "Second period cannot less than first period!"
            Exit Sub
        End If
        'ShowReport("crv")
        showPrint("crv")
    End Sub

    Protected Sub btnpdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnpdf.Click
        If range1.Text.Trim = "" Then
            Label2.Text = "First period cannot empty!"
            Exit Sub
        End If
        If range2.Text.Trim = "" Then
            Label2.Text = "Second period cannot empty!"
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "MM/dd/yyyy", Nothing, Nothing, date1) = False Then
            Label2.Text = "Date format for first period is invalid!"
            Exit Sub
        End If
        If Date.TryParseExact(range2.Text, "MM/dd/yyyy", Nothing, Nothing, date2) = False Then
            Label2.Text = "Date format for second period is invalid!"
            Exit Sub
        End If
        If date2 < date1 Then
            Label2.Text = "Second period cannot less than first period!"
            Exit Sub
        End If

        showPrint("pdf")
    End Sub

    Protected Sub ibExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibexcel.Click
        If range1.Text.Trim = "" Then
            Label2.Text = "First period cannot empty!"
            Exit Sub
        End If
        If range2.Text.Trim = "" Then
            Label2.Text = "Second period cannot empty!"
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "MM/dd/yyyy", Nothing, Nothing, date1) = False Then
            Label2.Text = "Date format for first period is invalid!"
            Exit Sub
        End If
        If Date.TryParseExact(range2.Text, "MM/dd/yyyy", Nothing, Nothing, date2) = False Then
            Label2.Text = "Date format for second period is invalid!"
            Exit Sub
        End If
        If date2 < date1 Then
            Label2.Text = "Second period cannot less than first period!"
            Exit Sub
        End If

        showPrint("excel")
    End Sub

    Protected Sub ibClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibClear.Click
        Response.Redirect("rptTranformItem.aspx?awal=true")
    End Sub

    Protected Sub barangsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles barangsearch.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        Session("TblMat") = Nothing
        Session("TblMatView") = Nothing
        gvListMat.DataSource = Nothing : gvListMat.DataBind()
        tbData.Text = "5" : gvListMat.PageSize = CInt(tbData.Text)
        'BindListMat()
        CProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Protected Sub barangerase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles barangerase.Click
        barang.Text = ""
        barangoid.Text = ""
        merk.Text = ""
        'matcode.Text = ""
        'GVBarang.DataSource = Nothing
        'GVBarang.DataBind()
        'GVBarang.Visible = False
        'Session("GVBarang") = Nothing
    End Sub

    Protected Sub CrystalReportViewer1_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles CrystalReportViewer1.Navigate
        showPrint("")
    End Sub

    Protected Sub DDLbranch_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DDLbranch.SelectedIndexChanged
        If Page.IsPostBack Then
            If Session("branch_id") = "01" Then
                If DDLbranch.SelectedValue = "All" Then
                    sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'INNER JOIN QL_mstgen c ON c.genoid = a.genother2 "

                    FillDDL(ddllocawal, sSql)
                    ddllocawal.Items.Add("All")
                    ddllocawal.Items(ddllocawal.Items.Count - 1).Value = "All"
                    ddllocawal.SelectedValue = "All"

                    sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'INNER JOIN QL_mstgen c ON c.genoid = a.genother2 "
                    FillDDL(ddllocakhir, sSql)
                    ddllocakhir.Items.Add("All")
                    ddllocakhir.Items(ddllocakhir.Items.Count - 1).Value = "All"
                    ddllocakhir.SelectedValue = "All"
                Else
                    sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'INNER JOIN QL_mstgen c ON c.genoid = a.genother2 WHERE c.gencode = '" & DDLbranch.SelectedValue & "'"

                    FillDDL(ddllocawal, sSql)
                    ddllocawal.Items.Add("All")
                    ddllocawal.Items(ddllocawal.Items.Count - 1).Value = "All"
                    ddllocawal.SelectedValue = "All"

                    sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'INNER JOIN QL_mstgen c ON c.genoid = a.genother2 WHERE c.gencode = '" & DDLbranch.SelectedValue & "'"
                    FillDDL(ddllocakhir, sSql)
                    ddllocakhir.Items.Add("All")
                    ddllocakhir.Items(ddllocakhir.Items.Count - 1).Value = "All"
                    ddllocakhir.SelectedValue = "All"
                End If
            Else
                sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'INNER JOIN QL_mstgen c ON c.genoid = a.genother2 WHERE c.gencode = '" & DDLbranch.SelectedValue & "'"

                FillDDL(ddllocawal, sSql)
                ddllocawal.Items.Add("All")
                ddllocawal.Items(ddllocawal.Items.Count - 1).Value = "All"
                ddllocawal.SelectedValue = "All"

                sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'INNER JOIN QL_mstgen c ON c.genoid = a.genother2 WHERE c.gencode = '" & DDLbranch.SelectedValue & "'"
                FillDDL(ddllocakhir, sSql)
                ddllocakhir.Items.Add("All")
                ddllocakhir.Items(ddllocakhir.Items.Count - 1).Value = "All"
                ddllocakhir.SelectedValue = "All"
            End If

        End If
    End Sub

    Protected Sub ddllocawal_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddllocawal.SelectedIndexChanged
        If ddllocawal.SelectedItem.Text <> "ALL" Then
            DDLToWh("WHERE tr.transformfromwhoid = " & ddllocawal.SelectedValue & "")
        Else
            DDLToWh("")
        End If
    End Sub

#Region "popup"
    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If barang.Text <> "" Then
                            barang.Text &= ";" + vbCrLf + dtView(C1)("matcode")
                        Else
                            barang.Text = dtView(C1)("matcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    CProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some Material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        'If cbCat01.Checked Then
        '    If FilterDDLCat01.SelectedValue <> "" Then
        '        sPlus &= " AND SUBSTRING(matcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'"
        '    Else
        '        cbCat01.Checked = False
        '    End If
        'End If

        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        BindListMat()
        'InitFilterDDLCat1()
        cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
        If Session("TblMat") Is Nothing Then
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matoid=" & dtTbl.Rows(C1)("matoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matoid=" & dtTbl.Rows(C1)("matoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Selected material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        CProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub 'OK
#End Region
End Class
