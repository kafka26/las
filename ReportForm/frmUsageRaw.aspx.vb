Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_RawMatUsage
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function 'OK
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub 'OK

    Private Sub InitAllDDL()
        ' DDL Division
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE' AND divcode<>'" & CompnyCode & "'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(FilterDDLDiv, sSql)
        If FilterDDLDiv.Items.Count > 0 Then
            InitDept()
        End If
    End Sub 'OK

    Private Sub InitDept()
        ' DDL Department
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE activeflag='ACTIVE' AND cmpcode='" & FilterDDLDiv.SelectedValue & "'"
        FillDDL(FilterDDLDept, sSql)
    End Sub 'OK

    Private Sub BindListUsg()
        sSql = "SELECT usgm.cmpcode, usgm.usagerawmstoid, usgm.periodacctg, CONVERT(VARCHAR(10), usgm.usagerawdate, 101) AS usagerawdate, usgm.usagerawno, usgm.deptoid, dept.deptname AS usagerawdept, usgm.usagerawmstnote, usgm.usagerawmststatus, usgm.createuser, usgm.createtime, usgm.upduser, usgm.updtime FROM QL_trnusagerawmst usgm INNER JOIN QL_mstdept dept ON dept.deptoid=usgm.deptoid AND dept.activeflag='ACTIVE' WHERE usgm.cmpcode='" & FilterDDLDiv.SelectedValue & "' AND " & DDLFilterspr.SelectedValue & " LIKE '%" & Tchar(txtFilterspr.Text) & "%'"
        If cbPeriod.Checked Then
            If IsValidPeriod() Then
                sSql &= " AND usgm.usagerawdate>='" & FilterPeriod1.Text & " 00:00:00' AND usgm.usagerawdate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbDept.Checked Then
            If FilterDDLDept.SelectedValue <> "" Then
                sSql &= " AND usgm.deptoid=" & FilterDDLDept.SelectedValue
            Else
                showMessage("Please select Department first!", 2)
                Exit Sub
            End If
        End If
        If checkPagePermission("~\ReportForm\frmUsageRaw.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND usgm.createuser='" & Session("UserID") & "'"
        End If
        sSql &= " ORDER BY usgm.usagerawdate DESC, usgm.usagerawmstoid DESC"
        FillGV(gvSpr, sSql, "QL_trnusagerawmst")
    End Sub 'OK

    Private Sub BindListMat()
        sSql = "SELECT DISTINCT usgd.matrawoid, m.matrawlongdesc , m.matrawcode , usgd.usagerawunitoid FROM QL_trnusagerawdtl usgd INNER JOIN QL_trnusagerawmst usgm ON usgm.cmpcode=usgd.cmpcode AND usgm.usagerawmstoid=usgd.usagerawmstoid INNER JOIN QL_mstmatraw m ON m.matrawoid=usgd.matrawoid INNER JOIN QL_mstgen g2 ON g2.genoid=usgd.usagerawunitoid WHERE usgd.cmpcode='" & FilterDDLDiv.SelectedValue & "'"
        If cbUsgNo.Checked = True Then
            If usagerawmstoid.Text <> "" Then
                sSql &= "AND usgd.usagerawmstoid=" & usagerawmstoid.Text & ""
            End If
        End If
        If cbPeriod.Checked Then
            If IsValidPeriod() Then
                sSql &= " AND usgm.usagerawdate>='" & FilterPeriod1.Text & " 00:00:00' AND usgm.usagerawdate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        Session("TblMat") = cKon.ambiltabel(sSql, "QL_mstmat")
        FillGV(gvListMat, sSql, "QL_mstmat")
    End Sub 'OK

    Private Sub SetEnable(ByVal bValue As Boolean)
        FilterDDLDiv.Enabled = bValue
        cbPeriod.Enabled = bValue
        FilterPeriod1.Enabled = bValue
        FilterPeriod2.Enabled = bValue
        CalPeriod1.Enabled = bValue
        CalPeriod2.Enabled = bValue
        cbDept.Enabled = bValue
        FilterDDLDept.Enabled = bValue
        cbUsgNo.Enabled = bValue
    End Sub 'OK

    Private Sub SetEnableMat(ByVal bValue As Boolean)
        If Not cbUsgNo.Checked Then
            FilterDDLDiv.Enabled = bValue
            cbPeriod.Enabled = bValue
            FilterPeriod1.Enabled = bValue
            FilterPeriod2.Enabled = bValue
            CalPeriod1.Enabled = bValue
            CalPeriod2.Enabled = bValue
            cbDept.Enabled = bValue
            FilterDDLDept.Enabled = bValue
            cbUsgNo.Enabled = bValue
        End If
        imbFindUsg.Enabled = bValue
        imbEraseUsg.Enabled = bValue
    End Sub 'OK

    Private Sub ShowReport(ByVal sType As String)
        If FilterDDLDiv.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        Try
            Dim sPeriod As String
            Dim sWhere As String = " WHERE usgm.cmpcode='" & FilterDDLDiv.SelectedValue & "'"
            Dim rptName As String = ""
            If FilterDDLType.SelectedValue = "Summary" Then
                If DDLRate.SelectedValue = "IDR" Then
                    report.Load(Server.MapPath(folderReport & "rptUsageRawMatSum.rpt"))
                    rptName = "RawMaterialUsageSummaryIDR_"
                Else
                    report.Load(Server.MapPath(folderReport & "rptUsageRawMatSumUSD.rpt"))
                    rptName = "RawMaterialUsageSummaryUSD_"
                End If
            Else
                If DDLRate.SelectedValue = "IDR" Then
                    report.Load(Server.MapPath(folderReport & "rptUsageRawMatDtl.rpt"))
                    rptName = "RawMaterialUsageDetailIDR_"
                Else
                    report.Load(Server.MapPath(folderReport & "rptUsageRawMatDtlUSD.rpt"))
                    rptName = "RawMaterialUsageDetailUSD_"
                End If
            End If
            If cbPeriod.Checked Then
                If IsValidPeriod() Then
                    sWhere &= " AND usgm.usagerawdate>='" & FilterPeriod1.Text & " 00:00:00' AND usgm.usagerawdate<='" & FilterPeriod2.Text & " 23:59:59'"
                    sPeriod = GetDateToPeriodAcctg(CDate(FilterPeriod2.Text))
                Else
                    Exit Sub
                End If
            Else
                sPeriod = GetDateToPeriodAcctg(CDate(Format(GetServerTime(), "MM/dd/yyyy")))
            End If
            If cbDept.Checked Then
                If FilterDDLDept.SelectedValue <> "" Then
                    sWhere &= " AND usgm.deptoid=" & FilterDDLDept.SelectedValue
                Else
                    showMessage("Please select Department first!", 2)
                    Exit Sub
                End If
            End If
            If cbUsgNo.Checked Then
                If usagerawmstoid.Text <> "" Then
                    sWhere &= " AND usgm.usagerawmstoid=" & usagerawmstoid.Text
                Else
                    showMessage("Please select Usage No. data first!", 2)
                    Exit Sub
                End If
            End If
            If cbMat.Checked Then
                If matrawoid.Text <> "" Then
                    sWhere &= " AND usgd.matrawoid = " & matrawoid.Text & ""
                Else
                    showMessage("Please select Material first!", 2)
                    Exit Sub
                End If
            End If
            If checkPagePermission("~\ReportForm\frmUsageRaw.aspx", Session("SpecialAccess")) = False Then
                sWhere &= " AND usgm.createuser='" & Session("UserID") & "'"
            End If
            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("sPeriod", sPeriod)
            cProc.SetDBLogonForReport(report)
            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, rptName & Format(GetServerTime(), "yyyyMMddHHmmss"))
                report.Close()
                report.Dispose()
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, rptName & Format(GetServerTime(), "yyyyMMddHHmmss"))
                report.Close()
                report.Dispose()
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub 'OK
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        'Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmUsageRaw.aspx")
        End If
        If checkPagePermission("~\ReportForm\frmUsageRaw.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Raw Material Usage Report"
        If Not Page.IsPostBack Then
            InitAllDDL()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        End If
    End Sub 'Blm

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub 'OK

    Protected Sub FilterDDLDiv_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLDiv.SelectedIndexChanged
        InitDept()
    End Sub 'OK

    Protected Sub cbUsgNo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbUsgNo.CheckedChanged
        If cbUsgNo.Checked Then
            imbFindUsg.Visible = True
            imbEraseUsg.Visible = True
        Else
            imbFindUsg.Visible = False
            imbEraseUsg.Visible = False
        End If
    End Sub 'OK

    Protected Sub imbFindUsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindUsg.Click
        If FilterDDLDiv.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        BindListUsg()
        cProc.SetModalPopUpExtender(btnHiddenspr, pnlspr, ModalPopupExtenderspr, True)
    End Sub 'OK

    Protected Sub imbEraseUsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseUsg.Click
        usagerawmstoid.Text = ""
        usagerawno.Text = ""
        SetEnable(True)
        cbUsgNo.Text = "Usage No."
        cbUsgNo.Checked = False
        cbUsgNo_CheckedChanged(Nothing, Nothing)
        imbEraseMat_Click(Nothing, Nothing)
    End Sub 'OK

    Protected Sub btnfindspr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfindspr.Click
        BindListUsg()
        ModalPopupExtenderspr.Show()
    End Sub 'OK

    Protected Sub btnViewAllspr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllspr.Click
        txtFilterspr.Text = ""
        DDLFilterspr.SelectedIndex = -1
        BindListUsg()
        ModalPopupExtenderspr.Show()
    End Sub 'OK

    Protected Sub gvSpr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSpr.SelectedIndexChanged
        If gvSpr.SelectedDataKey.Item("usagerawno").ToString <> "" Then
            usagerawno.Text = gvSpr.SelectedDataKey.Item("usagerawno").ToString
            cbUsgNo.Text = "Usage No."
        Else
            usagerawno.Text = gvSpr.SelectedDataKey.Item("usagerawmstoid").ToString
            cbUsgNo.Text = "Draft No."
        End If
        usagerawmstoid.Text = gvSpr.SelectedDataKey.Item("usagerawmstoid").ToString
        SetEnable(False)
        cProc.SetModalPopUpExtender(btnHiddenspr, pnlspr, ModalPopupExtenderspr, False)
    End Sub 'OK

    Protected Sub lkbClosespr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbClosespr.Click
        cProc.SetModalPopUpExtender(btnHiddenspr, pnlspr, ModalPopupExtenderspr, False)
    End Sub 'OK

    Protected Sub cbMat_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbMat.CheckedChanged
        If cbMat.Checked Then
            imbFindMat.Visible = True
            imbEraseMat.Visible = True
        Else
            imbFindMat.Visible = False
            imbEraseMat.Visible = False
        End If
    End Sub 'OK

    Protected Sub imbFindMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindMat.Click
        If FilterDDLDiv.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        BindListMat()
        cProc.SetModalPopUpExtender(btnHiddenListMat, PanelListMat, mpeListMat, True)
    End Sub 'OK

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Not Session("TblMat") Is Nothing Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = DDLFilterListMat.SelectedValue & " LIKE '%" & Tchar(txtFilterListMat.Text) & "%'"
            gvListMat.DataSource = dv.ToTable
            gvListMat.DataBind()
            dv.RowFilter = ""
        End If
        mpeListMat.Show()
    End Sub 'OK

    Protected Sub btnViewAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListMat.Click
        txtFilterListMat.Text = ""
        DDLFilterListMat.SelectedIndex = -1
        BindListMat()
        mpeListMat.Show()
    End Sub 'OK

    Protected Sub gvListMat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListMat.SelectedIndexChanged
        matrawlongdesc.Text = gvListMat.SelectedDataKey.Item("matrawlongdesc").ToString
        matrawoid.Text = gvListMat.SelectedDataKey.Item("matrawoid").ToString
        SetEnableMat(False)
        cProc.SetModalPopUpExtender(btnHiddenListMat, PanelListMat, mpeListMat, False)
    End Sub 'OK

    Protected Sub lkbListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListMat.Click
        cProc.SetModalPopUpExtender(btnHiddenListMat, PanelListMat, mpeListMat, False)
    End Sub 'OK

    Protected Sub imbEraseMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseMat.Click
        matrawlongdesc.Text = ""
        matrawoid.Text = ""
        SetEnableMat(True)
        cbMat.Checked = False
        cbMat_CheckedChanged(Nothing, Nothing)
    End Sub 'OK

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub 'OK

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub 'OK

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub 'OK

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmUsageRaw.aspx?awal=true")
    End Sub 'OK

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub 'OK

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub 'OK

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub 'OK

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        If Not report Is Nothing Then
            If report.IsLoaded Then
                report.Dispose()
                report.Close()
            End If
        End If
    End Sub 'OK
#End Region

End Class
