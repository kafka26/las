<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptPOStatus.aspx.vb" Inherits="rptPOStatus" title="Laporan Order Pembelian" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias">
        <tr>
            <th align="left" class="header" valign="center" style="width: 975px;">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="21px" ForeColor="Maroon" Text=".: Laporan Status PO"></asp:Label></th>
        </tr>
    </table>
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
<TABLE><TBODY><TR><TD align=center>&nbsp;&nbsp;&nbsp;&nbsp; <TABLE><TBODY><TR><TD style="WIDTH: 103px; TEXT-ALIGN: right" align=right></TD><TD align=left colSpan=3><asp:RadioButton id="rbPO" runat="server" Text="Status PO" __designer:wfdid="w1" AutoPostBack="True" Checked="True" GroupName="rbStatus" Visible="False"></asp:RadioButton> <asp:RadioButton id="rbPOLPB" runat="server" Text="Status PO-Purchase DO" __designer:wfdid="w2" AutoPostBack="True" GroupName="rbStatus" Visible="False"></asp:RadioButton> </TD></TR><TR><TD style="WIDTH: 103px; TEXT-ALIGN: right" id="tdTipeLap1" align=right runat="server"><asp:Label id="Label2" runat="server" Text="Tipe Laporan :" __designer:wfdid="w3" Visible="False" Width="100px"></asp:Label></TD><TD id="tdTipeLap2" align=left colSpan=3 runat="server"><asp:DropDownList id="type" runat="server" CssClass="inpText" __designer:wfdid="w4" Visible="False" Width="100px"><asp:ListItem>Summary</asp:ListItem>
<asp:ListItem>Detail</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 103px; TEXT-ALIGN: right" id="tdPeriod1" align=right runat="server" Visible="true"><asp:Label id="Label6" runat="server" Text="Periode : " __designer:wfdid="w5"></asp:Label></TD><TD id="tdperiod2" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="dateAwal" runat="server" CssClass="inpText" __designer:wfdid="w6" Width="75px"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w7"></asp:ImageButton>&nbsp; -&nbsp; <asp:TextBox id="dateAkhir" runat="server" CssClass="inpText" __designer:wfdid="w8" Width="75px"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w9"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" ForeColor="Red" __designer:wfdid="w10">(mm/dd/yyyy)</asp:Label><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w11" Format="MM/dd/yyyy" TargetControlID="dateAwal" PopupButtonID="imageButton1"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w12" Format="MM/dd/yyyy" TargetControlID="dateAkhir" PopupButtonID="imageButton2"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" __designer:wfdid="w13" TargetControlID="dateAwal" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" __designer:wfdid="w14" TargetControlID="dateAkhir" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 103px; TEXT-ALIGN: right" id="Td3" align=right runat="server" Visible="true">Nomor PO&nbsp;:</TD><TD id="Td4" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="nota" runat="server" CssClass="inpText" __designer:wfdid="w15" Width="250px"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchNota" onclick="btnSearchNota_Click1" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsBottom" __designer:wfdid="w16"></asp:ImageButton>&nbsp;<asp:ImageButton id="EraseNota" onclick="EraseNota_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom" __designer:wfdid="w17"></asp:ImageButton>&nbsp;<asp:Label id="oid" runat="server" __designer:wfdid="w18" Visible="False"></asp:Label><BR /><asp:GridView id="GVNota" runat="server" __designer:wfdid="w19" Width="450px" OnSelectedIndexChanged="GVNota_SelectedIndexChanged1" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="pomstoid,pono,suppname,suppoid" UseAccessibleHeader="False" BorderStyle="None" BorderWidth="1px" CellPadding="4" GridLines="None">
<RowStyle BackColor="#E3EAEB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="pono" HeaderText="No Nota">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow"></SelectedRowStyle>

<HeaderStyle BackColor="WhiteSmoke" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 103px; TEXT-ALIGN: right" align=right><asp:Label id="Label3" runat="server" Text="Item / Barang :" __designer:wfdid="w21"></asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="itemname" runat="server" CssClass="inpText" __designer:wfdid="w22" Width="250px"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" onclick="btnSearchItem_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsBottom" __designer:wfdid="w23" Width="17px" Height="17px"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseItem" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom" __designer:wfdid="w24"></asp:ImageButton>&nbsp;<asp:Label id="itemoid" runat="server" __designer:wfdid="w25" Visible="False"></asp:Label>&nbsp;&nbsp;<BR /><asp:GridView id="gvItem" runat="server" __designer:wfdid="w26" Width="100%" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="itemoid,itemCode,itemShortDescription" UseAccessibleHeader="False" BorderStyle="None" BorderWidth="1px" CellPadding="4" GridLines="None">
<RowStyle BackColor="#E3EAEB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemCode" HeaderText="Kode Barang">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Itemshortdescription" HeaderText="Nama Barang">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="satuan1" HeaderText="Sat Besar" Visible="False"></asp:BoundField>
<asp:BoundField DataField="Itempriceunit1" HeaderText="Harga" Visible="False"></asp:BoundField>
<asp:BoundField DataField="satuan2" HeaderText="Sat Sdg" Visible="False"></asp:BoundField>
<asp:BoundField DataField="Itempriceunit2" HeaderText="Harga" Visible="False"></asp:BoundField>
<asp:BoundField DataField="satuan3" HeaderText="Sat Std" Visible="False"></asp:BoundField>
<asp:BoundField DataField="Itempriceunit3" HeaderText="Harga" Visible="False"></asp:BoundField>
</Columns>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow"></SelectedRowStyle>

<HeaderStyle BackColor="WhiteSmoke" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 103px; TEXT-ALIGN: right" align=right>Supplier :</TD><TD align=left colSpan=3><asp:TextBox id="suppname" runat="server" CssClass="inpText" __designer:wfdid="w27" Width="250px"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchSupplier" onclick="btnSearchSupplier_click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsBottom" __designer:wfdid="w28"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseSupplier" onclick="btnEraseSupplier_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom" __designer:wfdid="w29"></asp:ImageButton>&nbsp;<asp:Label id="suppoid" runat="server" __designer:wfdid="w30" Visible="False"></asp:Label><asp:GridView id="gvSupp" runat="server" __designer:wfdid="w31" Width="100%" OnSelectedIndexChanged="gvSupp_SelectedIndexChanged1" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="suppcode,suppoid,suppname,supptype" UseAccessibleHeader="False" BorderStyle="None" BorderWidth="1px" CellPadding="4" GridLines="None">
<RowStyle BackColor="#E3EAEB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Kode Supplier">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Nama Supplier">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="supptype" HeaderText="Tipe">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow"></SelectedRowStyle>

<HeaderStyle BackColor="WhiteSmoke" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 103px; TEXT-ALIGN: right" align=right>Taxable :</TD><TD align=left colSpan=3><asp:DropDownList id="taxable" runat="server" CssClass="inpText" __designer:wfdid="w32" Width="100px"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>TAX</asp:ListItem>
<asp:ListItem>NON TAX</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="merk" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w33" Visible="False" Width="250px" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 103px; TEXT-ALIGN: right" align=right>Status&nbsp;Item PO&nbsp;:</TD><TD align=left colSpan=3><asp:DropDownList id="statusDelivery" runat="server" CssClass="inpText" __designer:wfdid="w34" Width="100px"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>COMPLETE</asp:ListItem>
<asp:ListItem>IN COMPLETE</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 103px; TEXT-ALIGN: right" align=right>Status PO :</TD><TD align=left colSpan=3><asp:DropDownList id="ddlstatusPO" runat="server" CssClass="inpText" __designer:wfdid="w35" Width="100px"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>In Approval</asp:ListItem>
<asp:ListItem>Approved</asp:ListItem>
<asp:ListItem>Force Close</asp:ListItem>
<asp:ListItem>Cancel</asp:ListItem>
<asp:ListItem>Closed</asp:ListItem>
</asp:DropDownList> </TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 103px; TEXT-ALIGN: right" align=right><asp:CheckBox id="chkGroup" runat="server" Text="Group :" CssClass="Label" __designer:wfdid="w52"></asp:CheckBox></TD><TD align=left colSpan=3><asp:DropDownList id="FilterDDLGrup" runat="server" CssClass="inpText" __designer:wfdid="w53" Width="150px"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 103px; TEXT-ALIGN: right" align=right></TD><TD style="HEIGHT: 1px" align=left colSpan=3><asp:DropDownList id="ddlPIC" runat="server" CssClass="inpText" __designer:wfdid="w36" Visible="False" Width="315px"></asp:DropDownList> <asp:DropDownList id="po_spb" runat="server" CssClass="inpText" __designer:wfdid="w37" AutoPostBack="True" Visible="False" Width="64px"><asp:ListItem Enabled="False">SPB/PO</asp:ListItem>
<asp:ListItem>PO</asp:ListItem>
<asp:ListItem Enabled="False">SPB</asp:ListItem>
</asp:DropDownList> <asp:DropDownList id="period" runat="server" CssClass="inpText" __designer:wfdid="w38" Visible="False" Width="135px"></asp:DropDownList> <asp:DropDownList id="ddlLocation" runat="server" CssClass="inpText" __designer:wfdid="w39" AutoPostBack="True" Visible="False" Width="236px"><asp:ListItem>ALL LOCATION</asp:ListItem>
</asp:DropDownList>&nbsp; </TD></TR></TBODY></TABLE><asp:Label id="labelEx" runat="server" __designer:wfdid="w40" Width="98px"></asp:Label></TD></TR><TR><TD style="WIDTH: 979px; HEIGHT: 15px"><asp:DropDownList id="FilterDDLSubGrup" runat="server" CssClass="inpText" __designer:wfdid="w41" Visible="False" Width="405px"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 979px; HEIGHT: 32px" align=center><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w42"></asp:ImageButton> <asp:ImageButton id="btnPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w43"></asp:ImageButton> <asp:ImageButton id="ibexcel" onclick="ibexcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w44"></asp:ImageButton> <asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w45"></asp:ImageButton><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w46"><ProgressTemplate>
<STRONG><SPAN style="FONT-SIZE: 14pt; COLOR: #800080">Please Wait....</SPAN></STRONG><BR /><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w47"></asp:Image> 
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD style="WIDTH: 979px" align=center><asp:Label id="lblkonfirmasi" runat="server" ForeColor="Red" __designer:wfdid="w48"></asp:Label></TD></TR><TR><TD style="WIDTH: 979px" align=center><TABLE><TBODY><TR><TD align=center><CR:CrystalReportViewer id="crvMutasiStock" runat="server" __designer:wfdid="w49" Width="350px" Height="50px" ShowAllPageIds="True" AutoDataBind="true" DisplayGroupTree="False" HasCrystalLogo="False" HasExportButton="False" HasPrintButton="False" HasRefreshButton="True" HasSearchButton="False" HasToggleGroupTreeButton="False"></CR:CrystalReportViewer></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="btnViewReport"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnPdf"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>
</asp:Content>

