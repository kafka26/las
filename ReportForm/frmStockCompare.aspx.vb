Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_frmStockCompare
    Inherits System.Web.UI.Page

#Region "Variable"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Function"
    Private Function checkReportCondition()
        Dim res As String = ""
        If tbPeriod1.Text.Trim = "" Then
            res = "Date for start period cannot empty !"
            Return res
        End If
        If tbPeriod2.Text.Trim = "" Then
            res = "Date for end period cannot empty !"
            Return res
        End If
        Return res
    End Function
#End Region

#Region "Procedure"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyCode
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub 'OK

    Private Sub ddl()
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE' "
        Dim x As String = Session("CompnyCode")
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
            busunit.Enabled = False
            busunit.CssClass = "inpTextDisabled"
        Else
            busunit.Enabled = True
            busunit.CssClass = "inpText"
        End If
        FillDDL(busunit, sSql)
        sSql = "SELECT gencode,gendesc from QL_mstgen where gengroup='GROUPITEM' AND cmpcode = '" & CompnyCode & "'"
        FillDDL(ddlType, sSql)
    End Sub

    Private Sub setDefaultValue()
        Dim firstday As New Date(Date.Now.Year, Date.Now.Month, 1)
        tbPeriod1.Text = Format(DateTime.Now, "MM/dd/yyyy")
        tbPeriod2.Text = Format(DateTime.Now, "MM/dd/yyyy")
        itemgroup.SelectedIndex = 0
        itemoid.Text = ""
        itemcode.Text = ""

        sSql = "SELECT DISTINCT g.genoid,g.gendesc FROM QL_mstgen g INNER JOIN QL_crdstock crd ON crd.mtrlocoid=g.genoid AND gengroup='WAREHOUSE' INNER JOIN QL_mstitem i ON i.itemoid=crd.refoid WHERE g.cmpcode='" & CompnyCode & "' AND i.itemGroup LIKE '%" & ddlType.SelectedValue & "%'"
        FillDDL(warehouseoid, sSql)
        'If warehouseoid.Items.Count = 0 Then
        '    '    showMessage("Please create and Setup AREA for Stock on General group 'WAREHOUSE' !", 2)
        '    '    Exit Sub
        'End If
        report.Close() : report.Dispose()
        crvStock.Visible = False
    End Sub

    Private Sub setReport(ByVal showtype As String)

        crvStock.ReportSource = Nothing
        ' Session("Report") = Nothing

        Try
            Dim filtertype As String = ""
            Dim period1 As New Date
            Dim period2 As New Date
            Dim reporttype As String = ""

            If itemgroup.SelectedValue <> "All" Then
                filtertype = itemgroup.SelectedValue.ToString
            End If

            If Date.TryParseExact(tbPeriod1.Text.Trim, "MM/dd/yyyy", Nothing, Nothing, period1) = False Then
                showMessage("Date for start period is invalid !", 2)
                crvStock.ReportSource = Nothing
                Exit Sub
            End If
            If Date.TryParseExact(tbPeriod2.Text.Trim, "MM/dd/yyyy", Nothing, Nothing, period2) = False Then
                showMessage("Date for end period is invalid !", 2)
                crvStock.ReportSource = Nothing
                Exit Sub
            End If
            If period1.Year < 1900 Then
                showMessage("Year for start period cannot less than 1900 !", 2)
                crvStock.ReportSource = Nothing
                Exit Sub
            End If
            If period2.Year < 1900 Then
                showMessage("Year for end period cannot less than 1900 !", 2)
                crvStock.ReportSource = Nothing
                Exit Sub
            End If
            If period2 < period1 Then
                showMessage("End period cannot less than start period !", 2)
                crvStock.ReportSource = Nothing
                Exit Sub
            End If

            reporttype = ddlType.SelectedValue.ToString

            showReport(filtertype, period1, period2, reporttype, showtype)

            Session("args1") = filtertype
            Session("args2") = period1
            Session("args3") = period2
            Session("args6") = reporttype
        Catch ex As Exception
            showMessage(ex.ToString, 2)
            Exit Sub
        End Try
    End Sub

    Private Sub showReport(ByVal filtertype As String, ByVal period1 As Date, ByVal period2 As Date, ByVal reporttype As String, ByVal showtype As String)
        Try
            Dim filterTypeText As String = ""
            Dim periodText As String = ""
            Dim sWhereText1 As String = ""
            Dim sWhereText2 As String = ""
            Dim REFOID As String = ""
            Dim REFCODE As String = ""
            Dim REFDESC As String = ""
            Dim tglsaldo As String = ""

            Dim startPeriod As Date = New Date(period1.Year, period1.Month, 1)
            periodText = Format(period1, "MMM dd yyyy").ToString & " - " & Format(period2, "MMM dd yyyy").ToString
            REFOID = " i.itemoid "
            REFCODE = " i.itemcode "
            REFDESC = " i.itemLongDescription "
            sWhereText2 &= " WHERE dtl.cmpcode='" & CompnyCode & "' "
            If CekTanggal() Then
                sWhereText2 &= " AND mst.stockopdate between '" & CDate(tbPeriod1.Text) & "' and '" & CDate(tbPeriod2.Text) & "' AND mst.stockopstatus='Post' "
            End If

            If cbWH.Checked Then
                sWhereText2 &= " AND dtl.stockopwarehouseoid= " & warehouseoid.SelectedValue & " "
            End If

            If ddlType.SelectedValue.Trim <> "" Then
                sWhereText2 &= " AND i.itemgroup like '%" & ddlType.SelectedValue & "%'"
            End If

            If itemoid.Text <> "" Then
                sWhereText2 &= " AND dtl.stockoprefoid =" & itemoid.Text & "  "
            End If
            Dim TipeneJeh = ddlType.SelectedValue
            report.Load(Server.MapPath(folderReport & "rptStockCompare.rpt"))

            cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("DB-Server"), System.Configuration.ConfigurationManager.AppSettings("DB-Name"))

            'report.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            report.SetParameterValue("TipeReporte", TipeneJeh)
            report.SetParameterValue("period", periodText)
            report.SetParameterValue("REFOID", REFOID)
            report.SetParameterValue("REFCODE", REFCODE)
            report.SetParameterValue("REFDESC", REFDESC)
            report.SetParameterValue("sWhere1", sWhereText1)
            report.SetParameterValue("sWhere2", sWhereText2)

            If showtype = "crv" Then
                crvStock.ReportSource = report
                crvStock.Visible = True
            ElseIf showtype = "pdf" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                Try
                    report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "StockOpCompReport_" & ddlType.SelectedItem.Text & "_" & Format(DateTime.Now, "yyyyMMddHHmmss"))
                    report.Close() : report.Dispose()
                Catch ex As Exception
                    report.Close() : report.Dispose()
                End Try
            ElseIf showtype = "excel" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                Try
                    report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "StockOpCompReport_" & ddlType.SelectedItem.Text & "_" & Format(DateTime.Now, "yyyyMMddHHmmss"))
                    report.Close() : report.Dispose()
                Catch ex As Exception
                    report.Close() : report.Dispose()
                End Try
            End If

        Catch ex As Exception
            showMessage(ex.ToString, 2)
        End Try
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmStockCompare.aspx")
        End If
        If checkPagePermission("~\ReportForm\frmStockCompare.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Stock Opname Comparation Report"
        If Not Page.IsPostBack Then
            setDefaultValue()
            tbPeriod1.Text = Format(DateTime.Now, "MM/01/yyyy")
            tbPeriod2.Text = Format(DateTime.Now, "MM/dd/yyyy")
            ddl()
            'ddlType_SelectedIndexChanged(Nothing, Nothing)
        Else
		End If
    End Sub

    Protected Sub crvStock_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvStock.Navigate
        If Not Session("args1") Is Nothing And Not Session("args2") Is Nothing And Not Session("args3") Is Nothing And Not Session("args6") Is Nothing Then
            showReport(Session("args1"), Session("args2"), Session("args3"), Session("args6"), "crv")
        End If
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        gvListMat.PageIndex = e.NewPageIndex
        BindMaterialData()
        mpeListMat.Show()
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        If Not report Is Nothing Then
            report.Close() : report.Dispose()
        End If
    End Sub
#End Region

    Protected Sub btnOKPopup_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub ddlTypeX_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'btnClearRPT_Click(Nothing, Nothing)
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        setDefaultValue()
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : gvListMat.SelectedIndex = -1
        BindMaterialData()
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Protected Sub btnClearMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDetail()
    End Sub

    Private Function CekTanggal() As Boolean
        Dim sErr As String = ""
        Dim sError As String = ""
        If tbPeriod1.Text = "" Then
            sError &= "- Please fill DATE 1 field!<BR>"
        Else
            If Not IsValidDate(tbPeriod1.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- DATE 1 is invalid. " & sErr & "<BR>"
            End If
        End If
        If tbPeriod2.Text = "" Then
            sError &= "- Please fill DATE 2 field!<BR>"
        Else
            If Not IsValidDate(tbPeriod2.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- DATE 2 is invalid. " & sErr & "<BR>"
            End If
        End If
        If sError.Trim <> "" Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub BindMaterialData()
        sSql = "SELECT i.itemoid, i.itemcode, i.itemLongDescription AS itemlocaldesc  FROM QL_mstitem i WHERE i.cmpcode='PRKP' /*AND i.activeflag='A'*/ AND i.itemoid IN (SELECT dtl.stockoprefoid FROM QL_trnstockopnamedtl dtl INNER JOIN QL_trnstockopname mst ON mst.stockopoid=dtl.stockopoid Where mst.stockopdate between '" & CDate(tbPeriod1.Text) & "' and '" & CDate(tbPeriod2.Text) & "' AND dtl.stockoprefname='QL_MSTITEM') "

        If FilterTextListMat.Text.Trim <> "" Then
            sSql &= " AND " & FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        End If
        sSql &= " ORDER BY i.itemoid"
        FillGV(gvListMat, sSql, "QL_mstitem")
    End Sub

    Private Sub ClearDetail()
        itemoid.Text = ""
        itemcode.Text = ""
        btnSearchMat.Visible = True
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindMaterialData()
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : gvListMat.SelectedIndex = -1
        BindMaterialData()
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Protected Sub gvListMat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        itemoid.Text = gvListMat.SelectedDataKey.Item("itemoid").ToString
        itemcode.Text = gvListMat.SelectedDataKey.Item("itemcode").ToString
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub areaoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FillDDLWarehouse()
        FillDDLLocation()
    End Sub

    Protected Sub warehouseoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FillDDLLocation()
    End Sub

    Private Sub FillDDLWarehouse()
        'DDL Warehouse

        Dim sWhere As String = ""
        sWhere &= "  AND genother1=" & areaoid.SelectedValue & ""
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='WAREHOUSE' " & sWhere & " ORDER BY gendesc"
        FillDDL(warehouseoid, sSql)
        If warehouseoid.Items.Count > 0 Then
            warehouseoid.Items.Add("ALL")
            warehouseoid.SelectedValue = "ALL"
        Else
            warehouseoid.Items.Add("-")
            warehouseoid.Items(0).Value = "NULL"
        End If
    End Sub

    Private Sub FillDDLLocation()
        'DDL Location
        Dim sWhere As String = ""
        sWhere &= "  AND genother1=" & areaoid.SelectedValue & ""
        If warehouseoid.SelectedValue <> "ALL" Then
            sWhere &= "  AND genother2=" & warehouseoid.SelectedValue & ""
        End If
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='LOCATION' " & sWhere & " ORDER BY gendesc"
        FillDDL(locationoid, sSql)
        If locationoid.Items.Count > 0 Then
            locationoid.Items.Add("ALL")
            locationoid.SelectedValue = "ALL"
        Else
            locationoid.Items.Add("-")
            locationoid.Items(0).Value = "NULL"
        End If
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub gvListMat_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListMat.SelectedIndexChanged

    End Sub

    Protected Sub areaoid_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        'FillDDLWarehouse()
        'FillDDLLocation()
    End Sub

    Protected Sub warehouseoid_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        'FillDDLLocation()
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmStockCompare.aspx?awal=true")
    End Sub

    Protected Sub ViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim res As String = checkReportCondition()
        If res <> "" Then
            showMessage(res, 2)
            crvStock.ReportSource = Nothing
        Else
            setReport("crv")
        End If
    End Sub

    Protected Sub BtnPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim res As String = checkReportCondition()
        If res <> "" Then
            showMessage(res, 2)
            crvStock.ReportSource = Nothing
        Else
            setReport("pdf")
        End If
    End Sub

    Protected Sub BtnExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim res As String = checkReportCondition()
        If res <> "" Then
            showMessage(res, 2)
            crvStock.ReportSource = Nothing
        Else
            setReport("excel")
        End If
    End Sub
End Class
