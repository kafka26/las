Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_BOMReport
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function 'OK
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub 'OK

    Private Sub InitAllDDL()
        ' DDL Division
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        FillDDL(FilterDDLDiv, sSql)
        'If FilterDDLDiv.Items.Count > 0 Then
        '    InitDept()
        'End If
    End Sub 'OK

    'Private Sub InitDept()
    '    ' DDL Department
    '    sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE activeflag='ACTIVE' AND cmpcode='" & FilterDDLDiv.SelectedValue & "'"
    '    If cbResultNo.Checked Then
    '        If bomoid.Text <> "" Then
    '            sSql &= " AND deptoid IN (SELECT DISTINCT deptoid FROM QL_mstbom WHERE cmpcode='" & FilterDDLDiv.SelectedValue & "' AND bomoid=" & bomoid.Text & ")"
    '        End If
    '    End If
    '    FillDDL(FilterDDLDept, sSql)
    'End Sub 'OK

    Private Sub BindListResult()
        Dim boleane As String = ""
        'If DDLFilterListResult.SelectedValue.ToUpper <> "BOMOID" Then
        boleane = "  " & DDLFilterListResult.SelectedValue & " LIKE '%" & Tchar(txtFilterListResult.Text) & "%'"
        'Else
        'boleane = "  CONVERT(VARCHAR(10), pr." & DDLFilterListResult.SelectedValue & ") LIKE '%" & ToDouble(txtFilterListResult.Text) & "%'"
        'End If
        sSql = "SELECT 'False' AS Checkvalue , bom.bomoid, bom.bomdesc, CONVERT(VARCHAR(10), bom.bomdate, 101) AS bomdate, bom.bomnote FROM QL_mstbom bom WHERE bom.cmpcode='" & FilterDDLDiv.SelectedValue & "' AND " & boleane
        If cbPeriod.Checked Then
            If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
                If IsValidPeriod() Then
                    sSql &= " AND " & FilterDDLDate.SelectedValue & ">='" & FilterPeriod1.Text & " 00:00:00' AND " & FilterDDLDate.SelectedValue & "<='" & FilterPeriod2.Text & " 23:59:59'"
                Else
                    Exit Sub
                End If
            End If
        End If
        sSql &= " ORDER BY bom.bomdate DESC, bom.bomoid DESC"
        'FillGV(gvListResult, sSql, "QL_mstbom")
        Session("TblMat2") = cKon.ambiltabel(sSql, "QL_mstbom")
    End Sub 'OK

    Private Sub SetEnable(ByVal bValue As Boolean)
        FilterDDLDiv.Enabled = bValue
        cbPeriod.Enabled = bValue
        FilterPeriod1.Enabled = bValue
        FilterPeriod2.Enabled = bValue
        CalPeriod1.Enabled = bValue
        CalPeriod2.Enabled = bValue
        cbResultNo.Enabled = bValue
    End Sub 'OK

    Private Sub BindListItem()
        sSql = "SELECT DISTINCT prd.bomrefoid, (CASE prd.bomreftype WHEN 'FG' THEN (SELECT x.itemcode FROM QL_mstitem x WHERE x.itemoid=prd.bomrefoid) WHEN 'WIP' THEN (SELECT x.matrawcode FROM QL_mstmatraw x WHERE x.matrawoid=prd.bomrefoid) ELSE '' END) AS bomrefcode, (CASE prd.bomreftype WHEN 'FG' THEN (SELECT x.itemshortdesc FROM QL_mstitem x WHERE x.itemoid=prd.bomrefoid) WHEN 'WIP' THEN (SELECT x.matrawshortdesc FROM QL_mstmatraw x WHERE x.matrawoid=prd.bomrefoid) ELSE '' END) AS bomrefshortdesc, prd.bomreftype, g.gendesc AS bomunit, prd.bomdtlnote FROM QL_trnbomdtl prd INNER JOIN QL_mstbom bom ON bom.cmpcode=prd.cmpcode AND bom.bomoid=prd.bomoid INNER JOIN QL_mstgen g ON g.genoid=prd.bomunitoid WHERE prd.cmpcode='" & FilterDDLDiv.SelectedValue & "'"
        'If cbResultNo.Checked Then
        '    sSql &= " AND prd.bomoid=" & bomoid.Text
        'Else
        If cbPeriod.Checked Then
            If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
                If IsValidPeriod() Then
                    sSql &= " AND " & FilterDDLDate.SelectedValue & ">='" & FilterPeriod1.Text & " 00:00:00' AND " & FilterDDLDate.SelectedValue & "<='" & FilterPeriod2.Text & " 23:59:59'"
                Else
                    Exit Sub
                End If
            End If
        End If
        'End If
        'If cbDept.Checked Then
        '    sSql &= " AND pr.deptoid=" & FilterDDLDept.SelectedValue
        'End If
        sSql &= " ORDER BY bomrefcode"
        Session("TblListItem") = cKon.ambiltabel(sSql, "QL_trnbomdtl")
        gvListMat.DataSource = Session("TblListItem")
        gvListMat.DataBind()
    End Sub 'OK

    Private Sub SetEnable2(ByVal bValue As Boolean)
        If Not cbResultNo.Checked Then
            FilterDDLDiv.Enabled = bValue
            cbPeriod.Enabled = bValue
            FilterPeriod1.Enabled = bValue
            FilterPeriod2.Enabled = bValue
            CalPeriod1.Enabled = bValue
            CalPeriod2.Enabled = bValue
            cbResultNo.Enabled = bValue
        End If
        imbFindResult.Enabled = bValue
        imbEraseResult.Enabled = bValue
        'cbDept.Enabled = bValue
        'FilterDDLDept.Enabled = bValue
    End Sub 'OK

    Private Sub ShowReport(ByVal sType As String)
        If FilterDDLDiv.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        Try
            Dim sWhere As String = " WHERE bom.cmpcode='" & FilterDDLDiv.SelectedValue & "'"
            Dim rptName As String = ""
            If sType = "Print Excel" Then
                If DDLGrouping.SelectedValue = "Finish Good Code" Then
                    report.Load(Server.MapPath(folderReport & "rptBOMReportFGCodeXls.rpt"))
                ElseIf DDLGrouping.SelectedValue = "Sequence" Then
                    report.Load(Server.MapPath(folderReport & "rptBOMReportSeqXls.rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptBOMReportMatCodeXls.rpt"))
                End If
                'report.Load(Server.MapPath(folderReport & "rptBOMReportXls.rpt"))
                'If DDLGrouping.SelectedValue = "Process" Then
                'report.Load(Server.MapPath(folderReport & "rptBOMReportProcessXls.rpt"))
                'Else
                'report.Load(Server.MapPath(folderReport & "rptBOMReportMatCodeXls.rpt"))
                'End If
            Else
                If DDLGrouping.SelectedValue = "Finish Good Code" Then
                    report.Load(Server.MapPath(folderReport & "rptBOMReportFGCode.rpt"))
                ElseIf DDLGrouping.SelectedValue = "Sequence" Then
                    report.Load(Server.MapPath(folderReport & "rptBOMReportSeq.rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptBOMReportMatCode.rpt"))
                End If
            End If
            rptName = "BOMReport_"
            If cbPeriod.Checked Then
                If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
                    If IsValidPeriod() Then
                        sWhere &= " AND " & FilterDDLDate.SelectedValue & ">='" & FilterPeriod1.Text & " 00:00:00' AND " & FilterDDLDate.SelectedValue & "<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
            End If

            If cbResultNo.Checked Then
                If bomno.Text <> "" Then
                    Dim sBOMNo() As String = Split(bomno.Text, ";")
                    sWhere &= " AND ("
                    For c1 As Integer = 0 To sBOMNo.Length - 1
                        sWhere &= "bom.bomdesc LIKE '%" & Tchar(sBOMNo(c1)) & "%'"
                        If c1 < sBOMNo.Length - 1 Then
                            sWhere &= " OR "
                        End If
                    Next
                    sWhere &= ")"
                End If
            End If

            If cbResultItem.Checked Then
                If finishgoodno.Text <> "" Then
                    Dim sFGCode() As String = Split(finishgoodno.Text, ";")
                    sWhere &= " AND ("
                    For c1 As Integer = 0 To sFGCode.Length - 1
                        sWhere &= " i.itemcode LIKE '%" & Tchar(sFGCode(c1)) & "%'"
                        If c1 < sFGCode.Length - 1 Then
                            sWhere &= " OR "
                        End If
                    Next
                    sWhere &= ")"
                End If
            End If


            'If cbResultNo.Checked Then
            '    If bomno.Text <> "" Then
            '        sWhere &= " AND pr.bomoid=" & bomoid.Text
            '    Else
            '        showMessage("Please select " & cbResultNo.Text & " data first!", 2)
            '        Exit Sub
            '    End If
            'End If
            'If cbDept.Checked Then
            '    If FilterDDLDept.SelectedValue <> "" Then
            '        sWhere &= " AND pr.deptoid=" & FilterDDLDept.SelectedValue
            '    Else
            '        showMessage("Please select Department first!", 2)
            '        Exit Sub
            '    End If
            'End If
            'If cbResultItem.Checked Then
            '    If bomrefoid.Text <> "" Then
            '        sWhere &= " AND prd.bomrefoid=" & bomrefoid.Text & " AND prd.bomreftype='" & bomreftype.Text & "'"
            '    Else
            '        showMessage("Please select Result Item first!", 2)
            '        Exit Sub
            '    End If
            'End If
            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            cProc.SetDBLogonForReport(report)
            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, rptName & Format(GetServerTime(), "yyyyMMddHHmmss"))
                report.Close()
                report.Dispose()
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, rptName & Format(GetServerTime(), "yyyyMMddHHmmss"))
                report.Close()
                report.Dispose()
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub 'OK
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        'Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmBOMReport.aspx")
        End If
        If checkPagePermission("~\ReportForm\frmBOMReport.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Bill of Material Report"
        If Not Page.IsPostBack Then
            InitAllDDL()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        End If
    End Sub 'OK

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
        cProc.SetModalPopUpExtender(btnHiddenListResult, PanelListResult, mpeListResult, False)
    End Sub 'OK

    Protected Sub FilterDDLDiv_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLDiv.SelectedIndexChanged
        'InitDept()
    End Sub 'OK

    Protected Sub cbResultNo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbResultNo.CheckedChanged
        If cbResultNo.Checked Then
            imbFindResult.Visible = True
            imbEraseResult.Visible = True
        Else
            imbFindResult.Visible = False
            imbEraseResult.Visible = False
        End If
    End Sub 'OK

    Protected Sub imbFindResult_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindResult.Click
        'If FilterDDLDiv.SelectedValue = "" Then
        '    showMessage("Please select Business Unit first!", 2)
        '    Exit Sub
        'End If
        'BindListResult()
        'cProc.SetModalPopUpExtender(btnHiddenListResult, PanelListResult, mpeListResult, True)
        DDLFilterListResult.SelectedIndex = -1 : txtFilterListResult.Text = ""
        Session("TblMat2") = Nothing : Session("TblMatView2") = Nothing : gvListResult.DataSource = Nothing : gvListResult.DataBind()
        cProc.SetModalPopUpExtender(btnHiddenListResult, PanelListResult, mpeListResult, True)
    End Sub 'OK

    Protected Sub imbEraseResult_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseResult.Click
        bomno.Text = ""
        'SetEnable(True)
        'cbResultNo.Text = "Result No."
        'cbResultNo.Checked = False
        'cbResultNo_CheckedChanged(Nothing, Nothing)
        'InitDept()
    End Sub 'OK

    Protected Sub btnFindListResult_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListResult.Click
        'BindListResult()
        'mpeListResult.Show()
        If Session("TblMat2") Is Nothing Then
            BindListResult()
            If Session("TblMat2").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Result data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = ""
        'If DDLFilterListResult.SelectedValue.ToUpper = "BOMOID" Then
        'sPlus = DDLFilterListResult.SelectedValue & " = " & Tchar(txtFilterListResult.Text) & ""
        'sPlus = "  CONVERT(VARCHAR(10), " & DDLFilterListResult.SelectedValue & ") LIKE '%" & ToDouble(txtFilterListResult.Text) & "%'"
        'Else
        sPlus = DDLFilterListResult.SelectedValue & " LIKE '%" & ToDouble(txtFilterListResult.Text) & "%'"
        'End If
        If UpdateCheckedMat3() Then
            Dim dv As DataView = Session("TblMat2").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView2") = dv.ToTable
                gvListResult.DataSource = Session("TblMatView2")
                gvListResult.DataBind()
                dv.RowFilter = ""
                mpeListResult.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView2") = Nothing
                gvListResult.DataSource = Session("TblMatView2")
                gvListResult.DataBind()
                Session("WarningListMat") = "Result data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListResult.Show()
        End If
    End Sub 'OK

    Protected Sub btnViewAllListResult_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListResult.Click
        'txtFilterListResult.Text = ""
        'DDLFilterListResult.SelectedIndex = -1
        'BindListResult()
        'mpeListResult.Show()
        DDLFilterListResult.SelectedIndex = -1 : txtFilterListResult.Text = ""
        'InitFilterDDLCat1()
        'cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
        If Session("TblMat2") Is Nothing Then
            BindListResult()
            If Session("TblMat2").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Result data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat3() Then
            Dim dt As DataTable = Session("TblMat2")
            Session("TblMatView2") = dt
            gvListResult.DataSource = Session("TblMatView2")
            gvListResult.DataBind()
        End If
        mpeListResult.Show()
    End Sub

    Protected Sub gvListResult_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListResult.PageIndexChanging
        If UpdateCheckedMat4() Then
            gvListResult.PageIndex = e.NewPageIndex
            gvListResult.DataSource = Session("TblMatView2")
            gvListResult.DataBind()
        End If
        mpeListResult.Show()
    End Sub 'OK

    Protected Sub gvListResult_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListResult.SelectedIndexChanged
        'If gvListResult.SelectedDataKey.Item("bomno").ToString <> "" Then
        '    bomno.Text = gvListResult.SelectedDataKey.Item("bomno").ToString
        '    cbResultNo.Text = "Result No."
        'Else
        '    bomno.Text = gvListResult.SelectedDataKey.Item("bomoid").ToString
        '    cbResultNo.Text = "Draft No."
        'End If
        'bomoid.Text = gvListResult.SelectedDataKey.Item("bomoid").ToString
        'SetEnable(False)
        ''InitDept()
        'cProc.SetModalPopUpExtender(btnHiddenListResult, PanelListResult, mpeListResult, False)
    End Sub 'OK

    Protected Sub lkbListResult_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListResult.Click
        cProc.SetModalPopUpExtender(btnHiddenListResult, PanelListResult, mpeListResult, False)
    End Sub 'OK

    Protected Sub cbResultItem_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbResultItem.CheckedChanged
        If cbResultItem.Checked Then
            imbFindItem.Visible = True
            imbEraseItem.Visible = True
        Else
            imbFindItem.Visible = False
            imbEraseItem.Visible = False
        End If
    End Sub 'OK

    Protected Sub imbFindItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindItem.Click
        'If FilterDDLDiv.SelectedValue = "" Then
        '    showMessage("Please select Business Unit first!", 2)
        '    Exit Sub
        'End If
        'If cbResultNo.Checked Then
        '    If bomno.Text = "" Then
        '        showMessage("Please select " & cbResultNo.Text & " first!", 2)
        '        Exit Sub
        '    End If
        'End If
        ''If cbDept.Checked Then
        ''    If FilterDDLDept.SelectedValue = "" Then
        ''        showMessage("Please select Department first!", 2)
        ''        Exit Sub
        ''    End If
        ''End If
        'DDLFilterListItem.SelectedIndex = -1
        'txtFilterListItem.Text = ""
        'BindListItem()
        'cProc.SetModalPopUpExtender(btnHiddenListItem, PanelListItem, mpeListItem, True)

        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        InitFilterDDLCat1()
        cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
        Session("TblMat") = Nothing : Session("TblMatView") = Nothing : gvListMat.DataSource = Nothing : gvListMat.DataBind() : tbData.Text = "5" : gvListMat.PageSize = CInt(tbData.Text)
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub 'OK

    Private Sub InitFilterDDLCat1()
        'Fill DDL Category1
        sSql = "SELECT cat1oid, cat1code+' - '+cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat1res1='FG' Order By cat1code"
        FillDDL(FilterDDLCat01, sSql)
        If FilterDDLCat01.Items.Count > 0 Then
            InitFilterDDLCat2()
        Else
            FilterDDLCat02.Items.Clear()
            FilterDDLCat03.Items.Clear()
            FilterDDLCat04.Items.Clear()
        End If

    End Sub

    Private Sub InitFilterDDLCat2()
        'Fill DDL Category2
        sSql = "SELECT cat2oid, cat2code+' - '+cat2shortdesc FROM QL_mstcat2 cat2 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat2.cmpcode AND cat1.cat1oid=cat2.cat1oid AND cat1.cat1oid='" & FilterDDLCat01.SelectedValue & "' WHERE cat2.cmpcode='" & CompnyCode & "' AND cat2.activeflag='ACTIVE' AND cat2res1='FG' Order By cat2code"
        FillDDL(FilterDDLCat02, sSql)
        If FilterDDLCat02.Items.Count > 0 Then
            InitFilterDDLCat3()
        Else
            FilterDDLCat03.Items.Clear()
            FilterDDLCat04.Items.Clear()
        End If

    End Sub

    Private Sub InitFilterDDLCat3()
        'Fill DDL Category3
        sSql = "SELECT cat3oid, cat3code+' - '+cat3shortdesc FROM QL_mstcat3 cat3 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat3.cmpcode AND cat1.cat1oid=cat3.cat1oid AND cat1.cat1oid='" & FilterDDLCat01.SelectedValue & "' INNER JOIN QL_mstcat2 cat2 ON cat2.cmpcode=cat3.cmpcode AND cat2.cat2oid=cat3.cat2oid AND cat2.cat2oid='" & FilterDDLCat02.SelectedValue & "' WHERE cat3.cmpcode='" & CompnyCode & "' AND cat3.activeflag='ACTIVE' AND cat3res1='FG' Order By cat3code"
        FillDDL(FilterDDLCat03, sSql)
        If FilterDDLCat03.Items.Count > 0 Then
            InitFilterDDLCat4()
        Else
            FilterDDLCat04.Items.Clear()
        End If

    End Sub

    Private Sub InitFilterDDLCat4()
        'Fill DDL Category4
        sSql = "SELECT genoid, gencode+' - '+gendesc FROM QL_mstgen WHERE gengroup='CAT4' AND activeflag='ACTIVE' ORDER BY gencode"
        FillDDL(FilterDDLCat04, sSql)

    End Sub

    Protected Sub imbEraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseItem.Click
        'bomrefoid.Text = ""
        'bomrefshortdesc.Text = ""
        'bomreftype.Text = ""
        'SetEnable2(True)
        'cbResultItem.Checked = False
        'cbResultItem_CheckedChanged(Nothing, Nothing)
        finishgoodno.Text = ""
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub 'OK

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                dtView2.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat4() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView2") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat2")
            Dim dtTbl2 As DataTable = Session("TblMatView2")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListResult.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListResult.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "bomoid=" & cbOid
                                dtView2.RowFilter = "bomoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat2") = dtTbl
                Session("TblMatView2") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    'Protected Sub btnFindListItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListItem.Click
    '    If Not Session("TblListItem") Is Nothing Then
    '        Dim dv As DataView = Session("TblListItem").DefaultView
    '        dv.RowFilter = DDLFilterListItem.SelectedValue & " LIKE '%" & Tchar(txtFilterListItem.Text) & "%'"
    '        gvListMat.DataSource = dv.ToTable
    '        gvListMat.DataBind()
    '        dv.RowFilter = ""
    '    End If
    '    mpeListItem.Show()
    'End Sub 'OK

    'Protected Sub btnViewAllListItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListItem.Click
    '    If Not Session("TblListItem") Is Nothing Then
    '        DDLFilterListItem.SelectedIndex = -1
    '        txtFilterListItem.Text = ""
    '        gvListMat.DataSource = Session("TblListItem")
    '        gvListMat.DataBind()
    '    End If
    '    mpeListItem.Show()
    'End Sub 'OK

    Protected Sub gvListMat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListMat.SelectedIndexChanged
        'bomrefoid.Text = gvListMat.SelectedDataKey.Item("bomrefoid").ToString
        'bomrefshortdesc.Text = gvListMat.SelectedDataKey.Item("bomrefshortdesc").ToString
        'bomreftype.Text = gvListMat.SelectedDataKey.Item("bomreftype").ToString
        'SetEnable2(False)
        'cProc.SetModalPopUpExtender(btnHiddenListItem, PanelListItem, mpeListItem, False)
    End Sub 'OK

    'Protected Sub lkbListItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListItem.Click
    '    cProc.SetModalPopUpExtender(btnHiddenListItem, PanelListItem, mpeListItem, False)
    'End Sub 'OK

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub 'OK

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub 'OK

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub 'OK

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmBOMReport.aspx?awal=true")
    End Sub 'OK

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub 'OK

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub 'OK

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub 'OK

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        If Not report Is Nothing Then
            If report.IsLoaded Then
                report.Dispose()
                report.Close()
            End If
        End If
    End Sub 'OK

#End Region

    Private Sub BindListMat()
        sSql = "SELECT 'False' AS checkvalue, itemcat1, itemcat2, itemcat3, itemcta4, itemoid, itemcode, itemshortdescription AS itemshortdesc, itemlongdescription AS itemlongdesc, g.gendesc AS unit FROM QL_mstitem m INNER JOIN QL_mstgen g ON g.genoid=itemunit1 WHERE m.cmpcode='" & CompnyCode & "' AND m.itemoid IN (SELECT h.itemoid FROM QL_mstbom h) AND m.itemrecordstatus='ACTIVE' ORDER BY itemcode"
        Session("TblMat") = cKon.ambiltabel(sSql, "QL_mstitem")
    End Sub 'OK

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Finish good data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        If cbCat01.Checked Then
            If FilterDDLCat01.SelectedValue <> "" Then
                sPlus &= " AND itemcat1='" & FilterDDLCat01.SelectedValue & "'"
            Else
                cbCat01.Checked = False
            End If
        End If
        If cbCat02.Checked Then
            If FilterDDLCat02.SelectedValue <> "" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND itemcat1='" & FilterDDLCat01.SelectedValue & "'") & " AND itemcat2='" & FilterDDLCat02.SelectedValue & "'"
            Else
                cbCat02.Checked = False
            End If
        End If
        If cbCat03.Checked Then
            If FilterDDLCat03.SelectedValue <> "" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND itemcat1='" & FilterDDLCat01.SelectedValue & "'") & IIf(cbCat02.Checked, "", " AND itemcat2='" & FilterDDLCat02.SelectedValue & "'") & " AND itemcat3='" & FilterDDLCat03.SelectedValue & "'"
            Else
                cbCat03.Checked = False
            End If
        End If
        If cbCat04.Checked Then
            If FilterDDLCat04.SelectedValue <> "" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND itemcat1='" & FilterDDLCat01.SelectedValue & "'") & IIf(cbCat02.Checked, "", " AND itemcat2='" & FilterDDLCat02.SelectedValue & "'") & IIf(cbCat03.Checked, "", " AND itemcat3='" & FilterDDLCat03.SelectedValue & "'") & " AND itemcat4='" & FilterDDLCat04.SelectedValue & "'"
            Else
                cbCat04.Checked = False
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "finish good data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        InitFilterDDLCat1()
        cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "finish good data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some finish good data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some finish good data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected finish good data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                InitFilterDDLCat1()
                cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Selected finish good data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat3() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat2") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat2")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListResult.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListResult.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "bomoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat2") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Protected Sub lbShowData_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Please find finish good data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If tbData.Text = "" Then
            Session("WarningListMat") = "Please fill total data to be shown!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        Else
            If ToDouble(tbData.Text) <= 0 Then
                Session("WarningListMat") = "Total data to be shown must be more than 0!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat2() Then
            gvListMat.PageSize = Int(tbData.Text)
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If finishgoodno.Text <> "" Then
                            finishgoodno.Text &= ";" + vbCrLf + dtView(C1)("itemcode")
                        Else
                            finishgoodno.Text = dtView(C1)("itemcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some Material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbAddToList_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Session("TblMat2") Is Nothing Then
            If UpdateCheckedMat3() Then
                Dim dtTbl As DataTable = Session("TblMat2")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If bomno.Text <> "" Then
                            bomno.Text &= ";" + vbCrLf + dtView(C1)("bomdesc")
                        Else
                            bomno.Text = dtView(C1)("bomdesc")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHiddenListResult, PanelListResult, mpeListResult, False)
                Else
                    Session("WarningListMat") = "Please select Result to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some Result data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub FilterDDLCat01_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitFilterDDLCat2()
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        'mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat02_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitFilterDDLCat3()
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        'mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat03_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitFilterDDLCat4()
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        'mpeListMat.Show()
    End Sub
End Class
