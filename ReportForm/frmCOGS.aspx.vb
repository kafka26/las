Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_COGS
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If FilterPeriod1.Text = "" Or FilterPeriod2.Text = "" Then
            showMessage("Please fill both of Period 1 and Period 2 first!", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitAllDDL()
        ' Init DDL Business Unit
        sSql = "SELECT divcode, UPPER(divname) AS divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)
    End Sub

    Private Sub InitDDLCat1()
        'Fill DDL Category 1
        sSql = "SELECT cat1code, cat1code + ' - ' + cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat1res1='Finish Good' AND ISNULL(cat1res2, '') IN ('WIP', 'Non WIP', '')"
        If FillDDL(DDLCat01, sSql) Then
            InitDDLCat2()
        Else
            DDLCat02.Items.Clear()
            DDLCat03.Items.Clear()
            DDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat2()
        'Fill DDL Category 2
        sSql = "SELECT cat1code + '.' + cat2code, cat2code + ' - ' + cat2shortdesc FROM QL_mstcat2 c2 INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c1.cat1oid=c2.cat1oid WHERE c2.cmpcode='" & CompnyCode & "' AND c2.activeflag='ACTIVE' AND cat1code='" & DDLCat01.SelectedValue & "' AND cat2res1='Finish Good' AND cat1res1='Finish Good' AND ISNULL(cat1res2, '') IN ('WIP', 'Non WIP', '')"
        If FillDDL(DDLCat02, sSql) Then
            InitDDLCat3()
        Else
            DDLCat03.Items.Clear()
            DDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat3()
        'Fill DDL Category 3
        sSql = "SELECT cat1code + '.' + cat2code + '.' + cat3code, cat3code + ' - ' + cat3shortdesc FROM QL_mstcat3 c3 INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c3.cmpcode AND c1.cat1oid=c3.cat1oid WHERE c3.cmpcode='" & CompnyCode & "' AND c3.activeflag='ACTIVE' AND cat1code + '.' + cat2code='" & DDLCat02.SelectedValue & "' AND cat3res1='Finish Good' AND cat2res1='Finish Good' AND cat1res1='Finish Good' AND ISNULL(cat1res2, '') IN ('WIP', 'Non WIP', '')"
        If FillDDL(DDLCat03, sSql) Then
            InitDDLCat4()
        Else
            DDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat4()
        'Fill DDL Category 3
        sSql = "SELECT cat1code + '.' + cat2code + '.' + cat3code + '.' + cat4code, cat4code + ' - ' + cat4shortdesc FROM QL_mstcat4 c4 INNER JOIN QL_mstcat3 c3 ON c3.cmpcode=c4.cmpcode AND c3.cat3oid=c4.cat3oid INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c4.cmpcode AND c2.cat2oid=c4.cat2oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c4.cmpcode AND c1.cat1oid=c4.cat1oid WHERE c4.cmpcode='" & CompnyCode & "' AND c4.activeflag='ACTIVE' AND cat1code + '.' + cat2code + '.' + cat3code='" & DDLCat03.SelectedValue & "' AND cat4res1='Finish Good' AND cat3res1='Finish Good' AND cat2res1='Finish Good' AND cat1res1='Finish Good' AND ISNULL(cat1res2, '') IN ('WIP', 'Non WIP', '')"
        FillDDL(DDLCat04, sSql)
    End Sub

    Private Sub BindListMat()
        sSql = "SELECT itemoid AS matoid, itemcode AS matcode, itemlongdesc AS matlongdesc, gendesc AS matunit, 'False' AS checkvalue FROM QL_mstitem m INNER JOIN QL_mstgen g ON genoid=itemunitoid WHERE m.cmpcode='" & CompnyCode & "' AND m.activeflag='ACTIVE' ORDER BY itemcode"
        Session("TblListMat") = cKon.ambiltabel(sSql, "QL_mstmat")
        Session("TblListMatView") = Session("TblListMat")
        gvListMat.DataSource = Session("TblListMatView")
        gvListMat.DataBind()
    End Sub

    Private Sub UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMat") = dt
        End If
        If Session("TblListMatView") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMatView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMatView") = dt
        End If
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        Dim sWhere As String = ""
        Dim rptName As String = ""
        Dim Dtl As String = ""
        Dim Join As String = ""
        Dim sFilterDate As String
        Dim sFilterDate2 As String
        Dim sDate As String
        Dim sDate2 As String
        Try
            If sType = "Print Excel" Then
                report.Load(Server.MapPath(folderReport & "rptCOGSXls.rpt"))
            Else
                report.Load(Server.MapPath(folderReport & "rptCOGS.rpt"))
            End If
            rptName = "CostOfGoodSold"

            If DDLFilterDate.SelectedValue = "createtime" Then
                sFilterDate = "conf.confirmdate"
                sFilterDate2 = "shm.createtime"
            Else
                sFilterDate = "conf.updtime"
                sFilterDate2 = "shm.updtime"
            End If

            If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
                If IsValidPeriod() Then
                    sDate = FilterPeriod1.Text
                    sDate2 = FilterPeriod2.Text
                Else
                    Exit Sub
                End If
            End If

            sSql = " SELECT [Business Unit], [CMPCODE], [Oid], [FG Code], [FG Short Desc.], [FG Long Desc.], [FG Unit], ISNULL([Confirm SAValue],0.0)-ISNULL([Shipment SAValue],0.0) AS [Saldo Awal], ISNULL([Confirm SAQty],0.0)-ISNULL([Shipment SAQty],0.0) AS [Saldo Awal Qty], ISNULL([Confirm Value],0.0) AS [Confirm Value], ISNULL([Confirm Qty],0.0) AS [Confirm Qty], ISNULL([Shipment Value],0.0) AS [Shipment Value], ISNULL([Shipment Qty],0.0) AS [Shipment Qty] FROM (SELECT (SELECT div.divname FROM QL_mstdivision div WHERE div.cmpcode='" & DDLBusUnit.SelectedValue & "') AS [Business Unit], '" & DDLBusUnit.SelectedValue & "' [CMPCODE],itemoid [Oid], itemcode [FG Code], itemshortdesc [FG Short Desc.], itemlongdesc [FG Long Desc.], (SELECT g.gendesc FROM QL_mstgen g WHERE g.genoid=itemunitoid) AS [FG Unit] ,(SELECT SUM(confirmvalueidr) FROM QL_trnwodtl1 wod1 INNER JOIN QL_trnwomst wom ON wod1.cmpcode = wom.cmpcode AND wom.womstoid = wod1.womstoid INNER JOIN QL_trnwodtl2 wod2 ON wod2.womstoid = wom.womstoid AND wod2.cmpcode = wom.cmpcode AND wod2.wodtl2type='FG' INNER JOIN QL_trnprodresdtl prod ON wom.cmpcode=prod.cmpcode AND prod.wodtl2oid=wod2.wodtl2oid INNER JOIN QL_trnprodresconfirm conf ON conf.cmpcode=prod.cmpcode AND prod.prodresdtloid=conf.prodresdtloid WHERE wod1.itemoid=i.itemoid AND " & sFilterDate & " < '" & sDate & " 00:00:00' AND conf.cmpcode='" & DDLBusUnit.SelectedValue & "') AS [Confirm SAValue],(SELECT SUM(confirmqty) FROM QL_trnwodtl1 wod1 INNER JOIN QL_trnwomst wom ON wod1.cmpcode = wom.cmpcode AND wom.womstoid = wod1.womstoid INNER JOIN QL_trnwodtl2 wod2 ON wod2.womstoid = wom.womstoid AND wod2.cmpcode = wom.cmpcode AND wod2.wodtl2type='FG' INNER JOIN QL_trnprodresdtl prod ON wom.cmpcode=prod.cmpcode AND prod.wodtl2oid=wod2.wodtl2oid INNER JOIN QL_trnprodresconfirm conf ON conf.cmpcode=prod.cmpcode AND prod.prodresdtloid=conf.prodresdtloid WHERE wod1.itemoid=i.itemoid AND " & sFilterDate & " < '" & sDate & " 00:00:00' AND conf.cmpcode='" & DDLBusUnit.SelectedValue & "') AS [Confirm SAQty],(SELECT SUM(shipmentitemvalueidr) FROM QL_trnshipmentitemdtl shd INNER JOIN QL_trnshipmentitemmst shm ON shm.shipmentitemmstoid=shd.shipmentitemmstoid WHERE i.itemoid=shd.itemoid AND shm.shipmentitemmststatus IN ('Approved','Closed') AND " & sFilterDate2 & " < '" & sDate & " 00:00:00' AND shm.cmpcode='" & DDLBusUnit.SelectedValue & "') AS [Shipment SAValue],(SELECT SUM(shipmentitemqty) FROM QL_trnshipmentitemdtl shd INNER JOIN QL_trnshipmentitemmst shm ON shm.shipmentitemmstoid=shd.shipmentitemmstoid WHERE i.itemoid=shd.itemoid AND shm.shipmentitemmststatus IN ('Approved','Closed') AND " & sFilterDate2 & " < '" & sDate & " 00:00:00' AND shm.cmpcode='" & DDLBusUnit.SelectedValue & "') AS [Shipment SAQty],(SELECT SUM(confirmvalueidr) FROM QL_trnwodtl1 wod1 INNER JOIN QL_trnwomst wom ON wod1.cmpcode = wom.cmpcode AND wom.womstoid = wod1.womstoid INNER JOIN QL_trnwodtl2 wod2 ON wod2.womstoid = wom.womstoid AND wod2.cmpcode = wom.cmpcode AND wod2.wodtl2type='FG' INNER JOIN QL_trnprodresdtl prod ON wom.cmpcode=prod.cmpcode AND prod.wodtl2oid=wod2.wodtl2oid INNER JOIN QL_trnprodresconfirm conf ON conf.cmpcode=prod.cmpcode AND prod.prodresdtloid=conf.prodresdtloid WHERE wod1.itemoid=i.itemoid AND " & sFilterDate & " >= '" & sDate & " 00:00:00' AND " & sFilterDate & " <= '" & sDate2 & " 23:59:59' AND conf.cmpcode='" & DDLBusUnit.SelectedValue & "') AS [Confirm Value],(SELECT SUM(confirmqty) FROM QL_trnwodtl1 wod1 INNER JOIN QL_trnwomst wom ON wod1.cmpcode = wom.cmpcode AND wom.womstoid = wod1.womstoid INNER JOIN QL_trnwodtl2 wod2 ON wod2.womstoid = wom.womstoid AND wod2.cmpcode = wom.cmpcode AND wod2.wodtl2type='FG' INNER JOIN QL_trnprodresdtl prod ON wom.cmpcode=prod.cmpcode AND prod.wodtl2oid=wod2.wodtl2oid INNER JOIN QL_trnprodresconfirm conf ON conf.cmpcode=prod.cmpcode AND prod.prodresdtloid=conf.prodresdtloid WHERE wod1.itemoid=i.itemoid AND " & sFilterDate & " >= '" & sDate & " 00:00:00' AND " & sFilterDate & " <= '" & sDate2 & " 23:59:59' AND conf.cmpcode='" & DDLBusUnit.SelectedValue & "') AS [Confirm Qty],(SELECT SUM(shipmentitemvalueidr) FROM QL_trnshipmentitemdtl shd INNER JOIN QL_trnshipmentitemmst shm ON shm.shipmentitemmstoid=shd.shipmentitemmstoid WHERE i.itemoid=shd.itemoid AND shm.shipmentitemmststatus IN ('Approved','Closed') AND " & sFilterDate2 & " >= '" & sDate & " 00:00:00' AND " & sFilterDate2 & " <= '" & sDate2 & " 23:59:59' AND shm.cmpcode='" & DDLBusUnit.SelectedValue & "') AS [Shipment Value],(SELECT SUM(shipmentitemqty) FROM QL_trnshipmentitemdtl shd INNER JOIN QL_trnshipmentitemmst shm ON shm.shipmentitemmstoid=shd.shipmentitemmstoid WHERE i.itemoid=shd.itemoid AND shm.shipmentitemmststatus IN ('Approved','Closed') AND " & sFilterDate2 & " >= '" & sDate & " 00:00:00' AND " & sFilterDate2 & " <= '" & sDate2 & " 23:59:59' AND shm.cmpcode='" & DDLBusUnit.SelectedValue & "') AS [Shipment Qty] FROM QL_mstitem i) AS tbl WHERE ([Confirm Value] IS NOT NULL OR [Shipment Value] IS NOT NULL)"

            If FilterMaterial.Text <> "" Then
                Dim sMatcode() As String = Split(FilterMaterial.Text, ";")
                sSql &= " AND ("
                For c1 As Integer = 0 To sMatcode.Length - 1
                    sSql &= " [FG Code] LIKE '%" & Tchar(sMatcode(c1)) & "%'"
                    If c1 < sMatcode.Length - 1 Then
                        sSql &= " OR"
                    End If
                Next
                sSql &= ")"
            End If

            sSql &= " ORDER BY [FG Code] ASC "

            Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstitem")
            Dim dvTbl As DataView = dtTbl.DefaultView
          
            report.SetDataSource(dvTbl.ToTable)
            report.SetParameterValue("StartPeriod", Format(CDate(FilterPeriod1.Text), "dd MMM yyyy"))
            report.SetParameterValue("EndPeriod", Format(CDate(FilterPeriod2.Text), "dd MMM yyyy"))
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            report.PrintOptions.PaperSize = PaperSize.PaperFolio
            cProc.SetDBLogonForReport(report)

            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, rptName)
                report.Close()
                report.Dispose()
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, rptName)
                report.Close()
                report.Dispose()
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmCOGS.aspx")
        End If
        If checkPagePermission("~\ReportForm\frmCOGS.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Cost of Good Sold"
        If Not Page.IsPostBack Then
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitAllDDL()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                mpeListMat.Show()
            End If
        End If
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : InitDDLCat1()
        cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
        Session("TblListMat") = Nothing : Session("TblListMatView") = Nothing : gvListMat.DataSource = Session("TblListMat") : gvListMat.DataBind()
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Protected Sub btnClearMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearMat.Click
        FilterMaterial.Text = ""
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblListMat") Is Nothing Then
            BindListMat()
        End If
        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                Dim dv As DataView = dt.DefaultView
                Dim sFilter As String = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
                If cbCat04.Checked And DDLCat04.SelectedValue <> "" Then
                    sFilter &= " AND matcode LIKE '" & DDLCat04.SelectedValue & "%'"
                Else
                    If cbCat03.Checked And DDLCat03.SelectedValue <> "" Then
                        sFilter &= " AND matcode LIKE '" & DDLCat03.SelectedValue & "%'"
                    Else
                        If cbCat02.Checked And DDLCat02.SelectedValue <> "" Then
                            sFilter &= " AND matcode LIKE '" & DDLCat02.SelectedValue & "%'"
                        Else
                            If cbCat01.Checked And DDLCat01.SelectedValue <> "" Then
                                sFilter &= " AND matcode LIKE '" & DDLCat01.SelectedValue & "%'"
                            End If
                        End If
                    End If
                End If
                dv.RowFilter = sFilter
                Session("TblListMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblListMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        If Session("TblListMat") Is Nothing Then
            BindListMat()
        End If
        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : InitDDLCat1()
                cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
                Session("TblListMatView") = Session("TblListMat")
                gvListMat.DataSource = Session("TblListMatView")
                gvListMat.DataBind()
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub DDLCat01_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLCat01.SelectedIndexChanged
        InitDDLCat2()
        mpeListMat.Show()
    End Sub

    Protected Sub DDLCat02_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLCat02.SelectedIndexChanged
        InitDDLCat3()
        mpeListMat.Show()
    End Sub

    Protected Sub DDLCat03_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLCat03.SelectedIndexChanged
        InitDDLCat4()
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        UpdateCheckedMat()
        gvListMat.PageIndex = e.NewPageIndex
        gvListMat.DataSource = Session("TblListMatView")
        gvListMat.DataBind()
        mpeListMat.Show()
    End Sub

    Protected Sub cbHdrLM_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For C1 As Integer = 0 To gvListMat.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        mpeListMat.Show()
    End Sub

    Protected Sub lbAddToListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListMat.Click
        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "checkvalue='True'"
                If dv.Count > 0 Then
                    For C1 As Integer = 0 To dv.Count - 1
                        FilterMaterial.Text &= dv(C1)("matcode").ToString & ";"
                    Next
                    FilterMaterial.Text = Left(FilterMaterial.Text, FilterMaterial.Text.Length - 1)
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                End If
            Else
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmCOGS.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not report Is Nothing Then
                If report.IsLoaded Then
                    report.Dispose()
                    report.Close()
                End If
            End If
        Catch ex As Exception
            report.Dispose()
            report.Close()
        End Try
    End Sub
#End Region

End Class
