Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_WOResult
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function 'OK

    Private Function UpdateCheckedSO() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSO") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSO")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListSO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "soitemmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblSO") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedSO2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSO")
            Dim dtTbl2 As DataTable = Session("TblSOView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListSO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "soitemmstoid=" & cbOid
                                dtView2.RowFilter = "soitemmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblSO") = dtTbl
                Session("TblSOView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function
    
    Private Function UpdateCheckedKIK() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListResult.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListResult.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "womstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedKIK2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListResult.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListResult.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "womstoid=" & cbOid
                                dtView2.RowFilter = "womstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub 'OK

    Private Sub InitDept()
        ' DDL Department
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE activeflag='ACTIVE' AND cmpcode='" & FilterDDLDiv.SelectedValue & "'"
        'If cbKIKNo.Checked Then
        '    If womstoid.Text <> "" Then
        '        sSql &= " AND deptoid IN (SELECT DISTINCT deptoid FROM QL_trnwomst WHERE cmpcode='" & FilterDDLDiv.SelectedValue & "' AND womstoid=" & womstoid.Text & ")"
        '    End If
        'End If
        'FillDDL(FilterDDLDept, sSql)
    End Sub 'OK

    Private Sub BindListSO()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, som.somstoid AS soitemmstoid, som.sono AS soitemno, som.sodate AS soitemdate, CONVERT(VARCHAR(10), som.sodate, 101) AS sodate, som.somststatus AS soitemmststatus, som.somstnote AS soitemmstnote FROM QL_trnsomst som"
        If FilterDDLDiv.SelectedValue <> "ALL" Then
            sSql &= " WHERE som.somstoid IN (SELECT wod1.soitemmstoid from QL_trnwodtl1 wod1 INNER JOIN QL_trnwomst wom ON wom.womstoid=wod1.womstoid WHERE wom.cmpcode='" & FilterDDLDiv.SelectedValue & "' "
        Else
            sSql &= " WHERE som.somstoid IN (SELECT wod1.soitemmstoid from QL_trnwodtl1 wod1 INNER JOIN QL_trnwomst wom ON wom.womstoid=wod1.womstoid WHERE wom.cmpcode LIKE '%' "
        End If

        If DDLDivision.SelectedValue <> "ALL" Then
            sSql &= " AND som.groupoid=" & DDLDivision.SelectedValue
        End If

        If FilterDDLDiv.SelectedValue <> "ALL" Then
            sSql &= "AND wom.cmpcode='" & FilterDDLDiv.SelectedValue & "'"
        Else
            sSql &= "AND wom.cmpcode LIKE '%%'"
        End If

        If FilterDDLStatus.SelectedValue = "In Process" Then
            sSql &= " AND wom.womststatus IN ('In Process')"
        ElseIf FilterDDLStatus.SelectedValue = "Post" Then
            sSql &= " AND wom.womststatus IN ('Post','Closed')"
        ElseIf FilterDDLStatus.SelectedValue = "Cancel" Then
            sSql &= " AND wom.womststatus IN ('Cancel')"
        End If

        If IsValidPeriod() Then
            sSql &= " AND wom.wodate>='" & FilterPeriod1.Text & " 00:00:00' AND wom.wodate<='" & FilterPeriod2.Text & " 23:59:59'"
        Else
            Exit Sub
        End If

        sSql &= ") ORDER BY som.sodate DESC, som.somstoid DESC"
        Session("TblSO") = cKon.ambiltabel(sSql, "QL_soitemmst")
    End Sub

    Private Sub BindListResult()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, pr.womstoid AS womstoid, pr.wono AS wono, CONVERT(VARCHAR(10), pr.wodate, 101) AS wodate, pr.womstnote AS womstnote FROM QL_trnwomst pr "
        sSql &= " WHERE " & DDLFilterListResult.SelectedValue & " LIKE '%" & Tchar(txtFilterListResult.Text) & "%'"

        'If FilterDDLDiv.SelectedValue.ToUpper = "ALL" Then
        'sSql &= ""
        'Else
        sSql &= " AND pr.cmpcode='" & FilterDDLDiv.SelectedValue & "'"
        'End If

        If DDLDivision.SelectedValue <> "ALL" Then
            sSql &= " AND pr.womstoid IN (SELECT wod1.womstoid FROM QL_trnwodtl1 wod1 INNER JOIN QL_trnsoitemmst sofg ON sofg.soitemmstoid = wod1.soitemmstoid AND sofg.cmpcode = wod1.cmpcode WHERE wod1.cmpcode = pr.cmpcode AND pr.womstoid = wod1.womstoid AND sofg.groupoid=" & DDLDivision.SelectedValue
            If sono.Text <> "" Then
                Dim scode() As String = Split(sono.Text, ";")
                sSql &= " AND ("
                For c1 As Integer = 0 To scode.Length - 1
                    sSql &= " sofg.soitemno = '" & Tchar(scode(c1)) & "'"
                    If c1 < scode.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            End If
            sSql &= ") "
        End If


        If IsValidPeriod() Then
            sSql &= " AND pr.wodate>='" & FilterPeriod1.Text & " 00:00:00' AND pr.wodate<='" & FilterPeriod2.Text & " 23:59:59'"
        Else
            Exit Sub
        End If

        If FilterDDLStatus.SelectedValue.ToUpper <> "ALL" Then
            sSql &= " AND pr.womststatus LIKE '%" & FilterDDLStatus.SelectedValue & "%'"
        End If

        sSql &= " ORDER BY pr.womstoid DESC"
        'FillGV(gvListResult, sSql, "QL_trnwomst")
        Session("TblMat") = cKon.ambiltabel(sSql, "QL_trnwomst")
    End Sub 'OK

    Private Function FillDDLWithALL(ByRef oDDLObject As DropDownList, ByVal sSql As String) As Boolean
        ' To fill data in dropdown list, return true when all data exist
        FillDDLWithALL = True
        oDDLObject.Items.Clear()
        oDDLObject.Items.Add("ALL")
        oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = "ALL"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            oDDLObject.Items.Add(xreader.GetValue(1))
            oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()
        ' Cek if all data exits
        If oDDLObject.Items.Count = 0 Then
            FillDDLWithALL = False
        End If
        Return FillDDLWithALL
    End Function

    Private Sub InitAllDDL()
        ' DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(FilterDDLDiv, sSql)
        If FilterDDLDiv.Items.Count > 0 Then
            InitDDLDivision(FilterDDLDiv.SelectedValue)
        End If
    End Sub

    Private Sub InitDDLDivision(ByVal sDiv As String)
        ' Init DDL Division
        sSql = "SELECT groupoid, groupcode+' - '+groupdesc FROM QL_mstdeptgroup WHERE activeflag='ACTIVE'"
        If sDiv <> "ALL" Then
            sSql &= " AND cmpcode='" & sDiv & "'"
            If FillDDLWithALL(DDLDivision, sSql) Then
                InitDDLDept()
            End If
        End If
       
    End Sub

    Private Sub InitDDLDept()
        ' Init DDL Department
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE cmpcode='" & FilterDDLDiv.SelectedValue & "' AND activeflag='ACTIVE'"
        If DDLDivision.SelectedValue <> "ALL" Then
            sSql &= " AND deptoid IN (SELECT deptoid FROM QL_mstdeptgroupdtl WHERE groupoid=" & DDLDivision.SelectedValue & ")"
        End If
        FillDDL(deptoid, sSql)
        If DDLDept.SelectedValue.ToUpper = "TO" Then
            deptoid.Items.Add("END")
            deptoid.Items.Item(deptoid.Items.Count - 1).Value = "END"
        End If
    End Sub

    Private Sub SetEnable(ByVal bValue As Boolean)
        FilterDDLDiv.Enabled = bValue
        FilterPeriod1.Enabled = bValue
        FilterPeriod2.Enabled = bValue
        CalPeriod1.Enabled = bValue
        CalPeriod2.Enabled = bValue
        cbKIKNo.Enabled = bValue
    End Sub 'OK

    Private Sub SetEnable2(ByVal bValue As Boolean)
        If Not cbKIKNo.Checked Then
            FilterDDLDiv.Enabled = bValue
            FilterPeriod1.Enabled = bValue
            FilterPeriod2.Enabled = bValue
            CalPeriod1.Enabled = bValue
            CalPeriod2.Enabled = bValue
            cbKIKNo.Enabled = bValue
        End If
        imbFindResult.Enabled = bValue
        imbEraseResult.Enabled = bValue

    End Sub 'OK

    Private Sub ShowReport(ByVal sType As String)
        If FilterDDLDiv.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        Try
            Dim sWhere As String = "WHERE 1=1"

            'If FilterDDLDiv.SelectedValue <> "ALL" Then
            sWhere &= " AND [CMP Code]='" & FilterDDLDiv.SelectedValue & "'"
            'End If

            If DDLDivision.SelectedValue <> "ALL" Then
                sWhere &= " AND [Group Oid]=" & DDLDivision.SelectedValue
            End If

            'Filter From To Department
            If cbDept.Checked = True Then
                If DDLDept.SelectedValue.ToUpper = "FROM" Then
                    sWhere &= " AND [Dept Oid]=" & deptoid.SelectedValue & ""
                Else
                    If deptoid.SelectedValue.ToUpper = "END" Then
                        sWhere &= " AND [ToDept]='END' "
                    Else
                        sWhere &= " AND [To Dept Oid]=" & deptoid.SelectedValue & " "
                    End If

                End If
            End If

            If sono.Text <> "" Then
                Dim scode() As String = Split(sono.Text, ";")
                sWhere &= " AND ("
                For c1 As Integer = 0 To scode.Length - 1
                    sWhere &= " [SO_No] = '" & Tchar(scode(c1)) & "'"
                    If c1 < scode.Length - 1 Then
                        sWhere &= " OR "
                    End If
                Next
                sWhere &= ")"
            End If

            Dim rptName As String = ""
            If ddlType.SelectedValue = "Summary" Then
                If sType = "Print Excel" Then
                    report.Load(Server.MapPath(folderReport & "rptJobCostMOResSum.rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptJobCostMOResSum.rpt"))
                End If
                rptName = "JobCostingMOResultSumStatus_"
            ElseIf ddlType.SelectedValue = "Detail" Then
                If sType = "Print Excel" Then
                    report.Load(Server.MapPath(folderReport & "rptJobCostMOResDtl.rpt"))
                Else
                    If DDLGrouping.SelectedValue.ToUpper = "KIK" Then
                        report.Load(Server.MapPath(folderReport & "rptJobCostMOResDtl.rpt"))
                    Else
                        report.Load(Server.MapPath(folderReport & "rptJobCostMOResDtlDept.rpt"))
                    End If
                End If
                rptName = "JobCostingMOResultDtlStatus_"
            End If
            If IsValidPeriod() Then
                sWhere &= " AND [KIK_Date]>='" & FilterPeriod1.Text & " 00:00:00' AND [KIK_Date]<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If

            If cbKIKNo.Checked Then
                'If womstoid.Text <> "" Then
                '    sWhere &= " AND pr.womstoid=" & womstoid.Text
                'Else
                '    showMessage("Please select " & cbKIKNo.Text & " data first!", 2)
                '    Exit Sub
                'End If
                If wono.Text <> "" Then
                    Dim sResNo() As String = Split(wono.Text, ";")
                    sWhere &= " AND ("
                    For c1 As Integer = 0 To sResNo.Length - 1
                        sWhere &= " [KIK_No] = '" & Tchar(sResNo(c1)) & "'"
                        If c1 < sResNo.Length - 1 Then
                            sWhere &= " OR"
                        End If
                    Next
                    sWhere &= ")"
                End If
            End If

            If FilterDDLStatus.SelectedValue.ToUpper <> "ALL" Then
                sWhere &= " AND [Status] LIKE '%" & FilterDDLStatus.SelectedValue & "%'"
            End If
            sWhere &= " ORDER BY [Seq] , [KIK_No]"
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)
            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, rptName & Format(GetServerTime(), "yyyyMMddHHmmss"))
                report.Close()
                report.Dispose()
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, rptName & Format(GetServerTime(), "yyyyMMddHHmmss"))
                report.Close()
                report.Dispose()
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub 'OK
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        'Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmWOResult.aspx")
        End If
        If checkPagePermission("~\ReportForm\frmWOResult.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Job Costing MO Result Status"
        If Not Page.IsPostBack Then
            InitAllDDL()
            ddlType_SelectedIndexChanged(Nothing, Nothing)
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        End If
    End Sub 'OK

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListResult, PanelListResult, mpeListResult, True)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListResult, PanelListResult, mpeListResult, True)
            End If
        End If
        If Not Session("EmptyListSO") Is Nothing And Session("EmptyListSO") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListSO") Then
                Session("EmptyListSO") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
            End If
        End If
        If Not Session("WarningListSO") Is Nothing And Session("WarningListSO") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListSO") Then
                Session("WarningListSO") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
            End If
        End If

    End Sub 'OK

    Protected Sub FilterDDLDiv_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLDiv.SelectedIndexChanged
        InitDDLDivision(FilterDDLDiv.SelectedValue)
    End Sub 'OK

    Protected Sub DDLDivision_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLDivision.SelectedIndexChanged
        InitDDLDept()
    End Sub

    Protected Sub DDLDept_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLDept.SelectedIndexChanged
        InitDDLDept()
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        If ddlType.SelectedValue = "Detail" Then
            DDLGrouping.Visible = True
            lblg.Visible = True
            lblGrouping.Visible = True
            DDLGrouping.Items.Clear()
            DDLGrouping.Items.Add("KIK")
            DDLGrouping.Items.Item(DDLGrouping.Items.Count - 1).Value = "kik"
            DDLGrouping.Items.Add("From Department")
            DDLGrouping.Items.Item(DDLGrouping.Items.Count - 1).Value = "dept"
        Else
            DDLGrouping.Visible = False
            lblg.Visible = False
            lblGrouping.Visible = False
        End If
    End Sub

    Protected Sub cbKIKNo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbKIKNo.CheckedChanged
        If cbKIKNo.Checked Then
            imbFindResult.Visible = True
            imbEraseResult.Visible = True
        Else
            imbFindResult.Visible = False
            imbEraseResult.Visible = False
        End If
    End Sub 'OK

    Protected Sub imbFindSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindSO.Click
        If IsValidPeriod() Then
            DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
            Session("TblSO") = Nothing : Session("TblSOView") = Nothing
            gvListSO.DataSource = Nothing : gvListSO.DataBind()
            cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbEraseSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseSO.Click
        sono.Text = ""
        FilterDDLDiv.Enabled = True
    End Sub

    Protected Sub btnFindListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSO.Click
        If Session("TblSO") Is Nothing Then
            BindListSO()
            If Session("TblSO").Rows.Count <= 0 Then
                Session("EmptyListSO") = "SO data can't be found!"
                showMessage(Session("EmptyListSO"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListSO.SelectedValue & " LIKE '%" & Tchar(txtFilterListSO.Text) & "%'"
        If UpdateCheckedSO() Then
            Dim dv As DataView = Session("TblSO").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblSOView") = dv.ToTable
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                dv.RowFilter = ""
                mpeListSO.Show()
            Else
                dv.RowFilter = ""
                Session("TblSOView") = Nothing
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                Session("WarningListSO") = "SO data can't be found!"
                showMessage(Session("WarningListSO"), 2)
            End If
        Else
            mpeListSO.Show()
        End If
    End Sub

    Protected Sub btnViewAllListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListSO.Click
        DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
        If Session("TblSO") Is Nothing Then
            BindListSO()
            If Session("TblSO").Rows.Count <= 0 Then
                Session("EmptyListSO") = "SO data can't be found!"
                showMessage(Session("EmptyListSO"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedSO() Then
            Dim dt As DataTable = Session("TblSO")
            Session("TblSOView") = dt
            gvListSO.DataSource = Session("TblSOView")
            gvListSO.DataBind()
        End If
        mpeListSO.Show()
    End Sub

    Protected Sub btnSelectAllSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllSO.Click
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "soitemmstoid=" & dtTbl.Rows(C1)("soitemmstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSO") = objTbl
                Session("TblSOView") = dtTbl
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
            End If
            mpeListSO.Show()
        Else
            Session("WarningListSO") = "Please show some SO data first!"
            showMessage(Session("WarningListSO"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneSO.Click
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "soitemmstoid=" & dtTbl.Rows(C1)("soitemmstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSO") = objTbl
                Session("TblSOView") = dtTbl
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
            End If
            mpeListSO.Show()
        Else
            Session("WarningListSO") = "Please show some SO data first!"
            showMessage(Session("WarningListSO"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedSO.Click
        If Session("TblSO") Is Nothing Then
            Session("WarningListSO") = "Selected SO data can't be found!"
            showMessage(Session("WarningListSO"), 2)
            Exit Sub
        End If
        If UpdateCheckedSO() Then
            Dim dtTbl As DataTable = Session("TblSO")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
                Session("TblSOView") = dtView.ToTable
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                dtView.RowFilter = ""
                mpeListSO.Show()
            Else
                dtView.RowFilter = ""
                Session("TblSOView") = Nothing
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                Session("WarningListSO") = "Selected SO data can't be found!"
                showMessage(Session("WarningListSO"), 2)
            End If
        Else
            mpeListSO.Show()
        End If
    End Sub

    Protected Sub gvListSO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSO.PageIndexChanging
        If UpdateCheckedSO2() Then
            gvListSO.PageIndex = e.NewPageIndex
            gvListSO.DataSource = Session("TblSOView")
            gvListSO.DataBind()
        End If
        mpeListSO.Show()
    End Sub 'OK

    Protected Sub lkbAddToListListSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListSO.Click
        If Not Session("TblSO") Is Nothing Then
            If UpdateCheckedSO() Then
                Dim dtTbl As DataTable = Session("TblSO")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If sono.Text <> "" Then
                            If dtView(C1)("soitemno") <> "" Then
                                sono.Text &= ";" + vbCrLf + dtView(C1)("soitemno")
                            End If
                        Else
                            If dtView(C1)("soitemno") <> "" Then
                                sono.Text &= dtView(C1)("soitemno")
                            End If
                        End If
                    Next
                    dtView.RowFilter = ""
                    FilterDDLDiv.Enabled = False
                    cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, False)
                Else
                    Session("WarningListSO") = "Please select SO to add to list!"
                    showMessage(Session("WarningListSO"), 2)
                    Exit Sub
                End If
            Else
                Session("WarningListSO") = "Please show some SO data first!"
                showMessage(Session("WarningListSO"), 2)
                Exit Sub
            End If
        Else
            Session("WarningListSO") = "Please show some SO data first!"
            showMessage(Session("WarningListSO"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListSO.Click
        cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, False)
    End Sub

    Protected Sub imbFindResult_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindResult.Click
        'If FilterDDLDiv.SelectedValue = "" Then
        '    showMessage("Please select Business Unit first!", 2)
        '    Exit Sub
        'End If
        'BindListResult()
        'cProc.SetModalPopUpExtender(btnHiddenListResult, PanelListResult, mpeListResult, True)
        If IsValidPeriod() Then
            DDLFilterListResult.SelectedIndex = -1 : txtFilterListResult.Text = ""
            Session("TblMat") = Nothing : Session("TblMatView") = Nothing : gvListResult.DataSource = Nothing : gvListResult.DataBind()
            cProc.SetModalPopUpExtender(btnHiddenListResult, PanelListResult, mpeListResult, True)
        Else
            Exit Sub
        End If
    End Sub 'OK

    Protected Sub imbEraseResult_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseResult.Click
        'womstoid.Text = ""
        'wono.Text = ""
        'SetEnable(True)
        'cbKIKNo.Text = "Result No."
        'cbKIKNo.Checked = False
        'cbKIKNo_CheckedChanged(Nothing, Nothing)
        'InitDept()
        wono.Text = ""
    End Sub 'OK

    Protected Sub btnFindListResult_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListResult.Click
        'BindListResult()
        'mpeListResult.Show()
        If Session("TblMat") Is Nothing Then
            BindListResult()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "KIK data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListResult.SelectedValue & " LIKE '%" & Tchar(txtFilterListResult.Text) & "%'"
        If UpdateCheckedKIK() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListResult.DataSource = Session("TblMatView")
                gvListResult.DataBind()
                dv.RowFilter = ""
                mpeListResult.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListResult.DataSource = Session("TblMatView")
                gvListResult.DataBind()
                Session("WarningListMat") = "KIK data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListResult.Show()
        End If
    End Sub 'OK

    Protected Sub btnViewAllListResult_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListResult.Click
        'txtFilterListResult.Text = ""
        'DDLFilterListResult.SelectedIndex = -1
        'BindListResult()
        'mpeListResult.Show()
        DDLFilterListResult.SelectedIndex = -1 : txtFilterListResult.Text = ""
        If Session("TblMat") Is Nothing Then
            BindListResult()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "KIK data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedKIK() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListResult.DataSource = Session("TblMatView")
            gvListResult.DataBind()
        End If
        mpeListResult.Show()
    End Sub

    Protected Sub gvListResult_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListResult.PageIndexChanging
        If UpdateCheckedKIK2() Then
            gvListResult.PageIndex = e.NewPageIndex
            gvListResult.DataSource = Session("TblMatView")
            gvListResult.DataBind()
        End If
        mpeListResult.Show()
    End Sub 'OK



    Protected Sub gvListResult_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListResult.SelectedIndexChanged
        'If gvListResult.SelectedDataKey.Item("wono").ToString <> "" Then
        '    wono.Text = gvListResult.SelectedDataKey.Item("wono").ToString
        '    cbKIKNo.Text = "Result No."
        'Else
        '    wono.Text = gvListResult.SelectedDataKey.Item("womstoid").ToString
        '    cbKIKNo.Text = "Draft No."
        'End If
        'womstoid.Text = gvListResult.SelectedDataKey.Item("womstoid").ToString
        'SetEnable(False)
        'InitDept()
        'cProc.SetModalPopUpExtender(btnHiddenListResult, PanelListResult, mpeListResult, False)
    End Sub 'OK

    Protected Sub lkbListResult_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListResult.Click
        cProc.SetModalPopUpExtender(btnHiddenListResult, PanelListResult, mpeListResult, False)
    End Sub 'OK

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub 'OK

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub 'OK

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub 'OK

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmWOResult.aspx?awal=true")
    End Sub 'OK

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub 'OK

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub 'OK

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub 'OK

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        If Not report Is Nothing Then
            If report.IsLoaded Then
                report.Dispose()
                report.Close()
            End If
        End If
    End Sub 'OK
#End Region

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedKIK() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If wono.Text <> "" Then
                            wono.Text &= ";" + vbCrLf + dtView(C1)("wono")
                        Else
                            wono.Text = dtView(C1)("wono")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHiddenListResult, PanelListResult, mpeListResult, False)
                Else
                    Session("WarningListMat") = "Please select Result to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some Result data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

End Class
