Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_incomingGiroReal
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function FillDDLWithALL(ByRef oDDLObject As DropDownList, ByVal sSql As String) As Boolean
        ' To fill data in dropdown list, return true when all data exist
        FillDDLWithALL = True
        oDDLObject.Items.Clear()
        oDDLObject.Items.Add("ALL")
        oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = "ALL"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            oDDLObject.Items.Add(xreader.GetValue(1))
            oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()
        ' Cek if all data exits
        If oDDLObject.Items.Count = 0 Then
            FillDDLWithALL = False
        End If
        Return FillDDLWithALL
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    'Private Sub InitCBL()
    '    sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='VAR_BANK' AND interfaceres1='" & DDLBusUnit.SelectedValue & "'"
    '    sSql = GetStrData(sSql)
    '    sSql = "SELECT acctgcode, ('(' + acctgcode + ') ' + acctgdesc) AS acctgdesc FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' ORDER BY acctgcode "
    '    cbldata.Items.Clear()
    '    cbldata.Items.Add("All")
    '    cbldata.Items(cbldata.Items.Count - 1).Value = "All"
    '    If conn.State = ConnectionState.Closed Then
    '        conn.Open()
    '    End If
    '    xCmd = New SqlCommand(sSql, conn)
    '    xreader = xCmd.ExecuteReader
    '    While xreader.Read
    '        cbldata.Items.Add(xreader.GetValue(1).ToString)
    '        cbldata.Items(cbldata.Items.Count - 1).Value = xreader.GetValue(0).ToString
    '    End While
    '    xreader.Close()
    '    conn.Close()
    '    Session("LastAllCheck") = False
    'End Sub

    Private Sub ShowReport(ByVal sType As String)
        Dim sWhere As String = ""
        Dim rptName As String = ""
        Dim Dtl As String = ""
        Dim Join As String = ""
        'Gae Njukuk Multi Check Box
        'Dim sValue As String = ""
        'For C1 As Integer = 0 To cbldata.Items.Count - 1
        '    If cbldata.Items(C1).Value <> "All" And cbldata.Items(C1).Selected = True Then
        '        sValue &= cbldata.Items(C1).Value & ","
        '    End If
        'Next
        'If sValue <> "" Then
        '    sValue = Left(sValue, sValue.Length - 1)
        'End If
        '" cb.acctgoid IN (" & sValue & ") "
        'If cbCOA.Checked = True Then
        '    If ddlCOA.Items.Count <= 0 Then
        '        showMessage("Please choose Bussiness Unit First ! If COA's not available, please Setup BANK COA for Selected Bussiness Unit !", 2)
        '        Exit Sub
        '    End If
        'End If

        Try
            report.Load(Server.MapPath(folderReport & "rptGiroInReal.rpt"))
            rptName = "GiroRealizationReport"

            '====Tambahan from ANDIKA for Date Selection====
            If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
                If IsValidPeriod() Then
                    sWhere &= " WHERE cb.cashbankdate >= '" & FilterPeriod1.Text & " 00:00:00' AND cb.cashbankdate <= '" & FilterPeriod2.Text & " 23:59:59'"
                Else
                    Exit Sub
                End If
            End If
            '====End of Tambahan from ANDIKA==== 

            'If DDLBusUnit.SelectedValue.ToUpper <> "ALL" Then
            sWhere &= " AND cb.cmpcode = '" & DDLBusUnit.SelectedValue & "'"
            'End If

            If checkPagePermission("~\ReportForm\frmGiroInReal.aspx", Session("SpecialAccess")) = False Then
                sWhere &= " AND cb.createuser='" & Session("UserID") & "'"
            End If

            If cashbankno.Text.Trim <> "" Then
                If DDLCashBankNo.SelectedValue = "Cash/Bank No." Then
                    Dim sCashBankno() As String = Split(cashbankno.Text, ";")
                    sWhere &= " AND ("
                    For c1 As Integer = 0 To sCashBankno.Length - 1
                        sWhere &= " cb.cashbankno LIKE '%" & Tchar(sCashBankno(c1)) & "%'"
                        If c1 < sCashBankno.Length - 1 Then
                            sWhere &= " OR"
                        End If
                    Next
                    sWhere &= ")"
                Else
                    Dim sCashBankno() As String = Split(cashbankno.Text, ";")
                    sWhere &= " AND ("
                    For c1 As Integer = 0 To sCashBankno.Length - 1
                        sWhere &= " cbref.cashbankno LIKE '%" & Tchar(sCashBankno(c1)) & "%'"
                        If c1 < sCashBankno.Length - 1 Then
                            sWhere &= " OR"
                        End If
                    Next
                    sWhere &= ")"
                End If
            End If

            If cbCOA.Checked Then
                If ddlCOA.Items.Count > 0 Then
                    sWhere &= " AND cb.acctgoid = " & ddlCOA.SelectedValue
                End If
            End If

            'report.SetDataSource(report)
            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", Session("UserID"))
            cProc.SetDBLogonForReport(report)

            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, rptName)
                report.Close()
                report.Dispose()
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, rptName)
                report.Close()
                report.Dispose()
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Private Sub InitAllDDL()
        ' DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)

    End Sub

    Private Sub InitCOA()
        ' Fill DDL Negotiating Bank
        FillDDLAcctg(ddlCOA, "VAR_BANK", DDLBusUnit.SelectedValue)
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmGiroInReal.aspx")

        End If

        If checkPagePermission("~\ReportForm\frmGiroInReal.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        Page.Title = CompnyName & " - Outgoing Giro Realization Report"
        If Not Page.IsPostBack Then
            InitAllDDL()
            DDLBusUnit_SelectedIndexChanged(Nothing, Nothing)
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitCOA()
    End Sub

    Protected Sub DDLCashBankNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLCashBankNo.SelectedIndexChanged
        cashbankno.Text = ""
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmGiroInReal.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        If Not report Is Nothing Then
            If report.IsLoaded Then
                report.Dispose()
                report.Close()
            End If
        End If
    End Sub
#End Region

End Class
