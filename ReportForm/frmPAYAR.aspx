<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="frmPAYAR.aspx.vb" Inherits="ReportForm_frmPAYAR" title="" EnableEventValidation="false" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Account Receiveable Report" CssClass="Title" ForeColor="SaddleBrown"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <table style="width: 100%">
                                <tr>
                                    <td align="center" valign="top" char="Label">
                                        <table>
                                            <tr>
                                                <td align="center" class="Label" valign="top">
                                                    <asp:UpdatePanel ID="upReportForm" runat="server">
                                                        <ContentTemplate>
<asp:Panel id="pnlReportForm" runat="server" Width="100%" DefaultButton="btnSearchSupplier"><TABLE><TBODY><TR><TD align=left><asp:Label id="Label5" runat="server" Text="Bussiness Unit"></asp:Label></TD><TD class="Label" align=center>:</TD><TD align=left><asp:DropDownList id="FilterDDLDiv" runat="server" CssClass="inpText" AutoPostBack="True">
            </asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label7" runat="server" Text="Type"></asp:Label></TD><TD class="Label" align=center>:</TD><TD align=left><asp:DropDownList id="FilterType" runat="server" CssClass="inpText" AutoPostBack="True"><asp:ListItem>SUMMARY</asp:ListItem>
<asp:ListItem>DETAIL</asp:ListItem>
<asp:ListItem>EKSTERNAL</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label3" runat="server" Text="Period"></asp:Label></TD><TD class="Label" align=center>:</TD><TD align=left><asp:DropDownList id="FilterDDLMonth" runat="server" CssClass="inpText" Width="100px" Visible="False"></asp:DropDownList><asp:DropDownList id="FilterDDLYear" runat="server" CssClass="inpText" Width="75px" Visible="False"></asp:DropDownList><asp:TextBox id="txtStart" runat="server" CssClass="inpText" Width="60px"></asp:TextBox> <asp:ImageButton id="imbStart" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="lblTo" runat="server" Text="to"></asp:Label> <asp:TextBox id="txtFinish" runat="server" CssClass="inpText" Width="60px"></asp:TextBox> <asp:ImageButton id="imbFinish" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="lblMMDD" runat="server" CssClass="Important" Text="(MM/DD/yyyy)"></asp:Label></TD></TR><TR><TD align=left></TD><TD class="Label" align=center></TD><TD align=left><asp:CheckBox id="cbDueDateGiro" runat="server" Text="Due Date Giro" Checked="True" __designer:wfdid="w2"></asp:CheckBox></TD></TR><TR><TD align=left><asp:DropDownList id="DDLCustomer" runat="server" CssClass="inpText" Width="136px" AutoPostBack="True" OnSelectedIndexChanged="DDLCustomer_SelectedIndexChanged">
        <asp:ListItem>CUSTOMER</asp:ListItem>
        <asp:ListItem>CUSTOMER GROUP</asp:ListItem>
    </asp:DropDownList></TD><TD class="Label" align=center>:</TD><TD align=left><TABLE cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD align=left><asp:RadioButtonList id="rbSupplier" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True">ALL</asp:ListItem>
                                <asp:ListItem>SELECT</asp:ListItem>
                            </asp:RadioButtonList></TD><TD align=left><asp:TextBox id="FilterTextSupplier" runat="server" CssClass="inpText" Width="175px" Visible="False"></asp:TextBox> <asp:ImageButton id="btnSearchSupplier" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnClearSupplier" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD></TR></TBODY></TABLE></TD></TR><TR><TD align=left></TD><TD class="Label" align=center></TD><TD align=left><asp:CheckBox id="chkEmpty" runat="server" Text="Hide Customer with 0 A/R amount" Checked="True"></asp:CheckBox></TD></TR><TR><TD align=left><asp:Label id="custoid" runat="server" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD align=left><asp:Panel id="pnlSupplier" runat="server"><asp:GridView id="gvSupplier" runat="server" Visible="False" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
<PagerSettings Mode="NumericFirstLast"></PagerSettings>
<Columns>
<asp:TemplateField><EditItemTemplate>
<asp:CheckBox id="CheckBox1" runat="server"></asp:CheckBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:CheckBox id="chkSelect" runat="server" ToolTip='<%# Eval("custoid") %>' Checked='<%# Eval("selected") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="custcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="False" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label4" runat="server" ForeColor="Red" Text="No data found."></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White"></SelectedRowStyle>

<HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White"></HeaderStyle>
</asp:GridView></asp:Panel></TD></TR><TR><TD align=left></TD><TD class="Label" align=center></TD><TD id="TDFilterCust" align=left runat="server" Visible="false"><asp:ImageButton id="btnSelectAll" onclick="btnSelectAll_Click" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" __designer:wfdid="w2"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNone" onclick="btnSelectNone_Click" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" __designer:wfdid="w3"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewChecked" onclick="btnViewChecked_Click" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" __designer:wfdid="w4"></asp:ImageButton></TD></TR><TR><TD align=left><asp:Label id="lblGroupBy" runat="server" Text="Group By"></asp:Label></TD><TD class="Label" align=center><asp:Label id="lblSepGroupBy" runat="server" Text=":"></asp:Label></TD><TD align=left><asp:DropDownList id="DDLGroupBy" runat="server" CssClass="inpText" Width="176px" AutoPostBack="True" OnSelectedIndexChanged="DDLGroupBy_SelectedIndexChanged"><asp:ListItem Value="Customer">CUSTOMER</asp:ListItem>
<asp:ListItem Value="SALES">SALES</asp:ListItem>
<asp:ListItem>CUSTOMER GROUP</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left><asp:CheckBox id="cbSales" runat="server" Text="Sales"></asp:CheckBox></TD><TD class="Label" align=center><asp:Label id="lblSepSales" runat="server" Text=":"></asp:Label></TD><TD align=left><asp:DropDownList id="DDLSales" runat="server" CssClass="inpText" Width="240px" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label6" runat="server" Text="Currency Value"></asp:Label></TD><TD class="Label" align=center>:</TD><TD align=left><asp:DropDownList id="FilterCurrency" runat="server" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD align=center colSpan=3><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnExportToPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnExportToExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=3><ajaxToolkit:CalendarExtender id="ceStart" runat="server" PopupButtonID="imbStart" Format="MM/dd/yyyy" TargetControlID="txtStart"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeStart" runat="server" TargetControlID="txtStart" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="ceFin" runat="server" PopupButtonID="imbFinish" Format="MM/dd/yyyy" TargetControlID="txtFinish"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeFin" runat="server" TargetControlID="txtFinish" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="HEIGHT: 75px" vAlign=top align=center colSpan=3><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upReportForm">
                                                                                <ProgressTemplate>
                                                                                    <span></span><span style="font-weight: bold; font-size: 10pt; color: purple">
                                                                                        <asp:Image ID="imgLoading" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />
                                                                                        Please Wait .....</span><br />
                                                                                </ProgressTemplate>
                                                                            </asp:UpdateProgress> </TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnViewReport" />
                                                            <asp:PostBackTrigger ControlID="btnExportToPdf" />
                                                            <asp:PostBackTrigger ControlID="btnExportToExcel" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Label" style="height: 10px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="Label" valign="top">
                                                    <CR:CrystalReportViewer ID="crvReportForm" runat="server" AutoDataBind="True" HasCrystalLogo="False"
                                                        HasDrillUpButton="False" HasExportButton="False" HasPrintButton="False" HasToggleGroupTreeButton="False"
                                                        HasViewList="False" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <HeaderTemplate>
                            &nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
        <tr>
            <td align="center">
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>

