Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_ProductionResult
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function 'OK
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub 'OK

    Private Sub InitAllDDL()
        ' Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If FillDDL(FilterDDLBusUnit, sSql) Then
            InitDept()
        End If
    End Sub 'OK

    Private Sub InitDept()
        '' DDL Department
        'sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE activeflag='ACTIVE' AND cmpcode='" & FilterDDLBusUnit.SelectedValue & "' AND deptoid IN (SELECT deptoid FROM QL_trnprodresmst) ORDER BY deptname"
        'FillDDLWithAdditionalText(FilterDDLDept, sSql, "All", "All")
        ' Init DDL Department
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE cmpcode='" & FilterDDLBusUnit.SelectedValue & "' AND activeflag='ACTIVE'"
        FillDDL(FilterDDLDept, sSql)
        If DDLDept.SelectedValue.ToUpper = "TO" Then
            FilterDDLDept.Items.Add("END")
            FilterDDLDept.Items.Item(FilterDDLDept.Items.Count - 1).Value = "END"
        End If
    End Sub 'OK

    Private Sub BindResultData()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, resm.prodresmstoid, prodresno, CONVERT(VARCHAR(10), prodresdate, 101) AS prodresdate, de.deptname FROMDEPT, dx.deptname TODEPT, prodresmstnote FROM QL_trnprodresmst resm INNER JOIN QL_trnprodresdtl resd ON resd.prodresmstoid=resm.prodresmstoid INNER JOIN QL_trnprodresconfirm resc ON resc.cmpcode=resd.cmpcode AND resc.prodresdtloid=resd.prodresdtloid INNER JOIN QL_mstdept dx ON dx.cmpcode=resm.cmpcode AND dx.deptoid=resd.todeptoid INNER JOIN QL_mstdept de ON de.cmpcode=resm.cmpcode AND de.deptoid=resm.deptoid WHERE resm.cmpcode='" & FilterDDLBusUnit.SelectedValue & "'"
        If FilterDDLStatus.SelectedValue <> "ALL" Then
            'Filter Status
            If FilterDDLStatus.SelectedValue.ToUpper = "IN PROCESS" Then
                sSql &= " AND resm.prodresmststatus='" & FilterDDLStatus.SelectedValue & "'"
            ElseIf FilterDDLStatus.SelectedValue.ToUpper = "POST" Then
                sSql &= " AND resm.prodresmststatus IN ('Post','Closed') "
            End If
        End If
        If cbDept.Checked = True Then
            If DDLDept.SelectedValue.ToUpper = "FROM" Then
                sSql &= " AND resm.deptoid=" & FilterDDLDept.SelectedValue & ""
            Else
                If FilterDDLDept.SelectedValue.ToUpper = "END" Then
                    sSql &= " AND resd.prodrestype='FG' "
                Else
                    sSql &= " AND resd.todeptoid=" & FilterDDLDept.SelectedValue & " "
                End If
            End If
        End If
        If FilterDDLType.SelectedValue <> "ALL" Then
            If FilterDDLDept.SelectedValue.ToUpper <> "END" Then
                sSql &= " AND resd.prodrestype='" & FilterDDLType.SelectedValue & "'"
            End If
        End If

        If cbPeriod.Checked Then
            If IsValidPeriod() Then
                sSql &= " AND " & DDLDate.SelectedValue & ">='" & FilterPeriod1.Text & " 00:00:00' AND " & DDLDate.SelectedValue & "<='" & FilterPeriod2.Text & " 23:59:59'"
                'sWhere &= " AND prodresdate>='" & FilterPeriod1.Text & " 00:00:00' AND prodresdate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If

        sSql &= " ORDER BY resm.prodresmstoid"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnprodresmst")
        If dt.Rows.Count > 0 Then
            Session("TblListResult") = dt
            Session("TblListResultView") = dt
            gvListResult.DataSource = dt
            gvListResult.DataBind()
            cProc.SetModalPopUpExtender(btnHideListResult, pnlListResult, mpeListResult, True)
        Else
            showMessage("No Production Result data available for this time!", 2)
        End If
    End Sub 'OK

    Private Sub BindListKIK()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, wom.womstoid, wom.wono, CONVERT(VARCHAR(10), wom.wodate, 101) AS wodate, wom.womststatus, wom.womstnote, som.soitemno FROM QL_trnprodresmst resm INNER JOIN QL_trnprodresdtl resd ON resd.cmpcode=resm.cmpcode AND resd.prodresmstoid=resm.prodresmstoid INNER JOIN QL_trnprodresconfirm resc ON resc.cmpcode=resd.cmpcode AND resc.prodresdtloid=resd.prodresdtloid INNER JOIN QL_trnwomst wom ON wom.cmpcode=resd.cmpcode AND wom.womstoid=resd.womstoid INNER JOIN QL_trnwodtl1 wod ON wod.cmpcode=wom.cmpcode AND wod.womstoid=wom.womstoid INNER JOIN QL_trnsoitemmst som ON wod.cmpcode=som.cmpcode AND wod.soitemmstoid=som.soitemmstoid WHERE resm.cmpcode='" & FilterDDLBusUnit.SelectedValue & "'"

        If FilterDDLStatus.SelectedValue <> "ALL" Then
            'Filter Status
            If FilterDDLStatus.SelectedValue.ToUpper = "IN PROCESS" Then
                sSql &= " AND resm.prodresmststatus='" & FilterDDLStatus.SelectedValue & "'"
            ElseIf FilterDDLStatus.SelectedValue.ToUpper = "POST" Then
                sSql &= " AND resm.prodresmststatus IN ('Post','Closed') "
            End If
        End If
        If cbDept.Checked = True Then
            If DDLDept.SelectedValue.ToUpper = "FROM" Then
                sSql &= " AND resm.deptoid=" & FilterDDLDept.SelectedValue & ""
            Else
                If FilterDDLDept.SelectedValue.ToUpper = "END" Then
                    sSql &= " AND resd.prodrestype='FG' "
                Else
                    sSql &= " AND resd.todeptoid=" & FilterDDLDept.SelectedValue & " "
                End If
            End If
        End If
        If FilterDDLType.SelectedValue <> "ALL" Then
            If FilterDDLDept.SelectedValue.ToUpper <> "END" Then
                sSql &= " AND resd.prodrestype='" & FilterDDLType.SelectedValue & "'"
            End If
        End If

        If cbPeriod.Checked Then
            If IsValidPeriod() Then
                sSql &= " AND " & DDLDate.SelectedValue & ">='" & FilterPeriod1.Text & " 00:00:00' AND " & DDLDate.SelectedValue & "<='" & FilterPeriod2.Text & " 23:59:59'"
                'sWhere &= " AND prodresdate>='" & FilterPeriod1.Text & " 00:00:00' AND prodresdate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If

        sSql &= " ORDER BY wom.wodate DESC, wom.womstoid DESC"
        Session("TblKIK") = cKon.ambiltabel(sSql, "QL_trnwomst")
    End Sub

    Private Sub UpdateCheckedResult()
        If Session("TblListResult") IsNot Nothing Then
            For C1 As Integer = 0 To gvListResult.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListResult.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            Session("TblListResult").DefaultView.RowFilter = "prodresmstoid=" & CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                            If Session("TblListResult").DefaultView.Count > 0 Then
                                If CType(myControl, System.Web.UI.WebControls.CheckBox).Checked Then
                                    Session("TblListResult").DefaultView(0)("checkvalue") = "True"
                                Else
                                    Session("TblListResult").DefaultView(0)("checkvalue") = "False"
                                End If
                            End If
                            Session("TblListResult").DefaultView.RowFilter = ""
                        End If
                    Next
                End If
            Next
        End If
        If Session("TblListResultView") IsNot Nothing Then
            For C1 As Integer = 0 To gvListResult.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListResult.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            Session("TblListResultView").DefaultView.RowFilter = "prodresmstoid=" & CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                            If Session("TblListResultView").DefaultView.Count > 0 Then
                                If CType(myControl, System.Web.UI.WebControls.CheckBox).Checked Then
                                    Session("TblListResultView").DefaultView(0)("checkvalue") = "True"
                                Else
                                    Session("TblListResultView").DefaultView(0)("checkvalue") = "False"
                                End If
                            End If
                            Session("TblListResultView").DefaultView.RowFilter = ""
                        End If
                    Next
                End If
            Next
        End If
    End Sub 'OK

    Private Function UpdateCheckedKIK() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblKIK") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblKIK")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListKIK.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListKIK.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "womstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblKIK") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedKIK2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblKIKView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblKIK")
            Dim dtTbl2 As DataTable = Session("TblKIKView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListKIK.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListKIK.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "womstoid=" & cbOid
                                dtView2.RowFilter = "womstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblKIK") = dtTbl
                Session("TblKIKView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Sub ShowReport(ByVal sType As String)
        If FilterDDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        Try
            If sType = "Print Excel" Then
                If FilterDDLTypeReport.SelectedValue = "Ginteg" Then
                    report.Load(Server.MapPath(folderReport & "rptProductionConfGintegXls.rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptProductionConf.rpt"))
                End If
            Else
                report.Load(Server.MapPath(folderReport & "rptProductionConf.rpt"))
            End If
            Dim sPeriodStart As String = "", sPeriodEnd As String = ""
            Dim sWhere As String = " WHERE resm.cmpcode='" & FilterDDLBusUnit.SelectedValue & "' AND prodrestype='FG'"
            If FilterDDLStatus.SelectedValue <> "ALL" Then
                'Filter Status
                If FilterDDLStatus.SelectedValue.ToUpper = "IN PROCESS" Then
                    sWhere &= " AND resm.prodresmststatus='" & FilterDDLStatus.SelectedValue & "'"
                ElseIf FilterDDLStatus.SelectedValue.ToUpper = "POST" Then
                    sWhere &= " AND resm.prodresmststatus IN ('Post','Closed') "
                End If
            End If
            If cbPeriod.Checked Then
                If IsValidPeriod() Then
                    sWhere &= " AND " & DDLDate.SelectedValue & ">='" & FilterPeriod1.Text & " 00:00:00' AND " & DDLDate.SelectedValue & "<='" & FilterPeriod2.Text & " 23:59:59'"
                    'sWhere &= " AND prodresdate>='" & FilterPeriod1.Text & " 00:00:00' AND prodresdate<='" & FilterPeriod2.Text & " 23:59:59'"
                    sPeriodStart = Format(CDate(FilterPeriod1.Text), "dd MMM yyyy")
                    sPeriodEnd = Format(CDate(FilterPeriod2.Text), "dd MMM yyyy")
                Else
                    Exit Sub
                End If
            End If
            'If FilterDDLDept.SelectedValue <> "All" Then
            '    sWhere &= " AND resm.deptoid=" & FilterDDLDept.SelectedValue
            'End If
            'Filter From To Department
            If cbDept.Checked = True Then
                If DDLDept.SelectedValue.ToUpper = "FROM" Then
                    sWhere &= " AND resm.deptoid=" & FilterDDLDept.SelectedValue & ""
                    'Else
                    '    If FilterDDLDept.SelectedValue.ToUpper = "END" Then
                    '        sWhere &= " AND prodrestype='FG' "
                    '    Else
                    '        sWhere &= " AND resd.todeptoid=" & FilterDDLDept.SelectedValue & " "
                    '    End If
                End If
            End If
            If FilterDDLType.SelectedValue <> "ALL" Then
                If FilterDDLDept.SelectedValue.ToUpper <> "END" Then
                    sWhere &= " AND prodrestype='" & FilterDDLType.SelectedValue & "'"
                End If
            End If
            If prodresno.Text <> "" Then
                If DDLNo.SelectedValue = "wono" Then
                    Dim sSplit() As String = prodresno.Text.Split(";")
                    If sSplit.Length > 0 Then
                        sWhere &= " AND wom." & DDLNo.SelectedValue & " IN ("
                        For C1 As Integer = 0 To sSplit.Length - 1
                            sWhere &= "'" & sSplit(C1) & "'"
                            If C1 < sSplit.Length - 1 Then
                                sWhere &= ","
                            End If
                        Next
                        sWhere &= ")"
                    End If
                Else
                    Dim sSplit() As String = prodresno.Text.Split(";")
                    If sSplit.Length > 0 Then
                        sWhere &= " AND resm." & DDLNo.SelectedValue & " IN ("
                        For C1 As Integer = 0 To sSplit.Length - 1
                            sWhere &= "'" & sSplit(C1) & "'"
                            If C1 < sSplit.Length - 1 Then
                                sWhere &= ","
                            End If
                        Next
                        sWhere &= ")"
                    End If
                End If
            End If
            Dim sOrderBy As String
            If DDLSorting.SelectedValue = "resm.prodresno" Then
                sOrderBy = " ORDER BY [From Dept. Code] ASC, [Result No.] ASC, [Oid] ASC,  [Result Date] ASC, [KIK No.] ASC "
            ElseIf DDLSorting.SelectedValue = "resm.prodresdate" Then
                sOrderBy = " ORDER BY [From Dept. Code] ASC,  [Result Date] ASC, [Oid] ASC, [Result No.] ASC, [KIK No.] ASC "
            Else
                'Sorting by KIK
                sOrderBy = " ORDER BY [From Dept. Code] ASC, [KIK No.] ASC,  [Result Date] ASC, [Oid] ASC, [Result No.] ASC "
            End If
            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("sOrderBy", sOrderBy)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            report.SetParameterValue("sDate", DDLDate.SelectedItem.Text)
            report.SetParameterValue("sPeriodStart", sPeriodStart)
            report.SetParameterValue("sPeriodEnd", sPeriodEnd)
            report.PrintOptions.PaperSize = PaperSize.PaperFolio
            cProc.SetDBLogonForReport(report)
            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "ProductionResultReport")
                report.Close()
                report.Dispose()
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "ProductionResultReport")
                report.Close()
                report.Dispose()
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub 'OK
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmProdConf.aspx")
        End If
        If checkPagePermission("~\ReportForm\frmProdConf.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Production Result Confirmation Report"
        If Not Page.IsPostBack Then
            InitAllDDL()
            FilterDDLStatus_SelectedIndexChanged(Nothing, Nothing)
            FilterDDLTypeReport_SelectedIndexChanged(Nothing, Nothing)
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        End If
    End Sub 'OK

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListResult") Is Nothing And Session("WarningListResult") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListResult") Then
                Session("WarningListResult") = Nothing
                cProc.SetModalPopUpExtender(btnHideListResult, pnlListResult, mpeListResult, True)
            End If
        End If
        If Not Session("EmptyListKIK") Is Nothing And Session("EmptyListKIK") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListKIK") Then
                Session("EmptyListKIK") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListKIK, PanelListKIK, mpeListKIK, True)
            End If
        End If
        If Not Session("WarningListKIK") Is Nothing And Session("WarningListKIK") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListKIK") Then
                Session("WarningListKIK") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListKIK, PanelListKIK, mpeListKIK, True)
            End If
        End If
    End Sub 'OK

    Protected Sub FilterDDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLBusUnit.SelectedIndexChanged
        InitDept()
    End Sub 'OK

    Protected Sub btnSearchResult_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchResult.Click
        If FilterDDLBusUnit.Text = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        If DDLNo.SelectedValue = "wono" Then
            If IsValidPeriod() Then
                DDLFilterListKIK.SelectedIndex = -1 : txtFilterListKIK.Text = ""
                Session("TblKIK") = Nothing : Session("TblKIKView") = Nothing : gvListKIK.DataSource = Nothing : gvListKIK.DataBind()
                cProc.SetModalPopUpExtender(btnHiddenListKIK, PanelListKIK, mpeListKIK, True)
            Else
                Exit Sub
            End If
        Else
            If IsValidPeriod() Then
                FilterDDLListResult.SelectedIndex = -1 : FilterTextListResult.Text = "" : Session("TblListResult") = Nothing : Session("TblListResultView") = Nothing : gvListResult.DataSource = Nothing : gvListResult.DataBind()
                BindResultData()
            Else
                Exit Sub
            End If
        End If
    End Sub 'OK

    Protected Sub btnClearResult_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearResult.Click
        prodresmstoid.Text = "" : prodresno.Text = ""
    End Sub 'OK

    Protected Sub btnFindListResult_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListResult.Click
        UpdateCheckedResult()
        If Session("TblListResult") IsNot Nothing Then
            If FilterDDLListResult.SelectedIndex = 0 Then
                Session("TblListResult").DefaultView.RowFilter = FilterDDLListResult.SelectedValue & " =" & Tchar(FilterTextListResult.Text)
            Else
                Session("TblListResult").DefaultView.RowFilter = FilterDDLListResult.SelectedValue & " LIKE '%" & Tchar(FilterTextListResult.Text) & "%'"
            End If

            If Session("TblListResult").DefaultView.Count > 0 Then
                Session("TblListResultView") = Session("TblListResult").DefaultView.ToTable
                gvListResult.DataSource = Session("TblListResultView")
                gvListResult.DataBind()
                Session("TblListResult").DefaultView.RowFilter = ""
                mpeListResult.Show()
            Else
                Session("WarningListResult") = "Production Result data can't be found!"
                showMessage(Session("WarningListResult"), 2)
                Session("TblListResult").DefaultView.RowFilter = ""
            End If
        Else
            Session("WarningListResult") = "No Production Result data available for this time!"
            showMessage(Session("WarningListResult"), 2)
        End If
    End Sub 'OK

    Protected Sub btnAllListResult_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListResult.Click
        UpdateCheckedResult()
        If Session("TblListResult") IsNot Nothing Then
            FilterDDLListResult.SelectedIndex = -1 : FilterTextListResult.Text = "" : gvListResult.SelectedIndex = -1
            Session("TblListResultView") = Session("TblListResult")
            gvListResult.DataSource = Session("TblListResultView")
            gvListResult.DataBind()
            mpeListResult.Show()
        Else
            Session("WarningListResult") = "No Production Result data available for this time!"
            showMessage(Session("WarningListResult"), 2)
        End If
    End Sub 'OK

    Protected Sub gvListResult_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListResult.PageIndexChanging
        UpdateCheckedResult()
        gvListResult.PageIndex = e.NewPageIndex
        gvListResult.DataSource = Session("TblListResultView")
        gvListResult.DataBind()
        mpeListResult.Show()
    End Sub 'OK

    Protected Sub cbListResultHdr_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim bVal As Boolean = sender.Checked
        For C1 As Integer = 0 To gvListResult.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListResult.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = bVal
                    End If
                Next
            End If
        Next
        mpeListResult.Show()
    End Sub 'OK

    Protected Sub lkbAddToListResult_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListResult.Click
        UpdateCheckedResult()
        If Session("TblListResult") IsNot Nothing Then
            Session("TblListResult").DefaultView.RowFilter = "checkvalue='True'"
            If Session("TblListResult").DefaultView.Count > 0 Then
                Dim sOid As String = "", sNo As String = ""
                For C1 As Integer = 0 To Session("TblListResult").DefaultView.Count - 1
                    sOid &= Session("TblListResult").DefaultView(C1)("prodresmstoid").ToString & ","
                    If DDLNo.SelectedValue = "prodresmstoid" Then
                        sNo &= Session("TblListResult").DefaultView(C1)("prodresmstoid").ToString & ";"
                    Else
                        sNo &= Session("TblListResult").DefaultView(C1)("prodresno").ToString & ";"
                    End If
                Next
                prodresmstoid.Text = Left(sOid, sOid.Length - 1)
                prodresno.Text = Left(sNo, sNo.Length - 1)
                cProc.SetModalPopUpExtender(btnHideListResult, pnlListResult, mpeListResult, False)
            Else
                Session("WarningListResult") = "Please select some Production Result data!"
                showMessage(Session("WarningListResult"), 2)
                Session("TblListResult").DefaultView.RowFilter = ""
            End If
        Else
            Session("WarningListResult") = "No Production Result data available for this time!"
            showMessage(Session("WarningListResult"), 2)
        End If
    End Sub 'OK

    Protected Sub lkbCloseListResult_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListResult.Click
        cProc.SetModalPopUpExtender(btnHideListResult, pnlListResult, mpeListResult, False)
    End Sub 'OK

    Protected Sub btnFindListKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListKIK.Click
        If Session("TblKIK") Is Nothing Then
            BindListKIK()
            If Session("TblKIK").Rows.Count <= 0 Then
                Session("EmptyListKIK") = "KIK data can't be found!"
                showMessage(Session("EmptyListKIK"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListKIK.SelectedValue & " LIKE '%" & Tchar(txtFilterListKIK.Text) & "%'"
        If UpdateCheckedKIK() Then
            Dim dv As DataView = Session("TblKIK").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblKIKView") = dv.ToTable
                gvListKIK.DataSource = Session("TblKIKView")
                gvListKIK.DataBind()
                dv.RowFilter = ""
                mpeListKIK.Show()
            Else
                dv.RowFilter = ""
                Session("TblKIKView") = Nothing
                gvListKIK.DataSource = Session("TblKIKView")
                gvListKIK.DataBind()
                Session("WarningListKIK") = "KIK data can't be found!"
                showMessage(Session("WarningListKIK"), 2)
            End If
        Else
            mpeListKIK.Show()
        End If
    End Sub

    Protected Sub btnViewAllListKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListKIK.Click
        DDLFilterListKIK.SelectedIndex = -1 : txtFilterListKIK.Text = ""
        If Session("TblKIK") Is Nothing Then
            BindListKIK()
            If Session("TblKIK").Rows.Count <= 0 Then
                Session("EmptyListKIK") = "KIK data can't be found!"
                showMessage(Session("EmptyListKIK"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedKIK() Then
            Dim dt As DataTable = Session("TblKIK")
            Session("TblKIKView") = dt
            gvListKIK.DataSource = Session("TblKIKView")
            gvListKIK.DataBind()
        End If
        mpeListKIK.Show()
    End Sub

    Protected Sub btnSelectAllKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllKIK.Click
        If Not Session("TblKIKView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblKIKView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblKIK")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "womstoid=" & dtTbl.Rows(C1)("womstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblKIK") = objTbl
                Session("TblKIKView") = dtTbl
                gvListKIK.DataSource = Session("TblKIKView")
                gvListKIK.DataBind()
            End If
            mpeListKIK.Show()
        Else
            Session("WarningListKIK") = "Please show some KIK data first!"
            showMessage(Session("WarningListKIK"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneKIK.Click
        If Not Session("TblKIKView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblKIKView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblKIK")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "womstoid=" & dtTbl.Rows(C1)("womstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblKIK") = objTbl
                Session("TblKIKView") = dtTbl
                gvListKIK.DataSource = Session("TblKIKView")
                gvListKIK.DataBind()
            End If
            mpeListKIK.Show()
        Else
            Session("WarningListKIK") = "Please show some KIK data first!"
            showMessage(Session("WarningListKIK"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedKIK.Click
        If Session("TblKIK") Is Nothing Then
            Session("WarningListKIK") = "Selected KIK data can't be found!"
            showMessage(Session("WarningListKIK"), 2)
            Exit Sub
        End If
        If UpdateCheckedKIK() Then
            Dim dtTbl As DataTable = Session("TblKIK")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListKIK.SelectedIndex = -1 : txtFilterListKIK.Text = ""
                Session("TblKIKView") = dtView.ToTable
                gvListKIK.DataSource = Session("TblKIKView")
                gvListKIK.DataBind()
                dtView.RowFilter = ""
                mpeListKIK.Show()
            Else
                dtView.RowFilter = ""
                Session("TblKIKView") = Nothing
                gvListKIK.DataSource = Session("TblKIKView")
                gvListKIK.DataBind()
                Session("WarningListKIK") = "Selected KIK data can't be found!"
                showMessage(Session("WarningListKIK"), 2)
            End If
        Else
            mpeListKIK.Show()
        End If
    End Sub

    Protected Sub gvListKIK_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListKIK.PageIndexChanging
        If UpdateCheckedKIK2() Then
            gvListKIK.PageIndex = e.NewPageIndex
            gvListKIK.DataSource = Session("TblKIKView")
            gvListKIK.DataBind()
        End If
        mpeListKIK.Show()
    End Sub 'OK

    Protected Sub lkbAddToListListKIK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListKIK.Click
        If Not Session("TblKIK") Is Nothing Then
            If UpdateCheckedKIK() Then
                Dim dtTbl As DataTable = Session("TblKIK")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If prodresno.Text <> "" Then
                            If dtView(C1)("wono") <> "" Then
                                prodresno.Text &= ";" + vbCrLf + dtView(C1)("wono")
                            End If
                        Else
                            If dtView(C1)("wono") <> "" Then
                                prodresno.Text &= dtView(C1)("wono")
                            End If
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHiddenListKIK, PanelListKIK, mpeListKIK, False)
                Else
                    Session("WarningListKIK") = "Please select KIK to add to list!"
                    showMessage(Session("WarningListKIK"), 2)
                    Exit Sub
                End If
            Else
                Session("WarningListKIK") = "Please show some KIK data first!"
                showMessage(Session("WarningListKIK"), 2)
                Exit Sub
            End If
        Else
            Session("WarningListKIK") = "Please show some KIK data first!"
            showMessage(Session("WarningListKIK"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListKIK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListKIK.Click
        cProc.SetModalPopUpExtender(btnHiddenListKIK, PanelListKIK, mpeListKIK, False)
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub 'OK

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub 'OK

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub 'OK

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmProdConf.aspx?awal=true")
    End Sub 'OK

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub 'OK

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub 'OK

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub 'OK

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not report Is Nothing Then
                If report.IsLoaded Then
                    report.Dispose()
                    report.Close()
                End If
            End If
        Catch ex As Exception
            report.Dispose()
            report.Close()
        End Try
    End Sub 'OK
#End Region

    Protected Sub DDLDept_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitDept()
    End Sub

    Protected Sub FilterDDLStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLStatus.SelectedIndexChanged
        If FilterDDLStatus.SelectedValue = "POST" Then
            'Fill DDL Date
            DDLDate.Items.Clear()
            DDLDate.Items.Add("Result Date")
            DDLDate.Items.Item(DDLDate.Items.Count - 1).Value = "resm.prodresdate"
            DDLDate.Items.Add("Confirmation Date")
            DDLDate.Items.Item(DDLDate.Items.Count - 1).Value = "resc.confirmdate"
            DDLDate.Items.Add("Post Date")
            DDLDate.Items.Item(DDLDate.Items.Count - 1).Value = "resm.updtime"
        Else
            'Fill DDL Date
            DDLDate.Items.Clear()
            DDLDate.Items.Add("Result Date")
            DDLDate.Items.Item(DDLDate.Items.Count - 1).Value = "resm.prodresdate"
            DDLDate.Items.Add("Confirmation Date")
            DDLDate.Items.Item(DDLDate.Items.Count - 1).Value = "resc.confirmdate"
        End If
    End Sub

    Protected Sub FilterDDLTypeReport_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLTypeReport.SelectedIndexChanged
        If FilterDDLTypeReport.SelectedValue = "Ginteg" Then
            btnViewReport.Visible = False
            btnExportToPdf.Visible = False
        Else
            btnViewReport.Visible = True
            btnExportToPdf.Visible = True
        End If
    End Sub
End Class
