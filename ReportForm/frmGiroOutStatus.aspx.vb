Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_OutgoingGiroStatus
	Inherits System.Web.UI.Page

#Region "Variables"
	Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
	Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
	Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
	Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
	Dim xCmd As New SqlCommand("", conn)
	Dim xreader As SqlDataReader
	Dim sSql As String = ""
	Dim cKon As New Koneksi
	Dim cProc As New ClassProcedure
	Dim report As New ReportDocument
	Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
	Private Function IsValidPeriod() As Boolean
		Dim sErr As String = ""
		If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
			showMessage("Your Period 1 is invalid. " & sErr, 2)
			Return False
		End If
		If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
			showMessage("Your Period 2 is invalid. " & sErr, 2)
			Return False
		End If
		If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
			showMessage("Period 2 must be more than Period 1 !", 2)
			Return False
		End If
		Return True
	End Function
#End Region

#Region "Procedures"
	Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
		Dim strCaption As String = CompnyName
		If iType = 1 Then ' Error
			imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
		ElseIf iType = 2 Then ' Warning
			imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
		ElseIf iType = 3 Then ' Information
			imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
		Else
			imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
		End If
		lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
		cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
	End Sub

	Private Sub InitDDL()
		' DDL Business Unit
		sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
		If Session("CompnyCode") <> CompnyCode Then
			sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
		End If
		If FillDDL(DDLBusUnit, sSql) Then
			InitDDLBank()
		End If
	End Sub

	Private Sub InitDDLBank()
		' DDL Bank Account
		FillDDLAcctg(DDLBankAccount, "VAR_BANK", DDLBusUnit.SelectedValue, "ALL", "ALL")
	End Sub

	Private Sub BindListSupp()
		sSql = "SELECT DISTINCT 'False' AS checkvalue, suppoid, suppcode [Supplier Code], suppname [Supplier], suppaddr FROM QL_trncashbankmst cb INNER JOIN QL_mstsupp s ON suppoid=(CASE WHEN ISNULL(refsuppoid, 0)=0 THEN (CASE WHEN cashbankgroup='MUTATION' THEN -1 ELSE personoid END) ELSE refsuppoid END) WHERE cb.cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbanktype='BGK' ORDER BY [Supplier Code]"
		Dim dtSupp As DataTable = cKon.ambiltabel(sSql, "QL_mstsupp")
		If dtSupp.Rows.Count > 0 Then
			Session("TblListSupp") = dtSupp
            gvListSupp.DataSource = Session("TblListSupp")
            gvListSupp.DataBind()
			cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
		Else
			showMessage("There is no Supplier data available for this time!", 2)
		End If
	End Sub

	Private Sub UpdateCheckedListSupp()
		If Not Session("TblListSupp") Is Nothing Then
			Dim dtTbl As DataTable = Session("TblListSupp")
			If dtTbl.Rows.Count > 0 Then
				Dim dtView As DataView = dtTbl.DefaultView
				For C1 As Integer = 0 To gvListSupp.Rows.Count - 1
					Dim row As System.Web.UI.WebControls.GridViewRow = gvListSupp.Rows(C1)
					If (row.RowType = DataControlRowType.DataRow) Then
						Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
						For Each myControl As System.Web.UI.Control In cc
							If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
								Dim cbCheckValue As String = "False"
								If CType(myControl, System.Web.UI.WebControls.CheckBox).Checked Then
									cbCheckValue = "True"
								End If
								dtView.RowFilter = "suppoid=" & CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
								If dtView.Count > 0 Then
									dtView(0)("checkvalue") = cbCheckValue
								End If
								dtView.RowFilter = ""
							End If
						Next
					End If
				Next
				dtTbl.AcceptChanges()
				Session("TblListSupp") = dtTbl
			End If
		End If
		If Not Session("TblListSuppView") Is Nothing Then
			Dim dtTbl As DataTable = Session("TblListSuppView")
			If dtTbl.Rows.Count > 0 Then
				Dim dtView As DataView = dtTbl.DefaultView
				For C1 As Integer = 0 To gvListSupp.Rows.Count - 1
					Dim row As System.Web.UI.WebControls.GridViewRow = gvListSupp.Rows(C1)
					If (row.RowType = DataControlRowType.DataRow) Then
						Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
						For Each myControl As System.Web.UI.Control In cc
							If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
								Dim cbCheckValue As String = "False"
								If CType(myControl, System.Web.UI.WebControls.CheckBox).Checked Then
									cbCheckValue = "True"
								End If
								dtView.RowFilter = "suppoid=" & CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
								If dtView.Count > 0 Then
									dtView(0)("checkvalue") = cbCheckValue
								End If
								dtView.RowFilter = ""
							End If
						Next
					End If
				Next
				dtTbl.AcceptChanges()
				Session("TblListSuppView") = dtTbl
			End If
		End If
	End Sub

	Private Sub BindListBGK()
        sSql = "SELECT 'False' AS checkvalue, cashbankoid, cashbankno, CONVERT(VARCHAR(10), cashbankdate, 101) AS cashbankdate, cashbankgroup,cashbanknote FROM QL_trncashbankmst cb WHERE cb.cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbanktype='BGK'"
		If DDLBankAccount.SelectedValue <> "ALL" Then
			If DDLBankAccount.SelectedValue <> "" Then
				sSql &= " AND acctgoid=" & DDLBankAccount.SelectedValue
			End If
		End If
		sSql &= " ORDER BY cashbankno"
		Dim dtBGK As DataTable = cKon.ambiltabel(sSql, "QL_trncashbankmst")
		If dtBGK.Rows.Count > 0 Then
			Session("TblListBGK") = dtBGK
            gvListBGK.DataSource = Session("TblListBGK")
            gvListBGK.DataBind()
			cProc.SetModalPopUpExtender(btnHideListBGK, pnlListBGK, mpeListBGK, True)
		Else
			showMessage("There is no Outgoing Giro data available for this time!", 2)
		End If
	End Sub

	Private Sub UpdateCheckedListBGK()
		If Not Session("TblListBGK") Is Nothing Then
			Dim dtTbl As DataTable = Session("TblListBGK")
			If dtTbl.Rows.Count > 0 Then
				Dim dtView As DataView = dtTbl.DefaultView
				For C1 As Integer = 0 To gvListBGK.Rows.Count - 1
					Dim row As System.Web.UI.WebControls.GridViewRow = gvListBGK.Rows(C1)
					If (row.RowType = DataControlRowType.DataRow) Then
						Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
						For Each myControl As System.Web.UI.Control In cc
							If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
								Dim cbCheckValue As String = "False"
								If CType(myControl, System.Web.UI.WebControls.CheckBox).Checked Then
									cbCheckValue = "True"
								End If
								dtView.RowFilter = "cashbankoid=" & CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
								If dtView.Count > 0 Then
									dtView(0)("checkvalue") = cbCheckValue
								End If
								dtView.RowFilter = ""
							End If
						Next
					End If
				Next
				dtTbl.AcceptChanges()
				Session("TblListBGK") = dtTbl
			End If
		End If
		If Not Session("TblListBGKView") Is Nothing Then
			Dim dtTbl As DataTable = Session("TblListBGKView")
			If dtTbl.Rows.Count > 0 Then
				Dim dtView As DataView = dtTbl.DefaultView
				For C1 As Integer = 0 To gvListBGK.Rows.Count - 1
					Dim row As System.Web.UI.WebControls.GridViewRow = gvListBGK.Rows(C1)
					If (row.RowType = DataControlRowType.DataRow) Then
						Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
						For Each myControl As System.Web.UI.Control In cc
							If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
								Dim cbCheckValue As String = "False"
								If CType(myControl, System.Web.UI.WebControls.CheckBox).Checked Then
									cbCheckValue = "True"
								End If
								dtView.RowFilter = "cashbankoid=" & CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
								If dtView.Count > 0 Then
									dtView(0)("checkvalue") = cbCheckValue
								End If
								dtView.RowFilter = ""
							End If
						Next
					End If
				Next
				dtTbl.AcceptChanges()
				Session("TblListBGKView") = dtTbl
			End If
		End If
	End Sub

	Private Sub ShowReport(ByVal sType As String)
		Try
			If sType = "Print Excel" Then
                report.Load(Server.MapPath(folderReport & "rptGiroOutStatus.rpt"))
			Else
				report.Load(Server.MapPath(folderReport & "rptGiroOutStatus.rpt"))
			End If
            sSql = "SELECT * FROM (" & _
             "SELECT cb.cmpcode, cb.cashbankoid [Oid], cashbankno [BGK No.], cashbankrefno [Giro No.], cashbankdate [Cash/Bank Date], (CASE cashbankgroup WHEN 'MUTATION' THEN '' ELSE suppcode END) [Supplier Code], (CASE cashbankgroup WHEN 'MUTATION' THEN '' ELSE suppname END) [Supplier], (CASE WHEN ISNULL(cashbankres1, '')='Cancel' THEN ISNULL(cashbankres1, '') ELSE cashbankstatus END) [Status], cashbankamt [Amount], cashbankamtidr [Amount IDR], cashbankamtusd [Amount USD], cb.acctgoid, acctgcode [COA], acctgdesc [COA Desc], cashbanknote [Note], cb.createuser [Create User], (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=cb.cmpcode) [Business Unit], currcode [Currency], cashbanktakegiroreal [Real Date Take Giro], (CASE WHEN ISNULL(cashbankres1, '')='Closed' THEN (CASE WHEN cashbanktakegiroreal>cashbankduedate THEN cashbanktakegiroreal ELSE cashbankduedate END) ELSE /*CAST('01/01/1900' AS DATETIME)*/ cashbankduedate END) [Real Due Date], cashbanktakegiro [Date Take Giro], cashbankduedate [Due Date], ISNULL(cashbankres1, '') [Giro Status], (CASE WHEN cashbankstatus<>'In Process' THEN cb.updtime ELSE /*CAST('01/01/1900' AS DATETIME)*/ cashbanktakegiro END) [Posting Date], (CASE cashbankgroup WHEN 'MUTATION' THEN 'Overbooking' WHEN 'EXPENSE' THEN 'Expense' ELSE 'A/P Payment' END) [Reference Trans] FROM QL_trncashbankmst cb LEFT JOIN QL_mstsupp s ON suppoid=(CASE WHEN ISNULL(refsuppoid, 0) > 0 THEN refsuppoid ELSE personoid END) INNER JOIN QL_mstacctg a ON a.acctgoid=cb.acctgoid INNER JOIN QL_mstcurr c ON c.curroid=cb.curroid WHERE cb.cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbanktype='BGK' AND cashbankgroup NOT IN ('EXPENSE GIRO', 'PAYAP GIRO') UNION ALL " & _
               "SELECT cb.cmpcode, cb.cashbankoid [Oid], cashbankno [BGK No.], cashbankglrefno [Giro No.], cashbankdate [Cash/Bank Date], suppcode [Supplier Code], suppname [Supplier], cashbankstatus [Status], cashbankglamt [Amount], cashbankglamtidr [Amount IDR], cashbankglamtusd [Amount USD], cb.acctgoid, acctgcode [COA], acctgdesc [COA Desc], cashbankglnote [Note], cb.createuser [Create User], (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=cb.cmpcode) [Business Unit], currcode [Currency], cashbankgltakegiroreal [Real Date Take Giro], (CASE WHEN ISNULL(cashbankglgiroflag, '')='Closed' THEN (CASE WHEN cashbankgltakegiroreal>cashbankglduedate THEN cashbankgltakegiroreal ELSE cashbankglduedate END) ELSE CAST('01/01/1900' AS DATETIME) END) [Real Due Date], cashbankgltakegiro [Date Take Giro], cashbankglduedate [Due Date], ISNULL(cashbankglgiroflag, '') [Giro Status], (CASE WHEN cashbankstatus<>'In Process' THEN cb.updtime ELSE CAST('01/01/1900' AS DATETIME) END) [Posting Date], 'Expense Multi Giro' [Reference Trans] FROM QL_trncashbankmst cb INNER JOIN QL_mstsupp s ON suppoid=refsuppoid INNER JOIN QL_mstacctg a ON a.acctgoid=cb.acctgoid INNER JOIN QL_mstcurr c ON c.curroid=cb.curroid INNER JOIN QL_trncashbankgl gl ON gl.cmpcode=cb.cmpcode AND gl.cashbankoid=cb.cashbankoid WHERE cb.cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbanktype='BGK' AND cashbankgroup IN ('EXPENSE GIRO') UNION ALL " & _
               "SELECT cb.cmpcode, cb.cashbankoid [Oid], cashbankno [BGK No.], payaprefno [Giro No.], cashbankdate [Cash/Bank Date], suppcode [Supplier Code], suppname [Supplier], cashbankstatus [Status], payapamt [Amount], payapamtidr [Amount IDR], payapamtusd [Amount USD], cb.acctgoid, acctgcode [COA], acctgdesc [COA Desc], payapnote [Note], cb.createuser [Create User], (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=cb.cmpcode) [Business Unit], currcode [Currency], payaptakegiroreal [Real Date Take Giro], (CASE WHEN ISNULL(payapgiroflag, '')='Closed' THEN (CASE WHEN payaptakegiroreal>payapduedate THEN payaptakegiroreal ELSE payapduedate END) ELSE CAST('01/01/1900' AS DATETIME) END) [Real Due Date], payaptakegiro [Date Take Giro], payapduedate [Due Date], ISNULL(payapgiroflag, '') [Giro Status], (CASE WHEN cashbankstatus<>'In Process' THEN cb.updtime ELSE CAST('01/01/1900' AS DATETIME) END) [Posting Date], 'A/P Payment Multi Giro' [Reference Trans] FROM QL_trncashbankmst cb INNER JOIN QL_mstsupp s ON suppoid=personoid INNER JOIN QL_mstacctg a ON a.acctgoid=cb.acctgoid INNER JOIN QL_mstcurr c ON c.curroid=cb.curroid INNER JOIN QL_trnpayap pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid WHERE cb.cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbanktype='BGK' AND cashbankgroup IN ('PAYAP GIRO')" & _
    ") AS tbl_GiroOut WHERE 1=1"
			If DDLStatus.SelectedValue.ToUpper <> "ALL" Then
				sSql &= " AND [Status]='" & DDLStatus.SelectedValue & "'"
			End If
			If DDLBankAccount.SelectedValue.ToUpper <> "ALL" Then
				If DDLBankAccount.SelectedValue <> "" Then
					sSql &= " AND acctgoid=" & DDLBankAccount.SelectedValue
				End If
			End If
			If rbInternal.Checked = True Then
				sSql &= " AND suppoid in (select suppoid from ql_mstsupp where suppres1='Internal')"
			ElseIf rbExternal.Checked = True Then
				sSql &= " AND suppoid in (select suppoid from ql_mstsupp where suppres1<>'Internal')"
			End If
			If IsValidPeriod() Then
				sSql &= " AND " & DDLDate.SelectedValue & ">=CAST('" & FilterPeriod1.Text & " 00:00:00' AS DATETIME) AND " & DDLDate.SelectedValue & "<=CAST('" & FilterPeriod2.Text & " 23:59:59' AS DATETIME)"
			End If
			Dim sErr As String = ""
			If cbGiroFlag.Checked Then
				If FilterPeriod2.Text <> "" Then
					If IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
						If DDLGiroFlag.SelectedIndex = 0 Then
							sSql &= " AND NOT ([Real Date Take Giro]<>CAST('01/01/1900' AS DATETIME) AND [Real Date Take Giro]<=CAST('" & FilterPeriod2.Text & " 23:59:59' AS DATETIME))"
                        ElseIf DDLGiroFlag.SelectedIndex = 1 Then
                            sSql &= " AND ([Real Date Take Giro]<>CAST('01/01/1900' AS DATETIME) AND [Real Date Take Giro]<=CAST('" & FilterPeriod2.Text & " 23:59:59' AS DATETIME))"
                        Else
                            sSql &= " AND NOT ([Real Date Take Giro]<>CAST('01/01/1900' AS DATETIME) AND [Real Date Take Giro]<=CAST('" & FilterPeriod2.Text & " 23:59:59' AS DATETIME)) AND ([Giro Status]='Cancel')"
                        End If
					End If
				End If
			End If
			If TextSupplier.Text <> "" Then
				Dim sFilter As String = ""
				Dim sSplit() As String = TextSupplier.Text.Split(";")
				For C1 As Integer = 0 To sSplit.Length - 1
					If sSplit(C1) <> "" Then
						sFilter &= DDLSupp.SelectedValue & " LIKE '" & sSplit(C1) & "' OR "
					End If
				Next
				If sFilter <> "" Then
					sSql &= " AND (" & Left(sFilter, sFilter.Length - 4) & ")"
				End If
			End If
			If TextBGKNo.Text <> "" Then
				Dim sFilter As String = ""
				Dim sSplit() As String = TextBGKNo.Text.Split(";")
				For C1 As Integer = 0 To sSplit.Length - 1
					If sSplit(C1) <> "" Then
						sFilter &= "[BGK No.] LIKE '" & sSplit(C1) & "' OR "
					End If
				Next
				If sFilter <> "" Then
					sSql &= " AND (" & Left(sFilter, sFilter.Length - 4) & ")"
				End If
			End If
			sSql &= " ORDER BY " & DDLSorting.SelectedValue & " " & DDLOrderby.SelectedValue & IIf(DDLSorting.SelectedValue <> "[BGK No.]", ", [BGK No.]", "")
			Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "tbl_GiroOut")
			report.SetDataSource(dtTbl)
			report.PrintOptions.PaperSize = PaperSize.PaperA4
			report.SetParameterValue("sPeriod", Format(CDate(FilterPeriod1.Text), "dd MMM yyyy") & " - " & Format(CDate(FilterPeriod2.Text), "dd MMM yyyy"))
			cProc.SetDBLogonForReport(report)
			If sType = "View" Then
				crvReportForm.DisplayGroupTree = False
				crvReportForm.ReportSource = report
				crvReportForm.SeparatePages = True
			ElseIf sType = "Print PDF" Then
				Response.Buffer = False
				Response.ClearContent()
				Response.ClearHeaders()
				report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "OutgoingGiroStatus")
				report.Close()
				report.Dispose()
			Else
				Response.Buffer = False
				Response.ClearContent()
				Response.ClearHeaders()
				report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "OutgoingGiroStatus")
				report.Close()
				report.Dispose()
			End If
		Catch ex As Exception
			report.Close()
			report.Dispose()
			showMessage(ex.Message, 1)
		End Try
	End Sub
#End Region

#Region "Events"
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("UserID") = "" Then
			Response.Redirect("~/Other/login.aspx")
		End If
		If Request.QueryString("awal") = "true" Then
			' Simpan session ke variabel temporary supaya tidak hilang
			Dim userId As String = Session("UserID")
			Dim xsetAcc As DataTable = Session("SpecialAccess")
			Dim appLimit As Decimal = Session("ApprovalLimit")
			Dim xsetRole As DataTable = Session("Role")
			Dim cmpcode As String = Session("CompnyCode")
			' Clear all session
			Session.Clear()
			' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
			Session("UserID") = userId
			Session("SpecialAccess") = xsetAcc
			Session("ApprovalLimit") = appLimit
			Session("Role") = xsetRole
			Session("CompnyCode") = cmpcode
			' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
			Response.Redirect("~\ReportForm\frmGiroOutStatus.aspx")
		End If
		If checkPagePermission("~\ReportForm\frmGiroOutStatus.aspx", Session("Role")) = False Then
			Response.Redirect("~\Other\NotAuthorize.aspx")
		End If
		Page.Title = CompnyName & " - Outgoing Giro Status"
		If Not Page.IsPostBack Then
			InitDDL()
			FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
			FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
		End If
	End Sub

	Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
		cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
		If Not Session("WarningListSupp") Is Nothing And Session("WarningListSupp") <> "" Then
			If lblPopUpMsg.Text = Session("WarningListSupp") Then
				Session("WarningListSupp") = Nothing
				cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
			End If
		End If
        If Not Session("WarningListBGK") Is Nothing And Session("WarningListBGK") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListBGK") Then
                Session("WarningListBGK") = Nothing
                cProc.SetModalPopUpExtender(btnHideListBGK, pnlListBGK, mpeListBGK, True)
            End If
        End If
	End Sub

	Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
		InitDDLBank()
	End Sub

	Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupp.Click
		If DDLBusUnit.SelectedValue = "" Then
			showMessage("Please select Business Unit first!", 2) : Exit Sub
		End If
		FilterDDLListSupp.SelectedIndex = -1 : FilterTextListSupp.Text = "" : Session("TblListSupp") = Nothing : Session("TblListSuppView") = Nothing : gvListSupp.DataSource = Nothing : gvListSupp.DataBind()
		BindListSupp()
	End Sub

	Protected Sub btnClearSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupp.Click
		TextSupplier.Text = ""
	End Sub

	Protected Sub btnFindListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSupp.Click
		UpdateCheckedListSupp()
		Dim dt As DataTable = Session("TblListSupp")
		If dt.Rows.Count > 0 Then
			Dim dv As DataView = dt.DefaultView
			dv.RowFilter = FilterDDLListSupp.SelectedValue & " LIKE '%" & Tchar(FilterTextListSupp.Text) & "%'"
			If dv.Count > 0 Then
				Session("TblListSuppView") = dv.ToTable
				gvListSupp.DataSource = Session("TblListSuppView")
				gvListSupp.DataBind()
				dv.RowFilter = ""
				mpeListSupp.Show()
			Else
				dv.RowFilter = ""
				Session("WarningListSupp") = "Supplier data can't be found!"
				showMessage(Session("WarningListSupp"), 2) : Exit Sub
			End If
		Else
			Session("WarningListSupp") = "Supplier data can't be found!"
			showMessage(Session("WarningListSupp"), 2) : Exit Sub
		End If
	End Sub

	Protected Sub btnAllListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListSupp.Click
		UpdateCheckedListSupp()
		FilterDDLListSupp.SelectedIndex = -1 : FilterTextListSupp.Text = ""
		Dim dt As DataTable = Session("TblListSupp")
		If dt.Rows.Count > 0 Then
			Session("TblListSuppView") = dt
			gvListSupp.DataSource = Session("TblListSuppView")
			gvListSupp.DataBind()
			mpeListSupp.Show()
		Else
			Session("WarningListSupp") = "Supplier data can't be found!"
			showMessage(Session("WarningListSupp"), 2) : Exit Sub
		End If
	End Sub

	Protected Sub gvListSupp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSupp.PageIndexChanging
		UpdateCheckedListSupp()
		gvListSupp.PageIndex = e.NewPageIndex
        gvListSupp.DataSource = Session("TblListSupp")
		gvListSupp.DataBind()
		mpeListSupp.Show()
	End Sub

	Protected Sub lbAddToListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListSupp.Click
		UpdateCheckedListSupp()
		Dim dtTbl As DataTable = Session("TblListSupp")
		If dtTbl.Rows.Count > 0 Then
			Dim dtView As DataView = dtTbl.DefaultView
			dtView.RowFilter = "checkvalue='True'"
			Dim iCheck As Integer = dtView.Count
			If dtView.Count > 0 Then
				For C1 As Integer = 0 To dtView.Count - 1
					If TextSupplier.Text <> "" Then
						TextSupplier.Text &= ";" + vbCrLf + dtView(C1)(DDLSupp.SelectedValue.Replace("[", "").Replace("]", "")).ToString
					Else
						TextSupplier.Text = dtView(C1)(DDLSupp.SelectedValue.Replace("[", "").Replace("]", "")).ToString
					End If
				Next
				dtView.RowFilter = ""
				cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
			Else
				dtView.RowFilter = ""
				Session("WarningListSupp") = "Please select some Supplier data to add to list!"
				showMessage(Session("WarningListSupp"), 2) : Exit Sub
			End If
		Else
			Session("WarningListSupp") = "Please select some Supplier data to add to list!"
			showMessage(Session("WarningListSupp"), 2) : Exit Sub
		End If
	End Sub

	Protected Sub lbCloseListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListSupp.Click
		cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
	End Sub

	Protected Sub btnSearchBGK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchBGK.Click
		If DDLBusUnit.SelectedValue = "" Then
			showMessage("Please select Business Unit first!", 2) : Exit Sub
		End If
		If DDLBankAccount.SelectedValue = "" Then
			showMessage("Please select Bank Account first!", 2) : Exit Sub
		End If
		FilterDDLListBGK.SelectedIndex = -1 : FilterTextListBGK.Text = "" : Session("TblListBGK") = Nothing : Session("TblListBGKView") = Nothing : gvListBGK.DataSource = Nothing : gvListBGK.DataBind()
		BindListBGK()
	End Sub

	Protected Sub btnClearBGK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearBGK.Click
		TextBGKNo.Text = ""
	End Sub

	Protected Sub btnFindListBGK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListBGK.Click
		UpdateCheckedListBGK()
		Dim dt As DataTable = Session("TblListBGK")
		If dt.Rows.Count > 0 Then
			Dim dv As DataView = dt.DefaultView
			dv.RowFilter = FilterDDLListBGK.SelectedValue & " LIKE '%" & Tchar(FilterTextListBGK.Text) & "%'"
			If dv.Count > 0 Then
				Session("TblListBGKView") = dv.ToTable
				gvListBGK.DataSource = Session("TblListBGKView")
				gvListBGK.DataBind()
				dv.RowFilter = ""
				mpeListBGK.Show()
            Else
                dv.RowFilter = ""
                Session("WarningListBGK") = "Outgoing Giro data can't be found!"
                showMessage(Session("WarningListBGK"), 2)
                Exit Sub
			End If
		Else
			Session("WarningListBGK") = "Outgoing Giro data can't be found!"
            showMessage(Session("WarningListBGK"), 2)
            Exit Sub
		End If
	End Sub

	Protected Sub btnAllListBGK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListBGK.Click
		UpdateCheckedListBGK()
		FilterDDLListBGK.SelectedIndex = -1 : FilterTextListBGK.Text = ""
		Dim dt As DataTable = Session("TblListBGK")
		If dt.Rows.Count > 0 Then
			Session("TblListBGKView") = dt
			gvListBGK.DataSource = Session("TblListBGKView")
			gvListBGK.DataBind()
			mpeListBGK.Show()
		Else
			Session("WarningListBGK") = "Outgoing Giro data can't be found!"
			showMessage(Session("WarningListBGK"), 2) : Exit Sub
		End If
	End Sub

	Protected Sub gvListBGK_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListBGK.PageIndexChanging
		UpdateCheckedListBGK()
		gvListBGK.PageIndex = e.NewPageIndex
		gvListBGK.DataSource = Session("TblListBGK")
		gvListBGK.DataBind()
		mpeListBGK.Show()
	End Sub

	Protected Sub lbAddToListBGK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListBGK.Click
		UpdateCheckedListBGK()
		Dim dtTbl As DataTable = Session("TblListBGK")
		If dtTbl.Rows.Count > 0 Then
			Dim dtView As DataView = dtTbl.DefaultView
			dtView.RowFilter = "checkvalue='True'"
			Dim iCheck As Integer = dtView.Count
			If dtView.Count > 0 Then
				For C1 As Integer = 0 To dtView.Count - 1
					If TextBGKNo.Text <> "" Then
						TextBGKNo.Text &= ";" + vbCrLf + dtView(C1)("cashbankno").ToString
					Else
						TextBGKNo.Text = dtView(C1)("cashbankno").ToString
					End If
				Next
				dtView.RowFilter = ""
				cProc.SetModalPopUpExtender(btnHideListBGK, pnlListBGK, mpeListBGK, False)
			Else
				dtView.RowFilter = ""
				Session("WarningListBGK") = "Please select some Outgoing Giro data to add to list!"
				showMessage(Session("WarningListBGK"), 2) : Exit Sub
			End If
		Else
			Session("WarningListBGK") = "Please select some Outgoing Giro data to add to list!"
			showMessage(Session("WarningListBGK"), 2) : Exit Sub
		End If
	End Sub

	Protected Sub lbCloseListBGK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListBGK.Click
		cProc.SetModalPopUpExtender(btnHideListBGK, pnlListBGK, mpeListBGK, False)
	End Sub

	Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
		ShowReport("View")
	End Sub

	Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
		ShowReport("Print PDF")
	End Sub

	Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
		ShowReport("Print Excel")
	End Sub

	Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
		Response.Redirect("~\ReportForm\frmGiroOutStatus.aspx?awal=true")
	End Sub

	Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
		ShowReport("View")
	End Sub

	Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
		ShowReport("View")
	End Sub

	Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
		ShowReport("View")
	End Sub

	Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
		Try
			If Not report Is Nothing Then
				If report.IsLoaded Then
					report.Dispose()
					report.Close()
				End If
			End If
		Catch ex As Exception
			report.Dispose()
			report.Close()
		End Try
	End Sub
#End Region

End Class
