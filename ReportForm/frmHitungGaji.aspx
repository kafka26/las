<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="frmHitungGaji.aspx.vb" Inherits="ReportForm_HitungGaji" title="Untitled Page" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="Table2" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" style="text-align: left" valign="center">
                <asp:Label ID="Label7" runat="server" Font-Bold="False" Text=".: Laporan Perhitungan Gaji" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="center">
    
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD align=center><TABLE><TBODY><TR><TD align=left>Bussiness Unit</TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="FilterDDLDiv" runat="server" CssClass="inpText" Width="200px" AutoPostBack="True">
            </asp:DropDownList></TD></TR><TR><TD align=left></TD><TD align=left></TD><TD align=left><asp:DropDownList id="DDLType" runat="server" CssClass="inpText" AutoPostBack="True" Visible="False">
<asp:ListItem Value="Dtl">Detail</asp:ListItem>
<asp:ListItem Value="Sum">Summary</asp:ListItem>
</asp:DropDownList></TD></TR>
    <tr>
        <td align="left">
            Periode</td>
        <td align="left">
            :</td>
        <td align="left">
            <asp:DropDownList ID="ddlmonth" runat="server" AutoPostBack="True" CssClass="inpText"
                Width="100px">
            </asp:DropDownList>
            -
            <asp:DropDownList ID="ddlyear" runat="server" AutoPostBack="True" CssClass="inpText"
                Width="75px">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td align="left">
            Bulan</td>
        <td align="left">
        </td>
        <td align="left">
            <asp:DropDownList ID="ddlperiodtype" runat="server" AutoPostBack="True" CssClass="inpText"
                Width="100px">
                <asp:ListItem Value="MINGGU 1">MINGGU 1</asp:ListItem>
                <asp:ListItem Value="MINGGU 2">MINGGU 2</asp:ListItem>
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td align="left">
            Employee</td>
        <td align="left">
            :</td>
        <td align="left">
            <asp:ListBox ID="lbPersonOid" runat="server" CssClass="inpTextDisabled" Rows="2"
                Width="186px" Height="45px"></asp:ListBox>&nbsp;<asp:ImageButton ID="btnSearchLP" runat="server"
                    ImageAlign="Middle" ImageUrl="~/Images/search.gif" Height="24px" Width="26px" />&nbsp;<asp:ImageButton ID="btnEraseLP"
                        runat="server" ImageAlign="Middle" ImageUrl="~/Images/erase.bmp" Height="24px" /></td>
    </tr>
</TBODY></TABLE></TD></TR><TR><TD align=center><asp:ImageButton id="imbView" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbPDF" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbXLS" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD align="center"><asp:UpdateProgress id="UpdateProgress2" runat="server">
        <ProgressTemplate>
            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/loadingbar.gif" />
            Please wait ..
        </ProgressTemplate>
    </asp:UpdateProgress> </TD></TR><TR><TD align=center><TABLE><TBODY><TR><TD align=left><CR:CrystalReportViewer id="CrvReport" runat="server" Width="350px" HasPrintButton="False" HasExportButton="False" AutoDataBind="True" DisplayGroupTree="False" HasCrystalLogo="False" HasDrillUpButton="False" HasToggleGroupTreeButton="False" HasViewList="False" Height="50px"></CR:CrystalReportViewer> </TD></TR></TBODY></TABLE></TD></TR><TR><TD align=center>
        <asp:UpdatePanel ID="upLP" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlLP" runat="server" CssClass="modalBox" Visible="False" Width="600px">
                    <table style="width: 100%">
                        <tr>
                            <td align="center" class="Label">
                                <asp:Label ID="lblTitleLP" runat="server" Font-Bold="True" Font-Size="Medium" Text="List of Employee"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="center" class="Label">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="Label">
                                <asp:Panel ID="pnlFindLP" runat="server" DefaultButton="btnFindLP" Width="100%">
                                    <table style="width: 70%">
                                        <tr>
                                            <td align="left" class="Label" style="width: 23%">
                                                <asp:Label ID="lblFilterLP" runat="server" Text="Filter"></asp:Label></td>
                                            <td align="center" class="Label" style="width: 2%">
                                                :</td>
                                            <td align="left" class="Label">
                                                <asp:DropDownList ID="FilterDDLLP" runat="server" CssClass="inpText" Width="100px">
                                                    <asp:ListItem Value="nip">NIP</asp:ListItem>
                                                    <asp:ListItem Value="personname">Name</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:TextBox ID="FilterTextLP" runat="server" CssClass="inpText" Width="170px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="Label" style="width: 23%">
                                            </td>
                                            <td align="center" class="Label" style="width: 2%">
                                            </td>
                                            <td align="left" class="Label">
                                                <asp:ImageButton ID="btnFindLP" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/btnfind.bmp" />
                                                <asp:ImageButton ID="btnAllLP" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewall.png" /></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="Label">
                                <asp:ImageButton ID="btnSelectAll" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/selectall.png" />&nbsp;<asp:ImageButton
                                    ID="btnSelectNone" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/selectnone.png" />&nbsp;<asp:ImageButton
                                        ID="btnViewChecked" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewselected.png" /></td>
                        </tr>
                        <tr>
                            <td align="center" class="Label" style="height: 14px">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="Label">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="Label" valign="top">
                                <fieldset id="Fieldset4" style="width: 98%; border-top-style: none; border-right-style: none;
                                    border-left-style: none; height: 200px; text-align: left; border-bottom-style: none">
                                    <div id="Div4">
                                    </div>
                                    <div style="overflow-y: scroll; width: 100%; height: 100%">
                                        <asp:GridView ID="gvLP" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                            EmptyDataRowStyle-ForeColor="Red" ForeColor="#333333" GridLines="None" Style="z-index: 100;
                                            left: -8px; position: static; top: -3px; background-color: transparent" Width="97%">
                                            <RowStyle BackColor="#EFF3FB" />
                                            <EmptyDataRowStyle ForeColor="Red" />
                                            <Columns>
                                                <asp:BoundField DataField="Nomer" HeaderText="No.">
                                                    <HeaderStyle CssClass="gvhdr" ForeColor="White" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="nip" HeaderText="NIP">
                                                    <HeaderStyle CssClass="gvhdr" ForeColor="White" HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="personname" HeaderText="Name">
                                                    <HeaderStyle CssClass="gvhdr" ForeColor="White" HorizontalAlign="Left" Width="30%" />
                                                    <ItemStyle HorizontalAlign="Left" Width="30%" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="cbSelectPerson" runat="server" Checked='<%# eval("CheckValue") %>'
                                                            ToolTip='<%# eval("personoid") %>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="gvhdr" ForeColor="White" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label19" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label>
                                            </EmptyDataTemplate>
                                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                            <HeaderStyle BackColor="#507CD1" BorderColor="White" CssClass="gvhdr" Font-Bold="True"
                                                ForeColor="White" />
                                            <EditRowStyle BackColor="#2461BF" />
                                            <AlternatingRowStyle BackColor="White" />
                                        </asp:GridView>
                                    </div>
                                </fieldset>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="Label" style="height: 5px">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="Label">
                                <asp:ImageButton ID="btnAddToListLP" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/addtolist.png" />
                                <asp:ImageButton ID="btnCloseLP" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Close.png" /></td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="mpeLP" runat="server" BackgroundCssClass="modalBackground"
                    Drag="True" PopupControlID="pnlLP" PopupDragHandleControlID="lblTitleLP" TargetControlID="btnHideLP">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Button ID="btnHideLP" runat="server" Visible="False" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False" Width="700px"><TABLE width="100%"><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD style="vertical-align: top; width: 40px; text-align: center"><asp:Image id="imIcon" runat="server" Height="24px" Width="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label" align="left"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>
    <asp:ImageButton ID="imbOKPopUp" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/ok.png" /></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" DropShadow="True" PopupDragHandleControlID="lblCaption" PopupControlID="pnlPopUpMsg" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel> </TD></TR><TR><TD align=left></TD></TR></TBODY></TABLE>
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="imbPDF"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="imbXLS"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td align="center">
                </td>
        </tr>
    </table>
</asp:Content>

