<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="frmTransformItemReport.aspx.vb" Inherits="ReportForm_ItemTransfer" title="" EnableEventValidation="false" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Transform Item Report" CssClass="Title" ForeColor="SaddleBrown"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD vAlign=top align=center char="Label"><TABLE><TBODY><TR><TD class="Label" vAlign=top align=center style="width: 364px">
    <asp:UpdatePanel id="upReportForm" runat="server">
        <contenttemplate>
<asp:Panel id="pnlReportForm" runat="server" Width="500px" DefaultButton="btnViewReport"><TABLE><TBODY><TR id="BusinessUnitA" runat="server" visible="true"><TD class="Label" align=left><asp:Label id="Label4" runat="server" Text="Business Unit"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpText" Width="275px" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label7" runat="server" Text="Type"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLType" runat="server" CssClass="inpText" Width="85px" AutoPostBack="True"><asp:ListItem>SUMMARY</asp:ListItem>
<asp:ListItem>DETAIL</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text="Period" AutoPostBack="True"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="60px" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> - <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="60px" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label3" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><ajaxToolkit:MaskedEditExtender id="meeDate1" runat="server" TargetControlID="FilterPeriod1" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="ceDate1" runat="server" TargetControlID="FilterPeriod1" PopupButtonID="imbDate1" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeDate2" runat="server" TargetControlID="FilterPeriod2" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="ceDate2" runat="server" TargetControlID="FilterPeriod2" PopupButtonID="imbDate2" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbTrans" runat="server" AutoPostBack="True"></asp:CheckBox> <asp:DropDownList id="DDLTransNo" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True"><asp:ListItem>Trans No.</asp:ListItem>
<asp:ListItem Value="Draft No.">Draft No.</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="transno" runat="server" CssClass="inpText" Width="158px" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindTrans" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseTrans" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR>
    <tr>
        <td align="left" class="Label">
            <asp:CheckBox id="cbType" runat="server" Text="Item From" AutoPostBack="True"></asp:CheckBox></td>
        <td align="center" class="Label">
            <asp:Label id="lbMat" runat="server" Text=":"></asp:Label></td>
        <td align="left" class="Label">
            <asp:TextBox id="matcode" runat="server" CssClass="inpText" Width="178px" TextMode="MultiLine"></asp:TextBox>
            <asp:ImageButton id="imbFindMat" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton>
            <asp:ImageButton id="imbEraseMat" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></td>
    </tr>
    <tr>
        <td align="left" class="Label">
            <asp:CheckBox id="cbWarehouse" runat="server" Text="To Business Unit" AutoPostBack="True" Visible="False"></asp:CheckBox></td>
        <td align="center" class="Label">
            <asp:Label id="lbWh" runat="server" Text=":" Visible="False"></asp:Label></td>
        <td align="left" class="Label">
            <asp:DropDownList id="DDLFromWarehouse" runat="server" CssClass="inpText" Width="275px" AutoPostBack="True" OnSelectedIndexChanged="DDLFromWarehouse_SelectedIndexChanged" Visible="False"></asp:DropDownList></td>
    </tr>
    <TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD style="HEIGHT: 14px" class="Label" align=left></TD><TD style="HEIGHT: 14px" class="Label" align=center></TD><TD style="HEIGHT: 14px" class="Label" align=left> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnExportToPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnExportToExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upReportForm"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="btnViewReport"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnExportToPdf"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnExportToExcel"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>&nbsp;</TD></TR><TR><TD class="Label" vAlign=top align=left style="width: 364px"><CR:CrystalReportViewer id="crvReportForm" runat="server" AutoDataBind="True" HasViewList="False" HasCrystalLogo="False" HasPrintButton="False" HasExportButton="False" HasToggleGroupTreeButton="False" HasDrillUpButton="False"></CR:CrystalReportViewer> </TD></TR></TBODY></TABLE>&nbsp; </TD></TR></TBODY></TABLE>
</ContentTemplate>
                        <HeaderTemplate>
                            &nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel id="upListMat" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListMat" runat="server" CssClass="modalBox" Width="900px" Visible="False"><TABLE style="WIDTH: 100%"><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Material" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMat" runat="server" Width="100%" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="matcode">Code</asp:ListItem>
<asp:ListItem Value="matlongdesc">Description</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" CssClass="inpText" Width="200px"></asp:TextBox> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:CheckBox id="cbCat01" runat="server" Text="Cat 1 :" Visible="False"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat01" runat="server" CssClass="inpText" Width="150px" AutoPostBack="True" Visible="False"></asp:DropDownList> <asp:CheckBox id="cbCat02" runat="server" Text="Cat 2 :" Visible="False"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat02" runat="server" CssClass="inpText" Width="150px" AutoPostBack="True" Visible="False"></asp:DropDownList> <asp:CheckBox id="cbCat03" runat="server" Text="Cat 3 :" Visible="False"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat03" runat="server" CssClass="inpText" Width="150px" AutoPostBack="True" Visible="False"></asp:DropDownList> <asp:CheckBox id="cbCat04" runat="server" Text="Cat 4 :" Visible="False"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat04" runat="server" CssClass="inpText" Width="150px" AutoPostBack="True" Visible="False"></asp:DropDownList></TD></TR></TABLE></asp:Panel></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNone" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewChecked" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:UpdateProgress id="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat"><ProgressTemplate>
<TABLE style="WIDTH: 200px"><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple">Load Data, Please Wait ...</TD></TR><TR><TD><asp:Image id="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif"></asp:Image></TD></TR></TABLE>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD class="Label" align=left colSpan=3>Show <asp:TextBox id="tbData" runat="server" CssClass="inpText" Width="25px"></asp:TextBox> Data Per Page <asp:LinkButton id="lbShowData" runat="server">>> View</asp:LinkButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat" runat="server" BorderColor="Black" ForeColor="#333333" Width="96%" DataKeyNames="matcode" CellPadding="4" AutoGenerateColumns="False" BorderStyle="None" AllowPaging="True" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:CheckBox id="cbLM" runat="server" ToolTip='<%# eval("matoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="matcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matlongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
&nbsp;
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListMat" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TABLE><ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" PopupDragHandleControlID="lblTitleListMat" PopupControlID="pnlListMat" BackgroundCssClass="modalBackground" Drag="True"></ajaxToolkit:ModalPopupExtender></asp:Panel> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upListTrans" runat="server">
        <contenttemplate>
<asp:Panel id="PanelListTrans" runat="server" CssClass="modalBox" Width="600px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListTrans" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Transform Item"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListTrans" runat="server" Width="100%" DefaultButton="btnFindListTrans"><asp:Label id="Label24" runat="server" Text="Filter :"></asp:Label> <asp:DropDownList id="DDLFilterListTrans" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="transno">Trans No</asp:ListItem>
<asp:ListItem Value="transmstnote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListTrans" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListTrans" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewAllListTrans" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> </asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAllTrans" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneTrans" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedTrans" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3>&nbsp;<asp:GridView id="gvListTrans" runat="server" BorderColor="Black" ForeColor="#333333" Width="98%" DataKeyNames="transmstoid,transno,transdate,transmststatus,transmstnote" CellPadding="4" AutoGenerateColumns="False" BorderStyle="None" AllowPaging="True" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:CheckBox id="cbLMTrans" runat="server" ToolTip='<%# eval("transmstoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="transmstoid" HeaderText="Draft No">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transno" HeaderText="Trans No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transdate" HeaderText="Trans Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>

<asp:BoundField DataField="transmstnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transmststatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
&nbsp;
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListTrans" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbListTrans" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListTrans" runat="server" TargetControlID="btnHiddenListTrans" PopupDragHandleControlID="lblListTrans" PopupControlID="PanelListTrans" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListTrans" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    &nbsp;
    
    
    
    
    
    
    
    
    
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

