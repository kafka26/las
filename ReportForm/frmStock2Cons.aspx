<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="frmStock2Cons.aspx.vb" Inherits="ReportForm_RawMaterialStockIncludeValue" title="" EnableEventValidation="false" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Material Stock Konsinyasi (Inc. Value)" CssClass="Title" ForeColor="SaddleBrown"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <table style="width: 100%">
                                <tr>
                                    <td align="center" valign="top" char="Label">
                                        <table>
                                            <tr>
                                                <td align="center" class="Label" valign="top">
                                                    <asp:UpdatePanel ID="upReportForm" runat="server">
                                                        <ContentTemplate>
<asp:Panel id="pnlReportForm" runat="server" Width="500px" DefaultButton="btnViewReport"><TABLE><TBODY><TR><TD class="Label" align=left><asp:Label id="Label4" runat="server" Text="Business Unit"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpText" Width="300px"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label7" runat="server" Text="Type"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLType" runat="server" CssClass="inpText" Width="105px" AutoPostBack="True"><asp:ListItem>SUMMARY</asp:ListItem>
<asp:ListItem>DETAIL</asp:ListItem>
</asp:DropDownList> <asp:CheckBox id="cbAllowNull" runat="server" Text="Allow Null Stock" Checked="True"></asp:CheckBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label2" runat="server" Text="Material Type"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLTypeMat" runat="server" CssClass="inpText" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="DDLTypeMat_SelectedIndexChanged"><asp:ListItem>FINISH GOOD</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label8" runat="server" Text="Period"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="80px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="lblto" runat="server" Text="-"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="80px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="lblDateFormat" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label> <asp:DropDownList id="DDLMonth" runat="server" CssClass="inpText" Width="105px" Visible="False"></asp:DropDownList> <asp:DropDownList id="DDLYear" runat="server" CssClass="inpText" Width="105px" Visible="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left rowSpan=2><asp:Label id="lblwh" runat="server" Text="Warehouse"></asp:Label></TD><TD class="Label" align=center rowSpan=2><asp:Label id="lblwhsep" runat="server" Text=":"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="DDLWarehouse" runat="server" CssClass="inpText" Width="275px"></asp:DropDownList> <asp:ImageButton id="btnAddWH" runat="server" ImageUrl="~/Images/plus.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:ListBox id="lbWarehouse" runat="server" CssClass="inpTextDisabled" Width="275px" Rows="5"></asp:ListBox> <asp:ImageButton id="btnMinWH" runat="server" ImageUrl="~/Images/minus.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left rowSpan=1></TD><TD class="Label" align=center rowSpan=1></TD><TD class="Label" align=left><asp:CheckBox id="cbCat1" runat="server"></asp:CheckBox>&nbsp;<asp:DropDownList id="FilterDDLCat1" runat="server" CssClass="inpText" Width="112px" AutoPostBack="True" OnSelectedIndexChanged="FilterDDLCat1_SelectedIndexChanged">
            </asp:DropDownList>&nbsp;<asp:CheckBox id="cbCat2" runat="server"></asp:CheckBox>&nbsp;<asp:DropDownList id="FilterDDLCat2" runat="server" CssClass="inpText" Width="112px" AutoPostBack="True" OnSelectedIndexChanged="FilterDDLCat2_SelectedIndexChanged">
            </asp:DropDownList></TD></TR><TR><TD class="Label" align=left rowSpan=1></TD><TD class="Label" align=center rowSpan=1></TD><TD class="Label" align=left><asp:CheckBox id="cbCat3" runat="server"></asp:CheckBox>&nbsp;<asp:DropDownList id="FilterDDLCat3" runat="server" CssClass="inpText" Width="112px">
            </asp:DropDownList></TD></TR><TR><TD class="Label" vAlign=middle align=left><asp:Label id="Label10" runat="server" Text="Material"></asp:Label></TD><TD class="Label" vAlign=middle align=center>:</TD><TD class="Label" vAlign=middle align=left><asp:TextBox id="FilterMaterial" runat="server" CssClass="inpText" Width="255px" Rows="5" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchMat" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClearMat" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" vAlign=middle align=left><asp:Label id="lblCurrency" runat="server" Text="Currency" Visible="False"></asp:Label></TD><TD class="Label" vAlign=middle align=center><asp:Label id="lblneedcurr" runat="server" Text=":" Visible="False"></asp:Label></TD><TD class="Label" vAlign=middle align=left><asp:DropDownList id="ddlcurrency" runat="server" CssClass="inpText" Width="105px" Visible="False">
                <asp:ListItem>IDR</asp:ListItem>
                <asp:ListItem>USD</asp:ListItem>
            </asp:DropDownList></TD></TR><TR><TD class="Label" vAlign=middle align=left><asp:Label id="lblGroupBy" runat="server" Text="Group By" Visible="False"></asp:Label></TD><TD class="Label" vAlign=middle align=center><asp:Label id="septGroupBy" runat="server" Text=":" Visible="False"></asp:Label></TD><TD class="Label" vAlign=middle align=left><asp:DropDownList id="DDLGroupBy" runat="server" CssClass="inpText" Width="105px" Visible="False"><asp:ListItem>WAREHOUSE</asp:ListItem>
<asp:ListItem>MATERIAL</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" vAlign=middle align=left></TD><TD class="Label" vAlign=middle align=center></TD><TD class="Label" vAlign=middle align=left><ajaxToolkit:MaskedEditExtender id="meeDate1" runat="server" TargetControlID="FilterPeriod1" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="ceDate1" runat="server" TargetControlID="FilterPeriod1" PopupButtonID="imbDate1" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeDate2" runat="server" TargetControlID="FilterPeriod2" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="ceDate2" runat="server" TargetControlID="FilterPeriod2" PopupButtonID="imbDate2" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender> </TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnExportToPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnExportToExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="upReportForm"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></DIV></TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnViewReport" />
                                                            <asp:PostBackTrigger ControlID="btnExportToPdf" />
                                                            <asp:PostBackTrigger ControlID="btnExportToExcel" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Label" style="height: 10px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="Label" valign="top">
                                                    <CR:CrystalReportViewer ID="crvReportForm" runat="server" AutoDataBind="True" HasCrystalLogo="False"
                                                        HasDrillUpButton="False" HasExportButton="False" HasPrintButton="False" HasToggleGroupTreeButton="False"
                                                        HasViewList="False" />
                                                </td>
                                            </tr>
                                        </table>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <HeaderTemplate>
                            &nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upListMat" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlListMat" runat="server" CssClass="modalBox" Width="750px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListMat" runat="server" Font-Size="Medium" Font-Bold="True" Font-Underline="False">List Of Material</asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMat" runat="server" Width="100%" DefaultButton="btnFindListMat">
                                    <table style="width: 100%">
                                        <tbody>
                                            <tr>
                                                <td align="center" class="Label" colspan="3">
                                                    Filter :
                                                    <asp:DropDownList ID="FilterDDLListMat" runat="server" CssClass="inpText" Width="100px">
                                                        <asp:ListItem Value="matcode">Code</asp:ListItem>
                                                        <asp:ListItem Value="itemoldcode">Old Code</asp:ListItem>
                                                        <asp:ListItem Value="matlongdesc">Description</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:TextBox ID="FilterTextListMat" runat="server" CssClass="inpText" Width="200px"></asp:TextBox>
                                                    <asp:ImageButton ID="btnFindListMat" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/btnfind.bmp" />
                                                    <asp:ImageButton ID="btnAllListMat" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewall.png" /></td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="Label" colspan="3">
                                                    <asp:CheckBox ID="cbCat01" runat="server" Text="Cat 1 :" />
                                                    <asp:DropDownList ID="DDLCat01" runat="server" AutoPostBack="True" CssClass="inpText"
                                                        Width="200px">
                                                    </asp:DropDownList>
                                                    <asp:CheckBox ID="cbCat02" runat="server" Text="Cat 2 :" />
                                                    <asp:DropDownList ID="DDLCat02" runat="server" AutoPostBack="True" CssClass="inpText"
                                                        Width="200px">
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="Label" colspan="3">
                                                    <asp:CheckBox ID="cbCat03" runat="server" Text="Cat 3 :" />
                                                    <asp:DropDownList ID="DDLCat03" runat="server" AutoPostBack="True" CssClass="inpText"
                                                        Width="200px">
                                                    </asp:DropDownList>
                                                    <asp:CheckBox ID="cbCat04" runat="server" Text="Cat 4 :" />
                                                    <asp:DropDownList ID="DDLCat04" runat="server" CssClass="inpText" Width="200px">
                                                    </asp:DropDownList></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat" runat="server" ForeColor="#333333" Width="99%" PageSize="5" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
                                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="cbHdrLM" runat="server" AutoPostBack="True" OnCheckedChanged="cbHdrLM_CheckedChanged" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbLM" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("matoid") %>' />
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="matcode" HeaderText="Code">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="itemoldcode" HeaderText="Old Code">
                                            <HeaderStyle CssClass="gvpopup" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="matlongdesc" HeaderText="Description">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="matunit" HeaderText="Unit">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                    </Columns>
                                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle CssClass="gvpopup" Font-Bold="True" ForeColor="White" HorizontalAlign="Right" />
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbAddToListMat" runat="server">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" PopupDragHandleControlID="lblListMat" PopupControlID="pnlListMat" Drag="True" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

