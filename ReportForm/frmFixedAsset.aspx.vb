Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Data.OleDb
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_frmFixedAsset
    Inherits System.Web.UI.Page
#Region "Variables"
    Dim Report As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommAND("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure

#End Region

#Region "Functions"
    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub
#End Region

    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(Fixdate1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid", 2)
            Return False
        End If
        If Not IsValidDate(Fixdate2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. ", 2)
            Return False
        End If
        If CDate(Fixdate1.Text) > CDate(Fixdate2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

#Region "Procedures"
    Private Sub bindDataAsset(ByVal sFilter As String)

        sSql = "SELECT DISTINCT fm.cmpcode,g.genoid,g.gencode,g.gendesc,g.gengroup FROM QL_mstgen g INNER JOIN QL_trnfixmst fm ON g.genoid=fm.fixgroup AND g.gengroup='ASSETTYPE' WHERE fm.cmpcode='" & Session("CompnyCode") & "' " & sFilter & ""
        'AND fm.fixdate BETWEEN '" & CDate(toDate(Fixdate1.Text)) & "' AND '" & CDate(toDate(Fixdate2.Text)) & "'"

        'If ordermstoid.Text <> "" Then
        '    sSql &= " AND so.ordermstoid=" & ordermstoid.Text
        'End If
        Dim objTable As DataTable = ckon.ambiltabel(sSql, "QL_trnfixmst")
        Session("tblFAP") = objTable
        GvFAPmst.DataSource = objTable
        GvFAPmst.DataBind()

    End Sub

    Private Sub InitAllDDL()
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE' "
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(ddlOutlet, sSql)
    End Sub

    Sub clearForm()
        CrystalReportViewer1.ReportSource = Nothing
        'GVFixAssets.Visible = False
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        Try
            Dim dat1 As Date = CDate(Fixdate1.Text)
            Dim dat2 As Date = CDate(Fixdate2.Text)
        Catch ex As Exception
            showMessage("Invalid format date !!", 2)
            Exit Sub
        End Try
        If CDate(Fixdate1.Text) > CDate(Fixdate2.Text) Then
            showMessage("Second/End Period must be greater than First Period !!", 2)
            Exit Sub
        End If
        Try
            Dim sWhere As String = ""
            Dim rptName As String = ""
            Dim sPeriode As String = ""
            Dim acctg As String = ""
            Dim oid As String = ""
            Dim startDate As Date = Format(GetServerTime, "yyyy/MM/dd")
            Dim tahunFA As String = Format(CDate(Fixdate1.Text), "yyyy")
            sWhere &= "WHERE fm.cmpcode='" & Session("CompnyCode") & "' AND cr.currcode='" & CurrDDL.SelectedValue & "'"
            If FixAcctgoid.Text <> "" Then
                sWhere &= " AND fm.fixgroup = " & FixAcctgoid.Text & " "
                oid = FixAssets.Text
            Else
                oid = "ALL"
            End If
            sWhere &= "AND g.gendesc LIKE '%" & Tchar(FixAssets.Text.Trim) & "%'"
            If DDLType.SelectedValue = "Detail" Then
                If IsValidPeriod() Then
                    Dim periode1 As String = Format(CDate(Fixdate1.Text), "yyyyMM")
                    Dim periode2 As String = Format(CDate(Fixdate2.Text), "yyyyMM")
                    sPeriode &= "AND fd.fixperiod BETWEEN '" & periode1 & "' AND '" & periode2 & "'"
                Else
                    Exit Sub
                End If
            Else
                sPeriode &= "AND fm.fixdate LIKE '%%'"
            End If

            If DDLType.SelectedValue = "Summary" Then
                If ddlStatus.SelectedValue <> "ALL" Then
                    sWhere &= " AND fm.fixflag='" & ddlStatus.SelectedValue & "'"
                End If
            Else
                If ddlStatusDtl.SelectedValue <> "ALL" Then
                    sWhere &= " AND fd.fixflag='" & ddlStatusDtl.SelectedValue & "' AND fd.fixflag NOT IN ('DISPOSED')"
                End If
            End If

            Report = New ReportDocument
            If DDLType.SelectedValue = "Summary" Then
                If sType = "xls" Then
                    Report.Load(Server.MapPath("~\Report\rptFixAssetsum.rpt"))
                    rptName = "Fixed_Assets_Report_(Sum)_"

                    Report.SetParameterValue("startDate", startDate)
                    Report.SetParameterValue("tahunFA", tahunFA)
                Else
                    Report.Load(Server.MapPath("~\Report\rptFixAssetsum.rpt"))
                    rptName = "Fixed_Assets_Report_(Sum)_"

                    Report.SetParameterValue("startDate", startDate)
                    Report.SetParameterValue("tahunFA", tahunFA)
                End If

            ElseIf DDLType.SelectedValue = "Detail" Then
                If sType = "xls" Then
                    Report.Load(Server.MapPath("~\Report\rptFixAssetdtl.rpt"))
                    rptName = "Fixed_Assets_Report_(Dtl)_"
                Else
                    Report.Load(Server.MapPath("~\Report\rptFixAssetdtl.rpt"))
                    rptName = "Fixed_Assets_Report_(Dtl)_"
                End If
            End If

            Report.SetParameterValue("sCmpcode", ddlOutlet.SelectedItem.Text)
            Report.SetParameterValue("sWhere", sWhere)
            Report.SetParameterValue("sPeriode", sPeriode)
            'Report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
            cProc.SetDBLogonForReport(Report)

            If sType = "crv" Then
                CrystalReportViewer1.DisplayGroupTree = False
                CrystalReportViewer1.ReportSource = Report
            ElseIf sType = "xls" Then
                'Report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "Fixed_Asset_Report" & Format(GetServerTime(), "MM_dd_yy"))
                Report.Close() : Report.Dispose()
                Response.Redirect("~\ReportForm\rptFixedAsset.aspx")
            ElseIf sType = "pdf" Then
                'Report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "Fixed_Asset_Report" & Format(GetServerTime(), "MM_dd_yy"))
                Report.Close()
                Report.Dispose()
                Response.Redirect("~\ReportForm\rptFixedAsset.aspx")
            Else
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub
#End Region

#Region "Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmFixedAsset.aspx")
        End If
        If checkPagePermission("~\ReportForm\frmFixedAsset.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Fixed Assets Report"

        If Not Page.IsPostBack Then
            InitAllDDL()

            CurrDDL.Items.Clear()
            CurrDDL.Items.Add(New ListItem("INDONESIAN RUPIAH", "IDR"))
            CurrDDL.Items.Add(New ListItem("US DOLLAR", "USD"))
            CurrDDL.Items.Add(New ListItem("TRANSACTION RATE", ""))
            CurrDDL.Items(2).Enabled = False
            Fixdate1.Text = Format(GetServerTime, "MM/01/yyyy")
            Fixdate2.Text = Format(GetServerTime, "MM/dd/yyyy")
        Else
        End If

    End Sub

    Protected Sub DDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLType.SelectedIndexChanged
        If DDLType.SelectedValue = "Detail" Then
            Label6.Visible = True
            Label9.Visible = True
            ddlStatusDtl.Visible = True
            ddlStatus.Visible = False
            Label4.Visible = True : Label21.Visible = True
            Fixdate1.Visible = True : Fixdate2.Visible = True
            ibcal1.Visible = True : ibcal2.Visible = True
            Label1.Visible = True : Label3.Visible = True
        Else
            Label6.Visible = False
            Label9.Visible = False
            ddlStatus.Visible = False
            ddlStatusDtl.Visible = False
            Fixdate1.Text = Format(GetServerTime, "MM/01/yyyy")
            Fixdate2.Text = Format(GetServerTime, "MM/dd/yyyy")
            Label4.Visible = False : Label21.Visible = False
            Fixdate1.Visible = False : Fixdate2.Visible = False
            ibcal1.Visible = False : ibcal2.Visible = False
            Label1.Visible = False : Label3.Visible = False
        End If
    End Sub

    Protected Sub ibClear_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("~\reportForm\rptFixedAsset.aspx")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not Report Is Nothing Then
                If Report.IsLoaded Then
                    Report.Dispose()
                    Report.Close()
                End If
            End If
        Catch ex As Exception
            Report.Dispose()
            Report.Close()
        End Try
    End Sub

    Protected Sub CrystalReportViewer1_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles CrystalReportViewer1.Navigate
        ShowReport("crv")
    End Sub

    Protected Sub imbClearAcctg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FixAssets.Text = ""
        'sjjualmstoid.Text = ""
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterFAP.Text = ""
        GvFAPmst.SelectedIndex = -1
        bindDataAsset("")
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnViewPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ShowReport("crv")
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ShowReport("pdf")
    End Sub

    Protected Sub ExportToXls_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ExportToXls.Click
        ShowReport("xls")
    End Sub

    Protected Sub ibClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibClear.Click
        Response.Redirect("~\reportForm\frmFixedAsset.aspx")
    End Sub

#End Region

    Protected Sub GvFAPmst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GvFAPmst.PageIndex = e.NewPageIndex
        FillGVAcctg(GvFAPmst, New String() {"VAR_ASSETS"}, "AND (a.acctgcode LIKE '%" & Tchar(FixAssets.Text) & "%' OR a.acctgdesc LIKE '%" & Tchar(FixAssets.Text.Trim) & "%') ")
        bindDataAsset("")
        mpeListSupp.Show()
        'GVFixAssets.Visible = True
        'bindDataAcct()
    End Sub

    Protected Sub GvFAPmst_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FixAcctgoid.Text = GvFAPmst.SelectedDataKey("genoid").ToString().Trim
        FixAssets.Text = GvFAPmst.SelectedDataKey("gendesc").ToString().Trim
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub btnAllListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListSupp.Click
        FilterFAP.Text = ""
        bindDataAsset("")
        mpeListSupp.Show()
    End Sub

    Protected Sub btnFindListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSupp.Click
        Dim sFilter As String = " AND " & FilterFAPDDL.SelectedValue & " = '" & Tchar(FilterFAP.Text) & "'"
        bindDataAsset(sFilter)
        mpeListSupp.Show()
    End Sub

    Protected Sub lkbCloseListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListSupp.Click
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub
End Class
