Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_frmPI
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function ValidateParam() As String
        Dim sReturn As String = ""
        Dim sErrTemp As String = ""

        If Not IsValidDate(txtStart.Text, "MM/dd/yyyy", sErrTemp) Then sReturn &= "- Invalid Start Date format: " & sErrTemp & ".<BR>"
        If Not IsValidDate(txtFinish.Text, "MM/dd/yyyy", sErrTemp) Then sReturn &= "- Invalid End Date format: " & sErrTemp & ".<BR>"
        If IsValidDate(txtStart.Text, "MM/dd/yyyy", sErrTemp) And IsValidDate(txtFinish.Text, "MM/dd/yyyy", sErrTemp) Then
            If DateDiff(DateInterval.Day, CDate(txtStart.Text), CDate(txtFinish.Text)) < 0 Then
                sReturn &= "- End Date must be greater than Start Date.<BR>"
            End If
        End If

        Return sReturn
    End Function

    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(txtStart.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(txtFinish.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(txtStart.Text) > CDate(txtFinish.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetIndoMonth(ByVal sVal As String) As String
        Select Case sVal
            Case "01"
                Return "JANUARI"
            Case "02"
                Return "FEBRUARI"
            Case "03"
                Return "MARET"
            Case "04"
                Return "APRIL"
            Case "05"
                Return "MEI"
            Case "06"
                Return "JUNI"
            Case "07"
                Return "JULI"
            Case "08"
                Return "AGUSTUS"
            Case "09"
                Return "SEPTEMBER"
            Case "10"
                Return "OKTOBER"
            Case "11"
                Return "NOVEMBER"
            Case "12"
                Return "DESEMBER"
            Case Else
                Return "Unknown"
        End Select
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindDataPO(ByVal sFilter As String)

        sSql = "SELECT DISTINCT pom.pomstoid, pom.pono, CONVERT(VARCHAR(10), pom.podate, 101) AS podate, pom.posuppref, pom.potaxtype,(CASE g.gencode WHEN 'RAW' THEN 'RM' WHEN 'GEN' THEN 'GM' ELSE g.gencode END) AS pogroup, pom.pomstnote, pom.curroid, s.suppoid, s.suppname, pom.potype AS poflag, pom.pogroup AS potype, (CASE WHEN pom.potaxtype_inex='INC' THEN 'INCLUSIVE' WHEN pom.potaxtype_inex='EXC' THEN 'EXCLUSIVE' ELSE 'NONTAX' END) AS potaxtype, potaxamt, povat FROM QL_trnpomst pom INNER JOIN QL_trnpodtl pod ON pom.pomstoid=pod.pomstoid INNER JOIN QL_trnmrmst mrm ON mrm.pomstoid=pom.pomstoid INNER JOIN QL_mstgen g ON g.gencode=pom.pogroup INNER JOIN QL_mstsupp s ON s.suppoid=pom.suppoid WHERE pom.cmpcode='" & FilterDDLDiv.SelectedValue & "' " & sFilter & ""

        Dim objTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnpomst")
        Session("PO") = objTbl
        gvListReg.DataSource = objTbl
        gvListReg.DataBind()
    End Sub

    Private Sub BindDataPI(ByVal sTeh As String)

        sSql = "SELECT bm.cmpcode,bm.trnbelimstoid,trnbelino,pom.pomstoid,CONVERT(VarChar(10),bm.trnbelidate,101) AS trnbelidate,bm.trnbelitype,sp.suppoid,sp.suppname,bm.trnbelimststatus,bm.trnbelimsttaxtype FROM QL_trnbelimst bm INNER JOIN QL_trnpomst pom ON pom.pomstoid=bm.pomstoid AND pom.cmpcode=bm.cmpcode INNER JOIN ql_mstsupp sp ON sp.suppoid=bm.suppoid WHERE bm.cmpcode='" & FilterDDLDiv.SelectedValue & "' " & sTeh & " AND pom.pono LIKE '%" & Tchar(pono.Text) & "%'"

        Dim objTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnbelimst")
        Session("PI") = objTbl
        gvListAP.DataSource = objTbl
        gvListAP.DataBind()
    End Sub

    Private Sub BindSupplierData()
        sSql = "SELECT 0 selected,suppoid,suppcode,suppname,suppaddr FROM QL_mstsupp WHERE activeflag='ACTIVE' AND " & FilterDDLListSupp.SelectedValue & " LIKE '%" & Tchar(FilterTextListSupp.Text) & "%'"
        Dim ObjSupp As DataTable = cKon.ambiltabel(sSql, "QL_MstSupp")
        Session("Supp") = ObjSupp
        gvListSupp.DataSource = ObjSupp
        gvListSupp.DataBind()
        '(suppcode LIKE '%" & Tchar(FilterTextSupplier.Text) & "%' OR suppname LIKE '%" & Tchar(FilterTextSupplier.Text) & "%') ORDER BY suppname"
        'Dim dtSupp As DataTable = cKon.ambiltabel(sSql, "QL_mstsupp")
        'gvListSupp.DataSource = dtSupp
        'gvSupplier.DataBind()
        'Session("QL_mstsupp") = dtSupp
        'gvListSupp.Visible = True
        'gvListSupp.SelectedIndex = -1
    End Sub

    Private Sub UpdateCheckedGV()
        If Not Session("QL_mstsupp") Is Nothing Then
            Dim dtab As DataTable = Session("QL_mstsupp")

            If dtab.Rows.Count > 0 Then
                Dim cb As System.Web.UI.WebControls.CheckBox
                Dim dView As DataView = dtab.DefaultView
                Dim drView As DataRowView

                For i As Integer = 0 To gvSupplier.Rows.Count - 1
                    cb = gvSupplier.Rows(i).FindControl("chkSelect")
                    dView.RowFilter = "suppoid = " & cb.ToolTip
                    drView = dView.Item(0)
                    drView.BeginEdit()
                    If cb.Checked = True Then
                        drView("selected") = 1
                    Else
                        drView("selected") = 0
                    End If
                    drView.EndEdit()
                    dView.RowFilter = ""
                Next
                dtab.AcceptChanges()
            End If
            Session("QL_mstsupp") = dtab
        End If
    End Sub

    Private Sub BindListMat()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, bd.itemoid AS matoid, m.itemShortDescription AS matshortdesc, m.itemLongDescription AS matlongdesc, m.itemCode AS matcode, m.itemoldcode AS oldcode FROM QL_trnbelidtl bd " & _
        " INNER JOIN QL_trnbelimst bm ON bm.cmpcode=bd.cmpcode" & _
        " AND bm.trnbelimstoid=bd.trnbelimstoid" & _
        " INNER JOIN QL_trnpomst po ON po.pomstoid=bm.pomstoid AND po.cmpcode=bm.cmpcode " & _
        " INNER JOIN QL_mstitem m ON m.itemoid=bd.itemoid" & _
        " INNER JOIN QL_mstgen g2 ON g2.genoid=bd.trnbelidtlunitoid " & _
        " INNER JOIN QL_mstcurr c ON c.curroid=bm.curroid" & _
        " WHERE bm.cmpcode='" & FilterDDLDiv.SelectedValue & "' "
        sSql &= " AND bm.trnbelidate > ='" & txtStart.Text & " 00:00:00' AND bm.trnbelidate < ='" & txtFinish.Text & " 23:59:59'"
        If PomstOid.Text <> "" Then
            sSql &= " AND po.pomstoid=" & PomstOid.Text & ""
        End If
        If BeliMstOid.Text <> "" Then
            sSql &= "  AND bm.trnbelimstoid=" & BeliMstOid.Text & ""
        End If

        Session("TblMat") = cKon.ambiltabel(sSql, "QL_mstitem")
    End Sub 'OK

    Public Sub SetReportPageSize(ByVal mPaperSize As String, ByVal PaperOrientation As Integer)
        Try
            Dim ObjPrinterSetting As New System.Drawing.Printing.PrinterSettings
            Dim PkSize As New System.Drawing.Printing.PaperSize
            ObjPrinterSetting.PrinterName = "Microsoft XPS Document Writer"
            For i As Integer = 0 To ObjPrinterSetting.PaperSizes.Count - 1
                If ObjPrinterSetting.PaperSizes.Item(i).PaperName = mPaperSize.Trim Then
                    PkSize = ObjPrinterSetting.PaperSizes.Item(i)
                    Exit For
                End If
            Next

            If PkSize IsNot Nothing Then
                Dim myAppPrintOptions As CrystalDecisions.CrystalReports.Engine.PrintOptions = report.PrintOptions
                myAppPrintOptions.PrinterName = "Microsoft XPS Document Writer"
                myAppPrintOptions.PaperSize = CType(PkSize.RawKind, CrystalDecisions.Shared.PaperSize)
                report.PrintOptions.PaperOrientation = IIf(PaperOrientation = 1, CrystalDecisions.Shared.PaperOrientation.Portrait, CrystalDecisions.Shared.PaperOrientation.Landscape)

            End If
            PkSize = Nothing
        Catch ex As Exception
            showMessage(ex.Message, 2)
            Exit Sub
        End Try
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        Try
            Dim dat1 As Date = CDate(txtStart.Text)
            Dim dat2 As Date = CDate(txtFinish.Text)
        Catch ex As Exception
            showMessage("Invalid format date !!", 2)
            Exit Sub
        End Try
        If CDate(txtStart.Text) > CDate(txtFinish.Text) Then
            showMessage("Second/End Period must be greater than First Period !!", 2)
            Exit Sub
        End If
        Try
            Dim sWhere As String = ""
            sWhere &= " WHERE bm.cmpcode ='" & FilterDDLDiv.SelectedValue & "' AND cr.currcode='" & FilterCurrency.SelectedValue & "' AND bm.trnbelidate BETWEEN '" & CDate(txtStart.Text) & " 00:00:00' AND '" & CDate(txtFinish.Text) & " 23:59:59'"

            If PomstOid.Text <> "" Then
                sWhere &= " AND pom.pono ='" & Tchar(pono.Text) & "' AND pom.pomstoid='" & PomstOid.Text & "'"
            Else
                sWhere &= " AND pom.pono LIKE '%%'"
            End If
            If BeliMstOid.Text <> "" Then
                sWhere &= " AND bm.trnbelimstoid ='" & BeliMstOid.Text & "' AND bm.trnbelino ='" & Tchar(BeliNo.Text) & "' "
            Else
                sWhere &= " AND bm.trnbelino LIKE '%%'"
            End If
            If suppoid.Text <> "" Then
                sWhere &= "AND sp.suppoid ='" & suppoid.Text & "' AND sp.suppname ='" & Tchar(FilterTextSupplier.Text) & "' "
            Else
                sWhere &= "AND sp.suppname LIKE '%%'"
            End If
            If StatusDDL.SelectedValue <> "ALL" Then
                sWhere &= "AND bm.trnbelimststatus='" & StatusDDL.SelectedValue & "'"
            Else
                sWhere &= "AND bm.trnbelimststatus LIKE '%'"
            End If
            If DDLTaxType.SelectedValue <> "ALL" Then
                If DDLTaxType.SelectedValue = "TAX" Then
                    sWhere &= " AND trnbelimsttaxamt > 0"
                Else
                    sWhere &= " AND trnbelimsttaxamt <= 0"
                End If
            End If
            If FilterType.SelectedValue.ToUpper = "SUMMARY" Then
                sSql = "SELECT bm.cmpcode,div.divname,pom.pomstoid,pom.podate AS [tgl PO],pom.pono AS [No Po],sp.suppoid,sp.suppname AS [Supp. Name], bm.trnbelidate AS [Tgl Nota],bm.trnbelimstoid,bm.trnbelino AS [No. Nota],bm.curroid,cr.currcode AS Currency,bm.trnbeliamt,ISNULL(bm.trnbelidiscamt,0) AS Diskon, bm.trnbelimsttaxamt AS PPn,ISNULL(bm.trnbeliamtnetto-bm.trnbelimsttaxamt,0) AS DPP,ISNULL(bm.trnbeliamtnetto,0) AS Netto,bm.trnbelimststatus AS Flag,(SELECT SUM(pd.trnbelidtlamtnetto) FROM QL_trnbelidtl pd WHERE bm.trnbelimstoid=pd.trnbelimstoid )+ (SELECT SUM(pd.trnbelidtltaxamt) FROM QL_trnbelidtl pd WHERE bm.trnbelimstoid=pd.trnbelimstoid ) AS TotalNet, ISNULL(trnbelimstres3,'') fakturpajakno " & _
                      " FROM QL_trnbelimst bm " & _
                      " INNER JOIN QL_trnpomst pom ON pom.pomstoid=bm.pomstoid " & _
                      " AND pom.cmpcode=bm.cmpcode " & _
                      " INNER JOIN QL_mstdivision div ON div.divcode=bm.cmpcode " & _
                      " INNER JOIN QL_mstsupp sp ON sp.suppoid=bm.suppoid " & _
                      " INNER JOIN QL_mstcurr cr ON cr.curroid=bm.curroid " & _
                      "" & sWhere & " GROUP BY bm.cmpcode,div.divname,pom.pomstoid,pom.podate,pom.pono,sp.suppoid,sp.suppname, bm.trnbelidate,bm.trnbelimstoid,bm.trnbelino ,bm.curroid,cr.currcode,bm.trnbelidiscamt, bm.trnbelimsttaxamt,bm.trnbeliamtnetto,bm.trnbeliamt,bm.trnbelimststatus, trnbelimstres3 "
                Dim dt As DataTable = cKon.ambiltabel(sSql, "tbldata")
                Dim nFile As String = ""

                If sType = "Print Excel" Then
                    nFile = Server.MapPath(folderReport & "rptPISumXls.rpt")
                Else
                    nFile = Server.MapPath(folderReport & "rptPISum.rpt")
                End If
                report.Load(nFile)
                dt = cKon.ambiltabel(sSql, "tbldata")
                report.SetDataSource(dt)
                report.SetParameterValue("sPeriod", Format(CDate(txtStart.Text), "MM/dd/yyyy") & " - " & Format(CDate(txtFinish.Text), "MM/dd/yyyy"))
                report.Load(nFile)

                Dim strLastModified As String = System.IO.File.GetLastWriteTime(nFile.ToString())
                cProc.SetDBLogonForReport(report)
            ElseIf FilterType.SelectedValue.ToUpper = "PP" Then
                sSql = "select * from dbo.getLaporanPembelianNew('" & Format(CDate(txtStart.Text), "MM/dd/yyyy") & "', '" & Format(CDate(txtFinish.Text), "MM/dd/yyyy") & "') t where 1=1"
                If PomstOid.Text <> "" Then
                    sSql &= " AND t.pono ='" & Tchar(pono.Text) & "' AND t.pomstoid='" & PomstOid.Text & "'"
                Else
                    sSql &= " AND t.pono LIKE '%%'"
                End If
                If BeliMstOid.Text <> "" Then
                    sSql &= " AND t.trnbelimstoid ='" & BeliMstOid.Text & "' AND bm.trnbelino ='" & Tchar(BeliNo.Text) & "' "
                Else
                    sSql &= " AND t.trnbelino LIKE '%%'"
                End If
                If suppoid.Text <> "" Then
                    sSql &= "AND t.suppoid ='" & suppoid.Text & "' AND sp.suppname ='" & Tchar(FilterTextSupplier.Text) & "' "
                Else
                    sSql &= "AND t.suppname LIKE '%%'"
                End If
                If StatusDDL.SelectedValue <> "ALL" Then
                    sSql &= "AND t.trnbelimststatus='" & StatusDDL.SelectedValue & "'"
                Else
                    sSql &= "AND t.trnbelimststatus LIKE '%'"
                End If
                If DDLTaxType.SelectedValue <> "ALL" Then
                    If DDLTaxType.SelectedValue = "TAX" Then
                        sSql &= " AND trnbelimsttaxamt > 0"
                    Else
                        sSql &= " AND trnbelimsttaxamt <= 0"
                    End If
                End If
                sSql += " order by trnbelidate, trnbelino"
                Dim dt As DataTable = cKon.ambiltabel(sSql, "tbldata")
                Dim nFile As String = ""

                If sType = "Print Excel" Then
                    nFile = Server.MapPath(folderReport & "rptPembelianNew.rpt")
                Else
                    nFile = Server.MapPath(folderReport & "rptPembelianNew.rpt")
                End If
                report.Load(nFile)
                dt = cKon.ambiltabel(sSql, "tbldata")
                report.SetDataSource(dt)
                report.SetParameterValue("periode", Format(CDate(txtStart.Text), "dd/MM/yyyy") & " - " & Format(CDate(txtFinish.Text), "dd/MM/yyyy"))
                report.SetParameterValue("user_id", Session("UserID"))
                report.Load(nFile)
                report.PrintOptions.PaperSize = System.Drawing.Printing.PaperKind.LetterExtra
            Else
                Dim sMat As String = ""
                If matcode.Text <> "" Then
                    Dim sMatcode() As String = Split(matcode.Text, ";")
                    sMat &= " AND ("
                    For c1 As Integer = 0 To sMatcode.Length - 1
                        sMat &= " i.itemCode LIKE '%" & Tchar(sMatcode(c1)) & "%'"
                        If c1 < sMatcode.Length - 1 Then
                            sMat &= " OR"
                        End If
                    Next
                    sMat &= ")"
                End If
                sSql = "SELECT cmpcode,[Tgl PI],[No. PI],[No. PO], [No. LPB],[Nama Supplier],[PO Date],[Kode Barang],[oldcode],[Nama Barang],[Satuan],[Qty],[SubtotalNet],[Note],DiscH,[trnbelinote],trnbelimsttaxtype,curroid,currcode,DPP,PPN , Netto,FlagMst,Flagdtl,[hrg Item],NetDtl,GrandTotal,AmtNett,DtlDiscAmt FROM (" & _
             " SELECT bm.cmpcode,bm.trnbelidate AS [Tgl PI],bm.trnbelino AS [No. PI],pom.pono AS [No. PO],mrm.mrno AS [No. LPB],sp.suppname AS [Nama Supplier],pom.podate AS [PO Date],i.itemcode AS [Kode Barang],i.itemoldcode AS [oldcode],i.itemLongDescription AS [Nama Barang],u.gendesc AS [Satuan],pid.trnbelidtlqty AS [Qty],(pid.trnbelidtlamtnetto+(Case bm.trnbelimsttaxtype When 'INCLUSIVE' Then (pid.trnbelidtlamtnetto*10/100) Else pid.trnbelidtltaxamt End)) AS [SubtotalNet],pid.trnbelidtlnote AS [Note],bm.trnbelidiscamt AS DiscH,bm.trnbelimstnote AS [trnbelinote],bm.trnbelimsttaxtype,cr.curroid,cr.currcode,pid.trnbelidtlamtnetto AS DPP,Case bm.trnbelimsttaxtype When 'INCLUSIVE' Then (pid.trnbelidtlamtnetto*10/100) Else pid.trnbelidtltaxamt End PPN ,bm.trnbeliamtnetto AS Netto,bm.trnbelimststatus AS FlagMst,pid.trnbelidtlstatus AS Flagdtl,pid.trnbelidtlprice AS [hrg Item],pid.trnbelidtlamtnetto + ((Case bm.trnbelimsttaxtype When 'INCLUSIVE' Then (pid.trnbelidtlamtnetto*10/100) Else pid.trnbelidtltaxamt End)) AS NetDtl,0.0 AS GrandTotal, bm.trnbeliamt+bm.trnbelimsttaxamt AS AmtNett,pid.trnbelidtldiscamt/pid.trnbelidtlqty AS DtlDiscAmt, ISNULL(trnbelimstres3,'') fakturpajakno" & _
             " FROM QL_trnbelimst bm " & _
             " INNER JOIN QL_trnbelidtl pid ON pid.trnbelimstoid=bm.trnbelimstoid" & _
             " AND pid.cmpcode=bm.cmpcode" & _
             " INNER JOIN QL_trnpomst pom ON bm.pomstoid=pom.pomstoid" & _
             " AND bm.cmpcode=pom.cmpcode " & _
             " INNER JOIN QL_trnmrmst mrm ON mrm.mrmstoid=pid.mrmstoid" & _
             " INNER JOIN QL_mstsupp sp ON sp.suppoid=bm.suppoid " & _
             " INNER JOIN QL_mstitem i on i.itemoid = pid.itemoid  " & _
             " INNER JOIN QL_mstgen u on u.genoid = pid.trnbelidtlunitoid " & _
             " INNER JOIN QL_mstcurr cr ON cr.curroid=bm.curroid " & sWhere & " " & sMat & " ) AS TblPI ORDER BY [No. PI]"
                Dim dt As DataTable = cKon.ambiltabel(sSql, "tbldata")
                For C1 As Integer = 0 To dt.Rows.Count - 1
                    dt.Rows(C1).Item("GrandTotal") = dt.Compute("SUM(NetDtl)", "[No. PI]='" & dt.Rows(C1).Item("No. PI").ToString & "'")
                Next
                dt.AcceptChanges()
                Dim nFile As String = ""

                If sType = "Print Excel" Then
                    nFile = Server.MapPath(folderReport & "rptPIDtl.rpt")
                Else
                    nFile = Server.MapPath(folderReport & "rptPIDtl.rpt")
                End If
                report.Load(nFile)
                Dim dv As DataView = dt.DefaultView
                report.SetDataSource(dv.ToTable)
                'Else
                If sType = "Print Excel" Then
                    If FilterCurrency.SelectedIndex <> "2" Then
                        nFile = Server.MapPath(folderReport & "rptPIDtl.rpt")
                    Else
                        nFile = Server.MapPath(folderReport & "rptPIDtl.rpt")
                    End If
                Else
                    If FilterCurrency.SelectedIndex <> "2" Then
                        nFile = Server.MapPath(folderReport & "rptPIDtl.rpt")
                    Else
                        nFile = Server.MapPath(folderReport & "rptPIDtl.rpt")
                    End If
                End If
                report.SetParameterValue("sPeriod", Format(CDate(txtStart.Text), "MM/dd/yyyy") & " - " & Format(CDate(txtFinish.Text), "MM/dd/yyyy"))
                report.Load(nFile)
                ' End If

                Dim strLastModified As String = System.IO.File.GetLastWriteTime(nFile.ToString())
                cProc.SetDBLogonForReport(report)
			End If

            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "ReportPI_" & FilterDDLMonth.SelectedItem.Text.ToUpper & FilterDDLYear.SelectedItem.Text)
                Catch ex As Exception
                    report.Close() : report.Dispose()
                End Try
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "ReportPI_" & FilterDDLMonth.SelectedItem.Text.ToUpper & FilterDDLYear.SelectedItem.Text)
                Catch ex As Exception
                    report.Close() : report.Dispose()
                End Try
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.ToString, 1)
        End Try
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmPI.aspx")
        End If
        If checkPagePermission("~\ReportForm\frmPI.aspx", Session("Role")) = False Then
            ' Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Purchase Inoice Report"
        If Not Page.IsPostBack Then
            ' DDL Division
            sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE' "
            If Session("CompnyCode") <> CompnyCode Then
                sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
            End If
            FillDDL(FilterDDLDiv, sSql)

            FilterDDLMonth.Items.Clear()
            For C1 As Int16 = 1 To 12
                FilterDDLMonth.Items.Add(MonthName(C1))
                FilterDDLMonth.Items(FilterDDLMonth.Items.Count - 1).Value = Format(C1, "00")
            Next
            FilterDDLMonth.SelectedValue = Format(Now.Month, "00")
            FilterDDLYear.Items.Clear()
            For C1 As Int16 = Now.Year - 2 To Now.Year + 2
                FilterDDLYear.Items.Add(C1)
                FilterDDLYear.Items(FilterDDLYear.Items.Count - 1).Value = C1
            Next
            FilterDDLYear.SelectedValue = Now.Year

            FilterCurrency.Items.Clear()
            FilterCurrency.Items.Add(New ListItem("INDONESIAN RUPIAH", "IDR"))
            FilterCurrency.Items.Add(New ListItem("US DOLLAR", "USD"))
            FilterCurrency.Items.Add(New ListItem("TRANSACTION RATE", ""))
            FilterCurrency.Items(2).Enabled = False

            FilterDDLCOA.Items.Clear()
            sSql = "SELECT DISTINCT a.acctgoid, acctgdesc FROM QL_conap con INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE payrefoid=0"
            FillDDLWithAdditionalText(FilterDDLCOA, sSql, "None", "0")

            pnlSupplier.CssClass = "popupControl"
            txtStart.Text = Format(GetServerTime, "MM/01/yyyy")
            txtFinish.Text = Format(GetServerTime, "MM/dd/yyyy")
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
    End Sub

    Protected Sub FilterDDLDiv_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLDiv.SelectedIndexChanged
        rbSupplier.SelectedIndex = 0 : rbSupplier_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub FilterType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterType.SelectedIndexChanged
        If FilterType.SelectedValue = "SUMMARY" Then
            imbFindMat.Visible = False : imbEraseMat.Visible = False
            matcode.Text = ""
        Else
            imbFindMat.Visible = True : imbEraseMat.Visible = True
        End If
        'If (FilterType.SelectedIndex = 0) Then
        '    FilterDDLMonth.Visible = True : FilterDDLYear.Visible = True
        '    txtStart.Visible = False : imbStart.Visible = False : lblTo.Visible = False
        '    txtFinish.Visible = False : imbFinish.Visible = False : lblMMDD.Visible = False
        'Else
        '    FilterDDLMonth.Visible = False : FilterDDLYear.Visible = False
        '    txtStart.Visible = True : imbStart.Visible = True : lblTo.Visible = True
        '    txtFinish.Visible = True : imbFinish.Visible = True : lblMMDD.Visible = True
        'End If
        'FilterCurrency.Items(2).Enabled = (FilterType.SelectedIndex = 1)
    End Sub

    Protected Sub rbSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbSupplier.SelectedIndexChanged
        'gvSupplier.DataSource = Nothing : gvSupplier.DataBind()
        'Session("QL_mstsupp") = Nothing : gvSupplier.Visible = False
        'If rbSupplier.SelectedValue = "ALL" Then
        '    FilterTextSupplier.Visible = False
        '    btnSearchSupplier.Visible = False
        '    btnClearSupplier.Visible = False
        'Else
        '    FilterTextSupplier.Visible = True
        '    btnSearchSupplier.Visible = True
        '    btnClearSupplier.Visible = True
        'End If
    End Sub

    Protected Sub btnSearchSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupplier.Click
        FilterDDLListSupp.SelectedIndex = -1
        FilterTextListSupp.Text = ""
        gvListSupp.SelectedIndex = -1
        BindSupplierData()
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
    End Sub

    Protected Sub btnClearSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupplier.Click
        FilterTextSupplier.Text = ""
        suppoid.Text = ""
        gvListSupp.DataSource = Nothing
        gvListSupp.DataBind()
        'gvSupplier.Visible = False
    End Sub

    Protected Sub gvSupplier_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupplier.PageIndexChanging
        UpdateCheckedGV()

        gvSupplier.PageIndex = e.NewPageIndex
        Dim dtSupp As DataTable = Session("QL_mstsupp")
        gvSupplier.DataSource = dtSupp
        gvSupplier.DataBind()
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        UpdateCheckedGV()
        Dim sMsg As String = ValidateParam()
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        Else
            ShowReport("Print PDF")
        End If
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        UpdateCheckedGV()
        Dim sMsg As String = ValidateParam()
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        Else
            ShowReport("Print Excel")
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmPI.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Dispose() : report.Close()
    End Sub
#End Region

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        FilterTextListReg.Text = ""
        FilterDDLListReg.SelectedIndex = -1
        gvListReg.SelectedIndex = -1
        BindDataPO("")
        cProc.SetModalPopUpExtender(btnHideListReg, pnlListReg, mpeListReg, True)
    End Sub

    Protected Sub gvListReg_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListReg.PageIndexChanging
        gvListReg.PageIndex = e.NewPageIndex
        BindDataPO("")
        mpeListReg.Show()
    End Sub

    Protected Sub gvListReg_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListReg.SelectedIndexChanged
        PomstOid.Text = gvListReg.SelectedDataKey.Item("pomstoid").ToString
        pono.Text = gvListReg.SelectedDataKey.Item("pono").ToString
        cProc.SetModalPopUpExtender(btnHideListReg, pnlListReg, mpeListReg, False)
    End Sub

    Protected Sub lkbCloseListReg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListReg.Click
        cProc.SetModalPopUpExtender(btnHideListReg, pnlListReg, mpeListReg, False)
    End Sub

    Protected Sub ImbErasePO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImbErasePO.Click
        PomstOid.Text = ""
        pono.Text = ""
    End Sub

    Protected Sub btnFindListReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListReg.Click
        Dim sFilter As String = " AND " & FilterDDLListReg.SelectedValue & " LIKE '%" & Tchar(FilterTextListReg.Text) & "%' AND pom.podate BETWEEN '" & CDate(txtStart.Text) & " 00:00:00' AND '" & CDate(txtFinish.Text) & " 23:59:59'"
        BindDataPO(sFilter)
        mpeListReg.Show()
    End Sub

    Protected Sub btnAllListReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListReg.Click
        FilterDDLListReg.SelectedIndex = -1
        FilterTextListReg.Text = ""
        gvListReg.SelectedIndex = -1
        BindDataPO("")
        mpeListReg.Show()
    End Sub

    Protected Sub BtnSearchPI_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnSearchPI.Click
        FilterDDLListAP.SelectedIndex = -1
        FilterTextListAP.Text = ""
        gvListAP.SelectedIndex = -1
        BindDataPI("")
        cProc.SetModalPopUpExtender(btnHideListAP, pnlListAP, mpeListAP, True)
    End Sub

    Protected Sub BtnClearPI_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnClearPI.Click
        BeliNo.Text = ""
        BeliMstOid.Text = ""
    End Sub

    Protected Sub gvListAP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListAP.SelectedIndexChanged
        BeliMstOid.Text = gvListAP.SelectedDataKey.Item("trnbelimstoid").ToString
        BeliNo.Text = gvListAP.SelectedDataKey.Item("trnbelino").ToString
        cProc.SetModalPopUpExtender(btnHideListAP, pnlListAP, mpeListAP, False)
    End Sub

    Protected Sub gvListAP_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListAP.PageIndexChanging
        gvListAP.PageIndex = e.NewPageIndex
        BindDataPI("")
        mpeListAP.Show()
    End Sub

    Protected Sub lkbCloseListAP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListAP.Click
        cProc.SetModalPopUpExtender(btnHideListAP, pnlListAP, mpeListAP, False)
    End Sub

    Protected Sub btnFindListAP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListAP.Click
        Dim sTeh As String = "AND bm.trnbelidate BETWEEN '" & CDate(txtStart.Text) & " 00:00:00' AND '" & CDate(txtFinish.Text) & " 23:59:59' AND " & FilterDDLListAP.SelectedValue & " LIKE '%" & Tchar(FilterTextListAP.Text) & "%'"
        BindDataPI(sTeh)
        mpeListAP.Show()
    End Sub

    Protected Sub btnAllListAP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListAP.Click
        BindDataPI("")
        mpeListAP.Show()
    End Sub

    Protected Sub btnFindListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSupp.Click
        BindSupplierData()
        mpeListSupp.Show()
    End Sub

    Protected Sub btnAllListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListSupp.Click
        FilterDDLListSupp.SelectedIndex = -1 : FilterTextListSupp.Text = "" : gvListSupp.SelectedIndex = -1
        BindSupplierData()
        mpeListSupp.Show()
    End Sub

    Protected Sub gvListSupp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListSupp.SelectedIndexChanged
        suppoid.Text = gvListSupp.SelectedDataKey.Item("suppoid").ToString
        FilterTextSupplier.Text = gvListSupp.SelectedDataKey.Item("suppname").ToString
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub gvListSupp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSupp.PageIndexChanging
        gvListSupp.PageIndex = e.NewPageIndex
        BindSupplierData()
        mpeListSupp.Show()
    End Sub

    Protected Sub lkbCloseListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub imbFindMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindMat.Click
        If IsValidPeriod() Then
            FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
            'InitFilterDDLCat1()
            Session("TblMat") = Nothing
            'Session("TblMatView") = Nothing
            gvListMat.DataSource = Nothing : gvListMat.DataBind()
            tbData.Text = "5" : gvListMat.PageSize = CInt(tbData.Text)
            'BindListMat()
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbEraseMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        matcode.Text = ""
        cbType.Checked = False
        'cbType_CheckedChanged(Nothing, Nothing)
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        If UpdateCheckedMat() Then
            BindListMat()
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matoid=" & dtTbl.Rows(C1)("matoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matoid=" & dtTbl.Rows(C1)("matoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Selected material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If matcode.Text <> "" Then
                            matcode.Text &= ";" + vbCrLf + dtView(C1)("matcode")
                        Else
                            matcode.Text = dtView(C1)("matcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some Material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub
End Class
