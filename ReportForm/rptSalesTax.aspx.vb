Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO

Partial Class reportForm_rptSalesTaxReport
	Inherits System.Web.UI.Page

#Region "Variables"
    Dim Report As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommAND("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
	Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure


#End Region

#Region "Functions"
    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
	End Function

	Private Function IsValidPeriod() As Boolean

		Dim sErr As String = ""
		If Not IsValidDate(invdate1.Text, "MM/dd/yyyy", sErr) Then
			showMessage("Your Period 1 is invalid. " & sErr, 2)
			Return False
		End If
		If Not IsValidDate(invdate2.Text, "MM/dd/yyyy", sErr) Then
			showMessage("Your Period 2 is invalid. " & sErr, 2)
			Return False
		End If
		If CDate(invdate1.Text) > CDate(invdate2.Text) Then
			showMessage("Period 2 Must be more than period 1" & sErr, 2)
			Return False
		End If
		Return True
	End Function
#End Region

#Region "Procedures"
	Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
		Dim strCaption As String = CompnyName
		If iType = 1 Then ' Error
			imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
		ElseIf iType = 2 Then ' Warning
			imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
		ElseIf iType = 3 Then ' Information
			imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
		Else
			imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
		End If
		lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
		cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
	End Sub

	Private Sub bindDataRefNo()
		If DDLppn.SelectedValue = "IN" Then
			If invdate1.Text <> "" And invdate2.Text <> "" Then
				If IsValidPeriod() Then
                    sSql = "SELECT cmpcode,oid,Noref,Void,name,tgl,typeTax FROM (" & _
                    " SELECT b.cmpcode,b.trnbelimstoid AS oid,b.trnbelino AS Noref,b.suppoid AS Void,sp.suppname AS name, b.trnbelidate AS tgl, b.trnbelimsttaxtype AS typeTax FROM QL_trnbelimst b INNER JOIN QL_mstsupp sp ON sp.suppoid=b.suppoid WHERE b.cmpcode='" & CompnyCode & "' AND b.trnbelimststatus='Approved' AND b.trnbelimsttaxtype NOT IN ('NT') AND b.trnbelino LIKE '%" & Tchar(FilterRef.Text.Trim) & "%' AND b.trnbelidate BETWEEN '" & CDate(invdate1.Text) & " 00:00' AND '" & CDate(invdate2.Text) & " 23:59' " & _
                    " UNION ALL " & _
                    " SELECT fa.cmpcode,fa.trnbelifamstoid AS oid,fa.trnbelifano AS Noref, fa.suppoid AS Void,sp.suppname AS name,fa.trnbelifadate AS tgl,fa.trnbelifamsttaxtype AS typeTax FROM QL_trnbelimst_fa fa INNER JOIN QL_mstsupp sp ON sp.suppoid=fa.suppoid WHERE fa.cmpcode='" & CompnyCode & "' AND fa.trnbelifamststatus='POST' AND fa.trnbelifamsttaxtype NOT IN ('NT') AND fa.trnbelifano LIKE '%" & Tchar(FilterRef.Text.Trim) & "%' AND fa.trnbelifadate BETWEEN '" & CDate(invdate1.Text) & " 00:00' AND '" & CDate(invdate2.Text) & " 23:59' " & _
                    " UNION ALL " & _
                    " SELECT rm.cmpcode,rm.retmstoid AS oid,rm.retno Noref,rm.suppoid AS Void,sp.suppname AS name, rm.retdate AS tgl,'NT' AS typeTax FROM QL_trnreturmst rm INNER JOIN QL_mstsupp sp ON sp.suppoid=rm.suppoid WHERE rm.cmpcode='" & CompnyCode & "' AND rm.retmststatus='Approved' /*AND b.potaxtype NOT IN ('NT')*/ AND rm.retno LIKE '%" & Tchar(FilterRef.Text.Trim) & "%' AND rm.retdate BETWEEN '" & CDate(invdate1.Text) & " 00:00' AND '" & CDate(invdate2.Text) & " 23:59' " & _
                    ") AS TblSupp GROUP BY cmpcode,oid,Noref,Void,name,tgl,typeTax"
				Else
					Exit Sub
				End If
			End If
		Else
			If invdate1.Text <> "" And invdate2.Text <> "" Then
				If IsValidPeriod() Then
					If DDLRefType.SelectedValue <> "ALL" Then
						Session("ReffType") = DDLRefType.SelectedValue
					Else
						Session("ReffType") = ""
					End If
                    sSql = "SELECT cmpcode,oid,Void,Noref,tgl,name,typeTax  FROM ( " & _
                    " SELECT jm.cmpcode,jm.trnjualmstoid oid,c.custoid Void,jm.trnjualno Noref,jm.trnjualdate tgl,c.custname name,jm.trntaxtype typeTax FROM QL_trnjualmst jm INNER JOIN QL_mstcust c ON c.custoid=jm.custoid WHERE jm.cmpcode='" & CompnyCode & "' AND jm.trnjualstatus='POST' AND jm.trntaxtype NOT IN ('NT') AND jm.trnjualno LIKE '%" & Tchar(FilterRef.Text.Trim) & "%' AND jm.trnjualdate BETWEEN '" & CDate(invdate1.Text) & " 00:00' AND '" & CDate(invdate2.Text) & " 23:59' /*AND jm.trnjualtype LIKE '%" & Session("ReffType") & "%'*/" & _
                    " UNION " & _
                    " SELECT rm.cmpcode,rm.sretmstoid AS oid,c.custoid AS Void,rm.sretno AS Noref,rm.sretdate AS tgl,c.custname AS name,'' AS typeTax FROM QL_trnsalesreturmst rm INNER JOIN QL_mstcust c ON c.custoid=rm.custoid WHERE rm.cmpcode='PRKP' AND rm.sretno LIKE '%" & Tchar(FilterRef.Text.Trim) & "%' AND rm.sretmststatus='Approved' AND /*jm.trntaxtype NOT IN ('NT')*/ rm.sretdate BETWEEN '" & CDate(invdate1.Text) & " 00:00' AND '" & CDate(invdate2.Text) & " 23:59'" & _
                    " ) AS TblCust GROUP BY cmpcode,oid, Void, Noref, tgl, name, typeTax "
                Else
                    Exit Sub
                End If
			End If
		End If
		FillGV(GvRefNo, sSql, "RefNo")
		GvRefNo.Visible = True
	End Sub

	Private Sub bindDataSupp()
		If DDLppn.SelectedValue = "IN" Then
			Dim Oid As String = ""
            sSql = "SELECT cmpcode,oid,code,name,npwp,addres FROM (" & _
            " SELECT sp.cmpcode,sp.suppoid oid,sp.suppcode code,sp.suppname name, sp.suppnpwp npwp,sp.suppaddr addres FROM QL_mstsupp sp INNER JOIN QL_trnbelimst_fa fa ON sp.suppoid=fa.suppoid WHERE sp.suppoid LIKE '%" & suppoid.Text & "%' AND (sp.suppcode LIKE '%" & Tchar(SuppFilter.Text.Trim) & "%' OR sp.suppname LIKE '%" & Tchar(SuppFilter.Text.Trim) & "%' ) AND fa.cmpcode='" & CompnyCode & "' AND fa.trnbelifadate BETWEEN '" & CDate(invdate1.Text) & " 00:00' AND '" & CDate(invdate2.Text) & " 23:59' AND fa.trnbelifano LIKE '% " & Tchar(FilterRef.Text) & " %' AND trnbelifamststatus='POST'" & _
            " UNION ALL " & _
            " SELECT sp.cmpcode,sp.suppoid oid,sp.suppcode code,sp.suppname name, sp.suppnpwp npwp,sp.suppaddr addres FROM QL_mstsupp sp INNER JOIN QL_trnreturmst rb ON sp.suppoid=rb.suppoid WHERE sp.suppoid LIKE '%" & suppoid.Text & "%' AND (sp.suppcode LIKE '%" & Tchar(SuppFilter.Text.Trim) & "%' OR sp.suppname LIKE '%" & Tchar(SuppFilter.Text.Trim) & "%' ) AND rb.cmpcode='" & CompnyCode & "' AND rb.retdate BETWEEN '" & CDate(invdate1.Text) & " 00:00' AND '" & CDate(invdate2.Text) & " 23:59' AND rb.retno LIKE '%" & Tchar(FilterRef.Text) & "%' AND retmststatus='POST'" & _
            " UNION ALL " & _
            " SELECT sp.cmpcode,sp.suppoid oid,sp.suppcode code,sp.suppname name,sp.suppnpwp npwp,sp.suppaddr addres FROM QL_mstsupp sp INNER JOIN QL_trnbelimst b ON sp.suppoid=b.suppoid WHERE sp.suppoid LIKE '%" & suppoid.Text & "%' AND (sp.suppcode LIKE '%" & Tchar(SuppFilter.Text.Trim) & "%' OR sp.suppname LIKE '%" & Tchar(SuppFilter.Text.Trim) & "%' ) AND b.cmpcode='" & CompnyCode & "' AND b.trnbelidate BETWEEN '" & CDate(invdate1.Text) & " 00:00' AND '" & CDate(invdate2.Text) & " 23:59' AND b.trnbelino LIKE '%" & Tchar(FilterRef.Text) & "%' AND trnbelimststatus='Approved'" & _
            " ) tblSupp GROUP BY cmpcode, oid, code, name, npwp, addres ORDER BY oid "
			FillGV(GvSupp, sSql, "Supp")
			SuppFilter.Text = ""
			GvSupp.Visible = True
		Else
            sSql = "SELECT cmpcode,oid,code,name,npwp,addres FROM ( " & _
            "SELECT c.cmpcode, c.custoid AS oid, c.custcode AS code, c.custname AS name, c.custnpwp AS npwp, c.custaddr AS addres FROM QL_mstcust c INNER JOIN QL_trnjualmst j ON j.custoid=c.custoid WHERE c.custoid LIKE '%" & suppoid.Text & "%' AND (c.custcode LIKE '%" & SuppFilter.Text.Trim & "%' OR c.custname LIKE '%" & SuppFilter.Text.Trim & "%') AND j.cmpcode='PRKP ' AND j.trnjualdate BETWEEN '" & CDate(invdate1.Text) & " 00:00' AND '" & CDate(invdate2.Text) & " 23:59' AND j.trnjualno LIKE '%" & Tchar(FilterRef.Text) & "%' AND j.trntaxtype NOT IN ('NONTAX','NT') AND trnjualstatus='POST' UNION ALL" & _
   " SELECT c.cmpcode, c.custoid AS oid, c.custcode AS code, c.custname AS name, c.custnpwp AS npwp,c.custaddr AS addres FROM QL_mstcust c INNER JOIN QL_trnsalesreturmst r ON r.custoid=c.custoid WHERE c.custoid LIKE '%" & suppoid.Text & "%' AND (c.custcode LIKE '%" & SuppFilter.Text.Trim & "%' OR c.custname LIKE '%" & SuppFilter.Text.Trim & "%') AND r.cmpcode='" & CompnyCode & "' AND r.sretdate BETWEEN '" & CDate(invdate1.Text) & " 00:00' AND '" & CDate(invdate2.Text) & " 23:59' AND r.sretno LIKE '%" & Tchar(FilterRef.Text) & "%' AND r.sretmststatus='Approved' " & _
   " ) AS TblCust GROUP BY cmpcode, oid, code, name, npwp,addres ORDER BY oid"
            FillGV(GvSupp, sSql, "Supp")
            SuppFilter.Text = ""
            GvSupp.Visible = True
        End If
	End Sub

	Sub clearForm()
		CrystalReportViewer1.ReportSource = Nothing
		GvRefNo.Visible = False
		GvSupp.Visible = False
		'gvCust.Visible = False
	End Sub

	Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
		Dim myTables As Tables = myReportDocument.Database.Tables
		For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
			Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
			myTableLogonInfo.ConnectionInfo = myConnectionInfo
			myTable.ApplyLogOnInfo(myTableLogonInfo)
		Next
	End Sub

	Private Sub ShowReport(ByVal sType As String)
		Try
            Dim RefType As String = "" : Dim rptName As String = ""
            Dim periode1 As String = "" : Dim periode2 As String = ""
            Dim periode3 As String = "" : Dim OidRef As String = ""
            Dim RefNo As String = "" : Dim InAll As String = "" : Dim SuppName As String = ""
			Dim CmpnyCode As String = Session("CompnyCode")

            If DDLppn.SelectedValue = "IN" Then
                If suppoid.Text <> "" Then
                    SuppName &= " AND sp.suppoid LIKE '%" & suppoid.Text & "%'"
                Else
                    InAll &= "ALL"
                End If
                If SuppFilter.Text <> "" Then
                    SuppName &= " AND sp.suppcode LIKE '%" & Tchar(SuppFilter.Text.Trim) & "%'" & _
                    " OR sp.suppname LIKE '%" & Tchar(SuppFilter.Text) & "%'"
                Else
                    InAll &= "ALL"
                End If
                If IsValidPeriod() Then
                    periode1 &= " AND b.trnbelidate BETWEEN '" & CDate(invdate1.Text) & " 00:00' AND '" & CDate(invdate2.Text) & " 23:59'"
                    periode2 = " AND fa.trnbelifadate BETWEEN '" & CDate(invdate1.Text) & " 00:00' AND '" & CDate(invdate2.Text) & " 23:59'"
                    periode3 &= " AND rm.retdate BETWEEN '" & CDate(invdate1.Text) & " 00:00' AND '" & CDate(invdate2.Text) & " 23:59' "
                Else
                    Exit Sub
                End If
            ElseIf DDLppn.SelectedValue = "OUT" Then
                If suppoid.Text <> "" Then
                    SuppName &= " AND c.custoid LIKE '%" & suppoid.Text & "%' "
                Else
                    InAll &= "ALL"
                End If
                If SuppFilter.Text <> "" Then
                    SuppName &= " AND c.custcode ='" & Tchar(SuppFilter.Text.Trim) & "'" & _
                    " OR c.custname LIKE '%" & Tchar(SuppFilter.Text.Trim) & "%'"
                Else
                    InAll &= "ALL"
                End If
                'If DDLRefType.SelectedValue <> "ALL" Then
                '	RefType = DDLRefType.SelectedValue
                'Else
                '	RefType = ""
                'End If
                If IsValidPeriod() Then
                    periode1 &= " AND jm.trnjualdate BETWEEN '" & CDate(invdate1.Text) & " 00:00' AND '" & CDate(invdate2.Text) & " 23:59'"
                    periode2 = " AND rm.sretdate BETWEEN '" & CDate(invdate1.Text) & " 00:00:00' AND '" & CDate(invdate2.Text) & " 23:59:59'"
                Else
                    Exit Sub
                End If
            Else
                If IsValidPeriod() Then
                    periode1 &= " AND jm.trnjualdate BETWEEN '" & CDate(invdate1.Text) & " 00:00' AND '" & CDate(invdate2.Text) & " 23:59'"
                    periode2 = " AND rm.sretdate BETWEEN '" & CDate(invdate1.Text) & " 00:00:00' AND '" & CDate(invdate2.Text) & " 23:59:59'"
                    SuppName &= " AND b.trnbelidate BETWEEN '" & CDate(invdate1.Text) & " 00:00' AND '" & CDate(invdate2.Text) & " 23:59'"
                    RefType &= " AND fa.trnbelifamstdate BETWEEN '" & CDate(invdate1.Text) & " 00:00' AND '" & CDate(invdate2.Text) & " 23:59'"
                    periode3 &= " AND rm.trnretbelidate BETWEEN '" & CDate(invdate1.Text) & " 00:00' AND '" & CDate(invdate2.Text) & " 23:59'"
                Else
                    Exit Sub
                End If
            End If
            If DDLppn.SelectedValue = "OUT" Then
                If DDLRefType.SelectedValue <> "ALL" Then
                    RefType &= " WHERE sType ='" & DDLRefType.SelectedValue & "'"
                End If
            Else
                If DDLRefType.SelectedValue <> "ALL" Then
                    RefType &= " WHERE sType ='" & DDLRefType.SelectedValue & "'"
                End If
            End If
            Report = New ReportDocument
            If DDLppn.SelectedValue = "IN" Then
                If sType = "xls" Then
                    Report.Load(Server.MapPath("~\Report\rptTaxBeli.rpt"))
                    rptName = "Report_Tax(Masukan)_"
                Else
                    Report.Load(Server.MapPath("~\Report\rptTaxBeli.rpt"))
                    rptName = "Report_Tax(Masukan)_"
                End If
            ElseIf DDLppn.SelectedValue = "OUT" Then
                If sType = "xls" Then
                    Report.Load(Server.MapPath("~\Report\rptTaxJual.rpt"))
                    rptName = "Report_Tax(Keluaran)_"
                Else
                    Report.Load(Server.MapPath("~\Report\rptTaxJual.rpt"))
                    rptName = "Report_Tax(Keluaran)_"
                End If
            Else
                'If sType = "xls" Then
                '	Report.Load(Server.MapPath("~\Report\rptTaxAll.rpt"))
                '	rptName = "Report_Tax(Gabungan)_"
                'Else
                '	Report.Load(Server.MapPath("~\Report\rptTaxAll.rpt"))
                '	rptName = "Report_Tax(Gabungan)_"
                'End If
            End If

            Report.SetParameterValue("CmpCode", CmpnyCode)
            Report.SetParameterValue("periode1", periode1)
            Report.SetParameterValue("periode2", periode2)
            Report.SetParameterValue("periode3", periode3)
            Report.SetParameterValue("SuppName", SuppName)
            Report.SetParameterValue("RefOid", NoReff.Text)
            Report.SetParameterValue("Reff", FilterRef.Text)
            Report.SetParameterValue("RefType", RefType)
            Report.SetParameterValue("ALLIN", InAll)
            Report.SetParameterValue("sDatePeriod", Format(CDate(invdate1.Text), "MM/dd/yyyy") & " - " & Format(CDate(invdate2.Text), "MM/dd/yyyy"))
            Report.PrintOptions.PaperSize = PaperSize.PaperFolio
            cProc.SetDBLogonForReport(Report)

            If sType = "crv" Then
                CrystalReportViewer1.ReportSource = Report
                CrystalReportViewer1.Visible = True
            ElseIf sType = "pdf" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, rptName)
                Report.Close()
                Report.Dispose()
            ElseIf sType = "xls" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, rptName)
                Report.Close()
                Report.Dispose()
            End If

        Catch ex As Exception
			Report.Close()
			Report.Dispose()
			showMessage(ex.Message, 1)
		End Try
	End Sub
#End Region

#Region "Events"

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("UserID") = "" Then
			Response.Redirect("~/Other/login.aspx")
		End If
		If Request.QueryString("awal") = "true" Then
			' Simpan session ke variabel temporary supaya tidak hilang
			Dim userId As String = Session("UserID")
			Dim xsetAcc As DataTable = Session("SpecialAccess")
			Dim appLimit As Decimal = Session("ApprovalLimit")
			Dim xsetRole As DataTable = Session("Role")
			Dim cmpcode As String = Session("CompnyCode")
			' Clear all session
			Session.Clear()
			' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
			Session("UserID") = userId
			Session("SpecialAccess") = xsetAcc
			Session("ApprovalLimit") = appLimit
			Session("Role") = xsetRole
			Session("CompnyCode") = cmpcode
			' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
			Response.Redirect("~\ReportForm\rptSalesTax.aspx")
		End If
		If checkPagePermission("~\ReportForm\rptSalesTax.aspx", Session("Role")) = False Then
			Response.Redirect("~\Other\NotAuthorize.aspx")
		End If

		Page.Title = CompnyName & " - Report Sales Tax Report"
        If Not Page.IsPostBack Then
            DDLppn_SelectedIndexChanged(Nothing, Nothing)
            invdate1.Text = Format(Now, "MM/01/yyyy")
            invdate2.Text = Format(Now, "MM/dd/yyyy")
        End If
	End Sub

    Protected Sub imbso_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindDataSupp()
        GvSupp.Visible = True
    End Sub

    Protected Sub imbcusdel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'trncustoid.Text = ""
        'customerFilter.Text = ""
        'gvCust.Visible = False
    End Sub

    Protected Sub imbsallist_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindDataRefNo()
        GvRefNo.Visible = True
    End Sub

    Protected Sub imbsaldel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        NoReff.Text = ""
        FilterRef.Text = ""
        SuppFilter.Text = ""
        suppoid.Text = ""
        GvRefNo.Visible = False
    End Sub

    Protected Sub iberasesupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'trncustoid.Text = ""
        'customerFilter.Text = ""
    End Sub

	Protected Sub imbsodel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
		SuppFilter.Text = ""
		suppoid.Text = ""
		GvSupp.Visible = False
	End Sub

    Protected Sub gvSupp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        suppoid.Text = GvSupp.SelectedDataKey("oid").ToString().Trim
        SuppFilter.Text = GvSupp.SelectedDataKey("name").ToString().Trim
        GvSupp.Visible = False
    End Sub

    Protected Sub GvRefNo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GvRefNo.PageIndex = e.NewPageIndex
        bindDataRefNo()
    End Sub

    Protected Sub GvRefNo_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
			e.Row.Cells(3).Text = Format(CDate(e.Row.Cells(3).Text), "MM/dd/yyyy")
        End If
    End Sub

    Protected Sub GvRefNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FilterRef.Text = GvRefNo.SelectedDataKey("Noref").ToString().Trim
        NoReff.Text = GvRefNo.SelectedDataKey("oid").ToString().Trim
        suppoid.Text = GvRefNo.SelectedDataKey("Void").ToString().Trim
        cProc.DisposeGridView(sender)
        GvRefNo.Visible = False
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not Report Is Nothing Then
                If Report.IsLoaded Then
                    Report.Dispose()
                    Report.Close()
                End If
            End If
        Catch ex As Exception
            Report.Dispose()
            Report.Close()
        End Try
    End Sub

    Protected Sub CrystalReportViewer1_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles CrystalReportViewer1.Navigate
        ShowReport("crv")
    End Sub

    Protected Sub gvSupp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvSupp.PageIndex = e.NewPageIndex
        bindDataSupp()
    End Sub

	Protected Sub DDLJnsInv_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
		FilterRef.Text = ""
		SuppFilter.Text = ""
		'customerFilter.Text = ""
	End Sub

    Protected Sub DDLppn_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GvRefNo.Visible = False
        GvSupp.Visible = False
        'gvCust.Visible = False
        FilterRef.Text = ""
        NoReff.Text = ""
        SuppFilter.Text = ""
        suppoid.Text = ""
        If DDLppn.SelectedValue = "IN" Then
            DDLRefType.Items(1).Enabled = False : DDLRefType.Items(2).Enabled = False
            DDLRefType.Items(3).Enabled = True : DDLRefType.Items(4).Enabled = True
            DDLRefType.Items(5).Enabled = True
            Label2.Visible = True : FilterRef.Visible = True
            Label1.Visible = True : Label11.Visible = False : Label6.Visible = True
            FilterRef.Visible = True : Nota.Visible = True : Label9.Visible = True
            imbsallist.Visible = True : imbsaldel.Visible = True : Label7.Visible = True
            ImageButton1.Visible = True : ImageButton2.Visible = True :SuppFilter.Visible = True
        Else
            DDLRefType.Items(1).Enabled = True : DDLRefType.Items(2).Enabled = True
            DDLRefType.Items(3).Enabled = False : DDLRefType.Items(4).Enabled = False
            DDLRefType.Items(5).Enabled = False
            DDLRefType.Visible = True : Label2.Visible = True : Label1.Visible = True
            Label11.Visible = True
            Label6.Visible = False : SuppFilter.Visible = True : Label7.Visible = True
            FilterRef.Visible = True : Nota.Visible = True : Label9.Visible = True
            imbsallist.Visible = True : imbsaldel.Visible = True
            ImageButton1.Visible = True : ImageButton2.Visible = True
        End If
        If DDLppn.SelectedValue = "ALL" Then
            Label1.Visible = False : Label2.Visible = False : DDLRefType.Visible = False
            Nota.Visible = False : Label7.Visible = False : FilterRef.Visible = False
            imbsallist.Visible = False : imbsaldel.Visible = False : NoReff.Visible = False
            Label6.Visible = False : Label9.Visible = False : SuppFilter.Visible = False
            ImageButton1.Visible = False : ImageButton2.Visible = False
            suppoid.Visible = False : Label11.Visible = False
        End If
    End Sub

#End Region

	Protected Sub ViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
		ShowReport("crv")
	End Sub

	Protected Sub ExPdfBtn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
		ShowReport("pdf")
	End Sub

	Protected Sub ExpExcelBtn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
		ShowReport("xls")
	End Sub

	Protected Sub ClearBn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
		Response.Redirect("~\reportForm\rptSalesTax.aspx?awal=true")
	End Sub

	Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
		cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
	End Sub
End Class
