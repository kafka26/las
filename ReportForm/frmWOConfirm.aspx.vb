Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_WOConfirm
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function 'OK

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matrefoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function 'OK

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matrefoid=" & cbOid
                                dtView2.RowFilter = "matrefoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function 'OK

    Private Function UpdateCheckedSO() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSO") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSO")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListSO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "soitemmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblSO") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function 'OK

    Private Function UpdateCheckedSO2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSO")
            Dim dtTbl2 As DataTable = Session("TblSOView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListSO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "soitemmstoid=" & cbOid
                                dtView2.RowFilter = "soitemmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblSO") = dtTbl
                Session("TblSOView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function 'OK

    Private Function UpdateCheckedKIK() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblKIK") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblKIK")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListKIK.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListKIK.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "womstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblKIK") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function 'OK

    Private Function UpdateCheckedKIK2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblKIKView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblKIK")
            Dim dtTbl2 As DataTable = Session("TblKIKView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListKIK.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListKIK.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "womstoid=" & cbOid
                                dtView2.RowFilter = "womstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblKIK") = dtTbl
                Session("TblKIKView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function 'OK

    Private Function FillDDLWithALL(ByRef oDDLObject As DropDownList, ByVal sSql As String) As Boolean
        ' To fill data in dropdown list, return true when all data exist
        FillDDLWithALL = True
        oDDLObject.Items.Clear()
        oDDLObject.Items.Add("ALL")
        oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = "ALL"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            oDDLObject.Items.Add(xreader.GetValue(1))
            oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()
        ' Cek if all data exits
        If oDDLObject.Items.Count = 0 Then
            FillDDLWithALL = False
        End If
        Return FillDDLWithALL
    End Function 'OK

#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub 'OK

    Private Sub BindListMat()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, wod1.itemoid AS matrefoid, m.itemlongdesc AS matreflongdesc, m.itemcode AS matrefcode, g2.gendesc unit FROM QL_trnwomst wom INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=wom.cmpcode AND wom.womstoid=wod1.womstoid INNER JOIN QL_trnsoitemmst som ON som.cmpcode=wod1.cmpcode AND som.soitemmstoid=wod1.soitemmstoid INNER JOIN QL_mstitem m ON m.itemoid=wod1.itemoid INNER JOIN QL_mstgen g2 ON g2.genoid=wod1.wodtl1unitoid "
        lblTitleListMat.Text = "Finish Good"

        sSql &= "WHERE wom.cmpcode='" & DDLBusUnit.SelectedValue & "'"

        If DDLDivision.SelectedValue <> "ALL" Then
            sSql &= " AND som.groupoid=" & DDLDivision.SelectedValue
        End If

        If FilterDDLStatus.SelectedValue = "Post" Then
            sSql &= " AND wom.womststatus IN ('Post','Closed')"
        ElseIf FilterDDLStatus.SelectedValue = "Cancel" Then
            sSql &= " AND wom.womststatus IN ('Cancel')"
        End If

        If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
            If IsValidPeriod() Then
                sSql &= " AND wom.wodate>='" & FilterPeriod1.Text & " 00:00:00' AND wom.wodate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If

        'If RDDO.Checked = False Then
        '    sSql &= " AND ISNULL(sod.soitemqty,0) " & FilterDDLSOQty.SelectedValue & " " & ToDouble(SOQtyTxt.Text)
        'End If

        'If checkPagePermission("~\ReportForm\frmWOConfirm.aspx", Session("SpecialAccess")) = False Then
        '    sSql &= " AND som.createuser='" & Session("UserID") & "'"
        'End If

        Session("TblMat") = cKon.ambiltabel(sSql, "QL_mstref")
    End Sub 'OK

    Private Sub BindListSO()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, som.soitemmstoid, som.soitemno, som.soitemdate, CONVERT(VARCHAR(10), som.soitemdate, 101) AS sodate, som.soitemmststatus, som.soitemmstnote FROM QL_trnsoitemmst som "

        sSql &= " WHERE som.soitemmstoid IN (SELECT wod1.soitemmstoid from QL_trnwodtl1 wod1 INNER JOIN QL_trnwomst wom ON wom.womstoid=wod1.womstoid INNER JOIN QL_mstitem i ON i.itemoid=wod1.itemoid "


        If DDLBusUnit.SelectedValue <> "ALL" Then
            sSql &= " WHERE wom.cmpcode='" & DDLBusUnit.SelectedValue & "'"
        Else
            sSql &= " WHERE wom.cmpcode LIKE '%'"
        End If

        If DDLDivision.SelectedValue <> "ALL" Then
            sSql &= " AND som.groupoid=" & DDLDivision.SelectedValue
        End If

        If DDLBusUnit.SelectedValue <> "ALL" Then
            sSql &= "AND wom.cmpcode='" & DDLBusUnit.SelectedValue & "'"
        Else
            sSql &= "AND wom.cmpcode LIKE '%%'"
        End If

        If FilterDDLStatus.SelectedValue = "In Process" Then
            sSql &= " AND wom.womststatus IN ('In Process')"
        ElseIf FilterDDLStatus.SelectedValue = "Post" Then
            sSql &= " AND wom.womststatus IN ('Post','Closed')"
        ElseIf FilterDDLStatus.SelectedValue = "Cancel" Then
            sSql &= " AND wom.womststatus IN ('Cancel')"
        End If

        If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
            If IsValidPeriod() Then
                sSql &= " AND wom.wodate>='" & FilterPeriod1.Text & " 00:00:00' AND wom.wodate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If

        If matrefcode.Text <> "" Then
            Dim scode() As String = Split(matrefcode.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " i.itemcode = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        sSql &= ") ORDER BY som.soitemdate DESC, som.soitemmstoid DESC"
        Session("TblSO") = cKon.ambiltabel(sSql, "QL_soitemmst")
    End Sub 'OK

    Private Sub BindListKIK()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, wom.womstoid AS womstoid, wom.wono AS wono, CONVERT(VARCHAR(10), wom.wodate, 101) AS wodate, wom.womstnote AS womstnote, som.soitemno FROM QL_trnwomst wom INNER JOIN QL_trnwodtl1 wod1 ON wom.womstoid=wod1.womstoid INNER JOIN QL_mstitem m ON m.itemoid=wod1.itemoid INNER JOIN QL_trnsoitemmst som ON som.soitemmstoid=wod1.soitemmstoid "

        sSql &= " WHERE wom.cmpcode='" & DDLBusUnit.SelectedValue & "'"

        If FilterDDLStatus.SelectedValue = "Post" Then
            sSql &= " AND wom.womststatus IN ('Post','Closed')"
        ElseIf FilterDDLStatus.SelectedValue = "Cancel" Then
            sSql &= " AND wom.womststatus IN ('Cancel')"
        End If

        If DDLDivision.SelectedValue <> "ALL" Then
            sSql &= " AND som.groupoid=" & DDLDivision.SelectedValue
        End If

        If matrefcode.Text <> "" Then
            Dim scode() As String = Split(matrefcode.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " m.itemcode = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If sono.Text <> "" Then
            Dim scode() As String = Split(sono.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " som.soitemno = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If


        If IsValidPeriod() Then
            sSql &= " AND wom.wodate>='" & FilterPeriod1.Text & " 00:00:00' AND wom.wodate<='" & FilterPeriod2.Text & " 23:59:59'"
        Else
            Exit Sub
        End If

        sSql &= " ORDER BY wom.wodate DESC, wom.womstoid DESC"
        Session("TblKIK") = cKon.ambiltabel(sSql, "QL_trnwomst")
    End Sub 'OK

    Private Sub ShowReport(ByVal sType As String)
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If

        Try
            Dim sWhere As String = ""
            sWhere &= " WHERE wom.cmpcode='" & DDLBusUnit.SelectedValue & "'"

            Dim rptName As String = ""

            If sType = "Print Excel" Then
                If FilterDDLType.SelectedValue = "Summary" Then
                    report.Load(Server.MapPath(folderReport & "rptJobCostMOConfXls.rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptJobCostMOConfDtlXls.rpt"))
                End If
                rptName = "JobCostingMOConfirmationStatus_"
            Else
                If FilterDDLGroupBy.SelectedValue = "Ungroup" Then
                    If FilterDDLType.SelectedValue = "Summary" Then
                        report.Load(Server.MapPath(folderReport & "rptJobCostMOConfFG.rpt"))
                    Else
                        report.Load(Server.MapPath(folderReport & "rptJobCostMOConfDtlFG.rpt"))
                    End If
                ElseIf FilterDDLGroupBy.SelectedValue = "SO" Then
                    If FilterDDLType.SelectedValue = "Summary" Then
                        report.Load(Server.MapPath(folderReport & "rptJobCostMOConfSO.rpt"))
                    Else
                        report.Load(Server.MapPath(folderReport & "rptJobCostMOConfDtlSO.rpt"))
                    End If
                Else
                    report.Load(Server.MapPath(folderReport & "rptJobCostMOConfDtlKIK.rpt"))
                End If
                rptName = "JobCostingMOConfirmationStatus_"
            End If

            If DDLDivision.SelectedValue <> "ALL" Then
                sWhere &= " AND som.groupoid=" & DDLDivision.SelectedValue
            End If

            If IsValidPeriod() Then
                sWhere &= " AND wom.wodate>='" & FilterPeriod1.Text & " 00:00:00' AND wom.wodate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If

            If matrefcode.Text <> "" Then
                Dim scode() As String = Split(matrefcode.Text, ";")
                sWhere &= " AND ("
                For c1 As Integer = 0 To scode.Length - 1
                    sWhere &= " item.itemcode = '" & Tchar(scode(c1)) & "'"
                    If c1 < scode.Length - 1 Then
                        sWhere &= " OR "
                    End If
                Next
                sWhere &= ")"
            End If

            If sono.Text <> "" Then
                Dim scode() As String = Split(sono.Text, ";")
                sWhere &= " AND ("
                For c1 As Integer = 0 To scode.Length - 1
                    sWhere &= " som.soitemno = '" & Tchar(scode(c1)) & "'"
                    If c1 < scode.Length - 1 Then
                        sWhere &= " OR "
                    End If
                Next
                sWhere &= ")"
            End If

            If wono.Text <> "" Then
                Dim sResNo() As String = Split(wono.Text, ";")
                sWhere &= " AND ("
                For c1 As Integer = 0 To sResNo.Length - 1
                    sWhere &= " wom.wono = '" & Tchar(sResNo(c1)) & "'"
                    If c1 < sResNo.Length - 1 Then
                        sWhere &= " OR"
                    End If
                Next
                sWhere &= ")"
            End If

            If FilterDDLStatus.SelectedValue = "Post" Then
                sWhere &= " AND wom.womststatus IN ('Post','Closed')"
            ElseIf FilterDDLStatus.SelectedValue = "Cancel" Then
                sWhere &= " AND wom.womststatus IN ('Cancel')"
            End If
            Dim sWhere2 As String
            If DDLFilterQty.SelectedValue = "Balance Confirm" Then
                sWhere2 = "WHERE ([KIK_FG_Qty]-[Confirm_Qty]) " & FilterDDLKIKQty.SelectedValue & " " & ToDouble(KIKQtyTxt.Text)
            Else
                If KIKQtyTxt.Visible = True Then
                    sWhere2 = "WHERE ([Confirm_Qty]) " & FilterDDLKIKQty.SelectedValue & " " & ToDouble(KIKQtyTxt.Text)
                Else
                    sWhere2 = "WHERE ([Confirm_Qty]) " & FilterDDLKIKQty.SelectedValue & " "
                End If

            End If
            report.SetParameterValue("sDate", FilterPeriod1.Text & "-" & FilterPeriod2.Text)
            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("sWhere2", sWhere2)
            cProc.SetDBLogonForReport(report)
            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, rptName & Format(GetServerTime(), "yyyyMMddHHmmss"))
                report.Close()
                report.Dispose()
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, rptName & Format(GetServerTime(), "yyyyMMddHHmmss"))
                report.Close()
                report.Dispose()
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Private Sub InitAllDDL()
        ' DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)
        If DDLBusUnit.Items.Count > 0 Then
            InitDDLDivision(DDLBusUnit.SelectedValue)
        End If
    End Sub 'OK

    Private Sub InitDDLDivision(ByVal sDiv As String)
        ' Init DDL Division
        sSql = "SELECT groupoid, groupcode+' - '+groupdesc FROM QL_mstdeptgroup WHERE activeflag='ACTIVE'"
        If sDiv <> "ALL" Then
            sSql &= " AND cmpcode='" & sDiv & "'"
            FillDDLWithALL(DDLDivision, sSql)
        End If
    End Sub 'OK

    Private Sub InitFilterDDLCat1()
        Dim sMatType As String = ""
        sMatType = "Finish Good"
        'Fill DDL Category1
        sSql = "SELECT cat1code, cat1code+' - '+cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat1res1='" & sMatType & "' Order By cat1code"
        FillDDL(FilterDDLCat01, sSql)
        If FilterDDLCat01.Items.Count > 0 Then
            InitFilterDDLCat2()
        Else
            FilterDDLCat02.Items.Clear()
            FilterDDLCat03.Items.Clear()
            FilterDDLCat04.Items.Clear()
        End If
    End Sub 'OK

    Private Sub InitFilterDDLCat2()
        Dim sMatType As String = ""
        sMatType = "Finish Good"
        'Fill DDL Category2
        sSql = "SELECT cat2code, cat2code+' - '+cat2shortdesc FROM QL_mstcat2 cat2 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat2.cmpcode AND cat1.cat1oid=cat2.cat1oid AND cat1.cat1code='" & FilterDDLCat01.SelectedValue & "' WHERE cat2.cmpcode='" & CompnyCode & "' AND cat2.activeflag='ACTIVE' AND cat2res1='" & sMatType & "' Order By cat2code"
        FillDDL(FilterDDLCat02, sSql)
        If FilterDDLCat02.Items.Count > 0 Then
            InitFilterDDLCat3()
        Else
            FilterDDLCat03.Items.Clear()
            FilterDDLCat04.Items.Clear()
        End If
    End Sub 'OK

    Private Sub InitFilterDDLCat3()
        Dim sMatType As String = ""
        sMatType = "Finish Good"
        'Fill DDL Category3
        sSql = "SELECT cat3code, cat3code+' - '+cat3shortdesc FROM QL_mstcat3 cat3 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat3.cmpcode AND cat1.cat1oid=cat3.cat1oid AND cat1.cat1code='" & FilterDDLCat01.SelectedValue & "' INNER JOIN QL_mstcat2 cat2 ON cat2.cmpcode=cat3.cmpcode AND cat2.cat2oid=cat3.cat2oid AND cat2.cat2code='" & FilterDDLCat02.SelectedValue & "' WHERE cat3.cmpcode='" & CompnyCode & "' AND cat3.activeflag='ACTIVE' AND cat3res1='" & sMatType & "' Order By cat3code"
        FillDDL(FilterDDLCat03, sSql)
        If FilterDDLCat03.Items.Count > 0 Then
            InitFilterDDLCat4()
        Else
            FilterDDLCat04.Items.Clear()
        End If
    End Sub 'OK

    Private Sub InitFilterDDLCat4()
        Dim sMatType As String = ""
        sMatType = "Finish Good"
        'Fill DDL Category4
        sSql = "SELECT cat4code, cat4code+' - '+cat4shortdesc FROM QL_mstcat4 cat4 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat4.cmpcode AND cat1.cat1oid=cat4.cat1oid AND cat1.cat1code='" & FilterDDLCat01.SelectedValue & "' INNER JOIN QL_mstcat2 cat2 ON cat2.cmpcode=cat4.cmpcode AND cat2.cat2oid=cat4.cat2oid AND cat2.cat2code='" & FilterDDLCat02.SelectedValue & "' INNER JOIN QL_mstcat3 cat3 ON cat3.cmpcode=cat4.cmpcode AND cat3.cat3oid=cat4.cat3oid AND cat3.cat3code='" & FilterDDLCat03.SelectedValue & "' WHERE cat4.cmpcode='" & CompnyCode & "' AND cat4.activeflag='ACTIVE' AND cat4res1='" & sMatType & "' Order By cat4code"
        FillDDL(FilterDDLCat04, sSql)
    End Sub 'OK
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If

        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmWOConfirm.aspx")

        End If

        If checkPagePermission("~\ReportForm\frmWOConfirm.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        Page.Title = CompnyName & " - Job Costing MO Confirmation Status"
        If Not Page.IsPostBack Then
            InitAllDDL()
            InitFilterDDLCat1()
            FilterDDLType_SelectedIndexChanged(Nothing, Nothing)
            DDLFilterQty_SelectedIndexChanged(Nothing, Nothing)
            FilterDDLKIKQty.SelectedValue = ">="
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        End If
    End Sub 'OK

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If

        If Not Session("EmptyListSO") Is Nothing And Session("EmptyListSO") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListSO") Then
                Session("EmptyListSO") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
            End If
        End If
        If Not Session("WarningListSO") Is Nothing And Session("WarningListSO") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListSO") Then
                Session("WarningListSO") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
            End If
        End If

        If Not Session("EmptyListKIK") Is Nothing And Session("EmptyListKIK") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListKIK") Then
                Session("EmptyListKIK") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListKIK, PanelListKIK, mpeListKIK, True)
            End If
        End If
        If Not Session("WarningListKIK") Is Nothing And Session("WarningListKIK") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListKIK") Then
                Session("WarningListKIK") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListKIK, PanelListKIK, mpeListKIK, True)
            End If
        End If
    End Sub 'OK

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        imbEraseKIK_Click(Nothing, Nothing)
        InitDDLDivision(DDLBusUnit.SelectedValue)
    End Sub 'OK

    Protected Sub DDLFilterQty_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLFilterQty.SelectedIndexChanged
        If DDLFilterQty.SelectedValue = "Total Confirm" Then
            FilterDDLKIKQty.Items(5).Enabled = True
            FilterDDLKIKQty.Items(6).Enabled = True
        Else
            FilterDDLKIKQty.Items(5).Enabled = False
            FilterDDLKIKQty.Items(6).Enabled = False
        End If
    End Sub 'OK

    Protected Sub FilterDDLKIKQty_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLKIKQty.SelectedIndexChanged
        If FilterDDLKIKQty.SelectedValue = "= [KIK_FG_Qty]" Then
            KIKQtyTxt.Visible = False
        ElseIf FilterDDLKIKQty.SelectedValue = "< [KIK_FG_Qty]" Then
            KIKQtyTxt.Visible = False
        Else
            KIKQtyTxt.Visible = True
        End If
    End Sub 'OK

    Protected Sub FilterDDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLType.SelectedIndexChanged
        If FilterDDLType.SelectedValue = "Summary" Then
            FilterDDLGroupBy.Items(2).Enabled = False
        Else
            FilterDDLGroupBy.Items(2).Enabled = True
        End If
    End Sub 'OK

    Protected Sub FilterDDLCat01_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat01.SelectedIndexChanged
        InitFilterDDLCat2()
        mpeListMat.Show()
    End Sub 'OK

    Protected Sub FilterDDLCat02_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat02.SelectedIndexChanged
        InitFilterDDLCat3()
        mpeListMat.Show()
    End Sub 'OK

    Protected Sub FilterDDLCat03_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat03.SelectedIndexChanged
        InitFilterDDLCat4()
        mpeListMat.Show()
    End Sub 'OK

    Protected Sub FilterDDLCat04_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat04.SelectedIndexChanged
        mpeListMat.Show()
    End Sub 'OK

    Protected Sub imbFindMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindMat.Click
        If IsValidPeriod() Then
            FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
            InitFilterDDLCat1()
            cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
            Session("TblMat") = Nothing : Session("TblMatView") = Nothing : gvListMat.DataSource = Nothing : gvListMat.DataBind() : tbData.Text = "5" : gvListMat.PageSize = CInt(tbData.Text)
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            Exit Sub
        End If
    End Sub 'OK

    Protected Sub imbEraseMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseMat.Click
        matrefcode.Text = ""
    End Sub 'OK

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        If cbCat01.Checked Then
            If FilterDDLCat01.SelectedValue <> "" Then
                sPlus &= " AND SUBSTRING(matrefcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'"
            Else
                cbCat01.Checked = False
            End If
        End If
        If cbCat02.Checked Then
            If FilterDDLCat02.SelectedValue <> "" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND SUBSTRING(matrefcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'") & " AND SUBSTRING(matrefcode, 4, 3)='" & FilterDDLCat02.SelectedValue & "'"
            Else
                cbCat02.Checked = False
            End If
        End If
        If cbCat03.Checked Then
            If FilterDDLCat03.SelectedValue <> "" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND SUBSTRING(matrefcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'") & IIf(cbCat02.Checked, "", " AND SUBSTRING(matrefcode, 4, 3)='" & FilterDDLCat02.SelectedValue & "'") & " AND SUBSTRING(matrefcode, 8, 4)='" & FilterDDLCat03.SelectedValue & "'"
            Else
                cbCat03.Checked = False
            End If
        End If
        If cbCat04.Checked Then
            If FilterDDLCat04.SelectedValue <> "" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND SUBSTRING(matrefcode, 1, 2)='" & FilterDDLCat01.SelectedValue & "'") & IIf(cbCat02.Checked, "", " AND SUBSTRING(matrefcode, 4, 3)='" & FilterDDLCat02.SelectedValue & "'") & IIf(cbCat03.Checked, "", " AND SUBSTRING(matrefcode, 8, 4)='" & FilterDDLCat03.SelectedValue & "'") & " AND SUBSTRING(matrefcode, 13, 4)='" & FilterDDLCat04.SelectedValue & "'"
            Else
                cbCat04.Checked = False
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub 'OK

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        InitFilterDDLCat1()
        cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub 'OK

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub 'OK

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If matrefcode.Text <> "" Then
                            matrefcode.Text &= ";" + vbCrLf + dtView(C1)("matrefcode")
                        Else
                            matrefcode.Text = dtView(C1)("matrefcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some Material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub 'OK

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matrefoid=" & dtTbl.Rows(C1)("matrefoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub 'OK

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matrefoid=" & dtTbl.Rows(C1)("matrefoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub 'OK

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                InitFilterDDLCat1()
                cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Selected material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub 'OK

    Protected Sub lbShowData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbShowData.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Please find material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If tbData.Text = "" Then
            Session("WarningListMat") = "Please fill total data to be shown!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        Else
            If ToDouble(tbData.Text) <= 0 Then
                Session("WarningListMat") = "Total data to be shown must be more than 0!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat2() Then
            gvListMat.PageSize = Int(tbData.Text)
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub 'OK

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub 'OK

    Protected Sub imbFindSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindSO.Click
        If IsValidPeriod() Then
            DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
            Session("TblSO") = Nothing : Session("TblSOView") = Nothing : gvListSO.DataSource = Nothing : gvListSO.DataBind()
            cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
        Else
            Exit Sub
        End If
    End Sub 'OK

    Protected Sub imbEraseSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseSO.Click
        sono.Text = ""
        DDLBusUnit.Enabled = True
    End Sub 'OK

    Protected Sub btnFindListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSO.Click
        If Session("TblSO") Is Nothing Then
            BindListSO()
            If Session("TblSO").Rows.Count <= 0 Then
                Session("EmptyListSO") = "SO data can't be found!"
                showMessage(Session("EmptyListSO"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListSO.SelectedValue & " LIKE '%" & Tchar(txtFilterListSO.Text) & "%'"
        If UpdateCheckedSO() Then
            Dim dv As DataView = Session("TblSO").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblSOView") = dv.ToTable
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                dv.RowFilter = ""
                mpeListSO.Show()
            Else
                dv.RowFilter = ""
                Session("TblSOView") = Nothing
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                Session("WarningListSO") = "SO data can't be found!"
                showMessage(Session("WarningListSO"), 2)
            End If
        Else
            mpeListSO.Show()
        End If
    End Sub 'OK

    Protected Sub btnViewAllListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListSO.Click
        DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
        If Session("TblSO") Is Nothing Then
            BindListSO()
            If Session("TblSO").Rows.Count <= 0 Then
                Session("EmptyListSO") = "SO data can't be found!"
                showMessage(Session("EmptyListSO"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedSO() Then
            Dim dt As DataTable = Session("TblSO")
            Session("TblSOView") = dt
            gvListSO.DataSource = Session("TblSOView")
            gvListSO.DataBind()
        End If
        mpeListSO.Show()
    End Sub 'OK

    Protected Sub btnSelectAllSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllSO.Click
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "soitemmstoid=" & dtTbl.Rows(C1)("soitemmstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSO") = objTbl
                Session("TblSOView") = dtTbl
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
            End If
            mpeListSO.Show()
        Else
            Session("WarningListSO") = "Please show some SO data first!"
            showMessage(Session("WarningListSO"), 2)
        End If
    End Sub 'OK

    Protected Sub btnSelectNoneSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneSO.Click
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "soitemmstoid=" & dtTbl.Rows(C1)("soitemmstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSO") = objTbl
                Session("TblSOView") = dtTbl
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
            End If
            mpeListSO.Show()
        Else
            Session("WarningListSO") = "Please show some SO data first!"
            showMessage(Session("WarningListSO"), 2)
        End If
    End Sub 'OK

    Protected Sub btnViewCheckedSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedSO.Click
        If Session("TblSO") Is Nothing Then
            Session("WarningListSO") = "Selected SO data can't be found!"
            showMessage(Session("WarningListSO"), 2)
            Exit Sub
        End If
        If UpdateCheckedSO() Then
            Dim dtTbl As DataTable = Session("TblSO")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
                Session("TblSOView") = dtView.ToTable
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                dtView.RowFilter = ""
                mpeListSO.Show()
            Else
                dtView.RowFilter = ""
                Session("TblSOView") = Nothing
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                Session("WarningListSO") = "Selected SO data can't be found!"
                showMessage(Session("WarningListSO"), 2)
            End If
        Else
            mpeListSO.Show()
        End If
    End Sub 'OK

    Protected Sub gvListSO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSO.PageIndexChanging
        If UpdateCheckedSO2() Then
            gvListSO.PageIndex = e.NewPageIndex
            gvListSO.DataSource = Session("TblSOView")
            gvListSO.DataBind()
        End If
        mpeListSO.Show()
    End Sub 'OK

    Protected Sub lkbAddToListListSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListSO.Click
        If Not Session("TblSO") Is Nothing Then
            If UpdateCheckedSO() Then
                Dim dtTbl As DataTable = Session("TblSO")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If sono.Text <> "" Then
                            If dtView(C1)("soitemno") <> "" Then
                                sono.Text &= ";" + vbCrLf + dtView(C1)("soitemno")
                            End If
                        Else
                            If dtView(C1)("soitemno") <> "" Then
                                sono.Text &= dtView(C1)("soitemno")
                            End If
                        End If
                    Next
                    dtView.RowFilter = ""
                    DDLBusUnit.Enabled = False
                    cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, False)
                Else
                    Session("WarningListSO") = "Please select SO to add to list!"
                    showMessage(Session("WarningListSO"), 2)
                    Exit Sub
                End If
            Else
                Session("WarningListSO") = "Please show some SO data first!"
                showMessage(Session("WarningListSO"), 2)
                Exit Sub
            End If
        Else
            Session("WarningListSO") = "Please show some SO data first!"
            showMessage(Session("WarningListSO"), 2)
            Exit Sub
        End If
    End Sub 'OK

    Protected Sub lkbListSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListSO.Click
        cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, False)
    End Sub 'OK

    Protected Sub imbFindKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindKIK.Click
        If IsValidPeriod() Then
            DDLFilterListKIK.SelectedIndex = -1 : txtFilterListKIK.Text = ""
            Session("TblKIK") = Nothing : Session("TblKIKView") = Nothing : gvListKIK.DataSource = Nothing : gvListKIK.DataBind()
            cProc.SetModalPopUpExtender(btnHiddenListKIK, PanelListKIK, mpeListKIK, True)
        Else
            Exit Sub
        End If
    End Sub 'OK

    Protected Sub imbEraseKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseKIK.Click
        wono.Text = ""
    End Sub 'OK

    Protected Sub btnFindListKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListKIK.Click
        If Session("TblKIK") Is Nothing Then
            BindListKIK()
            If Session("TblKIK").Rows.Count <= 0 Then
                Session("EmptyListKIK") = "KIK data can't be found!"
                showMessage(Session("EmptyListKIK"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListKIK.SelectedValue & " LIKE '%" & Tchar(txtFilterListKIK.Text) & "%'"
        If UpdateCheckedKIK() Then
            Dim dv As DataView = Session("TblKIK").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblKIKView") = dv.ToTable
                gvListKIK.DataSource = Session("TblKIKView")
                gvListKIK.DataBind()
                dv.RowFilter = ""
                mpeListKIK.Show()
            Else
                dv.RowFilter = ""
                Session("TblKIKView") = Nothing
                gvListKIK.DataSource = Session("TblKIKView")
                gvListKIK.DataBind()
                Session("WarningListKIK") = "KIK data can't be found!"
                showMessage(Session("WarningListKIK"), 2)
            End If
        Else
            mpeListKIK.Show()
        End If
    End Sub 'OK

    Protected Sub btnViewAllListKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListKIK.Click
        DDLFilterListKIK.SelectedIndex = -1 : txtFilterListKIK.Text = ""
        If Session("TblKIK") Is Nothing Then
            BindListKIK()
            If Session("TblKIK").Rows.Count <= 0 Then
                Session("EmptyListKIK") = "KIK data can't be found!"
                showMessage(Session("EmptyListKIK"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedKIK() Then
            Dim dt As DataTable = Session("TblKIK")
            Session("TblKIKView") = dt
            gvListKIK.DataSource = Session("TblKIKView")
            gvListKIK.DataBind()
        End If
        mpeListKIK.Show()
    End Sub 'OK

    Protected Sub btnSelectAllKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllKIK.Click
        If Not Session("TblKIKView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblKIKView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblKIK")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "womstoid=" & dtTbl.Rows(C1)("womstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblKIK") = objTbl
                Session("TblKIKView") = dtTbl
                gvListKIK.DataSource = Session("TblKIKView")
                gvListKIK.DataBind()
            End If
            mpeListKIK.Show()
        Else
            Session("WarningListKIK") = "Please show some KIK data first!"
            showMessage(Session("WarningListKIK"), 2)
        End If
    End Sub 'OK

    Protected Sub btnSelectNoneKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneKIK.Click
        If Not Session("TblKIKView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblKIKView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblKIK")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "womstoid=" & dtTbl.Rows(C1)("womstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblKIK") = objTbl
                Session("TblKIKView") = dtTbl
                gvListKIK.DataSource = Session("TblKIKView")
                gvListKIK.DataBind()
            End If
            mpeListKIK.Show()
        Else
            Session("WarningListKIK") = "Please show some KIK data first!"
            showMessage(Session("WarningListKIK"), 2)
        End If
    End Sub 'OK

    Protected Sub btnViewCheckedKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedKIK.Click
        If Session("TblKIK") Is Nothing Then
            Session("WarningListKIK") = "Selected KIK data can't be found!"
            showMessage(Session("WarningListKIK"), 2)
            Exit Sub
        End If
        If UpdateCheckedKIK() Then
            Dim dtTbl As DataTable = Session("TblKIK")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListKIK.SelectedIndex = -1 : txtFilterListKIK.Text = ""
                Session("TblKIKView") = dtView.ToTable
                gvListKIK.DataSource = Session("TblKIKView")
                gvListKIK.DataBind()
                dtView.RowFilter = ""
                mpeListKIK.Show()
            Else
                dtView.RowFilter = ""
                Session("TblKIKView") = Nothing
                gvListKIK.DataSource = Session("TblKIKView")
                gvListKIK.DataBind()
                Session("WarningListKIK") = "Selected KIK data can't be found!"
                showMessage(Session("WarningListKIK"), 2)
            End If
        Else
            mpeListKIK.Show()
        End If
    End Sub 'OK

    Protected Sub gvListKIK_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListKIK.PageIndexChanging
        If UpdateCheckedKIK2() Then
            gvListKIK.PageIndex = e.NewPageIndex
            gvListKIK.DataSource = Session("TblKIKView")
            gvListKIK.DataBind()
        End If
        mpeListKIK.Show()
    End Sub 'OK

    Protected Sub lkbAddToListListKIK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListKIK.Click
        If Not Session("TblKIK") Is Nothing Then
            If UpdateCheckedKIK() Then
                Dim dtTbl As DataTable = Session("TblKIK")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If wono.Text <> "" Then
                            If dtView(C1)("wono") <> "" Then
                                wono.Text &= ";" + vbCrLf + dtView(C1)("wono")
                            End If
                        Else
                            If dtView(C1)("wono") <> "" Then
                                wono.Text &= dtView(C1)("wono")
                            End If
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHiddenListKIK, PanelListKIK, mpeListKIK, False)
                Else
                    Session("WarningListKIK") = "Please select KIK to add to list!"
                    showMessage(Session("WarningListKIK"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListKIK") = "Please show some KIK data first!"
            showMessage(Session("WarningListKIK"), 2)
            Exit Sub
        End If
    End Sub 'OK

    Protected Sub lkbListKIK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListKIK.Click
        cProc.SetModalPopUpExtender(btnHiddenListKIK, PanelListKIK, mpeListKIK, False)
    End Sub 'OK

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub 'OK

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub 'OK

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub 'OK

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmWOConfirm.aspx?awal=true")
    End Sub 'OK

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub 'OK

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub 'OK

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub 'OK

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Dispose()
        report.Close()
    End Sub 'OK
#End Region


    'Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
    '    If IsValidPeriod() Then
    '        Type.Text = "MAT"
    '        InitFilterDDLCat1()
    '        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
    '        Session("TblMat") = Nothing : Session("TblMatView") = Nothing : gvListMat.DataSource = Nothing : gvListMat.DataBind()
    '        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    '    Else
    '        Exit Sub
    '    End If
    'End Sub

    'Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
    '    If ddlType.SelectedValue.ToUpper <> "ALL" Then
    '        LabelMat6.Visible = True
    '        separate.Visible = True
    '        matrefcode.Visible = True
    '        btnSearchMat.Visible = True
    '        btnEraseMap.Visible = True
    '        If ddlType.SelectedValue = "Raw" Then
    '            lblTitleListMat.Text = "List of " & ddlType.SelectedValue & " Material"
    '            LabelMat6.Text = ddlType.SelectedValue & " Material"
    '        ElseIf ddlType.SelectedValue = "Gen" Then
    '            lblTitleListMat.Text = "List of General Material"
    '            LabelMat6.Text = "General Material"
    '        ElseIf ddlType.SelectedValue = "SP" Then
    '            lblTitleListMat.Text = "List of Spare Part"
    '            LabelMat6.Text = "Spare Part"
    '        End If
    '    Else
    '        LabelMat6.Visible = False
    '        separate.Visible = False
    '        matrefcode.Visible = False
    '        btnSearchMat.Visible = False
    '        btnEraseMap.Visible = False
    '    End If
    '    matrefcode.Text = ""
    'End Sub

End Class
