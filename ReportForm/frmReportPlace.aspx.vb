Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.Drawing.Printing

Partial Class ReportForm_ReportPlace
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Public folderReport As String = "~/Report/"
    Dim pkSize = New System.Drawing.Printing.PaperSize
    Dim PrintDoc1 As New System.Drawing.Printing.PrintDocument
#End Region

#Region "Procedures"
    Private Sub ShowReport(ByVal sCompCode As String, ByVal sOid As String, ByVal sRptName As String)
        'ambilkan untuk no Fakturnya
        Dim sNoFaktur As String = ""
        'select distinct * from QL_trnbelimst where trnbelimstoid=27
        sSql = "select distinct trnbelino AS noFaktur from QL_trnbelimst where trnbelimstoid=" & sOid & ""
        Dim objTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnbelimst")
        For x As Integer = 0 To objTbl.Rows.Count - 1
            sNoFaktur = sNoFaktur & "," & objTbl.Rows(x).Item("noFaktur").ToString
        Next
        If sNoFaktur.Trim <> "" Then
            sNoFaktur = sNoFaktur.Remove(0, 1)
        End If
        '===============================================================

        Try
            report.Load(Server.MapPath(folderReport & sRptName & ".rpt"))
            report.SetParameterValue("sOid", sOid)
            report.SetParameterValue("sCompCode", sCompCode)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("sNoFaktur", sNoFaktur)
            PrintDoc1.PrinterSettings.PrinterName = "Microsoft XPS Document Writer"
            Dim k As Integer
            For k = 0 To PrintDoc1.PrinterSettings.PaperSizes.Count - 1
                If PrintDoc1.PrinterSettings.PaperSizes.Item(k).PaperName = "PRKPNew" Then
                    pkSize = PrintDoc1.PrinterSettings.PaperSizes.Item(k)
                End If
            Next
            report.PrintOptions.PrinterName = "Microsoft XPS Document Writer"
            report.PrintOptions.PaperSize = CType(pkSize.RawKind, CrystalDecisions.Shared.PaperSize)
            report.PrintOptions.PaperSource = CrystalDecisions.Shared.PaperSource.Manual
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, sRptName)
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
        End Try
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Dim sOid As String = Request.QueryString("oid")
        Dim sRptName As String = Request.QueryString("rpt")
        Dim sCompCode As String = Request.QueryString("code")
        'Dim sType As String = Request.QueryString("type")
        Page.Title = CompnyName & " - Report Preview"
        If Not Page.IsPostBack Then
            ShowReport(sCompCode, sOid, sRptName)
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not report Is Nothing Then
                If report.IsLoaded Then
                    report.Dispose()
                    report.Close()
                End If
            End If
        Catch ex As Exception
            report.Dispose()
            report.Close()
        End Try
    End Sub
#End Region

End Class
