Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_ExpenseReport
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function UpdateCheckedSO() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSO") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSO")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListSO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "somstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblSO") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedSO2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSO")
            Dim dtTbl2 As DataTable = Session("TblSOView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListSO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "somstoid=" & cbOid
                                dtView2.RowFilter = "somstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblSO") = dtTbl
                Session("TblSOView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedShipment() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblShipment") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblShipment")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListShipment.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListShipment.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "shipmentmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblShipment") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedShipment2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblShipmentView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblShipment")
            Dim dtTbl2 As DataTable = Session("TblShipmentView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListShipment.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListShipment.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "shipmentmstoid=" & cbOid
                                dtView2.RowFilter = "shipmentmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblShipment") = dtTbl
                Session("TblShipmentView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

#End Region

#Region "Procedures"

	Private Sub BindSupplierData()
        'sSql = "SELECT 0 selected,cmpcode,cashbankoid,cashbankno,Convert(Char(10),cashbankdate,101) AS cashbankdate,periodacctg,cashbanktaxtype FROM QL_trncashbankmst WHERE cmpcode='" & CompnyCode & "' AND cashbankgroup='EXPENSE' AND cashbankdate BETWEEN '" & FilterPeriod1.Text & "' AND '" & FilterPeriod2.Text & "' AND cashbankno LIKE '%" & Tchar(FilterTextSupplier.Text.Trim) & "%' ORDER BY cashbankno"

        'Dim dtSupp As DataTable = cKon.ambiltabel(sSql, "QL_mstsupp")
        'gvSupplier.DataSource = dtSupp : gvSupplier.DataBind()
        'Session("QL_mstsupp") = dtSupp
        'gvSupplier.Visible = True
        'gvSupplier.SelectedIndex = -1
    End Sub

    Private Sub BindListSO()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, som.cashbankoid somstoid, som.cashbankno sono, som.cashbankdate, CONVERT(VARCHAR(10), som.cashbankdate, 101) AS sodate, som.cashbanknote AS somstnote FROM QL_trncashbankmst som WHERE cmpcode='" & CompnyCode & "' AND cashbankgroup='EXPENSE' AND cashbankstatus='Post' AND cashbankdate >= '" & Format(CDate(FilterPeriod1.Text), "yyyy-MM-dd") & "' AND cashbankdate <= '" & Format(CDate(FilterPeriod2.Text), "yyyy-MM-dd") & "' ORDER BY som.cashbankdate DESC"
        Session("TblSO") = cKon.ambiltabel(sSql, "QL_trnsomst")
    End Sub

    Private Sub BindListShipment()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, shm.acctgoid shipmentmstoid, shd.acctgcode shipmentno, shd.acctgdesc AS custname FROM QL_trncashbankgl shm INNER JOIN QL_mstacctg shd ON shm.acctgoid=shd.acctgoid INNER JOIN QL_trncashbankmst c ON shm.cashbankoid=c.cashbankoid WHERE shm.cmpcode='" & CompnyCode & "' AND c.cashbankgroup='EXPENSE' AND c.cashbankstatus='Post' AND c.cashbankdate >= '" & FilterPeriod1.Text & "' AND c.cashbankdate <= '" & FilterPeriod2.Text & "'"

        If sono.Text <> "" Then
            Dim sSOno() As String = Split(sono.Text, ";")
            sSql &= "  AND ( "
            For c1 As Integer = 0 To sSOno.Length - 1
                sSql &= " c.cashbankno LIKE '%" & Tchar(sSOno(c1)) & "%'"
                If c1 < sSOno.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        sSql &= " ORDER BY shd.acctgcode"
        Session("TblShipment") = cKon.ambiltabel(sSql, "QL_trndomst")
    End Sub

	Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
		Dim strCaption As String = CompnyCode
		If iType = 1 Then ' Error
			imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
		ElseIf iType = 2 Then ' Warning
			imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
		ElseIf iType = 3 Then ' Information
			imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
		Else
			imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
		End If
		lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
		cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
	End Sub

	Private Sub InitDDLPerson()
		' Fill DDL PIC
		sSql = "SELECT personoid, personname FROM QL_mstperson WHERE activeflag='ACTIVE' AND cmpcode='" & CompnyCode & "'"
        FillDDLWithAdditionalText(PicDdl, sSql, "ALL", "ALL")

        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE' "
        FillDDL(FilterDDLDiv, sSql)
	End Sub

    Private Sub ShowReport(ByVal sType As String)
        Dim sWhere As String = ""
        Dim sAnd As String = ""
        Dim sPeriod As String = ""
        Dim rptName As String = "EXPENSE_REPORT_" & Format(CDate(FilterPeriod1.Text), "ddMMMyy").ToUpper & "_" & Format(CDate(FilterPeriod2.Text), "ddMMMyy").ToUpper & ""

        Try
            sWhere &= "WHERE cb.cmpcode='" & FilterDDLDiv.SelectedValue & "' AND cb.cashbankgroup='EXPENSE'"

            If IsValidPeriod() Then
                sPeriod = Format(CDate(FilterPeriod1.Text), "dd MMMM yyyy") & " s/d " & Format(CDate(FilterPeriod2.Text), "dd MMMM yyyy")
                sWhere &= " AND cb.cashbankdate BETWEEN '" & FilterPeriod1.Text & "' AND '" & FilterPeriod2.Text & "'"
            Else
                Exit Sub
            End If

            If sono.Text <> "" Then
                Dim sSOno() As String = Split(sono.Text, ";")
                sWhere &= " AND ("
                For c1 As Integer = 0 To sSOno.Length - 1
                    sWhere &= " cb.cashbankno LIKE '%" & Tchar(sSOno(c1)) & "%'"
                    If c1 < sSOno.Length - 1 Then
                        sWhere &= " OR "
                    End If
                Next
                sWhere &= ")"
            End If

            If shipmentno.Text <> "" Then
                Dim sShipmentno() As String = Split(shipmentno.Text, ";")
                sWhere &= " AND ("
                For c1 As Integer = 0 To sShipmentno.Length - 1
                    sWhere &= " a.acctgcode LIKE '%" & Tchar(sShipmentno(c1)) & "%'"
                    If c1 < sShipmentno.Length - 1 Then
                        sWhere &= " OR "
                    End If
                Next
                sWhere &= ")"
            Else
                sWhere &= " AND a.acctgoid IN (SELECT ax.acctgoid FROM QL_mstacctg ax WHERE ax.acctggrp1 IN ('BIAYA USAHA', 'PENDAPATAN DAN BIAYA LAIN-LAIN'))"
            End If

            If CBsTatus.Checked = True Then
                sWhere &= " AND cb.cashbankstatus ='" & sTatusDDL.SelectedValue & "' "
            End If

            If PicDdl.SelectedItem.Text <> "ALL" Then
                sWhere &= " AND p.personoid=" & PicDdl.SelectedValue
            End If

            If shipmentno.Text <> "" Then
                If sType = "Print Excel" Then
                    report.Load(Server.MapPath(folderReport & "rptExpenseReportExc.rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptExpenseReport1.rpt"))
                End If
            Else
                If sType = "Print Excel" Then
                    report.Load(Server.MapPath(folderReport & "rptExpenseReportExc.rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptExpenseReport.rpt"))
                End If
            End If

            cProc.SetDBLogonForReport(report)
            report.SetParameterValue("sCompnyName", FilterDDLDiv.SelectedItem.Text)
            report.SetParameterValue("sPeriod", sPeriod)
            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("sCurrency", FilterCurrency.SelectedValue)

            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, rptName)
                report.Close()
                report.Dispose()
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, rptName)
                report.Close()
                report.Dispose()
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub
#End Region

#Region "Events"
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("UserID") = "" Then
			Response.Redirect("~/Other/login.aspx")
		End If
		If Request.QueryString("awal") = "true" Then
			' Simpan session ke variabel temporary supaya tidak hilang
			Dim userId As String = Session("UserID")
			Dim xsetAcc As DataTable = Session("SpecialAccess")
			Dim appLimit As Decimal = Session("ApprovalLimit")
			Dim xsetRole As DataTable = Session("Role")
			Dim cmpcode As String = Session("CompnyCode")
			' Clear all session
			Session.Clear()
			' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
			Session("UserID") = userId
			Session("SpecialAccess") = xsetAcc
			Session("ApprovalLimit") = appLimit
			Session("Role") = xsetRole
			Session("CompnyCode") = cmpcode
			' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
			Response.Redirect("~\ReportForm\frmExpense.aspx")
		End If
		If checkPagePermission("~\ReportForm\frmExpense.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
		End If
        Page.Title = CompnyName & " - Expense Report"

		If Not Page.IsPostBack Then
            InitDDLPerson()
            FilterDDLDiv_SelectedIndexChanged(Nothing, Nothing)
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        End If
	End Sub

	Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListSO") Is Nothing And Session("EmptyListSO") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListSO") Then
                Session("EmptyListSO") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
            End If
        End If
        If Not Session("WarningListSO") Is Nothing And Session("WarningListSO") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListSO") Then
                Session("WarningListSO") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
            End If
        End If

        If Not Session("EmptyListShipment") Is Nothing And Session("EmptyListShipment") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListShipment") Then
                Session("EmptyListShipment") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListShipment, PanelListShipment, mpeListShipment, True)
            End If
        End If
        If Not Session("WarningListShipment") Is Nothing And Session("WarningListShipment") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListShipment") Then
                Session("WarningListShipment") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListShipment, PanelListShipment, mpeListShipment, True)
            End If
        End If
	End Sub

	Protected Sub FilterDDLDiv_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLDiv.SelectedIndexChanged
        Session("ViewReport") = "False" : crvReportForm.ReportSource = Nothing : crvReportForm.DataBind()
    End Sub

	Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
		ShowReport("View")
	End Sub

	Protected Sub sTatusDDL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
		ShowReport("View")
	End Sub

	Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
		ShowReport("Print PDF")
	End Sub

	Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
		ShowReport("Print Excel")
	End Sub

	Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmExpense.aspx?awal=true")
    End Sub

	Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
		report.Dispose() : report.Close()
	End Sub

	Protected Sub DDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLType.SelectedIndexChanged
		If DDLType.SelectedValue = "Summary" Then
			lbCurr.Visible = True : lbSeptCurr.Visible = True : FilterCurrency.Visible = True
		Else
			lbCurr.Visible = False : lbSeptCurr.Visible = False : FilterCurrency.Visible = False
		End If
    End Sub

    Protected Sub btnSearchSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindSupplierData()
    End Sub

    Protected Sub imbFindSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsValidPeriod() Then
            DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
            Session("TblSO") = Nothing : Session("TblSOView") = Nothing : gvListSO.DataSource = Nothing : gvListSO.DataBind()
            cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbEraseSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        sono.Text = ""
    End Sub

    Protected Sub imbFindShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsValidPeriod() Then
            DDLFilterListShipment.SelectedIndex = -1 : txtFilterListShipment.Text = ""
            Session("TblShipment") = Nothing : Session("TblShipmentView") = Nothing : gvListShipment.DataSource = Nothing : gvListShipment.DataBind()
            cProc.SetModalPopUpExtender(btnHiddenListShipment, PanelListShipment, mpeListShipment, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbEraseShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        shipmentno.Text = ""
    End Sub

    Protected Sub btnFindListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("TblSO") Is Nothing Then
            BindListSO()
            If Session("TblSO").Rows.Count <= 0 Then
                Session("EmptyListSO") = "No Bukti data can't be found!"
                showMessage(Session("EmptyListSO"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListSO.SelectedValue & " LIKE '%" & Tchar(txtFilterListSO.Text) & "%'"
        If UpdateCheckedSO() Then
            Dim dv As DataView = Session("TblSO").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblSOView") = dv.ToTable
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                dv.RowFilter = ""
                mpeListSO.Show()
            Else
                dv.RowFilter = ""
                Session("TblSOView") = Nothing
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                Session("WarningListSO") = "No Bukti data can't be found!"
                showMessage(Session("WarningListSO"), 2)
            End If
        Else
            mpeListSO.Show()
        End If
    End Sub

    Protected Sub btnViewAllListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
        If Session("TblSO") Is Nothing Then
            BindListSO()
            If Session("TblSO").Rows.Count <= 0 Then
                Session("EmptyListSO") = "No Bukti data can't be found!"
                showMessage(Session("EmptyListSO"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedSO() Then
            Dim dt As DataTable = Session("TblSO")
            Session("TblSOView") = dt
            gvListSO.DataSource = Session("TblSOView")
            gvListSO.DataBind()
        End If
        mpeListSO.Show()
    End Sub

    Protected Sub btnSelectAllSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "somstoid=" & dtTbl.Rows(C1)("somstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSO") = objTbl
                Session("TblSOView") = dtTbl
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
            End If
            mpeListSO.Show()
        Else
            Session("WarningListSO") = "Please show some No Bukti data first!"
            showMessage(Session("WarningListSO"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "somstoid=" & dtTbl.Rows(C1)("somstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSO") = objTbl
                Session("TblSOView") = dtTbl
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
            End If
            mpeListSO.Show()
        Else
            Session("WarningListSO") = "Please show some No Bukti data first!"
            showMessage(Session("WarningListSO"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("TblSO") Is Nothing Then
            Session("WarningListSO") = "Selected SO data can't be found!"
            showMessage(Session("WarningListSO"), 2)
            Exit Sub
        End If
        If UpdateCheckedSO() Then
            Dim dtTbl As DataTable = Session("TblSO")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
                Session("TblSOView") = dtView.ToTable
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                dtView.RowFilter = ""
                mpeListSO.Show()
            Else
                dtView.RowFilter = ""
                Session("TblSOView") = Nothing
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                Session("WarningListSO") = "Selected No Bukti data can't be found!"
                showMessage(Session("WarningListSO"), 2)
            End If
        Else
            mpeListSO.Show()
        End If
    End Sub

    Protected Sub gvListSO_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)

    End Sub

    Protected Sub gvListSO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        If UpdateCheckedSO2() Then
            gvListSO.PageIndex = e.NewPageIndex
            gvListSO.DataSource = Session("TblSOView")
            gvListSO.DataBind()
        End If
        mpeListSO.Show()
    End Sub

    Protected Sub lkbAddToListListSO_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Session("TblSO") Is Nothing Then
            If UpdateCheckedSO() Then
                Dim dtTbl As DataTable = Session("TblSO")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If sono.Text <> "" Then
                            If dtView(C1)("sono") <> "" Then
                                sono.Text &= ";" + vbCrLf + dtView(C1)("sono")
                            End If
                        Else
                            If dtView(C1)("sono") <> "" Then
                                sono.Text &= dtView(C1)("sono")
                            End If
                        End If
                    Next
                    dtView.RowFilter = ""
                    'DDLBusUnit.Enabled = False
                    cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, False)
                Else
                    Session("WarningListSO") = "Please select No Bukti to add to list!"
                    showMessage(Session("WarningListSO"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListSO") = "Please show some No Bukti data first!"
            showMessage(Session("WarningListSO"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListSO_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, False)
    End Sub

    Protected Sub btnFindListShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("TblShipment") Is Nothing Then
            BindListShipment()
            If Session("TblShipment").Rows.Count <= 0 Then
                Session("EmptyListShipment") = "Keterangan data can't be found!"
                showMessage(Session("EmptyListShipment"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListShipment.SelectedValue & " LIKE '%" & Tchar(txtFilterListShipment.Text) & "%'"
        If UpdateCheckedShipment() Then
            Dim dv As DataView = Session("TblShipment").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblShipmentView") = dv.ToTable
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
                dv.RowFilter = ""
                mpeListShipment.Show()
            Else
                dv.RowFilter = ""
                Session("TblShipmentView") = Nothing
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
                Session("WarningListShipment") = "Keterangan data can't be found!"
                showMessage(Session("WarningListShipment"), 2)
            End If
        Else
            mpeListShipment.Show()
        End If
    End Sub

    Protected Sub btnViewAllListShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        DDLFilterListShipment.SelectedIndex = -1 : txtFilterListShipment.Text = ""
        If Session("TblShipment") Is Nothing Then
            BindListShipment()
            If Session("TblShipment").Rows.Count <= 0 Then
                Session("EmptyListShipment") = "Keterangan data can't be found!"
                showMessage(Session("EmptyListShipment"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedShipment() Then
            Dim dt As DataTable = Session("TblShipment")
            Session("TblShipmentView") = dt
            gvListShipment.DataSource = Session("TblShipmentView")
            gvListShipment.DataBind()
        End If
        mpeListShipment.Show()
    End Sub

    Protected Sub btnSelectAllShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not Session("TblShipmentView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblShipmentView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblShipment")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "shipmentmstoid=" & dtTbl.Rows(C1)("shipmentmstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblShipment") = objTbl
                Session("TblShipmentView") = dtTbl
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
            End If
            mpeListShipment.Show()
        Else
            Session("WarningListShipment") = "Please show some Keterangan data first!"
            showMessage(Session("WarningListShipment"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not Session("TblShipmentView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblShipmentView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblShipment")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "shipmentmstoid=" & dtTbl.Rows(C1)("shipmentmstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblShipment") = objTbl
                Session("TblShipmentView") = dtTbl
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
            End If
            mpeListShipment.Show()
        Else
            Session("WarningListShipment") = "Please show some Keterangan data first!"
            showMessage(Session("WarningListShipment"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedShipment_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("TblShipment") Is Nothing Then
            Session("WarningListShipment") = "Selected Shipment data can't be found!"
            showMessage(Session("WarningListShipment"), 2)
            Exit Sub
        End If
        If UpdateCheckedShipment() Then
            Dim dtTbl As DataTable = Session("TblShipment")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListShipment.SelectedIndex = -1 : txtFilterListShipment.Text = ""
                Session("TblShipmentView") = dtView.ToTable
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
                dtView.RowFilter = ""
                mpeListShipment.Show()
            Else
                dtView.RowFilter = ""
                Session("TblShipmentView") = Nothing
                gvListShipment.DataSource = Session("TblShipmentView")
                gvListShipment.DataBind()
                Session("WarningListShipment") = "Selected Keterangan data can't be found!"
                showMessage(Session("WarningListShipment"), 2)
            End If
        Else
            mpeListShipment.Show()
        End If
    End Sub

    Protected Sub gvListShipment_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        If UpdateCheckedShipment2() Then
            gvListShipment.PageIndex = e.NewPageIndex
            gvListShipment.DataSource = Session("TblShipmentView")
            gvListShipment.DataBind()
        End If
        mpeListShipment.Show()
    End Sub

    Protected Sub lkbAddToListListShipment_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Session("TblShipment") Is Nothing Then
            If UpdateCheckedShipment() Then
                Dim dtTbl As DataTable = Session("TblShipment")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If shipmentno.Text <> "" Then
                            If dtView(C1)("shipmentno") <> "" Then
                                shipmentno.Text &= ";" + vbCrLf + dtView(C1)("shipmentno")
                            End If
                        Else
                            If dtView(C1)("shipmentno") <> "" Then
                                shipmentno.Text &= dtView(C1)("shipmentno")
                            End If
                        End If
                    Next
                    dtView.RowFilter = ""
                    'DDLBusUnit.Enabled = False
                    cProc.SetModalPopUpExtender(btnHiddenListShipment, PanelListShipment, mpeListShipment, False)
                Else
                    Session("WarningListShipment") = "Please select Keterangan to add to list!"
                    showMessage(Session("WarningListShipment"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListShipment") = "Please show some Keterangan data first!"
            showMessage(Session("WarningListShipment"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListShipment_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHiddenListShipment, PanelListShipment, mpeListShipment, False)
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs)
        ShowReport("View")
    End Sub

#End Region

End Class
