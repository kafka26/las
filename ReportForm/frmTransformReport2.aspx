<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="frmTransformReport2.aspx.vb" Inherits="ReportForm_Transformation2" title="" EnableEventValidation="false" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Material Transformation Report" CssClass="Title" ForeColor="SaddleBrown"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <table style="width: 100%">
                                <tr>
                                    <td align="center" valign="top" char="Label">
                                        <table>
                                            <tr>
                                                <td align="center" class="Label" valign="top">
                                                    <asp:UpdatePanel ID="upReportForm" runat="server">
                                                        <ContentTemplate>
<asp:Panel id="pnlReportForm" runat="server" Width="500px" DefaultButton="btnViewReport"><TABLE><TBODY><TR id="BusinessUnitA" runat="server" visible="true"><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text="Business Unit"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpText" Width="155px" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label8" runat="server" Text="Type"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="FilterDDLType" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True"><asp:ListItem Value="Summary">Summary</asp:ListItem>
<asp:ListItem>Detail LV5</asp:ListItem>
<asp:ListItem>Detail</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label2" runat="server" Text="Status"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" __designer:wfdid="w22"><asp:ListItem>All</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem Value="Post">Post/Closed</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left rowSpan=2><asp:DropDownList id="FilterDDLDate" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w21"></asp:DropDownList></TD><TD class="Label" align=center rowSpan=2>:</TD></TR><TR><TD class="Label" align=left><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="60px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="CalPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="lblTo" runat="server" Text="to"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="60px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="CalPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" Format="MM/dd/yyyy" PopupButtonID="CalPeriod1" TargetControlID="FilterPeriod1">
                                                                            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="FilterPeriod1" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear">
                                                                            </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" Format="MM/dd/yyyy" PopupButtonID="CalPeriod2" TargetControlID="FilterPeriod2">
                                                                            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" TargetControlID="FilterPeriod2" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear">
                                                                            </ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD class="Label" align=left rowSpan=2><asp:Label id="lbldept" runat="server" Text="Department" __designer:wfdid="w9"></asp:Label></TD><TD class="Label" align=center rowSpan=2>:</TD><TD style="HEIGHT: 9px" class="Label" align=left><asp:DropDownList id="DDLDept" runat="server" CssClass="inpText" Width="275px" __designer:wfdid="w5"></asp:DropDownList>&nbsp;<asp:ImageButton id="btnAddDept" runat="server" ImageUrl="~/Images/plus.png" ImageAlign="AbsMiddle" __designer:wfdid="w6"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 9px" class="Label" align=left><asp:ListBox id="lbDept" runat="server" CssClass="inpTextDisabled" Width="275px" __designer:wfdid="w7" Rows="3"></asp:ListBox>&nbsp;<asp:ImageButton id="btnMinDept" runat="server" ImageUrl="~/Images/minus.png" ImageAlign="AbsMiddle" __designer:wfdid="w8"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left rowSpan=1><asp:Label id="Label6" runat="server" Text="Type" __designer:wfdid="w10"></asp:Label></TD><TD class="Label" align=center rowSpan=1>:</TD><TD style="HEIGHT: 9px" class="Label" align=left><asp:DropDownList id="transformtype" runat="server" CssClass="inpText" Width="184px" AutoPostBack="True" __designer:wfdid="w11"><asp:ListItem>All</asp:ListItem>
<asp:ListItem>Log To Log</asp:ListItem>
<asp:ListItem>Log To Sawn Timber</asp:ListItem>
<asp:ListItem>Log To WIP/Non WIP</asp:ListItem>
<asp:ListItem>Sawn Timber To Sawn Timber</asp:ListItem>
<asp:ListItem>WIP/Non WIP To WIP/Non WIP</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="TDFrWH1" class="Label" align=left rowSpan=2 runat="server"><asp:Label id="lblFrWh" runat="server" Text="From Warehouse" __designer:wfdid="w12"></asp:Label></TD><TD id="TDFrWH2" class="Label" align=center rowSpan=2 runat="server"><asp:Label id="lblSepFrWh" runat="server" Text=":" __designer:wfdid="w13"></asp:Label></TD><TD id="TDFrWH3" class="Label" align=left runat="server"><asp:DropDownList id="DDLFrWh" runat="server" CssClass="inpText" Width="275px" __designer:wfdid="w14"></asp:DropDownList>&nbsp;<asp:ImageButton id="btnAddFrWh" runat="server" ImageUrl="~/Images/plus.png" ImageAlign="AbsMiddle" __designer:wfdid="w15"></asp:ImageButton></TD></TR><TR><TD id="TDFrWH4" class="Label" align=left runat="server"><asp:ListBox id="lbFrWh" runat="server" CssClass="inpTextDisabled" Width="275px" __designer:wfdid="w16" Rows="3"></asp:ListBox>&nbsp;<asp:ImageButton id="btnMinFrWh" runat="server" ImageUrl="~/Images/minus.png" ImageAlign="AbsMiddle" __designer:wfdid="w17"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbsupp" runat="server" Text="Group Name" __designer:wfdid="w23"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="suppoid" runat="server" CssClass="inpText" Width="305px" __designer:wfdid="w19">
            </asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:DropDownList id="DDLTransNo" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True"><asp:ListItem>Transform No.</asp:ListItem>
<asp:ListItem Value="Draft No.">Draft No.</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="transformno" runat="server" CssClass="inpText" Width="178px" TextMode="MultiLine"></asp:TextBox> <asp:ImageButton id="imbFindTrans" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbEraseTrans" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblGrouping" runat="server" Text="Grouping"></asp:Label></TD><TD class="Label" align=center><asp:Label id="lblg" runat="server" Text=":"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="DDLGrouping" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblSorting" runat="server" Text="Sorting"></asp:Label></TD><TD class="Label" align=center><asp:Label id="lbls" runat="server" Text=":"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="DDLSorting" runat="server" CssClass="inpText" Width="100px"></asp:DropDownList>&nbsp;<asp:Label id="lblmin" runat="server" Text="-"></asp:Label>&nbsp;<asp:DropDownList id="DDLOrderby" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"><asp:ListItem Value="ASC">ASC</asp:ListItem>
<asp:ListItem Value="DESC">DESC</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnExportToPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnExportToExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upReportForm">
                                                                                <ProgressTemplate>
                                                                                    <span></span><span style="font-weight: bold; font-size: 10pt; color: purple">
                                                                                        <asp:Image ID="imgLoading" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />
                                                                                        Please Wait .....</span><br />
                                                                                </ProgressTemplate>
                                                                            </asp:UpdateProgress> </TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnViewReport" />
                                                            <asp:PostBackTrigger ControlID="btnExportToPdf" />
                                                            <asp:PostBackTrigger ControlID="btnExportToExcel" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Label" style="height: 10px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="Label" valign="top" style="height: 61px">
                                                    <CR:CrystalReportViewer ID="crvReportForm" runat="server" AutoDataBind="True" HasCrystalLogo="False"
                                                        HasDrillUpButton="False" HasExportButton="False" HasPrintButton="False" HasToggleGroupTreeButton="False"
                                                        HasViewList="False" Height="50px" Width="350px" />
                                                </td>
                                            </tr>
                                        </table>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <HeaderTemplate>
                            &nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
    </table>
    &nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:UpdatePanel id="upListTrans" runat="server"><contenttemplate>
<asp:Panel id="pnlListTrans" runat="server" CssClass="modalBox" Width="800px" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListTrans" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Material Transformation"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListTrans" runat="server" Width="100%" DefaultButton="btnFindListTrans">Filter : <asp:DropDownList id="FilterDDLListTrans" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="transformno">Transform No.</asp:ListItem>
<asp:ListItem Value="transformmstnote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListTrans" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListTrans" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListTrans" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3><asp:ImageButton id="btnSelectAllTrans" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" __designer:wfdid="w56"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneTrans" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" __designer:wfdid="w57"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedTrans" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" __designer:wfdid="w58"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListTrans" runat="server" BorderColor="Black" ForeColor="#333333" Width="96%" GridLines="None" PageSize="5" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" BorderStyle="None">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbLM" runat="server" __designer:wfdid="w20" ToolTip='<%# eval("transformmstoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="transformmstoid" HeaderText="Draft No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transformno" HeaderText="Transform No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transformdate" HeaderText="Transform Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="deptname" HeaderText="Department">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transformtype" HeaderText="Type">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="fromwh" HeaderText="From Warehouse">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Group Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transformmstnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblInfoOnListGV" runat="server" CssClass="Important"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView>&nbsp; </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListTrans" runat="server" __designer:wfdid="w67">[ Add To List ]</asp:LinkButton>&nbsp;<asp:LinkButton id="lkbCloseListTrans" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListTrans" runat="server" TargetControlID="btnHideListTrans" BackgroundCssClass="modalBackground" PopupControlID="pnlListTrans" PopupDragHandleControlID="lblListTrans" Drag="True">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListTrans" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    &nbsp; &nbsp; &nbsp;
    <asp:UpdatePanel id="upListPallet" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListPallet" runat="server" CssClass="modalBox" Width="800px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListPallet" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Pallet" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="HEIGHT: 64px" class="Label" align=center colSpan=3><asp:Panel id="pnlFindListPallet" runat="server" Width="100%" DefaultButton="btnFindListPallet">Filter : <asp:DropDownList id="FilterDDLListPallet" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="palletno">Pallet No.</asp:ListItem>
<asp:ListItem Value="palletcode">Code</asp:ListItem>
<asp:ListItem Value="palletlongdesc">Description</asp:ListItem>
<asp:ListItem Value="palletrefno">Ref. No.</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListPallet" runat="server" CssClass="inpText" Width="200px"></asp:TextBox> <asp:ImageButton id="btnFindListPallet" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListPallet" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3><asp:ImageButton id="btnSelectAllPallet" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" __designer:wfdid="w62"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNonePallet" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" __designer:wfdid="w63"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedPallet" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" __designer:wfdid="w64"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListPallet" runat="server" ForeColor="#333333" Width="99%" GridLines="None" PageSize="5" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><HeaderTemplate>
&nbsp;
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="cbOnListMat" runat="server" __designer:wfdid="w65" ToolTip='<%# eval("palletoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="palletno" HeaderText="Pallet No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="palletcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="palletlongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="palletvolume" HeaderText="Volume">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="palletrefno" HeaderText="Ref. No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label114" runat="server" Text="No data found !!" CssClass="Important"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbAddToListListPallet" runat="server">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lkbCloseListPallet" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListPallet" runat="server" TargetControlID="btnHideListPallet" BackgroundCssClass="modalBackground" PopupControlID="pnlListPallet" PopupDragHandleControlID="lblTitleListPallet" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListPallet" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

