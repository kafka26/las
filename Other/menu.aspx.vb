Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class menu
    Inherits System.Web.UI.Page

#Region "Variables"
    Dim dv As DataView
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_conn"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
#End Region

#Region "Procedures"
    Private Sub SetupMenu()
        Dim bVisible As Boolean = False
        Dim bSubVisible As Boolean = False
        Dim bSubSubVisible As Boolean = False
        dv = Session("Role").DefaultView
        Dim iTotalMenuPerRow As Integer = 8
        Dim iRow As Integer = 1
        Dim iRowSpan As Integer = 0
        Dim sFormatHeader As String = "<tr><td align=" & Chr(34) & "left" & Chr(34) & " colspan=" & Chr(34) & "2" & Chr(34) & " valign=" & Chr(34) & "middle" & Chr(34) & "><span style=" & Chr(34) & "color: red; font-weight: bold; font-size: small;" & Chr(34) & ">"
        Dim sFormatMenu As String = "<td align=" & Chr(34) & "center" & Chr(34) & " style=" & Chr(34) & "width: 100px; height: 100px;" & Chr(34) & " valign=" & Chr(34) & "middle" & Chr(34) & ">"

        ' === SETUP MODULE ===
        LABELSETUP.Text = "<table>"
        ' === MASTER
        dv.RowFilter = "FORMMODULE='SETUP' AND FORMTYPE='MASTER'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            iRowSpan = Math.Ceiling(dv.Count / iTotalMenuPerRow)
            LABELSETUP.Text &= sFormatHeader & "Master :</span></td></tr>"
            LABELSETUP.Text &= "<tr><td align=" & Chr(34) & "center" & Chr(34) & " style=" & Chr(34) & "width: 50px;" & Chr(34) & " valign=" & Chr(34) & "middle" & Chr(34) & " rowspan=" & Chr(34) & iRowSpan.ToString & Chr(34) & "></td>"
            For C1 As Integer = 0 To dv.Count - 1
                If iRow = 1 And C1 <> 0 Then
                    LABELSETUP.Text &= "<tr>"
                End If
                LABELSETUP.Text &= sFormatMenu & SetLinking(dv.Item(C1).Item("FORMIMAGE").ToString, dv.Item(C1).Item("FORMADDRESS").ToString & "?awal=true", dv.Item(C1).Item("FORMMENU").ToString) & "</td>"
                If iRow = iTotalMenuPerRow Then
                    LABELSETUP.Text &= "</tr>"
                    iRow = 1
                Else
                    iRow += 1
                End If
            Next
            bVisible = True
        End If
        dv.RowFilter = ""
        iRow = 1
        ' === TRANSACTION
        dv.RowFilter = "FORMMODULE='SETUP' AND FORMTYPE='TRANSACTION'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            iRowSpan = Math.Ceiling(dv.Count / iTotalMenuPerRow)
            LABELSETUP.Text &= sFormatHeader & "Transaction :</span></td></tr>"
            LABELSETUP.Text &= "<tr><td align=" & Chr(34) & "center" & Chr(34) & " style=" & Chr(34) & "width: 50px;" & Chr(34) & " valign=" & Chr(34) & "middle" & Chr(34) & " rowspan=" & Chr(34) & iRowSpan.ToString & Chr(34) & "></td>"
            For C1 As Integer = 0 To dv.Count - 1
                If iRow = 1 And C1 <> 0 Then
                    LABELSETUP.Text &= "<tr>"
                End If
                LABELSETUP.Text &= sFormatMenu & SetLinking(dv.Item(C1).Item("FORMIMAGE").ToString, dv.Item(C1).Item("FORMADDRESS").ToString & "?awal=true", dv.Item(C1).Item("FORMMENU").ToString) & "</td>"
                If iRow = iTotalMenuPerRow Then
                    LABELSETUP.Text &= "</tr>"
                    iRow = 1
                Else
                    iRow += 1
                End If
            Next
            bVisible = True
        End If
        dv.RowFilter = ""
        iRow = 1
        ' === REPORT
        dv.RowFilter = "FORMMODULE='SETUP' AND FORMTYPE='REPORT'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            iRowSpan = Math.Ceiling(dv.Count / iTotalMenuPerRow)
            LABELSETUP.Text &= sFormatHeader & "Report :</span></td></tr>"
            LABELSETUP.Text &= "<tr><td align=" & Chr(34) & "center" & Chr(34) & " style=" & Chr(34) & "width: 50px;" & Chr(34) & " valign=" & Chr(34) & "middle" & Chr(34) & " rowspan=" & Chr(34) & iRowSpan.ToString & Chr(34) & "></td>"
            For C1 As Integer = 0 To dv.Count - 1
                If iRow = 1 And C1 <> 0 Then
                    LABELSETUP.Text &= "<tr>"
                End If
                LABELSETUP.Text &= sFormatMenu & SetLinking(dv.Item(C1).Item("FORMIMAGE").ToString, dv.Item(C1).Item("FORMADDRESS").ToString & "?awal=true", dv.Item(C1).Item("FORMMENU").ToString) & "</td>"
                If iRow = iTotalMenuPerRow Then
                    LABELSETUP.Text &= "</tr>"
                    iRow = 1
                Else
                    iRow += 1
                End If
            Next
            bVisible = True
        End If
        dv.RowFilter = ""
        iRow = 1
        LABELSETUP.Text &= "</table>"
        TabContainer1.Tabs(1).Enabled = bVisible
        bVisible = False
        ' === END SETUP MODULE ===

        ' === PURCHASING MODULE ===
        LABELPURCHASING.Text = "<table>"
        ' === MASTER
        dv.RowFilter = "FORMMODULE='PURCHASING' AND FORMTYPE='MASTER'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            iRowSpan = Math.Ceiling(dv.Count / iTotalMenuPerRow)
            LABELPURCHASING.Text &= sFormatHeader & "Master :</span></td></tr>"
            LABELPURCHASING.Text &= "<tr><td align=" & Chr(34) & "center" & Chr(34) & " style=" & Chr(34) & "width: 50px;" & Chr(34) & " valign=" & Chr(34) & "middle" & Chr(34) & " rowspan=" & Chr(34) & iRowSpan.ToString & Chr(34) & "></td>"
            For C1 As Integer = 0 To dv.Count - 1
                If iRow = 1 And C1 <> 0 Then
                    LABELPURCHASING.Text &= "<tr>"
                End If
                LABELPURCHASING.Text &= sFormatMenu & SetLinking(dv.Item(C1).Item("FORMIMAGE").ToString, dv.Item(C1).Item("FORMADDRESS").ToString & "?awal=true", dv.Item(C1).Item("FORMMENU").ToString) & "</td>"
                If iRow = iTotalMenuPerRow Then
                    LABELPURCHASING.Text &= "</tr>"
                    iRow = 1
                Else
                    iRow += 1
                End If
            Next
            bVisible = True
        End If
        dv.RowFilter = ""
        iRow = 1
        ' === TRANSACTION
        dv.RowFilter = "FORMMODULE='PURCHASING' AND FORMTYPE='TRANSACTION'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            iRowSpan = Math.Ceiling(dv.Count / iTotalMenuPerRow)
            LABELPURCHASING.Text &= sFormatHeader & "Transaction :</span></td></tr>"
            LABELPURCHASING.Text &= "<tr><td align=" & Chr(34) & "center" & Chr(34) & " style=" & Chr(34) & "width: 50px;" & Chr(34) & " valign=" & Chr(34) & "middle" & Chr(34) & " rowspan=" & Chr(34) & iRowSpan.ToString & Chr(34) & "></td>"
            For C1 As Integer = 0 To dv.Count - 1
                If iRow = 1 And C1 <> 0 Then
                    LABELPURCHASING.Text &= "<tr>"
                End If
                LABELPURCHASING.Text &= sFormatMenu & SetLinking(dv.Item(C1).Item("FORMIMAGE").ToString, dv.Item(C1).Item("FORMADDRESS").ToString & "?awal=true", dv.Item(C1).Item("FORMMENU").ToString) & "</td>"
                If iRow = iTotalMenuPerRow Then
                    LABELPURCHASING.Text &= "</tr>"
                    iRow = 1
                Else
                    iRow += 1
                End If
            Next
            bVisible = True
        End If
        dv.RowFilter = ""
        iRow = 1
        ' === REPORT
        dv.RowFilter = "FORMMODULE='PURCHASING' AND FORMTYPE='REPORT'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            iRowSpan = Math.Ceiling(dv.Count / iTotalMenuPerRow)
            LABELPURCHASING.Text &= sFormatHeader & "Report :</span></td></tr>"
            LABELPURCHASING.Text &= "<tr><td align=" & Chr(34) & "center" & Chr(34) & " style=" & Chr(34) & "width: 50px;" & Chr(34) & " valign=" & Chr(34) & "middle" & Chr(34) & " rowspan=" & Chr(34) & iRowSpan.ToString & Chr(34) & "></td>"
            For C1 As Integer = 0 To dv.Count - 1
                If iRow = 1 And C1 <> 0 Then
                    LABELPURCHASING.Text &= "<tr>"
                End If
                LABELPURCHASING.Text &= sFormatMenu & SetLinking(dv.Item(C1).Item("FORMIMAGE").ToString, dv.Item(C1).Item("FORMADDRESS").ToString & "?awal=true", dv.Item(C1).Item("FORMMENU").ToString) & "</td>"
                If iRow = iTotalMenuPerRow Then
                    LABELPURCHASING.Text &= "</tr>"
                    iRow = 1
                Else
                    iRow += 1
                End If
            Next
            bVisible = True
        End If
        dv.RowFilter = ""
        iRow = 1
        LABELPURCHASING.Text &= "</table>"
        TabContainer1.Tabs(2).Enabled = bVisible
        bVisible = False
        ' === END PURCHASING MODULE ===

        ' === MARKETING MODULE ===
        LABELMARKETING.Text = "<table>"
        ' === MASTER
        dv.RowFilter = "FORMMODULE='MARKETING' AND FORMTYPE='MASTER'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            iRowSpan = Math.Ceiling(dv.Count / iTotalMenuPerRow)
            LABELMARKETING.Text &= sFormatHeader & "Master :</span></td></tr>"
            LABELMARKETING.Text &= "<tr><td align=" & Chr(34) & "center" & Chr(34) & " style=" & Chr(34) & "width: 50px;" & Chr(34) & " valign=" & Chr(34) & "middle" & Chr(34) & " rowspan=" & Chr(34) & iRowSpan.ToString & Chr(34) & "></td>"
            For C1 As Integer = 0 To dv.Count - 1
                If iRow = 1 And C1 <> 0 Then
                    LABELMARKETING.Text &= "<tr>"
                End If
                LABELMARKETING.Text &= sFormatMenu & SetLinking(dv.Item(C1).Item("FORMIMAGE").ToString, dv.Item(C1).Item("FORMADDRESS").ToString & "?awal=true", dv.Item(C1).Item("FORMMENU").ToString) & "</td>"
                If iRow = iTotalMenuPerRow Then
                    LABELMARKETING.Text &= "</tr>"
                    iRow = 1
                Else
                    iRow += 1
                End If
            Next
            bVisible = True
        End If
        dv.RowFilter = ""
        iRow = 1
        ' === TRANSACTION
        dv.RowFilter = "FORMMODULE='MARKETING' AND FORMTYPE='TRANSACTION'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            iRowSpan = Math.Ceiling(dv.Count / iTotalMenuPerRow)
            LABELMARKETING.Text &= sFormatHeader & "Transaction :</span></td></tr>"
            LABELMARKETING.Text &= "<tr><td align=" & Chr(34) & "center" & Chr(34) & " style=" & Chr(34) & "width: 50px;" & Chr(34) & " valign=" & Chr(34) & "middle" & Chr(34) & " rowspan=" & Chr(34) & iRowSpan.ToString & Chr(34) & "></td>"
            For C1 As Integer = 0 To dv.Count - 1
                If iRow = 1 And C1 <> 0 Then
                    LABELMARKETING.Text &= "<tr>"
                End If
                LABELMARKETING.Text &= sFormatMenu & SetLinking(dv.Item(C1).Item("FORMIMAGE").ToString, dv.Item(C1).Item("FORMADDRESS").ToString & "?awal=true", dv.Item(C1).Item("FORMMENU").ToString) & "</td>"
                If iRow = iTotalMenuPerRow Then
                    LABELMARKETING.Text &= "</tr>"
                    iRow = 1
                Else
                    iRow += 1
                End If
            Next
            bVisible = True
        End If
        dv.RowFilter = ""
        iRow = 1
        ' === REPORT
        dv.RowFilter = "FORMMODULE='MARKETING' AND FORMTYPE='REPORT'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            iRowSpan = Math.Ceiling(dv.Count / iTotalMenuPerRow)
            LABELMARKETING.Text &= sFormatHeader & "Report :</span></td></tr>"
            LABELMARKETING.Text &= "<tr><td align=" & Chr(34) & "center" & Chr(34) & " style=" & Chr(34) & "width: 50px;" & Chr(34) & " valign=" & Chr(34) & "middle" & Chr(34) & " rowspan=" & Chr(34) & iRowSpan.ToString & Chr(34) & "></td>"
            For C1 As Integer = 0 To dv.Count - 1
                If iRow = 1 And C1 <> 0 Then
                    LABELMARKETING.Text &= "<tr>"
                End If
                LABELMARKETING.Text &= sFormatMenu & SetLinking(dv.Item(C1).Item("FORMIMAGE").ToString, dv.Item(C1).Item("FORMADDRESS").ToString & "?awal=true", dv.Item(C1).Item("FORMMENU").ToString) & "</td>"
                If iRow = iTotalMenuPerRow Then
                    LABELMARKETING.Text &= "</tr>"
                    iRow = 1
                Else
                    iRow += 1
                End If
            Next
            bVisible = True
        End If
        dv.RowFilter = ""
        iRow = 1
        LABELMARKETING.Text &= "</table>"
        TabContainer1.Tabs(3).Enabled = bVisible
        bVisible = False
        ' === END MARKETING MODULE ===

        ' === INVENTORY MODULE ===
        LABELINVENTORY.Text = "<table>"
        ' === MASTER
        dv.RowFilter = "FORMMODULE='INVENTORY' AND FORMTYPE='MASTER'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            iRowSpan = Math.Ceiling(dv.Count / iTotalMenuPerRow)
            LABELINVENTORY.Text &= sFormatHeader & "Master :</span></td></tr>"
            LABELINVENTORY.Text &= "<tr><td align=" & Chr(34) & "center" & Chr(34) & " style=" & Chr(34) & "width: 50px;" & Chr(34) & " valign=" & Chr(34) & "middle" & Chr(34) & " rowspan=" & Chr(34) & iRowSpan.ToString & Chr(34) & "></td>"
            For C1 As Integer = 0 To dv.Count - 1
                If iRow = 1 And C1 <> 0 Then
                    LABELINVENTORY.Text &= "<tr>"
                End If
                LABELINVENTORY.Text &= sFormatMenu & SetLinking(dv.Item(C1).Item("FORMIMAGE").ToString, dv.Item(C1).Item("FORMADDRESS").ToString & "?awal=true", dv.Item(C1).Item("FORMMENU").ToString) & "</td>"
                If iRow = iTotalMenuPerRow Then
                    LABELINVENTORY.Text &= "</tr>"
                    iRow = 1
                Else
                    iRow += 1
                End If
            Next
            bVisible = True
        End If
        dv.RowFilter = ""
        iRow = 1
        ' === TRANSACTION
        dv.RowFilter = "FORMMODULE='INVENTORY' AND FORMTYPE='TRANSACTION'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            iRowSpan = Math.Ceiling(dv.Count / iTotalMenuPerRow)
            LABELINVENTORY.Text &= sFormatHeader & "Transaction :</span></td></tr>"
            LABELINVENTORY.Text &= "<tr><td align=" & Chr(34) & "center" & Chr(34) & " style=" & Chr(34) & "width: 50px;" & Chr(34) & " valign=" & Chr(34) & "middle" & Chr(34) & " rowspan=" & Chr(34) & iRowSpan.ToString & Chr(34) & "></td>"
            For C1 As Integer = 0 To dv.Count - 1
                If iRow = 1 And C1 <> 0 Then
                    LABELINVENTORY.Text &= "<tr>"
                End If
                LABELINVENTORY.Text &= sFormatMenu & SetLinking(dv.Item(C1).Item("FORMIMAGE").ToString, dv.Item(C1).Item("FORMADDRESS").ToString & "?awal=true", dv.Item(C1).Item("FORMMENU").ToString) & "</td>"
                If iRow = iTotalMenuPerRow Then
                    LABELINVENTORY.Text &= "</tr>"
                    iRow = 1
                Else
                    iRow += 1
                End If
            Next
            bVisible = True
        End If
        dv.RowFilter = ""
        iRow = 1
        ' === REPORT
        dv.RowFilter = "FORMMODULE='INVENTORY' AND FORMTYPE='REPORT'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            iRowSpan = Math.Ceiling(dv.Count / iTotalMenuPerRow)
            LABELINVENTORY.Text &= sFormatHeader & "Report :</span></td></tr>"
            LABELINVENTORY.Text &= "<tr><td align=" & Chr(34) & "center" & Chr(34) & " style=" & Chr(34) & "width: 50px;" & Chr(34) & " valign=" & Chr(34) & "middle" & Chr(34) & " rowspan=" & Chr(34) & iRowSpan.ToString & Chr(34) & "></td>"
            For C1 As Integer = 0 To dv.Count - 1
                If iRow = 1 And C1 <> 0 Then
                    LABELINVENTORY.Text &= "<tr>"
                End If
                LABELINVENTORY.Text &= sFormatMenu & SetLinking(dv.Item(C1).Item("FORMIMAGE").ToString, dv.Item(C1).Item("FORMADDRESS").ToString & "?awal=true", dv.Item(C1).Item("FORMMENU").ToString) & "</td>"
                If iRow = iTotalMenuPerRow Then
                    LABELINVENTORY.Text &= "</tr>"
                    iRow = 1
                Else
                    iRow += 1
                End If
            Next
            bVisible = True
        End If
        dv.RowFilter = ""
        iRow = 1
        LABELINVENTORY.Text &= "</table>"
        TabContainer1.Tabs(4).Enabled = bVisible
        bVisible = False
        ' === END INVENTORY MODULE ===

        ' === PRODUCTION MODULE ===
        LABELPRODUCTION.Text = "<table>"
        ' === MASTER
        dv.RowFilter = "FORMMODULE='PRODUCTION' AND FORMTYPE='MASTER'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            iRowSpan = Math.Ceiling(dv.Count / iTotalMenuPerRow)
            LABELPRODUCTION.Text &= sFormatHeader & "Master :</span></td></tr>"
            LABELPRODUCTION.Text &= "<tr><td align=" & Chr(34) & "center" & Chr(34) & " style=" & Chr(34) & "width: 50px;" & Chr(34) & " valign=" & Chr(34) & "middle" & Chr(34) & " rowspan=" & Chr(34) & iRowSpan.ToString & Chr(34) & "></td>"
            For C1 As Integer = 0 To dv.Count - 1
                If iRow = 1 And C1 <> 0 Then
                    LABELPRODUCTION.Text &= "<tr>"
                End If
                LABELPRODUCTION.Text &= sFormatMenu & SetLinking(dv.Item(C1).Item("FORMIMAGE").ToString, dv.Item(C1).Item("FORMADDRESS").ToString & "?awal=true", dv.Item(C1).Item("FORMMENU").ToString) & "</td>"
                If iRow = iTotalMenuPerRow Then
                    LABELPRODUCTION.Text &= "</tr>"
                    iRow = 1
                Else
                    iRow += 1
                End If
            Next
            bVisible = True
        End If
        dv.RowFilter = ""
        iRow = 1
        ' === TRANSACTION
        dv.RowFilter = "FORMMODULE='PRODUCTION' AND FORMTYPE='TRANSACTION'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            iRowSpan = Math.Ceiling(dv.Count / iTotalMenuPerRow)
            LABELPRODUCTION.Text &= sFormatHeader & "Transaction :</span></td></tr>"
            LABELPRODUCTION.Text &= "<tr><td align=" & Chr(34) & "center" & Chr(34) & " style=" & Chr(34) & "width: 50px;" & Chr(34) & " valign=" & Chr(34) & "middle" & Chr(34) & " rowspan=" & Chr(34) & iRowSpan.ToString & Chr(34) & "></td>"
            For C1 As Integer = 0 To dv.Count - 1
                If iRow = 1 And C1 <> 0 Then
                    LABELPRODUCTION.Text &= "<tr>"
                End If
                LABELPRODUCTION.Text &= sFormatMenu & SetLinking(dv.Item(C1).Item("FORMIMAGE").ToString, dv.Item(C1).Item("FORMADDRESS").ToString & "?awal=true", dv.Item(C1).Item("FORMMENU").ToString) & "</td>"
                If iRow = iTotalMenuPerRow Then
                    LABELPRODUCTION.Text &= "</tr>"
                    iRow = 1
                Else
                    iRow += 1
                End If
            Next
            bVisible = True
        End If
        dv.RowFilter = ""
        iRow = 1
        ' === REPORT
        dv.RowFilter = "FORMMODULE='PRODUCTION' AND FORMTYPE='REPORT'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            iRowSpan = Math.Ceiling(dv.Count / iTotalMenuPerRow)
            LABELPRODUCTION.Text &= sFormatHeader & "Report :</span></td></tr>"
            LABELPRODUCTION.Text &= "<tr><td align=" & Chr(34) & "center" & Chr(34) & " style=" & Chr(34) & "width: 50px;" & Chr(34) & " valign=" & Chr(34) & "middle" & Chr(34) & " rowspan=" & Chr(34) & iRowSpan.ToString & Chr(34) & "></td>"
            For C1 As Integer = 0 To dv.Count - 1
                If iRow = 1 And C1 <> 0 Then
                    LABELPRODUCTION.Text &= "<tr>"
                End If
                LABELPRODUCTION.Text &= sFormatMenu & SetLinking(dv.Item(C1).Item("FORMIMAGE").ToString, dv.Item(C1).Item("FORMADDRESS").ToString & "?awal=true", dv.Item(C1).Item("FORMMENU").ToString) & "</td>"
                If iRow = iTotalMenuPerRow Then
                    LABELPRODUCTION.Text &= "</tr>"
                    iRow = 1
                Else
                    iRow += 1
                End If
            Next
            bVisible = True
        End If
        dv.RowFilter = ""
        iRow = 1
        LABELPRODUCTION.Text &= "</table>"
        TabContainer1.Tabs(5).Enabled = bVisible
        bVisible = False
        ' === END PRODUCTION MODULE ===

        ' === ACCOUNTING MODULE ===
        LABELACCOUNTING.Text = "<table>"
        ' === MASTER
        dv.RowFilter = "FORMMODULE='ACCOUNTING' AND FORMTYPE='MASTER'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            iRowSpan = Math.Ceiling(dv.Count / iTotalMenuPerRow)
            LABELACCOUNTING.Text &= sFormatHeader & "Master :</span></td></tr>"
            LABELACCOUNTING.Text &= "<tr><td align=" & Chr(34) & "center" & Chr(34) & " style=" & Chr(34) & "width: 50px;" & Chr(34) & " valign=" & Chr(34) & "middle" & Chr(34) & " rowspan=" & Chr(34) & iRowSpan.ToString & Chr(34) & "></td>"
            For C1 As Integer = 0 To dv.Count - 1
                If iRow = 1 And C1 <> 0 Then
                    LABELACCOUNTING.Text &= "<tr>"
                End If
                LABELACCOUNTING.Text &= sFormatMenu & SetLinking(dv.Item(C1).Item("FORMIMAGE").ToString, dv.Item(C1).Item("FORMADDRESS").ToString & "?awal=true", dv.Item(C1).Item("FORMMENU").ToString) & "</td>"
                If iRow = iTotalMenuPerRow Then
                    LABELACCOUNTING.Text &= "</tr>"
                    iRow = 1
                Else
                    iRow += 1
                End If
            Next
            bVisible = True
        End If
        dv.RowFilter = ""
        iRow = 1
        ' === TRANSACTION
        dv.RowFilter = "FORMMODULE='ACCOUNTING' AND FORMTYPE='TRANSACTION'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            iRowSpan = Math.Ceiling(dv.Count / iTotalMenuPerRow)
            LABELACCOUNTING.Text &= sFormatHeader & "Transaction :</span></td></tr>"
            LABELACCOUNTING.Text &= "<tr><td align=" & Chr(34) & "center" & Chr(34) & " style=" & Chr(34) & "width: 50px;" & Chr(34) & " valign=" & Chr(34) & "middle" & Chr(34) & " rowspan=" & Chr(34) & iRowSpan.ToString & Chr(34) & "></td>"
            For C1 As Integer = 0 To dv.Count - 1
                If iRow = 1 And C1 <> 0 Then
                    LABELACCOUNTING.Text &= "<tr>"
                End If
                LABELACCOUNTING.Text &= sFormatMenu & SetLinking(dv.Item(C1).Item("FORMIMAGE").ToString, dv.Item(C1).Item("FORMADDRESS").ToString & "?awal=true", dv.Item(C1).Item("FORMMENU").ToString) & "</td>"
                If iRow = iTotalMenuPerRow Then
                    LABELACCOUNTING.Text &= "</tr>"
                    iRow = 1
                Else
                    iRow += 1
                End If
            Next
            bVisible = True
        End If
        dv.RowFilter = ""
        iRow = 1
        ' === REPORT
        dv.RowFilter = "FORMMODULE='ACCOUNTING' AND FORMTYPE='REPORT'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            iRowSpan = Math.Ceiling(dv.Count / iTotalMenuPerRow)
            LABELACCOUNTING.Text &= sFormatHeader & "Report :</span></td></tr>"
            LABELACCOUNTING.Text &= "<tr><td align=" & Chr(34) & "center" & Chr(34) & " style=" & Chr(34) & "width: 50px;" & Chr(34) & " valign=" & Chr(34) & "middle" & Chr(34) & " rowspan=" & Chr(34) & iRowSpan.ToString & Chr(34) & "></td>"
            For C1 As Integer = 0 To dv.Count - 1
                If iRow = 1 And C1 <> 0 Then
                    LABELACCOUNTING.Text &= "<tr>"
                End If
                LABELACCOUNTING.Text &= sFormatMenu & SetLinking(dv.Item(C1).Item("FORMIMAGE").ToString, dv.Item(C1).Item("FORMADDRESS").ToString & "?awal=true", dv.Item(C1).Item("FORMMENU").ToString) & "</td>"
                If iRow = iTotalMenuPerRow Then
                    LABELACCOUNTING.Text &= "</tr>"
                    iRow = 1
                Else
                    iRow += 1
                End If
            Next
            bVisible = True
        End If
        dv.RowFilter = ""
        iRow = 1
        LABELACCOUNTING.Text &= "</table>"
        TabContainer1.Tabs(6).Enabled = bVisible
        bVisible = False
        ' === END ACCOUNTING MODULE ===

        ' === WIP LOG MODULE ===
        LABELWIPLOG.Text = "<table>"
        ' === MASTER
        dv.RowFilter = "FORMMODULE='WIP LOG' AND FORMTYPE='MASTER'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            iRowSpan = Math.Ceiling(dv.Count / iTotalMenuPerRow)
            LABELWIPLOG.Text &= sFormatHeader & "Master :</span></td></tr>"
            LABELWIPLOG.Text &= "<tr><td align=" & Chr(34) & "center" & Chr(34) & " style=" & Chr(34) & "width: 50px;" & Chr(34) & " valign=" & Chr(34) & "middle" & Chr(34) & " rowspan=" & Chr(34) & iRowSpan.ToString & Chr(34) & "></td>"
            For C1 As Integer = 0 To dv.Count - 1
                If iRow = 1 And C1 <> 0 Then
                    LABELWIPLOG.Text &= "<tr>"
                End If
                LABELWIPLOG.Text &= sFormatMenu & SetLinking(dv.Item(C1).Item("FORMIMAGE").ToString, dv.Item(C1).Item("FORMADDRESS").ToString & "?awal=true", dv.Item(C1).Item("FORMMENU").ToString) & "</td>"
                If iRow = iTotalMenuPerRow Then
                    LABELWIPLOG.Text &= "</tr>"
                    iRow = 1
                Else
                    iRow += 1
                End If
            Next
            bVisible = True
        End If
        dv.RowFilter = ""
        iRow = 1
        ' === TRANSACTION
        dv.RowFilter = "FORMMODULE='WIP LOG' AND FORMTYPE='TRANSACTION'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            iRowSpan = Math.Ceiling(dv.Count / iTotalMenuPerRow)
            LABELWIPLOG.Text &= sFormatHeader & "Transaction :</span></td></tr>"
            LABELWIPLOG.Text &= "<tr><td align=" & Chr(34) & "center" & Chr(34) & " style=" & Chr(34) & "width: 50px;" & Chr(34) & " valign=" & Chr(34) & "middle" & Chr(34) & " rowspan=" & Chr(34) & iRowSpan.ToString & Chr(34) & "></td>"
            For C1 As Integer = 0 To dv.Count - 1
                If iRow = 1 And C1 <> 0 Then
                    LABELWIPLOG.Text &= "<tr>"
                End If
                LABELWIPLOG.Text &= sFormatMenu & SetLinking(dv.Item(C1).Item("FORMIMAGE").ToString, dv.Item(C1).Item("FORMADDRESS").ToString & "?awal=true", dv.Item(C1).Item("FORMMENU").ToString) & "</td>"
                If iRow = iTotalMenuPerRow Then
                    LABELWIPLOG.Text &= "</tr>"
                    iRow = 1
                Else
                    iRow += 1
                End If
            Next
            bVisible = True
        End If
        dv.RowFilter = ""
        iRow = 1
        ' === REPORT
        dv.RowFilter = "FORMMODULE='WIP LOG' AND FORMTYPE='REPORT'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            iRowSpan = Math.Ceiling(dv.Count / iTotalMenuPerRow)
            LABELWIPLOG.Text &= sFormatHeader & "Report :</span></td></tr>"
            LABELWIPLOG.Text &= "<tr><td align=" & Chr(34) & "center" & Chr(34) & " style=" & Chr(34) & "width: 50px;" & Chr(34) & " valign=" & Chr(34) & "middle" & Chr(34) & " rowspan=" & Chr(34) & iRowSpan.ToString & Chr(34) & "></td>"
            For C1 As Integer = 0 To dv.Count - 1
                If iRow = 1 And C1 <> 0 Then
                    LABELWIPLOG.Text &= "<tr>"
                End If
                LABELWIPLOG.Text &= sFormatMenu & SetLinking(dv.Item(C1).Item("FORMIMAGE").ToString, dv.Item(C1).Item("FORMADDRESS").ToString & "?awal=true", dv.Item(C1).Item("FORMMENU").ToString) & "</td>"
                If iRow = iTotalMenuPerRow Then
                    LABELWIPLOG.Text &= "</tr>"
                    iRow = 1
                Else
                    iRow += 1
                End If
            Next
            bVisible = True
        End If
        dv.RowFilter = ""
        iRow = 1
        LABELWIPLOG.Text &= "</table>"
        TabContainer1.Tabs(7).Enabled = bVisible
        bVisible = False
        ' === END WIP LOG MODULE ===

        ' === FIXED ASSETS MODULE ===
        LABELFIXEDASSETS.Text = "<table>"
        ' === MASTER
        dv.RowFilter = "FORMMODULE='FIXED ASSETS' AND FORMTYPE='MASTER'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            iRowSpan = Math.Ceiling(dv.Count / iTotalMenuPerRow)
            LABELFIXEDASSETS.Text &= sFormatHeader & "Master :</span></td></tr>"
            LABELFIXEDASSETS.Text &= "<tr><td align=" & Chr(34) & "center" & Chr(34) & " style=" & Chr(34) & "width: 50px;" & Chr(34) & " valign=" & Chr(34) & "middle" & Chr(34) & " rowspan=" & Chr(34) & iRowSpan.ToString & Chr(34) & "></td>"
            For C1 As Integer = 0 To dv.Count - 1
                If iRow = 1 And C1 <> 0 Then
                    LABELFIXEDASSETS.Text &= "<tr>"
                End If
                LABELFIXEDASSETS.Text &= sFormatMenu & SetLinking(dv.Item(C1).Item("FORMIMAGE").ToString, dv.Item(C1).Item("FORMADDRESS").ToString & "?awal=true", dv.Item(C1).Item("FORMMENU").ToString) & "</td>"
                If iRow = iTotalMenuPerRow Then
                    LABELFIXEDASSETS.Text &= "</tr>"
                    iRow = 1
                Else
                    iRow += 1
                End If
            Next
            bVisible = True
        End If
        dv.RowFilter = ""
        iRow = 1
        ' === TRANSACTION
        dv.RowFilter = "FORMMODULE='FIXED ASSETS' AND FORMTYPE='TRANSACTION'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            iRowSpan = Math.Ceiling(dv.Count / iTotalMenuPerRow)
            LABELFIXEDASSETS.Text &= sFormatHeader & "Transaction :</span></td></tr>"
            LABELFIXEDASSETS.Text &= "<tr><td align=" & Chr(34) & "center" & Chr(34) & " style=" & Chr(34) & "width: 50px;" & Chr(34) & " valign=" & Chr(34) & "middle" & Chr(34) & " rowspan=" & Chr(34) & iRowSpan.ToString & Chr(34) & "></td>"
            For C1 As Integer = 0 To dv.Count - 1
                If iRow = 1 And C1 <> 0 Then
                    LABELFIXEDASSETS.Text &= "<tr>"
                End If
                LABELFIXEDASSETS.Text &= sFormatMenu & SetLinking(dv.Item(C1).Item("FORMIMAGE").ToString, dv.Item(C1).Item("FORMADDRESS").ToString & "?awal=true", dv.Item(C1).Item("FORMMENU").ToString) & "</td>"
                If iRow = iTotalMenuPerRow Then
                    LABELFIXEDASSETS.Text &= "</tr>"
                    iRow = 1
                Else
                    iRow += 1
                End If
            Next
            bVisible = True
        End If
        dv.RowFilter = ""
        iRow = 1
        ' === REPORT
        dv.RowFilter = "FORMMODULE='FIXED ASSETS' AND FORMTYPE='REPORT'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            iRowSpan = Math.Ceiling(dv.Count / iTotalMenuPerRow)
            LABELFIXEDASSETS.Text &= sFormatHeader & "Report :</span></td></tr>"
            LABELFIXEDASSETS.Text &= "<tr><td align=" & Chr(34) & "center" & Chr(34) & " style=" & Chr(34) & "width: 50px;" & Chr(34) & " valign=" & Chr(34) & "middle" & Chr(34) & " rowspan=" & Chr(34) & iRowSpan.ToString & Chr(34) & "></td>"
            For C1 As Integer = 0 To dv.Count - 1
                If iRow = 1 And C1 <> 0 Then
                    LABELFIXEDASSETS.Text &= "<tr>"
                End If
                LABELFIXEDASSETS.Text &= sFormatMenu & SetLinking(dv.Item(C1).Item("FORMIMAGE").ToString, dv.Item(C1).Item("FORMADDRESS").ToString & "?awal=true", dv.Item(C1).Item("FORMMENU").ToString) & "</td>"
                If iRow = iTotalMenuPerRow Then
                    LABELFIXEDASSETS.Text &= "</tr>"
                    iRow = 1
                Else
                    iRow += 1
                End If
            Next
            bVisible = True
        End If
        dv.RowFilter = ""
        iRow = 1
        LABELFIXEDASSETS.Text &= "</table>"
        TabContainer1.Tabs(8).Enabled = bVisible
        bVisible = False
        ' === END FIXED ASSETS MODULE ===

        dv.RowFilter = ""
    End Sub

    Private Sub SetActiveTab()
        For C1 As Integer = 0 To TabContainer1.Tabs.Count - 1
            If TabContainer1.Tabs(C1).Enabled Then
                TabContainer1.ActiveTabIndex = C1
                Exit Sub
            End If
        Next
    End Sub

    Private Sub PostNotification(ByVal sOid As String)
        ' Posted Expired Notification
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_notification SET activeflag='Posted', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & CompnyCode & "' AND notifyoid=" & sOid
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
        End Try
        conn.Close()
        Response.Redirect("~\Other\menu.aspx?awal=true")
    End Sub

    Private Sub InitNotifyAdmin()
        sSql = "SELECT notifyoid, createuser, notifymessage FROM QL_notification WHERE cmpcode='" & CompnyCode & "' AND notifyuser LIKE '%" & Session("UserID") & "%' AND activeflag='In Process'"
        Dim dtNote As DataTable = ckon.ambiltabel(sSql, "QL_notification")
        If dtNote.Rows.Count > 0 Then
            LabelHeaderNotifyAdmin.Text = "You have " & dtNote.Rows.Count & " message from another user :"
            Dim arColNameList() As String = {"noteseq", "noteuser", "notemessage", "noteoid"}
            Dim arColTypeList() As String = {"System.String", "System.String", "System.String", "System.Int32"}
            Dim dtList As DataTable = SetTableDetail("tblAdminList", arColNameList, arColTypeList)
            Dim drList As DataRow
            For C1 As Integer = 0 To dtNote.Rows.Count - 1
                drList = dtList.NewRow : drList("noteseq") = (C1 + 1).ToString & "." : drList("noteuser") = dtNote.Rows(C1)("createuser").ToString : drList("notemessage") = dtNote.Rows(C1)("notifymessage").ToString : drList("noteoid") = dtNote.Rows(C1)("notifyoid") : dtList.Rows.Add(drList)
            Next
            gvListAdmin.DataSource = dtList
            gvListAdmin.DataBind()
        End If
    End Sub

    Private Sub InitNotifyApproval()
        sSql = "SELECT COUNT(*) AS jmlapp, tablename FROM QL_approval WHERE approvaluser='" & Session("UserID") & "' AND statusrequest='New' AND event='In Approval' GROUP BY tablename"
        Dim dtApp As DataTable = ckon.ambiltabel(sSql, "QL_approval")
        Dim dvApp As DataView = dtApp.DefaultView
        If dvApp.Count > 0 Then
            Dim arColNameList() As String = {"apptype", "appsept", "appcount", "appformvalue", "appformtext"}
            Dim arColTypeList() As String = {"System.String", "System.String", "System.Int32", "System.String", "System.String"}
            Dim dtList As DataTable = SetTableDetail("tblAppList", arColNameList, arColTypeList)
            Dim drList As DataRow
            ' PR Raw Material
            dvApp.RowFilter = "tablename = 'QL_prrawmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PR Raw Material" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_PRRAWMST" : drList("appformtext") = "� PR Raw Material" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' PR General Material
            dvApp.RowFilter = "tablename = 'QL_prgenmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PR General Material" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_PRGENMST" : drList("appformtext") = "� PR General Material" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' PR Spare Part
            dvApp.RowFilter = "tablename = 'QL_prspmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PR Spare Part" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_PRSPMST" : drList("appformtext") = "� PR Spare Part" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' PR Finish Good
            dvApp.RowFilter = "tablename = 'QL_pritemmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PR Finish Good" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_PRITEMMST" : drList("appformtext") = "� PR Finish Good" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' PR Fixed Assets
            dvApp.RowFilter = "tablename = 'QL_prassetmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PR Fixed Assets" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_PRASSETMST" : drList("appformtext") = "� PR Fixed Assets" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' PR Log
            dvApp.RowFilter = "tablename = 'QL_prwipmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PR Log" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_PRWIPMST" : drList("appformtext") = "� PR Log" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' PR Sawn Timber
            dvApp.RowFilter = "tablename = 'QL_prsawnmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PR Sawn Timber" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_PRSAWNMST" : drList("appformtext") = "� PR Sawn Timber" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' PO Raw Material
            dvApp.RowFilter = "tablename = 'QL_trnporawmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PO Raw Material" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNPORAWMST" : drList("appformtext") = "� PO Raw Material" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' PO General Material
            dvApp.RowFilter = "tablename = 'QL_trnpogenmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PO General Material" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNPOGENMST" : drList("appformtext") = "� PO General Material" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' PO Spare Part
            dvApp.RowFilter = "tablename = 'QL_trnpospmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PO Spare Part" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNPOSPMST" : drList("appformtext") = "� PO Spare Part" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' PO Finish Good
            dvApp.RowFilter = "tablename = 'QL_trnpofgmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PO Finish Good" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNPOFGMST" : drList("appformtext") = "� PO Finish Good" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' PO Fixed Assets
            dvApp.RowFilter = "tablename = 'QL_trnpoassetmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PO Fixed Assets" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNPOASSETMST" : drList("appformtext") = "� PO Fixed Assets" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' PO Log
            dvApp.RowFilter = "tablename = 'QL_trnpowipmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PO Work In Process (WIP)" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNPOWIPMST" : drList("appformtext") = "� PO Work In Process (WIP)" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' PO Sawn Timber
            dvApp.RowFilter = "tablename = 'QL_trnposawnmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PO Sawn Timber" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNPOSAWNMST" : drList("appformtext") = "� PO Sawn Timber" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' PO Service
            dvApp.RowFilter = "tablename = 'QL_trnposervicemst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PO Service" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNPOSERVICEMST" : drList("appformtext") = "� PO Service" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' PO Subcon
            dvApp.RowFilter = "tablename = 'QL_trnposubconmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PO Subcon" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNPOSUBCONMST" : drList("appformtext") = "� PO Subcon" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' Purchase Return Raw Material
            dvApp.RowFilter = "tablename = 'QL_trnreturmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Purchase Return" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNRETURMST" : drList("appformtext") = "� Purchase Return" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' Purchase Return General Material
            dvApp.RowFilter = "tablename = 'QL_trnpretgenmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Purchase Return General Material" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNPRETGENMST" : drList("appformtext") = "� Purchase Return General Material" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' Purchase Return Spare Part
            dvApp.RowFilter = "tablename = 'QL_trnpretspmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Purchase Return Spare Part" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNPRETSPMST" : drList("appformtext") = "� Purchase Return Spare Part" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' Purchase Return Finish Good
            dvApp.RowFilter = "tablename = 'QL_trnpretitemmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Purchase Return Finish Good" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNPRETITEMMST" : drList("appformtext") = "� Purchase Return Finish Good" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' Purchase Return Fixed Assets
            dvApp.RowFilter = "tablename = 'QL_trnpretassetmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Purchase Return Fixed Assets" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNPRETASSETMST" : drList("appformtext") = "� Purchase Return Fixed Assets" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' Purchase Return Log
            dvApp.RowFilter = "tablename = 'QL_trnpretwipmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Purchase Return Log" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNPRETWIPMST" : drList("appformtext") = "� Purchase Return Log" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' Purchase Return Sawn Timber
            dvApp.RowFilter = "tablename = 'QL_trnpretsawnmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Purchase Return Sawn Timber" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNPRETSAWNMST" : drList("appformtext") = "� Purchase Return Sawn Timber" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' Purchase Return Subcon
            dvApp.RowFilter = "tablename = 'QL_trnpretsubconmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Purchase Return Subcon" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNPRETSUBCONMST" : drList("appformtext") = "� Purchase Return Subcon" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' Purchase Invoice Raw Material
            dvApp.RowFilter = "tablename = 'QL_trnaprawmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Purchase Invoice Raw Material" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNAPRAWMST" : drList("appformtext") = "� Purchase Invoice Raw Material" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' Purchase Invoice General Material
            dvApp.RowFilter = "tablename = 'QL_trnapgenmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Purchase Invoice General Material" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNAPGENMST" : drList("appformtext") = "� Purchase Invoice General Material" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' Purchase Invoice Finish Good
            dvApp.RowFilter = "tablename = 'QL_trnapfgmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Purchase Invoice Finish Good" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNAPFGMST" : drList("appformtext") = "� Purchase Invoice Finish Good" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' Purchase Invoice WIP
            dvApp.RowFilter = "tablename = 'QL_trnapwipmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Purchase Invoice WIP" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNAPWIPMST" : drList("appformtext") = "� Purchase Invoice WIP" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' SO Raw Material
            dvApp.RowFilter = "tablename = 'QL_trnsorawmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- SO Raw Material" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNSORAWMST" : drList("appformtext") = "� SO Raw Material" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' SO General Material
            dvApp.RowFilter = "tablename = 'QL_trnsogenmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- SO General Material" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNSOGENMST" : drList("appformtext") = "� SO General Material" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' SO Spare Part
            dvApp.RowFilter = "tablename = 'QL_trnsospmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- SO Spare Part" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNSOSPMST" : drList("appformtext") = "� SO Spare Part" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' SO Finish Good
            dvApp.RowFilter = "tablename = 'QL_trnsomst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- SALES ORDER" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNSOMST" : drList("appformtext") = "� SALES ORDER" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' SO Fixed Assets
            dvApp.RowFilter = "tablename = 'QL_trnsoassetmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- SO Fixed Assets" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNSOASSETMST" : drList("appformtext") = "� SO Fixed Assets" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' Shipment Raw Material
            dvApp.RowFilter = "tablename = 'QL_trndomst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- DELIVERY ORDER" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNDOMST" : drList("appformtext") = "� DELIVERY ORDER" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' Shipment General Material
            dvApp.RowFilter = "tablename = 'QL_trnshipmentgenmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Shipment General Material" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNSHIPMENTGENMST" : drList("appformtext") = "� Shipment General Material" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' Shipment Spare Part
            dvApp.RowFilter = "tablename = 'QL_trnshipmentspmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Shipment Spare Part" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNSHIPMENTSPMST" : drList("appformtext") = "� Shipment Spare Part" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' Shipment Finish Good
            dvApp.RowFilter = "tablename = 'QL_trnshipmentitemmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Shipment Finish Good" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNSHIPMENTITEMMST" : drList("appformtext") = "� Shipment Finish Good" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' Sales Return Raw Material
            dvApp.RowFilter = "tablename = 'QL_trnsalesreturmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Sales Return" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNSALESRETURMST" : drList("appformtext") = "� Sales Return" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' Sales Return General Material
            dvApp.RowFilter = "tablename = 'QL_trnsretgenmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Sales Return General Material" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNSRETGENMST" : drList("appformtext") = "� Sales Return General Material" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' Sales Return Spare Part
            dvApp.RowFilter = "tablename = 'QL_trnsretspmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Sales Return Spare Part" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNSRETSPMST" : drList("appformtext") = "� Sales Return Spare Part" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' Sales Return Finish Good
            dvApp.RowFilter = "tablename = 'QL_trnsretitemmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Sales Return Finish Good" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNSRETITEMMST" : drList("appformtext") = "� Sales Return Finish Good" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' Sales Return Fixed Assets
            dvApp.RowFilter = "tablename = 'QL_trnsretassetmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Sales Return Fixed Assets" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNSRETASSETMST" : drList("appformtext") = "� Sales Return Fixed Assets" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' A/R Raw Material
            dvApp.RowFilter = "tablename = 'QL_trnarrawmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- A/R Raw Material" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNARRAWMST" : drList("appformtext") = "� A/R Raw Material" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' A/R General Material
            dvApp.RowFilter = "tablename = 'QL_trnargenmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- A/R General Material" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNARGENMST" : drList("appformtext") = "� A/R General Material" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' A/R Spare Part
            dvApp.RowFilter = "tablename = 'QL_trnarspmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- A/R Spare Part" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNARSPMST" : drList("appformtext") = "� A/R Spare Part" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' A/R Finish Good
            dvApp.RowFilter = "tablename = 'QL_trnaritemmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- A/R Finish Good" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNARITEMMST" : drList("appformtext") = "� A/R Finish Good" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' A/R Fixed Assets
            dvApp.RowFilter = "tablename = 'QL_trnarassetmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- A/R Fixed Assets" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNARASSETMST" : drList("appformtext") = "� A/R Fixed Assets" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' Berita Acara
            dvApp.RowFilter = "tablename = 'QL_trnbrtacaramst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Berita Acara" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNBRTACARAMST" : drList("appformtext") = "� Berita Acara" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' Berita Acara Product
            dvApp.RowFilter = "tablename = 'QL_trnbrtacaraprodmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Berita Acara Product" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNBRTACARAPRODMST" : drList("appformtext") = "� Berita Acara Product" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""
            ' KIK Planning Date
            dvApp.RowFilter = "tablename = 'QL_trnplanmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- KIK Planning Date" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNPLANMST" : drList("appformtext") = "� KIK Planning Date" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""

            ' Adjustment
            dvApp.RowFilter = "tablename = 'QL_trnstockadj' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Stock Adjustment" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_TRNSTOCKADJ" : drList("appformtext") = "� Stock Adjustment" : dtList.Rows.Add(drList)
            End If
            dvApp.RowFilter = ""

            gvListApp.DataSource = dtList
            gvListApp.DataBind()
        End If
    End Sub

    Private Sub ClosedExpiredPR()
        ' Closed Expired PR
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_prrawdtl SET prrawdtlstatus='Complete', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE prrawdtlstatus='' AND prrawmstoid IN (SELECT prrawmstoid FROM QL_prrawmst WHERE prrawmststatus='Approved' AND DATEDIFF(DAY, CURRENT_TIMESTAMP, prrawexpdate) < 0"
            If Session("CompnyCode") <> CompnyCode Then
                sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
            End If
            sSql &= ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_prrawmst SET prrawmststatus='Closed', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE prrawmststatus='Approved' AND DATEDIFF(DAY, CURRENT_TIMESTAMP, prrawexpdate) < 0"
            If Session("CompnyCode") <> CompnyCode Then
                sSql &= " AND pr.cmpcode='" & Session("CompnyCode") & "'"
            End If
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
        End Try
        conn.Close()
    End Sub

    Private Sub InitNotifyExpiredPR()
        Dim objDV As DataView = Session("Role").DefaultView
        Dim arColNameList() As String = {"prbusunit", "prdept", "prname", "prcount", "prcmpcode", "prdeptoid", "prtype"}
        Dim arColTypeList() As String = {"System.String", "System.String", "System.String", "System.Int32", "System.String", "System.Int32", "System.String"}
        Dim dtList As DataTable = SetTableDetail("tblExpPRList", arColNameList, arColTypeList)
        Dim drList As DataRow
        Dim dtExpPR As DataTable
        ' PR Raw Material
        objDV.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME='PR RAW MATERIAL'"
        If objDV.Count > 0 Then
            sSql = "SELECT pr.cmpcode, divname, deptname, COUNT(prrawmstoid) AS prcount, pr.deptoid FROM QL_prrawmst pr INNER JOIN QL_mstdivision di ON di.cmpcode=pr.cmpcode INNER JOIN QL_mstdept de ON de.cmpcode=pr.cmpcode AND de.deptoid=pr.deptoid WHERE pr.prrawmststatus='Approved' AND DATEDIFF(DAY, CURRENT_TIMESTAMP, pr.prrawexpdate) < 7"
            If Session("CompnyCode") <> CompnyCode Then
                sSql &= " AND pr.cmpcode='" & Session("CompnyCode") & "'"
            End If
            sSql &= " GROUP BY pr.cmpcode, divname, deptname, pr.deptoid"
            dtExpPR = New DataTable
            dtExpPR = ckon.ambiltabel(sSql, "QL_prrawmst")
            If dtExpPR.Rows.Count > 0 Then
                For C1 As Integer = 0 To dtExpPR.Rows.Count - 1
                    drList = dtList.NewRow : drList("prbusunit") = dtExpPR.Rows(C1)("divname").ToString : drList("prdept") = dtExpPR.Rows(C1)("deptname").ToString : drList("prname") = "PR Raw Material" : drList("prcount") = CInt(dtExpPR.Rows(C1)("prcount").ToString) : drList("prcmpcode") = dtExpPR.Rows(C1)("cmpcode").ToString : drList("prdeptoid") = CInt(dtExpPR.Rows(C1)("deptoid").ToString) : drList("prtype") = "raw" : dtList.Rows.Add(drList)
                Next
            End If
        End If
        objDV.RowFilter = ""
        ' PR General Material
        objDV.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME='PR GENERAL MATERIAL'"
        If objDV.Count > 0 Then
            sSql = "SELECT pr.cmpcode, divname, deptname, COUNT(prgenmstoid) AS prcount, pr.deptoid FROM QL_prgenmst pr INNER JOIN QL_mstdivision di ON di.cmpcode=pr.cmpcode INNER JOIN QL_mstdept de ON de.cmpcode=pr.cmpcode AND de.deptoid=pr.deptoid WHERE pr.prgenmststatus='Approved' AND DATEDIFF(DAY, CURRENT_TIMESTAMP, pr.prgenexpdate) < 7"
            If Session("CompnyCode") <> CompnyCode Then
                sSql &= " AND pr.cmpcode='" & Session("CompnyCode") & "'"
            End If
            sSql &= " GROUP BY pr.cmpcode, divname, deptname, pr.deptoid"
            dtExpPR = New DataTable
            dtExpPR = ckon.ambiltabel(sSql, "QL_prgenmst")
            If dtExpPR.Rows.Count > 0 Then
                For C1 As Integer = 0 To dtExpPR.Rows.Count - 1
                    drList = dtList.NewRow : drList("prbusunit") = dtExpPR.Rows(C1)("divname").ToString : drList("prdept") = dtExpPR.Rows(C1)("deptname").ToString : drList("prname") = "PR General Material" : drList("prcount") = CInt(dtExpPR.Rows(C1)("prcount").ToString) : drList("prcmpcode") = dtExpPR.Rows(C1)("cmpcode").ToString : drList("prdeptoid") = CInt(dtExpPR.Rows(C1)("deptoid").ToString) : drList("prtype") = "gen" : dtList.Rows.Add(drList)
                Next
            End If
        End If
        objDV.RowFilter = ""
        ' PR Spare Part
        objDV.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME='PR SPARE PART'"
        If objDV.Count > 0 Then
            sSql = "SELECT pr.cmpcode, divname, deptname, COUNT(prspmstoid) AS prcount, pr.deptoid FROM QL_prspmst pr INNER JOIN QL_mstdivision di ON di.cmpcode=pr.cmpcode INNER JOIN QL_mstdept de ON de.cmpcode=pr.cmpcode AND de.deptoid=pr.deptoid WHERE pr.prspmststatus='Approved' AND DATEDIFF(DAY, CURRENT_TIMESTAMP, pr.prspexpdate) < 7"
            If Session("CompnyCode") <> CompnyCode Then
                sSql &= " AND pr.cmpcode='" & Session("CompnyCode") & "'"
            End If
            sSql &= " GROUP BY pr.cmpcode, divname, deptname, pr.deptoid"
            dtExpPR = New DataTable
            dtExpPR = ckon.ambiltabel(sSql, "QL_prspmst")
            If dtExpPR.Rows.Count > 0 Then
                For C1 As Integer = 0 To dtExpPR.Rows.Count - 1
                    drList = dtList.NewRow : drList("prbusunit") = dtExpPR.Rows(C1)("divname").ToString : drList("prdept") = dtExpPR.Rows(C1)("deptname").ToString : drList("prname") = "PR Spare Part" : drList("prcount") = CInt(dtExpPR.Rows(C1)("prcount").ToString) : drList("prcmpcode") = dtExpPR.Rows(C1)("cmpcode").ToString : drList("prdeptoid") = CInt(dtExpPR.Rows(C1)("deptoid").ToString) : drList("prtype") = "sp" : dtList.Rows.Add(drList)
                Next
            End If
        End If
        objDV.RowFilter = ""
        ' PR Finish Good
        objDV.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME='PR FINISH GOOD'"
        If objDV.Count > 0 Then
            sSql = "SELECT pr.cmpcode, divname, deptname, COUNT(pritemmstoid) AS prcount, pr.deptoid FROM QL_pritemmst pr INNER JOIN QL_mstdivision di ON di.cmpcode=pr.cmpcode INNER JOIN QL_mstdept de ON de.cmpcode=pr.cmpcode AND de.deptoid=pr.deptoid WHERE pr.pritemmststatus='Approved' AND DATEDIFF(DAY, CURRENT_TIMESTAMP, pr.pritemexpdate) < 7"
            If Session("CompnyCode") <> CompnyCode Then
                sSql &= " AND pr.cmpcode='" & Session("CompnyCode") & "'"
            End If
            sSql &= " GROUP BY pr.cmpcode, divname, deptname, pr.deptoid"
            dtExpPR = New DataTable
            dtExpPR = ckon.ambiltabel(sSql, "QL_pritemmst")
            If dtExpPR.Rows.Count > 0 Then
                For C1 As Integer = 0 To dtExpPR.Rows.Count - 1
                    drList = dtList.NewRow : drList("prbusunit") = dtExpPR.Rows(C1)("divname").ToString : drList("prdept") = dtExpPR.Rows(C1)("deptname").ToString : drList("prname") = "PR Finish Good" : drList("prcount") = CInt(dtExpPR.Rows(C1)("prcount").ToString) : drList("prcmpcode") = dtExpPR.Rows(C1)("cmpcode").ToString : drList("prdeptoid") = CInt(dtExpPR.Rows(C1)("deptoid").ToString) : drList("prtype") = "item" : dtList.Rows.Add(drList)
                Next
            End If
        End If
        objDV.RowFilter = ""
        ' PR WIP Log
        objDV.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME='PR WIP LOG'"
        If objDV.Count > 0 Then
            sSql = "SELECT pr.cmpcode, divname, deptname, COUNT(prwipmstoid) AS prcount, pr.deptoid FROM QL_prwipmst pr INNER JOIN QL_mstdivision di ON di.cmpcode=pr.cmpcode INNER JOIN QL_mstdept de ON de.cmpcode=pr.cmpcode AND de.deptoid=pr.deptoid WHERE pr.prwipmststatus='Approved' AND DATEDIFF(DAY, CURRENT_TIMESTAMP, pr.prwipexpdate) < 7"
            If Session("CompnyCode") <> CompnyCode Then
                sSql &= " AND pr.cmpcode='" & Session("CompnyCode") & "'"
            End If
            sSql &= " GROUP BY pr.cmpcode, divname, deptname, pr.deptoid"
            dtExpPR = New DataTable
            dtExpPR = ckon.ambiltabel(sSql, "QL_prwipmst")
            If dtExpPR.Rows.Count > 0 Then
                For C1 As Integer = 0 To dtExpPR.Rows.Count - 1
                    drList = dtList.NewRow : drList("prbusunit") = dtExpPR.Rows(C1)("divname").ToString : drList("prdept") = dtExpPR.Rows(C1)("deptname").ToString : drList("prname") = "PR WIP Log" : drList("prcount") = CInt(dtExpPR.Rows(C1)("prcount").ToString) : drList("prcmpcode") = dtExpPR.Rows(C1)("cmpcode").ToString : drList("prdeptoid") = CInt(dtExpPR.Rows(C1)("deptoid").ToString) : drList("prtype") = "wip" : dtList.Rows.Add(drList)
                Next
            End If
        End If
        objDV.RowFilter = ""
        ' PR Fixed Assets
        objDV.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME='PR FIXED ASSETS'"
        If objDV.Count > 0 Then
            sSql = "SELECT pr.cmpcode, divname, deptname, COUNT(prassetmstoid) AS prcount, pr.deptoid FROM QL_prassetmst pr INNER JOIN QL_mstdivision di ON di.cmpcode=pr.cmpcode INNER JOIN QL_mstdept de ON de.cmpcode=pr.cmpcode AND de.deptoid=pr.deptoid WHERE pr.prassetmststatus='Approved' AND DATEDIFF(DAY, CURRENT_TIMESTAMP, pr.prassetexpdate) < 7"
            If Session("CompnyCode") <> CompnyCode Then
                sSql &= " AND pr.cmpcode='" & Session("CompnyCode") & "'"
            End If
            sSql &= " GROUP BY pr.cmpcode, divname, deptname, pr.deptoid"
            dtExpPR = New DataTable
            dtExpPR = ckon.ambiltabel(sSql, "QL_prassetmst")
            If dtExpPR.Rows.Count > 0 Then
                For C1 As Integer = 0 To dtExpPR.Rows.Count - 1
                    drList = dtList.NewRow : drList("prbusunit") = dtExpPR.Rows(C1)("divname").ToString : drList("prdept") = dtExpPR.Rows(C1)("deptname").ToString : drList("prname") = "PR Fixed Assets" : drList("prcount") = CInt(dtExpPR.Rows(C1)("prcount").ToString) : drList("prcmpcode") = dtExpPR.Rows(C1)("cmpcode").ToString : drList("prdeptoid") = CInt(dtExpPR.Rows(C1)("deptoid").ToString) : drList("prtype") = "asset" : dtList.Rows.Add(drList)
                Next
            End If
        End If
        objDV.RowFilter = ""
        gvListPR.DataSource = dtList
        gvListPR.DataBind()
    End Sub

    Private Sub InitNotifyImport()
        Dim objDV As DataView = Session("Role").DefaultView
        objDV.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME='SALES INVOICE'"
        If objDV.Count > 0 Then
            sSql = "SELECT divname AS busunit, trnjualno registerno, CONVERT(VARCHAR(10), trnjualdate, 101) AS registerdate, s.custname suppname, reg.cmpcode, reg.trnjualmstoid registermstoid FROM QL_trnjualmst reg INNER JOIN QL_mstdivision di ON di.cmpcode=reg.cmpcode INNER JOIN QL_mstcust s ON s.custoid=reg.custoid WHERE trnjualstatus='Post' AND trnjualmstoid > 0 AND receivedate='1/1/1900'"
            If Session("CompnyCode") <> CompnyCode Then
                sSql &= " AND reg.cmpcode='" & Session("CompnyCode") & "'"
            End If
            sSql &= " ORDER BY trnjualmstoid DESC"
            FillGV(gvListImport, sSql, "QL_trnregistermst")
        End If
        objDV.RowFilter = ""
    End Sub

    Private Sub InitNotifyWO()
        Dim objDV As DataView = Session("Role").DefaultView
        objDV.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME='JOB COSTING MO'"
        If objDV.Count > 0 Then
            sSql = "SELECT divname AS busunit, womstoid, wono registerno, CONVERT(VARCHAR(10), wodate, 101) AS registerdate, s.suppname suppname, reg.cmpcode, reg.womstoid registermstoid, CONVERT(VARCHAR(10), wostartdate, 101) wostartdate, CONVERT(VARCHAR(10), wofinishdate, 101) wofinishdate, isnull((select transitemno from QL_trntransitemmst where transitemmstoid=lastplanmstoid), '') transitemno FROM QL_trnwomst reg INNER JOIN QL_mstdivision di ON di.cmpcode=reg.cmpcode INNER JOIN QL_mstsupp s ON s.suppoid=reg.suppoid WHERE womststatus IN ('Post', 'In Process') AND womstoid > 0 AND DATEADD( Year, DATEPART( Year, GETDATE()) - DATEPART( Year, wofinishdate ), wofinishdate ) BETWEEN CONVERT( DATE, GETDATE()) AND CONVERT( DATE, GETDATE() + 3)"
            If Session("CompnyCode") <> CompnyCode Then
                sSql &= " AND reg.cmpcode='" & Session("CompnyCode") & "'"
            End If
            sSql &= " ORDER BY womstoid DESC"
            FillGV(gvListWO, sSql, "QL_trnregistermst")
        End If
        objDV.RowFilter = ""
    End Sub

    Private Sub PostingGiroOutRealization()
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        ' Get Giro Out Data
        ' Summary
        sSql = "SELECT cashbankoid, cashbankno, '' AS newcashbankno, cashbankamt, cashbankamtidr, cashbankamtusd, (CASE cashbankgroup WHEN 'EXPENSE' THEN refsuppoid ELSE personoid END) AS personoid, cashbankrefno, cashbanknote, giroacctgoid, acctgoid, curroid, CONVERT(VARCHAR(10), cashbankduedate, 101) AS cashbankduedate FROM QL_trncashbankmst WHERE cmpcode='" & Session("CompnyCode") & "' AND cashbanktype='BGK' AND cashbankstatus='Post' AND cashbankres1='Post' AND cashbankduedate<=CONVERT(DATETIME, '" & sDate & " 23:59:59')"
        Dim dtSumGiro As DataTable = ckon.ambiltabel(sSql, "QL_girosummary")
        If dtSumGiro.Rows.Count > 0 Then
            ' Initiate Required Variable
            Dim cashbankoid As Integer = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
            Dim sOids As String = ""
            For C1 As Integer = 0 To dtSumGiro.Rows.Count - 1
                If Not sOids.Contains("[" & dtSumGiro.Rows(C1)("acctgoid").ToString & "]") Then
                    sOids &= "[" & dtSumGiro.Rows(C1)("acctgoid").ToString & "];"
                End If
            Next
            If sOids <> "" Then
                sOids = Left(sOids, sOids.Length - 1)
            End If
            Dim sSplitOid() As String = sOids.Replace("[", "").Replace("]", "").Split(";")
            Dim sNo As String = "BBK-" & Format(CDate(sDate), "yyyy.MM") & "-"
            Dim iNoSeq As Integer = 0
            For C1 As Integer = 0 To sSplitOid.Length - 1
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" & Session("CompnyCode") & "' AND cashbankno LIKE '%" & sNo & "%' AND acctgoid=" & sSplitOid(C1)
                If GetStrData(sSql) <> "" Then
                    iNoSeq = ToInteger(GetStrData(sSql))
                Else
                    iNoSeq = 1
                End If
                dtSumGiro.DefaultView.RowFilter = "acctgoid=" & sSplitOid(C1)
                For C2 As Integer = 0 To dtSumGiro.DefaultView.Count - 1
                    dtSumGiro.DefaultView(C2)("newcashbankno") = GenNumberString(sNo, "", iNoSeq + C2, DefaultFormatCounter)
                Next
                dtSumGiro.DefaultView.RowFilter = ""
            Next
            Dim payapgirooid As Integer = GenerateID("QL_TRNPAYAPGIRO", CompnyCode)
            Dim iGlMstOid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
            Dim iGlDtlOid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
            Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
            ' Posting Giro Out Realization
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                For C1 As Integer = 0 To dtSumGiro.Rows.Count - 1
                    sPeriod = GetDateToPeriodAcctg(CDate(dtSumGiro.Rows(C1)("cashbankduedate").ToString))
                    ' Posting Summary
                    sSql = "INSERT INTO QL_trncashbankmst (cmpcode, cashbankoid, periodacctg, cashbankno, cashbankdate, cashbanktype, cashbankgroup, acctgoid, curroid, cashbankamt, cashbankamtidr, cashbankamtusd, personoid, cashbankduedate, cashbankrefno, cashbanknote, cashbankstatus, createuser, createtime, upduser, updtime) VALUES ('" & Session("CompnyCode") & "', " & cashbankoid + C1 & ", '" & sPeriod & "', '" & dtSumGiro.Rows(C1)("newcashbankno").ToString & "', '" & dtSumGiro.Rows(C1)("cashbankduedate").ToString & "', 'BBK', 'GIRO OUT', " & dtSumGiro.Rows(C1)("acctgoid") & ", " & dtSumGiro.Rows(C1)("curroid") & ", " & ToDouble(dtSumGiro.Rows(C1)("cashbankamt").ToString) & ", " & ToDouble(dtSumGiro.Rows(C1)("cashbankamtidr").ToString) & ", " & ToDouble(dtSumGiro.Rows(C1)("cashbankamtusd").ToString) & ", 0, '" & dtSumGiro.Rows(C1)("cashbankduedate").ToString & "', '', 'Pencairan Cash/Bank No. " & dtSumGiro.Rows(C1)("cashbankno").ToString & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Posting Detail
                    sSql = "INSERT INTO QL_trnpayapgiro (cmpcode, payapgirooid, cashbankoid, suppoid, reftype, refoid, refacctgoid, payapgirorefno, payapgiroduedate, payapgiroamt, payapgiroamtidr, payapgiroamtusd, payapgironote, payapgirostatus, upduser, updtime) VALUES ('" & Session("CompnyCode") & "', " & payapgirooid + C1 & ", " & cashbankoid + C1 & ", " & dtSumGiro.Rows(C1)("personoid") & ", 'Pay A/P Giro Out', " & dtSumGiro.Rows(C1)("cashbankoid") & ", " & dtSumGiro.Rows(C1)("giroacctgoid") & ", '" & dtSumGiro.Rows(C1)("cashbankrefno").ToString & "', '" & dtSumGiro.Rows(C1)("cashbankduedate").ToString & "', " & ToDouble(dtSumGiro.Rows(C1)("cashbankamt").ToString) & ", " & ToDouble(dtSumGiro.Rows(C1)("cashbankamtidr").ToString) & ", " & ToDouble(dtSumGiro.Rows(C1)("cashbankamtusd").ToString) & ", 'Pencairan Cash/Bank No. " & Tchar(dtSumGiro.Rows(C1)("cashbankno").ToString) & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trncashbankmst SET cashbankres1='Closed' WHERE cmpcode='" & Session("CompnyCode") & "' AND cashbankoid=" & dtSumGiro.Rows(C1)("cashbankoid")
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Insert Into GL Mst
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & Session("CompnyCode") & "', " & iGlMstOid & ", '" & dtSumGiro.Rows(C1)("cashbankduedate").ToString & "', '" & sPeriod & "', 'Giro Out|No=" & dtSumGiro.Rows(C1)("newcashbankno").ToString & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 0, 0, 0, 0, 0, 0)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Insert Into GL Dtl
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & Session("CompnyCode") & "', " & iGlDtlOid & ", 1, " & iGlMstOid & ", " & dtSumGiro.Rows(C1)("giroacctgoid") & ", 'D', " & ToDouble(dtSumGiro.Rows(C1)("cashbankamt").ToString) & ", '" & dtSumGiro.Rows(C1)("newcashbankno").ToString & "', '', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dtSumGiro.Rows(C1)("cashbankamtidr").ToString) & ", " & ToDouble(dtSumGiro.Rows(C1)("cashbankamtusd").ToString) & ", 'QL_trncashbankmst " & cashbankoid + C1 & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & Session("CompnyCode") & "', " & iGlDtlOid & ", 2, " & iGlMstOid & ", " & dtSumGiro.Rows(C1)("acctgoid") & ", 'C', " & ToDouble(dtSumGiro.Rows(C1)("cashbankamt").ToString) & ", '" & dtSumGiro.Rows(C1)("newcashbankno").ToString & "', '', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dtSumGiro.Rows(C1)("cashbankamtidr").ToString) & ", " & ToDouble(dtSumGiro.Rows(C1)("cashbankamtusd").ToString) & ", 'QL_trncashbankmst " & cashbankoid + C1 & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1
                    iGlMstOid += 1
                Next
                sSql = "UPDATE QL_mstoid SET lastoid=" & cashbankoid + dtSumGiro.Rows.Count - 1 & " WHERE tablename='QL_TRNCASHBANKMST' and cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & payapgirooid + dtSumGiro.Rows.Count - 1 & " WHERE tablename='QL_TRNPAYAPGIRO' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid - 1 & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                objTrans.Commit()
                conn.Close()
            Catch ex As Exception
                objTrans.Rollback()
                conn.Close()
            End Try
            conn.Close()
        End If
    End Sub

    Private Sub UpdateCreditLimitTrans()
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DECLARE @cmpcode AS VARCHAR (10);"
            sSql &= "SET @cmpcode='" & Session("CompnyCode") & "'; "
            sSql &= "WITH cgx AS (select custgroupoid, custgroupname, custcreditlimitrupiah, custcreditlimitusagerupiah, SUM(amttrans-amtbayar) AS balance from (select cg.custgroupoid, custgroupname, custcreditlimitrupiah, custcreditlimitusagerupiah, (select SUM(amttransidr) from QL_conar con where con.cmpcode=cg.cmpcode and con.custoid=c.custoid and payrefoid=0) AS amttrans , isnull((select SUM(amtbayaridr) from QL_conar con left join QL_trnpayar pay on pay.cmpcode=con.cmpcode and pay.refoid=con.refoid and pay.reftype=con.reftype and pay.payaroid=con.payrefoid where con.cmpcode=cg.cmpcode and con.payrefoid<>0 and con.custoid=c.custoid and con.payrefoid<>0 and ISNULL(pay.payarres1,'')<>'Lebih Bayar'),0) AS amtbayar from QL_mstcustgroup cg inner join QL_mstcust c on c.custgroupoid=cg.custgroupoid where cg.cmpcode=@cmpcode and c.custoid in (select distinct con.custoid from QL_conar con)) as dt group by custgroupoid, custgroupname, custcreditlimitrupiah, custcreditlimitusagerupiah having SUM(amttrans-amtbayar)<>custcreditlimitusagerupiah) "
            sSql &= "update QL_mstcustgroup set custcreditlimitusagerupiah=cgx.balance from cgx where QL_mstcustgroup.custgroupoid=cgx.custgroupoid"

            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
        End Try
        conn.Close()
    End Sub
#End Region

#Region "Function"
    Private Function SetLinking(ByVal sImgPath As String, ByVal sMenuPath As String, ByVal sMenuText As String) As String
        Dim sTag As String = ""
        sTag = "<a href='../" & sMenuPath & "'><IMG Height='40px' Width='40' " & _
            "src='../Images/Icons/" & IIf(sMenuPath.ToUpper.Contains("UNDERMAINTAIN.ASPX"), "UnderMaintain.png", sImgPath) & "' Border='0'/><br />" & sMenuText.Replace("REPORT", "").Replace("ACCOUNTING", "") & "</a>"
        Return sTag
    End Function
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("menu.aspx")
        End If
        Page.Title = CompnyName & " - Home"
        If Not IsPostBack Then
            SetupMenu()
            'InitNotifyAdmin()
            TDNotifyAdmin.Visible = (gvListAdmin.Rows.Count > 0)
            InitNotifyApproval()
            TDNotifyApproval.Visible = (gvListApp.Rows.Count > 0)
            'ClosedExpiredPR()
            'InitNotifyExpiredPR()
            TDNotifyExpiredPR.Visible = (gvListPR.Rows.Count > 0)
            'InitNotifyImport()
            TDNotifyImport.Visible = (gvListImport.Rows.Count > 0)
            InitNotifyWO()
            TDNotifyWO.Visible = (gvListWO.Rows.Count > 0)
            TabContainer1.Tabs(0).Enabled = (TDNotifyAdmin.Visible Or TDNotifyApproval.Visible Or TDNotifyExpiredPR.Visible Or TDNotifyImport.Visible Or TDNotifyWO.Visible)
            SetActiveTab()
            UpdateCreditLimitTrans()
            'PostingGiroOutRealization()
        End If
    End Sub

    Protected Sub gvListAdmin_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListAdmin.SelectedIndexChanged
        PostNotification(gvListAdmin.SelectedDataKey.Item("noteoid").ToString)
    End Sub

    Protected Sub gvListApp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListApp.SelectedIndexChanged
        Response.Redirect("~\Other\WaitingActionNew.aspx?formvalue=" & gvListApp.SelectedDataKey.Item("appformvalue").ToString & "&formtext=" & gvListApp.SelectedDataKey.Item("appformtext").ToString)
    End Sub

    Protected Sub gvListPR_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListPR.SelectedIndexChanged
        Response.Redirect("~\Transaction\trnPRExt.aspx?prtype=" & gvListPR.SelectedDataKey.Item("prtype").ToString & "&prcmpcode=" & gvListPR.SelectedDataKey.Item("prcmpcode").ToString & "&prdeptoid=" & gvListPR.SelectedDataKey.Item("prdeptoid").ToString)
    End Sub

    Protected Sub gvListImport_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListImport.SelectedIndexChanged
        Response.Redirect("~\Transaction\trnAR.aspx?oid=" & gvListImport.SelectedDataKey.Item("registermstoid").ToString)
    End Sub

    Protected Sub gvListWO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("~\Transaction\trnWO.aspx?oid=" & gvListWO.SelectedDataKey.Item("registermstoid").ToString)
    End Sub
#End Region


End Class
