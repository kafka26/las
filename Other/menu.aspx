﻿<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="menu.aspx.vb" Inherits="menu" Title="" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="middleContent">
    <table id="MainTable" align="center" style="width: 100%;">
        <tr>
            <td valign="top">
                <table id="Table1" bgcolor="white" class="tabelhias" style="width: 100%;">
                    <tr>
                        <th align="left" class="header" valign="center" style="width: 50%">
                            <asp:Label ID="Label1" runat="server" CssClass="Title" Font-Bold="False"
                                Text=".: Home"></asp:Label></th>
                        <th align="right" class="header" valign="middle" style="text-align: right">
                            <img align="absMiddle" alt="" src="../Images/corner.gif">
                            <asp:LinkButton ID="LinkButton5" runat="server" Font-Bold="True" PostBackUrl="~/Other/WaitingActionNew.aspx?awal=true">:: Waiting for My Approval</asp:LinkButton></th>
                    </tr>
                    <tr>
                        <th align="left" colspan="2" valign="center">
                            <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0"
                                Width="100%">
                                <ajaxToolkit:TabPanel ID="TabNotification" runat="server" HeaderText="Notification">
                                    <HeaderTemplate>
                                        :. Notification
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <asp:UpdatePanel ID="upNotification" runat="server">
                                            <ContentTemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD id="TDNotifyAdmin" class="Label" align=left runat="server"><TABLE style="BORDER-RIGHT: darkgray thin solid; BORDER-TOP: darkgray thin solid; BORDER-LEFT: darkgray thin solid; WIDTH: 100%; BORDER-BOTTOM: darkgray thin solid"><TBODY><TR><TD class="Label" align=left><asp:Label id="LabelHeaderNotifyAdmin" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" __designer:wfdid="w242"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:GridView id="gvListAdmin" runat="server" Width="100%" __designer:wfdid="w243" DataKeyNames="noteoid" ShowHeader="False" GridLines="None" AutoGenerateColumns="False">
                                                                <Columns>
                                                                    <asp:BoundField DataField="noteseq">
                                                                        <ItemStyle Height="15px" HorizontalAlign="Left" Width="3%" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            From :
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" Width="7%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="noteuser">
                                                                        <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            Message :
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="notemessage">
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:CommandField SelectText="[ End Show ]" ShowSelectButton="True">
                                                                        <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                                    </asp:CommandField>
                                                                </Columns>
                                                            </asp:GridView> </TD></TR></TBODY></TABLE></TD></TR><TR><TD id="TDNotifyApproval" class="Label" align=left runat="server"><TABLE style="BORDER-RIGHT: darkgray thin solid; BORDER-TOP: darkgray thin solid; BORDER-LEFT: darkgray thin solid; WIDTH: 100%; BORDER-BOTTOM: darkgray thin solid"><TBODY><TR><TD class="Label" align=left><asp:Label id="LabelHeaderNotifyApproval" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Following transactions are waiting for your approval :" __designer:wfdid="w244"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:GridView id="gvListApp" runat="server" Width="50%" __designer:wfdid="w245" DataKeyNames="appformvalue,appformtext" ShowHeader="False" GridLines="None" AutoGenerateColumns="False"><Columns>
<asp:BoundField DataField="apptype">
<ItemStyle HorizontalAlign="Left" Height="15px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="appsept">
<ItemStyle HorizontalAlign="Center" Width="2%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="appcount">
<ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
</asp:BoundField>
<asp:CommandField SelectText="[ View ]" ShowSelectButton="True">
<ItemStyle HorizontalAlign="Center" Width="15%"></ItemStyle>
</asp:CommandField>
</Columns>
</asp:GridView> </TD></TR></TBODY></TABLE></TD></TR><TR><TD id="TDNotifyExpiredPR" class="Label" align=left runat="server"><TABLE style="BORDER-RIGHT: darkgray thin solid; BORDER-TOP: darkgray thin solid; BORDER-LEFT: darkgray thin solid; WIDTH: 100%; BORDER-BOTTOM: darkgray thin solid"><TBODY><TR><TD class="Label" align=left><asp:Label id="LabelHeaderNotifyExpiredPR" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Following PR transactions will be expired less than 7 days :" __designer:wfdid="w246"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:GridView id="gvListPR" runat="server" Width="100%" __designer:wfdid="w247" DataKeyNames="prtype,prcmpcode,prdeptoid" GridLines="None" AutoGenerateColumns="False">
                                                                <Columns>
                                                                    <asp:BoundField DataField="prbusunit" HeaderText="Business Unit">
                                                                        <HeaderStyle CssClass="gvnotify" Height="15px" HorizontalAlign="Left" />
                                                                        <ItemStyle HorizontalAlign="Left" Height="15px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="prdept" HeaderText="Department">
                                                                        <HeaderStyle CssClass="gvnotify" HorizontalAlign="Left" />
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="prname" HeaderText="PR Type">
                                                                        <HeaderStyle CssClass="gvnotify" HorizontalAlign="Left" />
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="prcount" HeaderText="Total">
                                                                        <HeaderStyle CssClass="gvnotify" HorizontalAlign="Right" />
                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                    </asp:BoundField>
                                                                    <asp:CommandField SelectText="[ View ]" ShowSelectButton="True">
                                                                        <HeaderStyle CssClass="gvnotify" HorizontalAlign="Center" />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:CommandField>
                                                                </Columns>
                                                            </asp:GridView> </TD></TR></TBODY></TABLE></TD></TR><TR><TD id="TDNotifyImport" class="Label" align=left runat="server"><TABLE style="BORDER-RIGHT: darkgray thin solid; BORDER-TOP: darkgray thin solid; BORDER-LEFT: darkgray thin solid; WIDTH: 100%; BORDER-BOTTOM: darkgray thin solid"><TBODY><TR><TD class="Label" align=left><asp:Label id="LabelHeaderNotifyImport" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Following Sales Invoice data are waiting receive date :" __designer:wfdid="w248"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:GridView id="gvListImport" runat="server" Width="100%" __designer:wfdid="w249" DataKeyNames="cmpcode,registermstoid,registerno" GridLines="None" AutoGenerateColumns="False"><Columns>
<asp:BoundField DataField="busunit" HeaderText="Business Unit">
<HeaderStyle HorizontalAlign="Left" CssClass="gvnotify" Height="15px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Height="15px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="registerno" HeaderText="SI. No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvnotify"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="registerdate" HeaderText="SI. Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvnotify"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvnotify"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField SelectText="[ View ]" ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvnotify"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
</Columns>
</asp:GridView> </TD></TR></TBODY></TABLE></TD></TR><TR><TD id="TDNotifyWO" class="Label" align=left runat="server"><TABLE style="BORDER-RIGHT: darkgray thin solid; BORDER-TOP: darkgray thin solid; BORDER-LEFT: darkgray thin solid; WIDTH: 100%; BORDER-BOTTOM: darkgray thin solid"><TBODY><TR><TD class="Label" align=left><asp:Label id="LabelHeaderNotifyWO" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Following Job Costing MO data Will be expired :" __designer:wfdid="w248"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:GridView id="gvListWO" runat="server" Width="100%" __designer:wfdid="w249" DataKeyNames="cmpcode,registermstoid,registerno,womstoid,transitemno" GridLines="None" AutoGenerateColumns="False" OnSelectedIndexChanged="gvListWO_SelectedIndexChanged"><Columns>
<asp:BoundField DataField="womstoid" HeaderText="Draft">
<HeaderStyle HorizontalAlign="Left" CssClass="gvnotify" Height="15px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Height="15px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="registerno" HeaderText="SPK No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvnotify"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="registerdate" HeaderText="Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvnotify"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvnotify"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wostartdate" HeaderText="Start Date">
<HeaderStyle CssClass="gvnotify"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="wofinishdate" HeaderText="Finish Date">
<HeaderStyle CssClass="gvnotify"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="transitemno" HeaderText="No. SJP" SortExpression="transitemno">
<HeaderStyle CssClass="gvnotify"></HeaderStyle>
</asp:BoundField>
<asp:CommandField SelectText="[ View ]" ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvnotify"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
</Columns>
</asp:GridView> </TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
</ContentTemplate>
                                        </asp:UpdatePanel>
                                    </ContentTemplate>
                                </ajaxToolkit:TabPanel>
                                <ajaxToolkit:TabPanel ID="TabSetup" runat="server" HeaderText="Setup">
                                    <ContentTemplate>
                                        <asp:Label ID="LABELSETUP" runat="server"></asp:Label>
                                    </ContentTemplate>
                                    <HeaderTemplate>
                                        :. Setup
                                    </HeaderTemplate>
                                </ajaxToolkit:TabPanel>
                                <ajaxToolkit:TabPanel ID="TabPurchasing" runat="server" HeaderText="Purchasing">
                                    <ContentTemplate>
                                        <asp:Label ID="LABELPURCHASING" runat="server"></asp:Label>
                                    </ContentTemplate>
                                    <HeaderTemplate>
                                        :. Purchasing
                                    </HeaderTemplate>
                                </ajaxToolkit:TabPanel>
                                <ajaxToolkit:TabPanel ID="TabMarketing" runat="server" HeaderText="Marketing">
                                    <ContentTemplate>
                                        <asp:Label ID="LABELMARKETING" runat="server"></asp:Label>
                                    </ContentTemplate>
                                    <HeaderTemplate>
                                        :. Marketing
                                    </HeaderTemplate>
                                </ajaxToolkit:TabPanel>
                                <ajaxToolkit:TabPanel ID="TabInventory" runat="server" HeaderText="Inventory">
                                    <ContentTemplate>
                                        <asp:Label ID="LABELINVENTORY" runat="server"></asp:Label>
                                    </ContentTemplate>
                                    <HeaderTemplate>
                                        :. Inventory
                                    </HeaderTemplate>
                                </ajaxToolkit:TabPanel>
                                <ajaxToolkit:TabPanel ID="TabProduction" runat="server" HeaderText="Production">
                                    <ContentTemplate>
                                        <asp:Label ID="LABELPRODUCTION" runat="server"></asp:Label>
                                    </ContentTemplate>
                                    <HeaderTemplate>
                                        :. Production
                                    </HeaderTemplate>
                                </ajaxToolkit:TabPanel>
                                <ajaxToolkit:TabPanel ID="TabAccounting" runat="server" HeaderText="Accounting">
                                    <ContentTemplate>
                                        <asp:Label ID="LABELACCOUNTING" runat="server"></asp:Label>
                                    </ContentTemplate>
                                    <HeaderTemplate>
                                        :. Accounting
                                    </HeaderTemplate>
                                </ajaxToolkit:TabPanel>
                                <ajaxToolkit:TabPanel ID="TabWIPLog" runat="server" HeaderText="WIP Log">
                                    <ContentTemplate>
                                        <asp:Label ID="LABELWIPLOG" runat="server"></asp:Label>
                                    </ContentTemplate>
                                    <HeaderTemplate>
                                        :. WIP Log
                                    </HeaderTemplate>
                                </ajaxToolkit:TabPanel>
                                <ajaxToolkit:TabPanel ID="TabFixedAssets" runat="server" HeaderText="Fixed Assets">
                                    <ContentTemplate>
                                        <asp:Label ID="LABELFIXEDASSETS" runat="server"></asp:Label>
                                    </ContentTemplate>
                                    <HeaderTemplate>
                                        :. Fixed Assets
                                    </HeaderTemplate>
                                </ajaxToolkit:TabPanel>
                                
                            </ajaxToolkit:TabContainer></th>
                    </tr>
                    <tr>
                        <th align="right" class="header" valign="middle" colspan="2" style="text-align: left">
                            <img align="absMiddle" alt="" src="../Images/corner.gif">
                            <asp:LinkButton ID="LinkButton1" runat="server" Font-Bold="True" PostBackUrl="~/Other/Notification.aspx?awal=true">:: Message</asp:LinkButton></th>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
