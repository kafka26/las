' Catatan Penting : Jika ada penambahan Waiting Action maka hal-hal yang harus dilakukan :
' 1. Meng-update Procedure InitMenuApp()
' 2. Menambahkan Region Transaksi yang dimaksud
' 3. Meng-update Procedure SetDataList()
' 4. Meng-update Procedure ApprovalAction(sAction As String)
' 5. Meng-update Event gvList_SelectedIndexChanged(sender AS Object, e As System.EventArgs)

Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class Other_WaitingActionNew
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim conna As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn_A"))
    Dim xCmdA As New SqlCommand("", conna)
    Dim connb As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn_B"))
    Dim xCmdB As New SqlCommand("", connb)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim sSqlPlus As String = ""
#End Region

#Region "Function"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", CompnyName & " - WARNING", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", CompnyName & " - WARNING", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", CompnyName & " - WARNING", 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetNumericValues(ByVal sData As String) As String
        Dim sNumeric As String = ""
        Dim sCurr As String = ""
        For C1 As Integer = 1 To Len(sData)
            sCurr = Mid(sData, C1, 1)
            If IsNumeric(sCurr) Then
                sNumeric &= sCurr
            End If
        Next
        GetNumericValues = sNumeric
    End Function

    Private Function FillDDLWithAll(ByRef oDDLObject As DropDownList, ByVal sSql As String) As Boolean
        ' To fill data in dropdown list, return true when all data exist
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim xreader As SqlDataReader
        FillDDLWithAll = True
        oDDLObject.Items.Clear()
        oDDLObject.Items.Add("All")
        oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = "All"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            oDDLObject.Items.Add(xreader.GetValue(1))
            oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()
        ' Cek if all data exits
        If oDDLObject.Items.Count = 0 Then
            FillDDLWithAll = False
        End If
        Return FillDDLWithAll
    End Function

    Private Function SetBusUnit(ByVal sCode As String) As String
        sSql = "SELECT divname FROM QL_mstdivision WHERE cmpcode='" & sCode & "'"
        Return GetStrData(sSql)
    End Function

    Private Function GetAcctgStock(ByVal sType As String) As String
        GetAcctgStock = ""
        sSql = "SELECT ISNULL(genother1,0) acctgoid FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='GROUPITEM' AND gencode='" & sType & "' AND activeflag='ACTIVE'"
        GetAcctgStock = GetStrData(sSql)
        Return GetAcctgStock
    End Function

    Private Function GetBackupValueIDR(ByVal sCmpCode As String, ByVal iRefOid As Integer, ByVal sPeriodAcctg As String) As Double
        Return ToDouble(GetStrData("SELECT TOP 1 valueidr FROM QL_constock WHERE cmpcode='" & sCmpCode & "' AND refoid=" & iRefOid & " AND valueidr>0 AND periodacctg<='" & sPeriodAcctg & "' ORDER BY updtime DESC"))
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
        End If
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Private Sub InitMenuApp()
        sSql = "SELECT COUNT(*) AS jmlapp, tablename FROM QL_approval WHERE approvaluser='" & Session("UserID") & "' AND statusrequest='New' AND event='In Approval' GROUP BY tablename"
        Dim dtApp As DataTable = ckon.ambiltabel(sSql, "QL_approval")
        Dim dvApp As DataView = dtApp.DefaultView
        If dvApp.Count > 0 Then
            Dim arColNameList() As String = {"apptype", "appsept", "appcount"}
            Dim arColTypeList() As String = {"System.String", "System.String", "System.Int32"}
            Dim dtList As DataTable = SetTableDetail("tblAppList", arColNameList, arColTypeList)
            Dim drList As DataRow
            Dim arColNameMenu() As String = {"appname", "tablename"}
            Dim arColTypeMenu() As String = {"System.String", "System.String"}
            Dim dtMenu As DataTable = SetTableDetail("tblAppMenu", arColNameMenu, arColTypeMenu)
            Dim drMenu As DataRow
            ' PR Raw Material
            dvApp.RowFilter = "tablename = 'QL_prrawmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PR Raw Material" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� PR Raw Material" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' PR General Material
            dvApp.RowFilter = "tablename = 'QL_prgenmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PR General Material" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� PR General Material" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' PR Spare Part
            dvApp.RowFilter = "tablename = 'QL_prspmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PR Spare Part" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� PR Spare Part" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' PR Finish Good
            dvApp.RowFilter = "tablename = 'QL_pritemmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PR Finish Good" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� PR Finish Good" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' PR Fixed Assets
            dvApp.RowFilter = "tablename = 'QL_prassetmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PR Fixed Assets" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� PR Fixed Assets" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            ' PR Log
            dvApp.RowFilter = "tablename = 'QL_prwipmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PR Log" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� PR Log" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' PR Sawn Timber
            dvApp.RowFilter = "tablename = 'QL_prsawnmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PR Sawn Timber" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� PR Sawn Timber" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' PO Raw Material
            dvApp.RowFilter = "tablename = 'QL_trnporawmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PO Raw Material" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� PO Raw Material" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' PO General Material
            dvApp.RowFilter = "tablename = 'QL_trnpogenmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PO General Material" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� PO General Material" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' PO Spare Part
            dvApp.RowFilter = "tablename = 'QL_trnpospmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PO Spare Part" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� PO Spare Part" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' PO Finish Good
            dvApp.RowFilter = "tablename = 'QL_trnpofgmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PO Finish Good" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� PO Finish Good" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' PO Fixed Assets
            dvApp.RowFilter = "tablename = 'QL_trnpoassetmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PO Fixed Assets" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� PO Fixed Assets" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' PO WIP
            dvApp.RowFilter = "tablename = 'QL_trnpowipmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PO Walk In Process (WIP)" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� PO Walk In Process (WIP)" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' PO Sawn Timber
            dvApp.RowFilter = "tablename = 'QL_trnposawnmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PO Sawn Timber" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� PO Sawn Timber" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' PO Service
            dvApp.RowFilter = "tablename = 'QL_trnposervicemst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PO Service" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� PO Service" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' PO Subcon
            dvApp.RowFilter = "tablename = 'QL_trnposubconmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- PO Subcon" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� PO Subcon" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' Purchase Return Raw Material
            dvApp.RowFilter = "tablename = 'QL_trnreturmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Purchase Return" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� Purchase Return" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' Purchase Return General Material
            dvApp.RowFilter = "tablename = 'QL_trnpretgenmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Purchase Return General Material" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� Purchase Return General Material" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' Purchase Return Spare Part
            dvApp.RowFilter = "tablename = 'QL_trnpretspmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Purchase Return Spare Part" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� Purchase Return Spare Part" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' Purchase Return Finish Good
            dvApp.RowFilter = "tablename = 'QL_trnpretitemmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Purchase Return Finish Good" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� Purchase Return Finish Good" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' Purchase Return Fixed Assets
            dvApp.RowFilter = "tablename = 'QL_trnpretassetmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Purchase Return Fixed Assets" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� Purchase Return Fixed Assets" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' Purchase Return Log
            dvApp.RowFilter = "tablename = 'QL_trnpretwipmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Purchase Return Log" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� Purchase Return Log" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' Purchase Return Sawn Timber
            dvApp.RowFilter = "tablename = 'QL_trnpretsawnmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Purchase Return Sawn Timber" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� Purchase Return Sawn Timber" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' Purchase Return Subcon
            dvApp.RowFilter = "tablename = 'QL_trnpretsubconmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Purchase Return Subcon" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� Purchase Return Subcon" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' Purchase Invoice Raw Material
            dvApp.RowFilter = "tablename = 'QL_trnaprawmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Purchase Invoice Raw Material" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� Purchase Invoice Raw Material" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' Purchase Invoice General Material
            dvApp.RowFilter = "tablename = 'QL_trnapgenmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Purchase Invoice General Material" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� Purchase Invoice General Material" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' Purchase Invoice Finish Good
            dvApp.RowFilter = "tablename = 'QL_trnapfgmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Purchase Invoice Finish Good" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� Purchase Invoice Finish Good" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' Purchase Invoice WIP
            dvApp.RowFilter = "tablename = 'QL_trnapwipmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Purchase Invoice WIP" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� Purchase Invoice WIP" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' SO Raw Material
            dvApp.RowFilter = "tablename = 'QL_trnsorawmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- SO Raw Material" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� SO Raw Material" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' SO General Material
            dvApp.RowFilter = "tablename = 'QL_trnsogenmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- SO General Material" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� SO General Material" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' SO Spare Part
            dvApp.RowFilter = "tablename = 'QL_trnsospmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- SO Spare Part" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� SO Spare Part" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' SO Finish Good
            dvApp.RowFilter = "tablename = 'QL_trnsomst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- SALES ORDER" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� SALES ORDER" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' SO Fixed Assets
            dvApp.RowFilter = "tablename = 'QL_trnsoassetmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- SO Fixed Assets" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� SO Fixed Assets" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' Shipment Raw Material
            dvApp.RowFilter = "tablename = 'QL_trndomst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- DELIVERY ORDER" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� DELIVERY ORDER" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' Shipment General Material
            dvApp.RowFilter = "tablename = 'QL_trnshipmentgenmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Shipment General Material" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� Shipment General Material" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' Shipment Spare Part
            dvApp.RowFilter = "tablename = 'QL_trnshipmentspmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Shipment Spare Part" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� Shipment Spare Part" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' Shipment Finish Good
            dvApp.RowFilter = "tablename = 'QL_trnshipmentitemmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Shipment Finish Good" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� Shipment Finish Good" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' Sales Return Raw Material
            dvApp.RowFilter = "tablename = 'QL_trnsalesreturmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Sales Return" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� Sales Return" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' Sales Return General Material
            dvApp.RowFilter = "tablename = 'QL_trnsretgenmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Sales Return General Material" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� Sales Return General Material" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' Sales Return Spare Part
            dvApp.RowFilter = "tablename = 'QL_trnsretspmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Sales Return Spare Part" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� Sales Return Spare Part" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' Sales Return Finish Good
            dvApp.RowFilter = "tablename = 'QL_trnsretitemmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Sales Return Finish Good" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� Sales Return Finish Good" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' Sales Return Fixed Assets
            dvApp.RowFilter = "tablename = 'QL_trnsretassetmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Sales Return Fixed Assets" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� Sales Return Fixed Assets" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' A/R Raw Material
            dvApp.RowFilter = "tablename = 'QL_trnarrawmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- A/R Raw Material" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� A/R Raw Material" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' A/R General Material
            dvApp.RowFilter = "tablename = 'QL_trnargenmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- A/R General Material" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� A/R General Material" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' A/R Spare Part
            dvApp.RowFilter = "tablename = 'QL_trnarspmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- A/R Spare Part" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� A/R Spare Part" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' A/R Finish Good
            dvApp.RowFilter = "tablename = 'QL_trnaritemmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- A/R Finish Good" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� A/R Finish Good" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' A/R Fixed Assets
            dvApp.RowFilter = "tablename = 'QL_trnarassetmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- A/R Fixed Assets" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� A/R Fixed Assets" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""
            ' Berita Acara
            dvApp.RowFilter = "tablename = 'QL_trnbrtacaramst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Berita Acara" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� Berita Acara" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""

            ' Adjustment
            'dvApp.RowFilter = "tablename = 'QL_trnstockadj' AND jmlapp > 0"
            'If dvApp.Count > 0 Then
            '    drList = dtList.NewRow : drList("apptype") = "- Stock Adjustment" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "QL_trnstockadj" : drList("appformtext") = "� Stock Adjustment" : dtList.Rows.Add(drList)
            'End If
            'dvApp.RowFilter = ""

            '' Berita Acara Product
            dvApp.RowFilter = "tablename = 'QL_trnstockadj' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- Stock Adjustment" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� Stock Adjustment" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""

            ' KIK Planning Date
            dvApp.RowFilter = "tablename = 'QL_trnplanmst' AND jmlapp > 0"
            If dvApp.Count > 0 Then
                drList = dtList.NewRow : drList("apptype") = "- KIK Planning Date" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : dtList.Rows.Add(drList)
                drMenu = dtMenu.NewRow : drMenu("appname") = "� KIK Planning Date" : drMenu("tablename") = dvApp(0)("tablename").ToString.ToUpper : dtMenu.Rows.Add(drMenu)
            End If
            dvApp.RowFilter = ""

            gvListApp.DataSource = dtList
            gvListApp.DataBind()
            gvMenu.DataSource = dtMenu
            gvMenu.DataBind()
        End If
    End Sub

    Private Sub SetDataList()
        MultiView1.SetActiveView(ViewList)
        gvList.DataSource = Nothing
        gvList.DataBind()
        gvList.Columns.Clear()
        Dim field As New System.Web.UI.WebControls.CommandField
        field.ShowSelectButton = True
        field.HeaderStyle.CssClass = "gvhdr"
        field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
        field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
        gvList.Columns.Add(field)
        TDDept1.Visible = False : TDDept2.Visible = False : TDDept3.Visible = False : LblinfoSO.Visible = False
        If lblAppType.Text = "QL_PRRAWMST" Then
            TDDept1.Visible = True : TDDept2.Visible = True : TDDept3.Visible = True
            SetGVListPRRM()
        ElseIf lblAppType.Text = "QL_PRGENMST" Then
            TDDept1.Visible = True : TDDept2.Visible = True : TDDept3.Visible = True
            SetGVListPRGM()
        ElseIf lblAppType.Text = "QL_PRSPMST" Then
            TDDept1.Visible = True : TDDept2.Visible = True : TDDept3.Visible = True
            SetGVListPRSP()
        ElseIf lblAppType.Text = "QL_PRITEMMST" Then
            TDDept1.Visible = True : TDDept2.Visible = True : TDDept3.Visible = True
            SetGVListPRItem()
        ElseIf lblAppType.Text = "QL_PRASSETMST" Then
            TDDept1.Visible = True : TDDept2.Visible = True : TDDept3.Visible = True
            SetGVListPRAssets()
        ElseIf lblAppType.Text = "QL_PRWIPMST" Then
            TDDept1.Visible = True : TDDept2.Visible = True : TDDept3.Visible = True
            SetGVListPRWIP()
        ElseIf lblAppType.Text = "QL_PRSAWNMST" Then
            TDDept1.Visible = True : TDDept2.Visible = True : TDDept3.Visible = True
            SetGVListPRSAWN()
        ElseIf lblAppType.Text = "QL_TRNPORAWMST" Then
            SetGVListPORM()
        ElseIf lblAppType.Text = "QL_TRNPOGENMST" Then
            SetGVListPOGM()
        ElseIf lblAppType.Text = "QL_TRNPOSPMST" Then
            SetGVListPOSP()
        ElseIf lblAppType.Text = "QL_TRNPOFGMST" Then
            SetGVListPOItem()
        ElseIf lblAppType.Text = "QL_TRNPOASSETMST" Then
            SetGVListPOAssets()
        ElseIf lblAppType.Text = "QL_TRNPOWIPMST" Then
            SetGVListPOWIP()
        ElseIf lblAppType.Text = "QL_TRNPOSAWNMST" Then
            SetGVListPOSawn()
        ElseIf lblAppType.Text = "QL_TRNPOSERVICEMST" Then
            SetGVListPOService()
        ElseIf lblAppType.Text = "QL_TRNPOSUBCONMST" Then
            SetGVListPOSubcon()
        ElseIf lblAppType.Text = "QL_TRNRETURMST" Then
            SetGVListPretRM()
        ElseIf lblAppType.Text = "QL_TRNAPRAWMST" Then
            SetGVListAPRM()
        ElseIf lblAppType.Text = "QL_TRNAPGENMST" Then
            SetGVListAPGM()
        ElseIf lblAppType.Text = "QL_TRNAPFGMST" Then
            SetGVListAPFG()
        ElseIf lblAppType.Text = "QL_TRNAPWIPMST" Then
            SetGVListAPLOG()
        ElseIf lblAppType.Text = "QL_TRNSORAWMST" Then
            SetGVListSORM()
        ElseIf lblAppType.Text = "QL_TRNSOGENMST" Then
            SetGVListSOGM()
        ElseIf lblAppType.Text = "QL_TRNSOSPMST" Then
            SetGVListSOSP()
        ElseIf lblAppType.Text = "QL_TRNSOMST" Then
            LblinfoSO.Visible = True
            SetGVListSOFG()
        ElseIf lblAppType.Text = "QL_TRNSOASSETMST" Then
            SetGVListSOAssets()
        ElseIf lblAppType.Text = "QL_TRNDOMST" Then
            SetGVListShipmentRM()
        ElseIf lblAppType.Text = "QL_TRNSHIPMENTGENMST" Then
            SetGVListShipmentGM()
        ElseIf lblAppType.Text = "QL_TRNSHIPMENTSPMST" Then
            SetGVListShipmentSP()
        ElseIf lblAppType.Text = "QL_TRNSHIPMENTITEMMST" Then
            SetGVListShipmentItem()
        ElseIf lblAppType.Text = "QL_TRNSALESRETURMST" Then
            SetGVListSretRM()
        ElseIf lblAppType.Text = "QL_TRNSRETGENMST" Then
            SetGVListSretGM()
        ElseIf lblAppType.Text = "QL_TRNSRETSPMST" Then
            SetGVListSretSP()
        ElseIf lblAppType.Text = "QL_TRNSRETITEMMST" Then
            SetGVListSretItem()
        ElseIf lblAppType.Text = "QL_TRNSRETASSETMST" Then
            SetGVListSretAsset()
        ElseIf lblAppType.Text = "QL_TRNARRAWMST" Then
            SetGVListARRM()
        ElseIf lblAppType.Text = "QL_TRNARGENMST" Then
            SetGVListARGM()
        ElseIf lblAppType.Text = "QL_TRNARSPMST" Then
            SetGVListARSP()
        ElseIf lblAppType.Text = "QL_TRNARITEMMST" Then
            SetGVListARITEM()
        ElseIf lblAppType.Text = "QL_TRNARASSETMST" Then
            SetGVListARAsset()
        ElseIf lblAppType.Text = "QL_TRNBRTACARAMST" Then
            SetGVListBA()
        ElseIf lblAppType.Text = "QL_TRNBRTACARAPRODMST" Then
            SetGVListBAProduct()
        ElseIf lblAppType.Text = "QL_TRNPLANMST" Then
            SetGVListKIKPD()
        ElseIf lblAppType.Text = "QL_TRNSTOCKADJ" Then
			SetGVListMA()
			lblCaptValue.Visible = True : lblSeptValue.Visible = True : tbValue.Visible = True : tbValue.Text = "" : btnInsert.Visible = True
		Else
			lblCaptValue.Visible = False : lblSeptValue.Visible = False : tbValue.Visible = False : tbValue.Text = "" : btnInsert.Visible = False
		End If
    End Sub

    Private Sub ApprovalAction(ByVal sAction As String)
        If lblAppType.Text = "QL_PRRAWMST" Then
            If sAction = "Approved" Then
                UpdatePRRM(GeneratePRRMNo(), sAction)
            Else
                UpdatePRRM("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_PRGENMST" Then
            If sAction = "Approved" Then
                UpdatePRGM(GeneratePRGMNo(), sAction)
            Else
                UpdatePRGM("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_PRSPMST" Then
            If sAction = "Approved" Then
                UpdatePRSP(GeneratePRSPNo(), sAction)
            Else
                UpdatePRSP("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_PRITEMMST" Then
            If sAction = "Approved" Then
                UpdatePRItem(GeneratePRItemNo(), sAction)
            Else
                UpdatePRItem("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_PRASSETMST" Then
            If sAction = "Approved" Then
                UpdatePRAssets(GeneratePRAssetsNo(), sAction)
            Else
                UpdatePRAssets("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_PRWIPMST" Then
            If sAction = "Approved" Then
                UpdatePRWIP(GeneratePRWIPNo(), sAction)
            Else
                UpdatePRWIP("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_PRSAWNMST" Then
            If sAction = "Approved" Then
                UpdatePRSawn(GeneratePRSAWNNo(), sAction)
            Else
                UpdatePRSawn("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNPORAWMST" Then
            If sAction = "Approved" Then
                UpdatePORM(GeneratePORMNo(), sAction)
            Else
                UpdatePORM("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNPOGENMST" Then
            If sAction = "Approved" Then
                UpdatePOGM(GeneratePOGMNo(), sAction)
            Else
                UpdatePOGM("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNPOSPMST" Then
            If sAction = "Approved" Then
                UpdatePOSP(GeneratePOSPNo(), sAction)
            Else
                UpdatePOSP("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNPOFGMST" Then
            If sAction = "Approved" Then
                UpdatePOItem(GeneratePOItemNo(), sAction)
            Else
                UpdatePOItem("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNPOASSETMST" Then
            If sAction = "Approved" Then
                UpdatePOAssets(GeneratePOAssetsNo(), sAction)
            Else
                UpdatePOAssets("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNPOWIPMST" Then
            If sAction = "Approved" Then
                UpdatePOWIP(GeneratePOWIPNo(), sAction)
            Else
                UpdatePOWIP("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNPOSAWNMST" Then
            If sAction = "Approved" Then
                UpdatePOSAWN(GeneratePOSAWNNo(), sAction)
            Else
                UpdatePOSAWN("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNPOSERVICEMST" Then
            If sAction = "Approved" Then
                UpdatePOService(GeneratePOServiceNo(), sAction)
            Else
                UpdatePOService("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNPOSUBCONMST" Then
            If sAction = "Approved" Then
                UpdatePOSubcon(GeneratePOSubconNo(), sAction)
            Else
                UpdatePOSubcon("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNRETURMST" Then
            If sAction = "Approved" Then
                UpdatePretRM(GeneratePretRMNo(), sAction)
            Else
                UpdatePretRM("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNAPRAWMST" Then
            If sAction = "Approved" Then
                UpdateAPRM(Session("trnno"), sAction)
            Else
                UpdateAPRM("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNAPGENMST" Then
            If sAction = "Approved" Then
                UpdateAPGM(Session("trnno"), sAction)
            Else
                UpdateAPGM("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNAPFGMST" Then
            If sAction = "Approved" Then
                UpdateAPFG(Session("trnno"), sAction)
            Else
                UpdateAPFG("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNAPWIPMST" Then
            If sAction = "Approved" Then
                UpdateAPLOG(Session("trnno"), sAction)
            Else
                UpdateAPLOG("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNSORAWMST" Then
            If sAction = "Approved" Then
                UpdateSORM(GenerateSORMNo(), sAction)
            Else
                UpdateSORM("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNSOGENMST" Then
            If sAction = "Approved" Then
                UpdateSOGM(GenerateSOGMNo(), sAction)
            Else
                UpdateSOGM("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNSOSPMST" Then
            If sAction = "Approved" Then
                UpdateSOSP(GenerateSOSPNo(), sAction)
            Else
                UpdateSOSP("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNSOMST" Then
            If sAction = "Approved" Then
                UpdateSOFG(GenerateSOFGNo(), sAction)
            Else
                UpdateSOFG("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNSOASSETMST" Then
            If sAction = "Approved" Then
                UpdateSOAssets(GenerateSOAssetsNo(), sAction)
            Else
                UpdateSOAssets("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNDOMST" Then
            If sAction = "Approved" Then
                UpdateShipmentRM(GenerateShipmentRMNo(), GenerateJualMstNo(), sAction)
            Else
                UpdateShipmentRM("", "", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNSHIPMENTGENMST" Then
            If sAction = "Approved" Then
                UpdateShipmentGM(GenerateShipmentGMNo(), sAction)
            Else
                UpdateShipmentGM("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNSHIPMENTSPMST" Then
            If sAction = "Approved" Then
                UpdateShipmentSP(GenerateShipmentSPNo(), sAction)
            Else
                UpdateShipmentSP("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNSHIPMENTITEMMST" Then
            If sAction = "Approved" Then
                UpdateShipmentItem(GenerateShipmentItemNo(), sAction)
            Else
                UpdateShipmentItem("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNSALESRETURMST" Then
            If sAction = "Approved" Then
                UpdateSretRM(GenerateSretRMNo(), sAction)
            Else
                UpdateSretRM("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNSRETGENMST" Then
            If sAction = "Approved" Then
                UpdateSretGM(GenerateSretGMNo(), sAction)
            Else
                UpdateSretGM("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNSRETSPMST" Then
            If sAction = "Approved" Then
                UpdateSretSP(GenerateSretSPNo(), sAction)
            Else
                UpdateSretSP("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNSRETITEMMST" Then
            If sAction = "Approved" Then
                UpdateSretItem(GenerateSretItemNo(), sAction)
            Else
                UpdateSretItem("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNSRETASSETMST" Then
            If sAction = "Approved" Then
                UpdateSretAsset(GenerateSretAssetNo(), sAction)
            Else
                UpdateSretAsset("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNARRAWMST" Then
            If sAction = "Approved" Then
                UpdateARRM(GenerateARRMNo(), sAction)
            Else
                UpdateARRM("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNARGENMST" Then
            If sAction = "Approved" Then
                UpdateARGM(GenerateARGMNo(), sAction)
            Else
                UpdateARGM("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNARSPMST" Then
            If sAction = "Approved" Then
                UpdateARSP(GenerateARSPNo(), sAction)
            Else
                UpdateARSP("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNARITEMMST" Then
            If sAction = "Approved" Then
                UpdateARITEM(GenerateARITEMNo(), sAction)
            Else
                UpdateARITEM("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNARASSETMST" Then
            If sAction = "Approved" Then
                UpdateARAsset(GenerateARAssetNo(), sAction)
            Else
                UpdateARAsset("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNBRTACARAMST" Then
            If sAction = "Approved" Then
                UpdateBA(GenerateBANo(), sAction)
            Else
                UpdateBA("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNBRTACARAPRODMST" Then
            If sAction = "Approved" Then
                UpdateBAProduct(GenerateBAProductNo(), sAction)
            Else
                UpdateBAProduct("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNPLANMST" Then
            If sAction = "Approved" Then
                UpdateKIKPD(Session("trnno"), sAction)
            Else
                UpdateKIKPD("", sAction)
            End If
        ElseIf lblAppType.Text = "QL_TRNSTOCKADJ" Then
            If sAction = "Approved" Then
                UpdateMA(GenerateMANo(), sAction)
            Else
                UpdateMA("", sAction)
            End If
        End If

    End Sub

    Private Sub InitDDL()
        ''Fill DDL Request User
        'sSql = "SELECT profoid, profoid FROM QL_mstprof WHERE activeflag='ACTIVE'"
        'If Session("CompnyCode") <> CompnyCode Then
        '    sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        'End If
        'FillDDLWithAll(DDLUser, sSql)
        'Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)
        'Fill DDL Department
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLDept, sSql)
    End Sub

    Private Sub InitDDLUser(ByVal sName As String)
        'Fill DDL Request User
        Dim sWhere As String = ""
        If sName <> "" Then
            sName = sName.Trim.Replace("� ", "")
            sWhere &= " AND rold.formname LIKE '%" & Tchar(sName) & "%' "
        End If
        sSql = "SELECT DISTINCT prof.profoid, prof.profoid FROM QL_mstprof prof INNER JOIN QL_mstuserrole urol ON urol.profoid=prof.profoid WHERE prof.activeflag='ACTIVE' AND roleoid IN (SELECT DISTINCT rolm.roleoid FROM QL_mstrole rolm INNER JOIN QL_mstroledtl rold ON rold.roleoid=rolm.roleoid WHERE rold.formtype='TRANSACTION' " & sWhere & " )"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND prof.cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDLWithAll(DDLUser, sSql)
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Other\WaitingActionNew.aspx")
        End If
        Page.Title = CompnyName & " - Waiting Action"
        Session("directappvalue") = Request.QueryString("formvalue")
        Session("directapptext") = Request.QueryString("formtext")

        btnApprove.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to APPROVE this Transaction ?');")
        btnRevise.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to REVISE this Transaction ?');")
        btnReject.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to REJECT this Transaction ?');")

        If Not Page.IsPostBack Then
            InitDDL()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitMenuApp()
            If Not Session("directappvalue") Is Nothing And Session("directappvalue") <> "" And Not Session("directapptext") Is Nothing And Session("directapptext") <> "" Then
                lblAppType.Text = Session("directappvalue")
                lblTitleList.Text = Session("directapptext") & " List :"
                lblTitleData.Text = Session("directapptext") & " Data :"
                InitDDLUser(Session("directapptext"))
                SetDataList()
            End If
        End If
    End Sub

    Protected Sub lkbBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbBack.Click
        MultiView1.SetActiveView(View1)
        InitMenuApp()
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False : beMsgBox.Visible = False : mpeMsgbox.Hide()
    End Sub

    Protected Sub lkbMenuApp_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblAppType.Text = sender.CommandArgument
        lblTitleList.Text = sender.Text & " List :"
        lblTitleData.Text = sender.Text & " Data :"
        InitDDLUser(sender.Text)
        SetDataList()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        If Not Session("TblGVList") Is Nothing Then
            Dim dt As DataTable = Session("TblGVList")
            Dim dv As DataView = dt.DefaultView
            Dim sFilter As String = ""
            If cbPeriod.Checked Then
                If IsValidPeriod() Then
                    sFilter &= "trndate>='" & FilterPeriod1.Text & "' AND trndate<='" & FilterPeriod2.Text & "'"
                Else
                    Exit Sub
                End If
            End If
            If DDLUser.SelectedValue <> "All" Then
                sFilter &= IIf(sFilter = "", "", " AND ") & "requestuser='" & DDLUser.SelectedValue & "'"
            End If
            If DDLBusUnit.SelectedValue <> "" Then
                sFilter &= IIf(sFilter = "", "", " AND ") & "cmpcode='" & DDLBusUnit.SelectedValue & "'"
            End If
            If lblTitleList.Text.Contains("PR") Then
                If DDLBusUnit.SelectedValue <> "" Then
                    sFilter &= IIf(sFilter = "", "", " AND ") & "deptoid='" & DDLDept.SelectedValue & "'"
                End If
            End If
            If FilterDraftNo.Text <> "" Then
                Dim SFilterDraftNo() As String = Split(FilterDraftNo.Text, ",")
                sFilter &= IIf(sFilter = "", "", " AND ") & " ("
                For c1 As Integer = 0 To SFilterDraftNo.Length - 1
                    sFilter &= " trnoid = " & ToDouble(SFilterDraftNo(c1))
                    If c1 < SFilterDraftNo.Length - 1 Then
                        sFilter &= " OR"
                    End If
                Next
                sFilter &= ")"
            End If
            dv.RowFilter = sFilter
            gvList.DataSource = dv.ToTable()
            gvList.DataBind()
            dv.RowFilter = ""
        End If
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        cbPeriod.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        DDLUser.SelectedIndex = -1
        DDLBusUnit.SelectedIndex = -1
        DDLDept.SelectedIndex = -1
        FilterDraftNo.Text = ""
        SetDataList()
    End Sub

    Protected Sub gvList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvList.SelectedIndexChanged
        Dim bFlag As Boolean

        btnApprove.Visible = True : btnReject.Visible = True : btnRevise.Visible = True
        MultiView1.SetActiveView(ViewData)
        gvData.DataSource = Nothing
        gvData.DataBind()
        gvData.Columns.Clear()
        Session("trnoid") = gvList.SelectedDataKey.Item("trnoid").ToString
        Session("trndate") = gvList.SelectedDataKey.Item("trndate").ToString
        Session("approvaloid") = gvList.SelectedDataKey.Item("approvaloid").ToString
        sSql = "SELECT COUNT(*) FROM QL_approval WHERE approvaloid=" & Session("approvaloid") & " AND statusrequest='New' AND event='In Approval'"
        If CheckDataExists(sSql) = False Then
            showMessage("Your selected data has been used by another user. Please select another data!", CompnyName & " - WARNING", 2)
            MultiView1.SetActiveView(ViewList)
            Exit Sub
        End If
        AppCmpCode.Text = gvList.SelectedDataKey.Item("cmpcode").ToString
        lblBusUnit.Text = SetBusUnit(AppCmpCode.Text)
        Dim arColNameList() As String = {"datacaption", "datasept", "datavalue"}
        Dim arColTypeList() As String = {"System.String", "System.String", "System.String"}
        Dim dtDataHeader As DataTable = SetTableDetail("tblDataHeader", arColNameList, arColTypeList)
        If lblAppType.Text = "QL_PRRAWMST" Then
            SetGVDataPRRM(dtDataHeader)
        ElseIf lblAppType.Text = "QL_PRGENMST" Then
            SetGVDataPRGM(dtDataHeader)
        ElseIf lblAppType.Text = "QL_PRSPMST" Then
            SetGVDataPRSP(dtDataHeader)
        ElseIf lblAppType.Text = "QL_PRITEMMST" Then
            SetGVDataPRItem(dtDataHeader)
        ElseIf lblAppType.Text = "QL_PRASSETMST" Then
            SetGVDataPRAssets(dtDataHeader)
        ElseIf lblAppType.Text = "QL_PRWIPMST" Then
            SetGVDataPRWIP(dtDataHeader)
        ElseIf lblAppType.Text = "QL_PRSAWNMST" Then
            SetGVDataPRSAWN(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNPORAWMST" Then
            SetGVDataPORM(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNPOGENMST" Then
            SetGVDataPOGM(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNPOSPMST" Then
            SetGVDataPOSP(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNPOFGMST" Then
            SetGVDataPOItem(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNPOASSETMST" Then
            SetGVDataPOAssets(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNPOWIPMST" Then
            SetGVDataPOWIP(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNPOSAWNMST" Then
            SetGVDataPOSAWN(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNPOSERVICEMST" Then
            SetGVDataPOService(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNPOSUBCONMST" Then
            SetGVDataPOSubcon(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNRETURMST" Then
            SetGVDataPretRM(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNAPRAWMST" Then
            SetGVDataAPRM(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNAPGENMST" Then
            SetGVDataAPGM(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNAPFGMST" Then
            SetGVDataAPFG(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNAPWIPMST" Then
            SetGVDataAPLOG(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNSORAWMST" Then
            SetGVDataSORM(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNSOGENMST" Then
            SetGVDataSOGM(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNSOSPMST" Then
            SetGVDataSOSP(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNSOMST" Then
            SetGVDataSOFG(dtDataHeader)
            'lblCaptReturn.Visible = True : lblSeptReturn.Visible = True : DDLReturnType.Visible = True : DDLReturnType.Items.Clear()
            'lblCaptReturn.Text = "Retur Type"
            'DDLReturnType.Items.Add("Retur Ganti Barang") : DDLReturnType.Items(DDLReturnType.Items.Count - 1).Value = "Retur Ganti Barang"
            'DDLReturnType.Items.Add("Retur Tidak Ganti Barang") : DDLReturnType.Items(DDLReturnType.Items.Count - 1).Value = "Retur Tidak Ganti Barang"
        ElseIf lblAppType.Text = "QL_TRNSOASSETMST" Then
            SetGVDataSOAssets(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNDOMST" Then
            SetGVDataShipmentRM(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNSHIPMENTGENMST" Then
            SetGVDataShipmentGM(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNSHIPMENTSPMST" Then
            SetGVDataShipmentSP(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNSHIPMENTITEMMST" Then
            SetGVDataShipmentItem(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNARRAWMST" Then
            SetGVDataARRM(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNARGENMST" Then
            SetGVDataARGM(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNARSPMST" Then
            SetGVDataarsp(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNARITEMMST" Then
            SetGVDataARITEM(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNARASSETMST" Then
            SetGVDataARAsset(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNSALESRETURMST" Then
            SetGVDataSretRM(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNSRETGENMST" Then
            SetGVDataSretGM(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNSRETSPMST" Then
            SetGVDataSretSP(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNSRETITEMMST" Then
            SetGVDataSretItem(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNSRETASSETMST" Then
            SetGVDataSretAsset(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNBRTACARAMST" Then
            SetGVDataBA(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNBRTACARAPRODMST" Then
            SetGVDataBAProduct(dtDataHeader)
        ElseIf lblAppType.Text = "QL_TRNPLANMST" Then
            SetGVDataKIKPD(dtDataHeader)
            btnRevise.Visible = False
        ElseIf lblAppType.Text = "QL_TRNSTOCKADJ" Then
            SetGVDataMA(dtDataHeader)
            lblCaptReturn.Visible = True
            lblCaptReturn.Text = "Reason Type" : lblSeptReturn.Visible = True
            DDLReturnType.Visible = True : DDLReturnType.Items.Clear()
            FillDDL(DDLReturnType, "SELECT genoid, gendesc FROM QL_mstgen WHERE gengroup='ADJUSMENT REASON' AND activeflag='ACTIVE' AND cmpcode='" & CompnyCode & "'")
        Else
            lblCaptReturn.Visible = False : lblSeptReturn.Visible = False : DDLReturnType.Visible = False : DDLReturnType.Items.Clear()
            lblCaptReturn.Text = "Retur Type"
            DDLReturnType.Items.Add("Retur Ganti Barang") : DDLReturnType.Items(DDLReturnType.Items.Count - 1).Value = "Retur Ganti Barang"
            DDLReturnType.Items.Add("Retur Tidak Ganti Barang") : DDLReturnType.Items(DDLReturnType.Items.Count - 1).Value = "Retur Tidak Ganti Barang"
        End If
    End Sub

    Protected Sub gvData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvData.RowDataBound
        If lblAppType.Text = "QL_TRNSOMST" Then
            If e.Row.RowType = DataControlRowType.DataRow Then
                If ToDouble(e.Row.Cells(14).Text) < ToDouble(e.Row.Cells(15).Text) Then
                    For C1 As Int16 = 1 To e.Row.Cells.Count - 1
                        e.Row.Cells(C1).ForeColor = Drawing.Color.Red
                    Next
                End If
            End If
        End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            For C1 As Int16 = 1 To e.Row.Cells.Count - 1
                If ToDouble(e.Row.Cells(C1).Text) > 0 Then
                    e.Row.Cells(C1).Text = ToMaskEdit(ToDouble(e.Row.Cells(C1).Text), 4)
                End If
            Next
        End If
	End Sub

	Protected Sub gvDataAdj_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDataAdj.RowDataBound
		If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 2)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 2)
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(8).Controls
			For Each myControl As System.Web.UI.Control In cc
				If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
					If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
						CType(myControl, System.Web.UI.WebControls.TextBox).Text = 0
					Else
						CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 2)
					End If
				End If
			Next
		End If
	End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnApprove.Click
        sSql = "SELECT COUNT(*) FROM QL_approval WHERE approvaloid=" & Session("approvaloid") & " AND statusrequest='New' AND event='In Approval'"
        If CheckDataExists(sSql) = False Then
            showMessage("This data has been confirmed by another user. Please select another data!", CompnyName & " - WARNING", 2)
            Exit Sub
        End If
        ApprovalAction("Approved")
    End Sub

    Protected Sub btnRevise_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnRevise.Click
        sSql = "SELECT COUNT(*) FROM QL_approval WHERE approvaloid=" & Session("approvaloid") & " AND statusrequest='New' AND event='In Approval'"
        If CheckDataExists(sSql) = False Then
            showMessage("This data has been confirmed by another user. Please select another data!", CompnyName & " - WARNING", 2)
            Exit Sub
        End If
        ApprovalAction("Revised")
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnReject.Click
        sSql = "SELECT COUNT(*) FROM QL_approval WHERE approvaloid=" & Session("approvaloid") & " AND statusrequest='New' AND event='In Approval'"
        If CheckDataExists(sSql) = False Then
            showMessage("This data has been confirmed by another user. Please select another data!", CompnyName & " - WARNING", 2)
            Exit Sub
        End If
        ApprovalAction("Rejected")
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBack.Click
        SetDataList()
        If gvList.Rows.Count = 0 Then
            Response.Redirect("~\Other\WaitingActionNew.aspx?awal=true")
        Else
            gvList.SelectedIndex = -1
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        'Fill DDL Department
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLDept, sSql)
	End Sub

	Protected Sub btnInsert_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnInsert.Click
		If Session("TblDtlMSA") IsNot Nothing Then
			Dim dt As DataTable = Session("TblDtlMSA")
			For C1 As Integer = 0 To dt.Rows.Count - 1
				dt.Rows(C1)("stockvalueidr") = ToDouble(tbValue.Text)
			Next
			dt.AcceptChanges()
			Session("TblDtlMSA") = dt
			gvDataAdj.DataSource = Session("TblDtlMSA")
			gvDataAdj.DataBind()
		End If
	End Sub

	Protected Sub gvDataAdj_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDataAdj.PageIndexChanging
        If Session("TblDtlMSA") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtlMSA")
            Dim dv As DataView = dt.DefaultView
            Dim dValue As Double = 0
            For C1 As Integer = 0 To gvDataAdj.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvDataAdj.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(8).Controls
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                            dv.RowFilter = "oid=" & CType(myControl, System.Web.UI.WebControls.TextBox).ToolTip
                            dValue = ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text)
                            dv(0)("stockvalueidr") = dValue
                            dv.RowFilter = ""
                        End If
                    Next
                End If
            Next
                    dt.AcceptChanges()
                    Session("TblDtlMSA") = dt
                    gvDataAdj.DataSource = Session("TblDtlMSA")
                    gvDataAdj.DataBind()
                End If

		gvDataAdj.PageIndex = e.NewPageIndex
		gvDataAdj.DataSource = Session("TblDtlMSA")
		gvDataAdj.DataBind()
	End Sub
#End Region

#Region "PR Raw Material Approval"
    Private Sub SetGVListPRRM()
        Dim arHeader() As String = {"Draft No.", "PR Date", "PR Dept.", "PR Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "prrawdept", "prrawmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "prrawdept", "prrawmstnote", "upduser", "updtime"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT pr.cmpcode, pr.prrawmstoid AS trnoid, pr.prrawno, CONVERT(VARCHAR(10), pr.prrawdate, 101) AS trndate, d.deptoid, d.deptname AS prrawdept, pr.prrawmstnote, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, pr.upduser, pr.updtime FROM QL_prrawmst pr INNER JOIN QL_mstdept d ON pr.deptoid=d.deptoid INNER JOIN QL_approval ap ON pr.prrawmstoid=ap.oid AND ap.statusrequest='New' WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_prrawmst' AND pr.prrawmststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_prrawmst")
    End Sub

    Private Sub SetGVDataPRRM(ByVal dtDataHeader As DataTable)
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "PR Date", "PR Dept.", "PR Note", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "prrawdept", "prrawmstnote", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arHeader() As String = {"No.", "Mat. Code", "Mat. Desc", "Arr. Date. Req", "Quantity", "Unit", "Last Price ", "Last Price IDR", "Last PO No", "Last PO Date", "Note"}
        Dim arField() As String = {"prrawdtlseq", "matrawcode", "matrawshortdesc", "prrawarrdatereq", "prrawqty", "unit", "lastprice", "lastpriceidr", "lasttransno", "lasttransdate", "prrawdtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT prd.prrawdtlseq, m.matrawcode, m.matrawlongdesc AS matrawshortdesc, prd.prrawqty, g.gendesc AS unit, ISNULL((SELECT TOP 1 lastpurchaseprice FROM QL_mstmatrawprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.matrawoid=m.matrawoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) AS lastprice, ISNULL((SELECT TOP 1 lastpurchaseprice * ratetoidr FROM QL_mstmatrawprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.matrawoid=m.matrawoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) AS lastpriceidr, ISNULL((SELECT TOP 1 lasttransno FROM QL_mstmatrawprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.matrawoid=m.matrawoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC),  '') AS lasttransno, ISNULL((SELECT TOP 1 CONVERT(VARCHAR(10),lasttransdate, 101) FROM QL_mstmatrawprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.matrawoid=m.matrawoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 1/1/1900) AS lasttransdate, CONVERT(VARCHAR(10), prd.prrawarrdatereq, 101) AS prrawarrdatereq, prd.prrawdtlnote FROM QL_prrawmst pr INNER JOIN QL_prrawdtl prd ON pr.prrawmstoid=prd.prrawmstoid INNER JOIN QL_mstmatraw m ON prd.matrawoid=m.matrawoid INNER JOIN QL_mstgen g ON prd.prrawunitoid=g.genoid WHERE pr.prrawmstoid=" & Session("trnoid") & " ORDER BY m.matrawcode"
        FillGV(gvData, sSql, "QL_prrawdtl")
    End Sub

    Private Function GeneratePRRMNo() As String
        Dim sNo As String = "PRRM-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(prrawno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_prrawmst WHERE cmpcode='" & AppCmpCode.Text & "' AND prrawno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdatePRRM(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select PR Raw Material Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select PR Raw Material Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update PR
            sSql = "UPDATE QL_prrawmst SET prrawno='" & sNo & "', prrawmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE prrawmststatus='In Approval' AND prrawmstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_prrawmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "PR General Material Approval"
    Private Sub SetGVListPRGM()
        Dim arHeader() As String = {"Draft No.", "PR Date", "PR Dept.", "PR Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "prgendept", "prgenmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "prgendept", "prgenmstnote", "upduser", "updtime"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT pr.cmpcode, pr.prgenmstoid AS trnoid, pr.prgenno, CONVERT(VARCHAR(10), pr.prgendate, 101) AS trndate, d.deptoid, d.deptname AS prgendept, pr.prgenmstnote, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, pr.upduser, pr.updtime FROM QL_prgenmst pr INNER JOIN QL_mstdept d ON pr.deptoid=d.deptoid INNER JOIN QL_approval ap ON pr.prgenmstoid=ap.oid AND ap.statusrequest='New' WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_prgenmst' AND pr.prgenmststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_prgenmst")
    End Sub

    Private Sub SetGVDataPRGM(ByVal dtDataHeader As DataTable)
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "PR Date", "PR Dept.", "PR Note", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "prgendept", "prgenmstnote", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arHeader() As String = {"No.", "Mat. Code", "Mat. Desc", "Arr. Date. Req", "Quantity", "Unit", "Last Price ", "Last Price IDR", "Last PO No", "Last PO Date", "Note"}
        Dim arField() As String = {"prgendtlseq", "matgencode", "matgenshortdesc", "prgenarrdatereq", "prgenqty", "unit", "lastprice", "lastpriceidr", "lasttransno", "lasttransdate", "prgendtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT prd.prgendtlseq, m.matgencode, m.matgenlongdesc AS matgenshortdesc, prd.prgenqty, g.gendesc AS unit, ISNULL((SELECT TOP 1 lastpurchaseprice FROM QL_mstmatgenprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.matgenoid=m.matgenoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) AS lastprice, ISNULL((SELECT TOP 1 lastpurchaseprice * ratetoidr FROM QL_mstmatgenprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.matgenoid=m.matgenoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) AS lastpriceidr, ISNULL((SELECT TOP 1 lasttransno FROM QL_mstmatgenprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.matgenoid=m.matgenoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC),  '') AS lasttransno, ISNULL((SELECT TOP 1 CONVERT(VARCHAR(10),lasttransdate, 101) FROM QL_mstmatgenprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.matgenoid=m.matgenoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 1/1/1900) AS lasttransdate, CONVERT(VARCHAR(10), prd.prgenarrdatereq, 101) AS prgenarrdatereq, prd.prgendtlnote FROM QL_prgenmst pr INNER JOIN QL_prgendtl prd ON pr.prgenmstoid=prd.prgenmstoid INNER JOIN QL_mstmatgen m ON prd.matgenoid=m.matgenoid INNER JOIN QL_mstgen g ON prd.prgenunitoid=g.genoid WHERE pr.prgenmstoid=" & Session("trnoid") & " ORDER BY m.matgencode"
        FillGV(gvData, sSql, "QL_prgendtl")
    End Sub

    Private Function GeneratePRGMNo() As String
        Dim sNo As String = "PRGM-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(prgenno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_prgenmst WHERE cmpcode='" & AppCmpCode.Text & "' AND prgenno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdatePRGM(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select PR General Material Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select PR General Material Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update PR
            sSql = "UPDATE QL_prgenmst SET prgenno='" & sNo & "', prgenmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE prgenmststatus='In Approval' AND prgenmstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_prgenmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "PR Spare Part Approval"
    Private Sub SetGVListPRSP()
        Dim arHeader() As String = {"Draft No.", "PR Date", "PR Dept.", "PR Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "prspdept", "prspmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "prspdept", "prspmstnote", "upduser", "updtime"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT pr.cmpcode, pr.prspmstoid AS trnoid, pr.prspno, CONVERT(VARCHAR(10), pr.prspdate, 101) AS trndate, d.deptoid, d.deptname AS prspdept, pr.prspmstnote, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, pr.upduser, pr.updtime FROM QL_prspmst pr INNER JOIN QL_mstdept d ON pr.deptoid=d.deptoid INNER JOIN QL_approval ap ON pr.prspmstoid=ap.oid AND ap.statusrequest='New' WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_prspmst' AND pr.prspmststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_prspmst")
    End Sub

    Private Sub SetGVDataPRSP(ByVal dtDataHeader As DataTable)
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "PR Date", "PR Dept.", "PR Note", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "prspdept", "prspmstnote", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arHeader() As String = {"No.", "Mat. Code", "Mat. Desc", "Arr. Date. Req", "Quantity", "Unit", "Last Price ", "Last Price IDR", "Last PO No", "Last PO Date", "Note"}
        Dim arField() As String = {"prspdtlseq", "sparepartcode", "sparepartshortdesc", "prsparrdatereq", "prspqty", "unit", "lastprice", "lastpriceidr", "lasttransno", "lasttransdate", "prspdtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT prd.prspdtlseq, m.sparepartcode, m.sparepartlongdesc AS sparepartshortdesc, prd.prspqty, g.gendesc AS unit, ISNULL((SELECT TOP 1 lastpurchaseprice FROM QL_mstsparepartprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.sparepartoid=m.sparepartoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) AS lastprice, ISNULL((SELECT TOP 1 lastpurchaseprice * ratetoidr FROM QL_mstsparepartprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.sparepartoid=m.sparepartoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) AS lastpriceidr, ISNULL((SELECT TOP 1 lasttransno FROM QL_mstsparepartprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.sparepartoid=m.sparepartoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC),  '') AS lasttransno, ISNULL((SELECT TOP 1 CONVERT(VARCHAR(10),lasttransdate, 101) FROM QL_mstsparepartprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.sparepartoid=m.sparepartoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 1/1/1900) AS lasttransdate, CONVERT(VARCHAR(10), prd.prsparrdatereq, 101) AS prsparrdatereq, prd.prspdtlnote FROM QL_prspmst pr INNER JOIN QL_prspdtl prd ON pr.prspmstoid=prd.prspmstoid INNER JOIN QL_mstsparepart m ON prd.sparepartoid=m.sparepartoid INNER JOIN QL_mstgen g ON prd.prspunitoid=g.genoid WHERE pr.prspmstoid=" & Session("trnoid") & " ORDER BY m.sparepartcode"
        FillGV(gvData, sSql, "QL_prspdtl")
    End Sub

    Private Function GeneratePRSPNo() As String
        Dim sNo As String = "PRSP-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(prspno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_prspmst WHERE cmpcode='" & AppCmpCode.Text & "' AND prspno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdatePRSP(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select PR Spare Part Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select PR Spare Part Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update PR
            sSql = "UPDATE QL_prspmst SET prspno='" & sNo & "', prspmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE prspmststatus='In Approval' AND prspmstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_prspmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "PR Finish Good Approval"
    Private Sub SetGVListPRItem()
        Dim arHeader() As String = {"Draft No.", "PR Date", "PR Dept.", "PR Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "pritemdept", "pritemmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "pritemdept", "pritemmstnote", "upduser", "updtime"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT pr.cmpcode, pr.pritemmstoid AS trnoid, pr.pritemno, CONVERT(VARCHAR(10), pr.pritemdate, 101) AS trndate, d.deptoid, d.deptname AS pritemdept, pr.pritemmstnote, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, pr.upduser, pr.updtime FROM QL_pritemmst pr INNER JOIN QL_mstdept d ON pr.deptoid=d.deptoid INNER JOIN QL_approval ap ON pr.pritemmstoid=ap.oid AND ap.statusrequest='New' WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_pritemmst' AND pr.pritemmststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_pritemmst")
    End Sub

    Private Sub SetGVDataPRItem(ByVal dtDataHeader As DataTable)
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "PR Date", "PR Dept.", "PR Note", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "pritemdept", "pritemmstnote", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arHeader() As String = {"No.", "Mat. Code", "Mat. Desc", "Arr. Date. Req", "Quantity", "Unit", "Last Price ", "Last Price IDR", "Last PO No", "Last PO Date", "Note"}
        Dim arField() As String = {"pritemdtlseq", "itemcode", "itemshortdesc", "pritemarrdatereq", "pritemqty", "unit", "lastprice", "lastpriceidr", "lasttransno", "lasttransdate", "pritemdtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT prd.pritemdtlseq, m.itemcode, m.itemlongdesc AS itemshortdesc, prd.pritemqty, g.gendesc AS unit, ISNULL((SELECT TOP 1 lastpurchaseprice FROM QL_mstitemprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.itemoid=m.itemoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) AS lastprice, ISNULL((SELECT TOP 1 lastpurchaseprice * ratetoidr FROM QL_mstitemprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.itemoid=m.itemoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) AS lastpriceidr, ISNULL((SELECT TOP 1 lasttransno FROM QL_mstitemprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.itemoid=m.itemoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC),  '') AS lasttransno, ISNULL((SELECT TOP 1 CONVERT(VARCHAR(10),lasttransdate, 101) FROM QL_mstitemprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.itemoid=m.itemoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 1/1/1900) AS lasttransdate, CONVERT(VARCHAR(10), prd.pritemarrdatereq, 101) AS pritemarrdatereq, prd.pritemdtlnote FROM QL_pritemmst pr INNER JOIN QL_pritemdtl prd ON pr.pritemmstoid=prd.pritemmstoid INNER JOIN QL_mstitem m ON prd.itemoid=m.itemoid INNER JOIN QL_mstgen g ON prd.pritemunitoid=g.genoid WHERE pr.pritemmstoid=" & Session("trnoid") & " ORDER BY m.itemcode"
        FillGV(gvData, sSql, "QL_pritemdtl")
    End Sub

    Private Function GeneratePRItemNo() As String
        Dim sNo As String = "PRFG-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(pritemno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_pritemmst WHERE cmpcode='" & AppCmpCode.Text & "' AND pritemno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdatePRItem(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select PR Finish Good Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select PR Finish Good Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update PR
            sSql = "UPDATE QL_pritemmst SET pritemno='" & sNo & "', pritemmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE pritemmststatus='In Approval' AND pritemmstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_pritemmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "PR Assets Approval"
    Private Sub SetGVListPRAssets()
        Dim arHeader() As String = {"Draft No.", "PR Date", "PR Dept.", "PR Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "prassetdept", "prassetmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "prassetdept", "prassetmstnote", "upduser", "updtime"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT pr.cmpcode, pr.prassetmstoid AS trnoid, pr.prassetno, CONVERT(VARCHAR(10), pr.prassetdate, 101) AS trndate, d.deptoid, d.deptname AS prassetdept, pr.prassetmstnote, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, pr.upduser, pr.updtime FROM QL_prassetmst pr INNER JOIN QL_mstdept d ON pr.deptoid=d.deptoid INNER JOIN QL_approval ap ON pr.prassetmstoid=ap.oid AND ap.statusrequest='New' WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_prassetmst' AND pr.prassetmststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_prassetmst")
    End Sub

    Private Sub SetGVDataPRAssets(ByVal dtDataHeader As DataTable)
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "PR Date", "PR Dept.", "PR Note", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "prassetdept", "prassetmstnote", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arHeader() As String = {"No.", "Type", "Mat. Code", "Mat. Desc", "Arr. Date. Req", "Quantity", "Unit", "Last Price ", "Last Price IDR", "Last PO No", "Last PO Date", "Note"}
        Dim arField() As String = {"prassetdtlseq", "prassetreftype", "prassetrefcode", "prassetrefshortdesc", "prassetarrdatereq", "prassetqty", "unit", "lastprice", "lastpriceidr", "lasttransno", "lasttransdate", "prassetdtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT prd.prassetdtlseq, prd.prassetreftype, (CASE prd.prassetreftype WHEN 'General' THEN (SELECT x.matgencode FROM QL_mstmatgen x WHERE x.matgenoid=prd.prassetrefoid) WHEN 'Spare Part' THEN (SELECT x.sparepartcode FROM QL_mstsparepart x WHERE x.sparepartoid=prd.prassetrefoid) ELSE '' END) AS prassetrefcode, (CASE prd.prassetreftype WHEN 'General' THEN (SELECT x.matgenlongdesc FROM QL_mstmatgen x WHERE x.matgenoid=prd.prassetrefoid) WHEN 'Spare Part' THEN (SELECT x.sparepartlongdesc FROM QL_mstsparepart x WHERE x.sparepartoid=prd.prassetrefoid) ELSE '' END) AS prassetrefshortdesc, CONVERT(VARCHAR(10), prd.prassetarrdatereq, 101) AS prassetarrdatereq, prd.prassetqty, g.gendesc AS unit, (CASE prd.prassetreftype WHEN 'General' THEN ISNULL((SELECT TOP 1 lastpurchaseprice FROM QL_mstmatgenprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.matgenoid=prd.prassetrefoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) ELSE ISNULL((SELECT TOP 1 lastpurchaseprice FROM QL_mstsparepartprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.sparepartoid=prd.prassetrefoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) END) AS lastprice, (CASE prd.prassetreftype WHEN 'General' THEN ISNULL((SELECT TOP 1 lastpurchaseprice * ratetoidr FROM QL_mstmatgenprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.matgenoid=prd.prassetrefoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) ELSE ISNULL((SELECT TOP 1 lastpurchaseprice * ratetoidr FROM QL_mstsparepartprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.sparepartoid=prd.prassetrefoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) END) AS lastpriceidr, (CASE prd.prassetreftype WHEN 'General' THEN ISNULL((SELECT TOP 1 lasttransno FROM QL_mstmatgenprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.matgenoid=prd.prassetrefoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), '') ELSE ISNULL((SELECT TOP 1 lasttransno FROM QL_mstsparepartprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.sparepartoid=prd.prassetrefoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), '') END) AS lasttransno, (CASE prd.prassetreftype WHEN 'General' THEN ISNULL((SELECT TOP 1 CONVERT(VARCHAR(10),lasttransdate, 101) FROM QL_mstmatgenprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.matgenoid=prd.prassetrefoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 1/1/1900) ELSE ISNULL((SELECT TOP 1 CONVERT(VARCHAR(10),lasttransdate, 101) FROM QL_mstsparepartprice mp WHERE mp.cmpcode=prd.cmpcode AND mp.sparepartoid=prd.prassetrefoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 1/1/1900) END) AS lasttransdate, prd.prassetdtlnote FROM QL_prassetmst pr INNER JOIN QL_prassetdtl prd ON pr.prassetmstoid=prd.prassetmstoid INNER JOIN QL_mstgen g ON prd.prassetunitoid=g.genoid WHERE pr.prassetmstoid=" & Session("trnoid") & " ORDER BY prassetrefcode"
        FillGV(gvData, sSql, "QL_prassetdtl")
    End Sub

    Private Function GeneratePRAssetsNo() As String
        Dim sNo As String = "PRFA-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(prassetno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_prassetmst WHERE cmpcode='" & AppCmpCode.Text & "' AND prassetno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdatePRAssets(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select PR Fixed Assets Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select PR Fixed Assets Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update PR
            sSql = "UPDATE QL_prassetmst SET prassetno='" & sNo & "', prassetmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE prassetmststatus='In Approval' AND prassetmstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_prassetmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "PR Log Approval"
    Private Sub SetGVListPRWIP()
        Dim arHeader() As String = {"Draft No.", "PR Date", "PR Dept.", "PR Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "prwipdept", "prwipmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "prwipdept", "prwipmstnote", "upduser", "updtime"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT pr.cmpcode, pr.prwipmstoid AS trnoid, pr.prwipno, CONVERT(VARCHAR(10), pr.prwipdate, 101) AS trndate, d.deptoid, d.deptname AS prwipdept, pr.prwipmstnote, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, pr.upduser, pr.updtime FROM QL_prwipmst pr INNER JOIN QL_mstdept d ON pr.deptoid=d.deptoid INNER JOIN QL_approval ap ON pr.prwipmstoid=ap.oid AND ap.statusrequest='New' WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_prwipmst' AND pr.prwipmststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_prwipmst")
    End Sub

    Private Sub SetGVDataPRWIP(ByVal dtDataHeader As DataTable)
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "PR Date", "PR Dept.", "PR Note", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "prwipdept", "prwipmstnote", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arHeader() As String = {"No.", "Code", "Description", "Arr. Date. Req", "Quantity", "Unit", "Last Price ", "Last Price IDR", "Last PO No", "Last PO Date", "Note"}
        Dim arField() As String = {"prwipdtlseq", "matwipcode", "matwiplongdesc", "prwiparrdatereq", "prwipqty", "unit", "lastprice", "lastpriceidr", "lasttransno", "lasttransdate", "prwipdtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT prd.prwipdtlseq, (c1.cat1code + '.' + c2.cat2code + '.' + c3.cat3code) AS matwipcode, (RTRIM((CASE WHEN (LTRIM(c1.cat1shortdesc)='None' OR LTRIM(c1.cat1shortdesc)='') THEN '' ELSE c1.cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(c2.cat2shortdesc)='None' OR LTRIM(c2.cat2shortdesc)='') THEN '' ELSE c2.cat2shortdesc + ' ' END) + (CASE WHEN (LTRIM(c3.cat3shortdesc)='None' OR LTRIM(c3.cat3shortdesc)='') THEN '' ELSE c3.cat3shortdesc + ' ' END))) AS matwiplongdesc, prd.prwipqty, g.gendesc AS unit, ISNULL((SELECT TOP 1 lastpurchaseprice FROM QL_mstcat3price mp WHERE mp.cmpcode=prd.cmpcode AND mp.cat3oid=c3.cat3oid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) AS lastprice, ISNULL((SELECT TOP 1 lastpurchaseprice * ratetoidr FROM QL_mstcat3price mp WHERE mp.cmpcode=prd.cmpcode AND mp.cat3oid=c3.cat3oid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) AS lastpriceidr, ISNULL((SELECT TOP 1 lasttransno FROM QL_mstcat3price mp WHERE mp.cmpcode=prd.cmpcode AND mp.cat3oid=c3.cat3oid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), '') AS lasttransno, ISNULL((SELECT TOP 1 CONVERT(VARCHAR(10),lasttransdate, 101) FROM QL_mstcat3price mp WHERE mp.cmpcode=prd.cmpcode AND mp.cat3oid=c3.cat3oid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 1/1/1900) AS lasttransdate, CONVERT(VARCHAR(10), prd.prwiparrdatereq, 101) AS prwiparrdatereq, prd.prwipdtlnote FROM QL_prwipmst pr INNER JOIN QL_prwipdtl prd ON pr.prwipmstoid=prd.prwipmstoid INNER JOIN QL_mstcat3 c3 ON c3.cat3oid=prd.matwipoid INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid AND c2.cat1oid=c3.cat1oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c3.cmpcode AND c1.cat1oid=c3.cat1oid INNER JOIN QL_mstgen g ON prd.prwipunitoid=g.genoid WHERE pr.prwipmstoid=" & Session("trnoid") & " ORDER BY matwipcode"
        FillGV(gvData, sSql, "QL_prwipdtl")
    End Sub

    Private Function GeneratePRWIPNo() As String
        Dim sNo As String = "PRLOG-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(prwipno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_prwipmst WHERE cmpcode='" & AppCmpCode.Text & "' AND prwipno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdatePRWIP(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select PR Log Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select PR Log Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update PR
            sSql = "UPDATE QL_prwipmst SET prwipno='" & sNo & "', prwipmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE prwipmststatus='In Approval' AND prwipmstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_prwipmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "PR Sawn Timber Approval"
    Private Sub SetGVListPRSAWN()
        Dim arHeader() As String = {"Draft No.", "PR Date", "PR Dept.", "PR Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "prsawndept", "prsawnmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "prsawndept", "prsawnmstnote", "upduser", "updtime"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT pr.cmpcode, pr.prsawnmstoid AS trnoid, pr.prsawnno, CONVERT(VARCHAR(10), pr.prsawndate, 101) AS trndate, d.deptoid, d.deptname AS prsawndept, pr.prsawnmstnote, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, pr.upduser, pr.updtime FROM QL_prsawnmst pr INNER JOIN QL_mstdept d ON pr.deptoid=d.deptoid INNER JOIN QL_approval ap ON pr.prsawnmstoid=ap.oid AND ap.statusrequest='New' WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_prsawnmst' AND pr.prsawnmststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_prsawnmst")
    End Sub

    Private Sub SetGVDataPRSAWN(ByVal dtDataHeader As DataTable)
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "PR Date", "PR Dept.", "PR Note", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "prsawndept", "prsawnmstnote", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arHeader() As String = {"No.", "Code", "Description", "Arr. Date. Req", "Quantity", "Unit", "Last Price ", "Last Price IDR", "Last PO No", "Last PO Date", "Note"}
        Dim arField() As String = {"prsawndtlseq", "matsawncode", "matsawnlongdesc", "prsawnarrdatereq", "prsawnqty", "unit", "lastprice", "lastpriceidr", "lasttransno", "lasttransdate", "prsawndtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT prd.prsawndtlseq, (c1.cat1code + '.' + c2.cat2code + '.' + c3.cat3code) AS matsawncode, (RTRIM((CASE WHEN (LTRIM(c1.cat1shortdesc)='None' OR LTRIM(c1.cat1shortdesc)='') THEN '' ELSE c1.cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(c2.cat2shortdesc)='None' OR LTRIM(c2.cat2shortdesc)='') THEN '' ELSE c2.cat2shortdesc + ' ' END) + (CASE WHEN (LTRIM(c3.cat3shortdesc)='None' OR LTRIM(c3.cat3shortdesc)='') THEN '' ELSE c3.cat3shortdesc + ' ' END))) AS matsawnlongdesc, prd.prsawnqty, g.gendesc AS unit, ISNULL((SELECT TOP 1 lastpurchaseprice FROM QL_mstcat3price mp WHERE mp.cmpcode=prd.cmpcode AND mp.cat3oid=c3.cat3oid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) AS lastprice, ISNULL((SELECT TOP 1 lastpurchaseprice * ratetoidr FROM QL_mstcat3price mp WHERE mp.cmpcode=prd.cmpcode AND mp.cat3oid=c3.cat3oid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0) AS lastpriceidr, ISNULL((SELECT TOP 1 lasttransno FROM QL_mstcat3price mp WHERE mp.cmpcode=prd.cmpcode AND mp.cat3oid=c3.cat3oid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), '') AS lasttransno, ISNULL((SELECT TOP 1 CONVERT(VARCHAR(10),lasttransdate, 101) FROM QL_mstcat3price mp WHERE mp.cmpcode=prd.cmpcode AND mp.cat3oid=c3.cat3oid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 1/1/1900) AS lasttransdate, CONVERT(VARCHAR(10), prd.prsawnarrdatereq, 101) AS prsawnarrdatereq, prd.prsawndtlnote FROM QL_prsawnmst pr INNER JOIN QL_prsawndtl prd ON pr.prsawnmstoid=prd.prsawnmstoid INNER JOIN QL_mstcat3 c3 ON c3.cat3oid=prd.matsawnoid INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid AND c2.cat1oid=c3.cat1oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c3.cmpcode AND c1.cat1oid=c3.cat1oid INNER JOIN QL_mstgen g ON prd.prsawnunitoid=g.genoid WHERE pr.prsawnmstoid=" & Session("trnoid") & " ORDER BY matsawncode"
        FillGV(gvData, sSql, "QL_prsawndtl")
    End Sub

    Private Function GeneratePRSAWNNo() As String
        Dim sNo As String = "PRST-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(prsawnno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_prsawnmst WHERE cmpcode='" & AppCmpCode.Text & "' AND prsawnno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdatePRSawn(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select PR Sawn Timber Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select PR Sawn Timber Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update PR
            sSql = "UPDATE QL_prsawnmst SET prsawnno='" & sNo & "', prsawnmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE prsawnmststatus='In Approval' AND prsawnmstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_prsawnmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "PO Raw Material Approval"
    Private Sub SetGVListPORM()
        Dim arHeader() As String = {"Draft No.", "PO Date", "Supplier", "PO Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "suppname", "pomstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "suppname", "pomstnote", "pototalamt", "pomstdiscamt", "povat", "poothercost", "pograndtotalamt", "curroid", "suppoid", "currcode", "poratetoidr", "upduser", "updtime", "potaxtype", "taxtype"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT po.cmpcode, po.pomstoid AS trnoid, po.pono, CONVERT(VARCHAR(10), po.podate, 101) AS trndate, s.suppname, po.pomstnote, (po.pototalamt - po.pototaldiscdtl) AS pototalamt, po.pomstdiscamt, po.povat, (po.podeliverycost + po.poothercost) AS poothercost, po.pograndtotalamt, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, po.curroid, po.suppoid, c.currcode, po.porate2toidr AS poratetoidr, po.upduser, po.updtime, (CASE WHEN po.potaxtype_inex='EXC' THEN 'E' WHEN po.potaxtype_inex='INC' THEN 'I' ELSE 'N' END ) AS potaxtype, potaxtype+'-'+potaxtype_inex AS taxtype FROM QL_trnpomst po INNER JOIN QL_mstsupp s ON po.suppoid=s.suppoid INNER JOIN QL_approval ap ON po.pomstoid=ap.oid AND ap.statusrequest='New' INNER JOIN QL_mstcurr c ON c.curroid=po.curroid WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_trnporawmst' AND po.pomststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnporawmst")
    End Sub

    Private Sub SetGVDataPORM(ByVal dtDataHeader As DataTable)
        Session("curroid") = gvList.SelectedDataKey.Item("curroid").ToString
        Session("suppoid") = gvList.SelectedDataKey.Item("suppoid").ToString
        sTaxType.Text = gvList.SelectedDataKey.Item("potaxtype").ToString
        lblreason.Visible = True : lblSeptReason.Visible = True : ActionReason.Visible = True
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "PO Date", "Supplier", "Tax Type", "PO Note", "Currency", "Rate To IDR", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "suppname", "taxtype", "pomstnote", "currcode", "poratetoidr", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S", "D", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N", "O", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            ElseIf arHidden(C1) = "O" Then
                If gvDataHeader.Rows(C1 - 1).Cells(2).Text = "IDR" Then
                    gvDataHeader.Rows(C1).Visible = False
                End If
            End If
        Next
        Dim arColNameList() As String = {"datacaption", "datasept", "datavalue", "dataempty"}
        Dim arColTypeList() As String = {"System.String", "System.String", "System.String", "System.String"}
        Dim dtDataFooter As DataTable = SetTableDetail("tblDataFooter", arColNameList, arColTypeList)
        Dim arCaptionF() As String = {"Total", "Header Disc.", "Tax", "Grand Total"}
        Dim arValueF() As String = {"pototalamt", "pomstdiscamt", "povat", "pograndtotalamt"}
        Dim arTypeF() As String = {"D", "D", "D", "D"}
        Dim arHiddenF() As String = {"N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaptionF.Length - 1
            dr = dtDataFooter.NewRow
            dr("datacaption") = arCaptionF(C1)
            dr("datasept") = ":"
            If arTypeF(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValueF(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValueF(C1)).ToString), 2)
            End If
            dr("dataempty") = ""
            dtDataFooter.Rows.Add(dr)
        Next
        gvDataFooter.DataSource = dtDataFooter
        gvDataFooter.DataBind()
        For C1 As Int16 = 0 To arHiddenF.Length - 1
            If arHiddenF(C1) = "Y" Then
                gvDataFooter.Rows(C1).Visible = False
            End If
        Next
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Font.Bold = True
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).ForeColor = Drawing.Color.Black
        Dim arHeader() As String = {"No.", "Item. Code", "Item. Desc", "Stock Qty", "Unit", "Qty", "Unit", "Last Price", "Price", "Amount", "Disc.", "Netto", "Note"}
        Dim arField() As String = {"podtlseq", "itemcode", "matshortdesc", "stockqty", "stockunit", "poqty", "unit", "lastpoprice", "poprice", "podtlamt", "podtldiscamt", "podtlnetto", "podtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT pod.podtlseq, pod.matoid, m.itemoid, m.itemcode, m.itemshortdescription AS matshortdesc, pod.poqty, g.gendesc AS unit, pod.poprice, pod.podtlamt, pod.podtldiscamt, pod.podtlnetto, pod.podtlnote, ISNULL((SELECT TOP 1 mp.lastpurchaseprice FROM QL_mstitemprice mp WHERE mp.cmpcode=po.cmpcode AND mp.itemoid=pod.matoid AND mp.refname='SUPPLIER' AND mp.curroid=po.curroid AND itemgroup='Raw' AND res1=g.genoid ORDER BY mp.updtime DESC), 0.0) AS lastpoprice, pod.pounitoid AS unitoid, po.pogroup AS itemgroup, ISNULL((SELECT SUM(saldoakhir) FROM QL_crdstock crd WHERE crd.refoid=m.itemoid AND crd.refname='RAW MATERIAL' AND periodacctg=po.periodacctg),0) AS stockqty, g1.gendesc AS stockunit FROM QL_trnpomst po INNER JOIN QL_trnpodtl pod ON po.pomstoid=pod.pomstoid INNER JOIN QL_mstitem m ON pod.matoid=m.itemoid INNER JOIN QL_mstgen g ON pod.pounitoid=g.genoid INNER JOIN QL_mstgen g1 ON g1.genoid=m.itemunit1 WHERE po.pomstoid=" & Session("trnoid") & " ORDER BY m.itemcode"
        Session("TblDtlPO") = ckon.ambiltabel(sSql, "QL_trnpodtl")
        gvData.DataSource = Session("TblDtlPO")
        gvData.DataBind()
    End Sub

    Private Function GeneratePORMNo() As String
        Dim sNo As String = "PORM" & sTaxType.Text & "-" & Format(GetServerTime(), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(pono, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnpomst WHERE cmpcode='" & AppCmpCode.Text & "' AND pono LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdatePORM(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select PO Raw Material Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select PO Raw Material Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim cRate As New ClassRate
        If sStatus = "Approved" Then
            cRate.SetRateValue(ToInteger(Session("curroid")), Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        If sStatus = "Rejected" Or sStatus = "Revised" Then
            If ActionReason.Text = "" Then
                showMessage("Please Fill " & sStatus & " Reason First!", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim iPriceOid As Integer = GenerateID("QL_MSTITEMPRICE", CompnyCode)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            ' Update PO
            If sStatus = "Approved" Then
                sSql = "UPDATE QL_trnpomst SET pono='" & sNo & "', pomststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, rateoid=" & cRate.GetRateDailyOid & ", rate2oid=" & cRate.GetRateMonthlyOid & ", poratetoidrchar='" & Left(cRate.GetRateDailyIDRValue.ToString, 50) & "', poratetousdchar='" & Left(cRate.GetRateDailyUSDValue.ToString, 50) & "', porate2toidrchar='" & Left(cRate.GetRateMonthlyIDRValue.ToString, 50) & "', porate2tousdchar='" & Left(cRate.GetRateMonthlyUSDValue.ToString, 50) & "' WHERE pomststatus='In Approval' AND pomstoid=" & Session("trnoid")
            Else
                sSql = "UPDATE QL_trnpomst SET pono='" & sNo & "', pomststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP " & IIf(sStatus = "Rejected", ", rejectreason='" & Tchar(ActionReason.Text) & "', rejectuser='" & Session("UserID") & "', rejecttime=CURRENT_TIMESTAMP", ", revisereason='" & Tchar(ActionReason.Text) & "', reviseuser='" & Session("UserID") & "', revisetime=CURRENT_TIMESTAMP") & " WHERE pomststatus='In Approval' AND pomstoid=" & Session("trnoid")
            End If
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            Dim iReason As String = ""
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnporawmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Rejected" Then
                    'nothing
                ElseIf sStatus = "Approved" Then
                    If Not Session("TblDtlPO") Is Nothing Then
                        Dim dtDtl As DataTable = Session("TblDtlPO")
                        For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                            sSql = "UPDATE QL_mstitemprice SET totalpurchaseqty=totalpurchaseqty + " & ToDouble(dtDtl.Rows(C1)("poqty").ToString) & ", avgpurchaseprice=((avgpurchaseprice * totalpurchaseqty) + " & ToDouble(dtDtl.Rows(C1)("podtlamt").ToString) & ") / (totalpurchaseqty + " & ToDouble(dtDtl.Rows(C1)("poqty").ToString) & "), lastpurchaseprice=" & ToDouble(dtDtl.Rows(C1)("poprice").ToString) & ", lasttrans='QL_trnpomst', lasttransoid=" & Session("trnoid") & ", lasttransno='" & sNo & "', lasttransdate='" & Session("trndate") & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, ratetoidr=" & cRate.GetRateMonthlyIDRValue & " WHERE cmpcode='" & AppCmpCode.Text & "' AND itemoid=" & ToDouble(dtDtl.Rows(C1)("itemoid").ToString) & " AND refoid=" & Session("suppoid") & " AND res1='" & dtDtl.Rows(C1)("unitoid").ToString & "' AND itemgroup='" & dtDtl.Rows(C1)("itemgroup").ToString & "' AND refname='SUPPLIER' AND curroid=" & Session("curroid")
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() <= 0 Then
                                sSql = "INSERT INTO QL_mstitemprice (cmpcode, itempriceoid, itemoid, refoid, refname, totalpurchaseqty, totalsalesqty, avgpurchaseprice, lastpurchaseprice, avgsalesprice, lastsalesprice, lasttrans, lasttransoid, lasttransno, lasttransdate, note, res1, res2, res3, upduser, updtime, curroid, ratetoidr, itemgroup) VALUES ('" & AppCmpCode.Text & "', " & iPriceOid & ", " & ToDouble(dtDtl.Rows(C1)("itemoid").ToString) & ", " & Session("suppoid") & ", 'SUPPLIER', " & ToDouble(dtDtl.Rows(C1)("poqty").ToString) & ", 0, " & ToDouble(dtDtl.Rows(C1)("poprice").ToString) & ", " & ToDouble(dtDtl.Rows(C1)("poprice").ToString) & ", 0, 0, 'QL_trnpomst', " & Session("trnoid") & ", '" & sNo & "', '" & Session("trndate") & "', '', '" & dtDtl.Rows(C1)("unitoid").ToString & "', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Session("curroid") & ", " & cRate.GetRateMonthlyIDRValue & ", '" & dtDtl.Rows(C1)("itemgroup").ToString & "')"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iPriceOid += 1
                            End If
                        Next
                        sSql = "UPDATE QL_mstoid SET lastoid=" & iPriceOid - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_MSTITEMPRICE'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                End If
            End If

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
        sTaxType.Text = ""
    End Sub
#End Region

#Region "PO General Material Approval"
    Private Sub SetGVListPOGM()
        Dim arHeader() As String = {"Draft No.", "PO Date", "Supplier", "PO Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "suppname", "pomstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "suppname", "pomstnote", "pototalamt", "pomstdiscamt", "povat", "poothercost", "pograndtotalamt", "curroid", "suppoid", "currcode", "poratetoidr", "upduser", "updtime", "potaxtype", "taxtype"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT po.cmpcode, po.pomstoid AS trnoid, po.pono, CONVERT(VARCHAR(10), po.podate, 101) AS trndate, s.suppname, po.pomstnote, (po.pototalamt - po.pototaldiscdtl) AS pototalamt, po.pomstdiscamt, po.povat, (po.podeliverycost + po.poothercost) AS poothercost, po.pograndtotalamt, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, po.curroid, po.suppoid, c.currcode, po.porate2toidr AS poratetoidr, po.upduser, po.updtime, (CASE WHEN po.potaxtype_inex='EXC' THEN 'E' WHEN po.potaxtype_inex='INC' THEN 'I' ELSE 'N' END ) AS potaxtype, potaxtype+'-'+potaxtype_inex AS taxtype FROM QL_trnpomst po INNER JOIN QL_mstsupp s ON po.suppoid=s.suppoid INNER JOIN QL_approval ap ON po.pomstoid=ap.oid AND ap.statusrequest='New' INNER JOIN QL_mstcurr c ON c.curroid=po.curroid WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_trnpogenmst' AND po.pomststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnpogenmst")
    End Sub

    Private Sub SetGVDataPOGM(ByVal dtDataHeader As DataTable)
        Session("curroid") = gvList.SelectedDataKey.Item("curroid").ToString
        Session("suppoid") = gvList.SelectedDataKey.Item("suppoid").ToString
        sTaxType.Text = gvList.SelectedDataKey.Item("potaxtype").ToString
        lblreason.Visible = True : lblSeptReason.Visible = True : ActionReason.Visible = True
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "PO Date", "Supplier", "Tax Type", "PO Note", "Currency", "Rate To IDR", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "suppname", "taxtype", "pomstnote", "currcode", "poratetoidr", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S", "D", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N", "O", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            ElseIf arHidden(C1) = "O" Then
                If gvDataHeader.Rows(C1 - 1).Cells(2).Text = "IDR" Then
                    gvDataHeader.Rows(C1).Visible = False
                End If
            End If
        Next
        Dim arColNameList() As String = {"datacaption", "datasept", "datavalue", "dataempty"}
        Dim arColTypeList() As String = {"System.String", "System.String", "System.String", "System.String"}
        Dim dtDataFooter As DataTable = SetTableDetail("tblDataFooter", arColNameList, arColTypeList)
        Dim arCaptionF() As String = {"Total", "Header Disc.", "Tax", "Grand Total"}
        Dim arValueF() As String = {"pototalamt", "pomstdiscamt", "povat", "pograndtotalamt"}
        Dim arTypeF() As String = {"D", "D", "D", "D"}
        Dim arHiddenF() As String = {"N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaptionF.Length - 1
            dr = dtDataFooter.NewRow
            dr("datacaption") = arCaptionF(C1)
            dr("datasept") = ":"
            If arTypeF(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValueF(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValueF(C1)).ToString), 2)
            End If
            dr("dataempty") = ""
            dtDataFooter.Rows.Add(dr)
        Next
        gvDataFooter.DataSource = dtDataFooter
        gvDataFooter.DataBind()
        For C1 As Int16 = 0 To arHiddenF.Length - 1
            If arHiddenF(C1) = "Y" Then
                gvDataFooter.Rows(C1).Visible = False
            End If
        Next
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Font.Bold = True
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).ForeColor = Drawing.Color.Black
        Dim arHeader() As String = {"No.", "Item. Code", "Item. Desc", "Stock Qty", "Unit", "Qty", "Unit", "Last Price", "Price", "Amount", "Disc.", "Netto", "Note"}
        Dim arField() As String = {"podtlseq", "itemcode", "matshortdesc", "stockqty", "stockunit", "poqty", "unit", "lastpoprice", "poprice", "podtlamt", "podtldiscamt", "podtlnetto", "podtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT pod.podtlseq, pod.matoid, m.itemoid, m.itemcode, m.itemshortdescription AS matshortdesc, pod.poqty, g.gendesc AS unit, pod.poprice, pod.podtlamt, pod.podtldiscamt, pod.podtlnetto, pod.podtlnote, ISNULL((SELECT TOP 1 mp.lastpurchaseprice FROM QL_mstitemprice mp WHERE mp.cmpcode=po.cmpcode AND mp.itemoid=pod.matoid AND mp.refname='SUPPLIER' AND mp.curroid=po.curroid AND itemgroup='GEN' AND res1=g.genoid ORDER BY mp.updtime DESC), 0.0) AS lastpoprice, pod.pounitoid AS unitoid, po.pogroup AS itemgroup, ISNULL((SELECT SUM(saldoakhir) FROM QL_crdstock crd WHERE crd.refoid=m.itemoid AND crd.refname='GENERAL MATERIAL' AND periodacctg=po.periodacctg),0) AS stockqty, g1.gendesc AS stockunit FROM QL_trnpomst po INNER JOIN QL_trnpodtl pod ON po.pomstoid=pod.pomstoid INNER JOIN QL_mstitem m ON pod.matoid=m.itemoid INNER JOIN QL_mstgen g ON pod.pounitoid=g.genoid INNER JOIN QL_mstgen g1 ON g1.genoid=m.itemunit1 WHERE po.pomstoid=" & Session("trnoid") & " ORDER BY m.itemcode"
        Session("TblDtlPO") = ckon.ambiltabel(sSql, "QL_trnpodtl")
        gvData.DataSource = Session("TblDtlPO")
        gvData.DataBind()
    End Sub

    Private Function GeneratePOGMNo() As String
        Dim sNo As String = "POGM" & sTaxType.Text & "-" & Format(GetServerTime(), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(pono, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnpomst WHERE cmpcode='" & AppCmpCode.Text & "' AND pono LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdatePOGM(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select PO Gen Material Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select PO Gen Material Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim cRate As New ClassRate
        If sStatus = "Approved" Then
            cRate.SetRateValue(ToInteger(Session("curroid")), Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        If sStatus = "Rejected" Or sStatus = "Revised" Then
            If ActionReason.Text = "" Then
                showMessage("Please Fill " & sStatus & " Reason First!", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim iPriceOid As Integer = GenerateID("QL_MSTITEMPRICE", CompnyCode)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        'CONNECTION TO GSMR
        Dim objTransB As SqlClient.SqlTransaction
        If connb.State = ConnectionState.Closed Then
            connb.Open()
        End If
        objTransB = connb.BeginTransaction()
        xCmdB.Transaction = objTransB

        Try
            ' Update PO
            If sStatus = "Approved" Then
                sSql = "UPDATE QL_trnpomst SET pono='" & sNo & "', pomststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, rateoid=" & cRate.GetRateDailyOid & ", rate2oid=" & cRate.GetRateMonthlyOid & ", poratetoidrchar='" & Left(cRate.GetRateDailyIDRValue.ToString, 50) & "', poratetousdchar='" & Left(cRate.GetRateDailyUSDValue.ToString, 50) & "', porate2toidrchar='" & Left(cRate.GetRateMonthlyIDRValue.ToString, 50) & "', porate2tousdchar='" & Left(cRate.GetRateMonthlyUSDValue.ToString, 50) & "' WHERE pomststatus='In Approval' AND pomstoid=" & Session("trnoid")
            Else
                sSql = "UPDATE QL_trnpomst SET pono='" & sNo & "', pomststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP " & IIf(sStatus = "Rejected", ", rejectreason='" & Tchar(ActionReason.Text) & "', rejectuser='" & Session("UserID") & "', rejecttime=CURRENT_TIMESTAMP", ", revisereason='" & Tchar(ActionReason.Text) & "', reviseuser='" & Session("UserID") & "', revisetime=CURRENT_TIMESTAMP") & " WHERE pomststatus='In Approval' AND pomstoid=" & Session("trnoid")
            End If
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
          
            Dim iReason As String = ""
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnpogenmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Rejected" Then
                    'nothing
                ElseIf sStatus = "Approved" Then
                    If Not Session("TblDtlPO") Is Nothing Then
                        Dim dtDtl As DataTable = Session("TblDtlPO")
                        For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                            sSql = "UPDATE QL_mstitemprice SET totalpurchaseqty=totalpurchaseqty + " & ToDouble(dtDtl.Rows(C1)("poqty").ToString) & ", avgpurchaseprice=((avgpurchaseprice * totalpurchaseqty) + " & ToDouble(dtDtl.Rows(C1)("podtlamt").ToString) & ") / (totalpurchaseqty + " & ToDouble(dtDtl.Rows(C1)("poqty").ToString) & "), lastpurchaseprice=" & ToDouble(dtDtl.Rows(C1)("poprice").ToString) & ", lasttrans='QL_trnpomst', lasttransoid=" & Session("trnoid") & ", lasttransno='" & sNo & "', lasttransdate='" & Session("trndate") & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, ratetoidr=" & cRate.GetRateMonthlyIDRValue & " WHERE cmpcode='" & AppCmpCode.Text & "' AND itemoid=" & ToDouble(dtDtl.Rows(C1)("itemoid").ToString) & " AND refoid=" & Session("suppoid") & " AND res1='" & dtDtl.Rows(C1)("unitoid").ToString & "' AND itemgroup='" & dtDtl.Rows(C1)("itemgroup").ToString & "' AND refname='SUPPLIER' AND curroid=" & Session("curroid")
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() <= 0 Then
                                sSql = "INSERT INTO QL_mstitemprice (cmpcode, itempriceoid, itemoid, refoid, refname, totalpurchaseqty, totalsalesqty, avgpurchaseprice, lastpurchaseprice, avgsalesprice, lastsalesprice, lasttrans, lasttransoid, lasttransno, lasttransdate, note, res1, res2, res3, upduser, updtime, curroid, ratetoidr, itemgroup) VALUES ('" & AppCmpCode.Text & "', " & iPriceOid & ", " & ToDouble(dtDtl.Rows(C1)("itemoid").ToString) & ", " & Session("suppoid") & ", 'SUPPLIER', " & ToDouble(dtDtl.Rows(C1)("poqty").ToString) & ", 0, " & ToDouble(dtDtl.Rows(C1)("poprice").ToString) & ", " & ToDouble(dtDtl.Rows(C1)("poprice").ToString) & ", 0, 0, 'QL_trnpomst', " & Session("trnoid") & ", '" & sNo & "', '" & Session("trndate") & "', '', '" & dtDtl.Rows(C1)("unitoid").ToString & "', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Session("curroid") & ", " & cRate.GetRateMonthlyIDRValue & ", '" & dtDtl.Rows(C1)("itemgroup").ToString & "')"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iPriceOid += 1
                            End If
                        Next
                        sSql = "UPDATE QL_mstoid SET lastoid=" & iPriceOid - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_MSTITEMPRICE'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                End If
            End If

            'INSERT SO INTO GSMR WITH IN APPROVAL STATUS
            sSql = "select count(-1) from QL_trnpomst where pomstoid=" & Session("trnoid") & " and popartnerid='GSMR'"
            xCmd.CommandText = sSql
            If xCmd.ExecuteScalar > 0 Then
                If sStatus = "Approved" Then
                    If Session("TblDtlPO") IsNot Nothing Then
                        'GET LAST SO OID
                        sSql = "select ISNULL(lastoid,0) from QL_mstoid where tablename = 'QL_trnsomst' and cmpcode='" & CompnyCode & "'"
                        xCmdB.CommandText = sSql
                        Dim tsooid As Integer = xCmdB.ExecuteScalar + 1
                        If tsooid = Nothing Then
                            tsooid = 0
                        End If

                        'GET APPROVAL USER
                        sSql = "SELECT approvaluser, apppersontype, (case when upper(apppersontype)='FINAL' then 0 else apppersonlevel end) apppersonlevel FROM QL_approvalperson WHERE tablename='QL_trnsomst' AND activeflag='ACTIVE' AND cmpcode IN ('" & CompnyCode & "', '" & DDLBusUnit.SelectedValue & "') order by apppersontype desc,apppersonlevel asc"
                        xCmdB.CommandText = sSql
                        Dim tadapterapp As SqlDataAdapter = New SqlDataAdapter
                        Dim tdsetapp As New DataSet
                        tadapterapp.SelectCommand = xCmdB
                        tadapterapp.Fill(tdsetapp, "QL_approvalperson")
                        Dim objtableapp As DataTable = tdsetapp.Tables("QL_approvalperson")
                        'INSERT INTO APPROVAL TABLE
                        sSql = "select ISNULL(lastoid,0) from QL_mstoid where tablename = 'QL_approval' and cmpcode='" & CompnyCode & "'"
                        xCmdB.CommandText = sSql
                        Dim iAppOid As Integer = xCmdB.ExecuteScalar + 1
                        If iAppOid = Nothing Then
                            iAppOid = 0
                        End If
                        Dim tempAppCode As String = ""
                        For C1 As Integer = 0 To objtableapp.Rows.Count - 1
                            If C1 = 0 Then
                                tempAppCode = objtableapp.Rows(C1)("apppersonlevel").ToString
                            End If
                            sSql = "INSERT INTO QL_approval (cmpcode, approvaloid, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvalnote, posisiLevel) VALUES ('" & CompnyCode & "', " & iAppOid + C1 & ", 'SO-" & tsooid.ToString & "_" & (iAppOid + C1) & "', 'PRKP', '" & Date.Now.Date & "', 'New', 'QL_trnsomst', " & tsooid & ", 'In Approval', '" & objtableapp.Rows(C1)("apppersonlevel").ToString & "', '" & objtableapp.Rows(C1)("approvaluser").ToString & "', '1/1/1900', '','" & IIf(tempAppCode = objtableapp.Rows(C1)("apppersonlevel").ToString, "1", "") & "')"
                            xCmdB.CommandText = sSql
                            xCmdB.ExecuteNonQuery()
                        Next
                        sSql = "UPDATE QL_mstoid SET lastoid=" & iAppOid + objtableapp.Rows.Count - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_approval'"
                        xCmdB.CommandText = sSql
                        xCmdB.ExecuteNonQuery()

                        'SETUP SO
                        sSql = "select genoid from QL_mstgen where cmpcode='" & CompnyCode & "' AND gengroup='SOTYPE' and gendesc='TRADING' order by gendesc"
                        xCmdB.CommandText = sSql
                        Dim tsotype As Integer = xCmdB.ExecuteScalar
                        If tsotype = Nothing Then
                            tsotype = 0
                        End If
                        sSql = "SELECT getdate() FROM ql_mstoid"
                        xCmdB.CommandText = sSql
                        Dim tperiodacctg As String = GetDateToPeriodAcctg(xCmdB.ExecuteScalar)
                        sSql = "select genoid from QL_mstgen where cmpcode='" & CompnyCode & "' AND gengroup='DELIVTYPE' and gendesc='DIRECT DELIVERY' order by gendesc"
                        xCmdB.CommandText = sSql
                        Dim tsodelivtype As Integer = xCmdB.ExecuteScalar
                        If tsodelivtype = Nothing Then
                            tsodelivtype = 0
                        End If
                        sSql = "select genoid from QL_mstgen where cmpcode='" & CompnyCode & "' AND gengroup='PROMO' and gendesc='NONE' order by gendesc"
                        xCmdB.CommandText = sSql
                        Dim tsopromo As Integer = xCmdB.ExecuteScalar
                        If tsopromo = Nothing Then
                            tsopromo = 0
                        End If
                        sSql = "SELECT genoid FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='PAYMENT TYPE' AND activeflag='ACTIVE' and gendesc='30'"
                        xCmdB.CommandText = sSql
                        Dim tsopaytype As Integer = xCmdB.ExecuteScalar
                        If tsopaytype = Nothing Then
                            tsopaytype = 0
                        End If
                        sSql = "SELECT curroid FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' and currcode='IDR'"
                        xCmdB.CommandText = sSql
                        Dim tsocurr As Integer = xCmdB.ExecuteScalar
                        If tsocurr = Nothing Then
                            tsocurr = 0
                        End If
                        sSql = "SELECT TOP 1 rateoid FROM QL_mstrate WHERE cmpcode='" & CompnyCode & "' AND curroid=" & tsocurr & " AND '" & Date.Now.Date & " 00:00:00'>=ratedate AND '" & Date.Now.Date & " 00:00:00'<=ratetodate AND activeflag='ACTIVE' ORDER BY ratedate DESC, rateoid DESC"
                        xCmdB.CommandText = sSql
                        Dim trateoid As Integer = xCmdB.ExecuteScalar
                        If trateoid = Nothing Then
                            trateoid = 0
                        End If
                        sSql = "SELECT TOP 1 rateres2 FROM QL_mstrate WHERE cmpcode='" & CompnyCode & "' AND curroid=" & tsocurr & " AND '" & Date.Now.Date & " 00:00:00'>=ratedate AND '" & Date.Now.Date & " 00:00:00'<=ratetodate AND activeflag='ACTIVE' ORDER BY ratedate DESC, rateoid DESC"
                        xCmdB.CommandText = sSql
                        Dim temprateusd As String = xCmdB.ExecuteScalar
                        Dim trateusd As Decimal = 0.0
                        If temprateusd <> Nothing Then
                            If temprateusd <> "" Then
                                trateusd = CDbl(temprateusd)
                                trateusd = ToMaskEdit(trateusd, GetRoundValue(trateusd))
                            Else
                                trateusd = 0.0
                            End If
                        End If
                        Dim ttotalamt As Decimal = 0.0
                        Dim ttotaldpp As Decimal = 0.0
                        Dim ttotalnett As Decimal = 0.0
                        Dim tvattype As String = ""
                        Dim ttotaltax As Decimal = 0.0
                        sSql = "select pototalamt,pototalnetto,pograndtotalamt,potaxtype_inex,povat from QL_trnpomst where cmpcode='" & CompnyCode & "' and pomstoid=" & Session("trnoid") & ""
                        xCmd.CommandText = sSql
                        xreader = xCmd.ExecuteReader
                        While xreader.Read
                            ttotalamt = xreader("pototalamt")
                            ttotaldpp = xreader("pototalnetto")
                            ttotalnett = xreader("pograndtotalamt")
                            tvattype = xreader("potaxtype_inex")
                            If tvattype <> "INC" Then
                                tvattype = "NT"
                            End If
                            ttotaltax = xreader("povat")
                        End While
                        xreader.Close()
                        sSql = "select top 1 salescode from ql_mstperson p inner join QL_mstlevel g on g.leveloid =p.leveloid where upper(g.leveldesc)='SALES' and p.activeflag='ACTIVE'"
                        xCmdB.CommandText = sSql
                        Dim tsales As String = xCmdB.ExecuteScalar
                        If tsales = Nothing Then
                            tsales = ""
                        End If
                        sSql = "select custoid from QL_mstcust where activeflag='ACTIVE' and custpartner=1"
                        xCmdB.CommandText = sSql
                        Dim tcust As Integer = xCmdB.ExecuteScalar
                        If tcust = Nothing Then
                            tcust = 0
                        End If

                        'SO MST
                        sSql = "INSERT INTO QL_trnsomst (cmpcode, somstoid, periodacctg, sotype, sotype2, sodelivtype, somstpromo, somstrefno, sodate, sono, custoid, sopaytypeoid, curroid, rateoid, sototalamt, somstdisctype, somstdiscvalue, somstdiscamt, somstdpp, sototalnetto, sotaxtype, sotaxamt, somstnote, somststatus, somstres1, somstres2, somstres3, createuser, createtime, upduser, updtime, soratetoidr, sototalamtidr, somstdiscamtidr, sototalnettoidr, soratetousd, sototalamtusd, somstdiscamtusd, sototalnettousd, soetd, sosalescode, popartneroid, popartner2oid) VALUES ('" & CompnyCode & "', " & tsooid & ", '" & tperiodacctg & "', 'SO', " & tsotype & ", " & tsodelivtype & ", " & tsopromo & ", '" & Tchar(sNo) & "', '" & Date.Now.Date & "', '" & tsooid.ToString & "'," & tcust & ",  " & tsopaytype & ", " & tsocurr & ", " & trateoid & ", " & ttotalamt & ", 'P', 0.0, 0.0, " & ttotaldpp & ", " & ttotalnett & ", '" & tvattype & "', " & ttotaltax & ", '', 'In Approval', 'FG', '', '', 'PRKP', CURRENT_TIMESTAMP, 'PRKP', CURRENT_TIMESTAMP, 1.0, " & ttotalamt & ", 0.0, " & ttotaldpp & ", " & trateusd & ", " & ttotalamt * trateusd & ", 0.0, " & ttotalnett * trateusd & ", '" & Now.Date & "', '" & tsales & "', 0, " & Session("trnoid") & ")"
                        xCmdB.CommandText = sSql
                        xCmdB.ExecuteNonQuery()
                        sSql = "UPDATE QL_mstoid SET lastoid=" & tsooid & " WHERE tablename='QL_TRNSOMST' AND cmpcode='" & CompnyCode & "'"
                        xCmdB.CommandText = sSql
                        xCmdB.ExecuteNonQuery()

                        'SO DTL
                        sSql = "select ISNULL(lastoid,0) from QL_mstoid where tablename = 'QL_trnsodtl' and cmpcode='" & CompnyCode & "'"
                        xCmdB.CommandText = sSql
                        Dim tsodtloid As Integer = xCmdB.ExecuteScalar + 1
                        If tsodtloid = Nothing Then
                            tsodtloid = 0
                        End If
                        'GET TAX TYPE
                        sSql = "select potaxtype_inex from QL_trnpomst where pomstoid=" & Session("trnoid") & ""
                        xCmdB.CommandText = sSql
                        Dim tpotax As String = xCmdB.ExecuteScalar
                        If tpotax = Nothing Then
                            tpotax = ""
                        End If
                        sSql = "select a.matoid itemoid,a.poqty sodtlqty,a.pounitoid sodtlunitoid,a.poprice sodtlprice,(a.povarpriceidr*a.poqty) sodtlamt,a.podtldisctype sodtldisctype1,a.podtldiscvalue sodtldiscvalue1,a.podtldiscamt sodtldiscamt1,'P' sodtldisctype2,0.0 sodtldiscvalue2,0.0 sodtldiscamt2,'P' sodtldisctype3,0.0 sodtldiscvalue3,0.0 sodtldiscamt3,(a.povarpriceidr*a.poqty) sodtlnetto,'' sodtlnote,'' sodtlstatus,'' sodtlres1,'' sodtlres2,'' sodtlres3,'PRKP' upduser,CURRENT_TIMESTAMP updtime,a.popriceidr sopriceidr,(a.povarpriceidr*a.poqty) sodtlamtidr,(a.povarpriceidr*a.poqty) sodtlnettoidr,(a.popriceidr*" & trateusd & ") sopriceusd,(a.povarpriceidr*a.poqty*" & trateusd & ") sodtlamtusd,(a.povarpriceidr*a.poqty*" & trateusd & ") sodtlnettousd,a.poqty_unitkecil soqty_unitkecil,a.poqty_unitbesar soqty_unitbesar from QL_trnpodtl a inner join QL_trnpomst b on a.pomstoid=b.pomstoid where a.pomstoid=" & Session("trnoid") & " order by a.podtlseq"
                        xCmd.CommandText = sSql
                        Dim tadapter As SqlDataAdapter = New SqlDataAdapter
                        Dim tdset As New DataSet
                        tadapter.SelectCommand = xCmd
                        tadapter.Fill(tdset, "QL_trnsjjualmst")
                        Dim objtable As DataTable = tdset.Tables("QL_trnsjjualmst")

                        If objtable.Rows.Count > 0 Then
                            For c1 As Integer = 0 To objtable.Rows.Count - 1
                                sSql = "INSERT INTO QL_trnsodtl (cmpcode, sodtloid, somstoid, sodtlseq, itemoid, sodtlqty, sodtlunitoid, sodtlprice, sodtlamt, sodtldisctype1, sodtldiscvalue1, sodtldiscamt1, sodtldisctype2, sodtldiscvalue2, sodtldiscamt2, sodtldisctype3, sodtldiscvalue3, sodtldiscamt3, sodtlnetto, sodtlnote, sodtlstatus, sodtlres1, sodtlres2, sodtlres3, upduser, updtime, sopriceidr, sodtlamtidr, sodtlnettoidr, sopriceusd, sodtlamtusd, sodtlnettousd, soqty_unitkecil, soqty_unitbesar) VALUES ('" & CompnyCode & "', " & (c1 + tsodtloid) & ", " & tsooid & ",  " & c1 + 1 & ", " & objtable.Rows(c1).Item("itemoid") & ", " & ToDouble(objtable.Rows(c1).Item("sodtlqty").ToString) & ", " & objtable.Rows(c1).Item("sodtlunitoid") & ", " & ToDouble(objtable.Rows(c1).Item("sodtlprice").ToString) & ", " & ToDouble(objtable.Rows(c1).Item("sodtlamt").ToString) & ", '" & objtable.Rows(c1).Item("sodtldisctype1").ToString & "', " & ToDouble(objtable.Rows(c1).Item("sodtldiscvalue1").ToString) & ", " & ToDouble(objtable.Rows(c1).Item("sodtldiscamt1").ToString) & ", '" & objtable.Rows(c1).Item("sodtldisctype2").ToString & "', " & ToDouble(objtable.Rows(c1).Item("sodtldiscvalue2").ToString) & ", " & ToDouble(objtable.Rows(c1).Item("sodtldiscamt2").ToString) & ", '" & objtable.Rows(c1).Item("sodtldisctype3").ToString & "', " & ToDouble(objtable.Rows(c1).Item("sodtldiscvalue3").ToString) & ", " & ToDouble(objtable.Rows(c1).Item("sodtldiscamt3").ToString) & "," & ToDouble(objtable.Rows(c1).Item("sodtlnetto").ToString) & ", '" & Tchar(objtable.Rows(c1).Item("sodtlnote")) & "', '" & Tchar(objtable.Rows(c1).Item("sodtlstatus")) & "', '" & Tchar(objtable.Rows(c1).Item("sodtlres1")) & "', '" & Tchar(objtable.Rows(c1).Item("sodtlres2")) & "', '" & Tchar(objtable.Rows(c1).Item("sodtlres3")) & "', '" & Tchar(objtable.Rows(c1).Item("upduser")) & "', CURRENT_TIMESTAMP, " & ToDouble(objtable.Rows(c1).Item("sopriceidr").ToString) & ", " & ToDouble(objtable.Rows(c1).Item("sodtlamtidr").ToString) & ", " & ToDouble(objtable.Rows(c1).Item("sodtlnettoidr").ToString) & ", " & ToDouble(objtable.Rows(c1).Item("sopriceusd").ToString) & ", " & ToDouble(objtable.Rows(c1).Item("sodtlamtusd").ToString) & ", " & ToDouble(objtable.Rows(c1).Item("sodtlnettousd").ToString) & ", " & ToDouble(objtable.Rows(c1).Item("soqty_unitkecil").ToString) & ", " & ToDouble(objtable.Rows(c1).Item("soqty_unitbesar").ToString) & ")"
                                xCmdB.CommandText = sSql
                                xCmdB.ExecuteNonQuery()
                            Next
                            sSql = "UPDATE QL_mstoid SET lastoid=" & (objtable.Rows.Count - 1 + tsodtloid) & " WHERE tablename='QL_trnsodtl' AND cmpcode='" & CompnyCode & "' "
                            xCmdB.CommandText = sSql
                            xCmdB.ExecuteNonQuery()
                        End If
                    End If
                End If
            End If

            objTransB.Commit()
            connb.Close()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTransB.Rollback() : connb.Close()
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
        sTaxType.Text = ""
    End Sub
#End Region

#Region "PO Spare Part Approval"
    Private Sub SetGVListPOSP()
        Dim arHeader() As String = {"Draft No.", "PO Date", "Supplier", "PO Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "suppname", "pomstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "suppname", "pomstnote", "pototalamt", "pomstdiscamt", "povat", "poothercost", "pograndtotalamt", "curroid", "suppoid", "currcode", "poratetoidr", "upduser", "updtime", "potaxtype"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT po.cmpcode, po.pomstoid AS trnoid, po.pono, CONVERT(VARCHAR(10), po.podate, 101) AS trndate, s.suppname, po.pomstnote, (po.pototalamt - po.pototaldiscdtl) AS pototalamt, po.pomstdiscamt, po.povat, (po.podeliverycost + po.poothercost) AS poothercost, po.pograndtotalamt, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, po.curroid, po.suppoid, c.currcode, po.porate2toidr AS poratetoidr, po.upduser, po.updtime, (CASE WHEN po.potaxtype_inex='EXC' THEN 'E' WHEN po.potaxtype_inex='INC' THEN 'I' ELSE 'N' END ) AS potaxtype, potaxtype+'-'+potaxtype_inex AS taxtype FROM QL_trnpomst po INNER JOIN QL_mstsupp s ON po.suppoid=s.suppoid INNER JOIN QL_approval ap ON po.pomstoid=ap.oid AND ap.statusrequest='New' INNER JOIN QL_mstcurr c ON c.curroid=po.curroid WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_trnpospmst' AND po.pomststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnpospmst")
    End Sub

    Private Sub SetGVDataPOSP(ByVal dtDataHeader As DataTable)
        Session("curroid") = gvList.SelectedDataKey.Item("curroid").ToString
        Session("suppoid") = gvList.SelectedDataKey.Item("suppoid").ToString
        sTaxType.Text = gvList.SelectedDataKey.Item("potaxtype").ToString
        lblreason.Visible = True : lblSeptReason.Visible = True : ActionReason.Visible = True
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "PO Date", "Supplier", "Tax Type", "PO Note", "Currency", "Rate To IDR", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "suppname", "taxtype", "pomstnote", "currcode", "poratetoidr", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S", "D", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N", "O", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            ElseIf arHidden(C1) = "O" Then
                If gvDataHeader.Rows(C1 - 1).Cells(2).Text = "IDR" Then
                    gvDataHeader.Rows(C1).Visible = False
                End If
            End If
        Next
        Dim arColNameList() As String = {"datacaption", "datasept", "datavalue", "dataempty"}
        Dim arColTypeList() As String = {"System.String", "System.String", "System.String", "System.String"}
        Dim dtDataFooter As DataTable = SetTableDetail("tblDataFooter", arColNameList, arColTypeList)
        Dim arCaptionF() As String = {"Total", "Header Disc.", "Tax", "Other Cost", "Grand Total"}
        Dim arValueF() As String = {"pototalamt", "pomstdiscamt", "povat", "poothercost", "pograndtotalamt"}
        Dim arTypeF() As String = {"D", "D", "D", "D", "D"}
        Dim arHiddenF() As String = {"N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaptionF.Length - 1
            dr = dtDataFooter.NewRow
            dr("datacaption") = arCaptionF(C1)
            dr("datasept") = ":"
            If arTypeF(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValueF(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValueF(C1)).ToString), 2)
            End If
            dr("dataempty") = ""
            dtDataFooter.Rows.Add(dr)
        Next
        gvDataFooter.DataSource = dtDataFooter
        gvDataFooter.DataBind()
        For C1 As Int16 = 0 To arHiddenF.Length - 1
            If arHiddenF(C1) = "Y" Then
                gvDataFooter.Rows(C1).Visible = False
            End If
        Next
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Font.Bold = True
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).ForeColor = Drawing.Color.Black
        Dim arHeader() As String = {"No.", "Item. Code", "Item. Desc", "Quantity", "Unit", "Last Price", "Price", "Amount", "Disc.", "Netto", "Note"}
        Dim arField() As String = {"podtlseq", "itemcode", "matshortdesc", "poqty", "unit", "lastpoprice", "poprice", "podtlamt", "podtldiscamt", "podtlnetto", "podtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT pod.podtlseq, pod.matoid, m.itemoid, m.itemcode, m.itemshortdescription AS matshortdesc, pod.poqty, g.gendesc AS unit, pod.poprice, pod.podtlamt, pod.podtldiscamt, pod.podtlnetto, pod.podtlnote, ISNULL((SELECT TOP 1 mp.lastpurchaseprice FROM QL_mstitemprice mp WHERE mp.cmpcode=po.cmpcode AND mp.itemoid=pod.matoid AND mp.refname='SUPPLIER' AND mp.curroid=po.curroid AND itemgroup='SP' AND res1=g.genoid ORDER BY mp.updtime DESC), 0.0) AS lastpoprice, pod.pounitoid AS unitoid, po.pogroup AS itemgroup FROM QL_trnpomst po INNER JOIN QL_trnpodtl pod ON po.pomstoid=pod.pomstoid INNER JOIN QL_mstitem m ON pod.matoid=m.itemoid INNER JOIN QL_mstgen g ON pod.pounitoid=g.genoid WHERE po.pomstoid=" & Session("trnoid") & " ORDER BY m.itemcode"
        Session("TblDtlPO") = ckon.ambiltabel(sSql, "QL_trnpodtl")
        gvData.DataSource = Session("TblDtlPO")
        gvData.DataBind()
    End Sub

    Private Function GeneratePOSPNo() As String
        Dim sNo As String = "POSP" & sTaxType.Text & "-" & Format(GetServerTime(), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(pono, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnpomst WHERE cmpcode='" & AppCmpCode.Text & "' AND pono LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdatePOSP(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select PO Spare Part Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select PO Spare Part Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim cRate As New ClassRate
        If sStatus = "Approved" Then
            cRate.SetRateValue(ToInteger(Session("curroid")), Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        If sStatus = "Rejected" Or sStatus = "Revised" Then
            If ActionReason.Text = "" Then
                showMessage("Please Fill " & sStatus & " Reason First!", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim iPriceOid As Integer = GenerateID("QL_MSTITEMPRICE", CompnyCode)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update PO
            If sStatus = "Approved" Then
                sSql = "UPDATE QL_trnpomst SET pono='" & sNo & "', pomststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, rateoid=" & cRate.GetRateDailyOid & ", rate2oid=" & cRate.GetRateMonthlyOid & ", poratetoidrchar='" & Left(cRate.GetRateDailyIDRValue.ToString, 50) & "', poratetousdchar='" & Left(cRate.GetRateDailyUSDValue.ToString, 50) & "', porate2toidrchar='" & Left(cRate.GetRateMonthlyIDRValue.ToString, 50) & "', porate2tousdchar='" & Left(cRate.GetRateMonthlyUSDValue.ToString, 50) & "' WHERE pomststatus='In Approval' AND pomstoid=" & Session("trnoid")
            Else
                sSql = "UPDATE QL_trnpomst SET pono='" & sNo & "', pomststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP " & IIf(sStatus = "Rejected", ", rejectreason='" & Tchar(ActionReason.Text) & "', rejectuser='" & Session("UserID") & "', rejecttime=CURRENT_TIMESTAMP", ", revisereason='" & Tchar(ActionReason.Text) & "', reviseuser='" & Session("UserID") & "', revisetime=CURRENT_TIMESTAMP") & " WHERE pomststatus='In Approval' AND pomstoid=" & Session("trnoid")
            End If
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
         
            Dim iReason As String = ""
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnpospmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Rejected" Then
                    'nothing
                ElseIf sStatus = "Approved" Then
                    If Not Session("TblDtlPO") Is Nothing Then
                        Dim dtDtl As DataTable = Session("TblDtlPO")
                        For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                            sSql = "UPDATE QL_mstitemprice SET totalpurchaseqty=totalpurchaseqty + " & ToDouble(dtDtl.Rows(C1)("poqty").ToString) & ", avgpurchaseprice=((avgpurchaseprice * totalpurchaseqty) + " & ToDouble(dtDtl.Rows(C1)("podtlamt").ToString) & ") / (totalpurchaseqty + " & ToDouble(dtDtl.Rows(C1)("poqty").ToString) & "), lastpurchaseprice=" & ToDouble(dtDtl.Rows(C1)("poprice").ToString) & ", lasttrans='QL_trnpomst', lasttransoid=" & Session("trnoid") & ", lasttransno='" & sNo & "', lasttransdate='" & Session("trndate") & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, ratetoidr=" & cRate.GetRateMonthlyIDRValue & " WHERE cmpcode='" & AppCmpCode.Text & "' AND itemoid=" & ToDouble(dtDtl.Rows(C1)("itemoid").ToString) & " AND refoid=" & Session("suppoid") & " AND res1='" & dtDtl.Rows(C1)("unitoid").ToString & "' AND itemgroup='" & dtDtl.Rows(C1)("itemgroup").ToString & "' AND refname='SUPPLIER' AND curroid=" & Session("curroid")
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() <= 0 Then
                                sSql = "INSERT INTO QL_mstitemprice (cmpcode, itempriceoid, itemoid, refoid, refname, totalpurchaseqty, totalsalesqty, avgpurchaseprice, lastpurchaseprice, avgsalesprice, lastsalesprice, lasttrans, lasttransoid, lasttransno, lasttransdate, note, res1, res2, res3, upduser, updtime, curroid, ratetoidr, itemgroup) VALUES ('" & AppCmpCode.Text & "', " & iPriceOid & ", " & ToDouble(dtDtl.Rows(C1)("itemoid").ToString) & ", " & Session("suppoid") & ", 'SUPPLIER', " & ToDouble(dtDtl.Rows(C1)("poqty").ToString) & ", 0, " & ToDouble(dtDtl.Rows(C1)("poprice").ToString) & ", " & ToDouble(dtDtl.Rows(C1)("poprice").ToString) & ", 0, 0, 'QL_trnpomst', " & Session("trnoid") & ", '" & sNo & "', '" & Session("trndate") & "', '', '" & dtDtl.Rows(C1)("unitoid").ToString & "', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Session("curroid") & ", " & cRate.GetRateMonthlyIDRValue & ", '" & dtDtl.Rows(C1)("itemgroup").ToString & "')"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iPriceOid += 1
                            End If
                        Next
                        sSql = "UPDATE QL_mstoid SET lastoid=" & iPriceOid - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_MSTITEMPRICE'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
        sTaxType.Text = ""
    End Sub
#End Region

#Region "PO Finish Good Approval"
    Private Sub SetGVListPOItem()
        Dim arHeader() As String = {"Draft No.", "PO Date", "Supplier", "PO Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "suppname", "pomstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "suppname", "pomstnote", "pototalamt", "pomstdiscamt", "povat", "poothercost", "pograndtotalamt", "curroid", "suppoid", "currcode", "poratetoidr", "upduser", "updtime", "potaxtype", "taxtype"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT po.cmpcode, po.pomstoid AS trnoid, po.pono, CONVERT(VARCHAR(10), po.podate, 101) AS trndate, s.suppname, po.pomstnote, (po.pototalamt - po.pototaldiscdtl) AS pototalamt, po.pomstdiscamt, po.povat, (po.podeliverycost + po.poothercost) AS poothercost, po.pograndtotalamt, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, po.curroid, po.suppoid, c.currcode, po.porate2toidr AS poratetoidr, po.upduser, po.updtime, (CASE WHEN po.potaxtype_inex='EXC' THEN 'E' WHEN po.potaxtype_inex='INC' THEN 'I' ELSE 'N' END ) AS potaxtype, potaxtype+'-'+potaxtype_inex AS taxtype FROM QL_trnpomst po INNER JOIN QL_mstsupp s ON po.suppoid=s.suppoid INNER JOIN QL_approval ap ON po.pomstoid=ap.oid AND ap.statusrequest='New' INNER JOIN QL_mstcurr c ON c.curroid=po.curroid WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_trnpofgmst' AND po.pomststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnpofgmst")
    End Sub

    Private Sub SetGVDataPOItem(ByVal dtDataHeader As DataTable)
        Session("curroid") = gvList.SelectedDataKey.Item("curroid").ToString
        Session("suppoid") = gvList.SelectedDataKey.Item("suppoid").ToString
        sTaxType.Text = gvList.SelectedDataKey.Item("potaxtype").ToString
        lblreason.Visible = True : lblSeptReason.Visible = True : ActionReason.Visible = True
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "PO Date", "Supplier", "Tax Type", "PO Note", "Currency", "Rate To IDR", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "suppname", "taxtype", "pomstnote", "currcode", "poratetoidr", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S", "D", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N", "O", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            ElseIf arHidden(C1) = "O" Then
                If gvDataHeader.Rows(C1 - 1).Cells(2).Text = "IDR" Then
                    gvDataHeader.Rows(C1).Visible = False
                End If
            End If
        Next
        Dim arColNameList() As String = {"datacaption", "datasept", "datavalue", "dataempty"}
        Dim arColTypeList() As String = {"System.String", "System.String", "System.String", "System.String"}
        Dim dtDataFooter As DataTable = SetTableDetail("tblDataFooter", arColNameList, arColTypeList)
        Dim arCaptionF() As String = {"Total", "Header Disc.", "Tax", "Grand Total"}
        Dim arValueF() As String = {"pototalamt", "pomstdiscamt", "povat", "pograndtotalamt"}
        Dim arTypeF() As String = {"D", "D", "D", "D"}
        Dim arHiddenF() As String = {"N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaptionF.Length - 1
            dr = dtDataFooter.NewRow
            dr("datacaption") = arCaptionF(C1)
            dr("datasept") = ":"
            If arTypeF(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValueF(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValueF(C1)).ToString), 2)
            End If
            dr("dataempty") = ""
            dtDataFooter.Rows.Add(dr)
        Next
        gvDataFooter.DataSource = dtDataFooter
        gvDataFooter.DataBind()
        For C1 As Int16 = 0 To arHiddenF.Length - 1
            If arHiddenF(C1) = "Y" Then
                gvDataFooter.Rows(C1).Visible = False
            End If
        Next
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Font.Bold = True
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).ForeColor = Drawing.Color.Black
        Dim arHeader() As String = {"No.", "Item. Code", "Item. Desc", "Stock Qty", "Unit", "Qty", "Unit", "Last Price", "Price", "Amount", "Disc.", "Netto", "Note"}
        Dim arField() As String = {"podtlseq", "itemcode", "matshortdesc", "stockqty", "stockunit", "poqty", "unit", "lastpoprice", "poprice", "podtlamt", "podtldiscamt", "podtlnetto", "podtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT pod.podtlseq, pod.matoid, m.itemoid, m.itemcode, m.itemshortdescription AS matshortdesc, pod.poqty, g.gendesc AS unit, pod.poprice, pod.podtlamt, pod.podtldiscamt, pod.podtlnetto, pod.podtlnote, ISNULL((SELECT TOP 1 mp.lastpurchaseprice FROM QL_mstitemprice mp WHERE mp.cmpcode=po.cmpcode AND mp.itemoid=pod.matoid AND mp.refname='SUPPLIER' AND mp.curroid=po.curroid AND itemgroup='FG' AND res1=g.genoid ORDER BY mp.updtime DESC), 0.0) AS lastpoprice, pod.pounitoid AS unitoid, po.pogroup AS itemgroup, ISNULL((SELECT SUM(saldoakhir) FROM QL_crdstock crd WHERE crd.refoid=m.itemoid AND crd.refname='GENERAL MATERIAL' AND periodacctg=po.periodacctg),0) AS stockqty, g1.gendesc AS stockunit FROM QL_trnpomst po INNER JOIN QL_trnpodtl pod ON po.pomstoid=pod.pomstoid INNER JOIN QL_mstitem m ON pod.matoid=m.itemoid INNER JOIN QL_mstgen g ON pod.pounitoid=g.genoid INNER JOIN QL_mstgen g1 ON g1.genoid=m.itemunit1 WHERE po.pomstoid=" & Session("trnoid") & " ORDER BY m.itemcode"
        Session("TblDtlPO") = ckon.ambiltabel(sSql, "QL_trnpodtl")
        gvData.DataSource = Session("TblDtlPO")
        gvData.DataBind()
    End Sub

    Private Function GeneratePOItemNo() As String
        Dim sNo As String = "POFG" & sTaxType.Text & "-" & Format(GetServerTime(), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(pono, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnpomst WHERE cmpcode='" & AppCmpCode.Text & "' AND pono LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdatePOItem(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select PO Finish Good Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select PO Finish Goodt Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim cRate As New ClassRate
        If sStatus = "Approved" Then
            cRate.SetRateValue(ToInteger(Session("curroid")), Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        If sStatus = "Rejected" Or sStatus = "Revised" Then
            If ActionReason.Text = "" Then
                showMessage("Please Fill " & sStatus & " Reason First!", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim iPriceOid As Integer = GenerateID("QL_MSTITEMPRICE", CompnyCode)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            ' Update PO
            If sStatus = "Approved" Then
                sSql = "UPDATE QL_trnpomst SET pono='" & sNo & "', pomststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, rateoid=" & cRate.GetRateDailyOid & ", rate2oid=" & cRate.GetRateMonthlyOid & ", poratetoidrchar='" & Left(cRate.GetRateDailyIDRValue.ToString, 50) & "', poratetousdchar='" & Left(cRate.GetRateDailyUSDValue.ToString, 50) & "', porate2toidrchar='" & Left(cRate.GetRateMonthlyIDRValue.ToString, 50) & "', porate2tousdchar='" & Left(cRate.GetRateMonthlyUSDValue.ToString, 50) & "' WHERE pomststatus='In Approval' AND pomstoid=" & Session("trnoid")
            Else
                sSql = "UPDATE QL_trnpomst SET pono='" & sNo & "', pomststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP " & IIf(sStatus = "Rejected", ", rejectreason='" & Tchar(ActionReason.Text) & "', rejectuser='" & Session("UserID") & "', rejecttime=CURRENT_TIMESTAMP", ", revisereason='" & Tchar(ActionReason.Text) & "', reviseuser='" & Session("UserID") & "', revisetime=CURRENT_TIMESTAMP") & " WHERE pomststatus='In Approval' AND pomstoid=" & Session("trnoid")
            End If
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If

            Dim iReason As String = ""
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnpofgmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Rejected" Then
                    'nothing
                ElseIf sStatus = "Approved" Then
                    If Not Session("TblDtlPO") Is Nothing Then
                        Dim dtDtl As DataTable = Session("TblDtlPO")
                        For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                            sSql = "UPDATE QL_mstitemprice SET totalpurchaseqty=totalpurchaseqty + " & ToDouble(dtDtl.Rows(C1)("poqty").ToString) & ", avgpurchaseprice=((avgpurchaseprice * totalpurchaseqty) + " & ToDouble(dtDtl.Rows(C1)("podtlamt").ToString) & ") / (totalpurchaseqty + " & ToDouble(dtDtl.Rows(C1)("poqty").ToString) & "), lastpurchaseprice=" & ToDouble(dtDtl.Rows(C1)("poprice").ToString) & ", lasttrans='QL_trnpomst', lasttransoid=" & Session("trnoid") & ", lasttransno='" & sNo & "', lasttransdate='" & Session("trndate") & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, ratetoidr=" & cRate.GetRateMonthlyIDRValue & " WHERE cmpcode='" & AppCmpCode.Text & "' AND itemoid=" & ToDouble(dtDtl.Rows(C1)("itemoid").ToString) & " AND refoid=" & Session("suppoid") & " AND res1='" & dtDtl.Rows(C1)("unitoid").ToString & "' AND itemgroup='" & dtDtl.Rows(C1)("itemgroup").ToString & "' AND refname='SUPPLIER' AND curroid=" & Session("curroid")
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() <= 0 Then
                                sSql = "INSERT INTO QL_mstitemprice (cmpcode, itempriceoid, itemoid, refoid, refname, totalpurchaseqty, totalsalesqty, avgpurchaseprice, lastpurchaseprice, avgsalesprice, lastsalesprice, lasttrans, lasttransoid, lasttransno, lasttransdate, note, res1, res2, res3, upduser, updtime, curroid, ratetoidr, itemgroup) VALUES ('" & AppCmpCode.Text & "', " & iPriceOid & ", " & ToDouble(dtDtl.Rows(C1)("itemoid").ToString) & ", " & Session("suppoid") & ", 'SUPPLIER', " & ToDouble(dtDtl.Rows(C1)("poqty").ToString) & ", 0, " & ToDouble(dtDtl.Rows(C1)("poprice").ToString) & ", " & ToDouble(dtDtl.Rows(C1)("poprice").ToString) & ", 0, 0, 'QL_trnpomst', " & Session("trnoid") & ", '" & sNo & "', '" & Session("trndate") & "', '', '" & dtDtl.Rows(C1)("unitoid").ToString & "', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Session("curroid") & ", " & cRate.GetRateMonthlyIDRValue & ", '" & dtDtl.Rows(C1)("itemgroup").ToString & "')"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iPriceOid += 1
                            End If
                        Next
                        sSql = "UPDATE QL_mstoid SET lastoid=" & iPriceOid - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_MSTITEMPRICE'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                End If
            End If

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
        sTaxType.Text = ""
    End Sub
#End Region

#Region "PO Work In Process (WIP)"
    Private Sub SetGVListPOWIP()
        Dim arHeader() As String = {"Draft No.", "PO Date", "Supplier", "PO Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "suppname", "pomstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "suppname", "pomstnote", "pototalamt", "pomstdiscamt", "povat", "poothercost", "pograndtotalamt", "curroid", "suppoid", "currcode", "poratetoidr", "upduser", "updtime", "potaxtype", "taxtype"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT po.cmpcode, po.pomstoid AS trnoid, po.pono, CONVERT(VARCHAR(10), po.podate, 101) AS trndate, s.suppname, po.pomstnote, (po.pototalamt - po.pototaldiscdtl) AS pototalamt, po.pomstdiscamt, po.povat, (po.podeliverycost + po.poothercost) AS poothercost, po.pograndtotalamt, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, po.curroid, po.suppoid, c.currcode, po.porate2toidr AS poratetoidr, po.upduser, po.updtime, (CASE WHEN po.potaxtype_inex='EXC' THEN 'E' WHEN po.potaxtype_inex='INC' THEN 'I' ELSE 'N' END ) AS potaxtype, potaxtype+'-'+potaxtype_inex AS taxtype FROM QL_trnpomst po INNER JOIN QL_mstsupp s ON po.suppoid=s.suppoid INNER JOIN QL_approval ap ON po.pomstoid=ap.oid AND ap.statusrequest='New' INNER JOIN QL_mstcurr c ON c.curroid=po.curroid WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_trnpowipmst' AND po.pomststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnpowipmst")
    End Sub

    Private Sub SetGVDataPOWIP(ByVal dtDataHeader As DataTable)
        Session("curroid") = gvList.SelectedDataKey.Item("curroid").ToString
        Session("suppoid") = gvList.SelectedDataKey.Item("suppoid").ToString
        sTaxType.Text = gvList.SelectedDataKey.Item("potaxtype").ToString
        lblreason.Visible = True : lblSeptReason.Visible = True : ActionReason.Visible = True
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "PO Date", "Supplier", "Tax Type", "PO Note", "Currency", "Rate To IDR", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "suppname", "taxtype", "pomstnote", "currcode", "poratetoidr", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S", "D", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N", "O", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            ElseIf arHidden(C1) = "O" Then
                If gvDataHeader.Rows(C1 - 1).Cells(2).Text = "IDR" Then
                    gvDataHeader.Rows(C1).Visible = False
                End If
            End If
        Next
        Dim arColNameList() As String = {"datacaption", "datasept", "datavalue", "dataempty"}
        Dim arColTypeList() As String = {"System.String", "System.String", "System.String", "System.String"}
        Dim dtDataFooter As DataTable = SetTableDetail("tblDataFooter", arColNameList, arColTypeList)
        Dim arCaptionF() As String = {"Total", "Header Disc.", "Tax", "Grand Total"}
        Dim arValueF() As String = {"pototalamt", "pomstdiscamt", "povat", "pograndtotalamt"}
        Dim arTypeF() As String = {"D", "D", "D", "D"}
        Dim arHiddenF() As String = {"N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaptionF.Length - 1
            dr = dtDataFooter.NewRow
            dr("datacaption") = arCaptionF(C1)
            dr("datasept") = ":"
            If arTypeF(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValueF(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValueF(C1)).ToString), 2)
            End If
            dr("dataempty") = ""
            dtDataFooter.Rows.Add(dr)
        Next
        gvDataFooter.DataSource = dtDataFooter
        gvDataFooter.DataBind()
        For C1 As Int16 = 0 To arHiddenF.Length - 1
            If arHiddenF(C1) = "Y" Then
                gvDataFooter.Rows(C1).Visible = False
            End If
        Next
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Font.Bold = True
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).ForeColor = Drawing.Color.Black
        Dim arHeader() As String = {"No.", "Item. Code", "Item. Desc", "Stock Qty", "Unit", "Qty", "Unit", "Last Price", "Price", "Amount", "Disc.", "Netto", "Note"}
        Dim arField() As String = {"podtlseq", "itemcode", "matshortdesc", "stockqty", "stockunit", "poqty", "unit", "lastpoprice", "poprice", "podtlamt", "podtldiscamt", "podtlnetto", "podtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT pod.podtlseq, pod.matoid, m.itemoid, m.itemcode, m.itemshortdescription AS matshortdesc, pod.poqty, g.gendesc AS unit, pod.poprice, pod.podtlamt, pod.podtldiscamt, pod.podtlnetto, pod.podtlnote, ISNULL((SELECT TOP 1 mp.lastpurchaseprice FROM QL_mstitemprice mp WHERE mp.cmpcode=po.cmpcode AND mp.itemoid=pod.matoid AND mp.refname='SUPPLIER' AND mp.curroid=po.curroid AND itemgroup='WIP' AND res1=g.genoid ORDER BY mp.updtime DESC), 0.0) AS lastpoprice, pod.pounitoid AS unitoid, po.pogroup AS itemgroup, ISNULL((SELECT SUM(saldoakhir) FROM QL_crdstock crd WHERE crd.refoid=m.itemoid AND crd.refname='WIP' AND periodacctg=po.periodacctg),0) AS stockqty, g1.gendesc AS stockunit FROM QL_trnpomst po INNER JOIN QL_trnpodtl pod ON po.pomstoid=pod.pomstoid INNER JOIN QL_mstitem m ON pod.matoid=m.itemoid INNER JOIN QL_mstgen g ON pod.pounitoid=g.genoid INNER JOIN QL_mstgen g1 ON g1.genoid=m.itemunit1 WHERE po.pomstoid=" & Session("trnoid") & " ORDER BY m.itemcode"
        Session("TblDtlPO") = ckon.ambiltabel(sSql, "QL_trnpodtl")
        gvData.DataSource = Session("TblDtlPO")
        gvData.DataBind()
    End Sub

    Private Function GeneratePOWIPNo() As String
        Dim sNo As String = "POWIP" & sTaxType.Text & "-" & Format(GetServerTime(), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(pono, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnpomst WHERE cmpcode='" & AppCmpCode.Text & "' AND pono LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdatePOWIP(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select PO WIP Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select PO WIP Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim cRate As New ClassRate
        If sStatus = "Approved" Then
            cRate.SetRateValue(ToInteger(Session("curroid")), Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        If sStatus = "Rejected" Or sStatus = "Revised" Then
            If ActionReason.Text = "" Then
                showMessage("Please Fill " & sStatus & " Reason First!", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim iPriceOid As Integer = GenerateID("QL_MSTITEMPRICE", CompnyCode)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        'CONNECTION TO GSMR
        Dim objTransB As SqlClient.SqlTransaction
        If connb.State = ConnectionState.Closed Then
            connb.Open()
        End If
        objTransB = connb.BeginTransaction()
        xCmdB.Transaction = objTransB

        Try
            ' Update PO
            If sStatus = "Approved" Then
                sSql = "UPDATE QL_trnpomst SET pono='" & sNo & "', pomststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, rateoid=" & cRate.GetRateDailyOid & ", rate2oid=" & cRate.GetRateMonthlyOid & ", poratetoidrchar='" & Left(cRate.GetRateDailyIDRValue.ToString, 50) & "', poratetousdchar='" & Left(cRate.GetRateDailyUSDValue.ToString, 50) & "', porate2toidrchar='" & Left(cRate.GetRateMonthlyIDRValue.ToString, 50) & "', porate2tousdchar='" & Left(cRate.GetRateMonthlyUSDValue.ToString, 50) & "' WHERE pomststatus='In Approval' AND pomstoid=" & Session("trnoid")
            Else
                sSql = "UPDATE QL_trnpomst SET pono='" & sNo & "', pomststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP " & IIf(sStatus = "Rejected", ", rejectreason='" & Tchar(ActionReason.Text) & "', rejectuser='" & Session("UserID") & "', rejecttime=CURRENT_TIMESTAMP", ", revisereason='" & Tchar(ActionReason.Text) & "', reviseuser='" & Session("UserID") & "', revisetime=CURRENT_TIMESTAMP") & " WHERE pomststatus='In Approval' AND pomstoid=" & Session("trnoid")
            End If
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If

            Dim iReason As String = ""
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnpowipmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Rejected" Then
                    'nothing
                ElseIf sStatus = "Approved" Then
                    If Not Session("TblDtlPO") Is Nothing Then
                        Dim dtDtl As DataTable = Session("TblDtlPO")
                        For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                            sSql = "UPDATE QL_mstitemprice SET totalpurchaseqty=totalpurchaseqty + " & ToDouble(dtDtl.Rows(C1)("poqty").ToString) & ", avgpurchaseprice=((avgpurchaseprice * totalpurchaseqty) + " & ToDouble(dtDtl.Rows(C1)("podtlamt").ToString) & ") / (totalpurchaseqty + " & ToDouble(dtDtl.Rows(C1)("poqty").ToString) & "), lastpurchaseprice=" & ToDouble(dtDtl.Rows(C1)("poprice").ToString) & ", lasttrans='QL_trnpomst', lasttransoid=" & Session("trnoid") & ", lasttransno='" & sNo & "', lasttransdate='" & Session("trndate") & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, ratetoidr=" & cRate.GetRateMonthlyIDRValue & " WHERE cmpcode='" & AppCmpCode.Text & "' AND itemoid=" & ToDouble(dtDtl.Rows(C1)("itemoid").ToString) & " AND refoid=" & Session("suppoid") & " AND res1='" & dtDtl.Rows(C1)("unitoid").ToString & "' AND itemgroup='" & dtDtl.Rows(C1)("itemgroup").ToString & "' AND refname='SUPPLIER' AND curroid=" & Session("curroid")
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() <= 0 Then
                                sSql = "INSERT INTO QL_mstitemprice (cmpcode, itempriceoid, itemoid, refoid, refname, totalpurchaseqty, totalsalesqty, avgpurchaseprice, lastpurchaseprice, avgsalesprice, lastsalesprice, lasttrans, lasttransoid, lasttransno, lasttransdate, note, res1, res2, res3, upduser, updtime, curroid, ratetoidr, itemgroup) VALUES ('" & AppCmpCode.Text & "', " & iPriceOid & ", " & ToDouble(dtDtl.Rows(C1)("itemoid").ToString) & ", " & Session("suppoid") & ", 'SUPPLIER', " & ToDouble(dtDtl.Rows(C1)("poqty").ToString) & ", 0, " & ToDouble(dtDtl.Rows(C1)("poprice").ToString) & ", " & ToDouble(dtDtl.Rows(C1)("poprice").ToString) & ", 0, 0, 'QL_trnpomst', " & Session("trnoid") & ", '" & sNo & "', '" & Session("trndate") & "', '', '" & dtDtl.Rows(C1)("unitoid").ToString & "', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Session("curroid") & ", " & cRate.GetRateMonthlyIDRValue & ", '" & dtDtl.Rows(C1)("itemgroup").ToString & "')"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iPriceOid += 1
                            End If
                        Next
                        sSql = "UPDATE QL_mstoid SET lastoid=" & iPriceOid - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_MSTITEMPRICE'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                End If
            End If

            'INSERT SO INTO GSMR WITH IN APPROVAL STATUS
            sSql = "select count(-1) from QL_trnpomst where pomstoid=" & Session("trnoid") & " and popartnerid='GSMR'"
            xCmd.CommandText = sSql
            If xCmd.ExecuteScalar > 0 Then
                If sStatus = "Approved" Then
                    If Session("TblDtlPO") IsNot Nothing Then
                        'GET LAST SO OID
                        sSql = "select ISNULL(lastoid,0) from QL_mstoid where tablename = 'QL_trnsomst' and cmpcode='" & CompnyCode & "'"
                        xCmdB.CommandText = sSql
                        Dim tsooid As Integer = xCmdB.ExecuteScalar + 1
                        If tsooid = Nothing Then
                            tsooid = 0
                        End If

                        'GET APPROVAL USER
                        sSql = "SELECT approvaluser, apppersontype, (case when upper(apppersontype)='FINAL' then 0 else apppersonlevel end) apppersonlevel FROM QL_approvalperson WHERE tablename='QL_trnsomst' AND activeflag='ACTIVE' AND cmpcode IN ('" & CompnyCode & "', '" & DDLBusUnit.SelectedValue & "') order by apppersontype desc,apppersonlevel asc"
                        xCmdB.CommandText = sSql
                        Dim tadapterapp As SqlDataAdapter = New SqlDataAdapter
                        Dim tdsetapp As New DataSet
                        tadapterapp.SelectCommand = xCmdB
                        tadapterapp.Fill(tdsetapp, "QL_approvalperson")
                        Dim objtableapp As DataTable = tdsetapp.Tables("QL_approvalperson")
                        'INSERT INTO APPROVAL TABLE
                        sSql = "select ISNULL(lastoid,0) from QL_mstoid where tablename = 'QL_approval' and cmpcode='" & CompnyCode & "'"
                        xCmdB.CommandText = sSql
                        Dim iAppOid As Integer = xCmdB.ExecuteScalar + 1
                        If iAppOid = Nothing Then
                            iAppOid = 0
                        End If
                        Dim tempAppCode As String = ""
                        For C1 As Integer = 0 To objtableapp.Rows.Count - 1
                            If C1 = 0 Then
                                tempAppCode = objtableapp.Rows(C1)("apppersonlevel").ToString
                            End If
                            sSql = "INSERT INTO QL_approval (cmpcode, approvaloid, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvalnote, posisiLevel) VALUES ('" & CompnyCode & "', " & iAppOid + C1 & ", 'SO-" & tsooid.ToString & "_" & (iAppOid + C1) & "', 'PRKP', '" & Date.Now.Date & "', 'New', 'QL_trnsomst', " & tsooid & ", 'In Approval', '" & objtableapp.Rows(C1)("apppersonlevel").ToString & "', '" & objtableapp.Rows(C1)("approvaluser").ToString & "', '1/1/1900', '','" & IIf(tempAppCode = objtableapp.Rows(C1)("apppersonlevel").ToString, "1", "") & "')"
                            xCmdB.CommandText = sSql
                            xCmdB.ExecuteNonQuery()
                        Next
                        sSql = "UPDATE QL_mstoid SET lastoid=" & iAppOid + objtableapp.Rows.Count - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_approval'"
                        xCmdB.CommandText = sSql
                        xCmdB.ExecuteNonQuery()

                        'SETUP SO
                        sSql = "select genoid from QL_mstgen where cmpcode='" & CompnyCode & "' AND gengroup='SOTYPE' and gendesc='TRADING' order by gendesc"
                        xCmdB.CommandText = sSql
                        Dim tsotype As Integer = xCmdB.ExecuteScalar
                        If tsotype = Nothing Then
                            tsotype = 0
                        End If
                        sSql = "SELECT getdate() FROM ql_mstoid"
                        xCmdB.CommandText = sSql
                        Dim tperiodacctg As String = GetDateToPeriodAcctg(xCmdB.ExecuteScalar)
                        sSql = "select genoid from QL_mstgen where cmpcode='" & CompnyCode & "' AND gengroup='DELIVTYPE' and gendesc='DIRECT DELIVERY' order by gendesc"
                        xCmdB.CommandText = sSql
                        Dim tsodelivtype As Integer = xCmdB.ExecuteScalar
                        If tsodelivtype = Nothing Then
                            tsodelivtype = 0
                        End If
                        sSql = "select genoid from QL_mstgen where cmpcode='" & CompnyCode & "' AND gengroup='PROMO' and gendesc='NONE' order by gendesc"
                        xCmdB.CommandText = sSql
                        Dim tsopromo As Integer = xCmdB.ExecuteScalar
                        If tsopromo = Nothing Then
                            tsopromo = 0
                        End If
                        sSql = "SELECT genoid FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='PAYMENT TYPE' AND activeflag='ACTIVE' and gendesc='30'"
                        xCmdB.CommandText = sSql
                        Dim tsopaytype As Integer = xCmdB.ExecuteScalar
                        If tsopaytype = Nothing Then
                            tsopaytype = 0
                        End If
                        sSql = "SELECT curroid FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' and currcode='IDR'"
                        xCmdB.CommandText = sSql
                        Dim tsocurr As Integer = xCmdB.ExecuteScalar
                        If tsocurr = Nothing Then
                            tsocurr = 0
                        End If
                        sSql = "SELECT TOP 1 rateoid FROM QL_mstrate WHERE cmpcode='" & CompnyCode & "' AND curroid=" & tsocurr & " AND '" & Date.Now.Date & " 00:00:00'>=ratedate AND '" & Date.Now.Date & " 00:00:00'<=ratetodate AND activeflag='ACTIVE' ORDER BY ratedate DESC, rateoid DESC"
                        xCmdB.CommandText = sSql
                        Dim trateoid As Integer = xCmdB.ExecuteScalar
                        If trateoid = Nothing Then
                            trateoid = 0
                        End If
                        sSql = "SELECT TOP 1 rateres2 FROM QL_mstrate WHERE cmpcode='" & CompnyCode & "' AND curroid=" & tsocurr & " AND '" & Date.Now.Date & " 00:00:00'>=ratedate AND '" & Date.Now.Date & " 00:00:00'<=ratetodate AND activeflag='ACTIVE' ORDER BY ratedate DESC, rateoid DESC"
                        xCmdB.CommandText = sSql
                        Dim temprateusd As String = xCmdB.ExecuteScalar
                        Dim trateusd As Decimal = 0.0
                        If temprateusd <> Nothing Then
                            If temprateusd <> "" Then
                                trateusd = CDbl(temprateusd)
                                trateusd = ToMaskEdit(trateusd, GetRoundValue(trateusd))
                            Else
                                trateusd = 0.0
                            End If
                        End If
                        Dim ttotalamt As Decimal = 0.0
                        Dim ttotaldpp As Decimal = 0.0
                        Dim ttotalnett As Decimal = 0.0
                        Dim tvattype As String = ""
                        Dim ttotaltax As Decimal = 0.0
                        sSql = "select pototalamt,pototalnetto,pograndtotalamt,potaxtype_inex,povat from QL_trnpomst where cmpcode='" & CompnyCode & "' and pomstoid=" & Session("trnoid") & ""
                        xCmd.CommandText = sSql
                        xreader = xCmd.ExecuteReader
                        While xreader.Read
                            ttotalamt = xreader("pototalamt")
                            ttotaldpp = xreader("pototalnetto")
                            ttotalnett = xreader("pograndtotalamt")
                            tvattype = xreader("potaxtype_inex")
                            If tvattype <> "INC" Then
                                tvattype = "NT"
                            End If
                            ttotaltax = xreader("povat")
                        End While
                        xreader.Close()
                        sSql = "select top 1 salescode from ql_mstperson p inner join QL_mstlevel g on g.leveloid =p.leveloid where upper(g.leveldesc)='SALES' and p.activeflag='ACTIVE'"
                        xCmdB.CommandText = sSql
                        Dim tsales As String = xCmdB.ExecuteScalar
                        If tsales = Nothing Then
                            tsales = ""
                        End If
                        sSql = "select custoid from QL_mstcust where activeflag='ACTIVE' and custpartner=1"
                        xCmdB.CommandText = sSql
                        Dim tcust As Integer = xCmdB.ExecuteScalar
                        If tcust = Nothing Then
                            tcust = 0
                        End If

                        'SO MST
                        sSql = "INSERT INTO QL_trnsomst (cmpcode, somstoid, periodacctg, sotype, sotype2, sodelivtype, somstpromo, somstrefno, sodate, sono, custoid, sopaytypeoid, curroid, rateoid, sototalamt, somstdisctype, somstdiscvalue, somstdiscamt, somstdpp, sototalnetto, sotaxtype, sotaxamt, somstnote, somststatus, somstres1, somstres2, somstres3, createuser, createtime, upduser, updtime, soratetoidr, sototalamtidr, somstdiscamtidr, sototalnettoidr, soratetousd, sototalamtusd, somstdiscamtusd, sototalnettousd, soetd, sosalescode, popartneroid, popartner2oid) VALUES ('" & CompnyCode & "', " & tsooid & ", '" & tperiodacctg & "', 'SO', " & tsotype & ", " & tsodelivtype & ", " & tsopromo & ", '" & Tchar(sNo) & "', '" & Date.Now.Date & "', '" & tsooid.ToString & "'," & tcust & ",  " & tsopaytype & ", " & tsocurr & ", " & trateoid & ", " & ttotalamt & ", 'P', 0.0, 0.0, " & ttotaldpp & ", " & ttotalnett & ", '" & tvattype & "', " & ttotaltax & ", '', 'In Approval', 'FG', '', '', 'PRKP', CURRENT_TIMESTAMP, 'PRKP', CURRENT_TIMESTAMP, 1.0, " & ttotalamt & ", 0.0, " & ttotaldpp & ", " & trateusd & ", " & ttotalamt * trateusd & ", 0.0, " & ttotalnett * trateusd & ", '" & Now.Date & "', '" & tsales & "', 0, " & Session("trnoid") & ")"
                        xCmdB.CommandText = sSql
                        xCmdB.ExecuteNonQuery()
                        sSql = "UPDATE QL_mstoid SET lastoid=" & tsooid & " WHERE tablename='QL_TRNSOMST' AND cmpcode='" & CompnyCode & "'"
                        xCmdB.CommandText = sSql
                        xCmdB.ExecuteNonQuery()

                        'SO DTL
                        sSql = "select ISNULL(lastoid,0) from QL_mstoid where tablename = 'QL_trnsodtl' and cmpcode='" & CompnyCode & "'"
                        xCmdB.CommandText = sSql
                        Dim tsodtloid As Integer = xCmdB.ExecuteScalar + 1
                        If tsodtloid = Nothing Then
                            tsodtloid = 0
                        End If
                        'GET TAX TYPE
                        sSql = "select potaxtype_inex from QL_trnpomst where pomstoid=" & Session("trnoid") & ""
                        xCmdB.CommandText = sSql
                        Dim tpotax As String = xCmdB.ExecuteScalar
                        If tpotax = Nothing Then
                            tpotax = ""
                        End If
                        sSql = "select a.matoid itemoid,a.poqty sodtlqty,a.pounitoid sodtlunitoid,a.poprice sodtlprice,(a.povarpriceidr*a.poqty) sodtlamt,a.podtldisctype sodtldisctype1,a.podtldiscvalue sodtldiscvalue1,a.podtldiscamt sodtldiscamt1,'P' sodtldisctype2,0.0 sodtldiscvalue2,0.0 sodtldiscamt2,'P' sodtldisctype3,0.0 sodtldiscvalue3,0.0 sodtldiscamt3,(a.povarpriceidr*a.poqty) sodtlnetto,'' sodtlnote,'' sodtlstatus,'' sodtlres1,'' sodtlres2,'' sodtlres3,'PRKP' upduser,CURRENT_TIMESTAMP updtime,a.popriceidr sopriceidr,(a.povarpriceidr*a.poqty) sodtlamtidr,(a.povarpriceidr*a.poqty) sodtlnettoidr,(a.popriceidr*" & trateusd & ") sopriceusd,(a.povarpriceidr*a.poqty*" & trateusd & ") sodtlamtusd,(a.povarpriceidr*a.poqty*" & trateusd & ") sodtlnettousd,a.poqty_unitkecil soqty_unitkecil,a.poqty_unitbesar soqty_unitbesar from QL_trnpodtl a inner join QL_trnpomst b on a.pomstoid=b.pomstoid where a.pomstoid=" & Session("trnoid") & " order by a.podtlseq"
                        xCmd.CommandText = sSql
                        Dim tadapter As SqlDataAdapter = New SqlDataAdapter
                        Dim tdset As New DataSet
                        tadapter.SelectCommand = xCmd
                        tadapter.Fill(tdset, "QL_trnsjjualmst")
                        Dim objtable As DataTable = tdset.Tables("QL_trnsjjualmst")

                        If objtable.Rows.Count > 0 Then
                            For c1 As Integer = 0 To objtable.Rows.Count - 1
                                sSql = "INSERT INTO QL_trnsodtl (cmpcode, sodtloid, somstoid, sodtlseq, itemoid, sodtlqty, sodtlunitoid, sodtlprice, sodtlamt, sodtldisctype1, sodtldiscvalue1, sodtldiscamt1, sodtldisctype2, sodtldiscvalue2, sodtldiscamt2, sodtldisctype3, sodtldiscvalue3, sodtldiscamt3, sodtlnetto, sodtlnote, sodtlstatus, sodtlres1, sodtlres2, sodtlres3, upduser, updtime, sopriceidr, sodtlamtidr, sodtlnettoidr, sopriceusd, sodtlamtusd, sodtlnettousd, soqty_unitkecil, soqty_unitbesar) VALUES ('" & CompnyCode & "', " & (c1 + tsodtloid) & ", " & tsooid & ",  " & c1 + 1 & ", " & objtable.Rows(c1).Item("itemoid") & ", " & ToDouble(objtable.Rows(c1).Item("sodtlqty").ToString) & ", " & objtable.Rows(c1).Item("sodtlunitoid") & ", " & ToDouble(objtable.Rows(c1).Item("sodtlprice").ToString) & ", " & ToDouble(objtable.Rows(c1).Item("sodtlamt").ToString) & ", '" & objtable.Rows(c1).Item("sodtldisctype1").ToString & "', " & ToDouble(objtable.Rows(c1).Item("sodtldiscvalue1").ToString) & ", " & ToDouble(objtable.Rows(c1).Item("sodtldiscamt1").ToString) & ", '" & objtable.Rows(c1).Item("sodtldisctype2").ToString & "', " & ToDouble(objtable.Rows(c1).Item("sodtldiscvalue2").ToString) & ", " & ToDouble(objtable.Rows(c1).Item("sodtldiscamt2").ToString) & ", '" & objtable.Rows(c1).Item("sodtldisctype3").ToString & "', " & ToDouble(objtable.Rows(c1).Item("sodtldiscvalue3").ToString) & ", " & ToDouble(objtable.Rows(c1).Item("sodtldiscamt3").ToString) & "," & ToDouble(objtable.Rows(c1).Item("sodtlnetto").ToString) & ", '" & Tchar(objtable.Rows(c1).Item("sodtlnote")) & "', '" & Tchar(objtable.Rows(c1).Item("sodtlstatus")) & "', '" & Tchar(objtable.Rows(c1).Item("sodtlres1")) & "', '" & Tchar(objtable.Rows(c1).Item("sodtlres2")) & "', '" & Tchar(objtable.Rows(c1).Item("sodtlres3")) & "', '" & Tchar(objtable.Rows(c1).Item("upduser")) & "', CURRENT_TIMESTAMP, " & ToDouble(objtable.Rows(c1).Item("sopriceidr").ToString) & ", " & ToDouble(objtable.Rows(c1).Item("sodtlamtidr").ToString) & ", " & ToDouble(objtable.Rows(c1).Item("sodtlnettoidr").ToString) & ", " & ToDouble(objtable.Rows(c1).Item("sopriceusd").ToString) & ", " & ToDouble(objtable.Rows(c1).Item("sodtlamtusd").ToString) & ", " & ToDouble(objtable.Rows(c1).Item("sodtlnettousd").ToString) & ", " & ToDouble(objtable.Rows(c1).Item("soqty_unitkecil").ToString) & ", " & ToDouble(objtable.Rows(c1).Item("soqty_unitbesar").ToString) & ")"
                                xCmdB.CommandText = sSql
                                xCmdB.ExecuteNonQuery()
                            Next
                            sSql = "UPDATE QL_mstoid SET lastoid=" & (objtable.Rows.Count - 1 + tsodtloid) & " WHERE tablename='QL_trnsodtl' AND cmpcode='" & CompnyCode & "' "
                            xCmdB.CommandText = sSql
                            xCmdB.ExecuteNonQuery()
                        End If
                    End If
                End If
            End If

            objTransB.Commit()
            connb.Close()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTransB.Rollback() : connb.Close()
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
        sTaxType.Text = ""
    End Sub
#End Region

#Region "PO Assets Approval"
    Private Sub SetGVListPOAssets()
        Dim arHeader() As String = {"Draft No.", "PO Date", "Supplier", "PO Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "suppname", "poassetmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "suppname", "poassetmstnote", "poassettotalamt", "poassetmstdiscamt", "poassetvat", "poassetothercost", "poassetgrandtotalamt", "curroid", "suppoid", "currcode", "poratetoidr", "upduser", "updtime"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT po.cmpcode, po.poassetmstoid AS trnoid, po.poassetno, CONVERT(VARCHAR(10), po.poassetdate, 101) AS trndate, s.suppname, po.poassetmstnote, (po.poassettotalamt - po.poassettotaldiscdtl) AS poassettotalamt, po.poassetmstdiscamt, po.poassetvat, (po.poassetdeliverycost + po.poassetothercost) AS poassetothercost, po.poassetgrandtotalamt, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, po.curroid, po.suppoid, c.currcode, po.poassetrate2toidr AS poratetoidr, po.upduser, po.updtime FROM QL_trnpoassetmst po INNER JOIN QL_mstsupp s ON po.suppoid=s.suppoid INNER JOIN QL_approval ap ON po.poassetmstoid=ap.oid AND ap.statusrequest='New' INNER JOIN QL_mstcurr c ON c.curroid=po.curroid WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_trnpoassetmst' AND po.poassetmststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnpoassetmst")
    End Sub

    Private Sub SetGVDataPOAssets(ByVal dtDataHeader As DataTable)
        Session("curroid") = gvList.SelectedDataKey.Item("curroid").ToString
        Session("suppoid") = gvList.SelectedDataKey.Item("suppoid").ToString
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "PO Date", "Supplier", "PO Note", "Currency", "Rate To IDR", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "suppname", "poassetmstnote", "currcode", "poratetoidr", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "D", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "O", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            ElseIf arHidden(C1) = "O" Then
                If gvDataHeader.Rows(C1 - 1).Cells(2).Text = "IDR" Then
                    gvDataHeader.Rows(C1).Visible = False
                End If
            End If
        Next
        Dim arColNameList() As String = {"datacaption", "datasept", "datavalue", "dataempty"}
        Dim arColTypeList() As String = {"System.String", "System.String", "System.String", "System.String"}
        Dim dtDataFooter As DataTable = SetTableDetail("tblDataFooter", arColNameList, arColTypeList)
        Dim arCaptionF() As String = {"Total", "Header Disc.", "Tax", "Other Cost", "Grand Total"}
        Dim arValueF() As String = {"poassettotalamt", "poassetmstdiscamt", "poassetvat", "poassetothercost", "poassetgrandtotalamt"}
        Dim arTypeF() As String = {"D", "D", "D", "D", "D"}
        Dim arHiddenF() As String = {"N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaptionF.Length - 1
            dr = dtDataFooter.NewRow
            dr("datacaption") = arCaptionF(C1)
            dr("datasept") = ":"
            If arTypeF(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValueF(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValueF(C1)).ToString), 2)
            End If
            dr("dataempty") = ""
            dtDataFooter.Rows.Add(dr)
        Next
        gvDataFooter.DataSource = dtDataFooter
        gvDataFooter.DataBind()
        For C1 As Int16 = 0 To arHiddenF.Length - 1
            If arHiddenF(C1) = "Y" Then
                gvDataFooter.Rows(C1).Visible = False
            End If
        Next
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Font.Bold = True
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).ForeColor = Drawing.Color.Black
        Dim arHeader() As String = {"No.", "Type", "Mat. Code", "Mat. Desc", "ETA", "Quantity", "Unit", "Last Price", "Price", "Amount", "Disc.", "Netto", "Note"}
        Dim arField() As String = {"poassetdtlseq", "poassetreftype", "poassetrefcode", "poassetrefshortdesc", "prarrdatereq", "poassetqty", "unit", "lastpoprice", "poassetprice", "poassetdtlamt", "poassetdtldiscamt", "poassetdtlnetto", "poassetdtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT pod.poassetdtlseq, pod.poassetreftype, (CASE pod.poassetreftype WHEN 'General' THEN (SELECT m.matgencode FROM QL_mstmatgen m WHERE m.matgenoid=pod.poassetrefoid) ELSE (SELECT m.sparepartcode FROM QL_mstsparepart m WHERE m.sparepartoid=pod.poassetrefoid) END) AS poassetrefcode, (CASE pod.poassetreftype WHEN 'General' THEN (SELECT m.matgenlongdesc FROM QL_mstmatgen m WHERE m.matgenoid=pod.poassetrefoid) ELSE (SELECT m.sparepartlongdesc FROM QL_mstsparepart m WHERE m.sparepartoid=pod.poassetrefoid) END) AS poassetrefshortdesc, pod.poassetqty, g.gendesc AS unit, pod.poassetprice, pod.poassetdtlamt, pod.poassetdtldiscamt, pod.poassetdtlnetto, pod.poassetdtlnote, pod.poassetrefoid, CONVERT(VARCHAR(10), prassetarrdatereq, 101) AS prarrdatereq, ISNULL((CASE pod.poassetreftype WHEN 'General' THEN (SELECT TOP 1 mp.lastpurchaseprice FROM QL_mstmatgenprice mp WHERE mp.cmpcode=po.cmpcode AND mp.matgenoid=pod.poassetrefoid AND mp.refname='SUPPLIER' AND mp.curroid=po.curroid ORDER BY mp.updtime DESC) ELSE (SELECT TOP 1 mp.lastpurchaseprice FROM QL_mstsparepartprice mp WHERE mp.cmpcode=po.cmpcode AND mp.sparepartoid=pod.poassetrefoid AND mp.refname='SUPPLIER' AND mp.curroid=po.curroid ORDER BY mp.updtime DESC) END), 0.0) AS lastpoprice FROM QL_trnpoassetmst po INNER JOIN QL_trnpoassetdtl pod ON po.poassetmstoid=pod.poassetmstoid INNER JOIN QL_mstgen g ON pod.poassetunitoid=g.genoid INNER JOIN QL_prassetdtl prd ON prd.cmpcode=pod.cmpcode AND prd.prassetdtloid=pod.prassetdtloid WHERE po.poassetmstoid=" & Session("trnoid") & " ORDER BY poassetrefcode"
        Session("TblDtlPO") = ckon.ambiltabel(sSql, "QL_trnpodtl")
        gvData.DataSource = Session("TblDtlPO")
        gvData.DataBind()
    End Sub

    Private Function GeneratePOAssetsNo() As String
        Dim sNo As String = "POFA-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(poassetno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnpoassetmst WHERE cmpcode='" & AppCmpCode.Text & "' AND poassetno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdatePOAssets(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select PO Fixed Assets Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select PO Fixed Assets Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim cRate As New ClassRate
        If sStatus = "Approved" Then
            cRate.SetRateValue(ToInteger(Session("curroid")), Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim iPriceGenOid As Integer = GenerateID("QL_MSTMATGENPRICE", CompnyCode)
        Dim iPriceSPOid As Integer = GenerateID("QL_MSTSPAREPARTPRICE", CompnyCode)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update PO
            If sStatus = "Approved" Then
                sSql = "UPDATE QL_trnpoassetmst SET poassetno='" & sNo & "', poassetmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, rateoid=" & cRate.GetRateDailyOid & ", rate2oid=" & cRate.GetRateMonthlyOid & ", poassetratetoidrchar='" & Left(cRate.GetRateDailyIDRValue.ToString, 50) & "', poassetratetousdchar='" & Left(cRate.GetRateDailyUSDValue.ToString, 50) & "', poassetrate2toidrchar='" & Left(cRate.GetRateMonthlyIDRValue.ToString, 50) & "', poassetrate2tousdchar='" & Left(cRate.GetRateMonthlyUSDValue.ToString, 50) & "' WHERE poassetmststatus='In Approval' AND poassetmstoid=" & Session("trnoid")
            Else
                sSql = "UPDATE QL_trnpoassetmst SET poassetno='" & sNo & "', poassetmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE poassetmststatus='In Approval' AND poassetmstoid=" & Session("trnoid")
            End If
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnpoassetmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Rejected" Then
                    sSql = "UPDATE QL_prassetdtl SET prassetdtlstatus='' WHERE cmpcode='" & AppCmpCode.Text & "' AND prassetdtloid IN (SELECT prassetdtloid FROM QL_trnpoassetdtl WHERE cmpcode='" & AppCmpCode.Text & "' AND poassetmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_prassetmst SET prassetmststatus='Approved' WHERE cmpcode='" & AppCmpCode.Text & "' AND prassetmstoid IN (SELECT DISTINCT prassetmstoid FROM QL_trnpoassetdtl WHERE cmpcode='" & AppCmpCode.Text & "' AND poassetmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                ElseIf sStatus = "Approved" Then
                    If Not Session("TblDtlPO") Is Nothing Then
                        Dim dtDtl As DataTable = Session("TblDtlPO")
                        For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                            If dtDtl.Rows(C1)("poassetreftype").ToString.ToUpper = "GENERAL" Then
                                sSql = "UPDATE QL_mstmatgenprice SET totalpurchaseqty=totalpurchaseqty + " & ToDouble(dtDtl.Rows(C1)("poassetqty").ToString) & ", avgpurchaseprice=((avgpurchaseprice * totalpurchaseqty) + " & ToDouble(dtDtl.Rows(C1)("poassetdtlamt").ToString) & ") / (totalpurchaseqty + " & ToDouble(dtDtl.Rows(C1)("poassetqty").ToString) & "), lastpurchaseprice=" & ToDouble(dtDtl.Rows(C1)("poassetprice").ToString) & ", lasttrans='QL_trnpoassetmst', lasttransoid=" & Session("trnoid") & ", lasttransno='" & sNo & "', lasttransdate='" & Session("trndate") & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, ratetoidr=" & cRate.GetRateMonthlyIDRValue & " WHERE cmpcode='" & AppCmpCode.Text & "' AND matgenoid=" & ToDouble(dtDtl.Rows(C1)("poassetrefoid").ToString) & " AND refoid=" & Session("suppoid") & " AND refname='SUPPLIER' AND curroid=" & Session("curroid")
                                xCmd.CommandText = sSql
                                If xCmd.ExecuteNonQuery() <= 0 Then
                                    sSql = "INSERT INTO QL_mstmatgenprice (cmpcode, matgenpriceoid, matgenoid, refoid, refname, totalpurchaseqty, totalsalesqty, avgpurchaseprice, lastpurchaseprice, avgsalesprice, lastsalesprice, lasttrans, lasttransoid, lasttransno, lasttransdate, note, res1, res2, res3, upduser, updtime, curroid, ratetoidr) VALUES ('" & AppCmpCode.Text & "', " & iPriceGenOid & ", " & ToDouble(dtDtl.Rows(C1)("poassetrefoid").ToString) & ", " & Session("suppoid") & ", 'SUPPLIER', " & ToDouble(dtDtl.Rows(C1)("poassetqty").ToString) & ", 0, " & ToDouble(dtDtl.Rows(C1)("poassetprice").ToString) & ", " & ToDouble(dtDtl.Rows(C1)("poassetprice").ToString) & ", 0, 0, 'QL_trnpoassetmst', " & Session("trnoid") & ", '" & sNo & "', '" & Session("trndate") & "', '', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Session("curroid") & ", " & cRate.GetRateMonthlyIDRValue & ")"
                                    xCmd.CommandText = sSql
                                    xCmd.ExecuteNonQuery()
                                    iPriceGenOid += 1
                                End If
                            Else
                                sSql = "UPDATE QL_mstsparepartprice SET totalpurchaseqty=totalpurchaseqty + " & ToDouble(dtDtl.Rows(C1)("poassetqty").ToString) & ", avgpurchaseprice=((avgpurchaseprice * totalpurchaseqty) + " & ToDouble(dtDtl.Rows(C1)("poassetdtlamt").ToString) & ") / (totalpurchaseqty + " & ToDouble(dtDtl.Rows(C1)("poassetqty").ToString) & "), lastpurchaseprice=" & ToDouble(dtDtl.Rows(C1)("poassetprice").ToString) & ", lasttrans='QL_trnpoassetmst', lasttransoid=" & Session("trnoid") & ", lasttransno='" & sNo & "', lasttransdate='" & Session("trndate") & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, ratetoidr=" & cRate.GetRateMonthlyIDRValue & " WHERE cmpcode='" & AppCmpCode.Text & "' AND sparepartoid=" & ToDouble(dtDtl.Rows(C1)("poassetrefoid").ToString) & " AND refoid=" & Session("suppoid") & " AND refname='SUPPLIER' AND curroid=" & Session("curroid")
                                xCmd.CommandText = sSql
                                If xCmd.ExecuteNonQuery() <= 0 Then
                                    sSql = "INSERT INTO QL_mstsparepartprice (cmpcode, sparepartpriceoid, sparepartoid, refoid, refname, totalpurchaseqty, totalsalesqty, avgpurchaseprice, lastpurchaseprice, avgsalesprice, lastsalesprice, lasttrans, lasttransoid, lasttransno, lasttransdate, note, res1, res2, res3, upduser, updtime, curroid, ratetoidr) VALUES ('" & AppCmpCode.Text & "', " & iPriceSPOid & ", " & ToDouble(dtDtl.Rows(C1)("poassetrefoid").ToString) & ", " & Session("suppoid") & ", 'SUPPLIER', " & ToDouble(dtDtl.Rows(C1)("poassetqty").ToString) & ", 0, " & ToDouble(dtDtl.Rows(C1)("poassetprice").ToString) & ", " & ToDouble(dtDtl.Rows(C1)("poassetprice").ToString) & ", 0, 0, 'QL_trnpoassetmst', " & Session("trnoid") & ", '" & sNo & "', '" & Session("trndate") & "', '', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Session("curroid") & ", " & cRate.GetRateMonthlyIDRValue & ")"
                                    xCmd.CommandText = sSql
                                    xCmd.ExecuteNonQuery()
                                    iPriceSPOid += 1
                                End If
                            End If
                        Next
                        sSql = "UPDATE QL_mstoid SET lastoid=" & iPriceGenOid - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_MSTMATGENPRICE'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        sSql = "UPDATE QL_mstoid SET lastoid=" & iPriceSPOid - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_MSTSPAREPARTPRICE'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "PO Sawn Timber Approval"
    Private Sub SetGVListPOSawn()
        Dim arHeader() As String = {"Draft No.", "PO Date", "Supplier", "PO Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "suppname", "posawnmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "suppname", "posawnmstnote", "posawntotalamt", "posawnmstdiscamt", "posawnvat", "posawnothercost", "posawngrandtotalamt", "curroid", "suppoid", "currcode", "poratetoidr", "upduser", "updtime"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT po.cmpcode, po.posawnmstoid AS trnoid, po.posawnno, CONVERT(VARCHAR(10), po.posawndate, 101) AS trndate, s.suppname, po.posawnmstnote, (po.posawntotalamt - po.posawntotaldiscdtl) AS posawntotalamt, po.posawnmstdiscamt, po.posawnvat, (po.posawndeliverycost + po.posawnothercost) AS posawnothercost, po.posawngrandtotalamt, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, po.curroid, po.suppoid, c.currcode, po.posawnrate2toidr AS poratetoidr, po.upduser, po.updtime FROM QL_trnposawnmst po INNER JOIN QL_mstsupp s ON po.suppoid=s.suppoid INNER JOIN QL_approval ap ON po.posawnmstoid=ap.oid AND ap.statusrequest='New' INNER JOIN QL_mstcurr c ON c.curroid=po.curroid WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_trnposawnmst' AND po.posawnmststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnposawnmst")
    End Sub

    Private Sub SetGVDataPOSAWN(ByVal dtDataHeader As DataTable)
        Session("curroid") = gvList.SelectedDataKey.Item("curroid").ToString
        Session("suppoid") = gvList.SelectedDataKey.Item("suppoid").ToString
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "PO Date", "Supplier", "PO Note", "Currency", "Rate To IDR", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "suppname", "posawnmstnote", "currcode", "poratetoidr", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "D", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "O", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            ElseIf arHidden(C1) = "O" Then
                If gvDataHeader.Rows(C1 - 1).Cells(2).Text = "IDR" Then
                    gvDataHeader.Rows(C1).Visible = False
                End If
            End If
        Next
        Dim arColNameList() As String = {"datacaption", "datasept", "datavalue", "dataempty"}
        Dim arColTypeList() As String = {"System.String", "System.String", "System.String", "System.String"}
        Dim dtDataFooter As DataTable = SetTableDetail("tblDataFooter", arColNameList, arColTypeList)
        Dim arCaptionF() As String = {"Total", "Header Disc.", "Tax", "Other Cost", "Grand Total"}
        Dim arValueF() As String = {"posawntotalamt", "posawnmstdiscamt", "posawnvat", "posawnothercost", "posawngrandtotalamt"}
        Dim arTypeF() As String = {"D", "D", "D", "D", "D"}
        Dim arHiddenF() As String = {"N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaptionF.Length - 1
            dr = dtDataFooter.NewRow
            dr("datacaption") = arCaptionF(C1)
            dr("datasept") = ":"
            If arTypeF(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValueF(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValueF(C1)).ToString), 2)
            End If
            dr("dataempty") = ""
            dtDataFooter.Rows.Add(dr)
        Next
        gvDataFooter.DataSource = dtDataFooter
        gvDataFooter.DataBind()
        For C1 As Int16 = 0 To arHiddenF.Length - 1
            If arHiddenF(C1) = "Y" Then
                gvDataFooter.Rows(C1).Visible = False
            End If
        Next
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Font.Bold = True
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).ForeColor = Drawing.Color.Black
        Dim arHeader() As String = {"No.", "Mat. Code", "Mat. Desc", "ETA", "Quantity", "Unit", "Last Price", "Price", "Amount", "Disc.", "Netto", "Note"}
        Dim arField() As String = {"posawndtlseq", "matrawcode", "matrawshortdesc", "prarrdatereq", "posawnqty", "unit", "lastpoprice", "posawnprice", "posawndtlamt", "posawndtldiscamt", "posawndtlnetto", "posawndtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT pod.posawndtlseq, pod.matsawnoid, m.matrawcode, m.matrawlongdesc AS matrawshortdesc, pod.posawnqty, g.gendesc AS unit, pod.posawnprice, pod.posawndtlamt, pod.posawndtldiscamt, pod.posawndtlnetto, pod.posawndtlnote, CONVERT(VARCHAR(10), prsawnarrdatereq, 101) AS prarrdatereq, ISNULL((SELECT TOP 1 mp.lastpurchaseprice FROM QL_mstmatrawprice mp WHERE mp.cmpcode=po.cmpcode AND mp.matrawoid=pod.matsawnoid AND mp.refname='SUPPLIER' AND mp.curroid=po.curroid ORDER BY mp.updtime DESC), 0.0) AS lastpoprice FROM QL_trnposawnmst po INNER JOIN QL_trnposawndtl pod ON po.posawnmstoid=pod.posawnmstoid INNER JOIN QL_mstmatraw m ON pod.matsawnoid=m.matrawoid INNER JOIN QL_mstgen g ON pod.posawnunitoid=g.genoid INNER JOIN QL_prsawndtl prd ON prd.cmpcode=pod.cmpcode AND prd.prsawndtloid=pod.prsawndtloid WHERE po.posawnmstoid=" & Session("trnoid") & " ORDER BY pod.posawndtlseq"
        Session("TblDtlPO") = ckon.ambiltabel(sSql, "QL_trnpodtl")
        gvData.DataSource = Session("TblDtlPO")
        gvData.DataBind()
    End Sub

    Private Function GeneratePOSAWNNo() As String
        Dim sNo As String = "POST-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(posawnno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnposawnmst WHERE cmpcode='" & AppCmpCode.Text & "' AND posawnno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdatePOSAWN(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select PO Sawn Timber Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select PO Sawn Timber Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim cRate As New ClassRate
        If sStatus = "Approved" Then
            cRate.SetRateValue(ToInteger(Session("curroid")), Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim iPriceOid As Integer = GenerateID("QL_MSTMATRAWPRICE", CompnyCode)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update PO
            If sStatus = "Approved" Then
                sSql = "UPDATE QL_trnposawnmst SET posawnno='" & sNo & "', posawnmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, rateoid=" & cRate.GetRateDailyOid & ", rate2oid=" & cRate.GetRateMonthlyOid & ", posawnratetoidrchar='" & Left(cRate.GetRateDailyIDRValue.ToString, 50) & "', posawnratetousdchar='" & Left(cRate.GetRateDailyUSDValue.ToString, 50) & "', posawnrate2toidrchar='" & Left(cRate.GetRateMonthlyIDRValue.ToString, 50) & "', posawnrate2tousdchar='" & Left(cRate.GetRateMonthlyUSDValue.ToString, 50) & "' WHERE posawnmststatus='In Approval' AND posawnmstoid=" & Session("trnoid")
            Else
                sSql = "UPDATE QL_trnposawnmst SET posawnno='" & sNo & "', posawnmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE posawnmststatus='In Approval' AND posawnmstoid=" & Session("trnoid")
            End If
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnposawnmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Rejected" Then
                    sSql = "UPDATE QL_prsawndtl SET prsawndtlstatus='' WHERE cmpcode='" & AppCmpCode.Text & "' AND prsawndtloid IN (SELECT prsawndtloid FROM QL_trnposawndtl WHERE cmpcode='" & AppCmpCode.Text & "' AND posawnmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_prsawnmst SET prsawnmststatus='Approved' WHERE cmpcode='" & AppCmpCode.Text & "' AND prsawnmstoid IN (SELECT DISTINCT prsawnmstoid FROM QL_trnposawndtl WHERE cmpcode='" & AppCmpCode.Text & "' AND posawnmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                ElseIf sStatus = "Approved" Then
                    If Not Session("TblDtlPO") Is Nothing Then
                        Dim dtDtl As DataTable = Session("TblDtlPO")
                        For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                            sSql = "UPDATE QL_mstmatrawprice SET totalpurchaseqty=totalpurchaseqty + " & ToDouble(dtDtl.Rows(C1)("posawnqty").ToString) & ", avgpurchaseprice=((avgpurchaseprice * totalpurchaseqty) + " & ToDouble(dtDtl.Rows(C1)("posawndtlamt").ToString) & ") / (totalpurchaseqty + " & ToDouble(dtDtl.Rows(C1)("posawnqty").ToString) & "), lastpurchaseprice=" & ToDouble(dtDtl.Rows(C1)("posawnprice").ToString) & ", lasttrans='QL_trnposawnmst', lasttransoid=" & Session("trnoid") & ", lasttransno='" & sNo & "', lasttransdate='" & Session("trndate") & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, ratetoidr=" & cRate.GetRateMonthlyIDRValue & " WHERE cmpcode='" & AppCmpCode.Text & "' AND matrawoid=" & ToDouble(dtDtl.Rows(C1)("matsawnoid").ToString) & " AND refoid=" & Session("suppoid") & " AND refname='SUPPLIER' AND curroid=" & Session("curroid")
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() <= 0 Then
                                sSql = "INSERT INTO QL_mstmatrawprice (cmpcode, matrawpriceoid, matrawoid, refoid, refname, totalpurchaseqty, totalsalesqty, avgpurchaseprice, lastpurchaseprice, avgsalesprice, lastsalesprice, lasttrans, lasttransoid, lasttransno, lasttransdate, note, res1, res2, res3, upduser, updtime, curroid, ratetoidr) VALUES ('" & AppCmpCode.Text & "', " & iPriceOid & ", " & ToDouble(dtDtl.Rows(C1)("matsawnoid").ToString) & ", " & Session("suppoid") & ", 'SUPPLIER', " & ToDouble(dtDtl.Rows(C1)("posawnqty").ToString) & ", 0, " & ToDouble(dtDtl.Rows(C1)("posawnprice").ToString) & ", " & ToDouble(dtDtl.Rows(C1)("posawnprice").ToString) & ", 0, 0, 'QL_trnposawnmst', " & Session("trnoid") & ", '" & sNo & "', '" & Session("trndate") & "', '', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Session("curroid") & ", " & cRate.GetRateMonthlyIDRValue & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iPriceOid += 1
                            End If
                        Next
                        sSql = "UPDATE QL_mstoid SET lastoid=" & iPriceOid - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_MSTMATRAWPRICE'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "PO Service Approval"
    Private Sub SetGVListPOService()
        Dim arHeader() As String = {"Draft No.", "PO Date", "Supplier", "PO Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "suppname", "poservicemstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "suppname", "poservicemstnote", "poservicetotalamt", "poservicemstdiscamt", "poservicevat", "poservicegrandtotalamt", "curroid", "suppoid", "currcode", "poratetoidr", "upduser", "updtime"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT po.cmpcode, po.poservicemstoid AS trnoid, po.poserviceno, CONVERT(VARCHAR(10), po.poservicedate, 101) AS trndate, s.suppname, po.poservicemstnote, (po.poservicetotalamt - po.poservicetotaldiscdtl) AS poservicetotalamt, po.poservicemstdiscamt, po.poservicevat, po.poservicegrandtotalamt, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, po.curroid, po.suppoid, c.currcode, po.poservicerate2toidr AS poratetoidr, po.upduser, po.updtime FROM QL_trnposervicemst po INNER JOIN QL_mstsupp s ON po.suppoid=s.suppoid INNER JOIN QL_approval ap ON po.poservicemstoid=ap.oid AND ap.statusrequest='New' INNER JOIN QL_mstcurr c ON c.curroid=po.curroid WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_trnposervicemst' AND po.poservicemststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnposervicemst")
    End Sub

    Private Sub SetGVDataPOService(ByVal dtDataHeader As DataTable)
        Session("curroid") = gvList.SelectedDataKey.Item("curroid").ToString
        Session("suppoid") = gvList.SelectedDataKey.Item("suppoid").ToString
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "PO Date", "Supplier", "PO Note", "Currency", "Rate To IDR", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "suppname", "poservicemstnote", "currcode", "poratetoidr", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "D", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "O", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            ElseIf arHidden(C1) = "O" Then
                If gvDataHeader.Rows(C1 - 1).Cells(2).Text = "IDR" Then
                    gvDataHeader.Rows(C1).Visible = False
                End If
            End If
        Next
        Dim arColNameList() As String = {"datacaption", "datasept", "datavalue", "dataempty"}
        Dim arColTypeList() As String = {"System.String", "System.String", "System.String", "System.String"}
        Dim dtDataFooter As DataTable = SetTableDetail("tblDataFooter", arColNameList, arColTypeList)
        Dim arCaptionF() As String = {"Total", "Header Disc.", "Tax", "Grand Total"}
        Dim arValueF() As String = {"poservicetotalamt", "poservicemstdiscamt", "poservicevat", "poservicegrandtotalamt"}
        Dim arTypeF() As String = {"D", "D", "D", "D"}
        Dim arHiddenF() As String = {"N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaptionF.Length - 1
            dr = dtDataFooter.NewRow
            dr("datacaption") = arCaptionF(C1)
            dr("datasept") = ":"
            If arTypeF(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValueF(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValueF(C1)).ToString), 2)
            End If
            dr("dataempty") = ""
            dtDataFooter.Rows.Add(dr)
        Next
        gvDataFooter.DataSource = dtDataFooter
        gvDataFooter.DataBind()
        For C1 As Int16 = 0 To arHiddenF.Length - 1
            If arHiddenF(C1) = "Y" Then
                gvDataFooter.Rows(C1).Visible = False
            End If
        Next
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Font.Bold = True
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).ForeColor = Drawing.Color.Black
        Dim arHeader() As String = {"No.", "Service Code", "Service", "Last Price", "Price", "Note"}
        Dim arField() As String = {"poservicedtlseq", "servicecode", "serviceshortdesc", "lastpoprice", "poserviceprice", "poservicedtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT pod.poservicedtlseq, pod.serviceoid, m.servicecode, m.servicelongdesc AS serviceshortdesc, pod.poserviceprice, pod.poservicedtlnote, ISNULL((SELECT TOP 1 crd.lastprice FROM QL_crdservice crd WHERE crd.cmpcode=pod.cmpcode AND crd.serviceoid=pod.serviceoid AND crd.curroid=po.curroid ORDER BY crd.updtime DESC), 0.0) AS lastpoprice FROM QL_trnposervicemst po INNER JOIN QL_trnposervicedtl pod ON po.poservicemstoid=pod.poservicemstoid INNER JOIN QL_mstservice m ON pod.serviceoid=m.serviceoid WHERE po.poservicemstoid=" & Session("trnoid") & " ORDER BY pod.poservicedtlseq"
        Session("TblDtlPO") = ckon.ambiltabel(sSql, "QL_trnpodtl")
        gvData.DataSource = Session("TblDtlPO")
        gvData.DataBind()
    End Sub

    Private Function GeneratePOServiceNo() As String
        Dim sNo As String = "POSVC-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(poserviceno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnposervicemst WHERE cmpcode='" & AppCmpCode.Text & "' AND poserviceno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdatePOService(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select PO Service Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select PO Service Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim cRate As New ClassRate
        If sStatus = "Approved" Then
            cRate.SetRateValue(ToInteger(Session("curroid")), Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim iCrdServiceOid As Integer = GenerateID("QL_CRDSERVICE", CompnyCode)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update PO
            If sStatus = "Approved" Then
                sSql = "UPDATE QL_trnposervicemst SET poserviceno='" & sNo & "', poservicemststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, rateoid=" & cRate.GetRateDailyOid & ", rate2oid=" & cRate.GetRateMonthlyOid & ", poserviceratetoidrchar='" & Left(cRate.GetRateDailyIDRValue.ToString, 50) & "', poserviceratetousdchar='" & Left(cRate.GetRateDailyUSDValue.ToString, 50) & "', poservicerate2toidrchar='" & Left(cRate.GetRateMonthlyIDRValue.ToString, 50) & "', poservicerate2tousdchar='" & Left(cRate.GetRateMonthlyUSDValue.ToString, 50) & "' WHERE poservicemststatus='In Approval' AND poservicemstoid=" & Session("trnoid")
            Else
                sSql = "UPDATE QL_trnposervicemst SET poserviceno='" & sNo & "', poservicemststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE poservicemststatus='In Approval' AND poservicemstoid=" & Session("trnoid")
            End If
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnposervicemst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Approved" Then
                    If Not Session("TblDtlPO") Is Nothing Then
                        Dim dtDtl As DataTable = Session("TblDtlPO")
                        For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                            sSql = "UPDATE QL_crdservice SET avgprice=(avgprice + " & ToDouble(dtDtl.Rows(C1)("poserviceprice").ToString) & ") / 2, avgperiodprice=(avgperiodprice + " & ToDouble(dtDtl.Rows(C1)("poserviceprice").ToString) & ") / 2, lastprice=" & ToDouble(dtDtl.Rows(C1)("poserviceprice").ToString) & ", lasttransoid=" & Session("trnoid") & ", lasttranstype='QL_trnposervicemst', lasttransdate='" & Session("trndate") & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & AppCmpCode.Text & "' AND periodacctg='" & GetDateToPeriodAcctg(CDate(Session("trndate"))) & "' AND serviceoid=" & dtDtl.Rows(C1)("serviceoid").ToString & " AND suppoid=" & Session("suppoid") & " AND curroid=" & Session("curroid")
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() <= 0 Then
                                sSql = "INSERT INTO QL_crdservice (cmpcode, crdserviceoid, periodacctg, serviceoid, suppoid, avgprice, avgperiodprice, lastprice, lasttransoid, lasttranstype, lasttransdate, note, res1, res2, res3, upduser, updtime, curroid) VALUES ('" & AppCmpCode.Text & "', " & iCrdServiceOid & ", '" & GetDateToPeriodAcctg(CDate(Session("trndate"))) & "', " & dtDtl.Rows(C1)("serviceoid").ToString & ", " & Session("suppoid") & ", " & ToDouble(dtDtl.Rows(C1)("poserviceprice").ToString) & ", " & ToDouble(dtDtl.Rows(C1)("poserviceprice").ToString) & ", " & ToDouble(dtDtl.Rows(C1)("poserviceprice").ToString) & ", " & Session("trnoid") & ", 'QL_trnposervicemst', '" & Session("trndate") & "', '', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Session("curroid") & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iCrdServiceOid += 1
                            End If
                        Next
                        sSql = "UPDATE QL_mstoid SET lastoid=" & iCrdServiceOid - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_CRDSERVICE'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "PO Subcon Approval"
    Private Sub SetGVListPOSubcon()
        Dim arHeader() As String = {"Draft No.", "PO Date", "Supplier", "PO Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "suppname", "posubconmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "suppname", "posubconmstnote", "posubcontotalcost", "curroid", "suppoid", "currcode", "poratetoidr", "upduser", "updtime", "posubconref", "posubcontype"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT po.cmpcode, po.posubconmstoid AS trnoid, po.posubconno, CONVERT(VARCHAR(10), po.posubcondate, 101) AS trndate, s.suppname, po.posubconmstnote, po.posubcontotalcost, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, (SELECT DISTINCT curroid FROM QL_mstcurr WHERE currcode='IDR' AND activeflag='ACTIVE') AS curroid, po.suppoid, 'IDR' AS currcode, 0.0 AS poratetoidr, po.upduser, po.updtime, po.posubconref, po.posubcontype FROM QL_trnposubconmst po INNER JOIN QL_mstsupp s ON po.suppoid=s.suppoid INNER JOIN QL_approval ap ON po.posubconmstoid=ap.oid AND ap.statusrequest='New' WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_trnposubconmst' AND po.posubconmststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnposubconmst")
    End Sub

    Private Sub SetGVDataPOSubcon(ByVal dtDataHeader As DataTable)
        Session("curroid") = gvList.SelectedDataKey.Item("curroid").ToString
        Session("suppoid") = gvList.SelectedDataKey.Item("suppoid").ToString
        Session("posubconref") = gvList.SelectedDataKey.Item("posubconref").ToString
        Session("posubcontotalcost") = ToDouble(gvList.SelectedDataKey.Item("posubcontotalcost").ToString)
        Session("posubcontype") = gvList.SelectedDataKey.Item("posubcontype").ToString
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "PO Date", "Supplier", "PO Note", "Currency", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "suppname", "posubconmstnote", "currcode", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arColNameList() As String = {"datacaption", "datasept", "datavalue", "dataempty"}
        Dim arColTypeList() As String = {"System.String", "System.String", "System.String", "System.String"}
        Dim dtDataFooter As DataTable = SetTableDetail("tblDataFooter", arColNameList, arColTypeList)
        Dim arCaptionF() As String = {"Total Cost"}
        Dim arValueF() As String = {"posubcontotalcost"}
        Dim arTypeF() As String = {"D"}
        Dim arHiddenF() As String = {"N"}
        For C1 As Int16 = 0 To arCaptionF.Length - 1
            dr = dtDataFooter.NewRow
            dr("datacaption") = arCaptionF(C1)
            dr("datasept") = ":"
            If arTypeF(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValueF(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValueF(C1)).ToString), 2)
            End If
            dr("dataempty") = ""
            dtDataFooter.Rows.Add(dr)
        Next
        gvDataFooter.DataSource = dtDataFooter
        gvDataFooter.DataBind()
        For C1 As Int16 = 0 To arHiddenF.Length - 1
            If arHiddenF(C1) = "Y" Then
                gvDataFooter.Rows(C1).Visible = False
            End If
        Next
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Font.Bold = True
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).ForeColor = Drawing.Color.Black
        Dim arHeader() As String = {"No.", "Mat. Code", "Mat. Desc", "Quantity", "Unit", "Price", "Note"}
        Dim arField() As String = {"posubcondtl2seq", "subconcode2", "subconshortdesc2", "posubcondtl2qty", "unit2", "posubconcost", "posubcondtl2note"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT pod.posubcondtl2seq, pod.matrefoid AS matrefoid2, (CASE po.posubconref WHEN 'Raw' THEN (SELECT mx.matrawcode FROM QL_mstmatraw mx WHERE mx.matrawoid=pod.matrefoid) WHEN 'General' THEN (SELECT mx.matgencode FROM QL_mstmatgen mx WHERE mx.matgenoid=pod.matrefoid) ElSE (SELECT mx.sparepartcode FROM QL_mstsparepart mx WHERE mx.sparepartoid=pod.matrefoid) END) AS subconcode2, (CASE po.posubconref WHEN 'Raw' THEN (SELECT mx.matrawlongdesc FROM QL_mstmatraw mx WHERE mx.matrawoid=pod.matrefoid) WHEN 'General' THEN (SELECT mx.matgenlongdesc FROM QL_mstmatgen mx WHERE mx.matgenoid=pod.matrefoid) ElSE (SELECT mx.sparepartlongdesc FROM QL_mstsparepart mx WHERE mx.sparepartoid=pod.matrefoid) END) AS subconshortdesc2, (CASE po.posubconref WHEN 'Raw' THEN 'RAW MATERIAL' WHEN 'General' THEN 'GENERAL MATERIAL' ElSE 'SPARE PART' END) AS posubconref, pod.posubcondtl2qty, g.gendesc AS unit2, pod.posubconcost, pod.posubcondtl2note, posubconpropvalueidr, posubconpropvalueusd, posubcondtl2oid, posubcondtl1oid, pod.posubconcost * pod.posubcondtl2qty AS posubcondtlcost FROM QL_trnposubconmst po INNER JOIN QL_trnposubcondtl2 pod ON po.posubconmstoid=pod.posubconmstoid INNER JOIN QL_mstgen g ON pod.posubcondtl2unitoid=g.genoid WHERE po.posubconmstoid=" & Session("trnoid") & " ORDER BY pod.posubcondtl2seq"
        Session("TblDtlPO") = ckon.ambiltabel(sSql, "QL_trnpodtl")
        gvData.DataSource = Session("TblDtlPO")
        gvData.DataBind()
        sSql = "SELECT posubcondtl1oid, posubcondtl2oid, matrefoid, posubcondtl1qty, (CASE posubconref WHEN 'Raw' THEN 'RAW MATERIAL' WHEN 'General' THEN 'GENERAL MATERIAL' ELSE 'SPARE PART' END) AS posubconref, posubconwhoid, posubconvalueidr, posubconvalueusd FROM QL_trnposubcondtl1 pod INNER JOIN QL_trnposubconmst pom ON pom.cmpcode=pod.cmpcode AND pom.posubconmstoid=pod.posubconmstoid WHERE pod.posubconmstoid= " & Session("trnoid")
        Session("TblDtlPOOut") = ckon.ambiltabel(sSql, "QL_trnposubcondtl1")
    End Sub

    Private Function GeneratePOSubconNo() As String
        Dim sNo As String = "POSUB-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(posubconno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnposubconmst WHERE cmpcode='" & AppCmpCode.Text & "' AND posubconno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdatePOSubcon(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select PO Subcon Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select PO Subcon Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim cRate As New ClassRate
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
        Dim iGlMstOid As Int32 = GenerateID("QL_TRNGLMST", CompnyCode)
        Dim iGlDtlOid As Int32 = GenerateID("QL_TRNGLDTL", CompnyCode)
        Dim conmtroid As Integer = GenerateID("QL_CONMAT", CompnyCode)
        Dim crdmatoid As Integer = GenerateID("QL_CRDMTR", CompnyCode)
        Dim iStockWIPAcctgOid, iStockOutAcctgOid As Integer
        Dim objTable As DataTable = Session("TblDtlPOOut")
        If sStatus = "Approved" Then
            cRate.SetRateValue(ToInteger(Session("curroid")), Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            Dim sVarErr As String = ""
            If Not IsInterfaceExists("VAR_PO_SUBCON", AppCmpCode.Text) Then
                sVarErr = "VAR_PO_SUBCON"
            End If
            Dim sVarOut As String = ""
            If Session("posubconref").ToString.ToUpper = "RAW" Then
                sVarOut = "VAR_STOCK_RM"
            ElseIf Session("posubconref").ToString.ToUpper = "GENERAL" Then
                sVarOut = "VAR_STOCK_GM"
            Else
                sVarOut = "VAR_STOCK_SP"
            End If
            If Not IsInterfaceExists(sVarOut, AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & sVarOut
            End If
            If sVarErr <> "" Then
                showMessage(GetInterfaceWarning(sVarErr, "approved"), CompnyName & " - WARNING", 2)
                Exit Sub
            End If
            iStockWIPAcctgOid = GetAcctgOID(GetVarInterface("VAR_PO_SUBCON", AppCmpCode.Text), CompnyCode)
            iStockOutAcctgOid = GetAcctgOID(GetVarInterface(sVarOut, AppCmpCode.Text), CompnyCode)
            For C1 As Int16 = 0 To objTable.Rows.Count - 1
                If Not IsStockAvailable(AppCmpCode.Text, sPeriod, objTable.Rows(C1)("matrefoid"), objTable.Rows(C1)("posubconwhoid"), ToDouble(objTable.Rows(C1)("posubcondtl1qty").ToString), objTable.Rows(C1)("posubconref").ToString) Then
                    showMessage("Every Qty of Transformation Input must be less than Stock Qty!", CompnyName & " - WARNING", 2) : Exit Sub
                Else
                    objTable.Rows(C1)("posubconvalueidr") = ToDouble(GetStrData("SELECT SUM(ISNULL(stockqty, 0) * ISNULL(stockvalueidr, 0)) / SUM(ISNULL(stockqty, 0)) FROM QL_stockvalue WHERE cmpcode='" & AppCmpCode.Text & "' AND periodacctg IN ('" & sPeriod & "', '" & GetLastPeriod(sPeriod) & "') AND refoid=" & objTable.Rows(C1)("matrefoid") & " AND refname='" & objTable.Rows(C1)("posubconref").ToString & "' AND closeflag=''"))
                    objTable.Rows(C1)("posubconvalueusd") = ToDouble(GetStrData("SELECT SUM(ISNULL(stockqty, 0) * ISNULL(stockvalueusd, 0)) / SUM(ISNULL(stockqty, 0)) FROM QL_stockvalue WHERE cmpcode='" & AppCmpCode.Text & "' AND periodacctg IN ('" & sPeriod & "', '" & GetLastPeriod(sPeriod) & "') AND refoid=" & objTable.Rows(C1)("matrefoid") & " AND refname='" & objTable.Rows(C1)("posubconref").ToString & "' AND closeflag=''"))
                End If
            Next
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update PO
            If sStatus = "Approved" Then
                sSql = "UPDATE QL_trnposubconmst SET posubconno='" & sNo & "', posubconmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, posubcontotalcostusd=" & Session("posubcontotalcost") * cRate.GetRateMonthlyUSDValue & " WHERE posubconmststatus='In Approval' AND posubconmstoid=" & Session("trnoid")
            Else
                sSql = "UPDATE QL_trnposubconmst SET posubconno='" & sNo & "', posubconmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE posubconmststatus='In Approval' AND posubconmstoid=" & Session("trnoid")
            End If
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnposubconmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Approved" Then
                    Dim dTotalIDR As Double = 0, dTotalUSD As Double = 0
                    If Session("posubcontype") = "MO" Then
                        Dim dtDtl As DataTable = Session("TblDtlPO")
                        Dim dvDtl As DataView = objTable.DefaultView
                        For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                            dvDtl.RowFilter = "posubcondtl2oid=" & dtDtl.Rows(C1)("posubcondtl2oid")
                            Dim dTmpIDR As Double = 0, dTmpUSD As Double = 0
                            For C2 As Int16 = 0 To dvDtl.Count - 1
                                ' Update QL_trnposubcondtl1
                                sSql = "UPDATE QL_trnposubcondtl1 SET posubconvalueidr=" & ToDouble(dvDtl(C2)("posubconvalueidr").ToString) & ", posubconvalueusd=" & ToDouble(dvDtl(C2)("posubconvalueusd").ToString) & " WHERE posubcondtl1oid=" & dvDtl(C2)("posubcondtl1oid")
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                dTmpIDR += ToDouble(dvDtl(C2)("posubconvalueidr").ToString) * ToDouble(dvDtl(C2)("posubcondtl1qty").ToString)
                                dTmpUSD += ToDouble(dvDtl(C2)("posubconvalueusd").ToString) * ToDouble(dvDtl(C2)("posubcondtl1qty").ToString)
                                dTotalIDR += ToDouble(dvDtl(C2)("posubconvalueidr").ToString) * ToDouble(dvDtl(C2)("posubcondtl1qty").ToString)
                                dTotalUSD += ToDouble(dvDtl(C2)("posubconvalueusd").ToString) * ToDouble(dvDtl(C2)("posubcondtl1qty").ToString)
                                ' Insert QL_conmat
                                sSql = "INSERT INTO QL_conmat (cmpcode, conmtroid, type, typemin, trndate, periodacctg, formaction, formoid, refoid, refname, mtrwhoid, qtyin, qtyout, reason, note, upduser, updtime, valueidr, valueusd) VALUES ('" & AppCmpCode.Text & "', " & conmtroid & ", 'POSUB', -1, '" & sDate & "', '" & sPeriod & "', 'QL_trnposubcondtl1', " & dvDtl(C2)("posubcondtl1oid") & ", " & dvDtl(C2)("matrefoid") & ", '" & dvDtl(C2)("posubconref").ToString & "', " & dvDtl(C2)("posubconwhoid") & ", 0, " & ToDouble(dvDtl(C2)("posubcondtl1qty").ToString) & ", 'PO Subcon Transformation Input', '" & sNo & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dvDtl(C2)("posubconvalueidr").ToString) & ", " & ToDouble(dvDtl(C2)("posubconvalueusd").ToString) & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                conmtroid += 1
                                ' Update QL_crdmtr
                                sSql = "UPDATE QL_crdmtr SET qtyout=qtyout + " & ToDouble(dvDtl(C2)("posubcondtl1qty").ToString) & ", saldoakhir=saldoakhir - " & ToDouble(dvDtl(C2)("posubcondtl1qty").ToString) & ", lasttranstype='QL_trnposubcondtl1', lasttransdate='" & sDate & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & AppCmpCode.Text & "' AND refoid=" & dvDtl(C2)("matrefoid") & " AND refname='" & dvDtl(C2)("posubconref").ToString & "' AND mtrwhoid=" & dvDtl(C2)("posubconwhoid") & " AND periodacctg='" & sPeriod & "'"
                                xCmd.CommandText = sSql
                                If xCmd.ExecuteNonQuery() = 0 Then
                                    ' Insert QL_crdmtr
                                    sSql = "INSERT INTO QL_crdmtr (cmpcode, crdmatoid, periodacctg, refoid, refname, mtrwhoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir, lasttranstype, lasttransdate, upduser, updtime, createuser, createdate, closeuser, closingdate) VALUES ('" & AppCmpCode.Text & "', " & crdmatoid & ", '" & sPeriod & "', " & dvDtl(C2)("matrefoid") & ", '" & dvDtl(C2)("posubconref").ToString & "', " & dvDtl(C2)("posubconwhoid") & ", 0, " & ToDouble(dvDtl(C2)("posubcondtl1qty").ToString) & ", 0, 0, 0, " & -ToDouble(dvDtl(C2)("posubcondtl1qty").ToString) & ", 'QL_trnposubcondtl1', '" & sDate & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', '1/1/1900')"
                                    xCmd.CommandText = sSql
                                    xCmd.ExecuteNonQuery()
                                    crdmatoid += 1
                                End If
                            Next
                            dvDtl.RowFilter = ""
                            If dTmpIDR > 0 And dTmpUSD > 0 Then
                                sSql = "UPDATE QL_trnposubcondtl2 SET posubconpropvalueidr=" & dTmpIDR / ToDouble(dtDtl.Rows(C1)("posubcondtl2qty").ToString) & ", posubconpropvalueusd=" & dTmpUSD / ToDouble(dtDtl.Rows(C1)("posubcondtl2qty").ToString) & " WHERE posubcondtl2oid=" & dtDtl.Rows(C1)("posubcondtl2oid")
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                            End If
                        Next
                    Else
                        Dim dtDtl As DataTable = Session("TblDtlPO")
                        Dim dvDtl As DataView = dtDtl.DefaultView
                        For C1 As Int16 = 0 To objTable.Rows.Count - 1
                            Dim dTmpIDR As Double = 0, dTmpUSD As Double = 0
                            ' Update QL_trnposubcondtl1
                            sSql = "UPDATE QL_trnposubcondtl1 SET posubconvalueidr=" & ToDouble(objTable.Rows(C1)("posubconvalueidr").ToString) & ", posubconvalueusd=" & ToDouble(objTable.Rows(C1)("posubconvalueusd").ToString) & " WHERE posubcondtl1oid=" & objTable.Rows(C1)("posubcondtl1oid")
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            dTmpIDR = ToDouble(objTable.Rows(C1)("posubconvalueidr").ToString) * ToDouble(objTable.Rows(C1)("posubcondtl1qty").ToString)
                            dTmpUSD = ToDouble(objTable.Rows(C1)("posubconvalueusd").ToString) * ToDouble(objTable.Rows(C1)("posubcondtl1qty").ToString)
                            dTotalIDR += ToDouble(objTable.Rows(C1)("posubconvalueidr").ToString) * ToDouble(objTable.Rows(C1)("posubcondtl1qty").ToString)
                            dTotalUSD += ToDouble(objTable.Rows(C1)("posubconvalueusd").ToString) * ToDouble(objTable.Rows(C1)("posubcondtl1qty").ToString)
                            ' Insert QL_conmat
                            sSql = "INSERT INTO QL_conmat (cmpcode, conmtroid, type, typemin, trndate, periodacctg, formaction, formoid, refoid, refname, mtrwhoid, qtyin, qtyout, reason, note, upduser, updtime, valueidr, valueusd) VALUES ('" & AppCmpCode.Text & "', " & conmtroid & ", 'POSUB', -1, '" & sDate & "', '" & sPeriod & "', 'QL_trnposubcondtl1', " & objTable.Rows(C1)("posubcondtl1oid") & ", " & objTable.Rows(C1)("matrefoid") & ", '" & objTable.Rows(C1)("posubconref").ToString & "', " & objTable.Rows(C1)("posubconwhoid") & ", 0, " & ToDouble(objTable.Rows(C1)("posubcondtl1qty").ToString) & ", 'PO Subcon Transformation Input', '" & sNo & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(C1)("posubconvalueidr").ToString) & ", " & ToDouble(objTable.Rows(C1)("posubconvalueusd").ToString) & ")"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            conmtroid += 1
                            ' Update QL_crdmtr
                            sSql = "UPDATE QL_crdmtr SET qtyout=qtyout + " & ToDouble(objTable.Rows(C1)("posubcondtl1qty").ToString) & ", saldoakhir=saldoakhir - " & ToDouble(objTable.Rows(C1)("posubcondtl1qty").ToString) & ", lasttranstype='QL_trnposubcondtl1', lasttransdate='" & sDate & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & AppCmpCode.Text & "' AND refoid=" & objTable.Rows(C1)("matrefoid") & " AND refname='" & objTable.Rows(C1)("posubconref").ToString & "' AND mtrwhoid=" & objTable.Rows(C1)("posubconwhoid") & " AND periodacctg='" & sPeriod & "'"
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() = 0 Then
                                ' Insert QL_crdmtr
                                sSql = "INSERT INTO QL_crdmtr (cmpcode, crdmatoid, periodacctg, refoid, refname, mtrwhoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir, lasttranstype, lasttransdate, upduser, updtime, createuser, createdate, closeuser, closingdate) VALUES ('" & AppCmpCode.Text & "', " & crdmatoid & ", '" & sPeriod & "', " & objTable.Rows(C1)("matrefoid") & ", '" & objTable.Rows(C1)("posubconref").ToString & "', " & objTable.Rows(C1)("posubconwhoid") & ", 0, " & ToDouble(objTable.Rows(C1)("posubcondtl1qty").ToString) & ", 0, 0, 0, " & -ToDouble(objTable.Rows(C1)("posubcondtl1qty").ToString) & ", 'QL_trnposubcondtl1', '" & sDate & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', '1/1/1900')"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                crdmatoid += 1
                            End If
                            If dTmpIDR > 0 And dTmpUSD > 0 Then
                                Dim dTotalCost As Double = ToDouble(dtDtl.Compute("SUM(posubcondtlcost)", "posubcondtl1oid=" & objTable.Rows(C1)("posubcondtl1oid")).ToString)
                                dvDtl.RowFilter = "posubcondtl1oid=" & objTable.Rows(C1)("posubcondtl1oid")
                                For C2 As Integer = 0 To dvDtl.Count - 1
                                    Dim dPropIDR As Double = Math.Round((ToDouble(dvDtl(C2)("posubconcost").ToString) * dTmpIDR) / dTotalCost, 2)
                                    Dim dPropUSD As Double = Math.Round((ToDouble(dvDtl(C2)("posubconcost").ToString) * dTmpUSD) / dTotalCost, 2)
                                    sSql = "UPDATE QL_trnposubcondtl2 SET posubconpropvalueidr=" & dPropIDR & ", posubconpropvalueusd=" & dPropUSD & " WHERE posubcondtl2oid=" & dvDtl(C2)("posubcondtl2oid")
                                    xCmd.CommandText = sSql
                                    xCmd.ExecuteNonQuery()
                                Next
                                dvDtl.RowFilter = ""
                            End If
                        Next
                    End If
                    If dTotalIDR > 0 And dTotalUSD > 0 Then
                        ' Insert QL_trnglmst
                        sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & AppCmpCode.Text & "', " & iGlMstOid & ", '" & sDate & "', '" & sPeriod & "', 'PO Subcon|No. " & sNo & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 0, 0, 1, 1, " & dTotalUSD / dTotalIDR & ", " & dTotalUSD / dTotalIDR & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        ' Insert QL_trngldtl
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", 1, " & iGlMstOid & ", " & iStockWIPAcctgOid & ", 'D', " & dTotalIDR & ", '" & sNo & "', 'PO Subcon|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalIDR & ", " & dTotalUSD & ", 'QL_trnposubconmst " & Session("trnoid") & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", 2, " & iGlMstOid & ", " & iStockOutAcctgOid & ", 'C', " & dTotalIDR & ", '" & sNo & "', 'PO Subcon|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalIDR & ", " & dTotalUSD & ", 'QL_trnposubconmst " & Session("trnoid") & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                    sSql = "UPDATE QL_mstoid SET lastoid=" & conmtroid - 1 & " WHERE tablename='QL_CONMAT' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & crdmatoid - 1 & " WHERE tablename='QL_CRDMTR' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "PI Raw Material Approval"
    Private Sub SetGVListAPRM()
        Dim arHeader() As String = {"Draft No.", "PI No.", "PI Date", "Supplier", "PI Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trnno", "trndate", "suppname", "aprawmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "suppname", "aprawmstnote", "aprawtotalamt", "aprawtotaldisc", "aprawtotaltax", "aprawgrandtotal", "potype", "appayment", "suppoid", "curroid", "upduser", "updtime", "trnno", "appaytypeoid", "apdatetakegiro", "apacctgoid", "trnbelidiscamtdtl", "trnbelidiscamt"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT ap.cmpcode, ap.trnbelimstoid AS trnoid, ap.trnbelino AS trnno, CONVERT(VARCHAR(10), ap.trnbelidate, 101) AS trndate, s.suppname, ap.trnbelimstnote aprawmstnote, ap.trnbeliamt aprawtotalamt, ap.trnbelidiscamt aprawtotaldisc, ap.trnbelimsttaxamt aprawtotaltax, ap.trnbeliamtnetto aprawgrandtotal, apr.approvaloid, apr.requestuser, CONVERT(VARCHAR(10), apr.requestdate, 101) AS requestdate, s.supptype AS potype, g.gendesc AS appayment, ap.suppoid, ap.curroid, ap.upduser, ap.updtime, ap.trnbelipaytype AS appaytypeoid, CONVERT(VARCHAR(10), ap.apdatetakegiro, 101) AS apdatetakegiro, s.apacctgoid, trnbelidiscamtdtl, trnbelidiscamt FROM QL_trnbelimst ap INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid INNER JOIN QL_approval apr ON ap.trnbelimstoid=apr.oid AND apr.statusrequest='New' INNER JOIN QL_mstgen g ON g.genoid=ap.trnbelipaytype WHERE apr.event='In Approval' AND apr.approvaluser='" & Session("UserID") & "' AND apr.tablename='QL_trnaprawmst' AND ap.trnbelimststatus='In Approval' ORDER BY apr.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnaprawmst")
    End Sub

    Private Sub SetGVDataAPRM(ByVal dtDataHeader As DataTable)
        Session("potype") = gvList.SelectedDataKey.Item("potype").ToString.ToUpper
        Session("appayment") = gvList.SelectedDataKey.Item("appayment").ToString
        Session("suppoid") = gvList.SelectedDataKey.Item("suppoid").ToString
        Session("curroid") = gvList.SelectedDataKey.Item("curroid").ToString
        Session("aprawtotalamt") = gvList.SelectedDataKey.Item("aprawtotalamt").ToString
        Session("aprawtotaltax") = gvList.SelectedDataKey.Item("aprawtotaltax").ToString
        Session("aprawgrandtotal") = gvList.SelectedDataKey.Item("aprawgrandtotal").ToString
        Session("apno") = gvList.SelectedDataKey.Item("trnno").ToString
        Session("appaytypeoid") = gvList.SelectedDataKey.Item("appaytypeoid").ToString
        Session("apdatetakegiro") = gvList.SelectedDataKey.Item("apdatetakegiro").ToString
        Session("apacctgoid") = gvList.SelectedDataKey.Item("apacctgoid").ToString
        Session("trnbelidiscamtdtl") = gvList.SelectedDataKey.Item("trnbelidiscamtdtl").ToString
        lblreason.Visible = True : lblSeptReason.Visible = True : ActionReason.Visible = True
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "PI No.", "PI Date", "Supplier", "PI Note", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trnno", "trndate", "suppname", "aprawmstnote", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arColNameList() As String = {"datacaption", "datasept", "datavalue", "dataempty"}
        Dim arColTypeList() As String = {"System.String", "System.String", "System.String", "System.String"}
        Dim dtDataFooter As DataTable = SetTableDetail("tblDataFooter", arColNameList, arColTypeList)
        Dim arCaptionF() As String = {"Total Amount", "Disc Detail", "Disc Header", "Total Tax", "Grand Total"}
        Dim arValueF() As String = {"aprawtotalamt", "trnbelidiscamtdtl", "trnbelidiscamt", "aprawtotaltax", "aprawgrandtotal"}
        Dim arTypeF() As String = {"D", "D", "D", "D", "D"}
        Dim arHiddenF() As String = {"N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaptionF.Length - 1
            dr = dtDataFooter.NewRow
            dr("datacaption") = arCaptionF(C1)
            dr("datasept") = ":"
            If arTypeF(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValueF(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValueF(C1)).ToString), 2)
            End If
            dr("dataempty") = ""
            dtDataFooter.Rows.Add(dr)
        Next
        gvDataFooter.DataSource = dtDataFooter
        gvDataFooter.DataBind()
        For C1 As Int16 = 0 To arHiddenF.Length - 1
            If arHiddenF(C1) = "Y" Then
                gvDataFooter.Rows(C1).Visible = False
            End If
        Next
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Font.Bold = True
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).ForeColor = Drawing.Color.Black
        Dim arHeader() As String = {"No.", "Mat. Desc", "Quantity", "Unit", "Price", "Amount", "Disc.", "Netto", "Note"}
        Dim arField() As String = {"aprawdtlseq", "matrawshortdesc", "aprawqty", "unit", "aprawprice", "aprawdtlamt", "aprawdtldiscamt", "aprawdtlnetto", "aprawdtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT apd.trnbelidtlseq AS aprawdtlseq, apd.itemoid AS matrawoid, m.itemLongDescription AS matrawshortdesc, apd.trnbelidtlqty AS aprawqty, g.gendesc AS unit, apd.trnbelidtlprice AS aprawprice, apd.trnbelidtlamt AS aprawdtlamt, apd.trnbelidtldiscamt AS aprawdtldiscamt, apd.trnbelidtlamtnetto AS aprawdtlnetto, apd.trnbelidtlnote AS aprawdtlnote, (apd.trnbelidtlqty * mrd.mrvalueidr) AS mrrawamtidr, (apd.trnbelidtlqty * mrd.mrvalueusd) AS mrrawamtusd, (apd.trnbelidtlqty * mrd.mrvalue) AS mrrawamt FROM QL_trnbelimst ap INNER JOIN QL_trnbelidtl apd ON ap.trnbelimstoid=apd.trnbelimstoid INNER JOIN QL_mstitem m ON apd.itemoid=m.itemoid INNER JOIN QL_mstgen g ON apd.trnbelidtlunitoid=g.genoid INNER JOIN QL_trnmrdtl mrd ON mrd.mrdtloid=apd.mrdtloid WHERE ap.trnbelimstoid=" & Session("trnoid") & " ORDER BY apd.trnbelidtlseq"
        Session("TblDtlAPRaw") = ckon.ambiltabel(sSql, "QL_trnaprawdtl")
        gvData.DataSource = Session("TblDtlAPRaw")
        gvData.DataBind()
    End Sub

    Private Function GenerateAPRMNo() As String
        Dim sNo As String = "PIRM-" & Format(GetServerTime(), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trnbelino, " & DefaultFormatCounter & ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnbelimst WHERE cmpcode='" & AppCmpCode.Text & "' AND trnbelino LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdateAPRM(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select PI General Material Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select PI General Material Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        If sStatus = "Rejected" Or sStatus = "Revised" Then
            If ActionReason.Text = "" Then
                showMessage("Please Fill " & sStatus & " Reason First!", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim iConAPOid As Int32 = GenerateID("QL_CONAP", CompnyCode)
        Dim iGlMstOid As Int32 = GenerateID("QL_TRNGLMST", CompnyCode)
        Dim iGlDtlOid As Int32 = GenerateID("QL_TRNGLDTL", CompnyCode)
        Dim iAPAcctgOid, iRecAcctgOid, iPPNAcctgOid, iDiffIDRAcctgOid, iDiffUSDAcctgOid, iRoundAcctgOid As Integer
        Dim dDueDate As Date = DateAdd(DateInterval.Day, ToDouble(GetNumericValues(Session("appayment"))), CDate(Session("trndate")))
        Dim iSeq As Int16 = 1
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
        Dim cRate As New ClassRate
        sNo = Session("apno")
        Dim dtImport As DataTable = Nothing
        Dim iICSeq As Integer = 1
        If GetStrData(sSql) <> "" Then
            iICSeq = ToInteger(GetStrData(sSql))
        End If
        If sStatus = "Approved" Then
            cRate.SetRateValue(CInt(Session("curroid")), sDate)
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            Dim sVarErr As String = ""
            If Not IsInterfaceExists("VAR_PURC_RECEIVED", AppCmpCode.Text) Then
                sVarErr = "VAR_PURC_RECEIVED"
            End If
            If Not IsInterfaceExists("VAR_PPN_IN", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_PPN_IN"
            End If
            If Not IsInterfaceExists("VAR_DIFF_CURR_IDR", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_DIFF_CURR_IDR"
            End If
            If Not IsInterfaceExists("VAR_DIFF_CURR_USD", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_DIFF_CURR_USD"
            End If
            If Not IsInterfaceExists("VAR_ROUNDING", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_ROUNDING"
            End If
            If Session("potype").ToString.ToUpper = "LOCAL" Then
                If Not IsInterfaceExists("VAR_AP_LOCAL", AppCmpCode.Text) Then
                    sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_AP_LOCAL"
                End If
            Else
                If Not IsInterfaceExists("VAR_AP_IMPORT", AppCmpCode.Text) Then
                    sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_AP_IMPORT"
                End If
            End If
            If sVarErr <> "" Then
                showMessage(GetInterfaceWarning(sVarErr, "approved"), CompnyName & " - WARNING", 2)
                Exit Sub
            End If
            iRecAcctgOid = GetAcctgOID(GetVarInterface("VAR_PURC_RECEIVED", AppCmpCode.Text), CompnyCode)
            iPPNAcctgOid = GetAcctgOID(GetVarInterface("VAR_PPN_IN", AppCmpCode.Text), CompnyCode)
            iDiffIDRAcctgOid = GetAcctgOID(GetVarInterface("VAR_DIFF_CURR_IDR", AppCmpCode.Text), CompnyCode)
            iDiffUSDAcctgOid = GetAcctgOID(GetVarInterface("VAR_DIFF_CURR_USD", AppCmpCode.Text), CompnyCode)
            iRoundAcctgOid = GetAcctgOID(GetVarInterface("VAR_ROUNDING", AppCmpCode.Text), CompnyCode)
            iAPAcctgOid = Session("apacctgoid")
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update PI
            sSql = "UPDATE QL_trnbelimst SET trnbelimststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP"
            If sStatus = "Approved" Then
                sSql &= ", rateoid=" & cRate.GetRateDailyOid & ", beliratetoidr='" & Left(cRate.GetRateDailyIDRValue.ToString, 50) & "', beliratetousd='" & Left(cRate.GetRateDailyUSDValue.ToString, 50) & "', rate2oid=" & cRate.GetRateMonthlyOid & ", belirate2toidr='" & Left(cRate.GetRateMonthlyIDRValue.ToString, 50) & "', belirate2tousd='" & Left(cRate.GetRateMonthlyUSDValue.ToString, 50) & "', trnbeliamtidr=trnbeliamt * " & cRate.GetRateMonthlyIDRValue & ", trnbeliamtusd=trnbeliamt * " & cRate.GetRateMonthlyUSDValue & ", trnbelimsttaxamtidr=trnbelimsttaxamt * " & cRate.GetRateMonthlyIDRValue & ", trnbelimsttaxamtusd=trnbelimsttaxamt * " & cRate.GetRateMonthlyUSDValue & ", trnbeliamtnettoidr=trnbeliamtnetto * " & cRate.GetRateMonthlyIDRValue & ", trnbeliamtnettousd=trnbeliamtnetto * " & cRate.GetRateMonthlyUSDValue & ""
            Else
                sSql = "UPDATE QL_trnbelimst SET trnbelimststatus='" & sStatus & "', " & IIf(sStatus = "Rejected", " rejectreason='" & Tchar(ActionReason.Text) & "', rejectuser='" & Session("UserID") & "', rejecttime=CURRENT_TIMESTAMP", " revisereason='" & Tchar(ActionReason.Text) & "', reviseuser='" & Session("UserID") & "', revisetime=CURRENT_TIMESTAMP") & ""
            End If
            sSql &= " WHERE trnbelimststatus='In Approval' AND trnbelimstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnaprawmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Rejected" Then
                    sSql = "UPDATE QL_trnmrdtl SET mrdtlstatus='' WHERE cmpcode='" & AppCmpCode.Text & "' AND mrdtloid IN (SELECT mrdtloid FROM QL_trnbelidtl WHERE cmpcode='" & AppCmpCode.Text & "' AND trnbelimstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnmrmst SET mrmststatus='Post' WHERE cmpcode='" & AppCmpCode.Text & "' AND mrmstoid IN (SELECT mrmstoid FROM QL_trnbelidtl WHERE cmpcode='" & AppCmpCode.Text & "' AND trnbelimstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                ElseIf sStatus = "Approved" Then
                    Dim dt As DataTable = Session("TblDtlAPRaw")
                    Dim dMRAmt As Double = ToDouble(dt.Compute("SUM(mrrawamtidr)", ""))
                    Dim dMRAmtIDR As Double = ToDouble(dt.Compute("SUM(mrrawamtidr)", "")) * cRate.GetRateMonthlyIDRValue
                    Dim dMRAmtUSD As Double = ToDouble(dt.Compute("SUM(mrrawamtidr)", "")) * cRate.GetRateMonthlyUSDValue

                    ' Insert CONAP
                    sSql = "INSERT INTO QL_conap (cmpcode, conapoid, reftype, refoid, payrefoid, suppoid, acctgoid, trnapstatus, trnaptype, trnapdate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans, amtbayar, trnapnote, trnapres1, trnapres2, trnapres3, createuser, createtime, upduser, updtime, amttransidr, amtbayaridr, amttransusd, amtbayarusd) VALUES ('" & AppCmpCode.Text & "', " & iConAPOid & ", 'QL_trnbelimst', " & Session("trnoid") & ", 0, " & Session("suppoid") & ", " & iAPAcctgOid & ", 'Post', 'APRM', '" & sDate & "', '" & sPeriod & "', 0, '1/1/1900', '', 0, '" & dDueDate & "', " & ToDouble(Session("aprawgrandtotal")) & ", 0, '', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(Session("aprawgrandtotal")) * cRate.GetRateMonthlyIDRValue & ", 0, " & ToDouble(Session("aprawgrandtotal")) * cRate.GetRateMonthlyUSDValue & ", 0)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iConAPOid += 1
                    Dim dTotalImportIDR As Double = 0, dTotalImportUSD As Double = 0, dTotalImport As Double = 0
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iConAPOid - 1 & " WHERE tablename='QL_CONAP' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Auto Jurnal Accounting
                    ' Insert GL MST
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & AppCmpCode.Text & "', " & iGlMstOid & ", '" & sDate & "', '" & sPeriod & "', 'PI Raw|No. " & sNo & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Insert GL DTL
                    Dim dRound As Double = (ToDouble(Session("aprawtotalamt")) - ToDouble(Session("trnbelidiscamtdtl"))) - dMRAmt

                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iRecAcctgOid & ", 'D', " & dMRAmt & ", '" & sNo & "', 'PI Raw|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dMRAmtIDR & ", " & dMRAmtUSD & ", 'QL_trnbelimst " & Session("trnoid") & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1
                    iSeq += 1
                    If ToDouble(Session("aprawtotaltax")) > 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iPPNAcctgOid & ", 'D', " & ToDouble(Session("aprawtotaltax")) & ", '" & sNo & "', 'PI Raw|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(Session("aprawtotaltax")) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(Session("aprawtotaltax")) * cRate.GetRateMonthlyUSDValue & ", 'QL_trnbelimst " & Session("trnoid") & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1
                        iSeq += 1
                    End If
                    If dRound <> 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iRoundAcctgOid & ", '" & IIf(dRound > 0, "D", "C") & "', " & Math.Abs(dRound) & ", '" & sNo & "', 'PI Raw|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Math.Abs(dRound) * cRate.GetRateMonthlyIDRValue & ", " & Math.Abs(dRound) * cRate.GetRateMonthlyUSDValue & ", 'QL_trnbelimst " & Session("trnoid") & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1
                        iSeq += 1
                    End If
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iAPAcctgOid & ", 'C', " & ToDouble(Session("aprawgrandtotal")) & ", '" & sNo & "', 'PI Raw|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(Session("aprawgrandtotal")) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(Session("aprawgrandtotal")) * cRate.GetRateMonthlyUSDValue & ", 'QL_trnbelimst " & Session("trnoid") & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1
                    iSeq += 1
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "PI General Material Approval"
    Private Sub SetGVListAPGM()
        Dim arHeader() As String = {"Draft No.", "PI No.", "PI Date", "Supplier", "PI Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trnno", "trndate", "suppname", "aprawmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "suppname", "aprawmstnote", "aprawtotalamt", "aprawtotaldisc", "aprawtotaltax", "aprawgrandtotal", "potype", "appayment", "suppoid", "curroid", "upduser", "updtime", "trnno", "appaytypeoid", "apdatetakegiro", "apacctgoid", "trnbelidiscamtdtl", "trnbelidiscamt"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT ap.cmpcode, ap.trnbelimstoid AS trnoid, ap.trnbelino AS trnno, CONVERT(VARCHAR(10), ap.trnbelidate, 101) AS trndate, s.suppname, ap.trnbelimstnote aprawmstnote, ap.trnbeliamt aprawtotalamt, ap.trnbelidiscamt aprawtotaldisc, ap.trnbelimsttaxamt aprawtotaltax, ap.trnbeliamtnetto aprawgrandtotal, apr.approvaloid, apr.requestuser, CONVERT(VARCHAR(10), apr.requestdate, 101) AS requestdate, s.supptype AS potype, g.gendesc AS appayment, ap.suppoid, ap.curroid, ap.upduser, ap.updtime, ap.trnbelipaytype AS appaytypeoid, CONVERT(VARCHAR(10), ap.apdatetakegiro, 101) AS apdatetakegiro, s.apacctgoid, trnbelidiscamtdtl, trnbelidiscamt FROM QL_trnbelimst ap INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid INNER JOIN QL_approval apr ON ap.trnbelimstoid=apr.oid AND apr.statusrequest='New' INNER JOIN QL_mstgen g ON g.genoid=ap.trnbelipaytype WHERE apr.event='In Approval' AND apr.approvaluser='" & Session("UserID") & "' AND apr.tablename='QL_trnapgenmst' AND ap.trnbelimststatus='In Approval' ORDER BY apr.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnapgenmst")
    End Sub

    Private Sub SetGVDataAPGM(ByVal dtDataHeader As DataTable)
        Session("potype") = gvList.SelectedDataKey.Item("potype").ToString.ToUpper
        Session("appayment") = gvList.SelectedDataKey.Item("appayment").ToString
        Session("suppoid") = gvList.SelectedDataKey.Item("suppoid").ToString
        Session("curroid") = gvList.SelectedDataKey.Item("curroid").ToString
        Session("aprawtotalamt") = gvList.SelectedDataKey.Item("aprawtotalamt").ToString
        Session("aprawtotaltax") = gvList.SelectedDataKey.Item("aprawtotaltax").ToString
        Session("aprawgrandtotal") = gvList.SelectedDataKey.Item("aprawgrandtotal").ToString
        Session("apno") = gvList.SelectedDataKey.Item("trnno").ToString
        Session("appaytypeoid") = gvList.SelectedDataKey.Item("appaytypeoid").ToString
        Session("apdatetakegiro") = gvList.SelectedDataKey.Item("apdatetakegiro").ToString
        Session("apacctgoid") = gvList.SelectedDataKey.Item("apacctgoid").ToString
        Session("trnbelidiscamtdtl") = gvList.SelectedDataKey.Item("trnbelidiscamtdtl").ToString
        lblreason.Visible = True : lblSeptReason.Visible = True : ActionReason.Visible = True
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "PI No.", "PI Date", "Supplier", "PI Note", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trnno", "trndate", "suppname", "aprawmstnote", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arColNameList() As String = {"datacaption", "datasept", "datavalue", "dataempty"}
        Dim arColTypeList() As String = {"System.String", "System.String", "System.String", "System.String"}
        Dim dtDataFooter As DataTable = SetTableDetail("tblDataFooter", arColNameList, arColTypeList)
        Dim arCaptionF() As String = {"Total Amount", "Disc Detail", "Disc Header", "Total Tax", "Grand Total"}
        Dim arValueF() As String = {"aprawtotalamt", "trnbelidiscamtdtl", "trnbelidiscamt", "aprawtotaltax", "aprawgrandtotal"}
        Dim arTypeF() As String = {"D", "D", "D", "D", "D"}
        Dim arHiddenF() As String = {"N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaptionF.Length - 1
            dr = dtDataFooter.NewRow
            dr("datacaption") = arCaptionF(C1)
            dr("datasept") = ":"
            If arTypeF(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValueF(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValueF(C1)).ToString), 2)
            End If
            dr("dataempty") = ""
            dtDataFooter.Rows.Add(dr)
        Next
        gvDataFooter.DataSource = dtDataFooter
        gvDataFooter.DataBind()
        For C1 As Int16 = 0 To arHiddenF.Length - 1
            If arHiddenF(C1) = "Y" Then
                gvDataFooter.Rows(C1).Visible = False
            End If
        Next
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Font.Bold = True
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).ForeColor = Drawing.Color.Black
        Dim arHeader() As String = {"No.", "Mat. Desc", "Quantity", "Unit", "Price", "Amount", "Disc.", "Netto", "Note"}
        Dim arField() As String = {"aprawdtlseq", "matrawshortdesc", "aprawqty", "unit", "aprawprice", "aprawdtlamt", "aprawdtldiscamt", "aprawdtlnetto", "aprawdtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT apd.trnbelidtlseq AS aprawdtlseq, apd.itemoid AS matrawoid, m.itemLongDescription AS matrawshortdesc, apd.trnbelidtlqty AS aprawqty, g.gendesc AS unit, apd.trnbelidtlprice AS aprawprice, apd.trnbelidtlamt AS aprawdtlamt, apd.trnbelidtldiscamt AS aprawdtldiscamt, apd.trnbelidtlamtnetto AS aprawdtlnetto, apd.trnbelidtlnote AS aprawdtlnote, (apd.trnbelidtlqty * mrd.mrvalueidr) AS mrrawamtidr, (apd.trnbelidtlqty * mrd.mrvalueusd) AS mrrawamtusd, (apd.trnbelidtlqty * mrd.mrvalue) AS mrrawamt FROM QL_trnbelimst ap INNER JOIN QL_trnbelidtl apd ON ap.trnbelimstoid=apd.trnbelimstoid INNER JOIN QL_mstitem m ON apd.itemoid=m.itemoid INNER JOIN QL_mstgen g ON apd.trnbelidtlunitoid=g.genoid INNER JOIN QL_trnmrdtl mrd ON mrd.mrdtloid=apd.mrdtloid WHERE ap.trnbelimstoid=" & Session("trnoid") & " ORDER BY apd.trnbelidtlseq"
        Session("TblDtlAPGen") = ckon.ambiltabel(sSql, "QL_trnapgendtl")
        gvData.DataSource = Session("TblDtlAPGen")
        gvData.DataBind()
    End Sub

    Private Function GenerateAPGMNo() As String
        Dim sNo As String = "PIGM-" & Format(GetServerTime(), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trnbelino, " & DefaultFormatCounter & ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnbelimst WHERE cmpcode='" & AppCmpCode.Text & "' AND trnbelino LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdateAPGM(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select PI Raw Material Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select PI Raw Material Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        If sStatus = "Rejected" Or sStatus = "Revised" Then
            If ActionReason.Text = "" Then
                showMessage("Please Fill " & sStatus & " Reason First!", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim iConAPOid As Int32 = GenerateID("QL_CONAP", CompnyCode)
        Dim iGlMstOid As Int32 = GenerateID("QL_TRNGLMST", CompnyCode)
        Dim iGlDtlOid As Int32 = GenerateID("QL_TRNGLDTL", CompnyCode)
        Dim iAPAcctgOid, iRecAcctgOid, iPPNAcctgOid, iDiffIDRAcctgOid, iDiffUSDAcctgOid, iRoundAcctgOid As Integer
        Dim dDueDate As Date = DateAdd(DateInterval.Day, ToDouble(GetNumericValues(Session("appayment"))), CDate(Session("trndate")))
        Dim iSeq As Int16 = 1
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
        Dim cRate As New ClassRate
        sNo = Session("apno")
        Dim dtImport As DataTable = Nothing
        Dim iICSeq As Integer = 1
        If GetStrData(sSql) <> "" Then
            iICSeq = ToInteger(GetStrData(sSql))
        End If
        If sStatus = "Approved" Then
            cRate.SetRateValue(CInt(Session("curroid")), sDate)
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            Dim sVarErr As String = ""
            If Not IsInterfaceExists("VAR_PURC_RECEIVED", AppCmpCode.Text) Then
                sVarErr = "VAR_PURC_RECEIVED"
            End If
            If Not IsInterfaceExists("VAR_PPN_IN", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_PPN_IN"
            End If
            If Not IsInterfaceExists("VAR_DIFF_CURR_IDR", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_DIFF_CURR_IDR"
            End If
            If Not IsInterfaceExists("VAR_DIFF_CURR_USD", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_DIFF_CURR_USD"
            End If
            If Not IsInterfaceExists("VAR_ROUNDING", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_ROUNDING"
            End If
            If Session("potype").ToString.ToUpper = "LOCAL" Then
                If Not IsInterfaceExists("VAR_AP_LOCAL", AppCmpCode.Text) Then
                    sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_AP_LOCAL"
                End If
            Else
                If Not IsInterfaceExists("VAR_AP_IMPORT", AppCmpCode.Text) Then
                    sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_AP_IMPORT"
                End If
            End If
            If sVarErr <> "" Then
                showMessage(GetInterfaceWarning(sVarErr, "approved"), CompnyName & " - WARNING", 2)
                Exit Sub
            End If
            iRecAcctgOid = GetAcctgOID(GetVarInterface("VAR_PURC_RECEIVED", AppCmpCode.Text), CompnyCode)
            iPPNAcctgOid = GetAcctgOID(GetVarInterface("VAR_PPN_IN", AppCmpCode.Text), CompnyCode)
            iDiffIDRAcctgOid = GetAcctgOID(GetVarInterface("VAR_DIFF_CURR_IDR", AppCmpCode.Text), CompnyCode)
            iDiffUSDAcctgOid = GetAcctgOID(GetVarInterface("VAR_DIFF_CURR_USD", AppCmpCode.Text), CompnyCode)
            iRoundAcctgOid = GetAcctgOID(GetVarInterface("VAR_ROUNDING", AppCmpCode.Text), CompnyCode)
            iAPAcctgOid = Session("apacctgoid")
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update PI
            sSql = "UPDATE QL_trnbelimst SET trnbelimststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP"
            If sStatus = "Approved" Then
                sSql &= ", rateoid=" & cRate.GetRateDailyOid & ", beliratetoidr='" & Left(cRate.GetRateDailyIDRValue.ToString, 50) & "', beliratetousd='" & Left(cRate.GetRateDailyUSDValue.ToString, 50) & "', rate2oid=" & cRate.GetRateMonthlyOid & ", belirate2toidr='" & Left(cRate.GetRateMonthlyIDRValue.ToString, 50) & "', belirate2tousd='" & Left(cRate.GetRateMonthlyUSDValue.ToString, 50) & "', trnbeliamtidr=trnbeliamt * " & cRate.GetRateMonthlyIDRValue & ", trnbeliamtusd=trnbeliamt * " & cRate.GetRateMonthlyUSDValue & ", trnbelimsttaxamtidr=trnbelimsttaxamt * " & cRate.GetRateMonthlyIDRValue & ", trnbelimsttaxamtusd=trnbelimsttaxamt * " & cRate.GetRateMonthlyUSDValue & ", trnbeliamtnettoidr=trnbeliamtnetto * " & cRate.GetRateMonthlyIDRValue & ", trnbeliamtnettousd=trnbeliamtnetto * " & cRate.GetRateMonthlyUSDValue & ""
            Else
                sSql = "UPDATE QL_trnbelimst SET " & IIf(sStatus = "Rejected", ", rejectreason='" & Tchar(ActionReason.Text) & "', rejectuser='" & Session("UserID") & "', rejecttime=CURRENT_TIMESTAMP", ", revisereason='" & Tchar(ActionReason.Text) & "', reviseuser='" & Session("UserID") & "', revisetime=CURRENT_TIMESTAMP") & ")"
            End If
            sSql &= " WHERE trnbelimststatus='In Approval' AND trnbelimstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnapgenmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Rejected" Then
                    sSql = "UPDATE QL_trnmrdtl SET mrdtlstatus='' WHERE cmpcode='" & AppCmpCode.Text & "' AND mrdtloid IN (SELECT mrdtloid FROM QL_trnbelidtl WHERE cmpcode='" & AppCmpCode.Text & "' AND trnbelimstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnmrmst SET mrmststatus='Post' WHERE cmpcode='" & AppCmpCode.Text & "' AND mrmstoid IN (SELECT mrmstoid FROM QL_trnbelidtl WHERE cmpcode='" & AppCmpCode.Text & "' AND trnbelimstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                ElseIf sStatus = "Approved" Then
                    Dim dt As DataTable = Session("TblDtlAPGen")
                    Dim dMRAmt As Double = ToDouble(dt.Compute("SUM(mrrawamtidr)", ""))
                    Dim dMRAmtIDR As Double = ToDouble(dt.Compute("SUM(mrrawamtidr)", "")) * cRate.GetRateMonthlyIDRValue
                    Dim dMRAmtUSD As Double = ToDouble(dt.Compute("SUM(mrrawamtidr)", "")) * cRate.GetRateMonthlyUSDValue

                    ' Insert CONAP
                    sSql = "INSERT INTO QL_conap (cmpcode, conapoid, reftype, refoid, payrefoid, suppoid, acctgoid, trnapstatus, trnaptype, trnapdate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans, amtbayar, trnapnote, trnapres1, trnapres2, trnapres3, createuser, createtime, upduser, updtime, amttransidr, amtbayaridr, amttransusd, amtbayarusd) VALUES ('" & AppCmpCode.Text & "', " & iConAPOid & ", 'QL_trnbelimst', " & Session("trnoid") & ", 0, " & Session("suppoid") & ", " & iAPAcctgOid & ", 'Post', 'APGM', '" & sDate & "', '" & sPeriod & "', 0, '1/1/1900', '', 0, '" & dDueDate & "', " & ToDouble(Session("aprawgrandtotal")) & ", 0, '', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(Session("aprawgrandtotal")) * cRate.GetRateMonthlyIDRValue & ", 0, " & ToDouble(Session("aprawgrandtotal")) * cRate.GetRateMonthlyUSDValue & ", 0)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iConAPOid += 1
                    Dim dTotalImportIDR As Double = 0, dTotalImportUSD As Double = 0, dTotalImport As Double = 0
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iConAPOid - 1 & " WHERE tablename='QL_CONAP' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Auto Jurnal Accounting
                    ' Insert GL MST
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & AppCmpCode.Text & "', " & iGlMstOid & ", '" & sDate & "', '" & sPeriod & "', 'PI Gen|No. " & sNo & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Insert GL DTL
                    Dim dRound As Double = (ToDouble(Session("aprawtotalamt")) - ToDouble(Session("trnbelidiscamtdtl"))) - dMRAmt

                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iRecAcctgOid & ", 'D', " & dMRAmt & ", '" & sNo & "', 'PI Gen|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dMRAmtIDR & ", " & dMRAmtUSD & ", 'QL_trnbelimst " & Session("trnoid") & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1
                    iSeq += 1
                    If ToDouble(Session("aprawtotaltax")) > 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iPPNAcctgOid & ", 'D', " & ToDouble(Session("aprawtotaltax")) & ", '" & sNo & "', 'PI Gen|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(Session("aprawtotaltax")) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(Session("aprawtotaltax")) * cRate.GetRateMonthlyUSDValue & ", 'QL_trnbelimst " & Session("trnoid") & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1
                        iSeq += 1
                    End If
                    If dRound <> 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iRoundAcctgOid & ", '" & IIf(dRound > 0, "D", "C") & "', " & Math.Abs(dRound) & ", '" & sNo & "', 'PI Raw|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Math.Abs(dRound) * cRate.GetRateMonthlyIDRValue & ", " & Math.Abs(dRound) * cRate.GetRateMonthlyUSDValue & ", 'QL_trnbelimst " & Session("trnoid") & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1
                        iSeq += 1
                    End If
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iAPAcctgOid & ", 'C', " & ToDouble(Session("aprawgrandtotal")) & ", '" & sNo & "', 'PI Gen|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(Session("aprawgrandtotal")) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(Session("aprawgrandtotal")) * cRate.GetRateMonthlyUSDValue & ", 'QL_trnbelimst " & Session("trnoid") & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1
                    iSeq += 1
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "PI Finish Good Approval"
    Private Sub SetGVListAPFG()
        Dim arHeader() As String = {"Draft No.", "PI No.", "PI Date", "Supplier", "PI Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trnno", "trndate", "suppname", "aprawmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "suppname", "aprawmstnote", "aprawtotalamt", "aprawtotaldisc", "aprawtotaltax", "aprawgrandtotal", "potype", "appayment", "suppoid", "curroid", "upduser", "updtime", "trnno", "appaytypeoid", "apdatetakegiro", "apacctgoid", "trnbelidiscamtdtl", "trnbelidiscamt"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT ap.cmpcode, ap.trnbelimstoid AS trnoid, ap.trnbelino AS trnno, CONVERT(VARCHAR(10), ap.trnbelidate, 101) AS trndate, s.suppname, ap.trnbelimstnote aprawmstnote, ap.trnbeliamt aprawtotalamt, ap.trnbelidiscamt aprawtotaldisc, ap.trnbelimsttaxamt aprawtotaltax, ap.trnbeliamtnetto aprawgrandtotal, apr.approvaloid, apr.requestuser, CONVERT(VARCHAR(10), apr.requestdate, 101) AS requestdate, s.supptype AS potype, g.gendesc AS appayment, ap.suppoid, ap.curroid, ap.upduser, ap.updtime, ap.trnbelipaytype AS appaytypeoid, CONVERT(VARCHAR(10), ap.apdatetakegiro, 101) AS apdatetakegiro, s.apacctgoid, trnbelidiscamtdtl, trnbelidiscamt FROM QL_trnbelimst ap INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid INNER JOIN QL_approval apr ON ap.trnbelimstoid=apr.oid AND apr.statusrequest='New' INNER JOIN QL_mstgen g ON g.genoid=ap.trnbelipaytype WHERE apr.event='In Approval' AND apr.approvaluser='" & Session("UserID") & "' AND apr.tablename='QL_trnapfgmst' AND ap.trnbelimststatus='In Approval' ORDER BY apr.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnapfgmst")
    End Sub

    Private Sub SetGVDataAPFG(ByVal dtDataHeader As DataTable)
        Session("potype") = gvList.SelectedDataKey.Item("potype").ToString.ToUpper
        Session("appayment") = gvList.SelectedDataKey.Item("appayment").ToString
        Session("suppoid") = gvList.SelectedDataKey.Item("suppoid").ToString
        Session("curroid") = gvList.SelectedDataKey.Item("curroid").ToString
        Session("aprawtotalamt") = gvList.SelectedDataKey.Item("aprawtotalamt").ToString
        Session("aprawtotaltax") = gvList.SelectedDataKey.Item("aprawtotaltax").ToString
        Session("aprawgrandtotal") = gvList.SelectedDataKey.Item("aprawgrandtotal").ToString
        Session("apno") = gvList.SelectedDataKey.Item("trnno").ToString
        Session("appaytypeoid") = gvList.SelectedDataKey.Item("appaytypeoid").ToString
        Session("apdatetakegiro") = gvList.SelectedDataKey.Item("apdatetakegiro").ToString
        Session("apacctgoid") = gvList.SelectedDataKey.Item("apacctgoid").ToString
        Session("trnbelidiscamtdtl") = gvList.SelectedDataKey.Item("trnbelidiscamtdtl").ToString
        lblreason.Visible = True : lblSeptReason.Visible = True : ActionReason.Visible = True
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "PI No.", "PI Date", "Supplier", "PI Note", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trnno", "trndate", "suppname", "aprawmstnote", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arColNameList() As String = {"datacaption", "datasept", "datavalue", "dataempty"}
        Dim arColTypeList() As String = {"System.String", "System.String", "System.String", "System.String"}
        Dim dtDataFooter As DataTable = SetTableDetail("tblDataFooter", arColNameList, arColTypeList)
        Dim arCaptionF() As String = {"Total Amount", "Disc Detail", "Disc Header", "Total Tax", "Grand Total"}
        Dim arValueF() As String = {"aprawtotalamt", "trnbelidiscamtdtl", "trnbelidiscamt", "aprawtotaltax", "aprawgrandtotal"}
        Dim arTypeF() As String = {"D", "D", "D", "D", "D"}
        Dim arHiddenF() As String = {"N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaptionF.Length - 1
            dr = dtDataFooter.NewRow
            dr("datacaption") = arCaptionF(C1)
            dr("datasept") = ":"
            If arTypeF(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValueF(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValueF(C1)).ToString), 2)
            End If
            dr("dataempty") = ""
            dtDataFooter.Rows.Add(dr)
        Next
        gvDataFooter.DataSource = dtDataFooter
        gvDataFooter.DataBind()
        For C1 As Int16 = 0 To arHiddenF.Length - 1
            If arHiddenF(C1) = "Y" Then
                gvDataFooter.Rows(C1).Visible = False
            End If
        Next
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Font.Bold = True
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).ForeColor = Drawing.Color.Black
        Dim arHeader() As String = {"No.", "Mat. Desc", "Quantity", "Unit", "Price", "Amount", "Disc.", "Netto", "Note"}
        Dim arField() As String = {"aprawdtlseq", "matrawshortdesc", "aprawqty", "unit", "aprawprice", "aprawdtlamt", "aprawdtldiscamt", "aprawdtlnetto", "aprawdtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT apd.trnbelidtlseq AS aprawdtlseq, apd.itemoid AS matrawoid, m.itemLongDescription AS matrawshortdesc, apd.trnbelidtlqty AS aprawqty, g.gendesc AS unit, apd.trnbelidtlprice AS aprawprice, apd.trnbelidtlamt AS aprawdtlamt, apd.trnbelidtldiscamt AS aprawdtldiscamt, apd.trnbelidtlamtnetto AS aprawdtlnetto, apd.trnbelidtlnote AS aprawdtlnote, (apd.trnbelidtlqty * mrd.mrvalueidr) AS mrrawamtidr, (apd.trnbelidtlqty * mrd.mrvalueusd) AS mrrawamtusd, (apd.trnbelidtlqty * mrd.mrvalue) AS mrrawamt FROM QL_trnbelimst ap INNER JOIN QL_trnbelidtl apd ON ap.trnbelimstoid=apd.trnbelimstoid INNER JOIN QL_mstitem m ON apd.itemoid=m.itemoid INNER JOIN QL_mstgen g ON apd.trnbelidtlunitoid=g.genoid INNER JOIN QL_trnmrdtl mrd ON mrd.mrdtloid=apd.mrdtloid WHERE ap.trnbelimstoid=" & Session("trnoid") & " ORDER BY apd.trnbelidtlseq"
        Session("TblDtlAPFG") = ckon.ambiltabel(sSql, "QL_trnapfgdtl")
        gvData.DataSource = Session("TblDtlAPFG")
        gvData.DataBind()
    End Sub

    Private Function GenerateAPFGNo() As String
        Dim sNo As String = "PIFG-" & Format(GetServerTime(), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trnbelino, " & DefaultFormatCounter & ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnbelimst WHERE cmpcode='" & AppCmpCode.Text & "' AND trnbelino LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdateAPFG(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select PI Finish Good Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select PI Finish Good Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        If sStatus = "Rejected" Or sStatus = "Revised" Then
            If ActionReason.Text = "" Then
                showMessage("Please Fill " & sStatus & " Reason First!", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim iConAPOid As Int32 = GenerateID("QL_CONAP", CompnyCode)
        Dim iGlMstOid As Int32 = GenerateID("QL_TRNGLMST", CompnyCode)
        Dim iGlDtlOid As Int32 = GenerateID("QL_TRNGLDTL", CompnyCode)
        Dim iAPAcctgOid, iRecAcctgOid, iPPNAcctgOid, iDiffIDRAcctgOid, iDiffUSDAcctgOid, iRoundAcctgOid As Integer
        Dim dDueDate As Date = DateAdd(DateInterval.Day, ToDouble(GetNumericValues(Session("appayment"))), CDate(Session("trndate")))
        Dim iSeq As Int16 = 1
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
        Dim cRate As New ClassRate
        sNo = Session("apno")
        Dim dtImport As DataTable = Nothing

        Dim iICSeq As Integer = 1
        If GetStrData(sSql) <> "" Then
            iICSeq = ToInteger(GetStrData(sSql))
        End If
        If sStatus = "Approved" Then
            cRate.SetRateValue(CInt(Session("curroid")), sDate)
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            Dim sVarErr As String = ""
            If Not IsInterfaceExists("VAR_PURC_RECEIVED", AppCmpCode.Text) Then
                sVarErr = "VAR_PURC_RECEIVED"
            End If
            If Not IsInterfaceExists("VAR_PPN_IN", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_PPN_IN"
            End If
            If Not IsInterfaceExists("VAR_DIFF_CURR_IDR", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_DIFF_CURR_IDR"
            End If
            If Not IsInterfaceExists("VAR_DIFF_CURR_USD", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_DIFF_CURR_USD"
            End If
            If Not IsInterfaceExists("VAR_ROUNDING", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_ROUNDING"
            End If
            If Session("potype").ToString.ToUpper = "LOCAL" Then
                If Not IsInterfaceExists("VAR_AP_LOCAL", AppCmpCode.Text) Then
                    sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_AP_LOCAL"
                End If
            Else
                If Not IsInterfaceExists("VAR_AP_IMPORT", AppCmpCode.Text) Then
                    sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_AP_IMPORT"
                End If
            End If
            If sVarErr <> "" Then
                showMessage(GetInterfaceWarning(sVarErr, "approved"), CompnyName & " - WARNING", 2)
                Exit Sub
            End If
            iRecAcctgOid = GetAcctgOID(GetVarInterface("VAR_PURC_RECEIVED", AppCmpCode.Text), CompnyCode)
            iPPNAcctgOid = GetAcctgOID(GetVarInterface("VAR_PPN_IN", AppCmpCode.Text), CompnyCode)
            iDiffIDRAcctgOid = GetAcctgOID(GetVarInterface("VAR_DIFF_CURR_IDR", AppCmpCode.Text), CompnyCode)
            iDiffUSDAcctgOid = GetAcctgOID(GetVarInterface("VAR_DIFF_CURR_USD", AppCmpCode.Text), CompnyCode)
            iRoundAcctgOid = GetAcctgOID(GetVarInterface("VAR_ROUNDING", AppCmpCode.Text), CompnyCode)
            iAPAcctgOid = Session("apacctgoid")

        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update PI
            sSql = "UPDATE QL_trnbelimst SET trnbelimststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP"
            If sStatus = "Approved" Then
                sSql &= ", rateoid=" & cRate.GetRateDailyOid & ", beliratetoidr='" & Left(cRate.GetRateDailyIDRValue.ToString, 50) & "', beliratetousd='" & Left(cRate.GetRateDailyUSDValue.ToString, 50) & "', rate2oid=" & cRate.GetRateMonthlyOid & ", belirate2toidr='" & Left(cRate.GetRateMonthlyIDRValue.ToString, 50) & "', belirate2tousd='" & Left(cRate.GetRateMonthlyUSDValue.ToString, 50) & "', trnbeliamtidr=trnbeliamt * " & cRate.GetRateMonthlyIDRValue & ", trnbeliamtusd=trnbeliamt * " & cRate.GetRateMonthlyUSDValue & ", trnbelimsttaxamtidr=trnbelimsttaxamt * " & cRate.GetRateMonthlyIDRValue & ", trnbelimsttaxamtusd=trnbelimsttaxamt * " & cRate.GetRateMonthlyUSDValue & ", trnbeliamtnettoidr=trnbeliamtnetto * " & cRate.GetRateMonthlyIDRValue & ", trnbeliamtnettousd=trnbeliamtnetto * " & cRate.GetRateMonthlyUSDValue & ""
            Else
                sSql = "UPDATE QL_trnbelimst SET " & IIf(sStatus = "Rejected", ", rejectreason='" & Tchar(ActionReason.Text) & "', rejectuser='" & Session("UserID") & "', rejecttime=CURRENT_TIMESTAMP", ", revisereason='" & Tchar(ActionReason.Text) & "', reviseuser='" & Session("UserID") & "', revisetime=CURRENT_TIMESTAMP") & ")"
            End If
            sSql &= " WHERE trnbelimststatus='In Approval' AND trnbelimstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnapfgmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Rejected" Then
                    sSql = "UPDATE QL_trnmrdtl SET mrdtlstatus='' WHERE cmpcode='" & AppCmpCode.Text & "' AND mrdtloid IN (SELECT mrdtloid FROM QL_trnbelidtl WHERE cmpcode='" & AppCmpCode.Text & "' AND trnbelimstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnmrmst SET mrmststatus='Post' WHERE cmpcode='" & AppCmpCode.Text & "' AND mrmstoid IN (SELECT mrmstoid FROM QL_trnbelidtl WHERE cmpcode='" & AppCmpCode.Text & "' AND trnbelimstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                ElseIf sStatus = "Approved" Then
                    Dim dt As DataTable = Session("TblDtlAPFG")
                    Dim dMRAmt As Double = ToDouble(dt.Compute("SUM(mrrawamtidr)", ""))
                    Dim dMRAmtIDR As Double = ToDouble(dt.Compute("SUM(mrrawamtidr)", "")) * cRate.GetRateMonthlyIDRValue
                    Dim dMRAmtUSD As Double = ToDouble(dt.Compute("SUM(mrrawamtidr)", "")) * cRate.GetRateMonthlyUSDValue

                    ' Insert CONAP
                    sSql = "INSERT INTO QL_conap (cmpcode, conapoid, reftype, refoid, payrefoid, suppoid, acctgoid, trnapstatus, trnaptype, trnapdate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans, amtbayar, trnapnote, trnapres1, trnapres2, trnapres3, createuser, createtime, upduser, updtime, amttransidr, amtbayaridr, amttransusd, amtbayarusd) VALUES ('" & AppCmpCode.Text & "', " & iConAPOid & ", 'QL_trnbelimst', " & Session("trnoid") & ", 0, " & Session("suppoid") & ", " & iAPAcctgOid & ", 'Post', 'APGM', '" & sDate & "', '" & sPeriod & "', 0, '1/1/1900', '', 0, '" & dDueDate & "', " & ToDouble(Session("aprawgrandtotal")) & ", 0, '', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(Session("aprawgrandtotal")) * cRate.GetRateMonthlyIDRValue & ", 0, " & ToDouble(Session("aprawgrandtotal")) * cRate.GetRateMonthlyUSDValue & ", 0)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iConAPOid += 1
                    Dim dTotalImportIDR As Double = 0, dTotalImportUSD As Double = 0, dTotalImport As Double = 0

                    sSql = "UPDATE QL_mstoid SET lastoid=" & iConAPOid - 1 & " WHERE tablename='QL_CONAP' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Auto Jurnal Accounting
                    ' Insert GL MST
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & AppCmpCode.Text & "', " & iGlMstOid & ", '" & sDate & "', '" & sPeriod & "', 'PI FG|No. " & sNo & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Insert GL DTL
                    Dim dRound As Double = (ToDouble(Session("aprawtotalamt")) - ToDouble(Session("trnbelidiscamtdtl"))) - dMRAmt

                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iRecAcctgOid & ", 'D', " & dMRAmt & ", '" & sNo & "', 'PI FG|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dMRAmtIDR & ", " & dMRAmtUSD & ", 'QL_trnbelimst " & Session("trnoid") & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1
                    iSeq += 1
                    If ToDouble(Session("aprawtotaltax")) > 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iPPNAcctgOid & ", 'D', " & ToDouble(Session("aprawtotaltax")) & ", '" & sNo & "', 'PI FG|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(Session("aprawtotaltax")) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(Session("aprawtotaltax")) * cRate.GetRateMonthlyUSDValue & ", 'QL_trnbelimst " & Session("trnoid") & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1
                        iSeq += 1
                    End If
                    If dRound <> 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iRoundAcctgOid & ", '" & IIf(dRound > 0, "D", "C") & "', " & Math.Abs(dRound) & ", '" & sNo & "', 'PI Raw|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Math.Abs(dRound) * cRate.GetRateMonthlyIDRValue & ", " & Math.Abs(dRound) * cRate.GetRateMonthlyUSDValue & ", 'QL_trnbelimst " & Session("trnoid") & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1
                        iSeq += 1
                    End If
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iAPAcctgOid & ", 'C', " & ToDouble(Session("aprawgrandtotal")) & ", '" & sNo & "', 'PI FG|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(Session("aprawgrandtotal")) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(Session("aprawgrandtotal")) * cRate.GetRateMonthlyUSDValue & ", 'QL_trnbelimst " & Session("trnoid") & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1
                    iSeq += 1

                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "PI WIP Approval"
    Private Sub SetGVListAPLOG()
        Dim arHeader() As String = {"Draft No.", "PI No.", "PI Date", "Supplier", "PI Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trnno", "trndate", "suppname", "aprawmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "suppname", "aprawmstnote", "aprawtotalamt", "aprawtotaldisc", "aprawtotaltax", "aprawgrandtotal", "potype", "appayment", "suppoid", "curroid", "upduser", "updtime", "trnno", "appaytypeoid", "apdatetakegiro", "apacctgoid"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT ap.cmpcode, ap.trnbelimstoid AS trnoid, ap.trnbelino AS trnno, CONVERT(VARCHAR(10), ap.trnbelidate, 101) AS trndate, s.suppname, ap.trnbelimstnote aprawmstnote, ap.trnbeliamt aprawtotalamt, ap.trnbelidiscamt aprawtotaldisc, ap.trnbelimsttaxamt aprawtotaltax, ap.trnbeliamtnetto aprawgrandtotal, apr.approvaloid, apr.requestuser, CONVERT(VARCHAR(10), apr.requestdate, 101) AS requestdate, s.supptype AS potype, g.gendesc AS appayment, ap.suppoid, ap.curroid, ap.upduser, ap.updtime, ap.trnbelipaytype AS appaytypeoid, CONVERT(VARCHAR(10), ap.apdatetakegiro, 101) AS apdatetakegiro, s.apacctgoid FROM QL_trnbelimst ap INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid INNER JOIN QL_approval apr ON ap.trnbelimstoid=apr.oid AND apr.statusrequest='New' INNER JOIN QL_mstgen g ON g.genoid=ap.trnbelipaytype WHERE apr.event='In Approval' AND apr.approvaluser='" & Session("UserID") & "' AND apr.tablename='QL_trnapwipmst' AND ap.trnbelimststatus='In Approval' ORDER BY apr.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnapwipmst")
    End Sub

    Private Sub SetGVDataAPLOG(ByVal dtDataHeader As DataTable)
        Session("potype") = gvList.SelectedDataKey.Item("potype").ToString.ToUpper
        Session("appayment") = gvList.SelectedDataKey.Item("appayment").ToString
        Session("suppoid") = gvList.SelectedDataKey.Item("suppoid").ToString
        Session("curroid") = gvList.SelectedDataKey.Item("curroid").ToString
        Session("aprawtotalamt") = gvList.SelectedDataKey.Item("aprawtotalamt").ToString
        Session("aprawtotaltax") = gvList.SelectedDataKey.Item("aprawtotaltax").ToString
        Session("aprawgrandtotal") = gvList.SelectedDataKey.Item("aprawgrandtotal").ToString
        Session("apno") = gvList.SelectedDataKey.Item("trnno").ToString
        Session("appaytypeoid") = gvList.SelectedDataKey.Item("appaytypeoid").ToString
        Session("apdatetakegiro") = gvList.SelectedDataKey.Item("apdatetakegiro").ToString
        Session("apacctgoid") = gvList.SelectedDataKey.Item("apacctgoid").ToString
        lblreason.Visible = True : lblSeptReason.Visible = True : ActionReason.Visible = True
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "PI No.", "PI Date", "Supplier", "PI Note", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trnno", "trndate", "suppname", "aprawmstnote", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arColNameList() As String = {"datacaption", "datasept", "datavalue", "dataempty"}
        Dim arColTypeList() As String = {"System.String", "System.String", "System.String", "System.String"}
        Dim dtDataFooter As DataTable = SetTableDetail("tblDataFooter", arColNameList, arColTypeList)
        Dim arCaptionF() As String = {"Total Amount", "Total Tax", "Grand Total"}
        Dim arValueF() As String = {"aprawtotalamt", "aprawtotaltax", "aprawgrandtotal"}
        Dim arTypeF() As String = {"D", "D", "D"}
        Dim arHiddenF() As String = {"N", "N", "N"}
        For C1 As Int16 = 0 To arCaptionF.Length - 1
            dr = dtDataFooter.NewRow
            dr("datacaption") = arCaptionF(C1)
            dr("datasept") = ":"
            If arTypeF(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValueF(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValueF(C1)).ToString), 2)
            End If
            dr("dataempty") = ""
            dtDataFooter.Rows.Add(dr)
        Next
        gvDataFooter.DataSource = dtDataFooter
        gvDataFooter.DataBind()
        For C1 As Int16 = 0 To arHiddenF.Length - 1
            If arHiddenF(C1) = "Y" Then
                gvDataFooter.Rows(C1).Visible = False
            End If
        Next
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Font.Bold = True
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).ForeColor = Drawing.Color.Black
        Dim arHeader() As String = {"No.", "Mat. Desc", "Quantity", "Unit", "Price", "Amount", "Disc.", "Netto", "Note"}
        Dim arField() As String = {"aprawdtlseq", "matrawshortdesc", "aprawqty", "unit", "aprawprice", "aprawdtlamt", "aprawdtldiscamt", "aprawdtlnetto", "aprawdtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT apd.trnbelidtlseq AS aprawdtlseq, apd.itemoid AS matrawoid, m.itemLongDescription AS matrawshortdesc, apd.trnbelidtlqty AS aprawqty, g.gendesc AS unit, apd.trnbelidtlprice AS aprawprice, apd.trnbelidtlamt AS aprawdtlamt, apd.trnbelidtldiscamt AS aprawdtldiscamt, apd.trnbelidtlamtnetto AS aprawdtlnetto, apd.trnbelidtlnote AS aprawdtlnote, (apd.trnbelidtlqty * mrd.mrvalueidr) AS mrrawamtidr, (apd.trnbelidtlqty * mrd.mrvalueusd) AS mrrawamtusd, (apd.trnbelidtlqty * mrd.mrvalue) AS mrrawamt FROM QL_trnbelimst ap INNER JOIN QL_trnbelidtl apd ON ap.trnbelimstoid=apd.trnbelimstoid INNER JOIN QL_mstitem m ON apd.itemoid=m.itemoid INNER JOIN QL_mstgen g ON apd.trnbelidtlunitoid=g.genoid INNER JOIN QL_trnmrdtl mrd ON mrd.mrdtloid=apd.mrdtloid WHERE ap.trnbelimstoid=" & Session("trnoid") & " ORDER BY apd.trnbelidtlseq"
        Session("TblDtlAPWIP") = ckon.ambiltabel(sSql, "QL_trnapwipdtl")
        gvData.DataSource = Session("TblDtlAPWIP")
        gvData.DataBind()
    End Sub

    Private Function GenerateAPLOGNo() As String
        Dim sNo As String = "PIWIP-" & Format(GetServerTime(), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trnbelino, " & DefaultFormatCounter & ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnbelimst WHERE cmpcode='" & AppCmpCode.Text & "' AND trnbelino LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdateAPLOG(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select PI WIP Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select PI WIP Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        If sStatus = "Rejected" Or sStatus = "Revised" Then
            If ActionReason.Text = "" Then
                showMessage("Please Fill " & sStatus & " Reason First!", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim iConAPOid As Int32 = GenerateID("QL_CONAP", CompnyCode)
        Dim iGlMstOid As Int32 = GenerateID("QL_TRNGLMST", CompnyCode)
        Dim iGlDtlOid As Int32 = GenerateID("QL_TRNGLDTL", CompnyCode)
        Dim iAPAcctgOid, iRecAcctgOid, iPPNAcctgOid, iDiffIDRAcctgOid, iDiffUSDAcctgOid, iRoundAcctgOid As Integer
        Dim dDueDate As Date = DateAdd(DateInterval.Day, ToDouble(GetNumericValues(Session("appayment"))), CDate(Session("trndate")))
        Dim iSeq As Int16 = 1
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
        Dim cRate As New ClassRate
        sNo = Session("apno")
        Dim dtImport As DataTable = Nothing
        Dim iICSeq As Integer = 1
        If GetStrData(sSql) <> "" Then
            iICSeq = ToInteger(GetStrData(sSql))
        End If
        If sStatus = "Approved" Then
            cRate.SetRateValue(CInt(Session("curroid")), sDate)
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            Dim sVarErr As String = ""
            If Not IsInterfaceExists("VAR_PURC_RECEIVED", AppCmpCode.Text) Then
                sVarErr = "VAR_PURC_RECEIVED"
            End If
            If Not IsInterfaceExists("VAR_PPN_IN", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_PPN_IN"
            End If
            If Not IsInterfaceExists("VAR_DIFF_CURR_IDR", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_DIFF_CURR_IDR"
            End If
            If Not IsInterfaceExists("VAR_DIFF_CURR_USD", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_DIFF_CURR_USD"
            End If
            If Not IsInterfaceExists("VAR_ROUNDING", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_ROUNDING"
            End If
            If Session("potype").ToString.ToUpper = "LOCAL" Then
                If Not IsInterfaceExists("VAR_AP_LOCAL", AppCmpCode.Text) Then
                    sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_AP_LOCAL"
                End If
            Else
                If Not IsInterfaceExists("VAR_AP_IMPORT", AppCmpCode.Text) Then
                    sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_AP_IMPORT"
                End If
            End If
            If sVarErr <> "" Then
                showMessage(GetInterfaceWarning(sVarErr, "approved"), CompnyName & " - WARNING", 2)
                Exit Sub
            End If
            iRecAcctgOid = GetAcctgOID(GetVarInterface("VAR_PURC_RECEIVED", AppCmpCode.Text), CompnyCode)
            iPPNAcctgOid = GetAcctgOID(GetVarInterface("VAR_PPN_IN", AppCmpCode.Text), CompnyCode)
            iDiffIDRAcctgOid = GetAcctgOID(GetVarInterface("VAR_DIFF_CURR_IDR", AppCmpCode.Text), CompnyCode)
            iDiffUSDAcctgOid = GetAcctgOID(GetVarInterface("VAR_DIFF_CURR_USD", AppCmpCode.Text), CompnyCode)
            iRoundAcctgOid = GetAcctgOID(GetVarInterface("VAR_ROUNDING", AppCmpCode.Text), CompnyCode)
            iAPAcctgOid = Session("apacctgoid")
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update PI
            sSql = "UPDATE QL_trnbelimst SET trnbelimststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP"
            If sStatus = "Approved" Then
                sSql &= ", rateoid=" & cRate.GetRateDailyOid & ", beliratetoidr='" & Left(cRate.GetRateDailyIDRValue.ToString, 50) & "', beliratetousd='" & Left(cRate.GetRateDailyUSDValue.ToString, 50) & "', rate2oid=" & cRate.GetRateMonthlyOid & ", belirate2toidr='" & Left(cRate.GetRateMonthlyIDRValue.ToString, 50) & "', belirate2tousd='" & Left(cRate.GetRateMonthlyUSDValue.ToString, 50) & "', trnbeliamtidr=trnbeliamt * " & cRate.GetRateMonthlyIDRValue & ", trnbeliamtusd=trnbeliamt * " & cRate.GetRateMonthlyUSDValue & ", trnbelimsttaxamtidr=trnbelimsttaxamt * " & cRate.GetRateMonthlyIDRValue & ", trnbelimsttaxamtusd=trnbelimsttaxamt * " & cRate.GetRateMonthlyUSDValue & ", trnbeliamtnettoidr=trnbeliamtnetto * " & cRate.GetRateMonthlyIDRValue & ", trnbeliamtnettousd=trnbeliamtnetto * " & cRate.GetRateMonthlyUSDValue & ""
            Else
                sSql = "UPDATE QL_trnbelimst SET " & IIf(sStatus = "Rejected", ", rejectreason='" & Tchar(ActionReason.Text) & "', rejectuser='" & Session("UserID") & "', rejecttime=CURRENT_TIMESTAMP", ", revisereason='" & Tchar(ActionReason.Text) & "', reviseuser='" & Session("UserID") & "', revisetime=CURRENT_TIMESTAMP") & ")"
            End If
            sSql &= " WHERE trnbelimststatus='In Approval' AND trnbelimstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnapwipmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Rejected" Then
                    sSql = "UPDATE QL_trnmrdtl SET mrdtlstatus='' WHERE cmpcode='" & AppCmpCode.Text & "' AND mrdtloid IN (SELECT mrdtloid FROM QL_trnbelidtl WHERE cmpcode='" & AppCmpCode.Text & "' AND trnbelimstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnmrmst SET mrmststatus='Post' WHERE cmpcode='" & AppCmpCode.Text & "' AND mrmstoid IN (SELECT mrmstoid FROM QL_trnbelidtl WHERE cmpcode='" & AppCmpCode.Text & "' AND trnbelimstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                ElseIf sStatus = "Approved" Then
                    Dim dt As DataTable = Session("TblDtlAPWIP")
                    Dim dMRAmt As Double = ToDouble(dt.Compute("SUM(mrrawamtidr)", ""))
                    Dim dMRAmtIDR As Double = ToDouble(dt.Compute("SUM(mrrawamtidr)", "")) * cRate.GetRateMonthlyIDRValue
                    Dim dMRAmtUSD As Double = ToDouble(dt.Compute("SUM(mrrawamtidr)", "")) * cRate.GetRateMonthlyUSDValue

                    ' Insert CONAP
                    sSql = "INSERT INTO QL_conap (cmpcode, conapoid, reftype, refoid, payrefoid, suppoid, acctgoid, trnapstatus, trnaptype, trnapdate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans, amtbayar, trnapnote, trnapres1, trnapres2, trnapres3, createuser, createtime, upduser, updtime, amttransidr, amtbayaridr, amttransusd, amtbayarusd) VALUES ('" & AppCmpCode.Text & "', " & iConAPOid & ", 'QL_trnbelimst', " & Session("trnoid") & ", 0, " & Session("suppoid") & ", " & iAPAcctgOid & ", 'Post', 'APGM', '" & sDate & "', '" & sPeriod & "', 0, '1/1/1900', '', 0, '" & dDueDate & "', " & ToDouble(Session("aprawgrandtotal")) & ", 0, '', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(Session("aprawgrandtotal")) * cRate.GetRateMonthlyIDRValue & ", 0, " & ToDouble(Session("aprawgrandtotal")) * cRate.GetRateMonthlyUSDValue & ", 0)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iConAPOid += 1
                    Dim dTotalImportIDR As Double = 0, dTotalImportUSD As Double = 0, dTotalImport As Double = 0
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iConAPOid - 1 & " WHERE tablename='QL_CONAP' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Auto Jurnal Accounting
                    ' Insert GL MST
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & AppCmpCode.Text & "', " & iGlMstOid & ", '" & sDate & "', '" & sPeriod & "', 'PI WIP|No. " & sNo & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Insert GL DTL
                    Dim dRound As Double = ToDouble(Session("aprawtotalamt")) - dMRAmt
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iRecAcctgOid & ", 'D', " & dMRAmt & ", '" & sNo & "', 'PI WIP|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dMRAmtIDR & ", " & dMRAmtUSD & ", 'QL_trnbelimst " & Session("trnoid") & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1
                    iSeq += 1
                    If ToDouble(Session("aprawtotaltax")) > 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iPPNAcctgOid & ", 'D', " & ToDouble(Session("aprawtotaltax")) & ", '" & sNo & "', 'PI WIP|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(Session("aprawtotaltax")) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(Session("aprawtotaltax")) * cRate.GetRateMonthlyUSDValue & ", 'QL_trnbelimst " & Session("trnoid") & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1
                        iSeq += 1
                    End If
                    If dRound <> 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iRoundAcctgOid & ", '" & IIf(dRound > 0, "D", "C") & "', " & Math.Abs(dRound) & ", '" & sNo & "', 'PI Raw|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Math.Abs(dRound) * cRate.GetRateMonthlyIDRValue & ", " & Math.Abs(dRound) * cRate.GetRateMonthlyUSDValue & ", 'QL_trnbelimst " & Session("trnoid") & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1
                        iSeq += 1
                    End If
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iAPAcctgOid & ", 'C', " & ToDouble(Session("aprawgrandtotal")) & ", '" & sNo & "', 'PI WIP|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(Session("aprawgrandtotal")) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(Session("aprawgrandtotal")) * cRate.GetRateMonthlyUSDValue & ", 'QL_trnbelimst " & Session("trnoid") & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1
                    iSeq += 1

                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "Purchase Return Approval"
    Private Sub SetGVListPretRM()
        Dim arHeader() As String = {"Draft No.", "Return Date", "Return Type", "Supplier", "Return Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "rettype", "suppname", "retmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "suppname", "retmstnote", "registerdate", "retmstres2", "curroid", "suppoid", "refoidmst", "retmstamt", "retmstamtnetto", "retmsttaxamt"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT pret.cmpcode, pret.retmstoid AS trnoid, pret.retno, pret.rettype, CONVERT(VARCHAR(10), pret.retdate, 101) AS trndate, s.suppoid, s.suppname, pret.retmstnote, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, CONVERT(VARCHAR(10), retdate, 101) AS registerdate, ISNULL(retmstres2, '') AS retmstres2, (SELECT TOP 1 pom.curroid FROM QL_trnpomst pom) AS curroid, 0 refoidmst, retmstamt, retmstamtnetto, retmsttaxamt FROM QL_trnreturmst pret INNER JOIN QL_approval ap ON pret.retmstoid=ap.oid AND ap.statusrequest='New' INNER JOIN QL_mstsupp s ON s.suppoid=pret.suppoid WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_trnreturmst' AND pret.retmststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnreturmst")
    End Sub

    Private Sub SetGVDataPretRM(ByVal dtDataHeader As DataTable)
        Session("registerdate") = gvList.SelectedDataKey.Item("registerdate").ToString
        Session("curroid") = gvList.SelectedDataKey.Item("curroid").ToString
        Session("suppoid") = gvList.SelectedDataKey.Item("suppoid").ToString
        Session("typeretur") = gvList.SelectedDataKey.Item("retmstres2").ToString
        Session("refoidmst") = gvList.SelectedDataKey.Item("refoidmst").ToString
        Session("trndate") = gvList.SelectedDataKey.Item("trndate").ToString
        Session("retmstamtnetto") = gvList.SelectedDataKey.Item("retmstamtnetto").ToString
        Session("retmstamt") = gvList.SelectedDataKey.Item("retmstamt").ToString
        Session("retmsttaxamt") = gvList.SelectedDataKey.Item("retmsttaxamt").ToString
        lblreason.Visible = True : lblSeptReason.Visible = True : ActionReason.Visible = True
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "Return Date", "Supplier", "Return Note"}
        Dim arValue() As String = {"trnoid", "trndate", "suppname", "retmstnote"}
        Dim arType() As String = {"S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arHeader() As String = {"No.", "Mat. Code", "Mat. Desc", "Quantity", "Unit", "Warehouse", "Note"}
        Dim arField() As String = {"seq", "itemcode", "itemdesc", "qty", "unit", "warehouse", "note"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        If Session("typeretur") = "MR" Then
            sSql = "SELECT pretd.retdtlseq AS seq, pretd.retdtloid, pret.rettype AS tipe, m.itemoid AS matoid, m.itemCode AS itemcode, m.itemLongDescription AS itemdesc, pretd.retqty AS qty, g.gendesc AS unit, g2.gendesc AS warehouse, pretd.retdtlnote AS note, pretd.retwhoid, pretd.retdtlamt AS dtlamt, pretd.retvalue AS mrvalue, pretd.retvalueidr AS mrvalueidr, pretd.retvalueusd AS mrvalueusd, mrm.potype AS refname, mrd.podtloid AS podtloid, pret.retmstres2 AS res2, sp.suppname AS suppname,retqty_unitkecil AS qtyunitkecil, retqty_unitbesar AS qtyunitbesar, ISNULL(gx.genother1,0) stockacctgoid, pretd.retdtlamtnetto FROM QL_trnreturmst pret INNER JOIN QL_trnreturdtl pretd ON pret.retmstoid=pretd.retmstoid INNER JOIN QL_mstitem m ON pretd.matoid=m.itemoid INNER JOIN QL_mstgen g ON pretd.retunitoid=g.genoid INNER JOIN QL_mstgen g2 ON pretd.retwhoid=g2.genoid INNER JOIN QL_trnmrdtl mrd ON mrd.cmpcode=pretd.cmpcode AND mrd.mrdtloid=pretd.refoiddtl INNER JOIN QL_trnmrmst mrm ON mrm.mrmstoid=mrd.mrmstoid INNER JOIN QL_mstsupp sp ON sp.suppoid=pret.suppoid INNER JOIN QL_mstgen gx ON gx.genoid=m.itemgroupoid WHERE pret.retmstoid=" & Session("trnoid") & " ORDER BY pretd.retdtlseq"
        ElseIf Session("typeretur") = "PI" Then
            sSql = "SELECT pretd.retdtlseq AS seq, pretd.retdtloid, pret.retmstoid, pret.rettype AS tipe, m.itemoid AS matoid, m.itemCode AS itemcode, m.itemLongDescription AS itemdesc, pretd.retqty AS qty, g.gendesc AS unit, gx.gendesc AS warehouse, pretd.retdtlnote AS note, pretd.retwhoid, pretd.retdtlamt AS dtlamt, pretd.retvalue AS mrvalue, pretd.retvalueidr AS mrvalueidr, pretd.retvalueusd AS mrvalueusd, (SELECT potype FROM QL_trnmrmst m WHERE m.cmpcode=mrd.cmpcode AND m.mrmstoid=mrd.mrmstoid) AS refname, (SELECT podtloid FROM QL_trnmrdtl md WHERE md.mrdtloid=pretd.mrdtloid) AS podtloid, pret.retmstres2 AS res2, sp.suppname AS suppname, retqty_unitkecil AS qtyunitkecil, retqty_unitbesar AS qtyunitbesar, mrm.trnbelimstoid, ISNULL(gx1.genother1,0) stockacctgoid, pretd.retdtlamtnetto FROM QL_trnreturmst pret INNER JOIN QL_trnreturdtl pretd ON pret.retmstoid=pretd.retmstoid INNER JOIN QL_mstitem m ON pretd.matoid=m.itemoid INNER JOIN QL_mstgen g ON pretd.retunitoid=g.genoid INNER JOIN QL_trnbelidtl mrd ON mrd.cmpcode=pretd.cmpcode AND mrd.trnbelidtloid=pretd.refoiddtl INNER JOIN QL_trnbelimst mrm ON mrm.trnbelimstoid=pretd.refoidmst INNER JOIN QL_mstsupp sp ON sp.suppoid=pret.suppoid INNER JOIN QL_mstgen gx ON gx.genoid=pretd.retwhoid INNER JOIN QL_mstgen gx1 ON gx1.genoid=m.itemgroupoid WHERE pret.retmstoid=" & Session("trnoid") & " ORDER BY pretd.retdtlseq"
        Else
            sSql = "SELECT pretd.retdtlseq AS seq, pretd.retdtloid, pret.retmstoid, pret.rettype AS tipe, m.itemoid AS matoid, m.itemCode AS itemcode, m.itemLongDescription AS itemdesc, pretd.retqty AS qty, g.gendesc AS unit, gx.gendesc AS warehouse, pretd.retdtlnote AS note, pretd.retwhoid, pretd.retdtlamt AS dtlamt, pretd.retvalue AS mrvalue, pretd.retvalueidr AS mrvalueidr, pretd.retvalueusd AS mrvalueusd, ISNULL((SELECT gx1.gendesc FROM QL_mstgen gx1 WHERE gx1.genoid=m.itemgroupoid),'') AS refname, 0 AS podtloid, pret.retmstres2 AS res2, sp.suppname AS suppname, retqty_unitkecil AS qtyunitkecil, retqty_unitbesar AS qtyunitbesar, mrm.trnbelimstoid, ISNULL(gx1.genother1,0) stockacctgoid, pretd.retdtlamtnetto FROM QL_trnreturmst pret INNER JOIN QL_trnreturdtl pretd ON pret.retmstoid=pretd.retmstoid INNER JOIN QL_mstitem m ON pretd.matoid=m.itemoid INNER JOIN QL_mstgen g ON pretd.retunitoid=g.genoid LEFT JOIN QL_trnbelimst mrm ON mrm.trnbelimstoid=pretd.refoidmst INNER JOIN QL_mstsupp sp ON sp.suppoid=pret.suppoid INNER JOIN QL_mstgen gx ON gx.genoid=pretd.retwhoid INNER JOIN QL_mstgen gx1 ON gx1.genoid=m.itemgroupoid WHERE pret.retmstoid=" & Session("trnoid") & " ORDER BY pretd.retdtlseq"
        End If
        Session("TblDtlPretRaw") = ckon.ambiltabel(sSql, "QL_trnreturdtl")
        gvData.DataSource = Session("TblDtlPretRaw")
        gvData.DataBind()
    End Sub

    Private Function GeneratePretRMNo() As String
        Dim rettype As String = GetStrData("SELECT retmstres2 FROM QL_trnreturmst where retmstoid=" & Session("trnoid") & "")
        If rettype.ToUpper = "INITIAL AP" Then
            rettype = "PIB"
        End If
        Dim sNo As String = "RET-" & rettype & "-" & Format(CDate(Session("trndate")), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(retno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnreturmst WHERE cmpcode='" & AppCmpCode.Text & "' AND retno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub GenerateDPAPno(ByVal numb1 As Double)
        Dim sNod As String = "DPAP-" & Format(CDate(Session("trndate")), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(dpapno, 4) AS INTEGER)) + 1 + " & numb1 & ", 1) AS IDNEW FROM QL_trndpap WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dpapno LIKE '%" & sNod & "%'"
        Session("sNodp") = GenNumberString(sNod, "", GetStrData(sSql), DefaultFormatCounter)
    End Sub

    Private Sub GenerateCBno(ByVal kas As Integer, ByVal numb2 As Double)
        Dim sNoc As String = "BKK-" & Format(CDate(Session("trndate")), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, 4) AS INTEGER)) + 1 + " & numb2 & ", 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankno LIKE '%" & sNoc & "%' AND acctgoid=" & kas
        Session("sNocb") = GenNumberString(sNoc, "", GetStrData(sSql), DefaultFormatCounter)
    End Sub

    Private Sub UpdatePretRM(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select Purchase Return Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select Purchase Return Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim iSeq As Integer = 1
        Dim isExec As Boolean = False
        Dim iConAPOid As Integer = GenerateID("QL_CONAP", CompnyCode)
        Dim conmtroid As Integer = GenerateID("QL_CONSTOCK", CompnyCode)
        Dim crdmatoid As Integer = GenerateID("QL_CRDSTOCK", CompnyCode)
        Dim iStockValOid As Integer = GenerateID("QL_STOCKVALUE", CompnyCode)
        Dim glmstoid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
        Dim gldtloid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
        Dim cashbankoid As Integer = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
        Dim dpapoid As Integer = GenerateID("QL_TRNDPAP", CompnyCode)
        Dim iStockAcctgOid, iRecAcctgOid, iKasAcctgOid, iDPAcctgOid, iGRIR, iPPNAcctgOid As Integer
        Dim dAcctgAmt, dAcctgAmtIDR, dAcctgAmtUSD As Double
        Dim cRate As New ClassRate()
        If cRate.GetRateDailyLastError <> "" Then
            showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
        End If
        If cRate.GetRateMonthlyLastError <> "" Then
            showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
        End If
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
        Dim sDate2 As String = Session("trndate")
        Dim sPeriod2 As String = GetDateToPeriodAcctg(CDate(sDate2))
        Dim podtloid, pomstoid, itemoid As Integer
        Dim retAmt, retAmtIDR, retAmtUSD, retAmtTax, retAmtTaxIDR, retAmtTaxUSD, taxAmt, taxAmtIDR, taxAmtUSD, cekBayar As Double
        Dim pomstALL As String = "" : Dim itemALL As String = ""
        Dim numb As Double = 0

        ' Define Tabel Strucure utk Auto Jurnal 
        sSql = "SELECT '' tipe, 0 acctgoid, 0.0 debet, 0.0 credit, '' refno, '' note, '' AS glother1 "
        Dim tbPostGL As DataTable = ckon.ambiltabel(sSql, "PostGL")
        tbPostGL.Rows.Clear()
        Dim oRow As DataRow
        Dim dvCek As DataView = tbPostGL.DefaultView

        If sStatus = "Approved" Then
            Dim sVarErr As String = ""
            If Not IsInterfaceExists("VAR_STOCK_RETUR", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_STOCK_RETUR"
            End If
            If Not IsInterfaceExists("VAR_AP_RETUR", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_AP_RETUR"
            End If
            If Not IsInterfaceExists("VAR_KAS_RETUR", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_KAS_RETUR"
            End If
            If Not IsInterfaceExists("VAR_DPAP_RETUR", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_DPAP_RETUR"
            End If
            If Not IsInterfaceExists("VAR_PURC_RECEIVED", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_PURC_RECEIVED"
            End If
            If Not IsInterfaceExists("VAR_PPN_IN", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_PPN_IN"
            End If
            If sVarErr <> "" Then
                showMessage(GetInterfaceWarning(sVarErr, "approved"), CompnyName & " - WARNING", 2)
                Exit Sub
            End If
            iGRIR = GetAcctgOID(GetVarInterface("VAR_PURC_RECEIVED", AppCmpCode.Text), CompnyCode)
            iRecAcctgOid = ToInteger(GetStrData("SELECT apacctgoid FROM QL_mstsupp WHERE suppoid=" & Session("suppoid").ToString))
            iKasAcctgOid = GetAcctgOID(GetVarInterface("VAR_KAS_RETUR", AppCmpCode.Text), CompnyCode)
            iDPAcctgOid = GetAcctgOID(GetVarInterface("VAR_DPAP_RETUR", AppCmpCode.Text), CompnyCode)
            iPPNAcctgOid = GetAcctgOID(GetVarInterface("VAR_PPN_IN", AppCmpCode.Text), CompnyCode)
            cRate.SetRateValue(ToInteger(Session("curroid")), Session("registerdate"))

            Dim dt As DataTable = Session("TblDtlPretRaw")
            For C1 As Integer = 0 To dt.Rows.Count - 1
                'If Session("typeretur") = "MR" Then
                If Not IsStockAvailable(AppCmpCode.Text, sPeriod, dt.Rows(C1)("matoid"), dt.Rows(C1)("retwhoid"), ToDouble(dt.Rows(C1)("qty").ToString)) Then
                    showMessage("Every Return Qty must be less than Stock Qty!", CompnyName & " - WARNING", 2)
                    Exit Sub
                End If
                'End If
            Next

            Dim obj As DataTable = Session("TblDtlPretRaw")
            For C1 As Int16 = 0 To obj.Rows.Count - 1
                'Set ID PO mst
                podtloid = obj.Rows(C1).Item("podtloid").ToString
                itemoid = obj.Rows(C1).Item("matoid").ToString
                sSql = "SELECT DISTINCT pomstoid FROM QL_trnpodtl WHERE podtloid=" & obj.Rows(C1).Item("podtloid").ToString & ""
                pomstoid = ckon.ambilscalar(sSql)
                pomstALL = pomstALL & pomstoid & ","
                itemALL = itemALL & itemoid & ","
            Next
            If (pomstALL <> "") Then
                pomstALL = pomstALL.Substring(0, pomstALL.Length - 1)
            End If
            sSql = "select SUM(rd.retdtlamtnetto) AS glamt, SUM(retdtltaxamt) AS gltaxamt, SUM(rd.retdtlamt) glamt_stock, (select genother1 from QL_mstgen where gengroup='GROUPITEM' AND genoid=i.itemgroupoid) AS acctgoid, refoidmst from QL_trnreturmst rm inner join QL_trnreturdtl rd on rd.retmstoid=rm.retmstoid INNER JOIN QL_mstitem i ON i.itemoid=rd.matoid where rm.retmstoid IN (" & Session("trnoid") & ") group by itemgroupoid, refoidmst"
            Dim tbl As DataTable = ckon.ambiltabel(sSql, "QL_trnpomst")
            Session("TblGroup") = tbl
            If (itemALL <> "") Then
                itemALL = itemALL.Substring(0, itemALL.Length - 1)
            End If
            If Session("typeretur").ToString.ToUpper = "NOTA LAMA" Then
                sSql = "select g.genother1 AS akun, SUM(rd.retdtlamtnetto) nett from QL_mstitem i inner join QL_mstgen g ON g.genoid=itemgroupoid INNER JOIN QL_trnreturdtl rd ON rd.matoid=i.itemoid INNER JOIN QL_trnreturmst rm ON rm.retmstoid=rd.retmstoid where rm.cmpcode='" & AppCmpCode.Text & "' AND rm.retmstoid=" & Session("trnoid") & " AND i.itemoid in(" & itemALL & ") group by g.genother1"
                Dim tbl1 As DataTable = ckon.ambiltabel(sSql, "QL_mstitem")
                Session("Tblitem") = tbl1
            End If
        End If

        If sStatus = "Rejected" Or sStatus = "Revised" Then
            If ActionReason.Text = "" Then
                showMessage("Please Fill " & sStatus & " Reason First!", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If

        If sStatus = "Approved" Then
            If Session("typeretur").ToString.ToUpper <> "MR" Then
                If Session("typeretur").ToString.ToUpper = "PI" Then
                    'Cek Pembayaran Lunas atau Belum
                    sSql = "SELECT refoidmst, suppoid, amtsupp, ISNULL((select (select ISNULL(SUM(con.amttrans),0) from QL_conap con where con.reftype='QL_trnbelimst' and con.refoid=jm.trnbelimstoid and con.suppoid=jm.suppoid and payrefoid=0) - (select ISNULL(SUM(con.amtbayar),0) from QL_conap con where con.reftype='QL_trnbelimst' and con.refoid=jm.trnbelimstoid and con.suppoid=jm.suppoid and payrefoid IN (select payapoid from QL_trnpayap pay where pay.reftype='QL_trnbelimst' and pay.refoid=jm.trnbelimstoid and pay.suppoid=jm.suppoid and ISNULL(pay.payapres1,'')<>'Lebih Bayar'))-(select ISNULL(SUM(con.amtbayar),0) from QL_conap con where con.reftype='QL_trnbelimst' and con.refoid=jm.trnbelimstoid and con.suppoid=jm.suppoid and trnaptype<>'PAYAP') from QL_trnbelimst jm WHERE jm.cmpcode='" & AppCmpCode.Text & "' AND jm.trnbelimstoid=refoidmst),0) cekbyr, '' AS cashbankno, '' AS dpapno FROM( SELECT rd.refoidmst, blm.suppoid, SUM(rd.retdtlamtnetto) AS amtsupp from QL_trnreturdtl rd INNER JOIN QL_trnbelidtl bld ON bld.trnbelidtloid=rd.refoiddtl AND bld.trnbelimstoid=rd.refoidmst INNER JOIN QL_trnbelimst blm ON blm.trnbelimstoid=rd.refoidmst where rd.cmpcode='" & AppCmpCode.Text & "' AND rd.retmstoid=" & Session("trnoid") & " group by blm.suppoid, refoidmst ) AS t"
                ElseIf Session("typeretur").ToString.ToUpper = "INITIAL AP" Then
                    'Cek Pembayaran Lunas atau Belum
                    sSql = "SELECT refoidmst, suppoid, amtsupp, ISNULL((select (select ISNULL(SUM(con.amttrans),0) from QL_conap con where con.reftype='QL_trnbelimst' and con.refoid=jm.trnbelimstoid and con.suppoid=jm.suppoid and payrefoid=0) - (select ISNULL(SUM(con.amtbayar),0) from QL_conap con where con.reftype='QL_trnbelimst' and con.refoid=jm.trnbelimstoid and con.suppoid=jm.suppoid and payrefoid IN (select payapoid from QL_trnpayap pay where pay.reftype='QL_trnbelimst' and pay.refoid=jm.trnbelimstoid and pay.suppoid=jm.suppoid and ISNULL(pay.payapres1,'')<>'Lebih Bayar'))-(select ISNULL(SUM(con.amtbayar),0) from QL_conap con where con.reftype='QL_trnbelimst' and con.refoid=jm.trnbelimstoid and con.suppoid=jm.suppoid and trnaptype<>'PAYAP') from QL_trnbelimst jm WHERE jm.cmpcode='" & AppCmpCode.Text & "' AND jm.trnbelimstoid=refoidmst),0) cekbyr, '' AS cashbankno, '' AS dpapno FROM( SELECT rd.refoidmst, blm.suppoid, SUM(rd.retdtlamtnetto) AS amtsupp from QL_trnreturdtl rd INNER JOIN QL_trnbelimst blm ON blm.trnbelimstoid=rd.refoidmst where rd.cmpcode='" & AppCmpCode.Text & "' AND rd.retmstoid=" & Session("trnoid") & " group by blm.suppoid, refoidmst ) AS t"
                ElseIf Session("typeretur").ToString.ToUpper = "NOTA LAMA" Then
                    'Cek Pembayaran Lunas atau Belum
                    sSql = "SELECT refoidmst, suppoid, amtsupp, 0 cekbyr, '' AS cashbankno, '' AS dpapno FROM( SELECT rd.refoidmst, blm.suppoid, SUM(rd.retdtlamtnetto) AS amtsupp from QL_trnreturdtl rd INNER JOIN QL_trnreturmst blm ON blm.retmstoid=rd.retmstoid where rd.cmpcode='" & AppCmpCode.Text & "' AND rd.retmstoid=" & Session("trnoid") & " group by blm.suppoid, refoidmst ) AS t"
                End If
                Dim tbl1 As DataTable = ckon.ambiltabel(sSql, "QL_conap")
                Session("TblBayar") = tbl1

                Dim obj2 As DataTable = Session("TblBayar")
                'For C1 As Int16 = 0 To obj2.Rows.Count - 1
                'If ToDouble(obj2.Rows(C1).Item("cekbyr")) > 0 Then
                'Generate cashbankno
                GenerateCBno(iKasAcctgOid, ToDouble(numb))
                'obj2.Rows(C1).Item("cashbankno") = Session("sNocb")
                'Generate dpapno
                GenerateDPAPno(ToDouble(numb))
                'obj2.Rows(C1).Item("dpapno") = Session("sNodp")
                numb += 1
                'End If
                'Next
            End If
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Dim objTable2 As DataTable = Session("TblDtlPretRaw")
        Try
            ' Update Purchase Return
            If sStatus = "Approved" Then
                sSql = "UPDATE QL_trnreturmst SET retno='" & sNo & "', retmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP"
            Else
                sSql = "UPDATE QL_trnreturmst SET retno='" & sNo & "', retmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP" & IIf(sStatus = "Rejected", ", rejectreason='" & Tchar(ActionReason.Text) & "', rejectuser='" & Session("UserID") & "', rejecttime=CURRENT_TIMESTAMP", ", revisereason='" & Tchar(ActionReason.Text) & "', reviseuser='" & Session("UserID") & "', revisetime=CURRENT_TIMESTAMP")
            End If
            sSql &= " WHERE retmststatus='In Approval' AND retmstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnreturmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            'If isExec Then
            If sStatus = "Rejected" Then
                If DDLReturnType.SelectedValue = "MR" Then
                    sSql = "UPDATE QL_trnmrdtl SET mrdtlres1='' WHERE cmpcode='" & AppCmpCode.Text & "' AND mrdtloid IN (SELECT refoiddtl FROM QL_trnreturdtl WHERE cmpcode='" & AppCmpCode.Text & "' AND retmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnmrmst SET mrmstres1='' WHERE cmpcode='" & AppCmpCode.Text & "' AND mrmstoid IN (SELECT refoidmst FROM QL_trnreturdtl WHERE cmpcode='" & AppCmpCode.Text & "' AND retmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trnbelidtl SET trnbelidtlres1='' WHERE cmpcode='" & AppCmpCode.Text & "' AND trnbelidtloid IN (SELECT refoiddtl FROM QL_trnreturdtl WHERE cmpcode='" & AppCmpCode.Text & "' AND retmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnbelimst SET trnbelimstres1='' WHERE cmpcode='" & AppCmpCode.Text & "' AND trnbelimstoid IN (SELECT refoidmst FROM QL_trnreturdtl WHERE cmpcode='" & AppCmpCode.Text & "' AND retmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If

            ElseIf sStatus = "Approved" Then
                Dim objTable As DataTable = Session("TblDtlPretRaw")
                For C1 As Int16 = 0 To objTable.Rows.Count - 1
                    ' Insert QL_conmat
                    sSql = "INSERT INTO QL_constock (cmpcode, constockoid, contype, trndate, formaction, formoid, periodacctg, refname, refoid, mtrlocoid, qtyin, qtyout, amount, hpp, typemin, note, reason, createuser, createtime, upduser, updtime, refno, valueidr, valueusd, qtyin_unitbesar, qtyout_unitbesar) VALUES ('" & AppCmpCode.Text & "', " & conmtroid & ", '" & objTable.Rows(C1).Item("tipe") & "',  '" & sDate & "', 'QL_trnreturdtl', " & Session("trnoid") & ", '" & sPeriod & "', '" & objTable.Rows(C1).Item("refname") & "', " & objTable.Rows(C1).Item("matoid") & "," & objTable.Rows(C1).Item("retwhoid") & ", 0, " & ToDouble(objTable.Rows(C1).Item("qtyunitkecil")) & ", 0, 0, -1, '" & sNo & " # Ke " & Tchar(objTable.Rows(C1).Item("suppname").ToString) & "', 'Purchase Return', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & sNo & "-" & objTable.Rows(C1).Item("suppname") & "', " & ToDouble(objTable.Rows(C1).Item("mrvalueidr").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("mrvalueusd").ToString) & ", 0, " & ToDouble(objTable.Rows(C1).Item("qtyunitbesar")) & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    conmtroid += 1

                    dvCek.RowFilter = "acctgoid=" & objTable.Rows(C1).Item("stockacctgoid").ToString & " "
                    If dvCek.Count > 0 Then
                        dvCek(0)("credit") += ToDouble(objTable.Rows(C1).Item("mrvalue").ToString) * ToDouble(objTable.Rows(C1).Item("qtyunitkecil").ToString)
                        dvCek.RowFilter = ""
                    Else
                        dvCek.RowFilter = ""

                        oRow = tbPostGL.NewRow
                        oRow("acctgoid") = objTable.Rows(C1).Item("stockacctgoid")
                        oRow("credit") = ToDouble(objTable.Rows(C1).Item("mrvalue").ToString) * ToDouble(objTable.Rows(C1).Item("qtyunitkecil").ToString)
                        tbPostGL.Rows.Add(oRow)
                    End If

                    'Set Amount IDR dan USD
                    dAcctgAmt += ToDouble(objTable.Rows(C1).Item("qtyunitkecil").ToString) * ToDouble(objTable.Rows(C1).Item("mrvalue").ToString)
                    dAcctgAmtIDR += ToDouble(objTable.Rows(C1).Item("qtyunitkecil").ToString) * ToDouble(objTable.Rows(C1).Item("mrvalueidr").ToString)
                    dAcctgAmtUSD += ToDouble(objTable.Rows(C1).Item("qtyunitkecil").ToString) * ToDouble(objTable.Rows(C1).Item("mrvalueusd").ToString)
                    'Set ID PO mst dan dtl
                    podtloid = objTable.Rows(C1).Item("podtloid").ToString
                    sSql = "SELECT DISTINCT pomstoid FROM QL_trnpodtl WHERE podtloid=" & objTable.Rows(C1).Item("podtloid").ToString & ""
                    xCmd.CommandText = sSql
                    pomstoid = xCmd.ExecuteScalar
                    If Session("typeretur") = "MR" Then
                        'Update PO Detail
                        sSql = "UPDATE QL_trnpodtl SET podtlstatus='' WHERE cmpcode='" & AppCmpCode.Text & "' AND podtloid=" & podtloid
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        'Update PO Mst
                        sSql = "UPDATE QL_trnpomst SET pomststatus='Approved' WHERE cmpcode='" & AppCmpCode.Text & "' AND pomstoid=" & pomstoid
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Else
                        If cekBayar > 0 Then
                            'Update PO Detail
                            sSql = "UPDATE QL_trnpodtl SET podtlstatus='' WHERE cmpcode='" & AppCmpCode.Text & "' AND podtloid=" & podtloid
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            'Update PO Mst
                            sSql = "UPDATE QL_trnpomst SET pomststatus='Approved' WHERE cmpcode='" & AppCmpCode.Text & "' AND pomstoid=" & pomstoid
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                    End If
                Next

                'update mstoid stock
                sSql = "UPDATE QL_mstoid SET lastoid=" & conmtroid - 1 & " WHERE tablename='QL_constock' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                If Session("typeretur").ToString.ToUpper = "MR" Then
                    ' Insert GL MST
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & AppCmpCode.Text & "', " & glmstoid & ", '" & sDate & "', '" & sPeriod & "', 'Retur|No. " & sNo & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Insert GL DTL
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & iGRIR & ", 'D', " & dAcctgAmt & ", '" & sNo & "', 'Retur|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dAcctgAmt & ", " & dAcctgAmt & ", 'QL_trnreturmst " & Session("trnoid") & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    gldtloid += 1 : iSeq += 1
                    For C3 As Integer = 0 To tbPostGL.Rows.Count - 1
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & tbPostGL.Rows(C3)("acctgoid").ToString & ", 'C', " & ToDouble(tbPostGL.Rows(C3)("credit")) & ", '" & sNo & "', 'Retur|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(tbPostGL.Rows(C3)("credit")) & ", " & ToDouble(tbPostGL.Rows(C3)("credit")) & ", 'QL_trnreturmst " & Session("trnoid") & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        gldtloid += 1 : iSeq += 1
                    Next
                    glmstoid += 1
                End If

                'Posting Jurnal Purchase Return
                Dim obj5 As DataTable = Session("TblBayar")
                If Not obj5 Is Nothing Then
                    If obj5.Rows.Count > 0 Then
                        For C2 As Int16 = 0 To obj5.Rows.Count - 1
                            'Jurnal Untuk Nota Lama
                            If Session("typeretur").ToString.ToUpper = "NOTA LAMA" Then
                                Dim obj4 As DataTable = Session("Tblitem")
                                For C1 As Int16 = 0 To obj4.Rows.Count - 1
                                    iStockAcctgOid = obj4.Rows(C1).Item("akun").ToString
                                    ' Insert GL MST
                                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & AppCmpCode.Text & "', " & glmstoid & ", '" & sDate & "', '" & sPeriod & "', 'Retur|No. " & sNo & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                                    xCmd.CommandText = sSql
                                    xCmd.ExecuteNonQuery()

                                    ' Insert GL DTL
                                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & gldtloid & ", 1, " & glmstoid & ", " & iKasAcctgOid & ", 'D', " & dAcctgAmt & ", '" & sNo & "', 'Retur|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dAcctgAmtIDR & ", " & dAcctgAmtUSD & ", 'QL_trnreturmst " & Session("trnoid") & "')"
                                    xCmd.CommandText = sSql
                                    xCmd.ExecuteNonQuery()
                                    gldtloid += 1
                                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & gldtloid & ", 2, " & glmstoid & ", " & iStockAcctgOid & ", 'C', " & ToDouble(obj4.Rows(C1).Item("nett")) & ", '" & sNo & "', 'Retur|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(obj4.Rows(C1).Item("nett")) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(obj4.Rows(C1).Item("nett")) * cRate.GetRateMonthlyUSDValue & ", 'QL_trnreturmst " & Session("trnoid") & "')"
                                    xCmd.CommandText = sSql
                                    xCmd.ExecuteNonQuery()
                                    gldtloid += 1
                                Next
                                glmstoid += 1

                            Else
                                'Jurnal Retur MR, PI dan Init
                                Dim dtData As DataTable = Session("TblGroup")
                                Dim dvData As DataView = dtData.DefaultView
                                Dim dAPAmt As Double = ToDouble(dtData.Compute("SUM(glamt)", ""))
                                Dim dTaxAmt As Double = ToDouble(dtData.Compute("SUM(gltaxamt)", ""))

                                'Journal Retur MR & PI
                                ' Insert GL MST
                                sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & AppCmpCode.Text & "', " & glmstoid & ", '" & sDate & "', '" & sPeriod & "', 'Retur|No. " & sNo & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                If Session("typeretur").ToString.ToUpper = "MR" Then
                                    'nothing
                                Else
                                    Dim db_AcctgOid As Integer = iRecAcctgOid
                                    If ToDouble(obj5.Rows(C2).Item("cekbyr")) <= 0 Then
                                        db_AcctgOid = iKasAcctgOid
                                    End If
                                    dvData.RowFilter = "refoidmst=" & obj5.Rows(C2).Item("refoidmst")
                                    For C3 As Integer = 0 To dvData.Count - 1
                                        ' Insert GL DTL
                                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & db_AcctgOid & ", 'D', " & ToDouble(dvData(C3)("glamt")) & ", '" & sNo & "', 'Retur|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dvData(C3)("glamt")) & ", " & ToDouble(dvData(C3)("glamt")) & ", 'QL_trnreturmst " & Session("trnoid") & "')"
                                        xCmd.CommandText = sSql
                                        xCmd.ExecuteNonQuery()
                                        gldtloid += 1 : iSeq += 1
                                        If dTaxAmt > 0 Then
                                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & iPPNAcctgOid & ", 'C', " & ToDouble(dvData(C3)("gltaxamt")) & ", '" & sNo & "', 'Retur|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dvData(C3)("gltaxamt")) & ", " & ToDouble(dvData(C3)("gltaxamt")) & ", 'QL_trnreturmst " & Session("trnoid") & "')"
                                            xCmd.CommandText = sSql
                                            xCmd.ExecuteNonQuery()
                                            gldtloid += 1 : iSeq += 1
                                        End If

                                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & dvData(C3)("acctgoid").ToString & ", 'C', " & ToDouble(dvData(C3)("glamt_stock")) & ", '" & sNo & "', 'Retur|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dvData(C3)("glamt_stock")) & ", " & ToDouble(dvData(C3)("glamt_stock")) & ", 'QL_trnreturmst " & Session("trnoid") & "')"
                                        xCmd.CommandText = sSql
                                        xCmd.ExecuteNonQuery()
                                        gldtloid += 1 : iSeq += 1
                                    Next
                                    dvData.RowFilter = ""
                                End If
                                glmstoid += 1
                            End If
                        Next
                    End If
                End If

                'Cek Pembayaran Lunas atau Belum
                If Session("typeretur").ToString.ToUpper <> "MR" Then
                    Dim obj3 As DataTable = Session("TblBayar")
                    If Not obj3 Is Nothing Then
                        If obj3.Rows.Count > 0 Then
                            Dim dtv As DataView = obj3.DefaultView
                            dtv.RowFilter = "cekbyr<=0"
                            If dtv.Count > 0 Then
                                Dim amtDP As Double = ToDouble(obj3.Compute("SUM(amtsupp)", "cekbyr<=0"))
                                'insert cashbankmst
                                sSql = "INSERT INTO QL_trncashbankmst (cmpcode, cashbankoid, periodacctg, cashbankno, cashbankdate, cashbanktype, cashbankgroup, acctgoid, curroid, cashbankamt, cashbankamtidr, cashbankamtusd, personoid, cashbankduedate, cashbankrefno, cashbanknote, cashbankstatus, createuser, createtime, upduser, updtime, cashbanktakegiro, giroacctgoid, addacctgoid1, addacctgamt1, addacctgoid2, addacctgamt2, addacctgoid3, addacctgamt3) VALUES ('" & DDLBusUnit.SelectedValue & "', " & cashbankoid & ", '" & sPeriod & "', '" & Session("sNocb") & "', '" & sDate & "', 'BKK', 'DPAP', " & iKasAcctgOid & ", 1, " & ToDouble(amtDP) & ", " & ToDouble(amtDP) & ", " & ToDouble(amtDP) & ", " & Session("suppoid") & ", '1/1/1900', '" & sNo & "', 'From Purchase Retur|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '1/1/1900', 0,0,0,0,0,0,0)"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()

                                'insert dpap
                                sSql = "INSERT INTO QL_trndpap (cmpcode, dpapoid, periodacctg, dpapno, dpapdate, suppoid, acctgoid, cashbankoid, dpappaytype, dpappayacctgoid, dpappayrefno, dpapduedate, curroid, rateoid, rate2oid, dpapamt, dpapaccumamt, dpapnote, dpapstatus, createuser, createtime, upduser, updtime, dpaptakegiro, giroacctgoid, addacctgoid1, addacctgamt1, addacctgoid2, addacctgamt2, addacctgoid3, addacctgamt3, dpapamtidr, dpapamtusd) VALUES ('" & AppCmpCode.Text & "', " & dpapoid & ", '" & sPeriod & "', '" & Session("sNodp") & "', '" & sDate & "', " & Session("suppoid") & ", " & iDPAcctgOid & ", " & cashbankoid & ", 'BKK', " & iKasAcctgOid & ", '" & sNo & "', '1/1/1900', 1, " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & ToDouble(amtDP) & ", 0, 'From Purchase Retur|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '1/1/1900', 0,0,0,0,0,0,0, " & ToDouble(amtDP) & ", " & ToDouble(amtDP) & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()

                                ' Insert GL MST utk DPAP
                                sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & AppCmpCode.Text & "', " & glmstoid & ", '" & sDate & "', '" & sPeriod & "', 'DP A/P|No=" & Session("sNodp") & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()

                                ' Insert GL DTL utk DPAP
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & gldtloid & ", 1, " & glmstoid & ", " & iDPAcctgOid & ", 'D', " & ToDouble(amtDP) & ", '" & Session("sNodp") & "', 'DP A/P|No=" & Session("sNodp") & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(amtDP) & ", " & ToDouble(amtDP) & ", 'QL_trndpap " & dpapoid & "')"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                gldtloid += 1
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & gldtloid & ", 2, " & glmstoid & ", " & iKasAcctgOid & ", 'C', " & ToDouble(amtDP) & ", '" & Session("sNodp") & "', 'DP A/P|No=" & Session("sNodp") & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(amtDP) & ", " & ToDouble(amtDP) & ", 'QL_trndpap " & dpapoid & "')"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                gldtloid += 1
                                'cashbankoid += 1 : dpapoid += 1

                                sSql = "UPDATE QL_mstoid SET lastoid=" & cashbankoid & " WHERE tablename='QL_TRNCASHBANKMST' and cmpcode='" & CompnyCode & "'"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                sSql = "UPDATE QL_mstoid SET lastoid=" & dpapoid & " WHERE tablename='QL_TRNDPAP' and cmpcode='" & CompnyCode & "'"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                glmstoid += 1
                            End If

                            For C1 As Int16 = 0 To obj3.Rows.Count - 1
                                If ToDouble(obj3.Rows(C1).Item("cekbyr")) > 0 Then 'Jika Belum Lunas
                                    'insert conap
                                    sSql = "INSERT INTO QL_conap (cmpcode, conapoid, reftype, refoid, payrefoid, suppoid, acctgoid, trnapstatus, trnaptype, trnapdate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans, amtbayar, trnapnote, trnapres1, trnapres2, trnapres3, createuser, createtime, upduser, updtime, amttransidr, amtbayaridr, amttransusd, amtbayarusd) VALUES ('" & AppCmpCode.Text & "', " & iConAPOid & ", 'QL_trnbelimst', " & obj3.Rows(C1).Item("refoidmst").ToString & ", " & Session("trnoid") & ", " & obj3.Rows(C1).Item("suppoid").ToString & ", " & iRecAcctgOid & ", 'Post', 'RETPI', '1/1/1900', '" & sPeriod & "', 0, '" & sDate & "', '" & sNo & "', 0, '" & sDate & "', 0," & ToDouble(obj3.Rows(C1).Item("amtsupp")) & ", 'AP Payment Retur | No. " & sNo & "', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0," & ToDouble(obj3.Rows(C1).Item("amtsupp")) * cRate.GetRateMonthlyIDRValue & ",  0," & ToDouble(obj3.Rows(C1).Item("amtsupp")) * cRate.GetRateMonthlyUSDValue & ")"
                                    xCmd.CommandText = sSql
                                    xCmd.ExecuteNonQuery()
                                    iConAPOid += 1

                                    sSql = "UPDATE QL_mstoid SET lastoid=" & iConAPOid - 1 & " WHERE tablename='QL_CONAP' AND cmpcode='" & CompnyCode & "'"
                                    xCmd.CommandText = sSql
                                    xCmd.ExecuteNonQuery()
                                End If
                            Next
                        End If
                    End If
                End If

                'update mstoid GL
                sSql = "UPDATE QL_mstoid SET lastoid=" & glmstoid - 1 & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & gldtloid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "SO Raw Material Approval"
    Private Sub SetGVListSORM()
        Dim arHeader() As String = {"Draft No.", "SO Date", "Customer", "SO Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "custname", "sorawmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "custname", "sorawmstnote", "sorawtotalamt", "sorawmstdiscamt", "sorawvat", "sorawgrandtotalamt", "curroid", "custoid", "currcode", "soratetoidr", "upduser", "updtime"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT so.cmpcode, so.sorawmstoid AS trnoid, so.sorawno, CONVERT(VARCHAR(10), so.sorawdate, 101) AS trndate, c.custname, so.sorawmstnote, (so.sorawtotalamt - so.sorawtotaldiscdtl) AS sorawtotalamt, so.sorawmstdiscamt, so.sorawvat, so.sorawgrandtotalamt, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, so.curroid, so.custoid, currcode, rate2res1 AS soratetoidr, so.upduser, so.updtime FROM QL_trnsorawmst so INNER JOIN QL_mstcust c ON so.custoid=c.custoid INNER JOIN QL_approval ap ON so.sorawmstoid=ap.oid AND ap.statusrequest='New' INNER JOIN QL_mstcurr cu ON cu.curroid=so.curroid INNER JOIN QL_mstrate2 r ON r.rate2oid=so.rate2oid WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_trnsorawmst' AND so.sorawmststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnsorawmst")
    End Sub

    Private Sub SetGVDataSORM(ByVal dtDataHeader As DataTable)
        Session("curroid") = gvList.SelectedDataKey.Item("curroid").ToString
        Session("custoid") = gvList.SelectedDataKey.Item("custoid").ToString
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "SO Date", "Customer", "SO Note", "Currency", "Rate To IDR", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "custname", "sorawmstnote", "currcode", "soratetoidr", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "D", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "O", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            ElseIf arHidden(C1) = "O" Then
                If gvDataHeader.Rows(C1 - 1).Cells(2).Text = "IDR" Then
                    gvDataHeader.Rows(C1).Visible = False
                End If
            End If
        Next
        Dim arColNameList() As String = {"datacaption", "datasept", "datavalue", "dataempty"}
        Dim arColTypeList() As String = {"System.String", "System.String", "System.String", "System.String"}
        Dim dtDataFooter As DataTable = SetTableDetail("tblDataFooter", arColNameList, arColTypeList)
        Dim arCaptionF() As String = {"Total", "Header Disc.", "Tax", "Grand Total"}
        Dim arValueF() As String = {"sorawtotalamt", "sorawmstdiscamt", "sorawvat", "sorawgrandtotalamt"}
        Dim arTypeF() As String = {"D", "D", "D", "D"}
        Dim arHiddenF() As String = {"N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaptionF.Length - 1
            dr = dtDataFooter.NewRow
            dr("datacaption") = arCaptionF(C1)
            dr("datasept") = ":"
            If arTypeF(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValueF(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValueF(C1)).ToString), 2)
            End If
            dr("dataempty") = ""
            dtDataFooter.Rows.Add(dr)
        Next
        gvDataFooter.DataSource = dtDataFooter
        gvDataFooter.DataBind()
        For C1 As Int16 = 0 To arHiddenF.Length - 1
            If arHiddenF(C1) = "Y" Then
                gvDataFooter.Rows(C1).Visible = False
            End If
        Next
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Font.Bold = True
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).ForeColor = Drawing.Color.Black
        Dim arHeader() As String = {"No.", "Mat. Desc", "Quantity", "Unit", "Last Price", "Price", "Amount", "Disc.", "Netto", "Note"}
        Dim arField() As String = {"sorawdtlseq", "matrawshortdesc", "sorawqty", "unit", "lastsoprice", "sorawprice", "sorawdtlamt", "sorawdtldiscamt", "sorawdtlnetto", "sorawdtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT sod.sorawdtlseq, sod.matrawoid, m.matrawlongdesc AS matrawshortdesc, sod.sorawqty, g.gendesc AS unit, sod.sorawprice, sod.sorawdtlamt, sod.sorawdtldiscamt, sod.sorawdtlnetto, sod.sorawdtlnote, ISNULL((SELECT TOP 1 mp.lastsalesprice FROM QL_mstmatrawprice mp WHERE mp.cmpcode=so.cmpcode AND mp.matrawoid=sod.matrawoid AND mp.refname='CUSTOMER' AND mp.curroid=so.curroid ORDER BY mp.updtime DESC), 0.0) AS lastsoprice FROM QL_trnsorawmst so INNER JOIN QL_trnsorawdtl sod ON so.sorawmstoid=sod.sorawmstoid INNER JOIN QL_mstmatraw m ON sod.matrawoid=m.matrawoid INNER JOIN QL_mstgen g ON sod.sorawunitoid=g.genoid WHERE so.sorawmstoid=" & Session("trnoid") & " ORDER BY sod.sorawdtlseq"
        Session("TblDtlSO") = ckon.ambiltabel(sSql, "QL_trnsorawdtl")
        gvData.DataSource = Session("TblDtlSO")
        gvData.DataBind()
    End Sub

    Private Function GenerateSORMNo() As String
        Dim sNo As String = "SORM-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(sorawno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnsorawmst WHERE cmpcode='" & AppCmpCode.Text & "' AND sorawno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdateSORM(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select SO Raw Material Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select SO Raw Material Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim cRate As New ClassRate()
        If sStatus = "Approved" Then
            cRate.SetRateValue(CInt(Session("curroid")), Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim iPriceOid As Integer = GenerateID("QL_MSTMATRAWPRICE", CompnyCode)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update SO
            sSql = "UPDATE QL_trnsorawmst SET sorawno='" & sNo & "', sorawmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP"
            If sStatus = "Approved" Then
                sSql &= ", rateoid=" & cRate.GetRateDailyOid & ", rate2oid=" & cRate.GetRateMonthlyOid
            End If
            sSql &= " WHERE sorawmststatus='In Approval' AND sorawmstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnsorawmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Approved" Then
                    If Not Session("TblDtlSO") Is Nothing Then
                        Dim dtDtl As DataTable = Session("TblDtlSO")
                        For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                            sSql = "UPDATE QL_mstmatrawprice SET totalsalesqty=totalsalesqty + " & ToDouble(dtDtl.Rows(C1)("sorawqty").ToString) & ", avgsalesprice=((avgsalesprice * totalsalesqty) + " & ToDouble(dtDtl.Rows(C1)("sorawdtlamt").ToString) & ") / (totalsalesqty + " & ToDouble(dtDtl.Rows(C1)("sorawqty").ToString) & "), lastsalesprice=" & ToDouble(dtDtl.Rows(C1)("sorawprice").ToString) & ", lasttrans='QL_trnsorawmst', lasttransoid=" & Session("trnoid") & ", lasttransno='" & sNo & "', lasttransdate='" & Session("trndate") & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, ratetoidr=" & cRate.GetRateMonthlyIDRValue & " WHERE cmpcode='" & AppCmpCode.Text & "' AND matrawoid=" & ToDouble(dtDtl.Rows(C1)("matrawoid").ToString) & " AND refoid=" & Session("custoid") & " AND refname='CUSTOMER' AND curroid=" & Session("curroid")
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() <= 0 Then
                                sSql = "INSERT INTO QL_mstmatrawprice (cmpcode, matrawpriceoid, matrawoid, refoid, refname, totalpurchaseqty, totalsalesqty, avgpurchaseprice, lastpurchaseprice, avgsalesprice, lastsalesprice, lasttrans, lasttransoid, lasttransno, lasttransdate, note, res1, res2, res3, upduser, updtime, curroid, ratetoidr) VALUES ('" & AppCmpCode.Text & "', " & iPriceOid & ", " & ToDouble(dtDtl.Rows(C1)("matrawoid").ToString) & ", " & Session("custoid") & ", 'CUSTOMER', 0, " & ToDouble(dtDtl.Rows(C1)("sorawqty").ToString) & ", 0, 0, " & ToDouble(dtDtl.Rows(C1)("sorawprice").ToString) & ", " & ToDouble(dtDtl.Rows(C1)("sorawprice").ToString) & ", 'QL_trnsorawmst', " & Session("trnoid") & ", '" & sNo & "', '" & Session("trndate") & "', '', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Session("curroid") & ", " & cRate.GetRateMonthlyIDRValue & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iPriceOid += 1
                            End If
                        Next
                        sSql = "UPDATE QL_mstoid SET lastoid=" & iPriceOid - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_MSTMATRAWPRICE'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "SO General Material Approval"
    Private Sub SetGVListSOGM()
        Dim arHeader() As String = {"Draft No.", "SO Date", "Customer", "SO Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "custname", "sogenmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "custname", "sogenmstnote", "sogentotalamt", "sogenmstdiscamt", "sogenvat", "sogengrandtotalamt", "curroid", "custoid", "currcode", "soratetoidr", "upduser", "updtime"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT so.cmpcode, so.sogenmstoid AS trnoid, so.sogenno, CONVERT(VARCHAR(10), so.sogendate, 101) AS trndate, c.custname, so.sogenmstnote, (so.sogentotalamt - so.sogentotaldiscdtl) AS sogentotalamt, so.sogenmstdiscamt, so.sogenvat, so.sogengrandtotalamt, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, so.curroid, so.custoid, currcode, rate2res1 AS soratetoidr, so.upduser, so.updtime FROM QL_trnsogenmst so INNER JOIN QL_mstcust c ON so.custoid=c.custoid INNER JOIN QL_approval ap ON so.sogenmstoid=ap.oid AND ap.statusrequest='New' INNER JOIN QL_mstcurr cu ON cu.curroid=so.curroid INNER JOIN QL_mstrate2 r ON r.rate2oid=so.rate2oid WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_trnsogenmst' AND so.sogenmststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnsogenmst")
    End Sub

    Private Sub SetGVDataSOGM(ByVal dtDataHeader As DataTable)
        Session("curroid") = gvList.SelectedDataKey.Item("curroid").ToString
        Session("custoid") = gvList.SelectedDataKey.Item("custoid").ToString
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "SO Date", "Customer", "SO Note", "Currency", "Rate To IDR", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "custname", "sogenmstnote", "currcode", "soratetoidr", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "D", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "O", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            ElseIf arHidden(C1) = "O" Then
                If gvDataHeader.Rows(C1 - 1).Cells(2).Text = "IDR" Then
                    gvDataHeader.Rows(C1).Visible = False
                End If
            End If
        Next
        Dim arColNameList() As String = {"datacaption", "datasept", "datavalue", "dataempty"}
        Dim arColTypeList() As String = {"System.String", "System.String", "System.String", "System.String"}
        Dim dtDataFooter As DataTable = SetTableDetail("tblDataFooter", arColNameList, arColTypeList)
        Dim arCaptionF() As String = {"Total", "Header Disc.", "Tax", "Grand Total"}
        Dim arValueF() As String = {"sogentotalamt", "sogenmstdiscamt", "sogenvat", "sogengrandtotalamt"}
        Dim arTypeF() As String = {"D", "D", "D", "D"}
        Dim arHiddenF() As String = {"N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaptionF.Length - 1
            dr = dtDataFooter.NewRow
            dr("datacaption") = arCaptionF(C1)
            dr("datasept") = ":"
            If arTypeF(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValueF(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValueF(C1)).ToString), 2)
            End If
            dr("dataempty") = ""
            dtDataFooter.Rows.Add(dr)
        Next
        gvDataFooter.DataSource = dtDataFooter
        gvDataFooter.DataBind()
        For C1 As Int16 = 0 To arHiddenF.Length - 1
            If arHiddenF(C1) = "Y" Then
                gvDataFooter.Rows(C1).Visible = False
            End If
        Next
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Font.Bold = True
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).ForeColor = Drawing.Color.Black
        Dim arHeader() As String = {"No.", "Mat. Desc", "Quantity", "Unit", "Last Price", "Price", "Amount", "Disc.", "Netto", "Note"}
        Dim arField() As String = {"sogendtlseq", "matgenshortdesc", "sogenqty", "unit", "lastsoprice", "sogenprice", "sogendtlamt", "sogendtldiscamt", "sogendtlnetto", "sogendtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT sod.sogendtlseq, sod.matgenoid, m.matgenlongdesc AS matgenshortdesc, sod.sogenqty, g.gendesc AS unit, sod.sogenprice, sod.sogendtlamt, sod.sogendtldiscamt, sod.sogendtlnetto, sod.sogendtlnote, ISNULL((SELECT TOP 1 mp.lastsalesprice FROM QL_mstmatgenprice mp WHERE mp.cmpcode=so.cmpcode AND mp.matgenoid=sod.matgenoid AND mp.refname='CUSTOMER' AND mp.curroid=so.curroid ORDER BY mp.updtime DESC), 0.0) AS lastsoprice FROM QL_trnsogenmst so INNER JOIN QL_trnsogendtl sod ON so.sogenmstoid=sod.sogenmstoid INNER JOIN QL_mstmatgen m ON sod.matgenoid=m.matgenoid INNER JOIN QL_mstgen g ON sod.sogenunitoid=g.genoid WHERE so.sogenmstoid=" & Session("trnoid") & " ORDER BY sod.sogendtlseq"
        Session("TblDtlSO") = ckon.ambiltabel(sSql, "QL_trnsogendtl")
        gvData.DataSource = Session("TblDtlSO")
        gvData.DataBind()
    End Sub

    Private Function GenerateSOGMNo() As String
        Dim sNo As String = "SOGM-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(sogenno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnsogenmst WHERE cmpcode='" & AppCmpCode.Text & "' AND sogenno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdateSOGM(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select SO General Material Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select SO General Material Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim cRate As New ClassRate()
        If sStatus = "Approved" Then
            cRate.SetRateValue(CInt(Session("curroid")), Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim iPriceOid As Integer = GenerateID("QL_MSTMATRAWPRICE", CompnyCode)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update SO
            sSql = "UPDATE QL_trnsogenmst SET sogenno='" & sNo & "', sogenmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP"
            If sStatus = "Approved" Then
                sSql &= ", rateoid=" & cRate.GetRateDailyOid & ", rate2oid=" & cRate.GetRateMonthlyOid
            End If
            sSql &= " WHERE sogenmststatus='In Approval' AND sogenmstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnsogenmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Approved" Then
                    If Not Session("TblDtlSO") Is Nothing Then
                        Dim dtDtl As DataTable = Session("TblDtlSO")
                        For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                            sSql = "UPDATE QL_mstmatgenprice SET totalsalesqty=totalsalesqty + " & ToDouble(dtDtl.Rows(C1)("sogenqty").ToString) & ", avgsalesprice=((avgsalesprice * totalsalesqty) + " & ToDouble(dtDtl.Rows(C1)("sogendtlamt").ToString) & ") / (totalsalesqty + " & ToDouble(dtDtl.Rows(C1)("sogenqty").ToString) & "), lastsalesprice=" & ToDouble(dtDtl.Rows(C1)("sogenprice").ToString) & ", lasttrans='QL_trnsogenmst', lasttransoid=" & Session("trnoid") & ", lasttransno='" & sNo & "', lasttransdate='" & Session("trndate") & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, ratetoidr=" & cRate.GetRateMonthlyIDRValue & " WHERE cmpcode='" & AppCmpCode.Text & "' AND matgenoid=" & ToDouble(dtDtl.Rows(C1)("matgenoid").ToString) & " AND refoid=" & Session("custoid") & " AND refname='CUSTOMER' AND curroid=" & Session("curroid")
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() <= 0 Then
                                sSql = "INSERT INTO QL_mstmatgenprice (cmpcode, matgenpriceoid, matgenoid, refoid, refname, totalpurchaseqty, totalsalesqty, avgpurchaseprice, lastpurchaseprice, avgsalesprice, lastsalesprice, lasttrans, lasttransoid, lasttransno, lasttransdate, note, res1, res2, res3, upduser, updtime, curroid, ratetoidr) VALUES ('" & AppCmpCode.Text & "', " & iPriceOid & ", " & ToDouble(dtDtl.Rows(C1)("matgenoid").ToString) & ", " & Session("custoid") & ", 'CUSTOMER', 0, " & ToDouble(dtDtl.Rows(C1)("sogenqty").ToString) & ", 0, 0, " & ToDouble(dtDtl.Rows(C1)("sogenprice").ToString) & ", " & ToDouble(dtDtl.Rows(C1)("sogenprice").ToString) & ", 'QL_trnsogenmst', " & Session("trnoid") & ", '" & sNo & "', '" & Session("trndate") & "', '', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Session("curroid") & ", " & cRate.GetRateMonthlyIDRValue & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iPriceOid += 1
                            End If
                        Next
                        sSql = "UPDATE QL_mstoid SET lastoid=" & iPriceOid - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_MSTMATRAWPRICE'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "SO Spare Part Approval"
    Private Sub SetGVListSOSP()
        Dim arHeader() As String = {"Draft No.", "SO Date", "Customer", "SO Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "custname", "sospmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "custname", "sospmstnote", "sosptotalamt", "sospmstdiscamt", "sospvat", "sospgrandtotalamt", "curroid", "custoid", "currcode", "soratetoidr", "upduser", "updtime"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT so.cmpcode, so.sospmstoid AS trnoid, so.sospno, CONVERT(VARCHAR(10), so.sospdate, 101) AS trndate, c.custname, so.sospmstnote, (so.sosptotalamt - so.sosptotaldiscdtl) AS sosptotalamt, so.sospmstdiscamt, so.sospvat, so.sospgrandtotalamt, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, so.curroid, so.custoid, currcode, rate2res1 AS soratetoidr, so.upduser, so.updtime FROM QL_trnsospmst so INNER JOIN QL_mstcust c ON so.custoid=c.custoid INNER JOIN QL_approval ap ON so.sospmstoid=ap.oid AND ap.statusrequest='New' INNER JOIN QL_mstcurr cu ON cu.curroid=so.curroid INNER JOIN QL_mstrate2 r ON r.rate2oid=so.rate2oid WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_trnsospmst' AND so.sospmststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnsospmst")
    End Sub

    Private Sub SetGVDataSOSP(ByVal dtDataHeader As DataTable)
        Session("curroid") = gvList.SelectedDataKey.Item("curroid").ToString
        Session("custoid") = gvList.SelectedDataKey.Item("custoid").ToString
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "SO Date", "Customer", "SO Note", "Currency", "Rate To IDR", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "custname", "sospmstnote", "currcode", "soratetoidr", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "D", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "O", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            ElseIf arHidden(C1) = "O" Then
                If gvDataHeader.Rows(C1 - 1).Cells(2).Text = "IDR" Then
                    gvDataHeader.Rows(C1).Visible = False
                End If
            End If
        Next
        Dim arColNameList() As String = {"datacaption", "datasept", "datavalue", "dataempty"}
        Dim arColTypeList() As String = {"System.String", "System.String", "System.String", "System.String"}
        Dim dtDataFooter As DataTable = SetTableDetail("tblDataFooter", arColNameList, arColTypeList)
        Dim arCaptionF() As String = {"Total", "Header Disc.", "Tax", "Grand Total"}
        Dim arValueF() As String = {"sosptotalamt", "sospmstdiscamt", "sospvat", "sospgrandtotalamt"}
        Dim arTypeF() As String = {"D", "D", "D", "D"}
        Dim arHiddenF() As String = {"N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaptionF.Length - 1
            dr = dtDataFooter.NewRow
            dr("datacaption") = arCaptionF(C1)
            dr("datasept") = ":"
            If arTypeF(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValueF(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValueF(C1)).ToString), 2)
            End If
            dr("dataempty") = ""
            dtDataFooter.Rows.Add(dr)
        Next
        gvDataFooter.DataSource = dtDataFooter
        gvDataFooter.DataBind()
        For C1 As Int16 = 0 To arHiddenF.Length - 1
            If arHiddenF(C1) = "Y" Then
                gvDataFooter.Rows(C1).Visible = False
            End If
        Next
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Font.Bold = True
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).ForeColor = Drawing.Color.Black
        Dim arHeader() As String = {"No.", "Mat. Desc", "Quantity", "Unit", "Last Price", "Price", "Amount", "Disc.", "Netto", "Note"}
        Dim arField() As String = {"sospdtlseq", "sparepartshortdesc", "sospqty", "unit", "lastsoprice", "sospprice", "sospdtlamt", "sospdtldiscamt", "sospdtlnetto", "sospdtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT sod.sospdtlseq, sod.sparepartoid, m.sparepartlongdesc AS sparepartshortdesc, sod.sospqty, g.gendesc AS unit, sod.sospprice, sod.sospdtlamt, sod.sospdtldiscamt, sod.sospdtlnetto, sod.sospdtlnote, ISNULL((SELECT TOP 1 mp.lastsalesprice FROM QL_mstsparepartprice mp WHERE mp.cmpcode=so.cmpcode AND mp.sparepartoid=sod.sparepartoid AND mp.refname='CUSTOMER' AND mp.curroid=so.curroid ORDER BY mp.updtime DESC), 0.0) AS lastsoprice FROM QL_trnsospmst so INNER JOIN QL_trnsospdtl sod ON so.sospmstoid=sod.sospmstoid INNER JOIN QL_mstsparepart m ON sod.sparepartoid=m.sparepartoid INNER JOIN QL_mstgen g ON sod.sospunitoid=g.genoid WHERE so.sospmstoid=" & Session("trnoid") & " ORDER BY sod.sospdtlseq"
        Session("TblDtlSO") = ckon.ambiltabel(sSql, "QL_trnsospdtl")
        gvData.DataSource = Session("TblDtlSO")
        gvData.DataBind()
    End Sub

    Private Function GenerateSOSPNo() As String
        Dim sNo As String = "SOSP-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(sospno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnsospmst WHERE cmpcode='" & AppCmpCode.Text & "' AND sospno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdateSOSP(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select SO Spare Part Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select SO Spare Part Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim cRate As New ClassRate()
        If sStatus = "Approved" Then
            cRate.SetRateValue(CInt(Session("curroid")), Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim iPriceOid As Integer = GenerateID("QL_MSTMATRAWPRICE", CompnyCode)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update SO
            sSql = "UPDATE QL_trnsospmst SET sospno='" & sNo & "', sospmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP"
            If sStatus = "Approved" Then
                sSql &= ", rateoid=" & cRate.GetRateDailyOid & ", rate2oid=" & cRate.GetRateMonthlyOid
            End If
            sSql &= " WHERE sospmststatus='In Approval' AND sospmstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnsospmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Approved" Then
                    If Not Session("TblDtlSO") Is Nothing Then
                        Dim dtDtl As DataTable = Session("TblDtlSO")
                        For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                            sSql = "UPDATE QL_mstsparepartprice SET totalsalesqty=totalsalesqty + " & ToDouble(dtDtl.Rows(C1)("sospqty").ToString) & ", avgsalesprice=((avgsalesprice * totalsalesqty) + " & ToDouble(dtDtl.Rows(C1)("sospdtlamt").ToString) & ") / (totalsalesqty + " & ToDouble(dtDtl.Rows(C1)("sospqty").ToString) & "), lastsalesprice=" & ToDouble(dtDtl.Rows(C1)("sospprice").ToString) & ", lasttrans='QL_trnsospmst', lasttransoid=" & Session("trnoid") & ", lasttransno='" & sNo & "', lasttransdate='" & Session("trndate") & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, ratetoidr=" & cRate.GetRateMonthlyIDRValue & " WHERE cmpcode='" & AppCmpCode.Text & "' AND sparepartoid=" & ToDouble(dtDtl.Rows(C1)("sparepartoid").ToString) & " AND refoid=" & Session("custoid") & " AND refname='CUSTOMER' AND curroid=" & Session("curroid")
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() <= 0 Then
                                sSql = "INSERT INTO QL_mstsparepartprice (cmpcode, sparepartpriceoid, sparepartoid, refoid, refname, totalpurchaseqty, totalsalesqty, avgpurchaseprice, lastpurchaseprice, avgsalesprice, lastsalesprice, lasttrans, lasttransoid, lasttransno, lasttransdate, note, res1, res2, res3, upduser, updtime, curroid, ratetoidr) VALUES ('" & AppCmpCode.Text & "', " & iPriceOid & ", " & ToDouble(dtDtl.Rows(C1)("sparepartoid").ToString) & ", " & Session("custoid") & ", 'CUSTOMER', 0, " & ToDouble(dtDtl.Rows(C1)("sospqty").ToString) & ", 0, 0, " & ToDouble(dtDtl.Rows(C1)("sospprice").ToString) & ", " & ToDouble(dtDtl.Rows(C1)("sospprice").ToString) & ", 'QL_trnsospmst', " & Session("trnoid") & ", '" & sNo & "', '" & Session("trndate") & "', '', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Session("curroid") & ", " & cRate.GetRateMonthlyIDRValue & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iPriceOid += 1
                            End If
                        Next
                        sSql = "UPDATE QL_mstoid SET lastoid=" & iPriceOid - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_MSTMATRAWPRICE'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "Sales Order Approval"
    Private Sub SetGVListSOFG()
        Dim arHeader() As String = {"Draft No.", "SO Date", "Customer", "SO Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "custname", "somstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "custname", "somstnote", "sototalamt", "somstdiscamt", "somstdiscamt2", "sotaxamt", "sotaxtype", "sograndtotalamt", "curroid", "custoid", "currcode", "soratetoidr", "upduser", "updtime", "paymenttype", "salesman", "credit_limit"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT so.cmpcode, so.somstoid AS trnoid, so.sono, CONVERT(VARCHAR(10), so.sodate, 101) AS trndate, c.custname, so.somstnote, so.somstdpp AS sototalamt, so.somstdiscamt, so.somstdiscamt2, so.sotaxamt, case WHEN so.sotaxtype='INC' then 'T' else 'N' end AS sotaxtype, so.sototalnetto AS sograndtotalamt, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, so.curroid, so.custoid, 'IDR' currcode, 1 AS soratetoidr, so.upduser, so.updtime, gx.gendesc AS paymenttype, ps.personname salesman, (cg.custcreditlimitrupiah-cg.custcreditlimitusagerupiah) credit_limit FROM QL_trnsomst so INNER JOIN QL_mstcust c ON so.custoid=c.custoid INNER JOIN QL_approval ap ON so.somstoid=ap.oid AND ap.statusrequest='New' INNER JOIN QL_mstgen gx ON gx.genoid=sopaytypeoid INNER JOIN QL_mstperson ps on so.sosalescode=ps.salescode INNER JOIN QL_mstcustgroup cg ON c.custgroupoid=cg.custgroupoid WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_trnsomst' AND so.somststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnsomst")
    End Sub

    Private Sub SetGVDataSOFG(ByVal dtDataHeader As DataTable)
        Session("curroid") = gvList.SelectedDataKey.Item("curroid").ToString
        Session("custoid") = gvList.SelectedDataKey.Item("custoid").ToString
        Session("grandtotal") = gvList.SelectedDataKey.Item("sograndtotalamt").ToString
        Session("sotaxtype") = gvList.SelectedDataKey.Item("sotaxtype").ToString
        Session("paymenttype") = gvList.SelectedDataKey.Item("paymenttype").ToString
        Session("credit_limit") = ToDouble(gvList.SelectedDataKey.Item("credit_limit").ToString)
        lblreason.Visible = True : lblSeptReason.Visible = True : ActionReason.Visible = True
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "SO Date", "Customer", "SO Note", "Sales", "Currency", "Rate To IDR", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "custname", "somstnote", "salesman", "currcode", "soratetoidr", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S", "D", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N", "O", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            ElseIf arHidden(C1) = "O" Then
                If gvDataHeader.Rows(C1 - 1).Cells(2).Text = "IDR" Then
                    gvDataHeader.Rows(C1).Visible = False
                End If
            End If
        Next
        Dim arColNameList() As String = {"datacaption", "datasept", "datavalue", "dataempty"}
        Dim arColTypeList() As String = {"System.String", "System.String", "System.String", "System.String"}
        Dim dtDataFooter As DataTable = SetTableDetail("tblDataFooter", arColNameList, arColTypeList)
        Dim arCaptionF() As String = {"Total", "Header Disc. 1", "Header Disc. 2", "Tax", "Grand Total"}
        Dim arValueF() As String = {"sototalamt", "somstdiscamt", "somstdiscamt2", "sotaxamt", "sograndtotalamt"}
        Dim arTypeF() As String = {"D", "D", "D", "D", "D"}
        Dim arHiddenF() As String = {"N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaptionF.Length - 1
            dr = dtDataFooter.NewRow
            dr("datacaption") = arCaptionF(C1)
            dr("datasept") = ":"
            If arTypeF(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValueF(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValueF(C1)).ToString), 2)
            End If
            dr("dataempty") = ""
            dtDataFooter.Rows.Add(dr)
        Next
        gvDataFooter.DataSource = dtDataFooter
        gvDataFooter.DataBind()
        For C1 As Int16 = 0 To arHiddenF.Length - 1
            If arHiddenF(C1) = "Y" Then
                gvDataFooter.Rows(C1).Visible = False
            End If
        Next
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Font.Bold = True
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).ForeColor = Drawing.Color.Black
        Dim arHeader() As String = {"No.", "ID", "Kode", "Old Kode", "Mat. Desc", "Quantity", "Unit", "Price", "Amount", "Disc. 1", "Disc. 2", "Disc. 3", "Netto", "Note", "Price Netto", "Minim Price"}
        Dim arField() As String = {"sodtlseq", "itemoid", "code", "oldcode", "itemlongdesc", "sodtlqty", "unit", "sodtlprice", "sodtlamt", "sodtldiscamt1", "sodtldiscamt2", "sodtldiscamt3", "sodtlnetto", "sodtlnote", "satuan", "itemminprice"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next

        sSql = "SELECT sod.sodtlseq, sod.itemoid, m.itemCode AS code, m.itemoldcode AS oldcode, m.itemLongDescription AS itemlongdesc, sod.sodtlqty, g.gendesc AS unit, sod.sodtlprice, (sod.sodtlqty*sod.sodtlprice) AS sodtlamt, sod.sodtldiscamt1,  sod.sodtldiscamt2, sod.sodtldiscamt3, sod.sodtlnetto, sod.sodtlnote, CONVERT (DECIMAL (18,2), ISNULL((sod.sodtlnetto/NULLIF(sod.sodtlqty,0)),0)) AS satuan, itemminprice FROM QL_trnsomst so INNER JOIN QL_trnsodtl sod ON so.somstoid=sod.somstoid INNER JOIN QL_mstitem m ON sod.itemoid=m.itemoid INNER JOIN QL_mstgen g ON sod.sodtlunitoid=g.genoid WHERE so.somstoid=" & Session("trnoid") & " ORDER BY sod.sodtlseq"
        Session("TblDtlSO") = ckon.ambiltabel(sSql, "QL_trnsodtl")
        gvData.DataSource = Session("TblDtlSO")
        gvData.DataBind()
    End Sub

    Private Function GenerateSOFGNo() As String
        Dim SOtype As String = GetStrData("SELECT somstres1 FROM QL_trnsomst WHERE somstoid=" & Session("trnoid") & "")
        If SOtype = "RAW" Then
            SOtype = "RM"
        End If
        Dim sNo As String = "SO" & Session("sotaxtype") & "-" & Format(CDate(Session("trndate")), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(sono, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnsomst WHERE cmpcode='" & AppCmpCode.Text & "' AND sono LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdateSOFG(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select Sales Order Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select Sales Order Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim cRate As New ClassRate()
        If sStatus = "Approved" Then
            cRate.SetRateValue(CInt(Session("curroid")), Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If ToDouble(Session("credit_limit")) < ToDouble(Session("grandtotal")) Then
                showMessage("Total Netto > Limit Credit Customer! Netto : <strong>" & ToMaskEdit(ToDouble(Session("grandtotal")), 2) & "</strong>, Sisa Limit: <strong>" & ToMaskEdit(ToDouble(Session("credit_limit")), 2) & "</strong><BR>", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        If sStatus = "Rejected" Or sStatus = "Revised" Then
            If ActionReason.Text = "" Then
                showMessage("Please Fill " & sStatus & " Reason First!", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim iPriceOid As Integer = GenerateID("QL_MSTITEMPRICE", CompnyCode)

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update SO
            If sStatus = "Approved" Then
                sSql = "UPDATE QL_trnsomst SET sono='" & sNo & "', somststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP"
            Else
                sSql = "UPDATE QL_trnsomst SET sono='" & sNo & "', somststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP" & IIf(sStatus = "Rejected", ", rejectreason='" & Tchar(ActionReason.Text) & "', rejectuser='" & Session("UserID") & "', rejecttime=CURRENT_TIMESTAMP", ", revisereason='" & Tchar(ActionReason.Text) & "', reviseuser='" & Session("UserID") & "', revisetime=CURRENT_TIMESTAMP")
            End If
            sSql &= " WHERE somststatus='In Approval' AND somstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnsomst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Approved" Then
                    If Not Session("TblDtlSO") Is Nothing Then
                        Dim dtDtl As DataTable = Session("TblDtlSO")
                        For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                            sSql = "UPDATE QL_mstitemprice SET totalsalesqty=totalsalesqty + " & ToDouble(dtDtl.Rows(C1)("sodtlqty").ToString) & ", avgsalesprice=((avgsalesprice * totalsalesqty) + " & ToDouble(dtDtl.Rows(C1)("sodtlamt").ToString) & ") / (totalsalesqty + " & ToDouble(dtDtl.Rows(C1)("sodtlqty").ToString) & "), lastsalesprice=" & ToDouble(dtDtl.Rows(C1)("sodtlprice").ToString) & ", lasttrans='QL_trnsomst', lasttransoid=" & Session("trnoid") & ", lasttransno='" & sNo & "', lasttransdate='" & Session("trndate") & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, ratetoidr=" & cRate.GetRateMonthlyIDRValue & " WHERE cmpcode='" & AppCmpCode.Text & "' AND itemoid=" & ToDouble(dtDtl.Rows(C1)("itemoid").ToString) & " AND refoid=" & Session("custoid") & " AND refname='CUSTOMER' AND curroid=" & Session("curroid")
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() <= 0 Then
                                sSql = "INSERT INTO QL_mstitemprice (cmpcode, itempriceoid, itemoid, refoid, refname, totalpurchaseqty, totalsalesqty, avgpurchaseprice, lastpurchaseprice, avgsalesprice, lastsalesprice, lasttrans, lasttransoid, lasttransno, lasttransdate, note, res1, res2, res3, upduser, updtime, curroid, ratetoidr) VALUES ('" & AppCmpCode.Text & "', " & iPriceOid & ", " & ToDouble(dtDtl.Rows(C1)("itemoid").ToString) & ", " & Session("custoid") & ", 'CUSTOMER', 0, " & ToDouble(dtDtl.Rows(C1)("sodtlqty").ToString) & ", 0, 0, " & ToDouble(dtDtl.Rows(C1)("sodtlprice").ToString) & ", " & ToDouble(dtDtl.Rows(C1)("sodtlprice").ToString) & ", 'QL_trnsomst', " & Session("trnoid") & ", '" & sNo & "', '" & Session("trndate") & "', '', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Session("curroid") & ", " & cRate.GetRateMonthlyIDRValue & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iPriceOid += 1
                            End If
                        Next
                        sSql = "UPDATE QL_mstoid SET lastoid=" & iPriceOid - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_MSTITEMPRICE'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()

                        'If sStatus <> "Revised" Then
                        '    If Session("paymenttype").ToString.ToUpper <> "CASH" Then
                        '        ' Update Credit limit Customer
                        '        sSql = "SELECT custgroupoid from QL_mstcust WHERE cmpcode='" & CompnyCode & "' AND custoid=" & Session("custoid") & ""
                        '        Dim group As Integer = ckon.ambilscalar(sSql)
                        '        sSql = "SELECT currcode from QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND curroid=" & Session("curroid") & ""
                        '        Dim curr As String = GetStrData(sSql)
                        '        If curr = "IDR" Then
                        '            sSql = "UPDATE QL_mstcustgroup SET custcreditlimitusagerupiah = custcreditlimitusagerupiah + (" & Session("grandtotal") * cRate.GetRateDailyIDRValue & ") WHERE cmpcode='" & CompnyCode & "' AND custgroupoid=" & group & ""
                        '            xCmd.CommandText = sSql
                        '            xCmd.ExecuteNonQuery()
                        '        Else
                        '            sSql = "UPDATE QL_mstcustgroup SET custcreditlimitusageusd = custcreditlimitusageusd + (" & Session("grandtotal") * cRate.GetRateDailyUSDValue & ") WHERE cmpcode='" & CompnyCode & "' AND custgroupoid=" & group & ""
                        '            xCmd.CommandText = sSql
                        '            xCmd.ExecuteNonQuery()
                        '        End If
                        '    End If
                        'End If

                    End If
                    End If
                End If
                objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "SO Assets Approval"
    Private Sub SetGVListSOAssets()
        Dim arHeader() As String = {"Draft No.", "SO Date", "Customer", "SO Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "custname", "soassetmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "custname", "soassetmstnote", "soassettotalamt", "soassetmstdiscamt", "soassetvat", "soassetothercost", "soassetgrandtotalamt"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT so.cmpcode, so.soassetmstoid AS trnoid, so.soassetno, CONVERT(VARCHAR(10), so.soassetdate, 101) AS trndate, c.custname, so.soassetmstnote, (so.soassettotalamt - so.soassettotaldiscdtl) AS soassettotalamt, so.soassetmstdiscamt, so.soassetvat, (so.soassetdeliverycost + so.soassetothercost) AS soassetothercost, so.soassetgrandtotalamt, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate FROM QL_trnsoassetmst so INNER JOIN QL_mstcust c ON so.custoid=c.custoid INNER JOIN QL_approval ap ON so.soassetmstoid=ap.oid AND ap.statusrequest='New' WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_trnsoassetmst' AND so.soassetmststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnsoassetmst")
    End Sub

    Private Sub SetGVDataSOAssets(ByVal dtDataHeader As DataTable)
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "SO Date", "Customer", "SO Note"}
        Dim arValue() As String = {"trnoid", "trndate", "custname", "soassetmstnote"}
        Dim arType() As String = {"S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arColNameList() As String = {"datacaption", "datasept", "datavalue", "dataempty"}
        Dim arColTypeList() As String = {"System.String", "System.String", "System.String", "System.String"}
        Dim dtDataFooter As DataTable = SetTableDetail("tblDataFooter", arColNameList, arColTypeList)
        Dim arCaptionF() As String = {"Total", "Header Disc.", "Tax", "Other Cost", "Grand Total"}
        Dim arValueF() As String = {"soassettotalamt", "soassetmstdiscamt", "soassetvat", "soassetothercost", "soassetgrandtotalamt"}
        Dim arTypeF() As String = {"D", "D", "D", "D", "D"}
        Dim arHiddenF() As String = {"N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaptionF.Length - 1
            dr = dtDataFooter.NewRow
            dr("datacaption") = arCaptionF(C1)
            dr("datasept") = ":"
            If arTypeF(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValueF(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValueF(C1)).ToString), 2)
            End If
            dr("dataempty") = ""
            dtDataFooter.Rows.Add(dr)
        Next
        gvDataFooter.DataSource = dtDataFooter
        gvDataFooter.DataBind()
        For C1 As Int16 = 0 To arHiddenF.Length - 1
            If arHiddenF(C1) = "Y" Then
                gvDataFooter.Rows(C1).Visible = False
            End If
        Next
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Font.Bold = True
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).ForeColor = Drawing.Color.Black
        Dim arHeader() As String = {"No.", "Assets", "Assets No.", "Quantity", "Unit", "Price", "Amount", "Disc.", "Netto", "Note"}
        Dim arField() As String = {"soassetdtlseq", "reflongdesc", "assetno", "soassetqty", "unit", "soassetprice", "soassetdtlamt", "soassetdtldiscamt", "soassetdtlnetto", "soassetdtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT sod.soassetdtlseq, (CASE asst.reftype WHEN 'General' THEN (SELECT x.matgenlongdesc FROM QL_mstmatgen x WHERE x.matgenoid=asst.refoid) WHEN 'Spare Part' THEN (SELECT x.sparepartlongdesc FROM QL_mstsparepart x WHERE x.sparepartoid=asst.refoid) ELSE '' END) AS reflongdesc, asst.assetno, sod.soassetqty, g.gendesc AS unit, sod.soassetprice, sod.soassetdtlamt, sod.soassetdtldiscamt, sod.soassetdtlnetto, sod.soassetdtlnote FROM QL_trnsoassetmst so INNER JOIN QL_trnsoassetdtl sod ON so.soassetmstoid=sod.soassetmstoid INNER JOIN QL_assetmst asst ON sod.assetmstoid=asst.assetmstoid INNER JOIN QL_mstgen g ON sod.soassetunitoid=g.genoid WHERE so.soassetmstoid=" & Session("trnoid") & " ORDER BY sod.soassetdtlseq"
        FillGV(gvData, sSql, "QL_trnsoassetdtl")
    End Sub

    Private Function GenerateSOAssetsNo() As String
        Dim sNo As String = "SOFA-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(soassetno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnsoassetmst WHERE cmpcode='" & AppCmpCode.Text & "' AND soassetno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdateSOAssets(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select SO Fixed Assets Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select SO Fixed Assets Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update SO
            sSql = "UPDATE QL_trnsoassetmst SET soassetno='" & sNo & "', soassetmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE soassetmststatus='In Approval' AND soassetmstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnsoassetmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "Delivery Order Approval"
    Private Sub SetGVListShipmentRM()
        Dim arHeader() As String = {"Draft No.", "Shipment Date", "Customer", "Shipment Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "custname", "domstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "custname", "domstnote", "upduser", "updtime", "curroid", "dhead", "discHead", "dhead2", "discHead2", "tTax", "disctype", "discval", "disctype2", "discval2", "komisi"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT sm.cmpcode, sm.domstoid AS trnoid, CONVERT(VARCHAR(10), sm.dodate, 101) AS trndate, c.custname, sm.domstnote, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, sm.upduser, sm.updtime, sm.curroid, isnull((som.somstdiscamt),0) AS discHead, isnull((som.somstdiscamt2),0) AS discHead2, isnull((som.somstdiscamt),0)/isnull((select SUM(sodtlqty) from QL_trnsodtl sd where sd.somstoid=som.somstoid),0) AS dhead, isnull((som.somstdiscamt2),0)/isnull((select SUM(sodtlqty) from QL_trnsodtl sd where sd.somstoid=som.somstoid),0) AS dhead2, som.sotaxtype AS tTax, som.somstdisctype AS disctype, som.somstdiscvalue AS discval, som.somstdisctype2 AS disctype2, som.somstdiscvalue2 AS discval2, (select p.komisi from QL_mstperson p inner join QL_trnsomst som ON som.sosalescode=p.salescode where som.somstoid=sm.somstoid) AS komisi FROM QL_trndomst sm INNER JOIN QL_approval ap ON sm.domstoid=ap.oid AND ap.statusrequest='New' INNER JOIN QL_mstcust c ON c.custoid=sm.custoid INNER JOIN QL_trnsomst som ON som.somstoid=sm.somstoid WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_trndomst' AND sm.domststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trndomst")
    End Sub

    Private Sub SetGVDataShipmentRM(ByVal dtDataHeader As DataTable)
        Session("dodate") = gvList.SelectedDataKey.Item("trndate").ToString
        Session("curroid") = gvList.SelectedDataKey.Item("curroid").ToString
        Session("dhead") = gvList.SelectedDataKey.Item("dhead").ToString
        Session("dhead2") = gvList.SelectedDataKey.Item("dhead2").ToString
        Session("discHead") = gvList.SelectedDataKey.Item("discHead").ToString
        Session("discHead2") = gvList.SelectedDataKey.Item("discHead2").ToString
        Session("tTax") = gvList.SelectedDataKey.Item("tTax").ToString
        Session("disctype") = gvList.SelectedDataKey.Item("disctype").ToString
        Session("discval") = gvList.SelectedDataKey.Item("discval").ToString
        Session("disctype2") = gvList.SelectedDataKey.Item("disctype2").ToString
        Session("discval2") = gvList.SelectedDataKey.Item("discval2").ToString
        Session("komisi") = ToDouble(gvList.SelectedDataKey.Item("komisi"))
        lblreason.Visible = True : lblSeptReason.Visible = True : ActionReason.Visible = True
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "DO Date", "Customer", "DO Note", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "custname", "domstnote", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arHeader() As String = {"No.", "Code", "Description", "Qty SO", "Qty DO", "Unit", "Warehouse", "Note"}
        Dim arField() As String = {"dodtlseq", "itemCode", "itemLongDescription", "sodtlqty", "doqty", "dounit", "dowh", "dodtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sSql = "SELECT sd.dodtlseq, sd.dodtloid, m.itemoid, m.itemCode, m.itemLongDescription, sod.sodtloid, sod.sodtlqty, sd.doqty, (select SUM(dd.doqty) from QL_trndodtl dd WHERE dd.sodtloid=sod.sodtloid) AS sisa,sd.dounitoid, g.gendesc AS dounit, sm.dowhoid,g2.gendesc AS dowh, ggg.gendesc AS sotype, som.somstres1, sd.dodtlnote, CAST(ROUND(ROUND(sodtlprice * CASE WHEN sodate<CAST('2022/04/01 00:00:00' AS DATETIME) THEN 1.1 ELSE 1.11 END, 0) / CASE WHEN sodate<CAST('2022/04/01 00:00:00' AS DATETIME) THEN 1.1 ELSE 1.11 END, 4) AS DECIMAL (18,4)) sodtlprice, sod.sodtldisctype1, sod.sodtldiscvalue1, ((sod.sodtldiscamt1/ sod.sodtlqty) * sd.doqty) AS sodtldiscamt1, sod.sodtldisctype2, sod.sodtldiscvalue2, ((sod.sodtldiscamt2/ sod.sodtlqty) * sd.doqty) AS sodtldiscamt2, sod.sodtldisctype3, sod.sodtldiscvalue3, ((sod.sodtldiscamt3/ sod.sodtlqty) * sd.doqty) AS sodtldiscamt3, ((sod.sodtlnetto / sod.sodtlqty) * sd.doqty) AS sodtlnetto, som.somstdisctype, som.somstdiscvalue, som.somstdiscamt, som.somstdisctype2, som.somstdiscvalue2, som.somstdiscamt2, CAST (ROUND(((sodtlnetto / CASE WHEN sodate<CAST('2022/04/01 00:00:00' AS DATETIME) THEN 1.1 ELSE 1.11 END) / sod.sodtlqty) * sd.doqty, 2) AS DECIMAL (18,2)) AS dppdtlamt, som.sototalnetto, som.sotaxtype, CAST(ROUND(CAST (ROUND(((sodtlnetto / CASE WHEN sodate<CAST('2022/04/01 00:00:00' AS DATETIME) THEN 1.1 ELSE 1.11 END) / sod.sodtlqty) * sd.doqty, 2) AS DECIMAL (18,2)) * CASE WHEN sodate<CAST('2022/04/01 00:00:00' AS DATETIME) THEN 10 ELSE 11 END / 100, 2) AS DECIMAL (18,2)) AS taxdtlamt, som.sograndtotalamt, som.sono, ISNULL(dodtlres3, '') AS sostatus, sod.sodtloid, som.somstoid, som.sopaytypeoid, som.sosalescode, cs.custoid, cs.custname, sd.doqty_unitkecil AS qtyunitkecil, sd.doqty_unitbesar AS qtyunitbesar, (select dbo.getStockValue(sd.matoid)) AS valueidr, 0.0 AS valueusd, (SELECT ISNULL(genother1,'') acctgoid FROM QL_mstgen gx WHERE gx.gengroup='GROUPITEM' AND gx.genoid=m.itemgroupoid AND activeflag='ACTIVE') AS stockacctgoid, sod.soprice_kons, sd.old_code FROM QL_trndomst sm INNER JOIN QL_trndodtl sd ON sd.cmpcode=sm.cmpcode AND sm.domstoid=sd.domstoid INNER JOIN QL_mstitem m ON sd.matoid=m.itemoid INNER JOIN QL_mstgen g ON sd.dounitoid=g.genoid INNER JOIN QL_mstgen g2 ON sm.dowhoid=g2.genoid INNER JOIN QL_trnsomst som ON som.cmpcode=sd.cmpcode AND som.somstoid=sd.somstoid INNER JOIN QL_trnsodtl sod ON sod.cmpcode=sd.cmpcode AND sod.sodtloid=sd.sodtloid INNER JOIN QL_mstcust cs ON cs.custoid=som.custoid INNER JOIN QL_mstgen ggg ON m.itemgroupoid=ggg.genoid WHERE sd.domstoid=" & Session("trnoid") & " ORDER BY sd.dodtlseq"
        Session("TblDtlShipmentRM") = ckon.ambiltabel(sSql, "QL_trndodtl")
        gvData.DataSource = Session("TblDtlShipmentRM")
        gvData.DataBind()
    End Sub

    Private Function GenerateShipmentRMNo() As String
        Dim DOtype As String = GetStrData("SELECT (case when som.sotaxtype='INC' then 'T' else 'N' end) AS tipe FROM QL_trndomst dom inner join QL_trnsomst som ON som.somstoid=dom.somstoid WHERE domstoid=" & Session("trnoid") & "")
        Session("sType") = DOtype
        Dim sNo As String = "SJ" & DOtype & "-" & Format(CDate(Session("trndate")), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(dono, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trndomst WHERE cmpcode='" & AppCmpCode.Text & "' AND dono LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Function GenerateJualMstNo() As String
        Dim sTypeTax As String = "N"
        If Session("tTax").ToString.ToUpper = "INC" Then
            sTypeTax = "T"
        End If
        Dim sNo2 As String = "LA" & sTypeTax & "-" & Format(CDate(Session("trndate")), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trnjualno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnjualmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnjualno LIKE '" & sNo2 & "%'"
        sNo2 = GenNumberString(sNo2, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo2
    End Function

    Private Sub UpdateShipmentRM(ByVal sNo As String, ByVal sNo2 As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select DO Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select DO first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        If sStatus = "Rejected" Or sStatus = "Revised" Then
            If ActionReason.Text = "" Then
                showMessage("Please Fill " & sStatus & " Reason First!", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim iConOid As Integer = GenerateID("QL_CONSTOCK", CompnyCode)
        Dim iCrdOid As Integer = GenerateID("QL_CRDSTOCK", CompnyCode)
        Dim iStockValOid As Integer = GenerateID("QL_STOCKVALUE", CompnyCode)
        Dim iJualMst As Integer = GenerateID("QL_TRNJUALMST", CompnyCode)
        Dim iJualDtl As Integer = GenerateID("QL_TRNJUALDTL", CompnyCode)
        Dim iGlMstOid As Int32 = GenerateID("QL_TRNGLMST", CompnyCode)
        Dim iGlDtlOid As Int32 = GenerateID("QL_TRNGLDTL", CompnyCode)
        Dim iDelAcctgOid, iStockAcctgOid, iPiutangAcctgOid, iDiscAcctgOid, iJualAcctgOid, iPpnAcctgOid, iHppAcctgOid, iPersediaanAcctgOid As Integer
        Dim iSeq As Int16 = 1 : Dim iSeq2 As Int16 = 1
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
        Dim cRate As New ClassRate()
        Dim sodtloid As Integer
        Dim doAmt, doAmtIDR, doAmtUSD, doAmtTax, doAmtTaxIDR, doAmtTaxUSD, taxAmt, taxAmtIDR, taxAmtUSD As Double
        Dim somstALL As String = ""

        Dim tmppopartoid As Integer = 0
        Dim tmppopart2oid As Integer = 0
        'If sStatus = "Approved" Then
        '    If conn.State = ConnectionState.Closed Then
        '        conn.Open()
        '    End If
        '    conn.Close()
        'End If

        If sStatus = "Approved" Then
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If isPeriodClosed(AppCmpCode.Text, sPeriod) Then
                showMessage("Cannot continue this transaction anymore because the period : " & MonthName(CInt(sPeriod.Substring(4, 2))) & " " & sPeriod.Substring(0, 4) & " have been closed.", CompnyName & " - WARNING", 2)
                Exit Sub
            End If
            If isPeriodAcctgClosed(AppCmpCode.Text, sPeriod) Then
                showMessage("Cannot continue this transaction anymore because the period : " & MonthName(CInt(sPeriod.Substring(4, 2))) & " " & sPeriod.Substring(0, 4) & " have been closed.", CompnyName & " - WARNING", 2)
                Exit Sub
            End If
            Dim sVarErr As String = ""
            If Not IsInterfaceExists("VAR_STOCK_TRANSIT", AppCmpCode.Text) Then
                sVarErr = "VAR_STOCK_TRANSIT"
            End If
            If Not IsInterfaceExists("VAR_STOCK", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_STOCK"
            End If
            If Not IsInterfaceExists("VAR_AR", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_AR"
            End If
            If Not IsInterfaceExists("VAR_DISC_JUAL", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_DISC_JUAL"
            End If
            If Not IsInterfaceExists("VAR_PENJUALAN", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_PENJUALAN"
            End If
            If Not IsInterfaceExists("VAR_PPN_OUT", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_PPN_OUT"
            End If
            If Not IsInterfaceExists("VAR_HPP", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_HPP"
            End If
            If Not IsInterfaceExists("VAR_PERSEDIAAN", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_PERSEDIAAN"
            End If
            Dim objr As DataTable = Session("TblDtlShipmentRM")
            For C1 As Int16 = 0 To objr.Rows.Count - 1
                If Not IsStockAvailable(CompnyCode, GetDateToPeriodAcctg(GetServerTime()), objr.Rows(C1)("itemoid"), objr.Rows(C1)("dowhoid"), ToDouble(objr.Compute("SUM(qtyunitkecil)", "itemoid='" & objr.Rows(C1)("itemoid") & "'"))) Then
                    sVarErr &= "- Material Code " & objr.Rows(C1)("itemCode") & " Must be less or equal than Stock Qty!<BR>"
                End If
            Next
            If sVarErr <> "" Then
                showMessage(GetInterfaceWarning(sVarErr, "approved"), CompnyName & " - WARNING", 2)
                Exit Sub
            End If
            iDelAcctgOid = GetAcctgOID(GetVarInterface("VAR_STOCK_TRANSIT", AppCmpCode.Text), CompnyCode)
            iPiutangAcctgOid = GetAcctgOID(GetVarInterface("VAR_AR", AppCmpCode.Text), CompnyCode)
            iDiscAcctgOid = GetAcctgOID(GetVarInterface("VAR_DISC_JUAL", AppCmpCode.Text), CompnyCode)
            iJualAcctgOid = GetAcctgOID(GetVarInterface("VAR_PENJUALAN", AppCmpCode.Text), CompnyCode)
            iPpnAcctgOid = GetAcctgOID(GetVarInterface("VAR_PPN_OUT", AppCmpCode.Text), CompnyCode)
            iHppAcctgOid = GetAcctgOID(GetVarInterface("VAR_HPP", AppCmpCode.Text), CompnyCode)
            iPersediaanAcctgOid = GetAcctgOID(GetVarInterface("VAR_PERSEDIAAN", AppCmpCode.Text), CompnyCode)
            cRate.SetRateValue(ToInteger(Session("curroid")), Session("dodate"))

            Dim obj As DataTable = Session("TblDtlShipmentRM")
            For C1 As Int16 = 0 To obj.Rows.Count - 1
                'Set ID PO mst
                sodtloid = obj.Rows(C1).Item("dodtloid").ToString
                somstALL = somstALL & sodtloid & ","
            Next
            If (somstALL <> "") Then
                somstALL = somstALL.Substring(0, somstALL.Length - 1)
            End If
            sSql = "select sum(rd.valueidr) AS doamt, i.itemGroup AS dogroup, (sum(pod.sodtltaxamt)/sum(pod.sodtlqty))*sum(rd.doqty) AS taxamt, sum(rd.dodtlamt)-((sum(pom.sotaxamt)/sum(pod.sodtlqty)) * sum(rd.doqty)) AS doamttax, (select genother1 from QL_mstgen where gengroup='GROUPITEM' AND gencode=i.itemGroup) AS acctgoid from QL_trndomst rm inner join QL_trndodtl rd on rd.domstoid=rm.domstoid inner join QL_trnsodtl pod on pod.sodtloid=rd.sodtloid inner join QL_trnsomst pom on pom.somstoid=pod.somstoid inner join QL_mstitem i ON i.itemoid=rd.matoid where rd.dodtloid IN(" & somstALL & ") group by itemGroup"
            Dim tbl As DataTable = ckon.ambiltabel(sSql, "QL_trndomst")
            Session("TblGroup") = tbl
        End If
        ' Define Tabel Strucure utk Auto Jurnal 
        sSql = "SELECT '' tipe, 0 acctgoid, 0.0 debet, 0.0 credit, '' refno, '' note, '' AS glother1 "
        Dim tbPostGL As DataTable = ckon.ambiltabel(sSql, "PostGL")
        tbPostGL.Rows.Clear()
        Dim oRow As DataRow
        Dim dvCek As DataView = tbPostGL.DefaultView

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            ' Update DO
            If sStatus = "Approved" Then
                sSql = "UPDATE QL_trndomst SET dono='" & sNo & "', domststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP"
            Else
                sSql = "UPDATE QL_trndomst SET dono='" & sNo & "', domststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP" & IIf(sStatus = "Rejected", ", rejectreason='" & Tchar(ActionReason.Text) & "', rejectuser='" & Session("UserID") & "', rejecttime=CURRENT_TIMESTAMP", ", revisereason='" & Tchar(ActionReason.Text) & "', reviseuser='" & Session("UserID") & "', revisetime=CURRENT_TIMESTAMP")
            End If
            sSql &= " WHERE domststatus='In Approval' AND domstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trndomst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Rejected" Then
                    sSql = "UPDATE QL_trndodtl SET dodtlstatus='' WHERE cmpcode='" & AppCmpCode.Text & "' AND dodtloid IN (SELECT dodtloid FROM QL_trndodtl WHERE cmpcode='" & AppCmpCode.Text & "' AND domstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trndomst SET domststatus='Post' WHERE cmpcode='" & AppCmpCode.Text & "' AND domstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                ElseIf sStatus = "Approved" Then
                    'Update Stock
                    Dim objTable As DataTable = Session("TblDtlShipmentRM")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        Dim RateIDR As Double = ToDouble(objTable.Rows(C1).Item("valueidr"))
                        Dim RateUSD As Double = ToDouble(objTable.Rows(C1).Item("valueidr"))

                        sSql = "UPDATE QL_trndodtl SET valueidr=" & ToDouble(objTable.Rows(C1).Item("valueidr").ToString) & ", valueusd=" & ToDouble(objTable.Rows(C1).Item("valueidr").ToString) & " WHERE cmpcode='" & AppCmpCode.Text & "' AND dodtloid=" & objTable.Rows(C1).Item("dodtloid")
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()

                        ' Insert QL_constock 
                        sSql = "INSERT INTO QL_constock (cmpcode, constockoid, contype, trndate, formaction, formoid, periodacctg, refname, refoid, mtrlocoid, qtyin, qtyout, amount, hpp, typemin, note, reason, createuser, createtime, upduser, updtime, refno, valueidr, valueusd, qtyin_unitbesar, qtyout_unitbesar) VALUES ('" & AppCmpCode.Text & "', " & iConOid & ", 'SJ', '" & sDate & "', 'QL_trndodtl', " & Session("trnoid") & ", '" & sPeriod & "', '" & objTable.Rows(C1).Item("sotype") & "', " & objTable.Rows(C1).Item("itemoid") & ",  " & objTable.Rows(C1).Item("dowhoid") & ", 0, " & ToDouble(objTable.Rows(C1).Item("qtyunitkecil")) & ", " & objTable.Rows(C1).Item("sodtlnetto") & ", " & objTable.Rows(C1).Item("soprice_kons") & ", -1, '" & sNo & " #" & Tchar(objTable.Rows(C1).Item("custname").ToString) & "', 'Delivery Order', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & sNo & "'," & RateIDR & "," & RateUSD & ", 0, " & objTable.Rows(C1).Item("qtyunitbesar") & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iConOid += 1
                        dvCek.RowFilter = "acctgoid=" & objTable.Rows(C1).Item("stockacctgoid").ToString & " "
                        If dvCek.Count > 0 Then
                            dvCek(0)("credit") += RateIDR * ToDouble(objTable.Rows(C1).Item("qtyunitkecil").ToString)
                            dvCek.RowFilter = ""
                        Else
                            dvCek.RowFilter = ""

                            oRow = tbPostGL.NewRow
                            oRow("acctgoid") = objTable.Rows(C1).Item("stockacctgoid")
                            oRow("debet") = 0
                            oRow("credit") = RateIDR * ToDouble(objTable.Rows(C1).Item("qtyunitkecil").ToString)

                            tbPostGL.Rows.Add(oRow)
                        End If
                        tbPostGL.AcceptChanges()
                        ' Insert trnjualdtl
                        sSql = "INSERT INTO QL_trnjualdtl (cmpcode, trnjualdtloid, trnjualmstoid, trnjualdtlseq, dodtloid, itemlocoid, itemoid, trnjualdtlqty, trnjualdtlunitoid, trnjualdtlprice, trnjualdtldisctype, trnjualdtldiscvalue, trnjualdtldiscamt, trnjualdtldisctype2, trnjualdtldiscvalue2, trnjualdtldiscamt2, trnjualdtldisctype3, trnjualdtldiscvalue3, trnjualdtldiscamt3, trnjualdtlnetto, crtuser, crttime, upduser, updtime, trnjualdtldpp, trnjualdtltaxamt, old_code) VALUES ('" & AppCmpCode.Text & "', " & iJualDtl & ", " & iJualMst & ", " & C1 + 1 & ", " & objTable.Rows(C1).Item("dodtloid") & ", " & objTable.Rows(C1).Item("dowhoid") & ", " & objTable.Rows(C1).Item("itemoid") & ", " & objTable.Rows(C1).Item("doqty") & ", " & objTable.Rows(C1).Item("dounitoid") & ", " & objTable.Rows(C1).Item("sodtlprice") & ", '" & objTable.Rows(C1).Item("sodtldisctype1") & "', " & objTable.Rows(C1).Item("sodtldiscvalue1") & ", " & objTable.Rows(C1).Item("sodtldiscamt1") & ", '" & objTable.Rows(C1).Item("sodtldisctype2") & "', " & objTable.Rows(C1).Item("sodtldiscvalue2") & ", " & objTable.Rows(C1).Item("sodtldiscamt2") & ", '" & objTable.Rows(C1).Item("sodtldisctype3") & "', " & objTable.Rows(C1).Item("sodtldiscvalue3") & ", " & objTable.Rows(C1).Item("sodtldiscamt3") & "," & objTable.Rows(C1).Item("sodtlnetto") & ", '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & objTable.Rows(C1).Item("dppdtlamt") & ", " & objTable.Rows(C1).Item("taxdtlamt") & ", '" & Tchar(objTable.Rows(C1)("old_code").ToString) & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iJualDtl += 1
                        'Update SO
                        If ToDouble(objTable.Rows(C1)("sodtlqty").ToString) <= ToDouble(objTable.Rows(C1)("sisa").ToString) Then
                            sSql = "UPDATE QL_trnsodtl SET sodtlstatus='Complete' WHERE cmpcode='" & AppCmpCode.Text & "' AND sodtloid=" & objTable.Rows(C1)("sodtloid")
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_trnsomst SET somststatus='Closed' WHERE cmpcode='" & AppCmpCode.Text & "' AND somstoid=" & objTable.Rows(C1)("somstoid") & " AND (select COUNT(*) from QL_trnsodtl where cmpcode='" & AppCmpCode.Text & "' AND somstoid=" & objTable.Rows(C1)("somstoid") & " AND sodtloid<>" & objTable.Rows(C1)("sodtloid") & " AND sodtlstatus='')=0"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iConOid - 1 & " WHERE tablename='QL_CONSTOCK' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iJualDtl - 1 & " WHERE tablename='QL_TRNJUALDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    ' Get Accounting Value
                    Dim QtyDtl, bruto, disc, valdisc, discAmt, disc2, valdisc2, discAmt2, tax, dAmt As Double
                    Dim custoid, unitoid, paytype As Integer
                    Dim tipe As String = ""
                    Dim custname As String = ""
                    Dim tipetax As String = ""
                    Dim tipedisc As String = "" : Dim tipedisc2 As String = ""
                    Dim salescode As String = ""
                    For C1 As Integer = 0 To objTable.Rows.Count - 1
                        Dim dNet As Double = ToDouble(objTable.Rows(C1)("sodtlnetto").ToString)
                        ' Including Discount Header
                        Dim dDisc As Double = 0
                        If tipedisc.ToUpper = "P" Then
                            dDisc = (dNet * ToDouble(objTable.Rows(C1)("somstdiscvalue").ToString)) / 100
                        Else
                            dDisc = (dNet * ToDouble(objTable.Rows(C1)("somstdiscvalue").ToString)) / ToDouble(objTable.Rows(C1)("sodtlnetto").ToString)
                        End If
                        dNet -= dDisc
                        Dim dDisc2 As Double = 0
                        If tipedisc2.ToUpper = "P" Then
                            dDisc2 = (dNet * ToDouble(objTable.Rows(C1)("somstdiscvalue2").ToString)) / 100
                        Else
                            dDisc2 = (dNet * ToDouble(objTable.Rows(C1)("somstdiscvalue2").ToString)) / ToDouble(objTable.Rows(C1)("sodtlnetto").ToString)
                        End If
                        dNet -= dDisc2
                        ' Including Amount
                        Dim dTax, DPP As Double
                        dTax = ToDouble(objTable.Rows(C1)("taxdtlamt").ToString)
                        dNet += dTax
                        dAmt += dNet
                        bruto += ToDouble(objTable.Rows(C1)("sodtlnetto").ToString)
                        disc += dDisc
                        disc2 += dDisc2
                        tax += dTax
                        DPP += ToDouble(objTable.Rows(C1)("dppdtlamt").ToString)
                        tipe = Tchar(objTable.Rows(C1)("sotype").ToString)
                        custoid = objTable.Rows(C1)("custoid").ToString
                        custname = Tchar(objTable.Rows(C1)("custname").ToString)
                        unitoid = objTable.Rows(C1)("dounitoid").ToString
                        paytype = objTable.Rows(C1)("sopaytypeoid").ToString
                        tipedisc = Tchar(objTable.Rows(C1)("somstdisctype").ToString)
                        valdisc = ToDouble(objTable.Rows(C1)("somstdiscvalue").ToString)
                        discAmt = ToDouble(objTable.Rows(C1)("somstdiscamt").ToString)
                        tipedisc2 = Tchar(objTable.Rows(C1)("somstdisctype2").ToString)
                        valdisc2 = ToDouble(objTable.Rows(C1)("somstdiscvalue2").ToString)
                        discAmt2 = ToDouble(objTable.Rows(C1)("somstdiscamt2").ToString)
                        tipetax = Tchar(objTable.Rows(C1)("sotaxtype").ToString)
                        salescode = Tchar(objTable.Rows(C1)("sosalescode").ToString)
                        QtyDtl += ToDouble(objTable.Rows(C1)("doqty").ToString)
                    Next

                    'Insert trnjualmst
                    Dim discHeader As Double = ToDouble(Session("dHead")) * QtyDtl
                    Dim discHeader2 As Double = ToDouble(Session("dHead2")) * QtyDtl
                    Dim brutoHeader As Double = 0
                    Dim dppHeader As Double = 0
                    Dim taxHeader As Double = 0
                    Dim nettoHeader As Double = 0
                    Dim dDiv As Double = GetNewTaxValueInclude(Session("dodate"))
                    If Session("tTax") = "EXC" Then
                        brutoHeader &= bruto / dDiv
                        dppHeader &= brutoHeader - (discHeader / dDiv) - (discHeader2 / dDiv)
                        taxHeader &= dppHeader * GetNewTaxValue(Session("dodate")) / 100
                        nettoHeader &= Math.Round(dppHeader + taxHeader, 0, MidpointRounding.AwayFromZero)
                    ElseIf Session("tTax") = "INC" Then
                        brutoHeader &= bruto / dDiv
                        dppHeader &= brutoHeader - (discHeader / dDiv) - (discHeader2 / dDiv)
                        taxHeader &= dppHeader * GetNewTaxValue(Session("dodate")) / 100
                        nettoHeader &= Math.Round(dppHeader + taxHeader, 0, MidpointRounding.AwayFromZero)
                    Else
                        brutoHeader &= bruto
                        dppHeader &= bruto - discHeader - discHeader2
                        taxHeader &= 0
                        nettoHeader &= dppHeader + taxHeader
                    End If
                    Dim komisiSales As Double = nettoHeader * (Session("komisi") / 100)
                    sSql = "INSERT INTO QL_trnjualmst (cmpcode, trnjualmstoid, periodacctg, trnjualtype, trnjualdate, custoid, custname, domstoid, trnpaytype, trnjualamt, trndisctype, trndiscvalue, trndiscamt, trndisctype2, trndiscvalue2, trndiscamt2, trntaxtype, trntaxamt, trnjualamtnetto, salescode, trnjualres1, trnjualstatus, crtuser, crttime, upduser, updtime, jualratetoidr, jualratetousd, trnjualamtidr, trnjualamtusd, trnjualamtnettoidr, trnjualamtnettousd, trnjualhpp, komisisales, trnjualno) VALUES ('" & AppCmpCode.Text & "', " & iJualMst & ", '" & sPeriod & "', '" & tipe & "', '" & sDate & "', " & custoid & ", '" & custname & "'," & Session("trnoid") & ", " & paytype & ", " & brutoHeader & ", '" & Session("disctype") & "', " & Session("discval") & ", " & discHeader & ", '" & Session("disctype2") & "', " & Session("discval2") & ", " & discHeader2 & ", '" & Session("tTax") & "', " & taxHeader & "," & nettoHeader & ", '" & salescode & "', '" & Session("sType") & "', 'In Process', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateMonthlyUSDValue & ", " & brutoHeader * cRate.GetRateMonthlyIDRValue & ", " & brutoHeader * cRate.GetRateMonthlyUSDValue & ", " & nettoHeader * cRate.GetRateMonthlyIDRValue & ", " & nettoHeader * cRate.GetRateMonthlyUSDValue & ", " & dppHeader & ", " & komisiSales & ", '" & sNo2 & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iJualMst & " WHERE tablename='QL_TRNJUALMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    ' Posting Auto Journal DO
                    ' Insert QL_trnglmst
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & AppCmpCode.Text & "', " & iGlMstOid & ", '" & sDate & "', '" & sPeriod & "', 'SJ|No. " & sNo & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    Dim dDebetAmt As Double = 0
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        dDebetAmt += ToDouble(objTable.Rows(C1).Item("valueidr")) * ToDouble(objTable.Rows(C1).Item("qtyunitkecil").ToString)
                    Next

                    ' Insert QL_trngldtl
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iDelAcctgOid & ", 'D', " & dDebetAmt & ", '" & sNo & "', 'SJ|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dDebetAmt & ", " & dDebetAmt * cRate.GetRateMonthlyUSDValue & ", 'QL_trndomst " & Session("trnoid") & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1
                    iSeq += 1
                    For C1 As Integer = 0 To tbPostGL.Rows.Count - 1
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & tbPostGL.Rows(C1)("acctgoid") & ", 'C', " & ToDouble(tbPostGL.Rows(C1)("credit").ToString) & ", '" & sNo & "', 'SJ|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(tbPostGL.Rows(C1)("credit").ToString) & ", " & ToDouble(tbPostGL.Rows(C1)("credit").ToString) * cRate.GetRateMonthlyUSDValue & ", 'QL_trndomst " & Session("trnoid") & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1
                        iSeq += 1
                    Next

                    Dim obj1 As DataTable = Session("TblGroup")
                    For C1 As Int16 = 0 To obj1.Rows.Count - 1
                        iStockAcctgOid = obj1.Rows(C1).Item("acctgoid").ToString
                        doAmt = ToDouble(obj1.Rows(C1).Item("doamt").ToString)
                        doAmtIDR = ToDouble(obj1.Rows(C1).Item("doamt").ToString) * cRate.GetRateMonthlyIDRValue
                        doAmtUSD = ToDouble(obj1.Rows(C1).Item("doamt").ToString) * cRate.GetRateMonthlyUSDValue
                        doAmtTax = ToDouble(obj1.Rows(C1).Item("doamttax").ToString)
                        doAmtTaxIDR = ToDouble(obj1.Rows(C1).Item("doamttax").ToString) * cRate.GetRateMonthlyIDRValue
                        doAmtTaxUSD = ToDouble(obj1.Rows(C1).Item("doamttax").ToString) * cRate.GetRateMonthlyUSDValue
                        taxAmt = ToDouble(obj1.Rows(C1).Item("taxamt").ToString)
                        taxAmtIDR = ToDouble(obj1.Rows(C1).Item("taxamt").ToString) * cRate.GetRateMonthlyIDRValue
                        taxAmtUSD = ToDouble(obj1.Rows(C1).Item("taxamt").ToString) * cRate.GetRateMonthlyUSDValue

                    Next

                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
            End If

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "Shipment General Material Approval"
    Private Sub SetGVListShipmentGM()
        Dim arHeader() As String = {"Draft No.", "Shipment Date", "Customer", "Shipment Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "custname", "shipmentgenmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "custname", "shipmentgenmstnote", "upduser", "updtime", "curroid"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT sm.cmpcode, sm.shipmentgenmstoid AS trnoid, CONVERT(VARCHAR(10), sm.shipmentgendate, 101) AS trndate, c.custname, sm.shipmentgenmstnote, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, sm.upduser, sm.updtime, sm.curroid FROM QL_trnshipmentgenmst sm INNER JOIN QL_approval ap ON sm.shipmentgenmstoid=ap.oid AND ap.statusrequest='New' INNER JOIN QL_mstcust c ON c.custoid=sm.custoid WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_trnshipmentgenmst' AND sm.shipmentgenmststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnshipmentgenmst")
    End Sub

    Private Sub SetGVDataShipmentGM(ByVal dtDataHeader As DataTable)
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "Shipment Date", "Customer", "Shipment Note", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "custname", "shipmentgenmstnote", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arHeader() As String = {"No.", "Code", "Description", "Quantity", "Unit", "Warehouse", "Note"}
        Dim arField() As String = {"shipmentgendtlseq", "matgencode", "matgenlongdesc", "shipmentgenqty", "shipmentgenunit", "shipmentgenwh", "shipmentgendtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT sd.shipmentgendtlseq, m.matgenoid, m.matgencode, m.matgenlongdesc, sd.shipmentgenqty, g.gendesc AS shipmentgenunit, g2.gendesc AS shipmentgenwh, sd.shipmentgendtlnote, sd.shipmentgenwhoid, ((sod.sogendtlnetto / sod.sogenqty) * sd.shipmentgenqty) AS sogendtlnetto, som.sogenmstdisctype, som.sogenmstdiscvalue, som.sogentotalnetto, som.sogentaxtype, som.sogentaxamt, (som.sogendeliverycost + som.sogenothercost) AS sogenothercost, som.sogengrandtotalamt, som.sogenno, ISNULL(shipmentgendtlres3, '') AS shipmentonsostatus, sod.sogendtloid, som.sogenmstoid FROM QL_trnshipmentgenmst sm INNER JOIN QL_trnshipmentgendtl sd ON sd.cmpcode=sm.cmpcode AND sm.shipmentgenmstoid=sd.shipmentgenmstoid INNER JOIN QL_mstmatgen m ON sd.matgenoid=m.matgenoid INNER JOIN QL_mstgen g ON sd.shipmentgenunitoid=g.genoid INNER JOIN QL_mstgen g2 ON sd.shipmentgenwhoid=g2.genoid INNER JOIN QL_trndogenmst dom ON dom.cmpcode=sd.cmpcode AND dom.dogenmstoid=sd.dogenmstoid INNER JOIN QL_trndogendtl dod ON dod.cmpcode=sd.cmpcode AND dod.dogendtloid=sd.dogendtloid INNER JOIN QL_trnsogenmst som ON som.cmpcode=dod.cmpcode AND som.sogenmstoid=dod.sogenmstoid INNER JOIN QL_trnsogendtl sod ON sod.cmpcode=dod.cmpcode AND sod.sogendtloid=dod.sogendtloid WHERE sd.shipmentgenmstoid=" & Session("trnoid") & " ORDER BY sd.shipmentgendtlseq"
        Session("TblDtlShipmentGM") = ckon.ambiltabel(sSql, "QL_trnshipmentgendtl")
        gvData.DataSource = Session("TblDtlShipmentGM")
        gvData.DataBind()
    End Sub

    Private Function GenerateShipmentGMNo() As String
        Dim sNo As String = "SJGM-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(shipmentgenno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnshipmentgenmst WHERE cmpcode='" & AppCmpCode.Text & "' AND shipmentgenno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdateShipmentGM(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select Shipment General Material Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select Shipment General Material Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim iConOid As Integer = GenerateID("QL_CONMAT", CompnyCode)
        Dim iCrdOid As Integer = GenerateID("QL_CRDMTR", CompnyCode)
        Dim iGlMstOid As Int32 = GenerateID("QL_TRNGLMST", CompnyCode)
        Dim iGlDtlOid As Int32 = GenerateID("QL_TRNGLDTL", CompnyCode)
        Dim iDelAcctgOid, iStockAcctgOid As Integer
        Dim iSeq As Int16 = 1
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
        Dim cRate As New ClassRate
        If sStatus = "Approved" Then
            cRate.SetRateValue(CInt(Session("curroid")), Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            Dim sVarErr As String = ""
            If Not IsInterfaceExists("VAR_STOCK_TRANSIT", AppCmpCode.Text) Then
                sVarErr = "VAR_STOCK_TRANSIT"
            End If
            If Not IsInterfaceExists("VAR_STOCK_GM", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_STOCK_GM"
            End If
            If sVarErr <> "" Then
                showMessage(GetInterfaceWarning(sVarErr, "approved"), CompnyName & " - WARNING", 2)
                Exit Sub
            End If
            iDelAcctgOid = GetAcctgOID(GetVarInterface("VAR_STOCK_TRANSIT", AppCmpCode.Text), CompnyCode)
            iStockAcctgOid = GetAcctgOID(GetVarInterface("VAR_STOCK_GM", AppCmpCode.Text), CompnyCode)
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update Shipment
            sSql = "UPDATE QL_trnshipmentgenmst SET shipmentgenno='" & sNo & "', shipmentgenmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE shipmentgenmststatus='In Approval' AND shipmentgenmstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnshipmentgenmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Rejected" Then
                    sSql = "UPDATE QL_trndogendtl SET dogendtlstatus='' WHERE cmpcode='" & AppCmpCode.Text & "' AND dogendtloid IN (SELECT dogendtloid FROM QL_trnshipmentgendtl WHERE cmpcode='" & AppCmpCode.Text & "' AND shipmentgenmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trndogenmst SET dogenmststatus='Post' WHERE cmpcode='" & AppCmpCode.Text & "' AND dogenmstoid IN (SELECT DISTINCT dogenmstoid FROM QL_trnshipmentgendtl WHERE cmpcode='" & AppCmpCode.Text & "' AND shipmentgenmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                ElseIf sStatus = "Approved" Then
                    Dim objTable As DataTable = Session("TblDtlShipmentGM")
                    'Update Stock
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        If objTable.Rows(C1)("shipmentonsostatus").ToString <> "" Then
                            sSql = "UPDATE QL_trnsogendtl SET sogendtlstatus='' WHERE cmpcode='" & AppCmpCode.Text & "' AND sogendtloid=" & objTable.Rows(C1)("sogendtloid")
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_trnsogenmst SET sogenmststatus='Approved' WHERE cmpcode='" & AppCmpCode.Text & "' AND sogenmstoid=" & objTable.Rows(C1)("sogenmstoid")
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                        ' Insert QL_conmat
                        sSql = "INSERT INTO QL_conmat (cmpcode, conmtroid, type, typemin, trndate, periodacctg, formaction, formoid, refoid, refname, mtrwhoid, qtyin, qtyout, reason, note, upduser, updtime, valueidr, valueusd) VALUES ('" & AppCmpCode.Text & "', " & iConOid & ", 'SJGM', -1, '" & sDate & "', '" & sPeriod & "', 'QL_trnshipmentgendtl', " & Session("trnoid") & ", " & objTable.Rows(C1).Item("matgenoid") & ", 'GENERAL MATERIAL', " & objTable.Rows(C1).Item("shipmentgenwhoid") & ", 0, " & ToDouble(objTable.Rows(C1).Item("shipmentgenqty")) & ", 'Shipment General Material', '" & sNo & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & (ToDouble(objTable.Rows(C1).Item("sogendtlnetto")) / ToDouble(objTable.Rows(C1).Item("shipmentgenqty"))) * cRate.GetRateMonthlyIDRValue & ", " & (ToDouble(objTable.Rows(C1).Item("sogendtlnetto")) / ToDouble(objTable.Rows(C1).Item("shipmentgenqty"))) * cRate.GetRateMonthlyUSDValue & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iConOid += 1
                        ' Update QL_crdmtr
                        sSql = "UPDATE QL_crdmtr SET qtyout=qtyout + " & ToDouble(objTable.Rows(C1).Item("shipmentgenqty")) & ", saldoakhir=saldoakhir - " & ToDouble(objTable.Rows(C1).Item("shipmentgenqty")) & ", lasttranstype='QL_trnshipmentgendtl', lasttransdate='" & sDate & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & AppCmpCode.Text & "' AND refoid=" & objTable.Rows(C1).Item("matgenoid") & " AND refname='GENERAL MATERIAL' AND mtrwhoid=" & objTable.Rows(C1).Item("shipmentgenwhoid") & " AND periodacctg='" & sPeriod & "'"
                        xCmd.CommandText = sSql
                        If xCmd.ExecuteNonQuery() = 0 Then
                            ' Insert QL_crdmtr
                            sSql = "INSERT INTO QL_crdmtr (cmpcode, crdmatoid, periodacctg, refoid, refname, mtrwhoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir, lasttranstype, lasttransdate, upduser, updtime, createuser, createdate, closeuser, closingdate) VALUES ('" & AppCmpCode.Text & "', " & iCrdOid & ", '" & sPeriod & "', " & objTable.Rows(C1).Item("matgenoid") & ", 'GENERAL MATERIAL', " & objTable.Rows(C1).Item("shipmentgenwhoid") & ", 0, " & ToDouble(objTable.Rows(C1).Item("shipmentgenqty")) & ", 0, 0, 0, " & -ToDouble(objTable.Rows(C1).Item("shipmentgenqty")) & ", 'QL_trnshipmentgendtl', '" & sDate & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', '1/1/1900')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iCrdOid += 1
                        End If
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iConOid - 1 & " WHERE tablename='QL_CONMAT' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iCrdOid - 1 & " WHERE tablename='QL_CRDMTR' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    ' Get Accounting Value
                    Dim dAmt As Double = 0
                    For C1 As Integer = 0 To objTable.Rows.Count - 1
                        Dim dNet As Double = ToDouble(objTable.Rows(C1)("sogendtlnetto").ToString)
                        ' Including Discount Header
                        Dim dDisc As Double = 0
                        If objTable.Rows(C1)("sogenmstdisctype").ToString.ToUpper = "P" Then
                            dDisc = (dNet * ToDouble(objTable.Rows(C1)("sogenmstdiscvalue").ToString)) / 100
                        Else
                            dDisc = (dNet * ToDouble(objTable.Rows(C1)("sogenmstdiscvalue").ToString)) / ToDouble(objTable.Rows(C1)("sogentotalnetto").ToString)
                        End If
                        dNet -= dDisc
                        ' Including Tax
                        Dim dTax As Double = 0
                        If objTable.Rows(C1)("sogentaxtype").ToString.ToUpper = "TAX" Then
                            dTax = (dNet * ToDouble(objTable.Rows(C1)("sogentaxamt").ToString)) / 100
                        Else
                            dTax = 0
                        End If
                        dNet += dTax
                        ' Including Other Cost
                        Dim dCost As Double = (dNet * ToDouble(objTable.Rows(C1)("sogenothercost").ToString)) / ToDouble(objTable.Rows(C1)("sogengrandtotalamt").ToString)
                        dNet += dCost
                        dAmt += dNet
                    Next

                    ' Posting Auto Journal Accounting
                    ' Insert QL_trnglmst
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & AppCmpCode.Text & "', " & iGlMstOid & ", '" & sDate & "', '" & sPeriod & "', 'Shipment GM|No. " & sNo & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Insert QL_trngldtl
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iDelAcctgOid & ", 'D', " & dAmt & ", '" & sNo & "', 'Shipment GM|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dAmt * cRate.GetRateMonthlyIDRValue & ", " & dAmt * cRate.GetRateMonthlyUSDValue & ", 'QL_trnshipmentgenmst " & Session("trnoid") & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1
                    iSeq += 1
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iStockAcctgOid & ", 'C', " & dAmt & ", '" & sNo & "', 'Shipment GM|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dAmt * cRate.GetRateMonthlyIDRValue & ", " & dAmt * cRate.GetRateMonthlyUSDValue & ", 'QL_trnshipmentgenmst " & Session("trnoid") & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "Shipment Spare Part Approval"
    Private Sub SetGVListShipmentSP()
        Dim arHeader() As String = {"Draft No.", "Shipment Date", "Customer", "Shipment Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "custname", "shipmentspmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "custname", "shipmentspmstnote", "upduser", "updtime", "curroid"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT sm.cmpcode, sm.shipmentspmstoid AS trnoid, CONVERT(VARCHAR(10), sm.shipmentspdate, 101) AS trndate, c.custname, sm.shipmentspmstnote, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, sm.upduser, sm.updtime, sm.curroid FROM QL_trnshipmentspmst sm INNER JOIN QL_approval ap ON sm.shipmentspmstoid=ap.oid AND ap.statusrequest='New' INNER JOIN QL_mstcust c ON c.custoid=sm.custoid WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_trnshipmentspmst' AND sm.shipmentspmststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnshipmentspmst")
    End Sub

    Private Sub SetGVDataShipmentSP(ByVal dtDataHeader As DataTable)
        Session("curroid") = gvList.SelectedDataKey.Item("curroid").ToString
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "Shipment Date", "Customer", "Shipment Note", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "custname", "shipmentspmstnote", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arHeader() As String = {"No.", "Code", "Description", "Quantity", "Unit", "Warehouse", "Note"}
        Dim arField() As String = {"shipmentspdtlseq", "sparepartcode", "sparepartlongdesc", "shipmentspqty", "shipmentspunit", "shipmentspwh", "shipmentspdtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT sd.shipmentspdtlseq, m.sparepartoid, m.sparepartcode, m.sparepartlongdesc, sd.shipmentspqty, g.gendesc AS shipmentspunit, g2.gendesc AS shipmentspwh, sd.shipmentspdtlnote, sd.shipmentspwhoid, ((sod.sospdtlnetto / sod.sospqty) * sd.shipmentspqty) AS sospdtlnetto, som.sospmstdisctype, som.sospmstdiscvalue, som.sosptotalnetto, som.sosptaxtype, som.sosptaxamt, (som.sospdeliverycost + som.sospothercost) AS sospothercost, som.sospgrandtotalamt, som.sospno, ISNULL(shipmentspdtlres3, '') AS shipmentonsostatus, sod.sospdtloid, som.sospmstoid FROM QL_trnshipmentspmst sm INNER JOIN QL_trnshipmentspdtl sd ON sd.cmpcode=sm.cmpcode AND sm.shipmentspmstoid=sd.shipmentspmstoid INNER JOIN QL_mstsparepart m ON sd.sparepartoid=m.sparepartoid INNER JOIN QL_mstgen g ON sd.shipmentspunitoid=g.genoid INNER JOIN QL_mstgen g2 ON sd.shipmentspwhoid=g2.genoid INNER JOIN QL_trndospmst dom ON dom.cmpcode=sd.cmpcode AND dom.dospmstoid=sd.dospmstoid INNER JOIN QL_trndospdtl dod ON dod.cmpcode=sd.cmpcode AND dod.dospdtloid=sd.dospdtloid INNER JOIN QL_trnsospmst som ON som.cmpcode=dod.cmpcode AND som.sospmstoid=dod.sospmstoid INNER JOIN QL_trnsospdtl sod ON sod.cmpcode=dod.cmpcode AND sod.sospdtloid=dod.sospdtloid WHERE sd.shipmentspmstoid=" & Session("trnoid") & " ORDER BY sd.shipmentspdtlseq"
        Session("TblDtlShipmentSP") = ckon.ambiltabel(sSql, "QL_trnshipmentspdtl")
        gvData.DataSource = Session("TblDtlShipmentSP")
        gvData.DataBind()
    End Sub

    Private Function GenerateShipmentSPNo() As String
        Dim sNo As String = "SJSP-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(shipmentspno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnshipmentspmst WHERE cmpcode='" & AppCmpCode.Text & "' AND shipmentspno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdateShipmentSP(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select Shipment Spare Part Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select Shipment Spare Part Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim iConOid As Integer = GenerateID("QL_CONMAT", CompnyCode)
        Dim iCrdOid As Integer = GenerateID("QL_CRDMTR", CompnyCode)
        Dim iGlMstOid As Int32 = GenerateID("QL_TRNGLMST", CompnyCode)
        Dim iGlDtlOid As Int32 = GenerateID("QL_TRNGLDTL", CompnyCode)
        Dim iDelAcctgOid, iStockAcctgOid As Integer
        Dim iSeq As Int16 = 1
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
        Dim cRate As New ClassRate
        If sStatus = "Approved" Then
            cRate.SetRateValue(CInt(Session("curroid")), Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            Dim sVarErr As String = ""
            If Not IsInterfaceExists("VAR_STOCK_TRANSIT", AppCmpCode.Text) Then
                sVarErr = "VAR_STOCK_TRANSIT"
            End If
            If Not IsInterfaceExists("VAR_STOCK_SP", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_STOCK_SP"
            End If
            If sVarErr <> "" Then
                showMessage(GetInterfaceWarning(sVarErr, "approved"), CompnyName & " - WARNING", 2)
                Exit Sub
            End If
            iDelAcctgOid = GetAcctgOID(GetVarInterface("VAR_STOCK_TRANSIT", AppCmpCode.Text), CompnyCode)
            iStockAcctgOid = GetAcctgOID(GetVarInterface("VAR_STOCK_SP", AppCmpCode.Text), CompnyCode)
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update Shipment
            sSql = "UPDATE QL_trnshipmentspmst SET shipmentspno='" & sNo & "', shipmentspmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE shipmentspmststatus='In Approval' AND shipmentspmstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnshipmentspmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Rejected" Then
                    sSql = "UPDATE QL_trndospdtl SET dospdtlstatus='' WHERE cmpcode='" & AppCmpCode.Text & "' AND dospdtloid IN (SELECT dospdtloid FROM QL_trnshipmentspdtl WHERE cmpcode='" & AppCmpCode.Text & "' AND shipmentspmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trndospmst SET dospmststatus='Post' WHERE cmpcode='" & AppCmpCode.Text & "' AND dospmstoid IN (SELECT DISTINCT dospmstoid FROM QL_trnshipmentspdtl WHERE cmpcode='" & AppCmpCode.Text & "' AND shipmentspmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                ElseIf sStatus = "Approved" Then
                    Dim objTable As DataTable = Session("TblDtlShipmentSP")
                    'Update Stock
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        If objTable.Rows(C1)("shipmentonsostatus").ToString <> "" Then
                            sSql = "UPDATE QL_trnsospdtl SET sospdtlstatus='' WHERE cmpcode='" & AppCmpCode.Text & "' AND sospdtloid=" & objTable.Rows(C1)("sospdtloid")
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_trnsospmst SET sospmststatus='Approved' WHERE cmpcode='" & AppCmpCode.Text & "' AND sospmstoid=" & objTable.Rows(C1)("sospmstoid")
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                        ' Insert QL_conmat
                        sSql = "INSERT INTO QL_conmat (cmpcode, conmtroid, type, typemin, trndate, periodacctg, formaction, formoid, refoid, refname, mtrwhoid, qtyin, qtyout, reason, note, upduser, updtime, valueidr, valueusd) VALUES ('" & AppCmpCode.Text & "', " & iConOid & ", 'SJSP', -1, '" & sDate & "', '" & sPeriod & "', 'QL_trnshipmentspdtl', " & Session("trnoid") & ", " & objTable.Rows(C1).Item("sparepartoid") & ", 'SPARE PART', " & objTable.Rows(C1).Item("shipmentspwhoid") & ", 0, " & ToDouble(objTable.Rows(C1).Item("shipmentspqty")) & ", 'Shipment Spare Part', '" & sNo & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & (ToDouble(objTable.Rows(C1).Item("sospdtlnetto")) / ToDouble(objTable.Rows(C1).Item("shipmentspqty"))) * cRate.GetRateMonthlyIDRValue & ", " & (ToDouble(objTable.Rows(C1).Item("sospdtlnetto")) / ToDouble(objTable.Rows(C1).Item("shipmentspqty"))) * cRate.GetRateMonthlyUSDValue & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iConOid += 1
                        ' Update QL_crdmtr
                        sSql = "UPDATE QL_crdmtr SET qtyout=qtyout + " & ToDouble(objTable.Rows(C1).Item("shipmentspqty")) & ", saldoakhir=saldoakhir - " & ToDouble(objTable.Rows(C1).Item("shipmentspqty")) & ", lasttranstype='QL_trnshipmentspdtl', lasttransdate='" & sDate & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & AppCmpCode.Text & "' AND refoid=" & objTable.Rows(C1).Item("sparepartoid") & " AND refname='SPARE PART' AND mtrwhoid=" & objTable.Rows(C1).Item("shipmentspwhoid") & " AND periodacctg='" & sPeriod & "'"
                        xCmd.CommandText = sSql
                        If xCmd.ExecuteNonQuery() = 0 Then
                            ' Insert QL_crdmtr
                            sSql = "INSERT INTO QL_crdmtr (cmpcode, crdmatoid, periodacctg, refoid, refname, mtrwhoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir, lasttranstype, lasttransdate, upduser, updtime, createuser, createdate, closeuser, closingdate) VALUES ('" & AppCmpCode.Text & "', " & iCrdOid & ", '" & sPeriod & "', " & objTable.Rows(C1).Item("sparepartoid") & ", 'SPARE PART', " & objTable.Rows(C1).Item("shipmentspwhoid") & ", 0, " & ToDouble(objTable.Rows(C1).Item("shipmentspqty")) & ", 0, 0, 0, " & -ToDouble(objTable.Rows(C1).Item("shipmentspqty")) & ", 'QL_trnshipmentspdtl', '" & sDate & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', '1/1/1900')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iCrdOid += 1
                        End If
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iConOid - 1 & " WHERE tablename='QL_CONMAT' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iCrdOid - 1 & " WHERE tablename='QL_CRDMTR' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    ' Get Accounting Value
                    Dim dAmt As Double = 0
                    For C1 As Integer = 0 To objTable.Rows.Count - 1
                        Dim dNet As Double = ToDouble(objTable.Rows(C1)("sospdtlnetto").ToString)
                        ' Including Discount Header
                        Dim dDisc As Double = 0
                        If objTable.Rows(C1)("sospmstdisctype").ToString.ToUpper = "P" Then
                            dDisc = (dNet * ToDouble(objTable.Rows(C1)("sospmstdiscvalue").ToString)) / 100
                        Else
                            dDisc = (dNet * ToDouble(objTable.Rows(C1)("sospmstdiscvalue").ToString)) / ToDouble(objTable.Rows(C1)("sosptotalnetto").ToString)
                        End If
                        dNet -= dDisc
                        ' Including Tax
                        Dim dTax As Double = 0
                        If objTable.Rows(C1)("sosptaxtype").ToString.ToUpper = "TAX" Then
                            dTax = (dNet * ToDouble(objTable.Rows(C1)("sosptaxamt").ToString)) / 100
                        Else
                            dTax = 0
                        End If
                        dNet += dTax
                        ' Including Other Cost
                        Dim dCost As Double = (dNet * ToDouble(objTable.Rows(C1)("sospothercost").ToString)) / ToDouble(objTable.Rows(C1)("sospgrandtotalamt").ToString)
                        dNet += dCost
                        dAmt += dNet
                    Next

                    ' Posting Auto Journal Accounting
                    ' Insert QL_trnglmst
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & AppCmpCode.Text & "', " & iGlMstOid & ", '" & sDate & "', '" & sPeriod & "', 'Shipment SP|No. " & sNo & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Insert QL_trngldtl
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iDelAcctgOid & ", 'D', " & dAmt & ", '" & sNo & "', 'Shipment SP|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dAmt * cRate.GetRateMonthlyIDRValue & ", " & dAmt * cRate.GetRateMonthlyUSDValue & ", 'QL_trnshipmentspmst " & Session("trnoid") & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1
                    iSeq += 1
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iStockAcctgOid & ", 'C', " & dAmt & ", '" & sNo & "', 'Shipment SP|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dAmt * cRate.GetRateMonthlyIDRValue & ", " & dAmt * cRate.GetRateMonthlyUSDValue & ", 'QL_trnshipmentspmst " & Session("trnoid") & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "Shipment Finish Good Approval"
    Private Sub SetGVListShipmentItem()
        Dim arHeader() As String = {"Draft No.", "Shipment Date", "Customer", "Shipment Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "custname", "shipmentitemmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "custname", "shipmentitemmstnote", "upduser", "updtime", "curroid"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT sm.cmpcode, sm.shipmentitemmstoid AS trnoid, CONVERT(VARCHAR(10), sm.shipmentitemdate, 101) AS trndate, c.custname, sm.shipmentitemmstnote, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, sm.upduser, sm.updtime, sm.curroid FROM QL_trnshipmentitemmst sm INNER JOIN QL_approval ap ON sm.shipmentitemmstoid=ap.oid AND ap.statusrequest='New' INNER JOIN QL_mstcust c ON c.custoid=sm.custoid WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_trnshipmentitemmst' AND sm.shipmentitemmststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnshipmentitemmst")
    End Sub

    Private Sub SetGVDataShipmentItem(ByVal dtDataHeader As DataTable)
        Session("curroid") = gvList.SelectedDataKey.Item("curroid").ToString
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "Shipment Date", "Customer", "Shipment Note", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "custname", "shipmentitemmstnote", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arHeader() As String = {"No.", "Code", "Description", "Quantity", "Unit", "Warehouse", "Note"}
        Dim arField() As String = {"shipmentitemdtlseq", "itemcode", "itemlongdesc", "shipmentitemqty", "shipmentitemunit", "shipmentitemwh", "shipmentitemdtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT sd.shipmentitemdtlseq, m.itemoid, m.itemcode, m.itemlongdesc, sd.shipmentitemqty, g.gendesc AS shipmentitemunit, g2.gendesc AS shipmentitemwh, sd.shipmentitemdtlnote, sd.shipmentitemwhoid, ((sod.soitemdtlnetto / sod.soitemqty) * sd.shipmentitemqty) AS soitemdtlnetto, som.soitemmstdisctype, som.soitemmstdiscvalue, som.soitemtotalnetto, som.soitemtaxtype, som.soitemtaxamt, (som.soitemdeliverycost + som.soitemothercost) AS soitemothercost, som.soitemgrandtotalamt, som.soitemno, ISNULL(shipmentitemdtlres3, '') AS shipmentonsostatus, sod.soitemdtloid, som.soitemmstoid FROM QL_trnshipmentitemmst sm INNER JOIN QL_trnshipmentitemdtl sd ON sd.cmpcode=sm.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid INNER JOIN QL_mstitem m ON sd.itemoid=m.itemoid INNER JOIN QL_mstgen g ON sd.shipmentitemunitoid=g.genoid INNER JOIN QL_mstgen g2 ON sd.shipmentitemwhoid=g2.genoid INNER JOIN QL_trndoitemmst dom ON dom.cmpcode=sd.cmpcode AND dom.doitemmstoid=sd.doitemmstoid INNER JOIN QL_trndoitemdtl dod ON dod.cmpcode=sd.cmpcode AND dod.doitemdtloid=sd.doitemdtloid INNER JOIN QL_trnsoitemmst som ON som.cmpcode=dod.cmpcode AND som.soitemmstoid=dod.soitemmstoid INNER JOIN QL_trnsoitemdtl sod ON sod.cmpcode=dod.cmpcode AND sod.soitemdtloid=dod.soitemdtloid WHERE sd.shipmentitemmstoid=" & Session("trnoid") & " ORDER BY sd.shipmentitemdtlseq"
        Session("TblDtlShipmentItem") = ckon.ambiltabel(sSql, "QL_trnshipmentitemdtl")
        gvData.DataSource = Session("TblDtlShipmentItem")
        gvData.DataBind()
    End Sub

    Private Function GenerateShipmentItemNo() As String
        Dim sNo As String = "SJFG-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(shipmentitemno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnshipmentitemmst WHERE cmpcode='" & AppCmpCode.Text & "' AND shipmentitemno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdateShipmentItem(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select Shipment Finish Good Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select Shipment Finish Good Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim iConOid As Integer = GenerateID("QL_CONMAT", CompnyCode)
        Dim iCrdOid As Integer = GenerateID("QL_CRDMTR", CompnyCode)
        Dim iGlMstOid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
        Dim iGlDtlOid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
        Dim iDelAcctgOid, iStockAcctgOid As Integer
        Dim iSeq As Int16 = 1
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
        Dim cRate As New ClassRate
        If sStatus = "Approved" Then
            cRate.SetRateValue(CInt(Session("curroid")), Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            Dim sVarErr As String = ""
            If Not IsInterfaceExists("VAR_STOCK_TRANSIT", AppCmpCode.Text) Then
                sVarErr = "VAR_STOCK_TRANSIT"
            End If
            If Not IsInterfaceExists("VAR_STOCK_FG", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_STOCK_FG"
            End If
            If sVarErr <> "" Then
                showMessage(GetInterfaceWarning(sVarErr, "approved"), CompnyName & " - WARNING", 2)
                Exit Sub
            End If
            iDelAcctgOid = GetAcctgOID(GetVarInterface("VAR_STOCK_TRANSIT", AppCmpCode.Text), CompnyCode)
            iStockAcctgOid = GetAcctgOID(GetVarInterface("VAR_STOCK_FG", AppCmpCode.Text), CompnyCode)
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update Shipment
            sSql = "UPDATE QL_trnshipmentitemmst SET shipmentitemno='" & sNo & "', shipmentitemmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE shipmentitemmststatus='In Approval' AND shipmentitemmstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnshipmentitemmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Rejected" Then
                    sSql = "UPDATE QL_trndoitemdtl SET doitemdtlstatus='' WHERE cmpcode='" & AppCmpCode.Text & "' AND doitemdtloid IN (SELECT doitemdtloid FROM QL_trnshipmentitemdtl WHERE cmpcode='" & AppCmpCode.Text & "' AND shipmentitemmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trndoitemmst SET doitemmststatus='Post' WHERE cmpcode='" & AppCmpCode.Text & "' AND doitemmstoid IN (SELECT DISTINCT doitemmstoid FROM QL_trnshipmentitemdtl WHERE cmpcode='" & AppCmpCode.Text & "' AND shipmentitemmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                ElseIf sStatus = "Approved" Then
                    Dim objTable As DataTable = Session("TblDtlShipmentItem")
                    'Update Stock
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        If objTable.Rows(C1)("shipmentonsostatus").ToString <> "" Then
                            sSql = "UPDATE QL_trnsoitemdtl SET soitemdtlstatus='' WHERE cmpcode='" & AppCmpCode.Text & "' AND soitemdtloid=" & objTable.Rows(C1)("soitemdtloid")
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_trnsoitemmst SET soitemmststatus='Approved' WHERE cmpcode='" & AppCmpCode.Text & "' AND soitemmstoid=" & objTable.Rows(C1)("soitemmstoid")
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                        ' Insert QL_conmat
                        sSql = "INSERT INTO QL_conmat (cmpcode, conmtroid, type, typemin, trndate, periodacctg, formaction, formoid, refoid, refname, mtrwhoid, qtyin, qtyout, reason, note, upduser, updtime, valueidr, valueusd) VALUES ('" & AppCmpCode.Text & "', " & iConOid & ", 'SJFG', -1, '" & sDate & "', '" & sPeriod & "', 'QL_trnshipmentitemdtl', " & Session("trnoid") & ", " & objTable.Rows(C1).Item("itemoid") & ", 'FINISH GOOD', " & objTable.Rows(C1).Item("shipmentitemwhoid") & ", 0, " & ToDouble(objTable.Rows(C1).Item("shipmentitemqty")) & ", 'Shipment Finish Good', '" & sNo & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & (ToDouble(objTable.Rows(C1).Item("soitemdtlnetto")) / ToDouble(objTable.Rows(C1).Item("shipmentitemqty"))) * cRate.GetRateMonthlyIDRValue & ", " & (ToDouble(objTable.Rows(C1).Item("soitemdtlnetto")) / ToDouble(objTable.Rows(C1).Item("shipmentitemqty"))) * cRate.GetRateMonthlyUSDValue & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iConOid += 1
                        ' Update QL_crdmtr
                        sSql = "UPDATE QL_crdmtr SET qtyout=qtyout + " & ToDouble(objTable.Rows(C1).Item("shipmentitemqty")) & ", saldoakhir=saldoakhir - " & ToDouble(objTable.Rows(C1).Item("shipmentitemqty")) & ", lasttranstype='QL_trnshipmentitemdtl', lasttransdate='" & sDate & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & AppCmpCode.Text & "' AND refoid=" & objTable.Rows(C1).Item("itemoid") & " AND refname='FINISH GOOD' AND mtrwhoid=" & objTable.Rows(C1).Item("shipmentitemwhoid") & " AND periodacctg='" & sPeriod & "'"
                        xCmd.CommandText = sSql
                        If xCmd.ExecuteNonQuery() = 0 Then
                            ' Insert QL_crdmtr
                            sSql = "INSERT INTO QL_crdmtr (cmpcode, crdmatoid, periodacctg, refoid, refname, mtrwhoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir, lasttranstype, lasttransdate, upduser, updtime, createuser, createdate, closeuser, closingdate) VALUES ('" & AppCmpCode.Text & "', " & iCrdOid & ", '" & sPeriod & "', " & objTable.Rows(C1).Item("itemoid") & ", 'FINISH GOOD', " & objTable.Rows(C1).Item("shipmentitemwhoid") & ", 0, " & ToDouble(objTable.Rows(C1).Item("shipmentitemqty")) & ", 0, 0, 0, " & -ToDouble(objTable.Rows(C1).Item("shipmentitemqty")) & ", 'QL_trnshipmentitemdtl', '" & sDate & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', '1/1/1900')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iCrdOid += 1
                        End If
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iConOid - 1 & " WHERE tablename='QL_CONMAT' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iCrdOid - 1 & " WHERE tablename='QL_CRDMTR' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    ' Get Accounting Value
                    Dim dAmt As Double = 0
                    For C1 As Integer = 0 To objTable.Rows.Count - 1
                        Dim dNet As Double = ToDouble(objTable.Rows(C1)("soitemdtlnetto").ToString)
                        ' Including Discount Header
                        Dim dDisc As Double = 0
                        If objTable.Rows(C1)("soitemmstdisctype").ToString.ToUpper = "P" Then
                            dDisc = (dNet * ToDouble(objTable.Rows(C1)("soitemmstdiscvalue").ToString)) / 100
                        Else
                            dDisc = (dNet * ToDouble(objTable.Rows(C1)("soitemmstdiscvalue").ToString)) / ToDouble(objTable.Rows(C1)("soitemtotalnetto").ToString)
                        End If
                        dNet -= dDisc
                        ' Including Tax
                        Dim dTax As Double = 0
                        If objTable.Rows(C1)("soitemtaxtype").ToString.ToUpper = "TAX" Then
                            dTax = (dNet * ToDouble(objTable.Rows(C1)("soitemtaxamt").ToString)) / 100
                        Else
                            dTax = 0
                        End If
                        dNet += dTax
                        ' Including Other Cost
                        Dim dCost As Double = (dNet * ToDouble(objTable.Rows(C1)("soitemothercost").ToString)) / ToDouble(objTable.Rows(C1)("soitemgrandtotalamt").ToString)
                        dNet += dCost
                        dAmt += dNet
                    Next

                    ' Posting Auto Journal Accounting
                    ' Insert QL_trnglmst
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & AppCmpCode.Text & "', " & iGlMstOid & ", '" & sDate & "', '" & sPeriod & "', 'Shipment FG|No. " & sNo & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Insert QL_trngldtl
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iDelAcctgOid & ", 'D', " & dAmt & ", '" & sNo & "', 'Shipment FG|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dAmt * cRate.GetRateMonthlyIDRValue & ", " & dAmt * cRate.GetRateMonthlyUSDValue & ", 'QL_trnshipmentitemmst " & Session("trnoid") & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1
                    iSeq += 1
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iStockAcctgOid & ", 'C', " & dAmt & ", '" & sNo & "', 'Shipment FG|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dAmt * cRate.GetRateMonthlyIDRValue & ", " & dAmt * cRate.GetRateMonthlyUSDValue & ", 'QL_trnshipmentitemmst " & Session("trnoid") & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "A/R Raw Material Approval"
    Private Sub SetGVListARRM()
        Dim arHeader() As String = {"Draft No.", "A/R Date", "Customer", "A/R Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "custname", "arrawmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "custname", "arrawmstnote", "arrawtotalamt", "arrawmstdiscamt", "arrawvat", "arrawgrandtotalamt", "upduser", "updtime", "custoid", "curroid", "sotype", "arpayment"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT ar.cmpcode, ar.arrawmstoid AS trnoid, ar.arrawno, CONVERT(VARCHAR(10), ar.arrawdate, 101) AS trndate, s.custname, ar.arrawmstnote, (ar.arrawtotalamt - ar.arrawtotaldiscdtl) AS arrawtotalamt, ar.arrawmstdiscamt, ar.arrawtaxamt AS arrawvat, ar.arrawgrandtotal AS arrawgrandtotalamt, apr.approvaloid, apr.requestuser, CONVERT(VARCHAR(10), apr.requestdate, 101) AS requestdate, ar.upduser, ar.updtime, s.custtype AS sotype, g.gendesc AS arpayment, ar.custoid, ar.curroid FROM QL_trnarrawmst ar INNER JOIN QL_mstcust s ON ar.custoid=s.custoid INNER JOIN QL_mstgen g ON g.genoid=ar.arrawpaytypeoid INNER JOIN QL_approval apr ON ar.arrawmstoid=apr.oid AND apr.statusrequest='New' WHERE apr.event='In Approval' AND apr.approvaluser='" & Session("UserID") & "' AND apr.tablename='QL_trnarrawmst' AND ar.arrawmststatus='In Approval' ORDER BY apr.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnarrawmst")
    End Sub

    Private Sub SetGVDataARRM(ByVal dtDataHeader As DataTable)
        Session("sotype") = gvList.SelectedDataKey.Item("sotype").ToString.ToUpper
        Session("arpayment") = gvList.SelectedDataKey.Item("arpayment").ToString
        Session("custoid") = gvList.SelectedDataKey.Item("custoid").ToString
        Session("curroid") = gvList.SelectedDataKey.Item("curroid").ToString
        Session("arrawtotalamt") = gvList.SelectedDataKey.Item("arrawtotalamt").ToString
        Session("arrawmstdiscamt") = gvList.SelectedDataKey.Item("arrawmstdiscamt").ToString
        Session("arrawtotaltax") = gvList.SelectedDataKey.Item("arrawvat").ToString
        Session("arrawgrandtotal") = gvList.SelectedDataKey.Item("arrawgrandtotalamt").ToString
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "A/R Date", "Customer", "A/R Note", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "custname", "arrawmstnote", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arColNameList() As String = {"datacaption", "datasept", "datavalue", "dataempty"}
        Dim arColTypeList() As String = {"System.String", "System.String", "System.String", "System.String"}
        Dim dtDataFooter As DataTable = SetTableDetail("tblDataFooter", arColNameList, arColTypeList)
        Dim arCaptionF() As String = {"Total", "Header Disc.", "Tax", "Grand Total"}
        Dim arValueF() As String = {"arrawtotalamt", "arrawmstdiscamt", "arrawvat", "arrawgrandtotalamt"}
        Dim arTypeF() As String = {"D", "D", "D", "D"}
        Dim arHiddenF() As String = {"N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaptionF.Length - 1
            dr = dtDataFooter.NewRow
            dr("datacaption") = arCaptionF(C1)
            dr("datasept") = ":"
            If arTypeF(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValueF(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValueF(C1)).ToString), 2)
            End If
            dr("dataempty") = ""
            dtDataFooter.Rows.Add(dr)
        Next
        gvDataFooter.DataSource = dtDataFooter
        gvDataFooter.DataBind()
        For C1 As Int16 = 0 To arHiddenF.Length - 1
            If arHiddenF(C1) = "Y" Then
                gvDataFooter.Rows(C1).Visible = False
            End If
        Next
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Font.Bold = True
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).ForeColor = Drawing.Color.Black
        Dim arHeader() As String = {"No.", "Mat. Desc", "Quantity", "Unit", "Price", "Amount", "Disc.", "Netto", "Note"}
        Dim arField() As String = {"arrawdtlseq", "matshortdesc", "arrawqty", "unit", "arrawprice", "arrawdtlamt", "arrawdtldiscamt", "arrawdtlnetto", "arrawdtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT ard.arrawdtlseq, ard.matrawoid, m.matrawlongdesc AS matshortdesc, ard.arrawqty, g.gendesc AS unit, ard.arrawprice, ard.arrawdtlamt, ard.arrawdtldiscamt, ard.arrawdtlnetto, ard.arrawdtlnote FROM QL_trnarrawmst ar INNER JOIN QL_trnarrawdtl ard ON ar.arrawmstoid=ard.arrawmstoid INNER JOIN QL_mstmatraw m ON ard.matrawoid=m.matrawoid INNER JOIN QL_mstgen g ON ard.arrawunitoid=g.genoid WHERE ar.arrawmstoid=" & Session("trnoid") & " ORDER BY ard.arrawdtlseq"
        Session("TblDtlarraw") = ckon.ambiltabel(sSql, "QL_trnarrawdtl")
        gvData.DataSource = Session("TblDtlarraw")
        gvData.DataBind()
    End Sub

    Private Function GenerateARRMNo() As String
        Dim sNo As String = "ARRM-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(arrawno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnarrawmst WHERE cmpcode='" & AppCmpCode.Text & "' AND arrawno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdateARRM(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select A/R Raw Material Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select A/R Raw Material Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim iConAPOid As Int32 = GenerateID("QL_CONAR", CompnyCode)
        Dim iGlMstOid As Int32 = GenerateID("QL_TRNGLMST", CompnyCode)
        Dim iGlDtlOid As Int32 = GenerateID("QL_TRNGLDTL", CompnyCode)
        Dim iARAcctgOid, iTransitAcctgOid, iPPNAcctgOid As Integer
        Dim dDueDate As Date = DateAdd(DateInterval.Day, ToDouble(GetNumericValues(Session("arpayment"))), CDate(Session("trndate")))
        Dim iSeq As Int16 = 1
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
        Dim cRate As New ClassRate
        If sStatus = "Approved" Then
            cRate.SetRateValue(CInt(Session("curroid")), Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            Dim sVarErr As String = ""
            If Not IsInterfaceExists("VAR_STOCK_TRANSIT", AppCmpCode.Text) Then
                sVarErr = "VAR_STOCK_TRANSIT"
            End If
            If Not IsInterfaceExists("VAR_PPN_OUT", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_PPN_OUT"
            End If
            If Not IsInterfaceExists("VAR_AR", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_AR"
            End If
            If sVarErr <> "" Then
                showMessage(GetInterfaceWarning(sVarErr, "approved"), CompnyName & " - WARNING", 2)
                Exit Sub
            End If
            iTransitAcctgOid = GetAcctgOID(GetVarInterface("VAR_STOCK_TRANSIT", AppCmpCode.Text), CompnyCode)
            iPPNAcctgOid = GetAcctgOID(GetVarInterface("VAR_PPN_OUT", AppCmpCode.Text), CompnyCode)
            iARAcctgOid = GetAcctgOID(GetVarInterface("VAR_AR", AppCmpCode.Text), CompnyCode)
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update A/R
            sSql = "UPDATE QL_trnarrawmst SET arrawno='" & sNo & "', arrawmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP"
            If sStatus = "Approved" Then
                sSql &= ", rateoid=" & cRate.GetRateDailyOid & ", arrawratetoidr='" & Left(cRate.GetRateDailyIDRValue.ToString, 50) & "', arrawratetousd='" & Left(cRate.GetRateDailyUSDValue.ToString, 50) & "', rate2oid=" & cRate.GetRateMonthlyOid & ", arrawrate2toidr='" & Left(cRate.GetRateMonthlyIDRValue.ToString, 50) & "', arrawrate2tousd='" & Left(cRate.GetRateMonthlyUSDValue.ToString, 50) & "'"
            End If
            sSql &= " WHERE arrawmststatus='In Approval' AND arrawmstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnarrawmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Rejected" Then
                    sSql = "UPDATE QL_trnshipmentrawdtl SET shipmentrawdtlstatus='' WHERE cmpcode='" & AppCmpCode.Text & "' AND shipmentrawdtloid IN (SELECT shipmentrawdtloid FROM QL_trnarrawdtl WHERE cmpcode='" & AppCmpCode.Text & "' AND arrawmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnshipmentrawmst SET shipmentrawmststatus='Approved' WHERE cmpcode='" & AppCmpCode.Text & "' AND shipmentrawmstoid IN (SELECT shipmentrawmstoid FROM QL_trnarrawdtl WHERE cmpcode='" & AppCmpCode.Text & "' AND arrawmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                ElseIf sStatus = "Approved" Then
                    ' Insert CONAR
                    sSql = "INSERT INTO QL_conar (cmpcode, conaroid, reftype, refoid, payrefoid, custoid, acctgoid, trnarstatus, trnartype, trnardate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans, amtbayar, trnarnote, trnarres1, trnarres2, trnarres3, createuser, createtime, upduser, updtime, amttransidr, amtbayaridr, amttransusd, amtbayarusd) VALUES ('" & AppCmpCode.Text & "', " & iConAPOid & ", 'QL_trnarrawmst', " & Session("trnoid") & ", 0, " & Session("custoid") & ", " & iARAcctgOid & ", 'Post', 'ARRM', '" & sDate & "', '" & sPeriod & "', 0, '1/1/1900', '', 0, '" & dDueDate & "', " & ToDouble(Session("arrawgrandtotal")) & ", 0, '', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(Session("arrawgrandtotal")) * cRate.GetRateMonthlyIDRValue & ", 0, " & ToDouble(Session("arrawgrandtotal")) * cRate.GetRateMonthlyUSDValue & ", 0)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iConAPOid & " WHERE tablename='QL_CONAR' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Auto Jurnal Accounting
                    ' Insert GL MST
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & AppCmpCode.Text & "', " & iGlMstOid & ", '" & sDate & "', '" & sPeriod & "', 'A/R RAW|No. " & sNo & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Insert GL DTL
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iARAcctgOid & ", 'D', " & ToDouble(Session("arrawgrandtotal")) & ", '" & sNo & "', '', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(Session("arrawgrandtotal")) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(Session("arrawgrandtotal")) * cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1
                    iSeq += 1
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iTransitAcctgOid & ", 'C', " & ToDouble(Session("arrawtotalamt")) - ToDouble(Session("arrawmstdiscamt")) & ", '" & sNo & "', '', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & (ToDouble(Session("arrawtotalamt")) - -ToDouble(Session("arrawmstdiscamt"))) * cRate.GetRateMonthlyIDRValue & ", " & (ToDouble(Session("arrawtotalamt")) - -ToDouble(Session("arrawmstdiscamt"))) * cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1
                    iSeq += 1
                    If ToDouble(Session("arrawtotaltax")) > 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iPPNAcctgOid & ", 'C', " & ToDouble(Session("arrawtotaltax")) * cRate.GetRateMonthlyIDRValue & ", '" & sNo & "', '', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(Session("arrawtotaltax")) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(Session("arrawtotaltax")) * cRate.GetRateMonthlyUSDValue & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1
                        iSeq += 1
                    End If
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "A/R General Material Approval"
    Private Sub SetGVListARGM()
        Dim arHeader() As String = {"Draft No.", "A/R Date", "Customer", "A/R Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "custname", "argenmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "custname", "argenmstnote", "argentotalamt", "argenmstdiscamt", "argenvat", "argengrandtotalamt", "upduser", "updtime", "custoid", "curroid", "sotype", "arpayment"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT ar.cmpcode, ar.argenmstoid AS trnoid, ar.argenno, CONVERT(VARCHAR(10), ar.argendate, 101) AS trndate, s.custname, ar.argenmstnote, (ar.argentotalamt - ar.argentotaldiscdtl) AS argentotalamt, ar.argenmstdiscamt, ar.argentaxamt AS argenvat, ar.argengrandtotal AS argengrandtotalamt, apr.approvaloid, apr.requestuser, CONVERT(VARCHAR(10), apr.requestdate, 101) AS requestdate, ar.upduser, ar.updtime, s.custtype AS sotype, g.gendesc AS arpayment, ar.custoid, ar.curroid FROM QL_trnargenmst ar INNER JOIN QL_mstcust s ON ar.custoid=s.custoid INNER JOIN QL_mstgen g ON g.genoid=ar.argenpaytypeoid INNER JOIN QL_approval apr ON ar.argenmstoid=apr.oid AND apr.statusrequest='New' WHERE apr.event='In Approval' AND apr.approvaluser='" & Session("UserID") & "' AND apr.tablename='QL_trnargenmst' AND ar.argenmststatus='In Approval' ORDER BY apr.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnargenmst")
    End Sub

    Private Sub SetGVDataARGM(ByVal dtDataHeader As DataTable)
        Session("sotype") = gvList.SelectedDataKey.Item("sotype").ToString.ToUpper
        Session("arpayment") = gvList.SelectedDataKey.Item("arpayment").ToString
        Session("custoid") = gvList.SelectedDataKey.Item("custoid").ToString
        Session("curroid") = gvList.SelectedDataKey.Item("curroid").ToString
        Session("argentotalamt") = gvList.SelectedDataKey.Item("argentotalamt").ToString
        Session("argenmstdiscamt") = gvList.SelectedDataKey.Item("argenmstdiscamt").ToString
        Session("argentotaltax") = gvList.SelectedDataKey.Item("argenvat").ToString
        Session("argengrandtotal") = gvList.SelectedDataKey.Item("argengrandtotalamt").ToString
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "A/R Date", "Customer", "A/R Note", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "custname", "argenmstnote", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arColNameList() As String = {"datacaption", "datasept", "datavalue", "dataempty"}
        Dim arColTypeList() As String = {"System.String", "System.String", "System.String", "System.String"}
        Dim dtDataFooter As DataTable = SetTableDetail("tblDataFooter", arColNameList, arColTypeList)
        Dim arCaptionF() As String = {"Total", "Header Disc.", "Tax", "Grand Total"}
        Dim arValueF() As String = {"argentotalamt", "argenmstdiscamt", "argenvat", "argengrandtotalamt"}
        Dim arTypeF() As String = {"D", "D", "D", "D"}
        Dim arHiddenF() As String = {"N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaptionF.Length - 1
            dr = dtDataFooter.NewRow
            dr("datacaption") = arCaptionF(C1)
            dr("datasept") = ":"
            If arTypeF(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValueF(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValueF(C1)).ToString), 2)
            End If
            dr("dataempty") = ""
            dtDataFooter.Rows.Add(dr)
        Next
        gvDataFooter.DataSource = dtDataFooter
        gvDataFooter.DataBind()
        For C1 As Int16 = 0 To arHiddenF.Length - 1
            If arHiddenF(C1) = "Y" Then
                gvDataFooter.Rows(C1).Visible = False
            End If
        Next
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Font.Bold = True
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).ForeColor = Drawing.Color.Black
        Dim arHeader() As String = {"No.", "Mat. Desc", "Quantity", "Unit", "Price", "Amount", "Disc.", "Netto", "Note"}
        Dim arField() As String = {"argendtlseq", "matshortdesc", "argenqty", "unit", "argenprice", "argendtlamt", "argendtldiscamt", "argendtlnetto", "argendtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT ard.argendtlseq, ard.matgenoid, m.matgenlongdesc AS matshortdesc, ard.argenqty, g.gendesc AS unit, ard.argenprice, ard.argendtlamt, ard.argendtldiscamt, ard.argendtlnetto, ard.argendtlnote FROM QL_trnargenmst ar INNER JOIN QL_trnargendtl ard ON ar.argenmstoid=ard.argenmstoid INNER JOIN QL_mstmatgen m ON ard.matgenoid=m.matgenoid INNER JOIN QL_mstgen g ON ard.argenunitoid=g.genoid WHERE ar.argenmstoid=" & Session("trnoid") & " ORDER BY ard.argendtlseq"
        Session("TblDtlargen") = ckon.ambiltabel(sSql, "QL_trnargendtl")
        gvData.DataSource = Session("TblDtlargen")
        gvData.DataBind()
    End Sub

    Private Function GenerateARGMNo() As String
        Dim sNo As String = "ARGM-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(argenno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnargenmst WHERE cmpcode='" & AppCmpCode.Text & "' AND argenno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdateARGM(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select A/R General Material Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select A/R General Material Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim iConAPOid As Int32 = GenerateID("QL_CONAR", CompnyCode)
        Dim iGlMstOid As Int32 = GenerateID("QL_TRNGLMST", CompnyCode)
        Dim iGlDtlOid As Int32 = GenerateID("QL_TRNGLDTL", CompnyCode)
        Dim iARAcctgOid, iTransitAcctgOid, iPPNAcctgOid As Integer
        Dim dDueDate As Date = DateAdd(DateInterval.Day, ToDouble(GetNumericValues(Session("arpayment"))), CDate(Session("trndate")))
        Dim iSeq As Int16 = 1
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
        Dim cRate As New ClassRate
        If sStatus = "Approved" Then
            cRate.SetRateValue(CInt(Session("curroid")), Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            Dim sVarErr As String = ""
            If Not IsInterfaceExists("VAR_STOCK_TRANSIT", AppCmpCode.Text) Then
                sVarErr = "VAR_STOCK_TRANSIT"
            End If
            If Not IsInterfaceExists("VAR_PPN_OUT", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_PPN_OUT"
            End If
            If Not IsInterfaceExists("VAR_AR", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_AR"
            End If
            If sVarErr <> "" Then
                showMessage(GetInterfaceWarning(sVarErr, "approved"), CompnyName & " - WARNING", 2)
                Exit Sub
            End If
            iTransitAcctgOid = GetAcctgOID(GetVarInterface("VAR_STOCK_TRANSIT", AppCmpCode.Text), CompnyCode)
            iPPNAcctgOid = GetAcctgOID(GetVarInterface("VAR_PPN_OUT", AppCmpCode.Text), CompnyCode)
            iARAcctgOid = GetAcctgOID(GetVarInterface("VAR_AR", AppCmpCode.Text), CompnyCode)
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update A/R
            sSql = "UPDATE QL_trnargenmst SET argenno='" & sNo & "', argenmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP"
            If sStatus = "Approved" Then
                sSql &= ", rateoid=" & cRate.GetRateDailyOid & ", argenratetoidr='" & Left(cRate.GetRateDailyIDRValue.ToString, 50) & "', argenratetousd='" & Left(cRate.GetRateDailyUSDValue.ToString, 50) & "', rate2oid=" & cRate.GetRateMonthlyOid & ", argenrate2toidr='" & Left(cRate.GetRateMonthlyIDRValue.ToString, 50) & "', argenrate2tousd='" & Left(cRate.GetRateMonthlyUSDValue.ToString, 50) & "'"
            End If
            sSql &= " WHERE argenmststatus='In Approval' AND argenmstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnargenmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Rejected" Then
                    sSql = "UPDATE QL_trnshipmentgendtl SET shipmentgendtlstatus='' WHERE cmpcode='" & AppCmpCode.Text & "' AND shipmentgendtloid IN (SELECT shipmentgendtloid FROM QL_trnargendtl WHERE cmpcode='" & AppCmpCode.Text & "' AND argenmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnshipmentgenmst SET shipmentgenmststatus='Approved' WHERE cmpcode='" & AppCmpCode.Text & "' AND shipmentgenmstoid IN (SELECT shipmentgenmstoid FROM QL_trnargendtl WHERE cmpcode='" & AppCmpCode.Text & "' AND argenmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                ElseIf sStatus = "Approved" Then
                    ' Insert CONAR
                    sSql = "INSERT INTO QL_conar (cmpcode, conaroid, reftype, refoid, payrefoid, custoid, acctgoid, trnarstatus, trnartype, trnardate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans, amtbayar, trnarnote, trnarres1, trnarres2, trnarres3, createuser, createtime, upduser, updtime, amttransidr, amtbayaridr, amttransusd, amtbayarusd) VALUES ('" & AppCmpCode.Text & "', " & iConAPOid & ", 'QL_trnargenmst', " & Session("trnoid") & ", 0, " & Session("custoid") & ", " & iARAcctgOid & ", 'Post', 'ARGM', '" & sDate & "', '" & sPeriod & "', 0, '1/1/1900', '', 0, '" & dDueDate & "', " & ToDouble(Session("argengrandtotal")) & ", 0, '', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(Session("argengrandtotal")) * cRate.GetRateMonthlyIDRValue & ", 0, " & ToDouble(Session("argengrandtotal")) * cRate.GetRateMonthlyUSDValue & ", 0)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iConAPOid & " WHERE tablename='QL_CONAR' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Auto Jurnal Accounting
                    ' Insert GL MST
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & AppCmpCode.Text & "', " & iGlMstOid & ", '" & sDate & "', '" & sPeriod & "', 'A/R GEN|No. " & sNo & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Insert GL DTL
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iARAcctgOid & ", 'D', " & ToDouble(Session("argengrandtotal")) & ", '" & sNo & "', '', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(Session("argengrandtotal")) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(Session("argengrandtotal")) * cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1
                    iSeq += 1
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iTransitAcctgOid & ", 'C', " & ToDouble(Session("argentotalamt")) - ToDouble(Session("argenmstdiscamt")) & ", '" & sNo & "', '', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & (ToDouble(Session("argentotalamt")) - -ToDouble(Session("argenmstdiscamt"))) * cRate.GetRateMonthlyIDRValue & ", " & (ToDouble(Session("argentotalamt")) - -ToDouble(Session("argenmstdiscamt"))) * cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1
                    iSeq += 1
                    If ToDouble(Session("argentotaltax")) > 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iPPNAcctgOid & ", 'C', " & ToDouble(Session("argentotaltax")) * cRate.GetRateMonthlyIDRValue & ", '" & sNo & "', '', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(Session("argentotaltax")) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(Session("argentotaltax")) * cRate.GetRateMonthlyUSDValue & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1
                        iSeq += 1
                    End If
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "A/R Spare Part Approval"
    Private Sub SetGVListarsp()
        Dim arHeader() As String = {"Draft No.", "A/R Date", "Customer", "A/R Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "custname", "arspmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "custname", "arspmstnote", "arsptotalamt", "arspmstdiscamt", "arspvat", "arspgrandtotalamt", "upduser", "updtime", "custoid", "curroid", "sotype", "arpayment"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT ar.cmpcode, ar.arspmstoid AS trnoid, ar.arspno, CONVERT(VARCHAR(10), ar.arspdate, 101) AS trndate, s.custname, ar.arspmstnote, (ar.arsptotalamt - ar.arsptotaldiscdtl) AS arsptotalamt, ar.arspmstdiscamt, ar.arsptaxamt AS arspvat, ar.arspgrandtotal AS arspgrandtotalamt, apr.approvaloid, apr.requestuser, CONVERT(VARCHAR(10), apr.requestdate, 101) AS requestdate, ar.upduser, ar.updtime, s.custtype AS sotype, g.gendesc AS arpayment, ar.custoid, ar.curroid FROM QL_trnarspmst ar INNER JOIN QL_mstcust s ON ar.custoid=s.custoid INNER JOIN QL_mstgen g ON g.genoid=ar.arsppaytypeoid INNER JOIN QL_approval apr ON ar.arspmstoid=apr.oid AND apr.statusrequest='New' WHERE apr.event='In Approval' AND apr.approvaluser='" & Session("UserID") & "' AND apr.tablename='QL_trnarspmst' AND ar.arspmststatus='In Approval' ORDER BY apr.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnarspmst")
    End Sub

    Private Sub SetGVDataarsp(ByVal dtDataHeader As DataTable)
        Session("sotype") = gvList.SelectedDataKey.Item("sotype").ToString.ToUpper
        Session("arpayment") = gvList.SelectedDataKey.Item("arpayment").ToString
        Session("custoid") = gvList.SelectedDataKey.Item("custoid").ToString
        Session("curroid") = gvList.SelectedDataKey.Item("curroid").ToString
        Session("arsptotalamt") = gvList.SelectedDataKey.Item("arsptotalamt").ToString
        Session("arspmstdiscamt") = gvList.SelectedDataKey.Item("arspmstdiscamt").ToString
        Session("arsptotaltax") = gvList.SelectedDataKey.Item("arspvat").ToString
        Session("arspgrandtotal") = gvList.SelectedDataKey.Item("arspgrandtotalamt").ToString
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "A/R Date", "Customer", "A/R Note", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "custname", "arspmstnote", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arColNameList() As String = {"datacaption", "datasept", "datavalue", "dataempty"}
        Dim arColTypeList() As String = {"System.String", "System.String", "System.String", "System.String"}
        Dim dtDataFooter As DataTable = SetTableDetail("tblDataFooter", arColNameList, arColTypeList)
        Dim arCaptionF() As String = {"Total", "Header Disc.", "Tax", "Grand Total"}
        Dim arValueF() As String = {"arsptotalamt", "arspmstdiscamt", "arspvat", "arspgrandtotalamt"}
        Dim arTypeF() As String = {"D", "D", "D", "D"}
        Dim arHiddenF() As String = {"N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaptionF.Length - 1
            dr = dtDataFooter.NewRow
            dr("datacaption") = arCaptionF(C1)
            dr("datasept") = ":"
            If arTypeF(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValueF(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValueF(C1)).ToString), 2)
            End If
            dr("dataempty") = ""
            dtDataFooter.Rows.Add(dr)
        Next
        gvDataFooter.DataSource = dtDataFooter
        gvDataFooter.DataBind()
        For C1 As Int16 = 0 To arHiddenF.Length - 1
            If arHiddenF(C1) = "Y" Then
                gvDataFooter.Rows(C1).Visible = False
            End If
        Next
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Font.Bold = True
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).ForeColor = Drawing.Color.Black
        Dim arHeader() As String = {"No.", "Mat. Desc", "Quantity", "Unit", "Price", "Amount", "Disc.", "Netto", "Note"}
        Dim arField() As String = {"arspdtlseq", "matshortdesc", "arspqty", "unit", "arspprice", "arspdtlamt", "arspdtldiscamt", "arspdtlnetto", "arspdtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT ard.arspdtlseq, ard.sparepartoid, m.sparepartlongdesc AS matshortdesc, ard.arspqty, g.gendesc AS unit, ard.arspprice, ard.arspdtlamt, ard.arspdtldiscamt, ard.arspdtlnetto, ard.arspdtlnote FROM QL_trnarspmst ar INNER JOIN QL_trnarspdtl ard ON ar.arspmstoid=ard.arspmstoid INNER JOIN QL_mstsparepart m ON ard.sparepartoid=m.sparepartoid INNER JOIN QL_mstgen g ON ard.arspunitoid=g.genoid WHERE ar.arspmstoid=" & Session("trnoid") & " ORDER BY ard.arspdtlseq"
        Session("TblDtlarsp") = ckon.ambiltabel(sSql, "QL_trnarspdtl")
        gvData.DataSource = Session("TblDtlarsp")
        gvData.DataBind()
    End Sub

    Private Function GeneratearspNo() As String
        Dim sNo As String = "ARSP-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(arspno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnarspmst WHERE cmpcode='" & AppCmpCode.Text & "' AND arspno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub Updatearsp(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select A/R Spare Part Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select A/R Spare Part Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim iConAPOid As Int32 = GenerateID("QL_CONAR", CompnyCode)
        Dim iGlMstOid As Int32 = GenerateID("QL_TRNGLMST", CompnyCode)
        Dim iGlDtlOid As Int32 = GenerateID("QL_TRNGLDTL", CompnyCode)
        Dim iARAcctgOid, iTransitAcctgOid, iPPNAcctgOid As Integer
        Dim dDueDate As Date = DateAdd(DateInterval.Day, ToDouble(GetNumericValues(Session("arpayment"))), CDate(Session("trndate")))
        Dim iSeq As Int16 = 1
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
        Dim cRate As New ClassRate
        If sStatus = "Approved" Then
            cRate.SetRateValue(CInt(Session("curroid")), Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            Dim sVarErr As String = ""
            If Not IsInterfaceExists("VAR_STOCK_TRANSIT", AppCmpCode.Text) Then
                sVarErr = "VAR_STOCK_TRANSIT"
            End If
            If Not IsInterfaceExists("VAR_PPN_OUT", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_PPN_OUT"
            End If
            If Not IsInterfaceExists("VAR_AR", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_AR"
            End If
            If sVarErr <> "" Then
                showMessage(GetInterfaceWarning(sVarErr, "approved"), CompnyName & " - WARNING", 2)
                Exit Sub
            End If
            iTransitAcctgOid = GetAcctgOID(GetVarInterface("VAR_STOCK_TRANSIT", AppCmpCode.Text), CompnyCode)
            iPPNAcctgOid = GetAcctgOID(GetVarInterface("VAR_PPN_OUT", AppCmpCode.Text), CompnyCode)
            iARAcctgOid = GetAcctgOID(GetVarInterface("VAR_AR", AppCmpCode.Text), CompnyCode)
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update A/R
            sSql = "UPDATE QL_trnarspmst SET arspno='" & sNo & "', arspmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP"
            If sStatus = "Approved" Then
                sSql &= ", rateoid=" & cRate.GetRateDailyOid & ", arspratetoidr='" & Left(cRate.GetRateDailyIDRValue.ToString, 50) & "', arspratetousd='" & Left(cRate.GetRateDailyUSDValue.ToString, 50) & "', rate2oid=" & cRate.GetRateMonthlyOid & ", arsprate2toidr='" & Left(cRate.GetRateMonthlyIDRValue.ToString, 50) & "', arsprate2tousd='" & Left(cRate.GetRateMonthlyUSDValue.ToString, 50) & "'"
            End If
            sSql &= " WHERE arspmststatus='In Approval' AND arspmstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnarspmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Rejected" Then
                    sSql = "UPDATE QL_trnshipmentspdtl SET shipmentspdtlstatus='' WHERE cmpcode='" & AppCmpCode.Text & "' AND shipmentspdtloid IN (SELECT shipmentspdtloid FROM QL_trnarspdtl WHERE cmpcode='" & AppCmpCode.Text & "' AND arspmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnshipmentspmst SET shipmentspmststatus='Approved' WHERE cmpcode='" & AppCmpCode.Text & "' AND shipmentspmstoid IN (SELECT shipmentspmstoid FROM QL_trnarspdtl WHERE cmpcode='" & AppCmpCode.Text & "' AND arspmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                ElseIf sStatus = "Approved" Then
                    ' Insert CONAR
                    sSql = "INSERT INTO QL_conar (cmpcode, conaroid, reftype, refoid, payrefoid, custoid, acctgoid, trnarstatus, trnartype, trnardate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans, amtbayar, trnarnote, trnarres1, trnarres2, trnarres3, createuser, createtime, upduser, updtime, amttransidr, amtbayaridr, amttransusd, amtbayarusd) VALUES ('" & AppCmpCode.Text & "', " & iConAPOid & ", 'QL_trnarspmst', " & Session("trnoid") & ", 0, " & Session("custoid") & ", " & iARAcctgOid & ", 'Post', 'ARSP', '" & sDate & "', '" & sPeriod & "', 0, '1/1/1900', '', 0, '" & dDueDate & "', " & ToDouble(Session("arspgrandtotal")) & ", 0, '', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(Session("arspgrandtotal")) * cRate.GetRateMonthlyIDRValue & ", 0, " & ToDouble(Session("arspgrandtotal")) * cRate.GetRateMonthlyUSDValue & ", 0)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iConAPOid & " WHERE tablename='QL_CONAR' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Auto Jurnal Accounting
                    ' Insert GL MST
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & AppCmpCode.Text & "', " & iGlMstOid & ", '" & sDate & "', '" & sPeriod & "', 'A/R SP|No. " & sNo & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Insert GL DTL
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iARAcctgOid & ", 'D', " & ToDouble(Session("arspgrandtotal")) & ", '" & sNo & "', '', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(Session("arspgrandtotal")) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(Session("arspgrandtotal")) * cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1
                    iSeq += 1
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iTransitAcctgOid & ", 'C', " & ToDouble(Session("arsptotalamt")) - ToDouble(Session("arspmstdiscamt")) & ", '" & sNo & "', '', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & (ToDouble(Session("arsptotalamt")) - -ToDouble(Session("arspmstdiscamt"))) * cRate.GetRateMonthlyIDRValue & ", " & (ToDouble(Session("arsptotalamt")) - -ToDouble(Session("arspmstdiscamt"))) * cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1
                    iSeq += 1
                    If ToDouble(Session("arsptotaltax")) > 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iPPNAcctgOid & ", 'C', " & ToDouble(Session("arsptotaltax")) * cRate.GetRateMonthlyIDRValue & ", '" & sNo & "', '', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(Session("arsptotaltax")) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(Session("arsptotaltax")) * cRate.GetRateMonthlyUSDValue & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1
                        iSeq += 1
                    End If
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "A/R Finish Good Approval"
    Private Sub SetGVListARITEM()
        Dim arHeader() As String = {"Draft No.", "A/R Date", "Customer", "A/R Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "custname", "aritemmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "custname", "aritemmstnote", "aritemtotalamt", "aritemmstdiscamt", "aritemvat", "aritemgrandtotalamt", "upduser", "updtime", "custoid", "curroid", "sotype", "arpayment"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT ar.cmpcode, ar.aritemmstoid AS trnoid, ar.aritemno, CONVERT(VARCHAR(10), ar.aritemdate, 101) AS trndate, s.custname, ar.aritemmstnote, (ar.aritemtotalamt - ar.aritemtotaldiscdtl) AS aritemtotalamt, ar.aritemmstdiscamt, ar.aritemtaxamt AS aritemvat, ar.aritemgrandtotal AS aritemgrandtotalamt, apr.approvaloid, apr.requestuser, CONVERT(VARCHAR(10), apr.requestdate, 101) AS requestdate, ar.upduser, ar.updtime, s.custtype AS sotype, g.gendesc AS arpayment, ar.custoid, ar.curroid FROM QL_trnaritemmst ar INNER JOIN QL_mstcust s ON ar.custoid=s.custoid INNER JOIN QL_mstgen g ON g.genoid=ar.aritempaytypeoid INNER JOIN QL_approval apr ON ar.aritemmstoid=apr.oid AND apr.statusrequest='New' WHERE apr.event='In Approval' AND apr.approvaluser='" & Session("UserID") & "' AND apr.tablename='QL_trnaritemmst' AND ar.aritemmststatus='In Approval' ORDER BY apr.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnaritemmst")
    End Sub

    Private Sub SetGVDataARITEM(ByVal dtDataHeader As DataTable)
        Session("sotype") = gvList.SelectedDataKey.Item("sotype").ToString.ToUpper
        Session("arpayment") = gvList.SelectedDataKey.Item("arpayment").ToString
        Session("custoid") = gvList.SelectedDataKey.Item("custoid").ToString
        Session("curroid") = gvList.SelectedDataKey.Item("curroid").ToString
        Session("aritemtotalamt") = gvList.SelectedDataKey.Item("aritemtotalamt").ToString
        Session("aritemmstdiscamt") = gvList.SelectedDataKey.Item("aritemmstdiscamt").ToString
        Session("aritemtotaltax") = gvList.SelectedDataKey.Item("aritemvat").ToString
        Session("aritemgrandtotal") = gvList.SelectedDataKey.Item("aritemgrandtotalamt").ToString
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "A/R Date", "Customer", "A/R Note", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "custname", "aritemmstnote", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arColNameList() As String = {"datacaption", "datasept", "datavalue", "dataempty"}
        Dim arColTypeList() As String = {"System.String", "System.String", "System.String", "System.String"}
        Dim dtDataFooter As DataTable = SetTableDetail("tblDataFooter", arColNameList, arColTypeList)
        Dim arCaptionF() As String = {"Total", "Header Disc.", "Tax", "Grand Total"}
        Dim arValueF() As String = {"aritemtotalamt", "aritemmstdiscamt", "aritemvat", "aritemgrandtotalamt"}
        Dim arTypeF() As String = {"D", "D", "D", "D"}
        Dim arHiddenF() As String = {"N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaptionF.Length - 1
            dr = dtDataFooter.NewRow
            dr("datacaption") = arCaptionF(C1)
            dr("datasept") = ":"
            If arTypeF(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValueF(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValueF(C1)).ToString), 2)
            End If
            dr("dataempty") = ""
            dtDataFooter.Rows.Add(dr)
        Next
        gvDataFooter.DataSource = dtDataFooter
        gvDataFooter.DataBind()
        For C1 As Int16 = 0 To arHiddenF.Length - 1
            If arHiddenF(C1) = "Y" Then
                gvDataFooter.Rows(C1).Visible = False
            End If
        Next
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Font.Bold = True
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).ForeColor = Drawing.Color.Black
        Dim arHeader() As String = {"No.", "Mat. Desc", "Quantity", "Unit", "Price", "Amount", "Disc.", "Netto", "Note"}
        Dim arField() As String = {"aritemdtlseq", "itemshortdesc", "aritemqty", "unit", "aritemprice", "aritemdtlamt", "aritemdtldiscamt", "aritemdtlnetto", "aritemdtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT ard.aritemdtlseq, ard.itemoid, m.itemlongdesc AS itemshortdesc, ard.aritemqty, g.gendesc AS unit, ard.aritemprice, ard.aritemdtlamt, ard.aritemdtldiscamt, ard.aritemdtlnetto, ard.aritemdtlnote FROM QL_trnaritemmst ar INNER JOIN QL_trnaritemdtl ard ON ar.aritemmstoid=ard.aritemmstoid INNER JOIN QL_mstitem m ON ard.itemoid=m.itemoid INNER JOIN QL_mstgen g ON ard.aritemunitoid=g.genoid WHERE ar.aritemmstoid=" & Session("trnoid") & " ORDER BY ard.aritemdtlseq"
        Session("TblDtlARItem") = ckon.ambiltabel(sSql, "QL_trnaritemdtl")
        gvData.DataSource = Session("TblDtlARItem")
        gvData.DataBind()
    End Sub

    Private Function GenerateARITEMNo() As String
        Dim sNo As String = "ARFG-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(aritemno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnaritemmst WHERE cmpcode='" & AppCmpCode.Text & "' AND aritemno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdateARITEM(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select A/R Finish Good Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select A/R Finish Good Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim iConAPOid As Int32 = GenerateID("QL_CONAR", CompnyCode)
        Dim iGlMstOid As Int32 = GenerateID("QL_TRNGLMST", CompnyCode)
        Dim iGlDtlOid As Int32 = GenerateID("QL_TRNGLDTL", CompnyCode)
        Dim iARAcctgOid, iTransitAcctgOid, iPPNAcctgOid As Integer
        Dim dDueDate As Date = DateAdd(DateInterval.Day, ToDouble(GetNumericValues(Session("arpayment"))), CDate(Session("trndate")))
        Dim iSeq As Int16 = 1
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
        Dim cRate As New ClassRate
        If sStatus = "Approved" Then
            cRate.SetRateValue(CInt(Session("curroid")), Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            Dim sVarErr As String = ""
            If Not IsInterfaceExists("VAR_STOCK_TRANSIT", AppCmpCode.Text) Then
                sVarErr = "VAR_STOCK_TRANSIT"
            End If
            If Not IsInterfaceExists("VAR_PPN_OUT", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_PPN_OUT"
            End If
            If Not IsInterfaceExists("VAR_AR", AppCmpCode.Text) Then
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_AR"
            End If
            If sVarErr <> "" Then
                showMessage(GetInterfaceWarning(sVarErr, "approved"), CompnyName & " - WARNING", 2)
                Exit Sub
            End If
            iTransitAcctgOid = GetAcctgOID(GetVarInterface("VAR_STOCK_TRANSIT", AppCmpCode.Text), CompnyCode)
            iPPNAcctgOid = GetAcctgOID(GetVarInterface("VAR_PPN_OUT", AppCmpCode.Text), CompnyCode)
            iARAcctgOid = GetAcctgOID(GetVarInterface("VAR_AR", AppCmpCode.Text), CompnyCode)
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update A/R
            sSql = "UPDATE QL_trnaritemmst SET aritemno='" & sNo & "', aritemmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP"
            If sStatus = "Approved" Then
                sSql &= ", rateoid=" & cRate.GetRateDailyOid & ", aritemratetoidr='" & Left(cRate.GetRateDailyIDRValue.ToString, 50) & "', aritemratetousd='" & Left(cRate.GetRateDailyUSDValue.ToString, 50) & "', rate2oid=" & cRate.GetRateMonthlyOid & ", aritemrate2toidr='" & Left(cRate.GetRateMonthlyIDRValue.ToString, 50) & "', aritemrate2tousd='" & Left(cRate.GetRateMonthlyUSDValue.ToString, 50) & "'"
            End If
            sSql &= " WHERE aritemmststatus='In Approval' AND aritemmstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnaritemmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Rejected" Then
                    sSql = "UPDATE QL_trnshipmentitemdtl SET shipmentitemdtlstatus='' WHERE cmpcode='" & AppCmpCode.Text & "' AND shipmentitemdtloid IN (SELECT shipmentitemdtloid FROM QL_trnaritemdtl WHERE cmpcode='" & AppCmpCode.Text & "' AND aritemmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnshipmentitemmst SET shipmentitemmststatus='Approved' WHERE cmpcode='" & AppCmpCode.Text & "' AND shipmentitemmstoid IN (SELECT shipmentitemmstoid FROM QL_trnaritemdtl WHERE cmpcode='" & AppCmpCode.Text & "' AND aritemmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                ElseIf sStatus = "Approved" Then
                    ' Insert CONAR
                    sSql = "INSERT INTO QL_conar (cmpcode, conaroid, reftype, refoid, payrefoid, custoid, acctgoid, trnarstatus, trnartype, trnardate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans, amtbayar, trnarnote, trnarres1, trnarres2, trnarres3, createuser, createtime, upduser, updtime, amttransidr, amtbayaridr, amttransusd, amtbayarusd) VALUES ('" & AppCmpCode.Text & "', " & iConAPOid & ", 'QL_trnaritemmst', " & Session("trnoid") & ", 0, " & Session("custoid") & ", " & iARAcctgOid & ", 'Post', 'ARFG', '" & sDate & "', '" & sPeriod & "', 0, '1/1/1900', '', 0, '" & dDueDate & "', " & ToDouble(Session("aritemgrandtotal")) & ", 0, '', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(Session("aritemgrandtotal")) * cRate.GetRateMonthlyIDRValue & ", 0, " & ToDouble(Session("aritemgrandtotal")) * cRate.GetRateMonthlyUSDValue & ", 0)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iConAPOid & " WHERE tablename='QL_CONAR' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Auto Jurnal Accounting
                    ' Insert GL MST
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & AppCmpCode.Text & "', " & iGlMstOid & ", '" & sDate & "', '" & sPeriod & "', 'A/R FG|No. " & sNo & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Insert GL DTL
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iARAcctgOid & ", 'D', " & ToDouble(Session("aritemgrandtotal")) & ", '" & sNo & "', '', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(Session("aritemgrandtotal")) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(Session("aritemgrandtotal")) * cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1
                    iSeq += 1
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iTransitAcctgOid & ", 'C', " & ToDouble(Session("aritemtotalamt")) - ToDouble(Session("aritemmstdiscamt")) & ", '" & sNo & "', '', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & (ToDouble(Session("aritemtotalamt")) - -ToDouble(Session("aritemmstdiscamt"))) * cRate.GetRateMonthlyIDRValue & ", " & (ToDouble(Session("aritemtotalamt")) - -ToDouble(Session("aritemmstdiscamt"))) * cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1
                    iSeq += 1
                    If ToDouble(Session("aritemtotaltax")) > 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iPPNAcctgOid & ", 'C', " & ToDouble(Session("aritemtotaltax")) * cRate.GetRateMonthlyIDRValue & ", '" & sNo & "', '', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(Session("aritemtotaltax")) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(Session("aritemtotaltax")) * cRate.GetRateMonthlyUSDValue & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1
                        iSeq += 1
                    End If
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "A/R Fixed Assets Approval"
    Private Sub SetGVListARAsset()
        Dim arHeader() As String = {"Draft No.", "AR Date", "Customer", "AR Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "custname", "arassetmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "custname", "arassetmstnote", "arassettotalamt", "arassetmstdiscamt", "arassetvat", "arassetothercost", "arassetgrandtotalamt"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT ar.cmpcode, ar.arassetmstoid AS trnoid, ar.arassetno, CONVERT(VARCHAR(10), ar.arassetdate, 101) AS trndate, s.custname, ar.arassetmstnote, (ar.arassettotalamt - ar.arassettotaldiscdtl) AS arassettotalamt, ar.arassetmstdiscamt, ar.arassetvat, (ar.arassetdeliverycost + ar.arassetothercost) AS arassetothercost, ar.arassetgrandtotalamt, apr.approvaloid, apr.requestuser, CONVERT(VARCHAR(10), apr.requestdate, 101) AS requestdate FROM QL_trnarassetmst ar INNER JOIN QL_mstcust s ON ar.custoid=s.custoid INNER JOIN QL_approval apr ON ar.arassetmstoid=apr.oid AND apr.statusrequest='New' WHERE apr.event='In Approval' AND apr.approvaluser='" & Session("UserID") & "' AND apr.tablename='QL_trnarassetmst' AND ar.argenmststatus='In Approval' ORDER BY apr.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnarassetmst")
    End Sub

    Private Sub SetGVDataARAsset(ByVal dtDataHeader As DataTable)
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "AR Date", "Customer", "AR Note"}
        Dim arValue() As String = {"trnoid", "trndate", "custname", "arassetmstnote"}
        Dim arType() As String = {"S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arColNameList() As String = {"datacaption", "datasept", "datavalue", "dataempty"}
        Dim arColTypeList() As String = {"System.String", "System.String", "System.String", "System.String"}
        Dim dtDataFooter As DataTable = SetTableDetail("tblDataFooter", arColNameList, arColTypeList)
        Dim arCaptionF() As String = {"Total", "Header Disc.", "Tax", "Other Cost", "Grand Total"}
        Dim arValueF() As String = {"arassettotalamt", "arassetmstdiscamt", "arassetvat", "arassetothercost", "arassetgrandtotalamt"}
        Dim arTypeF() As String = {"D", "D", "D", "D", "D"}
        Dim arHiddenF() As String = {"N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaptionF.Length - 1
            dr = dtDataFooter.NewRow
            dr("datacaption") = arCaptionF(C1)
            dr("datasept") = ":"
            If arTypeF(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValueF(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValueF(C1)).ToString), 2)
            End If
            dr("dataempty") = ""
            dtDataFooter.Rows.Add(dr)
        Next
        gvDataFooter.DataSource = dtDataFooter
        gvDataFooter.DataBind()
        For C1 As Int16 = 0 To arHiddenF.Length - 1
            If arHiddenF(C1) = "Y" Then
                gvDataFooter.Rows(C1).Visible = False
            End If
        Next
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Font.Bold = True
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(0).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(1).ForeColor = Drawing.Color.Black
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).Font.Size = System.Web.UI.WebControls.FontUnit.Small
        gvDataFooter.Rows(gvDataFooter.Rows.Count - 1).Cells(2).ForeColor = Drawing.Color.Black
        Dim arHeader() As String = {"No.", "Assets No.", "Assets Desc", "Quantity", "Unit", "Price", "Amount", "Disc.", "Netto", "Note"}
        Dim arField() As String = {"arassetdtlseq", "assetno", "assetlongdesc", "arassetqty", "unit", "arassetprice", "arassetdtlamt", "arassetdtldiscamt", "arassetdtlnetto", "arassetdtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT ard.arassetdtlseq, ard.assetmstoid, fa.assetno, (CASE fa.reftype WHEN 'General' THEN (SELECT x.matgenlongdesc FROM QL_mstmatgen x WHERE x.matgenoid=fa.refoid) WHEN 'Spare Part' THEN (SELECT x.sparepartlongdesc FROM QL_mstsparepart x WHERE x.sparepartoid=fa.refoid) ELSE '' END) AS assetlongdesc, ard.arassetqty, g.gendesc AS unit, ard.arassetprice, ard.arassetdtlamt, ard.arassetdtldiscamt, ard.arassetdtlnetto, ard.arassetdtlnote FROM QL_trnarassetmst ar INNER JOIN QL_trnarassetdtl ard ON ar.arassetmstoid=ard.arassetmstoid INNER JOIN QL_assetmst fa ON fa.assetmstoid=ard.assetmstoid INNER JOIN QL_mstgen g ON ard.arassetunitoid=g.genoid WHERE ar.arassetmstoid=" & Session("trnoid") & " ORDER BY ard.arassetdtlseq"
        Session("TblDtlARAsset") = ckon.ambiltabel(sSql, "QL_trnarassetdtl")
        gvData.DataSource = Session("TblDtlARAsset")
        gvData.DataBind()
    End Sub

    Private Function GenerateARAssetNo() As String
        Dim sNo As String = "ARFA-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(arassetno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnarassetmst WHERE cmpcode='" & AppCmpCode.Text & "' AND arassetno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdateARAsset(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select A/R Fixed Assets Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select A/R Fixed Assets Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim iCrdPriceOid As Integer = GenerateID("QL_CRDPRICE", CompnyCode)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update A/R
            sSql = "UPDATE QL_trnarassetmst SET arassetno='" & sNo & "', arassetmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE arassetmststatus='In Approval' AND arassetmstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnarassetmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Rejected" Then
                    sSql = "UPDATE QL_trnshipmentassetdtl SET doassetdtlstatus='' WHERE cmpcode='" & AppCmpCode.Text & "' AND doassetdtloid IN (SELECT doassetdtloid FROM QL_trnarassetdtl WHERE cmpcode='" & AppCmpCode.Text & "' AND arassetmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnshipmentassetmst SET doassetmststatus='Approved' WHERE cmpcode='" & AppCmpCode.Text & "' AND doassetmstoid IN (SELECT doassetmstoid FROM QL_trnarassetdtl WHERE cmpcode='" & AppCmpCode.Text & "' AND arassetmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "Sales Return Approval"
    Private Sub SetGVListSretRM()
        Dim arHeader() As String = {"Draft No.", "Return Date", "Customer", "Return Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "custname", "sretmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "custname", "sretmstnote", "curroid", "custoid", "dpp", "srettaxamt", "sretamtnetto", "taxtype", "trnjualmstoid", "reason", "reason2", "reason3"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT sret.cmpcode, sret.sretmstoid AS trnoid, sret.sretno, CONVERT(VARCHAR(10), sret.sretdate, 101) AS trndate, c.custname, c.custoid, sret.curroid, sret.sretmstnote, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, (case when jm.trntaxtype='INC' then sret.sretamtnetto/" & GetNewTaxValueInclude(GetServerTime()) & " else sret.sretamt+sret.srettaxamt end) AS dpp, sret.srettaxamt, sret.sretamtnetto, jm.trntaxtype AS taxtype, sret.trnjualmstoid, sret.sretmstres2 reason, (CASE sretmstres2 WHEN 'ITEM' THEN 'KEMBALI BARANG' WHEN 'DP' THEN 'DIJADIKAN DP' ELSE 'KEMBALI CASH' END) AS reason2, ISNULL(sretmstres3,'') AS reason3 FROM QL_trnsalesreturmst sret INNER JOIN QL_approval ap ON sret.sretmstoid=ap.oid AND ap.statusrequest='New' INNER JOIN QL_mstcust c ON c.custoid=sret.custoid LEFT JOIN QL_trnjualmst jm ON jm.trnjualmstoid=sret.trnjualmstoid WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_trnsalesreturmst' AND sret.sretmststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnsalesreturmst")
    End Sub

    Private Sub SetGVDataSretRM(ByVal dtDataHeader As DataTable)
        Session("returdate") = gvList.SelectedDataKey.Item("trndate").ToString
        Session("curroid") = gvList.SelectedDataKey.Item("curroid").ToString
        Session("custoid") = gvList.SelectedDataKey.Item("custoid").ToString
        Session("custname") = gvList.SelectedDataKey.Item("custname").ToString
        Session("dpp") = ToDouble(gvList.SelectedDataKey.Item("dpp").ToString)
        Session("taxtype") = gvList.SelectedDataKey.Item("taxtype").ToString
        Session("tax") = ToDouble(gvList.SelectedDataKey.Item("srettaxamt").ToString)
        Session("netto") = ToDouble(gvList.SelectedDataKey.Item("sretamtnetto").ToString)
        Session("trnjualmstoid") = gvList.SelectedDataKey.Item("trnjualmstoid").ToString
        Session("reason") = gvList.SelectedDataKey.Item("reason").ToString
        Session("reason3") = gvList.SelectedDataKey.Item("reason3").ToString
        lblreason.Visible = True : lblSeptReason.Visible = True : ActionReason.Visible = True
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "Return Date", "Customer", "Return Note", "Type"}
        Dim arValue() As String = {"trnoid", "trndate", "custname", "sretmstnote", "reason2"}
        Dim arType() As String = {"S", "S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arHeader() As String = {"No.", "Mat. Code", "Mat. Desc", "Quantity", "Unit", "Warehouse", "Note"}
        Dim arField() As String = {"sretdtlseq", "itemcode", "itemdesc", "sretqty", "unit", "warehouse", "sretdtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT sretd.sretdtlseq, sretd.sretdtloid, m.itemoid AS matoid, m.itemcode, m.itemLongDescription AS itemdesc, sretd.sretqty, g.gendesc AS unit, g2.gendesc AS warehouse, sretd.sretdtlnote, sretd.sretwhoid, sretd.sretdtlamt AS dtlamt, g5.gendesc AS refname, sretvalueidr, sretvalueusd, ISNULL(jd.trnjualdtlqty,0) AS trnjualdtlqty, ISNULL(jd.trnjualdtloid,0) AS trnjualdtloid, sretd.qtyunitkecil, sretd.qtyunitbesar, sretd.valueidr, sretd.valueusd, ISNULL(jd.dodtloid,0) AS dodtloid, ISNULL(dod.doqty_unitkecil, 0) AS doqty_unitkecil, ISNULL(dod.doqty_unitbesar,0) AS doqty_unitbesar FROM QL_trnsalesreturmst sret INNER JOIN QL_trnsalesreturdtl sretd ON sret.sretmstoid=sretd.sretmstoid INNER JOIN QL_mstitem m ON sretd.matoid=m.itemoid INNER JOIN QL_mstgen g ON sretd.sretunitoid=g.genoid INNER JOIN QL_mstgen g2 ON sretd.sretwhoid=g2.genoid LEFT JOIN QL_trnjualdtl jd ON jd.trnjualdtloid=sretd.trnjualdtloid INNER JOIN QL_mstgen g5 ON g5.genoid=m.itemgroupoid LEFT JOIN QL_trndodtl dod ON dod.dodtloid=jd.dodtloid WHERE sret.sretmstoid=" & Session("trnoid") & " ORDER BY sretd.sretdtlseq"
        Session("TblDtlSretRaw") = ckon.ambiltabel(sSql, "QL_trnsalesreturdtl")
        gvData.DataSource = Session("TblDtlSretRaw")
        gvData.DataBind()
    End Sub

    Private Function GenerateSretRMNo() As String
        'Dim SRtype As String = GetStrData("SELECT sretmstres1 FROM QL_trnsalesreturmst WHERE sretmstoid=" & Session("trnoid") & "")
        Dim SRtype As String = ""
        If Session("taxtype") = "INC" Then
            SRtype = "B"
        Else
            SRtype = "A"
        End If
        Dim sNo As String = "SRT" & SRtype & "-" & Format(CDate(Session("trndate")), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(sretno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnsalesreturmst WHERE cmpcode='" & AppCmpCode.Text & "' AND sretno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub GenerateDPARno()
        Dim sNod As String = "DPAR-" & Format(CDate(Session("trndate")), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(dparno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trndpar WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dparno LIKE '%" & sNod & "%'"
        Session("sNodp") = GenNumberString(sNod, "", GetStrData(sSql), DefaultFormatCounter)
    End Sub

    Private Sub GenerateCBARno(ByVal kas As Integer)
        Dim sNoc As String = "BKM-" & Format(CDate(Session("trndate")), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankno LIKE '%" & sNoc & "%' AND acctgoid=" & kas
        Session("sNocb") = GenNumberString(sNoc, "", 1, DefaultFormatCounter)
    End Sub

    Private Sub GenerateCBARno2(ByVal kas As Integer)
        Dim sNoc As String = "BKK-" & Format(CDate(Session("trndate")), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankno LIKE '%" & sNoc & "%' AND acctgoid=" & kas
        Session("sNocb") = GenNumberString(sNoc, "", 1, DefaultFormatCounter)
    End Sub

    Private Sub UpdateSretRM(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select Sales Return Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select Sales Return Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        If sStatus = "Rejected" Or sStatus = "Revised" Then
            If ActionReason.Text = "" Then
                showMessage("Please Fill " & sStatus & " Reason First!", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim conmtroid As Integer = GenerateID("QL_CONSTOCK", CompnyCode)
        Dim crdmatoid As Integer = GenerateID("QL_CRDSTOCK", CompnyCode)
        Dim conaroid As Integer = GenerateID("QL_CONAR", CompnyCode)
        Dim iStockValOid As Integer = GenerateID("QL_STOCKVALUE", CompnyCode)
        Dim iGlMstOid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
        Dim iGlDtlOid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
        Dim cashbankoid As Integer = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
        Dim cashbankgloid As Integer = GenerateID("QL_TRNCASHBANKGL", CompnyCode)
        Dim dparoid As Integer = GenerateID("QL_TRNDPAR", CompnyCode)
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
        Dim returAcctg, ppnAcctg, piutangAcctg, persediaanAcctg, hppAcctg, iKasAcctgOid, iDPAcctgOid, iCustGroupOid, iRoundAcctgOid As Integer
        Dim AmtIDR, AmtUSD, dAmtIDR, dAmtUSD, cekBayar As Double
        Dim somstALL As String = ""
        Dim iSeq As Int16 = 1 : Dim iSeq2 As Int16 = 1
        Dim cRate As New ClassRate()
        If sStatus = "Approved" Then
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If isPeriodClosed(AppCmpCode.Text, sPeriod) Then
                showMessage("Cannot continue this transaction anymore because the period : " & MonthName(CInt(sPeriod.Substring(4, 2))) & " " & sPeriod.Substring(0, 4) & " have been closed.", CompnyName & " - WARNING", 2)
                Exit Sub
            End If
            If isPeriodAcctgClosed(AppCmpCode.Text, sPeriod) Then
                showMessage("Cannot continue this transaction anymore because the period : " & MonthName(CInt(sPeriod.Substring(4, 2))) & " " & sPeriod.Substring(0, 4) & " have been closed.", CompnyName & " - WARNING", 2)
                Exit Sub
            End If
            piutangAcctg = GetAcctgOID(GetVarInterface("VAR_AR", AppCmpCode.Text), CompnyCode)
            returAcctg = GetAcctgOID(GetVarInterface("VAR_RETUR_JUAL", AppCmpCode.Text), CompnyCode)
            ppnAcctg = GetAcctgOID(GetVarInterface("VAR_PPN_OUT", AppCmpCode.Text), CompnyCode)
            hppAcctg = GetAcctgOID(GetVarInterface("VAR_HPP", AppCmpCode.Text), CompnyCode)
            iKasAcctgOid = GetAcctgOID(GetVarInterface("VAR_KAS_RETUR", AppCmpCode.Text), CompnyCode)
            iDPAcctgOid = GetAcctgOID(GetVarInterface("VAR_DPAR_RETUR", AppCmpCode.Text), CompnyCode)
            iRoundAcctgOid = GetAcctgOID(GetVarInterface("VAR_ROUNDING", AppCmpCode.Text), CompnyCode)
            cRate.SetRateValue(ToInteger(Session("curroid")), Session("returdate"))

            'Set Group Item
            sSql = "select sum(rd.valueidr*qtyunitkecil) AS amtidr, sum(rd.valueusd*qtyunitkecil) AS amtusd, i.itemGroup AS dogroup, (select genother1 from QL_mstgen where gengroup='GROUPITEM' AND gencode=i.itemGroup) AS acctgoid from QL_trnsalesreturmst rm inner join QL_trnsalesreturdtl rd on rd.sretmstoid=rm.sretmstoid inner join QL_mstitem i ON i.itemoid=rd.matoid WHERE rm.cmpcode='" & AppCmpCode.Text & "' AND rm.sretmstoid=" & Session("trnoid") & " group by itemgroup"
            Dim tbl As DataTable = ckon.ambiltabel(sSql, "QL_trnsalesreturmst")
            Session("TblGroup") = tbl
        End If

        If sStatus = "Approved" Then
            'Cek Pembayaran Lunas atau Belum
            sSql = "select (select ISNULL(SUM(con.amttrans),0) from QL_conar con  where con.reftype='QL_trnjualmst' and con.refoid=jm.trnjualmstoid and con.custoid=jm.custoid and payrefoid=0) - (select ISNULL(SUM(con.amtbayar),0) from QL_conar con LEFT JOIN QL_trnpayar pay ON pay.payaroid=con.payrefoid AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' where con.reftype='QL_trnjualmst' and con.refoid=jm.trnjualmstoid and con.custoid=jm.custoid) from QL_trnjualmst jm WHERE jm.cmpcode='" & AppCmpCode.Text & "' AND jm.trnjualmstoid=" & Session("trnjualmstoid") & ""
            If Session("reason3") = "NOTA LAMA" Then
                cekBayar = 0
            Else
                cekBayar = ToDouble(GetStrData(sSql))
            End If
            If cekBayar <= 0 Then
                If Session("reason") = "DP" Then
                    'Generate cashbankno
                    GenerateCBARno(iKasAcctgOid)
                    'Generate dparno
                    GenerateDPARno()
                Else
                    'Generate cashbankno
                    GenerateCBARno2(iKasAcctgOid)
                End If
            End If
            'Ambil ID CustGroup
            sSql = "SELECT custgroupoid from QL_mstcust WHERE cmpcode='" & AppCmpCode.Text & "' AND custoid=" & Session("custoid") & ""
            iCustGroupOid = ckon.ambilscalar(sSql)
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            ' Update Sales Return
            If sStatus = "Approved" Then
                sSql = "UPDATE QL_trnsalesreturmst SET sretno='" & sNo & "', sretmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP"
            Else
                sSql = "UPDATE QL_trnsalesreturmst SET sretno='" & sNo & "', sretmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP " & IIf(sStatus = "Rejected", ", rejectreason='" & Tchar(ActionReason.Text) & "', rejectuser='" & Session("UserID") & "', rejecttime=CURRENT_TIMESTAMP", ", revisereason='" & Tchar(ActionReason.Text) & "', reviseuser='" & Session("UserID") & "', revisetime=CURRENT_TIMESTAMP")
            End If
            sSql &= " WHERE sretmststatus='In Approval' AND sretmstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnsalesreturmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Rejected" Then
                    sSql = "UPDATE QL_trnjualdtl SET trnjualdtlres2='' WHERE cmpcode='" & AppCmpCode.Text & "' AND trnjualdtloid IN (SELECT trnjualdtloid FROM QL_trnsalesreturdtl WHERE cmpcode='" & AppCmpCode.Text & "' AND sretmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnjualmst SET trnjualmstres2='" & sStatus & "' WHERE cmpcode='" & AppCmpCode.Text & "' AND trnjualmstoid IN (SELECT DISTINCT trnjualmstoid FROM QL_trnsalesreturmst WHERE cmpcode='" & AppCmpCode.Text & "' AND sretmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                ElseIf sStatus = "Approved" Then
                    Dim objTable As DataTable = Session("TblDtlSretRaw")
                    'Update Stock
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        Dim RateIDR As Double = ToDouble(objTable.Rows(C1).Item("valueidr") * cRate.GetRateMonthlyIDRValue)
                        Dim RateUSD As Double = ToDouble(objTable.Rows(C1).Item("valueusd") * cRate.GetRateMonthlyUSDValue)
                        ' Insert QL_conmat
                        sSql = "INSERT INTO QL_constock (cmpcode, constockoid, contype, trndate, formaction, formoid, periodacctg, refname, refoid, mtrlocoid, qtyin, qtyout, amount, hpp, typemin, note, reason, createuser, createtime, upduser, updtime, refno, valueidr, valueusd, qtyin_unitbesar, qtyout_unitbesar) VALUES ('" & AppCmpCode.Text & "', " & conmtroid & ", 'Sales Retur',  '" & sDate & "', 'QL_trnsalesreturdtl', " & Session("trnoid") & ", '" & sPeriod & "', '" & objTable.Rows(C1).Item("refname") & "', " & objTable.Rows(C1).Item("matoid") & "," & objTable.Rows(C1).Item("sretwhoid") & ", " & ToDouble(objTable.Rows(C1).Item("qtyunitkecil")) & ", 0, 0, 0, -1, '" & sNo & " # Dari " & Tchar(Session("custname").ToString) & "', 'Sales Return', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & sNo & "'," & RateIDR & "," & RateUSD & ", " & ToDouble(objTable.Rows(C1).Item("qtyunitbesar")) & ", 0)"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        conmtroid += 1
                        ' Update QL_crdstock
                        sSql = "UPDATE QL_crdstock SET qtyin=qtyin + " & ToDouble(objTable.Rows(C1).Item("qtyunitkecil")) & ", saldoakhir=saldoakhir + " & ToDouble(objTable.Rows(C1).Item("qtyunitkecil")) & ", qtyin_unitbesar=qtyin_unitbesar + " & ToDouble(objTable.Rows(C1).Item("qtyunitbesar")) & ", saldoakhir_unitbesar=saldoakhir_unitbesar + " & ToDouble(objTable.Rows(C1).Item("qtyunitbesar")) & ", lasttranstype='QL_trnsalesreturdtl', lasttransdate='" & sDate & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & AppCmpCode.Text & "' AND refoid=" & objTable.Rows(C1).Item("matoid") & " AND refname='" & objTable.Rows(C1).Item("refname") & "' AND mtrlocoid=" & objTable.Rows(C1).Item("sretwhoid") & " AND periodacctg='" & sPeriod & "'"
                        xCmd.CommandText = sSql
                        If xCmd.ExecuteNonQuery() = 0 Then
                            ' Insert QL_crdstock
                            sSql = "INSERT INTO QL_crdstock (cmpcode, crdstockoid, periodacctg, refoid, refname, mtrlocoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir, lasttranstype, lasttransdate, upduser, updtime, createuser, createtime, closeuser, closingdate, qtyin_unitbesar, qtyout_unitbesar, qtyadjin_unitbesar, qtyadjout_unitbesar, saldoawal_unitbesar, saldoakhir_unitbesar) VALUES ('" & AppCmpCode.Text & "', " & crdmatoid & ", '" & sPeriod & "', " & objTable.Rows(C1).Item("matoid") & ", '" & objTable.Rows(C1).Item("refname") & "', " & objTable.Rows(C1).Item("sretwhoid") & ", " & ToDouble(objTable.Rows(C1).Item("qtyunitkecil")) & ", 0, 0, 0, 0, " & ToDouble(objTable.Rows(C1).Item("qtyunitkecil")) & ", 'QL_trnsalesreturdtl', '" & sDate & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', '1/1/1900', " & ToDouble(objTable.Rows(C1).Item("qtyunitbesar")) & ", 0, 0, 0, 0, " & ToDouble(objTable.Rows(C1).Item("qtyunitbesar")) & ")"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            crdmatoid += 1
                        End If

                        'Insert STockvalue
                        sSql = GetQueryUpdateStockValue(ToDouble(objTable.Rows(C1)("qtyunitkecil").ToString), ToDouble(objTable.Rows(C1)("qtyunitbesar").ToString), RateIDR, RateUSD, "QL_trnsalesreturdtl", sDate, Session("UserID"), AppCmpCode.Text, sPeriod, objTable.Rows(C1)("matoid"))
                        xCmd.CommandText = sSql
                        If xCmd.ExecuteNonQuery() = 0 Then
                            sSql = GetQueryInsertStockValue(ToDouble(objTable.Rows(C1)("qtyunitkecil").ToString), ToDouble(objTable.Rows(C1)("qtyunitbesar").ToString), RateIDR, RateUSD, "QL_trnsalesreturdtl", sDate, Session("UserID"), AppCmpCode.Text, sPeriod, objTable.Rows(C1)("matoid"), iStockValOid)
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iStockValOid += 1
                        End If
                        If Session("trnjualmstoid") > 0 Then
                            'Update trnjualdtl
                            If ToDouble(objTable.Rows(C1).Item("qtyunitkecil")) >= ToDouble(objTable.Rows(C1).Item("doqty_unitkecil")) Then
                                sSql = "UPDATE QL_trnjualdtl SET trnjualdtlres2='Complete' WHERE cmpcode='" & AppCmpCode.Text & "' AND trnjualdtloid=" & objTable.Rows(C1).Item("trnjualdtloid") & " "
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                            End If
                            sSql = "UPDATE QL_trnjualmst SET trnjualres2='Closed' WHERE cmpcode='" & AppCmpCode.Text & "' AND trnjualmstoid IN (SELECT DISTINCT trnjualmstoid FROM QL_trnsalesreturdtl rd INNER JOIN QL_trnsalesreturmst rm ON rd.sretmstoid=rm.sretmstoid WHERE rm.cmpcode='" & AppCmpCode.Text & "' AND (select count(*) from QL_trnjualdtl jd WHERE trnjualdtlres2<>'' AND trnjualdtloid<>" & objTable.Rows(C1).Item("trnjualdtloid") & ")=0)"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If

                        dAmtIDR += ToDouble(objTable.Rows(C1).Item("valueidr")) * ToDouble(objTable.Rows(C1).Item("qtyunitkecil"))
                        dAmtUSD += ToDouble(objTable.Rows(C1).Item("valueusd")) * ToDouble(objTable.Rows(C1).Item("qtyunitkecil"))
                    Next
                    ' Update mstoid
                    sSql = "UPDATE QL_mstoid SET lastoid=" & conmtroid - 1 & " WHERE tablename='QL_CONSTOCK' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & crdmatoid - 1 & " WHERE tablename='QL_CRDSTOCK' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iStockValOid - 1 & " WHERE tablename='QL_STOCKVALUE' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    ' Posting Auto Journal hpp & persediaan
                    ' Insert QL_trnglmst
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & AppCmpCode.Text & "', " & iGlMstOid & ", '" & sDate & "', '" & sPeriod & "', 'Sales Retur|No. " & sNo & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    'insert jurnal persediaan
                    Dim obj1 As DataTable = Session("TblGroup")
                    For C1 As Int16 = 0 To obj1.Rows.Count - 1
                        persediaanAcctg = obj1.Rows(C1).Item("acctgoid").ToString
                        AmtIDR = ToDouble(obj1.Rows(C1).Item("amtidr").ToString)
                        AmtUSD = ToDouble(obj1.Rows(C1).Item("amtusd").ToString)
                        ' Insert QL_trngldtl
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & persediaanAcctg & ", 'D', " & AmtIDR & ", '" & sNo & "', 'Sales Retur|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & AmtIDR * cRate.GetRateMonthlyIDRValue & ", " & AmtUSD * cRate.GetRateMonthlyUSDValue & ", 'QL_trnsalesreturmst " & Session("trnoid") & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1 : iSeq += 1
                    Next
                    'insert jurnal hpp
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & hppAcctg & ", 'C', " & dAmtIDR & ", '" & sNo & "', 'Sales Retur|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dAmtIDR * cRate.GetRateMonthlyIDRValue & ", " & dAmtUSD * cRate.GetRateMonthlyUSDValue & ", 'QL_trnsalesreturmst " & Session("trnoid") & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1 : iGlMstOid += 1

                    ' Posting Auto Journal retur penjualan
                    ' Insert QL_trnglmst
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & AppCmpCode.Text & "', " & iGlMstOid & ", '" & sDate & "', '" & sPeriod & "', 'Sales Retur|No. " & sNo & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    'Debet COA Retur Penjualan
                    If Session("trnjualmstoid") > 0 Then
                        ' Insert QL_trngldtl retur penjualan
                        If Session("taxtype") = "NT" Then
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq2 & ", " & iGlMstOid & ", " & returAcctg & ", 'D', " & Session("netto") & ", '" & sNo & "', 'Sales Retur|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Session("netto") * cRate.GetRateMonthlyIDRValue & ", " & Session("netto") * cRate.GetRateMonthlyUSDValue & ", 'QL_trnsalesreturmst " & Session("trnoid") & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        Else
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq2 & ", " & iGlMstOid & ", " & returAcctg & ", 'D', " & Session("dpp") & ", '" & sNo & "', 'Sales Retur|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Session("dpp") * cRate.GetRateMonthlyIDRValue & ", " & Session("dpp") * cRate.GetRateMonthlyUSDValue & ", 'QL_trnsalesreturmst " & Session("trnoid") & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                        iGlDtlOid += 1 : iSeq2 += 1
                        ' Insert QL_trngldtl PPN
                        If Session("tax") > 0 Then
                            Dim dRound As Double = (ToDouble(Session("dpp")) + ToDouble(Session("tax"))) - Session("netto")

                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq2 & ", " & iGlMstOid & ", " & ppnAcctg & ", 'D', " & Session("tax") & ", '" & sNo & "', 'Sales Retur|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Session("tax") * cRate.GetRateMonthlyIDRValue & ", " & Session("tax") * cRate.GetRateMonthlyUSDValue & ", 'QL_trnsalesreturmst " & Session("trnoid") & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iGlDtlOid += 1 : iSeq2 += 1
                            If dRound <> 0 Then
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iRoundAcctgOid & ", '" & IIf(dRound > 0, "C", "D") & "', " & Math.Abs(dRound) & ", '" & sNo & "', 'Sales Retur|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Math.Abs(dRound) * cRate.GetRateMonthlyIDRValue & ", " & Math.Abs(dRound) * cRate.GetRateMonthlyUSDValue & ", 'QL_trnsalesreturmst " & Session("trnoid") & "')"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iGlDtlOid += 1
                                iSeq += 1
                            End If
                        End If
                    Else 'Return Penjualan INIT AR & Nota Lama
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq2 & ", " & iGlMstOid & ", " & returAcctg & ", 'D', " & Session("netto") & ", '" & sNo & "', 'Sales Retur|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Session("netto") * cRate.GetRateMonthlyIDRValue & ", " & Session("netto") * cRate.GetRateMonthlyUSDValue & ", 'QL_trnsalesreturmst " & Session("trnoid") & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1 : iSeq2 += 1
                    End If
                    'Credit COA Retur Penjualan
                    If Session("trnjualmstoid") = 0 Then 'Untuk Nota Lama
                        'insert jurnal Kas
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq2 & ", " & iGlMstOid & ", " & iKasAcctgOid & ", 'C', " & Session("netto") & ", '" & sNo & "', 'Sales Retur|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Session("netto") * cRate.GetRateMonthlyIDRValue & ", " & Session("netto") * cRate.GetRateMonthlyUSDValue & ", 'QL_trnsalesreturmst " & Session("trnoid") & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Else
                        If Session("reason") = "DP" Then
                            'insert jurnal Kas
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq2 & ", " & iGlMstOid & ", " & iKasAcctgOid & ", 'C', " & Session("netto") & ", '" & sNo & "', 'Sales Retur|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Session("netto") * cRate.GetRateMonthlyIDRValue & ", " & Session("netto") * cRate.GetRateMonthlyUSDValue & ", 'QL_trnsalesreturmst " & Session("trnoid") & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        Else
                            'insert jurnal piutang
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", " & iSeq2 & ", " & iGlMstOid & ", " & piutangAcctg & ", 'C', " & Session("netto") & ", '" & sNo & "', 'Sales Retur|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Session("netto") * cRate.GetRateMonthlyIDRValue & ", " & Session("netto") * cRate.GetRateMonthlyUSDValue & ", 'QL_trnsalesreturmst " & Session("trnoid") & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If                        
                    End If
                    iGlDtlOid += 1 : iGlMstOid += 1

                    'Cek Pembayaran sudah Lunas atau Belum 
                    If cekBayar <= 0 Then 'jika Sudah Lunas
                        Dim dAcctgAmt As Double = ToDouble(Session("netto"))
                        Dim dAcctgAmtIDR As Double = ToDouble(Session("netto")) * cRate.GetRateMonthlyIDRValue
                        Dim dAcctgAmtUSD As Double = ToDouble(Session("netto")) * cRate.GetRateMonthlyUSDValue
                        If Session("reason") = "DP" Then
                            'insert cashbankmst
                            sSql = "INSERT INTO QL_trncashbankmst (cmpcode, cashbankoid, periodacctg, cashbankno, cashbankdate, cashbanktype, cashbankgroup, acctgoid, curroid, cashbankamt, cashbankamtidr, cashbankamtusd, personoid, cashbankduedate, cashbankrefno, cashbanknote, cashbankstatus, createuser, createtime, upduser, updtime, cashbanktakegiro, giroacctgoid, addacctgoid1, addacctgamt1, addacctgoid2, addacctgamt2, addacctgoid3, addacctgamt3) VALUES ('" & AppCmpCode.Text & "', " & cashbankoid & ", '" & sPeriod & "', '" & Session("sNocb") & "', '" & sDate & "', 'BKM', 'DPAR', " & iKasAcctgOid & ", 1, " & dAcctgAmt & ", " & dAcctgAmtIDR & ", " & dAcctgAmtUSD & ", " & Session("custoid") & ", '1/1/1900', '" & sNo & "', 'From Sales Retur|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '1/1/1900', 0,0,0,0,0,0,0)"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_mstoid SET lastoid=" & cashbankoid & " WHERE tablename='QL_TRNCASHBANKMST' and cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            'insert dpar
                            sSql = "INSERT INTO QL_trndpar (cmpcode, dparoid, periodacctg, dparno, dpardate, custoid, acctgoid, cashbankoid, dparpaytype, dparpayacctgoid, dparpayrefno, dparduedate, curroid, rateoid, rate2oid, dparamt, dparaccumamt, dparnote, dparstatus, createuser, createtime, upduser, updtime, dpartakegiro, giroacctgoid, addacctgoid1, addacctgamt1, addacctgoid2, addacctgamt2, addacctgoid3, addacctgamt3, dparamtidr, dparamtusd) VALUES ('" & AppCmpCode.Text & "', " & dparoid & ", '" & sPeriod & "', '" & Session("sNodp") & "', '" & sDate & "', " & Session("custoid") & ", " & iDPAcctgOid & ", " & cashbankoid & ", 'BKM', " & iKasAcctgOid & ", '" & sNo & "', '1/1/1900', 1, " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & dAcctgAmt & ", 0, 'From Sales Retur|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '1/1/1900', 0,0,0,0,0,0,0, " & dAcctgAmtIDR & ", " & dAcctgAmtUSD & ")"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_mstoid SET lastoid=" & dparoid & " WHERE tablename='QL_TRNDPAR' and cmpcode='" & AppCmpCode.Text & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()

                            ' Insert GL MST utk DPAR
                            sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & AppCmpCode.Text & "', " & iGlMstOid & ", '" & sDate & "', '" & sPeriod & "', 'DP A/R|No=" & Session("sNodp") & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()

                            ' Insert GL DTL utk DPAR
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", 1, " & iGlMstOid & ", " & iKasAcctgOid & ", 'D', " & dAcctgAmt & ", '" & Session("sNodp") & "', 'DP A/R|No=" & Session("sNodp") & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dAcctgAmtIDR & ", " & dAcctgAmtUSD & ", 'QL_trndpar " & dparoid & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iGlDtlOid += 1
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", 2, " & iGlMstOid & ", " & iDPAcctgOid & ", 'C', " & dAcctgAmt & ", '" & Session("sNodp") & "', 'DP A/R|No=" & Session("sNodp") & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dAcctgAmtIDR & ", " & dAcctgAmtUSD & ", 'QL_trncashbankmst " & cashbankoid & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iGlDtlOid += 1 : iGlMstOid += 1
                        Else
                            'Journal Pengembalian Uang
                            'insert cashbankmst
                            sSql = "INSERT INTO QL_trncashbankmst (cmpcode, cashbankoid, periodacctg, cashbankno, cashbankdate, cashbanktype, cashbankgroup, acctgoid, curroid, cashbankamt, cashbankamtidr, cashbankamtusd, personoid, cashbankduedate, cashbankrefno, cashbanknote, cashbankstatus, createuser, createtime, upduser, updtime, cashbanktakegiro, giroacctgoid, addacctgoid1, addacctgamt1, addacctgoid2, addacctgamt2, addacctgoid3, addacctgamt3) VALUES ('" & AppCmpCode.Text & "', " & cashbankoid & ", '" & sPeriod & "', '" & Session("sNocb") & "', '" & sDate & "', 'BKK', 'EXPENSE', " & iKasAcctgOid & ", 1, " & dAcctgAmt & ", " & dAcctgAmtIDR & ", " & dAcctgAmtUSD & ", " & Session("custoid") & ", '1/1/1900', '" & sNo & "', 'Pengembalian Cash Dari Sales Retur|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '1/1/1900', 0,0,0,0,0,0,0)"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_mstoid SET lastoid=" & cashbankoid & " WHERE tablename='QL_TRNCASHBANKMST' and cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            'insert cashbankgl
                            sSql = "INSERT INTO QL_trncashbankgl (cmpcode, cashbankgloid, cashbankoid, acctgoid, cashbankglamt, cashbankglamtidr, cashbankglamtusd, cashbankglduedate, cashbankglrefno, cashbankglnote, cashbankglstatus, createuser, createtime, upduser, updtime) VALUES ('" & AppCmpCode.Text & "', " & cashbankgloid & ", " & cashbankoid & ", " & returAcctg & ", " & dAcctgAmt & ", " & dAcctgAmtIDR & ", " & dAcctgAmtUSD & ", '1/1/1900', '" & sNo & "', 'Pengembalian Cash Dari Sales Retur|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_mstoid SET lastoid=" & cashbankgloid & " WHERE tablename='QL_TRNCASHBANKGL' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()

                            ' Insert GL MST
                            sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & AppCmpCode.Text & "', " & iGlMstOid & ", '" & sDate & "', '" & sPeriod & "', 'Cash/Bank Expense|No=" & Session("sNocb") & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()

                            ' Insert GL DTL
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", 1, " & iGlMstOid & ", " & returAcctg & ", 'D', " & dAcctgAmt & ", '" & Session("sNocb") & "', 'Cash/Bank Expense|No=" & Session("sNocb") & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dAcctgAmtIDR & ", " & dAcctgAmtUSD & ", 'QL_trncashbankmst " & cashbankoid & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iGlDtlOid += 1
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & AppCmpCode.Text & "', " & iGlDtlOid & ", 2, " & iGlMstOid & ", " & iKasAcctgOid & ", 'C', " & dAcctgAmt & ", '" & Session("sNocb") & "', 'Cash/Bank Expense|No=" & Session("sNocb") & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dAcctgAmtIDR & ", " & dAcctgAmtUSD & ", 'QL_trncashbankmst " & cashbankoid & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iGlDtlOid += 1 : iGlMstOid += 1
                        End If

                    Else 'jika Belum Lunas
                        '�nsert conar
                        sSql = "INSERT INTO QL_conar (cmpcode, conaroid, reftype, refoid, payrefoid, custoid, acctgoid, trnarstatus, trnartype, trnardate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans, amtbayar, trnarnote, trnarres1, trnarres2, trnarres3, createuser, createtime, upduser, updtime, amttransidr, amtbayaridr, amttransusd, amtbayarusd) VALUES ('" & AppCmpCode.Text & "', " & conaroid & ", 'QL_trnjualmst', " & Session("trnjualmstoid") & ", " & Session("trnoid") & ", " & Session("custoid") & ", " & piutangAcctg & ", 'Post', 'SRET', '1/1/1900', '" & sPeriod & "', 0, '" & sDate & "', '" & sNo & "', 0, '" & sDate & "', 0, " & Session("netto") & ", '', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, " & Session("netto") * cRate.GetRateMonthlyIDRValue & ", 0, " & Session("netto") * cRate.GetRateMonthlyUSDValue & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        sSql = "UPDATE QL_mstoid SET lastoid=" & conaroid & " WHERE tablename='QL_CONAR' AND cmpcode='" & AppCmpCode.Text & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()

                        'Update Credit Limit
                        sSql = "UPDATE QL_mstcustgroup SET custcreditlimitusagerupiah = custcreditlimitusagerupiah - " & Session("netto") & " WHERE cmpcode='" & AppCmpCode.Text & "' AND custgroupoid=" & iCustGroupOid & ""
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        'Update Trnjualmst
                        Dim sisa As Double = ToDouble(cekBayar) - ToDouble(Session("netto"))
                        If ToDouble(sisa) <= 0 Then
                            sSql = "UPDATE QL_trnjualmst SET trnjualstatus='Closed' WHERE cmpcode='" & AppCmpCode.Text & "' AND trnjualmstoid=" & Session("trnjualmstoid") & ""
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If

                        'Update SO
                        'sSql = "UPDATE QL_trnsomst SET somststatus='Approved' WHERE cmpcode='" & AppCmpCode.Text & "' AND somstoid IN (SELECT sm.somstoid from QL_trnsalesreturmst rm inner join QL_trnjualmst jm on jm.trnjualmstoid=rm.trnjualmstoid inner join QL_trndomst dm ON dm.domstoid=jm.domstoid inner join QL_trnsomst sm on sm.somstoid=dm.somstoid WHERE rm.cmpcode='" & AppCmpCode.Text & "' AND rm.sretmstoid=" & Session("trnoid") & ")"
                        'xCmd.CommandText = sSql
                        'xCmd.ExecuteNonQuery()
                        'sSql = "UPDATE QL_trnsodtl SET sodtlstatus='' WHERE cmpcode='" & AppCmpCode.Text & "' AND sodtloid IN (SELECT sm.sodtloid from QL_trnsalesreturdtl rm inner join QL_trnjualdtl jm on jm.trnjualdtloid=rm.trnjualdtloid inner join QL_trndodtl dm ON dm.dodtloid=jm.dodtloid inner join QL_trnsodtl sm on sm.sodtloid=dm.sodtloid WHERE rm.cmpcode='" & AppCmpCode.Text & "' AND rm.sretmstoid=" & Session("trnoid") & ")"
                        'xCmd.CommandText = sSql
                        'xCmd.ExecuteNonQuery()
                    End If

                    'update jurnal mstoid
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid - 1 & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "Sales Return General Material Approval"
    Private Sub SetGVListSretGM()
        Dim arHeader() As String = {"Draft No.", "Return Date", "Customer", "Return Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "custname", "sretgenmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "custname", "sretgenmstnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT sret.cmpcode, sret.sretgenmstoid AS trnoid, sret.sretgenno, CONVERT(VARCHAR(10), sret.sretgendate, 101) AS trndate, c.custname, sret.sretgenmstnote, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate FROM QL_trnsretgenmst sret INNER JOIN QL_approval ap ON sret.sretgenmstoid=ap.oid AND ap.statusrequest='New' INNER JOIN QL_mstcust c ON c.custoid=sret.custoid WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_trnsretgenmst' AND sret.sretgenmststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnsretgenmst")
    End Sub

    Private Sub SetGVDataSretGM(ByVal dtDataHeader As DataTable)
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "Return Date", "Customer", "Return Note"}
        Dim arValue() As String = {"trnoid", "trndate", "custname", "sretgenmstnote"}
        Dim arType() As String = {"S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arHeader() As String = {"No.", "Mat. Code", "Mat. Desc", "Quantity", "Unit", "Warehouse", "Note"}
        Dim arField() As String = {"sretgendtlseq", "matgencode", "matgenshortdesc", "sretgenqty", "unit", "warehouse", "sretgendtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT sretd.sretgendtlseq, sretd.sretgendtloid, m.matgenoid, m.matgencode, m.matgenlongdesc AS matgenshortdesc, sretd.sretgenqty, g.gendesc AS unit, g2.gendesc AS warehouse, sretd.sretgendtlnote, sretd.sretgenwhoid FROM QL_trnsretgenmst sret INNER JOIN QL_trnsretgendtl sretd ON sret.sretgenmstoid=sretd.sretgenmstoid INNER JOIN QL_mstmatgen m ON sretd.matgenoid=m.matgenoid INNER JOIN QL_mstgen g ON sretd.sretgenunitoid=g.genoid INNER JOIN QL_mstgen g2 ON sretd.sretgenwhoid=g2.genoid WHERE sret.sretgenmstoid=" & Session("trnoid") & " ORDER BY sretd.sretgendtlseq"
        Session("TblDtlSretGen") = ckon.ambiltabel(sSql, "QL_trnsretgendtl")
        gvData.DataSource = Session("TblDtlSretGen")
        gvData.DataBind()
    End Sub

    Private Function GenerateSretGMNo() As String
        Dim sNo As String = "SRTGM-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(sretgenno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnsretgenmst WHERE cmpcode='" & AppCmpCode.Text & "' AND sretgenno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdateSretGM(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select Sales Return General Material Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select Sales Return General Material Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim conmtroid As Integer = GenerateID("QL_CONMAT", CompnyCode)
        Dim crdmatoid As Integer = GenerateID("QL_CRDMTR", CompnyCode)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update Sales Return
            sSql = "UPDATE QL_trnsretgenmst SET sretgenno='" & sNo & "', sretgenmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP"
            If sStatus = "Approved" Then
                sSql &= ", sretgenmstres2='" & DDLReturnType.SelectedValue & "'"
            End If
            sSql &= " WHERE sretgenmststatus='In Approval' AND sretgenmstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnsretgenmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Rejected" Then
                    sSql = "UPDATE QL_trnshipmentgendtl SET dogendtlres3='' WHERE cmpcode='" & AppCmpCode.Text & "' AND dogendtloid IN (SELECT dogendtloid FROM QL_trnsretgendtl WHERE cmpcode='" & AppCmpCode.Text & "' AND sretgenmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnshipmentgenmst SET dogenmstres3='' WHERE cmpcode='" & AppCmpCode.Text & "' AND dogenmstoid IN (SELECT DISTINCT dogenmstoid FROM QL_trnsretgendtl WHERE cmpcode='" & AppCmpCode.Text & "' AND sretgenmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                ElseIf sStatus = "Approved" Then
                    Dim objTable As DataTable = Session("TblDtlSretGen")
                    'Update Stock
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        ' Insert QL_conmat
                        sSql = "INSERT INTO QL_conmat (cmpcode, conmtroid, type, typemin, trndate, periodacctg, formaction, formoid, refoid, refname, mtrwhoid, qtyin, qtyout, reason, note, upduser, updtime) VALUES ('" & AppCmpCode.Text & "', " & conmtroid & ", 'SRETGM', 1, '" & Session("trndate") & "', '" & GetDateToPeriodAcctg(CDate(Session("trndate"))) & "', 'QL_trnsretgendtl', " & Session("trnoid") & ", " & objTable.Rows(C1).Item("matgenoid") & ", 'GENERAL MATERIAL', " & objTable.Rows(C1).Item("sretgenwhoid") & ", " & ToDouble(objTable.Rows(C1).Item("sretgenqty")) & ", 0, 'Sales Return General Material', '" & sNo & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        conmtroid += 1
                        ' Update QL_crdmtr
                        sSql = "UPDATE QL_crdmtr SET qtyin=qtyin + " & ToDouble(objTable.Rows(C1).Item("sretgenqty")) & ", lasttranstype='QL_trnsretgendtl', lasttransdate='" & Session("trndate") & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & AppCmpCode.Text & "' AND refoid=" & objTable.Rows(C1).Item("matgenoid") & " AND refname='GENERAL MATERIAL' AND mtrwhoid=" & objTable.Rows(C1).Item("sretgenwhoid") & " AND periodacctg='" & GetDateToPeriodAcctg(CDate(Session("trndate"))) & "'"
                        xCmd.CommandText = sSql
                        If xCmd.ExecuteNonQuery() = 0 Then
                            ' Insert QL_crdmtr
                            sSql = "INSERT INTO QL_crdmtr (cmpcode, crdmatoid, periodacctg, refoid, refname, mtrwhoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir, lasttranstype, lasttransdate, upduser, updtime, createuser, createdate, closeuser, closingdate) VALUES ('" & AppCmpCode.Text & "', " & crdmatoid & ", '" & GetDateToPeriodAcctg(CDate(Session("trndate"))) & "', " & objTable.Rows(C1).Item("matgenoid") & ", 'GENERAL MATERIAL', " & objTable.Rows(C1).Item("sretgenwhoid") & ", " & ToDouble(objTable.Rows(C1).Item("sretgenqty")) & ", 0, 0, 0, 0, 0, 'QL_trnsretgendtl', '" & Session("trndate") & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', '1/1/1900')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            crdmatoid += 1
                        End If
                        If DDLReturnType.SelectedValue = "Retur Ganti Barang" Then
                            sSql = "UPDATE QL_trnsogendtl SET sogendtlstatus='' WHERE cmpcode='" & AppCmpCode.Text & "' AND sogendtloid IN (SELECT sogendtloid FROM QL_trnshipmentgendtl dord INNER JOIN QL_trnsretgendtl srtd ON dord.dogendtloid=srtd.dogendtloid WHERE srtd.cmpcode='" & AppCmpCode.Text & "' AND sretgendtloid=" & objTable.Rows(C1).Item("sretgendtloid") & ")"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                    Next
                    If DDLReturnType.SelectedValue = "Retur Ganti Barang" Then
                        sSql = "UPDATE QL_trnsogenmst SET sogenmststatus='Approved' WHERE cmpcode='" & AppCmpCode.Text & "' AND sogenmstoid IN (SELECT sogenmstoid FROM QL_trnsretgenmst srtm WHERE cmpcode='" & AppCmpCode.Text & "' AND srtm.sretgenmstoid=" & Session("trnoid") & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                    sSql = "UPDATE QL_mstoid SET lastoid=" & conmtroid - 1 & " WHERE tablename='QL_CONMAT' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & crdmatoid - 1 & " WHERE tablename='QL_CRDMTR' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "Sales Return Spare Part Approval"
    Private Sub SetGVListSretSP()
        Dim arHeader() As String = {"Draft No.", "Return Date", "Customer", "Return Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "custname", "sretspmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "custname", "sretspmstnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT sret.cmpcode, sret.sretspmstoid AS trnoid, sret.sretspno, CONVERT(VARCHAR(10), sret.sretspdate, 101) AS trndate, c.custname, sret.sretspmstnote, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate FROM QL_trnsretspmst sret INNER JOIN QL_approval ap ON sret.sretspmstoid=ap.oid AND ap.statusrequest='New' INNER JOIN QL_mstcust c ON c.custoid=sret.custoid WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_trnsretspmst' AND sret.sretspmststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnsretspmst")
    End Sub

    Private Sub SetGVDataSretSP(ByVal dtDataHeader As DataTable)
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "Return Date", "Customer", "Return Note"}
        Dim arValue() As String = {"trnoid", "trndate", "custname", "sretspmstnote"}
        Dim arType() As String = {"S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arHeader() As String = {"No.", "Mat. Code", "Mat. Desc", "Quantity", "Unit", "Warehouse", "Note"}
        Dim arField() As String = {"sretspdtlseq", "sparepartcode", "sparepartshortdesc", "sretspqty", "unit", "warehouse", "sretspdtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT sretd.sretspdtlseq, sretd.sretspdtloid, m.sparepartoid, m.sparepartcode, m.sparepartlongdesc AS sparepartshortdesc, sretd.sretspqty, g.gendesc AS unit, g2.gendesc AS warehouse, sretd.sretspdtlnote, sretd.sretspwhoid FROM QL_trnsretspmst sret INNER JOIN QL_trnsretspdtl sretd ON sret.sretspmstoid=sretd.sretspmstoid INNER JOIN QL_mstsparepart m ON sretd.sparepartoid=m.sparepartoid INNER JOIN QL_mstgen g ON sretd.sretspunitoid=g.genoid INNER JOIN QL_mstgen g2 ON sretd.sretspwhoid=g2.genoid WHERE sret.sretspmstoid=" & Session("trnoid") & " ORDER BY sretd.sretspdtlseq"
        Session("TblDtlSretSP") = ckon.ambiltabel(sSql, "QL_trnsretspdtl")
        gvData.DataSource = Session("TblDtlSretSP")
        gvData.DataBind()
    End Sub

    Private Function GenerateSretSPNo() As String
        Dim sNo As String = "SRTSP-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(sretspno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnsretspmst WHERE cmpcode='" & AppCmpCode.Text & "' AND sretspno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdateSretSP(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select Sales Return Spare Part Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select Sales Return Spare Part Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim conmtroid As Integer = GenerateID("QL_CONMAT", CompnyCode)
        Dim crdmatoid As Integer = GenerateID("QL_CRDMTR", CompnyCode)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update Sales Return
            sSql = "UPDATE QL_trnsretspmst SET sretspno='" & sNo & "', sretspmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP "
            If sStatus = "Approved" Then
                sSql &= ", sretspmstres2='" & DDLReturnType.SelectedValue & "'"
            End If
            sSql &= " WHERE sretspmststatus='In Approval' AND sretspmstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnsretspmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Rejected" Then
                    sSql = "UPDATE QL_trnshipmentspdtl SET dospdtlres3='' WHERE cmpcode='" & AppCmpCode.Text & "' AND dospdtloid IN (SELECT dospdtloid FROM QL_trnsretspdtl WHERE cmpcode='" & AppCmpCode.Text & "' AND sretspmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnshipmentspmst SET dospmstres3='' WHERE cmpcode='" & AppCmpCode.Text & "' AND dospmstoid IN (SELECT DISTINCT dospmstoid FROM QL_trnsretspdtl WHERE cmpcode='" & AppCmpCode.Text & "' AND sretspmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                ElseIf sStatus = "Approved" Then
                    Dim objTable As DataTable = Session("TblDtlSretSP")
                    'Update Stock
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        ' Insert QL_conmat
                        sSql = "INSERT INTO QL_conmat (cmpcode, conmtroid, type, typemin, trndate, periodacctg, formaction, formoid, refoid, refname, mtrwhoid, qtyin, qtyout, reason, note, upduser, updtime) VALUES ('" & AppCmpCode.Text & "', " & conmtroid & ", 'SRETSP', 1, '" & Session("trndate") & "', '" & GetDateToPeriodAcctg(CDate(Session("trndate"))) & "', 'QL_trnsretspdtl', " & Session("trnoid") & ", " & objTable.Rows(C1).Item("sparepartoid") & ", 'SPARE PART', " & objTable.Rows(C1).Item("sretspwhoid") & ", " & ToDouble(objTable.Rows(C1).Item("sretspqty")) & ", 0, 'Sales Return Spare Part', '" & sNo & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        conmtroid += 1
                        ' Update QL_crdmtr
                        sSql = "UPDATE QL_crdmtr SET qtyin=qtyin + " & ToDouble(objTable.Rows(C1).Item("sretspqty")) & ", lasttranstype='QL_trnsretspdtl', lasttransdate='" & Session("trndate") & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & AppCmpCode.Text & "' AND refoid=" & objTable.Rows(C1).Item("sparepartoid") & " AND refname='SPARE PART' AND mtrwhoid=" & objTable.Rows(C1).Item("sretspwhoid") & " AND periodacctg='" & GetDateToPeriodAcctg(CDate(Session("trndate"))) & "'"
                        xCmd.CommandText = sSql
                        If xCmd.ExecuteNonQuery() = 0 Then
                            ' Insert QL_crdmtr
                            sSql = "INSERT INTO QL_crdmtr (cmpcode, crdmatoid, periodacctg, refoid, refname, mtrwhoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir, lasttranstype, lasttransdate, upduser, updtime, createuser, createdate, closeuser, closingdate) VALUES ('" & AppCmpCode.Text & "', " & crdmatoid & ", '" & GetDateToPeriodAcctg(CDate(Session("trndate"))) & "', " & objTable.Rows(C1).Item("sparepartoid") & ", 'SPARE PART', " & objTable.Rows(C1).Item("sretspwhoid") & ", " & ToDouble(objTable.Rows(C1).Item("sretspqty")) & ", 0, 0, 0, 0, 0, 'QL_trnsretspdtl', '" & Session("trndate") & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', '1/1/1900')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            crdmatoid += 1
                        End If
                        If DDLReturnType.SelectedValue = "Retur Ganti Barang" Then
                            sSql = "UPDATE QL_trnsospdtl SET sospdtlstatus='' WHERE cmpcode='" & AppCmpCode.Text & "' AND sospdtloid IN (SELECT sospdtloid FROM QL_trnshipmentspdtl dord INNER JOIN QL_trnsretspdtl srtd ON dord.dospdtloid=srtd.dospdtloid WHERE srtd.cmpcode='" & AppCmpCode.Text & "' AND sretspdtloid=" & objTable.Rows(C1).Item("sretspdtloid") & ")"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                    Next
                    If DDLReturnType.SelectedValue = "Retur Ganti Barang" Then
                        sSql = "UPDATE QL_trnsospmst SET sospmststatus='Approved' WHERE cmpcode='" & AppCmpCode.Text & "' AND sospmstoid IN (SELECT sospmstoid FROM QL_trnsretspmst srtm WHERE cmpcode='" & AppCmpCode.Text & "' AND srtm.sretspmstoid=" & Session("trnoid") & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                    sSql = "UPDATE QL_mstoid SET lastoid=" & conmtroid - 1 & " WHERE tablename='QL_CONMAT' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & crdmatoid - 1 & " WHERE tablename='QL_CRDMTR' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "Sales Return Finish Good Approval"
    Private Sub SetGVListSretItem()
        Dim arHeader() As String = {"Draft No.", "Return Date", "Customer", "Return Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "custname", "sretitemmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "custname", "sretitemmstnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT sret.cmpcode, sret.sretitemmstoid AS trnoid, sret.sretitemno, CONVERT(VARCHAR(10), sret.sretitemdate, 101) AS trndate, c.custname, sret.sretitemmstnote, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate FROM QL_trnsretitemmst sret INNER JOIN QL_approval ap ON sret.sretitemmstoid=ap.oid AND ap.statusrequest='New' INNER JOIN QL_mstcust c ON c.custoid=sret.custoid WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_trnsretitemmst' AND sret.sretitemmststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnsretitemmst")
    End Sub

    Private Sub SetGVDataSretItem(ByVal dtDataHeader As DataTable)
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "Return Date", "Customer", "Return Note"}
        Dim arValue() As String = {"trnoid", "trndate", "custname", "sretitemmstnote"}
        Dim arType() As String = {"S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arHeader() As String = {"No.", "Mat. Code", "Mat. Desc", "Quantity", "Unit", "Warehouse", "Note"}
        Dim arField() As String = {"sretitemdtlseq", "itemcode", "itemshortdesc", "sretitemqty", "unit", "warehouse", "sretitemdtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT sretd.sretitemdtlseq, sretd.sretitemdtloid, m.itemoid, m.itemcode, m.itemlongdesc AS itemshortdesc, sretd.sretitemqty, g.gendesc AS unit, g2.gendesc AS warehouse, sretd.sretitemdtlnote, sretd.sretitemwhoid FROM QL_trnsretitemmst sret INNER JOIN QL_trnsretitemdtl sretd ON sret.sretitemmstoid=sretd.sretitemmstoid INNER JOIN QL_mstitem m ON sretd.itemoid=m.itemoid INNER JOIN QL_mstgen g ON sretd.sretitemunitoid=g.genoid INNER JOIN QL_mstgen g2 ON sretd.sretitemwhoid=g2.genoid WHERE sret.sretitemmstoid=" & Session("trnoid") & " ORDER BY sretd.sretitemdtlseq"
        Session("TblDtlSretItem") = ckon.ambiltabel(sSql, "QL_trnsretitemdtl")
        gvData.DataSource = Session("TblDtlSretItem")
        gvData.DataBind()
    End Sub

    Private Function GenerateSretItemNo() As String
        Dim sNo As String = "SRTFG-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(sretitemno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnsretitemmst WHERE cmpcode='" & AppCmpCode.Text & "' AND sretitemno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdateSretItem(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select Sales Return Finish Good Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select Sales Return Finish Good Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim conmtroid As Integer = GenerateID("QL_CONMAT", CompnyCode)
        Dim crdmatoid As Integer = GenerateID("QL_CRDMTR", CompnyCode)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update Sales Return
            sSql = "UPDATE QL_trnsretitemmst SET sretitemno='" & sNo & "', sretitemmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP"
            If sStatus = "Approved" Then
                sSql &= ", sretitemmstres2='" & DDLReturnType.SelectedValue & "'"
            End If
            sSql &= " WHERE sretitemmststatus='In Approval' AND sretitemmstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnsretitemmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Rejected" Then
                    sSql = "UPDATE QL_trnshipmentitemdtl SET doitemdtlres3='' WHERE cmpcode='" & AppCmpCode.Text & "' AND doitemdtloid IN (SELECT doitemdtloid FROM QL_trnsretitemdtl WHERE cmpcode='" & AppCmpCode.Text & "' AND sretitemmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnshipmentitemmst SET doitemmstres3='' WHERE cmpcode='" & AppCmpCode.Text & "' AND doitemmstoid IN (SELECT DISTINCT doitemmstoid FROM QL_trnsretitemdtl WHERE cmpcode='" & AppCmpCode.Text & "' AND sretitemmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                ElseIf sStatus = "Approved" Then
                    Dim objTable As DataTable = Session("TblDtlSretItem")
                    'Update Stock
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        ' Insert QL_conmat
                        sSql = "INSERT INTO QL_conmat (cmpcode, conmtroid, type, typemin, trndate, periodacctg, formaction, formoid, refoid, refname, mtrwhoid, qtyin, qtyout, reason, note, upduser, updtime) VALUES ('" & AppCmpCode.Text & "', " & conmtroid & ", 'SRETFG', 1, '" & Session("trndate") & "', '" & GetDateToPeriodAcctg(CDate(Session("trndate"))) & "', 'QL_trnsretitemdtl', " & Session("trnoid") & ", " & objTable.Rows(C1).Item("itemoid") & ", 'FINISH GOOD', " & objTable.Rows(C1).Item("sretitemwhoid") & ", " & ToDouble(objTable.Rows(C1).Item("sretitemqty")) & ", 0, 'Sales Return Finish Good', '" & sNo & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        conmtroid += 1
                        ' Update QL_crdmtr
                        sSql = "UPDATE QL_crdmtr SET qtyin=qtyin + " & ToDouble(objTable.Rows(C1).Item("sretitemqty")) & ", lasttranstype='QL_trnsretitemdtl', lasttransdate='" & Session("trndate") & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & AppCmpCode.Text & "' AND refoid=" & objTable.Rows(C1).Item("itemoid") & " AND refname='FINISH GOOD' AND mtrwhoid=" & objTable.Rows(C1).Item("sretitemwhoid") & " AND periodacctg='" & GetDateToPeriodAcctg(CDate(Session("trndate"))) & "'"
                        xCmd.CommandText = sSql
                        If xCmd.ExecuteNonQuery() = 0 Then
                            ' Insert QL_crdmtr
                            sSql = "INSERT INTO QL_crdmtr (cmpcode, crdmatoid, periodacctg, refoid, refname, mtrwhoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir, lasttranstype, lasttransdate, upduser, updtime, createuser, createdate, closeuser, closingdate) VALUES ('" & AppCmpCode.Text & "', " & crdmatoid & ", '" & GetDateToPeriodAcctg(CDate(Session("trndate"))) & "', " & objTable.Rows(C1).Item("itemoid") & ", 'FINISH GOOD', " & objTable.Rows(C1).Item("sretitemwhoid") & ", " & ToDouble(objTable.Rows(C1).Item("sretitemqty")) & ", 0, 0, 0, 0, 0, 'QL_trnsretitemdtl', '" & Session("trndate") & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', '1/1/1900')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            crdmatoid += 1
                        End If
                        If DDLReturnType.SelectedValue = "Retur Ganti Barang" Then
                            sSql = "UPDATE QL_trnsoitemdtl SET soitemdtlstatus='' WHERE cmpcode='" & AppCmpCode.Text & "' AND soitemdtloid IN (SELECT soitemdtloid FROM QL_trnshipmentitemdtl dord INNER JOIN QL_trnsretitemdtl srtd ON dord.doitemdtloid=srtd.doitemdtloid WHERE srtd.cmpcode='" & AppCmpCode.Text & "' AND sretitemdtloid=" & objTable.Rows(C1).Item("sretitemdtloid") & ")"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                    Next
                    If DDLReturnType.SelectedValue = "Retur Ganti Barang" Then
                        sSql = "UPDATE QL_trnsoitemmst SET soitemmststatus='Approved' WHERE cmpcode='" & AppCmpCode.Text & "' AND soitemmstoid IN (SELECT soitemmstoid FROM QL_trnsretitemmst srtm WHERE cmpcode='" & AppCmpCode.Text & "' AND srtm.sretitemmstoid=" & Session("trnoid") & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                    sSql = "UPDATE QL_mstoid SET lastoid=" & conmtroid - 1 & " WHERE tablename='QL_CONMAT' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & crdmatoid - 1 & " WHERE tablename='QL_CRDMTR' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "Sales Return Fixed Assets Approval"
    Private Sub SetGVListSretAsset()
        Dim arHeader() As String = {"Draft No.", "Return Date", "Customer", "Return Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "custname", "sretassetmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "custname", "sretassetmstnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT sret.cmpcode, sret.sretassetmstoid AS trnoid, sret.sretassetno, CONVERT(VARCHAR(10), sret.sretassetdate, 101) AS trndate, c.custname, sret.sretassetmstnote, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate FROM QL_trnsretassetmst sret INNER JOIN QL_approval ap ON sret.sretassetmstoid=ap.oid AND ap.statusrequest='New' INNER JOIN QL_mstcust c ON c.custoid=sret.custoid WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_trnsretassetmst' AND sret.sretassetmststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnsretassetmst")
    End Sub

    Private Sub SetGVDataSretAsset(ByVal dtDataHeader As DataTable)
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "Return Date", "Customer", "Return Note"}
        Dim arValue() As String = {"trnoid", "trndate", "custname", "sretassetmstnote"}
        Dim arType() As String = {"S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 2)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arHeader() As String = {"No.", "Assets No.", "Assets Desc.", "Quantity", "Unit", "Warehouse", "Note"}
        Dim arField() As String = {"sretassetdtlseq", "assetno", "assetlongdesc", "sretassetqty", "unit", "warehouse", "sretassetdtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        sSql = "SELECT sretd.sretassetdtlseq, sretd.sretassetdtloid, sretd.assetmstoid, fa.assetno, (CASE fa.reftype WHEN 'General' THEN (SELECT x.matgenlongdesc FROM QL_mstmatgen x WHERE x.matgenoid=fa.refoid) WHEN 'Spare Part' THEN (SELECT x.sparepartlongdesc FROM QL_mstsparepart x WHERE x.sparepartoid=fa.refoid) ELSE '' END) AS assetlongdesc, sretd.sretassetqty, g.gendesc AS unit, g2.gendesc AS warehouse, sretd.sretassetdtlnote, sretd.sretassetwhoid FROM QL_trnsretassetmst sret INNER JOIN QL_trnsretassetdtl sretd ON sret.sretassetmstoid=sretd.sretassetmstoid INNER JOIN QL_assetmst fa ON sretd.assetmstoid=fa.assetmstoid INNER JOIN QL_mstgen g ON sretd.sretassetunitoid=g.genoid INNER JOIN QL_mstgen g2 ON sretd.sretassetwhoid=g2.genoid WHERE sret.sretassetmstoid=" & Session("trnoid") & " ORDER BY sretd.sretassetdtlseq"
        Session("TblDtlSretAsset") = ckon.ambiltabel(sSql, "QL_trnsretassetdtl")
        gvData.DataSource = Session("TblDtlSretAsset")
        gvData.DataBind()
    End Sub

    Private Function GenerateSretAssetNo() As String
        Dim sNo As String = "SRTFA-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(sretassetno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnsretassetmst WHERE cmpcode='" & AppCmpCode.Text & "' AND sretassetno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdateSretAsset(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select Sales Return Fixed Assets Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select Sales Return Fixed Assets Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update Sales Return
            sSql = "UPDATE QL_trnsretassetmst SET sretassetno='" & sNo & "', sretassetmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP"
            If sStatus = "Approved" Then
                sSql &= ", sretassetmstres2='" & DDLReturnType.SelectedValue & "'"
            End If
            sSql &= " WHERE sretassetmststatus='In Approval' AND sretassetmstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnsretassetmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Rejected" Then
                    sSql = "UPDATE QL_trnshipmentassetdtl SET doassetdtlres3='' WHERE cmpcode='" & AppCmpCode.Text & "' AND doassetdtloid IN (SELECT doassetdtloid FROM QL_trnsretassetdtl WHERE cmpcode='" & AppCmpCode.Text & "' AND sretassetmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnshipmentassetmst SET doassetmstres3='' WHERE cmpcode='" & AppCmpCode.Text & "' AND doassetmstoid IN (SELECT DISTINCT doassetmstoid FROM QL_trnsretassetdtl WHERE cmpcode='" & AppCmpCode.Text & "' AND sretassetmstoid=" & Session("trnoid") & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                ElseIf sStatus = "Approved" Then
                    If DDLReturnType.SelectedValue = "Retur Ganti Barang" Then
                        sSql = "UPDATE QL_trnsoassetdtl SET soassetdtlstatus='' WHERE cmpcode='" & AppCmpCode.Text & "' AND soassetdtloid IN (SELECT soassetdtloid FROM QL_trnshipmentassetdtl dord INNER JOIN QL_trnsretassetdtl srtd ON dord.doassetdtloid=srtd.doassetdtloid WHERE srtd.cmpcode='" & AppCmpCode.Text & "' AND sretassetmstoid=" & Session("trnoid") & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        sSql = "UPDATE QL_trnsoassetmst SET soassetmststatus='Approved' WHERE cmpcode='" & AppCmpCode.Text & "' AND soassetmstoid IN (SELECT soassetmstoid FROM QL_trnsretassetmst srtm WHERE cmpcode='" & AppCmpCode.Text & "' AND srtm.sretassetmstoid=" & Session("trnoid") & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "Berita Acara Approval"
    Private Sub SetGVListBA()
        Dim arHeader() As String = {"Draft No.", "BA Date", "SPK No.", "Dept.", "Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "wono", "deptname", "acaramstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "wono", "deptname", "acaramstnote", "updtime", "upduser", "acaratype", "acarawhoid", "deptoid", "wodesc"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT ba.cmpcode, ba.acaramstoid AS trnoid, ba.acarano, CONVERT(VARCHAR(10), ba.acaradate, 101) AS trndate, wom.wono, d.deptname, ba.acaramstnote, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, ba.acaratype, ba.updtime, ba.upduser, ba.acarawhoid, ba.deptoid, womstres2 AS wodesc FROM QL_trnbrtacaramst ba INNER JOIN QL_trnwomst wom ON wom.womstoid=ba.womstoid INNER JOIN QL_mstdept d ON d.deptoid=ba.deptoid INNER JOIN QL_approval ap ON ap.oid=ba.acaramstoid AND ap.statusrequest='New' WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_trnbrtacaramst' AND ba.acaramststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnbrtacaramst")
    End Sub

    Private Sub SetGVDataBA(ByVal dtDataHeader As DataTable)
        Session("acaratype") = gvList.SelectedDataKey.Item("acaratype").ToString
        Session("acarawhoid") = gvList.SelectedDataKey.Item("acarawhoid")
        Session("deptoid") = gvList.SelectedDataKey.Item("deptoid")
        Session("wono") = gvList.SelectedDataKey.Item("wono")
        Session("acaratype") = gvList.SelectedDataKey.Item("acaratype")
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "BA Date", "SPK No.", "Finish Good", "Dept.", "Note", "Last Update By", "Last Update On", "Acara Type"}
        Dim arValue() As String = {"trnoid", "trndate", "wono", "wodesc", "deptname", "acaramstnote", "upduser", "updtime", "acaratype"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 4)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arHeader() As String = {"No.", "Type", "Code", "Description", "Qty", "Unit", "Note", "Code SPK", "Description SPK", "Qty SPK", "Unit SPK"}
        Dim arField() As String = {"acaradtlseq", "acarareftype", "matcode", "matlongdesc", "acaraqty", "acaraunit", "acaradtlnote", "matcode_kik", "matlongdesc_kik", "acaraqty_kik", "acarunit_kik"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        If Session("acaratype") <> "Material Transform" Then
            gvData.Columns(gvData.Columns.Count - 1).Visible = False
            gvData.Columns(gvData.Columns.Count - 2).Visible = False
            gvData.Columns(gvData.Columns.Count - 3).Visible = False
            gvData.Columns(gvData.Columns.Count - 4).Visible = False
        End If

        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sSql = "SELECT bad.acaradtlseq, acaradtloid, bad.acarareftype, bad.acararefoid, (SELECT m.itemCode FROM QL_mstitem m WHERE m.itemoid=bad.acararefoid) AS matcode, (SELECT m.itemLongDescription FROM QL_mstitem m WHERE m.itemoid=bad.acararefoid) AS matlongdesc, bad.acaraqty, g.gendesc AS acaraunit, bad.acaradtlnote, (select dbo.getStockValue(bad.acararefoid)) acaravalueidr, ISNULL((SELECT SUM(ISNULL(stockqty, 0) * ISNULL(stockvalueusd, 0)) / NULLIF(SUM(stockqty),0) FROM QL_stockvalue st WHERE st.cmpcode=bad.cmpcode AND st.periodacctg IN ('" & sPeriod & "', '" & GetLastPeriod(sPeriod) & "') AND st.refoid=bad.acararefoid AND closeflag=''), 0.0) AS acaravalueusd, ISNULL((SELECT m.itemCode FROM QL_mstitem m WHERE m.itemoid=bad.matoid_kik),'') AS matcode_kik, ISNULL((SELECT m.itemLongDescription FROM QL_mstitem m WHERE m.itemoid=bad.matoid_kik),'') AS matlongdesc_kik, bad.matqty_kik AS acaraqty_kik, bad.matunitoid_kik, ISNULL(gx.gendesc,'') AS acarunit_kik, acaraqty_unitkecil, acaraqty_unitbesar FROM QL_trnbrtacaradtl bad INNER JOIN QL_mstgen g ON g.genoid=bad.acaraunitoid LEFT JOIN QL_mstgen gx ON gx.cmpcode=bad.cmpcode AND gx.genoid=bad.matunitoid_kik WHERE bad.acaramstoid=" & Session("trnoid") & " ORDER BY bad.acaradtlseq"
        Dim dtData As DataTable = ckon.ambiltabel(sSql, "QL_trnbrtacaradtl")
        For C1 As Integer = 0 To dtData.Rows.Count - 1
            If ToDouble(dtData.Rows(C1)("acaravalueidr")) <= 0 Then
                dtData.Rows(C1)("acaravalueidr") = GetBackupValueIDR(AppCmpCode.Text, dtData.Rows(C1)("acararefoid").ToString, sPeriod)
            End If
        Next
        dtData.AcceptChanges()
        Session("TblDtlBA") = dtData
        gvData.DataSource = Session("TblDtlBA")
        gvData.DataBind()
    End Sub

    Private Function GenerateBANo() As String
        Dim sNo As String = "BA-" & Format(GetServerTime(), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(acarano, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnbrtacaramst WHERE cmpcode='" & AppCmpCode.Text & "' AND acarano LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdateBA(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select Berita Acara Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select Berita Acara Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
        Dim conmtroid As Integer, crdmatoid As Integer, iGLMstOid As Integer, iGLDtlOid As Integer, iStockValOid As Integer
        Dim arAcctgOid() As Integer = {0, 0, 0}
        Dim arPostAmtIDR() As Double = {0, 0, 0}
        Dim arPostAmtUSD() As Double = {0, 0, 0}
        Dim iGroupOid As Integer = 0
        If sStatus = "Approved" Then
            conmtroid = GenerateID("QL_CONSTOCK", CompnyCode)
            crdmatoid = GenerateID("QL_CRDSTOCK", CompnyCode)
            iGLMstOid = GenerateID("QL_TRNGLMST", CompnyCode)
            iGLDtlOid = GenerateID("QL_TRNGLDTL", CompnyCode)
            iStockValOid = GenerateID("QL_STOCKVALUE", CompnyCode)
            arAcctgOid(0) = GetAcctgStock("WIP")
            arAcctgOid(1) = GetAcctgStock("FG")
            arAcctgOid(2) = GetAcctgStock("RAW")
            'arAcctgOid(3) = GetAcctgStock("GEN")

            If Session("acaratype") <> "Material Return" Then
                Dim dt As DataTable = Session("TblDtlBA")
                Dim sErr As String = ""
                For C1 As Integer = 0 To dt.Rows.Count - 1
                    Dim sRef As String = ""
                    If dt.Rows(C1)("acarareftype").ToString.ToUpper = "RAW" Then
                        sRef = "RAW MATERIAL"
                    ElseIf dt.Rows(C1)("acarareftype").ToString.ToUpper = "GENERAL" Then
                        sRef = "GENERAL MATERIAL"
                    ElseIf dt.Rows(C1)("acarareftype").ToString.ToUpper = "FINISH GOOD" Then
                        sRef = "FINISH GOOD"
                    ElseIf dt.Rows(C1)("acarareftype").ToString.ToUpper = "WIP" Then
                        sRef = "WIP"
                    End If
                    If Not IsStockAvailable(AppCmpCode.Text, sPeriod, dt.Rows(C1)("acararefoid"), Session("acarawhoid"), ToDouble(dt.Rows(C1)("acaraqty").ToString), sRef) Then
                        sErr &= "- Qty Detail BA Code : " & dt.Rows(C1)("matcode").ToString & " must be less than Stock Qty!<BR>"
                    End If
                Next
                If sErr <> "" Then
                    showMessage(sErr, CompnyName & " - WARNING", 2) : Exit Sub
                End If
            End If
            iGroupOid = ToInteger(GetStrData("SELECT TOP 1 groupoid FROM QL_mstdeptgroupdtl WHERE cmpcode='" & AppCmpCode.Text & "' AND deptoid=" & Session("deptoid") & " ORDER BY updtime DESC"))
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update Berita Acara
            sSql = "UPDATE QL_trnbrtacaramst SET acarano='" & sNo & "', acaramststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE acaramststatus='In Approval' AND acaramstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnbrtacaramst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Rejected" Then
                    sSql = "UPDATE QL_trnwodtl3 SET wodtl3reqflag='' WHERE cmpcode='" & AppCmpCode.Text & "' AND wodtl3oid IN (SELECT wodtl3oid FROM QL_trnbrtacaradtl WHERE cmpcode='" & AppCmpCode.Text & "' AND acaramstoid=" & Session("trnoid") & " AND wodtl3oid<>0)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnwodtl2 SET wodtl2reqflag='' WHERE cmpcode='" & AppCmpCode.Text & "' AND wodtl2oid IN (SELECT wodtl2oid FROM QL_trnbrtacaradtl WHERE cmpcode='" & AppCmpCode.Text & "' AND acaramstoid=" & Session("trnoid") & " AND wodtl2oid<>0)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                ElseIf sStatus = "Approved" Then
                    Dim objTable As DataTable = Session("TblDtlBA")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        Dim sRef As String = ""
                        arPostAmtIDR(0) += ToDouble(objTable.Rows(C1)("acaravalueidr").ToString) * ToDouble(objTable.Rows(C1)("acaraqty").ToString)
                        arPostAmtUSD(0) += ToDouble(objTable.Rows(C1)("acaravalueusd").ToString) * ToDouble(objTable.Rows(C1)("acaraqty").ToString)
                        If objTable.Rows(C1)("acarareftype").ToString = "Finish Good" Then
                            sRef = "FINISH GOOD"
                            arPostAmtIDR(1) += ToDouble(objTable.Rows(C1)("acaravalueidr").ToString) * ToDouble(objTable.Rows(C1)("acaraqty").ToString)
                            arPostAmtUSD(1) += ToDouble(objTable.Rows(C1)("acaravalueusd").ToString) * ToDouble(objTable.Rows(C1)("acaraqty").ToString)
                        ElseIf objTable.Rows(C1)("acarareftype").ToString = "Raw" Then
                            sRef = "RAW MATERIAL"
                            arPostAmtIDR(2) += ToDouble(objTable.Rows(C1)("acaravalueidr").ToString) * ToDouble(objTable.Rows(C1)("acaraqty").ToString)
                            arPostAmtUSD(2) += ToDouble(objTable.Rows(C1)("acaravalueusd").ToString) * ToDouble(objTable.Rows(C1)("acaraqty").ToString)
                        End If
                        sSql = "UPDATE QL_trnbrtacaradtl SET acaravalueidr=" & ToDouble(objTable.Rows(C1).Item("acaravalueidr")) & " WHERE cmpcode='" & AppCmpCode.Text & "' AND acaradtloid=" & objTable.Rows(C1).Item("acaradtloid")
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()

                        If Session("acaratype") <> "Material Return" Then
                            ' Insert QL_conmat
                            sSql = "INSERT INTO QL_constock (cmpcode, constockoid, contype, typemin, trndate, periodacctg, formaction, formoid, refoid, refname, mtrlocoid, qtyin, qtyout, reason, note, upduser, updtime, refno, deptoid, qtyin_unitbesar, qtyout_unitbesar, valueidr, valueusd) VALUES ('" & AppCmpCode.Text & "', " & conmtroid & ", 'BA-', -1, '" & sDate & "', '" & sPeriod & "', 'QL_trnbrtacaradtl', " & Session("trnoid") & ", " & objTable.Rows(C1).Item("acararefoid") & ", '" & sRef & "', " & Session("acarawhoid") & ", 0, " & ToDouble(objTable.Rows(C1).Item("acaraqty").ToString) & ", 'Berita Acara - " & Session("acaratype") & "', '" & sNo & " # Usage " & Session("wono") & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("wono") & "', " & Session("deptoid") & ", 0, " & ToDouble(objTable.Rows(C1).Item("acaraqty_unitbesar").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("acaravalueidr")) & ", " & ToDouble(objTable.Rows(C1).Item("acaravalueusd")) & ")"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            conmtroid += 1
                            ' Insert QL_crdmtr
                            sSql = "UPDATE QL_crdstock SET qtyout=qtyout + " & ToDouble(objTable.Rows(C1).Item("acaraqty").ToString) & ", saldoakhir=saldoakhir - " & ToDouble(objTable.Rows(C1).Item("acaraqty").ToString) & ", lasttranstype='QL_trnbrtacaradtl', lasttransdate='" & sDate & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & AppCmpCode.Text & "' AND refoid=" & objTable.Rows(C1).Item("acararefoid") & " AND mtrlocoid=" & Session("acarawhoid") & " AND periodacctg='" & sPeriod & "'"
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() = 0 Then
                                sSql = "INSERT INTO QL_crdstock (cmpcode, crdstockoid, periodacctg, refoid, refname, mtrlocoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir, lasttranstype, lasttransdate, upduser, updtime, createuser, createtime, closeuser, closingdate) VALUES ('" & AppCmpCode.Text & "', " & crdmatoid & ", '" & sPeriod & "', " & objTable.Rows(C1).Item("acararefoid") & ", '" & sRef & "', " & Session("acarawhoid") & ", 0, " & ToDouble(objTable.Rows(C1).Item("acaraqty").ToString) & ", 0, 0, 0, " & -ToDouble(objTable.Rows(C1).Item("acaraqty").ToString) & ", 'QL_trnbrtacaradtl', '" & sDate & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', '1/1/1900')"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                crdmatoid += 1
                            End If

                            'Insert STockvalue
                            sSql = GetQueryUpdateStockValue(-ToDouble(objTable.Rows(C1)("acaraqty").ToString), 0, ToDouble(objTable.Rows(C1).Item("acaravalueidr").ToString), ToDouble(objTable.Rows(C1).Item("acaravalueusd").ToString), "QL_trnbrtacaradtl", sDate, Session("UserID"), DDLBusUnit.SelectedValue, sPeriod, objTable.Rows(C1)("acararefoid"))
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() = 0 Then
                                sSql = GetQueryInsertStockValue(-ToDouble(objTable.Rows(C1)("acaraqty").ToString), 0, ToDouble(objTable.Rows(C1).Item("acaravalueidr").ToString), ToDouble(objTable.Rows(C1).Item("acaravalueusd").ToString), "QL_trnbrtacaradtl", sDate, Session("UserID"), DDLBusUnit.SelectedValue, sPeriod, objTable.Rows(C1)("acararefoid"), iStockValOid)
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iStockValOid += 1
                            End If
                        Else
                            ' Insert QL_conmat
                            sSql = "INSERT INTO QL_constock (cmpcode, constockoid, contype, typemin, trndate, periodacctg, formaction, formoid, refoid, refname, mtrlocoid, qtyin, qtyout, reason, note, upduser, updtime, refno, deptoid, valueidr, valueusd) VALUES ('" & AppCmpCode.Text & "', " & conmtroid & ", 'BA+', 1, '" & sDate & "', '" & sPeriod & "', 'QL_trnbrtacaradtl', " & Session("trnoid") & ", " & objTable.Rows(C1).Item("acararefoid") & ", '" & sRef & "', " & Session("acarawhoid") & ", " & ToDouble(objTable.Rows(C1).Item("acaraqty").ToString) & ", 0, 'Berita Acara - Material Return', '" & sNo & " # Retur " & Session("wono") & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("wono") & "', " & Session("deptoid") & ", " & ToDouble(objTable.Rows(C1).Item("acaravalueidr")) & ", " & ToDouble(objTable.Rows(C1).Item("acaravalueusd")) & ")"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            conmtroid += 1
                            ' Insert QL_crdmtr
                            sSql = "UPDATE QL_crdstock SET qtyin=qtyin + " & ToDouble(objTable.Rows(C1).Item("acaraqty").ToString) & ", saldoakhir=saldoakhir + " & ToDouble(objTable.Rows(C1).Item("acaraqty").ToString) & ", lasttranstype='QL_trnbrtacaradtl', lasttransdate='" & sDate & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & AppCmpCode.Text & "' AND refoid=" & objTable.Rows(C1).Item("acararefoid") & " AND mtrlocoid=" & Session("acarawhoid") & " AND periodacctg='" & sPeriod & "'"
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() = 0 Then
                                sSql = "INSERT INTO QL_crdstock (cmpcode, crdstockoid, periodacctg, refoid, refname, mtrlocoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir, lasttranstype, lasttransdate, upduser, updtime, createuser, createtime, closeuser, closingdate) VALUES ('" & AppCmpCode.Text & "', " & crdmatoid & ", '" & sPeriod & "', " & objTable.Rows(C1).Item("acararefoid") & ", '" & sRef & "', " & Session("acarawhoid") & ", " & ToDouble(objTable.Rows(C1).Item("acaraqty").ToString) & ", 0, 0, 0, 0, " & ToDouble(objTable.Rows(C1).Item("acaraqty").ToString) & ", 'QL_trnbrtacaradtl', '" & sDate & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', '1/1/1900')"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                crdmatoid += 1
                            End If

                            'Insert STockvalue
                            sSql = GetQueryUpdateStockValue(ToDouble(objTable.Rows(C1)("acaraqty").ToString), 0, ToDouble(objTable.Rows(C1).Item("acaravalueidr").ToString), ToDouble(objTable.Rows(C1).Item("acaravalueusd").ToString), "QL_trnbrtacaradtl", sDate, Session("UserID"), DDLBusUnit.SelectedValue, sPeriod, objTable.Rows(C1)("acararefoid"))
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() = 0 Then
                                sSql = GetQueryInsertStockValue(ToDouble(objTable.Rows(C1)("acaraqty").ToString), 0, ToDouble(objTable.Rows(C1).Item("acaravalueidr").ToString), ToDouble(objTable.Rows(C1).Item("acaravalueusd").ToString), "QL_trnbrtacaradtl", sDate, Session("UserID"), DDLBusUnit.SelectedValue, sPeriod, objTable.Rows(C1)("acararefoid"), iStockValOid)
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iStockValOid += 1
                            End If
                        End If
                    Next
                    Dim dTotalIDR As Double = arPostAmtIDR(0)
                    Dim dTotalUSD As Double = arPostAmtUSD(0)
                    Dim arDBCR() As String = {"D", "C", "C"}
                    Dim arDBCR2() As String = {"C", "D", "D"}
                    Dim sNote As String = "BA Usage"
                    If Session("acaratype") = "Material Return" Then
                        Dim i As Integer = arAcctgOid(0)
                        Dim d1 As Double = arPostAmtIDR(0)
                        Dim d2 As Double = arPostAmtUSD(0)
                        For C1 As Integer = 0 To arAcctgOid.Length - 1
                            arAcctgOid(C1) = arAcctgOid(C1)
                            arPostAmtIDR(C1) = arPostAmtIDR(C1)
                            arPostAmtUSD(C1) = arPostAmtUSD(C1)
                            arDBCR(C1) = "D"
                        Next
                        arAcctgOid(0) = i
                        arPostAmtIDR(0) = d1
                        arPostAmtUSD(0) = d2
                        sNote = "BA Return"
                    ElseIf Session("acaratype") = "Material Transform" Then
                        sNote = "BA Transform"
                    End If
                    If dTotalIDR > 0 Or dTotalUSD > 0 Then
                        ' Insert QL_trnglmst
                        sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGLMstOid & ", '" & sDate & "', '" & sPeriod & "', '" & sNote & "|No. " & sNo & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 0, 0, 1, 1, 1, 1)"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        ' Insert QL_trngldtl
                        For C1 As Integer = 0 To arAcctgOid.Length - 1
                            If arPostAmtIDR(C1) > 0 Then
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, groupoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGLDtlOid & ", " & C1 + 1 & ", " & iGLMstOid & ", " & arAcctgOid(C1) & ", '" & IIf(Session("acaratype") = "Material Return", arDBCR2(C1), arDBCR(C1)) & "', " & arPostAmtIDR(C1) & ", '" & sNo & "', '" & sNote & "|No. " & sNo & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & arPostAmtIDR(C1) & ", " & arPostAmtUSD(C1) & ", 'QL_trnbrtacaramst " & Session("trnoid") & "', " & iGroupOid & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iGLDtlOid += 1
                            End If
                        Next
                    End If
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGLMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGLDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & conmtroid - 1 & " WHERE tablename='QL_CONSTOCK' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & crdmatoid - 1 & " WHERE tablename='QL_CRDSTOCK' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iStockValOid - 1 & " WHERE tablename='QL_STOCKVALUE' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch exSql As SqlException
            objTrans.Rollback() : conn.Close()
            If ToDouble(Session("ErrorCounter")) < 5 Then
                If exSql.Number = 2627 Then
                    Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                    UpdateBA(sNo, sStatus)
                    Exit Sub
                Else
                    showMessage(exSql.Message, CompnyName & " - ERROR", 1) : Exit Sub
                End If
            Else
                showMessage(exSql.Message, CompnyName & " - ERROR", 1) : Exit Sub
            End If
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "Berita Acara Product Approval"
    Private Sub SetGVListBAProduct()
        Dim arHeader() As String = {"Draft No.", "BA Date", "Group Name", "Note", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "suppname", "acaraprodmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "suppname", "acaraprodmstnote", "updtime", "upduser"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT ba.cmpcode, ba.acaraprodmstoid AS trnoid, ba.acaraprodno, CONVERT(VARCHAR(10), ba.acaraproddate, 101) AS trndate, ISNULL(suppname, 'None') AS suppname, ba.acaraprodmstnote, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, ba.updtime, ba.upduser FROM QL_trnbrtacaraprodmst ba LEFT JOIN QL_mstsupp s ON s.suppoid=ba.suppoid INNER JOIN QL_approval ap ON ap.oid=ba.acaraprodmstoid AND ap.statusrequest='New' WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_trnbrtacaraprodmst' AND ba.acaraprodmststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnbrtacaraprodmst")
    End Sub

    Private Sub SetGVDataBAProduct(ByVal dtDataHeader As DataTable)
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "BA Date", "Group Name", "Note", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "suppname", "acaraprodmstnote", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 4)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arHeader() As String = {"No.", "SPK No.", "Code FG", "Finish Good", "Department", "Qty", "Unit", "Note"}
        Dim arField() As String = {"acaraproddtlseq", "wono", "itemcode", "itemlongdesc", "deptname", "acaraprodqty", "acaraprodunit", "acaraproddtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sSql = "SELECT acaraproddtlseq, wono, itemcode, itemlongdesc, deptname, acaraprodqty, gendesc AS acaraprodunit, acaraproddtlnote FROM QL_trnbrtacaraproddtl bad INNER JOIN QL_trnwomst wom ON wom.womstoid=bad.womstoid INNER JOIN QL_trnwodtl1 wod ON wod.womstoid=wom.womstoid INNER JOIN QL_mstitem i ON i.itemoid=wod.itemoid INNER JOIN QL_mstgen g ON genoid=acaraprodunitoid INNER JOIN QL_mstdept de ON de.deptoid=bad.deptoid WHERE acaraprodmstoid=" & Session("trnoid") & " ORDER BY acaraproddtlseq"
        Session("TblDtlBAProduct") = ckon.ambiltabel(sSql, "QL_trnbrtacaraproddtl")
        gvData.DataSource = Session("TblDtlBAProduct")
        gvData.DataBind()
    End Sub

    Private Function GenerateBAProductNo() As String
        Dim sNo As String = "BAP-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(acaraprodno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnbrtacaraprodmst WHERE cmpcode='" & AppCmpCode.Text & "' AND acaraprodno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdateBAProduct(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select Berita Acara Product Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select Berita Acara Product Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update Berita Acara Product
            sSql = "UPDATE QL_trnbrtacaraprodmst SET acaraprodno='" & sNo & "', acaraprodmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE acaraprodmststatus='In Approval' AND acaraprodmstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnbrtacaraprodmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "KIK Planning Date Approval"
    Private Sub SetGVListKIKPD()
        Dim arHeader() As String = {"Planning No.", "Note", "Request User", "Request Date"}
        Dim arField() As String = {"planno", "planmstnote", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "planno", "planmstnote", "updtime", "upduser"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT pm.cmpcode, pm.planmstoid AS trnoid, pm.planno, CONVERT(VARCHAR(10), pm.createtime, 101) AS trndate, pm.planmstnote, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, pm.updtime, pm.upduser FROM QL_trnplanmst pm INNER JOIN QL_approval ap ON ap.oid=pm.planmstoid AND ap.statusrequest='New' WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_trnplanmst' AND pm.planmststatus='In Approval' ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnplanmst")
    End Sub

    Private Sub SetGVDataKIKPD(ByVal dtDataHeader As DataTable)
        Dim dr As DataRow
        Dim arCaption() As String = {"Planning No.", "Note", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"planno", "planmstnote", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 4)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        Dim arHeader() As String = {"No.", "KIK No.", "Finish Good", "KIK Qty", "Prep.", "Proc.", "Sand.", "Rakit", "Fin. Sand.", "Fin.", "Pack.", "Note"}
        Dim arField() As String = {"plandtlseq", "wono", "itemlongdesc", "wodtl1qty", "planprepdate", "planprocdate", "plansanddate", "planrakitdate", "planfinsanddate", "planfindate", "planpackdate", "plandtlnote"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvData.Columns.Add(field)
        Next
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sSql = "SELECT pd.plandtlseq, wom.wono, i.itemlongdesc, wod1.wodtl1qty, CONVERT(VARCHAR(10), pd.planprepdate, 101) AS planprepdate, CONVERT(VARCHAR(10), pd.planprocdate, 101) AS planprocdate, CONVERT(VARCHAR(10), pd.plansanddate, 101) AS plansanddate, CONVERT(VARCHAR(10), pd.planrakitdate, 101) AS planrakitdate, CONVERT(VARCHAR(10), pd.planfinsanddate, 101) AS planfinsanddate, CONVERT(VARCHAR(10), pd.planfindate, 101) AS planfindate, CONVERT(VARCHAR(10), pd.planpackdate, 101) AS planpackdate, pd.plandtlnote, pd.plandtloid, pd.planmstoid, pd.womstoid FROM QL_trnplandtl pd INNER JOIN QL_trnwomst wom ON wom.cmpcode=pd.cmpcode AND wom.womstoid=pd.womstoid INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=wom.cmpcode AND wod1.womstoid=wom.womstoid INNER JOIN QL_mstitem i ON i.itemoid=wod1.itemoid WHERE pd.planmstoid=" & Session("trnoid") & " ORDER BY pd.plandtlseq"
        Session("TblDtlKIKPD") = ckon.ambiltabel(sSql, "QL_trnplandtl")
        gvData.DataSource = Session("TblDtlKIKPD")
        gvData.DataBind()
    End Sub

    Private Sub UpdateKIKPD(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select KIK Planning Date Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select KIK Planning Date Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update KIK Planning Date
            sSql = "UPDATE QL_trnplanmst SET planmststatus='" & sStatus & "', approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE planmststatus='In Approval' AND planmstoid=" & Session("trnoid")
            xCmd.CommandText = sSql
            If xCmd.ExecuteNonQuery() > 0 Then
                isExec = True
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnplanmst' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If isExec Then
                If sStatus = "Approved" Then
                    Dim objTable As DataTable = Session("TblDtlKIKPD")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "UPDATE QL_trnwomst SET woplandate='" & objTable.Rows(C1)("planprepdate") & "', woprocdate='" & objTable.Rows(C1)("planprocdate") & "', wosanddate='" & objTable.Rows(C1)("plansanddate") & "', worakitdate='" & objTable.Rows(C1)("planrakitdate") & "', wofinsanddate='" & objTable.Rows(C1)("planfinsanddate") & "', wofindate='" & objTable.Rows(C1)("planfindate") & "', wopackdate='" & objTable.Rows(C1)("planpackdate") & "', lastplandtloid=" & objTable.Rows(C1)("plandtloid") & ", lastplanmstoid=" & objTable.Rows(C1)("planmstoid") & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid=" & objTable.Rows(C1)("womstoid")
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region

#Region "Material Stock Adjustment Approval"

    Private Sub SetGVListMA()
        Dim arHeader() As String = {"Draft No.", "Adj. Date", "Type", "Request User", "Request Date"}
        Dim arField() As String = {"trnoid", "trndate", "stockadjtype", "requestuser", "requestdate"}
        Dim arKey() As String = {"cmpcode", "trnoid", "approvaloid", "trndate", "stockadjtype", "adjcount", "updtime", "upduser"}
        For C1 As Int16 = 0 To arHeader.Length - 1
            Dim field As New System.Web.UI.WebControls.BoundField
            field.HeaderText = arHeader(C1)
            field.DataField = arField(C1)
            field.HeaderStyle.CssClass = "gvhdr"
            field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
            gvList.Columns.Add(field)
        Next
        gvList.DataKeyNames = arKey
        sSql = "SELECT DISTINCT a.cmpcode, CAST(resfield1 AS INTEGER) AS trnoid, stockadjno, CONVERT(VARCHAR(10), stockadjdate, 101) AS trndate, COUNT(-1) AS adjcount, ap.approvaloid, ap.requestuser, CONVERT(VARCHAR(10), ap.requestdate, 101) AS requestdate, ap.requestdate, MAX(a.updtime) AS updtime, a.upduser,  adjtype AS stockadjtype FROM QL_trnstockadj a INNER JOIN QL_approval ap ON ap.oid=CAST(resfield1 AS INTEGER) AND ap.statusrequest='New' WHERE ap.event='In Approval' AND ap.approvaluser='" & Session("UserID") & "' AND ap.tablename='QL_trnstockadj' AND stockadjstatus='In Approval' GROUP BY a.cmpcode, resfield1, stockadjno, stockadjdate, ap.approvaloid, ap.requestuser, ap.requestdate, a.upduser, adjtype ORDER BY ap.requestdate DESC"
        FillGV(gvList, Session("TblGVList"), sSql, "QL_trnstockadj")
    End Sub

    Private Sub SetGVDataMA(ByVal dtDataHeader As DataTable)
        lblreason.Visible = True : lblSeptReason.Visible = True : ActionReason.Visible = True
        Dim dr As DataRow
        Dim arCaption() As String = {"Draft No.", "Adj. Date", "Type", "Total Adj.", "Last Update By", "Last Update On"}
        Dim arValue() As String = {"trnoid", "trndate", "stockadjtype", "adjcount", "upduser", "updtime"}
        Dim arType() As String = {"S", "S", "S", "D", "S", "S"}
        Dim arHidden() As String = {"N", "N", "N", "N", "N", "N"}
        For C1 As Int16 = 0 To arCaption.Length - 1
            dr = dtDataHeader.NewRow
            dr("datacaption") = arCaption(C1)
            dr("datasept") = ":"
            If arType(C1) = "S" Then
                dr("datavalue") = gvList.SelectedDataKey.Item(arValue(C1)).ToString
            Else
                dr("datavalue") = ToMaskEdit(ToDouble(gvList.SelectedDataKey.Item(arValue(C1)).ToString), 4)
            End If
            dtDataHeader.Rows.Add(dr)
        Next
        gvDataHeader.DataSource = dtDataHeader
        gvDataHeader.DataBind()
        For C1 As Int16 = 0 To arHidden.Length - 1
            If arHidden(C1) = "Y" Then
                gvDataHeader.Rows(C1).Visible = False
            End If
        Next
        'Dim arHeader() As String = {"No.", "Type", "Code", "Description", "Location", "Current Qty", "New Qty", "Unit", "Note"}
        'Dim arField() As String = {"seq", "refname", "refcode", "reflongdesc", "location", "lastqty", "adjqty", "unit", "stockadjnote"}
        'For C1 As Int16 = 0 To arHeader.Length - 1
        '    Dim field As New System.Web.UI.WebControls.BoundField
        '    field.HeaderText = arHeader(C1)
        '    field.DataField = arField(C1)
        '    field.HeaderStyle.CssClass = "gvhdr"
        '    field.HeaderStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
        '    field.ItemStyle.Font.Size = System.Web.UI.WebControls.FontUnit.XSmall
        '    gvData.Columns.Add(field)
        'Next
        gvDataAdj.Visible = True : gvData.Visible = False
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sSql = "SELECT 0 seq, stockadjoid AS oid, a.refoid, a.refname, (SELECT m.itemCode FROM QL_mstitem m WHERE m.itemoid=a.refoid) AS refcode, (SELECT m.itemoldcode FROM QL_mstitem m WHERE m.itemoid=a.refoid) AS itemoldcode, (SELECT m.itemShortDescription FROM QL_mstitem m WHERE m.itemoid=a.refoid) AS reflongdesc, a.mtrwhoid, g1.gendesc AS location, a.stockadjqtybefore AS lastqty, a.stockadjqty AS adjqty, CONVERT(DECIMAL (18,2), (CASE adjtype WHEN 'ADJUSTMENT' THEN (select dbo.getStockValue(a.refoid)) ELSE 0.0 END)) AS stockvalueidr, 0.0 AS stockvalueusd, g2.gendesc AS unit, (CASE a.refname WHEN 'RAW MATERIAL' THEN 0 WHEN 'FINISH GOOD' THEN 1 ELSE -1 END) AS stockacctgoid, (CASE a.refname WHEN 'RAW MATERIAL' THEN 'RMA' WHEN 'FINISH GOOD' THEN 'FGA' ELSE '' END) AS adjtype, (select dbo.getstockqty(a.refoid, a.mtrwhoid)) AS laststock, stockadjnote, adjtype AS stockadjtype, stockadjnote AS note FROM QL_trnstockadj a INNER JOIN QL_mstgen g1 ON g1.genoid=a.mtrwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=a.stockadjunit WHERE a.cmpcode='" & AppCmpCode.Text & "' AND a.resfield1='" & Session("trnoid") & "' ORDER BY a.stockadjoid"
        Dim dtTmp As DataTable = ckon.ambiltabel(sSql, "QL_trnstockadjdtl")
        For C1 As Integer = 0 To dtTmp.Rows.Count - 1
            dtTmp.Rows(C1)("seq") = C1 + 1
            If dtTmp.Rows(C1)("stockadjtype").ToString.ToUpper = "ADJUSTMENT" Then
                If ToDouble(dtTmp.Rows(C1)("stockvalueidr").ToString) <= 0 Then
                    dtTmp.Rows(C1)("stockvalueidr") = ToDouble(GetStrData("SELECT TOP 1 valueidr FROM QL_constock WHERE cmpcode='" & AppCmpCode.Text & "' AND refoid=" & dtTmp.Rows(C1)("refoid") & " AND periodacctg<='" & sPeriod & "' AND valueidr>0 ORDER BY updtime DESC"))
                End If
                If ToDouble(dtTmp.Rows(C1)("stockvalueusd").ToString) <= 0 Then
                    dtTmp.Rows(C1)("stockvalueusd") = ToDouble(GetStrData("SELECT TOP 1 valueusd FROM QL_constock WHERE cmpcode='" & AppCmpCode.Text & "' AND refoid=" & dtTmp.Rows(C1)("refoid") & " AND periodacctg<='" & sPeriod & "' AND valueusd>0 ORDER BY updtime DESC"))
                End If
            End If
        Next
        dtTmp.AcceptChanges()
        Session("TblDtlMSA") = dtTmp
        gvDataAdj.DataSource = Session("TblDtlMSA")
        gvDataAdj.DataBind()
    End Sub

    Private Function GenerateMANo() As String
        Dim sNo As String = "ADJ-" & Format(GetServerTime(), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(stockadjno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnstockadj WHERE cmpcode='" & AppCmpCode.Text & "' AND stockadjno LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Sub UpdateMA(ByVal sNo As String, ByVal sStatus As String)
        If IsDBNull(Session("trnoid")) Then
            showMessage("Please select Material Stock Adjustment Data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trnoid") = "" Then
                showMessage("Please select Material Stock Adjustment Data first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim isExec As Boolean = False
        Dim cRate As New ClassRate
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
        Dim iConMtrOid As Integer, iCrdMatOid As Integer, iGLMstOid As Integer, iGLDtlOid As Integer, iStockValOid As Integer, iReasonAcctgOid As Integer
        Dim arStockAcctgOid() As Integer = {0, 0}
        Dim dt As DataTable = Session("TblDtlMSA")
        Dim dv As DataView = dt.DefaultView
        For C1 As Integer = 0 To gvDataAdj.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvDataAdj.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(8).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                        dv.RowFilter = "oid=" & CType(myControl, System.Web.UI.WebControls.TextBox).ToolTip
                        Dim dValue As Double = 0
                        dValue = ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text)
                        If dv.Count > 0 Then
                            dv(0)("stockvalueidr") = ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text)
                        End If
                        dv.RowFilter = ""
                    End If
                Next
            End If
        Next
        If sStatus = "Approved" Then
            cRate.SetRateValue(1, sDate)
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
           
            If DDLReturnType.SelectedValue = "" Then
                showMessage("Please select Reason first!", CompnyName & " - WARNING", 2) : Exit Sub
            End If
            iConMtrOid = GenerateID("QL_CONSTOCK", CompnyCode)
            iCrdMatOid = GenerateID("QL_CRDSTOCK", CompnyCode)
            iGLMstOid = GenerateID("QL_TRNGLMST", CompnyCode)
            iGLDtlOid = GenerateID("QL_TRNGLDTL", CompnyCode)
            iStockValOid = GenerateID("QL_STOCKVALUE", CompnyCode)
            iReasonAcctgOid = ToInteger(GetStrData("SELECT genother1 FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND genoid=" & DDLReturnType.SelectedValue & ""))
            arStockAcctgOid(0) = GetAcctgStock("RAW") 'GetAcctgOID(GetVarInterface("VAR_STOCK_RM", AppCmpCode.Text), CompnyCode)
            arStockAcctgOid(1) = GetAcctgStock("FG") 'GetAcctgOID(GetVarInterface("VAR_STOCK_GM", AppCmpCode.Text), CompnyCode)
           
            For C1 As Integer = 0 To dt.Rows.Count - 1
                If ToDouble(dt.Rows(C1)("lastqty").ToString) > ToDouble(dt.Rows(C1)("adjqty").ToString) Then
                    If ToDouble(dt.Rows(C1)("lastqty").ToString) - ToDouble(dt.Rows(C1)("adjqty").ToString) > ToMaskEdit(ToDouble(dt.Rows(C1)("laststock").ToString), 4) Then
                        showMessage("Every Stock Qty after adjustment must be more than 0!<BR>Detail : No. " & dt.Rows(C1)("seq").ToString & " -> Last stock before adjustment = " & ToMaskEdit(ToDouble(dt.Rows(C1)("laststock").ToString), 4) & ", stock out by adjustment = " & ToMaskEdit(ToDouble(dt.Rows(C1)("lastqty").ToString) - ToDouble(dt.Rows(C1)("adjqty").ToString), 4) & ", then last stock after adjustment = " & ToMaskEdit(ToDouble(dt.Rows(C1)("lastqty").ToString) - ToDouble(dt.Rows(C1)("adjqty").ToString) - ToDouble(dt.Rows(C1)("laststock").ToString), 4) & "", CompnyName & " - WARNING", 2) : Exit Sub
                    End If
                End If
                dt.Rows(C1)("stockacctgoid") = arStockAcctgOid(ToInteger(dt.Rows(C1)("stockacctgoid").ToString))
                If ToDouble(dt.Rows(C1)("stockvalueidr").ToString) <= 0 Then
                    showMessage("Every Stock Value IDR must be more than 0!<BR>Detail : No. " & dt.Rows(C1)("seq").ToString, CompnyName & " - WARNING", 2) : Exit Sub
                Else
                    If ToDouble(dt.Rows(C1)("stockvalueusd").ToString) <= 0 Then
                        dt.Rows(C1)("stockvalueusd") = ToDouble(dt.Rows(C1)("stockvalueidr").ToString) * cRate.GetRateMonthlyUSDValue
                    End If
                End If
            Next
        End If
        If sStatus = "Rejected" Or sStatus = "Revised" Then
            If ActionReason.Text = "" Then
                showMessage("Please Fill " & sStatus & " Reason First!", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='" & sStatus & "', event='" & sStatus & "', approvaldate=CURRENT_TIMESTAMP WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnstockadj' AND oid=" & Session("trnoid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            For C1 As Int16 = 0 To dt.Rows.Count - 1
                sSql = "UPDATE QL_trnstockadj SET stockadjstatus='" & sStatus & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP"
                If sStatus = "Approved" Then
                    sSql &= ", approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, stockadjno='" & sNo & "', reasonoid=" & DDLReturnType.SelectedValue & ", reasonacctgoid=" & iReasonAcctgOid & ", stockadjamtidr=" & ToDouble(dt.Rows(C1)("stockvalueidr").ToString) & ", stockadjamtusd=" & ToDouble(dt.Rows(C1)("stockvalueusd").ToString) & ", resfield2='" & dt.Rows(C1)("stockacctgoid").ToString & "'"
                ElseIf sStatus = "Rejected" Then
                    sSql &= ", rejectreason='" & Tchar(ActionReason.Text) & "', rejectuser='" & Session("UserID") & "', rejecttime=CURRENT_TIMESTAMP"
                Else
                    sSql &= ", revisereason='" & Tchar(ActionReason.Text) & "', reviseuser='" & Session("UserID") & "', revisetime=CURRENT_TIMESTAMP"
                End If
                sSql &= " WHERE cmpcode='" & AppCmpCode.Text & "' AND stockadjoid=" & dt.Rows(C1)("oid") & ""
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                If sStatus = "Approved" Then
                    Dim dQtyAdjIn As Decimal = 0 : Dim dQtyAdjOut As Decimal = 0
                    If ToDouble(dt.Rows(C1)("lastqty").ToString) > ToDouble(dt.Rows(C1)("adjqty").ToString) Then
                        dQtyAdjIn = 0
                        dQtyAdjOut = ToDouble(dt.Rows(C1)("lastqty").ToString) - ToDouble(dt.Rows(C1)("adjqty").ToString)
                    ElseIf ToDouble(dt.Rows(C1)("lastqty").ToString) < ToDouble(dt.Rows(C1)("adjqty").ToString) Then
                        dQtyAdjIn = ToDouble(dt.Rows(C1)("adjqty").ToString) - ToDouble(dt.Rows(C1)("lastqty").ToString)
                        dQtyAdjOut = 0
                    End If
                    ' Insert QL_conmat
                    sSql = "INSERT INTO QL_constock (cmpcode, constockoid, contype, trndate, periodacctg, formaction, formoid, refoid, refname, mtrlocoid, qtyin, qtyout, reason, upduser, updtime, typemin, note, valueidr, valueusd, createuser, createtime) VALUES ('" & AppCmpCode.Text & "', " & iConMtrOid & ", '" & dt.Rows(C1)("adjtype").ToString & "', '" & sDate & "', '" & sPeriod & "', 'QL_trnstockadj', " & dt.Rows(C1)("oid") & ", " & dt.Rows(C1)("refoid") & ", '" & dt.Rows(C1)("refname").ToString.ToUpper & "', " & dt.Rows(C1)("mtrwhoid") & ", " & dQtyAdjIn & ", " & dQtyAdjOut & ", 'STOCK ADJUSTMENT', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & IIf(dQtyAdjIn = 0, -1, 1) & ", '" & sNo & "', " & ToDouble(dt.Rows(C1)("stockvalueidr").ToString) & ", " & ToDouble(dt.Rows(C1)("stockvalueusd").ToString) & ", '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    iConMtrOid += 1
                End If
            Next
            If sStatus = "Approved" Then
                ' Insert QL_trnglmst
                sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & AppCmpCode.Text & "', " & iGLMstOid & ", '" & sDate & "', '" & sPeriod & "', 'Stock Adjustment|No. " & sNo & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                For C1 As Int16 = 0 To dt.Rows.Count - 1
                    Dim iCOADebet, iCOACredit As Integer
                    Dim dQty As Double = 0
                    If ToDouble(dt.Rows(C1)("lastqty").ToString) > ToDouble(dt.Rows(C1)("adjqty").ToString) Then
                        iCOADebet = iReasonAcctgOid : iCOACredit = ToInteger(dt.Rows(C1)("stockacctgoid").ToString)
                        dQty = ToDouble(dt.Rows(C1)("lastqty").ToString) - ToDouble(dt.Rows(C1)("adjqty").ToString)
                    Else
                        iCOADebet = ToInteger(dt.Rows(C1)("stockacctgoid").ToString) : iCOACredit = iReasonAcctgOid
                        dQty = ToDouble(dt.Rows(C1)("adjqty").ToString) - ToDouble(dt.Rows(C1)("lastqty").ToString)
                    End If
                    ' Insert QL_trngldtl
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glother1, glother2, glother3, glflag, upduser, updtime, glamtidr, glamtusd) VALUES ('" & AppCmpCode.Text & "', " & iGLDtlOid & ", 1, " & iGLMstOid & ", " & iCOADebet & ", 'D', " & ToDouble(dt.Rows(C1)("stockvalueidr").ToString) * dQty & ", '" & sNo & "', 'Stock Adjustment|No. " & sNo & "', 'QL_trnstockadj " & dt.Rows(C1)("oid").ToString & "', '', '', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dt.Rows(C1)("stockvalueidr").ToString) * dQty & ", " & ToDouble(dt.Rows(C1)("stockvalueusd").ToString) * dQty & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    iGLDtlOid += 1
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glother1, glother2, glother3, glflag, upduser, updtime, glamtidr, glamtusd) VALUES ('" & AppCmpCode.Text & "', " & iGLDtlOid & ", 2, " & iGLMstOid & ", " & iCOACredit & ", 'C', " & ToDouble(dt.Rows(C1)("stockvalueidr").ToString) * dQty & ", '" & sNo & "', 'Stock Adjustment|No. " & sNo & "', 'QL_trnstockadj " & dt.Rows(C1)("oid").ToString & "', '', '', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dt.Rows(C1)("stockvalueidr").ToString) * dQty & ", " & ToDouble(dt.Rows(C1)("stockvalueusd").ToString) * dQty & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    iGLDtlOid += 1
                Next
                sSql = "UPDATE QL_mstoid SET lastoid=" & iConMtrOid - 1 & " WHERE tablename='QL_CONSTOCK' AND cmpcode='" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & iGLMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & iGLDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit() : conn.Close()
        Catch exSql As SqlException
            objTrans.Rollback() : conn.Close()
            If ToDouble(Session("ErrorCounter")) < 5 Then
                If exSql.Number = 2627 Then
                    Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                    UpdateMA(sNo, sStatus)
                    Exit Sub
                Else
                    showMessage(exSql.Message, CompnyName & " - ERROR", 1) : Exit Sub
                End If
            Else
                showMessage(exSql.Message, CompnyName & " - ERROR", 1) : Exit Sub
            End If
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message & sSql, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        btnBack_Click(Nothing, Nothing)
    End Sub
#End Region
End Class
