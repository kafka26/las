Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_RawMaterialReceived
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
    Structure VarRate
        Public VarRateOid As Integer
        Public VarRateIDR As Double
        Public VarRateUSD As Double
    End Structure
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "registerdtloid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("mrqty") = ToDouble(GetTextBoxValue(row.Cells(7).Controls))
                                    dtView(0)("mrrawdtlnote") = GetTextBoxValue(row.Cells(8).Controls)
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl = dtView.ToTable
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "registerdtloid=" & cbOid
                                dtView2.RowFilter = "registerdtloid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("mrqty") = ToDouble(GetTextBoxValue(row.Cells(7).Controls))
                                    dtView(0)("mrrawdtlnote") = GetTextBoxValue(row.Cells(8).Controls)
                                    If dtView2.Count > 0 Then
                                        dtView2(0)("mrqty") = ToDouble(GetTextBoxValue(row.Cells(7).Controls))
                                        dtView2(0)("mrrawdtlnote") = GetTextBoxValue(row.Cells(8).Controls)
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If registerdtloid.Text = "" Then
            sError &= "- Please select MATERIAL field!<BR>"
        End If
        If mrqty.Text = "" Then
            sError &= "- Please fill RECEIVED QTY field!<BR>"
        Else
            If ToDouble(mrqty.Text) <= 0 Then
                sError &= "- RECEIVED QTY must be more than 0!<BR>"
            Else
                If ToDouble(mrrawqty.Text) >= 1000000 Then
                    sError &= "- MR QTY must be less than 1,000,000!<BR>"
                End If
                If ToDouble(matrawlimitqty.Text) > 0 Then
                    If Not IsQtyRounded(ToDouble(mrrawqty.Text), ToDouble(matrawlimitqty.Text)) Then
                        sError &= "- QUANTITY field must be rounded by ROUNDING QTY!<BR>"
                    End If
                End If
                If ToDouble(mrrawbonusqty.Text) >= 1000 Then
                    sError &= "- MR BONUS QTY must be less than 1,000!<BR>"
                End If
            End If
        End If
        If mrrawunitoid.SelectedValue = "" Then
            sError &= "- Please select UNIT field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If suppoid.Text = "" Then
            sError &= "- Please select SUPPLIER field!<BR>"
        End If
        If registermstoid.Text = "" Then
            sError &= "- Please select REG. NO. field!<BR>"
        End If
        If mrrawwhoid.SelectedValue = "" Then
            sError &= "- Please select WAREHOUSE field!<BR>"
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill DETAIL DATA!<BR>"
        Else
            Dim objTbl As DataTable = Session("TblDtl")
            If objTbl.Rows.Count <= 0 Then
                sError &= "- Please fill DETAIL DATA!<BR>"
            Else
                If DDLBusUnit.SelectedValue <> "" Then
                    For C1 As Int16 = 0 To objTbl.Rows.Count - 1
                        sSql = "SELECT (registerqty - ISNULL((SELECT SUM(mrrawqty) FROM QL_trnmrrawdtl mrd WHERE mrd.cmpcode=regd.cmpcode AND mrd.registerdtloid=regd.registerdtloid AND mrrawmstoid<>" & mrrawmstoid.Text & "), 0) - ISNULL((SELECT SUM(retd.retrawqty) FROM QL_trnretrawdtl retd WHERE retd.cmpcode=regd.cmpcode AND retd.registerdtloid=regd.registerdtloid), 0)) AS registerqty FROM QL_trnregisterdtl regd WHERE regd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND regd.registerdtloid=" & objTbl.Rows(C1)("registerdtloid").ToString
                        If ToDouble(objTbl.Rows(C1)("registerqty").ToString) <> ToDouble(GetStrData(sSql)) Then
                            objTbl.Rows(C1)("registerqty") = ToDouble(GetStrData(sSql))
                        End If
                    Next
                    objTbl.AcceptChanges()
                    For C1 As Int16 = 0 To objTbl.Rows.Count - 1
                        If objTbl.Rows(C1)("registerqty") = 0 Then
                            sError &= "- REG. QTY for some detail data has been updated by another user. Please check that every detail REG. QTY must be more than 0!<BR>"
                            Exit For
                        End If
                    Next
                    For C1 As Int16 = 0 To objTbl.Rows.Count - 1
                        If objTbl.Rows(C1)("mrrawbonusqty") >= 1000 Then
                            sError &= "- BONUS QTY for some detail data must be less than 1,000!<BR>"
                            Exit For
                        End If
                    Next
                    Session("TblDtl") = objTbl
                    gvListDtl.DataSource = Session("TblDtl")
                    gvListDtl.DataBind()
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            mrrawmststatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function IsRegisterWillBeClosed() As Boolean
        Dim bRet As Boolean = False
        If Session("TblDtl") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            Dim sOid As String = ""
            For C1 As Integer = 0 To dt.Rows.Count - 1
                If ToDouble(dt.Rows(C1)("mrrawqty").ToString) >= ToDouble(dt.Rows(C1)("registerqty").ToString) Then
                    sOid &= dt.Rows(C1)("registerdtloid").ToString & ","
                End If
            Next
            If sOid <> "" Then
                sOid = Left(sOid, sOid.Length - 1)
                sSql = "SELECT COUNT(*) FROM QL_trnregisterdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND registermstoid=" & registermstoid.Text & " AND registerdtlstatus='' AND registerdtloid NOT IN (" & sOid & ")"
                bRet = Not CheckDataExists(sSql)
            End If
        End If
        Return bRet
    End Function

    Private Function GetImportCost() As Double
        sSql = "SELECT icd.curroid, SUM(icd.importvalue) AS importvalue FROM QL_trnimportdtl icd INNER JOIN QL_trnimportmst icm ON icm.cmpcode=icd.cmpcode AND icm.importmstoid=icd.importmstoid WHERE icd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND icm.registermstoid=" & registermstoid.Text & " GROUP BY icd.curroid"
        Dim dtImport As DataTable = cKon.ambiltabel(sSql, "QL_trnimportdtl")
        Dim dRet As Double = 0
        Dim cRateTmp As New ClassRate
        For C1 As Integer = 0 To dtImport.Rows.Count - 1
            cRateTmp.SetRateValue(dtImport.Rows(C1)("curroid"), registerdate.Text)
            dRet += ToDouble(dtImport.Rows(C1)("importvalue").ToString) * cRateTmp.GetRateMonthlyIDRValue
        Next
        Return dRet
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckMRStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trnmrrawmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND mrrawmststatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Other\trnMRRaw.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(cKon.ambilscalar(sSql).ToString) > 0 Then
            lbMRInProcess.Visible = True
            lbMRInProcess.Text = "You have " & cKon.ambilscalar(sSql).ToString & " In Process Raw Material Received data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitAllDDL()
        'Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)
        'Fill DDL Warehouse
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='MATERIAL LOCATION' AND activeflag='ACTIVE'"
        FillDDL(mrrawwhoid, sSql)
        'Fill DDL Unit
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='MATERIAL UNIT' AND activeflag='ACTIVE'"
        FillDDL(mrrawunitoid, sSql)
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT mrrawmstoid, mrrawno, CONVERT(VARCHAR(10), mrrawdate, 101) AS mrrawdate, suppname, registerno, mrrawmststatus, mrrawmstnote, divname, 'False' AS checkvalue FROM QL_trnmrrawmst mrm INNER JOIN QL_trnregistermst reg ON reg.cmpcode=mrm.cmpcode AND reg.registermstoid=mrm.registermstoid AND registertype='RAW' INNER JOIN QL_mstsupp s ON s.suppoid=mrm.suppoid INNER JOIN QL_mstdivision div ON div.cmpcode=mrm.cmpcode WHERE"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " mrm.cmpcode='" & Session("CompnyCode") & "'"
        Else
            sSql &= " mrm.cmpcode LIKE '%'"
        End If
        sSql &= sSqlPlus & " ORDER BY mrm.mrrawdate DESC, mrm.mrrawmstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trnmrrawmst")
        gvList.DataSource = Session("TblMst")
        gvList.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvList.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvList.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvList.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "mrrawmstoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub BindSupplierData()
        sSql = "SELECT DISTINCT suppoid, suppcode, suppname, suppaddr FROM QL_mstsupp WHERE cmpcode='" & CompnyCode & "' AND " & FilterDDLListSupp.SelectedValue & " LIKE '%" & Tchar(FilterTextListSupp.Text) & "%' AND suppoid IN (SELECT suppoid FROM QL_trnregistermst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND registertype='RAW' AND registermststatus='Post' AND (CASE registerflag WHEN 'IMPORT' THEN registermstres1 ELSE 'Closed' END)='Closed' AND ISNULL(registermstres2, '')<>'Closed') ORDER BY suppcode, suppname"
        FillGV(gvListSupp, sSql, "QL_mstsupp")
    End Sub

    Private Sub BindRegisterData()
        sSql = "SELECT registermstoid, registerno, CONVERT(VARCHAR(10), registerdate, 101) AS registerdate, registerdocrefno, (CASE WHEN CONVERT(VARCHAR(10), registerdocrefdate, 101)='01/01/1900' THEN '' ELSE CONVERT(VARCHAR(10), registerdocrefdate, 101) END) AS registerdocrefdate, registermstnote, registerflag, (SELECT TOP 1 curroid FROM QL_trnporawmst pom INNER JOIN QL_trnregisterdtl regd ON regd.cmpcode=pom.cmpcode AND porefmstoid=porawmstoid WHERE regd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND regd.registermstoid=QL_trnregistermst.registermstoid) AS curroid FROM QL_trnregistermst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND suppoid=" & suppoid.Text & " AND registertype='RAW' AND registermststatus='Post' AND " & FilterDDLListReg.SelectedValue & " LIKE '%" & Tchar(FilterTextListReg.Text) & "%' AND (CASE registerflag WHEN 'IMPORT' THEN registermstres1 ELSE 'Closed' END)='Closed' AND ISNULL(registermstres2, '')<>'Closed' ORDER BY registermstoid"
        FillGV(gvListReg, sSql, "QL_trnregistermst")
    End Sub

    Private Sub BindMaterialData()
        Dim sAdd As String = ""
        If Not Session("oid") Is Nothing And Session("oid") <> "" Then
            sAdd = "AND mrrawmstoid<>" & Session("oid")
        End If
        sSql = "SELECT DISTINCT 'False' AS checkvalue, registerdtloid, porawno, regd.matrefoid AS matrawoid, matrawcode, matrawlongdesc, matrawlimitqty, registerunitoid, gendesc AS registerunit, (registerqty - ISNULL((SELECT SUM(mrrawqty) FROM QL_trnmrrawdtl mrd WHERE mrd.cmpcode=regd.cmpcode AND mrd.registerdtloid=regd.registerdtloid " & sAdd & "), 0) - ISNULL((SELECT SUM(retd.retrawqty) FROM QL_trnretrawdtl retd WHERE retd.cmpcode=regd.cmpcode AND retd.registerdtloid=regd.registerdtloid), 0)) AS registerqty, 0.0 AS mrqty, '' AS mrrawdtlnote, registerdtlseq, (porawdtlnetto / porawqty) AS mrrawvalue FROM QL_trnregisterdtl regd INNER JOIN QL_trnregistermst regm ON regd.registermstoid=regm.registermstoid INNER JOIN QL_trnporawmst pom ON pom.cmpcode=regd.cmpcode AND porawmstoid=porefmstoid INNER JOIN QL_trnporawdtl pod ON pod.cmpcode=regd.cmpcode AND porawdtloid=porefdtloid INNER JOIN QL_mstmatraw m ON m.matrawoid=regd.matrefoid INNER JOIN QL_mstgen g ON genoid=registerunitoid WHERE regd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND regd.registermstoid=" & registermstoid.Text & " AND registerdtlstatus='' AND ISNULL(registerdtlres2, '')<>'Complete' ORDER BY registerdtlseq"
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnregisterdtl")
        If dtTbl.Rows.Count > 0 Then
            Session("TblMat") = dtTbl
            Session("TblMatView") = dtTbl
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            showMessage("Material data can't be found!", 2)
        End If
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("QL_trnmrrawdtl")
        dtlTable.Columns.Add("mrrawdtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("registerdtloid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("porawno", Type.GetType("System.String"))
        dtlTable.Columns.Add("matrawoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matrawcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("matrawlongdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("matrawlimitqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("registerqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("mrqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("mrrawqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("mrrawbonusqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("mrrawunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("mrrawunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("mrrawdtlnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("mrrawvalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("mrrawamt", Type.GetType("System.Double"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub ClearDetail()
        mrrawdtlseq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                EnableHeader(False)
                mrrawdtlseq.Text = objTable.Rows.Count + 1
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        i_u2.Text = "New Detail"
        registerdtloid.Text = ""
        matrawoid.Text = ""
        matrawcode.Text = ""
        matrawlongdesc.Text = ""
        matrawlimitqty.Text = ""
        porawno.Text = ""
        registerqty.Text = ""
        mrqty.Text = ""
        mrrawqty.Text = ""
        mrrawbonusqty.Text = ""
        mrrawunitoid.SelectedIndex = -1
        mrrawdtlnote.Text = ""
        mrrawvalue.Text = ""
        gvListDtl.SelectedIndex = -1
        btnSearchMat.Visible = True
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal : DDLBusUnit.CssClass = sCss
        btnSearchSupp.Visible = bVal : btnClearSupp.Visible = bVal
        btnSearchReg.Visible = bVal : btnClearReg.Visible = bVal
    End Sub

    Private Sub GenerateRMRNo()
        Dim sNo As String = "RMR-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(mrrawno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnmrrawmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrrawno LIKE '" & sNo & "%'"
        mrrawno.Text = GenNumberString(sNo, "", cKon.ambilscalar(sSql), DefaultFormatCounter)
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT mrm.cmpcode, mrrawmstoid, mrm.periodacctg, mrm.mrrawdate, mrm.mrrawno, mrm.suppoid, suppname, reg.registermstoid, registerno, registerflag, registerdate, mrrawwhoid, mrrawmststatus, mrrawmstnote, mrm.createuser, mrm.createtime, mrm.upduser, mrm.updtime, mrm.curroid FROM QL_trnmrrawmst mrm INNER JOIN QL_trnregistermst reg ON reg.cmpcode=mrm.cmpcode AND reg.registermstoid=mrm.registermstoid INNER JOIN QL_mstsupp s ON s.suppoid=mrm.suppoid WHERE mrm.mrrawmstoid=" & sOid
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = xreader("cmpcode").ToString
                mrrawmstoid.Text = xreader("mrrawmstoid").ToString
                periodacctg.Text = xreader("periodacctg").ToString
                mrrawdate.Text = Format(xreader("mrrawdate"), "MM/dd/yyyy")
                registerdate.Text = Format(xreader("registerdate"), "MM/dd/yyyy")
                mrrawno.Text = xreader("mrrawno").ToString
                suppoid.Text = xreader("suppoid").ToString
                suppname.Text = xreader("suppname").ToString
                registermstoid.Text = xreader("registermstoid").ToString
                registerno.Text = xreader("registerno").ToString
                registerflag.Text = xreader("registerflag").ToString
                mrrawwhoid.SelectedValue = xreader("mrrawwhoid").ToString
                mrrawmstnote.Text = xreader("mrrawmstnote").ToString
                mrrawmststatus.Text = xreader("mrrawmststatus").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
                curroid.Text = xreader("curroid").ToString
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False : btnPost.Visible = False
            Exit Sub
        End Try
        DDLBusUnit.Enabled = False : DDLBusUnit.CssClass = "inpTextDisabled"
        If mrrawmststatus.Text = "Post" Or mrrawmststatus.Text = "Closed" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            'btnPost.Visible = False
            btnAddToList.Visible = False
            gvListDtl.Columns(0).Visible = False
            gvListDtl.Columns(gvListDtl.Columns.Count - 1).Visible = False
            lblTrnNo.Text = "MR No."
            mrrawmstoid.Visible = False
            mrrawno.Visible = True
        End If
        sSql = "SELECT mrrawdtlseq, mrd.registerdtloid, porawno, mrd.matrawoid, matrawcode, matrawlongdesc, matrawlimitqty, (registerqty - ISNULL((SELECT SUM(x.mrrawqty) FROM QL_trnmrrawdtl x WHERE x.cmpcode=mrd.cmpcode AND x.registerdtloid=mrd.registerdtloid AND x.mrrawmstoid<>mrd.mrrawmstoid), 0) - ISNULL((SELECT SUM(retrawqty) FROM QL_trnretrawdtl x WHERE x.cmpcode=mrd.cmpcode AND x.registerdtloid=mrd.registerdtloid), 0)) AS registerqty, (mrrawqty + mrrawbonusqty) AS mrqty, mrrawqty, mrrawbonusqty, mrrawunitoid, gendesc AS mrrawunit, mrrawdtlnote, mrrawvalue, (mrrawvalue * mrrawqty) AS mrrawamt FROM QL_trnmrrawdtl mrd INNER JOIN QL_mstmatraw m ON m.matrawoid=mrd.matrawoid INNER JOIN QL_mstgen g ON genoid=mrrawunitoid INNER JOIN QL_trnregisterdtl reg ON reg.cmpcode=mrd.cmpcode AND reg.registerdtloid=mrd.registerdtloid INNER JOIN QL_trnporawmst pom ON pom.cmpcode=reg.cmpcode AND pom.porawmstoid=porefmstoid WHERE mrrawmstoid=" & sOid & " ORDER BY mrrawdtlseq"
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnmrrawdtl")
        Session("TblDtl") = dtTbl
        gvListDtl.DataSource = dtTbl
        gvListDtl.DataBind()
        ClearDetail()
    End Sub

    Private Sub PrintReport()
        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("mrrawmstoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            report.Load(Server.MapPath(folderReport & "rptMRRaw.rpt"))
            Dim sWhere As String
            If Session("CompnyCode") <> CompnyCode Then
                sWhere = " WHERE mrm.cmpcode='" & Session("CompnyCode") & "'"
            Else
                sWhere = " WHERE mrm.cmpcode LIKE '%'"
            End If
            If sOid = "" Then
                sWhere &= " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND mrrawdate>='" & FilterPeriod1.Text & " 00:00:00' AND mrrawdate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    If FilterDDLStatus.SelectedValue <> "All" Then
                        sWhere &= " AND mrrawmststatus='" & FilterDDLStatus.SelectedValue & "'"
                    End If
                End If
                If checkPagePermission("~\Other\trnMRRaw.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND mrm.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sWhere &= " AND mrm.mrrawmstoid IN (" & sOid & ")"
            End If
            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "RawMaterialReceivedPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            report.Close()
            report.Dispose()
        End Try
        Response.Redirect("~\Other\trnMRRaw.aspx?awal=true")
    End Sub

    Private Sub GetLastData(ByRef dtHdr As DataTable, ByRef dtDtl As DataTable)
        sSql = "SELECT mrrawmstoid, mrrawdate, mrrawno, mrrawwhoid FROM QL_trnmrrawmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrrawmststatus='Post' AND registermstoid=" & registermstoid.Text
        If Session("oid") IsNot Nothing And Session("oid") <> "" Then
            sSql &= " AND mrrawmstoid<>" & Session("oid")
        End If
        sSql &= " ORDER BY mrrawmstoid"
        dtHdr = cKon.ambiltabel(sSql, "QL_trnmrrawmstlast")
        sSql = "SELECT mrm.mrrawmstoid, mrrawdtloid, mrrawdtlseq, matrawoid, mrrawqty, mrrawvalue, (mrrawqty * mrrawvalue) AS mrrawamt FROM QL_trnmrrawmst mrm INNER JOIN QL_trnmrrawdtl mrd ON mrd.cmpcode=mrm.cmpcode AND mrd.mrrawmstoid=mrm.mrrawmstoid WHERE mrm.cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrrawmststatus='Post' AND registermstoid=" & registermstoid.Text
        If Session("oid") IsNot Nothing And Session("oid") <> "" Then
            sSql &= " AND mrm.mrrawmstoid<>" & Session("oid")
        End If
        sSql &= " ORDER BY mrm.mrrawmstoid, mrrawdtlseq"
        dtDtl = cKon.ambiltabel(sSql, "QL_trnmrrawdtllast")
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Other\trnMRRaw.aspx")
        End If
        If checkPagePermission("~\Other\trnMRRaw.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Raw Material Received"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPost.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            CheckMRStatus()
            InitAllDDL()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                mrrawmstoid.Text = GenerateID("QL_TRNMRRAWMST", CompnyCode)
                mrrawmststatus.Text = "In Process"
                mrrawdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime)
                TabContainer1.ActiveTabIndex = 0
                btnDelete.Visible = False
                btnShowCOA.Visible = False
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvListDtl.DataSource = dt
            gvListDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Other\trnMRRaw.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lbMRInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbMRInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, mrm.updtime, GETDATE()) > " & nDays & " AND mrrawmststatus='In Process' "
        If checkPagePermission("~\Other\trnMRRaw.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND mrm.createuser='" & Session("UserID") & "' "
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND mrrawdate>='" & FilterPeriod1.Text & " 00:00:00' AND mrrawdate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            If FilterDDLStatus.SelectedValue <> "All" Then
                sSqlPlus &= " AND mrrawmststatus='" & FilterDDLStatus.SelectedValue & "'"
            End If
        End If
        If checkPagePermission("~\Other\trnMRRaw.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND mrm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Other\trnMRRaw.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND mrm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub gvList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvList.PageIndexChanging
        UpdateCheckedValue()
        gvList.PageIndex = e.NewPageIndex
        gvList.DataSource = Session("TblMst")
        gvList.DataBind()
    End Sub

    Protected Sub gvList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvList.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvList.DataSource = Session("TblMst")
            gvList.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        btnClearSupp_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupp.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        FilterDDLListSupp.SelectedIndex = -1 : FilterTextListSupp.Text = "" : gvListSupp.SelectedIndex = -1
        BindSupplierData()
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
    End Sub

    Protected Sub btnClearSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupp.Click
        suppoid.Text = ""
        suppname.Text = ""
        btnClearReg_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnFindListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSupp.Click
        BindSupplierData()
        mpeListSupp.Show()
    End Sub

    Protected Sub btnAllListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListSupp.Click
        FilterDDLListSupp.SelectedIndex = -1 : FilterTextListSupp.Text = "" : gvListSupp.SelectedIndex = -1
        BindSupplierData()
        mpeListSupp.Show()
    End Sub

    Protected Sub gvListSupp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSupp.PageIndexChanging
        gvListSupp.PageIndex = e.NewPageIndex
        BindSupplierData()
        mpeListSupp.Show()
    End Sub

    Protected Sub gvListSupp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListSupp.SelectedIndexChanged
        If suppoid.Text <> gvListSupp.SelectedDataKey.Item("suppoid").ToString Then
            btnClearSupp_Click(Nothing, Nothing)
        End If
        suppoid.Text = gvListSupp.SelectedDataKey.Item("suppoid").ToString
        suppname.Text = gvListSupp.SelectedDataKey.Item("suppname").ToString
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub lkbCloseListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListSupp.Click
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub btnSearchReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchReg.Click
        If suppoid.Text = "" Then
            showMessage("Please select Supplier first!", 2)
            Exit Sub
        End If
        FilterDDLListReg.SelectedIndex = -1 : FilterTextListReg.Text = "" : gvListReg.SelectedIndex = -1
        BindRegisterData()
        cProc.SetModalPopUpExtender(btnHideListReg, pnlListReg, mpeListReg, True)
    End Sub

    Protected Sub btnClearReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearReg.Click
        registermstoid.Text = "" : registerno.Text = "" : registerflag.Text = "" : curroid.Text = ""
    End Sub

    Protected Sub btnFindListReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListReg.Click
        BindRegisterData()
        mpeListReg.Show()
    End Sub

    Protected Sub btnAllListReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListReg.Click
        FilterDDLListReg.SelectedIndex = -1 : FilterTextListReg.Text = "" : gvListReg.SelectedIndex = -1
        BindRegisterData()
        mpeListReg.Show()
    End Sub

    Protected Sub gvListReg_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListReg.PageIndexChanging
        gvListReg.PageIndex = e.NewPageIndex
        BindRegisterData()
        mpeListReg.Show()
    End Sub

    Protected Sub gvListReg_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListReg.SelectedIndexChanged
        If registermstoid.Text <> gvListReg.SelectedDataKey.Item("registermstoid").ToString Then
            btnClearReg_Click(Nothing, Nothing)
        End If
        registermstoid.Text = gvListReg.SelectedDataKey.Item("registermstoid").ToString
        registerno.Text = gvListReg.SelectedDataKey.Item("registerno").ToString
        registerflag.Text = gvListReg.SelectedDataKey.Item("registerflag").ToString
        registerdate.Text = gvListReg.SelectedDataKey.Item("registerdate").ToString
        curroid.Text = gvListReg.SelectedDataKey.Item("curroid").ToString
        cProc.SetModalPopUpExtender(btnHideListReg, pnlListReg, mpeListReg, False)
    End Sub

    Protected Sub lkbCloseListReg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListReg.Click
        cProc.SetModalPopUpExtender(btnHideListReg, pnlListReg, mpeListReg, False)
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        If registermstoid.Text = "" Then
            showMessage("Please select Reg. No. first!", 2)
            Exit Sub
        End If
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : Session("TblMat") = Nothing : Session("TblMatView") = Nothing : gvListMat.DataSource = Nothing : gvListMat.DataBind()
        BindMaterialData()
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If UpdateCheckedMat() Then
            Session("TblMatView") = Session("TblMat")
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "registerdtloid=" & dtTbl.Rows(C1)("registerdtloid")
                    objView(0)("CheckValue") = "True"
                    dtTbl.Rows(C1)("CheckValue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                dtTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "No material data can't be selected!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "No material data can't be selected!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "registerdtloid=" & dtTbl.Rows(C1)("registerdtloid")
                    objView(0)("CheckValue") = "False"
                    objView(0)("mrqty") = 0
                    objView(0)("mrrawdtlnote") = ""
                    dtTbl.Rows(C1)("CheckValue") = "False"
                    dtTbl.Rows(C1)("mrqty") = 0
                    dtTbl.Rows(C1)("mrrawdtlnote") = ""
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                dtTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "No material data can't be selected!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "No material data can't be selected!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "CheckValue='True'"
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListMat") = "No material data have been selected before!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(7).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 4)
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub lbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListListMat.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "CheckValue='True'"
            Dim iCheck As Integer = dtView.Count
            If iCheck > 0 Then
                dtView.RowFilter = ""
                dtView.RowFilter = "CheckValue='True' AND mrqty>0"
                If dtView.Count <> iCheck Then
                    Session("WarningListMat") = "Qty for every checked material data must be more than 0!"
                    showMessage(Session("WarningListMat"), 2)
                    dtView.RowFilter = ""
                    Exit Sub
                End If
                dtView.RowFilter = ""
                dtView.RowFilter = "CheckValue='True' AND mrqty<1000000"
                If dtView.Count <> iCheck Then
                    Session("WarningListMat") = "Qty for every checked material data must be less than 1,000,000!"
                    showMessage(Session("WarningListMat"), 2)
                    dtView.RowFilter = ""
                    Exit Sub
                End If
                'dtView.RowFilter = ""
                'dtView.RowFilter = "CheckValue='True' AND mrqty<1000000 AND ((mrqty-registerqy)< 1000)"
                'If dtView.Count <> iCheck Then
                '    Session("WarningListMat") = "Qty for every checked material data must be less than 1,000,000!"
                '    showMessage(Session("WarningListMat"), 2)
                '    dtView.RowFilter = ""
                '    Exit Sub
                'End If
                If Not IsQtyRounded(dtView, "mrqty", "matrawlimitqty") Then
                    Session("WarningListMat") = "Quantity for every checked material data must be rounded by Round Qty!"
                    showMessage(Session("WarningListMat"), 2)
                    dtView.RowFilter = ""
                    Exit Sub
                End If
                If Session("TblDtl") Is Nothing Then
                    CreateTblDetail()
                End If
                Dim objTable As DataTable
                objTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                Dim objRow As DataRow
                Dim counter As Integer = objTable.Rows.Count
                Dim dQty As Double
                For C1 As Integer = 0 To dtView.Count - 1
                    dv.RowFilter = "registerdtloid=" & dtView(C1)("registerdtloid")
                    If dv.Count > 0 Then
                        dv.AllowEdit = True
                        dv(0)("mrqty") = dtView(C1)("mrqty")
                        dQty = dtView(C1)("mrqty") - dtView(C1)("registerqty")
                        If dQty > 0 Then
                            dv(0)("mrrawqty") = dtView(C1)("registerqty")
                            dv(0)("mrrawbonusqty") = dQty
                            dv(0)("mrrawamt") = dtView(C1)("mrrawvalue") * dtView(C1)("registerqty")
                        Else
                            dv(0)("mrrawqty") = dtView(C1)("mrqty")
                            dv(0)("mrrawbonusqty") = 0
                            dv(0)("mrrawamt") = dtView(C1)("mrrawvalue") * dtView(C1)("mrqty")
                        End If
                        dv(0)("mrrawvalue") = dtView(C1)("mrrawvalue")
                        dv(0)("mrrawdtlnote") = dtView(C1)("mrrawdtlnote")
                    Else
                        counter += 1
                        objRow = objTable.NewRow()
                        objRow("mrrawdtlseq") = counter
                        objRow("registerdtloid") = dtView(C1)("registerdtloid")
                        objRow("porawno") = dtView(C1)("porawno")
                        objRow("matrawoid") = dtView(C1)("matrawoid")
                        objRow("matrawcode") = dtView(C1)("matrawcode").ToString
                        objRow("matrawlongdesc") = dtView(C1)("matrawlongdesc").ToString
                        objRow("matrawlimitqty") = dtView(C1)("matrawlimitqty")
                        objRow("registerqty") = dtView(C1)("registerqty")
                        objRow("mrqty") = dtView(C1)("mrqty")
                        dQty = dtView(C1)("mrqty") - dtView(C1)("registerqty")
                        If dQty > 0 Then
                            objRow("mrrawqty") = dtView(C1)("registerqty")
                            objRow("mrrawbonusqty") = dQty
                            objRow("mrrawamt") = dtView(C1)("mrrawvalue") * dtView(C1)("registerqty")
                        Else
                            objRow("mrrawqty") = dtView(C1)("mrqty")
                            objRow("mrrawbonusqty") = 0
                            objRow("mrrawamt") = dtView(C1)("mrrawvalue") * dtView(C1)("mrqty")
                        End If
                        objRow("mrrawunitoid") = dtView(C1)("registerunitoid")
                        objRow("mrrawunit") = dtView(C1)("registerunit").ToString
                        objRow("mrrawdtlnote") = dtView(C1)("mrrawdtlnote")
                        objRow("mrrawvalue") = dtView(C1)("mrrawvalue")
                        objTable.Rows.Add(objRow)
                    End If
                    dv.RowFilter = ""
                Next
                dtView.RowFilter = ""
                objTable.AcceptChanges()
                Session("TblDtl") = objTable
                gvListDtl.DataSource = objTable
                gvListDtl.DataBind()
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                ClearDetail()
            Else
                Session("WarningListMat") = "Please select material to add to list!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        Else
            Session("WarningListMat") = "Please select material to add to list!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub mrqty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles mrqty.TextChanged
        mrqty.Text = ToMaskEdit(ToDouble(mrqty.Text), 4)
        Dim dQty As Double = ToDouble(mrqty.Text) - ToDouble(registerqty.Text)
        If dQty > 0 Then
            mrrawqty.Text = ToMaskEdit(ToDouble(registerqty.Text), 4)
            mrrawbonusqty.Text = ToMaskEdit(dQty, 4)
        Else
            mrrawqty.Text = ToMaskEdit(ToDouble(mrqty.Text), 4)
            mrrawbonusqty.Text = ToMaskEdit(0, 4)
        End If
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            Dim objTable As DataTable = Session("TblDtl")
            Dim objRow As DataRow = objTable.Rows(mrrawdtlseq.Text - 1)
            objRow.BeginEdit()
            objRow("registerdtloid") = registerdtloid.Text
            objRow("matrawoid") = matrawoid.Text
            objRow("matrawcode") = matrawcode.Text
            objRow("matrawlongdesc") = matrawlongdesc.Text
            objRow("matrawlimitqty") = ToDouble(matrawlimitqty.Text)
            objRow("porawno") = porawno.Text
            objRow("registerqty") = ToDouble(registerqty.Text)
            objRow("mrqty") = ToDouble(mrqty.Text)
            objRow("mrrawqty") = ToDouble(mrrawqty.Text)
            objRow("mrrawbonusqty") = ToDouble(mrrawbonusqty.Text)
            objRow("mrrawunitoid") = mrrawunitoid.SelectedValue
            objRow("mrrawunit") = mrrawunitoid.SelectedItem.Text
            objRow("mrrawdtlnote") = mrrawdtlnote.Text
            objRow("mrrawvalue") = ToDouble(mrrawvalue.Text)
            objRow("mrrawamt") = ToDouble(mrrawvalue.Text) * ToDouble(mrrawqty.Text)
            objRow.EndEdit()
            objTable.AcceptChanges()
            Session("TblDtl") = objTable
            gvListDtl.DataSource = objTable
            gvListDtl.DataBind()
            ClearDetail()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvListDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
        End If
    End Sub

    Protected Sub gvListDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvListDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("mrrawdtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvListDtl.DataSource = objTable
        gvListDtl.DataBind()
        ClearDetail()
    End Sub

    Protected Sub gvListDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListDtl.SelectedIndexChanged
        Try
            mrrawdtlseq.Text = gvListDtl.SelectedDataKey.Item("mrrawdtlseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "mrrawdtlseq=" & mrrawdtlseq.Text
                registerdtloid.Text = dv.Item(0).Item("registerdtloid").ToString
                matrawoid.Text = dv.Item(0).Item("matrawoid").ToString
                matrawcode.Text = dv.Item(0).Item("matrawcode").ToString
                matrawlongdesc.Text = dv.Item(0).Item("matrawlongdesc").ToString
                porawno.Text = dv.Item(0).Item("porawno").ToString
                matrawlimitqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("matrawlimitqty").ToString), 4)
                registerqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("registerqty").ToString), 4)
                mrqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("mrqty").ToString), 4)
                mrrawqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("mrrawqty").ToString), 4)
                mrrawbonusqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("mrrawbonusqty").ToString), 4)
                mrrawunitoid.SelectedValue = dv.Item(0).Item("mrrawunitoid").ToString
                mrrawdtlnote.Text = dv.Item(0).Item("mrrawdtlnote").ToString
                mrrawvalue.Text = dv.Item(0).Item("mrrawvalue").ToString
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        Finally
            btnSearchMat.Visible = False
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                If CheckDataExists("SELECT COUNT(*) FROM QL_trnmrrawmst WHERE mrrawmstoid=" & mrrawmstoid.Text) Then
                    mrrawmstoid.Text = GenerateID("QL_TRNMRRAWMST", CompnyCode)
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNMRRAWMST", "mrrawmstoid", mrrawmstoid.Text, "mrrawmststatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    mrrawmststatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            mrrawdtloid.Text = GenerateID("QL_TRNMRRAWDTL", CompnyCode)
            Dim iConMatOid, iCrdMtrOid, iGLMstOid, iGLDtlOid, iStockAcctgOid, iRecAcctgOid, iStockValOid As Integer
            Dim dTotalAmt As Double
            Dim dtLastHdrData As DataTable = Nothing, dtLastDtlData As DataTable = Nothing
            Dim dvLastDtlData As DataView = Nothing
            Dim isRegClosed As Boolean = False
            Dim dImportCost As Double = 0, dSumMR As Double = 0
            Dim cRate As New ClassRate
            periodacctg.Text = GetDateToPeriodAcctg(CDate(mrrawdate.Text))
            Dim sDate As String = mrrawdate.Text 'Format(GetServerTime(), "MM/dd/yyyy")
            Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
            If mrrawmststatus.Text = "Post" Then
                cRate.SetRateValue(CInt(curroid.Text), registerdate.Text)
                If cRate.GetRateDailyLastError <> "" Then
                    showMessage(cRate.GetRateDailyLastError, 2)
                    mrrawmststatus.Text = "In Process"
                    Exit Sub
                End If
                If cRate.GetRateMonthlyLastError <> "" Then
                    showMessage(cRate.GetRateMonthlyLastError, 2)
                    mrrawmststatus.Text = "In Process"
                    Exit Sub
                End If
                If registerflag.Text.ToUpper = "IMPORT" Then
                    isRegClosed = IsRegisterWillBeClosed()
                End If
                If registerflag.Text.ToUpper = "IMPORT" And isRegClosed = True Then
                    dImportCost = GetImportCost() / cRate.GetRateMonthlyIDRValue
                    GetLastData(dtLastHdrData, dtLastDtlData)
                    dSumMR += ToDouble(dtLastDtlData.Compute("SUM(mrrawamt)", "").ToString)
                    dvLastDtlData = dtLastDtlData.DefaultView
                End If
                If registerflag.Text.ToUpper = "LOCAL" Or (registerflag.Text.ToUpper = "IMPORT" And isRegClosed = True) Then
                    iConMatOid = GenerateID("QL_CONMAT", CompnyCode)
                    iCrdMtrOid = GenerateID("QL_CRDMTR", CompnyCode)
                    iGLMstOid = GenerateID("QL_TRNGLMST", CompnyCode)
                    iGLDtlOid = GenerateID("QL_TRNGLDTL", CompnyCode)
                    iStockValOid = GenerateID("QL_STOCKVALUE", CompnyCode)
                    Dim sVarErr As String = ""
                    If Not IsInterfaceExists("VAR_STOCK_RM", DDLBusUnit.SelectedValue) Then
                        sVarErr = "VAR_STOCK_RM"
                    End If
                    If Not IsInterfaceExists("VAR_PURC_RECEIVED", DDLBusUnit.SelectedValue) Then
                        sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_PURC_RECEIVED"
                    End If
                    If sVarErr <> "" Then
                        showMessage(GetInterfaceWarning(sVarErr, "posting"), 2)
                        mrrawmststatus.Text = "In Process"
                        Exit Sub
                    End If
                    iStockAcctgOid = GetAcctgOID(GetVarInterface("VAR_STOCK_RM", DDLBusUnit.SelectedValue), CompnyCode)
                    iRecAcctgOid = GetAcctgOID(GetVarInterface("VAR_PURC_RECEIVED", DDLBusUnit.SelectedValue), CompnyCode)
                End If
                'GenerateRMRNo()
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trnmrrawmst (cmpcode, mrrawmstoid, periodacctg, mrrawdate, mrrawno, suppoid, registermstoid, mrrawwhoid, mrrawmstnote, mrrawmststatus, createuser, createtime, upduser, updtime, curroid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & mrrawmstoid.Text & ", '" & periodacctg.Text & "', '" & mrrawdate.Text & "', '" & mrrawno.Text & "', " & suppoid.Text & ", " & registermstoid.Text & ", " & mrrawwhoid.SelectedValue & ", '" & Tchar(mrrawmstnote.Text) & "', '" & mrrawmststatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & curroid.Text & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & mrrawmstoid.Text & " WHERE tablename='QL_TRNMRRAWMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    'sSql = "UPDATE QL_trnmrrawmst SET periodacctg='" & periodacctg.Text & "', mrrawdate='" & mrrawdate.Text & "', mrrawno='" & mrrawno.Text & "', suppoid=" & suppoid.Text & ", registermstoid=" & registermstoid.Text & ", mrrawwhoid=" & mrrawwhoid.SelectedValue & ", mrrawmstnote='" & Tchar(mrrawmstnote.Text) & "', mrrawmststatus='" & mrrawmststatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, curroid=" & curroid.Text & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrrawmstoid=" & mrrawmstoid.Text
                    'xCmd.CommandText = sSql
                    'xCmd.ExecuteNonQuery()
                    'sSql = "UPDATE QL_trnregisterdtl SET registerdtlstatus='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND registerdtloid IN (SELECT registerdtloid FROM QL_trnmrrawdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrrawmstoid=" & mrrawmstoid.Text & ")"
                    'xCmd.CommandText = sSql
                    'xCmd.ExecuteNonQuery()
                    'sSql = "UPDATE QL_trnregistermst SET registermststatus='Post' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND registermstoid=" & registermstoid.Text
                    'xCmd.CommandText = sSql
                    'xCmd.ExecuteNonQuery()
                    'sSql = "DELETE FROM QL_trnmrrawdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrrawmstoid=" & mrrawmstoid.Text
                    'xCmd.CommandText = sSql
                    'xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    dSumMR += ToDouble(objTable.Compute("SUM(mrrawamt)", "").ToString)
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        Dim dMRVal As Double = 0
                        If dSumMR > 0 Then
                            dMRVal = ToDouble(objTable.Rows(C1)("mrrawvalue").ToString) + ((ToDouble(objTable.Rows(C1)("mrrawvalue").ToString) / dSumMR) * dImportCost)
                        End If
                        'sSql = "INSERT INTO QL_trnmrrawdtl (cmpcode, mrrawdtloid, mrrawmstoid, mrrawdtlseq, registerdtloid, matrawoid, mrrawqty, mrrawbonusqty, mrrawunitoid, mrrawvalue, mrrawvalueidr, mrrawvalueusd, mrrawdtlstatus, mrrawdtlnote, upduser, updtime) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C1 + CInt(mrrawdtloid.Text)) & ", " & mrrawmstoid.Text & ", " & C1 + 1 & ", " & objTable.Rows(C1)("registerdtloid") & ", " & objTable.Rows(C1)("matrawoid") & ", " & ToDouble(objTable.Rows(C1)("mrrawqty").ToString) & ", " & ToDouble(objTable.Rows(C1)("mrrawbonusqty").ToString) & ", " & objTable.Rows(C1)("mrrawunitoid") & ", " & dMRVal & ", " & dMRVal * cRate.GetRateMonthlyIDRValue & ", " & dMRVal * cRate.GetRateMonthlyUSDValue & ", '', '" & Tchar(objTable.Rows(C1)("mrrawdtlnote")) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                        'xCmd.CommandText = sSql
                        'xCmd.ExecuteNonQuery()
                        'If ToDouble(objTable.Rows(C1)("mrrawqty").ToString) >= ToDouble(objTable.Rows(C1)("registerqty").ToString) Then
                        '    sSql = "UPDATE QL_trnregisterdtl SET registerdtlstatus='COMPLETE' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND registerdtloid=" & objTable.Rows(C1)("registerdtloid")
                        '    xCmd.CommandText = sSql
                        '    xCmd.ExecuteNonQuery()
                        '    sSql = "UPDATE QL_trnregistermst SET registermststatus='Closed' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND registermstoid=" & registermstoid.Text & " AND (SELECT COUNT(*) FROM QL_trnregisterdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND registermstoid=" & registermstoid.Text & " AND registerdtloid<>" & objTable.Rows(C1)("registerdtloid") & " AND registerdtlstatus='')=0"
                        '    xCmd.CommandText = sSql
                        '    xCmd.ExecuteNonQuery()
                        'End If
                    Next
                    'sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(mrrawdtloid.Text)) & " WHERE tablename='QL_TRNMRRAWDTL' AND cmpcode='" & CompnyCode & "' "
                    'xCmd.CommandText = sSql
                    'xCmd.ExecuteNonQuery()
                    If mrrawmststatus.Text = "Post" Then
                        If registerflag.Text.ToUpper = "IMPORT" And isRegClosed = True Then
                            For C1 As Int16 = 0 To dtLastHdrData.Rows.Count - 1
                                Dim dTotalMR As Double = 0
                                dvLastDtlData.RowFilter = "mrrawmstoid=" & dtLastHdrData.Rows(C1)("mrrawmstoid")
                                For C2 As Integer = 0 To dvLastDtlData.Count - 1
                                    Dim dMRVal As Double = 0
                                    If dSumMR > 0 Then
                                        dMRVal = ToDouble(dvLastDtlData(C2)("mrrawvalue").ToString) + ((ToDouble(dvLastDtlData(C2)("mrrawvalue").ToString) / dSumMR) * dImportCost)
                                    End If
                                    sSql = "UPDATE QL_trnmrrawdtl SET mrrawvalue=" & dMRVal & ", mrrawvalueidr=" & dMRVal * cRate.GetRateMonthlyIDRValue & ", mrrawvalueusd=" & dMRVal * cRate.GetRateMonthlyUSDValue & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrrawdtloid=" & dvLastDtlData(C2)("mrrawdtloid")
                                    xCmd.CommandText = sSql
                                    xCmd.ExecuteNonQuery()
                                    sSql = "INSERT INTO QL_conmat (cmpcode, conmtroid, type, typemin, trndate, periodacctg, formaction, formoid, refoid, refname, mtrwhoid, qtyin, qtyout, reason, note, upduser, updtime) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iConMatOid & ", 'RMR', 1, '" & sDate & "', '" & sPeriod & "', 'QL_trnmrrawdtl', " & dtLastHdrData.Rows(C1)("mrrawmstoid") & ", " & dvLastDtlData(C2)("matrawoid") & ", 'RAW MATERIAL', " & dtLastHdrData.Rows(C1)("mrrawwhoid") & ", " & ToDouble(dvLastDtlData(C2)("mrrawqty").ToString) & ", 0, 'Raw Material Received', '" & dtLastHdrData.Rows(C1)("mrrawno").ToString & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                                    xCmd.CommandText = sSql
                                    xCmd.ExecuteNonQuery()
                                    iConMatOid += 1
                                    sSql = "UPDATE QL_crdmtr SET qtyin=qtyin + " & ToDouble(dvLastDtlData(C2)("mrrawqty").ToString) & ", saldoakhir=saldoakhir + " & ToDouble(dvLastDtlData(C2)("mrrawqty").ToString) & ", lasttranstype='QL_trnmrrawdtl', lasttransdate='" & sDate & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND refoid=" & dvLastDtlData(C2)("matrawoid") & " AND refname='RAW MATERIAL' AND mtrwhoid=" & dtLastHdrData.Rows(C1)("mrrawwhoid") & " AND periodacctg='" & sPeriod & "'"
                                    xCmd.CommandText = sSql
                                    If xCmd.ExecuteNonQuery() = 0 Then
                                        sSql = "INSERT INTO QL_crdmtr (cmpcode, crdmatoid, periodacctg, refoid, refname, mtrwhoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir, lasttranstype, lasttransdate, upduser, updtime, createuser, createdate, closeuser, closingdate) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iCrdMtrOid & ", '" & sPeriod & "', " & dvLastDtlData(C2)("matrawoid") & ", 'RAW MATERIAL', " & dtLastHdrData.Rows(C1)("mrrawwhoid") & ", " & ToDouble(dvLastDtlData(C2)("mrrawqty").ToString) & ", 0, 0, 0, 0, " & ToDouble(dvLastDtlData(C2)("mrrawqty").ToString) & ", 'QL_trnmrrawdtl', '" & sDate & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', '1/1/1900')"
                                        xCmd.CommandText = sSql
                                        xCmd.ExecuteNonQuery()
                                        iCrdMtrOid += 1
                                    End If
                                    dTotalMR += ToDouble(dvLastDtlData(C2)("mrrawamt").ToString)
                                    Dim dFactor As Double = 0
                                    If dSumMR > 0 Then
                                        dFactor = ToDouble(dvLastDtlData(C2)("mrrawamt").ToString) / dSumMR
                                    End If
                                    sSql = "UPDATE QL_stockvalue SET stockqty=stockqty + " & ToDouble(dvLastDtlData(C2)("mrrawqty").ToString) & ", stockvalueidr=((stockvalueidr * stockqty) + " & (ToDouble(dvLastDtlData(C2)("mrrawamt").ToString) + (dFactor * dImportCost)) * cRate.GetRateMonthlyIDRValue & ") / (stockqty + " & ToDouble(dvLastDtlData(C2)("mrrawqty").ToString) & "), stockvalueusd=((stockvalueusd * stockqty) + " & (ToDouble(dvLastDtlData(C2)("mrrawamt").ToString) + (dFactor * dImportCost)) * cRate.GetRateMonthlyUSDValue & ") / (stockqty + " & ToDouble(dvLastDtlData(C2)("mrrawqty").ToString) & "), lasttranstype='QL_trnmrrawdtl', lasttransdate='" & sDate & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND periodacctg='" & sPeriod & "' AND refoid=" & dvLastDtlData(C2)("matrawoid") & " AND refname='RAW MATERIAL'"
                                    xCmd.CommandText = sSql
                                    If xCmd.ExecuteNonQuery() = 0 Then
                                        sSql = "INSERT INTO QL_stockvalue (cmpcode, stockvalueoid, periodacctg, refoid, refname, stockqty, stockvalueidr, stockvalueusd, lasttranstype, lasttransdate, note, upduser, updtime) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iStockValOid & ", '" & sPeriod & "', " & dvLastDtlData(C2)("matrawoid") & ", 'RAW MATERIAL', " & ToDouble(dvLastDtlData(C2)("mrrawqty").ToString) & ", " & (ToDouble(dvLastDtlData(C2)("mrrawvalue").ToString) + (dFactor * dImportCost)) * cRate.GetRateMonthlyIDRValue & ", " & (ToDouble(dvLastDtlData(C2)("mrrawvalue").ToString) + (dFactor * dImportCost)) * cRate.GetRateMonthlyUSDValue & ", 'QL_trnmrrawdtl', '" & sDate & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                                        xCmd.CommandText = sSql
                                        xCmd.ExecuteNonQuery()
                                        iStockValOid += 1
                                    End If
                                Next
                                dvLastDtlData.RowFilter = ""
                            Next
                        End If
                        If registerflag.Text.ToUpper = "LOCAL" Or (registerflag.Text.ToUpper = "IMPORT" And isRegClosed = True) Then
                            Dim dTotalMR As Double = 0
                            For C1 As Int16 = 0 To objTable.Rows.Count - 1
                                sSql = "INSERT INTO QL_conmat (cmpcode, conmtroid, type, typemin, trndate, periodacctg, formaction, formoid, refoid, refname, mtrwhoid, qtyin, qtyout, reason, note, upduser, updtime) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iConMatOid & ", 'RMR', 1, '" & sDate & "', '" & sPeriod & "', 'QL_trnmrrawdtl', " & mrrawmstoid.Text & ", " & objTable.Rows(C1)("matrawoid") & ", 'RAW MATERIAL', " & mrrawwhoid.SelectedValue & ", " & ToDouble(objTable.Rows(C1)("mrrawqty").ToString) & ", 0, 'Raw Material Received', '" & mrrawno.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iConMatOid += 1
                                sSql = "UPDATE QL_crdmtr SET qtyin=qtyin + " & ToDouble(objTable.Rows(C1)("mrrawqty").ToString) & ", saldoakhir=saldoakhir + " & ToDouble(objTable.Rows(C1)("mrrawqty").ToString) & ", lasttranstype='QL_trnmrrawdtl', lasttransdate='" & sDate & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND refoid=" & objTable.Rows(C1)("matrawoid") & " AND refname='RAW MATERIAL' AND mtrwhoid=" & mrrawwhoid.SelectedValue & " AND periodacctg='" & sPeriod & "'"
                                xCmd.CommandText = sSql
                                If xCmd.ExecuteNonQuery() = 0 Then
                                    sSql = "INSERT INTO QL_crdmtr (cmpcode, crdmatoid, periodacctg, refoid, refname, mtrwhoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir, lasttranstype, lasttransdate, upduser, updtime, createuser, createdate, closeuser, closingdate) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iCrdMtrOid & ", '" & sPeriod & "', " & objTable.Rows(C1)("matrawoid") & ", 'RAW MATERIAL', " & mrrawwhoid.SelectedValue & ", " & ToDouble(objTable.Rows(C1)("mrrawqty").ToString) & ", 0, 0, 0, 0, " & ToDouble(objTable.Rows(C1)("mrrawqty").ToString) & ", 'QL_trnmrrawdtl', '" & sDate & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', '1/1/1900')"
                                    xCmd.CommandText = sSql
                                    xCmd.ExecuteNonQuery()
                                    iCrdMtrOid += 1
                                End If
                                Dim dFactor As Double = 0
                                If dSumMR > 0 Then
                                    dFactor = ToDouble(objTable.Rows(C1)("mrrawamt").ToString) / dSumMR
                                End If
                                dTotalMR += (ToDouble(objTable.Rows(C1)("mrrawamt").ToString) + (dFactor * dImportCost))
                                sSql = "UPDATE QL_stockvalue SET stockqty=stockqty + " & ToDouble(objTable.Rows(C1)("mrrawqty").ToString) & ", stockvalueidr=((stockvalueidr * stockqty) + " & (ToDouble(objTable.Rows(C1)("mrrawamt").ToString) + (dFactor * dImportCost)) * cRate.GetRateMonthlyIDRValue & ") / (stockqty + " & ToDouble(objTable.Rows(C1)("mrrawqty").ToString) & "), stockvalueusd=((stockvalueusd * stockqty) + " & (ToDouble(objTable.Rows(C1)("mrrawamt").ToString) + (dFactor * dImportCost)) * cRate.GetRateMonthlyUSDValue & ") / (stockqty + " & ToDouble(objTable.Rows(C1)("mrrawqty").ToString) & "), lasttranstype='QL_trnmrrawdtl', lasttransdate='" & sDate & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND periodacctg='" & sPeriod & "' AND refoid=" & objTable.Rows(C1)("matrawoid") & " AND refname='RAW MATERIAL'"
                                xCmd.CommandText = sSql
                                If xCmd.ExecuteNonQuery() = 0 Then
                                    sSql = "INSERT INTO QL_stockvalue (cmpcode, stockvalueoid, periodacctg, refoid, refname, stockqty, stockvalueidr, stockvalueusd, lasttranstype, lasttransdate, note, upduser, updtime) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iStockValOid & ", '" & sPeriod & "', " & objTable.Rows(C1)("matrawoid") & ", 'RAW MATERIAL', " & ToDouble(objTable.Rows(C1)("mrrawqty").ToString) & ", " & (ToDouble(objTable.Rows(C1)("mrrawvalue").ToString) + (dFactor * dImportCost)) * cRate.GetRateMonthlyIDRValue & ", " & (ToDouble(objTable.Rows(C1)("mrrawvalue").ToString) + (dFactor * dImportCost)) * cRate.GetRateMonthlyUSDValue & ", 'QL_trnmrrawdtl', '" & sDate & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                                    xCmd.CommandText = sSql
                                    xCmd.ExecuteNonQuery()
                                    iStockValOid += 1
                                End If
                            Next
                            dTotalAmt = dSumMR + dImportCost
                            If dTotalAmt > 0 Then
                                ' Insert GL MST
                                sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGLMstOid & ", '" & sDate & "', '" & sPeriod & "', 'MR Raw|No. " & mrrawno.Text & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                ' Insert GL DTL
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGLDtlOid & ", 1, " & iGLMstOid & ", " & iStockAcctgOid & ", 'D', " & dTotalMR & ", '" & mrrawno.Text & "', 'MR Raw|No. " & mrrawno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalMR * cRate.GetRateMonthlyIDRValue & ", " & dTotalMR * cRate.GetRateMonthlyUSDValue & ", 'QL_trnmrrawmst " & mrrawmstoid.Text & "')"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iGLDtlOid += 1
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGLDtlOid & ", 2, " & iGLMstOid & ", " & iRecAcctgOid & ", 'C', " & dTotalMR & ", '" & mrrawno.Text & "', 'MR Raw|No. " & mrrawno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalMR * cRate.GetRateMonthlyIDRValue & ", " & dTotalMR * cRate.GetRateMonthlyUSDValue & ", 'QL_trnmrrawmst " & mrrawmstoid.Text & "')"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iGLDtlOid += 1
                                iGLMstOid += 1
                            End If
                            sSql = "UPDATE QL_mstoid SET lastoid=" & iConMatOid - 1 & " WHERE tablename='QL_CONMAT' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_mstoid SET lastoid=" & iCrdMtrOid - 1 & " WHERE tablename='QL_CRDMTR' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_mstoid SET lastoid=" & iGLMstOid - 1 & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_mstoid SET lastoid=" & iGLDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_mstoid SET lastoid=" & iStockValOid - 1 & " WHERE tablename='QL_STOCKVALUE' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                    End If
                End If
                objTrans.Commit()
                xCmd.Connection.Close()
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 1)
                xCmd.Connection.Close()
                mrrawmststatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & mrrawmstoid.Text & ".<BR>"
            End If
            If mrrawmststatus.Text = "Post" Then
                Session("SavedInfo") &= "Data have been posted with MR No. = " & mrrawno.Text & "."
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Other\trnMRRaw.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Other\trnMRRaw.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If mrrawmstoid.Text.Trim = "" Then
            showMessage("Please select Raw Material Received data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNMRRAWMST", "mrrawmstoid", mrrawmstoid.Text, "mrrawmststatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                mrrawmststatus.Text = "In Process"
                Exit Sub
            End If
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trnregisterdtl SET registerdtlstatus='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND registerdtloid IN (SELECT registerdtloid FROM QL_trnmrrawdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrrawmstoid=" & mrrawmstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_trnregistermst SET registermststatus='Post' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND registermstoid=" & registermstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnmrrawdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrrawmstoid=" & mrrawmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnmrrawmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrrawmstoid=" & mrrawmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Other\trnMRRaw.aspx?awal=true")
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPost.Click
        mrrawmststatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnShowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnShowCOA.Click
        DDLRateType.SelectedIndex = -1
        ShowCOAPosting(mrrawno.Text, DDLBusUnit.SelectedValue, gvCOAPosting, DDLRateType.SelectedValue)
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, True)
    End Sub

    Protected Sub lkbCloseCOAPosting_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseCOAPosting.Click
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, False)
    End Sub

    Protected Sub gvCOAPosting_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCOAPosting.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 4)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
        End If
    End Sub

    Protected Sub DDLRateType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLRateType.SelectedIndexChanged
        ShowCOAPosting(mrrawno.Text, DDLBusUnit.SelectedValue, gvCOAPosting, DDLRateType.SelectedValue)
        mpeCOAPosting.Show()
    End Sub

    Protected Sub btnPrintHdr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrintHdr.Click
        UpdateCheckedValue()
        PrintReport()
    End Sub
#End Region

End Class