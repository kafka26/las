<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="Notification.aspx.vb" Inherits="Other_Notification" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Message" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Message :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="PanelFilter1" runat="server" DefaultButton="btnSearch" BorderColor="#E0E0E0" BorderStyle="Solid" BorderWidth="1px"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR>
    <tr>
        <td align="left" class="Label" colspan="4">
            <asp:Label ID="lblListOutMsg" runat="server" Font-Bold="True" Font-Size="8pt" Font-Underline="True"
                ForeColor="Black" Text="List of Outgoing Message"></asp:Label></td>
    </tr>
    <TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="lblFilter" runat="server" Text="Filter"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="100px">
<asp:ListItem Value="notifymessage">Message</asp:ListItem>
<asp:ListItem Value="notifyuser">Dest. User</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="200px"></asp:TextBox></TD></TR>
    <tr>
        <td align="left" class="Label" style="width: 10%">
            <asp:CheckBox ID="cbPeriod" runat="server" />
            <asp:DropDownList id="FilterDDLPeriod" runat="server" CssClass="inpText" Width="100px">
                <asp:ListItem Value="createtime">Create Time</asp:ListItem>
                <asp:ListItem Value="updtime">Post Time</asp:ListItem>
            </asp:DropDownList></td>
        <td align="center" class="Label" style="width: 1%">
            :</td>
        <td align="left" class="Label" colspan="2">
            <asp:TextBox ID="FilterPeriod1" runat="server" CssClass="inpText" ToolTip="MM/dd/yyyy"
                Width="60px"></asp:TextBox>&nbsp;<asp:ImageButton ID="imbDate1" runat="server" ImageAlign="AbsMiddle"
                    ImageUrl="~/Images/oCalendar.gif" />
            <asp:Label ID="lblTo" runat="server" Text="to"></asp:Label>
            <asp:TextBox ID="FilterPeriod2" runat="server" CssClass="inpText" ToolTip="MM/dd/yyyy"
                Width="60px"></asp:TextBox>
            <asp:ImageButton ID="imbDate2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/oCalendar.gif" />
            <asp:Label ID="lblInfoPeriod" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></td>
    </tr>
    <TR><TD class="Label" align=left><asp:Label id="lblFilterStatus" runat="server" Text="Status"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="100px"><asp:ListItem>All</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Posted</asp:ListItem>
</asp:DropDownList></TD></TR>
    <tr>
        <td align="left" class="Label">
        </td>
        <td align="center" class="Label">
        </td>
        <td align="left" class="Label" colspan="2">
            <ajaxToolkit:CalendarExtender ID="cePeriod1" runat="server" Format="MM/dd/yyyy" PopupButtonID="imbDate1"
                TargetControlID="FilterPeriod1">
            </ajaxToolkit:CalendarExtender>
            <ajaxToolkit:MaskedEditExtender ID="meePeriod1" runat="server" Mask="99/99/9999"
                MaskType="Date" TargetControlID="FilterPeriod1">
            </ajaxToolkit:MaskedEditExtender>
            <ajaxToolkit:CalendarExtender ID="cePeriod2" runat="server" Format="MM/dd/yyyy" PopupButtonID="imbDate2"
                TargetControlID="FilterPeriod2">
            </ajaxToolkit:CalendarExtender>
            <ajaxToolkit:MaskedEditExtender ID="meePeriod2" runat="server" Mask="99/99/9999"
                MaskType="Date" TargetControlID="FilterPeriod2">
            </ajaxToolkit:MaskedEditExtender>
        </td>
    </tr>
    <TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvMst" runat="server" ForeColor="#333333" Width="100%" CellPadding="4" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" GridLines="None" AllowSorting="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="notifyoid" DataNavigateUrlFormatString="~/Other/Notification.aspx?oid={0}" DataTextField="selecttext">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="createtime" HeaderText="Create Time" SortExpression="createtime">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="notifymessage" HeaderText="Message" SortExpression="notifymessage">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="notifyuser" HeaderText="Dest. User" SortExpression="notifyuser">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="activeflag" HeaderText="Status" SortExpression="activeflag">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
    <asp:BoundField DataField="updtime" HeaderText="Post Time" SortExpression="updtime">
        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Center" />
        <ItemStyle HorizontalAlign="Center" />
    </asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="Sienna" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data"></asp:Label></TD></TR>
</TBODY></TABLE></asp:Panel> 
                                    <br />
                                    <asp:Panel id="PanelFilter2" runat="server" DefaultButton="btnSearch2" BorderColor="#E0E0E0" BorderStyle="Solid" BorderWidth="1px">
    <TABLE style="WIDTH: 100%">
        <TBODY>
            <TR>
                <TD style="HEIGHT: 5px" class="Label" align=left colSpan=4>
                </td>
            </tr>
            <tr>
                <td align="left" class="Label" colspan="4">
                    <asp:Label ID="lblListInMsg" runat="server" Font-Bold="True" Font-Size="8pt" Font-Underline="True"
                        ForeColor="Black" Text="List of Incoming Message"></asp:Label></td>
            </tr>
            <TR>
                <TD style="WIDTH: 14%" class="Label" align=left>
                    <asp:Label ID="lblFilter2" runat="server" Text="Filter"></asp:Label></td>
                <TD style="WIDTH: 1%" class="Label" align=center>
                    :</td>
                <TD class="Label" align=left colSpan=2>
                    <asp:DropDownList id="FilterDDL2" runat="server" CssClass="inpText" Width="100px">
                        <asp:ListItem Value="notifymessage">Message</asp:ListItem>
                        <asp:ListItem Value="createuser">From User</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="FilterText2" runat="server" CssClass="inpText" Width="200px"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" class="Label" style="width: 10%">
                    <asp:CheckBox ID="cbPeriod2" runat="server" />
                    <asp:DropDownList id="FilterDDLPeriod2" runat="server" CssClass="inpText" Width="100px">
                        <asp:ListItem Value="createtime">Create Time</asp:ListItem>
                        <asp:ListItem Value="updtime">Post Time</asp:ListItem>
                    </asp:DropDownList></td>
                <td align="center" class="Label" style="width: 1%">
                    :</td>
                <td align="left" class="Label" colspan="2">
                    <asp:TextBox ID="FilterPeriod21" runat="server" CssClass="inpText" ToolTip="MM/dd/yyyy"
                        Width="60px"></asp:TextBox>&nbsp;<asp:ImageButton ID="imbDate21" runat="server" ImageAlign="AbsMiddle"
                            ImageUrl="~/Images/oCalendar.gif" />
                    <asp:Label ID="lblTo2" runat="server" Text="to"></asp:Label>
                    <asp:TextBox ID="FilterPeriod22" runat="server" CssClass="inpText" ToolTip="MM/dd/yyyy"
                        Width="60px"></asp:TextBox>
                    <asp:ImageButton ID="imbDate22" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/oCalendar.gif" />
                    <asp:Label ID="lblInfoPeriod2" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></td>
            </tr>
            <TR>
                <TD class="Label" align=left>
                    <asp:Label ID="lblFilterStatus2" runat="server" Text="Status"></asp:Label></td>
                <TD class="Label" align=center>
                    :</td>
                <TD class="Label" align=left colSpan=2>
                    <asp:DropDownList id="FilterDDLStatus2" runat="server" CssClass="inpText" Width="100px">
                        <asp:ListItem>All</asp:ListItem>
                        <asp:ListItem>In Process</asp:ListItem>
                        <asp:ListItem>Posted</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td align="left" class="Label">
                </td>
                <td align="center" class="Label">
                </td>
                <td align="left" class="Label" colspan="2">
                    <ajaxToolkit:CalendarExtender ID="cePeriod21" runat="server" Format="MM/dd/yyyy"
                        PopupButtonID="imbDate21" TargetControlID="FilterPeriod21">
                    </ajaxToolkit:CalendarExtender>
                    <ajaxToolkit:MaskedEditExtender ID="meePeriod21" runat="server" Mask="99/99/9999"
                        MaskType="Date" TargetControlID="FilterPeriod21">
                    </ajaxToolkit:MaskedEditExtender>
                    <ajaxToolkit:CalendarExtender ID="cePeriod22" runat="server" Format="MM/dd/yyyy"
                        PopupButtonID="imbDate22" TargetControlID="FilterPeriod22">
                    </ajaxToolkit:CalendarExtender>
                    <ajaxToolkit:MaskedEditExtender ID="meePeriod22" runat="server" Mask="99/99/9999"
                        MaskType="Date" TargetControlID="FilterPeriod22">
                    </ajaxToolkit:MaskedEditExtender>
                </td>
            </tr>
            <TR>
                <TD style="HEIGHT: 5px" class="Label" align=left colSpan=4>
                </td>
            </tr>
            <TR>
                <TD class="Label" align=left colSpan=4>
                    <asp:ImageButton ID="btnSearch2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/btnfind.bmp" />
                    <asp:ImageButton ID="btnAll2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewall.png" /></td>
            </tr>
            <TR>
                <TD style="HEIGHT: 5px" class="Label" align=left colSpan=4>
                </td>
            </tr>
            <TR>
                <TD class="Label" align=left colSpan=4>
                    <asp:GridView id="gvMst2" runat="server" ForeColor="#333333" Width="100%" CellPadding="4" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" GridLines="None" AllowSorting="True">
                        <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                        <RowStyle BackColor="#EFF3FB" Font-Size="X-Small" />
                        <EmptyDataRowStyle BackColor="#FFFFC0" />
                        <Columns>
                            <asp:BoundField DataField="createtime" HeaderText="Create Time" SortExpression="createtime">
                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="notifymessage" HeaderText="Message" SortExpression="notifymessage">
                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="createuser" HeaderText="From User" SortExpression="createuser">
                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="activeflag" HeaderText="Status" SortExpression="activeflag">
                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="updtime" HeaderText="Post Time" SortExpression="updtime">
                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                        </Columns>
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="Sienna" ForeColor="White" HorizontalAlign="Right" />
                        <EmptyDataTemplate>
                            <asp:Label ID="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label>
                        </EmptyDataTemplate>
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White" />
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>
                    <asp:Label ID="lblViewInfo2" runat="server" ForeColor="Black" Text="Click button Find or View All to view data"></asp:Label></td>
            </tr>
        </tbody>
    </table>
</asp:Panel>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 10%" class="Label" align=left><asp:Label id="I_U" runat="server" CssClass="Important" Text="New Data"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 30%" class="Label" align=left><asp:Label id="notifyoid" runat="server" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label7" runat="server">Create User</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="createuser" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label11" runat="server">Create Time</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="createtime" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox>&nbsp;<asp:Label id="Label17" runat="server" CssClass="Important" Text="(MM/dd/yyyy HH:mm:ss)"></asp:Label></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label18" runat="server">Message</asp:Label>&nbsp;<asp:Label id="Label21" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" vAlign=middle align=left><asp:TextBox id="notifymessage" runat="server" CssClass="inpText" Width="250px" Rows="2" TextMode="MultiLine"></asp:TextBox> </TD><TD class="Label" vAlign=middle align=left><asp:Label id="Label10" runat="server" CssClass="Important" Text="Maks. 200 Characters"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label2" runat="server">Dest. User</asp:Label> <asp:Label id="Label22" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:Panel id="pnlUser" runat="server" Width="100%" Height="100px" ScrollBars="Vertical"><asp:CheckBoxList id="notifyuser" runat="server" Width="97%" RepeatColumns="4" AutoPostBack="True"></asp:CheckBoxList></asp:Panel> </TD></TR><TR><TD class="Label" align=left><asp:Label id="Label5" runat="server">Status</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="activeflag" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="notifystarttime" runat="server" CssClass="inpText" Width="25px" Visible="False"></asp:TextBox> <asp:TextBox id="notifyendtime" runat="server" CssClass="inpText" Width="25px" Visible="False"></asp:TextBox></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left colSpan=4><ajaxToolkit:MaskedEditExtender id="meeStart" runat="server" TargetControlID="notifystarttime" UserDateFormat="MonthDayYear" UserTimeFormat="TwentyFourHour" MaskType="DateTime" Mask="99/99/9999 99:99:99"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meeEnd" runat="server" TargetControlID="notifyendtime" UserDateFormat="MonthDayYear" UserTimeFormat="TwentyFourHour" MaskType="DateTime" Mask="99/99/9999 99:99:99"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=4></TD></TR>
    <tr>
        <td align="left" class="Label" colspan="4" style="color: #585858">
            Last Update By
            <asp:Label ID="upduser" runat="server" Font-Bold="True"></asp:Label>
            On
            <asp:Label ID="updtime" runat="server" Font-Bold="True"></asp:Label></td>
    </tr>
</TBODY></TABLE><TABLE><TBODY><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Message :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

