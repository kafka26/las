Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Other_PreCostingUnlock
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Dim xCmd As New SqlCommand("", conn)
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT pr.cmpcode, pr.precostoid, CONVERT(CHAR(10), pr.precostdate, 101) AS precostdate, pr.precostdesc, pr.activeflag, ISNULL(pr.bomstatus, '' ) AS bomstatus, ISNULL(pr.dlcstatus, '') AS dlcstatus, pr.precostnote, i.itemlongdesc AS itemshortdesc, i.itemcode FROM QL_mstprecost pr INNER JOIN QL_mstitem i ON i.itemoid=pr.itemoid WHERE"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " pr.cmpcode='" & Session("CompnyCode") & "'"
        Else
            sSql &= " pr.cmpcode LIKE '%'"
        End If
        sSql &= " AND precostlock='Y'" & sSqlPlus & " ORDER BY pr.precostdate DESC, pr.precostoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_mstprecost")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        btnUnlock.Visible = (gvTRN.Rows.Count > 0)
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Other\PreCostingUnlock.aspx")
        End If
        If checkPagePermission("~\Other\PreCostingUnlock.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Unlock Pre Costing"
        btnUnlock.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to UNLOCK this data?');")
        If Not Page.IsPostBack Then
            sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
            If Session("CompnyCode") <> CompnyCode Then
                sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
            End If
            FillDDL(DDLBusUnit, sSql)
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            btnUnlock.Visible = False
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND pr.precostdate>='" & FilterPeriod1.Text & " 00:00:00' AND pr.precostdate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND pr.activeflag='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Other\PreCostingUnlock.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND pr.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Other\PreCostingUnlock.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND pr.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub btnUnlock_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnUnlock.Click
        Dim objTbl As DataTable = Nothing
        If Session("TblMst") Is Nothing Then
            showMessage("Please select Pre Costing data first!", 2)
            Exit Sub
        Else
            objTbl = Session("TblMst")
            If objTbl.Rows.Count <= 0 Then
                showMessage("Please select Pre Costing data first!", 2)
                Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                If cKon.getCheckBoxValue(C1, 0, gvTRN) Then
                    sSql = "UPDATE QL_mstprecost SET precostlock='N', lastunlockuser='" & Session("UserID") & "', lastunlocktime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & objTbl.Rows(C1)("cmpcode").ToString & "' AND precostoid=" & objTbl.Rows(C1)("precostoid").ToString
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
            Next
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            showMessage(ex.Message, 1)
            conn.Close()
            Exit Sub
        End Try
        Response.Redirect("~\Other\PreCostingUnlock.aspx?awal=true")
    End Sub
#End Region

End Class