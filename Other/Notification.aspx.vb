Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Other_Notification
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 List of Outgoing Message is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 List of Outgoing Message is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 List of Outgoing Message must be more than Period 1 List of Outgoing Message!", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsValidPeriod2() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod21.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 List of Incoming Message is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod22.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 List of Incoming Message is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod21.Text) > CDate(FilterPeriod22.Text) Then
            showMessage("Period 2 List of Incoming Message must be more than Period 1 List of Incoming Message!", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If notifystarttime.Text = "" Then
            sError &= "- Please fill START SHOW field!<BR>"
            sErr = "Empty"
        Else
            If Not IsValidDate(Left(notifystarttime.Text, 10), "MM/dd/yyyy", sErr) Then
                sError &= "- Date of START SHOW field is invalid. " & sErr & "<BR>"
            Else
                If Not IsValidTime(Right(notifystarttime.Text, 8), sErr) Then
                    sError &= "- Time of START SHOW field is invalid. " & sErr & "<BR>"
                Else
                    If CDate(createtime.Text) >= CDate(notifystarttime.Text) Then
                        sError &= "- START SHOW field must be more than CREATE TIME field!<BR>"
                        sErr = "More"
                    End If
                End If
            End If
        End If
        Dim sErr2 As String = ""
        If notifyendtime.Text = "" Then
            sError &= "- Please fill END SHOW field!<BR>"
            sErr2 = "Empty"
        Else
            If Not IsValidDate(Left(notifyendtime.Text, 10), "MM/dd/yyyy", sErr2) Then
                sError &= "- Date of END SHOW field is invalid. " & sErr & "<BR>"
            Else
                If Not IsValidTime(Right(notifyendtime.Text, 8), sErr2) Then
                    sError &= "- Time of END SHOW field is invalid. " & sErr & "<BR>"
                Else
                    If CDate(createtime.Text) >= CDate(notifyendtime.Text) Then
                        sError &= "- END SHOW field must be more than CREATE TIME field!<BR>"
                        sErr2 = "More"
                    End If
                End If
            End If
        End If
        If sErr = "" And sErr2 = "" Then
            If CDate(notifystarttime.Text) >= CDate(notifyendtime.Text) Then
                sError &= "- END SHOW field must be more than START SHOW field!<BR>"
            End If
        End If
        If notifymessage.Text.Trim = "" Then
            sError &= "- Please fill MESSAGE field!<BR>"
        Else
            If notifymessage.Text.Length > 200 Then
                sError &= "- Length of MESSAGE field must be less or equal than 200 characters!<BR>"
            End If
        End If
        Dim bVal As Boolean = False
        For C1 As Integer = 0 To notifyuser.Items.Count - 1
            If notifyuser.Items(C1).Selected = True Then
                bVal = True
                Exit For
            End If
        Next
        If bVal = False Then
            sError &= "- Please select minimally 1 DEST. USER field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsValidTime(ByVal sTime As String, ByRef sErr As String) As Boolean
        Dim sPartTime() As String = Split(sTime, ":")
        If sPartTime.Length < 3 Then
            sErr = "Time is incomplete. Time component must be consisted of second, minute and hour value."
            Return False
        End If
        If sPartTime.Length > 3 Then
            sErr = "Time is out of range. Time component must be consisted of second, minute and hour value only."
            Return False
        End If
        If IsNumeric(sPartTime(0)) Then
            If ToDouble(sPartTime(0)) < 0 Or ToDouble(sPartTime(0)) > 23 Then
                sErr = "Hour value is invalid. Hour value must be between 00 and 23."
                Return False
            End If
        End If
        If IsNumeric(sPartTime(1)) Then
            If ToDouble(sPartTime(1)) < 0 Or ToDouble(sPartTime(1)) > 59 Then
                sErr = "Minute value is invalid. Minute value must be between 00 and 59."
                Return False
            End If
        End If
        If IsNumeric(sPartTime(2)) Then
            If ToDouble(sPartTime(2)) < 0 Or ToDouble(sPartTime(2)) > 59 Then
                sErr = "Second value is invalid. Second value must be between 00 and 59."
                Return False
            End If
        End If
        Return True
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitCBL()
        sSql = "SELECT profoid, (cmpcode + ' - ' + profoid) AS profoidtext FROM QL_mstprof WHERE activeflag='ACTIVE' ORDER BY profoidtext"
        notifyuser.Items.Clear()
        notifyuser.Items.Add("All")
        notifyuser.Items(notifyuser.Items.Count - 1).Value = "All"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd = New SqlCommand(sSql, conn)
        xreader = xCmd.ExecuteReader
        While xreader.Read
            'If Session("CompnyCode") <> CompnyCode Then
            '    notifyuser.Items.Add(xreader.GetValue(0).ToString)
            'Else
            notifyuser.Items.Add(xreader.GetValue(1).ToString)
            'End If
            notifyuser.Items(notifyuser.Items.Count - 1).Value = xreader.GetValue(0).ToString
        End While
        xreader.Close()
        conn.Close()
        Session("LastAllCheck") = False
    End Sub

    Private Sub BindMstData(ByVal sFilter As String)
        FilterDDLStatus.CssClass = "inpText" : FilterDDLStatus.Enabled = True
        sSql = "SELECT 'Select' AS selecttext, notifyoid, CONVERT(VARCHAR(10), createtime, 101) AS createtime, (CONVERT(VARCHAR(10), notifystarttime, 101) + ' ' + CONVERT(VARCHAR(8), notifystarttime, 108) + ' to ' + CONVERT(VARCHAR(10), notifyendtime, 101) + ' ' + CONVERT(VARCHAR(8), notifyendtime, 108)) AS notifystartend, notifymessage, notifyuser, activeflag, (CASE activeflag WHEN 'In Process' THEN '-' ELSE CONVERT(VARCHAR(10), updtime, 101) END) AS updtime FROM QL_notification WHERE cmpcode='" & CompnyCode & "' AND createuser='" & Session("UserID") & "' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(sFilter) & "%'"
        If cbPeriod.Checked Then
            If IsValidPeriod() Then
                sSql &= " AND " & FilterDDLPeriod.SelectedValue & " >= '" & FilterPeriod1.Text & " 00:00:00' AND " & FilterDDLPeriod.SelectedValue & " <= '" & FilterPeriod2.Text & " 23:59:59'"
                If FilterDDLPeriod.SelectedIndex = 1 Then
                    FilterDDLStatus.SelectedIndex = 2 : FilterDDLStatus.CssClass = "inpTextDisabled" : FilterDDLStatus.Enabled = False
                End If
            Else
                Exit Sub
            End If
        End If
        If FilterDDLStatus.SelectedValue <> "All" Then
            sSql &= " AND activeflag='" & FilterDDLStatus.SelectedValue & "'"
        End If
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_notification")
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub BindMstData2(ByVal sFilter As String)
        FilterDDLStatus2.CssClass = "inpText" : FilterDDLStatus2.Enabled = True
        sSql = "SELECT notifyoid, CONVERT(VARCHAR(10), createtime, 101) AS createtime, (CONVERT(VARCHAR(10), notifystarttime, 101) + ' ' + CONVERT(VARCHAR(8), notifystarttime, 108) + ' to ' + CONVERT(VARCHAR(10), notifyendtime, 101) + ' ' + CONVERT(VARCHAR(8), notifyendtime, 108)) AS notifystartend, notifymessage, createuser, activeflag, (CASE activeflag WHEN 'In Process' THEN '-' ELSE CONVERT(VARCHAR(10), updtime, 101) END) AS updtime FROM QL_notification WHERE cmpcode='" & CompnyCode & "' AND notifyuser LIKE '%" & Session("UserID") & "%' AND " & FilterDDL2.SelectedValue & " LIKE '%" & Tchar(sFilter) & "%'"
        If cbPeriod2.Checked Then
            If IsValidPeriod2() Then
                sSql &= " AND " & FilterDDLPeriod2.SelectedValue & " >= '" & FilterPeriod21.Text & " 00:00:00' AND " & FilterDDLPeriod2.SelectedValue & " <= '" & FilterPeriod22.Text & " 23:59:59'"
                If FilterDDLPeriod2.SelectedIndex = 1 Then
                    FilterDDLStatus2.SelectedIndex = 2 : FilterDDLStatus2.CssClass = "inpTextDisabled" : FilterDDLStatus2.Enabled = False
                End If
            Else
                Exit Sub
            End If
        End If
        If FilterDDLStatus2.SelectedValue <> "All" Then
            sSql &= " AND activeflag='" & FilterDDLStatus2.SelectedValue & "'"
        End If
        Session("TblMst2") = cKon.ambiltabel(sSql, "QL_notification2")
        gvMst2.DataSource = Session("TblMst2")
        gvMst2.DataBind()
        lblViewInfo2.Visible = False
    End Sub

    Private Sub FillTextBox()
        Try
            sSql = "SELECT notifyoid, notifyuser, notifymessage, activeflag, createuser, createtime, notifystarttime, notifyendtime, upduser, updtime FROM QL_notification WHERE cmpcode='" & CompnyCode & "' AND notifyoid=" & Session("oid")
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                notifyoid.Text = xreader("notifyoid").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = Format(xreader("createtime"), "MM/dd/yyyy HH:mm:ss")
                notifystarttime.Text = Format(xreader("notifystarttime"), "MM/dd/yyyy HH:mm:ss")
                notifyendtime.Text = Format(xreader("notifyendtime"), "MM/dd/yyyy HH:mm:ss")
                notifymessage.Text = xreader("notifymessage").ToString
                Dim sUser() As String = xreader("notifyuser").ToString.Split(";")
                If sUser.Length > 0 Then
                    If notifyuser.Items.Count > 0 Then
                        For C1 As Integer = 0 To sUser.Length - 1
                            Dim li As ListItem = notifyuser.Items.FindByValue(sUser(C1).Trim)
                            Dim i As Integer = notifyuser.Items.IndexOf(li)
                            If i >= 0 Then
                                notifyuser.Items(notifyuser.Items.IndexOf(li)).Selected = True
                            End If
                        Next
                    End If
                End If
                notifyuser_SelectedIndexChanged(Nothing, Nothing)
                activeflag.Text = xreader("activeflag").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False
        End Try
        If activeflag.Text = "Posted" Then
            createtime.Text = Format(GetServerTime(), "MM/dd/yyyy HH:mm:ss")
        End If
        btnSave.Visible = False : btnDelete.Visible = False
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Other\Notification.aspx")
        End If
        Page.Title = CompnyName & " - Message"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            I_U.Text = "New Data"
        Else
            I_U.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        If Not Page.IsPostBack Then
            upduser.Text = "-" : updtime.Text = "-"
            InitCBL()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            FilterPeriod21.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod22.Text = Format(GetServerTime(), "MM/dd/yyyy")
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox()
                TabContainer1.ActiveTabIndex = 1
            Else
                createuser.Text = Session("UserID") : createtime.Text = Format(GetServerTime(), "MM/dd/yyyy HH:mm:ss")
                notifystarttime.Text = Format(DateAdd(DateInterval.Minute, 10, GetServerTime()), "MM/dd/yyyy HH:mm:ss")
                notifyendtime.Text = Format(GetServerTime(), "MM/dd/yyyy") & " 23:59:59"
                notifyoid.Text = GenerateID("QL_NOTIFICATION", CompnyCode)
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
                activeflag.Text = "In Process"
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriod.Checked = False
        FilterDDLPeriod.SelectedIndex = -1
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        FilterDDLStatus.SelectedIndex = -1
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
    End Sub

    Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvMst.DataSource = Session("TblMst")
            gvMst.DataBind()
        End If
    End Sub

    Protected Sub btnSearch2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch2.Click
        BindMstData2(FilterText2.Text)
    End Sub

    Protected Sub btnAll2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll2.Click
        FilterText2.Text = ""
        FilterDDL2.SelectedIndex = -1
        cbPeriod2.Checked = False
        FilterDDLPeriod2.SelectedIndex = -1
        FilterPeriod21.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod22.Text = Format(GetServerTime(), "MM/dd/yyyy")
        FilterDDLStatus2.SelectedIndex = -1
        BindMstData2(FilterText2.Text)
    End Sub

    Protected Sub gvMst2_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst2.PageIndexChanging
        gvMst2.PageIndex = e.NewPageIndex
        gvMst2.DataSource = Session("TblMst2")
        gvMst2.DataBind()
    End Sub

    Protected Sub gvMst2_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst2.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst2"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvMst2.DataSource = Session("TblMst2")
            gvMst2.DataBind()
        End If
    End Sub

    Protected Sub notifyuser_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles notifyuser.SelectedIndexChanged
        Dim iTotal As Integer = 0
        If notifyuser.Items(0).Selected = True Then
            If notifyuser.Items.Count > 1 Then
                For C1 As Integer = 1 To notifyuser.Items.Count - 1
                    If notifyuser.Items(C1).Selected = False Then
                        iTotal += 1
                    End If
                Next
                If iTotal = notifyuser.Items.Count - 1 Then
                    For C1 As Integer = 1 To notifyuser.Items.Count - 1
                        notifyuser.Items(C1).Selected = True
                    Next
                Else
                    If Session("LastAllCheck") = True Then
                        notifyuser.Items(0).Selected = False
                    Else
                        For C1 As Integer = 1 To notifyuser.Items.Count - 1
                            notifyuser.Items(C1).Selected = True
                        Next
                    End If
                End If
                Session("LastAllCheck") = notifyuser.Items(0).Selected
            End If
        Else
            If notifyuser.Items.Count > 1 Then
                For C1 As Integer = 1 To notifyuser.Items.Count - 1
                    If notifyuser.Items(C1).Selected = True Then
                        iTotal += 1
                    End If
                Next
                If iTotal = notifyuser.Items.Count - 1 Then
                    If Session("LastAllCheck") = True Then
                        For C1 As Integer = 1 To notifyuser.Items.Count - 1
                            notifyuser.Items(C1).Selected = False
                        Next
                    Else
                        notifyuser.Items(0).Selected = True
                    End If
                End If
                Session("LastAllCheck") = notifyuser.Items(0).Selected
            End If
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            If Session("oid") = Nothing Or Session("oid") = "" Then
                notifyoid.Text = GenerateID("QL_NOTIFICATION", CompnyCode)
            End If
            Dim sUser As String = ""
            For C1 As Integer = 0 To notifyuser.Items.Count - 1
                If notifyuser.Items(C1).Value <> "All" And notifyuser.Items(C1).Selected = True Then
                    sUser &= notifyuser.Items(C1).Value
                    If C1 < notifyuser.Items.Count - 1 Then
                        sUser &= "; "
                    End If
                End If
            Next
            If sUser.Length > 500 Then
                sUser = Left(sUser, 500)
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_notification (cmpcode, notifyoid, notifyuser, notifymessage, activeflag, createuser, createtime, notifystarttime, notifyendtime, upduser, updtime) VALUES ('" & CompnyCode & "', " & notifyoid.Text & ", '" & sUser & "', '" & Tchar(notifymessage.Text) & "', '" & activeflag.Text & "', '" & createuser.Text & "', '" & createtime.Text & "', '" & notifystarttime.Text & "', '" & notifyendtime.Text & "', '" & createuser.Text & "', CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & notifyoid.Text & " WHERE tablename='QL_NOTIFICATION' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_notification SET notifyuser='" & sUser & "', notifymessage='" & Tchar(notifymessage.Text) & "', activeflag='In Process', createuser='" & createuser.Text & "', createtime='" & createtime.Text & "', notifystarttime='" & notifystarttime.Text & "', notifyendtime='" & notifyendtime.Text & "', upduser='" & createuser.Text & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & CompnyCode & "' AND notifyoid=" & notifyoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                conn.Close()
            Catch ex As Exception
                objTrans.Rollback()
                conn.Close()
                showMessage(ex.Message, 1)
                Exit Sub
            End Try
            Response.Redirect("~\Other\Notification.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Other\Notification.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If notifyoid.Text = "" Then
            showMessage("Please select Message data first!", 1)
            Exit Sub
        End If
        If DeleteData("QL_notification", "notifyoid", notifyoid.Text, CompnyCode) = True Then
            Response.Redirect("~\Other\Notification.aspx?awal=true")
        End If
    End Sub
#End Region

End Class
