Partial Class Other_NotAuthorize
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If
        lblWelcome.Text = "User Active, " & Session("USerID")
        Page.Title = System.Configuration.ConfigurationManager.AppSettings("CompanyName") & " - Not Authorize"
    End Sub

End Class
