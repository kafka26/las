Imports System
Imports System.Data
Imports System.Data.OleDb
Imports System.Windows.Forms
Imports ClassFunction

Partial Class login
    Inherits System.Web.UI.Page

#Region "Variables"
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_CONN")
    'Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim cKoneksi As New Koneksi
    Dim sSql As String = ""
#End Region

#Region "Procedures"
    Private Sub InitializeCompany()
        sSql = "SELECT gendesc FROM QL_mstgen WHERE gengroup='COMPANY ADDRESS'"
        If CStr(cKoneksi.ambilscalar(sSql)) <> "" Then
            lblAddr.Text = cKoneksi.ambilscalar(sSql).ToString
        End If
        sSql = "SELECT gendesc FROM QL_mstgen WHERE gengroup='COMPANY PHONE'"
        If CStr(cKoneksi.ambilscalar(sSql)) <> "" Then
            lblPhone.Text = cKoneksi.ambilscalar(sSql).ToString
        End If
        sSql = "SELECT gendesc FROM QL_mstgen WHERE gengroup='COMPANY FAX'"
        If CStr(cKoneksi.ambilscalar(sSql)) <> "" Then
            lblFax.Text = cKoneksi.ambilscalar(sSql).ToString
        End If
        sSql = "SELECT gendesc FROM QL_mstgen WHERE gengroup='COMPANY EMAIL'"
        If CStr(cKoneksi.ambilscalar(sSql)) <> "" Then
            lblEmail.Text = cKoneksi.ambilscalar(sSql).ToString
        End If
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session.Clear()
        InitializeCompany()
        If Trim(txtInputuserid.Text) <> "" Then
            txtInputPassword.Focus()
        Else
            txtInputuserid.Focus()
        End If
        Page.Title = CompnyName & " - Login"
        lblCompny.Text = CompnyName.ToUpper
    End Sub

    Protected Sub imbLogin_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbLogin.Click
        Try
            Dim vuserid As String
            Dim vpassword As String
            vuserid = Tchar(txtInputuserid.Text)
            vpassword = Tchar(txtInputPassword.Text)
            Session("UserID") = Nothing
            Session("Role") = Nothing
            Session("ApprovalLimit") = Nothing
            Session("SpecialAccess") = Nothing
            Session("CompnyCode") = Nothing
            If (vuserid <> "" And vpassword <> "") Then
                Dim myDS As New DataSet()
                Dim sqlSelect As String
                sqlSelect = "SELECT cmpcode, profoid, profname, profpass, activeflag, profapplimit, upduser, updtime FROM QL_mstprof WHERE profoid='" & ClassFunction.Tchar(vuserid) & "' AND profpass='" & ClassFunction.Tchar(vpassword) & "'"
                Dim mySqlDA As New SqlClient.SqlDataAdapter(sqlSelect, ConnStr)
                mySqlDA.Fill(myDS)
                Dim objTable As DataTable
                Dim objRow() As DataRow
                objTable = myDS.Tables(0)
                objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
                If objRow.Length > 0 Then
                    If objRow(0).Item("activeflag") = "ACTIVE" Then
                        Session("UserID") = txtInputuserid.Text
                        Session("ApprovalLimit") = 0    'objRow(0).Item("profapplimit")
                        Session("CompnyCode") = objRow(0).Item("cmpcode")
                        sqlSelect = "SELECT DISTINCT rdtl.formtype, rdtl.formname, rdtl.formaddress, ISNULL(g.genother3, '') AS formmodule, ISNULL(g.genother4, 0) AS formnumber, (CASE WHEN g.genother5 IS NULL THEN rdtl.formname WHEN g.genother5='' THEN rdtl.formname ELSE g.genother5 END) AS formmenu, (CASE WHEN g.genother6 IS NULL THEN 'no_logo.png' WHEN g.genother6='' THEN 'no_logo.png' ELSE g.genother6 END) AS formimage, g.activeflag FROM QL_mstroledtl rdtl INNER JOIN QL_mstuserrole ur ON rdtl.roleoid=ur.roleoid INNER JOIN QL_mstgen g ON g.gendesc=rdtl.formname AND g.gengroup='FORMNAME' AND g.activeflag='ACTIVE' WHERE ur.profoid='" & Session("UserID") & "'"
                        Session("Role") = cKoneksi.ambiltabel(sqlSelect, "Role")
                        sqlSelect = "SELECT DISTINCT rdtl.formaddress, ur.special FROM QL_mstroledtl rdtl INNER JOIN QL_mstuserrole ur ON rdtl.roleoid=ur.roleoid WHERE ur.special='Yes' AND ur.profoid='" & Session("UserID") & "'"
                        Session("SpecialAccess") = cKoneksi.ambiltabel(sqlSelect, "SpecialAccess")
                        Response.Redirect("~/Other/menu.aspx")
                    ElseIf objRow(0).Item("activeflag") = "INACTIVE" Then
                        lblMessage.Text = "<br>Your user ID is Inactive, please contact your administrator"
                        txtInputuserid.Text = ""
                        txtInputPassword.Text = ""
                        txtInputuserid.Focus()
                    ElseIf objRow(0).Item("activeflag") = "SUSPENDED" Then
                        lblMessage.Text = "<br>Your user ID is Suspended, please contact your administrator"
                        txtInputuserid.Text = ""
                        txtInputPassword.Text = ""
                        txtInputuserid.Focus()
                    End If
                Else
                    lblMessage.Text = "<br>User ID and password do not match!"
                    txtInputuserid.Text = ""
                    txtInputPassword.Text = ""
                    txtInputuserid.Focus()
                End If
            End If
        Catch ex As Exception
            lblMessage.Text = ex.ToString
        End Try
    End Sub
#End Region

End Class