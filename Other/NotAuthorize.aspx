<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="NotAuthorize.aspx.vb" Inherits="Other_NotAuthorize" title="" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<table id="tableutama" align="center" style="width: 100%">
        <tr>
            <td style="width: 300px; height: 350px" valign="top">
                <table id="tbRight" bgcolor="white" class="tabelhias" style="vertical-align: top; height: 100%; width: 100%;">
                    <tr>
                        <th align="left" class="header" valign="center">
                            <img align="absMiddle" alt="" src="../Images/Menu.gif" />
                            User Active :.</th>
                    </tr>
                    <tr>
                        <td style="vertical-align: top; height: 100%;">
                            <table align="center" border="0" width="100%">
                                <tr>
                                    <td align="left" style="width: 26px; text-align: center">
                                        <asp:Image ID="Image2" runat="server" Height="24px" ImageUrl="~/Images/user.png"
                                            Width="24px" /></td>
                                    <td align="left">
                                        <asp:Label ID="lblWelcome" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                                            ForeColor="LightSkyBlue"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                    </td>
                                    <td align="left">
                                        </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="top" width="9">
                <br />
                &nbsp;
                <br />
            </td>
            <td style="height: 350px" valign="top">
                <table id="Table1" bgcolor="white" class="tabelhias"
                    width="100%" style="vertical-align: top; height: 100%">
                    <tr>
                        <th align="left" class="header" valign="center">
                            <img align="absMiddle" alt="" src="../Images/Menu.gif" /></th>
                    </tr>
                    <tr>
                        <td style="text-align: center; vertical-align: top; height: 100%;">
                            <table align="center" width="100%">
                                <tr>
                                    <td align="left" colspan="2">
                                        <span style="font-size: 12pt; color: navy"><strong>You don't have permission to access
                                            this page ...</strong></span></td>
                                </tr>
                            <tr>
                                <td align="left" colspan="2">
                                    <strong><span style="font-size: 12pt">
                                        <br />
                                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Other/menu.aspx" Font-Size="Small">Back To Menu</asp:HyperLink></span></strong></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    &nbsp;&nbsp;
                
</asp:Content>

