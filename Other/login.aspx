<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" 
CodeFile="login.aspx.vb" Inherits="login" title=""%>


<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="middleContent">  
    <table id="MainTable" align="center" style="width: 100%;">
        <tr>
            <td style="width: 220px;" valign="top" align="right">
                <table id="Table2Left" bgcolor="white" class="tabelhias" style="width: 100%;">
                    <tr>
                        <th align="left" valign="center" style="font-size: small;" class="header">
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />&nbsp; ::. LOGIN</th>
                    </tr>
                    <tr>
                        <td valign="top" align="center">
                            <table class="tabelhias" style="width: 100%;" id="Table3Left">
                                <tr>
                                    <td style="height: 30px" align="center">
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" style="height: 250px" align="center">
                                        <asp:Panel ID="Panel1" runat="server" DefaultButton="imbLogin" Width="100%">
                                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                                <contenttemplate>
<TABLE style="WIDTH: 100%" id="Table4Left"><TBODY><TR><TD class="Label" align=right>User ID :</TD><TD class="Label" align=left><asp:TextBox id="txtInputuserid" runat="server" Width="125px" CssClass="inpText" maxlength="10"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator id="rv_userid" runat="server" ControlToValidate="txtInputuserid" Display="None" ErrorMessage="* Please enter User ID"></asp:RequiredFieldValidator> </TD></TR><TR><TD class="Label" align=right>Password :</TD><TD class="Label" align=left><asp:TextBox id="txtInputPassword" runat="server" Width="125px" CssClass="inpText" TextMode="Password"></asp:TextBox> <asp:RequiredFieldValidator id="rv_password" runat="server" ControlToValidate="txtInputPassword" Display="None" ErrorMessage="* Please enter Password" Enabled="False"></asp:RequiredFieldValidator></TD></TR><TR><TD style="HEIGHT: 10px" align=center colSpan=2><ajaxToolkit:ValidatorCalloutExtender id="vce_password" runat="server" Enabled="False" TargetControlID="rv_password"></ajaxToolkit:ValidatorCalloutExtender><ajaxToolkit:ValidatorCalloutExtender id="vce_userid" runat="server" TargetControlID="rv_userid">
                                        </ajaxToolkit:ValidatorCalloutExtender></TD></TR><TR><TD align=center colSpan=2><asp:ImageButton id="imbLogin" runat="server" ImageUrl="~/Images/login.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 20px" align=center colSpan=2><asp:Label id="lblMessage" runat="server" CssClass="Important"></asp:Label></TD></TR></TBODY></TABLE>
</contenttemplate>
                                            </asp:UpdatePanel>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 5px;" valign="top" align="center">
            </td>
            <td valign="top" align="left">
                <table id="Table2Right" bgcolor="white" class="tabelhias" style="width: 100%;">
                <tr>
                    <th align="left" valign="center" style="font-size: small;" class="header">
                        <img align="absMiddle" alt="" src="../Images/corner.gif" />&nbsp; ::. COMPANY PROFILE</th>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <table width="100%" class="tabelhias" id="Table3Right">
                            <tr>
                                <td style="height: 30px;" colspan="2" align="right">
                                <asp:Label ID="lblCompny" runat="server" CssClass="Title" Font-Size="Large" ForeColor="#9A1D14"></asp:Label>
                                    &nbsp;&nbsp; &nbsp;
                                </td>
                            </tr>
                            <tr>
                            </tr>
                               <tr><td align="center" valign="top" style="height: 250px">
                                   <table id="Table4Right" style="width: 100%">
                                       <tr>
                                           <td>
                                               <div align=justify style="width: 95%;">
                                                   <font size="2"><font style="font-weight: bold; font-size: 8pt; color: dimgray">
    Company Profile ....</font></font></div>
                                           </td>
                                       </tr>
                                       <tr>
                                           <td>
                                    <img src="../Images/line.gif"></td>
                                       </tr>
                                       <tr>
                                           <td>
                                               <div align=justify style="width: 95%;">
                                                   <font size="2"><font style="font-weight: bold; font-size: 7pt; color: dimgray; font-family: Tahoma;"><asp:Label ID="lblAddr" runat="server"></asp:Label><br />
                                                       Telp.
                                    <asp:Label ID="lblPhone" runat="server"></asp:Label><br />
                                                       Fax.
                                    <asp:Label ID="lblFax" runat="server"></asp:Label><br />
                                                       Email :
                                    <asp:Label ID="lblEmail" runat="server"></asp:Label></font></font></div>
                                           </td>
                                       </tr>
                                   </table>
</td></tr>
                        </table>
                                 
                    </td>
                </tr>
            </table>
            </td>
        </tr>
    </table>
   
</asp:Content>