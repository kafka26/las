<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="WaitingActionNew.aspx.vb" Inherits="Other_WaitingActionNew" title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<table id="tableutama" align="center"
        width="100%">
        <tr>
            <td style="width: 200px;" valign="top">
                <table id="tbRight" bgcolor="white" class="tabelhias"
                    width="100%">
                    <tr>
                        <th align="left" class="header" style="height: 15px;" valign="middle" rowspan="2">
                            <asp:Image ID="Image2" runat="server" Height="16px" ImageUrl="~/Images/corner.gif"
                                            Width="16px" ImageAlign="AbsMiddle" />
                            ::
                            <asp:Label ID="lblWelcome" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Navy">Approval Type</asp:Label></th>
                    </tr>
                </table>
                <table class="tabelhias" style="width: 100%;">
                    <tr>
                        <td align="left" rowspan="2" valign="top">
                            <br />
                            <table width="100%">
                                <tr>
                                    <td style="height: 30px">
                            <asp:LinkButton ID="lkbBack" runat="server" CssClass="submenu" Font-Size="Small">� Back</asp:LinkButton></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="gvMenu" runat="server" AutoGenerateColumns="False" GridLines="None"
                                            ShowHeader="False" Width="100%">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lkbMenuApp" runat="server" CommandArgument='<%# eval("tablename") %>'
                                                            CssClass="submenu" Font-Size="Small" OnClick="lkbMenuApp_Click" Text='<%# eval("appname") %>'></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle Height="30px" HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <asp:UpdatePanel id="upMsgbox" runat="server">
                                <contenttemplate>
<asp:Panel id="PanelMsgBox" runat="server" CssClass="modalMsgBox" DefaultButton="btnMsgBoxOK" Visible="False"><TABLE cellSpacing=1 cellPadding=1 border=0><TR><TD style="COLOR: white; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" vAlign=top align=left colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2></TD></TR><TR><TD vAlign=top align=left><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image> </TD><TD vAlign=top align=left><asp:Label id="lblMessage" runat="server" ForeColor="Red"></asp:Label> </TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:Label id="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:ImageButton id="btnMsgBoxOK" onclick="btnErrOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsgbox" runat="server" TargetControlID="beMsgBox" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" PopupControlID="PanelMsgBox" DropShadow="True" Drag="True">
                        </ajaxToolkit:ModalPopupExtender> <asp:Button id="beMsgBox" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
                            </asp:UpdatePanel></td>
                    </tr>
                    <tr>
                    </tr>
                </table>
            </td>
            <td style="width: 1%;" valign="top">
            </td>
            <td valign="top">
                <table id="Table1" bgcolor="white" class="tabelhias"
                    width="100%">
                    <tr>
                        <th align="left" class="header" style="height: 15px;" valign="middle">
                            <img align="absMiddle" alt="" src="../Images/corner1.gif" /> :: 
                            <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                                ForeColor="Navy">Approval Data</asp:Label></th>
                    </tr>
                    <tr>
                        <td style="text-align: center" valign="top">
                            <div style="width: 100%; text-align: left">
                                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                    <contenttemplate>
<asp:MultiView id="MultiView1" runat="server" ActiveViewIndex="0"><asp:View id="View1" runat="server"><STRONG><BR />Following transactions are waiting for your approval :</STRONG><BR /><BR /><TABLE><TBODY><TR><TD align=left colSpan=2><asp:Label id="apppersonlevel" runat="server" Font-Bold="False" Visible="False"></asp:Label> <asp:Label id="apppersontype" runat="server" Font-Bold="False" Visible="False"></asp:Label> <asp:Label id="AppCmpCode" runat="server" Font-Bold="False" Visible="False"></asp:Label></TD></TR><TR><TD align=left colSpan=2><asp:GridView id="gvListApp" runat="server" Width="250px" ShowHeader="False" GridLines="None" AutoGenerateColumns="False"><Columns>
<asp:BoundField DataField="apptype">
<ItemStyle HorizontalAlign="Left" Height="15px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="appsept">
<ItemStyle HorizontalAlign="Center" Width="3%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="appcount">
<ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
</asp:BoundField>
</Columns>
</asp:GridView></TD></TR></TBODY></TABLE></asp:View> <asp:View id="ViewList" runat="server"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=3><asp:Label id="lblAppType" runat="server" Font-Bold="False" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Label id="lblTitleList" runat="server" ForeColor="Black" Font-Size="Small" Font-Bold="True" Font-Underline="True"></asp:Label></TD></TR><TR><TD style="WIDTH: 15%" class="Label" align=left><asp:CheckBox id="cbPeriod" runat="server" Text="Period"></asp:CheckBox></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="FilterPeriod1" runat="server" Width="60px" CssClass="inpText" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label5" runat="server" Text="to"></asp:Label>&nbsp;<asp:TextBox id="FilterPeriod2" runat="server" Width="60px" CssClass="inpText" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label9" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label1" runat="server" Text="Request User"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLUser" runat="server" Width="100px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label2" runat="server" Text="Business Unit"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLBusUnit" runat="server" Width="200px" CssClass="inpText" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD id="TDDept1" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label6" runat="server" Text="Department"></asp:Label></TD><TD id="TDDept2" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TDDept3" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="DDLDept" runat="server" Width="200px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text="Draft No."></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="FilterDraftNo" runat="server" Width="195px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:Label id="Label4" runat="server" CssClass="Important" Text="Separate each draft no with comma (,)"></asp:Label></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" TargetControlID="FilterPeriod1" Format="MM/dd/yyyy" PopupButtonID="imbDate1"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="FilterPeriod1" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" TargetControlID="FilterPeriod2" Format="MM/dd/yyyy" PopupButtonID="imbDate2"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" TargetControlID="FilterPeriod2" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbDraftNo" runat="server" TargetControlID="FilterDraftNo" ValidChars="1234567890,"></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=3></TD></TR><TR><TD class="Label" align=left colSpan=3>&nbsp;<asp:GridView id="gvList" runat="server" ForeColor="#333333" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" Width="97%" Height="1px" GridLines="None" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data in database.">
<RowStyle BackColor="#EFF3FB"></RowStyle>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                                    <asp:Label ID="lblmsg2" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!"></asp:Label>
                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE></asp:View> <asp:View id="ViewData" runat="server"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=3></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Label id="lblTitleData" runat="server" ForeColor="Black" Font-Size="Small" Font-Bold="True" Font-Underline="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=3></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Label id="lblBusUnit" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Font-Underline="False" Font-Strikeout="False"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:GridView id="gvDataHeader" runat="server" Width="100%" ShowHeader="False" GridLines="None" AutoGenerateColumns="False"><Columns>
<asp:BoundField DataField="datacaption">
<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Height="5px" Width="15%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="datasept">
<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="2%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="datavalue">
<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>
</asp:GridView></TD></TR><TR><TD style="WIDTH: 15%" class="Label" align=left><asp:Label id="lblCaptReturn" runat="server" Width="80px" Visible="False" Text="Return Type"></asp:Label></TD><TD style="WIDTH: 2%" class="Label" align=center><asp:Label id="lblSeptReturn" runat="server" Visible="False" Text=":"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="DDLReturnType" runat="server" Width="150px" CssClass="inpText" Visible="False"><asp:ListItem Value="MR">Retur MR</asp:ListItem>
<asp:ListItem Value="PI">Retur PI</asp:ListItem>
<asp:ListItem Value="PAY">Return Payment</asp:ListItem>
</asp:DropDownList> <asp:Label id="sTaxType" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 15%" class="Label" align=left><asp:Label id="lblreason" runat="server" Visible="False" Text="Reason"></asp:Label></TD><TD style="WIDTH: 2%" class="Label" align=center><asp:Label id="lblSeptReason" runat="server" Visible="False" Text=":"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="ActionReason" runat="server" Width="300px" CssClass="inpText" Visible="False" MaxLength="250"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 15%" class="Label" align=left><asp:Label id="lblCaptValue" runat="server" Visible="False" Text="Value Per Unit"></asp:Label></TD><TD style="WIDTH: 2%" class="Label" align=center><asp:Label id="lblSeptValue" runat="server" Visible="False" Text=":"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="tbValue" runat="server" Width="100px" CssClass="inpText" Visible="False" MaxLength="14"></asp:TextBox> <asp:ImageButton id="btnInsert" runat="server" ImageUrl="~/Images/plus.png" ImageAlign="AbsMiddle" Visible="False" AlternateText="Insert Value"></asp:ImageButton> </TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=3><ajaxToolkit:FilteredTextBoxExtender id="ftbValue" runat="server" TargetControlID="tbValue" ValidChars="1234567890,.">
            </ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD class="Label" align=left colSpan=3>&nbsp;<asp:GridView id="gvData" runat="server" ForeColor="#333333" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" Width="97%" Height="1px" GridLines="None" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data in database.">
<RowStyle BackColor="#EFF3FB"></RowStyle>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                                        <asp:Label ID="lblmsg2" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!"></asp:Label>
                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:GridView id="gvDataAdj" runat="server" ForeColor="#333333" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" Width="100%" Height="1px" GridLines="None" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data in database." AllowPaging="True" PageSize="20">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#EFF3FB"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="refname" HeaderText="Type">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="refcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
    <asp:BoundField DataField="itemoldcode" HeaderText="Old Code">
        <HeaderStyle CssClass="gvhdr" Font-Size="XX-Small" HorizontalAlign="Left" />
        <ItemStyle Font-Size="XX-Small" HorizontalAlign="Left" />
    </asp:BoundField>
<asp:BoundField DataField="reflongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="location" HeaderText="Location">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="lastqty" HeaderText="Current Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="adjqty" HeaderText="New Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Value "><ItemTemplate>
                            <asp:TextBox ID="tbValue" runat="server" CssClass="inpText" Font-Size="XX-Small"
                                MaxLength="14" Text='<%# eval("stockvalueidr") %>' ToolTip='<%# eval("oid") %>'
                                Width="75px"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbValue" runat="server" TargetControlID="tbValue"
                                ValidChars="1234567890,.">
                            </ajaxToolkit:FilteredTextBoxExtender>
                        
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" Font-Size="XX-Small" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                    <asp:Label ID="lblmsg2" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!"></asp:Label>
                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=3><asp:Label id="LblinfoSO" runat="server" ForeColor="Red" Font-Bold="False" Text="*Jika Price Berwarna Merah maka Price < Price list Item"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:GridView id="gvDataFooter" runat="server" Width="100%" ShowHeader="False" GridLines="None" AutoGenerateColumns="False"><Columns>
<asp:BoundField DataField="datacaption">
<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Height="5px" Width="15%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="datasept">
<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="2%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="datavalue">
<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="25%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dataempty">
<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=3></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:ImageButton id="btnApprove" runat="server" ImageUrl="~/Images/approve.png" ImageAlign="AbsMiddle" AlternateText="Approve"></asp:ImageButton> <asp:ImageButton id="btnRevise" runat="server" ImageUrl="~/Images/revise.png" ImageAlign="AbsMiddle" AlternateText="Revise"></asp:ImageButton> <asp:ImageButton id="btnReject" runat="server" ImageUrl="~/Images/reject.png" ImageAlign="AbsMiddle" AlternateText="Reject"></asp:ImageButton> <asp:ImageButton id="btnBack" runat="server" ImageUrl="~/Images/Back.png" ImageAlign="AbsMiddle" AlternateText="Back"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:View></asp:MultiView> <DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="upProgWA" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgWA" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></DIV>
</contenttemplate>
                                </asp:UpdatePanel></div>
                            </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
                
</asp:Content>

