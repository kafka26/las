Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_Currency
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        If currcode.Text = "" Then
            sError &= "- Please fill CODE field!"
        End If
        If currdesc.Text = "" Then
            sError &= "<BR>- Please fill DESCRIPTION field!"
        End If
        If currsymbol.Text = "" Then
            sError &= "<BR>- Please fill SYMBOL field!"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsCodeExists() As Boolean
        sSql = "SELECT COUNT(*) FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND currcode='" & Tchar(currcode.Text) & "'"
        If Not Session("oid") Is Nothing And Session("oid") <> "" Then
            sSql &= " AND curroid<>" & Session("oid")
        End If
        If ToDouble(GetStrData(sSql).ToString) > 0 Then
            showMessage("Code have been used by another data. Please fill another Code!", 2)
            Return True
        End If
        Return False
    End Function

    Private Function IsDescExists() As Boolean
        sSql = "SELECT COUNT(*) FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND currdesc='" & Tchar(currdesc.Text) & "'"
        If Not Session("oid") Is Nothing And Session("oid") <> "" Then
            sSql &= " AND curroid<>" & Session("oid")
        End If
        If ToDouble(GetStrData(sSql).ToString) > 0 Then
            showMessage("Description have been used by another data. Please fill another Description!", 2)
            Return True
        End If
        Return False
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindMstData(ByVal sFilter As String)
        sSql = "SELECT curroid, currcode, currdesc, activeflag, currnote FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(sFilter) & "%'"
        If cbStatus.Checked Then
            If FilterDDLStatus.SelectedValue <> "ALL" Then
                sSql &= " AND activeflag='" & FilterDDLStatus.SelectedValue & "'"
            End If
        End If
        If checkPagePermission("~\Master\mstCurr.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        sSql &= " ORDER BY curroid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_mstcurr")
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub FillTextBox()
        Try
            sSql = "SELECT curroid, currcode, currdesc, activeflag, currnote, createuser, createtime, upduser, updtime, currsymbol FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND curroid=" & Session("oid")
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            btnDelete.Visible = False
            While xreader.Read
                curroid.Text = xreader("curroid").ToString
                currcode.Text = xreader("currcode").ToString
                currcode.Enabled = False
                currcode.CssClass = "inpTextDisabled"
                currdesc.Text = xreader("currdesc").ToString
                currsymbol.Text = xreader("currsymbol").ToString
                activeflag.SelectedValue = xreader("activeflag").ToString
                currnote.Text = xreader("currnote").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
        Finally
            btnDelete.Visible = True
        End Try
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Master\mstCurr.aspx")
        End If
        If checkPagePermission("~\Master\mstCurr.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Currency"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            I_U.Text = "New Data"
        Else
            I_U.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox()
                TabContainer1.ActiveTabIndex = 1
            Else
                curroid.Text = GenerateID("QL_MSTCURR", CompnyCode)
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
    End Sub

    Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvMst.DataSource = Session("TblMst")
            gvMst.DataBind()
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            If Not IsCodeExists() And Not IsDescExists() Then
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    curroid.Text = GenerateID("QL_MSTCURR", CompnyCode)
                End If
                Dim objTrans As SqlClient.SqlTransaction
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If
                objTrans = conn.BeginTransaction()
                xCmd.Transaction = objTrans
                Try
                    If Session("oid") = Nothing Or Session("oid") = "" Then
                        sSql = "INSERT INTO QL_mstcurr (cmpcode, curroid, currcode, currdesc, activeflag, currnote, createuser, createtime, upduser, updtime, currsymbol) VALUES('" & CompnyCode & "', " & curroid.Text & ", '" & Tchar(currcode.Text.ToUpper) & "', '" & Tchar(currdesc.Text.ToUpper) & "', '" & activeflag.SelectedValue & "', '" & Tchar(currnote.Text) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Tchar(currsymbol.Text) & "' )"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        sSql = "UPDATE QL_mstoid SET lastoid=" & curroid.Text & " WHERE tablename LIKE 'QL_MSTCURR' AND cmpcode LIKE '%" & CompnyCode & "%'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Else
                        sSql = "UPDATE QL_mstcurr SET currcode='" & Tchar(currcode.Text.ToUpper) & "', currdesc='" & Tchar(currdesc.Text.ToUpper) & "', activeflag='" & activeflag.SelectedValue & "', currnote='" & Tchar(currnote.Text) & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, currsymbol='" & Tchar(currsymbol.Text) & "' WHERE cmpcode='" & CompnyCode & "' AND curroid=" & curroid.Text & ""
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If

                    sSql = "UPDATE QL_mstoid SET lastoid=" & curroid.Text & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_mstcurr'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    objTrans.Commit()
                    conn.Close()
                Catch ex As Exception
                    objTrans.Rollback()
                    conn.Close()
                    showMessage(ex.Message, 1)
                    Exit Sub
                End Try
                Response.Redirect("~\Master\mstCurr.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Master\mstCurr.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If curroid.Text = "" Then
            showMessage("Please select Currency data first!", 1)
            Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            'cek table pomst
            If CheckDataExists("SELECT curroid FROM QL_trnpomst WHERE curroid =" & curroid.Text) = True Then
                showMessage("CURRENCY TIDAK DAPAT DIHAPUS", 1)
                Exit Sub
            End If
            'cek table belimst
            If CheckDataExists("SELECT curroid FROM QL_trnbelimst WHERE curroid =" & curroid.Text) = True Then
                showMessage("CURRENCY TIDAK DAPAT DIHAPUS", 1)
                Exit Sub
            End If
            'cek table belimst_fa
            If CheckDataExists("SELECT curroid FROM QL_trnbelimst_fa WHERE curroid =" & curroid.Text) = True Then
                showMessage("CURRENCY TIDAK DAPAT DIHAPUS", 1)
                Exit Sub
            End If
            'cek table conap
            If CheckDataExists("SELECT curroid FROM QL_conap WHERE curroid =" & curroid.Text) = True Then
                showMessage("CURRENCY TIDAK DAPAT DIHAPUS", 1)
                Exit Sub
            End If
            'cek table fix asset mst
            If CheckDataExists("SELECT curroid FROM QL_trnfixmst WHERE curroid =" & curroid.Text) = True Then
                showMessage("CURRENCY TIDAK DAPAT DIHAPUS", 1)
                Exit Sub
            End If
            sSql = "DELETE QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND curroid=" & curroid.Text & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        If DeleteData("QL_mstcurr", "curroid", curroid.Text, CompnyCode) = True Then
            Response.Redirect("~\Master\mstCurr.aspx?awal=true")
        End If
    End Sub
#End Region

End Class
