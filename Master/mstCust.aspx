﻿<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="mstCust.aspx.vb" Inherits="Master_Customer" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Customer" CssClass="Title" ForeColor="SaddleBrown"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Customer :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left>Filter</TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="125px"><asp:ListItem Value="custcode">Code</asp:ListItem>
<asp:ListItem Value="custname">Name</asp:ListItem>
<asp:ListItem Value="custaddr">Address</asp:ListItem>
<asp:ListItem Value="g2.gendesc">City</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="200px"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="125px"><asp:ListItem>ACTIVE</asp:ListItem>
<asp:ListItem>INACTIVE</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnExportPDF" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnExportExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvMst" runat="server" BorderColor="#DEDFDE" ForeColor="Black" Width="100%" GridLines="None" AutoGenerateColumns="False" BorderStyle="None" BorderWidth="1px" BackColor="White" CellPadding="4" AllowPaging="True" PageSize="8" AllowSorting="True" OnSelectedIndexChanged="gvMst_SelectedIndexChanged">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#F7F7DE" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="custoid" DataNavigateUrlFormatString="~/Master/mstCust.aspx?oid={0}" DataTextField="custcode" HeaderText="Code" SortExpression="custcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="custname" HeaderText="Name" SortExpression="custname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custaddr" HeaderText="Address" SortExpression="custaddr">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
    <asp:BoundField DataField="custcity" HeaderText="City" SortExpression="custcity">
        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
        <ItemStyle HorizontalAlign="Left" />
    </asp:BoundField>
<asp:BoundField DataField="custphone" HeaderText="Phone" SortExpression="custphone">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custcp" HeaderText="Contact Person" SortExpression="custcp">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="activeflag" HeaderText="Status" SortExpression="activeflag">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#CCCC99"></FooterStyle>

<PagerStyle HorizontalAlign="Right" ForeColor="White" CssClass="gvhdr" Font-Bold="True"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White"></SelectedRowStyle>

<HeaderStyle BackColor="#6B696B" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view general data"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=4><asp:UpdateProgress id="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image21" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnExportPDF"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnExportExcel"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD style="WIDTH: 14%; HEIGHT: 26px" class="Label" align=left><asp:Label id="I_U" runat="server" CssClass="Important" Text="New Data"></asp:Label></TD><TD style="WIDTH: 1%; HEIGHT: 26px" class="Label" align=center></TD><TD style="WIDTH: 35%; HEIGHT: 26px" class="Label" align=left><asp:Label id="custoid" runat="server" Visible="False"></asp:Label> <asp:TextBox id="CodeCust" runat="server" CssClass="inpText" Width="50px" Visible="False" AutoPostBack="True" OnTextChanged="custname_TextChanged" MaxLength="50"></asp:TextBox></TD><TD style="WIDTH: 14%; HEIGHT: 26px" class="Label" align=left><asp:Label id="Label3" runat="server" Text="Customer Group" Width="98px"></asp:Label></TD><TD style="WIDTH: 1%; HEIGHT: 26px" class="Label" align=center>:</TD><TD style="HEIGHT: 26px" class="Label" align=left><asp:DropDownList id="custgroupoid" runat="server" CssClass="inpText" Width="205px"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblCode" runat="server" Text="Code"></asp:Label> <asp:Label id="StarCode" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custcode" runat="server" CssClass="inpTextDisabled" Width="96px" MaxLength="30" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left>Old Code</TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="OldCode" runat="server" CssClass="inpText" Width="100px" MaxLength="20"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblName" runat="server" Text="Name"></asp:Label> <asp:Label id="Label2" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="custprefixoid" runat="server" CssClass="inpText" Width="65px"></asp:DropDownList> <asp:TextBox id="custname" runat="server" CssClass="inpText" Width="200px" AutoPostBack="True" OnTextChanged="custname_TextChanged" MaxLength="500"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="lblType" runat="server" Text="Type"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="custtype" runat="server" CssClass="inpText" Width="105px"><asp:ListItem>LOCAL</asp:ListItem>
<asp:ListItem>EXPORT</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblTolerance" runat="server" Text="Taxable"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="custtaxable" runat="server" CssClass="inpText" Width="105px"><asp:ListItem Value="1">TAX</asp:ListItem>
<asp:ListItem Value="0">NON TAX</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label4" runat="server" Text="Default TOP"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="custpaymentoid" runat="server" CssClass="inpText" Width="155px"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblAddress" runat="server" Text="Address"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custaddr" runat="server" CssClass="inpText" Width="260px" MaxLength="500" Height="50px" TextMode="MultiLine"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="lblNPWP" runat="server" Text="NPWP"></asp:Label> <asp:Label id="Label5" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custnpwp" runat="server" CssClass="inpText" Width="150px" MaxLength="30"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label21" runat="server" Text="Kode Komisi" __designer:wfdid="w4"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="kode_komisi" runat="server" CssClass="inpText" Width="260px" __designer:wfdid="w3"><asp:ListItem Value="JPU">Jual Putus (JPU)</asp:ListItem>
<asp:ListItem Value="KTS">Konsinasi Tanpa SPG (KTS)</asp:ListItem>
<asp:ListItem Value="KSG">Konsinasi SPG
 (KSG)</asp:ListItem>
<asp:ListItem Value="LP">Luar Pulau (LP)</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblPhone1" runat="server" Text="Phone 1"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custphone1" runat="server" CssClass="inpText" Width="100px" MaxLength="20"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="lblCity" runat="server" Text="City"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="custcityoid" runat="server" CssClass="inpText" Width="205px" AutoPostBack="True" OnSelectedIndexChanged="custcityoid_SelectedIndexChanged"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblPhone2" runat="server" Text="Phone 2"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custphone2" runat="server" CssClass="inpText" Width="100px" MaxLength="20"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="lblFax1" runat="server" Text="Fax 1"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custfax1" runat="server" CssClass="inpText" Width="100px" MaxLength="20"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label13" runat="server" Text="Phone 3"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custphone3" runat="server" CssClass="inpText" Width="100px" MaxLength="20"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="lblFax2" runat="server" Text="Fax 2"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custfax2" runat="server" CssClass="inpText" Width="100px" MaxLength="20"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblEmail" runat="server" Text="Email"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custemail" runat="server" CssClass="inpText" Width="150px" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="lblWebsite" runat="server" Text="Website"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custwebsite" runat="server" CssClass="inpText" Width="150px" MaxLength="100"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblCP1Name" runat="server" Text="CP 1 Name"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custcp1name" runat="server" CssClass="inpText" Width="200px" MaxLength="50"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label8" runat="server" Text="CP 2 Name"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custcp2name" runat="server" CssClass="inpText" Width="200px" MaxLength="50"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label9" runat="server" Text="CP 1 Phone" Width="75px" Height="4px"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custcp1phone" runat="server" CssClass="inpText" Width="100px" MaxLength="20"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label10" runat="server" Text="CP 2 Phone"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custcp2phone" runat="server" CssClass="inpText" Width="100px" MaxLength="20"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label11" runat="server" Text="CP 1 Email"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custcp1email" runat="server" CssClass="inpText" Width="150px" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label12" runat="server" Text="CP 2 Email"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custcp2email" runat="server" CssClass="inpText" Width="150px" MaxLength="100"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label6" runat="server" Text="Header Disc."></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custheaddisc" runat="server" CssClass="inpText" Width="56px" AutoPostBack="True" OnTextChanged="custheaddisc_TextChanged" MaxLength="6"></asp:TextBox> <asp:Label id="Label7" runat="server" CssClass="Important" Text="%"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblNote" runat="server" Text="Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custnote" runat="server" CssClass="inpText" Width="260px" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="lblStatus" runat="server" Text="Status"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="activeflag" runat="server" CssClass="inpText" Width="105px"><asp:ListItem>ACTIVE</asp:ListItem>
<asp:ListItem>INACTIVE</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label14" runat="server" Text="Limit Konsinyasi (Rp)" __designer:wfdid="w2"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="credit_limit" runat="server" CssClass="inpText" Width="150px" AutoPostBack="True" OnTextChanged="credit_limit_TextChanged" MaxLength="16" __designer:wfdid="w3"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label15" runat="server" Text="Terpakai" __designer:wfdid="w2"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="limit_usage" runat="server" CssClass="inpTextDisabled" Width="150px" MaxLength="100" Enabled="False" __designer:wfdid="w5"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label16" runat="server" Text="Sales" __designer:wfdid="w1"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="person_id" runat="server" CssClass="inpText" Width="260px" __designer:wfdid="w2"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label17" runat="server" Text="Gudang" __designer:wfdid="w2"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="warehouse_id" runat="server" CssClass="inpText" Width="260px" __designer:wfdid="w4"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label18" runat="server" Text="Disc. 1" __designer:wfdid="w1"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="tbDisc1" runat="server" CssClass="inpText" Width="56px" MaxLength="6" __designer:wfdid="w11"></asp:TextBox>&nbsp;<asp:Label id="Label22" runat="server" CssClass="Important" Text="%" __designer:wfdid="w14"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label19" runat="server" Text="Disc. 2" __designer:wfdid="w1"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="tbDisc2" runat="server" CssClass="inpText" Width="56px" MaxLength="6" __designer:wfdid="w12"></asp:TextBox>&nbsp;<asp:Label id="Label25" runat="server" CssClass="Important" Text="%" __designer:wfdid="w16"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label20" runat="server" Text="Disc. 3" __designer:wfdid="w1"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="tbDisc3" runat="server" CssClass="inpText" Width="56px" MaxLength="6" __designer:wfdid="w13"></asp:TextBox>&nbsp;<asp:Label id="Label24" runat="server" CssClass="Important" Text="%" __designer:wfdid="w15"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label26" runat="server" Text="Initial Disc." __designer:wfdid="w1"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:CheckBox id="cbInit" runat="server" Text="Update Semua SJK" __designer:wfdid="w23"></asp:CheckBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label23" runat="server" Visible="False">Tag</asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=4><asp:Panel id="pnlTag" runat="server" CssClass="inpText" Width="100%" Visible="False" Height="100px" ScrollBars="Vertical"><asp:CheckBoxList id="custres3" runat="server" CssClass="inpText" Width="97%" RepeatColumns="3"></asp:CheckBoxList></asp:Panel> </TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="ftbPhone1" runat="server" TargetControlID="custphone1" ValidChars=" 1234567890-+*#()">
            </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbPhone2" runat="server" TargetControlID="custphone2" ValidChars=" 1234567890-+*#()">
            </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbPhone3" runat="server" TargetControlID="custphone3" ValidChars=" 1234567890-+*#()">
            </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbFax1" runat="server" TargetControlID="custfax1" ValidChars=" 1234567890-+*#()">
            </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbFax2" runat="server" TargetControlID="custfax2" ValidChars=" 1234567890-+*#()">
            </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbPhoneCP1" runat="server" TargetControlID="custcp1phone" ValidChars=" 1234567890-+*#()">
            </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbPhoneCP2" runat="server" TargetControlID="custcp2phone" ValidChars=" 1234567890-+*#()">
            </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbcustcode" runat="server" TargetControlID="custcode" FilterMode="InvalidChars" InvalidChars=" "></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FteHeaderDisc" runat="server" TargetControlID="custheaddisc" ValidChars="0123456789.," InvalidChars=" ">
            </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftb_credit_limit" runat="server" __designer:wfdid="w6" TargetControlID="credit_limit" ValidChars="0123456789.," InvalidChars=" "></ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=6></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Create By <asp:Label id="createuser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Customer :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

