Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_PreCosting
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsMatExistsInBOM(ByVal sType As String, ByVal sOid As String) As Boolean
        sSql = "SELECT COUNT(*) FROM QL_mstbomdtl2 bod INNER JOIN QL_mstbom bom ON bom.cmpcode=bod.cmpcode AND bom.bomoid=bod.bomoid WHERE bod.cmpcode='" & DDLBusUnit.SelectedValue & "' AND precostdtlrefoid=" & sOid & " AND precostoid=" & precostoid.Text & ""
        If ToDouble(GetStrData(sSql).ToString) > 0 Then
            Return True
        End If
        Return False
    End Function

    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function UpdateCheckedListMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblListMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblListMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "precostdtlrefoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("precostdtlamount") = Math.Round(ToDouble(GetTextBoxValue(row.Cells(3).Controls)), 4, MidpointRounding.AwayFromZero)
                                    dtView(0)("curroid") = ToInteger(GetDropDownValue(row.Cells(3).Controls))
                                    dtView(0)("currcode") = GetDropDownValue(row.Cells(3).Controls, "Text").ToString
                                    dtView(0)("precostdtlnote") = GetTextBoxValue(row.Cells(4).Controls)
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblListMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function GetDropDownValue(ByVal cc As System.Web.UI.ControlCollection, Optional ByVal sText As String = "") As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                If sText = "" Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
                Else
                    sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedItem.Text
                End If
            End If
        Next
        Return sReturn
    End Function

    Private Function UpdateCheckedListMatView() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblListMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblListMat")
            Dim dtTbl2 As DataTable = Session("TblListMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "precostdtlrefoid=" & cbOid
                                dtView2.RowFilter = "precostdtlrefoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("precostdtlamount") = Math.Round(ToDouble(GetTextBoxValue(row.Cells(3).Controls)), 4, MidpointRounding.AwayFromZero)
                                    dtView(0)("curroid") = ToInteger(GetDropDownValue(row.Cells(3).Controls))
                                    dtView(0)("currcode") = GetDropDownValue(row.Cells(3).Controls, "Text").ToString
                                    dtView(0)("precostdtlnote") = GetTextBoxValue(row.Cells(4).Controls)
                                    If dtView2.Count > 0 Then
                                        dtView2(0)("precostdtlamount") = Math.Round(ToDouble(GetTextBoxValue(row.Cells(3).Controls)), 4, MidpointRounding.AwayFromZero)
                                        dtView2(0)("curroid") = ToInteger(GetDropDownValue(row.Cells(3).Controls))
                                        dtView2(0)("currcode") = GetDropDownValue(row.Cells(3).Controls, "Text").ToString
                                        dtView2(0)("precostdtlnote") = GetTextBoxValue(row.Cells(4).Controls)
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblListMat") = dtTbl
                Session("TblListMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        Dim sErrReply As String = ""
        If precostdtlrefoid.Text = "" Then
            sError &= "- Please select BUDGET ITEM field!<BR>"
        End If
        If precostdtlamount.Text = "" Then
            sError &= "- Please fill BUDGET field!<BR>"
        Else
            If ToDouble(precostdtlamount.Text) <= 0 Then
                sError &= "- BUDGET field must be more than 0!<BR>"
            Else
                If Not isLengthAccepted("precostdtlamount", "QL_mstprecostdtl", "BUDGET", ToDouble(precostdtlamount.Text), sErrReply) Then
                    sError &= "- BUDGET field must be less than MAX BUDGET (" & sErrReply & ") allowed stored in database!<BR>"
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid(Optional ByVal isSaveAs As Boolean = False) As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        Dim sErrReply As String = ""
        If itemoid.Text = "" Then
            sError &= "- Please select FINISH GOOD field!<BR>"
        Else
            sSql = "SELECT COUNT(*) FROM QL_mstprecost WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND itemoid=" & itemoid.Text
            If Not isSaveAs Then
                If Session("oid") IsNot Nothing And Session("oid") <> "" Then
                    sSql &= " AND precostoid<>" & Session("oid")
                End If
            End If
            If CheckDataExists(sSql) Then
                sError &= "- FINISH GOOD has been used by another data. Please check that every FINISH GOOD only available for 1 PRE COSTING data!<BR>"
            End If
        End If
        If precostdate.Text = "" Then
            sError &= "- Please fill DATE field!<BR>"
            sErr = "None"
        Else
            If Not IsValidDate(precostdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If Not isLengthAccepted("precosttotalmatamt", "QL_mstprecost", "TOTAL BUDGET (IDR)", ToDouble(precosttotalmatamt.Text), sErrReply) Then
            sError &= "- TOTAL BUDGET (IDR) field must be less than MAX TOTAL BUDGET (IDR) (" & sErrReply & ") allowed stored in database!<BR>"
        End If
        If precostdlcamt.Text = "" Then
            sError &= "- Please fill DLC COST field!<BR>"
        Else
            If ToDouble(precostdlcamt.Text) <= 0 Then
                sError &= "- DLC COST field must be more than 0!<BR>"
            Else
                If Not isLengthAccepted("precostdlcamt", "QL_mstprecost", "DLC COST", ToDouble(precostdlcamt.Text), sErrReply) Then
                    sError &= "- DLC COST field must be less than MAX DLC COST (" & sErrReply & ") allowed stored in database!<BR>"
                End If
            End If
        End If
        If precostoverheadamt.Text = "" Then
            sError &= "- Please fill OVERHEAD COST field!<BR>"
        Else
            If ToDouble(precostoverheadamt.Text) <= 0 Then
                sError &= "- OVERHEAD COST field must be more than 0!<BR>"
            Else
                If Not isLengthAccepted("precostoverheadamt", "QL_mstprecost", "OVERHEAD COST", ToDouble(precostoverheadamt.Text), sErrReply) Then
                    sError &= "- OVERHEAD COST field must be less than MAX OVERHEAD COST (" & sErrReply & ") allowed stored in database!<BR>"
                End If
            End If
        End If
        If precostsalesprice.Text = "" Then
            sError &= "- Please fill RECM. SALES PRICE field!<BR>"
        Else
            If ToDouble(precostsalesprice.Text) <= 0 Then
                sError &= "- RECM. SALES PRICE field must be more than 0!<BR>"
            Else
                If Not isLengthAccepted("precostsalesprice", "QL_mstprecost", "RECM. SALES PRICE", ToDouble(precostsalesprice.Text), sErrReply) Then
                    sError &= "- RECM. SALES PRICE field must be less than MAX RECM. SALES PRICE (" & sErrReply & ") allowed stored in database!<BR>"
                End If
            End If
        End If
        'If ToDouble(cbf.Text) <= 0 Then
        '    sError &= "- CBF field must be more than 0!<BR>"
        'End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill Detail Data!<BR>"
        Else
            If Session("TblDtl") IsNot Nothing Then
                Dim dtTbl As DataTable = Session("TblDtl")
                If dtTbl.Rows.Count <= 0 Then
                    sError &= "- Please fill Detail Data!<BR>"
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetRateIDR(ByVal iCurr As Integer) As Double
        Dim dRet As Double = 0
        Try
            sSql = "SELECT TOP 1 rate2res1 FROM QL_mstrate2 WHERE cmpcode='" & CompnyCode & "' AND curroid=" & iCurr & " AND rate2month=MONTH(GETDATE()) AND rate2year=YEAR(GETDATE()) ORDER BY updtime DESC"
            dRet = ToDouble(GetStrData(sSql))
        Catch ex As Exception
            dRet = 0
        End Try
        Return dRet
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitAllDDL()
        ' Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        FillDDL(DDLBusUnit, sSql)
        ' Fill DDL Currency Overhead
        sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        If FillDDL(curroid_overhead, sSql) Then
            curroid_overhead_SelectedIndexChanged(Nothing, Nothing)
        End If
        ' Fill DDL Currency Sales Price
        sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        If FillDDL(curroid_salesprice, sSql) Then
            curroid_salesprice_SelectedIndexChanged(Nothing, Nothing)
        End If
        ' Fill DDL Currency Budget
        sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        FillDDL(curroid_budget, sSql)
    End Sub

    Private Sub DisableGridViewCell()
        If Not Session("oid") Is Nothing And Session("oid") <> "" Then
            If Not Session("TblDtl") Is Nothing Then
                Dim dt As DataTable = Session("TblDtl")
                If dt.Rows.Count > 0 Then
                    If dt.Rows.Count = gvDtl.Rows.Count Then
                        For C1 As Integer = 0 To dt.Rows.Count - 1
                            If IsMatExistsInBOM("", dt.Rows(C1)("precostdtlrefoid").ToString) Then
                                gvDtl.Rows(C1).Cells(gvDtl.Columns.Count - 1).Enabled = False
                            Else
                                gvDtl.Rows(C1).Cells(gvDtl.Columns.Count - 1).Enabled = True
                            End If
                        Next
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT precostoid, CONVERT(VARCHAR(10), precostdate, 101) AS precostdate, pr.activeflag, ISNULL((SELECT DISTINCT 'Created' FROM QL_mstbom bom WHERE bom.cmpcode=pr.cmpcode AND bom.precostoid=pr.precostoid), '' ) AS bomstatus, ISNULL((SELECT DISTINCT 'Created' FROM QL_mstdlc dlc INNER JOIN QL_mstbom bom ON bom.cmpcode=dlc.cmpcode AND bom.bomoid=dlc.bomoid WHERE dlc.cmpcode=pr.cmpcode AND bom.precostoid=pr.precostoid), '') AS dlcstatus, precostnote, itemlongdescription AS itemlongdesc, 'False' AS checkvalue, pr.cmpcode, divname, itemcode, 'Edit' AS edittext, 'Save As' AS saveastext FROM QL_mstprecost pr INNER JOIN QL_mstitem i ON i.itemoid=pr.itemoid INNER JOIN QL_mstdivision div ON div.cmpcode=pr.cmpcode WHERE pr.cmpcode='" & CompnyCode & "' "
     
        sSql &= sSqlPlus & " ORDER BY CAST(precostdate AS DATETIME) DESC, precostoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_mstprecost")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "precostoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub PrintReport(Optional ByVal sType As String = "")
        Try
            Dim sOid As String = ""
            Dim sCode As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("precostoid").ToString & ","
                    If Not sCode.Contains("'" & dv(C1)("cmpcode").ToString.ToUpper & "'") Then
                        sCode &= "'" & dv(C1)("cmpcode").ToString.ToUpper & "',"
                    End If
                Next
                dv.RowFilter = ""
            End If
            If sType = "" Then
                report.Load(Server.MapPath(folderReport & "rptPreCosting.rpt"))
            Else
                report.Load(Server.MapPath(folderReport & "rptPreCostingExcel.rpt"))
            End If
            Dim sWhere As String = " WHERE"
            If Session("CompnyCode") <> CompnyCode Then
                sWhere &= " pr.cmpcode='" & Session("CompnyCode") & "'"
            Else
                If sCode <> "" Then
                    sWhere &= " pr.cmpcode IN (" & Left(sCode, sCode.Length - 1) & ")"
                Else
                    sWhere &= " pr.cmpcode LIKE '%'"
                End If
            End If
            If sOid = "" Then
                sWhere &= " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND pr.precostdate>=CAST('" & FilterPeriod1.Text & " 00:00:00' AS DATETIME) AND pr.precostdate<=CAST('" & FilterPeriod2.Text & " 23:59:59' AS DATETIME)"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sWhere &= " AND pr.activeflag='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Master\mstPreCosting.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND pr.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sWhere &= " AND pr.precostoid IN (" & sOid & ")"
            End If
            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("sSetDate", Format(GetServerTime(), "MM/dd/yyyy"))
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            If sType = "" Then
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "PreCostingPrintOut")
            Else
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "PreCostingPrintOut")
            End If
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message & ex.ToString, 1)
        End Try
        Response.Redirect("~\Master\mstPreCosting.aspx?awal=true")
    End Sub

    Private Sub BindListItem()
        sSql = "SELECT i.itemoid, i.itemcode, i.itemlongdescription AS itemshortdesc, /*(i.itempackvolume*1000000000 / 28316846.59)*/ 0.0 AS cbf FROM QL_mstitem i WHERE i.cmpcode='" & CompnyCode & "' AND i.itemoid NOT IN (SELECT pr.itemoid FROM QL_mstprecost pr WHERE pr.cmpcode='" & DDLBusUnit.SelectedValue & "') AND i.itemrecordstatus='ACTIVE' AND itemgroup='FG' AND " & FilterDDLListItem.SelectedValue & " LIKE '%" & Tchar(FilterTextListItem.Text) & "%' ORDER BY i.itemcode"
        FillGV(gvListItem, sSql, "QL_mstitem")
    End Sub

    Private Sub InitPreCostDesc()
        sSql = "SELECT ISNULL(MAX(precostcounter), 0) FROM QL_mstprecost WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND itemoid=" & itemoid.Text
        precostcounter.Text = ToDouble(CStr(GetStrData(sSql))) + 1
        precostdesc.Text = "Pre Costing " & itemshortdesc.Text & " " & precostcounter.Text
    End Sub

    Private Sub SumTotalCost()
        precostoverheadamtidr.Text = ToMaskEdit(ToDouble(ratetoidr_overhead.Text) * ToDouble(precostoverheadamt.Text), 4)
        precosttotalamt.Text = ToMaskEdit(ToDouble(precosttotalmatamt.Text) + ToDouble(precostdlcamt.Text) + ToDouble(precostoverheadamtidr.Text), 4)
        precostsalespriceidr.Text = ToMaskEdit(ToDouble(ratetoidr_salesprice.Text) * ToDouble(precostsalesprice.Text), 4)
        precostmarginamt.Text = ToMaskEdit(ToDouble(precostsalespriceidr.Text) - ToDouble(precosttotalamt.Text), 4)
    End Sub

    Private Sub BindListMat()
        sSql = "SELECT 'False' AS checkvalue, cat1oid AS precostdtlrefoid, cat1code AS precostdtlrefcode, (case rtrim(cat1shortdesc) when 'NONE' then rtrim(cat1shortdesc)+' ('+upper(cat1res1)+')' else cat1shortdesc end) precostdtlrefshortdesc, 0.0 AS precostdtlamount, '' AS precostdtlnote, 1 AS curroid, '' AS currcode FROM QL_mstcat1 c1 WHERE c1.cmpcode='" & CompnyCode & "' AND c1.activeflag='ACTIVE' AND c1.cat1res1<>'FG' ORDER BY cat1code"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstcat1")
        If dt.Rows.Count > 0 Then
            Session("TblListMat") = dt
            Session("TblListMatView") = dt
            gvListMat.DataSource = Session("TblListMatView")
            gvListMat.DataBind()
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            showMessage("Budget Item data can't found!", 2)
        End If
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("QL_mstprecostdtl")
        dtlTable.Columns.Add("precostdtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("precostdtlreftype", Type.GetType("System.String"))
        dtlTable.Columns.Add("precostdtlrefoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("precostdtlrefcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("precostdtlrefshortdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("precostdtlamount", Type.GetType("System.Double"))
        dtlTable.Columns.Add("precostdtlnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("curroid_budget", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("currcode_budget", Type.GetType("System.String"))
        dtlTable.Columns.Add("precostdtlamountidr", Type.GetType("System.Double"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub SumTotalMatCost()
        Dim dVal As Double = 0
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            For C1 As Integer = 0 To dt.Rows.Count - 1
                dVal += ToDouble(dt.Rows(C1)("precostdtlamountidr").ToString)
            Next
        End If
        precosttotalmatamt.Text = ToMaskEdit(dVal, 4)
        SumTotalCost()
    End Sub

    Private Sub ClearDetail()
        precostdtlseq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable = Session("TblDtl")
            precostdtlseq.Text = objTable.Rows.Count + 1
        End If
        i_u2.Text = "New Detail"
        precostdtlreftype.SelectedIndex = -1
        precostdtlrefoid.Text = ""
        precostdtlrefcode.Text = ""
        precostdtlrefshortdesc.Text = ""
        btnSearchMat.Visible = True
        precostdtlamount.Text = ""
        precostdtlnote.Text = ""
        curroid_budget.SelectedIndex = -1
        precostdtlamountidr.Text = ""
        ratetoidr_budget.Text = ""
        gvDtl.SelectedIndex = -1
    End Sub

    Private Sub FillTextBox(ByVal sOid As String, ByVal sType As String)
        sSql = "SELECT pr.cmpcode, precostoid, precostdate, precostcounter, precostdesc, pr.itemoid, precosttotalmatamt, precostdlcamt, precostoverheadamt, precostmarginamt, precosttotalamt, precostnote, pr.activeflag, pr.createuser, pr.createtime, pr.upduser, pr.updtime, itemlongdescription AS itemlongdesc, /*(itempackvolume*1000000000 / 28316846.59)*/ 0.0 AS cbf, precostlock, ISNULL((SELECT DISTINCT 'Created' FROM QL_mstbom bom WHERE bom.cmpcode=pr.cmpcode AND bom.precostoid=pr.precostoid), '' ) AS bomstatus, ISNULL((SELECT DISTINCT 'Created' FROM QL_mstdlc dlc INNER JOIN QL_mstbom bom ON bom.cmpcode=dlc.cmpcode AND bom.bomoid=dlc.bomoid WHERE dlc.cmpcode=pr.cmpcode AND bom.precostoid=pr.precostoid), '') AS dlcstatus, precostsalesprice, curroid_overhead, precostoverheadamtidr, curroid_salesprice, precostsalespriceidr FROM QL_mstprecost pr INNER JOIN QL_mstitem i ON i.itemoid=pr.itemoid WHERE pr.precostoid=" & sOid
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        Dim sLock As String = ""
        While xreader.Read
            DDLBusUnit.SelectedValue = Trim(xreader("cmpcode").ToString)
            precostoid.Text = Trim(xreader("precostoid").ToString)
            precostdate.Text = Format(xreader("precostdate"), "MM/dd/yyyy")
            precostcounter.Text = Trim(xreader("precostcounter").ToString)
            precostdesc.Text = Trim(xreader("precostdesc").ToString)
            itemoid.Text = Trim(xreader("itemoid").ToString)
            last_itemoid.Text = itemoid.Text
            itemshortdesc.Text = Trim(xreader("itemlongdesc").ToString)
            cbf.Text = ToMaskEdit(ToDouble(xreader("cbf").ToString), 4)
            precosttotalmatamt.Text = ToMaskEdit(ToDouble(xreader("precosttotalmatamt").ToString), 4)
            precostdlcamt.Text = ToMaskEdit(ToDouble(xreader("precostdlcamt").ToString), 4)
            precostoverheadamt.Text = ToMaskEdit(ToDouble(xreader("precostoverheadamt").ToString), 4)
            precostmarginamt.Text = ToMaskEdit(ToDouble(xreader("precostmarginamt").ToString), 4)
            precosttotalamt.Text = ToMaskEdit(ToDouble(xreader("precosttotalamt").ToString), 4)
            precostsalesprice.Text = ToMaskEdit(ToDouble(xreader("precostsalesprice").ToString), 4)
            precostnote.Text = Trim(xreader("precostnote").ToString)
            activeflag.SelectedValue = Trim(xreader("activeflag").ToString)
            createuser.Text = Trim(xreader("createuser").ToString)
            createtime.Text = Trim(xreader("createtime").ToString)
            upduser.Text = Trim(xreader("upduser").ToString)
            updtime.Text = Trim(xreader("updtime").ToString)
            sLock = Trim(xreader("precostlock").ToString)
            bom_status.Text = Trim(xreader("bomstatus").ToString)
            dlcstatus.Text = Trim(xreader("dlcstatus").ToString)
            curroid_overhead.SelectedValue = Trim(xreader("curroid_overhead").ToString)
            curroid_overhead_SelectedIndexChanged(Nothing, Nothing)
            curroid_salesprice.SelectedValue = Trim(xreader("curroid_salesprice").ToString)
            curroid_salesprice_SelectedIndexChanged(Nothing, Nothing)
        End While
        xreader.Close()
        conn.Close()
        If sLock = "Y" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnSaveAs.Visible = True
            precostlock.Text = "(LOCK)"
        Else
            btnSave.Visible = True
            btnDelete.Visible = True
            btnSaveAs.Visible = True
            precostlock.Text = "(UNLOCK)"
        End If
        If sType.ToUpper = "EDIT" Then
            DDLBusUnit.Enabled = False : DDLBusUnit.CssClass = "inpTextDisabled"
            btnSaveAs.Visible = False
            btnSearchItem.Visible = False : btnClearItem.Visible = False
        Else
            btnSave.Visible = False
            btnDelete.Visible = False
        End If
        sSql = "SELECT precostdtlseq, precostdtlreftype, precostdtlrefoid, cat1code AS precostdtlrefcode, cat1shortdesc AS precostdtlrefshortdesc, precostdtlamount, precostdtlnote, curroid_budget, currcode AS currcode_budget, (precostdtlamount * ISNULL((SELECT TOP 1 CAST(rate2res1 AS REAL) FROM QL_mstrate2 r2 WHERE r2.curroid=curroid_budget AND rate2month=MONTH(GETDATE()) AND rate2year=YEAR(GETDATE())ORDER BY rate2oid DESC), 0.0)) AS precostdtlamountidr FROM QL_mstprecostdtl prd INNER JOIN QL_mstcat1 c1 ON cat1oid=precostdtlrefoid INNER JOIN QL_mstcurr c ON c.curroid=curroid_budget WHERE precostoid=" & Session("oid") & " ORDER BY precostdtlseq"
        Session("TblDtl") = cKon.ambiltabel(sSql, "QL_mstprecostdtl")
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        DisableGridViewCell()
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Master\mstPreCosting.aspx")
        End If
        If checkPagePermission("~\Master\mstPreCosting.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Pre Costing"
        Session("oid") = Request.QueryString("oid")
        Session("type") = Request.QueryString("type")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            InitAllDDL()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"), Session("type"))
                TabContainer1.ActiveTabIndex = 1
            Else
                precostoid.Text = GenerateID("QL_MSTPRECOST", CompnyCode)
                precostdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                btnDelete.Visible = False
                btnSaveAs.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvDtl.DataSource = dt
            gvDtl.DataBind()
            DisableGridViewCell()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            End If
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Master\mstPreCosting.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND pr.precostdate>=CAST('" & FilterPeriod1.Text & " 00:00:00' AS DATETIME) AND pr.precostdate<=CAST('" & FilterPeriod2.Text & " 23:59:59' AS DATETIME)"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND pr.activeflag='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Master\mstPreCosting.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND pr.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Master\mstPreCosting.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND pr.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnPrintHdr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrintHdr.Click
        UpdateCheckedValue()
        PrintReport()
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExport.Click
        UpdateCheckedValue()
        PrintReport("PreCosting")
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchItem.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2) : Exit Sub
        End If
        FilterDDLListItem.SelectedIndex = -1 : FilterTextListItem.Text = "" : gvListItem.SelectedIndex = -1
        BindListItem()
        cProc.SetModalPopUpExtender(btnHideListItem, pnlListItem, mpeListItem, True)
    End Sub

    Protected Sub btnClearItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearItem.Click
        itemoid.Text = ""
        itemshortdesc.Text = ""
        cbf.Text = "0"
    End Sub

    Protected Sub btnFindListItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListItem.Click
        BindListItem()
        mpeListItem.Show()
    End Sub

    Protected Sub btnAllListItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListItem.Click
        FilterDDLListItem.SelectedIndex = -1 : FilterTextListItem.Text = "" : gvListItem.SelectedIndex = -1
        BindListItem()
        mpeListItem.Show()
    End Sub

    Protected Sub gvListItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListItem.PageIndexChanging
        gvListItem.PageIndex = e.NewPageIndex
        BindListItem()
        mpeListItem.Show()
    End Sub

    Protected Sub gvListItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListItem.SelectedIndexChanged
        itemoid.Text = gvListItem.SelectedDataKey.Item("itemoid").ToString
        itemshortdesc.Text = gvListItem.SelectedDataKey.Item("itemshortdesc").ToString
        cbf.Text = ToMaskEdit(ToDouble(gvListItem.SelectedDataKey.Item("cbf").ToString), 4)
        InitPreCostDesc()
        cProc.SetModalPopUpExtender(btnHideListItem, pnlListItem, mpeListItem, False)
    End Sub

    Protected Sub lkbCloseListItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListItem.Click
        cProc.SetModalPopUpExtender(btnHideListItem, pnlListItem, mpeListItem, False)
    End Sub

    Protected Sub precostdlcamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles precostdlcamt.TextChanged
        precostdlcamt.Text = ToMaskEdit(ToDouble(precostdlcamt.Text), 4)
        SumTotalCost()
    End Sub

    Protected Sub curroid_overhead_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles curroid_overhead.SelectedIndexChanged
        If curroid_overhead.SelectedValue <> "" Then
            Dim cRate As New ClassRate
            cRate.SetRateValue(ToInteger(curroid_overhead.SelectedValue), Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateMonthlyLastError <> "" Then
                lblWarn01.Text = "Rate Not Set"
                ratetoidr_overhead.Text = 0
            Else
                lblWarn01.Text = ""
                ratetoidr_overhead.Text = cRate.GetRateMonthlyIDRValue
            End If
        End If
        SumTotalCost()
    End Sub

    Protected Sub precostoverheadamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles precostoverheadamt.TextChanged
        precostoverheadamt.Text = ToMaskEdit(ToDouble(precostoverheadamt.Text), 4)
        SumTotalCost()
    End Sub

    Protected Sub curroid_salesprice_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles curroid_salesprice.SelectedIndexChanged
        If curroid_salesprice.SelectedValue <> "" Then
            Dim cRate As New ClassRate
            cRate.SetRateValue(ToInteger(curroid_salesprice.SelectedValue), Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateMonthlyLastError <> "" Then
                lblWarn02.Text = "Rate Not Set"
                ratetoidr_salesprice.Text = 0
            Else
                lblWarn02.Text = ""
                ratetoidr_salesprice.Text = cRate.GetRateMonthlyIDRValue
            End If
        End If
        SumTotalCost()
    End Sub

    Protected Sub precostsalesprice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles precostsalesprice.TextChanged
        precostsalesprice.Text = ToMaskEdit(ToDouble(precostsalesprice.Text), 4)
        SumTotalCost()
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : Session("TblListMat") = Nothing : Session("TblListMatView") = Nothing : gvListMat.DataSource = Nothing : gvListMat.DataBind()
        BindListMat()
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        Dim sPlus As String = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        If UpdateCheckedListMat() Then
            Dim dv As DataView = Session("TblListMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblListMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblListMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblListMatView") = Nothing
                gvListMat.DataSource = Session("TblListMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Budget Item data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If UpdateCheckedListMat() Then
            Dim dt As DataTable = Session("TblListMat")
            Session("TblListMatView") = dt
            gvListMat.DataSource = Session("TblListMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedListMatView() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblListMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(3).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 4)
                    End If
                ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                    sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
                    FillDDL(CType(myControl, System.Web.UI.WebControls.DropDownList), sSql)
                    CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue = CType(myControl, System.Web.UI.WebControls.DropDownList).ToolTip
                End If
            Next
        End If
    End Sub

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        If Not Session("TblListMat") Is Nothing Then
            If UpdateCheckedListMat() Then
                Dim dtTbl As DataTable = Session("TblListMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    dtView.RowFilter = ""
                    dtView.RowFilter = "checkvalue='True' AND precostdtlamount>0"
                    If dtView.Count <> iCheck Then
                        Session("WarningListMat") = "Budget for every checked Budget Item data must be more than 0!"
                        showMessage(Session("WarningListMat"), 2)
                        dtView.RowFilter = ""
                        Exit Sub
                    End If
                    If Session("TblDtl") Is Nothing Then
                        CreateTblDetail()
                    End If
                    Dim objTable As DataTable = Session("TblDtl")
                    Dim dv As DataView = objTable.DefaultView
                    Dim objRow As DataRow
                    Dim counter As Integer = objTable.Rows.Count
                    For C1 As Integer = 0 To dtView.Count - 1
                        dv.RowFilter = "precostdtlrefoid=" & dtView(C1)("precostdtlrefoid")
                        If dv.Count > 0 Then
                            dv.AllowEdit = True
                            dv(0)("precostdtlamount") = ToDouble(dtView(C1)("precostdtlamount").ToString)
                            dv(0)("precostdtlnote") = dtView(C1)("precostdtlnote").ToString
                            dv(0)("curroid_budget") = ToInteger(dtView(C1)("curroid").ToString)
                            dv(0)("currcode_budget") = dtView(C1)("currcode").ToString
                            dv(0)("precostdtlamountidr") = ToDouble(dtView(C1)("precostdtlamount").ToString) * GetRateIDR(ToInteger(dtView(C1)("curroid").ToString))
                        Else
                            counter += 1
                            objRow = objTable.NewRow()
                            objRow("precostdtlseq") = counter
                            objRow("precostdtlreftype") = ""
                            objRow("precostdtlrefoid") = ToInteger(dtView(C1)("precostdtlrefoid").ToString)
                            objRow("precostdtlrefcode") = dtView(C1)("precostdtlrefcode").ToString
                            objRow("precostdtlrefshortdesc") = dtView(C1)("precostdtlrefshortdesc").ToString
                            objRow("precostdtlamount") = ToDouble(dtView(C1)("precostdtlamount").ToString)
                            objRow("precostdtlnote") = dtView(C1)("precostdtlnote").ToString
                            objRow("curroid_budget") = ToInteger(dtView(C1)("curroid").ToString)
                            objRow("currcode_budget") = dtView(C1)("currcode").ToString
                            objRow("precostdtlamountidr") = ToDouble(dtView(C1)("precostdtlamount").ToString) * GetRateIDR(ToInteger(dtView(C1)("curroid").ToString))
                            objTable.Rows.Add(objRow)
                        End If
                        dv.RowFilter = ""
                    Next
                    dtView.RowFilter = ""
                    Session("TblDtl") = objTable
                    gvDtl.DataSource = objTable
                    gvDtl.DataBind()
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                    DisableGridViewCell()
                    SumTotalMatCost()
                    ClearDetail()
                Else
                    Session("WarningListMat") = "Please select Budget Item to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some Budget Item data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub curroid_budget_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles curroid_budget.SelectedIndexChanged
        If curroid_budget.SelectedValue <> "" Then
            Dim cRate As New ClassRate
            cRate.SetRateValue(ToInteger(curroid_budget.SelectedValue), Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateMonthlyLastError <> "" Then
                lblWarn03.Text = "Rate Not Set"
                ratetoidr_budget.Text = 0
            Else
                lblWarn03.Text = ""
                ratetoidr_budget.Text = cRate.GetRateMonthlyIDRValue
            End If
        End If
        precostdtlamount_TextChanged(Nothing, Nothing)
    End Sub

    Protected Sub precostdtlamount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles precostdtlamount.TextChanged
        precostdtlamountidr.Text = ToMaskEdit(ToDouble(precostdtlamount.Text) * ToDouble(ratetoidr_budget.Text), 4)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            If Session("TblDtl") Is Nothing Then
                CreateTblDetail()
            End If
            Dim objTable As DataTable = Session("TblDtl")
            Dim dv As DataView = objTable.DefaultView
            If i_u2.Text = "New Detail" Then
                dv.RowFilter = "precostdtlrefoid=" & precostdtlrefoid.Text
            Else
                dv.RowFilter = "precostdtlrefoid=" & precostdtlrefoid.Text & " AND precostdtlseq <>" & precostdtlseq.Text
            End If
            If dv.Count > 0 Then
                showMessage("This Data has been added before!", 2)
                dv.RowFilter = ""
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                objRow("precostdtlseq") = objTable.Rows.Count + 1
            Else
                objRow = objTable.Rows(precostdtlseq.Text - 1)
                objRow.BeginEdit()
            End If
            objRow("precostdtlreftype") = ""
            objRow("precostdtlrefoid") = precostdtlrefoid.Text
            objRow("precostdtlrefcode") = precostdtlrefcode.Text
            objRow("precostdtlrefshortdesc") = precostdtlrefshortdesc.Text
            objRow("precostdtlamount") = ToDouble(precostdtlamount.Text)
            objRow("precostdtlnote") = precostdtlnote.Text
            objRow("curroid_budget") = curroid_budget.SelectedValue
            objRow("currcode_budget") = curroid_budget.SelectedItem.Text
            objRow("precostdtlamountidr") = ToDouble(precostdtlamountidr.Text)
            If i_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            Session("TblDtl") = objTable
            gvDtl.DataSource = objTable
            gvDtl.DataBind()
            DisableGridViewCell()
            SumTotalMatCost()
            ClearDetail()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
        End If
    End Sub

    Protected Sub gvDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("precostdtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvDtl.DataSource = objTable
        gvDtl.DataBind()
        DisableGridViewCell()
        SumTotalMatCost()
        ClearDetail()
    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDtl.SelectedIndexChanged
        Try
            precostdtlseq.Text = gvDtl.SelectedDataKey.Item("precostdtlseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "precostdtlseq=" & precostdtlseq.Text
                btnSearchMat.Visible = False
                precostdtlrefoid.Text = dv.Item(0).Item("precostdtlrefoid").ToString
                precostdtlrefcode.Text = dv.Item(0).Item("precostdtlrefcode").ToString
                precostdtlrefshortdesc.Text = dv.Item(0).Item("precostdtlrefshortdesc").ToString
                precostdtlamount.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("precostdtlamount").ToString), 4)
                precostdtlnote.Text = dv.Item(0).Item("precostdtlnote").ToString
                curroid_budget.SelectedValue = dv.Item(0).Item("curroid_budget").ToString
                curroid_budget_SelectedIndexChanged(Nothing, Nothing)
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            If Session("oid") = Nothing Or Session("oid") = "" Then
                precostoid.Text = GenerateID("QL_MSTPRECOST", CompnyCode)
            End If
            precostdtloid.Text = GenerateID("QL_MSTPRECOSTDTL", CompnyCode)
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_mstprecost (cmpcode, precostoid, precostdate, precostcounter, precostdesc, itemoid, precosttotalmatamt, precostdlcamt, precostoverheadamt, precostmarginamt, precosttotalamt, precostnote, activeflag, bomstatus, dlcstatus, createuser, createtime, upduser, updtime, precostlock, precostsalesprice, curroid_overhead, precostoverheadamtidr, curroid_salesprice, precostsalespriceidr) VALUES ('" & DDLBusUnit.SelectedValue & "', " & precostoid.Text & ", '" & precostdate.Text & "', " & precostcounter.Text & ", '" & Tchar(precostdesc.Text) & "', " & itemoid.Text & ", " & ToDouble(precosttotalmatamt.Text) & ", " & ToDouble(precostdlcamt.Text) & ", " & ToDouble(precostoverheadamt.Text) & ", " & ToDouble(precostmarginamt.Text) & ", " & ToDouble(precosttotalamt.Text) & ", '" & Tchar(precostnote.Text) & "', '" & activeflag.SelectedValue & "', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, 'N', " & ToDouble(precostsalesprice.Text) & ", " & curroid_overhead.SelectedValue & ", " & ToDouble(precostoverheadamtidr.Text) & ", " & curroid_salesprice.SelectedValue & ", " & ToDouble(precostsalespriceidr.Text) & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & precostoid.Text & " WHERE tablename='QL_MSTPRECOST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_mstprecost SET precostdate='" & precostdate.Text & "', precostcounter=" & precostcounter.Text & ", precostdesc='" & Tchar(precostdesc.Text) & "', itemoid=" & itemoid.Text & ", precosttotalmatamt=" & ToDouble(precosttotalmatamt.Text) & ", precostdlcamt=" & ToDouble(precostdlcamt.Text) & ", precostoverheadamt=" & ToDouble(precostoverheadamt.Text) & ", precostmarginamt=" & ToDouble(precostmarginamt.Text) & ", precosttotalamt=" & ToDouble(precosttotalamt.Text) & ", precostnote='" & Tchar(precostnote.Text) & "', activeflag='" & activeflag.SelectedValue & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, precostlock='N', bomstatus='" & bom_status.Text & "', dlcstatus='" & dlcstatus.Text & "', precostsalesprice=" & ToDouble(precostsalesprice.Text) & ", curroid_overhead=" & curroid_overhead.SelectedValue & ", precostoverheadamtidr=" & ToDouble(precostoverheadamtidr.Text) & ", curroid_salesprice=" & curroid_salesprice.SelectedValue & ", precostsalespriceidr=" & ToDouble(precostsalespriceidr.Text) & " WHERE precostoid=" & precostoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    If objTable.Rows.Count > 0 Then
                        sSql = "DELETE FROM QL_mstprecostdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND precostoid=" & precostoid.Text
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        For C1 As Int16 = 0 To objTable.Rows.Count - 1
                            sSql = "INSERT INTO QL_mstprecostdtl (cmpcode, precostdtloid, precostoid, precostdtlseq, precostdtlreftype, precostdtlrefoid, precostdtlrefqty, precostdtlrefunitoid, precostdtlamount, precostdtltotalamt, precostdtlnote, precostdtlstatus, upduser, updtime, curroid_budget, precostdtlamountidr) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C1 + CInt(precostdtloid.Text)) & ", " & precostoid.Text & ", " & C1 + 1 & ", '" & objTable.Rows(C1).Item("precostdtlreftype").ToString & "', " & objTable.Rows(C1).Item("precostdtlrefoid") & ", 0, 0, " & ToDouble(objTable.Rows(C1).Item("precostdtlamount").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("precostdtlamount").ToString) & ", '" & Tchar(objTable.Rows(C1).Item("precostdtlnote").ToString) & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & objTable.Rows(C1).Item("curroid_budget") & ", " & ToDouble(objTable.Rows(C1).Item("precostdtlamountidr").ToString) & ")"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        Next
                        sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(precostdtloid.Text)) & " WHERE tablename='QL_MSTPRECOSTDTL' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                End If
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 2)
                conn.Close()
                Exit Sub
            End Try
            Response.Redirect("~\Master\mstPreCosting.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Master\mstPreCosting.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If precostoid.Text.Trim = "" Then
            showMessage("Please select Pre Costing data first!", 2)
            Exit Sub
        End If
        If CheckDataExists("SELECT COUNT(*) FROM QL_mstbom WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND precostoid=" & precostoid.Text) = True Then
            showMessage("This data can't be deleted because it is being used by another data!", 2)
            Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE FROM QL_mstprecostdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND precostoid=" & precostoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_mstprecost WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND precostoid=" & precostoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Master\mstPreCosting.aspx?awal=true")
    End Sub

    Protected Sub btnSaveAs_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSaveAs.Click
        If IsInputValid(True) Then
            precostoid.Text = GenerateID("QL_MSTPRECOST", CompnyCode)
            precostdtloid.Text = GenerateID("QL_MSTPRECOSTDTL", CompnyCode)
            InitPreCostDesc()
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                sSql = "INSERT INTO QL_mstprecost (cmpcode, precostoid, precostdate, precostcounter, precostdesc, itemoid, precosttotalmatamt, precostdlcamt, precostoverheadamt, precostmarginamt, precosttotalamt, precostnote, activeflag, bomstatus, dlcstatus, createuser, createtime, upduser, updtime, precostlock, precostsalesprice, curroid_overhead, precostoverheadamtidr, curroid_salesprice, precostsalespriceidr) VALUES ('" & DDLBusUnit.SelectedValue & "', " & precostoid.Text & ", '" & precostdate.Text & "', " & precostcounter.Text & ", '" & Tchar(precostdesc.Text) & "', " & itemoid.Text & ", " & ToDouble(precosttotalmatamt.Text) & ", " & ToDouble(precostdlcamt.Text) & ", " & ToDouble(precostoverheadamt.Text) & ", " & ToDouble(precostmarginamt.Text) & ", " & ToDouble(precosttotalamt.Text) & ", '" & Tchar(precostnote.Text) & "', '" & activeflag.SelectedValue & "', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & upduser.Text & "', CURRENT_TIMESTAMP, 'N', " & ToDouble(precostsalesprice.Text) & ", " & curroid_overhead.SelectedValue & ", " & ToDouble(precostoverheadamtidr.Text) & ", " & curroid_salesprice.SelectedValue & ", " & ToDouble(precostsalespriceidr.Text) & ")"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & precostoid.Text & " WHERE tablename='QL_MSTPRECOST' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_mstprecostdtl (cmpcode, precostdtloid, precostoid, precostdtlseq, precostdtlreftype, precostdtlrefoid, precostdtlrefqty, precostdtlrefunitoid, precostdtlamount, precostdtltotalamt, precostdtlnote, precostdtlstatus, upduser, updtime, curroid_budget, precostdtlamountidr) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C1 + CInt(precostdtloid.Text)) & ", " & precostoid.Text & ", " & C1 + 1 & ", '" & objTable.Rows(C1).Item("precostdtlreftype") & "', " & objTable.Rows(C1).Item("precostdtlrefoid") & ", 0, 0, " & ToDouble(objTable.Rows(C1).Item("precostdtlamount").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("precostdtlamount").ToString) & ", '" & Tchar(objTable.Rows(C1).Item("precostdtlnote").ToString) & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & objTable.Rows(C1).Item("curroid_budget") & ", " & ToDouble(objTable.Rows(C1).Item("precostdtlamountidr").ToString) & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(precostdtloid.Text)) & " WHERE tablename='QL_MSTPRECOSTDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                xCmd.Connection.Close()
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 2)
                xCmd.Connection.Close()
                Exit Sub
            End Try
            Session("SavedInfo") = "Data have been save with ID = " & precostoid.Text & " AND DESC = " & precostdesc.Text & "."
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            End If
        End If
    End Sub
#End Region

End Class