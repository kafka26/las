<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="mstDivision.aspx.vb" Inherits="Master_Division" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Business Unit" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Business Unit :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" DefaultButton="btnSearch" __designer:wfdid="w25"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 7%" class="Label" align=left>Filter</TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w26"><asp:ListItem Value="divcode">Code</asp:ListItem>
<asp:ListItem Value="divname">Name</asp:ListItem>
    <asp:ListItem Value="divpic">PIC</asp:ListItem>
    <asp:ListItem>Address</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="200px" __designer:wfdid="w27"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status" Checked="True" __designer:wfdid="w28"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w29">
                <asp:ListItem Value="All">ALL</asp:ListItem>
                <asp:ListItem Selected="True">ACTIVE</asp:ListItem>
                <asp:ListItem>INACTIVE</asp:ListItem>
            </asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w30"></asp:ImageButton> <asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w31"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w32"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvMst" runat="server" ForeColor="#333333" Width="100%" AllowSorting="True" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" PageSize="8" GridLines="None" __designer:wfdid="w33">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="divoid" DataNavigateUrlFormatString="~\Master\mstDivision.aspx?oid={0}" DataTextField="divcode" HeaderText="Code" SortExpression="divcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="divname" HeaderText="Name " SortExpression="divname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="creationdate" HeaderText="Creation Date" SortExpression="creationdate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="divpic" HeaderText="PIC" SortExpression="divpic">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="divaddress" HeaderText="Address" SortExpression="divaddress">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="divtotalperson" HeaderText="Total Employee" SortExpression="divtotalperson">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="Sienna" Font-Bold="False" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="There is no data !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w34"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD style="WIDTH: 13%" class="Label" align=left><asp:Label id="I_U" runat="server" CssClass="Important" Text="New Data" __designer:wfdid="w103"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 257px" class="Label" align=left><asp:Label id="divoid" runat="server" __designer:wfdid="w104" Visible="False"></asp:Label></TD><TD style="WIDTH: 13%" class="Label" align=left></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label2" runat="server" Text="Code" __designer:wfdid="w105"></asp:Label> <asp:Label id="Label16" runat="server" CssClass="Important" Text="*" __designer:wfdid="w106"></asp:Label></TD><TD class="Label" align=center>:</TD><TD style="WIDTH: 257px" class="Label" align=left><asp:TextBox id="divcode" runat="server" CssClass="inpText" Width="75px" __designer:wfdid="w107" MaxLength="10"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label7" runat="server" Text="Creation Date" __designer:wfdid="w108"></asp:Label> <asp:Label id="Label20" runat="server" CssClass="Important" Text="*" __designer:wfdid="w109"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="divcreationdate" runat="server" CssClass="inpText" Width="60px" __designer:wfdid="w110" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w111"></asp:ImageButton> <asp:Label id="Label21" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w112"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text="Name" __designer:wfdid="w113"></asp:Label> <asp:Label id="Label17" runat="server" CssClass="Important" Text="*" __designer:wfdid="w114"></asp:Label></TD><TD class="Label" align=center>:</TD><TD style="WIDTH: 257px" class="Label" align=left><asp:TextBox id="divname" runat="server" CssClass="inpText" Width="150px" __designer:wfdid="w115" MaxLength="50"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label8" runat="server" Text="PIC" __designer:wfdid="w116"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="divpic" runat="server" CssClass="inpText" Width="150px" __designer:wfdid="w117" MaxLength="50"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label4" runat="server" Text="Address" __designer:wfdid="w118"></asp:Label></TD><TD class="Label" align=center>:</TD><TD style="WIDTH: 257px" class="Label" align=left><asp:TextBox id="divaddress" runat="server" CssClass="inpText" Width="250px" __designer:wfdid="w119" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label27" runat="server" Text="Country" __designer:wfdid="w3"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="divcountryoid" runat="server" CssClass="inpText" Width="155px" __designer:wfdid="w4" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD style="WIDTH: 257px" class="Label" align=left><asp:TextBox id="divaddress1" runat="server" CssClass="inpText" Width="250px" __designer:wfdid="w2" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label28" runat="server" Text="Province" __designer:wfdid="w6"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="divprovinceoid" runat="server" CssClass="inpText" Width="155px" __designer:wfdid="w5" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD style="WIDTH: 257px" class="Label" align=left><asp:TextBox id="divaddress2" runat="server" CssClass="inpText" Width="250px" __designer:wfdid="w3" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label5" runat="server" Text="City" __designer:wfdid="w2"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="divcityoid" runat="server" CssClass="inpText" Width="155px" __designer:wfdid="w1">
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label6" runat="server" Text="Phone 1" __designer:wfdid="w122"></asp:Label></TD><TD class="Label" align=center>:</TD><TD style="WIDTH: 257px" class="Label" align=left><asp:TextBox id="divphone" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w123" MaxLength="20"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label25" runat="server" __designer:wfdid="w124">Fax 1</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="divFax1" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w125" MaxLength="20"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label23" runat="server" Text="Phone 2" __designer:wfdid="w126"></asp:Label></TD><TD class="Label" align=center>:</TD><TD style="WIDTH: 257px" class="Label" align=left><asp:TextBox id="divphone2" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w127" MaxLength="20"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label26" runat="server" __designer:wfdid="w128">Email E-Faktur</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="divFax2" runat="server" CssClass="inpText" Width="150px" __designer:wfdid="w129" MaxLength="20"></asp:TextBox>&nbsp;<asp:Label id="Label29" runat="server" CssClass="Important" Text="(mail@sample.com)" __designer:wfdid="w277"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label24" runat="server" Text="Postal Code" __designer:wfdid="w130"></asp:Label></TD><TD class="Label" align=center>:</TD><TD style="WIDTH: 257px" class="Label" align=left><asp:TextBox id="divpostcode" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w131" MaxLength="20"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label10" runat="server" Text="Email Office" __designer:wfdid="w132"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="divemail" runat="server" CssClass="inpText" Width="150px" __designer:wfdid="w133" MaxLength="50"></asp:TextBox><asp:Label id="Label22" runat="server" CssClass="Important" Text="(mail@sample.com)" __designer:wfdid="w134"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label13" runat="server" Text="Note" __designer:wfdid="w135"></asp:Label></TD><TD class="Label" align=center>:</TD><TD style="WIDTH: 257px" class="Label" align=left><asp:TextBox id="divnote" runat="server" CssClass="inpText" Width="250px" __designer:wfdid="w136" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label15" runat="server" Text="Status" __designer:wfdid="w137"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="activeflag" runat="server" CssClass="inpText" Width="85px" __designer:wfdid="w138">
        <asp:ListItem>ACTIVE</asp:ListItem>
        <asp:ListItem>INACTIVE</asp:ListItem>
            </asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label12" runat="server" Text="Total Employee" __designer:wfdid="w139"></asp:Label></TD><TD class="Label" align=center>:</TD><TD style="WIDTH: 257px" class="Label" align=left><asp:Label id="divtotalperson" runat="server" Text="0" __designer:wfdid="w140"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label18" runat="server" Text="JKK Value" __designer:wfdid="w141" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD style="WIDTH: 257px" class="Label" align=left><asp:TextBox id="divjkkvalue" runat="server" CssClass="inpText" Width="50px" __designer:wfdid="w142" Visible="False"></asp:TextBox> <asp:Label id="Label19" runat="server" CssClass="Important" Text="%" __designer:wfdid="w143" Visible="False"></asp:Label> <asp:RangeValidator id="RangeValidator1" runat="server" ForeColor CssClass="Important" __designer:wfdid="w144" Visible="False" ControlToValidate="divjkkvalue" Type="Double" SetFocusOnError="True" MaximumValue="100" MinimumValue="0" ErrorMessage="Must be between 0 - 100"></asp:RangeValidator></TD><TD class="Label" align=left><asp:Label id="Label14" runat="server" Text="Total Base Salary" __designer:wfdid="w145" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="divtotalbasesalary" runat="server" Text="0" __designer:wfdid="w146" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label9" runat="server" Text="Max Employee" __designer:wfdid="w147" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD style="WIDTH: 257px" class="Label" align=left><asp:TextBox id="divmaxperson" runat="server" CssClass="inpText" Width="50px" __designer:wfdid="w148" Visible="False" MaxLength="4">0</asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label11" runat="server" Text="Max Base Salary" __designer:wfdid="w149" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="divmaxbasesalary" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w150" Visible="False" MaxLength="9" AutoPostBack="True">0</asp:TextBox></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD style="WIDTH: 257px" class="Label" align=left><ajaxToolkit:CalendarExtender id="ceDate" runat="server" __designer:wfdid="w151" TargetControlID="divcreationdate" PopupButtonID="imbDate" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeDate" runat="server" __designer:wfdid="w152" TargetControlID="divcreationdate" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999" InputDirection="RightToLeft"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbMaxEmp" runat="server" __designer:wfdid="w154" TargetControlID="divmaxperson" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbMaxSalary" runat="server" __designer:wfdid="w155" TargetControlID="divmaxbasesalary" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbjkk" runat="server" __designer:wfdid="w156" TargetControlID="divjkkvalue" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender> </TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=6><asp:Label id="lblWarning" runat="server" CssClass="Important" __designer:wfdid="w157"></asp:Label></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w158"></asp:Label> By <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w159"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w160"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w161"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w162" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w163" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w164" AlternateText="Delete"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w165" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w166"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Business Unit :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

