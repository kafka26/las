Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_ChartOfAccount
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        If cbHasParent.Checked Then
            If acctgoid_parent.Text = "" Then
                sError &= "- Please select COA PARENT field!<BR>"
            End If
        End If
        If acctgcode.Text = "" Then
            sError &= "- Please fill ACCOUNT NO. field!<BR>"
        Else
            If acctgcode.Text.Length <= 0 Then
                sError &= "- ACCOUNT NO. must be less than 20 Characters!<BR>"
            End If
        End If
        If acctgdesc.Text = "" Then
            sError &= "- Please fill ACCOUNT NAME field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function 'OK

    Private Function IsAccountNoExists() As Boolean
        sSql = "SELECT COUNT(*) FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgcode='" & acctgcodeprefix.Text & Tchar(acctgcode.Text) & "' and acctg_incexc='" & DDLacctg_incexc.SelectedValue & "'"
        If Not Session("oid") Is Nothing And Session("oid") <> "" Then
            sSql &= " AND acctgoid<>" & Session("oid")
        End If
        If ToDouble(cKon.ambilscalar(sSql).ToString) > 0 Then
            showMessage("Account No. have been used by another data. Please fill another Account No.!", 2)
            Return True
        End If
        Return False
    End Function 'OK

    Private Function IsAccountNameExists() As Boolean
        sSql = "SELECT COUNT(*) FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgdesc='" & Tchar(acctgdesc.Text) & "' and acctg_incexc='" & DDLacctg_incexc.SelectedValue & "'"
        If Not Session("oid") Is Nothing And Session("oid") <> "" Then
            sSql &= " AND acctgoid<>" & Session("oid")
        End If
        If ToDouble(cKon.ambilscalar(sSql).ToString) > 0 Then
            showMessage("Account Name have been used by another data. Please fill another Account Name!", 2)
            Return True
        End If
        Return False
    End Function 'OK
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub 'OK

    Private Sub BindMstData(ByVal sFilter As String)
        sSql = "SELECT acctgoid, acctgcode, acctgdesc, acctggrp1, (CASE acctgdbcr WHEN 'D' THEN 'DEBET' ELSE 'CREDIT' END) AS acctgdbcr, activeflag, acctgnote,acctg_incexc FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(sFilter) & "%'"
        If cbCategory.Checked Then
            sSql &= " AND acctggrp1='" & FilterDDLCategory.SelectedValue & "'"
        End If
        If cbStatus.Checked Then
            If FilterDDLStatus.SelectedValue <> "ALL" Then
                sSql &= " AND activeflag='" & FilterDDLStatus.SelectedValue & "'"
            End If
        End If
        If checkPagePermission("~\Master\mstAcctg.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        sSql &= " ORDER BY acctgcode"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_mstacctg")
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
        lblViewInfo.Visible = False
    End Sub 'OK

    Private Sub BindCOAParent()
        sSql = "SELECT acctgoid, acctgcode, acctgdesc, acctggrp2,acctg_incexc FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND acctggrp1='" & acctggrp1.SelectedValue & "' /*AND acctg_incexc='" & DDLacctg_incexc.SelectedValue & "'*/ AND " & FilterDDLListParent.SelectedValue & " LIKE '%" & Tchar(FilterTextListParent.Text) & "%' ORDER BY acctgcode"
        FillGV(gvListParent, sSql, "QL_mstacctgparent")
    End Sub 'OK

    Private Sub FillTextBox()
        Try
            sSql = "SELECT a.cmpcode, a.acctgoid, a.acctgcode, a.acctgdesc, a.acctgdbcr, a.acctggrp1, a.acctggrp2, a.acctggrp3, a.acctgnote, a.activeflag, a.createuser, a.createtime, a.upduser, a.updtime, (CASE acctggrp2 WHEN '0' THEN '' ELSE (SELECT '[' + x.acctgcode + ' - ' + x.acctgdesc + '] ' FROM QL_mstacctg x WHERE x.cmpcode=a.cmpcode AND x.acctgoid=CONVERT(INT, a.acctggrp3)) END) AS acctgparent,acctg_incexc,curroid FROM QL_mstacctg a WHERE a.cmpcode='" & CompnyCode & "' AND a.acctgoid=" & Session("oid")
            xreader = cKon.ambilreader(sSql)
            btnDelete.Visible = False
            While xreader.Read
                acctgoid.Text = xreader("acctgoid").ToString
                acctggrp1.SelectedValue = xreader("acctggrp1").ToString
                acctgoid_parent.Text = xreader("acctggrp3").ToString
                acctggrp2.Text = xreader("acctggrp2").ToString
                If acctggrp2.Text <> "0" Then
                    cbHasParent.Checked = True
                    lblParent.Text = xreader("acctgparent").ToString
                Else
                    cbHasParent.Checked = False
                    lblParent.Text = "-"
                End If
                acctgcode.Text = xreader("acctgcode").ToString
                acctgdesc.Text = xreader("acctgdesc").ToString
                acctgdbcr.SelectedValue = xreader("acctgdbcr").ToString
                acctgnote.Text = xreader("acctgnote").ToString
                activeflag.SelectedValue = xreader("activeflag").ToString
                DDLacctg_incexc.SelectedValue = xreader("acctg_incexc").ToString
                createuser.Text = xreader("createuser").ToString
                curroid.SelectedValue = xreader("curroid").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
            End While
        Catch ex As Exception
            xreader.Close()
            showMessage(ex.Message, 1)
        Finally
            xreader.Close()
            btnDelete.Visible = True
            acctggrp1.Enabled = False : acctggrp1.CssClass = "inpTextDisabled"
            cbHasParent.Enabled = False
            acctgcode.CssClass = "inpTextDisabled" : acctgcode.Enabled = False
            lblMaxCar.Visible = False
        End Try
    End Sub 'OK

    Private Sub PrintReport(Optional ByVal sType As String = "")
        Try
            If sType = "" Then
                report.Load(Server.MapPath(folderReport & "rptAcctg.rpt"))
            Else
                report.Load(Server.MapPath(folderReport & "rptAcctgXls.rpt"))
            End If
            Dim sWhere As String = " WHERE a.cmpcode='" & CompnyCode & "' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
            If cbCategory.Checked Then
                sWhere &= " AND acctggrp1='" & FilterDDLCategory.SelectedValue & "'"
            End If
            If cbStatus.Checked Then
                If FilterDDLStatus.SelectedValue <> "ALL" Then
                    sWhere &= " AND a.activeflag='" & FilterDDLStatus.SelectedValue & "'"
                End If
            End If
            If checkPagePermission("~\Master\mstAcctg.aspx", Session("SpecialAccess")) = False Then
                sWhere &= " AND a.createuser='" & Session("UserID") & "'"
            End If
            sWhere &= " ORDER BY acctgcode"
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            If sType = "" Then
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "ChartOfAccountReport")
            Else
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "ChartOfAccountReport")
            End If
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub 'OK
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Master\mstAcctg.aspx")
        End If
        If checkPagePermission("~\Master\mstAcctg.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Chart Of Account"
        Session("oid") = Request.QueryString("oid")
        DDLacctg_incexc.Visible = False
        Label4.Visible = False
        If Session("oid") = "" Or Session("oid") = Nothing Then
            I_U.Text = "New Data"
        Else
            I_U.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        If Not Page.IsPostBack Then
            FillDDLWithAdditionalText(curroid, "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'", "NONE", "0")
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox()
                TabContainer1.ActiveTabIndex = 1
            Else
                acctgoid.Text = GenerateID("QL_MSTACCTG", CompnyCode)
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
                acctggrp1_SelectedIndexChanged(Nothing, Nothing)
            End If
        End If
    End Sub 'OK

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub 'OK

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindMstData(FilterText.Text)
    End Sub 'OK

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbCategory.Checked = False
        FilterDDLCategory.SelectedIndex = -1
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        BindMstData(FilterText.Text)
    End Sub 'OK

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
    End Sub 'OK

    Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvMst.DataSource = Session("TblMst")
            gvMst.DataBind()
        End If
    End Sub

    Protected Sub acctggrp1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles acctggrp1.SelectedIndexChanged
        cbHasParent.Checked = False
        cbHasParent_CheckedChanged(Nothing, Nothing)
    End Sub 'OK

    Protected Sub cbHasParent_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbHasParent.CheckedChanged
        If cbHasParent.Checked Then
            btnChoose.Visible = True : btnChoose.ImageUrl = "~/Images/choose.png" : btnChoose.AlternateText = "Choose"
            lblParent.Text = ""
            acctgcodeprefix.Text = "" : acctgcodeprefix.Visible = True
            lblSept.Visible = True
        Else
            btnChoose.Visible = False : btnChoose.ImageUrl = "~/Images/choose.png" : btnChoose.AlternateText = "Choose"
            lblParent.Text = "-"
            acctgcodeprefix.Text = "" : acctgcodeprefix.Visible = False
            lblSept.Visible = False
        End If
    End Sub 'OK

    Protected Sub btnChoose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnChoose.Click
        BindCOAParent()
        gvListParent.SelectedIndex = -1
        FilterTextListParent.Text = ""
        FilterDDLListParent.SelectedIndex = -1
        cProc.SetModalPopUpExtender(btnHideListParent, pnlListParent, mpeListParent, True)
    End Sub 'OK

    Protected Sub btnFindListParent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListParent.Click
        BindCOAParent()
        mpeListParent.Show()
    End Sub 'OK

    Protected Sub btnAllListParent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListParent.Click
        FilterDDLListParent.SelectedIndex = -1
        FilterTextListParent.Text = ""
        BindCOAParent()
        mpeListParent.Show()
    End Sub 'OK

    Protected Sub gvListParent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListParent.SelectedIndexChanged
        acctgoid_parent.Text = gvListParent.SelectedDataKey.Item("acctgoid").ToString
        acctggrp2.Text = CInt(gvListParent.SelectedDataKey.Item("acctggrp2").ToString) + 1
        lblParent.Text = "[" & gvListParent.SelectedDataKey.Item("acctgcode").ToString & " - " & gvListParent.SelectedDataKey.Item("acctgdesc").ToString & "] "
        acctgcodeprefix.Text = gvListParent.SelectedDataKey.Item("acctgcode").ToString
        acctgcode.MaxLength -= acctgcodeprefix.Text.Length
        lblMaxCar.Text = "Maks. " & acctgcode.MaxLength & " Chars"
        btnChoose.ImageUrl = "~/Images/change.png" : btnChoose.AlternateText = "Change"
        cProc.SetModalPopUpExtender(btnHideListParent, pnlListParent, mpeListParent, False)
    End Sub 'OK

    Protected Sub lkbCloseListParent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListParent.Click
        cProc.SetModalPopUpExtender(btnHideListParent, pnlListParent, mpeListParent, False)
    End Sub 'OK

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            If IsAccountNoExists() Then
                Exit Sub
            End If
            If IsAccountNameExists() Then
                Exit Sub
            End If
            If Session("oid") = Nothing Or Session("oid") = "" Then
                acctgoid.Text = GenerateID("QL_MSTACCTG", CompnyCode)
            End If
            Dim sRes2 As String = ""
            Dim sRes3 As String = "NERACA"
            If acctggrp1.SelectedValue.ToUpper = "PENDAPATAN USAHA" Or acctggrp1.SelectedValue.ToUpper = "HARGA POKOK" Or acctggrp1.SelectedValue.ToUpper = "BIAYA USAHA" Or acctggrp1.SelectedValue.ToUpper = "PENDAPATAN DAN BIAYA LAIN-LAIN" Then
                sRes3 = "LABARUGI"
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_mstacctg (cmpcode, acctgoid, acctgcode, acctgdesc, acctgdbcr, acctggrp1, acctggrp2, acctggrp3, acctgnote, activeflag, createuser, createtime, upduser, updtime, acctg_incexc,curroid, acctgres3) VALUES ('" & CompnyCode & "', " & acctgoid.Text & ", '" & acctgcodeprefix.Text & Tchar(acctgcode.Text) & "', '" & Tchar(acctgdesc.Text) & "', '" & acctgdbcr.SelectedValue & "', '" & acctggrp1.SelectedValue & "', '" & IIf(cbHasParent.Checked, acctggrp2.Text, 0) & "', '" & acctgoid_parent.Text & "', '" & Tchar(acctgnote.Text) & "', '" & activeflag.SelectedValue & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & DDLacctg_incexc.SelectedValue & "'," & curroid.SelectedValue & ", '" & sRes3 & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & acctgoid.Text & " WHERE tablename LIKE 'QL_MSTACCTG' AND cmpcode LIKE '%" & CompnyCode & "%'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_mstacctg SET acctgdesc='" & Tchar(acctgdesc.Text) & "', acctgdbcr='" & acctgdbcr.SelectedValue & "', acctgnote='" & Tchar(acctgnote.Text) & "', activeflag='" & activeflag.SelectedValue & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, acctg_incexc='" & DDLacctg_incexc.SelectedValue & "', curroid=" & curroid.SelectedValue & ", acctgres3='" & sRes3 & "' WHERE cmpcode='" & CompnyCode & "' AND acctgoid=" & acctgoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                conn.Close()
            Catch ex As Exception
                objTrans.Rollback()
                conn.Close()
                showMessage(ex.Message, 1)
                Exit Sub
            End Try
            Response.Redirect("~\Master\mstAcctg.aspx?awal=true")
        End If
    End Sub 'OK

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Master\mstAcctg.aspx?awal=true")
    End Sub 'OK

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If acctgoid.Text = "" Then
            showMessage("Please select Chart Of Account data first!", 1)
            Exit Sub
        End If
        sSql = "SELECT ta.name,col.name colum FROM sys.tables ta INNER JOIN sys.columns col ON col.object_id=ta.object_id WHERE col.name IN ('acctgoid') AND ta.name NOT IN ('QL_mstacctg')"
        Dim objTblUsage As DataTable = cKon.ambiltabel(sSql, "QL_tables")
        For C1 As Integer = 0 To objTblUsage.Rows.Count - 1
            If CheckDataExists("SELECT COUNT(*) FROM " & objTblUsage.Rows(C1).Item("name").ToString & " WHERE " & objTblUsage.Rows(C1).Item("colum").ToString & "=" & acctgoid.Text) Then
                showMessage("This data can't be Deleted because it is being used by another data!", "WARNING")
                Exit Sub
            End If
        Next

        sSql = "SELECT COUNT(*) FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctggrp3 IN (" & acctgoid.Text & ")"
        If CheckDataExists(sSql) = True Then
            showMessage("This data can't be deleted because it is being used by another data!", 2)
            Exit Sub
        End If
        If DeleteData("QL_mstacctg", "acctgoid", acctgoid.Text, CompnyCode) = True Then
            Response.Redirect("~\Master\mstAcctg.aspx?awal=true")
        End If
    End Sub 'OK

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        PrintReport()
    End Sub 'OK

    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExcel.Click
        PrintReport("Excel")
    End Sub
#End Region

End Class
