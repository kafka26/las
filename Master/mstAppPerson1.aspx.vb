Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class Master_ApprovalPerson
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Functions"
    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        If ddlapprovaluser.SelectedValue = "" Then
            sError &= "- Please select USER ID field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputExists(ByVal sUser As String, ByVal sTblName As String, ByVal sType As String) As Boolean
        sSql = "SELECT COUNT(-1) FROM QL_approvalperson WHERE approvaluser='" & sUser & "' AND tablename='" & sTblName & "'"
        If Session("oid") <> Nothing And Session("oid") <> "" Then
            sSql &= " AND apppersonoid<>" & Session("oid")
        End If
        If ToDouble(GetStrData(sSql).ToString) > 0 Then
            showMessage("User has been assigned in this table name before. Please check data in list of approval user!", 2)
            Return True
        End If
        Return False
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindMstData(ByVal sFilter As String)
        sSql = "SELECT app.apppersonoid, app.approvaluser, p.profname, app.apppersonnote, app.activeflag FROM QL_approvalperson app INNER JOIN QL_mstprof p ON app.approvaluser=p.profoid AND p.activeflag='ACTIVE' WHERE " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(sFilter) & "%'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND (app.cmpcode='" & Session("CompnyCode") & "' OR app.cmpcode='" & CompnyCode & "')"
        End If
        If cbStatus.Checked Then
            If ddlFilterStatus.SelectedValue <> "All" Then
                sSql &= " AND app.activeflag='" & ddlFilterStatus.SelectedValue & "'"
            End If
        End If
        If checkPagePermission("~\Master\mstAppPerson.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND app.createuser='" & Session("UserID") & "'"
        End If
        sSql &= " ORDER BY apppersonnote, approvaluser"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_mstappperson")
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub InitAllDDL()
        ' Fill DDL User ID
        sSql = "SELECT profoid, profoid FROM QL_mstprof WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            'sSql &= " AND (cmpcode='" & Session("CompnyCode") & "' OR cmpcode='" & CompnyCode & "')"
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        sSql &= " ORDER BY profoid"
        FillDDL(ddlapprovaluser, sSql)
        If ddlapprovaluser.Items.Count = 0 Then
            lblWarning.Text &= "- Please fill <STRONG>User ID</STRONG> data first in <STRONG>Profile</STRONG> Form!<BR>"
        End If
    End Sub

    Private Sub FillTextBox()
        Try
            sSql = "SELECT apppersonoid, tablename, apppersonlevel, apppersontype, activeflag, approvaluser, createuser, createtime, upduser, updtime FROM QL_approvalperson WHERE apppersonoid=" & Session("oid")
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            btnDelete.Visible = False
            While xreader.Read
                apppersonoid.Text = xreader("apppersonoid").ToString
                ddlapprovaluser.SelectedValue = xreader("approvaluser").ToString
                lastuser.Text = ddlapprovaluser.SelectedValue
                ddltablename.SelectedValue = xreader("tablename").ToString
                lasttable.Text = ddltablename.SelectedValue
                apppersonlevel.SelectedValue = xreader("apppersonlevel").ToString
                ddlapppersontype.SelectedValue = xreader("apppersontype").ToString
                ddlapppersonstatus.SelectedValue = xreader("activeflag").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
        Finally
            btnDelete.Visible = True
        End Try
        'sSql = "SELECT COUNT(*) FROM QL_approval WHERE approvaluser='" & ddlapprovaluser.SelectedValue & "' AND tablename='" & ddltablename.SelectedValue & "' AND event='In Approval'"
        'If ToDouble(GetStrData(sSql).ToString) > 0 Then
        '    lblWarn.Visible = True
        '    btnSave.Visible = False
        '    btnDelete.Visible = False
        '    ddlapprovaluser.Enabled = False
        '    ddltablename.Enabled = False
        '    apppersonlevel.Enabled = False
        '    ddlapppersontype.Enabled = False
        '    ddlapppersonstatus.Enabled = False
        '    I_U.Visible = False
        'End If
        ddlapprovaluser.Enabled = False
        ddltablename.Enabled = False
        apppersonlevel.Enabled = False
        ddlapppersontype.Enabled = False
        ddlapppersonstatus.Enabled = False
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Master\mstAppPerson.aspx")
        End If
        If checkPagePermission("~\Master\mstAppPerson.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Approval User"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            I_U.Text = "New Data"
        Else
            I_U.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            InitAllDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox()
                TabContainer1.ActiveTabIndex = 1
            Else
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbStatus.Checked = False
        ddlFilterStatus.SelectedIndex = -1
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            If Not IsInputExists(ddlapprovaluser.SelectedValue, ddltablename.SelectedValue, ddlapppersontype.SelectedValue) Then
                Dim sCode As String = ""
                sSql = "SELECT cmpcode FROM QL_mstprof WHERE profoid='" & ddlapprovaluser.SelectedValue & "'"
                If Not IsDBNull(GetStrData(sSql)) Then
                    sCode = CStr(GetStrData(sSql))
                End If
                ' Check Data Approval
                sSql = "SELECT DISTINCT (LEFT(requestcode, LEN(requestcode) - LEN(approvaloid))) AS requestcode, requestuser, requestdate, oid FROM QL_approval WHERE statusrequest='New' AND tablename='" & ddltablename.SelectedValue & "' AND event='In Approval' AND oid NOT IN (SELECT oid FROM QL_approval WHERE statusrequest='New' AND tablename='QL_prgenmst' AND event='In Approval' AND approvaluser='" & ddlapprovaluser.SelectedValue & "')"
                If sCode <> CompnyCode Then
                    sSql &= " AND cmpcode='" & sCode & "'"
                End If
                Dim dtApp As DataTable = cKon.ambiltabel(sSql, "QL_approval")
                Dim iAppOid As Integer = GenerateID("QL_APPROVAL", CompnyCode)
                Dim objTrans As SqlClient.SqlTransaction
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If
                objTrans = conn.BeginTransaction()
                xCmd.Transaction = objTrans
                Try
                    Dim iOid As Int64 = GenerateID("QL_approvalperson", CompnyCode)
                    If Session("oid") Is Nothing Or Session("oid") = "" Then
                        sSql = "INSERT INTO QL_approvalperson (cmpcode, apppersonoid, tablename, apppersonlevel, apppersontype, approvaluser, activeflag, apppersonnote, createuser, createtime, upduser, updtime) VALUES ('" & sCode & "', " & iOid & ", '" & ddltablename.SelectedValue & "', " & apppersonlevel.SelectedValue & ", '" & ddlapppersontype.SelectedValue & "', '" & ddlapprovaluser.SelectedValue & "', '" & ddlapppersonstatus.SelectedValue & "', '" & ddltablename.SelectedItem.Text & "', '" & Session("UserID") & "', current_timestamp, '" & Session("UserID") & "', current_timestamp)"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        sSql = "UPDATE QL_mstoid SET lastoid=" & iOid & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_approvalperson'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Else
                        sSql = "UPDATE QL_approvalperson SET cmpcode='" & sCode & "', tablename='" & ddltablename.SelectedValue & "', apppersonlevel=" & apppersonlevel.SelectedValue & ", apppersontype='" & ddlapppersontype.SelectedValue & "', approvaluser='" & ddlapprovaluser.SelectedValue & "', activeflag='" & ddlapppersonstatus.SelectedValue & "', apppersonnote='" & ddltablename.SelectedItem.Text & "', upduser='" & Session("UserID") & "', updtime=current_timestamp WHERE apppersonoid=" & Session("oid")
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                    If dtApp.Rows.Count > 0 Then
                        For C1 As Integer = 0 To dtApp.Rows.Count - 1
                            sSql = "INSERT INTO QL_approval (cmpcode, approvaloid, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvalnote) VALUES ('" & sCode & "', " & iAppOid + C1 & ", '" & dtApp.Rows(C1)("requestcode").ToString & (iAppOid + C1).ToString & "', '" & dtApp.Rows(C1)("requestuser").ToString & "', '" & dtApp.Rows(C1)("requestdate").ToString & "', 'New', '" & ddltablename.SelectedValue & "', " & dtApp.Rows(C1)("oid").ToString & ", 'In Approval', '0', '" & ddlapprovaluser.SelectedValue & "', '1/1/1900', '')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        Next
                        sSql = "UPDATE QL_mstOid SET lastoid=" & iAppOid + dtApp.Rows.Count - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_APPROVAL'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                    objTrans.Commit()
                    conn.Close()
                Catch ex As Exception
                    objTrans.Rollback()
                    conn.Close()
                    showMessage(ex.Message, 1)
                    Exit Sub
                End Try
                Response.Redirect("~\Master\mstAppPerson.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Master\mstAppPerson.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If apppersonoid.Text = "" Then
            showMessage("Please select approval person data first!", 2)
            Exit Sub
        End If
        sSql = "SELECT COUNT(*) FROM QL_approval WHERE approvaluser='" & lastuser.Text & "' AND tablename='" & lasttable.Text & "' AND statusrequest='New' AND event='In Approval'"
        If ToDouble(GetStrData(sSql).ToString) > 0 Then
            sSql = "SELECT COUNT(*) FROM QL_approval WHERE approvaluser<>'" & lastuser.Text & "' AND tablename='" & lasttable.Text & "' AND statusrequest='New' AND event='In Approval'"
            If ToDouble(GetStrData(sSql).ToString) <= 0 Then
                showMessage("This data can't be deleted because it is being used by another data!", 2)
                Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE QL_approval WHERE approvaluser='" & lastuser.Text & "' AND tablename='" & lasttable.Text & "' AND statusrequest='New' AND event='In Approval'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE QL_approvalperson WHERE apppersonoid=" & apppersonoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Master\mstAppPerson.aspx?awal=true")
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
    End Sub

    Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvMst.DataSource = Session("TblMst")
            gvMst.DataBind()
        End If
    End Sub

#End Region

End Class
