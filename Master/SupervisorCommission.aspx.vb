Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_DirectLaborCost
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If m_person_id.SelectedValue = "" Then
            sError &= "- Please select SALES field!<br />"
        End If
        If ToDouble(m_spv_comm_pct.Text) <= 0 Then
            sError &= "- Komisi % must more than 0!<br />"
        Else
            If ToDouble(m_spv_comm_pct.Text) > 100 Then
                sError &= "- Komisi % must less or equal than 100% !<br />"
            End If
        End If
        If ToInteger(m_spv_comm_pay_from.Text) < 0 Then
            sError &= "- TOP From must be more than -1!<br />"
        End If
        If ToInteger(m_spv_comm_pay_to.Text) < 0 Then
            sError &= "- TOP To must be more than -1!<br />"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        If m_person_name.Text = "" Then
            sError &= "- Please select SUPERVISOR field!<br />"
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim objTbl As DataTable = Session("TblDtl")
            If objTbl.Rows.Count = 0 Then
                sError &= "- Please fill Detail Data!<br />"
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitAllDDL()
        sSql = "SELECT personoid, personname FROM QL_mstperson WHERE activeflag='ACTIVE' AND leveloid=1 AND personoid NOT IN (SELECT m_person_id FROM QL_m_spv_comm_d WHERE m_spv_comm_id<>" & ToInteger(m_spv_comm_id.Text) & ") ORDER BY personname"
        FillDDL(m_person_id, sSql)
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT m_spv_comm_id, m_spv_comm_code, personname m_person_name, salescode m_person_code, m_spv_comm_note, m_active_flag, 'False' Checkvalue FROM QL_m_spv_comm h INNER JOIN QL_mstperson p ON p.personoid=h.m_person_id WHERE 1=1"
        sSql &= sSqlPlus & " ORDER BY m_spv_comm_code"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_m_spv_comm")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "dlcoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub BindSupervisorData()
        sSql = "SELECT personoid m_person_id, salescode m_person_code, personname m_person_name FROM QL_mstperson WHERE activeflag='ACTIVE' AND leveloid=1 AND personoid NOT IN (SELECT m_person_id FROM QL_m_spv_comm WHERE m_active_flag='ACTIVE') AND " & FilterDDLListBOM.SelectedValue & " LIKE '%" & Tchar(FilterTextListBOM.Text) & "%' ORDER BY m_person_name"
        FillGV(gvListBOM, sSql, "QL_mstperson")
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("QL_m_spv_comm_d")
        dtlTable.Columns.Add("m_spv_comm_seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("m_person_id", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("m_person_code", Type.GetType("System.String"))
        dtlTable.Columns.Add("m_person_name", Type.GetType("System.String"))
        dtlTable.Columns.Add("m_spv_comm_pct", Type.GetType("System.Double"))
        dtlTable.Columns.Add("m_spv_comm_pay_from", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("m_spv_comm_pay_to", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("m_spv_comm_d_note", Type.GetType("System.String"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub GenerateNo()
        Dim sNo As String = "SC-" & Format(CDate(createtime.Text), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(m_spv_comm_code, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_m_spv_comm WHERE m_spv_comm_code LIKE '%" & sNo & "%'"
        If GetStrData(sSql) = "" Then
            m_spv_comm_code.Text = GenNumberString(sNo, "", 1, DefaultFormatCounter)
        Else
            m_spv_comm_code.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        End If
    End Sub

    Private Sub ClearDetail()
        m_spv_comm_seq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable = Session("TblDtl")
            m_spv_comm_seq.Text = objTable.Rows.Count + 1
            If objTable.Rows.Count > 0 Then
                EnableHeader(False)
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        i_u2.Text = "New Detail"
        m_person_id.SelectedIndex = -1
        m_spv_comm_pct.Text = ""
        m_spv_comm_d_note.Text = ""
        gvDtl.SelectedIndex = -1
        m_spv_comm_pay_from.Text = ""
        m_spv_comm_pay_to.Text = ""
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        btnSearchBOM.Visible = bVal
        btnClearBOM.Visible = bVal
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        sSql = "SELECT h.*, personname m_person_name FROM QL_m_spv_comm h INNER JOIN QL_mstperson p ON p.personoid=h.m_person_id WHERE h.m_spv_comm_id=" & sOid
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        While xreader.Read
            m_spv_comm_id.Text = xreader("m_spv_comm_id").ToString
            m_spv_comm_code.Text = xreader("m_spv_comm_code").ToString
            m_person_name.Text = xreader("m_person_name").ToString
            m_person_name.ToolTip = xreader("m_person_id").ToString
            m_spv_comm_note.ToolTip = xreader("m_spv_comm_note").ToString
            m_active_flag.SelectedValue = xreader("m_active_flag").ToString
            createuser.Text = Trim(xreader("create_by").ToString)
            createtime.Text = Trim(xreader("create_at").ToString)
            upduser.Text = Trim(xreader("last_edited_by").ToString)
            updtime.Text = Trim(xreader("last_edited_at").ToString)
        End While
        xreader.Close()
        conn.Close()

        sSql = "SELECT m_spv_comm_seq, m_spv_comm_id, salescode m_person_code, personname m_person_name, m_person_id, m_spv_comm_d_note, m_spv_comm_pct, m_spv_comm_pay_from, m_spv_comm_pay_to FROM QL_m_spv_comm_d d INNER JOIN QL_mstperson p ON p.personoid=d.m_person_id WHERE d.m_spv_comm_id=" & sOid & " ORDER BY d.m_spv_comm_seq"
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstdlcdtl")
        Session("TblDtl") = dtTbl
        gvDtl.DataSource = dtTbl
        gvDtl.DataBind()
        EnableHeader(False)
    End Sub

    Private Sub PrintReport()
        Dim rptName As String = ""
        Dim Dtl As String = ""
        Dim Join As String = ""
        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("dlcoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            report.Load(Server.MapPath(folderReport & "rptDLC_DtlFGPdf.rpt"))

            sSql = "SELECT (SELECT div.divname FROM QL_mstdivision div WHERE dlcm.cmpcode = div.cmpcode) AS [Business Unit], dlcm.cmpcode [CMPCODE], dlcm.dlcoid [Oid], dlcm.createtime [DLC Create Date], dlcm.dlcdesc [DLC Desc], dlcm.dlcamount [DLC Amount], dlcoverheadamt [DLC Overhead Amt], dlcm.dlctotalpct [Total Cost %], dlcm.dlcnote [Header Note],CONVERT(VARCHAR(10),dlcm.dlcres1,101) AS [Last BOM Update], 0.0 [Max DLC], 0.0 [Max OHD], itemcode AS [FG Code], itemShortDescription AS [FG Short Desc.], itemLongDescription AS [FG Long Desc.], 0.0 AS [FG Length], 0.0 AS [FG Width], 0.0 AS [FG Height], 0.0 AS [FG Diameter], 0.0 AS [FG Volume], 0.0 AS [FG Packing Length], 0.0 AS [FG Packing Width], 0.0 AS [FG Packing Height], 0.0 AS [FG Packing Diameter], 0.0 AS [FG Packing Volume], 0.0 AS [CBF], g.gendesc [Unit], dlcm.createuser [Create User], dlcm.upduser [Update User], dlcm.updtime [Update Time], dlcm.activeflag [Status], GETDATE() [Precosting Date], GETDATE() [Precosting Create Date], '' [Precosting Create User], GETDATE() [Precosting Update Time], '' [Precosting Update User] , bom.bomdate [BOM Date], bom.createtime [BOM Create Date], bom.createuser [BOM Create User], bom.updtime [BOM Update Time], bom.upduser [BOM Update User] , dlcd.m_spv_comm_seq [DLC Seq] , dlcd.dlcpct [Cost %], dlcd.dlcdtlamount [DL Cost Amt.], dlcd.dlcohdamount [OHD Cost Amt.], dlcd.dlcdtlnote [Detail Note], dlcd.deptoid [Dept Oid From], (SELECT deptname FROM QL_mstdept df WHERE df.cmpcode=dlcd.cmpcode AND df.deptoid=dlcd.deptoid)  [Dept. From], bom.bomoid [BOM Oid], bomd1.bomdtl1todeptoid [Dept Oid To], (CASE bomd1.bomdtl1reftype WHEN 'FG' THEN 'END' ELSE (SELECT deptname FROM QL_mstdept dt WHERE dt.cmpcode=bomd1.cmpcode AND dt.deptoid=bomd1.bomdtl1todeptoid) END) [Dept. To] FROM QL_mstdlc dlcm INNER JOIN QL_mstbom bom ON bom.cmpcode=dlcm.cmpcode AND bom.bomoid=dlcm.bomoid INNER JOIN QL_mstitem i ON bom.itemoid=i.itemoid INNER JOIN QL_mstgen g ON g.genoid=i.itemUnit1 INNER JOIN QL_mstdlcdtl dlcd ON dlcm.cmpcode=dlcd.cmpcode AND dlcm.dlcoid=dlcd.dlcoid INNER JOIN QL_mstbomdtl1 bomd1 ON dlcm.cmpcode=bomd1.cmpcode AND bomd1.bomoid=dlcm.bomoid AND dlcd.deptoid=bomd1.bomdtl1deptoid"

            If sOid = "" Then
                If FilterDDL.SelectedValue = "dlc.dlcoid" Then
                    sSql &= " WHERE dlcm.dlcoid LIKE '%" & Tchar(FilterText.Text) & "%'"
                Else
                    sSql &= " WHERE " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                End If

                If Session("CompnyCode") <> CompnyCode Then
                    sSql &= " AND dlcm.cmpcode='" & Session("CompnyCode") & "'"
                Else
                    sSql &= " AND dlcm.cmpcode LIKE '%'"
                End If
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sSql &= " AND dlcm.dlcdate>='" & FilterPeriod1.Text & " 00:00:00' AND dlcm.dlcdate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sSql &= " AND dlcm.activeflag='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Master\SupervisorCommission.aspx", Session("SpecialAccess")) = False Then
                    sSql &= " AND dlcm.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sSql &= " WHERE dlcm.dlcoid IN (" & sOid & ")"
            End If
            Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstdlc")
            Dim dvTbl As DataView = dtTbl.DefaultView
            report.SetDataSource(dvTbl.ToTable)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            cProc.SetDBLogonForReport(report)
            report.PrintOptions.PaperSize = PaperSize.PaperFolio
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "DLCPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
        Response.Redirect("~\Master\SupervisorCommission.aspx?awal=true")
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Master\SupervisorCommission.aspx")
        End If
        'If checkPagePermission("~\Master\SupervisorCommission.aspx", Session("Role")) = False Then
        '    Response.Redirect("~\Other\NotAuthorize.aspx")
        'End If
        Page.Title = CompnyName & " - Supervisor Commission"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                GenerateNo()
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
            InitAllDDL()
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvDtl.DataSource = dt
            gvDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND h.created_at>='" & FilterPeriod1.Text & " 00:00:00' AND h.created_at<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND h.m_actieve_flag='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Master\SupervisorCommission.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND h.created_by='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = ""
        FilterDDL.SelectedIndex = 0
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Master\SupervisorCommission.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND h.created_by='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnPrintHdr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrintHdr.Click
        UpdateCheckedValue()
        PrintReport()
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub btnSearchBOM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchBOM.Click
        BindSupervisorData()
        cProc.SetModalPopUpExtender(btnHideListBOM, pnlListBOM, mpeListBOM, True)
    End Sub

    Protected Sub btnClearBOM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearBOM.Click
        m_person_name.Text = "" : m_person_name.ToolTip = ""
    End Sub

    Protected Sub btnFindListBOM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListBOM.Click
        BindSupervisorData()
        mpeListBOM.Show()
    End Sub

    Protected Sub btnAllListBOM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListBOM.Click
        FilterDDLListBOM.SelectedIndex = -1
        FilterTextListBOM.Text = ""
        BindSupervisorData()
        mpeListBOM.Show()
    End Sub

    Protected Sub gvListBOM_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListBOM.PageIndexChanging
        gvListBOM.PageIndex = e.NewPageIndex
        BindSupervisorData()
        mpeListBOM.Show()
    End Sub

    Protected Sub gvListBOM_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListBOM.SelectedIndexChanged
        If m_person_name.ToolTip = gvListBOM.SelectedDataKey.Item("m_person_id").ToString Then
            btnClearBOM_Click(Nothing, Nothing)
        End If
        m_person_name.ToolTip = gvListBOM.SelectedDataKey.Item("m_person_id").ToString
        m_person_name.Text = gvListBOM.SelectedDataKey.Item("m_person_name").ToString
        cProc.SetModalPopUpExtender(btnHideListBOM, pnlListBOM, mpeListBOM, False)
    End Sub

    Protected Sub lkbCloseListBOM_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListBOM.Click
        cProc.SetModalPopUpExtender(btnHideListBOM, pnlListBOM, mpeListBOM, False)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            If Session("TblDtl") Is Nothing Then
                CreateTblDetail()
            End If
            Dim objTable As DataTable
            objTable = Session("TblDtl")
            Dim dv As DataView = objTable.DefaultView
            If i_u2.Text = "New Detail" Then
                dv.RowFilter = "m_spv_comm_pay_from=" & ToInteger(m_spv_comm_pay_from.Text) & " AND m_spv_comm_pay_to=" & ToInteger(m_spv_comm_pay_to.Text) & " AND m_person_id=" & m_person_id.SelectedValue
            Else
                dv.RowFilter = "m_spv_comm_seq <>" & m_spv_comm_seq.Text & " AND m_spv_comm_pay_from=" & ToInteger(m_spv_comm_pay_from.Text) & " AND m_spv_comm_pay_to=" & ToInteger(m_spv_comm_pay_to.Text) & " AND m_person_id=" & m_person_id.SelectedValue
            End If
            If dv.Count > 0 Then
                showMessage("This Data has been added before!", 2)
                dv.RowFilter = ""
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                objRow("m_spv_comm_seq") = objTable.Rows.Count + 1
            Else
                objRow = objTable.Rows(m_spv_comm_seq.Text - 1)
                objRow.BeginEdit()
            End If
            objRow("m_person_id") = m_person_id.SelectedValue
            objRow("m_person_code") = GetStrData("SELECT salescode FROM QL_mstperson WHERE personoid=" & ToInteger(m_person_id.SelectedValue))
            objRow("m_person_name") = m_person_id.SelectedItem.Text
            objRow("m_spv_comm_pct") = ToDouble(m_spv_comm_pct.Text)
            objRow("m_spv_comm_pay_from") = ToInteger(m_spv_comm_pay_from.Text)
            objRow("m_spv_comm_pay_to") = ToInteger(m_spv_comm_pay_to.Text)
            objRow("m_spv_comm_d_note") = m_spv_comm_d_note.Text
            If i_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            Session("TblDtl") = objTable
            gvDtl.DataSource = objTable
            gvDtl.DataBind()
            ClearDetail()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 2)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 2)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 2)
        End If
    End Sub

    Protected Sub gvDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("m_spv_comm_seq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvDtl.DataSource = objTable
        gvDtl.DataBind()
        ClearDetail()
    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDtl.SelectedIndexChanged
        Try
            m_spv_comm_seq.Text = gvDtl.SelectedDataKey.Item("m_spv_comm_seq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "m_spv_comm_seq=" & m_spv_comm_seq.Text
                m_person_id.SelectedValue = dv.Item(0).Item("m_person_id").ToString
                m_spv_comm_pct.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("m_spv_comm_pct").ToString), 2)
                m_spv_comm_d_note.Text = dv.Item(0).Item("m_spv_comm_d_note").ToString
                m_spv_comm_pay_from.Text = dv.Item(0).Item("m_spv_comm_pay_from").ToString
                m_spv_comm_pay_to.Text = dv.Item(0).Item("m_spv_comm_pay_to").ToString
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_m_spv_comm WHERE m_spv_comm_code='" & m_spv_comm_code.Text & "'"
                If CheckDataExists(sSql) Then
                    GenerateNo()
                End If
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_m_spv_comm SELECT '" & m_spv_comm_code.Text & "', " & m_person_name.ToolTip & ", '" & Tchar(m_spv_comm_note.Text) & "', '" & m_active_flag.SelectedValue & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP; SELECT SCOPE_IDENTITY()"
                    xCmd.CommandText = sSql
                    m_spv_comm_id.Text = xCmd.ExecuteScalar()
                Else
                    sSql = "UPDATE QL_m_spv_comm SET m_person_id=" & m_person_name.ToolTip & ", m_spv_comm_note='" & Tchar(m_spv_comm_note.Text) & "', m_active_flag='" & m_active_flag.SelectedValue & "', last_edited_by='" & Session("UserID") & "', last_edited_at=CURRENT_TIMESTAMP WHERE m_spv_comm_id=" & m_spv_comm_id.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_m_spv_comm_d WHERE m_spv_comm_id=" & m_spv_comm_id.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_m_spv_comm_d SELECT " & m_spv_comm_id.Text & ", " & C1 + 1 & ", " & objTable.Rows(C1)("m_person_id") & ", " & ToDouble(objTable.Rows(C1)("m_spv_comm_pct")) & ", " & ToInteger(objTable.Rows(C1)("m_spv_comm_pay_from")) & ", " & ToInteger(objTable.Rows(C1)("m_spv_comm_pay_to")) & ", '" & Tchar(objTable.Rows(C1)("m_spv_comm_d_note").ToString) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                End If
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 2)
                conn.Close()
                Exit Sub
            End Try
            Response.Redirect("~\Master\SupervisorCommission.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Master\SupervisorCommission.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If m_spv_comm_id.Text.Trim = "" Then
            showMessage("Please select Supervisor Commission data first!", 2)
            Exit Sub
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE FROM QL_m_spv_comm_d WHERE m_spv_comm_id=" & m_spv_comm_id.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_m_spv_comm WHERE m_spv_comm_id=" & m_spv_comm_id.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Master\SupervisorCommission.aspx?awal=true")
    End Sub
#End Region

End Class