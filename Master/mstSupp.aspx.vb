Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_Supplier
    Inherits System.Web.UI.Page

#Region "Variables"
  Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
  Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
  Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
  Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
  Dim xCmd As New SqlCommand("", conn)
  Dim xreader As SqlDataReader
  Dim sSql As String = ""
  Dim cKon As New Koneksi
  Dim cProc As New ClassProcedure
  Dim report As New ReportDocument
  Public folderReport As String = "~/Report/"
    Private oRegex As Regex : Dim oMatches As MatchCollection
    Dim isRegenOid As Boolean = False
#End Region

#Region "Functions"
    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        If SuppCode.Text.Trim = "" Then
            sError &= "- Please fill CODE field!<BR>"
        End If
        If SuppName.Text.Trim = "" Then
            sError &= "- Please fill NAME field!<BR>"
        End If
        If SuppPhone1.Text.Trim = "" Then
            sError &= "- Please fill Phone 1 field!<BR>"
        End If
        If SuppAddr.Text.Trim = "" Then
            sError &= "- Please fill ADDRESS field!<BR>"
        End If
        If SuppTaxable.SelectedValue = 1 And SuppNPWP.Text.Contains("_") Then
            sError &= "- Please fill NPWP field!<BR>"
        End If
        If SuppTaxable.SelectedValue = 1 Then
            If suppname2.Text = "" Then
                sError &= "- Please fill Name (Faktur)!<BR>"
            End If
        End If
        If SuppEmail.Text <> "" Then
            oMatches = Regex.Matches(SuppEmail.Text, "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")
            If oMatches.Count <= 0 Then
                sError &= "- EMAIL is invalid. Email format should like mail@sample.com!<BR>"
            End If
        End If
        If SuppWebsite.Text <> "" Then
            oMatches = Regex.Matches(SuppWebsite.Text, "([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?")
            If oMatches.Count <= 0 Then
                sError &= "- WEBSITE is invalid. Website format should like www.sample.com!<BR>"
            End If
        End If
        If SuppCP1Email.Text <> "" Then
            oMatches = Regex.Matches(SuppCP1Email.Text, "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")
            If oMatches.Count <= 0 Then
                sError &= "- CP1 EMAIL is invalid. Email format should like mail@sample.com!<BR>"
            End If
        End If
        If SuppCP2Email.Text <> "" Then
            oMatches = Regex.Matches(SuppCP2Email.Text, "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")
            If oMatches.Count <= 0 Then
                sError &= "- CP2 EMAIL is invalid. Email format should like mail@sample.com!<BR>"
            End If
        End If
        If SuppNote.Text.Length > 100 Then
            sError &= "- NOTE must be less than 100 characters!<BR>"
        End If
        If suppres3.Text.Length > 1000 Then
            sError &= "- TAG must be less than 1000 characters!<BR>"
        End If
        If DDLAkun.SelectedValue = 0 Then
            sError &= "- Please Select Account Payable First!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Sub GenerateCodeSupp()
        Dim sCode As String = ""
        Dim Temp_String As String = ""

		If Trim(CodeSupp.Text) <> "" Then
			Temp_String = Tchar(LTrim(CodeSupp.Text))
			Temp_String = Temp_String.Replace(" ", "")
			sCode = Left(Temp_String, 3).ToUpper
			If Session("oid") = "" Or Session("oid") = Nothing Then
				sSql = "SELECT ISNULL(MAX(CAST(RIGHT(SuppCode, 3) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstSupp WHERE cmpcode='" & Session("CompnyCode") & "' and suppname LIKE '" & Left(Temp_String, 3) & "%'"
				SuppCode.Text = GenNumberString(CodeSupp.Text, "", cKon.ambilscalar(sSql), 3)
			Else
				sSql = "SELECT ISNULL(MAX(CAST(RIGHT(SuppCode, 3) AS INTEGER)), 1) AS IDNEW FROM QL_mstSupp WHERE cmpcode='" & Session("CompnyCode") & "' and suppname like '" & Left(Temp_String, 2) & "%'"
				SuppCode.Text = GenNumberString(sCode, "", cKon.ambilscalar(sSql), 3)
			End If
		Else
			SuppCode.Text = ""
		End If
    End Sub

    Private Function UpdateCheckedCat1() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblCat1") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCat1")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListCat1.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListCat1.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView.RowFilter = "cat1oid=" & cbOid
                                dtView(0)("CheckValue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl = dtView.ToTable
                Session("TblCat1") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblCat1View") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCat1")
            Dim dtTbl2 As DataTable = Session("TblCat1View")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListCat1.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListCat1.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "cat1oid=" & cbOid
                                dtView2.RowFilter = "cat1oid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblCat1") = dtTbl
                Session("TblCat1View") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function ShowPrint()
        Try
            report.Load(Server.MapPath(folderReport & "rptSupp.rpt"))
            Dim sWhere As String = " WHERE s.cmpcode='" & CompnyCode & "' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
            If cbTag.Checked = True Then
                sWhere &= " AND (suppres3 LIKE '%" & ddlTag.SelectedItem.Text & "%' OR suppres3='' OR suppres3 IS NULL)"
            End If
            If checkPagePermission("~\Master\mstSupp.aspx", Session("SpecialAccess")) = False Then
                sWhere &= " AND s.createuser='" & Session("UserID") & "'"
            End If
            sWhere &= " ORDER BY supptype, suppoid"
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)
            Return True
        Catch ex As Exception
            showMessage(ex.Message, 1)
            Return False
        End Try
    End Function

    Private Function IsCodeExists() As Boolean
        isRegenOid = False
        sSql = "SELECT COUNT(-1) FROM QL_mstsupp WHERE cmpcode='" & CompnyCode & "' AND suppcode='" & Tchar(SuppCode.Text) & "'"
        If Not Session("oid") Is Nothing And Session("oid") <> "" Then
            sSql &= " AND suppoid<>" & Session("oid")
        End If
        If ToDouble(GetStrData(sSql).ToString) > 0 Then
            GenerateCodeSupp()
            'showMessage("CODE has been used by another data.. System will generate your CODE became "& &"!", 2)
            isRegenOid = True
            Return True
        End If
        Return False
    End Function

    Private Function GetCheckedTag() As String
        Dim sRet As String = ""
        If cblsuppres3.Items.Count > 0 Then
            For C1 As Integer = 0 To cblsuppres3.Items.Count - 1
                If cblsuppres3.Items(C1).Selected = True Then
                    sRet &= cblsuppres3.Items(C1).Value
                    If C1 < cblsuppres3.Items.Count - 1 Then
                        sRet &= ";"
                    End If
                End If
            Next
        End If
        Return sRet
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindMstData(ByVal sFilter As String)
        sSql = "SELECT S.SuppOid, S.SuppCode, S.supptype, S.SuppName, S.SuppAddr, C.gendesc City, S.SuppPhone1, S.SuppPhone2, S.SuppPhone3, S.SuppFax1, S.SuppFax2, S.SuppEmail, S.SuppWebsite, S.SuppCP1Name, S.SuppCP1Phone, SuppCP1Email, S.SuppCP2Name, S.SuppCP2Phone, SuppCP2Email, P.gendesc PaymentType, S.SuppNote, S.SuppRes3 FROM QL_mstSupp S INNER JOIN QL_mstgen C ON (C.cmpcode='" & CompnyCode & "' AND C.gengroup='CITY' AND C.genoid=S.SuppCityOid AND C.activeflag='ACTIVE') INNER JOIN QL_mstgen P ON (P.cmpcode='" & CompnyCode & "' AND P.gengroup='PAYMENT TYPE' AND P.genoid=S.SuppPaymentOid AND P.activeflag='ACTIVE') INNER JOIN QL_mstgen g1 ON g1.genoid=S.suppprefixoid AND g1.activeflag='ACTIVE' WHERE S.cmpcode='" & CompnyCode & "' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(sFilter) & "%' AND S.supptype <> 'EKSPEDISI' "
        If cbType.Checked = True Then
            sSql &= " AND S.suppgroup='" & DDLMatType.SelectedValue & "' "
        End If
        'If cbTag.Checked = True Then
        '    sSql &= " AND (S.SuppRes3 LIKE '%" & ddlTag.SelectedItem.Text & "%' OR S.SuppRes3='' OR S.SuppRes3 IS NULL) "
        'End If
        If checkPagePermission("~\Master\mstSupp.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND S.createuser='" & Session("UserID") & "'"
        End If
        sSql &= " ORDER BY SuppOid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_mstSupp")
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub BindCategory1()
        'sSql = "SELECT cat1oid, 'False' AS CheckValue, cat1code, cat1shortdesc, cat1res1, cat1note FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        'sSql &= " UNION ALL "
        'sSql &= "SELECT serviceoid AS cat1oid, 'False' AS CheckValue, servicecode AS cat1code, servicelongdesc AS cat1shortdesc, 'Jasa' AS cat1res1, servicenote AS cat1note FROM QL_mstservice WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        'Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstcat1")
        'If dtTbl.Rows.Count > 0 Then
        '    Session("TblCat1") = dtTbl
        '    Session("TblCat1View") = dtTbl
        '    gvListCat1.DataSource = Session("TblCat1View")
        '    gvListCat1.DataBind()
        '    cProc.SetModalPopUpExtender(btnHideListCat1, pnlListCat1, mpeListCat1, True)
        'Else
        '    showMessage("Category1 data can't be found!", 2)
        'End If
    End Sub

    Private Sub InitAllDDL()
        Dim sError As String = ""
        ' Fill DDL Type
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='PREFIX NAME' AND activeflag='ACTIVE'"
        FillDDL(SuppPrefixOid, sSql)
        If SuppPrefixOid.Items.Count = 0 Then
            sError = "PREFIX NAME data is empty. Please input PREFIX NAME data first or contact your administrator!<BR>"
        End If
        FillDDLAcctg(DDLAkun, "VAR_AP", CompnyCode, "0", "-")
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='CITY' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDL(SuppCityOid, sSql)
        If SuppPrefixOid.Items.Count = 0 Then
            sError = "CITY data is empty. Please input CITY data first or contact your administrator!<BR>"
        End If
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='PAYMENT TYPE' AND activeflag='ACTIVE'"
        FillDDL(SuppPaymentOid, sSql)
        If SuppPrefixOid.Items.Count = 0 Then
            sError = "PAYMENT TYPE data is empty. Please input PAYMENT TYPE data first or contact your administrator!<BR>"
        End If
        sSql = "SELECT custoid, custname FROM QL_mstcust WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' order by custname"
        FillDDLWithAdditionalText(DDLcust, sSql, "-", 0)
        If sError <> "" Then
            showMessage(sError, 2)
            btnSave.Visible = False
            btnAll.Visible = False
            btnSearch.Visible = False
        End If
    End Sub

    Private Sub FillTextBox()
        Try
            sSql = "SELECT SuppOid, SuppCode, supptype, ISNULL(suppres1, 'Supplier') AS suppres1, SuppPrefixOid, SuppName, SuppAddr, SuppCityOid, SuppPhone1, SuppPhone2, SuppPhone3, SuppFax1, SuppFax2, SuppEmail, SuppWebsite, SuppCP1Name, SuppCP2Name, SuppCP1Phone, SuppCP2Phone, SuppCP1Email, SuppCP2Email, SuppPaymentOid, SuppNote, SuppTaxable, SuppNPWP, SuppRes3, ActiveFlag, createuser, createtime, upduser, updtime, apacctgoid, suppres2, custoid, suppgroup, suppname2 FROM QL_mstSupp WHERE cmpcode='" & CompnyCode & "' AND SuppOid=" & Session("oid")
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            btnDelete.Enabled = False
            While xreader.Read
                SuppOid.Text = xreader("SuppOid").ToString
                SuppCode.Text = xreader("SuppCode").ToString
                supptype.SelectedValue = xreader("supptype").ToString
                If xreader("suppres1").ToString = "External" Then
                    rbType1.Checked = True
                    rbType2.Checked = False
                Else
                    rbType1.Checked = False
                    rbType2.Checked = True
                End If
                SuppPrefixOid.SelectedValue = xreader("SuppPrefixOid").ToString
                OldCode.Text = xreader("suppres2").ToString
                SuppName.Text = xreader("SuppName").ToString
                SuppAddr.Text = xreader("SuppAddr").ToString
                SuppCityOid.SelectedValue = xreader("SuppCityOid").ToString
                SuppPhone1.Text = xreader("SuppPhone1").ToString
                SuppPhone2.Text = xreader("SuppPhone2").ToString
                SuppPhone3.Text = xreader("SuppPhone3").ToString
                SuppFax1.Text = xreader("SuppFax1").ToString
                SuppFax2.Text = xreader("SuppFax2").ToString
                SuppEmail.Text = xreader("SuppEmail").ToString
                SuppWebsite.Text = xreader("SuppWebsite").ToString
                SuppCP1Name.Text = xreader("SuppCP1Name").ToString
                SuppCP2Name.Text = xreader("SuppCP2Name").ToString
                SuppCP1Phone.Text = xreader("SuppCP1Phone").ToString
                SuppCP2Phone.Text = xreader("SuppCP2Phone").ToString
                SuppCP1Email.Text = xreader("SuppCP1Email").ToString
                SuppCP2Email.Text = xreader("SuppCP2Email").ToString
                SuppTaxable.SelectedValue = xreader("SuppTaxable").ToString
                SuppPaymentOid.SelectedValue = xreader("SuppPaymentOid").ToString
                SuppNote.Text = xreader("SuppNote").ToString
                DDLAkun.SelectedValue = xreader("apacctgoid").ToString
                If xreader("SuppTaxable").ToString = 0 Then
                    NPWP.Visible = False : NPWP4.Visible = False : SuppNPWP.Visible = False
                End If
                SuppNPWP.Text = xreader("SuppNPWP").ToString
                SetCheckedTag(xreader("suppres3").ToString)
                ActiveFlag.SelectedValue = xreader("ActiveFlag").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
                DDLcust.SelectedValue = xreader("custoid").ToString
                suppgroup.SelectedValue = xreader("suppgroup")
                suppname2.Text = xreader("suppname2")
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
        Finally
            btnDelete.Enabled = True
            SuppCode.CssClass = "inpTextDisabled"
            SuppCode.Enabled = False
        End Try
    End Sub

    Private Sub GenerateCode()
        Dim sCode As String = "SUP-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(SuppCode, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstSupp WHERE cmpcode='" & CompnyCode & "' AND SuppCode LIKE '" & sCode & "%'"
        SuppCode.Text = GenNumberString(sCode, "", GetStrData(sSql), DefaultFormatCounter)
    End Sub

    Private Sub SetCheckedTag(ByVal sTag As String)
        If sTag <> "" Then
            Dim sSplit() As String = sTag.Split(";")
            If sSplit.Length > 0 Then
                If cblsuppres3.Items.Count > 0 Then
                    For C1 As Integer = 0 To sSplit.Length - 1
                        Dim li As ListItem = cblsuppres3.Items.FindByValue(sSplit(C1))
                        Dim i As Integer = cblsuppres3.Items.IndexOf(li)
                        If i >= 0 Then
                            cblsuppres3.Items(cblsuppres3.Items.IndexOf(li)).Selected = True
                        End If
                    Next
                End If
            End If
        End If
    End Sub

    Private Sub PrintReport(ByVal sType As String)
        Try
            If sType = "PDF" Then
                report.Load(Server.MapPath(folderReport & "rptSupp.rpt"))
            Else
                report.Load(Server.MapPath(folderReport & "rptSuppExcel.rpt"))
            End If
            Dim sWhere As String = "WHERE s.cmpcode='" & CompnyCode & "' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
            If cbTag.Checked = True Then
                sWhere &= " AND (suppres3 LIKE '%" & ddlTag.SelectedItem.Text & "%' OR suppres3='' OR suppres3 IS NULL)"
            End If
            If checkPagePermission("~\Master\mstSupp.aspx", Session("SpecialAccess")) = False Then
                sWhere &= " AND s.createuser='" & Session("UserID") & "'"
            End If
            sWhere &= " ORDER BY supptype, suppoid"
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            If sType = "PDF" Then
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "SupplierPrintOut")
            Else
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "SupplierPrintOut")
            End If
            report.Close()
            report.Dispose()
            Response.Redirect("~\Master\mstSupp.aspx?awal=true")
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = CompnyCode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Master\mstSupp.aspx")
        End If
        If checkPagePermission("~\Master\mstSupp.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Supplier"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            I_U.Text = "New Data"
        Else
            I_U.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-"
            updtime.Text = "-"
            InitAllDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox()
                TabContainer1.ActiveTabIndex = 1
            Else
                SuppOid.Text = GenerateID("QL_mstSupp", CompnyCode)
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
                GenerateCode()
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListCat1") Is Nothing And Session("WarningListCat1") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListCat1") Then
                Session("WarningListCat1") = Nothing
                cProc.SetModalPopUpExtender(btnHideListCat1, pnlListCat1, mpeListCat1, True)
            End If
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Master\mstSupp.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbType.Checked = False
        cbType_CheckedChanged(Nothing, Nothing)
        DDLMatType.SelectedIndex = 0
        cbTag.Checked = False
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
    End Sub

    Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvMst.DataSource = Session("TblMst")
            gvMst.DataBind()
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            If Not IsCodeExists() Then
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    SuppOid.Text = GenerateID("QL_MSTSUPP", CompnyCode)
                End If
                Dim objTrans As SqlClient.SqlTransaction
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If
                objTrans = conn.BeginTransaction()
                xCmd.Transaction = objTrans
                Try
                    If Session("oid") = Nothing Or Session("oid") = "" Then
                        sSql = "INSERT INTO QL_mstSupp (cmpcode, SuppOid, SuppCode, supptype, apacctgoid,SuppPrefixOid, SuppName, SuppAddr, SuppCityOid, SuppPhone1, SuppPhone2, SuppPhone3, SuppFax1, SuppFax2, SuppEmail, SuppWebsite, SuppCP1Name, SuppCP2Name, SuppCP1Phone, SuppCP2Phone, SuppCP1Email, SuppCP2Email, SuppPaymentOid, SuppNote, SuppTaxable, SuppNPWP, SuppRes3, ActiveFlag, createuser, createtime, upduser, updtime, suppres1, suppres2, custoid, suppgroup, suppname2) VALUES('" & CompnyCode & "', " & SuppOid.Text & ", '" & Tchar(SuppCode.Text) & "', '" & supptype.SelectedValue & "'," & DDLAkun.SelectedValue & ", " & SuppPrefixOid.SelectedValue & ", '" & Tchar(SuppName.Text) & "', '" & Tchar(SuppAddr.Text) & "', " & SuppCityOid.SelectedValue & ", '" & SuppPhone1.Text & "', '" & SuppPhone2.Text & "', '" & SuppPhone3.Text & "', '" & SuppFax1.Text & "', '" & SuppFax2.Text & "', '" & Tchar(SuppEmail.Text) & "', '" & Tchar(SuppWebsite.Text) & "', '" & Tchar(SuppCP1Name.Text) & "', '" & Tchar(SuppCP2Name.Text) & "', '" & SuppCP1Phone.Text & "', '" & SuppCP2Phone.Text & "', '" & Tchar(SuppCP1Email.Text) & "', '" & Tchar(SuppCP2Email.Text) & "', " & SuppPaymentOid.SelectedValue & ", '" & Tchar(SuppNote.Text) & "', " & SuppTaxable.SelectedValue & ", '" & Tchar(SuppNPWP.Text) & "', '" & Tchar(GetCheckedTag()) & "', '" & ActiveFlag.SelectedValue & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & IIf(rbType1.Checked = True, "External", "Internal") & "', '" & Tchar(OldCode.Text) & "', " & DDLcust.SelectedValue & ", '" & Tchar(suppgroup.SelectedValue.ToString) & "', '" & Tchar(suppname2.Text) & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        sSql = "UPDATE QL_mstoid SET lastoid=" & SuppOid.Text & " WHERE tablename='QL_MSTSUPP' AND cmpcode LIKE '%" & CompnyCode & "%'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Else
                        sSql = "UPDATE QL_mstSupp SET supptype='" & supptype.SelectedValue & "',apacctgoid=" & DDLAkun.SelectedValue & ", SuppPrefixOid=" & SuppPrefixOid.SelectedValue & ", SuppName='" & Tchar(SuppName.Text) & "', SuppAddr='" & Tchar(SuppAddr.Text) & "', SuppCityOid=" & SuppCityOid.SelectedValue & ", SuppPhone1='" & SuppPhone1.Text & "', SuppPhone2='" & SuppPhone2.Text & "', SuppPhone3='" & SuppPhone3.Text & "', SuppFax1='" & SuppFax1.Text & "', SuppFax2='" & SuppFax2.Text & "', SuppEmail='" & Tchar(SuppEmail.Text) & "', SuppWebsite='" & Tchar(SuppWebsite.Text) & "', SuppCP1Name='" & Tchar(SuppCP1Name.Text) & "', SuppCP2Name='" & Tchar(SuppCP2Name.Text) & "', SuppCP1Phone='" & SuppCP1Phone.Text & "', SuppCP2Phone='" & SuppCP2Phone.Text & "', SuppCP1Email='" & Tchar(SuppCP1Email.Text) & "', SuppCP2Email='" & Tchar(SuppCP2Email.Text) & "', SuppPaymentOid=" & SuppPaymentOid.SelectedValue & ", SuppNote='" & Tchar(SuppNote.Text) & "', SuppTaxable=" & SuppTaxable.SelectedValue & ", SuppNPWP='" & Tchar(SuppNPWP.Text) & "', SuppRes3='" & Tchar(GetCheckedTag()) & "', ActiveFlag='" & ActiveFlag.SelectedValue & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, suppres1='" & IIf(rbType1.Checked = True, "External", "Internal") & "',suppres2='" & Tchar(OldCode.Text) & "', custoid=" & DDLcust.SelectedValue & ", suppgroup='" & Tchar(suppgroup.SelectedValue.ToString) & "', suppname2='" & Tchar(suppname2.Text) & "' WHERE cmpcode='" & CompnyCode & "' AND SuppOid=" & SuppOid.Text
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                    objTrans.Commit()
                    conn.Close()
                Catch ex As Exception
                    objTrans.Rollback()
                    conn.Close()
                    showMessage(ex.Message, 1)
                    Exit Sub
                End Try
                If isRegenOid Then
                    Session("SavedInfo") = "CODE has been used by another data . Your new CODE is " & SuppCode.Text & ".<BR>"
                End If
                If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                    showMessage(Session("SavedInfo"), 3)
                Else
                    Response.Redirect("~\Master\mstSupp.aspx?awal=true")
                End If
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Master\mstSupp.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If SuppOid.Text = "" Then
            showMessage("Please select Template data first!", 1)
            Exit Sub
        End If
        'sSql = "SELECT tblusage, colusage FROM QL_oidusage WHERE cmpcode='" & CompnyCode & "' AND tblname='QL_mstSupp'"

 sSql = "SELECT ta.name FROM sys.tables ta INNER JOIN sys.columns col ON col.object_id=ta.object_id WHERE col.name='suppoid' AND ta.name NOT IN ('QL_mstSupp')"

        'Dim objTblUsage As DataTable = cKon.ambiltabel(sSql, "QL_oidusage")
        'Dim sColomnName(objTblUsage.Rows.Count - 1) As String
        'Dim sTable(objTblUsage.Rows.Count - 1) As String
        'For c1 As Integer = 0 To objTblUsage.Rows.Count - 1
        '    sColomnName(c1) = objTblUsage.Rows(c1).Item("colusage").ToString
        '    sTable(c1) = objTblUsage.Rows(c1).Item("tblusage").ToString
        'Next
        'If CheckDataExists(SuppOid.Text, sColomnName, sTable) = True Then
        '    showMessage("This data can't be deleted because it is being used by another data!", 2)
        '    Exit Sub
        'End If
        'If DeleteData("QL_mstSupp", "SuppOid", SuppOid.Text, CompnyCode) = True Then
         '   Response.Redirect("~\Master\mstSupp.aspx?awal=true")
        'End If


Dim objTblUsage As DataTable = cKon.ambiltabel(sSql, "QL_tables")
        For C1 As Integer = 0 To objTblUsage.Rows.Count - 1
            If CheckDataExists("SELECT COUNT(*) FROM " & objTblUsage.Rows(C1).Item("name").ToString & " WHERE suppoid=" & SuppOid.Text) Then
                showMessage("This data can't be deleted because it is being used by another data!", 2)
                Exit Sub
            End If
        Next
        If DeleteData("QL_mstSupp", "SuppOid", SuppOid.Text, CompnyCode) = True Then
            Response.Redirect("~\Master\mstSupp.aspx?awal=true")
        End If

    End Sub

    Protected Sub SuppTaxable_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SuppTaxable.SelectedIndexChanged
        If SuppTaxable.SelectedValue = 0 Then
            NPWP.Visible = False
            NPWP4.Visible = False
            SuppNPWP.Visible = False
        Else
            NPWP.Visible = True
            NPWP4.Visible = True
            SuppNPWP.Visible = True
        End If
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        PrintReport("PDF")
    End Sub

    Protected Sub btnExportExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportExcel.Click
        PrintReport("Excel")
    End Sub

    Protected Sub btnshowcat1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnshowcat1.Click
        FilterDDLListCat1.SelectedIndex = -1 : FilterTextListCat1.Text = "" : Session("TblCat1") = Nothing : Session("TblCat1View") = Nothing : gvListCat1.DataSource = Nothing : gvListCat1.DataBind()
        BindCategory1()
    End Sub

    Protected Sub lbAddToListListCat1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListListCat1.Click
        If UpdateCheckedCat1() Then
            Dim dtTbl As DataTable = Session("TblCat1")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim resTemp As String = suppres3.Text
            dtView.RowFilter = "CheckValue='True'"
            Dim iCheck As Integer = dtView.Count
            If iCheck > 0 Then
                For C1 As Integer = 0 To dtView.Count - 1
                    Dim countcek As Integer = 0
                    If suppres3.Text <> "" Then
                        Dim sRes3() As String = Split(suppres3.Text, ";")
                        For C2 As Integer = 0 To sRes3.Length - 1
                            If dtView(C1)("cat1shortdesc").ToString.ToUpper = Tchar(sRes3(C2).ToUpper) Then
                                countcek = 1
                                Exit For
                            End If
                        Next
                        If countcek = 0 Then
                            resTemp &= ";" + dtView(C1)("cat1shortdesc").ToString.ToUpper
                        End If
                    Else
                        If resTemp <> "" Then
                            resTemp &= ";" + dtView(C1)("cat1shortdesc").ToString.ToUpper
                        Else
                            resTemp = dtView(C1)("cat1shortdesc").ToString.ToUpper
                        End If
                    End If
                Next
                suppres3.Text = resTemp
                cProc.SetModalPopUpExtender(btnHideListCat1, pnlListCat1, mpeListCat1, False)
            Else
                Session("WarningListCat1") = "Please select category1 to add to list!"
                showMessage(Session("WarningListCat1"), 2)
                Exit Sub
            End If
        Else
            Session("WarningListCat1") = "Please select category1 to add to list!"
            showMessage(Session("WarningListCat1"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lbCloseListCat1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListCat1.Click
        cProc.SetModalPopUpExtender(btnHideListCat1, pnlListCat1, mpeListCat1, False)
    End Sub

    Protected Sub btnFindListCat1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListCat1.Click
        If UpdateCheckedCat1() Then
            Dim dtTbl As DataTable = Session("TblCat1")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = FilterDDLListCat1.SelectedValue & " LIKE '%" & Tchar(FilterTextListCat1.Text) & "%'"
            If dtView.Count > 0 Then
                Session("TblCat1View") = dtView.ToTable
                gvListCat1.DataSource = Session("TblCat1View")
                gvListCat1.DataBind()
                dtView.RowFilter = ""
                mpeListCat1.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListCat1") = "Category1 data can't be found!"
                showMessage(Session("WarningListCat1"), 2)
            End If
        Else
            mpeListCat1.Show()
        End If
    End Sub

    Protected Sub btnAllListCat1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListCat1.Click
        FilterDDLListCat1.SelectedIndex = -1 : FilterTextListCat1.Text = ""
        If UpdateCheckedCat1() Then
            Session("TblCat1View") = Session("TblCat1")
            gvListCat1.DataSource = Session("TblCat1View")
            gvListCat1.DataBind()
        End If
        mpeListCat1.Show()
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblCat1View") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCat1View")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblCat1")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "cat1oid=" & dtTbl.Rows(C1)("cat1oid")
                    objView(0)("CheckValue") = "True"
                    dtTbl.Rows(C1)("CheckValue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                dtTbl.AcceptChanges()
                Session("TblCat1") = objTbl
                Session("TblCat1View") = dtTbl
                gvListCat1.DataSource = Session("TblCat1View")
                gvListCat1.DataBind()
                mpeListCat1.Show()
            Else
                Session("WarningListCat1") = "No category1 data can't be selected!"
                showMessage(Session("WarningListCat1"), 2)
            End If
        Else
            Session("WarningListCat1") = "No category1 data can't be selected!"
            showMessage(Session("WarningListCat1"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblCat1View") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCat1View")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblCat1")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "cat1oid=" & dtTbl.Rows(C1)("cat1oid")
                    objView(0)("CheckValue") = "False"
                    dtTbl.Rows(C1)("CheckValue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                dtTbl.AcceptChanges()
                Session("TblCat1") = objTbl
                Session("TblCat1View") = dtTbl
                gvListCat1.DataSource = Session("TblCat1View")
                gvListCat1.DataBind()
                mpeListCat1.Show()
            Else
                Session("WarningListCat1") = "No category1 data can't be selected!"
                showMessage(Session("WarningListCat1"), 2)
            End If
        Else
            Session("WarningListCat1") = "No category1 data can't be selected!"
            showMessage(Session("WarningListCat1"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If UpdateCheckedCat1() Then
            Dim dtTbl As DataTable = Session("TblCat1")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "CheckValue='True'"
            If dtView.Count > 0 Then
                Session("TblCat1View") = dtView.ToTable
                gvListCat1.DataSource = Session("TblCat1View")
                gvListCat1.DataBind()
                dtView.RowFilter = ""
                mpeListCat1.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListCat1") = "No category1 data have been selected before!"
                showMessage(Session("WarningListCat1"), 2)
            End If
        Else
            mpeListCat1.Show()
        End If
    End Sub

    Protected Sub gvListCat1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListCat1.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListCat1.PageIndex = e.NewPageIndex
            gvListCat1.DataSource = Session("TblCat1View")
            gvListCat1.DataBind()
        End If
        mpeListCat1.Show()
    End Sub

    Protected Sub cbType_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbType.CheckedChanged
        If cbType.Checked = True Then
            sSql = "SELECT cat1oid, cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND cat1res1='" & DDLMatType.SelectedValue & "' AND activeflag='ACTIVE'"
            FillDDL(ddlTag, sSql)
        Else
            sSql = "SELECT cat1oid, cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
            FillDDL(ddlTag, sSql)
        End If
    End Sub

    Protected Sub DDLMatType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLMatType.SelectedIndexChanged
        If cbType.Checked = True Then
            sSql = "SELECT cat1oid, cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND cat1res1='" & DDLMatType.SelectedValue & "' AND activeflag='ACTIVE'"
            FillDDL(ddlTag, sSql)
        End If
    End Sub

    Protected Sub SuppName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
		'GenerateCodeSupp()
    End Sub

	Protected Sub CodeSupp_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'GenerateCodeSupp()
    End Sub
#End Region
End Class
