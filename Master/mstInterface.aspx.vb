Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_Interface
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        'If DDLBusUnit.SelectedValue = "" Then
        '    sError &= "- Please select BUSINESS UNIT field!<BR>"
        'End If
        Dim bVal As Boolean = False
        For C1 As Integer = 0 To cbldata.Items.Count - 1
            If cbldata.Items(C1).Selected = True Then
                bVal = True
                Exit For
            End If
        Next
        If bVal = False Then
            sError &= "- Please select minimally 1 COA field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitDDL(ByVal sAdd As String)
        ' Fill DDL Business Unit
        'sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        'If Session("CompnyCode") <> CompnyCode Then
        '    sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        'End If
        'If sAdd = "" Then
        '    FillDDL(DDLBusUnit, sSql)
        'Else
        '    FillDDLWithAdditionalText(DDLBusUnit, sSql, sAdd, sAdd)
        'End If
        'FillDDLWithAdditionalText(FilterDDLBusUnit, sSql, "All", "All")
    End Sub

    Private Sub BindMstData(ByVal sFilter As String)
        'ISNULL(divname, interfaceres1) AS interfaceres1,
        sSql = "SELECT interfaceoid, interfacevar, interfacevalue, i.activeflag, interfacenote,  ISNULL(interfaceres2, '') AS interfaceres2 FROM QL_mstinterface i WHERE i.cmpcode='" & CompnyCode & "' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(sFilter) & "%'"
        'LEFT JOIN QL_mstdivision d ON d.cmpcode=i.interfaceres1 WHERE i.cmpcode='" & CompnyCode & "' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(sFilter) & "%'"
        'If cbBusUnit.Checked Then
        '    sSql &= " AND interfaceres1='" & FilterDDLBusUnit.SelectedValue & "'"
        'End If
        If cbType.Checked Then
            sSql &= " AND interfaceres2='" & FilterDDLType.SelectedValue & "'"
        End If
        If cbStatus.Checked Then
            sSql &= " AND i.activeflag='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Master\mstInterface.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND i.createuser='" & Session("UserID") & "'"
        End If
        sSql &= " ORDER BY interfacevar"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_mstinterface")
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub FillTextBox()
        Try
            sSql = "SELECT interfaceoid, interfacevar, interfacevalue, interfacenote, activeflag, createuser, createtime, upduser, updtime, ISNULL(interfaceres1, 'All') AS interfaceres1, interfaceres2 FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfaceoid=" & Session("oid")
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                interfaceoid.Text = xreader("interfaceoid").ToString
                interfacevar.Text = xreader("interfacevar").ToString
                Dim sValue() As String = xreader("interfacevalue").ToString.Split(",")
                If sValue.Length > 0 Then
                    If cbldata.Items.Count > 0 Then
                        For C1 As Integer = 0 To sValue.Length - 1
                            Dim li As ListItem = cbldata.Items.FindByValue(sValue(C1).Trim)
                            Dim i As Integer = cbldata.Items.IndexOf(li)
                            If i >= 0 Then
                                cbldata.Items(cbldata.Items.IndexOf(li)).Selected = True
                            End If
                        Next
                    End If
                End If
                cbldata_SelectedIndexChanged(Nothing, Nothing)
                DDLBusUnit.SelectedValue = xreader("interfaceres1")
                interfacenote.Text = xreader("interfacenote").ToString
                activeflag.SelectedValue = xreader("activeflag")
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
                interfaceres2.Text = xreader("interfaceres2").ToString
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
        Finally
            DDLBusUnit.CssClass = "inpTextDisabled" : DDLBusUnit.Enabled = False
        End Try
    End Sub

    Private Sub InitCBL()
        sSql = "SELECT acctgcode, ('(' + acctgcode + ') ' + acctgdesc) AS acctgdesc FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' ORDER BY acctgcode"
        cbldata.Items.Clear()
        cbldata.Items.Add("All")
        cbldata.Items(cbldata.Items.Count - 1).Value = "All"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd = New SqlCommand(sSql, conn)
        xreader = xCmd.ExecuteReader
        While xreader.Read
            cbldata.Items.Add(xreader.GetValue(1).ToString)
            cbldata.Items(cbldata.Items.Count - 1).Value = xreader.GetValue(0).ToString
        End While
        xreader.Close()
        conn.Close()
        Session("LastAllCheck") = False
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Master\mstInterface.aspx")
        End If
        If checkPagePermission("~\Master\mstInterface.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Interface"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            I_U.Text = "New Data"
        Else
            I_U.Text = "Update Data"
        End If
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            InitDDL("All")
            InitCBL()
            btnSaveAs.Visible = False
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox()
                TabContainer1.ActiveTabIndex = 1
            Else
                interfaceoid.Text = GenerateID("QL_MSTINTERFACE", CompnyCode)
                TabContainer1.ActiveTabIndex = 0
                btnSave.Visible = False
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbBusUnit.Checked = False
        FilterDDLBusUnit.SelectedIndex = -1
        cbType.Checked = False
        FilterDDLType.SelectedIndex = -1
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
    End Sub

    Protected Sub gvMst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvMst.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If e.Row.Cells(4).Text.ToUpper = "ALL" Then
                e.Row.Cells(5).Enabled = False
            Else
                e.Row.Cells(5).Enabled = True
            End If
        End If
    End Sub

    Protected Sub gvMst_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvMst.SelectedIndexChanged
        Try
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            InitDDL("") : InitCBL()
            'ISNULL(interfaceres1, 'All') AS interfaceres1, 
            sSql = "SELECT interfacevar, interfacevalue, interfacenote, activeflag, interfaceres2 FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfaceoid=" & gvMst.SelectedDataKey.Item("interfaceoid").ToString
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                interfacevar.Text = xreader("interfacevar").ToString
                Dim sValue() As String = xreader("interfacevalue").ToString.Split(",")
                If sValue.Length > 0 Then
                    If cbldata.Items.Count > 0 Then
                        For C1 As Integer = 0 To sValue.Length - 1
                            Dim li As ListItem = cbldata.Items.FindByValue(sValue(C1).Trim)
                            Dim i As Integer = cbldata.Items.IndexOf(li)
                            If i >= 0 Then
                                cbldata.Items(cbldata.Items.IndexOf(li)).Selected = True
                            End If
                        Next
                    End If
                End If
                cbldata_SelectedIndexChanged(Nothing, Nothing)
                'DDLBusUnit.SelectedValue = xreader("interfaceres1")
                interfacenote.Text = xreader("interfacenote").ToString
                activeflag.SelectedValue = xreader("activeflag")
                interfaceres2.Text = xreader("interfaceres2").ToString
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
        Finally
            btnSave.Visible = False : btnSaveAs.Visible = True
        End Try
        TabContainer1.ActiveTabIndex = 1
    End Sub

    Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvMst.DataSource = Session("TblMst")
            gvMst.DataBind()
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim sValue As String = ""
            For C1 As Integer = 0 To cbldata.Items.Count - 1
                If cbldata.Items(C1).Value <> "All" And cbldata.Items(C1).Selected = True Then
                    sValue &= cbldata.Items(C1).Value & ", "
                End If
            Next
            If sValue <> "" Then
                sValue = Left(sValue, sValue.Length - 2)
            End If
            If sValue.Length > 4000 Then
                sValue = Left(sValue, 4000)
            End If
            Dim sSplit() As String = sValue.Split(", ")
            If interfaceres2.Text = "Single COA" Then
                If sSplit.Length > 1 Then
                    showMessage("Selected COA for this variable must be 1 (Single COA)!", 2)
                    Exit Sub
                Else
                    sSql = "SELECT COUNT(*) FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgcode LIKE '" & sSplit(0) & "%' AND activeflag='ACTIVE'"
                    If ToDouble(GetStrData(sSql).ToString) > 1 Then
                        showMessage("Selected COA for this variable must be 1 (Single COA)!", 2)
                        Exit Sub
                    End If
                End If
            End If
            If Session("oid") = Nothing Or Session("oid") = "" Then
                interfaceoid.Text = GenerateID("QL_MSTINTERFACE", CompnyCode)
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_mstinterface (cmpcode, interfaceoid, interfacevar, interfacevalue, interfacegroup, interfacenote, activeflag, createuser, createtime, upduser, updtime, interfaceres1, interfaceres2) VALUES ('" & CompnyCode & "', " & interfaceoid.Text & ", '" & interfacevar.Text & "', '" & sValue & "', 'ACCTG', '" & Tchar(interfacenote.Text) & "', '" & activeflag.SelectedValue & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & CompnyCode & "', '" & interfaceres2.Text & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & interfaceoid.Text & " WHERE tablename LIKE 'QL_MSTINTERFACE' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_mstinterface SET interfacevalue='" & sValue & "', interfacenote='" & Tchar(interfacenote.Text) & "', activeflag='" & activeflag.SelectedValue & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, interfaceres1='" & CompnyCode & "' WHERE cmpcode='" & CompnyCode & "' AND interfaceoid=" & interfaceoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                conn.Close()
            Catch ex As Exception
                objTrans.Rollback()
                conn.Close()
                showMessage(ex.Message, 1)
                Exit Sub
            End Try
            Response.Redirect("~\Master\mstInterface.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Master\mstInterface.aspx?awal=true")
    End Sub

    Protected Sub cbldata_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbldata.SelectedIndexChanged
        Dim iTotal As Integer = 0
        If cbldata.Items(0).Selected = True Then
            If cbldata.Items.Count > 1 Then
                For C1 As Integer = 1 To cbldata.Items.Count - 1
                    If cbldata.Items(C1).Selected = False Then
                        iTotal += 1
                    End If
                Next
                If iTotal = cbldata.Items.Count - 1 Then
                    For C1 As Integer = 1 To cbldata.Items.Count - 1
                        cbldata.Items(C1).Selected = True
                    Next
                Else
                    If Session("LastAllCheck") = True Then
                        cbldata.Items(0).Selected = False
                    Else
                        For C1 As Integer = 1 To cbldata.Items.Count - 1
                            cbldata.Items(C1).Selected = True
                        Next
                    End If
                End If
                Session("LastAllCheck") = cbldata.Items(0).Selected
            End If
        Else
            If cbldata.Items.Count > 1 Then
                For C1 As Integer = 1 To cbldata.Items.Count - 1
                    If cbldata.Items(C1).Selected = True Then
                        iTotal += 1
                    End If
                Next
                If iTotal = cbldata.Items.Count - 1 Then
                    If Session("LastAllCheck") = True Then
                        For C1 As Integer = 1 To cbldata.Items.Count - 1
                            cbldata.Items(C1).Selected = False
                        Next
                    Else
                        cbldata.Items(0).Selected = True
                    End If
                End If
                Session("LastAllCheck") = cbldata.Items(0).Selected
            End If
        End If
    End Sub

    Protected Sub btnSaveAs_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSaveAs.Click
        If IsInputValid() Then
            sSql = "SELECT COUNT(*) FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & interfacevar.Text & "' AND interfaceres1='" & DDLBusUnit.SelectedValue & "'"
            If CheckDataExists(sSql) Then
                showMessage("Variable : " & interfacevar.Text.ToUpper & " has been defined for Business Unit : " & DDLBusUnit.SelectedItem.Text.ToUpper & " in another data. Please choose another combination!", 2)
                Exit Sub
            End If
            Dim sValue As String = ""
            For C1 As Integer = 0 To cbldata.Items.Count - 1
                If cbldata.Items(C1).Value <> "All" And cbldata.Items(C1).Selected = True Then
                    sValue &= cbldata.Items(C1).Value & ", "
                End If
            Next
            If sValue <> "" Then
                sValue = Left(sValue, sValue.Length - 2)
            End If
            If sValue.Length > 2000 Then
                sValue = Left(sValue, 2000)
            End If
            Dim sSplit() As String = sValue.Split(", ")
            If interfaceres2.Text = "Single COA" Then
                If sSplit.Length > 1 Then
                    showMessage("Selected COA for this variable must be 1 (Single COA)!", 2)
                    Exit Sub
                Else
                    sSql = "SELECT COUNT(*) FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgcode LIKE '" & sSplit(0) & "%' AND activeflag='ACTIVE'"
                    If ToDouble(GetStrData(sSql).ToString) > 1 Then
                        showMessage("Selected COA for this variable must be 1 (Single COA)!", 2)
                        Exit Sub
                    End If
                End If
            End If
            interfaceoid.Text = GenerateID("QL_MSTINTERFACE", CompnyCode)
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                sSql = "INSERT INTO QL_mstinterface (cmpcode, interfaceoid, interfacevar, interfacevalue, interfacegroup, interfacenote, activeflag, createuser, createtime, upduser, updtime, interfaceres1, interfaceres2) VALUES ('" & CompnyCode & "', " & interfaceoid.Text & ", '" & interfacevar.Text & "', '" & sValue & "', 'ACCTG', '" & Tchar(interfacenote.Text) & "', '" & activeflag.SelectedValue & "', '" & createuser.Text & "', '" & createtime.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & DDLBusUnit.SelectedValue & "', '" & interfaceres2.Text & "')"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & interfaceoid.Text & " WHERE tablename='QL_MSTINTERFACE' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                objTrans.Commit()
                conn.Close()
            Catch ex As Exception
                objTrans.Rollback()
                conn.Close()
                showMessage(ex.Message, 1)
                Exit Sub
            End Try
            Response.Redirect("~\Master\mstInterface.aspx?awal=true")
        End If
    End Sub
#End Region

End Class
