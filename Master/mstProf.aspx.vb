Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_Profile
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        'If DDLCmpCode.SelectedValue = "" Then
        '    sError &= "- Please select BUSINESS UNIT field!<BR>"
        'End If
        'If personoid.Text = "" Then
        '    sError &= "- Please select NIK field!<BR>"
        'End If
        If profoid.Text = "" Then
            sError &= "- Please fill USER ID field!<BR>"
        End If
        If profname.Text = "" Then
            sError &= "- Please fill USER NAME field!<BR>"
        End If
        If profpass.Text = "" And lblProfPass.Text = "" Then
            sError &= "- Please fill PASSWORD field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputExists() As Boolean
        If Session("oid") = Nothing Or Session("oid") = "" Then
            sSql = "SELECT COUNT(-1) FROM QL_mstprof WHERE profoid='" & Tchar(profoid.Text.Trim) & "'"
            If ToDouble(cKon.ambilscalar(sSql).ToString) > 0 Then
                showMessage("User ID has been used before. Please fill another User ID!", 2)
                Return True
            End If
        End If
        Return False
    End Function

    'Private Function IsPersonExists() As Boolean
    '    Dim bReturn As Boolean = False
    '    sSql = "SELECT COUNT(*) FROM QL_mstperson WHERE cmpcode='" & DDLCmpCode.SelectedValue & "' AND activeflag='ACTIVE'"
    '    Dim iAllPersCount As Integer = CInt(cKon.ambilscalar(sSql))
    '    sSql &= " AND personoid NOT IN (SELECT personoid FROM QL_mstprof)"
    '    Dim iAvPersCount As Integer = CInt(cKon.ambilscalar(sSql))
    '    If iAllPersCount = 0 Then
    '        showMessage("Employee data is empty. Please input employee data first or contact your Administrator", 2)
    '    Else
    '        If iAvPersCount = 0 Then
    '            showMessage("All Employee data already have profile.", 2)
    '        Else
    '            bReturn = True
    '        End If
    '    End If
    '    Return bReturn
    'End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindMstData(ByVal sFilter As String)
        sSql = "SELECT profoid, profname, activeflag FROM QL_mstprof WHERE " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(sFilter) & "%' "
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & CompnyCode & "'"
        End If
        'If cbBU.Visible Then
        '    If cbBU.Checked Then
        '        If FilterDDLBU.SelectedValue <> "" Then
        '            sSql &= " AND cmpcode='" & CompnyCode & "'"
        '        End If
        '    End If
        'End If
        If cbStatus.Checked Then
            sSql &= " AND activeflag='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Master\mstProf.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        sSql &= " ORDER BY activeflag"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_mstprof")
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub FillTextBox()
        Try
            sSql = "SELECT pr.cmpcode, pr.profoid, pr.profname, pr.profpass, pr.activeflag, pr.personoid, pr.createuser, pr.createtime, pr.upduser, pr.updtime FROM QL_mstprof pr WHERE pr.cmpcode='" & CompnyCode & "' AND pr.profoid='" & Session("oid") & "'" ' "
            'LEFT JOIN QL_mstperson p ON pr.personoid=p.personoid WHERE pr.cmpcode='" & Session("CompnyCode") & "' AND pr.profoid='" & Session("oid") & "'"
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            btnDelete.Visible = False
            While xreader.Read
                profoid.Text = xreader("profoid").ToString
                profname.Text = xreader("profname").ToString
                lblProfPass.Text = xreader("profpass").ToString
                profpass.Text = lblProfPass.Text
                activeflag.SelectedValue = xreader("activeflag").ToString
                'nip.Text = xreader("nip").ToString
                personoid.Text = xreader("personoid").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
                'DDLCmpCode.SelectedValue = xreader("cmpcode").ToString
            End While
        Catch ex As Exception
            showMessage(ex.Message, 1)
        Finally
            xreader.Close()
            conn.Close()
            btnDelete.Visible = True
            profoid.Enabled = False
            profoid.CssClass = "inpTextDisabled"
            lblPassword.Text = "New Password"
            'imbSearchPerson.Visible = False
            'imbErasePerson.Visible = False
            'DDLCmpCode.CssClass = "inpTextDisabled"
            'DDLCmpCode.Enabled = False
        End Try
        If profoid.Text.Contains("Admin") Or profoid.Text.Contains("admin") Or profoid.Text.Contains("ADMIN") Or profoid.Text.Contains("Administrator") Or profoid.Text.Contains("administrator") Or profoid.Text.Contains("ADIMINISTRATOR") Then
            btnSave.Visible = False
            btnDelete.Visible = False
        End If
    End Sub

    Private Sub BindListPerson()
        'sSql = "SELECT p.personoid, p.nip, p.personname, de.deptname FROM QL_mstperson p INNER JOIN QL_mstdept de ON p.deptoid=de.deptoid WHERE p.activeflag='ACTIVE' AND " & FilterDDLListPerson.SelectedValue & " LIKE '%" & Tchar(FilterTextListPerson.Text) & "%' AND p.personoid NOT IN (SELECT DISTINCT personoid FROM QL_mstprof) AND p.cmpcode='" & DDLCmpCode.SelectedValue & "'"
        'If cbListPersonDept.Checked Then
        '    sSql &= " AND p.deptoid=" & FilterDDLListPersonDept.SelectedValue
        'End If
        'sSql &= " ORDER BY p.personoid"
        'FillGV(gvListPerson, sSql, "QL_mstperson")
    End Sub

    Private Sub InitDDLBusinessUnit()
        'sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        'If Session("CompnyCode") <> CompnyCode Then
        '    sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        'End If
        'FillDDL(DDLCmpCode, sSql)
        'FillDDL(FilterDDLBU, sSql)
    End Sub

    Private Sub InitDDLDept()
        'If DDLCmpCode.SelectedValue <> "" Then
        '    sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE cmpcode='" & DDLCmpCode.SelectedValue & "' AND activeflag='ACTIVE'"
        '    FillDDL(FilterDDLListPersonDept, sSql)
        'End If
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Master\mstProf.aspx")
        End If
        If checkPagePermission("~\Master\mstProf.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Profile"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            I_U.Text = "New Data"
        Else
            I_U.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID")
            createtime.Text = GetServerTime().ToString
            upduser.Text = "-"
            updtime.Text = "-"
            cbStatus.Checked = False
            'InitDDLBusinessUnit()
            'If Session("CompnyCode") <> CompnyCode Then
            '    cbBU.Visible = False
            '    septBU.Visible = False
            '    FilterDDLBU.Visible = False
            'Else
            '    cbBU.Visible = False
            '    septBU.Visible = False
            '    FilterDDLBU.Visible = False
            'End If
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox()
                TabContainer1.ActiveTabIndex = 1
            Else
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        cbStatus.Checked = False
        FilterText.Text = ""
        FilterDDL.SelectedIndex = 0
        FilterDDLStatus.SelectedIndex = 0
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() And Not IsInputExists() Then
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_mstprof (cmpcode, profoid, profname, profpass, activeflag, profapplimit, createuser, upduser, updtime, personoid) VALUES ('" & CompnyCode & "', '" & Tchar(profoid.Text.Trim) & "', '" & Tchar(profname.Text.Trim) & "', '" & Tchar(profpass.Text.Trim) & "', '" & Tchar(activeflag.SelectedValue) & "', " & profapplimit.Text & ", '" & Session("UserID") & "', '" & Session("UserID") & "', current_timestamp," & personoid.Text & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_mstprof SET profname='" & Tchar(profname.Text.Trim) & "', profpass='" & IIf(profpass.Text = "", Tchar(lblProfPass.Text.Trim), Tchar(profpass.Text.Trim)) & "', activeflag='" & Tchar(activeflag.SelectedValue) & "', profapplimit=" & profapplimit.Text & ", upduser='" & Session("UserID") & "', updtime=current_timestamp WHERE cmpcode='" & Session("CompnyCode") & "' AND profoid='" & Session("oid") & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                conn.Close()
            Catch ex As Exception
                objTrans.Rollback()
                conn.Close()
                showMessage(ex.ToString, 1)
                Exit Sub
            End Try
            Response.Redirect("~\Master\mstProf.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Master\mstProf.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If profoid.Text = "" Then
            showMessage("Please select profile data first!", 1)
            Exit Sub
        End If
        If DeleteData("QL_mstprof", "profoid", profoid.Text.ToString, CompnyCode) = True Then
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                sSql = "DELETE QL_mstuserrole WHERE cmpcode='" & CompnyCode & "' AND profoid='" & profoid.Text & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                sSql = "DELETE QL_approvalperson WHERE cmpcode='" & CompnyCode & "' AND approvaluser='" & profoid.Text & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                objTrans.Commit()
                conn.Close()
            Catch ex As Exception
                objTrans.Rollback()
                conn.Close()
                showMessage(ex.Message, 1)
                Exit Sub
            End Try
            Response.Redirect("~\Master\mstProf.aspx?awal=true")
        End If
    End Sub

    Protected Sub imbSearchPerson_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSearchPerson.Click
        'If IsPersonExists() Then
        BindListPerson()
        cProc.SetModalPopUpExtender(btnHidListPerson, pnlListPerson, mpeListPerson, True)
        'End If
    End Sub

    Protected Sub imbErasePerson_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbErasePerson.Click
        nip.Text = ""
        personoid.Text = ""
        FilterDDLListPerson.SelectedIndex = -1
        FilterTextListPerson.Text = ""
        gvListPerson.SelectedIndex = -1
        cbListPersonDept.Checked = False
    End Sub

    Protected Sub imbFindListPerson_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindListPerson.Click
        BindListPerson()
        mpeListPerson.Show()
    End Sub

    Protected Sub imbAllListPerson_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbAllListPerson.Click
        FilterDDLListPerson.SelectedIndex = -1
        FilterTextListPerson.Text = ""
        cbListPersonDept.Checked = False
        FilterDDLListPersonDept.SelectedIndex = -1
        BindListPerson()
        mpeListPerson.Show()
    End Sub

    Protected Sub gvListPerson_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListPerson.SelectedIndexChanged
        'nip.Text = gvListPerson.SelectedDataKey.Item("nip").ToString
        personoid.Text = gvListPerson.SelectedDataKey.Item("personoid").ToString
        profname.Text = gvListPerson.SelectedDataKey.Item("personname").ToString
        cProc.SetModalPopUpExtender(btnHidListPerson, pnlListPerson, mpeListPerson, False)
    End Sub

    Protected Sub lkbCloseListPerson_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListPerson.Click
        cProc.SetModalPopUpExtender(btnHidListPerson, pnlListPerson, mpeListPerson, False)
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
    End Sub

    Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvMst.DataSource = Session("TblMst")
            gvMst.DataBind()
        End If
    End Sub

    Protected Sub DDLCmpCode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLCmpCode.SelectedIndexChanged
        InitDDLDept()
        personoid.Text = ""
        'nip.Text = ""
        profname.Text = ""
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        Try
            report.Load(Server.MapPath(folderReport & "rptProf.rpt"))
            Dim sWhere As String = " WHERE " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
            If cbStatus.Checked Then
                sWhere &= " AND pr.activeflag='" & FilterDDLStatus.SelectedValue & "'"
            End If
            If checkPagePermission("~\Master\mstProf.aspx", Session("SpecialAccess")) = False Then
                sWhere &= " AND pr.createuser='" & Session("UserID") & "'"
            End If
            sWhere &= " ORDER BY divname, profoid"
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "ProfilePrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub
#End Region

End Class