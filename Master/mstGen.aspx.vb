Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_General
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        If gencode.Text = "" Then
            sError &= "- Please fill CODE field!<BR>"
        End If
        If gendesc.Text = "" Then
            sError &= "- Please fill DESCRIPTION field!<BR>"
        End If
        If ddlgengroup.SelectedValue = "FORMNAME" Then
            If genother1.Text = "" Then
                sError &= "- Please fill FORM URL field!<BR>"
            End If
            If ddlgenother2.SelectedValue = "" Then
                sError &= "- Please fill FORM TYPE field!<BR>"
            End If
            If ddlgenother3.SelectedValue = "" Then
                sError &= "- Please fill FORM MODULE field!<BR>"
            End If
        ElseIf ddlgengroup.SelectedValue = "CITY" Then
            If ddlgenother1.SelectedValue = "" Then
                sError &= "- Please select PROVINCE field!<BR>"
            End If
            If ddlgenother2.SelectedValue = "" Then
                sError &= "- Please select COUNTRY field!<BR>"
            End If
            If gencode.Text.Length > 2 Then
                sError &= "- Max. Code 2 Character!<BR>"
            End If
        ElseIf ddlgengroup.SelectedValue = "PROVINCE" Then
            If ddlgenother1.SelectedValue = "" Then
                sError &= "- Please select COUNTRY field!<BR>"
            End If
        ElseIf ddlgengroup.SelectedValue = "CAT1" Or ddlgengroup.SelectedValue = "CAT2" Or ddlgengroup.SelectedValue = "CAT3" Then
            Dim sSqlCheck As String = "SELECT COUNT(-1) FROM ql_mstgen WHERE gengroup='" & ddlgengroup.SelectedValue & "' AND genother1 = '" & genother1.Text & "'"
            If Session("oid") IsNot Nothing And Session("oid") <> "" Then
                sSqlCheck &= " AND genoid <> " & Session("oid")
            End If
            If cKon.ambilscalar(sSqlCheck) > 0 Then
                showMessage("Code already used!", 2)
                Exit Function
            End If
            If genother1.Text = "" Then
                sError &= "- Please insert Code Item!<BR>"
            End If
        End If
        If ddlgengroup.SelectedValue = "CUTOFDATE" Then
            Dim sErr As String = ""
            If Not IsValidDate(genother7.Text.Trim, "MM/dd/yyyy", sErr) Then
                showMessage("Tgl Cut Of Date is invalid. " & sErr & "", 2)
                Exit Function
            End If
        End If
        If ddlgengroup.SelectedValue = "ASSETTYPE" Then
            Dim sErr As String = ""
            If genother7.Text.Trim = 0 Then
                showMessage("Dep.Value Tidak Boleh = 0 " & sErr & "", 2)
                Exit Function
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputExists() As Boolean
        sSql = "SELECT COUNT(-1) FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gencode='" & Tchar(gencode.Text.ToUpper) & "' AND gendesc='" & Tchar(gendesc.Text.ToUpper) & "' AND gengroup='" & ddlgengroup.SelectedValue & "'"
        If Session("oid") <> Nothing And Session("oid") <> "" Then
            sSql &= " AND genoid<>" & Session("oid")
        End If
        If ToDecimal(GetStrData(sSql)) > 0 Then
            showMessage("CODE has been used by another data!", 2)
            Return True
        End If
        sSql = "SELECT COUNT(-1) FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gendesc='" & Tchar(gendesc.Text.ToUpper) & "' AND gengroup='" & ddlgengroup.SelectedValue & "'"
        If ddlgengroup.SelectedValue.ToUpper.Contains("HPP PERCENTAGE (%)") Then
            sSql &= " AND gengroup<>'HPP PERCENTAGE (%)'"
        End If
        If Session("oid") <> Nothing And Session("oid") <> "" Then
            sSql &= " AND genoid<>" & Session("oid")
        End If
        If ToDecimal(GetStrData(sSql)) > 0 Then
            showMessage("DESCRIPTION has been used by another data!", 2)
            Return True
        End If
        If ddlgengroup.SelectedValue.ToUpper.Contains("CATEGORY") Then
            sSql = "SELECT COUNT(-1) FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='" & ddlgengroup.SelectedValue & "' AND genother1='" & Tchar(genother1.Text) & "'"
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                sSql &= " AND genoid<>" & Session("oid")
            End If
            If ToDecimal(GetStrData(sSql)) > 0 Then
                showMessage("CATEGORY CODE has been used by another data in the same group!", 2)
                Return True
            End If
        End If
        If ddlgengroup.SelectedValue.ToUpper.Contains("HPP PERCENTAGE (%)") Then
            sSql = "SELECT COUNT(-1) FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='" & ddlgengroup.SelectedValue & "' AND genother1='" & Tchar(ddlgenother1.SelectedValue) & "' AND gendesc='" & Tchar(gendesc.Text.ToUpper) & "'"
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                sSql &= " AND genoid<>" & Session("oid")
            End If
            If ToDecimal(GetStrData(sSql)) > 0 Then
                showMessage("CATEGORY CODE has been used by another data in the same group!", 2)
                Return True
            End If
        End If
        Return False
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindMstData(ByVal sFilter As String)
        sSql = "SELECT genoid, gencode, gendesc, gengroup, genother1, genother2, genother3 FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(sFilter) & "%'"
        If cbGroup.Checked Then
            sSql &= " AND gengroup='" & FilterDDLGroup.SelectedValue & "'"
        End If
        If cbStatus.Checked Then
            If FilterDDLStatus.SelectedValue <> "ALL" Then
                sSql &= " AND activeflag='" & FilterDDLStatus.SelectedValue & "'"
            End If
        End If
        If checkPagePermission("~\Master\mstGen.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        sSql &= " ORDER BY gengroup, gencode"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_mstgen")
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub FillDDLGroup()
        sSql = "SELECT tablename, tablename FROM QL_mstoid WHERE cmpcode='" & CompnyCode & "' AND tablegroup='GENERAL'"
        'If I_U.Text = "New Data" Then
        '    sSql &= " AND tablename<>'CONTAINER TYPE'"
        'End If
        'sSql &= " ORDER BY tablename"
        FillDDL(ddlgengroup, sSql)
        ddlgengroup_SelectedIndexChanged(Nothing, Nothing)
        sSql = "SELECT tablename, tablename FROM QL_mstoid WHERE cmpcode='" & CompnyCode & "' AND tablegroup='GENERAL'"
        FillDDL(FilterDDLGroup, sSql)
    End Sub

    Private Sub FillDDLPropinsi()
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='PROVINCE' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDL(ddlgenother1, sSql)
    End Sub

    Private Sub FillDDLNegara()
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='COUNTRY' AND activeflag='ACTIVE' ORDER BY gendesc"
        If ddlgengroup.SelectedValue = "CITY" Then
            FillDDL(ddlgenother2, sSql)
        Else
            FillDDL(ddlgenother1, sSql)
        End If
    End Sub

    Private Sub FillDDLFormType()
        ddlgenother2.Items.Clear()
        ddlgenother2.Items.Add("MASTER") : ddlgenother2.Items(ddlgenother2.Items.Count - 1).Value = "MASTER"
        ddlgenother2.Items.Add("TRANSACTION") : ddlgenother2.Items(ddlgenother2.Items.Count - 1).Value = "TRANSACTION"
        ddlgenother2.Items.Add("REPORT") : ddlgenother2.Items(ddlgenother2.Items.Count - 1).Value = "REPORT"
        ddlgenother2.SelectedIndex = -1
    End Sub

    Private Sub FillDDLModule()
        sSql = "SELECT tablename, tablename FROM QL_mstoid WHERE cmpcode='" & CompnyCode & "' AND tablegroup='MODULE'"
        FillDDL(ddlgenother3, sSql)
    End Sub

    Private Sub FillDDLBusUnit()
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        FillDDL(ddlgenother1, sSql)
    End Sub

    Private Sub FillTextBox()
        Try
            sSql = "SELECT genoid, gencode, gendesc, gengroup, ISNULL(genother1, '') AS genother1, ISNULL(genother2, '') AS genother2, ISNULL(genother3, '') AS genother3, ISNULL(genother4, 0) AS genother4, ISNULL(genother5, '') AS genother5, ISNULL(genother6, '') AS genother6, activeflag, createuser, createtime, upduser, updtime FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND genoid=" & Session("oid")
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            btnDelete.Visible = False
            While xreader.Read
                genoid.Text = xreader("genoid").ToString
                gencode.Text = xreader("gencode").ToString
                gendesc.Text = xreader("gendesc").ToString
                ddlgengroup.SelectedValue = xreader("gengroup").ToString
                ddlgengroup_SelectedIndexChanged(Nothing, Nothing)
                genother1.Text = xreader("genother1").ToString
                ddlgenother1.SelectedValue = xreader("genother1").ToString
                genother2.Text = xreader("genother2").ToString
                ddlgenother2.SelectedValue = xreader("genother2").ToString
                genother3.Text = xreader("genother3").ToString
                ddlgenother3.SelectedValue = xreader("genother3").ToString
                genother4.Text = xreader("genother4").ToString
                genother5.Text = xreader("genother5").ToString
                genother6.Text = xreader("genother6").ToString
                activeflag.SelectedValue = xreader("activeflag")
                upduser.Text = xreader("createuser").ToString
                updtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
                If ddlgengroup.SelectedValue = "CUTOFDATE" Then
                    genother7.Text = xreader("genother1").ToString
                    lbl7.Visible = True
                ElseIf ddlgengroup.SelectedValue = "ASSETTYPE" Then
                    genother7.Text = xreader("genother5").ToString
                End If
            End While
        Catch ex As Exception
            showMessage(ex.Message, 1)
        Finally
            xreader.Close()
            conn.Close()
            btnDelete.Visible = True
            gencode.ReadOnly = True
            gencode.CssClass = "inpTextDisabled"
            If ddlgengroup.SelectedValue = "GROUPITEM" Then
                gendesc.ReadOnly = True
                gendesc.CssClass = "inpTextDisabled"
            End If
        End Try
    End Sub

    Private Sub PrintReport(ByVal sOid As String)
        Try
            report.Load(Server.MapPath(folderReport & "rptGeneral.rpt"))
            Dim sWhere As String
            sWhere = " WHERE cmpcode='" & CompnyCode & "'"
            If cbGroup.Checked Then
                sWhere &= " AND gengroup='" & FilterDDLGroup.SelectedValue & "'"
            End If
            If cbStatus.Checked Then
                If FilterDDLStatus.SelectedValue <> "ALL" Then
                    sWhere &= " AND activeflag='" & FilterDDLStatus.SelectedValue & "'"
                End If
            End If
            If sOid = "" Then
                sWhere &= " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If checkPagePermission("~\Master\mstGen.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND createuser='" & Session("UserID") & "'"
                End If
            Else
                sWhere &= " AND genoid=" & sOid
            End If
            sWhere &= " ORDER BY gengroup, gencode, gendesc"
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "GeneralPrintOut_" & Format(GetServerTime(), "yyyyMMddHHmmss"))
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
        Response.Redirect("~\Master\mstgen.aspx?awal=true")
    End Sub

    Private Sub FillDDLCategory2()
        sSql = "SELECT DISTINCT c2.cat2res2, cat2res2 FROM QL_mstcat2 c2 WHERE cmpcode='" & CompnyCode & "' AND cat2res1='FG' ORDER BY c2.cat2res2"
        FillDDL(ddlgenother1, sSql)
    End Sub

    Private Sub FillDDLWarehouse()
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='WAREHOUSE' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDL(ddlgenother1, sSql)
    End Sub

#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Master\mstGen.aspx")
        End If
        If checkPagePermission("~\Master\mstGen.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - General"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            I_U.Text = "New Data"
        Else
            I_U.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            cbGroup.Checked = False
            FillDDLGroup()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox()
                TabContainer1.ActiveTabIndex = 1
            Else
                btnDelete.Visible = False
                btnPrint2.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindMstData(FilterText.Text.Trim)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        cbGroup.Checked = False
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        FilterDDLGroup.SelectedIndex = -1
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        BindMstData("")
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        PrintReport("")
    End Sub

    Protected Sub btnPrint2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint2.Click
        PrintReport(genoid.Text)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim cProv As String = ""
        If IsInputValid() Then
            If Not IsInputExists() Then
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    If ddlgengroup.SelectedValue = "FORMNAME" Then
                        genother4.Text = CStr(GetStrData("SELECT MAX(genother4) FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND genother2='" & ddlgenother2.SelectedValue & "' AND genother3='" & ddlgenother3.SelectedValue & "'").ToString)
                        genother4.Text = ToDecimal(genother4.Text) + 1
                    Else
                        genother4.Text = "0"
                    End If
                End If
                If ddlgengroup.SelectedValue = "CITY" Then
                    sSql = " SELECT gencode FROM QL_mstgen WHERE gengroup='PROVINCE' and genoid=" & ddlgenother1.SelectedValue & ""
                    gencode.Text = GetStrData(sSql) & "" & gencode.Text
                End If
                Dim objTrans As SqlClient.SqlTransaction
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If
                objTrans = conn.BeginTransaction()
                xCmd.Transaction = objTrans
                genoid.Text = GenerateID("QL_mstgen", CompnyCode)
                Try
                    If Session("oid") = Nothing Or Session("oid") = "" Then
                        sSql = "INSERT INTO QL_mstgen (cmpcode, genoid, gencode, gendesc, gengroup, genother1, genother2, genother3, genother4, genother5, genother6, activeflag, createuser, createtime, upduser, updtime) VALUES('" & CompnyCode & "', " & genoid.Text & ", '" & Tchar(gencode.Text.Trim) & "', '" & Tchar(gendesc.Text.Trim) & "', '" & Tchar(ddlgengroup.SelectedValue) & "', '" & IIf(ddlgenother1.Visible, Tchar(ddlgenother1.SelectedValue), Tchar(genother1.Text.Trim)) & "', '" & IIf(ddlgenother2.Visible, Tchar(ddlgenother2.SelectedValue), Tchar(genother2.Text.Trim)) & "', '" & IIf(ddlgenother3.Visible, Tchar(ddlgenother3.SelectedValue), Tchar(genother3.Text.Trim)) & "', " & genother4.Text & ", '" & Tchar(genother5.Text) & "', '" & Tchar(genother6.Text) & "', '" & Tchar(activeflag.SelectedValue) & "', '" & Session("UserID") & "', current_timestamp, '" & Session("UserID") & "', current_timestamp)"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()

                        sSql = "UPDATE QL_mstoid SET lastoid=" & genoid.Text & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_mstgen'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Else
                        sSql = "UPDATE QL_mstgen SET gencode='" & Tchar(gencode.Text.Trim) & "', gendesc='" & Tchar(gendesc.Text.Trim) & "', gengroup='" & Tchar(ddlgengroup.SelectedValue) & "', genother1='" & IIf(ddlgenother1.Visible, Tchar(ddlgenother1.SelectedValue), Tchar(genother1.Text.Trim)) & "', genother2='" & IIf(ddlgenother2.Visible, Tchar(ddlgenother2.SelectedValue), Tchar(genother2.Text.Trim)) & "', genother3='" & IIf(ddlgenother3.Visible, Tchar(ddlgenother3.SelectedValue), Tchar(genother3.Text.Trim)) & "', genother4='" & genother4.Text & "', genother5='" & Tchar(genother5.Text) & "', genother6='" & Tchar(genother6.Text) & "', activeflag='" & Tchar(activeflag.SelectedValue) & "', upduser='" & Session("UserID") & "', updtime=current_timestamp WHERE cmpcode LIKE '%" & CompnyCode & "%' AND genoid=" & Session("oid")
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()

                        If ddlgengroup.SelectedValue = "CUTOFDATE" Then
                            sSql = "UPDATE QL_mstgen SET genother1='" & genother7.Text.Trim & "' WHERE cmpcode='" & CompnyCode & "' AND genoid=" & Session("oid")
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        ElseIf ddlgengroup.SelectedValue = "ASSETTYPE" Then
                            sSql = "UPDATE QL_mstgen SET genother5='" & genother7.Text.Trim & "' WHERE cmpcode='" & CompnyCode & "' AND genoid=" & Session("oid")
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                    End If

                    If ddlgengroup.SelectedValue = "CUTOFDATE" Then
                        sSql = "UPDATE QL_mstgen SET genother1='" & genother7.Text.Trim & "' WHERE cmpcode='" & CompnyCode & "' AND genoid=" & genoid.Text
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    ElseIf ddlgengroup.SelectedValue = "ASSETTYPE" Then
                        sSql = "UPDATE QL_mstgen SET genother5='" & genother7.Text.Trim & "' WHERE cmpcode='" & CompnyCode & "' AND genoid=" & genoid.Text
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If

                    objTrans.Commit()
                    conn.Close()
                Catch ex As Exception
                    objTrans.Rollback()
                    conn.Close()
                    showMessage(ex.Message, 1)
                    Exit Sub
                End Try
                Session("oid") = Nothing
                Response.Redirect("~\Master\mstGen.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Master\mstGen.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If genoid.Text = "" Then
            showMessage("Please select general data first!", 1)
            Exit Sub
        End If
        'sSql = "SELECT tblusage, colusage FROM QL_oidusage WHERE cmpcode='" & CompnyCode & "' AND tblname='QL_MSTGEN'"
        'Dim objTblUsage As DataTable = cKon.ambiltabel(sSql, "QL_oidusage")
        'Dim sColomnName(objTblUsage.Rows.Count - 1) As String
        'Dim sTable(objTblUsage.Rows.Count - 1) As String
        'For c1 As Integer = 0 To objTblUsage.Rows.Count - 1
        '    sColomnName(c1) = objTblUsage.Rows(c1).Item("colusage").ToString
        '    sTable(c1) = objTblUsage.Rows(c1).Item("tblusage").ToString
        'Next
        'If CheckDataExists(genoid.Text, sColomnName, sTable) = True Then
        '    showMessage("This data can't be deleted because it is being used by another data!", 2)
        '    Exit Sub
        'End If
        If DeleteData("QL_mstgen", "genoid", genoid.Text, CompnyCode) = True Then
            Response.Redirect("~\Master\mstGen.aspx?awal=true")
        End If
    End Sub

    Protected Sub ddlgengroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlgengroup.SelectedIndexChanged
        lblLain1.Visible = False : lblLain2.Visible = False : lblLain3.Visible = False : lblLain4.Visible = False : lblLain5.Visible = False : lblLain6.Visible = False
        lblLain1.Text = "Another 1" : lblLain2.Text = "Another 2" : lblLain3.Text = "Another 3" : lblLain4.Text = "Another 4" : lblLain5.Text = "Another 5" : lblLain6.Text = "Another 6"
        TDAnother1.Visible = False : TDAnother2.Visible = False : TDAnother3.Visible = False : TDAnother4.Visible = False : TDAnother5.Visible = False : TDAnother6.Visible = False
        genother1.Visible = False : genother2.Visible = False : genother3.Visible = False : genother4.Visible = False : genother5.Visible = False : genother6.Visible = False : genother7.Visible = False
        genother1.Text = "" : genother2.Text = "" : genother3.Text = "" : genother4.Text = "" : genother5.Text = "" : genother6.Text = ""
        ddlgenother1.Visible = False : ddlgenother2.Visible = False : ddlgenother3.Visible = False
        ddlgenother1.Items.Clear() : ddlgenother2.Items.Clear() : ddlgenother3.Items.Clear()

        If ddlgengroup.SelectedValue = "FORMNAME" Then
            lblLain1.Visible = True : lblLain2.Visible = True : lblLain3.Visible = True : lblLain5.Visible = True : lblLain6.Visible = True
            lblLain1.Text = "Form URL" : lblLain2.Text = "Form Type" : lblLain3.Text = "Form Module" : lblLain5.Text = "Menu Text" : lblLain6.Text = "Menu Icon"
            genother1.Visible = True : genother5.Visible = True : genother6.Visible = True
            TDAnother1.Visible = True : TDAnother2.Visible = True : TDAnother3.Visible = True : TDAnother5.Visible = True : TDAnother6.Visible = True : genother1.MaxLength = 50 : genother1.Text = ""
            ddlgenother2.Visible = True : ddlgenother3.Visible = True
            FillDDLFormType()
            FillDDLModule()
            btnSave.Visible = True
        ElseIf ddlgengroup.SelectedValue = "CITY" Then
            lblLain1.Visible = True : lblLain2.Visible = True
            lblLain1.Text = "Province" : lblLain2.Text = "Country"
            ddlgenother1.Visible = True : ddlgenother2.Visible = True
            TDAnother1.Visible = True : TDAnother2.Visible = True
            FillDDLPropinsi()
            FillDDLNegara()
            btnSave.Visible = True
        ElseIf ddlgengroup.SelectedValue = "PROVINCE" Then
            lblLain1.Visible = True
            lblLain1.Text = "Country"
            ddlgenother1.Visible = True
            TDAnother1.Visible = True
            FillDDLNegara()
            btnSave.Visible = True
        ElseIf ddlgengroup.SelectedValue = "HPP PERCENTAGE (%)" Then
            lblLain1.Visible = True
            lblLain1.Text = "Category 2 Finish Good"
            ddlgenother1.Visible = True
            TDAnother1.Visible = True
            FillDDLCategory2()
            btnSave.Visible = True
        ElseIf ddlgengroup.SelectedValue = "CUTOFDATE" Then
            lblLain6.Visible = True
            lblLain6.Text = "Tgl Cut Off"
            TDAnother6.Visible = True
            genother7.Visible = True
            genother7.Text = ""
            btnSave.Visible = True
        ElseIf ddlgengroup.SelectedValue = "ADJUSMENT REASON" Then
            lblLain1.Visible = True : TDAnother1.Visible = True
            lblLain1.Text = "COA ADJ Stock"
            ddlgenother1.Visible = True
            FillDDLAcctg(ddlgenother1, "VAR_STOCK_ADJUSTMENT", CompnyCode, "")
            btnSave.Visible = True
        ElseIf ddlgengroup.SelectedValue = "GROUPITEM" Then
            lblLain1.Visible = True : lblLain2.Visible = True
            lblLain1.Text = "COA Persediaan" : lblLain2.Text = "COA HPP"
            TDAnother1.Visible = True : TDAnother2.Visible = True
            ddlgenother1.Visible = True
            FillDDLAcctg(ddlgenother1, "VAR_PERSEDIAAN", CompnyCode, "")
            ddlgenother2.Visible = True
            FillDDLAcctg(ddlgenother2, "VAR_HPP", CompnyCode, "")
            sSql = "SELECT COUNT(-1) FROM ql_mstgen WHERE gengroup like 'GROUPITEM%'"
            Dim cekGroup As Integer = cKon.ambilscalar(sSql)
            If cekGroup > 3 Then
                If I_U.Text = "New Data" Then
                    btnSave.Visible = False
                Else
                    btnSave.Visible = True
                    ddlgengroup.Enabled = False
                    ddlgengroup.CssClass = "inpTextDisabled"
                End If
            End If
        ElseIf ddlgengroup.SelectedValue = "ASSETTYPE" Then
            lblLain1.Visible = True : lblLain2.Visible = True : lblLain3.Visible = True : lblLain6.Visible = True
            lblLain1.Text = "COA Asset" : lblLain2.Text = "COA Accum" : lblLain3.Text = "COA Dep_Expense"
            lblLain6.Text = "Dep. Value"
            TDAnother1.Visible = True : TDAnother2.Visible = True : TDAnother3.Visible = True : TDAnother6.Visible = True
            ddlgenother1.Visible = True
            FillDDLAcctg(ddlgenother1, "VAR_ASSET", CompnyCode, "")
            ddlgenother2.Visible = True
            FillDDLAcctg(ddlgenother2, "VAR_ASSET_ACCUM", CompnyCode, "")
            ddlgenother3.Visible = True
            FillDDLAcctg(ddlgenother3, "VAR_ACCUM_DEP_EXPENSE", CompnyCode, "")
            genother7.Visible = True
            genother7.Text = ""
            btnSave.Visible = True
        ElseIf ddlgengroup.SelectedValue = "WH SISA PROD" Then
            lblLain1.Visible = True : TDAnother1.Visible = True
            lblLain1.Text = "Warehouse"
            ddlgenother1.Visible = True
            FillDDL(ddlgenother1, "SELECT genoid, gendesc FROM QL_mstgen WHERE gengroup='WAREHOUSE' AND activeflag='ACTIVE' ORDER BY gendesc")
            btnSave.Visible = True
        ElseIf ddlgengroup.SelectedValue = "GUDANG PRODUKSI" Then
            lblLain1.Visible = True : TDAnother1.Visible = True
            lblLain1.Text = "Warehouse"
            ddlgenother1.Visible = True
            FillDDLWarehouse()
            btnSave.Visible = True
        Else
            btnSave.Visible = True
        End If
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
    End Sub

    Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvMst.DataSource = Session("TblMst")
            gvMst.DataBind()
        End If
    End Sub
#End Region

End Class