Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_Category3
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        If cat1oid.SelectedValue = "" Then
            sError &= "- Please select CATEGORY 1 field!<BR>"
        End If
        If cat2oid.SelectedValue = "" Then
            sError &= "- Please select CATEGORY 2 field!<BR>"
        End If
        If cat3code.Text.Trim = "" Then
            sError &= "- Please fill CODE field!"
        Else
            If cat3code.Text.Length <> 1 Then
                sError &= "- Length of CODE must be 1 characters!"
            End If
        End If
        If cat3shortdesc.Text.Trim = "" Then
            sError &= "<BR>- Please fill DESCRIPTION field!"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsCodeExists() As Boolean
        sSql = "SELECT COUNT(*) FROM QL_mstcat3 WHERE cmpcode='" & CompnyCode & "' AND cat3code='" & Tchar(cat3code.Text) & "' AND cat1oid=" & cat1oid.SelectedValue & " AND cat2oid=" & cat2oid.SelectedValue & " AND cat3res1='" & cat3res1.SelectedValue & "'"
        If Not Session("oid") Is Nothing And Session("oid") <> "" Then
            sSql &= " AND cat3oid<>" & Session("oid")
        End If
        'If ToDouble(GetStrData(sSql).ToString) > 0 Then
            'showMessage("Code have been used by another data. Please fill another Code!", 2)
            'Return True
        'End If
        Return False
    End Function

    Private Function IsDescExists() As Boolean
        sSql = "SELECT COUNT(*) FROM QL_mstcat3 WHERE cmpcode='" & CompnyCode & "' AND cat3shortdesc='" & Tchar(cat3shortdesc.Text) & "' AND cat1oid=" & cat1oid.SelectedValue & " AND cat2oid=" & cat2oid.SelectedValue & " AND cat3code='" & Tchar(cat3code.Text) & "' AND cat3res1='" & cat3res1.SelectedValue & "'"
        If Not Session("oid") Is Nothing And Session("oid") <> "" Then
            sSql &= " AND cat3oid<>" & Session("oid")
        End If
        If ToDouble(GetStrData(sSql).ToString) > 0 Then
            showMessage("Description have been used by another data. Please fill another Description!", 2)
            Return True
        End If
        Return False
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Public Sub InitAllDDL()
        sSql = "select gencode,gendesc from QL_mstgen where gengroup='GROUPITEM' AND gencode='FG' order by gendesc"
        FillDDL(cat3res1, sSql)
        FillDDL(FilterDDLType, sSql)
    End Sub

    Private Sub InitDDLCat1()
        'Fill DDL Category1
        sSql = "SELECT cat1oid, cat1code+' - '+cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat1res1='" & cat3res1.SelectedValue & "' order by cat1shortdesc"
        FillDDL(cat1oid, sSql)
        If cat1oid.Items.Count > 0 Then
            InitDDLCat2()
        End If
    End Sub

    Private Sub InitDDLCat2()
        'Fill DDL Category2
        sSql = "SELECT cat2oid, cat2res2+cat2code+' - '+cat2shortdesc FROM QL_mstcat2 cat2 INNER JOIN QL_mstcat1 cat1 on cat1.cat1oid=cat2.cat1oid WHERE cat2.cmpcode='" & CompnyCode & "' AND cat2.cat1oid=" & cat1oid.SelectedValue & " AND cat2.activeflag='ACTIVE' order by cat2shortdesc"
        FillDDL(cat2oid, sSql)
    End Sub

    Private Sub InitFilterDDLCat1()
        'Fill DDL Category1
        sSql = "SELECT cat1oid, cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat1res1='" & FilterDDLType.SelectedValue & "' order by cat1shortdesc"
        FillDDL(FilterDDLCat01, sSql)
        If FilterDDLCat01.Items.Count > 0 Then
            InitFilterDDLCat2()
        End If
    End Sub

    Private Sub InitFilterDDLCat2()
        'Fill DDL Category2
        sSql = "SELECT cat2oid, cat2shortdesc FROM QL_mstcat2 cat2 INNER JOIN QL_mstcat1 cat1 on cat1.cat1oid=cat2.cat1oid AND cat1res1='" & FilterDDLType.SelectedValue & "' WHERE cat2.cmpcode='" & CompnyCode & "' AND cat2.cat1oid=" & FilterDDLCat01.SelectedValue & " AND cat2res1='" & FilterDDLType.SelectedValue & "' AND cat2.activeflag='ACTIVE' order by cat2shortdesc"
        FillDDL(FilterDDLCat02, sSql)
    End Sub

    Private Sub BindMstData(ByVal sFilter As String)
        sSql = "SELECT cat3oid, cat1.cat1shortdesc,cat2.cat2shortdesc, cat3code, cat3shortdesc, cat3longdesc, cat3.activeflag, cat3note, cat3res1 FROM QL_mstcat3 cat3 INNER JOIN QL_mstcat1 cat1 ON cat3.cat1oid = cat1.cat1oid INNER JOIN QL_mstcat2 cat2 ON cat3.cat2oid = cat2.cat2oid WHERE cat3.cmpcode='" & CompnyCode & "' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(sFilter) & "%'"
        If cbType.Checked Then
            sSql &= " AND cat3res1='" & FilterDDLType.SelectedValue & "'"
        End If
        If cbCat01.Checked Then
            If FilterDDLCat01.SelectedValue <> "" Then
                sSql &= " AND cat3.cat1oid=" & FilterDDLCat01.SelectedValue
            End If
        End If
        If cbCat02.Checked Then
            If FilterDDLCat02.SelectedValue <> "" Then
                sSql &= " AND cat3.cat2oid=" & FilterDDLCat02.SelectedValue
            End If
        End If
        If cbStatus.Checked Then
            If FilterDDLStatus.SelectedValue <> "ALL" Then
                sSql &= " AND cat3.activeflag='" & FilterDDLStatus.SelectedValue & "'"
            End If
        End If
        If checkPagePermission("~\Master\mstCat3.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND cat3.createuser='" & Session("UserID") & "'"
        End If
        sSql &= " ORDER BY cat3code"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_mstCat3")
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub FillTextBox()
        Try
            sSql = "SELECT cat3.cat3oid, cat3.cat2oid, cat3.cat1oid, cat3.cat3code, cat2.cat2code, cat1.cat1code, cat3.cat3shortdesc, cat3longdesc, cat3.activeflag, cat3note, cat3.createuser, cat3.createtime, cat3.upduser, cat3.updtime, cat3res1 FROM QL_mstcat3 cat3 INNER JOIN QL_mstcat2 cat2 ON cat3.cat2oid=cat2.cat2oid INNER JOIN QL_mstcat1 cat1 ON cat3.cat1oid=cat1.cat1oid WHERE cat3.cmpcode='" & CompnyCode & "' AND cat3.cat3oid=" & Session("oid")
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            btnDelete.Visible = False
            While xreader.Read
                cat3oid.Text = xreader("cat3oid").ToString
                cat3res1.SelectedValue = xreader("cat3res1").ToString
                InitDDLCat1()
                cat1oid.SelectedValue = xreader("cat1oid").ToString
                cat1code.Text = xreader("cat1code").ToString
                InitDDLCat2()
                cat2oid.SelectedValue = xreader("cat2oid").ToString
                cat2code.Text = xreader("cat2code").ToString
                cat3code.Text = xreader("cat3code").ToString
                cat3shortdesc.Text = xreader("cat3shortdesc").ToString
                cat3longdesc.Text = xreader("cat3longdesc").ToString
                activeflag.SelectedValue = xreader("activeflag").ToString
                cat3note.Text = xreader("cat3note").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
        Finally
            btnDelete.Visible = True
            cat1oid.CssClass = "inpTextDisabled"
            cat1oid.Enabled = False
            cat2oid.CssClass = "inpTextDisabled"
            cat2oid.Enabled = False
            cat3code.CssClass = "inpTextDisabled"
            cat3code.Enabled = False
            cat3res1.CssClass = "inpTextDisabled"
            cat3res1.Enabled = False
        End Try
    End Sub

    Private Sub PrintReport()
        Try
            report.Load(Server.MapPath(folderReport & "rptCat3.rpt"))
            Dim sWhere As String = " WHERE cat3.cmpcode='" & CompnyCode & "' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
            If cbType.Checked Then
                sWhere &= " AND cat3res1='" & FilterDDLType.SelectedValue & "'"
            End If
            If cbCat01.Checked Then
                If FilterDDLCat01.SelectedValue <> "" Then
                    sWhere &= " AND cat3.cat1oid=" & FilterDDLCat01.SelectedValue
                End If
            End If
            If cbCat02.Checked Then
                If FilterDDLCat02.SelectedValue <> "" Then
                    sWhere &= " AND cat3.cat2oid=" & FilterDDLCat02.SelectedValue
                End If
            End If
            If cbStatus.Checked Then
                If FilterDDLStatus.SelectedValue <> "ALL" Then
                    sWhere &= " AND cat3.activeflag='" & FilterDDLStatus.SelectedValue & "'"
                End If
            End If
            If checkPagePermission("~\Master\mstCat3.aspx", Session("SpecialAccess")) = False Then
                sWhere &= " AND cat3.createuser='" & Session("UserID") & "'"
            End If
            sWhere &= " ORDER BY cat3code"
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "Category3Report_" & Format(GetServerTime(), "yyyyMMddHHmmss"))
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Master\mstCat3.aspx")
        End If
        If checkPagePermission("~\Master\mstCat3.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Category 3"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            I_U.Text = "New Data"
        Else
            I_U.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        If Not Page.IsPostBack Then
            InitAllDDL()
            InitDDLCat1()
            InitFilterDDLCat1()
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox()
                TabContainer1.ActiveTabIndex = 1
            Else
                cat3oid.Text = GenerateID("QL_MSTCAT3", CompnyCode)
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub FilterDDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLType.SelectedIndexChanged
        InitFilterDDLCat1()
    End Sub

    Protected Sub FilterDDLCat01_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat01.SelectedIndexChanged
        InitFilterDDLCat2()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        cbType.Checked = False
        FilterDDLType.SelectedIndex = -1
        FilterDDLType_SelectedIndexChanged(Nothing, Nothing)
        cbCat01.Checked = False
        FilterDDLCat01.SelectedIndex = -1
        cbCat02.Checked = False
        FilterDDLCat02.SelectedIndex = -1
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
    End Sub

    Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvMst.DataSource = Session("TblMst")
            gvMst.DataBind()
        End If
    End Sub

    Protected Sub cat3res1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cat3res1.SelectedIndexChanged
        InitDDLCat1()
    End Sub

    Protected Sub cat1oid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cat1oid.SelectedIndexChanged
        InitDDLCat2()
        cat1code.Text = GetStrData("SELECT cat1code FROM QL_mstcat1 WHERE cat1oid=" & cat1oid.SelectedValue)
    End Sub

    Protected Sub cat2oid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cat2oid.SelectedIndexChanged
        cat2code.Text = GetStrData("SELECT cat2code FROM QL_mstcat2 WHERE cat2oid=" & cat2oid.SelectedValue)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            If IsCodeExists() Then
                Exit Sub
            End If
            If IsDescExists() Then
                Exit Sub
            End If
            If Session("oid") = Nothing Or Session("oid") = "" Then
                cat3oid.Text = GenerateID("QL_MSTCAT3", CompnyCode)
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_mstcat3 (cmpcode, cat3oid, cat2oid, cat1oid, cat3code, cat3shortdesc, cat3longdesc, activeflag, cat3note, cat3res1, createuser, createtime, upduser, updtime) VALUES ('" & CompnyCode & "', " & cat3oid.Text & ", " & cat2oid.SelectedValue & ", " & cat1oid.SelectedValue & ", '" & Tchar(cat3code.Text) & "', '" & Tchar(cat3shortdesc.Text) & "', '" & Tchar(cat3longdesc.Text) & "', '" & activeflag.SelectedValue & "', '" & Tchar(cat3note.Text) & "', '" & cat3res1.SelectedValue & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & cat3oid.Text & " WHERE tablename LIKE 'QL_MSTCAT3' AND cmpcode LIKE '%" & CompnyCode & "%'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_mstcat3 SET cat1oid=" & cat1oid.SelectedValue & ", cat2oid=" & cat2oid.SelectedValue & ", cat3code='" & Tchar(cat3code.Text) & "', cat3shortdesc='" & Tchar(cat3shortdesc.Text) & "', cat3longdesc='" & Tchar(cat3longdesc.Text) & "', activeflag='" & activeflag.SelectedValue & "', cat3note='" & Tchar(cat3note.Text) & "', cat3res1='" & cat3res1.SelectedValue & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & CompnyCode & "' AND cat3oid=" & cat3oid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                conn.Close()
            Catch ex As Exception
                objTrans.Rollback()
                conn.Close()
                showMessage(ex.Message, 1)
                Exit Sub
            End Try
            Response.Redirect("~\Master\mstCat3.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Master\mstCat3.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If cat2oid.Text = "" Then
            showMessage("Please select Category 3 first!", 1)
            Exit Sub
        End If
        'cek table mstitem
        If CheckDataExists("SELECT itemoid FROM QL_mstitem WHERE itemCat3 =" & cat3oid.Text) = True Then
            showMessage("KATEGORI TIDAK DAPAT DIHAPUS", 2)
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE QL_mstcat3 WHERE cat3oid='" & cat3oid.Text & "' and cmpcode='" & CompnyCode & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try

        If DeleteData("QL_mstcat3", "cat3oid", cat3oid.Text, CompnyCode) = True Then
            Response.Redirect("~\Master\mstCat3.aspx?awal=true")
        End If

        'If cat3res1.SelectedValue = "Raw" Then
        '    sSql = "SELECT COUNT(*) FROM QL_mstmatraw WHERE cmpcode='" & CompnyCode & "' AND SUBSTRING(matrawcode, 1, 11)='" & Tchar(cat1code.Text) & "." & Tchar(cat2code.Text) & "." & Tchar(cat3code.Text) & "'"
        'ElseIf cat3res1.SelectedValue = "General" Then
        '    sSql = "SELECT COUNT(*) FROM QL_mstmatgen WHERE cmpcode='" & CompnyCode & "' AND SUBSTRING(matgencode, 1, 11)='" & Tchar(cat1code.Text) & "." & Tchar(cat2code.Text) & "." & Tchar(cat3code.Text) & "'"
        'ElseIf cat3res1.SelectedValue = "Spare Part" Then
        '    sSql = "SELECT COUNT(*) FROM QL_mstsparepart WHERE cmpcode='" & CompnyCode & "' AND SUBSTRING(sparepartcode, 1, 11)='" & Tchar(cat1code.Text) & "." & Tchar(cat2code.Text) & "." & Tchar(cat3code.Text) & "'"
        'Else
        '    sSql = "SELECT COUNT(*) FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND SUBSTRING(itemcode, 1, 11)='" & Tchar(cat1code.Text) & "." & Tchar(cat2code.Text) & "." & Tchar(cat3code.Text) & "'"
        'End If
        'Dim sCol() As String = {"cat3oid"}
        'Dim sTbl() As String = {"QL_mstcat4"}
        'If CheckDataExists(sSql) = True Or CheckDataExists(CInt(cat3oid.Text), sCol, sTbl) = True Then
        '    showMessage("This data can't be deleted because it is being used by another data!", 2)
        '    Exit Sub
        'End If
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        PrintReport()
    End Sub
#End Region

End Class
