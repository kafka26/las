Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_DirectLaborCost
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetMaxDLCPct() As Double
        sSql = "SELECT TOP 1 gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='MAX DLC PERCENTAGE' AND activeflag='ACTIVE' ORDER BY updtime DESC"
        Return ToDouble(GetStrData(sSql))
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If deptoid.SelectedValue = "" Then
            sError &= "- Please select DEPARTMENT field!<BR>"
        End If
        If dlcpct.Text = "" Then
            sError &= "- Please fill COST % field!<BR>"
        Else
            'If ToDouble(dlcpct.Text) <= 0 Then
            '    sError &= "- COST % field must be more than 0!<BR>"
            'Else
            If (ToDouble(dlcpct.Text) + ToDouble(dlctotalpct.Text) - ToDouble(lastpct.Text)) > GetMaxDLCPct() Then
                sError &= "- TOTAL COST % must be less or equal than 100%!<BR>"
            End If
            'End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If bomoid.Text = "" Then
            sError &= "- Please select BOM field!<BR>"
        End If
        If dlcdate.Text = "" Then
            sError &= "- Please fill DATE field!<BR>"
            sErr = "None"
        Else
            If Not IsValidDate(dlcdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        Dim sErrReply As String = ""
        If dlcamount.Text = "" Then
            sError &= "- Please fill DL COST field!<BR>"
        Else
            If ToDouble(dlcamount.Text) <= 0 Then
                sError &= "- DL COST field must be more than 0!<BR>"
            Else
                If Not isLengthAccepted("dlcamount", "QL_mstdlc", ToDouble(dlcamount.Text), sErrReply) Then
                    sError &= "- DL COST field must be less than MAX DL COST (" & sErrReply & ") allowed stored in database!<BR>"
                End If
            End If
        End If
        If dlcoverheadamt.Text = "" Then
            sError &= "- Please fill OHD COST field!<BR>"
        Else
            If ToDouble(dlcoverheadamt.Text) <= 0 Then
                sError &= "- OHD COST field must be more than 0!<BR>"
            Else
                If Not isLengthAccepted("dlcoverheadamt", "QL_mstdlc", ToDouble(dlcoverheadamt.Text), sErrReply) Then
                    sError &= "- OHD COST field must be less than MAX OHD COST (" & sErrReply & ") allowed stored in database!<BR>"
                End If
            End If
        End If
        If dlcacctgoid.SelectedValue = "" Then
            sError &= "- Please select DLC COA field!<BR>"
        End If
        If ohdacctgoid.SelectedValue = "" Then
            sError &= "- Please select OHD COA field!<BR>"
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill Detail Data!<BR>"
        Else
            Dim dtTbl As DataTable = Session("TblDtl")
            If dtTbl.Rows.Count <= 0 Then
                sError &= "- Please fill Detail Data!<BR>"
            Else
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    If Not isLengthAccepted("dlcdtlamount", "QL_mstdlcdtl", ToDouble(dtTbl.Rows(C1)("dlcdtlamount").ToString), sErrReply) Then
                        sError &= "- DL COST AMT. for every detail data must be less than MAX DL COST AMT. (" & sErrReply & ") allowed stored in database!<BR>"
                        Exit For
                    End If
                    If Not isLengthAccepted("dlcohdamount", "QL_mstdlcdtl", ToDouble(dtTbl.Rows(C1)("dlcohdamount").ToString), sErrReply) Then
                        sError &= "- OHD COST AMT. for every detail data must be less than MAX OHD COST AMT. (" & sErrReply & ") allowed stored in database!<BR>"
                        Exit For
                    End If
                Next
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitAllDDL()
        ' Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        FillDDL(DDLBusUnit, sSql)
        InitDDLDept()
        InitDDLCOA()
        'Fill DDL Unit DLCOHD
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE activeflag='ACTIVE' ORDER BY gendesc"
        FillDDL(DDLUnitDL, sSql)
        FillDDL(DDLUnitOHD, sSql)
    End Sub

    Private Sub InitDDLDept()
        ' Fill DDL Department
        If DDLBusUnit.SelectedValue <> "" Then
            If bomoid.Text <> "" Then
                sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE deptoid IN (SELECT bomdtl1deptoid FROM QL_mstbomdtl1 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND bomdtl1reftype='FG' AND bomoid=" & bomoid.Text & ")"
                FillDDL(deptoid, sSql)
            Else
                deptoid.Items.Clear()
            End If
        Else
            deptoid.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCOA()
        If DDLBusUnit.SelectedValue <> "" Then
            FillDDLAcctg(dlcacctgoid, "VAR_DIRECT_LABOR_COST", DDLBusUnit.SelectedValue)
            FillDDLAcctg(ohdacctgoid, "VAR_OVERHEAD_COST", DDLBusUnit.SelectedValue)
        Else
            dlcacctgoid.Items.Clear()
            ohdacctgoid.Items.Clear()
        End If
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT dlc.dlcoid, CONVERT(VARCHAR(10), dlc.dlcdate, 101) AS dlcdate, dlc.dlcdesc, dlc.activeflag, dlc.dlcnote, bom.bomdesc, 'False' AS checkvalue, i.itemlongdescription AS itemlongdesc, i.itemcode FROM QL_mstdlc dlc INNER JOIN QL_mstbom bom ON bom.cmpcode=dlc.cmpcode AND bom.bomoid=dlc.bomoid INNER JOIN QL_mstitem i ON i.itemoid=bom.itemoid WHERE"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " dlc.cmpcode='" & Session("CompnyCode") & "'"
        Else
            sSql &= " dlc.cmpcode LIKE '%'"
        End If
        sSql &= sSqlPlus & " ORDER BY dlc.dlcdate DESC, dlc.dlcoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_mstdlc")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "dlcoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub BindBOMData()
        sSql = "SELECT bom.bomoid, bom.bomdesc, CONVERT(VARCHAR(10), bom.bomdate, 101) AS bomdate, i.itemcode, i.itemlongdescription AS itemlongdesc, '' AS precostdesc, 0.0 AS precostdlcamt, 0.0 AS precostoverheadamt, bom.updtime, bom.itemunitoid FROM QL_mstbom bom INNER JOIN QL_mstitem i ON i.itemoid=bom.itemoid WHERE bom.cmpcode='" & DDLBusUnit.SelectedValue & "' AND bom.activeflag='ACTIVE' AND " & FilterDDLListBOM.SelectedValue & " LIKE '%" & Tchar(FilterTextListBOM.Text) & "%' AND bom.dlcstatus='' ORDER BY bom.bomdate DESC, bom.bomoid DESC"
        FillGV(gvListBOM, sSql, "QL_mstbom")
    End Sub

    Private Sub InitDLCDesc()
        sSql = "SELECT COUNT(*) FROM QL_mstdlc WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND bomoid=" & bomoid.Text
        dlccounter.Text = ToDouble(CStr(GetStrData(sSql))) + 1
        dlcdesc.Text = "DLC " & bomdesc.Text & " " & dlccounter.Text
    End Sub

    Private Sub RecalculateDtlPct()
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            For C1 As Integer = 0 To dt.Rows.Count - 1
                dt.Rows(C1)("dlcdtlamount") = (ToDouble(dt.Rows(C1)("dlcpct").ToString) * ToDouble(dlcamount.Text)) / 100
            Next
            gvDtl.DataSource = Session("TblDtl")
            gvDtl.DataBind()
        End If
    End Sub

    Private Sub RecalculateDtlOHDPct()
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            For C1 As Integer = 0 To dt.Rows.Count - 1
                dt.Rows(C1)("dlcohdamount") = (ToDouble(dt.Rows(C1)("dlcpct").ToString) * ToDouble(dlcoverheadamt.Text)) / 100
            Next
            gvDtl.DataSource = Session("TblDtl")
            gvDtl.DataBind()
        End If
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("QL_mstdlcdtl")
        dtlTable.Columns.Add("dlcdtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("deptoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("deptname", Type.GetType("System.String"))
        dtlTable.Columns.Add("dlcpct", Type.GetType("System.Double"))
        dtlTable.Columns.Add("dlcdtlamount", Type.GetType("System.Double"))
        dtlTable.Columns.Add("dlcohdamount", Type.GetType("System.Double"))
        dtlTable.Columns.Add("dlcdtlnote", Type.GetType("System.String"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub SumPercentage()
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            Dim dVal As Double = 0
            For C1 As Integer = 0 To dt.Rows.Count - 1
                dVal += ToDouble(dt.Rows(C1)("dlcpct").ToString)
            Next
            dlctotalpct.Text = ToMaskEdit(dVal, 4)
        End If
    End Sub

    Private Sub ClearDetail()
        dlcdtlseq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable = Session("TblDtl")
            dlcdtlseq.Text = objTable.Rows.Count + 1
            If objTable.Rows.Count > 0 Then
                EnableHeader(False)
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        i_u2.Text = "New Detail"
        deptoid.SelectedIndex = -1
        dlcpct.Text = ""
        dlcdtlamount.Text = ""
        dlcohdamount.Text = ""
        dlcdtlnote.Text = ""
        lastpct.Text = ""
        gvDtl.SelectedIndex = -1
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal
        DDLBusUnit.CssClass = sCss
        btnSearchBOM.Visible = bVal
        btnClearBOM.Visible = bVal
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        sSql = "SELECT dlc.cmpcode, dlc.dlcoid, dlc.dlcdate, dlccounter, dlc.dlcdesc, dlc.bomoid, dlc.dlctotalpct, dlc.dlcamount, dlc.dlcoverheadamt, dlc.dlcnote, dlc.activeflag, dlc.createuser, dlc.createtime, dlc.upduser, dlc.updtime, bom.bomdesc, 0.0 AS precostdlcamt, 0.0 AS precostoverheadamt, dlcacctgoid, ohdacctgoid, bom.updtime AS dlcres1, i.itemcode, i.itemlongdescription AS itemlongdesc, bom.itemunitoid FROM QL_mstdlc dlc INNER JOIN QL_mstbom bom ON bom.cmpcode=dlc.cmpcode AND bom.bomoid=dlc.bomoid INNER JOIN QL_mstitem i ON bom.itemoid=i.itemoid WHERE dlc.dlcoid=" & sOid
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        While xreader.Read
            DDLBusUnit.SelectedValue = Trim(xreader("cmpcode").ToString)
            dlcoid.Text = Trim(xreader("dlcoid").ToString)
            dlcdate.Text = Format(xreader("dlcdate"), "MM/dd/yyyy")
            dlccounter.Text = Trim(xreader("dlccounter").ToString)
            dlcdesc.Text = Trim(xreader("dlcdesc").ToString)
            bomoid.Text = Trim(xreader("bomoid").ToString)
            InitDDLDept() : InitDDLCOA()
            lastbomoid.Text = bomoid.Text
            bomdesc.Text = Trim(xreader("bomdesc").ToString)
            itemcode.Text = Trim(xreader("itemcode").ToString)
            itemlongdesc.Text = Trim(xreader("itemlongdesc").ToString)
            dlctotalpct.Text = ToMaskEdit(ToDouble(xreader("dlctotalpct").ToString), 4)
            dlcamount.Text = ToMaskEdit(ToDouble(xreader("dlcamount").ToString), 4)
            dlcoverheadamt.Text = ToMaskEdit(ToDouble(xreader("dlcoverheadamt").ToString), 4)
            dlcnote.Text = Trim(xreader("dlcnote").ToString)
            activeflag.SelectedValue = Trim(xreader("activeflag").ToString)
            createuser.Text = Trim(xreader("createuser").ToString)
            createtime.Text = Trim(xreader("createtime").ToString)
            upduser.Text = Trim(xreader("upduser").ToString)
            updtime.Text = Trim(xreader("updtime").ToString)
            precostdlcamt.Text = ToMaskEdit(ToDouble(xreader("precostdlcamt").ToString), 4)
            precostoverheadamt.Text = ToMaskEdit(ToDouble(xreader("precostoverheadamt").ToString), 4)
            dlcacctgoid.SelectedValue = Trim(xreader("dlcacctgoid").ToString)
            ohdacctgoid.SelectedValue = Trim(xreader("ohdacctgoid").ToString)
            dlcres1.Text = Trim(xreader("dlcres1").ToString)
            DDLUnitDL.SelectedValue = Trim(xreader("itemunitoid").ToString)
            DDLUnitOHD.SelectedValue = Trim(xreader("itemunitoid").ToString)
        End While
        xreader.Close()
        conn.Close()
        sSql = "SELECT dlcd.dlcdtlseq, dlcd.deptoid, de.deptname, dlcd.dlcpct, dlcd.dlcdtlamount, dlcd.dlcohdamount, dlcd.dlcdtlnote FROM QL_mstdlcdtl dlcd INNER JOIN QL_mstdept de ON de.cmpcode=dlcd.cmpcode AND de.deptoid=dlcd.deptoid WHERE dlcd.dlcoid=" & sOid & " ORDER BY dlcd.dlcdtlseq"
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstdlcdtl")
        Session("TblDtl") = dtTbl
        gvDtl.DataSource = dtTbl
        gvDtl.DataBind()
        EnableHeader(False)
    End Sub

    Private Sub PrintReport()
        Dim rptName As String = ""
        Dim Dtl As String = ""
        Dim Join As String = ""
        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("dlcoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            report.Load(Server.MapPath(folderReport & "rptDLC_DtlFGPdf.rpt"))

            sSql = "SELECT (SELECT div.divname FROM QL_mstdivision div WHERE dlcm.cmpcode = div.cmpcode) AS [Business Unit], dlcm.cmpcode [CMPCODE], dlcm.dlcoid [Oid], dlcm.createtime [DLC Create Date], dlcm.dlcdesc [DLC Desc], dlcm.dlcamount [DLC Amount], dlcoverheadamt [DLC Overhead Amt], dlcm.dlctotalpct [Total Cost %], dlcm.dlcnote [Header Note],CONVERT(VARCHAR(10),dlcm.dlcres1,101) AS [Last BOM Update], 0.0 [Max DLC], 0.0 [Max OHD], itemcode AS [FG Code], itemShortDescription AS [FG Short Desc.], itemLongDescription AS [FG Long Desc.], 0.0 AS [FG Length], 0.0 AS [FG Width], 0.0 AS [FG Height], 0.0 AS [FG Diameter], 0.0 AS [FG Volume], 0.0 AS [FG Packing Length], 0.0 AS [FG Packing Width], 0.0 AS [FG Packing Height], 0.0 AS [FG Packing Diameter], 0.0 AS [FG Packing Volume], 0.0 AS [CBF], g.gendesc [Unit], dlcm.createuser [Create User], dlcm.upduser [Update User], dlcm.updtime [Update Time], dlcm.activeflag [Status], GETDATE() [Precosting Date], GETDATE() [Precosting Create Date], '' [Precosting Create User], GETDATE() [Precosting Update Time], '' [Precosting Update User] , bom.bomdate [BOM Date], bom.createtime [BOM Create Date], bom.createuser [BOM Create User], bom.updtime [BOM Update Time], bom.upduser [BOM Update User] , dlcd.dlcdtlseq [DLC Seq] , dlcd.dlcpct [Cost %], dlcd.dlcdtlamount [DL Cost Amt.], dlcd.dlcohdamount [OHD Cost Amt.], dlcd.dlcdtlnote [Detail Note], dlcd.deptoid [Dept Oid From], (SELECT deptname FROM QL_mstdept df WHERE df.cmpcode=dlcd.cmpcode AND df.deptoid=dlcd.deptoid)  [Dept. From], bom.bomoid [BOM Oid], bomd1.bomdtl1todeptoid [Dept Oid To], (CASE bomd1.bomdtl1reftype WHEN 'FG' THEN 'END' ELSE (SELECT deptname FROM QL_mstdept dt WHERE dt.cmpcode=bomd1.cmpcode AND dt.deptoid=bomd1.bomdtl1todeptoid) END) [Dept. To] FROM QL_mstdlc dlcm INNER JOIN QL_mstbom bom ON bom.cmpcode=dlcm.cmpcode AND bom.bomoid=dlcm.bomoid INNER JOIN QL_mstitem i ON bom.itemoid=i.itemoid INNER JOIN QL_mstgen g ON g.genoid=i.itemUnit1 INNER JOIN QL_mstdlcdtl dlcd ON dlcm.cmpcode=dlcd.cmpcode AND dlcm.dlcoid=dlcd.dlcoid INNER JOIN QL_mstbomdtl1 bomd1 ON dlcm.cmpcode=bomd1.cmpcode AND bomd1.bomoid=dlcm.bomoid AND dlcd.deptoid=bomd1.bomdtl1deptoid"

            If sOid = "" Then
                If FilterDDL.SelectedValue = "dlc.dlcoid" Then
                    sSql &= " WHERE dlcm.dlcoid LIKE '%" & Tchar(FilterText.Text) & "%'"
                Else
                    sSql &= " WHERE " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                End If

                If Session("CompnyCode") <> CompnyCode Then
                    sSql &= " AND dlcm.cmpcode='" & Session("CompnyCode") & "'"
                Else
                    sSql &= " AND dlcm.cmpcode LIKE '%'"
                End If
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sSql &= " AND dlcm.dlcdate>='" & FilterPeriod1.Text & " 00:00:00' AND dlcm.dlcdate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sSql &= " AND dlcm.activeflag='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Master\mstDLC.aspx", Session("SpecialAccess")) = False Then
                    sSql &= " AND dlcm.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sSql &= " WHERE dlcm.dlcoid IN (" & sOid & ")"
            End If
            Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstdlc")
            Dim dvTbl As DataView = dtTbl.DefaultView
            report.SetDataSource(dvTbl.ToTable)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            cProc.SetDBLogonForReport(report)
            report.PrintOptions.PaperSize = PaperSize.PaperFolio
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "DLCPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
        Response.Redirect("~\Master\mstDLC.aspx?awal=true")
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Master\mstDLC.aspx")
        End If
        'If checkPagePermission("~\Master\mstDLC.aspx", Session("Role")) = False Then
        '    Response.Redirect("~\Other\NotAuthorize.aspx")
        'End If
        Page.Title = CompnyName & " - Direct Labor Cost"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            InitAllDDL()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                dlcoid.Text = GenerateID("QL_MSTDLC", CompnyCode)
                dlcdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvDtl.DataSource = dt
            gvDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND dlc.dlcdate>='" & FilterPeriod1.Text & " 00:00:00' AND dlc.dlcdate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND dlc.activeflag='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Master\mstDLC.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND dlc.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = ""
        FilterDDL.SelectedIndex = 0
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Master\mstDLC.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND dlc.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnPrintHdr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrintHdr.Click
        UpdateCheckedValue()
        PrintReport()
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        btnClearBOM_Click(Nothing, Nothing)
        InitDDLCOA()
    End Sub

    Protected Sub btnSearchBOM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchBOM.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        BindBOMData()
        cProc.SetModalPopUpExtender(btnHideListBOM, pnlListBOM, mpeListBOM, True)
    End Sub

    Protected Sub btnClearBOM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearBOM.Click
        bomoid.Text = "" : bomdesc.Text = "" : itemcode.Text = "" : itemlongdesc.Text = "" : precostdlcamt.Text = "" : precostoverheadamt.Text = ""
        InitDDLDept()
    End Sub

    Protected Sub btnFindListBOM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListBOM.Click
        BindBOMData()
        mpeListBOM.Show()
    End Sub

    Protected Sub btnAllListBOM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListBOM.Click
        FilterDDLListBOM.SelectedIndex = -1
        FilterTextListBOM.Text = ""
        BindBOMData()
        mpeListBOM.Show()
    End Sub

    Protected Sub gvListBOM_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListBOM.PageIndexChanging
        gvListBOM.PageIndex = e.NewPageIndex
        BindBOMData()
        mpeListBOM.Show()
    End Sub

    Protected Sub gvListBOM_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListBOM.SelectedIndexChanged
        If bomoid.Text = gvListBOM.SelectedDataKey.Item("bomoid").ToString Then
            btnClearBOM_Click(Nothing, Nothing)
        End If
        bomoid.Text = gvListBOM.SelectedDataKey.Item("bomoid").ToString
        itemcode.Text = gvListBOM.SelectedDataKey.Item("itemcode").ToString
        itemlongdesc.Text = gvListBOM.SelectedDataKey.Item("itemlongdesc").ToString
        bomdesc.Text = gvListBOM.SelectedDataKey.Item("bomdesc").ToString
        precostdlcamt.Text = ToMaskEdit(ToDouble(gvListBOM.SelectedDataKey.Item("precostdlcamt").ToString), 4)
        precostoverheadamt.Text = ToMaskEdit(ToDouble(gvListBOM.SelectedDataKey.Item("precostoverheadamt").ToString), 4)
        dlcres1.Text = gvListBOM.SelectedDataKey.Item("updtime").ToString
        DDLUnitDL.SelectedValue = gvListBOM.SelectedDataKey.Item("itemunitoid").ToString
        DDLUnitOHD.SelectedValue = gvListBOM.SelectedDataKey.Item("itemunitoid").ToString
        InitDLCDesc()
        InitDDLDept()
        cProc.SetModalPopUpExtender(btnHideListBOM, pnlListBOM, mpeListBOM, False)
    End Sub

    Protected Sub lkbCloseListBOM_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListBOM.Click
        cProc.SetModalPopUpExtender(btnHideListBOM, pnlListBOM, mpeListBOM, False)
    End Sub

    Protected Sub dlcamount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlcamount.TextChanged
        'If ToDouble(dlcamount.Text) > ToDouble(precostdlcamt.Text) Then
        '    dlcamount.Text = ToMaskEdit(ToDouble(precostdlcamt.Text), 4)
        'Else
        '    dlcamount.Text = ToMaskEdit(ToDouble(dlcamount.Text), 4)
        'End If
        dlcamount.Text = ToMaskEdit(ToDouble(dlcamount.Text), 4)
        RecalculateDtlPct()
    End Sub

    Protected Sub dlcoverheadamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlcoverheadamt.TextChanged
        'If ToDouble(dlcoverheadamt.Text) > ToDouble(precostoverheadamt.Text) Then
        '    dlcoverheadamt.Text = ToMaskEdit(ToDouble(precostoverheadamt.Text), 4)
        'Else
        '    dlcoverheadamt.Text = ToMaskEdit(ToDouble(dlcoverheadamt.Text), 4)
        'End If
        dlcoverheadamt.Text = ToMaskEdit(ToDouble(dlcoverheadamt.Text), 4)
        RecalculateDtlOHDPct()
    End Sub

    Protected Sub dlcpct_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlcpct.TextChanged
        dlcdtlamount.Text = ToMaskEdit((ToDouble(dlcpct.Text) * ToDouble(dlcamount.Text)) / 100, 4)
        dlcohdamount.Text = ToMaskEdit((ToDouble(dlcpct.Text) * ToDouble(dlcoverheadamt.Text)) / 100, 4)
    End Sub

    Protected Sub dlcdtlamount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlcdtlamount.TextChanged
        If ToDouble(dlcamount.Text) > 0 Then
            dlcpct.Text = ToMaskEdit((ToDouble(dlcdtlamount.Text) * 100) / ToDouble(dlcamount.Text), 4)
            dlcohdamount.Text = ToMaskEdit((ToDouble(dlcpct.Text) * ToDouble(dlcoverheadamt.Text)) / 100, 4)
        End If
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            If Session("TblDtl") Is Nothing Then
                CreateTblDetail()
            End If
            Dim objTable As DataTable
            objTable = Session("TblDtl")
            Dim dv As DataView = objTable.DefaultView
            If i_u2.Text = "New Detail" Then
                dv.RowFilter = "deptoid=" & deptoid.SelectedValue
            Else
                dv.RowFilter = "dlcdtlseq <>" & dlcdtlseq.Text & " AND deptoid=" & deptoid.SelectedValue
            End If
            If dv.Count > 0 Then
                showMessage("This Data has been added before!", 2)
                dv.RowFilter = ""
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                objRow("dlcdtlseq") = objTable.Rows.Count + 1
            Else
                objRow = objTable.Rows(dlcdtlseq.Text - 1)
                objRow.BeginEdit()
            End If
            objRow("deptoid") = deptoid.SelectedValue
            objRow("deptname") = deptoid.SelectedItem.Text
            objRow("dlcpct") = ToDouble(dlcpct.Text)
            objRow("dlcdtlamount") = ToDouble(dlcdtlamount.Text)
            objRow("dlcohdamount") = ToDouble(dlcohdamount.Text)
            objRow("dlcdtlnote") = dlcdtlnote.Text
            If i_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            Session("TblDtl") = objTable
            gvDtl.DataSource = objTable
            gvDtl.DataBind()
            SumPercentage()
            ClearDetail()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 4)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
        End If
    End Sub

    Protected Sub gvDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("dlcdtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvDtl.DataSource = objTable
        gvDtl.DataBind()
        SumPercentage()
        ClearDetail()
    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDtl.SelectedIndexChanged
        Try
            dlcdtlseq.Text = gvDtl.SelectedDataKey.Item("dlcdtlseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "dlcdtlseq=" & dlcdtlseq.Text
                deptoid.SelectedValue = dv.Item(0).Item("deptoid").ToString
                dlcpct.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("dlcpct").ToString), 4)
                dlcdtlamount.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("dlcdtlamount").ToString), 4)
                dlcohdamount.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("dlcohdamount").ToString), 4)
                dlcdtlnote.Text = dv.Item(0).Item("dlcdtlnote").ToString
                lastpct.Text = ToDouble(dv.Item(0).Item("dlcpct").ToString)
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            If Session("oid") = Nothing Or Session("oid") = "" Then
                If CheckDataExists("SELECT COUNT(*) FROM QL_mstdlc WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND bomoid=" & bomoid.Text) Then
                    showMessage("Selected BOM already used by another data!", 2)
                    Exit Sub
                End If
                dlcoid.Text = GenerateID("QL_MSTDLC", CompnyCode)
            End If
            dlcdtloid.Text = GenerateID("QL_MSTDLCDTL", CompnyCode)
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_mstdlc (cmpcode, dlcoid, dlcdate, dlccounter, dlcdesc, bomoid, dlctotalpct, dlcamount, dlcoverheadamt, dlcnote, activeflag, createuser, createtime, upduser, updtime, dlcacctgoid, ohdacctgoid, dlcres1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & dlcoid.Text & ", '" & dlcdate.Text & "', " & dlccounter.Text & ", '" & Tchar(dlcdesc.Text) & "', " & bomoid.Text & ", " & ToDouble(dlctotalpct.Text) & ", " & ToDouble(dlcamount.Text) & ", " & ToDouble(dlcoverheadamt.Text) & ", '" & Tchar(dlcnote.Text) & "', '" & activeflag.SelectedValue & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dlcacctgoid.SelectedValue & ", " & ohdacctgoid.SelectedValue & ", '" & dlcres1.Text & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & dlcoid.Text & " WHERE tablename='QL_MSTDLC' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_mstdlc SET dlcdate='" & dlcdate.Text & "', dlccounter=" & dlccounter.Text & ", dlcdesc='" & Tchar(dlcdesc.Text) & "', bomoid=" & bomoid.Text & ", dlctotalpct=" & ToDouble(dlctotalpct.Text) & ", dlcamount=" & ToDouble(dlcamount.Text) & ", dlcoverheadamt=" & ToDouble(dlcoverheadamt.Text) & ", dlcnote='" & Tchar(dlcnote.Text) & "', activeflag='" & activeflag.SelectedValue & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, dlcacctgoid=" & dlcacctgoid.SelectedValue & ", ohdacctgoid=" & ohdacctgoid.SelectedValue & ", dlcres1='" & dlcres1.Text & "' WHERE dlcoid=" & dlcoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    If lastbomoid.Text <> bomoid.Text Then
                        sSql = "UPDATE QL_mstprecost SET dlcstatus='' WHERE cmpcode='" & CompnyCode & "' AND precostoid IN (SELECT precostoid FROM QL_mstbom WHERE bomoid=" & lastbomoid.Text & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        sSql = "UPDATE QL_mstbom SET dlcstatus='' WHERE bomoid=" & lastbomoid.Text
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                    sSql = "DELETE FROM QL_mstdlcdtl WHERE dlcoid=" & dlcoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                sSql = "UPDATE QL_mstprecost SET dlcstatus='Created' WHERE cmpcode='" & CompnyCode & "' AND precostoid IN (SELECT precostoid FROM QL_mstbom WHERE bomoid=" & bomoid.Text & ")"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstbom SET dlcstatus='Created' WHERE bomoid=" & bomoid.Text
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_mstdlcdtl (cmpcode, dlcdtloid, dlcoid, dlcdtlseq, deptoid, dlcrefoid, dlcreftype, dlcpct, dlcdtlamount, dlcdtlnote, dlcdtlstatus, upduser, updtime, dlcohdamount) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C1 + CInt(dlcdtloid.Text)) & ", " & dlcoid.Text & ", " & C1 + 1 & ", " & objTable.Rows(C1).Item("deptoid") & ", 0, '', " & ToDouble(objTable.Rows(C1).Item("dlcpct").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("dlcdtlamount").ToString) & ", '" & Tchar(objTable.Rows(C1).Item("dlcdtlnote").ToString) & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(C1).Item("dlcohdamount").ToString) & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(dlcdtloid.Text)) & " WHERE tablename='QL_MSTDLCDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 2)
                conn.Close()
                Exit Sub
            End Try
            Response.Redirect("~\Master\mstDLC.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Master\mstDLC.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If dlcoid.Text.Trim = "" Then
            showMessage("Please select Direct Labor Cost data first!", 2)
            Exit Sub
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_mstprecost SET dlcstatus='' WHERE cmpcode='" & CompnyCode & "' AND precostoid IN (SELECT precostoid FROM QL_mstbom WHERE bomoid=" & bomoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_mstbom SET dlcstatus='' WHERE bomoid=" & bomoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_mstdlcdtl WHERE dlcoid=" & dlcoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_mstdlc WHERE dlcoid=" & dlcoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Master\mstDLC.aspx?awal=true")
    End Sub
#End Region

End Class