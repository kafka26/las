<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="mstDept.aspx.vb" Inherits="Master_Department" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Department" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Department :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" DefaultButton="btnSearch" __designer:wfdid="w157"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 15%" class="Label" align=left>Filter</TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w158"><asp:ListItem Value="deptcode">Code</asp:ListItem>
<asp:ListItem Value="deptname">Name</asp:ListItem>
    <asp:ListItem Value="deptpic">PIC</asp:ListItem>
    <asp:ListItem>Address</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="200px" __designer:wfdid="w159"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbDiv" runat="server" Text="Business Unit" __designer:wfdid="w160"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLDiv" runat="server" CssClass="inpText" Width="155px" __designer:wfdid="w161">
            </asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status" Checked="True" __designer:wfdid="w162"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w163">
                <asp:ListItem Value="All">ALL</asp:ListItem>
                <asp:ListItem Selected="True">ACTIVE</asp:ListItem>
                <asp:ListItem>INACTIVE</asp:ListItem>
            </asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w164"></asp:ImageButton> <asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w165"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w166"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvMst" runat="server" ForeColor="#333333" Width="100%" AllowSorting="True" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" PageSize="8" GridLines="None" __designer:wfdid="w153">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="deptoid" DataNavigateUrlFormatString="~\Master\mstDept.aspx?oid={0}" DataTextField="deptcode" HeaderText="Code" SortExpression="deptcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="deptname" HeaderText="Name " SortExpression="deptname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="creationdate" HeaderText="Creation Date" SortExpression="creationdate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="divname" HeaderText="Business Unit" SortExpression="divname">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="deptpic" HeaderText="PIC" SortExpression="deptpic">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="deptaddress" HeaderText="Address" SortExpression="deptaddress">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="depttotalperson" HeaderText="Total Employee" SortExpression="depttotalperson">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="There is no data !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w168"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD style="WIDTH: 13%" class="Label" align=left><asp:Label id="I_U" runat="server" CssClass="Important" Text="New Data" __designer:wfdid="w85"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="deptoid" runat="server" Visible="False" __designer:wfdid="w86"></asp:Label></TD><TD style="WIDTH: 13%" class="Label" align=left></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label16" runat="server" Text="Business Unit" __designer:wfdid="w87"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="divoid" runat="server" CssClass="inpText" Width="155px" __designer:wfdid="w88" AutoPostBack="True">
            </asp:DropDownList></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="divcode" runat="server" Visible="False" __designer:wfdid="w89"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label2" runat="server" Text="Code" __designer:wfdid="w90"></asp:Label> <asp:Label id="Label17" runat="server" CssClass="Important" Text="*" __designer:wfdid="w91"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="deptcode" runat="server" CssClass="inpText" Width="75px" __designer:wfdid="w92" MaxLength="10"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label7" runat="server" Text="Creation Date" __designer:wfdid="w93"></asp:Label> <asp:Label id="Label21" runat="server" CssClass="Important" Text="*" __designer:wfdid="w94"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="deptcreationdate" runat="server" CssClass="inpText" Width="60px" __designer:wfdid="w95" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w96"></asp:ImageButton> <asp:Label id="Label22" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w97"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text="Name" __designer:wfdid="w98"></asp:Label> <asp:Label id="Label18" runat="server" CssClass="Important" Text="*" __designer:wfdid="w99"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="deptname" runat="server" CssClass="inpText" Width="150px" __designer:wfdid="w100" MaxLength="50"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label8" runat="server" Text="PIC" __designer:wfdid="w101"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="deptpic" runat="server" CssClass="inpText" Width="150px" __designer:wfdid="w102" MaxLength="50"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label4" runat="server" Text="Address" __designer:wfdid="w103"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="deptaddress" runat="server" CssClass="inpText" Width="250px" __designer:wfdid="w104" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label5" runat="server" Text="City" __designer:wfdid="w105"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="deptcityoid" runat="server" CssClass="inpText" Width="155px" __designer:wfdid="w106">
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label6" runat="server" Text="Phone" __designer:wfdid="w107"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="deptphone" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w108" MaxLength="20"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label10" runat="server" Text="Email" __designer:wfdid="w109"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="deptemail" runat="server" CssClass="inpText" Width="150px" __designer:wfdid="w110" MaxLength="50"></asp:TextBox> <asp:Label id="Label23" runat="server" CssClass="Important" Text="(mail@sample.com)" __designer:wfdid="w111"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label9" runat="server" Text="Max Employee" Visible="False" __designer:wfdid="w112"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="deptmaxperson" runat="server" CssClass="inpText" Width="50px" Visible="False" __designer:wfdid="w113" MaxLength="4">0</asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label11" runat="server" Text="Max Base Salary" Visible="False" __designer:wfdid="w114"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="deptmaxbasesalary" runat="server" CssClass="inpText" Width="100px" Visible="False" __designer:wfdid="w115" AutoPostBack="True" MaxLength="9">0</asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label19" runat="server" Text="COA Usage" __designer:wfdid="w116"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="deptacctgoid" runat="server" CssClass="inpText" Width="255px" __designer:wfdid="w117"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label15" runat="server" Text="Status" __designer:wfdid="w118"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="activeflag" runat="server" CssClass="inpText" Width="85px" __designer:wfdid="w119">
        <asp:ListItem>ACTIVE</asp:ListItem>
        <asp:ListItem>INACTIVE</asp:ListItem>
            </asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label13" runat="server" Text="Note" __designer:wfdid="w120"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="deptnote" runat="server" CssClass="inpText" Width="250px" __designer:wfdid="w121" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label12" runat="server" Text="Total Employee" __designer:wfdid="w122"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:Label id="depttotalperson" runat="server" Text="0" __designer:wfdid="w123"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label14" runat="server" Text="Total Base Salary" Visible="False" __designer:wfdid="w124"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="depttotalbasesalary" runat="server" Text="0" Visible="False" __designer:wfdid="w125"></asp:Label></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><ajaxToolkit:CalendarExtender id="ceDate" runat="server" TargetControlID="deptcreationdate" __designer:wfdid="w126" PopupButtonID="imbDate" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeDate" runat="server" TargetControlID="deptcreationdate" __designer:wfdid="w127" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999" InputDirection="RightToLeft"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:FilteredTextBoxExtender id="ftbPhone" runat="server" TargetControlID="deptphone" __designer:wfdid="w128" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbMaxEmp" runat="server" TargetControlID="deptmaxperson" __designer:wfdid="w129" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbMaxSalary" runat="server" TargetControlID="deptmaxbasesalary" __designer:wfdid="w130" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender> </TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=6><asp:Label id="lblWarning" runat="server" CssClass="Important" __designer:wfdid="w131"></asp:Label></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w132"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w133"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w134"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w135"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w136" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w137" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w138" AlternateText="Delete"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w139" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w140"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Department :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

