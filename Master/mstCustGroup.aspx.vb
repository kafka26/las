Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_custgroup
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Public Function IsEmailAddress(ByVal email As String) As Boolean
        email = CStr(email)
        Dim strRegex As String = "^[a-zA-Z0-9][\w\.-_]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
        Dim re As New Regex(strRegex)
        If re.IsMatch(email) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function IsValidUrl(ByVal url As String) As Boolean
        If Not url.ToLower().StartsWith("www.") Then
            Return False
        End If
        url = CStr(url)
        Dim strRegex As String = "^[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z0-9][\w\.-_]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
        Dim re As New Regex(strRegex)
        If Not re.IsMatch(url) Then
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        If custgroupcode.Text.Trim = "" Then
            sError &= "- Please fill CODE field!<BR>"
        End If
        If custgroupprefixoid.SelectedValue = "" Then
            sError &= "- Please select PREFIX NAME field!<BR>"
        End If
        If custgroupname.Text.Trim = "" Then
            sError &= "- Please fill NAME field!<BR>"
        End If
        If custgrouppaymentoid.SelectedValue = "" Then
            sError &= "- Please select DEFAULT TOP field!<BR>"
        End If
        If custgroupcityoid.SelectedValue = "" Then
            sError &= "- Please select CITY field!<BR>"
        End If
        If custgroupemail.Text <> "" Then
            If Not IsEmailAddress(custgroupemail.Text) Then
                sError &= "- EMAIL field is invalid. The format should like '<STRONG>example@email.com</STRONG>'.<BR>"
            End If
        End If
        If custgroupwebsite.Text <> "" Then
            If Not IsValidUrl(custgroupwebsite.Text) Then
                sError &= "- WEBSITE field is invalid. The format should like '<STRONG>www.example.com</STRONG>'.<BR>"
            End If
        End If
        If custgroupcp1email.Text <> "" Then
            If Not IsEmailAddress(custgroupcp1email.Text) Then
                sError &= "- CP1 EMAIL field is invalid. The format should like '<STRONG>example@email.com</STRONG>'.<BR>"
            End If
        End If
        If custgroupcp2email.Text <> "" Then
            If Not IsEmailAddress(custgroupcp2email.Text) Then
                sError &= "- CP2 EMAIL is invalid. The format should like '<STRONG>example@email.com</STRONG>'.<BR>"
            End If
        End If
        If ToDouble(custcreditlimitrupiah.Text) <= 0 Then
            sError &= "- Please fill CREDIT LIMIT RUPIAH field!<BR>"
        Else
            If ToDouble(custcreditlimitrupiah.Text) = 1 Then
                sError &= "- Credit Limit Jangan di isi 1 boss!<BR>"
            End If
        End If
		'If custcreditlimitusd.Text = "" Then
		'    sError &= "- Please fill CREDIT LIMIT USD field!<BR>"
		'End If
        If custcurrdefaultoid.Items.Count <= 0 Then
            sError &= "Please fill Currency in master currency first !<BR>"
        End If
        If custgroupaddr.Text <> "" Then
            If custgroupaddr.Text.Length > 500 Then
                sError &= " - Alamat harus kurang dari 500 karakter !<BR>"
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputExists() As Boolean
        sSql = "SELECT COUNT(*) FROM QL_mstcustgroup WHERE cmpcode='" & CompnyCode & "' AND custgroupcode='" & Tchar(custgroupcode.Text.Trim) & "'"
        If Session("oid") <> Nothing And Session("oid") <> "" Then
            sSql &= " AND custgroupoid<>" & Session("oid")
        End If
        If ToDouble(GetStrData(sSql).ToString) > 0 Then
            ' showMessage("CODE has been used before by another data. Please fill another CODE!", 2)
            ' Return True
        End If
        sSql = "SELECT COUNT(*) FROM QL_mstcustgroup WHERE cmpcode='" & CompnyCode & "' AND custgroupname='" & Tchar(custgroupname.Text.Trim) & "' AND custgroupprefixoid=" & custgroupprefixoid.SelectedValue
        If Session("oid") <> Nothing And Session("oid") <> "" Then
            sSql &= " AND custgroupoid<>" & Session("oid")
        End If
        If ToDouble(GetStrData(sSql).ToString) > 0 Then
            ' showMessage("NAME has been used before by another data. Please fill another NAME!", 2)
            ' Return True
        End If
        Return False
    End Function

    Private Function GetCheckedTag() As String
        Dim sRet As String = ""
        If custgroupres3.Items.Count > 0 Then
            For C1 As Integer = 0 To custgroupres3.Items.Count - 1
                If custgroupres3.Items(C1).Selected = True Then
                    sRet &= custgroupres3.Items(C1).Value & "; "
                End If
            Next
        End If
        If sRet <> "" Then
            sRet = Left(sRet, sRet.Length - 2)
        End If
        Return sRet
    End Function

#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitAllDDL()
        ' Fill DDL Prefix Name
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='PREFIX NAME' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDL(custgroupprefixoid, sSql)
        'Fill DDL Default TOP
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='PAYMENT TYPE' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDL(custgrouppaymentoid, sSql)
        'Fill DDL Currency
        sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        FillDDL(custcurrdefaultoid, sSql)
        If custcurrdefaultoid.Items.Count <= 0 Then
            showMessage("Please fill Currency in master currency first !", 2) : Exit Sub
        End If
        ' Fill DDL City
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='CITY' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDL(custgroupcityoid, sSql)
        FillFilterDDLTag()
        ' Fill List Box Tag
        sSql = "SELECT cat1shortdesc, (cat1res1 + ' - ' + cat1shortdesc) AS cat1desc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' and cat1res1 in ('Finish Good','Service','Raw') ORDER BY cat1desc"
        custgroupres3.Items.Clear()
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            custgroupres3.Items.Add(xreader.GetValue(1).ToString)
            custgroupres3.Items(custgroupres3.Items.Count - 1).Value = xreader.GetValue(0).ToString
        End While
        xreader.Close()
        conn.Close()
    End Sub

    Private Sub FillFilterDDLTag()
        ' Fill Filter DDL Tag
        sSql = "SELECT cat1oid, cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        If cbTagType.Checked Then
            sSql &= " AND cat1res1='" & FilterDDLTagType.SelectedValue & "'"
        End If
        sSql &= " ORDER BY cat1shortdesc"
        FillDDL(FilterDDLTag, sSql)
    End Sub

    Private Sub BindMstData(ByVal sFilter As String)
        sSql = "SELECT custgroupoid, custgroupcode, custgroupname, custgroupaddr, gendesc AS custgroupcity, (CASE custgroupphone1 WHEN '' THEN (CASE custgroupphone2 WHEN '' THEN (CASE custgroupphone3 WHEN '' THEN '' ELSE ('[1] ' + custgroupphone3) END) ELSE ('[1] ' + custgroupphone2 + (CASE custgroupphone3 WHEN '' THEN '' ELSE (' [2] ' + custgroupphone3) END)) END) ELSE ('[1] ' + custgroupphone1 + (CASE custgroupphone2 WHEN '' THEN (CASE custgroupphone3 WHEN '' THEN '' ELSE (' [2] ' + custgroupphone3) END) ELSE (' [2] ' + custgroupphone2 + (CASE custgroupphone3 WHEN '' THEN '' ELSE (' [3] ' + custgroupphone3) END)) END)) END) AS custgroupphone, (CASE custgroupcp1name WHEN '' THEN (CASE custgroupcp2name WHEN '' THEN '' ELSE ('[1] ' + custgroupcp2name + ' Telp. ' + custgroupcp2phone + ' Email: ' + custgroupcp2email) END) ELSE ('[1] ' + custgroupcp1name + ' Telp. ' + custgroupcp1phone + ' Email: ' + custgroupcp1email + (CASE custgroupcp2name WHEN '' THEN '' ELSE (' [2] ' + custgroupcp2name + ' Telp. ' + custgroupcp2phone + ' Email: ' + custgroupcp2email) END)) END) AS custgroupcp, c.activeflag FROM QL_mstcustgroup c INNER JOIN QL_mstgen g2 ON g2.cmpcode=c.cmpcode AND genoid=custgroupcityoid WHERE c.cmpcode='" & CompnyCode & "' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(sFilter) & "%'"
        If cbTag.Checked Then
            If FilterDDLTag.SelectedValue <> "" Then
                sSql &= " AND (c.custgroupres3 LIKE '%" & FilterDDLTag.SelectedItem.Text & "%' OR c.custgroupres3='' OR c.custgroupres3 IS NULL)"
            End If
        End If
        If cbStatus.Checked Then
            sSql &= " AND c.activeflag='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Master\mstcustgroup.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND c.createuser='" & Session("UserID") & "'"
        End If
        sSql &= " ORDER BY custgroupcode DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_mstcustgroup")
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub FillTextBox()
        Try
            sSql = "SELECT custgroupoid, custgroupcode, custgrouptype, custgroupprefixoid, custgroupname, custgroupaddr, custgroupcityoid, custgrouppaymentoid, custgrouptaxable, custgroupnpwp, custgroupphone1, custgroupphone2, custgroupphone3, custgroupfax1, custgroupfax2, custgroupemail, custgroupwebsite, custgroupcp1name, custgroupcp2name, custgroupcp1phone, custgroupcp2phone, custgroupcp1email, custgroupcp2email, custgroupnote, custgroupres3, activeflag, createuser, createtime, upduser, updtime, custcreditlimitrupiah, custcreditlimitusd, (select dbo.getBalanceCL(custgroupoid)) custcreditlimitusagerupiah, 0 custcreditlimitusageusd, custcurrdefaultoid,flagCustomer,custgroupdischeader1,custgroupdischeader2,custgroupdischeader3, kode_komisi FROM QL_mstcustgroup WHERE cmpcode='" & CompnyCode & "' AND custgroupoid=" & Session("oid")
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                custgroupoid.Text = xreader("custgroupoid").ToString
                custgroupcode.Text = xreader("custgroupcode").ToString
                custgrouptype.SelectedValue = xreader("custgrouptype").ToString
                custgroupprefixoid.SelectedValue = xreader("custgroupprefixoid").ToString
                custgroupname.Text = xreader("custgroupname").ToString
                custgroupaddr.Text = xreader("custgroupaddr").ToString
                custgroupcityoid.SelectedValue = xreader("custgroupcityoid").ToString
                custgrouppaymentoid.SelectedValue = xreader("custgrouppaymentoid").ToString
                custgrouptaxable.SelectedValue = xreader("custgrouptaxable").ToString
                custgroupnpwp.Text = xreader("custgroupnpwp").ToString
                custgroupphone1.Text = xreader("custgroupphone1").ToString
                custgroupphone2.Text = xreader("custgroupphone2").ToString
                custgroupphone3.Text = xreader("custgroupphone3").ToString
                custgroupfax1.Text = xreader("custgroupfax1").ToString
                custgroupfax2.Text = xreader("custgroupfax2").ToString
                custgroupemail.Text = xreader("custgroupemail").ToString
                custgroupwebsite.Text = xreader("custgroupwebsite").ToString
                custgroupcp1name.Text = xreader("custgroupcp1name").ToString
                custgroupcp2name.Text = xreader("custgroupcp2name").ToString
                custgroupcp1phone.Text = xreader("custgroupcp1phone").ToString
                custgroupcp2phone.Text = xreader("custgroupcp2phone").ToString
                custgroupcp1email.Text = xreader("custgroupcp1email").ToString
                custgroupcp2email.Text = xreader("custgroupcp2email").ToString
                custgroupnote.Text = xreader("custgroupnote").ToString
                SetCheckedTag(xreader("custgroupres3").ToString)
                activeflag.SelectedValue = xreader("activeflag").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
                custcreditlimitrupiah.Text = ToMaskEdit(ToDouble(xreader("custcreditlimitrupiah").ToString), 2)
                custcreditlimitusd.Text = ToMaskEdit(ToDouble(xreader("custcreditlimitusd").ToString), 2)
                custcurrdefaultoid.SelectedValue = xreader("custcurrdefaultoid").ToString
                custcreditlimitusagerupiah.Text = ToMaskEdit(ToDouble(xreader("custcreditlimitusagerupiah").ToString), 2)
                custcreditlimitsisarupiah.Text = ToMaskEdit(ToDouble(xreader("custcreditlimitrupiah").ToString - xreader("custcreditlimitusagerupiah").ToString), 2)
                custcreditlimitusageusd.Text = ToMaskEdit(ToDouble(xreader("custcreditlimitusageusd").ToString), 2)
                custcreditlimitsisausd.Text = ToMaskEdit(ToDouble(xreader("custcreditlimitusd").ToString - xreader("custcreditlimitusageusd").ToString), 2)
                If xreader("flagCustomer").ToString = "1" Then
                    cbCustomer.Checked = True
                Else
                    cbCustomer.Checked = False
                End If
                disc1.Text = xreader("custgroupdischeader1").ToString
                disc2.Text = xreader("custgroupdischeader2").ToString
                disc3.Text = xreader("custgroupdischeader3").ToString
                kode_komisi.SelectedValue = xreader("kode_komisi").ToString
            End While
            xreader.Close()
            conn.Close()
            'ambil last oid jika dy centang untuk flag customer
            custoid.Text = GetStrData("select top 1 custoid from ql_mstcust where custgroupoid=" & Session("oid") & " order by custoid asc")
        Catch ex As Exception
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False
        End Try
        custgroupcode.CssClass = "inpTextDisabled" : custgroupcode.Enabled = False
        CodeCust.Visible = False
        'custgrouptype.CssClass = "inpTextDisabled" : custgrouptype.Enabled = False
        'custgrouppaymentoid.CssClass = "inpTextDisabled" : custgrouppaymentoid.Enabled = False
        'custgrouptaxable.CssClass = "inpTextDisabled" : custgrouptaxable.Enabled = False
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        Try
            If sType = "PDF" Then
                report.Load(Server.MapPath(folderReport & "rptcustgroup.rpt"))
            Else
                report.Load(Server.MapPath(folderReport & "rptcustgroupExcel.rpt"))
            End If
            Dim sWhere As String = " WHERE c.cmpcode='" & CompnyCode & "' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
            If cbTag.Checked Then
                If FilterDDLTag.SelectedValue <> "" Then
                    sWhere &= " AND (c.custgroupres3 LIKE '%" & FilterDDLTag.SelectedItem.Text & "%' OR c.custgroupres3='' OR c.custgroupres3 IS NULL)"
                End If
            End If
            If cbStatus.Checked Then
                sWhere &= " AND c.activeflag='" & FilterDDLStatus.SelectedValue & "'"
            End If
            If checkPagePermission("~\Master\mstcustgroup.aspx", Session("SpecialAccess")) = False Then
                sWhere &= " AND c.createuser='" & Session("UserID") & "'"
            End If
            sWhere &= " ORDER BY custgroupcode DESC"
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            If sType = "PDF" Then
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "customergroupPrintOut")
            Else
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "customergroupPrintOut")
            End If
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
        Response.Redirect("~\Master\mstcustgroup.aspx?awal=true")
    End Sub

    Private Sub SetCheckedTag(ByVal sTag As String)
        If sTag <> "" Then
            Dim sSplit() As String = sTag.Split(";")
            If sSplit.Length > 0 Then
                If custgroupres3.Items.Count > 0 Then
                    For C1 As Integer = 0 To sSplit.Length - 1
                        Dim li As ListItem = custgroupres3.Items.FindByValue(LTrim(sSplit(C1)))
                        Dim i As Integer = custgroupres3.Items.IndexOf(li)
                        If i >= 0 Then
                            custgroupres3.Items(custgroupres3.Items.IndexOf(li)).Selected = True
                        End If
                    Next
                End If
            End If
        End If
    End Sub

    Private Sub GenerateCodeCustGroup()
        Dim sNo As String = "CUST-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(CustGroupCode, " & DefaultFormatCounter & ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstCustGroup WHERE cmpcode='" & Session("CompnyCode") & "' AND CustGroupCode LIKE '" & sNo & "%'"
        custgroupcode.Text = GenNumberString(sNo, "", cKon.ambilscalar(sSql), DefaultFormatCounter)
    End Sub

    Private Sub GenerateCodeCust()
        Dim sNo As String = "CUST-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(custcode, " & DefaultFormatCounter & ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstcust WHERE cmpcode='" & Session("CompnyCode") & "' AND custcode LIKE '" & sNo & "%'"
        custcode.Text = GenNumberString(sNo, "", cKon.ambilscalar(sSql), DefaultFormatCounter)
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Master\mstcustgroup.aspx")
        End If
        If checkPagePermission("~\Master\mstcustgroup.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - CUSTOMER GROUP"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            I_U.Text = "New Data"
        Else
            I_U.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")

        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            InitAllDDL()
            GenerateCodeCustGroup()
            GenerateCodeCust()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox()
                TabContainer1.ActiveTabIndex = 1
            Else
                custgroupoid.Text = GenerateID("QL_MSTcustgroup", CompnyCode)
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub FilterDDLTagType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLTagType.SelectedIndexChanged
        FillFilterDDLTag()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbTagType.Checked = False
        FilterDDLTagType.SelectedIndex = -1
        FillFilterDDLTag()
        cbTag.Checked = False
        FilterDDLTag.SelectedIndex = -1
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
    End Sub

    Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvMst.DataSource = Session("TblMst")
            gvMst.DataBind()
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim cekCust As Integer = 0
            If IsInputExists() Then
                Exit Sub
            End If
            If Session("oid") = Nothing Or Session("oid") = "" Then
                custgroupoid.Text = GenerateID("QL_MSTcustgroup", CompnyCode)
                custoid.Text = GenerateID("QL_MSTCUST", CompnyCode)
            Else
                'cek apakah sudah ada customer belum
                If GetStrData("select count(-1) from ql_mstcust where custgroupoid=" & custgroupoid.Text & "") > 0 Then
                    cekCust = 1
                Else
                    cekCust = 0
                    custoid.Text = GenerateID("QL_MSTCUST", CompnyCode)
                End If
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_mstcustgroup (cmpcode, custgroupoid, custgroupcode, custgrouptype, custgroupprefixoid, custgroupname, custgroupaddr, custgroupcityoid, custgrouppaymentoid, custgrouptaxable, custgroupnpwp, custgroupphone1, custgroupphone2, custgroupphone3, custgroupfax1, custgroupfax2, custgroupemail, custgroupwebsite, custgroupcp1name, custgroupcp2name, custgroupcp1phone, custgroupcp2phone, custgroupcp1email, custgroupcp2email, custgroupnote, custgroupres3, activeflag, createuser, createtime, upduser, updtime, custcurrdefaultoid, custcreditlimitrupiah, custcreditlimitusd, custcreditlimitusagerupiah, custcreditlimitusageusd, statusAR, custacctgoid,flagCustomer,custgroupdischeader1,custgroupdischeader2,custgroupdischeader3, kode_komisi) VALUES('" & CompnyCode & "', " & custgroupoid.Text & ", '" & Tchar(custgroupcode.Text.Trim) & "" & Tchar(CodeCust.Text) & "', '" & custgrouptype.SelectedValue & "', " & custgroupprefixoid.SelectedValue & ", '" & Tchar(custgroupname.Text.Trim) & "', '" & Tchar(custgroupaddr.Text.Trim) & "', " & custgroupcityoid.SelectedValue & ", " & custgrouppaymentoid.SelectedValue & ", " & custgrouptaxable.SelectedValue & ", '" & Tchar(custgroupnpwp.Text.Trim) & "', '" & Tchar(custgroupphone1.Text.Trim) & "', '" & Tchar(custgroupphone2.Text.Trim) & "', '" & Tchar(custgroupphone3.Text.Trim) & "', '" & Tchar(custgroupfax1.Text.Trim) & "', '" & Tchar(custgroupfax2.Text.Trim) & "', '" & Tchar(custgroupemail.Text.Trim) & "', '" & Tchar(custgroupwebsite.Text.Trim) & "', '" & Tchar(custgroupcp1name.Text.Trim) & "', '" & Tchar(custgroupcp2name.Text.Trim) & "', '" & Tchar(custgroupcp1phone.Text.Trim) & "', '" & Tchar(custgroupcp2phone.Text.Trim) & "', '" & Tchar(custgroupcp1email.Text.Trim) & "', '" & Tchar(custgroupcp2email.Text.Trim) & "', '" & Tchar(custgroupnote.Text.Trim) & "', '" & Tchar(Left(GetCheckedTag(), 1000)) & "', '" & activeflag.SelectedValue & "', '" & createuser.Text & "', '" & createtime.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & custcurrdefaultoid.SelectedValue & "," & ToDouble(custcreditlimitrupiah.Text) & ", " & ToDouble(custcreditlimitusd.Text) & ",0, 0, '', 0,'" & IIf(cbCustomer.Checked = True, "1", "0") & "'," & ToDouble(disc1.Text) & "," & ToDouble(disc2.Text) & "," & ToDouble(disc3.Text) & ", '" & kode_komisi.SelectedValue & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & custgroupoid.Text & " WHERE tablename='QL_MSTcustgroup' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    If cbCustomer.Checked = True Then
                        'Insertkan juga ke master customer
                        sSql = "INSERT INTO QL_mstcust (cmpcode, custgroupoid, custoid, custcode, custtype, custprefixoid, custname, custaddr, custcityoid, custpaymentoid, custtaxable, custnpwp, custphone1, custphone2, custphone3, custfax1, custfax2, custemail, custwebsite, custcp1name, custcp2name, custcp1phone, custcp2phone, custcp1email, custcp2email, custnote, custres3, activeflag, createuser, createtime, upduser, updtime, kode_komisi) VALUES('" & CompnyCode & "', " & custgroupoid.Text & "," & custoid.Text & ", '" & Tchar(custcode.Text.Trim) & "" & Tchar(CodeCust.Text) & "', '" & custgrouptype.SelectedValue & "', " & custgroupprefixoid.SelectedValue & ", '" & Tchar(custgroupname.Text.Trim) & "', '" & Tchar(custgroupaddr.Text.Trim) & "', " & custgroupcityoid.SelectedValue & ", " & custgrouppaymentoid.SelectedValue & ", " & custgrouptaxable.SelectedValue & ", '" & Tchar(custgroupnpwp.Text.Trim) & "', '" & Tchar(custgroupphone1.Text.Trim) & "', '" & Tchar(custgroupphone2.Text.Trim) & "', '" & Tchar(custgroupphone3.Text.Trim) & "', '" & Tchar(custgroupfax1.Text.Trim) & "', '" & Tchar(custgroupfax2.Text.Trim) & "', '" & Tchar(custgroupemail.Text.Trim) & "', '" & Tchar(custgroupwebsite.Text.Trim) & "', '" & Tchar(custgroupcp1name.Text.Trim) & "', '" & Tchar(custgroupcp2name.Text.Trim) & "', '" & Tchar(custgroupcp1name.Text.Trim) & "', '" & Tchar(custgroupcp2name.Text.Trim) & "', '" & Tchar(custgroupcp1email.Text.Trim) & "', '" & Tchar(custgroupcp2email.Text.Trim) & "', '', '" & Tchar(Left(GetCheckedTag(), 1000)) & "', '" & activeflag.SelectedValue & "', '" & createuser.Text & "', '" & createtime.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & kode_komisi.SelectedValue & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        sSql = "UPDATE QL_mstoid SET lastoid=" & custoid.Text & " WHERE tablename='QL_MSTCUST' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                Else
                    sSql = "UPDATE QL_mstcustgroup SET custgrouptype='" & custgrouptype.SelectedValue & "', custgroupprefixoid=" & custgroupprefixoid.SelectedValue & ", custgroupname='" & Tchar(custgroupname.Text.Trim) & "', custgroupaddr='" & Tchar(custgroupaddr.Text.Trim) & "', custgroupcityoid=" & custgroupcityoid.SelectedValue & ", custgrouppaymentoid=" & custgrouppaymentoid.SelectedValue & ", custgrouptaxable=" & custgrouptaxable.SelectedValue & ", custgroupnpwp='" & Tchar(custgroupnpwp.Text.Trim) & "', custgroupphone1='" & Tchar(custgroupphone1.Text.Trim) & "', custgroupphone2='" & Tchar(custgroupphone2.Text.Trim) & "', custgroupphone3='" & Tchar(custgroupphone3.Text.Trim) & "', custgroupfax1='" & Tchar(custgroupfax1.Text.Trim) & "', custgroupfax2='" & Tchar(custgroupfax2.Text.Trim) & "', custgroupemail='" & Tchar(custgroupemail.Text.Trim) & "', custgroupwebsite='" & Tchar(custgroupwebsite.Text.Trim) & "', custgroupcp1name='" & Tchar(custgroupcp1name.Text.Trim) & "', custgroupcp2name='" & Tchar(custgroupcp2name.Text.Trim) & "', custgroupcp1phone='" & Tchar(custgroupcp1phone.Text.Trim) & "', custgroupcp2phone='" & Tchar(custgroupcp2phone.Text.Trim) & "', custgroupcp1email='" & Tchar(custgroupcp1email.Text.Trim) & "', custgroupcp2email='" & Tchar(custgroupcp2email.Text.Trim) & "', custgroupnote='" & Tchar(custgroupnote.Text.Trim) & "', custgroupres3='" & Tchar(Left(GetCheckedTag(), 1000)) & "', activeflag='" & activeflag.SelectedValue & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, custcreditlimitrupiah=" & ToDouble(custcreditlimitrupiah.Text) & ", custcreditlimitusd=" & ToDouble(custcreditlimitusd.Text) & ",custgroupdischeader1=" & ToDouble(disc1.Text) & ",custgroupdischeader2=" & ToDouble(disc2.Text) & ",custgroupdischeader3=" & ToDouble(disc3.Text) & ",flagCustomer='" & IIf(cbCustomer.Checked = True, "1", "0") & "', kode_komisi='" & kode_komisi.SelectedValue & "' WHERE cmpcode='" & CompnyCode & "' AND custgroupoid=" & custgroupoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    If cbCustomer.Checked = True Then
						'updatekan ke customernya
                        sSql = "UPDATE QL_mstcust SET custtype='" & custgrouptype.SelectedValue & "', custgroupoid=" & custgroupoid.Text & ", custprefixoid=" & custgroupprefixoid.SelectedValue & ", custname='" & Tchar(custgroupname.Text.Trim) & "', custaddr='" & Tchar(custgroupaddr.Text.Trim) & "', custcityoid=" & custgroupcityoid.SelectedValue & ", custpaymentoid=" & custgrouppaymentoid.SelectedValue & ", custtaxable=" & custgrouptaxable.SelectedValue & ", custnpwp='" & Tchar(custgroupnpwp.Text.Trim) & "', custphone1='" & Tchar(custgroupphone1.Text.Trim) & "', custphone2='" & Tchar(custgroupphone2.Text.Trim) & "', custphone3='" & Tchar(custgroupphone3.Text.Trim) & "', custfax1='" & Tchar(custgroupfax1.Text.Trim) & "', custfax2='" & Tchar(custgroupfax2.Text.Trim) & "', custemail='" & Tchar(custgroupemail.Text.Trim) & "', custwebsite='" & Tchar(custgroupwebsite.Text.Trim) & "', custcp1name='" & Tchar(custgroupcp1name.Text.Trim) & "', custcp2name='" & Tchar(custgroupcp2name.Text.Trim) & "', custcp1phone='" & Tchar(custgroupcp1phone.Text.Trim) & "', custcp2phone='" & Tchar(custgroupcp2phone.Text.Trim) & "', custcp1email='" & Tchar(custgroupcp1email.Text.Trim) & "', custcp2email='" & Tchar(custgroupcp2email.Text.Trim) & "', custres3='" & Tchar(Left(GetCheckedTag(), 1000)) & "', activeflag='" & activeflag.SelectedValue & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, kode_komisi='" & kode_komisi.SelectedValue & "' WHERE cmpcode='" & CompnyCode & "' AND custoid=" & custoid.Text
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        ' If cekCust = 0 Then
                            ' sSql = "INSERT INTO QL_mstcust (cmpcode, custgroupoid, custoid, custcode, custtype, custprefixoid, custname, custaddr, custcityoid, custpaymentoid, custtaxable, custnpwp, custphone1, custphone2, custphone3, custfax1, custfax2, custemail, custwebsite, custcp1name, custcp2name, custcp1phone, custcp2phone, custcp1email, custcp2email, custnote, custres3, activeflag, createuser, createtime, upduser, updtime) VALUES('" & CompnyCode & "', " & custgroupoid.Text & "," & custoid.Text & ", '" & Tchar(custcode.Text.Trim) & "" & Tchar(CodeCust.Text) & "', '" & custgrouptype.SelectedValue & "', " & custgroupprefixoid.SelectedValue & ", '" & Tchar(custgroupname.Text.Trim) & "', '" & Tchar(custgroupaddr.Text.Trim) & "', " & custgroupcityoid.SelectedValue & ", " & custgrouppaymentoid.SelectedValue & ", " & custgrouptaxable.SelectedValue & ", '" & Tchar(custgroupnpwp.Text.Trim) & "', '" & Tchar(custgroupphone1.Text.Trim) & "', '" & Tchar(custgroupphone2.Text.Trim) & "', '" & Tchar(custgroupphone3.Text.Trim) & "', '" & Tchar(custgroupfax1.Text.Trim) & "', '" & Tchar(custgroupfax2.Text.Trim) & "', '" & Tchar(custgroupemail.Text.Trim) & "', '" & Tchar(custgroupwebsite.Text.Trim) & "', '" & Tchar(custgroupcp1name.Text.Trim) & "', '" & Tchar(custgroupcp2name.Text.Trim) & "', '" & Tchar(custgroupcp1name.Text.Trim) & "', '" & Tchar(custgroupcp2name.Text.Trim) & "', '" & Tchar(custgroupcp1email.Text.Trim) & "', '" & Tchar(custgroupcp2email.Text.Trim) & "', '', '" & Tchar(Left(GetCheckedTag(), 1000)) & "', '" & activeflag.SelectedValue & "', '" & createuser.Text & "', '" & createtime.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                            ' xCmd.CommandText = sSql
                            ' xCmd.ExecuteNonQuery()
                            ' sSql = "UPDATE QL_mstoid SET lastoid=" & custoid.Text & " WHERE tablename='QL_MSTCUST' AND cmpcode='" & CompnyCode & "'"
                            ' xCmd.CommandText = sSql
                            ' xCmd.ExecuteNonQuery()
                        ' Else
                            ' 'updatekan ke customernya
                            ' sSql = "UPDATE QL_mstcust SET custtype='" & custgrouptype.SelectedValue & "', custgroupoid=" & custgroupoid.Text & ", custprefixoid=" & custgroupprefixoid.SelectedValue & ", custname='" & Tchar(custgroupname.Text.Trim) & "', custaddr='" & Tchar(custgroupaddr.Text.Trim) & "', custcityoid=" & custgroupcityoid.SelectedValue & ", custpaymentoid=" & custgrouppaymentoid.SelectedValue & ", custtaxable=" & custgrouptaxable.SelectedValue & ", custnpwp='" & Tchar(custgroupnpwp.Text.Trim) & "', custphone1='" & Tchar(custgroupphone1.Text.Trim) & "', custphone2='" & Tchar(custgroupphone2.Text.Trim) & "', custphone3='" & Tchar(custgroupphone3.Text.Trim) & "', custfax1='" & Tchar(custgroupfax1.Text.Trim) & "', custfax2='" & Tchar(custgroupfax2.Text.Trim) & "', custemail='" & Tchar(custgroupemail.Text.Trim) & "', custwebsite='" & Tchar(custgroupwebsite.Text.Trim) & "', custcp1name='" & Tchar(custgroupcp1name.Text.Trim) & "', custcp2name='" & Tchar(custgroupcp2name.Text.Trim) & "', custcp1phone='" & Tchar(custgroupcp1phone.Text.Trim) & "', custcp2phone='" & Tchar(custgroupcp2phone.Text.Trim) & "', custcp1email='" & Tchar(custgroupcp1email.Text.Trim) & "', custcp2email='" & Tchar(custgroupcp2email.Text.Trim) & "', custres3='" & Tchar(Left(GetCheckedTag(), 1000)) & "', activeflag='" & activeflag.SelectedValue & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & CompnyCode & "' AND custoid=" & custoid.Text
                            ' xCmd.CommandText = sSql
                            ' xCmd.ExecuteNonQuery()
                        ' End If
                    End If
                End If
                objTrans.Commit()
                conn.Close()
            Catch ex As Exception
                objTrans.Rollback()
                conn.Close()
                showMessage(ex.Message, 1)
                Exit Sub
            End Try
            Response.Redirect("~\Master\mstcustgroup.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Master\mstcustgroup.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If custgroupoid.Text = "" Then
            showMessage("Please select customer group data first!", 1)
            Exit Sub
        End If
        sSql = "SELECT ta.name FROM sys.tables ta INNER JOIN sys.columns col ON col.object_id=ta.object_id WHERE col.name='custgroupoid' AND ta.name NOT IN ('QL_mstcustgroup')"
        Dim objTblUsage As DataTable = cKon.ambiltabel(sSql, "QL_tables")
        For C1 As Integer = 0 To objTblUsage.Rows.Count - 1
            If CheckDataExists("SELECT COUNT(*) FROM " & objTblUsage.Rows(C1).Item("name").ToString & " WHERE custgroupoid=" & custgroupoid.Text) Then
                showMessage("This data can't be deleted because it is being used by another data!", 2)
                Exit Sub
            End If
        Next
        If DeleteData("QL_mstcustgroup", "custgroupoid", custgroupoid.Text, CompnyCode) = True Then
            Response.Redirect("~\Master\mstcustgroup.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnExportPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportPDF.Click
        ShowReport("PDF")
    End Sub

    Protected Sub btnExportExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportExcel.Click
        ShowReport("Excel")
    End Sub

    Protected Sub gvMst_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub custgroupname_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub custcreditlimitrupiah_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles custcreditlimitrupiah.TextChanged
        'custcreditlimitrupiah.Text = ToDouble(custcreditlimitrupiah.Text)
        custcreditlimitusagerupiah.Text = ToDouble(custcreditlimitusagerupiah.Text)
        custcreditlimitsisarupiah.Text = ToMaskEdit(custcreditlimitrupiah.Text - custcreditlimitusagerupiah.Text, 2)
    End Sub

    Protected Sub custcreditlimitusd_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles custcreditlimitusd.TextChanged
        custcreditlimitusd.Text = ToMaskEdit(custcreditlimitusd.Text, 2)
        custcreditlimitusageusd.Text = ToDouble(custcreditlimitusageusd.Text)
        custcreditlimitsisausd.Text = ToMaskEdit(custcreditlimitusd.Text - custcreditlimitusageusd.Text, 2)
    End Sub

    Protected Sub custgroupcityoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
#End Region

End Class
