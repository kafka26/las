<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="mstBOM.aspx.vb" Inherits="Master_BillOfMaterial" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Bill Of Material" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Bill Of Material :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" __designer:wfdid="w29" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 8%" class="Label" align=left><asp:Label id="lblFilter" runat="server" Text="Filter" __designer:wfdid="w30"></asp:Label></TD><TD style="WIDTH: 2%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="105px" __designer:wfdid="w31"><asp:ListItem Value="tbl.bomoid">ID</asp:ListItem>
<asp:ListItem Value="tbl.bomdesc">BOM Desc.</asp:ListItem>
<asp:ListItem Value="tbl.itemcode">Code FG/WIP</asp:ListItem>
<asp:ListItem Value="tbl.itemshortdesc">Description FG/WIP</asp:ListItem>
<asp:ListItem Value="bom.bomnote">Note</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="250px" __designer:wfdid="w32"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text="Period" __designer:wfdid="w33"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="60px" __designer:wfdid="w34" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w35"></asp:ImageButton>&nbsp;<asp:Label id="Label5" runat="server" Text="to" __designer:wfdid="w36"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="60px" __designer:wfdid="w37" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w38"></asp:ImageButton> <asp:Label id="lblLabelPeriod" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w39"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status" __designer:wfdid="w40"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="105px" __designer:wfdid="w41">
<asp:ListItem>ACTIVE</asp:ListItem>
<asp:ListItem>INACTIVE</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" __designer:wfdid="w42" TargetControlID="FilterPeriod1" Format="MM/dd/yyyy" PopupButtonID="imbDate1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" __designer:wfdid="w43" TargetControlID="FilterPeriod1" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" __designer:wfdid="w44" TargetControlID="FilterPeriod2" Format="MM/dd/yyyy" PopupButtonID="imbDate2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" __designer:wfdid="w45" TargetControlID="FilterPeriod2" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w46"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w47"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPrintHdr" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w48"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvTRN" runat="server" ForeColor="#333333" Width="100%" __designer:wfdid="w23" PageSize="8" AutoGenerateColumns="False" DataKeyNames="bomoid" CellPadding="4" GridLines="None" AllowPaging="True" AllowSorting="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="bomoid" DataNavigateUrlFormatString="~\Master\mstBOM.aspx?oid={0}" DataTextField="bomoid" HeaderText="ID" SortExpression="bomoid">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="bomdesc" HeaderText="BOM Desc." SortExpression="bomdesc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomdate" HeaderText="BOM Date" SortExpression="bomdate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Code FG/WIP" SortExpression="itemcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemshortdesc" HeaderText="Description FG/WIP" SortExpression="itemshortdesc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="activeflag" HeaderText="Status" SortExpression="activeflag">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dlcstatus" HeaderText="DLC Status" SortExpression="dlcstatus">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomnote" HeaderText="Note" SortExpression="bomnote">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
            <asp:CheckBox ID="cbGVMst" runat="server" Checked='<%# eval("checkvalue") %>' />
            <asp:Label ID="lblOidGVMst" runat="server" Text='<%# eval("bomoid") %>' Visible="False"></asp:Label>
        
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w50"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=4><asp:UpdateProgress id="UpdateProgress2" runat="server" __designer:wfdid="w51" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image21" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w52"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrintHdr"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label16" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Bill Of Material Header" __designer:wfdid="w208" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="i_u" runat="server" CssClass="Important" Text="New Data" __designer:wfdid="w209" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="precostoid" runat="server" __designer:wfdid="w210" Visible="False"></asp:Label> <asp:Label id="itemoid" runat="server" __designer:wfdid="w211" Visible="False"></asp:Label> <asp:Label id="itemunit" runat="server" __designer:wfdid="w211" Visible="False"></asp:Label></TD><TD style="WIDTH: 14%" class="Label" align=left></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="bomcounter" runat="server" __designer:wfdid="w213" Visible="False"></asp:Label> <asp:Label id="lastprecostoid" runat="server" __designer:wfdid="w214" Visible="False"></asp:Label> <asp:Label id="bomres2" runat="server" __designer:wfdid="w215" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="Label21" runat="server" Text="Business Unit" __designer:wfdid="w218" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpTextDisabled" Width="305px" __designer:wfdid="w219" Visible="False" AutoPostBack="True" Enabled="False"></asp:DropDownList></TD><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="Label38" runat="server" Text="Type BOM" __designer:wfdid="w220" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="ddlbomtype" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w67" Visible="False" AutoPostBack="True" OnSelectedIndexChanged="ddlbomtype_SelectedIndexChanged"><asp:ListItem>Finish Good</asp:ListItem>
<asp:ListItem>WIP</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text="ID" __designer:wfdid="w216"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:Label id="bomoid" runat="server" __designer:wfdid="w217"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label6" runat="server" Text="BOM Date" __designer:wfdid="w220"></asp:Label> <asp:Label id="Label7" runat="server" CssClass="Important" Text="*" __designer:wfdid="w221"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="bomdate" runat="server" CssClass="inpTextDisabled" Width="75px" __designer:wfdid="w222" Enabled="False" ToolTip="MM/dd/yyyy"></asp:TextBox></TD></TR><TR><TD class="Label" align=left rowSpan=2><asp:Label id="Label10" runat="server" Text="Finish Good" __designer:wfdid="w5"></asp:Label>&nbsp;<asp:Label id="Label28" runat="server" CssClass="Important" Text="*" __designer:wfdid="w6"></asp:Label></TD><TD class="Label" align=center rowSpan=2>:</TD><TD class="Label" align=left rowSpan=2><asp:TextBox id="itemshortdesc" runat="server" CssClass="inpTextDisabled" Width="150px" __designer:wfdid="w7" Enabled="False"></asp:TextBox>&nbsp;-&nbsp;<asp:DropDownList id="itemunitoid" runat="server" CssClass="inpTextDisabled" Width="70px" __designer:wfdid="w1" Enabled="False"></asp:DropDownList>&nbsp;<asp:ImageButton id="btnSearchItem" onclick="btnSearchItem_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w8"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearItem" onclick="btnClearItem_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w9"></asp:ImageButton></TD><TD class="Label" align=left rowSpan=2><asp:Label id="lblTrnNo" runat="server" Text="BOM Desc." __designer:wfdid="w228"></asp:Label></TD><TD class="Label" align=center rowSpan=2>:</TD><TD class="Label" align=left rowSpan=2><asp:TextBox id="bomdesc" runat="server" CssClass="inpText" Width="300px" __designer:wfdid="w229"></asp:TextBox></TD></TR><TR></TR><TR><TD class="Label" align=left rowSpan=1><asp:Label id="Label14" runat="server" Text="FG Code" __designer:wfdid="w230"></asp:Label></TD><TD class="Label" align=center rowSpan=1>:</TD><TD class="Label" align=left><asp:TextBox id="itemcode" runat="server" CssClass="inpTextDisabled" Width="150px" __designer:wfdid="w231" Enabled="False"></asp:TextBox>&nbsp;- <asp:TextBox id="formulaseq" runat="server" CssClass="inpTextDisabled" Width="67px" __designer:wfdid="w4" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label33" runat="server" Text="Qty" __designer:wfdid="w228" Visible="False"></asp:Label>&nbsp;<asp:Label id="Label34" runat="server" CssClass="Important" Text="*" __designer:wfdid="w6" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="bomqty" runat="server" CssClass="inpText" Width="75px" __designer:wfdid="w2" Visible="False" AutoPostBack="True" OnTextChanged="bomqty_TextChanged"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label8" runat="server" Text="Header Note" __designer:wfdid="w232"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="bomnote" runat="server" CssClass="inpText" Width="300px" __designer:wfdid="w233" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label9" runat="server" Text="Status" __designer:wfdid="w234"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="activeflag" runat="server" CssClass="inpText" Width="80px" __designer:wfdid="w235"><asp:ListItem>ACTIVE</asp:ListItem>
<asp:ListItem>INACTIVE</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="nomorlot" runat="server" CssClass="inpText" Width="300px" __designer:wfdid="w7" Visible="False" MaxLength="100"></asp:TextBox></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6 rowSpan=1><asp:Label id="Label11" runat="server" Text="Pre Costing" __designer:wfdid="w223" Visible="False"></asp:Label> <asp:Label id="Label27" runat="server" CssClass="Important" Text="*" __designer:wfdid="w4" Visible="False"></asp:Label> <asp:TextBox id="itemshortdesc2" runat="server" CssClass="inpTextDisabled" Width="260px" __designer:wfdid="w225" Visible="False" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSearchPC" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w226" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnClearPC" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w227" Visible="False"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><asp:Label id="Label17" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Bill Of Material Detail" __designer:wfdid="w236" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><asp:MultiView id="MultiView1" runat="server" __designer:wfdid="w237"><asp:View id="View1" runat="server" __designer:wfdid="w238"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label2" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Detail Process" __designer:wfdid="w239" Font-Underline="False"></asp:Label> <asp:Label id="Label12" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="|" __designer:wfdid="w240" Font-Underline="False"></asp:Label> <asp:LinkButton id="lkbDetMat" runat="server" ForeColor="SteelBlue" Font-Size="8pt" Font-Bold="True" __designer:wfdid="w241">Detail Material</asp:LinkButton></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="i_u2" runat="server" Text="New Detail" __designer:wfdid="w242" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="bomdtl1seq" runat="server" __designer:wfdid="w243" Visible="False">1</asp:Label> <asp:Label id="bomdtl1oid" runat="server" __designer:wfdid="w244" Visible="False"></asp:Label> <asp:Label id="bomdtl1refoid" runat="server" __designer:wfdid="w245" Visible="False"></asp:Label></TD><TD style="WIDTH: 14%" class="Label" align=left></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label24" runat="server" Text="Sequence" __designer:wfdid="w246"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="bomdtl1res1" runat="server" CssClass="inpText" Width="50px" __designer:wfdid="w247" AutoPostBack="True" MaxLength="4"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label23" runat="server" Text="Process Type" __designer:wfdid="w248"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="bomdtl1reftype" runat="server" CssClass="inpText" Width="80px" __designer:wfdid="w249" AutoPostBack="True"><asp:ListItem Enabled="False">WIP</asp:ListItem>
<asp:ListItem>FG</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label4" runat="server" Text="From Department" __designer:wfdid="w250"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="bomdtl1deptoid" runat="server" CssClass="inpTextDisabled" Width="305px" __designer:wfdid="w251" AutoPostBack="True" Enabled="False"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="lbltodept" runat="server" Text="To Department" __designer:wfdid="w252"></asp:Label></TD><TD class="Label" align=center><asp:Label id="septtodept" runat="server" Text=":" __designer:wfdid="w253"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="bomdtl1todeptoid" runat="server" CssClass="inpTextDisabled" Width="305px" __designer:wfdid="w254" Enabled="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label15" runat="server" Text="Detail Note" __designer:wfdid="w255"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="bomdtl1note" runat="server" CssClass="inpText" Width="300px" __designer:wfdid="w256" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:TextBox id="bomdtl1refdesc" runat="server" CssClass="inpTextDisabled" Width="260px" __designer:wfdid="w257" Visible="False" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSearchOutput" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w258" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnClearOutput" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w259" Visible="False"></asp:ImageButton> <asp:TextBox id="bomdtl1refqty" runat="server" CssClass="inpText" Width="75px" __designer:wfdid="w260" Visible="False" MaxLength="16"></asp:TextBox> <asp:DropDownList id="bomdtl1refunitoid" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w261" Visible="False" Enabled="False"></asp:DropDownList> <asp:TextBox id="roundqtydtl1" runat="server" CssClass="inpTextDisabled" Width="75px" __designer:wfdid="w262" Visible="False" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="ftbSeq" runat="server" __designer:wfdid="w263" TargetControlID="bomdtl1res1" FilterType="Numbers"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FteBOM" runat="server" __designer:wfdid="w3" TargetControlID="bomqty" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbQty1" runat="server" __designer:wfdid="w264" TargetControlID="bomdtl1refqty" ValidChars="1234567890,.">
            </ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnAddToList1" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w265"></asp:ImageButton> <asp:ImageButton id="btnClear1" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w266"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:GridView id="GVDtl1" runat="server" ForeColor="#333333" Width="100%" __designer:wfdid="w267" PageSize="5" AutoGenerateColumns="False" DataKeyNames="bomdtl1seq" CellPadding="4" GridLines="None">
<RowStyle BackColor="#E3EAEB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="bomdtl1res1" HeaderText="Sequence">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomdtl1reftype" HeaderText="Type">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomdtl1dept" HeaderText="From Department">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomdtl1todept" HeaderText="To Department">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomdtl1note" HeaderText="Detail Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#666666" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#7C6F57"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR></TBODY></TABLE></asp:View> <asp:View id="View2" runat="server" __designer:wfdid="w268"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left colSpan=6><asp:LinkButton id="lkbDetItem" runat="server" ForeColor="SteelBlue" Font-Size="8pt" Font-Bold="True" __designer:wfdid="w269">Detail Process</asp:LinkButton> <asp:Label id="Label19" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="|" Font-Underline="False" __designer:wfdid="w270"></asp:Label> <asp:Label id="Label18" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Detail Material" Font-Underline="False" __designer:wfdid="w271"></asp:Label></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="Label39" runat="server" Text="Process" __designer:wfdid="w272"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD style="WIDTH: 35%" class="Label" align=left><asp:DropDownList id="DDLProcess" runat="server" CssClass="inpText" Width="305px" __designer:wfdid="w273"></asp:DropDownList></TD><TD style="WIDTH: 14%" class="Label" align=left></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Panel id="pnlDtlLevel5" runat="server" BorderColor="Silver" Width="100%" __designer:wfdid="w274" BorderStyle="Outset" BorderWidth="1px"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label41" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Detail Material Level 5" Font-Underline="False" __designer:wfdid="w275"></asp:Label> <asp:Label id="Label43" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="|" Font-Underline="False" __designer:wfdid="w276" Visible="False"></asp:Label> <asp:LinkButton id="lbLevel03" runat="server" ForeColor="SteelBlue" Font-Size="8pt" Font-Bold="True" __designer:wfdid="w277" Visible="False">Detail Material Level 2</asp:LinkButton></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnUpload" runat="server" ImageUrl="~/Images/upload.png" ImageAlign="AbsMiddle" __designer:wfdid="w278" Visible="False" AlternateText="Upload"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="i_u3" runat="server" Text="New Detail" __designer:wfdid="w279" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="bomdtl2seq" runat="server" __designer:wfdid="w280" Visible="False">1</asp:Label> <asp:Label id="bomdtl2oid" runat="server" __designer:wfdid="w281" Visible="False"></asp:Label> <asp:Label id="bomdtl2refoid" runat="server" __designer:wfdid="w282" Visible="False"></asp:Label> <asp:Label id="precostdtlrefoid" runat="server" __designer:wfdid="w283" Visible="False"></asp:Label></TD><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="precostdtloid" runat="server" __designer:wfdid="w284" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="bomdtl2refcode" runat="server" __designer:wfdid="w285" Visible="False"></asp:Label> <asp:Label id="itemqtyxx" runat="server" __designer:wfdid="w286" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label20" runat="server" Text="Mat. Type" __designer:wfdid="w287"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="bomdtl2reftype" runat="server" CssClass="inpText" Width="80px" __designer:wfdid="w288" AutoPostBack="True"><asp:ListItem>Raw</asp:ListItem>
<asp:ListItem Value="WIP">WIP</asp:ListItem>
<asp:ListItem Value="FG">Finish Good</asp:ListItem>
<asp:ListItem Value="GEN">General</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label22" runat="server" Text="Material" __designer:wfdid="w289"></asp:Label> <asp:Label id="Label29" runat="server" CssClass="Important" Text="*" __designer:wfdid="w290"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="bomdtl2refdesc" runat="server" CssClass="inpTextDisabled" Width="280px" __designer:wfdid="w291" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSearchMat" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w292"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label25" runat="server" Text="Quantity" __designer:wfdid="w293"></asp:Label> <asp:Label id="Label30" runat="server" CssClass="Important" Text="*" __designer:wfdid="w294"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="bomdtl2refqty" runat="server" CssClass="inpText" Width="75px" __designer:wfdid="w295" MaxLength="16"></asp:TextBox> <asp:DropDownList id="bomdtl2refunitoid" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w296"></asp:DropDownList></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="precostdtlrefqty" runat="server" CssClass="inpTextDisabled" Width="75px" __designer:wfdid="w297" Visible="False" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label26" runat="server" Text="Detail Note" __designer:wfdid="w298"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="bomdtl2note" runat="server" CssClass="inpText" Width="300px" __designer:wfdid="w299" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label13" runat="server" Text="Rounding Qty" __designer:wfdid="w300"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="roundqtydtl2" runat="server" CssClass="inpTextDisabled" Width="75px" __designer:wfdid="w301" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="ftbQty2" runat="server" __designer:wfdid="w302" TargetControlID="bomdtl2refqty" ValidChars="1234567890,.">
        </ajaxToolkit:FilteredTextBoxExtender> <asp:Label id="uploaddedfile" runat="server" __designer:wfdid="w303" Visible="False"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnAddToList2" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w304"></asp:ImageButton> <asp:ImageButton id="btnClear2" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w305"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:GridView id="GVDtl2" runat="server" ForeColor="#333333" Width="100%" __designer:wfdid="w306" PageSize="5" AutoGenerateColumns="False" DataKeyNames="bomdtl2seq" CellPadding="4" GridLines="None">
<RowStyle BackColor="#E3EAEB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="bomdtl1process" HeaderText="Process">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomdtl2reftype" HeaderText="Type">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomdtl2seq" HeaderText="No.">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomdtl2refcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomdtl2refdesc" HeaderText="Material">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomdtl2refqty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomdtl2refunit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomdtl2note" HeaderText="Detail Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#666666" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#7C6F57"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR></TBODY></TABLE></asp:Panel></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Panel id="pnlDtlLevel3" runat="server" BorderColor="Silver" Width="100%" __designer:wfdid="w307" BorderStyle="Outset" BorderWidth="1px"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left colSpan=6><asp:LinkButton id="lbLevel05" runat="server" ForeColor="SteelBlue" Font-Size="8pt" Font-Bold="True" __designer:wfdid="w308">Detail Material Level 5</asp:LinkButton> <asp:Label id="Label44" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="|" __designer:wfdid="w309" Font-Underline="False"></asp:Label> <asp:Label id="Label42" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Detail Material Level 2" __designer:wfdid="w310" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="i_u4" runat="server" Text="New Detail" __designer:wfdid="w311" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="bomdtl3seq" runat="server" __designer:wfdid="w312" Visible="False">1</asp:Label> <asp:Label id="bomdtl3oid" runat="server" __designer:wfdid="w313" Visible="False"></asp:Label> <asp:Label id="bomdtl3refoid" runat="server" __designer:wfdid="w314" Visible="False"></asp:Label> <asp:Label id="precostdtlrefoid2" runat="server" __designer:wfdid="w315" Visible="False"></asp:Label></TD><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="precostdtl2oid" runat="server" __designer:wfdid="w316" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="bomdtl3refcode" runat="server" __designer:wfdid="w317" Visible="False"></asp:Label> <asp:Label id="itemqtyxx2" runat="server" __designer:wfdid="w318" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label31" runat="server" Text="Mat. Type" __designer:wfdid="w319"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="bomdtl3reftype" runat="server" CssClass="inpTextDisabled" Width="80px" __designer:wfdid="w320" Enabled="False"><asp:ListItem>Raw</asp:ListItem>
<asp:ListItem>General</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label32" runat="server" Text="Material" __designer:wfdid="w321"></asp:Label> <asp:Label id="Label35" runat="server" CssClass="Important" Text="*" __designer:wfdid="w322"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="bomdtl3refdesc" runat="server" CssClass="inpTextDisabled" Width="280px" __designer:wfdid="w323" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSearchMat2" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w324"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label36" runat="server" Text="Quantity" __designer:wfdid="w325"></asp:Label> <asp:Label id="Label37" runat="server" CssClass="Important" Text="*" __designer:wfdid="w326"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="bomdtl3refqty" runat="server" CssClass="inpText" Width="75px" __designer:wfdid="w327" MaxLength="16"></asp:TextBox> <asp:DropDownList id="bomdtl3refunitoid" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w328" Enabled="False"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label40" runat="server" Text="Detail Note" __designer:wfdid="w329"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="bomdtl3note" runat="server" CssClass="inpText" Width="300px" __designer:wfdid="w330" MaxLength="100"></asp:TextBox></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="precostdtl2refqty" runat="server" CssClass="inpTextDisabled" Width="75px" __designer:wfdid="w331" Visible="False" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="ftbQty3" runat="server" __designer:wfdid="w332" TargetControlID="bomdtl3refqty" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnAddToList3" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w333"></asp:ImageButton> <asp:ImageButton id="btnClear3" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w334"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:GridView id="GVDtl3" runat="server" ForeColor="#333333" Width="100%" __designer:wfdid="w335" GridLines="None" CellPadding="4" DataKeyNames="bomdtl3seq" AutoGenerateColumns="False" PageSize="5">
<RowStyle BackColor="#E3EAEB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="bomdtl1process" HeaderText="Process">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomdtl3reftype" HeaderText="Type">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomdtl3refcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomdtl3refdesc" HeaderText="Material">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomdtl3refqty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomdtl3refunit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomdtl3note" HeaderText="Detail Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#666666" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#7C6F57"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR></TBODY></TABLE></asp:Panel></TD></TR></TBODY></TABLE></asp:View> </asp:MultiView></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=6></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w336"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w337"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w338"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w339"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w340" AlternateText="Save"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSaveAs" runat="server" ImageUrl="~/Images/saveas.png" ImageAlign="AbsMiddle" __designer:wfdid="w3" Visible="False" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w341" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w342" AlternateText="Delete"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w343" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w344"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Bill Of Material :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upListPC" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListPC" runat="server" CssClass="modalBox" Width="900px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListPC" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Pre Costing" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListPC" runat="server" Width="100%" DefaultButton="btnFindListPC">Filter : <asp:DropDownList id="FilterDDLListPC" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="precostoid">ID</asp:ListItem>
<asp:ListItem Value="precostdesc">Description</asp:ListItem>
<asp:ListItem Value="itemcode">FG Code</asp:ListItem>
<asp:ListItem Value="itemlongdesc">Finish Good</asp:ListItem>
<asp:ListItem Value="precostnote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListPC" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListPC" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListPC" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListPC" runat="server" ForeColor="#333333" Width="99%" GridLines="None" CellPadding="4" DataKeyNames="itemoid,itemcode,itemlongdesc,itemunitoid,precostoid,updtime" AutoGenerateColumns="False" PageSize="5" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="XX-Small"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="precostoid" HeaderText="ID">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="precostdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="precostdate" HeaderText="Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="FG Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdesc" HeaderText="Finish Good">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="precostnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="upduser" HeaderText="Update User">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="updtime" HeaderText="Update Time">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label114" runat="server" Text="No data found !!" CssClass="Important"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbCloseListPC" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListPC" runat="server" TargetControlID="btnHideListPC" Drag="True" PopupControlID="pnlListPC" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListPC"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListPC" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upListItem" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListItem" runat="server" CssClass="modalBox" Width="600px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListItem" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Finish Good" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListItem" runat="server" Width="100%" DefaultButton="btnFindListItem">Filter : <asp:DropDownList id="FilterDDLListItem" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="i.itemcode">Code</asp:ListItem>
<asp:ListItem Value="i.itemlongdescription">Description</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListItem" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListItem" onclick="btnFindListItem_Click" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListItem" onclick="btnAllListItem_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD align=center colSpan=3><asp:GridView id="gvListItem" runat="server" ForeColor="#333333" Width="99%" GridLines="None" CellPadding="4" DataKeyNames="itemoid,itemshortdesc,itemcode,itemunitoid" AutoGenerateColumns="False" PageSize="5" AllowPaging="True" OnSelectedIndexChanged="gvListItem_SelectedIndexChanged">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemshortdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle CssClass="gvpopup" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label114" runat="server" Text="No data found !!" CssClass="Important"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListItem" onclick="lkbCloseListItem_Click" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHideListItem" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="mpeListItem" runat="server" TargetControlID="btnHideListItem" PopupDragHandleControlID="lblTitleListItem" BackgroundCssClass="modalBackground" PopupControlID="pnlListItem" Drag="True"></ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upListWIP" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListWIP" runat="server" CssClass="modalBox" Width="700px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListWIP" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of WIP" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListWIP" runat="server" Width="100%" DefaultButton="btnFindListWIP">
    <table style="width: 100%">
        <tr>
            <td align="center" class="Label">
                Filter : <asp:DropDownList id="FilterDDLListWIP" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="matrawcode">Code</asp:ListItem>
<asp:ListItem Value="matrawlongdesc">Description</asp:ListItem>
<asp:ListItem Value="gendesc">Unit</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListWIP" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListWIP" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListWIP" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></td>
        </tr>
        <tr>
            <td align="center" class="Label">
                <asp:CheckBox ID="cbCat01ListWIP" runat="server" Text="Cat. 1 :" />
                <asp:DropDownList ID="FilterDDLCat01ListWIP" runat="server" AutoPostBack="True" CssClass="inpText"
                    Width="150px">
                </asp:DropDownList>
                <asp:CheckBox ID="cbCat02ListWIP" runat="server" Text="Cat. 2 :" />
                <asp:DropDownList ID="FilterDDLCat02ListWIP" runat="server" AutoPostBack="True" CssClass="inpText"
                    Width="150px">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="center" class="Label">
                <asp:CheckBox ID="cbCat03ListWIP" runat="server" Text="Cat. 3 :" />
                <asp:DropDownList ID="FilterDDLCat03ListWIP" runat="server" AutoPostBack="True" CssClass="inpText"
                    Width="150px">
                </asp:DropDownList>
                <asp:CheckBox ID="cbCat04ListWIP" runat="server" Text="Cat. 4 :" />
                <asp:DropDownList ID="FilterDDLCat04ListWIP" runat="server" CssClass="inpText" Width="150px">
                </asp:DropDownList></td>
        </tr>
    </table>
</asp:Panel></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListWIP" runat="server" ForeColor="#333333" Width="99%" PageSize="5" AutoGenerateColumns="False" DataKeyNames="matrawoid,matrawcode,matrawlongdesc,matrawunitoid,matrawlimitqty" CellPadding="4" GridLines="None" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="matrawcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matrawlongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matrawunit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label114" runat="server" Text="No data found !!" CssClass="Important"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbCloseListWIP" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListWIP" runat="server" TargetControlID="btnHideListWIP" PopupDragHandleControlID="lblListWIP" BackgroundCssClass="modalBackground" PopupControlID="pnlListWIP" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListWIP" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel><asp:UpdatePanel id="upListMat" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlListMat2" runat="server" CssClass="modalBox" Width="900px" Visible="False" DefaultButton="btnFindListMat2"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat2" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Material Level 5" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat2" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="matrefcode">Code</asp:ListItem>
<asp:ListItem Value="matrefshortdesc">Description</asp:ListItem>
<asp:ListItem Value="matrefunit">Unit</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat2" runat="server" CssClass="inpText" Width="200px"></asp:TextBox> <asp:ImageButton id="btnFindListMat2" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat2" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:CheckBox id="cbCat01ListMat" runat="server" Text="Cat. 1 :"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat01ListMat" runat="server" CssClass="inpText" Width="200px" AutoPostBack="True"></asp:DropDownList> <asp:CheckBox id="cbCat02ListMat" runat="server" Text="Cat. 2 :"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat02ListMat" runat="server" CssClass="inpText" Width="200px" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:CheckBox id="cbCat03ListMat" runat="server" Text="Cat. 3 :"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat03ListMat" runat="server" CssClass="inpText" Width="200px" AutoPostBack="True"></asp:DropDownList> <asp:CheckBox id="cbCat04ListMat" runat="server" Text="Cat. 4 :"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat04ListMat" runat="server" CssClass="inpText" Width="200px"></asp:DropDownList></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:UpdateProgress id="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat"><ProgressTemplate>
<TABLE style="WIDTH: 200px"><TBODY><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple">Load Data, Please Wait ...</TD></TR><TR><TD><asp:Image id="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif"></asp:Image></TD></TR></TBODY></TABLE>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat2" runat="server" ForeColor="#333333" Width="99%" PageSize="5" AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbListMat2" runat="server" ToolTip='<%# eval("matrefoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="matrefcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matrefshortdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="BOM Qty"><ItemTemplate>
<asp:TextBox id="tbBOMQty" runat="server" CssClass="inpText" Text='<%# eval("bomdtl2qty") %>' Width="50px" MaxLength="16"></asp:TextBox>&nbsp;<ajaxToolkit:FilteredTextBoxExtender id="ftbQtyGV" runat="server" TargetControlID="tbBOMQty" ValidChars="1234567890,.">
</ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Unit"><ItemTemplate>
<asp:DropDownList id="DDLUnit" runat="server" CssClass="inpTextDisabled" Width="80px" __designer:wfdid="w1" Enabled="False" ToolTip="<%# GetParameterID() %>"></asp:DropDownList> 
</ItemTemplate>

<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Note"><ItemTemplate>
<asp:TextBox id="tbBOMNote" runat="server" CssClass="inpText" Text='<%# eval("bomdtl2note") %>' Width="150px" MaxLength="100"></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="roundqtydtl2" HeaderText="Round Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 15px" align=center colSpan=3><asp:LinkButton id="lkbAddToList" runat="server">[ Add To List ]</asp:LinkButton>&nbsp;<asp:LinkButton id="lkbCloseListMat2" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat2" runat="server" TargetControlID="btnHideListMat2" PopupDragHandleControlID="lblTitleListMat2" BackgroundCssClass="modalBackground" PopupControlID="pnlListMat2" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat2" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel><asp:UpdatePanel id="upListMat2" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListMat3" runat="server" CssClass="modalBox" Width="900px" Visible="False" DefaultButton="btnFindListMat3"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat3" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Material Level 2" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat3" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="matrefcode2">Code</asp:ListItem>
<asp:ListItem Value="matrefshortdesc2">Description</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat3" runat="server" CssClass="inpText" Width="200px"></asp:TextBox> <asp:ImageButton id="btnFindListMat3" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat3" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:CheckBox id="cbCat01ListMat2" runat="server" Text="Cat. 1 :"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat01ListMat2" runat="server" CssClass="inpText" Width="200px" AutoPostBack="True"></asp:DropDownList> <asp:CheckBox id="cbCat02ListMat2" runat="server" Text="Cat. 2 :"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat02ListMat2" runat="server" CssClass="inpText" Width="200px"></asp:DropDownList></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:UpdateProgress id="uprogListMat2" runat="server" AssociatedUpdatePanelID="upListMat2"><ProgressTemplate>
<TABLE style="WIDTH: 200px"><TBODY><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple">Load Data, Please Wait ...</TD></TR><TR><TD><asp:Image id="imgLoadListMat2" runat="server" ImageUrl="~/Images/loadingbar.gif"></asp:Image></TD></TR></TBODY></TABLE>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat3" runat="server" ForeColor="#333333" Width="99%" PageSize="5" AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbListMat2" runat="server" ToolTip='<%# eval("matrefoid2") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="matrefcode2" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matrefshortdesc2" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="BOM Qty"><ItemTemplate>
<asp:TextBox id="tbBOMQty" runat="server" CssClass="inpText" Text='<%# eval("bomdtl3qty") %>' Width="50px" MaxLength="16"></asp:TextBox>&nbsp;<ajaxToolkit:FilteredTextBoxExtender id="ftbQtyGV" runat="server" TargetControlID="tbBOMQty" ValidChars="1234567890,.">
</ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Unit"><ItemTemplate>
<asp:DropDownList id="DDLUnit" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w2" Enabled="False" ToolTip='<%# eval("matrefunitoid2") %>'></asp:DropDownList> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Note"><ItemTemplate>
<asp:TextBox id="tbBOMNote" runat="server" CssClass="inpText" Text='<%# eval("bomdtl3note") %>' Width="150px" MaxLength="100"></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 15px" align=center colSpan=3><asp:LinkButton id="lkbAddToList2" runat="server">[ Add To List ]</asp:LinkButton>&nbsp;<asp:LinkButton id="lkbCloseListMat3" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat3" runat="server" TargetControlID="btnHideListMat3" PopupDragHandleControlID="lblTitleListMat3" BackgroundCssClass="modalBackground" PopupControlID="pnlListMat3" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat3" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upListUpload" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlListUpload" runat="server" CssClass="modalBox" Width="900px" Visible="False" __designer:wfdid="w1"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListUpload" runat="server" Font-Size="Medium" Font-Bold="True" Text="Upload Detail Material Level 5" Font-Underline="False" __designer:wfdid="w2"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblFileUpload" runat="server" __designer:wfdid="w3">File Upload :</asp:Label> <asp:FileUpload id="fuListUpload" runat="server" CssClass="inpText" Width="300px" __designer:wfdid="w4"></asp:FileUpload> <asp:ImageButton id="btnUploadList" runat="server" ImageUrl="~/Images/upload.png" ImageAlign="AbsMiddle" __designer:wfdid="w5"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlListUploadData" runat="server" CssClass="inpText" Width="100%" Height="200px" ScrollBars="Vertical" __designer:wfdid="w6"><asp:GridView id="gvListUpload" runat="server" ForeColor="#333333" Width="97%" PageSize="5" AutoGenerateColumns="False" CellPadding="4" GridLines="None" __designer:wfdid="w7">
                                        <RowStyle BackColor="#E3EAEB" />
                                        <Columns>
                                            <asp:BoundField DataField="bomdtl2seq" HeaderText="No.">
                                                <HeaderStyle CssClass="gvhdr" Font-Size="XX-Small" HorizontalAlign="Center" />
                                                <ItemStyle Font-Size="XX-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="bomdtl2reftype" HeaderText="Jenis Material">
                                                <HeaderStyle CssClass="gvhdr" Font-Size="XX-Small" HorizontalAlign="Left" />
                                                <ItemStyle Font-Size="XX-Small" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="bomdtl2refcode" HeaderText="Kode Material">
                                                <HeaderStyle CssClass="gvhdr" Font-Size="XX-Small" HorizontalAlign="Left" />
                                                <ItemStyle Font-Size="XX-Small" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="bomdtl2refdesc" HeaderText="Nama Material">
                                                <HeaderStyle CssClass="gvhdr" Font-Size="XX-Small" HorizontalAlign="Left" />
                                                <ItemStyle Font-Size="XX-Small" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="bomdtl2refqty" HeaderText="Qty">
                                                <HeaderStyle CssClass="gvhdr" Font-Size="XX-Small" HorizontalAlign="Right" />
                                                <ItemStyle Font-Size="XX-Small" HorizontalAlign="Right" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="bomdtl2refunit" HeaderText="Satuan">
                                                <HeaderStyle CssClass="gvhdr" Font-Size="XX-Small" HorizontalAlign="Left" />
                                                <ItemStyle Font-Size="XX-Small" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="bomdtl2deptfrom" HeaderText="Dept From">
                                                <HeaderStyle CssClass="gvhdr" Font-Size="XX-Small" HorizontalAlign="Left" />
                                                <ItemStyle Font-Size="XX-Small" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="bomdtl2note" HeaderText="Detil Note">
                                                <HeaderStyle CssClass="gvhdr" Font-Size="XX-Small" HorizontalAlign="Left" />
                                                <ItemStyle Font-Size="XX-Small" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="validation" HeaderText="Validation">
                                                <HeaderStyle CssClass="gvhdr" Font-Size="XX-Small" HorizontalAlign="Left" />
                                                <ItemStyle Font-Size="XX-Small" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                        </Columns>
                                        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                                        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                                        <EditRowStyle BackColor="#7C6F57" />
                                        <AlternatingRowStyle BackColor="White" />
                                    </asp:GridView> </asp:Panel> </TD></TR><TR><TD align=center colSpan=3><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="uprogListUpload" runat="server" AssociatedUpdatePanelID="upListUpload" __designer:wfdid="w8"><ProgressTemplate>
<DIV id="Div1" class="progressBackgroundFilter"></DIV><DIV id="Div2" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="iprogListUpload" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w9"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> </DIV>&nbsp;</TD></TR><TR><TD align=center colSpan=3><asp:ImageButton id="btnAddUpload" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" AlternateText="Add To List" __designer:wfdid="w10"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancelUpload" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" AlternateText="Cancel" __designer:wfdid="w11"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListUpload" runat="server" TargetControlID="btnHideListUpload" PopupDragHandleControlID="lblListUpload" BackgroundCssClass="modalBackground" PopupControlID="pnlListUpload" Drag="True" __designer:wfdid="w12">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListUpload" runat="server" ForeColor="Transparent" Visible="False" __designer:wfdid="w13"></asp:Button> 
</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUploadList" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upPopUpConf" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlPopUpConf" runat="server" CssClass="modalMsgBox" Visible="False">
                <table>
                    <tr>
                        <td colspan="2" style="background-color: #cc0000; text-align: left">
                            <asp:Label ID="lblCaptionConf" runat="server" Font-Bold="True" Font-Size="Small"
                                ForeColor="White"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 10px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Image ID="imIconConf" runat="server" Height="24px" ImageUrl="~/Images/error.jpg"
                                Width="24px" /></td>
                        <td class="Label" style="text-align: left">
                            <asp:Label ID="lblPopUpConf" runat="server" ForeColor="Red"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 10px; text-align: center">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center">
                            &nbsp;<asp:ImageButton ID="imbYesPopUpConf" runat="server" ImageAlign="AbsMiddle"
                                ImageUrl="~/Images/yes.png" />
                            <asp:ImageButton ID="imbNoPopUpConf" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/no.png" /></td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpePopUpConf" runat="server" BackgroundCssClass="modalBackground"
                Drag="True" DropShadow="True" PopupControlID="pnlPopUpConf" PopupDragHandleControlID="lblCaptionConf"
                TargetControlID="bePopUpConf">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="bePopUpConf" runat="server" CausesValidation="False" Visible="False" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

