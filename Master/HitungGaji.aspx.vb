Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.Web.UI.WebControls

Partial Class Master_HitungGaji
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Function"
    Private Function IsPersonExists() As Boolean
        Dim bReturn As Boolean = True
        sSql = "select COUNT(*) from QL_r_salary r where r.cmpcode='" & CompnyCode & "' and r.personoid=" & personoid.Text & " AND r.r_salary_month=" & ddlmonth.SelectedValue & " AND r.r_salary_year=" & ddlyear.SelectedValue & " AND salary_type='" & ddlperiodtype.SelectedValue & "'"
        If cKon.ambilscalar(sSql) = 0 Then : bReturn = False : End If
        Return bReturn
    End Function

    Private Function UpdateCheckedPerson() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("DataPerson") Is Nothing Then
            Dim dtTbl As DataTable = Session("DataPerson")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvLP.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvLP.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(3).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "personoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl = dtView.ToTable
                Session("DataPerson") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function isChecked(ByVal cbCheked As Boolean)
        Dim sText As String = "Ya"
        If cbCheked = True Then
            sText = "Ya"
        Else
            sText = "Tidak"
        End If
        Return sText
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyCode
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub 'OK

    Private Sub ClearAllSession()
        Session("DataPerson") = Nothing
        Session("DataPersonView") = Nothing
        Session("SalClcOid") = Nothing
        Session("TblSalDed") = Nothing
        Session("TblMst") = Nothing
        gvMst.DataSource = Nothing
        gvMst.DataBind()
        'btnSave.Visible = False
    End Sub 'OK

    Private Sub InitDDLPeriod(ByVal period As String)
        'sSql = "SELECT periodoid, CONVERT(VARCHAR(10), periodstart, 101) + ' - ' + CONVERT(VARCHAR(10), periodend, 101) AS dateperiod FROM QL_mstperiod WHERE cmpcode='" & CompnyCode & "' AND periodmonth=" & ddlmonth.SelectedValue & " AND periodyear=" & ddlyear.SelectedValue & " AND periodtype='" & period & "' ORDER BY periodoid"
        'FillDDL(ddlperiod, sSql)
        If ddlperiod.Items.Count > 0 Then
            ddlperiod.Enabled = True
            ddlperiod.CssClass = "inpText"
            ddlperiod_SelectedIndexChanged(Nothing, Nothing)
        Else
            lblPeriod1.Text = ""
            lblPeriod2.Text = ""
            ddlperiod.Enabled = False
            ddlperiod.CssClass = "inpTextDisabled"
            ClearAllSession()
        End If
    End Sub 'OK 

    Private Sub BindPersonData(ByVal sOid As String, ByVal Gv As GridView, ByVal sType As String)
        sSql = "Select ROW_NUMBER() OVER(ORDER BY p.personoid ASC) AS Nomer, p.personoid, p.nip, p.personname, p.personsex, Isnull(s.salary_premi_hadir, r.salary_premi_hadir) salary_premi_hadir, Isnull(s.salary_premi_extra, r.salary_premi_extra) salary_premi_extra, Isnull(s.salary_gaji_bln, r.salary_gaji_bln) salary_gaji_bln, isnull(s.salary_harian_lk, r.salary_harian_lk) salary_harian_lk, Isnull(s.salary_jumlah_hari_lk, 0) salary_jumlah_hari_lk, ISNULL(s.salary_harian_dk, r.salary_harian_dk) salary_harian_dk, Isnull(s.salary_jumlah_hari_dk, 0) salary_jumlah_hari_dk, Isnull(s.salary_bonus, r.salary_bonus) salary_bonus, Isnull(s.salary_lembur, r.salary_lembur) salary_lembur, Isnull(s.salary_lembur_lk, r.salary_lembur_lk) salary_lembur_lk, 'False' AS CheckValue, Isnull(s.salary_tunjangan_jabatan, r.salary_tunjangan_jabatan) salary_tunjangan_jabatan, Isnull(s.pengurangan_salary, 0.0) pengurangan_salary, isnull(s.salary_jam_lembur, 0) salary_jam_lembur, isnull(s.salary_jam_lembur_lk, 0) salary_jam_lembur_lk, '" & ddlperiodtype.SelectedValue & "' salary_type, Isnull(s.salary_jumlah_hari_gaji, 1) salary_jumlah_hari_gaji, isnull(case when isnull(s.salary_gaji_harian, 0.0)=0 and r.salary_gaji_harian>0 then r.salary_gaji_harian else 0.0 end, r.salary_gaji_harian) salary_gaji_harian, Isnull(case salary_jumlah_hari_gaji_harian when 0 then 1 else salary_jumlah_hari_gaji_harian end, 1) salary_jumlah_hari_gaji_harian, Isnull(s.salary_kerajinan, r.salary_kerajinan) salary_kerajinan, Isnull(s.salary_other, r.salary_other) salary_other, Isnull(s.salary_bonus_pcs, 1) salary_bonus_pcs, r.salary_harian_dk salary_harian_dk_val, r.salary_harian_lk salary_harian_lk_val, r.salary_lembur salary_lembur_val, r.salary_lembur_lk salary_lembur_lk_val From QL_mstperson p Inner Join QL_mstsalary r ON r.personoid=p.personoid Left Join ( Select s.personoid, s.cmpcode, salary_premi_hadir, salary_premi_extra, salary_gaji_bln, salary_harian_lk, salary_jumlah_hari_lk, salary_harian_dk, salary_jumlah_hari_dk,salary_bonus, salary_lembur, salary_lembur_lk, salary_tunjangan_jabatan, pengurangan_salary, salary_jam_lembur, salary_jam_lembur_lk, s.salary_type, s.salary_jumlah_hari_gaji, salary_gaji_harian, salary_jumlah_hari_gaji_harian, salary_kerajinan, salary_other, salary_bonus_pcs from QL_r_salary s Where s.r_salary_month=" & ddlmonth.SelectedValue & " AND r_salary_year=" & ddlyear.SelectedValue & " and s.salary_type='" & ddlperiodtype.SelectedValue & "' ) s ON p.personoid=s.personoid AND s.cmpcode=p.cmpcode Where 1=1"
        If ddlperiodtype.SelectedValue = "MINGGU 1" Then
            'sSql &= " AND (isnull(s.salary_gaji_harian, r.salary_gaji_harian) > 0 or ISNULL(s.salary_harian_dk, r.salary_harian_dk) > 0 or isnull(s.salary_harian_lk, r.salary_harian_lk) > 0)"
        End If

        'WHERE r.salary_type like '" & ddlperiodtype.SelectedValue.Replace(" 1", "").Replace(" 2", "") & "%'"

        If sOid <> "" Then
            sSql &= " AND p.personoid IN (" & sOid & ")"
        End If

        sSql &= " Order By personname"
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstsalary")

        If dtTbl.Rows.Count > 0 Then
            If sType = "show" Then
                Dim HdrText() As String = {"No", "NIK", "Nama Pegawai", "Tunjangan Jabatan", "Premi hadir", "Premi Extra", "Gaji Bulanan", "Gaji Harian", "Luar Kota", "Dalam Kota", "Bonus", "Lembur", "Lembur Luar Kota", "Kerajinan", "Lain"}
                Dim DtField() As String = {"Nomer", "nip", "personname", "salary_tunjangan_jabatan", "salary_premi_hadir", "salary_premi_extra", "salary_gaji_bln", "salary_gaji_harian", "salary_harian_lk", "salary_harian_dk", "salary_bonus", "salary_lembur", "salary_lembur_lk", "salary_kerajinan", "salary_other"}
                Dim arKey() As String = {"Nomer", "nip", "personname", "salary_premi_hadir", "salary_premi_extra", "salary_gaji_bln", "salary_harian_lk", "salary_harian_dk", "salary_jumlah_hari_lk", "salary_jumlah_hari_dk", "salary_bonus", "salary_lembur", "salary_lembur_lk", "salary_tunjangan_jabatan", "personoid", "personsex", "pengurangan_salary", "salary_jam_lembur", "salary_jam_lembur_lk", "salary_type", "salary_jumlah_hari_gaji", "salary_gaji_harian", "salary_jumlah_hari_gaji_harian", "salary_kerajinan", "salary_other", "salary_bonus_pcs", "salary_harian_dk_val", "salary_harian_lk_val", "salary_lembur_val", "salary_lembur_lk_val"}
                Gv.Columns.Clear()
                For C1 As Int16 = 0 To HdrText.Length - 1
                    Dim sfield As New BoundField
                    sfield.HeaderStyle.CssClass = "gvhdr"
                    sfield.HeaderText = HdrText(C1)
                    If DtField(C1) = "nip" Or DtField(C1) = "personname" Then
                        sfield.HeaderStyle.HorizontalAlign = HorizontalAlign.Left
                        sfield.ItemStyle.HorizontalAlign = HorizontalAlign.Left
                    ElseIf DtField(C1) = "Nomer" Then
                        sfield.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                        sfield.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                    Else
                        sfield.HeaderStyle.HorizontalAlign = HorizontalAlign.Right
                        sfield.ItemStyle.HorizontalAlign = HorizontalAlign.Right
                    End If
                    sfield.DataField = DtField(C1)
                    sfield.HeaderStyle.Font.Size = FontUnit.XSmall
                    sfield.ItemStyle.Font.Size = FontUnit.XSmall
                    Gv.Columns.Add(sfield)
                Next
                Gv.DataKeyNames = arKey
                Dim cField As New CommandField
                cField.ShowSelectButton = True
                cField.SelectText = "View data"
                cField.HeaderStyle.CssClass = "gvhdr"
                cField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                Gv.Columns.Add(cField)
            End If

            Gv.DataSource = dtTbl
            Session("DataPerson") = dtTbl
            Session("DataPersonView") = dtTbl
            Gv.DataBind()
        End If
    End Sub 'OK 

    Private Sub GetDataMaster(ByVal isCheked As String, ByVal sType As String)
        sSql = "select salary_premi_hadir, salary_premi_extra from QL_mstsalary where personoid=" & personoid.Text
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstsalary")
        For i As Int16 = 0 To dtTbl.Rows.Count - 1
            If sType = "hadir" And isCheked = "Ya" Then
                salary_premi_hadir.Text = ToMaskEdit(ToDouble(dtTbl.Rows(i)("salary_premi_hadir")), 0)
            ElseIf sType = "extra" And isCheked = "Ya" Then
                salary_premi_extra.Text = ToMaskEdit(ToDouble(dtTbl.Rows(i)("salary_premi_extra")), 0)
            End If
        Next
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Master\HitungGaji.aspx")
        End If
        If checkPagePermission("~\Master\HitungGaji.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Perhitungan Gaji"
        If Not Page.IsPostBack Then
            upduser.Text = Session("UserID") : updtime.Text = GetServerTime().ToString
            'SetAvailablePerson()
            ' Fill DDL MOnth
            ddlmonth.Items.Clear()
            For iMonth As Integer = 1 To 12
                Dim li As New ListItem(MonthName(iMonth).ToUpper, iMonth)
                ddlmonth.Items.Add(li)
            Next
            ddlmonth.SelectedValue = Now.Month
            ' Fill DDL Year
            ddlyear.Items.Clear()
            For iYear As Integer = Now.Year + 2 To Now.Year - 2 Step -1
                ddlyear.Items.Add(iYear)
            Next
            ddlyear.SelectedValue = Now.Year
            'InitDDLPeriod(ddlperiodtype.SelectedValue)
            'btnSave.Visible = False
        End If
    End Sub

    Private Sub btnShowData_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnShowData.Click
        Dim sOid As String = ""
        If cbEmployee.Checked Then
            If lbPersonOid.Items.Count > 0 Then
                For C1 As Integer = 0 To lbPersonOid.Items.Count - 1
                    sOid &= lbPersonOid.Items(C1).Value & ","
                Next
            Else
                showMessage("Please select Person data first!", 2)
                Exit Sub
            End If
        End If
        If sOid <> "" Then
            sOid = Left(sOid, sOid.Length - 1)
        End If
        BindPersonData(sOid, gvMst, "show")
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        If UpdateCheckedPerson() Then
            gvMst.PageIndex = e.NewPageIndex
            gvMst.DataSource = Session("DataPerson")
            gvMst.DataSource = Session("DataPersonView")
            gvMst.DataBind()
        End If
    End Sub

    Private Sub gvMst_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvMst.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(e.Row.Cells(3).Text, 2)
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 2)
            e.Row.Cells(5).Text = ToMaskEdit(e.Row.Cells(5).Text, 2)
            e.Row.Cells(6).Text = ToMaskEdit(e.Row.Cells(6).Text, 2)
            e.Row.Cells(7).Text = ToMaskEdit(e.Row.Cells(7).Text, 2)
            e.Row.Cells(8).Text = ToMaskEdit(e.Row.Cells(8).Text, 2)
            e.Row.Cells(9).Text = ToMaskEdit(e.Row.Cells(9).Text, 2)
            e.Row.Cells(10).Text = ToMaskEdit(e.Row.Cells(10).Text, 2)
            e.Row.Cells(11).Text = ToMaskEdit(e.Row.Cells(11).Text, 2)
            e.Row.Cells(12).Text = ToMaskEdit(e.Row.Cells(12).Text, 2)
            e.Row.Cells(13).Text = ToMaskEdit(e.Row.Cells(13).Text, 2) 
        End If
    End Sub

    Private Sub HitungGaji()
        Dim premi_hadir As Decimal = 0.0 : Dim premi_extra As Decimal = 0.0
        total_salary_harian_lk.Text = ToMaskEdit(ToDouble(salary_harian_lk.Text) * ToDouble(salary_jumlah_hari_lk.Text), 0)
        total_salary_harian_dk.Text = ToMaskEdit(ToDouble(salary_harian_dk.Text) * ToDouble(salary_jumlah_hari_dk.Text), 0)
        total_salary_lembur.Text = ToMaskEdit(ToDouble(salary_lembur.Text) * ToDouble(salary_jam_lembur.Text), 0)
        total_salary_lembur_lk.Text = ToMaskEdit(ToDouble(salary_lembur_lk.Text) * ToDouble(salary_jam_lembur_lk.Text), 0)
        total_salary_gaji_harian.Text = ToMaskEdit(ToDouble(salary_gaji_harian.Text) * ToDouble(salary_jumlah_hari_gaji_harian.Text), 0)
        total_salary_bonus.Text = ToMaskEdit(ToDouble(salary_bonus.Text) * ToDouble(salary_bonus_pcs.Text), 0)

        If isChecked(cb_premi_hadir.Checked) = "Ya" Then
            premi_hadir = ToDouble(salary_premi_hadir.Text)
        End If
        salary_premi_hadir.Text = ToMaskEdit(premi_hadir, 2)

        If isChecked(cb_premi_extra.Checked) = "Ya" Then
            premi_extra = ToDouble(salary_premi_extra.Text)
        End If

        If ddlperiodtype.SelectedValue <> "BULANAN" Then
            total_salary_gaji_bln.Text = ToMaskEdit(ToDouble(salary_gaji_bln.Text) * ToDouble(salary_jumlah_hari_gaji.Text), 0)
        End If
        salary_premi_extra.Text = ToMaskEdit(premi_extra, 2)
        total_salary.Text = ToMaskEdit(ToDouble(premi_hadir) + ToDouble(premi_extra) + ToDouble(total_salary_gaji_bln.Text) + ToDouble(total_salary_gaji_harian.Text) + ToDouble(total_salary_harian_lk.Text) + ToDouble(salary_kerajinan.Text) + ToDouble(salary_tunjangan_jabatan.Text) + ToDouble(total_salary_bonus.Text) + ToDouble(total_salary_lembur.Text) + ToDouble(total_salary_lembur_lk.Text) + ToDouble(total_salary_harian_dk.Text) + ToDouble(salary_other.Text) - ToDouble(pengurangan_salary.Text), 0)
    End Sub

    Protected Sub gvMst_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvMst.SelectedIndexChanged
        Dim isCheck As Boolean = True
        ddlperiodtype.SelectedValue = gvMst.SelectedDataKey.Item("salary_type").ToString()
        'If ddlperiodtype.SelectedValue <> "BULANAN" Then
        '    Txtsalary_gaji_bln.Text = "Gaji harian"
        'Else
        '    salary_jumlah_hari_gaji.Enabled = False
        '    salary_jumlah_hari_gaji.CssClass = "inpTextDisabled"
        'End If
        personoid.Text = gvMst.SelectedDataKey.Item("personoid")
        nip.Text = gvMst.SelectedDataKey.Item("nip")
        personname.Text = gvMst.SelectedDataKey.Item("personname")
        personsex.Text = gvMst.SelectedDataKey.Item("personsex")
        salary_tunjangan_jabatan.Text = ToMaskEdit(gvMst.SelectedDataKey.Item("salary_tunjangan_jabatan"), 0)
        salary_premi_hadir.Text = ToMaskEdit(gvMst.SelectedDataKey.Item("salary_premi_hadir"), 0)
        salary_premi_extra.Text = ToMaskEdit(gvMst.SelectedDataKey.Item("salary_premi_extra"), 0)
        salary_gaji_bln.Text = 0
        salary_jumlah_hari_gaji.Text = ToMaskEdit(gvMst.SelectedDataKey.Item("salary_jumlah_hari_gaji"), 0)
        If gvMst.SelectedDataKey.Item("salary_jumlah_hari_gaji") > 0 Then
            salary_gaji_bln.Text = ToMaskEdit(gvMst.SelectedDataKey.Item("salary_gaji_bln") / gvMst.SelectedDataKey.Item("salary_jumlah_hari_gaji"), 0)
        End If
        total_salary_gaji_bln.Text = ToMaskEdit(ToDouble(salary_gaji_bln.Text) * ToDouble(salary_jumlah_hari_gaji.Text), 0)

        If ddlperiodtype.SelectedValue = "MINGGU 1" Then
            salary_premi_hadir.Text = 0 : salary_premi_extra.Text = 0
            salary_gaji_bln.Text = 0 : total_salary_gaji_bln.Text = 0
            salary_tunjangan_jabatan.Text = 0
        End If
        salary_jumlah_hari_lk.Text = ToMaskEdit(gvMst.SelectedDataKey.Item("salary_jumlah_hari_lk"), 0)
        salary_harian_lk.Text = 0
        If gvMst.SelectedDataKey.Item("salary_jumlah_hari_lk") > 0 Then
            salary_harian_lk.Text = ToMaskEdit(gvMst.SelectedDataKey.Item("salary_harian_lk") / gvMst.SelectedDataKey.Item("salary_jumlah_hari_lk"), 0)
        Else
            salary_harian_lk.Text = ToMaskEdit(gvMst.SelectedDataKey.Item("salary_harian_lk_val"), 0)
        End If
        total_salary_harian_lk.Text = ToMaskEdit(ToDouble(salary_harian_lk.Text) * ToDouble(salary_jumlah_hari_lk.Text), 0)

        salary_jumlah_hari_dk.Text = ToMaskEdit(gvMst.SelectedDataKey.Item("salary_jumlah_hari_dk"), 0)
        salary_harian_dk.Text = 0
        If gvMst.SelectedDataKey.Item("salary_jumlah_hari_dk") > 0 Then
            salary_harian_dk.Text = ToMaskEdit(gvMst.SelectedDataKey.Item("salary_harian_dk") / gvMst.SelectedDataKey.Item("salary_jumlah_hari_dk"), 0)
        Else
            salary_harian_dk.Text = ToMaskEdit(gvMst.SelectedDataKey.Item("salary_harian_dk_val"), 0)
        End If
        total_salary_harian_dk.Text = ToMaskEdit(ToDouble(salary_harian_dk.Text) * ToDouble(salary_jumlah_hari_dk.Text), 0)

        salary_jam_lembur.Text = ToMaskEdit(gvMst.SelectedDataKey.Item("salary_jam_lembur"), 0)
        salary_lembur.Text = 0
        If gvMst.SelectedDataKey.Item("salary_jam_lembur") > 0 Then
            salary_lembur.Text = ToMaskEdit(gvMst.SelectedDataKey.Item("salary_lembur") / gvMst.SelectedDataKey.Item("salary_jam_lembur"), 0)
        Else
            salary_lembur.Text = ToMaskEdit(gvMst.SelectedDataKey.Item("salary_lembur_val"), 0)
        End If
        total_salary_lembur.Text = ToMaskEdit(ToDouble(salary_lembur.Text) * ToDouble(salary_jam_lembur.Text), 0)

        salary_jam_lembur_lk.Text = ToMaskEdit(gvMst.SelectedDataKey.Item("salary_jam_lembur_lk"), 0)
        salary_lembur_lk.Text = 0
        If gvMst.SelectedDataKey.Item("salary_jam_lembur_lk") > 0 Then
            salary_lembur_lk.Text = ToMaskEdit(gvMst.SelectedDataKey.Item("salary_lembur_lk") / gvMst.SelectedDataKey.Item("salary_jam_lembur_lk"), 0)
        Else
            salary_lembur_lk.Text = ToMaskEdit(gvMst.SelectedDataKey.Item("salary_lembur_lk_val"), 0)
        End If
        total_salary_lembur_lk.Text = ToMaskEdit(ToDouble(salary_lembur_lk.Text) * ToDouble(salary_jam_lembur_lk.Text), 0)

        salary_jumlah_hari_gaji_harian.Text = ToMaskEdit(gvMst.SelectedDataKey.Item("salary_jumlah_hari_gaji_harian"), 2)
        salary_gaji_harian.Text = 0
        If gvMst.SelectedDataKey.Item("salary_jumlah_hari_gaji_harian") > 0 Then
            salary_gaji_harian.Text = ToMaskEdit(gvMst.SelectedDataKey.Item("salary_gaji_harian") / gvMst.SelectedDataKey.Item("salary_jumlah_hari_gaji_harian"), 0)
        End If
        total_salary_gaji_harian.Text = ToMaskEdit(ToDouble(salary_gaji_harian.Text) * ToDouble(salary_jumlah_hari_gaji_harian.Text), 0)

        salary_bonus_pcs.Text = ToMaskEdit(gvMst.SelectedDataKey.Item("salary_bonus_pcs"), 0)
        If gvMst.SelectedDataKey.Item("salary_bonus") > 0 Then
            salary_bonus.Text = ToMaskEdit(gvMst.SelectedDataKey.Item("salary_bonus") / gvMst.SelectedDataKey.Item("salary_bonus_pcs"), 0)
        End If
        total_salary_bonus.Text = ToMaskEdit(ToDouble(salary_bonus.Text) * ToDouble(salary_bonus_pcs.Text), 0)

        pengurangan_salary.Text = ToMaskEdit(gvMst.SelectedDataKey.Item("pengurangan_salary"), 0)

        salary_kerajinan.Text = ToMaskEdit(gvMst.SelectedDataKey.Item("salary_kerajinan"), 0)
        salary_other.Text = ToMaskEdit(gvMst.SelectedDataKey.Item("salary_other"), 0)

        If ToDouble(salary_premi_hadir.Text) = 0 Then : isCheck = False : End If
        cb_premi_hadir.Checked = isCheck
        cb_premi_hadir.Text = isChecked(isCheck)

        If ToDouble(salary_premi_extra.Text) = 0 Then : isCheck = False : End If
        cb_premi_extra.Checked = isCheck
        cb_premi_extra.Text = isChecked(isCheck)

        HitungGaji()
        MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub cbEmployee_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbEmployee.CheckedChanged
        If cbEmployee.Checked Then
            btnSearchLP.Visible = True
            btnEraseLP.Visible = True
        Else
            btnSearchLP.Visible = False
            btnEraseLP.Visible = False
        End If
    End Sub

    Protected Sub ddlperiod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlperiod.SelectedIndexChanged
        If ddlperiod.Items.Count > 0 Then
            Dim sPeriod() As String = Split(ddlperiod.SelectedItem.Text, "-")
            If sPeriod.Length > 0 Then
                lblPeriod1.Text = RTrim(sPeriod(0))
                lblPeriod2.Text = LTrim(sPeriod(1))
            End If
            ClearAllSession()
        End If
    End Sub

    Protected Sub ddlmonth_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlmonth.TextChanged
        InitDDLPeriod(ddlperiodtype.SelectedValue)
    End Sub

    Protected Sub ddlperiodtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlperiodtype.SelectedIndexChanged
        If ddlperiodtype.SelectedValue = "MINGGUAN" Then
            FilterDDLSalPeriod.SelectedValue = "D"
        ElseIf ddlperiodtype.SelectedValue = "BULANAN" Then
            FilterDDLSalPeriod.SelectedValue = "M"
        End If
        InitDDLPeriod(ddlperiodtype.SelectedValue)
    End Sub

    Protected Sub btnSearchLP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchLP.Click 
        Session("DataPerson") = Nothing
        Session("DataPersonView") = Nothing
        FilterDDLLP.SelectedIndex = -1
        FilterTextLP.Text = ""
        BindPersonData("", gvLP, "person")
        cProc.SetModalPopUpExtender(btnHideLP, pnlLP, mpeLP, True) 
    End Sub

    Protected Sub btnEraseLP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseLP.Click
        lbPersonOid.Items.Clear()
    End Sub

    Protected Sub btnAddToListLP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToListLP.Click
        If UpdateCheckedPerson() Then
            Dim dtTbl As DataTable = Session("DataPerson")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "CheckValue='True'"
            lbPersonOid.Items.Clear()
            For C1 As Integer = 0 To dtView.Count - 1
                Dim li As New ListItem(dtView(C1)("personname").ToString, dtView(C1)("personoid"))
                lbPersonOid.Items.Add(li)
            Next
        End If
        cProc.SetModalPopUpExtender(btnHideLP, pnlLP, mpeLP, False)
    End Sub

    Protected Sub btnCloseLP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCloseLP.Click
        cProc.SetModalPopUpExtender(btnHideLP, pnlLP, mpeLP, False)
    End Sub

    Protected Sub pengurangan_salary_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles pengurangan_salary.TextChanged
        If sender.Equals(salary_premi_hadir) Then
            salary_premi_hadir.Text = ToMaskEdit(ToDouble(salary_premi_hadir.Text), 0)
        ElseIf sender.Equals(salary_premi_extra) Then
            salary_premi_extra.Text = ToMaskEdit(ToDouble(salary_premi_extra.Text), 0)
        ElseIf sender.Equals(salary_gaji_bln) Then
            salary_gaji_bln.Text = ToMaskEdit(ToDouble(salary_gaji_bln.Text), 0)
        ElseIf sender.Equals(salary_harian_lk) Then
            salary_harian_lk.Text = ToMaskEdit(ToDouble(salary_harian_lk.Text), 0)
        ElseIf sender.Equals(salary_harian_dk) Then
            salary_harian_dk.Text = ToMaskEdit(ToDouble(salary_harian_dk.Text), 0)
        ElseIf sender.Equals(salary_lembur) Then
            salary_lembur.Text = ToMaskEdit(ToDouble(salary_lembur.Text), 0)
        ElseIf sender.Equals(salary_lembur_lk) Then
            salary_lembur_lk.Text = ToMaskEdit(ToDouble(salary_lembur_lk.Text), 0)
        ElseIf sender.Equals(salary_bonus) Then
            salary_bonus.Text = ToMaskEdit(ToDouble(salary_bonus.Text), 0)
        ElseIf sender.Equals(salary_bonus_pcs) Then
            total_salary_gaji_harian.Text = ToMaskEdit(ToDouble(salary_bonus_pcs.Text), 0)
        ElseIf sender.Equals(pengurangan_salary) Then
            pengurangan_salary.Text = ToMaskEdit(ToDouble(pengurangan_salary.Text), 0)
        ElseIf sender.Equals(total_salary) Then
            total_salary.Text = ToMaskEdit(ToDouble(total_salary.Text), 0)
        ElseIf sender.Equals(salary_jumlah_hari_lk) Then
            salary_jumlah_hari_lk.Text = ToMaskEdit(ToDouble(salary_jumlah_hari_lk.Text), 0)
        ElseIf sender.Equals(salary_jumlah_hari_dk) Then
            salary_jumlah_hari_dk.Text = ToMaskEdit(ToDouble(salary_jumlah_hari_dk.Text), 0)
        ElseIf sender.Equals(total_salary_harian_lk) Then
            total_salary_harian_lk.Text = ToMaskEdit(ToDouble(total_salary_harian_lk.Text), 0)
        ElseIf sender.Equals(total_salary_harian_dk) Then
            total_salary_harian_dk.Text = ToMaskEdit(ToDouble(total_salary_harian_dk.Text), 0)
        ElseIf sender.Equals(salary_jam_lembur) Then
            salary_jam_lembur.Text = ToMaskEdit(ToDouble(salary_jam_lembur.Text), 0)
        ElseIf sender.Equals(salary_jam_lembur_lk) Then
            salary_jam_lembur_lk.Text = ToMaskEdit(ToDouble(salary_jam_lembur_lk.Text), 0)
        ElseIf sender.Equals(salary_jumlah_hari_gaji_harian) Then
            salary_jumlah_hari_gaji_harian.Text = ToMaskEdit(ToDouble(salary_jumlah_hari_gaji_harian.Text), 2)
        ElseIf sender.Equals(salary_gaji_harian) Then
            salary_gaji_harian.Text = ToMaskEdit(ToDouble(salary_gaji_harian.Text), 0)
        ElseIf sender.Equals(total_salary_gaji_harian) Then
            total_salary_gaji_harian.Text = ToMaskEdit(ToDouble(total_salary_gaji_harian.Text), 0)
        End If
        HitungGaji()
    End Sub

    Protected Sub btnCancelSalary_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelSalary.Click
        Response.Redirect("~\Master\HitungGaji.aspx")
    End Sub

    Protected Sub btnSaveSalary_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSaveSalary.Click
        Dim premi_hadir As Decimal = 0.0 : Dim premi_extra As Decimal = 0.0
        HitungGaji()
        If isChecked(cb_premi_hadir.Checked) = "Ya" Then
            premi_hadir = ToDouble(salary_premi_hadir.Text)
        End If
        If isChecked(cb_premi_extra.Checked) = "Ya" Then
            premi_extra = ToDouble(salary_premi_extra.Text)
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            If IsPersonExists() = False Then
                sSql = "INSERT INTO QL_r_salary ( cmpcode, r_salary_month, r_salary_year, personoid, salary_premi_hadir, salary_premi_extra, salary_gaji_bln, salary_harian_lk, salary_jumlah_hari_lk, salary_harian_dk, salary_jumlah_hari_dk, salary_bonus, salary_lembur, salary_jam_lembur, salary_lembur_lk, salary_jam_lembur_lk, pengurangan_salary, total_salary, createuser, createtime, salary_tunjangan_jabatan, salary_type, salary_jumlah_hari_gaji, salary_jumlah_hari_gaji_harian, salary_gaji_harian, salary_kerajinan, salary_other, salary_bonus_pcs )" & _
                        " VALUES ( '" & CompnyCode & "' , " & ddlmonth.SelectedValue & ", " & ddlyear.SelectedValue & ", " & personoid.Text & ", " & ToDouble(premi_hadir) & ", " & ToDouble(premi_extra) & ", " & ToDouble(total_salary_gaji_bln.Text) & ", " & ToDouble(total_salary_harian_lk.Text) & ", " & ToDouble(salary_jumlah_hari_lk.Text) & ", " & ToDouble(total_salary_harian_dk.Text) & ", " & ToDouble(salary_jumlah_hari_dk.Text) & ", " & ToDouble(total_salary_bonus.Text) & ", " & ToDouble(total_salary_lembur.Text) & ", " & ToDouble(salary_jam_lembur.Text) & ", " & ToDouble(total_salary_lembur_lk.Text) & ", " & ToDouble(salary_jam_lembur_lk.Text) & ", " & ToDouble(pengurangan_salary.Text) & ", " & ToDouble(total_salary.Text) & ", '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(salary_tunjangan_jabatan.Text) & ", '" & ddlperiodtype.SelectedValue & "', " & salary_jumlah_hari_gaji.Text & ", " & ToDouble(salary_jumlah_hari_gaji_harian.Text) & ", " & ToDouble(total_salary_gaji_harian.Text) & ", " & ToDouble(salary_kerajinan.Text) & ", " & ToDouble(salary_other.Text) & ", " & ToDouble(salary_bonus_pcs.Text) & " ) "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery() 
            Else
                sSql = "UPDATE QL_r_salary SET salary_premi_hadir = " & ToDouble(premi_hadir) & ", salary_premi_extra = " & ToDouble(premi_extra) & ", salary_gaji_bln = " & ToDouble(total_salary_gaji_bln.Text) & ", salary_harian_lk = " & ToDouble(total_salary_harian_lk.Text) & ", salary_jumlah_hari_lk = " & ToDouble(salary_jumlah_hari_lk.Text) & ", salary_harian_dk = " & ToDouble(total_salary_harian_dk.Text) & ", salary_bonus = " & ToDouble(total_salary_bonus.Text) & ", salary_jumlah_hari_dk = " & ToDouble(salary_jumlah_hari_dk.Text) & ", salary_lembur = " & ToDouble(total_salary_lembur.Text) & ", salary_jam_lembur=" & ToDouble(salary_jam_lembur.Text) & ", salary_lembur_lk = " & ToDouble(total_salary_lembur_lk.Text) & ", salary_jam_lembur_lk=" & ToDouble(salary_jam_lembur_lk.Text) & ", pengurangan_salary = " & ToDouble(pengurangan_salary.Text) & ", total_salary = " & ToDouble(total_salary.Text) & ", salary_tunjangan_jabatan=" & ToDouble(salary_tunjangan_jabatan.Text) & ", salary_jumlah_hari_gaji=" & salary_jumlah_hari_gaji.Text & ", salary_jumlah_hari_gaji_harian=" & ToDouble(salary_jumlah_hari_gaji_harian.Text) & ", salary_gaji_harian=" & ToDouble(total_salary_gaji_harian.Text) & ", salary_kerajinan = " & ToDouble(salary_kerajinan.Text) & ", salary_other = " & ToDouble(salary_other.Text) & ", salary_bonus_pcs = " & ToDouble(salary_bonus_pcs.Text) & " WHERE personoid= " & personoid.Text & " AND r_salary_month = " & ddlmonth.SelectedValue & " AND r_salary_year=" & ddlyear.SelectedValue & " AND salary_type='" & ddlperiodtype.SelectedValue & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit()
            xCmd.Connection.Close()
            MultiView1.ActiveViewIndex = 0
            Response.Redirect("~\Master\HitungGaji.aspx")
        Catch ex As Exception
            objTrans.Rollback()
            showMessage(ex.Message, 1)
            xCmd.Connection.Close()
            Exit Sub
        End Try
    End Sub

    Protected Sub cb_premi_hadir_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_premi_hadir.CheckedChanged
        cb_premi_hadir.Text = isChecked(cb_premi_hadir.Checked)
        GetDataMaster(isChecked(cb_premi_hadir.Checked), "hadir")
        HitungGaji()
    End Sub

    Protected Sub cb_premi_extra_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_premi_extra.CheckedChanged
        cb_premi_extra.Text = isChecked(cb_premi_extra.Checked)
        GetDataMaster(isChecked(cb_premi_extra.Checked), "extra")
        HitungGaji()
    End Sub

    Protected Sub btnFindLP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindLP.Click
        If UpdateCheckedPerson() Then
            Dim dtTbl As DataTable = Session("DataPerson")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim sFilter As String = FilterDDLLP.SelectedValue & " LIKE '%" & Tchar(FilterTextLP.Text) & "%'"
            dtView.RowFilter = sFilter
            Session("DataPersonView") = dtView.ToTable
            gvLP.DataSource = Session("DataPersonView")
            gvLP.DataBind()
            dtView.RowFilter = ""
        End If
        mpeLP.Show()
    End Sub

    Protected Sub btnAllLP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllLP.Click
        If UpdateCheckedPerson() Then
            FilterDDLLP.SelectedIndex = -1
            FilterTextLP.Text = "" 
            Session("DataPersonView") = Session("DataPerson")
            gvLP.DataSource = Session("DataPersonView")
            gvLP.DataBind()
        End If
        mpeLP.Show()
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("DataPersonView") Is Nothing Then
            Dim dtTbl As DataTable = Session("DataPersonView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("DataPerson")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "personoid=" & dtTbl.Rows(C1)("personoid")
                    objView(0)("CheckValue") = "True"
                    dtTbl.Rows(C1)("CheckValue") = "True"
                    objView.RowFilter = ""
                Next
                Session("DataPerson") = objView.ToTable
                Session("DataPersonView") = dtTbl
                gvLP.DataSource = Session("DataPersonView")
                gvLP.DataBind()
            End If
        End If
        mpeLP.Show()
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("DataPersonView") Is Nothing Then
            Dim dtTbl As DataTable = Session("DataPersonView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("DataPerson")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "personoid=" & dtTbl.Rows(C1)("personoid")
                    objView(0)("CheckValue") = "False"
                    dtTbl.Rows(C1)("CheckValue") = "False"
                    objView.RowFilter = ""
                Next
                Session("DataPerson") = objView.ToTable
                Session("DataPersonView") = dtTbl
                gvLP.DataSource = Session("DataPersonView")
                gvLP.DataBind()
            End If
        End If
        mpeLP.Show()
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If UpdateCheckedPerson() Then
            Dim dtTbl As DataTable = Session("DataPerson")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "CheckValue='True'"
            Session("DataPersonView") = dtView.ToTable
            gvLP.DataSource = Session("DataPersonView")
            gvLP.DataBind()
            dtView.RowFilter = ""
        End If
        mpeLP.Show()
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

#End Region
End Class
