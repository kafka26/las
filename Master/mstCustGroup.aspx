﻿<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="mstCustGroup.aspx.vb" Inherits="Master_custgroup" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2" style="height: 38px">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Customer Group" CssClass="Title" ForeColor="Black"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Customer Group :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left>Filter</TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="125px"><asp:ListItem Value="custgroupcode">Code</asp:ListItem>
<asp:ListItem Value="custgroupname">Name</asp:ListItem>
<asp:ListItem Value="custgroupaddr">Address</asp:ListItem>
    <asp:ListItem Value="g2.gendesc">City</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="200px"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbTagType" runat="server" Text="Tag Type" Width="80px" Visible="False"></asp:CheckBox></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLTagType" runat="server" CssClass="inpText" Width="125px" Visible="False" AutoPostBack="True"><asp:ListItem Value="Raw">Raw Material</asp:ListItem>
<asp:ListItem Value="General">General Material</asp:ListItem>
<asp:ListItem Value="Spare Part">Spare Part</asp:ListItem>
<asp:ListItem Value="Finish Good">Finish Good</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbTag" runat="server" Text="Tag" Visible="False"></asp:CheckBox></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLTag" runat="server" CssClass="inpText" Width="200px" Visible="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="125px"><asp:ListItem>ACTIVE</asp:ListItem>
<asp:ListItem>INACTIVE</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnExportPDF" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnExportExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvMst" runat="server" BorderColor="#DEDFDE" ForeColor="Black" Width="100%" OnSelectedIndexChanged="gvMst_SelectedIndexChanged" AllowSorting="True" PageSize="8" AllowPaging="True" CellPadding="4" BackColor="White" BorderWidth="1px" BorderStyle="None" AutoGenerateColumns="False" GridLines="None">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#F7F7DE" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="custgroupoid" DataNavigateUrlFormatString="~/Master/mstCustgroup.aspx?oid={0}" DataTextField="custgroupcode" HeaderText="Code" SortExpression="custgroupcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="custgroupname" HeaderText="Name" SortExpression="custgroupname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custgroupaddr" HeaderText="Address" SortExpression="custgroupaddr">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custgroupcity" HeaderText="City" SortExpression="custgroupcity">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custgroupphone" HeaderText="Phone" SortExpression="custgroupphone">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custgroupcp" HeaderText="Contact Person" SortExpression="custgroupcp">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="activeflag" HeaderText="Status" SortExpression="activeflag">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#CCCC99"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White"></SelectedRowStyle>

<HeaderStyle BackColor="#6B696B" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view general data"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=4><asp:UpdateProgress id="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image21" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnExportPDF"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnExportExcel"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="I_U" runat="server" CssClass="Important" Text="New Data" Width="130px"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left>&nbsp;<asp:Label id="custgroupoid" runat="server" Text="custgroupoid" Visible="False"></asp:Label></TD><TD style="WIDTH: 14%" class="Label" align=left></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="custoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblCode" runat="server" Text="Code"></asp:Label> <asp:Label id="StarCode" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custgroupcode" runat="server" CssClass="inpTextDisabled" Width="96px" MaxLength="30" Enabled="False"></asp:TextBox> <asp:TextBox id="CodeCust" runat="server" CssClass="inpText" Width="50px" Visible="False" MaxLength="10" AutoPostBack="True" OnTextChanged="custgroupname_TextChanged"></asp:TextBox> <asp:TextBox id="custcode" runat="server" CssClass="inpTextDisabled" Width="96px" Visible="False" MaxLength="10" Enabled="False" AutoPostBack="True" OnTextChanged="custgroupname_TextChanged" __designer:wfdid="w263"></asp:TextBox></TD><TD class="Label" align=left colSpan=3><asp:CheckBox id="cbCustomer" runat="server" Text="Single Customer"></asp:CheckBox> <asp:DropDownList id="custcurrdefaultoid" runat="server" CssClass="inpText" Width="105px" Visible="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblName" runat="server" Text="Name"></asp:Label> <asp:Label id="Label2" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="custgroupprefixoid" runat="server" CssClass="inpText" Width="65px"></asp:DropDownList> <asp:TextBox id="custgroupname" runat="server" CssClass="inpText" Width="200px" MaxLength="500" AutoPostBack="True" OnTextChanged="custgroupname_TextChanged"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="lblType" runat="server" Text="Type"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="custgrouptype" runat="server" CssClass="inpText" Width="105px"><asp:ListItem>LOCAL</asp:ListItem>
<asp:ListItem>EXPORT</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 22px" class="Label" align=left><asp:Label id="lblTolerance" runat="server" Text="Taxable"></asp:Label></TD><TD style="HEIGHT: 22px" class="Label" align=center>:</TD><TD style="HEIGHT: 22px" class="Label" align=left><asp:DropDownList id="custgrouptaxable" runat="server" CssClass="inpText" Width="105px"><asp:ListItem Value="1">TAX</asp:ListItem>
<asp:ListItem Value="0">NON TAX</asp:ListItem>
</asp:DropDownList></TD><TD style="HEIGHT: 22px" class="Label" align=left><asp:Label id="Label4" runat="server" Text="Default TOP"></asp:Label></TD><TD style="HEIGHT: 22px" class="Label" align=center>:</TD><TD style="HEIGHT: 22px" class="Label" align=left><asp:DropDownList id="custgrouppaymentoid" runat="server" CssClass="inpText" Width="155px"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblAddress" runat="server" Text="Address"></asp:Label> <asp:Label id="Label6" runat="server" Text="Currency" Visible="False"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custgroupaddr" runat="server" CssClass="inpText" Width="272px" MaxLength="500" Height="64px" TextMode="MultiLine"></asp:TextBox> </TD><TD class="Label" align=left><asp:Label id="lblNPWP" runat="server" Text="NPWP"></asp:Label></TD><TD class="Label" align=center><asp:Label id="NPWPlbl" runat="server" Text=":"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="custgroupnpwp" runat="server" CssClass="inpText" Width="150px" MaxLength="30"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label21" runat="server" Text="Kode Komisi" __designer:wfdid="w1"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="kode_komisi" runat="server" CssClass="inpText" Width="272px" __designer:wfdid="w2"><asp:ListItem Value="JPU">Jual Putus (JPU)</asp:ListItem>
<asp:ListItem Value="KTS">Konsinasi Tanpa SPG (KTS)</asp:ListItem>
<asp:ListItem Value="KSG">Konsinasi SPG
 (KSG)</asp:ListItem>
<asp:ListItem Value="LP">Luar Pulau (LP)</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblPhone1" runat="server" Text="Phone 1"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custgroupphone1" runat="server" CssClass="inpText" Width="100px" MaxLength="20"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="lblCity" runat="server" Text="City"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="custgroupcityoid" runat="server" CssClass="inpText" Width="205px" AutoPostBack="True" OnSelectedIndexChanged="custgroupcityoid_SelectedIndexChanged"></asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 20px" class="Label" align=left><asp:Label id="lblPhone2" runat="server" Text="Phone 2"></asp:Label></TD><TD style="HEIGHT: 20px" class="Label" align=center>:</TD><TD style="HEIGHT: 20px" class="Label" align=left><asp:TextBox id="custgroupphone2" runat="server" CssClass="inpText" Width="100px" MaxLength="20"></asp:TextBox></TD><TD style="HEIGHT: 20px" class="Label" align=left><asp:Label id="lblFax1" runat="server" Text="Fax 1"></asp:Label></TD><TD style="HEIGHT: 20px" class="Label" align=center>:</TD><TD style="HEIGHT: 20px" class="Label" align=left><asp:TextBox id="custgroupfax1" runat="server" CssClass="inpText" Width="100px" MaxLength="20"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label13" runat="server" Text="Phone 3"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custgroupphone3" runat="server" CssClass="inpText" Width="100px" MaxLength="20"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="lblFax2" runat="server" Text="Fax 2"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custgroupfax2" runat="server" CssClass="inpText" Width="100px" MaxLength="20"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblEmail" runat="server" Text="Email"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custgroupemail" runat="server" CssClass="inpText" Width="150px" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblCP1Name" runat="server" Text="CP 1 Name"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custgroupcp1name" runat="server" CssClass="inpText" Width="200px" MaxLength="50"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="lblWebsite" runat="server" Text="Website"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custgroupwebsite" runat="server" CssClass="inpText" Width="150px" MaxLength="100"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label9" runat="server" Text="CP 1 Phone"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custgroupcp1phone" runat="server" CssClass="inpText" Width="100px" MaxLength="20"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label8" runat="server" Text="CP 2 Name"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custgroupcp2name" runat="server" CssClass="inpText" Width="200px" MaxLength="50"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label11" runat="server" Text="CP 1 Email"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custgroupcp1email" runat="server" CssClass="inpText" Width="150px" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label10" runat="server" Text="CP 2 Phone" Width="71px"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custgroupcp2phone" runat="server" CssClass="inpText" Width="100px" MaxLength="20"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblNote" runat="server" Text="Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custgroupnote" runat="server" CssClass="inpText" Width="260px" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label12" runat="server" Text="CP 2 Email"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custgroupcp2email" runat="server" CssClass="inpText" Width="150px" MaxLength="100"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label19" runat="server" Text="Def. Disc. 1" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="disc1" runat="server" CssClass="inpText" Width="63px" Visible="False" MaxLength="20"></asp:TextBox> <asp:Label id="Label20" runat="server" Text="%" Visible="False"></asp:Label> <asp:TextBox id="disc3" runat="server" CssClass="inpText" Width="63px" Visible="False" MaxLength="20"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="lblStatus" runat="server" Text="Status"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="activeflag" runat="server" CssClass="inpText" Width="105px"><asp:ListItem>ACTIVE</asp:ListItem>
<asp:ListItem>INACTIVE</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="disc2" runat="server" CssClass="inpText" Width="63px" Visible="False" MaxLength="20"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text="Credit Limit Rupiah"></asp:Label> <asp:Label id="Label7" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custcreditlimitrupiah" runat="server" CssClass="inpText" Width="150px" MaxLength="10" AutoPostBack="True" OnTextChanged="custcreditlimitrupiah_TextChanged"></asp:TextBox> </TD><TD class="Label" align=left><asp:Label id="Label5" runat="server" Text="Credit Limit USD" Width="96px"></asp:Label> <asp:Label id="Label14" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custcreditlimitusd" runat="server" CssClass="inpText" Width="150px" MaxLength="10" AutoPostBack="True" OnTextChanged="custcreditlimitusd_TextChanged"></asp:TextBox> </TD></TR><TR><TD class="Label" align=left><asp:Label id="Label15" runat="server" Text="Credit Limit Usage Rupiah" Width="153px"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custcreditlimitusagerupiah" runat="server" CssClass="inpTextDisabled" Width="150px" MaxLength="100" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label17" runat="server" Text="Credit Limit Usage USD"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custcreditlimitusageusd" runat="server" CssClass="inpTextDisabled" Width="150px" MaxLength="100" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label16" runat="server" Text="Sisa Credit Limit Rupiah"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custcreditlimitsisarupiah" runat="server" CssClass="inpTextDisabled" Width="150px" MaxLength="100" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label18" runat="server" Text="Sisa Credit Limit USD" Width="133px"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custcreditlimitsisausd" runat="server" CssClass="inpTextDisabled" Width="150px" MaxLength="100" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label23" runat="server" Visible="False">Tag</asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=4><asp:Panel id="pnlTag" runat="server" CssClass="inpText" Width="100%" Visible="False" Height="100px" ScrollBars="Vertical"><asp:CheckBoxList id="custgroupres3" runat="server" CssClass="inpText" Width="97%" RepeatColumns="3"></asp:CheckBoxList></asp:Panel> </TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="ftbPhone1" runat="server" ValidChars=" 1234567890-+*#()" TargetControlID="custgroupphone1">
            </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbPhone2" runat="server" ValidChars=" 1234567890-+*#()" TargetControlID="custgroupphone2">
            </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbPhone3" runat="server" ValidChars=" 1234567890-+*#()" TargetControlID="custgroupphone3">
            </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbFax1" runat="server" ValidChars=" 1234567890-+*#()" TargetControlID="custgroupfax1">
            </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbFax2" runat="server" ValidChars=" 1234567890-+*#()" TargetControlID="custgroupfax2">
            </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbPhoneCP1" runat="server" ValidChars=" 1234567890-+*#()" TargetControlID="custgroupcp1phone">
            </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbPhoneCP2" runat="server" ValidChars=" 1234567890-+*#()" TargetControlID="custgroupcp2phone">
            </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftecreditlimitrp" runat="server" ValidChars="1234567890,." TargetControlID="custcreditlimitrupiah"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftecreditlimitusd" runat="server" ValidChars="1234567890,." TargetControlID="custcreditlimitusd"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbcustgroupcode" runat="server" TargetControlID="custgroupcode" FilterMode="InvalidChars" InvalidChars=" "></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbDisc1" runat="server" ValidChars="1234567890,." TargetControlID="disc1"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbDisc2" runat="server" ValidChars="1234567890,." TargetControlID="disc2"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbDisc3" runat="server" ValidChars="1234567890,." TargetControlID="disc3"></ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=6></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Create By <asp:Label id="createuser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Customer Group:.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

