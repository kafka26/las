<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="HitungGaji.aspx.vb" Inherits="Master_HitungGaji" title="" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Perhitungan Gaji" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            &nbsp;<strong><span style="font-size: 9pt">Form Perhitungan Gaji&nbsp; :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:MultiView id="MultiView1" runat="server" ActiveViewIndex="0"><asp:View id="View1" runat="server"><asp:Panel id="Panel1" runat="server" Width="100%"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=7><asp:Label id="lblPeriod1" runat="server" Text="lblPeriod1" Visible="False"></asp:Label> <asp:Label id="lblPeriod2" runat="server" Text="lblPeriod2" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 12%" class="Label" align=left><asp:Label id="Label2" runat="server" Text="Month"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD style="WIDTH: 30%" class="Label" align=left colSpan=2><asp:DropDownList id="ddlmonth" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True"></asp:DropDownList></TD><TD style="WIDTH: 12%" class="Label" align=left colSpan=1><asp:Label id="Label3" runat="server" Text="Year"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center colSpan=1>:</TD><TD class="Label" align=left colSpan=1><asp:DropDownList id="ddlyear" runat="server" CssClass="inpText" Width="75px" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblPeriod" runat="server" Text="Period Type"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="ddlperiodtype" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True"><asp:ListItem Value="MINGGU 1">MINGGU 1</asp:ListItem>
<asp:ListItem Value="MINGGU 2">MINGGU 2</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left colSpan=1><asp:Label id="Label5" runat="server" Text="Salary Period" Visible="False"></asp:Label></TD><TD class="Label" align=center colSpan=1></TD><TD class="Label" align=left colSpan=1><asp:DropDownList id="FilterDDLSalPeriod" runat="server" CssClass="inpTextDisabled" Width="100px" Visible="False" AutoPostBack="True"><asp:ListItem Value="D">HARIAN</asp:ListItem>
<asp:ListItem Enabled="False" Value="W">MINGGUAN</asp:ListItem>
<asp:ListItem Value="M">BULANAN</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbEmployee" runat="server" Text="Employee (s)" AutoPostBack="True"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:ListBox id="lbPersonOid" runat="server" CssClass="inpTextDisabled" Width="200px" Rows="2"></asp:ListBox>&nbsp;<asp:ImageButton id="btnSearchLP" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseLP" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD><TD class="Label" align=left colSpan=1></TD><TD class="Label" align=center colSpan=1></TD><TD class="Label" align=left colSpan=1><asp:DropDownList id="ddlperiod" runat="server" CssClass="inpTextDisabled" Width="150px" Visible="False" AutoPostBack="True" Enabled="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><asp:ImageButton id="btnShowData" runat="server" ImageUrl="~/Images/showdata.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD class="Label" align=left colSpan=1></TD><TD class="Label" align=center colSpan=1></TD><TD class="Label" align=left colSpan=1></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2></TD><TD class="Label" align=left colSpan=1></TD><TD class="Label" align=center colSpan=1></TD><TD class="Label" align=left colSpan=1></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=7><asp:GridView id="gvMst" runat="server" Width="100%" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True" PageSize="8">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                                <asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label> 
                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD class="Label" align=left colSpan=7></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=7></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left colSpan=7>Processed By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=7></TD></TR><TR><TD class="Label" align=center colSpan=7><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image ID="Image11" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><br />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </DIV></TD></TR></TBODY></TABLE></asp:Panel> </asp:View> <asp:View id="View2" runat="server"><TABLE width=800><TBODY><TR><TD colSpan=6><asp:Label id="personoid" runat="server" Visible="False">0</asp:Label></TD></TR><TR><TD>NIP</TD><TD>:</TD><TD><asp:Label id="nip" runat="server" Text="nip"></asp:Label></TD><TD></TD><TD></TD><TD></TD></TR><TR><TD>Nama Lengkap</TD><TD>:</TD><TD><asp:Label id="personname" runat="server" Text="personname"></asp:Label></TD><TD>Gender</TD><TD>:</TD><TD><asp:Label id="personsex" runat="server" Text="personsex"></asp:Label></TD></TR><TR><TD>Premi Hadir</TD><TD>:</TD><TD><asp:TextBox id="salary_premi_hadir" runat="server" CssClass="inpTextDisabled" Width="100px" AutoPostBack="True" MaxLength="10" OnTextChanged="pengurangan_salary_TextChanged" ReadOnly="True"></asp:TextBox> <asp:CheckBox id="cb_premi_hadir" runat="server" Text="Ya" AutoPostBack="True" Checked="True"></asp:CheckBox> <ajaxToolkit:FilteredTextBoxExtender id="ftePremiHadir" runat="server" TargetControlID="salary_premi_hadir" ValidChars="1234567890.,">
                </ajaxToolkit:FilteredTextBoxExtender> </TD><TD>Premi Extra</TD><TD>:</TD><TD><asp:TextBox id="salary_premi_extra" runat="server" CssClass="inpTextDisabled" Width="100px" AutoPostBack="True" OnTextChanged="pengurangan_salary_TextChanged" ReadOnly="True"></asp:TextBox><asp:CheckBox id="cb_premi_extra" runat="server" Text="Ya" AutoPostBack="True" Checked="True"></asp:CheckBox> <ajaxToolkit:FilteredTextBoxExtender id="ftePremiExtra" runat="server" TargetControlID="salary_premi_extra" ValidChars="1234567890.,">
                </ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD><asp:Label id="Txtsalary_gaji_bln" runat="server" Text="Gaji Bulanan" Width="113px"></asp:Label></TD><TD>:</TD><TD><asp:TextBox id="salary_gaji_bln" runat="server" CssClass="inpTextDisabled" Width="100px" AutoPostBack="True" Enabled="False" MaxLength="10" OnTextChanged="pengurangan_salary_TextChanged"></asp:TextBox>&nbsp;- <asp:TextBox id="salary_jumlah_hari_gaji" runat="server" CssClass="inpTextDisabled" Width="50px" AutoPostBack="True" Enabled="False" MaxLength="10" OnTextChanged="pengurangan_salary_TextChanged"></asp:TextBox>&nbsp;Hari<ajaxToolkit:FilteredTextBoxExtender id="ftesalary_jumlah_hari_gaji" runat="server" TargetControlID="salary_jumlah_hari_gaji" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftetotal_salary_gaji_bln" runat="server" TargetControlID="total_salary_gaji_bln" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender></TD><TD>Tunjangan Jabatan</TD><TD>:</TD><TD><asp:TextBox id="salary_tunjangan_jabatan" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" MaxLength="10" OnTextChanged="pengurangan_salary_TextChanged"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="fteTunjangan" runat="server" TargetControlID="salary_tunjangan_jabatan" ValidChars="1234567890.,">
                    </ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD>Total Gaji Bulanan</TD><TD>:</TD><TD><asp:TextBox id="total_salary_gaji_bln" runat="server" CssClass="inpTextDisabled" Width="100px" AutoPostBack="True" MaxLength="10" OnTextChanged="pengurangan_salary_TextChanged" ReadOnly="True"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftegajibulanan" runat="server" TargetControlID="salary_gaji_bln" ValidChars="1234567890.,">
                </ajaxToolkit:FilteredTextBoxExtender></TD><TD>Bonus</TD><TD>:</TD><TD><asp:TextBox id="salary_bonus" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" MaxLength="10" OnTextChanged="pengurangan_salary_TextChanged">0</asp:TextBox> - <asp:TextBox id="salary_bonus_pcs" runat="server" CssClass="inpText" Width="50px" AutoPostBack="True" MaxLength="10" OnTextChanged="pengurangan_salary_TextChanged">1</asp:TextBox> Pcs<ajaxToolkit:FilteredTextBoxExtender id="fteBonus" runat="server" TargetControlID="salary_bonus" ValidChars="1234567890.,">
                </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftesalary_bonus_pcs" runat="server" TargetControlID="salary_bonus_pcs" ValidChars="1234567890.,">
                </ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD>Kerajinan</TD><TD>:</TD><TD><asp:TextBox id="salary_kerajinan" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" MaxLength="10" OnTextChanged="pengurangan_salary_TextChanged">0</asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftesalary_kerajinan" runat="server" TargetControlID="salary_kerajinan" ValidChars="1234567890.,">
            </ajaxToolkit:FilteredTextBoxExtender> </TD><TD>Total Bonus</TD><TD>:</TD><TD><asp:TextBox id="total_salary_bonus" runat="server" CssClass="inpTextDisabled" Width="100px" AutoPostBack="True" MaxLength="10" OnTextChanged="pengurangan_salary_TextChanged" ReadOnly="True"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftetotal_salary_bonus" runat="server" TargetControlID="total_salary_bonus" ValidChars="1234567890.,">
            </ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD><asp:Label id="Label4" runat="server" Text="Gaji Harian" Width="113px"></asp:Label></TD><TD>:</TD><TD><asp:TextBox id="salary_gaji_harian" runat="server" CssClass="inpTextDisabled" Width="100px" AutoPostBack="True" MaxLength="10" OnTextChanged="pengurangan_salary_TextChanged" ReadOnly="True"></asp:TextBox> - <asp:TextBox id="salary_jumlah_hari_gaji_harian" runat="server" CssClass="inpText" Width="50px" AutoPostBack="True" MaxLength="10" OnTextChanged="pengurangan_salary_TextChanged"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftesalary_gaji_harian" runat="server" TargetControlID="salary_gaji_harian" ValidChars="1234567890.,">
            </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftesalary_jumlah_hari_gaji_harian" runat="server" TargetControlID="salary_jumlah_hari_gaji_harian" ValidChars="1234567890.,">
            </ajaxToolkit:FilteredTextBoxExtender> </TD><TD>Lembur</TD><TD>:</TD><TD><asp:TextBox id="salary_lembur" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" MaxLength="10" OnTextChanged="pengurangan_salary_TextChanged"></asp:TextBox> - <asp:TextBox id="salary_jam_lembur" runat="server" CssClass="inpText" Width="50px" AutoPostBack="True" MaxLength="10" OnTextChanged="pengurangan_salary_TextChanged"></asp:TextBox> Jam<ajaxToolkit:FilteredTextBoxExtender id="ftesalary_jam_lembur" runat="server" TargetControlID="salary_jam_lembur" ValidChars="1234567890.,">
                </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fteLembur" runat="server" TargetControlID="salary_lembur" ValidChars="1234567890.,">
                </ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD>Total gaji Harian</TD><TD>:</TD><TD><asp:TextBox id="total_salary_gaji_harian" runat="server" CssClass="inpTextDisabled" Width="100px" AutoPostBack="True" MaxLength="10" OnTextChanged="pengurangan_salary_TextChanged" ReadOnly="True"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftetotal_salary_gaji_harian" runat="server" TargetControlID="total_salary_gaji_harian" ValidChars="1234567890.,">
            </ajaxToolkit:FilteredTextBoxExtender> </TD><TD>Total Lembur</TD><TD>:</TD><TD><asp:TextBox id="total_salary_lembur" runat="server" CssClass="inpTextDisabled" Width="100px" AutoPostBack="True" MaxLength="10" OnTextChanged="pengurangan_salary_TextChanged" ReadOnly="True"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="fteTotal_salary_lembur" runat="server" TargetControlID="Total_salary_lembur" ValidChars="1234567890.,">
                    </ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD><asp:Label id="TxtHarianluarkota" runat="server" Text="Harian luar kota" Width="100px"></asp:Label></TD><TD>:</TD><TD><asp:TextBox id="salary_harian_lk" runat="server" CssClass="inpTextDisabled" Width="100px" AutoPostBack="True" Enabled="False" MaxLength="10" OnTextChanged="pengurangan_salary_TextChanged"></asp:TextBox> - <asp:TextBox id="salary_jumlah_hari_lk" runat="server" CssClass="inpText" Width="50px" AutoPostBack="True" MaxLength="10" OnTextChanged="pengurangan_salary_TextChanged"></asp:TextBox> Hari<ajaxToolkit:FilteredTextBoxExtender id="ftesalary_harian_lk" runat="server" TargetControlID="salary_harian_lk" ValidChars="1234567890.,">
                </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftesalary_jumlah_hari_lk" runat="server" TargetControlID="salary_jumlah_hari_lk" ValidChars="1234567890.,">
                </ajaxToolkit:FilteredTextBoxExtender> </TD><TD><asp:Label id="TxtHariandalamkota" runat="server" Text="Harian dalam kota" Width="113px"></asp:Label></TD><TD>:</TD><TD><asp:TextBox id="salary_harian_dk" runat="server" CssClass="inpTextDisabled" Width="100px" AutoPostBack="True" Enabled="False" MaxLength="10" OnTextChanged="pengurangan_salary_TextChanged"></asp:TextBox> - <asp:TextBox id="salary_jumlah_hari_dk" runat="server" CssClass="inpText" Width="50px" AutoPostBack="True" MaxLength="10" OnTextChanged="pengurangan_salary_TextChanged"></asp:TextBox> Hari <ajaxToolkit:FilteredTextBoxExtender id="fteDalamKota" runat="server" TargetControlID="salary_harian_dk" ValidChars="1234567890.,">
                </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftesalary_jumlah_hari_dk" runat="server" TargetControlID="salary_jumlah_hari_dk" ValidChars="1234567890.,">
                </ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD>Total Luar Kota</TD><TD>:</TD><TD><asp:TextBox id="total_salary_harian_lk" runat="server" CssClass="inpTextDisabled" Width="100px" AutoPostBack="True" MaxLength="10" OnTextChanged="pengurangan_salary_TextChanged" ReadOnly="True"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftetotal_salary_harian_lk" runat="server" TargetControlID="total_salary_harian_lk" ValidChars="1234567890.,">
                    </ajaxToolkit:FilteredTextBoxExtender> </TD><TD>Total Dalam Kota</TD><TD>:</TD><TD><asp:TextBox id="total_salary_harian_dk" runat="server" CssClass="inpTextDisabled" Width="100px" AutoPostBack="True" MaxLength="10" OnTextChanged="pengurangan_salary_TextChanged" ReadOnly="True"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftetotal_salary_harian_dk" runat="server" TargetControlID="total_salary_harian_dk" ValidChars="1234567890.,">
                </ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD>Lain - Lain</TD><TD>:</TD><TD><asp:TextBox id="salary_other" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" MaxLength="10" OnTextChanged="pengurangan_salary_TextChanged"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftesalary_other" runat="server" TargetControlID="pengurangan_salary" ValidChars="1234567890.,">
                    </ajaxToolkit:FilteredTextBoxExtender> </TD><TD>Potongan</TD><TD>:</TD><TD><asp:TextBox id="pengurangan_salary" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" MaxLength="10"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftepengurangan_salary" runat="server" TargetControlID="pengurangan_salary" ValidChars="1234567890.,">
                            </ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD>Lembur Luar Kota</TD><TD>:</TD><TD><asp:TextBox id="salary_lembur_lk" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" MaxLength="10" OnTextChanged="pengurangan_salary_TextChanged"></asp:TextBox>&nbsp;- <asp:TextBox id="salary_jam_lembur_lk" runat="server" CssClass="inpText" Width="50px" AutoPostBack="True" MaxLength="10" OnTextChanged="pengurangan_salary_TextChanged"></asp:TextBox>&nbsp;Jam<ajaxToolkit:FilteredTextBoxExtender id="ftesalary_jam_lembur_lk" runat="server" TargetControlID="salary_jam_lembur_lk" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender></TD><TD>Total Lembur</TD><TD>:</TD><TD><asp:TextBox id="total_salary_lembur_lk" runat="server" CssClass="inpTextDisabled" Width="100px" AutoPostBack="True" MaxLength="10" OnTextChanged="pengurangan_salary_TextChanged" ReadOnly="True"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="fteTotal_salary_lembur_lk" runat="server" TargetControlID="total_salary_lembur_lk" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD>Total Salary</TD><TD>:</TD><TD><asp:TextBox id="total_salary" runat="server" CssClass="inpTextDisabled" Width="100px" MaxLength="10" OnTextChanged="pengurangan_salary_TextChanged" ReadOnly="True"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftetotal_salary" runat="server" TargetControlID="total_salary" ValidChars="1234567890.,">
                    </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fteLembur_lk" runat="server" TargetControlID="salary_lembur_lk" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> </TD><TD></TD><TD></TD><TD></TD></TR><TR><TD colSpan=6><asp:ImageButton id="btnSaveSalary" runat="server" ImageUrl="~/Images/Update.png" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancelSalary" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton>&nbsp;</TD></TR></TBODY></TABLE></asp:View> </asp:MultiView> 
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
    </table>
    &nbsp;
    <asp:UpdatePanel id="upLP" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlLP" runat="server" CssClass="modalBox" Width="600px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center><asp:Label id="lblTitleLP" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Employee"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center></TD></TR><TR><TD class="Label" align=center><asp:Panel id="pnlFindLP" runat="server" Width="100%" DefaultButton="btnFindLP">
                                <table style="width: 70%">
                                    <tr>
                                        <td align="left" class="Label" style="width: 23%">
                                            <asp:Label ID="lblFilterLP" runat="server" Text="Filter"></asp:Label></td>
                                        <td align="center" class="Label" style="width: 2%">
                                            :</td>
                                        <td align="left" class="Label">
                                            <asp:DropDownList ID="FilterDDLLP" runat="server" CssClass="inpText" Width="100px">
                                                <asp:ListItem Value="nip">NIP</asp:ListItem>
                                                <asp:ListItem Value="personname">Name</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox ID="FilterTextLP" runat="server" CssClass="inpText" Width="170px"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="Label" style="width: 23%">
                                        </td>
                                        <td align="center" class="Label" style="width: 2%">
                                        </td>
                                        <td align="left" class="Label">
                                            <asp:ImageButton ID="btnFindLP" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/btnfind.bmp" />
                                            <asp:ImageButton ID="btnAllLP" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewall.png" /></td>
                                    </tr>
                                </table>
                            </asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center><asp:ImageButton id="btnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNone" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewChecked" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center></TD></TR><TR><TD class="Label" align=center></TD></TR><TR><TD class="Label" vAlign=top align=center><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset4"><DIV id="Div4"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="gvLP" runat="server" ForeColor="#333333" Width="97%" AutoGenerateColumns="False" CellPadding="4" GridLines="None" EmptyDataRowStyle-ForeColor="Red">
<RowStyle BackColor="#EFF3FB"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:BoundField DataField="Nomer" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="nip" HeaderText="NIP">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="personname" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="White" Width="30%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbSelectPerson" runat="server" Checked='<%# eval("CheckValue") %>' ToolTip='<%# eval("personoid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                                            <asp:Label ID="Label19" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label>
                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" BorderColor="White" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center></TD></TR><TR><TD class="Label" align=center><asp:ImageButton id="btnAddToListLP" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnCloseLP" runat="server" ImageUrl="~/Images/Close.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeLP" runat="server" TargetControlID="btnHideLP" PopupDragHandleControlID="lblTitleLP" PopupControlID="pnlLP" Drag="True" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideLP" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upPopUpMsg" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False">
                <table>
                    <tbody>
                        <tr>
                            <td colspan="2" style="background-color: #cc0000; text-align: left">
                                <asp:Label ID="lblCaption" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="White"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="height: 10px">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Image ID="imIcon" runat="server" Height="24px" ImageUrl="~/Images/error.jpg"
                                    Width="24px" /></td>
                            <td class="Label" style="text-align: left">
                                <asp:Label ID="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="height: 10px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center">
                                &nbsp;<asp:ImageButton ID="imbOKPopUpMsg" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/ok.png" /></td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpePopUpMsg" runat="server" BackgroundCssClass="modalBackground"
                Drag="True" DropShadow="True" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption"
                TargetControlID="bePopUpMsg">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="bePopUpMsg" runat="server" CausesValidation="False" Visible="False" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>