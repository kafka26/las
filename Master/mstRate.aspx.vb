Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class Master_Rate
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Functions"
    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        If curroid.SelectedValue = "" Then
            sError &= "- Please select Currency field!<BR>"
        End If
        Dim sErr As String = ""
        If ratedate.Text = "" Then
            sError &= "- Please fill START DATE field!<BR>"
            sErr = "Err"
        Else
            If Not IsValidDate(ratedate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- START DATE field is invalid. " & sErr & "<BR>"
            End If
        End If
        Dim sErr2 As String = ""
        If ratetodate.Text = "" Then
            sError &= "- Please fill END DATE field!<BR>"
            sErr2 = "Err"
        Else
            If Not IsValidDate(ratetodate.Text, "MM/dd/yyyy", sErr2) Then
                sError &= "- END DATE field is invalid. " & sErr2 & "<BR>"
            End If
        End If
        If sErr = "" And sErr2 = "" Then
            If CDate(ratedate.Text) > CDate(ratetodate.Text) Then
                sError &= "- END DATE must be more than START DATE!<BR>"
            End If
        End If
        If rateres1.Text = "" Then
            sError &= "- Please fill RATE TO IDR field!<BR>"
        Else
            If ToDouble(rateres1.Text) <= 0 Then
                sError &= "- RATE TO IDR field must be more than 0!<BR>"
            End If
        End If
        If rateres2.Text = "" Then
            sError &= "- Please fill RATE TO USD field!<BR>"
        Else
            If ToDouble(rateres2.Text) <= 0 Then
                sError &= "- RATE TO USD field must be more than 0!<BR>"
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If FilterPeriod1.Text = "" Or FilterPeriod2.Text = "" Then
            showMessage("Please fill both of period first!", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 1 must be less than Period 2!", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsDataIncluded() As Boolean
        sSql = "SELECT COUNT(*) FROM QL_mstrate WHERE cmpcode='" & CompnyCode & "' AND curroid=" & curroid.SelectedValue & " AND (('" & ratedate.Text & "' BETWEEN ratedate AND ratetodate) OR ('" & ratetodate.Text & "' BETWEEN ratedate AND ratetodate))"
        If Session("oid") IsNot Nothing And Session("oid") <> "" Then
            sSql &= " AND rateoid<>" & Session("oid")
        End If
        If CheckDataExists(sSql) Then
            showMessage("Your selected date have been included in another data. Please select another date!", 2)
            Return True
        End If
        Return False
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindMstData(ByVal sFilter As String)
        sSql = "SELECT 0 AS nmr, rateoid, r.curroid, currcode, CONVERT(VARCHAR(10), ratedate, 101) + ' - ' + CONVERT(VARCHAR(10), ratetodate, 101) AS ratedate, rateres1 AS rateidrvalue, rateres2 AS rateusdvalue, ratenote, r.activeflag FROM QL_mstrate r INNER JOIN QL_mstcurr c ON c.cmpcode=r.cmpcode AND c.curroid=r.curroid AND c.activeflag='ACTIVE' WHERE r.cmpcode='" & CompnyCode & "' AND currcode<>'IDR'"
        If cbPeriod.Checked Then
            If IsValidPeriod() Then
                sSql &= " AND ((ratedate>='" & FilterPeriod1.Text & " 00:00:00' AND ratedate<='" & FilterPeriod2.Text & " 23:59:59') OR (ratetodate>='" & FilterPeriod1.Text & " 00:00:00' AND ratetodate<='" & FilterPeriod2.Text & " 23:59:59'))"
            Else
                Exit Sub
            End If
        End If
        If cbCurrency.Checked Then
            If FilterDDLCurrency.SelectedValue <> "" Then
                sSql &= " AND r.curroid=" & FilterDDLCurrency.SelectedValue
            Else
                cbCurrency.Checked = False
            End If
        End If
        If cbStatus.Checked Then
            If FilterDDLStatus.SelectedValue <> "ALL" Then
                sSql &= " AND r.activeflag='" & FilterDDLStatus.SelectedValue & "'"
            End If
        End If
        If checkPagePermission("~\Master\mstRate.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND r.createuser='" & Session("UserID") & "'"
        End If
        sSql &= " ORDER BY rateoid DESC"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstrate")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(C1)("nmr") = C1 + 1
        Next
        Session("TblMst") = dt
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub InitAllDDL()
        ' Fill DDL Currency
        sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND currcode<>'IDR'"
        FillDDL(curroid, sSql)
        FillDDL(FilterDDLCurrency, sSql)
        If curroid.Items.Count = 0 Then
            showMessage("Please input some currency data first before continue using this form!", 2)
        End If
    End Sub

    Private Sub FillTextBox()
        Try
            sSql = "SELECT rateoid, curroid, ratedate, ratetodate, rateidrvalue, rateusdvalue, ratenote, activeflag, createuser, createtime, upduser, updtime, rateres1, rateres2 FROM QL_mstrate WHERE cmpcode='" & CompnyCode & "' AND rateoid=" & Session("oid")
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            btnDelete.Visible = False : btnSave.Visible = False
            While xreader.Read
                rateoid.Text = xreader("rateoid").ToString
                curroid.SelectedValue = xreader("curroid").ToString
                ratedate.Text = Format(xreader("ratedate"), "MM/dd/yyyy")
                ratetodate.Text = Format(xreader("ratetodate"), "MM/dd/yyyy")
                'rateidrvalue.Text = ToMaskEdit(xreader("rateidrvalue"), 4)
                'rateusdvalue.Text = ToMaskEdit(xreader("rateusdvalue"), 6)
                ratenote.Text = xreader("ratenote").ToString
                activeflag.SelectedValue = xreader("activeflag").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
                rateres1.Text = ToMaskEdit(xreader("rateres1"), GetRoundValue(xreader("rateres1")))
                rateres2.Text = ToMaskEdit(xreader("rateres2"), GetRoundValue(xreader("rateres1")))
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Master\mstRate.aspx")
        End If
        If checkPagePermission("~\Master\mstRate.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Rate Pajak"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            I_U.Text = "New Data"
        Else
            I_U.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnSave.Attributes.Add("OnClick", "javascript:return confirm('Your data will not be able to be edited or deleted anymore. Are you sure to continue?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitAllDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox()
                TabContainer1.ActiveTabIndex = 1
            Else
                rateoid.Text = GenerateID("QL_MSTRATE", CompnyCode)
                ratedate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                ratetodate.Text = Format(DateAdd(DateInterval.Day, 7, CDate(ratedate.Text)), "MM/dd/yyyy")
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindMstData("")
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        cbPeriod.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbCurrency.Checked = False
        FilterDDLCurrency.SelectedIndex = -1
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        BindMstData("")
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
    End Sub

    Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvMst.DataSource = Session("TblMst")
            gvMst.DataBind()
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            'If IsDataIncluded() Then
            '    Exit Sub
            'End If
            Dim sCurrCode As String = curroid.SelectedItem.Text
            Dim iCurrOid As Integer = CInt(GetStrData("SELECT TOP 1 curroid FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND currcode='IDR'"))
            If Session("oid") = Nothing Or Session("oid") = "" Then
                rateoid.Text = GenerateID("QL_MSTRATE", CompnyCode)
            End If
            Dim sExt As String = ""
            If ratedate.Text = ratetodate.Text Then
                sExt = " 23:59:59"
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_mstrate (cmpcode, rateoid, curroid, ratedate, ratetodate, rateidrvalue, rateusdvalue, ratenote, activeflag, createuser, createtime, upduser, updtime, rateres1, rateres2) VALUES('" & CompnyCode & "', " & rateoid.Text & ", " & curroid.SelectedValue & ", '" & ratedate.Text & "', '" & ratetodate.Text & sExt & "', " & ToDouble(rateres1.Text) & ", " & ToDouble(rateres2.Text) & ", '" & Tchar(ratenote.Text) & "', '" & Tchar(activeflag.SelectedValue) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & ToDouble(rateres1.Text) & "', '" & ToDouble(rateres2.Text) & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    If sCurrCode = "USD" Then
                        rateoid.Text = CInt(rateoid.Text) + 1
                        sSql = "INSERT INTO QL_mstrate (cmpcode, rateoid, curroid, ratedate, ratetodate, rateidrvalue, rateusdvalue, ratenote, activeflag, createuser, createtime, upduser, updtime, rateres1, rateres2) VALUES('" & CompnyCode & "', " & rateoid.Text & ", " & iCurrOid & ", '" & ratedate.Text & "', '" & ratetodate.Text & sExt & "', 1, " & 1 / ToDouble(rateres1.Text) & ", '" & Tchar(ratenote.Text) & "', '" & Tchar(activeflag.SelectedValue) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '1', '" & 1 / ToDouble(rateres1.Text) & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                    sSql = "UPDATE QL_mstoid SET lastoid=" & rateoid.Text & " WHERE tablename='QL_MSTRATE' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_mstrate SET curroid=" & curroid.SelectedValue & ", ratedate='" & ratedate.Text & "', ratetodate='" & ratetodate.Text & sExt & "', rateidrvalue=" & ToDouble(rateres1.Text) & ", rateusdvalue=" & ToDouble(rateres2.Text) & ", ratenote='" & Tchar(ratenote.Text) & "', activeflag='" & Tchar(activeflag.SelectedValue) & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, rateres1='" & ToDouble(rateres1.Text) & "', rateres2='" & ToDouble(rateres2.Text) & "' WHERE cmpcode='" & CompnyCode & "' AND rateoid=" & rateoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                conn.Close()
            Catch ex As Exception
                objTrans.Rollback()
                conn.Close()
                showMessage(ex.Message, 1)
                Exit Sub
            End Try
            Response.Redirect("~\Master\mstRate.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Master\mstRate.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If rateoid.Text = "" Then
            showMessage("Please select Rate Pajak data first!", 1)
            Exit Sub
        End If
        'sSql = "SELECT tblusage, colusage FROM QL_oidusage WHERE cmpcode='" & CompnyCode & "' AND tblname='QL_MSTRATE'"
        'Dim objTblUsage As DataTable = cKon.ambiltabel(sSql, "QL_oidusage")
        'Dim sColomnName(objTblUsage.Rows.Count - 1) As String
        'Dim sTable(objTblUsage.Rows.Count - 1) As String
        'For c1 As Integer = 0 To objTblUsage.Rows.Count - 1
        '    sColomnName(c1) = objTblUsage.Rows(c1).Item("colusage").ToString
        '    sTable(c1) = objTblUsage.Rows(c1).Item("tblusage").ToString
        'Next
        'If CheckDataExists(rateoid.Text, sColomnName, sTable) = True Then
        '    showMessage("This data can't be deleted because it is being used by another data!", 2)
        '    Exit Sub
        'End If
        sSql = "SELECT ta.name FROM sys.tables ta INNER JOIN sys.columns col ON col.object_id=ta.object_id WHERE col.name='rateoid' AND ta.name NOT IN ('QL_mstrate')"
        Dim objTblUsage As DataTable = cKon.ambiltabel(sSql, "QL_tables")
        For C1 As Integer = 0 To objTblUsage.Rows.Count - 1
            If CheckDataExists("SELECT COUNT(*) FROM " & objTblUsage.Rows(C1).Item("name").ToString & " WHERE rateoid=" & rateoid.Text) Then
                showMessage("This data can't be deleted because it is being used by another data!", 2)
                Exit Sub
            End If
        Next
        If DeleteData("QL_mstrate", "rateoid", rateoid.Text, CompnyCode) = True Then
            Response.Redirect("~\Master\mstRate.aspx?awal=true")
        End If
    End Sub

    Protected Sub gvMst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvMst.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), GetRoundValue(e.Row.Cells(3).Text))
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), GetRoundValue(e.Row.Cells(4).Text))
        End If
    End Sub

    Protected Sub rateres1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rateres1.TextChanged, rateres2.TextChanged
        sender.Text = ToMaskEdit(ToDouble(sender.Text), GetRoundValue(sender.Text))
        If sender.ID = "rateres1" Then
            If ToDouble(sender.Text) > 0 Then
                If curroid.SelectedValue <> "" Then
                    If ratedate.Text <> "" And ratetodate.Text <> "" Then
                        Dim sErr As String = ""
                        If IsValidDate(ratedate.Text, "MM/dd/yyyy", sErr) And IsValidDate(ratetodate.Text, "MM/dd/yyyy", sErr) Then
                            If CDate(ratetodate.Text) >= CDate(ratedate.Text) Then
                                Dim sCurrCode As String = curroid.SelectedItem.Text
                                If sCurrCode <> "USD" Then
                                    sSql = "SELECT TOP 1 rateres1 FROM QL_mstrate r INNER JOIN QL_mstcurr c ON c.cmpcode=r.cmpcode AND c.curroid=r.curroid WHERE r.cmpcode='" & CompnyCode & "' AND currcode='USD' AND r.activeflag='ACTIVE' AND '" & ratedate.Text & " 00:00:00'>=ratedate AND '" & ratedate.Text & " 00:00:00'<=ratetodate AND '" & ratetodate.Text & " 00:00:00'>=ratedate AND '" & ratetodate.Text & " 00:00:00'<=ratetodate ORDER BY r.updtime"
                                    Dim dVal As Double = ToDouble(GetStrData(sSql))
                                    If dVal > 0 Then
                                        rateres2.Text = ToMaskEdit(ToDouble(sender.Text) / dVal, GetRoundValue((ToDouble(sender.Text) / dVal).ToString))
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub
#End Region

End Class
