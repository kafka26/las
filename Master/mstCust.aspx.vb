Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_Customer
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Public Function IsEmailAddress(ByVal email As String) As Boolean
        email = CStr(email)
        Dim strRegex As String = "^[a-zA-Z0-9][\w\.-_]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
        Dim re As New Regex(strRegex)
        If re.IsMatch(email) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function IsValidUrl(ByVal url As String) As Boolean
        If Not url.ToLower().StartsWith("www.") Then
            Return False
        End If
        url = CStr(url)
        Dim strRegex As String = "^[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z0-9][\w\.-_]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
        Dim re As New Regex(strRegex)
        If Not re.IsMatch(url) Then
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        If custcode.Text.Trim = "" Then
            sError &= "- Please fill CODE field!<BR>"
        End If
        If custprefixoid.SelectedValue = "" Then
            sError &= "- Please select PREFIX NAME field!<BR>"
        End If
        If custname.Text.Trim = "" Then
            sError &= "- Please fill NAME field!<BR>"
        End If
        If custpaymentoid.SelectedValue = "" Then
            sError &= "- Please select DEFAULT TOP field!<BR>"
        End If
        If custtaxable.SelectedValue = "1" Then
            If custnpwp.Text.Trim = "" Then
                sError &= "- Please fill NPWP field!<BR>"
            End If
        End If
        If custcityoid.SelectedValue = "" Then
            sError &= "- Please select CITY field!<BR>"
        End If
        If custemail.Text <> "" Then
            If Not IsEmailAddress(custemail.Text) Then
                sError &= "- EMAIL field is invalid. The format should like '<STRONG>example@email.com</STRONG>'.<BR>"
            End If
        End If
        If custwebsite.Text <> "" Then
            If Not IsValidUrl(custwebsite.Text) Then
                sError &= "- WEBSITE field is invalid. The format should like '<STRONG>www.example.com</STRONG>'.<BR>"
            End If
        End If
        If custcp1email.Text <> "" Then
            If Not IsEmailAddress(custcp1email.Text) Then
                sError &= "- CP1 EMAIL field is invalid. The format should like '<STRONG>example@email.com</STRONG>'.<BR>"
            End If
        End If
        If custcp2email.Text <> "" Then
            If Not IsEmailAddress(custcp2email.Text) Then
                sError &= "- CP2 EMAIL is invalid. The format should like '<STRONG>example@email.com</STRONG>'.<BR>"
            End If
        End If
        If custaddr.Text <> "" Then
            If custaddr.Text.Length > 500 Then
                sError &= " - Alamat harus kurang dari 500 karakter !<BR>"
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputExists() As Boolean
        sSql = "SELECT COUNT(*) FROM QL_mstcust WHERE cmpcode='" & CompnyCode & "' AND custcode='" & Tchar(custcode.Text.Trim) & "'"
        If Session("oid") <> Nothing And Session("oid") <> "" Then
            sSql &= " AND custoid<>" & Session("oid")
        End If
        If ToDouble(GetStrData(sSql).ToString) > 0 Then
            ' showMessage("CODE has been used before by another data. Please fill another CODE!", 2)
            ' Return True
        End If
        sSql = "SELECT COUNT(*) FROM QL_mstcust WHERE cmpcode='" & CompnyCode & "' AND custname='" & Tchar(custname.Text.Trim) & "' AND custprefixoid=" & custprefixoid.SelectedValue
        If Session("oid") <> Nothing And Session("oid") <> "" Then
            sSql &= " AND custoid<>" & Session("oid")
        End If
        If ToDouble(GetStrData(sSql).ToString) > 0 Then
            ' showMessage("NAME has been used before by another data. Please fill another NAME!", 2)
            ' Return True
        End If
        Return False
    End Function

    Private Function GetCheckedTag() As String
        Dim sRet As String = ""
        If custres3.Items.Count > 0 Then
            For C1 As Integer = 0 To custres3.Items.Count - 1
                If custres3.Items(C1).Selected = True Then
                    sRet &= custres3.Items(C1).Value & "; "
                End If
            Next
        End If
        If sRet <> "" Then
            sRet = Left(sRet, sRet.Length - 2)
        End If
        Return sRet
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitAllDDL()
        ' Fill DDL Prefix Name
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='PREFIX NAME' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDL(custprefixoid, sSql)
        ' Fill DDL Customer Group
        sSql = "select custgroupoid,custgroupname from QL_mstcustgroup WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' ORDER BY custgroupname"
        FillDDL(custgroupoid, sSql)
        'Fill DDL Default TOP
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='PAYMENT TYPE' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDL(custpaymentoid, sSql)
        ' Fill DDL City
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='CITY' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDL(custcityoid, sSql)
        ' Fill DDL Warehouse
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='WAREHOUSE' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDLWithAdditionalText(warehouse_id, sSql, "-- Pilih Gudang --", "0")
        ' Fill DDL Sales
        sSql = "select personoid, personname from QL_mstperson p inner join QL_mstlevel g on g.leveloid =p.leveloid where salescode<>'' and upper(g.leveldesc)='SALES' and p.activeflag='active' order by personname"
        FillDDLWithAdditionalText(person_id, sSql, "-- Pilih Sales --", "0")
        FillFilterDDLTag()
        ' Fill List Box Tag
        sSql = "SELECT cat1shortdesc, (cat1res1 + ' - ' + cat1shortdesc) AS cat1desc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' ORDER BY cat1desc"
        custres3.Items.Clear()
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            custres3.Items.Add(xreader.GetValue(1).ToString)
            custres3.Items(custres3.Items.Count - 1).Value = xreader.GetValue(0).ToString
        End While
        xreader.Close()
        conn.Close()
    End Sub

    Private Sub FillFilterDDLTag()
        ' Fill Filter DDL Tag
        'sSql = "SELECT cat1oid, cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        'If cbTagType.Checked Then
        '    sSql &= " AND cat1res1='" & FilterDDLTagType.SelectedValue & "'"
        'End If
        'sSql &= " ORDER BY cat1shortdesc"
        'FillDDL(FilterDDLTag, sSql)
    End Sub

    Private Sub BindMstData(ByVal sFilter As String)
        sSql = "SELECT custoid, custcode, custname, custaddr, gendesc AS custcity, (CASE custphone1 WHEN '' THEN (CASE custphone2 WHEN '' THEN (CASE custphone3 WHEN '' THEN '' ELSE ('[1] ' + custphone3) END) ELSE ('[1] ' + custphone2 + (CASE custphone3 WHEN '' THEN '' ELSE (' [2] ' + custphone3) END)) END) ELSE ('[1] ' + custphone1 + (CASE custphone2 WHEN '' THEN (CASE custphone3 WHEN '' THEN '' ELSE (' [2] ' + custphone3) END) ELSE (' [2] ' + custphone2 + (CASE custphone3 WHEN '' THEN '' ELSE (' [3] ' + custphone3) END)) END)) END) AS custphone, (CASE custcp1name WHEN '' THEN (CASE custcp2name WHEN '' THEN '' ELSE ('[1] ' + custcp2name + ' Telp. ' + custcp2phone + ' Email: ' + custcp2email) END) ELSE ('[1] ' + custcp1name + ' Telp. ' + custcp1phone + ' Email: ' + custcp1email + (CASE custcp2name WHEN '' THEN '' ELSE (' [2] ' + custcp2name + ' Telp. ' + custcp2phone + ' Email: ' + custcp2email) END)) END) AS custcp, c.activeflag FROM QL_mstcust c INNER JOIN QL_mstgen g2 ON g2.cmpcode=c.cmpcode AND genoid=custcityoid WHERE c.cmpcode='" & CompnyCode & "' and " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(sFilter) & "%'"
        'LIKE '% & Tchar(sFilter) & %'
        'If cbTag.Checked Then
        '    If FilterDDLTag.SelectedValue <> "" Then
        '        sSql &= " AND (c.custres3 LIKE '%" & FilterDDLTag.SelectedItem.Text & "%' OR c.custres3='' OR c.custres3 IS NULL)"
        '    End If
        'End If
        If cbStatus.Checked Then
            sSql &= " AND c.activeflag='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Master\mstCust.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND c.createuser='" & Session("UserID") & "'"
        End If
        sSql &= " ORDER BY custcode DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_mstcust")
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub FillTextBox()
        Try
            sSql = "SELECT custoid, custgroupoid, custcode, custtype, custprefixoid, custname, custaddr, custcityoid, custpaymentoid, custtaxable, custnpwp, custphone1, custphone2, custphone3, custfax1, custfax2, custemail, custwebsite, custcp1name, custcp2name, custcp1phone, custcp2phone, custcp1email, custcp2email, custnote, custres3, activeflag, createuser, createtime, upduser, updtime,custres1, custheaderdisc, kode_komisi, credit_limit, limit_usage, person_id, warehouse_id, cust_disc_1, cust_disc_2, cust_disc_3 FROM QL_mstcust WHERE cmpcode='" & CompnyCode & "' AND custoid=" & Session("oid")
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                custoid.Text = xreader("custoid").ToString
                custgroupoid.SelectedValue = xreader("custgroupoid").ToString
                custcode.Text = xreader("custcode").ToString
                custtype.SelectedValue = xreader("custtype").ToString
                custprefixoid.SelectedValue = xreader("custprefixoid").ToString
                custname.Text = xreader("custname").ToString
                custaddr.Text = xreader("custaddr").ToString
                custcityoid.SelectedValue = xreader("custcityoid").ToString
                custpaymentoid.SelectedValue = xreader("custpaymentoid").ToString
                custtaxable.SelectedValue = xreader("custtaxable").ToString
                custnpwp.Text = xreader("custnpwp").ToString
                custphone1.Text = xreader("custphone1").ToString
                custphone2.Text = xreader("custphone2").ToString
                custphone3.Text = xreader("custphone3").ToString
                custfax1.Text = xreader("custfax1").ToString
                custfax2.Text = xreader("custfax2").ToString
                custemail.Text = xreader("custemail").ToString
                custwebsite.Text = xreader("custwebsite").ToString
                custcp1name.Text = xreader("custcp1name").ToString
                custcp2name.Text = xreader("custcp2name").ToString
                custcp1phone.Text = xreader("custcp1phone").ToString
                custcp2phone.Text = xreader("custcp2phone").ToString
                custcp1email.Text = xreader("custcp1email").ToString
                custcp2email.Text = xreader("custcp2email").ToString
                custnote.Text = xreader("custnote").ToString
                SetCheckedTag(xreader("custres3").ToString)
                activeflag.SelectedValue = xreader("activeflag").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
                OldCode.Text = xreader("custres1").ToString
                custheaddisc.Text = ToMaskEdit(ToDouble(xreader("custheaderdisc").ToString), 2)
                kode_komisi.SelectedValue = xreader("kode_komisi").ToString
                credit_limit.Text = ToMaskEdit(ToDouble(xreader("credit_limit").ToString), 2)
                limit_usage.Text = ToMaskEdit(ToDouble(xreader("limit_usage").ToString), 2)
                person_id.SelectedValue = xreader("person_id").ToString
                warehouse_id.SelectedValue = xreader("warehouse_id").ToString
                tbDisc1.Text = ToDouble(xreader("cust_disc_1").ToString)
                tbDisc2.Text = ToDouble(xreader("cust_disc_2").ToString)
                tbDisc3.Text = ToDouble(xreader("cust_disc_3").ToString)
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False
        End Try
        custcode.CssClass = "inpTextDisabled"
        custcode.Enabled = False
        CodeCust.Visible = False
        'custtype.CssClass = "inpTextDisabled" : custtype.Enabled = False
        'custpaymentoid.CssClass = "inpTextDisabled" : custpaymentoid.Enabled = False
        'custtaxable.CssClass = "inpTextDisabled" : custtaxable.Enabled = False
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        Try
            If sType = "PDF" Then
                report.Load(Server.MapPath(folderReport & "rptCust.rpt"))
            Else
                report.Load(Server.MapPath(folderReport & "rptCustExcel.rpt"))
            End If
            Dim sWhere As String = " WHERE c.cmpcode='" & CompnyCode & "' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
            'If cbTag.Checked Then
            '    If FilterDDLTag.SelectedValue <> "" Then
            '        sWhere &= " AND (c.custres3 LIKE '%" & FilterDDLTag.SelectedItem.Text & "%' OR c.custres3='' OR c.custres3 IS NULL)"
            '    End If
            'End If
            If cbStatus.Checked Then
                sWhere &= " AND c.activeflag='" & FilterDDLStatus.SelectedValue & "'"
            End If
            If checkPagePermission("~\Master\mstCust.aspx", Session("SpecialAccess")) = False Then
                sWhere &= " AND c.createuser='" & Session("UserID") & "'"
            End If
            sWhere &= " ORDER BY custcode DESC"
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            If sType = "PDF" Then
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "CustomerPrintOut")
            Else
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "CustomerPrintOut")
            End If
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
        Response.Redirect("~\Master\mstCust.aspx?awal=true")
    End Sub

    Private Sub SetCheckedTag(ByVal sTag As String)
        If sTag <> "" Then
            Dim sSplit() As String = sTag.Split(";")
            If sSplit.Length > 0 Then
                If custres3.Items.Count > 0 Then
                    For C1 As Integer = 0 To sSplit.Length - 1
                        Dim li As ListItem = custres3.Items.FindByValue(LTrim(sSplit(C1)))
                        Dim i As Integer = custres3.Items.IndexOf(li)
                        If i >= 0 Then
                            custres3.Items(custres3.Items.IndexOf(li)).Selected = True
                        End If
                    Next
                End If
            End If
        End If
    End Sub

    Private Sub GenerateCodeCust()
        Dim sNo As String = "CUST-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(custcode, " & DefaultFormatCounter & ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstcust WHERE cmpcode='" & Session("CompnyCode") & "' AND custcode LIKE '" & sNo & "%'"
        custcode.Text = GenNumberString(sNo, "", cKon.ambilscalar(sSql), DefaultFormatCounter)
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Master\mstCust.aspx")
        End If
        If checkPagePermission("~\Master\mstCust.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Customer"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            I_U.Text = "New Data"
        Else
            I_U.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            InitAllDDL()
            custcityoid_SelectedIndexChanged(Nothing, Nothing)
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox()
                TabContainer1.ActiveTabIndex = 1
            Else
                custoid.Text = GenerateID("QL_MSTCUST", CompnyCode)
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
                GenerateCodeCust()
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    'Protected Sub FilterDDLTagType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLTagType.SelectedIndexChanged
    '    FillFilterDDLTag()
    'End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        'cbTagType.Checked = False
        'FilterDDLTagType.SelectedIndex = -1
        'FillFilterDDLTag()
        'cbTag.Checked = False
        'FilterDDLTag.SelectedIndex = -1
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
    End Sub

    Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvMst.DataSource = Session("TblMst")
            gvMst.DataBind()
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            If IsInputExists() Then
                Exit Sub
            End If
            If Session("oid") = Nothing Or Session("oid") = "" Then
                custoid.Text = GenerateID("QL_MSTCUST", CompnyCode)
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_mstcust (cmpcode, custgroupoid, custoid, custcode, custtype, custprefixoid, custname, custaddr, custcityoid, custpaymentoid, custtaxable, custnpwp, custphone1, custphone2, custphone3, custfax1, custfax2, custemail, custwebsite, custcp1name, custcp2name, custcp1phone, custcp2phone, custcp1email, custcp2email, custnote, custres3, activeflag, createuser, createtime, upduser, updtime,custres1, custheaderdisc, kode_komisi, credit_limit, person_id, warehouse_id, cust_disc_1, cust_disc_2, cust_disc_3) VALUES('" & CompnyCode & "', " & custgroupoid.SelectedValue & "," & custoid.Text & ", '" & Tchar(custcode.Text.Trim) & "" & Tchar(CodeCust.Text) & "', '" & custtype.SelectedValue & "', " & custprefixoid.SelectedValue & ", '" & Tchar(custname.Text.Trim) & "', '" & Tchar(custaddr.Text.Trim) & "', " & custcityoid.SelectedValue & ", " & custpaymentoid.SelectedValue & ", " & custtaxable.SelectedValue & ", '" & Tchar(custnpwp.Text.Trim) & "', '" & Tchar(custphone1.Text.Trim) & "', '" & Tchar(custphone2.Text.Trim) & "', '" & Tchar(custphone3.Text.Trim) & "', '" & Tchar(custfax1.Text.Trim) & "', '" & Tchar(custfax2.Text.Trim) & "', '" & Tchar(custemail.Text.Trim) & "', '" & Tchar(custwebsite.Text.Trim) & "', '" & Tchar(custcp1name.Text.Trim) & "', '" & Tchar(custcp2name.Text.Trim) & "', '" & Tchar(custcp1phone.Text.Trim) & "', '" & Tchar(custcp2phone.Text.Trim) & "', '" & Tchar(custcp1email.Text.Trim) & "', '" & Tchar(custcp2email.Text.Trim) & "', '" & Tchar(custnote.Text.Trim) & "', '" & Tchar(Left(GetCheckedTag(), 1000)) & "', '" & activeflag.SelectedValue & "', '" & createuser.Text & "', '" & createtime.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP,'" & Tchar(OldCode.Text) & "', " & ToDouble(custheaddisc.Text) & ", '" & kode_komisi.SelectedValue & "', " & ToDouble(credit_limit.Text) & ", " & person_id.SelectedValue & ", " & warehouse_id.SelectedValue & ", " & ToDouble(tbDisc1.Text) & ", " & ToDouble(tbDisc2.Text) & ", " & ToDouble(tbDisc3.Text) & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & custoid.Text & " WHERE tablename='QL_MSTCUST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_mstcust SET custtype='" & custtype.SelectedValue & "', custgroupoid=" & custgroupoid.SelectedValue & ", custprefixoid=" & custprefixoid.SelectedValue & ", custname='" & Tchar(custname.Text.Trim) & "', custaddr='" & Tchar(custaddr.Text.Trim) & "', custcityoid=" & custcityoid.SelectedValue & ", custpaymentoid=" & custpaymentoid.SelectedValue & ", custtaxable=" & custtaxable.SelectedValue & ", custnpwp='" & Tchar(custnpwp.Text.Trim) & "', custphone1='" & Tchar(custphone1.Text.Trim) & "', custphone2='" & Tchar(custphone2.Text.Trim) & "', custphone3='" & Tchar(custphone3.Text.Trim) & "', custfax1='" & Tchar(custfax1.Text.Trim) & "', custfax2='" & Tchar(custfax2.Text.Trim) & "', custemail='" & Tchar(custemail.Text.Trim) & "', custwebsite='" & Tchar(custwebsite.Text.Trim) & "', custcp1name='" & Tchar(custcp1name.Text.Trim) & "', custcp2name='" & Tchar(custcp2name.Text.Trim) & "', custcp1phone='" & Tchar(custcp1phone.Text.Trim) & "', custcp2phone='" & Tchar(custcp2phone.Text.Trim) & "', custcp1email='" & Tchar(custcp1email.Text.Trim) & "', custcp2email='" & Tchar(custcp2email.Text.Trim) & "', custnote='" & Tchar(custnote.Text.Trim) & "', custres3='" & Tchar(Left(GetCheckedTag(), 1000)) & "', activeflag='" & activeflag.SelectedValue & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP,custres1='" & Tchar(OldCode.Text) & "', custheaderdisc=" & ToDouble(custheaddisc.Text) & ", kode_komisi='" & kode_komisi.SelectedValue & "', credit_limit=" & ToDouble(credit_limit.Text) & ", person_id=" & person_id.SelectedValue & ", warehouse_id=" & warehouse_id.SelectedValue & ", cust_disc_1=" & ToDouble(tbDisc1.Text) & ", cust_disc_2=" & ToDouble(tbDisc2.Text) & ", cust_disc_3=" & ToDouble(tbDisc3.Text) & " WHERE cmpcode='" & CompnyCode & "' AND custoid=" & custoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If cbInit.Checked Then
                    sSql = "update t set cust_disc_1=" & ToDouble(tbDisc1.Text) & ", cust_disc_2=" & ToDouble(tbDisc2.Text) & ", cust_disc_3=" & ToDouble(tbDisc3.Text) & " from (select d.cust_disc_1, d.cust_disc_2, d.cust_disc_3 from QL_trntransitemdtl d inner join QL_trntransitemmst h on h.transitemmstoid=d.transitemmstoid where h.transitemmstres3=" & custoid.Text & " and h.iskonsinyasi='true' and h.cons_type='transfer' and h.transitemno like 'sjk%') t"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                objTrans.Commit()
                conn.Close()
            Catch ex As Exception
                objTrans.Rollback()
                conn.Close()
                showMessage(ex.Message, 1)
                Exit Sub
            End Try
            Response.Redirect("~\Master\mstCust.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Master\mstCust.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If custoid.Text = "" Then
            showMessage("Please select customer data first!", 1)
            Exit Sub
        End If
        sSql = "SELECT ta.name FROM sys.tables ta INNER JOIN sys.columns col ON col.object_id=ta.object_id WHERE col.name='custoid' AND ta.name NOT IN ('QL_mstcust')"
        Dim objTblUsage As DataTable = cKon.ambiltabel(sSql, "QL_tables")
        For C1 As Integer = 0 To objTblUsage.Rows.Count - 1
            If CheckDataExists("SELECT COUNT(*) FROM " & objTblUsage.Rows(C1).Item("name").ToString & " WHERE custoid=" & custoid.Text) Then
                showMessage("This data can't be deleted because it is being used by another data!", 2)
                Exit Sub
            End If
        Next
        If DeleteData("QL_mstcust", "custoid", custoid.Text, CompnyCode) = True Then
            Response.Redirect("~\Master\mstCust.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnExportPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportPDF.Click
        ShowReport("PDF")
    End Sub

    Protected Sub btnExportExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportExcel.Click
        ShowReport("Excel")
    End Sub

    Protected Sub gvMst_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub custname_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles custname.TextChanged

    End Sub

    Protected Sub custcityoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
#End Region

    Protected Sub custheaddisc_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Text = ToMaskEdit(ToDouble(sender.Text), 2)
    End Sub

    Protected Sub credit_limit_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        credit_limit.Text = ToMaskEdit(credit_limit.Text, 2)
    End Sub
End Class
