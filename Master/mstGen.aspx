﻿<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="mstGen.aspx.vb" Inherits="Master_General" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: General" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of General :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD style="WIDTH: 15%" class="Label" align=left>Filter</TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=4><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="gencode">Code</asp:ListItem>
<asp:ListItem Value="gendesc">Description</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="150px"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbGroup" runat="server" Text="Group"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=4><asp:DropDownList id="FilterDDLGroup" runat="server" CssClass="inpText" Width="200px"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status" Checked="True"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=4><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="100px">
                <asp:ListItem>ALL</asp:ListItem>
                <asp:ListItem Selected="True" Value="ACTIVE">ACTIVE</asp:ListItem>
                <asp:ListItem Value="INACTIVE">INACTIVE</asp:ListItem>
            </asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:GridView id="gvMst" runat="server" ForeColor="#333333" Width="100%" AllowSorting="True" GridLines="None" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="genoid" DataNavigateUrlFormatString="~/Master/mstGen.aspx?oid={0}" DataTextField="gencode" HeaderText="Code" SortExpression="gencode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="gendesc" HeaderText="Description" SortExpression="gendesc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="50%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="50%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gengroup" HeaderText="Group" SortExpression="gengroup">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="There is no data !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data"></asp:Label> </TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3></TD></TR><TR><TD style="WIDTH: 80px" class="Label" align=left><asp:Label id="I_U" runat="server" CssClass="Important" Text="New Data"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="genoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 80px" class="Label" align=left><asp:Label id="lblKode" runat="server" Text="General Code" Width="80px"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="gencode" runat="server" CssClass="inpText" Width="100px" MaxLength="30"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 80px" class="Label" align=left><asp:Label id="lblDeskripsi" runat="server" Text="Description"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="gendesc" runat="server" CssClass="inpText" Width="200px" MaxLength="65"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 80px" class="Label" align=left><asp:Label id="lblGroup" runat="server" Text="Group"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="ddlgengroup" runat="server" CssClass="inpText" Width="205px" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 80px" class="Label" align=left><asp:Label id="lblLain1" runat="server" Text="Another 1" Visible="False"></asp:Label></TD><TD id="TDAnother1" class="Label" align=center runat="server" visible="false">:</TD><TD class="Label" align=left><asp:TextBox id="genother1" runat="server" CssClass="inpText" Width="200px" Visible="False" MaxLength="50"></asp:TextBox><asp:DropDownList id="ddlgenother1" runat="server" CssClass="inpText" Width="205px" Visible="False"></asp:DropDownList> </TD></TR><TR><TD style="WIDTH: 80px" class="Label" align=left><asp:Label id="lblLain2" runat="server" Text="Another 2" Visible="False"></asp:Label></TD><TD id="TDAnother2" class="Label" align=center runat="server" visible="false">:</TD><TD class="Label" align=left><asp:TextBox id="genother2" runat="server" CssClass="inpText" Width="200px" Visible="False" MaxLength="50"></asp:TextBox><asp:DropDownList id="ddlgenother2" runat="server" CssClass="inpText" Width="205px" Visible="False"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 80px" class="Label" align=left><asp:Label id="lblLain3" runat="server" Text="Another 3" Visible="False"></asp:Label></TD><TD id="TDAnother3" class="Label" align=center runat="server" visible="false">:</TD><TD class="Label" align=left><asp:TextBox id="genother3" runat="server" CssClass="inpText" Width="200px" Visible="False" MaxLength="50"></asp:TextBox><asp:DropDownList id="ddlgenother3" runat="server" CssClass="inpText" Width="205px" Visible="False"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 80px" class="Label" align=left><asp:Label id="lblLain4" runat="server" Text="Another 4" Visible="False"></asp:Label></TD><TD id="TDAnother4" class="Label" align=center runat="server" visible="false">:</TD><TD class="Label" align=left><asp:TextBox id="genother4" runat="server" CssClass="inpText" Width="200px" Visible="False" MaxLength="50"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 80px" class="Label" align=left><asp:Label id="lblLain5" runat="server" Text="Another 5" Visible="False"></asp:Label></TD><TD id="TDAnother5" class="Label" align=center runat="server" visible="false">:</TD><TD class="Label" align=left><asp:TextBox id="genother5" runat="server" CssClass="inpText" Width="200px" Visible="False" MaxLength="50"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 80px" class="Label" align=left><asp:Label id="lblLain6" runat="server" Text="Another 6" Visible="False"></asp:Label></TD><TD id="TDAnother6" class="Label" align=center runat="server" visible="false">:</TD><TD class="Label" align=left><asp:TextBox id="genother6" runat="server" CssClass="inpText" Width="200px" Visible="False" MaxLength="50"></asp:TextBox><asp:TextBox id="genother7" runat="server" CssClass="inpText" Width="100px" Visible="False" MaxLength="10" __designer:wfdid="w1"></asp:TextBox> <asp:Label id="lbl7" runat="server" ForeColor="Red" Font-Bold="True" Text="MM/DD/YYYY" Visible="False" __designer:wfdid="w1"></asp:Label></TD></TR><TR><TD style="WIDTH: 80px" class="Label" align=left><asp:Label id="lblFlag" runat="server" Text="Status"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="activeflag" runat="server" CssClass="inpText" Width="105px"><asp:ListItem>ACTIVE</asp:ListItem>
<asp:ListItem>INACTIVE</asp:ListItem>
</asp:DropDownList> </TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=3><ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender1" runat="server" __designer:wfdid="w2" TargetControlID="genother7" ValidChars="1234567890-/"></ajaxToolkit:FilteredTextBoxExtender></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True"></asp:Label>&nbsp;On <asp:Label id="createtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update&nbsp;By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD align=left colSpan=1><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton id="btnPrint2" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint2"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form General :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

