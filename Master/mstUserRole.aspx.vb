Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_UserRole
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        If ddluserprof.SelectedValue = "" Then
            sError &= "- Please select USER ID Field!<BR>"
        End If
        If ddlroleoid.SelectedValue = "" Then
            sError &= "- Please select ROLE NAME Field!<BR>"
        End If
        If Session("TblDataRole") Is Nothing Then
            sError &= "- Please select minimally one role for this User ID!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindMstData(ByVal sFilter As String)
        sSql = "SELECT DISTINCT ur.profoid, p.profname, COUNT(ur.profoid) AS totalrole FROM QL_mstuserrole ur INNER JOIN QL_mstprof p ON ur.cmpcode=p.cmpcode AND ur.profoid=p.profoid AND p.activeflag='ACTIVE' WHERE " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(sFilter) & "%'"
        'If Session("CompnyCode") <> CompnyCode Then
        '    sSql &= " AND ur.cmpcode='" & Session("CompnyCode") & "'"
        'End If
        'If checkPagePermission("~\Master\mstUserRole.aspx", Session("SpecialAccess")) = False Then
        '    sSql &= " AND ur.createuser='" & Session("UserID") & "'"
        'End If
        sSql &= "  GROUP BY ur.profoid, p.profname ORDER BY ur.profoid"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_mstuserrole")
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub InitAllDDL()
        ' Fill DDL User ID
        sSql = "SELECT profoid + ';' + cmpcode, profoid FROM QL_mstprof WHERE activeflag='ACTIVE'"
        'If Session("CompnyCode") <> CompnyCode Then
        '    sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        'End If
        'sSql &= " AND profoid NOT IN (SELECT DISTINCT profoid FROM QL_mstuserrole)"
        FillDDL(ddluserprof, sSql)
        ' Fill DDL Nama Role
        sSql = "SELECT roleoid, rolename FROM QL_mstrole WHERE cmpcode='" & CompnyCode & "'"
        FillDDL(ddlroleoid, sSql)
    End Sub

    Private Sub FillTextBox()
        Try
            ' Fill Data Master
            btnDelete.Visible = False
            sSql = "SELECT DISTINCT profoid + ';' + cmpcode, profoid FROM QL_mstuserrole WHERE profoid='" & Session("oid") & "'"
            FillDDL(ddluserprof, sSql)
            ddluserprof.SelectedIndex = -1
            ddluserprof.CssClass = "inpTextDisabled"
            ddluserprof.Enabled = False
            ' Select Update User and Update Time
            sSql = "SELECT TOP 1 userroleoid, createuser, createtime, upduser, updtime FROM QL_mstuserrole WHERE profoid='" & Session("oid") & "' ORDER BY userroleoid DESC"
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            While xreader.Read
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
            End While
            ' Fill Data Detail
            sSql = "SELECT ur.roleoid, r.rolename, ur.special, ur.createuser, ur.upduser, ur.updtime FROM QL_mstuserrole ur INNER JOIN QL_mstrole r ON ur.roleoid=r.roleoid WHERE ur.profoid='" & Session("oid") & "'"
            Dim objTable As DataTable = cKon.ambiltabel(sSql, "QL_mstuserrole")
            Session("TblDataRole") = objTable
            gvDtl.DataSource = Session("TblDataRole")
            gvDtl.DataBind()
            imbRemove.Visible = (objTable.Rows.Count > 0)
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
        Dim sUser As String = ddluserprof.SelectedItem.Text.ToUpper.Trim
        If sUser.Contains("ADMIN") Then
            btnDelete.Visible = False
        Else
            btnDelete.Visible = True
        End If
    End Sub

    Private Sub CreateDataRole()
        Dim objTable As New DataTable
        objTable.Columns.Add("roleoid", Type.GetType("System.Int32"))
        objTable.Columns.Add("rolename", Type.GetType("System.String"))
        objTable.Columns.Add("special", Type.GetType("System.String"))
        objTable.Columns.Add("createuser", Type.GetType("System.String"))
        objTable.Columns.Add("upduser", Type.GetType("System.String"))
        objTable.Columns.Add("updtime", Type.GetType("System.DateTime"))
        Session("TblDataRole") = objTable
    End Sub

    Private Sub PrintReport(ByVal sOid As String)
        Try
            report.Load(Server.MapPath(folderReport & "rptUserRole.rpt"))
            Dim sWhere As String
            sWhere = " WHERE "
            If sOid = "" Then
                sWhere &= " " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If checkPagePermission("~\Master\mstUserRole.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND ur.createuser='" & Session("UserID") & "'"
                End If
            Else
                sWhere &= " ur.profoid='" & sOid & "'"
            End If
            sWhere &= " ORDER BY ur.profoid"
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "UserRolePrintOut_" & Format(GetServerTime(), "yyyyMMddHHmmss"))
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
        Response.Redirect("~\Master\mstUserRole.aspx?awal=true")
    End Sub

    Private Sub RemoveList(ByVal checkCol As Integer)
        Dim objTable As DataTable
        Dim objRow() As DataRow
        objTable = Session("TblDataRole")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length() > 0 Then
            For i As Integer = objRow.Length() - 1 To 0 Step -1
                If cKon.getCheckBoxValue(i, checkCol, gvDtl) = True Then
                    objTable.Rows.Remove(objRow(i))
                End If
            Next
            Session("TblDataRole") = objTable
            gvDtl.DataSource = Session("TblDataRole")
            gvDtl.DataBind()
        End If
        imbRemove.Visible = (objTable.Rows.Count > 0)
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Master\mstUserRole.aspx")
        End If
        If checkPagePermission("~\Master\mstUserRole.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - User Role"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            I_U.Text = "New Data"
        Else
            I_U.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            InitAllDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox()
                TabContainer1.ActiveTabIndex = 1
            Else
                btnDelete.Visible = False
                btnPrint2.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        FilterText.Text = ""
        FilterDDL.SelectedIndex = 0
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        PrintReport("")
    End Sub

    Protected Sub btnPrint2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint2.Click
        PrintReport(Session("oid"))
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                Dim iOid As Int64 = GenerateID("QL_mstuserrole", CompnyCode)
                If Not Session("oid") Is Nothing And Session("oid") <> "" Then
                    sSql = "DELETE QL_mstuserrole WHERE profoid='" & ddluserprof.SelectedItem.Text & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDataRole") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDataRole")
                    For C1 As Integer = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_mstuserrole (cmpcode, userroleoid, roleoid, profoid, special, createuser, upduser, updtime) VALUES ('" & ddluserprof.SelectedValue.Split(";")(1) & "', " & iOid & ", " & objTable.Rows(C1)("roleoid") & ", '" & ddluserprof.SelectedItem.Text & "', '" & Tchar(objTable.Rows(C1)("special").ToString) & "', '" & Session("UserID") & "', '" & Session("UserID") & "', current_timestamp)"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iOid += 1
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iOid - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_mstuserrole'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                conn.Close()
            Catch ex As Exception
                objTrans.Rollback()
                conn.Close()
                showMessage(ex.Message, 1)
                Exit Sub
            End Try
            Response.Redirect("~\Master\mstUserRole.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Master\mstUserRole.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If ddluserprof.SelectedItem.Text = "" Then
            showMessage("Please select user role data first!", 2)
            Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE QL_mstuserrole WHERE profoid='" & ddluserprof.SelectedItem.Text & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Master\mstUserRole.aspx?awal=true")
    End Sub

    Protected Sub cbspecial_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbspecial.CheckedChanged
        If cbspecial.Checked Then
            lblspecial.Text = "Yes"
        Else
            lblspecial.Text = "No"
        End If
    End Sub

    Protected Sub imbTambah_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbTambah.Click
        If ddlroleoid.SelectedValue = "" Then
            showMessage("Please select role name first !", 2)
            Exit Sub
        End If
        If Session("TblDataRole") Is Nothing Then
            CreateDataRole()
        End If
        Dim objTable As DataTable = Session("TblDataRole")
        Dim objRow As DataRow
        Dim objView As DataView = objTable.DefaultView
        objView.RowFilter = "roleoid=" & ddlroleoid.SelectedValue
        If objView.Count > 0 Then
            showMessage("This data has been added before", 2)
            Exit Sub
        End If
        objView.RowFilter = ""
        objRow = objTable.NewRow
        objRow("roleoid") = ddlroleoid.SelectedValue
        objRow("rolename") = ddlroleoid.SelectedItem.Text
        objRow("special") = lblspecial.Text
        objRow("createuser") = Session("UserID")
        objRow("upduser") = Session("UserID")
        objRow("updtime") = GetServerTime()
        objTable.Rows.Add(objRow)
        Session("TblDataRole") = objTable
        gvDtl.DataSource = Session("TblDataRole")
        gvDtl.DataBind()
        imbRemove.Visible = (objTable.Rows.Count > 0)
        cbspecial.Checked = False
        cbspecial_CheckedChanged(Nothing, Nothing)
    End Sub

    Protected Sub imbRemove_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbRemove.Click
        RemoveList(2)
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
    End Sub

    Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvMst.DataSource = Session("TblMst")
            gvMst.DataBind()
        End If
    End Sub
#End Region

End Class
