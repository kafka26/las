<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="mstInterface.aspx.vb" Inherits="Master_Interface" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Interface" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
<asp:Image id="Image1" runat="server" ImageUrl="~/Images/List.gif" ImageAlign="AbsMiddle"></asp:Image> <STRONG><SPAN style="FONT-SIZE: 9pt">List of Interface :.</SPAN></STRONG> 
</HeaderTemplate>
                        <ContentTemplate>
<asp:UpdatePanel id="UpdatePanel2" runat="server"><ContentTemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left>Filter</TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="interfacevar">Variable</asp:ListItem>
<asp:ListItem Value="interfacevalue">Selected COA No.</asp:ListItem>
<asp:ListItem Value="interfacenote">Note</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="200px"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbBusUnit" runat="server" Text="Bus. Unit" Width="74px" Visible="False"></asp:CheckBox></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLBusUnit" runat="server" CssClass="inpText" Width="200px" Visible="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbType" runat="server" Text="Type"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLType" runat="server" CssClass="inpText" Width="100px">
                <asp:ListItem>Single COA</asp:ListItem>
                <asp:ListItem>Multi COA</asp:ListItem>
            </asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="100px">
    <asp:ListItem>ACTIVE</asp:ListItem>
    <asp:ListItem>INACTIVE</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 7px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="HEIGHT: 27px" class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvMst" runat="server" ForeColor="#333333" Width="100%" AllowSorting="True" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" PageSize="8" DataKeyNames="interfaceoid">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="interfaceoid" DataNavigateUrlFormatString="~/Master/mstInterface.aspx?oid={0}" DataTextField="interfacevar" HeaderText="Variable" SortExpression="interfacevar">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="interfacevalue" HeaderText="Selected COA No." SortExpression="interfacevalue">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="activeflag" HeaderText="Status" SortExpression="activeflag">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="interfacenote" HeaderText="Note" SortExpression="interfacenote">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="interfaceres2" HeaderText="Type" SortExpression="interfaceres2">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:CommandField SelectText="Save As" ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="There is no data have been saved before !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=4><asp:UpdateProgress id="UpdateProgress21" runat="server" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image21" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
    <triggers>
<asp:PostBackTrigger ControlID="gvMst"></asp:PostBackTrigger>
</triggers>
</asp:UpdatePanel> 
</ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
<asp:UpdatePanel id="UpdatePanel1" runat="server"><ContentTemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3></TD></TR><TR><TD style="WIDTH: 10%" class="Label" align=left><asp:Label id="I_U" runat="server" CssClass="Important" Text="New Data" Width="96px"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="interfaceoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="interfaceres2" runat="server" ForeColor="Red" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text="Business Unit" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpText" Visible="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label2" runat="server" Text="Variable"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="interfacevar" runat="server" CssClass="inpTextDisabled" Width="200px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label9" runat="server" Text="Status"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="activeflag" runat="server" CssClass="inpText" Width="105px">
        <asp:ListItem Value="ACTIVE">Active</asp:ListItem>
        <asp:ListItem Value="INACTIVE">Inactive</asp:ListItem>
    </asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label7" runat="server" Text="Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="interfacenote" runat="server" CssClass="inpText" Width="200px" MaxLength="100"></asp:TextBox></TD></TR><TR><TD class="Label" vAlign=top align=left><asp:Label id="Label8" runat="server" Text="COA"></asp:Label></TD><TD class="Label" vAlign=top align=center>:</TD><TD class="Label" vAlign=top align=left><asp:CheckBoxList id="cbldata" runat="server" Font-Bold="False" Width="97%" AutoPostBack="True" Font-Underline="False" RepeatColumns="2"></asp:CheckBoxList></TD></TR><TR><TD class="Label" align=left colSpan=3></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=3></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnSaveAs" runat="server" ImageUrl="~/Images/saveas.png" ImageAlign="AbsMiddle" AlternateText="Save As"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</ContentTemplate>
</asp:UpdatePanel> 
</ContentTemplate>
                        <HeaderTemplate>
<asp:Image id="Image2" runat="server" ImageUrl="~/Images/Form.gif" ImageAlign="AbsMiddle"></asp:Image> <STRONG><SPAN style="FONT-SIZE: 9pt">Form Interface :.</SPAN></STRONG> 
</HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

