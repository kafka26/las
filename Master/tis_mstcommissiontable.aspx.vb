Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class tis_mstcommissiontable
    Inherits System.Web.UI.Page

#Region "Variables"
    Dim dttemp As New DataTable
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim cProc As New ClassProcedure
    Dim sSql As String
    Dim cKoneksi As New Koneksi
    Dim RegDetail As Boolean = False
    Dim report As New ReportDocument
    Dim folderReport As String = "~/Report/"
    Dim vReport As New ReportDocument
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        'If checkPagePermission("~\Master\tis_mstcommissiontable.aspx", Session("Role")) = False Then
        '    Response.Redirect("~\Other\NotAuthorize.aspx")
        'End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim scode As String = Session("CompnyCode")
            Session.Clear()
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = scode
            Response.Redirect("tis_mstcommissiontable.aspx")
        End If

        Page.Title = CompnyName & " - Master Commission Table"
        Session("oid") = Request.QueryString("oid")

        btndelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure you want to delete?');")
        btnsaveas.Attributes.Add("OnClick", "javascript:return confirm('Are you sure you want to save this data as a new record?');")

        If Not IsPostBack Then
            binddata(" and a.commissionstatus='ACTIVE'")
            initddl()
            GVadddetail.DataSource = Nothing
            GVadddetail.DataBind()
            If Session("oid") Is Nothing Or Session("oid") = "" Then
                generateno()
                upduser.Text = Session("UserID")
                updtime.Text = Format(GetServerTime(), "MM/dd/yyyy HH:mm:ss")
                validdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                region_SelectedIndexChanged(Nothing, Nothing)
                TabContainer1.ActiveTabIndex = 0
            Else
                filltextbox()
                clearadddetail()
                TabContainer1.ActiveTabIndex = 1
            End If
        End If
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub generateno()
        Dim sNo As String = "INST/"
        sSQL = "select isnull(max(cast(right(commissioncode,4) as integer))+1,1) as idnew from QL_tis_mstcommission where cmpcode='" & CompnyCode & "' and commissioncode like '" & sNo & "%'"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSQL
        commissioncode.Text = sNo & Format(xCmd.ExecuteScalar, "0000")
        conn.Close()
    End Sub

    Private Sub binddata(ByVal swhere As String)
        sSql = "select distinct a.commissionoid,a.commissioncode,a.validdate,a.commissionnote,c.personname teamname,a.commissionstatus,a.updtime from QL_tis_mstcommission a inner join QL_tis_mstcommissiondtl b on a.cmpcode=b.cmpcode and a.commissionoid=b.commissionoid inner join QL_mstperson c on a.cmpcode=c.cmpcode and a.salesteamoid=c.personoid where a.cmpcode='" & CompnyCode & "' " & swhere & " order by a.validdate desc, a.updtime desc"
        Dim dtab As DataTable = cKoneksi.ambiltabel(sSQL, "QL_mstinsentive")
        Session("GVmst") = dtab
        GVmst.DataSource = dtab
        GVmst.DataBind()
    End Sub

    Private Sub initddl()
        ' GET TOP
        sSQL = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='PAYMENT TYPE' AND activeflag='ACTIVE'"
        FillDDL(paymentrate, sSQL)
        ' GET SALES TEAM NAME
        sSql = "SELECT p.personoid, UPPER(p.personname) + ' (' + UPPER(salescode) + ')' personname FROM QL_mstperson p INNER JOIN QL_mstlevel l ON l.leveloid=p.leveloid WHERE p.cmpcode='" & CompnyCode & "' AND p.activeflag='ACTIVE' AND l.leveloid=1 ORDER BY p.personname"
        FillDDL(salesteam, sSQL)
    End Sub

    Private Sub filltextbox()
        If Session("oid") IsNot Nothing And Session("oid") <> "" Then
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            sSql = "select commissionoid,commissioncode,validdate,commissionnote,commissionstatus,commissionflag,salesteamoid,upduser,updtime from QL_tis_mstcommission where cmpcode='" & CompnyCode & "' and commissionoid=" & Session("oid") & ""
            xCmd.CommandText = sSql
            xreader = xCmd.ExecuteReader
            While xreader.Read
                oid.Text = xreader("commissionoid")
                commissioncode.Text = xreader("commissioncode")
                validdate.Text = Format(xreader("validdate"), "MM/dd/yyyy")
                commissionnote.Text = xreader("commissionnote")
                commissionstatus.SelectedValue = xreader("commissionstatus")
                tmpstatus.Text = xreader("commissionstatus")
                commissionflag.SelectedValue = xreader("commissionflag")
                salesteam.SelectedValue = xreader("salesteamoid")
                tmpsales.Text = xreader("salesteamoid")
                upduser.Text = xreader("upduser")
                updtime.Text = Format(xreader("updtime"), "MM/dd/yyyy HH:mm")
            End While
            xreader.Close()
            conn.Close()
            sSql = "select d.seq,d.rangeone,d.rangetwo,d.amountrange,d.insentiverate,g1.gendesc paymentrate,d.paymentrateoid,case d.regionoid when 1 then 'JAWA' else 'LUAR JAWA' end region,d.regionoid,d.payrangeone,d.payrangetwo,cast(d.payrangeone as varchar)+' - '+(case d.payrangetwo when 9999 then '~' else cast(d.payrangetwo as varchar) end) payrange,d.commissionrate, kode_komisi from QL_tis_mstcommissiondtl d inner join QL_mstgen g1 on d.cmpcode=g1.cmpcode and d.paymentrateoid=g1.genoid where d.cmpcode='" & CompnyCode & "' and d.commissionoid=" & Session("oid") & " order by d.seq"
            Dim atab As DataTable = cKoneksi.ambiltabel(sSQL, "QL_tis_mstcommissiondtl")
            Session("GVadddetail") = atab
            GVadddetail.DataSource = atab
            GVadddetail.DataBind()

            'btndelete.Visible = False
            'btnsave.Visible = False
            'btnsaveas.Visible = True

            checkbuttonstate(Integer.Parse(Session("oid")))
        End If
    End Sub

    Private Sub checkbuttonstate(ByVal soid As Integer)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        'sSql = "select count(-1) from QL_tis_trninvinsentiveratehist where (commissions1mstoid=" & soid & " or commissions2mstoid=" & soid & ") and cmpcode='" & CompnyCode & "'"
        'xCmd.CommandText = sSQL
        'Dim tmpcounter As Integer = xCmd.ExecuteScalar
        If 0 = 1 Then
            btndelete.Visible = False
            btnsave.Visible = False
            btnsaveas.Visible = True
            btnupdatestatus.Visible = True
        Else
            btndelete.Visible = True
            btnsave.Visible = True
            btnsaveas.Visible = True
            btnupdatestatus.Visible = False
        End If
        conn.Close()
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btncancel.Click
        Response.Redirect("~/Master/tis_mstcommissiontable.aspx?awal=true")
    End Sub

    Protected Sub btndelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btndelete.Click
        If Session("oid") = Nothing Or Session("oid") = "" Then
            showMessage("Please choose data to delete!", 2)
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            sSQL = "delete from QL_tis_mstcommission where cmpcode='" & CompnyCode & "' and commissionoid=" & Session("oid") & ""
            xCmd.CommandText = sSQL
            xCmd.ExecuteNonQuery()

            sSQL = "delete from QL_tis_mstcommissiondtl where cmpcode='" & CompnyCode & "' and commissionoid=" & Session("oid") & ""
            xCmd.CommandText = sSQL
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("tis_mstcommissiontable.aspx?awal=true")
    End Sub

    Protected Sub GVmst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVmst.PageIndexChanging
        If Session("GVmst") IsNot Nothing Then
            Dim dtab As DataTable = Session("GVmst")
            GVmst.PageIndex = e.NewPageIndex
            GVmst.DataSource = dtab
            GVmst.DataBind()
        End If
    End Sub

    Protected Sub btnsearchlist_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsearchlist.Click
        If filterstatus.SelectedValue <> "%" Then
            binddata(" and " & ddlfilter.SelectedValue.ToString & " like '%" & Tchar(textfilter.Text) & "%' and a.commissionstatus='" & filterstatus.SelectedValue.ToString & "'")
        Else
            binddata(" and " & ddlfilter.SelectedValue.ToString & " like '%" & Tchar(textfilter.Text) & "%'")
        End If
    End Sub

    Protected Sub btnalllist_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnalllist.Click
        ddlfilter.SelectedIndex = 0
        textfilter.Text = ""
        filterstatus.SelectedValue = "ACTIVE"
        binddata(" and a.commissionstatus='ACTIVE'")
    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsave.Click
        If validdate.Text.Trim = "" Then
            showMessage("Please fill valid date!", 2)
            Exit Sub
        End If
        Dim tdate As New Date
        If Date.TryParseExact(validdate.Text.Trim, "MM/dd/yyyy", Nothing, Nothing, tdate) = False Then
            showMessage("Valid date is not valid!", 2)
            Exit Sub
        End If
        If Session("GVadddetail") Is Nothing Then
            showMessage("Please fill insentive detail!", 2)
            Exit Sub
        Else
            Dim dtab As DataTable = Session("GVadddetail")
            If dtab.Rows.Count <= 0 Then
                showMessage("Please fill insentive detail!", 2)
                Exit Sub
            End If
        End If

        If Session("oid") <> Nothing And Session("oid") <> "" Then
            If savestatus.Text = "" Then
                savestatus.Text = "SAVE"
            End If
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            generateno()
        Else
            If savestatus.Text = "SAVEAS" Then
                generateno()
            End If
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Dim commstoid As Integer = 0
        If Session("oid") = Nothing Or Session("oid") = "" Then
            commstoid = GenerateID("QL_tis_mstcommission", CompnyCode)
        Else
            If savestatus.Text = "SAVEAS" Then
                commstoid = GenerateID("QL_tis_mstcommission", CompnyCode)
            ElseIf savestatus.Text = "STATUS" Then
                commstoid = Integer.Parse(oid.Text)
            ElseIf savestatus.Text = "SAVE" Then
                commstoid = Integer.Parse(oid.Text)
            End If
        End If
        Dim comdtloid As Integer = GenerateID("QL_tis_mstcommissiondtl", CompnyCode)

        Try
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSQL = "insert into QL_tis_mstcommission (cmpcode, commissionoid, commissioncode, validdate, commissionnote, commissionstatus, commissionflag, salesteamoid, resfield1, resfield2, createuser, createtime, upduser, updtime) values ('" & CompnyCode & "'," & commstoid & ",'" & Tchar(commissioncode.Text) & "','" & tdate & "','" & Tchar(commissionnote.Text) & "','" & commissionstatus.SelectedValue.ToString & "','" & commissionflag.SelectedValue.ToString & "'," & salesteam.SelectedValue & ",'','','" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp)"
                xCmd.CommandText = sSQL
                xCmd.ExecuteNonQuery()

                sSQL = "update QL_mstoid set lastoid=" & commstoid & " where tablename='QL_tis_mstcommission' and cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSQL
                xCmd.ExecuteNonQuery()

                If Session("GVadddetail") IsNot Nothing Then
                    Dim objTable As DataTable
                    objTable = Session("GVadddetail")
                    For C1 As Integer = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_tis_mstcommissiondtl (cmpcode, commissionoid, commissiondtloid, seq, rangeone, rangetwo, amountrange, insentiverate, paymentrateoid, regionoid, payrangeone, payrangetwo, commissionrate, resfield1, resfield2, upduser, updtime, kode_komisi) VALUES ('" & CompnyCode & "'," & commstoid & "," & comdtloid & "," & objTable.Rows(C1)("seq") & "," & objTable.Rows(C1)("rangeone") & "," & objTable.Rows(C1)("rangetwo") & ",'" & Tchar(objTable.Rows(C1)("amountrange")) & "'," & objTable.Rows(C1)("insentiverate") & "," & objTable.Rows(C1)("paymentrateoid") & "," & objTable.Rows(C1)("regionoid") & "," & objTable.Rows(C1)("payrangeone") & "," & objTable.Rows(C1)("payrangetwo") & "," & objTable.Rows(C1)("commissionrate") & ",'','','" & Session("UserID") & "',current_timestamp, '" & objTable.Rows(C1)("kode_komisi") & "')"
                        xCmd.CommandText = sSQL
                        xCmd.ExecuteNonQuery()
                        comdtloid = comdtloid + 1
                    Next
                    sSQL = "update QL_mstoid set lastoid=" & comdtloid - 1 & " where tablename='QL_tis_mstcommissiondtl' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSQL
                    xCmd.ExecuteNonQuery()
                End If
            Else
                If savestatus.Text = "SAVEAS" Then
                    sSQL = "insert into QL_tis_mstcommission (cmpcode, commissionoid, commissioncode, validdate, commissionnote, commissionstatus, commissionflag, salesteamoid, resfield1, resfield2, createuser, createtime, upduser, updtime) values ('" & CompnyCode & "'," & commstoid & ",'" & Tchar(commissioncode.Text) & "','" & tdate & "','" & Tchar(commissionnote.Text) & "','" & commissionstatus.SelectedValue.ToString & "','" & commissionflag.SelectedValue.ToString & "'," & salesteam.SelectedValue & ",'','','" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp)"
                    xCmd.CommandText = sSQL
                    xCmd.ExecuteNonQuery()

                    sSQL = "update QL_mstoid set lastoid=" & commstoid & " where tablename='QL_tis_mstcommission' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSQL
                    xCmd.ExecuteNonQuery()

                    If Session("GVadddetail") IsNot Nothing Then
                        Dim objTable As DataTable
                        objTable = Session("GVadddetail")
                        For C1 As Integer = 0 To objTable.Rows.Count - 1
                            sSql = "INSERT INTO QL_tis_mstcommissiondtl (cmpcode, commissionoid, commissiondtloid, seq, rangeone, rangetwo, amountrange, insentiverate, paymentrateoid, regionoid, payrangeone, payrangetwo, commissionrate, resfield1, resfield2, upduser, updtime, kode_komisi) VALUES ('" & CompnyCode & "'," & commstoid & "," & comdtloid & "," & objTable.Rows(C1)("seq") & "," & objTable.Rows(C1)("rangeone") & "," & objTable.Rows(C1)("rangetwo") & ",'" & Tchar(objTable.Rows(C1)("amountrange")) & "'," & objTable.Rows(C1)("insentiverate") & "," & objTable.Rows(C1)("paymentrateoid") & "," & objTable.Rows(C1)("regionoid") & "," & objTable.Rows(C1)("payrangeone") & "," & objTable.Rows(C1)("payrangetwo") & "," & objTable.Rows(C1)("commissionrate") & ",'','','" & Session("UserID") & "',current_timestamp, '" & objTable.Rows(C1)("kode_komisi") & "')"
                            xCmd.CommandText = sSQL
                            xCmd.ExecuteNonQuery()
                            comdtloid = comdtloid + 1
                        Next
                        sSQL = "update QL_mstoid set lastoid=" & comdtloid - 1 & " where tablename='QL_tis_mstcommissiondtl' and cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSQL
                        xCmd.ExecuteNonQuery()
                    End If
                ElseIf savestatus.Text = "STATUS" Then
                    sSQL = "update QL_tis_mstcommission set commissionstatus='" & commissionstatus.SelectedValue.ToString & "', upduser='" & Session("UserID") & "',updtime=current_timestamp where cmpcode='" & CompnyCode & "' and commissionoid=" & commstoid & ""
                    xCmd.CommandText = sSQL
                    xCmd.ExecuteNonQuery()
                ElseIf savestatus.Text = "SAVE" Then
                    sSQL = "update QL_tis_mstcommission set validdate='" & tdate & "',commissionnote='" & Tchar(commissionnote.Text) & "',commissionstatus='" & commissionstatus.SelectedValue & "',commissionflag='" & commissionflag.SelectedValue.ToString & "',salesteamoid=" & salesteam.SelectedValue & ",resfield1='',resfield2='',upduser='" & Session("UserID") & "',updtime=current_timestamp where cmpcode='" & CompnyCode & "' and commissionoid=" & commstoid & ""
                    xCmd.CommandText = sSQL
                    xCmd.ExecuteNonQuery()

                    sSQL = "delete from QL_tis_mstcommissiondtl where cmpcode='" & CompnyCode & "' and commissionoid=" & commstoid & " and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSQL
                    xCmd.ExecuteNonQuery()

                    If Session("GVadddetail") IsNot Nothing Then
                        Dim objTable As DataTable
                        objTable = Session("GVadddetail")
                        For C1 As Integer = 0 To objTable.Rows.Count - 1
                            sSql = "INSERT INTO QL_tis_mstcommissiondtl (cmpcode, commissionoid, commissiondtloid, seq, rangeone, rangetwo, amountrange, insentiverate, paymentrateoid, regionoid, payrangeone, payrangetwo, commissionrate, resfield1, resfield2, upduser, updtime, kode_komisi) VALUES ('" & CompnyCode & "'," & commstoid & "," & comdtloid & "," & objTable.Rows(C1)("seq") & "," & objTable.Rows(C1)("rangeone") & "," & objTable.Rows(C1)("rangetwo") & ",'" & Tchar(objTable.Rows(C1)("amountrange")) & "'," & objTable.Rows(C1)("insentiverate") & "," & objTable.Rows(C1)("paymentrateoid") & "," & objTable.Rows(C1)("regionoid") & "," & objTable.Rows(C1)("payrangeone") & "," & objTable.Rows(C1)("payrangetwo") & "," & objTable.Rows(C1)("commissionrate") & ",'','','" & Session("UserID") & "',current_timestamp, '" & objTable.Rows(C1)("kode_komisi") & "')"
                            xCmd.CommandText = sSQL
                            xCmd.ExecuteNonQuery()
                            comdtloid = comdtloid + 1
                        Next
                        sSQL = "update QL_mstoid set lastoid=" & comdtloid - 1 & " where tablename='QL_tis_mstcommissiondtl' and cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSQL
                        xCmd.ExecuteNonQuery()
                    End If
                End If
            End If

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("tis_mstcommissiontable.aspx?awal=true")
    End Sub

    Protected Sub btnsaveas_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsaveas.Click
        savestatus.Text = "SAVEAS"
        btnsave_Click(Nothing, Nothing)
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Private Sub clearadddetail()
        editaddseq.Text = ""
        amountrange1.Text = ""
        amountrange2.Text = ""
        insentiverate.Text = ""
        PayRangeOne.Text = ""
        PayRangeTwo.Text = ""
        region.SelectedIndex = -1
        paymentrate.SelectedIndex = -1
        region_SelectedIndexChanged(Nothing, Nothing)
        'commissionrate.Text = ""
        GVadddetail.SelectedIndex = -1
    End Sub

    Protected Sub btnaddvalue_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnaddvalue.Click
        'If amountrange1.Text = "" Then
        '    showMessage("Please fill payment range value!", 2)
        '    Exit Sub
        'End If
        'If amountrange2.Text = "" Then
        '    showMessage("Please fill payment range value!", 2)
        '    Exit Sub
        'End If
        If ToDouble(amountrange1.Text) > 0 And ToDouble(amountrange2.Text) > 0 Then
            If ToDouble(amountrange2.Text) < ToDouble(amountrange1.Text) Then
                showMessage("The second amount in payment range cannot less than the first amount in payment range!", 2)
                Exit Sub
            End If
        End If
        If ToDouble(PayRangeOne.Text) < 0 Then
            showMessage("Payment Range 1 must be more than 0!", 2)
            Exit Sub
        End If
        If ToDouble(PayRangeTwo.Text) <= 0 Then
            showMessage("Payment Range 2 must be more than 0!", 2)
            Exit Sub
        End If
        If ToDouble(amountrange1.Text) > 0 And ToDouble(amountrange2.Text) > 0 Then
            If ToInteger(PayRangeOne.Text) > ToInteger(PayRangeTwo.Text) Then
                showMessage("Payment Time Range 1 must less than Payment Time Range 2!", 2)
                Exit Sub
            End If
            If ToInteger(PayRangeOne.Text) = ToInteger(PayRangeTwo.Text) Then
                showMessage("Payment Time Range 1 must be difference with Payment Time Range 2!", 2)
                Exit Sub
            End If
        End If
        If ToDouble(insentiverate.Text) <= 0 Then
            showMessage("Commission Rate must be more than 0 !", 2)
            Exit Sub
        End If
        If ToDouble(commissionrate.Text) < 0 Then
            showMessage("Additional Commission Rate must be more than 0 !", 2)
            Exit Sub
        End If
        If paymentrate.Items.Count = 0 Then
            showMessage("Payment rate not defined!", 2)
            Exit Sub
        End If

        Dim dtab As DataTable = Nothing
        If Session("GVadddetail") Is Nothing Then
            dtab = New DataTable
            dtab.Columns.Add("seq", Type.GetType("System.Int32"))
            dtab.Columns.Add("rangeone", Type.GetType("System.Double"))
            dtab.Columns.Add("rangetwo", Type.GetType("System.Double"))
            dtab.Columns.Add("amountrange", Type.GetType("System.String"))
            dtab.Columns.Add("insentiverate", Type.GetType("System.Double"))
            dtab.Columns.Add("paymentrate", Type.GetType("System.Int32"))
            dtab.Columns.Add("paymentrateoid", Type.GetType("System.Int32"))
            dtab.Columns.Add("region", Type.GetType("System.String"))
            dtab.Columns.Add("regionoid", Type.GetType("System.Int32"))
            dtab.Columns.Add("payrangeone", Type.GetType("System.Int32"))
            dtab.Columns.Add("payrangetwo", Type.GetType("System.Int32"))
            dtab.Columns.Add("payrange", Type.GetType("System.String"))
            dtab.Columns.Add("commissionrate", Type.GetType("System.Double"))
            dtab.Columns.Add("kode_komisi", Type.GetType("System.String"))
        Else
            dtab = Session("GVadddetail")
        End If

        If dtab.Rows.Count > 0 Then
            Dim dv As DataView = dtab.DefaultView
            If editaddseq.Text <> "" Then
                dv.RowFilter = "rangeone=" & ToDouble(amountrange1.Text) & " and rangetwo=" & ToDouble(amountrange2.Text) & " and paymentrateoid=" & paymentrate.SelectedValue & " and regionoid=" & region.SelectedValue & " and payrangeone=" & Integer.Parse(paytermrange1.Text) & " and payrangetwo=" & Integer.Parse(paytermrange2.Text) & " and seq<>" & Integer.Parse(editaddseq.Text) & ""
            Else
                dv.RowFilter = "rangeone=" & ToDouble(amountrange1.Text) & " and rangetwo=" & ToDouble(amountrange2.Text) & " and paymentrateoid=" & paymentrate.SelectedValue & " and regionoid=" & region.SelectedValue & " and payrangeone=" & Integer.Parse(paytermrange1.Text) & " and payrangetwo=" & Integer.Parse(paytermrange2.Text) & ""
            End If
            If dv.Count > 0 Then
                dv.RowFilter = ""
                showMessage("This data has been inserted before!", 2)
                Exit Sub
            End If
            dv.RowFilter = ""
        End If

        Dim tmplinkseq As Integer = 0
        If editaddseq.Text <> "" Then
            Dim dv As DataView = dtab.DefaultView
            dv.RowFilter = "seq=" & Integer.Parse(editaddseq.Text) & ""
            tmplinkseq = Integer.Parse(editaddseq.Text)
            If dv.Count > 0 Then
                dv(0).BeginEdit()
                dv(0)("rangeone") = ToDouble(amountrange1.Text)
                dv(0)("rangetwo") = ToDouble(amountrange2.Text)
                dv(0)("amountrange") = NewMaskEdit(ToDouble(amountrange1.Text)) & " - " & NewMaskEdit(ToDouble(amountrange2.Text))
                dv(0)("insentiverate") = ToDouble(insentiverate.Text)
                dv(0)("paymentrate") = paymentrate.SelectedItem.ToString
                dv(0)("paymentrateoid") = paymentrate.SelectedValue
                dv(0)("region") = ""
                dv(0)("regionoid") = 0
                dv(0)("payrangeone") = ToInteger(PayRangeOne.Text) 'paytermrange1.SelectedValue
                dv(0)("payrangetwo") = ToInteger(PayRangeTwo.Text) 'paytermrange2.SelectedValue
                dv(0)("payrange") = paytermrange1.Text & " - " & paytermrange2.Text
                dv(0)("commissionrate") = Math.Round(ToDouble(commissionrate.Text), 2, MidpointRounding.AwayFromZero)
                dv(0)("kode_komisi") = kode_komisi.SelectedValue
                dv(0).EndEdit()
            End If
            dv.RowFilter = ""
            dtab.AcceptChanges()
        Else
            tmplinkseq = dtab.Rows.Count + 1
            Dim drow As DataRow = dtab.NewRow
            drow("seq") = dtab.Rows.Count + 1
            drow("rangeone") = ToDouble(amountrange1.Text)
            drow("rangetwo") = ToDouble(amountrange2.Text)
            drow("amountrange") = NewMaskEdit(ToDouble(amountrange1.Text)) & " - " & NewMaskEdit(ToDouble(amountrange2.Text))
            drow("insentiverate") = ToDouble(insentiverate.Text)
            drow("paymentrate") = paymentrate.SelectedItem.ToString
            drow("paymentrateoid") = paymentrate.SelectedValue
            drow("region") = ""
            drow("regionoid") = 0
            drow("payrangeone") = ToInteger(PayRangeOne.Text) 'Integer.Parse(paytermrange1.SelectedValue.ToString)
            drow("payrangetwo") = ToInteger(PayRangeTwo.Text) 'Integer.Parse(paytermrange2.SelectedValue.ToString)
            drow("payrange") = paytermrange1.Text & " - " & paytermrange2.Text
            drow("commissionrate") = Math.Round(ToDouble(commissionrate.Text), 2, MidpointRounding.AwayFromZero)
            drow("kode_komisi") = kode_komisi.SelectedValue
            dtab.Rows.Add(drow)
        End If

        Session("GVadddetail") = dtab
        GVadddetail.DataSource = dtab
        GVadddetail.DataBind()

        clearadddetail()
    End Sub

    Protected Sub btnclearadd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnclearadd.Click
        clearadddetail()
    End Sub

    Protected Sub GVadddetail_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVadddetail.RowDeleting
        Dim tmpindex As Integer = 0
        tmpindex = GVadddetail.DataKeys(e.RowIndex).Item(0)

        Dim dtab As DataTable = Session("GVadddetail")
        Dim objRow() As DataRow
        objRow = dtab.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        dtab.Rows.Remove(objRow(e.RowIndex))

        For C1 As Int16 = 0 To dtab.Rows.Count - 1
            Dim dr As DataRow = dtab.Rows(C1)
            dr.BeginEdit()
            dr("seq") = C1 + 1
            dr.EndEdit()
        Next
        Session("GVadddetail") = dtab
        GVadddetail.DataSource = dtab
        GVadddetail.DataBind()
    End Sub

    Protected Sub GVadddetail_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVadddetail.SelectedIndexChanged
        editaddseq.Text = GVadddetail.SelectedDataKey("seq")
        If Session("GVadddetail") IsNot Nothing Then
            Dim dtab As DataTable = Session("GVadddetail")
            Dim dv As DataView = dtab.DefaultView
            dv.RowFilter = "seq=" & Integer.Parse(editaddseq.Text) & ""

            If dv.Count > 0 Then
                amountrange1.Text = NewMaskEdit(dv(0).Item("rangeone"))
                amountrange2.Text = NewMaskEdit(dv(0).Item("rangetwo"))
                insentiverate.Text = NewMaskEdit(dv(0).Item("insentiverate"))
                paymentrate.SelectedValue = dv(0).Item("paymentrateoid")
                'region.SelectedValue = dv(0).Item("regionoid")
                'region_SelectedIndexChanged(Nothing, Nothing)
                'paytermrange1.SelectedValue = dv(0).Item("payrangeone")
                'paytermrange2.SelectedValue = dv(0).Item("payrangetwo")
                PayRangeOne.Text = dv(0).Item("payrangeone")
                PayRangeTwo.Text = dv(0).Item("payrangetwo")
                kode_komisi.SelectedValue = dv(0).Item("kode_komisi")
                commissionrate.Text = NewMaskEdit(dv(0).Item("commissionrate"))
            End If

            dv.RowFilter = ""
        End If
    End Sub

    Protected Sub region_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles region.SelectedIndexChanged
        If Integer.Parse(region.SelectedValue) = 1 Then
            paymentrate.SelectedValue = paymentrate.Items.FindByText("60").Value
        Else
            paymentrate.SelectedValue = paymentrate.Items.FindByText("90").Value
        End If

        'Fill payment term range
        paytermrange1.Items.Clear()
        Dim tmprange1 As String = ""
        paytermrange1.Items.Add("0")
        paytermrange1.Items(paytermrange1.Items.Count - 1).Value = 0
        For i As Integer = 0 To 6
            tmprange1 = ((i * 15) + 1 + Integer.Parse(paymentrate.SelectedItem.ToString)).ToString
            paytermrange1.Items.Add(tmprange1)
            paytermrange1.Items(paytermrange1.Items.Count - 1).Value = Integer.Parse(tmprange1)
        Next
        paytermrange2.Items.Clear()
        Dim tmprange2 As String = ""
        paytermrange2.Items.Add(paymentrate.SelectedItem.ToString)
        paytermrange2.Items(paytermrange2.Items.Count - 1).Value = Integer.Parse(paymentrate.SelectedItem.ToString)
        For i As Integer = 0 To 6
            tmprange2 = (((i + 1) * 15) + Integer.Parse(paymentrate.SelectedItem.ToString)).ToString
            paytermrange2.Items.Add(tmprange2)
            paytermrange2.Items(paytermrange2.Items.Count - 1).Value = Integer.Parse(tmprange2)
        Next
        paytermrange2.Items.Add("~")
        paytermrange2.Items(paytermrange2.Items.Count - 1).Value = 9999
    End Sub

    Protected Sub amountrange1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles amountrange1.TextChanged
        amountrange1.Text = ToMaskEdit(ToDouble(amountrange1.Text), 2)
        cProc.SetFocusToControl(Me.Page, amountrange2)
    End Sub

    Protected Sub amountrange2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles amountrange2.TextChanged
        amountrange2.Text = ToMaskEdit(ToDouble(amountrange2.Text), 2)
        cProc.SetFocusToControl(Me.Page, insentiverate)
    End Sub

    Protected Sub btnupdatestatus_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnupdatestatus.Click
        savestatus.Text = "STATUS"
        btnsave_Click(Nothing, Nothing)
    End Sub

    Protected Sub GVadddetail_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
        '    e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 4)
        'End If
    End Sub
End Class
