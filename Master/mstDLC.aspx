<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="mstDLC.aspx.vb" Inherits="Master_DirectLaborCost" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Direct Labor Cost" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Direct Labor Cost :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" DefaultButton="btnSearch" __designer:wfdid="w28"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 8%" class="Label" align=left>Filter</TD><TD style="WIDTH: 2%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="105px" __designer:wfdid="w29"><asp:ListItem Value="dlc.dlcoid">ID</asp:ListItem>
<asp:ListItem Value="bom.bomdesc">BOM</asp:ListItem>
<asp:ListItem Value="i.itemcode">Code</asp:ListItem>
<asp:ListItem Value="i.itemlongdescription">Description</asp:ListItem>
<asp:ListItem Value="dlcnote">Note</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="250px" __designer:wfdid="w30"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text="Period" __designer:wfdid="w31"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="60px" ToolTip="MM/dd/yyyy" __designer:wfdid="w32"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w33"></asp:ImageButton> <asp:Label id="Label5" runat="server" Text="to" __designer:wfdid="w34"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="60px" ToolTip="MM/dd/yyyy" __designer:wfdid="w35"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w36"></asp:ImageButton>&nbsp;<asp:Label id="Label311" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w37"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status" __designer:wfdid="w38"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="105px" __designer:wfdid="w39">
<asp:ListItem>ACTIVE</asp:ListItem>
<asp:ListItem>INACTIVE</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" TargetControlID="FilterPeriod1" __designer:wfdid="w40" Format="MM/dd/yyyy" PopupButtonID="imbDate1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="FilterPeriod1" __designer:wfdid="w41" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" TargetControlID="FilterPeriod2" __designer:wfdid="w42" Format="MM/dd/yyyy" PopupButtonID="imbDate2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" TargetControlID="FilterPeriod2" __designer:wfdid="w43" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w44"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w45"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPrintHdr" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w46"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvTRN" runat="server" ForeColor="#333333" Width="100%" GridLines="None" CellPadding="4" AutoGenerateColumns="False" PageSize="8" AllowPaging="True" __designer:wfdid="w47" AllowSorting="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="dlcoid" DataNavigateUrlFormatString="~\Master\mstDLC.aspx?oid={0}" DataTextField="dlcoid" HeaderText="ID" SortExpression="dlcoid">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="bomdesc" HeaderText="BOM" SortExpression="bomdesc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dlcdate" HeaderText="Date" SortExpression="dlcdate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Code" SortExpression="itemcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdesc" HeaderText="Description" SortExpression="itemlongdesc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="activeflag" HeaderText="Status" SortExpression="activeflag">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dlcnote" HeaderText="Note" SortExpression="dlcnote">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
            <asp:CheckBox ID="cbGVMst" runat="server" Checked='<%# eval("checkvalue") %>' />
            <asp:Label ID="lblOidGVMst" runat="server" Text='<%# eval("dlcoid") %>' Visible="False"></asp:Label>
        
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="No Data Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w48"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=4><asp:UpdateProgress id="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel2" __designer:wfdid="w49"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image12" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w50"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrintHdr"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label16" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Direct Labor Cost Header" Font-Underline="False" __designer:wfdid="w309"></asp:Label></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="i_u" runat="server" CssClass="Important" Text="New Data" __designer:wfdid="w310"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="bomoid" runat="server" __designer:wfdid="w311" Visible="False"></asp:Label> <asp:Label id="dlccounter" runat="server" __designer:wfdid="w312" Visible="False"></asp:Label></TD><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="lastbomoid" runat="server" __designer:wfdid="w313" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="dlcres1" runat="server" __designer:wfdid="w314" Visible="False"></asp:Label></TD></TR><TR runat="server" visible="true"><TD class="Label" align=left><asp:Label id="Label18" runat="server" Text="ID" __designer:wfdid="w315"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:Label id="dlcoid" runat="server" __designer:wfdid="w316"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR id="BusinessUnit" runat="server" visible="true"><TD class="Label" align=left><asp:Label id="Label28" runat="server" Text="Business Unit" __designer:wfdid="w317" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpText" Width="305px" __designer:wfdid="w318" Visible="False" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label23" runat="server" Text="BOM" __designer:wfdid="w319"></asp:Label> <asp:Label id="Label25" runat="server" CssClass="Important" Text="*" __designer:wfdid="w320"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="bomdesc" runat="server" CssClass="inpTextDisabled" Width="260px" __designer:wfdid="w321" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchBOM" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w322"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearBOM" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w323"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="Label6" runat="server" Text="Date" __designer:wfdid="w324"></asp:Label>&nbsp;<asp:Label id="Label7" runat="server" CssClass="Important" Text="*" __designer:wfdid="w325"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="dlcdate" runat="server" CssClass="inpTextDisabled" Width="80px" __designer:wfdid="w326" AutoPostBack="True" Enabled="False" ToolTip="MM/dd/yyyy"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label31" runat="server" Text="Code FG" __designer:wfdid="w327"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="itemcode" runat="server" CssClass="inpTextDisabled" Width="260px" __designer:wfdid="w328" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label30" runat="server" Text="Finish Good" __designer:wfdid="w329"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="itemlongdesc" runat="server" CssClass="inpTextDisabled" Width="300px" __designer:wfdid="w330" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label2" runat="server" Text="Description" __designer:wfdid="w331"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="dlcdesc" runat="server" CssClass="inpText" Width="300px" __designer:wfdid="w332"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label4" runat="server" Text="Total Cost %" __designer:wfdid="w333"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="dlctotalpct" runat="server" CssClass="inpTextDisabled" Width="50px" __designer:wfdid="w334" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label21" runat="server" Text="DL Cost" __designer:wfdid="w335"></asp:Label> <asp:Label id="Label26" runat="server" CssClass="Important" Text="*" __designer:wfdid="w336"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="dlcamount" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w337" AutoPostBack="True" MaxLength="16"></asp:TextBox>&nbsp;/ <asp:DropDownList id="DDLUnitDL" runat="server" CssClass="inpTextDisabled" Width="85px" __designer:wfdid="w2" Enabled="False"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label10" runat="server" Text="Max DLC" __designer:wfdid="w338" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="precostdlcamt" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w339" Visible="False" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label19" runat="server" Text="OHD Cost" __designer:wfdid="w340"></asp:Label> <asp:Label id="Label27" runat="server" CssClass="Important" Text="*" __designer:wfdid="w341"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="dlcoverheadamt" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w342" AutoPostBack="True" MaxLength="16"></asp:TextBox>&nbsp;/ <asp:DropDownList id="DDLUnitOHD" runat="server" CssClass="inpTextDisabled" Width="85px" __designer:wfdid="w3" Enabled="False"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label13" runat="server" Text="Max OHD" __designer:wfdid="w343" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="precostoverheadamt" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w344" Visible="False" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text="DLC COA" __designer:wfdid="w345"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="dlcacctgoid" runat="server" CssClass="inpText" Width="305px" __designer:wfdid="w346">
            </asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label20" runat="server" Text="OHD COA" __designer:wfdid="w347"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="ohdacctgoid" runat="server" CssClass="inpText" Width="305px" __designer:wfdid="w348"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label8" runat="server" Text="Header Note" __designer:wfdid="w349"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="dlcnote" runat="server" CssClass="inpText" Width="300px" __designer:wfdid="w350" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label9" runat="server" Text="Status" __designer:wfdid="w351"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="activeflag" runat="server" CssClass="inpText" Width="85px" __designer:wfdid="w352"><asp:ListItem>ACTIVE</asp:ListItem>
<asp:ListItem>INACTIVE</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="ftbDLC" runat="server" __designer:wfdid="w353" ValidChars="0123456789.," TargetControlID="dlcamount">
            </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbOHD" runat="server" __designer:wfdid="w354" ValidChars="0123456789.," TargetControlID="dlcoverheadamt">
            </ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><asp:Label id="Label17" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Direct Labor Cost Detail" Font-Underline="False" __designer:wfdid="w355"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="i_u2" runat="server" Text="New Detail" __designer:wfdid="w356" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="dlcdtlseq" runat="server" __designer:wfdid="w357" Visible="False">1</asp:Label> <asp:Label id="dlcdtloid" runat="server" __designer:wfdid="w358" Visible="False"></asp:Label> <asp:Label id="bomdtl1oid" runat="server" __designer:wfdid="w359" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="lastpct" runat="server" __designer:wfdid="w360" Visible="False">0</asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label12" runat="server" Text="Department" __designer:wfdid="w361"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="deptoid" runat="server" CssClass="inpText" Width="305px" __designer:wfdid="w362"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label22" runat="server" Text="Cost %" __designer:wfdid="w363"></asp:Label> <asp:Label id="Label29" runat="server" CssClass="Important" Text="*" __designer:wfdid="w364"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="dlcpct" runat="server" CssClass="inpText" Width="50px" __designer:wfdid="w365" AutoPostBack="True" MaxLength="8"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label24" runat="server" Text="DL Cost Amt" __designer:wfdid="w366"></asp:Label> <asp:Label id="Label11" runat="server" CssClass="Important" Text="*" __designer:wfdid="w367"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="dlcdtlamount" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w368" AutoPostBack="True" MaxLength="16"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label14" runat="server" Text="OHD Cost Amt" __designer:wfdid="w369"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="dlcohdamount" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w370" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label15" runat="server" Text="Detail Note" __designer:wfdid="w371"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="dlcdtlnote" runat="server" CssClass="inpText" Width="300px" __designer:wfdid="w372" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="ftbPct" runat="server" __designer:wfdid="w373" ValidChars="0123456789.," TargetControlID="dlcpct">
            </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbCost" runat="server" __designer:wfdid="w374" ValidChars="0123456789.," TargetControlID="dlcdtlamount">
            </ajaxToolkit:FilteredTextBoxExtender>&nbsp; </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w375"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w376"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:GridView id="gvDtl" runat="server" ForeColor="#333333" Width="98%" __designer:wfdid="w377" DataKeyNames="dlcdtlseq" GridLines="None" CellPadding="4" AutoGenerateColumns="False" PageSize="5">
<RowStyle BackColor="#E3EAEB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="deptname" HeaderText="Department">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dlcpct" HeaderText="Cost %">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dlcdtlamount" HeaderText="DL Cost Amt.">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dlcohdamount" HeaderText="OHD Cost Amt.">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dlcdtlnote" HeaderText="Detail Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#666666" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#7C6F57"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="lblWarning" runat="server" ForeColor="Red" __designer:wfdid="w378"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=6></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w379"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w380"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w381"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w382"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w383" AlternateText="Save"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w384" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w385" AlternateText="Delete"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSaveAs" runat="server" ImageUrl="~/Images/saveas.png" ImageAlign="AbsMiddle" __designer:wfdid="w386" Visible="False"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w387" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w388"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Direct Labor Cost :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upListBOM" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListBOM" runat="server" CssClass="modalBox" Width="700px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListBOM" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of BOM" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListBOM" runat="server" Width="100%" DefaultButton="btnFindListBOM">Filter : <asp:DropDownList id="FilterDDLListBOM" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="i.itemcode">FG Code</asp:ListItem>
<asp:ListItem Value="i.itemlongdescription">Finish Good</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListBOM" runat="server" CssClass="inpText" Width="200px"></asp:TextBox> <asp:ImageButton id="btnFindListBOM" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListBOM" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListBOM" runat="server" ForeColor="#333333" Width="99%" PageSize="5" AutoGenerateColumns="False" CellPadding="4" GridLines="None" DataKeyNames="bomoid,bomdesc,precostdlcamt,precostoverheadamt,itemcode,itemlongdesc,updtime,itemunitoid" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="FG Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdesc" HeaderText="Finish Good">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomdesc" HeaderText="Description" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomdate" HeaderText="Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label114" runat="server" Text="No data found !!" CssClass="Important"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListBOM" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListBOM" runat="server" TargetControlID="btnHideListBOM" PopupDragHandleControlID="lblTitleListBOM" BackgroundCssClass="modalBackground" PopupControlID="pnlListBOM" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListBOM" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

