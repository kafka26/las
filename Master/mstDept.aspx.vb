Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_Department
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Function"
    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        If deptcode.Text = "" Then
            sError &= "- Please fill CODE field!<BR>"
        End If
        If deptname.Text = "" Then
            sError &= "- Please fill NAME field!<BR>"
        End If
        Dim sErr As String = ""
        If deptcreationdate.Text = "" Then
            sError &= "- Please fill CREATION DATE field!<BR>"
        Else
            If Not IsValidDate(deptcreationdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- CREATION DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        Dim oMatches As MatchCollection
        If deptemail.Text <> "" Then
            oMatches = Regex.Matches(deptemail.Text, "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")
            If oMatches.Count <= 0 Then
                sError &= "- EMAIL is invalid. Format should like '<STRONG>mail@sample.com</STRONG>'.<BR>"
            End If
        End If
        If deptcityoid.SelectedValue = "" Then
            sError &= "- Please select CITY field!<br>"
        End If
        If divoid.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<br>"
        End If
        If deptacctgoid.SelectedValue = "" Then
            sError &= "- Please select COA USAGE field!<br>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputExists() As Boolean
        Dim sError As String = ""
        sSql = "SELECT COUNT(*) FROM QL_mstdept WHERE deptcode='" & Tchar(deptcode.Text.Trim) & "' AND divoid=" & divoid.SelectedValue
        If Not Session("oid") = Nothing And Session("oid") <> "" Then
            sSql &= " AND deptoid<>" & Session("oid")
        End If
        If ToDouble(GetStrData(sSql).ToString) > 0 Then
            showMessage("CODE has been used by another data in Business Unit " & divoid.SelectedItem.Text & ". Please fill another CODE!", 2)
            Return True
        End If
        sSql = "SELECT COUNT(*) FROM QL_mstdept WHERE deptname='" & Tchar(deptname.Text.Trim) & "' AND divoid=" & divoid.SelectedValue
        If Not Session("oid") = Nothing And Session("oid") <> "" Then
            sSql &= " AND deptoid<>" & Session("oid")
        End If
        If ToDouble(GetStrData(sSql).ToString) > 0 Then
            showMessage("NAME has been used by another data in Business Unit " & divoid.SelectedItem.Text & ". Please fill another NAME!", 2)
            Return True
        End If
        Return False
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitAllDDL()
        ' DDL City
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE gengroup='CITY' AND activeflag='ACTIVE'"
        FillDDL(deptcityoid, sSql)
        If deptcityoid.Items.Count = 0 Then
            lblWarning.Text &= "- Please fill <STRONG>City</STRONG> data in <STRONG>General Form</STRONG> with selected group is CITY!<BR>"
        End If
        ' DDL Division
        sSql = "SELECT divoid, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(divoid, sSql)
        FillDDL(FilterDDLDiv, sSql)
        If divoid.Items.Count = 0 Then
            lblWarning.Text &= "- Please fill <STRONG>Business Unit</STRONG> data in <STRONG>Business Unit Form</STRONG>!"
        Else
            SetDivCode(divoid.SelectedValue)
        End If
    End Sub

    Private Sub BindMstData()
        sSql = "SELECT deptoid, deptcode, deptname, divname, CONVERT(varchar(10), deptcreationdate, 101) AS creationdate, depttotalperson, deptpic, (deptaddress + ' ' + gendesc + (CASE deptphone WHEN NULL THEN '' WHEN '' THEN '' ELSE '. Phone: ' + deptphone END) + (CASE deptemail WHEN NULL THEN '' WHEN '' THEN '' ELSE '. Email: ' + deptemail END)) AS deptaddress FROM QL_mstdept de INNER JOIN QL_mstdivision di ON de.divoid=di.divoid AND di.activeflag='ACTIVE' INNER JOIN QL_mstgen g ON genoid=deptcityoid AND g.activeflag='ACTIVE'"
        If FilterDDL.SelectedValue = "Address" Then
            sSql &= " WHERE (deptaddress LIKE '%" & Tchar(FilterText.Text.Trim) & "%' OR gendesc LIKE '%" & Tchar(FilterText.Text.Trim) & "%' OR deptphone LIKE '%" & Tchar(FilterText.Text.Trim) & "%' OR deptemail LIKE '%" & Tchar(FilterText.Text.Trim) & "%')"
        Else
            sSql &= " WHERE " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text.Trim) & "%'"
        End If
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND de.cmpcode='" & Session("CompnyCode") & "'"
        End If
        If cbDiv.Checked Then
            If FilterDDLDiv.Items.Count > 0 Then
                sSql &= " AND de.divoid=" & FilterDDLDiv.SelectedValue
            Else
                cbDiv.Checked = False
            End If
        End If
        If cbStatus.Checked Then
            If FilterDDLStatus.SelectedValue <> "All" Then
                sSql &= " AND de.activeflag='" & FilterDDLStatus.SelectedValue & "'"
            End If
        End If
        If checkPagePermission("~\Master\mstDept.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND de.createuser='" & Session("UserID") & "'"
        End If
        sSql &= " ORDER BY deptoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_mstdept")
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub FillTextBox()
        Try
            sSql = "SELECT cmpcode, deptoid, deptcode, deptname, deptcreationdate, deptmaxperson, deptmaxbasesalary, depttotalperson, depttotalbasesalary, deptaddress, deptcityoid, deptphone, deptemail, deptpic, deptnote, activeflag, divoid, createuser, createtime, upduser, updtime, deptacctgoid FROM QL_mstdept WHERE deptoid=" & Session("oid")
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            btnDelete.Visible = False
            While xreader.Read
                deptoid.Text = xreader("deptoid").ToString
                deptcode.Text = xreader("deptcode").ToString
                deptname.Text = xreader("deptname").ToString.Trim
                deptcreationdate.Text = Format(xreader("deptcreationdate"), "MM/dd/yyyy")
                deptmaxperson.Text = xreader("deptmaxperson")
                deptmaxbasesalary.Text = ToMaskEdit(xreader("deptmaxbasesalary"), 4)
                depttotalperson.Text = xreader("depttotalperson")
                depttotalbasesalary.Text = ToMaskEdit(xreader("depttotalbasesalary"), 4)
                deptaddress.Text = Trim(xreader("deptaddress").ToString)
                deptcityoid.SelectedValue = xreader("deptcityoid")
                deptphone.Text = xreader("deptphone").ToString
                deptemail.Text = xreader("deptemail").ToString
                deptpic.Text = xreader("deptpic").ToString
                deptnote.Text = xreader("deptnote").ToString
                activeflag.SelectedValue = xreader("activeflag")
                divoid.SelectedValue = xreader("divoid")
                divoid_SelectedIndexChanged(Nothing, Nothing)
                divcode.Text = xreader("cmpcode").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
                deptacctgoid.SelectedValue = xreader("deptacctgoid").ToString
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            'showMessage(ex.Message, 1)
        Finally
            deptcode.CssClass = "inpTextDisabled"
            deptcode.ReadOnly = True
            btnDelete.Visible = True
            divoid.CssClass = "inpTextDisabled"
            divoid.Enabled = False
        End Try
    End Sub

    Private Sub ShowReport()
        'Try
        '    report.Load(Server.MapPath(folderReport & "rptDept.rpt"))
        '    Dim sWhere As String = " WHERE de.cmpcode='" & CompnyCode & "' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text.Trim) & "%'"
        '    If cbDiv.Checked Then
        '        If FilterDDLDiv.Items.Count > 0 Then
        '            sWhere &= " AND de.divoid=" & FilterDDLDiv.SelectedValue
        '        Else
        '            cbDiv.Checked = False
        '        End If
        '    End If
        '    If cbStatus.Checked Then
        '        If FilterDDLStatus.SelectedValue <> "All" Then
        '            sWhere &= " AND de.activeflag='" & FilterDDLStatus.SelectedValue & "'"
        '        End If
        '    End If
        '    If checkPagePermission("~\Master\mstDept.aspx", Session("SpecialAccess")) = False Then
        '        sWhere &= " AND de.createuser='" & Session("UserID") & "'"
        '    End If
        '    sWhere &= " ORDER BY de.deptoid"
        '    report.SetParameterValue("sWhere", sWhere)
        '    cProc.SetDBLogonForReport(report)
        '    report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
        '    Response.Buffer = False
        '    Response.ClearContent()
        '    Response.ClearHeaders()
        '    report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "DepartmentReport_" & Format(GetServerTime(), "yyyyMMddHHmmss"))
        '    report.Close()
        '    report.Dispose()
        'Catch ex As Exception
        '    report.Close()
        '    report.Dispose()
        '    showMessage(ex.Message, 1)
        'End Try
    End Sub

    Private Sub SetDivCode(ByVal sOid As String)
        sSql = "SELECT DISTINCT TOP 1 divcode FROM QL_mstdivision WHERE divoid=" & sOid
        divcode.Text = GetStrData(sSql).ToString
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Master\mstDept.aspx")
        End If
        If checkPagePermission("~\Master\mstDept.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Department"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            I_U.Text = "New Data"
        Else
            I_U.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            InitAllDDL()
            deptcreationdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox()
                TabContainer1.ActiveTabIndex = 1
            Else
                deptoid.Text = GenerateID("QL_MSTDEPT", CompnyCode)
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
                divoid_SelectedIndexChanged(Nothing, Nothing)
            End If
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindMstData()
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbDiv.Checked = False
        FilterDDLDiv.SelectedIndex = -1
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        BindMstData()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() And Not IsInputExists() Then
            If Session("oid") = Nothing Or Session("oid") = "" Then
                deptoid.Text = GenerateID("QL_MSTDEPT", CompnyCode)
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_mstdept (cmpcode, deptoid, divoid, deptcode, deptname, deptcreationdate, deptmaxperson, deptmaxbasesalary, depttotalperson, depttotalbasesalary, deptaddress, deptcityoid, deptphone, deptemail, deptpic, deptnote, activeflag, createuser, upduser, updtime, createtime, deptacctgoid) VALUES ('" & divcode.Text & "', " & deptoid.Text & ", " & divoid.SelectedValue & ", '" & Tchar(deptcode.Text.Trim) & "', '" & Tchar(deptname.Text.Trim) & "', '" & deptcreationdate.Text & "', " & ToDouble(deptmaxperson.Text) & ", " & ToDouble(deptmaxbasesalary.Text) & ", " & ToDouble(depttotalperson.Text) & ", " & ToDouble(depttotalbasesalary.Text) & ", '" & Tchar(deptaddress.Text.Trim) & "', " & deptcityoid.SelectedValue & ", '" & Tchar(deptphone.Text.Trim) & "', '" & Tchar(deptemail.Text.Trim) & "', '" & Tchar(deptpic.Text.Trim) & "', '" & Tchar(deptnote.Text.Trim) & "', '" & activeflag.SelectedValue & "', '" & Session("UserId") & "', '" & Session("UserId") & "', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, " & deptacctgoid.SelectedValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & deptoid.Text & " WHERE tablename LIKE 'QL_MSTDEPT' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_mstdept SET deptname='" & Tchar(deptname.Text.Trim) & "', deptcreationdate='" & deptcreationdate.Text & "', deptmaxperson=" & ToDouble(deptmaxperson.Text) & ", deptmaxbasesalary=" & ToDouble(deptmaxbasesalary.Text) & ", depttotalperson=" & ToDouble(depttotalperson.Text) & ", depttotalbasesalary=" & ToDouble(depttotalbasesalary.Text) & ", deptaddress='" & Tchar(deptaddress.Text.Trim) & "', deptcityoid=" & deptcityoid.SelectedValue & ", deptphone='" & Tchar(deptphone.Text.Trim) & "', deptemail='" & Tchar(deptemail.Text.Trim) & "', deptpic='" & Tchar(deptpic.Text.Trim) & "', deptnote='" & Tchar(deptnote.Text.Trim) & "', activeflag='" & activeflag.SelectedValue & "', upduser='" & Session("UserId") & "', updtime=CURRENT_TIMESTAMP, deptacctgoid=" & deptacctgoid.SelectedValue & " WHERE deptoid=" & deptoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                conn.Close()
                showMessage(ex.Message, 1)
                Exit Sub
            End Try
            Response.Redirect("~\Master\mstDept.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Master\mstDept.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If deptoid.Text = "" Then
            showMessage("Please select department data first!", 1)
            Exit Sub
        End If
        sSql = "SELECT ta.name FROM sys.tables ta INNER JOIN sys.columns col ON col.object_id=ta.object_id WHERE col.name='deptoid' AND ta.name NOT IN ('QL_mstdept')"
        Dim objTblUsage As DataTable = cKon.ambiltabel(sSql, "QL_tables")
        For C1 As Integer = 0 To objTblUsage.Rows.Count - 1
            If CheckDataExists("SELECT COUNT(*) FROM " & objTblUsage.Rows(C1).Item("name").ToString & " WHERE deptoid=" & deptoid.Text) Then
                showMessage("This data can't be deleted because it is being used by another data!", 2)
                Exit Sub
            End If
        Next
        If DeleteData("QL_mstdept", "deptoid", deptoid.Text, divcode.Text) = True Then
            Response.Redirect("~\Master\mstDept.aspx?awal=true")
        End If
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
    End Sub

    Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvMst.DataSource = Session("TblMst")
            gvMst.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub divoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles divoid.SelectedIndexChanged
        SetDivCode(divoid.SelectedValue)
        If divcode.Text <> "" Then
            FillDDLAcctg(deptacctgoid, "VAR_USAGE_NON_KIK", divcode.Text)
        End If
    End Sub
#End Region

End Class
