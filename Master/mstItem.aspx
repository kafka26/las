<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="mstitem.aspx.vb" Inherits="Master_mstitem" title="PT. PRKP - Item Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
                  <table id="Table2" bgcolor="white" border="0" cellpadding="5" cellspacing="1" class="tabelhias"
                    width="100%">
                    <tr>
                        <td align="left" class="header" valign="center">
                            <asp:Label ID="Label23" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Maroon" Text=".: Item/Material Data"></asp:Label>
                        </td>
                    </tr>
                </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
<ContentTemplate>
    <asp:UpdatePanel id="UpdatePanelSearch" runat="server">
        <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" DefaultButton="btnSearch" __designer:wfdid="w1"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 70px" class="Label" align=left>Filter :</TD><TD align=left colSpan=5><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" __designer:wfdid="w2"><asp:ListItem Value="itemCode">Code</asp:ListItem>
<asp:ListItem Value="itemLongDescription">Description</asp:ListItem>
    <asp:ListItem Value="itemoldcode">Old Code</asp:ListItem>
<asp:ListItem Value="itemType">Type</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" Width="304px" CssClass="inpText" MaxLength="30" __designer:wfdid="w3"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 70px" class="Label" align=left>Type :</TD><TD align=left colSpan=5><asp:DropDownList id="FilterType" runat="server" Width="300px" CssClass="inpText" OnSelectedIndexChanged="FilterType_SelectedIndexChanged" AutoPostBack="True" __designer:wfdid="w4"><asp:ListItem Value="INV">INVENTORY</asp:ListItem>
<asp:ListItem Value="NON">NON INVENTORY</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="WIDTH: 70px" class="Label" align=left><asp:CheckBox id="chkGroup" runat="server" Text="Group" CssClass="Label" __designer:wfdid="w5"></asp:CheckBox></TD><TD align=left colSpan=5><asp:DropDownList id="FilterGroup" runat="server" Width="300px" CssClass="inpText" OnSelectedIndexChanged="FilterGroup_SelectedIndexChanged" AutoPostBack="True" __designer:wfdid="w6"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 70px; HEIGHT: 10px" align=left><asp:CheckBox id="Chkcat1" runat="server" Text="Cat 1" CssClass="Label" __designer:wfdid="w7"></asp:CheckBox></TD><TD style="HEIGHT: 10px" align=left colSpan=5><asp:DropDownList id="DDLFilterCat1" runat="server" Width="300px" CssClass="inpText" OnSelectedIndexChanged="DDLFilterCat1_SelectedIndexChanged" AutoPostBack="True" __designer:wfdid="w8"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 70px; HEIGHT: 10px" align=left><asp:CheckBox id="Chkcat2" runat="server" Text="Cat 2" CssClass="Label" __designer:wfdid="w9"></asp:CheckBox></TD><TD style="HEIGHT: 10px" align=left colSpan=5><asp:DropDownList id="DDLFilterCat2" runat="server" Width="300px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w10"></asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 10px" align=left colSpan=6><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w11"></asp:ImageButton> <asp:ImageButton id="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w12"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w13"></asp:ImageButton> <asp:ImageButton id="btnexcell" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w14"></asp:ImageButton> <asp:ImageButton id="ImbPriceList" onclick="ImbPriceList_Click" runat="server" ImageUrl="~/Images/price_list.png" ImageAlign="AbsMiddle" Width="112px" __designer:wfdid="w15"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 10px" align=left colSpan=6><asp:GridView id="GVmstmat" runat="server" Width="100%" __designer:wfdid="w16" PageSize="8" GridLines="None" OnPageIndexChanging="GVmstmat_PageIndexChanging" AllowPaging="True" DataKeyNames="itemoid,itemCode" AutoGenerateColumns="False" EmptyDataText="No data in database." CellPadding="4" OnRowDataBound="GVmstmat_RowDataBound" BorderStyle="None">
<RowStyle BackColor="#EFF3FB" ForeColor="White"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="itemoid" DataNavigateUrlFormatString="~\master\mstitem.aspx?oid={0}" DataTextField="itemCode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="itemoldcode" HeaderText="Old Code">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemLongDescription" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="Group">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="ItemType" HeaderText="Type">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemPriceList" HeaderText="Price">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
    <asp:BoundField DataField="suppname" HeaderText="Supplier/Penjahit">
        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
        <ItemStyle HorizontalAlign="Left" />
    </asp:BoundField>
<asp:BoundField DataField="itemRecordStatus" HeaderText="Status">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
    <asp:TemplateField>
        <ItemTemplate>
            <asp:ImageButton ID="ImbPrint" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/print.gif"
                OnClick="ImbPrint_Click" ToolTip='<%# eval("itemoid") %>' />
        </ItemTemplate>
        <HeaderStyle CssClass="gvhdr" />
    </asp:TemplateField>
</Columns>

<FooterStyle BackColor="#CCCC99"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblmsg" runat="server" ForeColor="Red" Text="No Data Found !"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True"></SelectedRowStyle>

<HeaderStyle BackColor="#124E81" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE></asp:Panel>&nbsp; 
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnexcell"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="GVmstmat"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ImbPriceList"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>
    &nbsp;
</ContentTemplate>
<HeaderTemplate>
    <asp:Image ID="Image111esrdftybhjk" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" /><SPAN style="FONT-SIZE: 9pt"><STRONG> List of Item/Material :.&nbsp;</STRONG></SPAN> 
</HeaderTemplate>
</ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
<ContentTemplate>
<asp:UpdatePanel id="UpdatePanel1" runat="server"><ContentTemplate>
<TABLE><TBODY><TR><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:TextBox id="CodeItem" runat="server" Font-Size="X-Small" Text="" Width="150px" CssClass="inpTextDisabled" MaxLength="30" Enabled="False" Visible="False" __designer:wfdid="w1"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">Type</TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:DropDownList id="matinvtype" runat="server" Width="155px" CssClass="inpText" OnSelectedIndexChanged="matinvtype_SelectedIndexChanged" AutoPostBack="True"><asp:ListItem Value="INV">INVENTORY</asp:ListItem>
<asp:ListItem Value="NON">NON INVENTORY</asp:ListItem>
</asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">Group</TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:DropDownList id="matgroup" runat="server" Width="155px" CssClass="inpText" OnSelectedIndexChanged="matgroup_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">Cat 1 <asp:Label id="Label5" runat="server" Text="*" CssClass="Important" __designer:wfdid="w4"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:DropDownList id="Cat1" runat="server" Width="155px" CssClass="inpText" OnSelectedIndexChanged="Cat1_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">Code</TD><TD><asp:TextBox id="backupcode" runat="server" Font-Size="X-Small" Text="" Width="150px" CssClass="inpTextDisabled" MaxLength="30" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">Cat 2 <asp:Label id="Label4" runat="server" Text="*" CssClass="Important" __designer:wfdid="w3"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:DropDownList id="Cat2" runat="server" Width="155px" CssClass="inpText" OnSelectedIndexChanged="Cat2_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">Merk</TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:DropDownList id="merk" runat="server" Width="155px" CssClass="inpText">
        </asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">Cat 3 <asp:Label id="Label3" runat="server" Text="*" CssClass="Important" __designer:wfdid="w2"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:DropDownList id="cat3" runat="server" Width="155px" CssClass="inpText" OnSelectedIndexChanged="cat3_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">Old Code</TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:TextBox id="oldcode" runat="server" Text="" Width="150px" CssClass="inpText" MaxLength="20"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">Model <asp:Label id="Label2" runat="server" Text="*" CssClass="Important" __designer:wfdid="w1"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:DropDownList id="DDLModel" runat="server" Width="155px" CssClass="inpText" OnSelectedIndexChanged="DDLModel_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">Supplier</TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:DropDownList id="suppoid" runat="server" Width="155px" CssClass="inpText" __designer:wfdid="w2"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">Jenis Bahan</TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:DropDownList id="ddlJenisBahan" runat="server" Width="155px" CssClass="inpText" __designer:wfdid="w1"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"></TD></TR><TR><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">Description<asp:Label id="Label9" runat="server" Text="*" CssClass="Important"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:TextBox id="matlongdesc" runat="server" Text="" Width="320px" CssClass="inpText" MaxLength="10000" AutoPostBack="True" OnTextChanged="matlongdesc_TextChanged" TextMode="MultiLine" Height="75px"></asp:TextBox><BR /><asp:Label id="Label1" runat="server" Text="* 1000 characters" CssClass="Important"></asp:Label><BR /><asp:Button id="btngendesc" runat="server" Text="Generate Description" CssClass="tombol"></asp:Button></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">Unit</TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:DropDownList id="matunitoid1" runat="server" Width="155px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">Price</TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:TextBox id="Price" runat="server" Text="0" Width="100px" CssClass="inpTextDisabled" MaxLength="35" Enabled="False" AutoPostBack="True" OnTextChanged="Price_TextChanged"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">Price Percentage</TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:TextBox id="pricepercentage" runat="server" Font-Size="X-Small" Text="" Width="56px" CssClass="inpTextDisabled" MaxLength="30" Enabled="False"></asp:TextBox>&nbsp;%</TD></TR><TR><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">Note</TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:TextBox id="note1" runat="server" Font-Size="8pt" Text="" Width="150px" CssClass="inpText" MaxLength="99" TextMode="MultiLine" Height="50px"></asp:TextBox><BR /><asp:Label id="Label14" runat="server" Text="* 100 characters" CssClass="Important"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">Status</TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:DropDownList id="ddlStatus" runat="server" Width="155px" CssClass="inpText"><asp:ListItem Value="ACTIVE">ACTIVE</asp:ListItem>
<asp:ListItem Value="INACTIVE">INACTIVE</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:TextBox id="pictureItem1desc" runat="server" Width="150px" CssClass="inpText" MaxLength="100" __designer:wfdid="w19"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:FileUpload id="matpictureloc" runat="server" Font-Size="X-Small" Width="150px" CssClass="inpText" Font-Overline="False"></asp:FileUpload>&nbsp;<asp:ImageButton id="btnUpload" runat="server" ImageUrl="~/Images/upload.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:TextBox id="pictureItem2desc" runat="server" Width="150px" CssClass="inpText" MaxLength="100" __designer:wfdid="w20"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:FileUpload id="matpictureloc1" runat="server" Font-Size="X-Small" Width="150px" CssClass="inpText" Font-Overline="False"></asp:FileUpload>&nbsp;<asp:ImageButton id="btnUpload1" onclick="btnUpload1_Click" runat="server" ImageUrl="~/Images/upload.png" ImageAlign="AbsMiddle" __designer:wfdid="w1"></asp:ImageButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:Label id="lblPic1" runat="server"></asp:Label><BR /><asp:HyperLink id="Link_1" runat="server" __designer:wfdid="w2">[ Full View ]</asp:HyperLink></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:Image id="matpicture" runat="server" BorderColor="#999999" ImageUrl="~/Images/no_image.jpg" Width="150px" Height="77px" BorderWidth="1px" BorderStyle="Solid"></asp:Image> </TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:Label id="lblPic2" runat="server"></asp:Label><BR /><asp:HyperLink id="Link_2" runat="server" __designer:wfdid="w2">[ Full View ]</asp:HyperLink></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:Image id="matpicture1" runat="server" BorderColor="#999999" ImageUrl="~/Images/no_image.jpg" Width="150px" Height="77px" BorderWidth="1px" BorderStyle="Solid"></asp:Image></TD></TR><TR><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:TextBox id="pictureItem3desc" runat="server" Width="150px" CssClass="inpText" MaxLength="100" __designer:wfdid="w21"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:FileUpload id="matpictureloc3" runat="server" Font-Size="X-Small" Width="150px" CssClass="inpText" __designer:wfdid="w2"></asp:FileUpload>&nbsp;<asp:ImageButton id="btnUpload3" onclick="btnUpload3_Click" runat="server" ImageUrl="~/Images/upload.png" ImageAlign="AbsMiddle" __designer:wfdid="w7"></asp:ImageButton></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:TextBox id="pictureItem4desc" runat="server" Width="150px" CssClass="inpText" MaxLength="100" __designer:wfdid="w22"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:FileUpload id="matpictureloc4" runat="server" Font-Size="X-Small" Width="150px" CssClass="inpText" __designer:wfdid="w8" Font-Overline="False"></asp:FileUpload>&nbsp;<asp:ImageButton id="btnUpload4" onclick="btnUpload4_Click" runat="server" ImageUrl="~/Images/upload.png" ImageAlign="AbsMiddle" __designer:wfdid="w9"></asp:ImageButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:Label id="lblPic3" runat="server" __designer:wfdid="w4"></asp:Label><BR /><asp:HyperLink id="Link_3" runat="server" __designer:wfdid="w2">[ Full View ]</asp:HyperLink></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:Image id="matpicture3" runat="server" BorderColor="#999999" ImageUrl="~/Images/no_image.jpg" Width="150px" __designer:wfdid="w10" Height="77px" BorderWidth="1px" BorderStyle="Solid"></asp:Image></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:Label id="lblPic4" runat="server" __designer:wfdid="w5"></asp:Label><BR /><asp:HyperLink id="Link_4" runat="server" __designer:wfdid="w2">[ Full View ]</asp:HyperLink></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:Image id="matpicture4" runat="server" BorderColor="#999999" ImageUrl="~/Images/no_image.jpg" Width="150px" __designer:wfdid="w11" Height="77px" BorderWidth="1px" BorderStyle="Solid"></asp:Image></TD></TR><TR><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:TextBox id="pictureItem5desc" runat="server" Width="150px" CssClass="inpText" MaxLength="100" __designer:wfdid="w23"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:FileUpload id="matpictureloc5" runat="server" Font-Size="X-Small" Width="150px" CssClass="inpText" __designer:wfdid="w13" Font-Overline="False"></asp:FileUpload>&nbsp;<asp:ImageButton id="btnUpload5" onclick="btnUpload5_Click" runat="server" ImageUrl="~/Images/upload.png" ImageAlign="AbsMiddle" __designer:wfdid="w14"></asp:ImageButton></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"></TD></TR><TR><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:Label id="lblPic5" runat="server" __designer:wfdid="w4"></asp:Label><BR /><asp:HyperLink id="Link_5" runat="server" __designer:wfdid="w2">[ Full View ]</asp:HyperLink></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><asp:Image id="matpicture5" runat="server" BorderColor="#999999" ImageUrl="~/Images/no_image.jpg" Width="150px" __designer:wfdid="w15" Height="77px" BorderWidth="1px" BorderStyle="Solid"></asp:Image></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"></TD><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"></TD></TR><TR><TD></TD><TD></TD><TD></TD><TD></TD></TR><TR><TD id="TD1" colSpan=4 runat="server" Visible="false"><asp:Label id="lblpictureloc" runat="server" Visible="False"></asp:Label>&nbsp; <asp:Label id="lblMenuPic" runat="server" ForeColor="Red" Font-Size="8pt" Font-Names="Arial" Visible="False"></asp:Label> <asp:Label id="lblpictureloc1" runat="server" Visible="False"></asp:Label>&nbsp; <asp:Label id="lblMenuPic1" runat="server" ForeColor="Red" Font-Size="8pt" Font-Names="Arial" Visible="False" __designer:wfdid="w3"></asp:Label> <asp:TextBox id="matoid" runat="server" Text="" Width="25px" CssClass="inpText" Enabled="False" Visible="False" ReadOnly="True"></asp:TextBox> <asp:TextBox id="minprice" runat="server" Text="0" Width="100px" CssClass="inpTextDisabled" MaxLength="35" Enabled="False" Visible="False" AutoPostBack="True"></asp:TextBox> <asp:Label id="i_u" runat="server" Text="New" CssClass="Important" Visible="False"></asp:Label> <asp:Label id="lblpictureloc3" runat="server" Visible="False" __designer:wfdid="w16"></asp:Label> <asp:Label id="lblpictureloc4" runat="server" Visible="False" __designer:wfdid="w16"></asp:Label> <asp:Label id="lblpictureloc5" runat="server" Visible="False" __designer:wfdid="w16"></asp:Label> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender2" runat="server" TargetControlID="price" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender1" runat="server" TargetControlID="minprice" ValidChars="1234567890,.">
</ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD colSpan=4>Created on <asp:Label id="createtime" runat="server" ForeColor="#585858" Font-Bold="True" Text="[Createtime]"></asp:Label> by <asp:Label id="createuser" runat="server" ForeColor="#585858" Font-Bold="True" Text="[createuser]"></asp:Label></TD></TR><TR><TD colSpan=4>Last of Update <asp:Label id="Updtime" runat="server" ForeColor="#585858" Font-Bold="True" Text="[Updtime]"></asp:Label>&nbsp;by <asp:Label id="Upduser" runat="server" ForeColor="#585858" Font-Bold="True" Text="[upduser]"></asp:Label></TD></TR><TR><TD colSpan=4><asp:ImageButton id="btnSave" tabIndex=39 runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" CausesValidation="False"></asp:ImageButton> <asp:ImageButton id="BtnCancel" tabIndex=40 runat="server" ImageUrl="~/Images/btnCancel.bmp" ImageAlign="AbsMiddle" CausesValidation="False"></asp:ImageButton> <asp:ImageButton id="BtnDelete" tabIndex=41 runat="server" ImageUrl="~/Images/btnDelete.bmp" ImageAlign="AbsMiddle" Visible="False" CommandName="Delete"></asp:ImageButton> <asp:ImageButton id="btnNew" tabIndex=39 runat="server" ImageUrl="~/Images/btnNewj.png" ImageAlign="AbsMiddle" CausesValidation="False" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnUpdate" tabIndex=39 runat="server" ImageUrl="~/Images/btnupdate.jpg" ImageAlign="AbsMiddle" CausesValidation="False" Visible="False"></asp:ImageButton></TD></TR><TR><TD><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </TD><TD></TD><TD></TD><TD colSpan=1></TD></TR></TBODY></TABLE>
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="btnUpload"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnUpload1"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnUpload3"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnUpload4"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnUpload5"></asp:PostBackTrigger>
</Triggers>
</asp:UpdatePanel> &nbsp;
</ContentTemplate>
<HeaderTemplate>
    <asp:Image ID="Image221" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
    <span style="font-size: 9pt">
                                <strong> Form Item/Material :.</strong></span>&nbsp;
                        
</HeaderTemplate>
</ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel id="UpdatePanelValidasi" runat="server">
        <contenttemplate>
<asp:Panel id="PanelValidasi" runat="server" Visible="False" CssClass="modalMsgBox"><TABLE><TBODY><TR><TD style="COLOR: white; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: center" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" Text="header"></asp:Label></TD></TR><TR><TD style="HEIGHT: 3px" colSpan=2></TD></TR><TR><TD vAlign=top align=left><asp:Image id="Image1" runat="server" ImageUrl="~/Images/warn.png"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="Validasi" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 3px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2><asp:ImageButton id="btnErrOK" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel>&nbsp; <ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderValidasi" runat="server" TargetControlID="ButtonExtendervalidasi" DropShadow="True" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="Panelvalidasi" PopupDragHandleControlID="Validasi"></ajaxToolkit:ModalPopupExtender><asp:Button id="ButtonExtendervalidasi" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

