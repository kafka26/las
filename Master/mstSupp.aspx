<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="mstSupp.aspx.vb" Inherits="Master_Supplier" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Supplier" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Supplier :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 10%" class="Label" align=left><asp:Label id="lblFilter" runat="server" Text="Filter"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="SuppCode">Code</asp:ListItem>
<asp:ListItem Value="SuppName">Name</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="200px"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 10%" class="Label" align=left><asp:CheckBox id="cbType" runat="server" Text="Type" AutoPostBack="True"></asp:CheckBox></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="DDLMatType" runat="server" CssClass="inpText" Width="125px"><asp:ListItem Value="Supplier Barang">SUPPLIER</asp:ListItem>
<asp:ListItem Value="Supplier Maklon">MAKLOON</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbTag" runat="server" Text="Tag" Visible="False"></asp:CheckBox></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="ddlTag" runat="server" CssClass="inpText" Width="235px" Visible="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnExportExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvMst" runat="server" ForeColor="#333333" Width="100%" CellPadding="4" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" GridLines="None" AllowSorting="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="SuppOid" DataNavigateUrlFormatString="~/Master/mstSupp.aspx?oid={0}" DataTextField="SuppCode" HeaderText="Code" SortExpression="SuppCode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="SuppName" HeaderText="Name" SortExpression="SuppName">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SuppAddr" HeaderText="Address" SortExpression="SuppAddr">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="City" HeaderText="City" SortExpression="City">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="supptype" HeaderText="Type" SortExpression="supptype">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SuppPhone1" HeaderText="Phone 1" SortExpression="SuppPhone1">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SuppPhone2" HeaderText="Phone 2" SortExpression="SuppPhone2">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SuppFax1" HeaderText="Fax 1" SortExpression="SuppFax1">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SuppFax2" HeaderText="Fax 2" SortExpression="SuppFax2">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SuppEmail" HeaderText="Email" SortExpression="SuppEmail">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                              <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnExportExcel"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left><asp:Label id="I_U" runat="server" CssClass="Important" Text="New Data" Width="93px"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="SuppOid" runat="server" Visible="False"></asp:Label> <asp:TextBox id="CodeSupp" runat="server" CssClass="inpText" Width="30px" AutoPostBack="True" Visible="False" OnTextChanged="CodeSupp_TextChanged" MaxLength="3"></asp:TextBox></TD><TD style="WIDTH: 11%" class="Label" align=left></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label2" runat="server">Code</asp:Label> <asp:Label id="Label22" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="SuppCode" runat="server" CssClass="inpTextDisabled" Width="100px" MaxLength="30" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label24" runat="server" Width="86px">Supplier Type</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:RadioButton id="rbType1" runat="server" Text="External" Checked="True" GroupName="rb"></asp:RadioButton> <asp:RadioButton id="rbType2" runat="server" Text="Internal" GroupName="rb"></asp:RadioButton></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label53" runat="server" Width="63px">PO Type</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="supptype" runat="server" CssClass="inpText" Width="100px"><asp:ListItem>LOCAL</asp:ListItem>
<asp:ListItem>IMPORT</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label4" runat="server">Default TOP</asp:Label> <asp:Label id="Label43" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="SuppPaymentOid" runat="server" CssClass="inpText" Width="100px"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label12" runat="server">Name</asp:Label><asp:Label id="Label14" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="SuppPrefixOid" runat="server" CssClass="inpText" Width="60px"></asp:DropDownList> <asp:TextBox id="SuppName" runat="server" CssClass="inpText" Width="150px" AutoPostBack="True" OnTextChanged="SuppName_TextChanged" MaxLength="50"></asp:TextBox></TD><TD class="Label" align=left>Old Code</TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="OldCode" runat="server" CssClass="inpText" Width="150px" MaxLength="100"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label15" runat="server">Taxable</asp:Label></TD><TD class="Label" align=center><asp:Label id="Label31" runat="server">:</asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="SuppTaxable" runat="server" CssClass="inpText" Width="50px" AutoPostBack="True"><asp:ListItem Value="1">Yes</asp:ListItem>
<asp:ListItem Value="0">No</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="NPWP" runat="server">NPWP</asp:Label></TD><TD class="Label" align=center><asp:Label id="NPWP4" runat="server">:</asp:Label></TD><TD class="Label" align=left><asp:TextBox id="SuppNPWP" runat="server" CssClass="inpText" Width="150px" MaxLength="30"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label28" runat="server" __designer:wfdid="w1">Name (Faktur)</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="suppname2" runat="server" CssClass="inpText" Width="200px" MaxLength="200" __designer:wfdid="w2"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label3" runat="server">Address</asp:Label> <asp:Label id="Label39" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="SuppAddr" runat="server" CssClass="inpText" Width="200px" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label40" runat="server">City</asp:Label> <asp:Label id="Label42" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="SuppCityOid" runat="server" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label8" runat="server">Phone 1</asp:Label> <asp:Label id="Label46" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="SuppPhone1" runat="server" CssClass="inpText" Width="100px" MaxLength="20"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label19" runat="server">Fax 1</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="SuppFax1" runat="server" CssClass="inpText" Width="100px" MaxLength="20"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label10" runat="server">Phone 2</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="SuppPhone2" runat="server" CssClass="inpText" Width="100px" MaxLength="20"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label20" runat="server">Fax 2</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="SuppFax2" runat="server" CssClass="inpText" Width="100px" MaxLength="20"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label9" runat="server">Phone 3</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="SuppPhone3" runat="server" CssClass="inpText" Width="100px" MaxLength="20"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label5" runat="server">Status</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="ActiveFlag" runat="server" CssClass="inpText" Width="100px"><asp:ListItem>ACTIVE</asp:ListItem>
<asp:ListItem>INACTIVE</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label13" runat="server">Email</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="SuppEmail" runat="server" CssClass="inpText" Width="150px" MaxLength="100"></asp:TextBox> <asp:Label id="Label49" runat="server" CssClass="Important" Text="<-- Ex: mail@sample.com"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label11" runat="server">Website</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="SuppWebsite" runat="server" CssClass="inpText" Width="150px" MaxLength="100"></asp:TextBox> <asp:Label id="Label52" runat="server" CssClass="Important" Text="<-- Ex: www.sample.com"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label16" runat="server" Width="103px">Contact Person 1</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="SuppCP1Name" runat="server" CssClass="inpText" Width="150px" MaxLength="50"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label17" runat="server" Width="104px">Contact Person 2</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="SuppCP2Name" runat="server" CssClass="inpText" Width="150px" MaxLength="50"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label6" runat="server" Width="74px">CP1 Phone</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="SuppCP1Phone" runat="server" CssClass="inpText" Width="100px" MaxLength="20"></asp:TextBox></TD><TD class="Label" align=left>&nbsp;<asp:Label id="Label21" runat="server">CP2 Phone</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="SuppCP2Phone" runat="server" CssClass="inpText" Width="100px" MaxLength="20"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label7" runat="server" Width="71px">CP2 Email</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="SuppCP1Email" runat="server" CssClass="inpText" Width="150px" MaxLength="100"></asp:TextBox> <asp:Label id="Label50" runat="server" CssClass="Important" Text="<-- Ex: mail@sample.com"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label45" runat="server">CP2 Email</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="SuppCP2Email" runat="server" CssClass="inpText" Width="150px" MaxLength="100"></asp:TextBox> <asp:Label id="Label51" runat="server" CssClass="Important" Text="<-- Ex: mail@sample.com"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label18" runat="server">Note</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="SuppNote" runat="server" CssClass="inpText" Width="200px" MaxLength="100" Rows="1" TextMode="MultiLine"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label25" runat="server">Account Payable</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLAkun" runat="server" CssClass="inpText" Width="200px"></asp:DropDownList></TD></TR><TR><TD id="TD1" class="Label" align=left runat="server" Visible="true"><asp:Label id="Label27" runat="server">Group</asp:Label></TD><TD id="TD6" class="Label" align=center runat="server" Visible="true">:</TD><TD id="TD5" class="Label" align=left runat="server" Visible="true"><asp:DropDownList id="suppgroup" runat="server" CssClass="inpText" Width="125px">
    <asp:ListItem>Supplier Barang</asp:ListItem>
    <asp:ListItem>Supplier Maklon</asp:ListItem>
</asp:DropDownList></TD><TD id="TD4" class="Label" align=left runat="server" Visible="true"><asp:Label id="Label26" runat="server">Customer</asp:Label></TD><TD id="TD3" class="Label" align=center runat="server" Visible="true">:</TD><TD id="TD2" class="Label" align=left runat="server" Visible="true"><asp:DropDownList id="DDLcust" runat="server" CssClass="inpText" Width="200px"></asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 110px" id="TD8" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label23" runat="server" Visible="False">Tag</asp:Label> <asp:ImageButton id="btnshowcat1" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD><TD style="HEIGHT: 110px" id="TD9" class="Label" align=center runat="server" Visible="false"></TD><TD style="HEIGHT: 110px" id="TD7" class="Label" align=left colSpan=4 runat="server" Visible="false"><asp:Panel id="pnlTag" runat="server" CssClass="inpText" Width="100%" Visible="False" ScrollBars="Vertical" Height="100px"><asp:CheckBoxList id="cblsuppres3" runat="server" CssClass="inpText" Width="97%" RepeatColumns="3">
                </asp:CheckBoxList></asp:Panel> </TD></TR><TR><TD id="TD12" class="Label" align=left runat="server" Visible="false"></TD><TD id="TD10" class="Label" align=center runat="server" Visible="false"></TD><TD id="TD11" class="Label" align=left colSpan=4 runat="server" Visible="false"><asp:TextBox id="suppres3" runat="server" CssClass="inpText" Width="200px" Visible="False" MaxLength="1000" Rows="3" TextMode="MultiLine"></asp:TextBox></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="ftbSuppPhone1" runat="server" TargetControlID="SuppPhone1" ValidChars="1234567890-+*#()"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbSuppPhone2" runat="server" TargetControlID="SuppPhone2" ValidChars="1234567890-+*#()"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbSuppPhone3" runat="server" TargetControlID="SuppPhone3" ValidChars="1234567890-+*#()"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbSuppFax1" runat="server" TargetControlID="SuppFax1" ValidChars="1234567890-+*#()"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbSuppFax2" runat="server" TargetControlID="SuppFax2" ValidChars="1234567890-+*#()"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbSuppCP1Phone" runat="server" TargetControlID="SuppCP1Phone" ValidChars="1234567890-+*#()"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbSuppCP2Phone" runat="server" TargetControlID="SuppCP2Phone" ValidChars="1234567890-+*#()"></ajaxToolkit:FilteredTextBoxExtender>&nbsp; </TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 24px" align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Supplier :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upListCat1" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListCat1" runat="server" CssClass="modalBox" Width="800px" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListMat" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Category 1"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListCat1" runat="server" Width="100%" DefaultButton="btnFindListCat1"><asp:Label id="LabelFilterListMat" runat="server" Text="Filter : "></asp:Label>
    <asp:DropDownList id="FilterDDLListCat1" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="cat1code">Code</asp:ListItem>
<asp:ListItem Value="cat1shortdesc">Description</asp:ListItem>
        <asp:ListItem Value="cat1res1">Type</asp:ListItem>
</asp:DropDownList>
    <asp:TextBox id="FilterTextListCat1" runat="server" CssClass="inpText" Width="150px"></asp:TextBox>
    <asp:ImageButton id="btnFindListCat1" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton>
    <asp:ImageButton id="btnAllListCat1" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnSelectNone" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewChecked" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlGVListCat1" runat="server" Width="100%" Height="150px" ScrollBars="Vertical"><asp:GridView id="gvListCat1" runat="server" ForeColor="#333333" Width="98%" GridLines="None" AutoGenerateColumns="False" AllowPaging="True" PageSize="5" CellPadding="4">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbSelectCat" runat="server" Checked='<%# eval("CheckValue") %>' ToolTip='<%# eval("cat1oid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="cat1res1" HeaderText="Type">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cat1code" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cat1shortdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cat1note" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD colSpan=3><asp:LinkButton id="lbAddToListListCat1" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lbCloseListCat1" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListCat1" runat="server" PopupControlID="pnlListCat1" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListCat1" Drag="True" TargetControlID="btnHideListCat1"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListCat1" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" DropShadow="True" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True" TargetControlID="bePopUpMsg"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

