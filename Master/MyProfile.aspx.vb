Imports System.Data.SqlClient
Imports System.Data
Imports ClassFunction

Partial Class Master_MyProfile
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Master\MyProfile.aspx")
        End If
        Page.Title = CompnyName & " - My Profile"
        btnUpdateInitStock.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to UPDATE STOCK VALUE?');")
        If Not Page.IsPostBack Then
            userid.Text = Session("UserID")
            username.Text = GetStrData("SELECT ISNULL(profname, '') FROM QL_mstprof WHERE cmpcode='" & Session("CompnyCode") & "' AND profoid='" & Session("UserID") & "'")
            Dim sUser As String = Session("UserID").ToString.ToUpper.Trim
            If sUser.Contains("ADMIN") Then
                imbSave.Visible = False
            End If
            btnUpdateInitStock.Visible = (userid.Text = "aDmIn")
        End If
    End Sub

    Protected Sub imbBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbBack.Click
        Response.Redirect("~/other/menu.aspx?awal=true")
    End Sub

    Protected Sub imbSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSave.Click
        sSql = "SELECT COUNT(-1) FROM QL_mstprof WHERE cmpcode='" & Session("CompnyCode") & "' AND profoid='" & Session("UserID") & "' AND profpass='" & Tchar(oldPassword.Text) & "'"
        If GetStrData(sSql) < 1 Then
            lblWarning.Text = "Your old password is wrong !!" : Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        Dim objCmd As New SqlClient.SqlCommand
        lblWarning.Text = ""
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        objCmd.Connection = conn
        objCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_mstprof SET profname='" & Tchar(username.Text) & "', profpass='" & Tchar(Password1.Text) & "' WHERE cmpcode='" & Session("CompnyCode") & "' AND profoid='" & Session("UserID") & "'"
            objCmd.CommandText = sSql
            objCmd.ExecuteNonQuery()
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            lblWarning.Text = ex.ToString : Exit Sub
        End Try
        lblWarning.Text = "Your Profile has been updated !!"
    End Sub
#End Region

    Protected Sub btnUpdateInitStock_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateInitStock.Click
        lblWarning.Text = ""
        Dim dtInit As DataTable
        Try
            ' SELECT INIT STOCK VALUE DATA
            sSql = "SELECT * FROM QL_stockvalue_init_stock WHERE note<>'UPDATED'"
            dtInit = ckon.ambiltabel(sSql, "initstockvalue")
        Catch ex As Exception
            lblWarning.Text = ex.ToString : Exit Sub
        End Try

        If dtInit.Rows.Count <= 0 Then
            lblWarning.Text = "No data to be updated." : Exit Sub
        End If

        ' UPDATING PROCESS
        Dim stockvalueoid As Integer = GenerateID("QL_stockvalue", CompnyCode)
        Dim objTrans As SqlClient.SqlTransaction
        Dim objCmd As New SqlClient.SqlCommand

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        objCmd.Connection = conn
        objCmd.Transaction = objTrans

        Try
            For R1 As Integer = 0 To dtInit.Rows.Count - 1
                sSql = "UPDATE QL_stockvalue SET stockqty=stockqty+" & ToDouble(dtInit.Rows(R1).Item("stockqty").ToString) & "," & _
                    "stockvalueidr=((stockvalueidr*stockqty)+(" & ToDouble(dtInit.Rows(R1).Item("stockvalueidr").ToString) * ToDouble(dtInit.Rows(R1).Item("stockqty").ToString) & "))/(stockqty+" & ToDouble(dtInit.Rows(R1).Item("stockqty").ToString) & ")," & _
                    "stockvalueusd=((stockvalueusd*stockqty)+(" & ToDouble(dtInit.Rows(R1).Item("stockvalueusd").ToString) * ToDouble(dtInit.Rows(R1).Item("stockqty").ToString) & "))/(stockqty+" & ToDouble(dtInit.Rows(R1).Item("stockqty").ToString) & ")," & _
                    "lasttranstype='UPDATE INIT STOCK VALUE',lasttransdate=GETDATE(),upduser='" & Session("UserID") & "',updtime=CURRENT_TIMESTAMP " & _
                    "WHERE cmpcode='" & dtInit.Rows(R1).Item("cmpcode").ToString & "' AND refoid=" & dtInit.Rows(R1).Item("refoid").ToString & " " & _
                    "AND refname='" & dtInit.Rows(R1).Item("refname").ToString & "' AND periodacctg='" & dtInit.Rows(R1).Item("periodacctg").ToString & "'"
                objCmd.CommandText = sSql
                If objCmd.ExecuteNonQuery() = 0 Then
                    sSql = "INSERT INTO QL_stockvalue (cmpcode,stockvalueoid,periodacctg,refoid,refname," & _
                        "stockqty,stockvalueidr,stockvalueusd,lasttranstype,lasttransdate,note,upduser,updtime) VALUES " & _
                        "('" & dtInit.Rows(R1).Item("cmpcode").ToString & "'," & stockvalueoid & ",'" & dtInit.Rows(R1).Item("periodacctg").ToString & "'," & _
                        "" & dtInit.Rows(R1).Item("refoid").ToString & ",'" & dtInit.Rows(R1).Item("refname").ToString & "'," & _
                        "" & ToDouble(dtInit.Rows(R1).Item("stockqty").ToString) & "," & ToDouble(dtInit.Rows(R1).Item("stockvalueidr").ToString) & "," & _
                        "" & ToDouble(dtInit.Rows(R1).Item("stockvalueusd").ToString) & ",'UPDATE INIT STOCK VALUE',GETDATE(),'UPDATE INIT STOCK VALUE'," & _
                        "'" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    stockvalueoid += 1
                End If

                sSql = "UPDATE QL_stockvalue_init_stock SET note='UPDATED',upduser='" & Session("UserID") & "',updtime=CURRENT_TIMESTAMP " & _
                    "WHERE stockvalueoid=" & dtInit.Rows(R1).Item("stockvalueoid").ToString & ""
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
            Next

            sSql = "UPDATE QL_mstoid SET lastoid=" & stockvalueoid - 1 & " WHERE tablename='QL_stockvalue' AND cmpcode='" & CompnyCode & "'"
            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            lblWarning.Text = ex.ToString : Exit Sub
        End Try

        lblWarning.Text = "Stock Value has beed updated succesfully."
    End Sub
End Class
