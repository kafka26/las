﻿<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="mstProf.aspx.vb" Inherits="Master_Profile" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Profile" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Profile :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD style="WIDTH: 10%" class="Label" align=left>Filter</TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=4><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="profoid">User ID</asp:ListItem>
<asp:ListItem Value="profname">User Name</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="150px"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbBU" runat="server" Text="Bus. Unit" Width="74px" Visible="False"></asp:CheckBox></TD><TD class="Label" align=center><asp:Label id="septBU" runat="server" Text=":" Visible="False"></asp:Label></TD><TD class="Label" align=left colSpan=4><asp:DropDownList id="FilterDDLBU" runat="server" CssClass="inpText" Width="260px" Visible="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status" Checked="True"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=4><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Selected="True">ACTIVE</asp:ListItem>
<asp:ListItem>INACTIVE</asp:ListItem>
<asp:ListItem>SUSPENDED</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:GridView id="gvMst" runat="server" ForeColor="#333333" Width="100%" AllowSorting="True" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="profoid" DataNavigateUrlFormatString="~/Master/mstProf.aspx?oid={0}" DataTextField="profoid" HeaderText="User ID" SortExpression="profoid">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="profname" HeaderText="User Name" SortExpression="profname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="60%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="60%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="activeflag" HeaderText="Status" SortExpression="activeflag">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="Sienna" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="There is no data !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 13%" class="Label" align=left><asp:Label id="I_U" runat="server" ForeColor="Red" Text="New Data" __designer:wfdid="w126"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=right></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="profapplimit" runat="server" Text="1" __designer:wfdid="w127" Visible="False"></asp:Label> <asp:Label id="Label2" runat="server" Text="Business Unit" __designer:wfdid="w90" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="DDLCmpCode" runat="server" CssClass="inpText" __designer:wfdid="w129" Visible="False" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblNIP" runat="server" Text="NIK" __designer:wfdid="w130"></asp:Label></TD><TD class="Label" align=right>:</TD><TD class="Label" align=left><asp:TextBox id="nip" runat="server" CssClass="inpTextDisabled" Width="160px" __designer:wfdid="w131" Enabled="False"></asp:TextBox> <asp:ImageButton id="imbSearchPerson" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w132"></asp:ImageButton> <asp:ImageButton id="imbErasePerson" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w133"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="personoid" runat="server" __designer:wfdid="w134" Visible="False">0</asp:Label> <asp:Label id="divcode" runat="server" __designer:wfdid="w135" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblUserID" runat="server" Text="User ID" __designer:wfdid="w136"></asp:Label> <asp:Label id="Label4" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w137"></asp:Label></TD><TD class="Label" align=right>:</TD><TD class="Label" align=left><asp:TextBox id="profoid" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w138" MaxLength="10"></asp:TextBox></TD><TD class="Label" align=left><ajaxToolkit:FilteredTextBoxExtender id="ftbeUserID" runat="server" __designer:wfdid="w139" ValidChars="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 -_" TargetControlID="profoid">
    </ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblNmUser" runat="server" Text="User Name" __designer:wfdid="w140"></asp:Label> <asp:Label id="Label5" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w141"></asp:Label></TD><TD class="Label" align=right>:</TD><TD class="Label" align=left><asp:TextBox id="profname" runat="server" CssClass="inpText" Width="200px" __designer:wfdid="w142" MaxLength="50"></asp:TextBox></TD><TD class="Label" align=left><ajaxToolkit:FilteredTextBoxExtender id="ftbeUserName" runat="server" __designer:wfdid="w143" ValidChars="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 -_" TargetControlID="profname">
    </ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD class="Label" align=left><asp:Label id="lblPassword" runat="server" Text="Password" __designer:wfdid="w144"></asp:Label> <asp:Label id="Label6" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w145"></asp:Label></TD><TD class="Label" align=right>:</TD><TD class="Label" align=left><asp:TextBox id="profpass" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w146" MaxLength="20" TextMode="Password"></asp:TextBox><asp:Label id="lblPassStrength" runat="server" CssClass="Important" __designer:wfdid="w147" Visible="False"></asp:Label></TD><TD class="Label" align=left><ajaxToolkit:PasswordStrength id="PasswordStrength1" runat="server" __designer:wfdid="w148" TargetControlID="profpass" HelpHandlePosition="RightSide" HelpStatusLabelID="lblPassStrength" PreferredPasswordLength="8" TextStrengthDescriptions="Very Poor;Weak;Average;Strong;Excellent"></ajaxToolkit:PasswordStrength> <ajaxToolkit:FilteredTextBoxExtender id="ftbePassword" runat="server" __designer:wfdid="w149" ValidChars="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890" TargetControlID="profpass"></ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD class="Label" align=left><asp:Label id="lblStatus" runat="server" Text="Status" __designer:wfdid="w150"></asp:Label></TD><TD class="Label" align=right>:</TD><TD class="Label" align=left><asp:DropDownList id="activeflag" runat="server" CssClass="inpText" Width="105px" __designer:wfdid="w151">
            <asp:ListItem>ACTIVE</asp:ListItem>
<asp:ListItem>INACTIVE</asp:ListItem>
<asp:ListItem>SUSPENDED</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="lblProfPass" runat="server" __designer:wfdid="w152" Visible="False"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=center colSpan=4></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w153"></asp:Label>&nbsp;On <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w154"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update&nbsp;By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w155"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w156"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w157" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w158" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w159" AlternateText="Delete"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w160" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w161"></asp:Image><BR />Please Wait .....</SPAN> <BR />
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                            &nbsp;
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Profile :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upListPerson" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListPerson" runat="server" CssClass="modalBox" Width="600px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD align=center><asp:Label id="lblTitleListPerson" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Employee"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" align=center></TD></TR><TR><TD align=center><asp:Panel id="pnlFilterListPerson" runat="server" Width="100%" DefaultButton="imbFindListPerson"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 30%" class="Label" align=right><asp:Label id="lblListPersonFilter" runat="server" Text="Filter :"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="FilterDDLListPerson" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="p.nip">NIK</asp:ListItem>
<asp:ListItem Value="p.personname">Employee Name</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListPerson" runat="server" CssClass="inpText" Width="150px"></asp:TextBox></TD></TR><TR><TD class="Label" align=right><asp:CheckBox id="cbListPersonDiv" runat="server" Text="Division :" Visible="False"></asp:CheckBox></TD><TD class="Label" align=left><asp:DropDownList id="FilterDDLListPersonDiv" runat="server" CssClass="inpText" Width="175px" Visible="False" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=right><asp:CheckBox id="cbListPersonDept" runat="server" Text="Department :"></asp:CheckBox></TD><TD class="Label" align=left><asp:DropDownList id="FilterDDLListPersonDept" runat="server" CssClass="inpText" Width="175px"></asp:DropDownList></TD></TR><TR><TD class="Label" align=right></TD><TD class="Label" align=left><asp:ImageButton id="imbFindListPerson" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbAllListPerson" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel></TD></TR><TR><TD vAlign=top align=center><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset1"><DIV id="Div1"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="gvListPerson" runat="server" ForeColor="#333333" Width="98%" AutoGenerateColumns="False" CellPadding="4" GridLines="None" DataKeyNames="personoid,nip,personname" EmptyDataRowStyle-ForeColor="Red">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="nip" HeaderText="NIK">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="personname" HeaderText="Employee Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="deptname" HeaderText="Department">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                                                <asp:Label ID="Label19" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label>
                                                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD align=center><asp:LinkButton id="lkbCloseListPerson" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListPerson" runat="server" TargetControlID="btnHidListPerson" Drag="True" PopupDragHandleControlID="lblTitleListPerson" BackgroundCssClass="modalBackground" PopupControlID="pnlListPerson"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidListPerson" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

