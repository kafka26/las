Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class Master_RateMonthly
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Functions"
    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        If curroid.SelectedValue = "" Then
            sError &= "- Please select Currency field!"
        End If
        If createdate.Text = "" Then
            sError &= "<BR>- Please fill DATE field!"
        Else
            Dim sErr As String = ""
            If Not IsValidDate(createdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "<BR>- DATE field is invalid. " & sErr
            End If
        End If
        If rate2res1.Text = "" Then
            sError &= "<BR>- Please fill RATE TO IDR field!"
        Else
            If ToDouble(rate2res1.Text) <= 0 Then
                sError &= "<BR>- RATE TO IDR field must be more than 0!"
            End If
        End If
        If rate2res2.Text = "" Then
            sError &= "<BR>- Please fill RATE TO USD field!"
        Else
            If ToDouble(rate2res2.Text) <= 0 Then
                sError &= "<BR>- RATE TO USD field must be more than 0!"
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    'Private Function IsDataExists() As Boolean
    'sSql = "SELECT COUNT(*) FROM QL_mstrate2 WHERE cmpcode='" & CompnyCode & "' AND rate2month='" & ddlmonth.SelectedValue & "' AND rate2year='" & ddlyear.SelectedValue & "'"
    'If Not Session("oid") Is Nothing And Session("oid") <> "" Then
    '    sSql &= " AND rate2oid<>" & Session("oid")
    'End If
    'If ToDouble(GetStrData(sSql).ToString) > 0 Then
    '    showMessage("Rate Monthly in " & ddlmonth.SelectedItem.Text & " " & ddlyear.SelectedItem.Text & " is already exist!", 2)
    '    Return True
    'End If
    'Return False
    ' End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindMstData(ByVal sFilter As String)
        sSql = "SELECT 0 AS nmr, rate2oid, r.curroid, currcode, CONVERT(VARCHAR(10), rate2date, 101) AS rate2date, rate2month, '' AS month, rate2year, rate2res1 AS rate2idrvalue, rate2res2 AS rate2usdvalue, rate2note, r.activeflag FROM QL_mstrate2 r INNER JOIN QL_mstcurr c ON c.cmpcode=r.cmpcode AND c.curroid=r.curroid AND c.activeflag='ACTIVE' WHERE r.cmpcode='" & CompnyCode & "' AND currcode<>'IDR'"
        If cbPeriod.Checked Then
            If ddlmonth.SelectedValue <> "" And ddlyear.SelectedValue <> "" Then
                sSql &= " AND rate2month=" & ddlmonth2.SelectedValue & " AND rate2year=" & ddlyear2.SelectedValue
            Else
                cbPeriod.Checked = False
            End If
        End If
        If cbCurrency.Checked Then
            If FilterDDLCurrency.SelectedValue <> "" Then
                sSql &= " AND r.curroid=" & FilterDDLCurrency.SelectedValue
            Else
                cbCurrency.Checked = False
            End If
        End If
        If cbStatus.Checked Then
            If FilterDDLStatus.SelectedValue <> "ALL" Then
                sSql &= " AND r.activeflag='" & FilterDDLStatus.SelectedValue & "'"
            End If
        End If
        If checkPagePermission("~\Master\mstRateMonthly.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND r.createuser='" & Session("UserID") & "'"
        End If
        sSql &= " ORDER BY rate2oid DESC"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstrate2")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(C1)("nmr") = C1 + 1
            dt.Rows(C1)("month") = MonthName(dt.Rows(C1)("rate2month")).ToUpper()
        Next
        Session("TblMst") = dt
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub InitAllDDL()
        ' Fill DDL Currency
        sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND currcode<>'IDR'"
        FillDDL(curroid, sSql)
        FillDDL(FilterDDLCurrency, sSql)
        If curroid.Items.Count = 0 Then
            showMessage("Please input some currency data first before continue using this form!", 2)
        End If
        ' Fill DDL Month
        ddlmonth.Items.Clear()
        ddlmonth2.Items.Clear()
        For iMonth As Integer = 1 To 12
            Dim li As New ListItem(MonthName(iMonth).ToUpper, iMonth)
            ddlmonth.Items.Add(li)
            ddlmonth2.Items.Add(li)
        Next
        ddlmonth.SelectedValue = Now.Month
        ddlmonth2.SelectedValue = Now.Month
        ' Fill DDL Year
        ddlyear.Items.Clear()
        ddlyear2.Items.Clear()
        For iYear As Integer = Now.Year + 2 To Now.Year - 2 Step -1
            ddlyear.Items.Add(iYear)
            ddlyear2.Items.Add(iYear)
        Next
        ddlyear.SelectedValue = Now.Year
        ddlyear2.SelectedValue = Now.Year
    End Sub

    Private Sub FillTextBox()
        Try
            sSql = "SELECT rate2oid, curroid, rate2date, rate2month, rate2year, rate2idrvalue, rate2usdvalue, rate2note, activeflag, createuser, createtime, upduser, updtime, rate2res1, rate2res2 FROM QL_mstrate2 WHERE cmpcode='" & CompnyCode & "' AND rate2oid=" & Session("oid")
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            btnDelete.Visible = False : btnSave.Visible = False
            While xreader.Read
                rate2oid.Text = xreader("rate2oid").ToString
                curroid.SelectedValue = xreader("curroid").ToString
                createdate.Text = Format(xreader("rate2date"), "MM/dd/yyyy")
                ddlmonth.SelectedValue = xreader("rate2month").ToString
                ddlyear.SelectedValue = xreader("rate2year").ToString
                'rate2idrvalue.Text = ToMaskEdit(xreader("rate2idrvalue"), 4)
                'rate2usdvalue.Text = ToMaskEdit(xreader("rate2usdvalue"), 6)
                ratenote.Text = xreader("rate2note").ToString
                activeflag.SelectedValue = xreader("activeflag").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
                rate2res1.Text = ToMaskEdit(xreader("rate2res1"), GetRoundValue(xreader("rate2res1")))
                rate2res2.Text = ToMaskEdit(xreader("rate2res2"), GetRoundValue(xreader("rate2res2")))
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
        Finally
            curroid.CssClass = "inpTextDisabled" : curroid.Enabled = False
            createdate.Enabled = False : createdate.CssClass = "inpTextDisabled" : imbDate.Enabled = False
            ddlmonth.Enabled = False : ddlmonth.CssClass = "inpTextDisabled"
            ddlyear.Enabled = False : ddlyear.CssClass = "inpTextDisabled"
            rate2idrvalue.Enabled = False : rate2idrvalue.CssClass = "inpTextDisabled"
            rate2usdvalue.Enabled = False : rate2usdvalue.CssClass = "inpTextDisabled"
            btnAvg.Enabled = False
        End Try
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Master\mstRateMonthly.aspx")
        End If
        If checkPagePermission("~\Master\mstRateMonthly.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Rate Standard"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            I_U.Text = "New Data"
        Else
            I_U.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnSave.Attributes.Add("OnClick", "javascript:return confirm('Your data will not be able to be edited or deleted anymore. Are you sure to continue?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            InitAllDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox()
                TabContainer1.ActiveTabIndex = 1
            Else
                rate2oid.Text = GenerateID("QL_MSTRATE2", CompnyCode)
                createdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindMstData("")
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        ddlmonth2.SelectedValue = Now.Month
        ddlyear2.SelectedValue = Now.Year
        cbPeriod.Checked = False
        cbCurrency.Checked = False
        FilterDDLCurrency.SelectedIndex = -1
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        BindMstData("")
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
    End Sub

    Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvMst.DataSource = Session("TblMst")
            gvMst.DataBind()
        End If
    End Sub

    Protected Sub btnAvg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAvg.Click
        Dim iMonth As Integer = CInt(ddlmonth.SelectedValue) - 1
        Dim iYear As Integer = CInt(ddlyear.SelectedValue)
        If iMonth = 0 Then
            iMonth = 12
            iYear -= 1
        End If
        sSql = "SELECT ISNULL(AVG(rateidrvalue), 0) AS avgidr, ISNULL(AVG(rateusdvalue), 0) AS avgusd FROM QL_mstrate WHERE cmpcode='" & CompnyCode & "' AND MONTH(ratedate)=" & iMonth & " AND YEAR(ratedate)=" & iYear & " AND curroid=" & curroid.SelectedValue
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd = New SqlCommand(sSql, conn)
        xreader = xCmd.ExecuteReader
        While xreader.Read
            rate2res1.Text = ToMaskEdit(ToDouble(xreader("avgidr").ToString), GetRoundValue(xreader("avgidr").ToString))
            rate2res2.Text = ToMaskEdit(ToDouble(xreader("avgusd").ToString), GetRoundValue(xreader("avgusd").ToString))
        End While
        xreader.Close()
        conn.Close()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            'If IsDataExists() Then
            '    Exit Sub
            'End If
            Dim sCurrCode As String = curroid.SelectedItem.Text
            Dim iCurrOid As Integer = CInt(GetStrData("SELECT TOP 1 curroid FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND currcode='IDR'"))
            If Session("oid") = Nothing Or Session("oid") = "" Then
                rate2oid.Text = GenerateID("QL_MSTRATE2", CompnyCode)
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_mstrate2 (cmpcode, rate2oid, curroid, rate2date, rate2month, rate2year, rate2idrvalue, rate2usdvalue, rate2note, activeflag, createuser, createtime, upduser, updtime, rate2res1, rate2res2) VALUES('" & CompnyCode & "', " & rate2oid.Text & ", " & curroid.SelectedValue & ", '" & createdate.Text & "', " & ddlmonth.SelectedValue & ", " & ddlyear.SelectedValue & ", " & ToDouble(rate2res1.Text) & ", " & ToDouble(rate2res2.Text) & ", '" & Tchar(ratenote.Text) & "', '" & Tchar(activeflag.SelectedValue) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & ToDouble(rate2res1.Text) & "', '" & ToDouble(rate2res2.Text) & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    If sCurrCode = "USD" Then
                        rate2oid.Text = CInt(rate2oid.Text) + 1
                        sSql = "INSERT INTO QL_mstrate2 (cmpcode, rate2oid, curroid, rate2date, rate2month, rate2year, rate2idrvalue, rate2usdvalue, rate2note, activeflag, createuser, createtime, upduser, updtime, rate2res1, rate2res2) VALUES('" & CompnyCode & "', " & rate2oid.Text & ", " & iCurrOid & ", '" & createdate.Text & "', " & ddlmonth.SelectedValue & ", " & ddlyear.SelectedValue & ", 1, " & 1 / ToDouble(rate2res1.Text) & ", '" & Tchar(ratenote.Text) & "', '" & Tchar(activeflag.SelectedValue) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '1', '" & 1 / ToDouble(rate2res1.Text) & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                    sSql = "UPDATE QL_mstoid SET lastoid=" & rate2oid.Text & " WHERE tablename LIKE 'QL_MSTRATE2' AND cmpcode LIKE '%" & CompnyCode & "%'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_mstrate2 SET curroid=" & curroid.SelectedValue & ", rate2date='" & createdate.Text & "',rate2month=" & ddlmonth.SelectedValue & ",rate2year=" & ddlyear.SelectedValue & ", rate2idrvalue=" & ToDouble(rate2res1.Text) & ", rate2usdvalue=" & ToDouble(rate2res2.Text) & ", rate2note='" & Tchar(ratenote.Text) & "', activeflag='" & Tchar(activeflag.SelectedValue) & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, rate2res1='" & ToDouble(rate2res1.Text) & "', rate2res2='" & ToDouble(rate2res2.Text) & "' WHERE cmpcode='" & CompnyCode & "' AND rate2oid=" & rate2oid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                conn.Close()
            Catch ex As Exception
                objTrans.Rollback()
                conn.Close()
                showMessage(ex.Message, 1)
                Exit Sub
            End Try
            Response.Redirect("~\Master\mstRateMonthly.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Master\mstRateMonthly.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If rate2oid.Text = "" Then
            showMessage("Please select Rate Standard data first!", 1)
            Exit Sub
        End If
        sSql = "SELECT tblusage, colusage FROM QL_oidusage WHERE cmpcode='" & CompnyCode & "' AND tblname='QL_MSTRATE2'"
        Dim objTblUsage As DataTable = cKon.ambiltabel(sSql, "QL_oidusage")
        Dim sColomnName(objTblUsage.Rows.Count - 1) As String
        Dim sTable(objTblUsage.Rows.Count - 1) As String
        For c1 As Integer = 0 To objTblUsage.Rows.Count - 1
            sColomnName(c1) = objTblUsage.Rows(c1).Item("colusage").ToString
            sTable(c1) = objTblUsage.Rows(c1).Item("tblusage").ToString
        Next
        If CheckDataExists(rate2oid.Text, sColomnName, sTable) = True Then
            showMessage("This data can't be deleted because it is being used by another data!", 2)
            Exit Sub
        End If
        If DeleteData("QL_mstrate2", "rate2oid", rate2oid.Text, CompnyCode) = True Then
            Response.Redirect("~\Master\mstRateMonthly.aspx?awal=true")
        End If
    End Sub

    Protected Sub gvMst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvMst.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), GetRoundValue(e.Row.Cells(5).Text))
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), GetRoundValue(e.Row.Cells(6).Text))
        End If
    End Sub

    Protected Sub rate2res1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rate2res1.TextChanged, rate2res2.TextChanged
        sender.Text = ToMaskEdit(ToDouble(sender.Text), GetRoundValue(sender.Text))
        If sender.ID = "rate2res1" Then
            If ToDouble(sender.Text) > 0 Then
                If curroid.SelectedValue <> "" Then
                    If ddlmonth.SelectedValue <> "" And ddlyear.SelectedValue <> "" Then
                        Dim sCurrCode As String = curroid.SelectedItem.Text
                        If sCurrCode <> "USD" Then
                            sSql = "SELECT TOP 1 rate2res1 FROM QL_mstrate2 r INNER JOIN QL_mstcurr c ON c.cmpcode=r.cmpcode AND c.curroid=r.curroid WHERE r.cmpcode='" & CompnyCode & "' AND currcode='USD' AND r.activeflag='ACTIVE' AND rate2month=" & ddlmonth.SelectedValue & " AND rate2year=" & ddlyear.SelectedValue & " ORDER BY r.updtime"
                            Dim dVal As Double = ToDouble(GetStrData(sSql))
                            If dVal > 0 Then
                                rate2res2.Text = ToMaskEdit(ToDouble(sender.Text) / dVal, GetRoundValue((ToDouble(sender.Text) / dVal).ToString))
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub
#End Region

End Class
