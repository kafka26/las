Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_Category1
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        If cat1res1.SelectedValue.ToString.ToLower = "fg" Then
            If cat1code.Text.Trim = "" Then
                sError &= "- Please fill CODE field!"
            End If
        End If
        If cat1res1.SelectedValue.ToString.ToLower = "wip" Then
            If cat1code.Text.Length <> 2 Then
                'sError &= "- Type WIP Length of CODE must be 2 characters!"
            End If
        ElseIf cat1res1.SelectedValue.ToString.ToLower = "fg" Then
            If cat1code.Text.Length > 5 Then
                sError &= "- Length of CODE must be less than 5 characters!"
            End If
        End If
        If cat1shortdesc.Text.Trim = "" Then
            sError &= "<BR>- Please fill DESCRIPTION field!"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsCodeExists() As Boolean
        sSql = "SELECT COUNT(*) FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND cat1code='" & Tchar(cat1code.Text) & "' AND cat1res1='" & cat1res1.SelectedValue & "'"
        'If cat1res1.SelectedValue = "Raw" Then
        '    sSql &= " AND cat1res2='" & cat1res2.SelectedValue & "'"
        'End If
        If Not Session("oid") Is Nothing And Session("oid") <> "" Then
            sSql &= " AND cat1oid<>" & Session("oid")
        End If
        If ToDouble(GetStrData(sSql).ToString) > 0 Then
            showMessage("Code have been used by another data. Please fill another Code!", 2)
            Return True
        End If
        Return False
    End Function

    Private Function IsDescExists() As Boolean
        sSql = "SELECT COUNT(*) FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND cat1shortdesc='" & Tchar(cat1shortdesc.Text) & "' AND cat1res1='" & cat1res1.SelectedValue & "'"
        'If cat1res1.SelectedValue = "Raw" Then
        '    sSql &= " AND cat1res2='" & cat1res2.SelectedValue & "'"
        'End If
        If Not Session("oid") Is Nothing And Session("oid") <> "" Then
            sSql &= " AND cat1oid<>" & Session("oid")
        End If
        If ToDouble(GetStrData(sSql).ToString) > 0 Then
            showMessage("Description have been used by another data. Please fill another Description!", 2)
            Return True
        End If
        Return False
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindMstData(ByVal sFilter As String)
        sSql = "SELECT cat1oid, cat1code, cat1shortdesc, cat1longdesc, activeflag, cat1note, (cat1res1 + (CASE WHEN cat1res2 IS NULL THEN '' WHEN cat1res2='' THEN '' ELSE ' - ' + cat1res2 END)) AS cat1res1 FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(sFilter) & "%'"
        If cbType.Checked Then
            sSql &= " AND cat1res1='" & FilterDDLType.SelectedValue & "'"
            'If FilterDDLType.SelectedValue = "Raw" Then
            'sSql &= " AND cat1res2='" & FilterDDLWip.SelectedValue & "'"
            'End If
        End If
        If cbStatus.Checked Then
            If FilterDDLStatus.SelectedValue <> "ALL" Then
                sSql &= " AND activeflag='" & FilterDDLStatus.SelectedValue & "'"
            End If
        End If
        If checkPagePermission("~\Master\mstCat1.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        sSql &= " ORDER BY cat1code"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_mstCat")
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub FillTextBox()
        Try
            sSql = "SELECT cat1oid, cat1code, cat1shortdesc, cat1longdesc, activeflag, cat1note, createuser, createtime, upduser, updtime, cat1res1, cat1res2 FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND cat1oid=" & Session("oid")
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            btnDelete.Visible = False
            While xreader.Read
                cat1oid.Text = xreader("cat1oid").ToString
                cat1code.Text = xreader("cat1code").ToString
                cat1shortdesc.Text = xreader("cat1shortdesc").ToString
                cat1longdesc.Text = xreader("cat1longdesc").ToString
                activeflag.SelectedValue = xreader("activeflag").ToString
                cat1note.Text = xreader("cat1note").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
                cat1res1.SelectedValue = xreader("cat1res1").ToString
                If cat1res1.SelectedValue = "Raw" Then
                    cat1res2.SelectedValue = xreader("cat1res2").ToString
                End If
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
        Finally
            btnDelete.Visible = True
            cat1code.CssClass = "inpTextDisabled"
            cat1code.Enabled = False
            cat1res1.Enabled = False
            cat1res1.CssClass = "inpTextDisabled"
            cat1res2.Enabled = False
            cat1res2.CssClass = "inpTextDisabled"
        End Try
    End Sub

    Private Sub PrintReport()
        Try
            report.Load(Server.MapPath(folderReport & "rptCat1.rpt"))
            Dim sWhere As String = " WHERE m.cmpcode='" & CompnyCode & "' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
            If cbType.Checked Then
                sWhere &= " AND cat1res1='" & FilterDDLType.SelectedValue & "'"
                'If FilterDDLType.SelectedValue = "Raw" Then
                '    sWhere &= " AND cat1res2='" & FilterDDLWip.SelectedValue & "'"
                'End If
            End If
            If cbStatus.Checked Then
                If FilterDDLStatus.SelectedValue <> "ALL" Then
                    sWhere &= " AND m.activeflag='" & FilterDDLStatus.SelectedValue & "'"
                End If
            End If
            'If checkPagePermission("~\Master\mstCat1.aspx", Session("SpecialAccess")) = False Then
            '    sWhere &= " AND createuser='" & Session("UserID") & "'"
            'End If
            sWhere &= " ORDER BY cat1code"
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "Category1Report_" & Format(GetServerTime(), "yyyyMMddHHmmss"))
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Public Sub InitAllDDL()
        sSql = "select gencode,gendesc from QL_mstgen where gengroup='GROUPITEM' and activeflag='ACTIVE' order by gendesc"
        FillDDL(cat1res1, sSql)
        FillDDL(FilterDDLType, sSql)
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Master\mstCat1.aspx")
        End If
        If checkPagePermission("~\Master\mstCat1.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Category 1"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            I_U.Text = "New Data"
        Else
            I_U.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        If Not Page.IsPostBack Then
            InitAllDDL()
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox()
                TabContainer1.ActiveTabIndex = 1
            Else
                cat1oid.Text = GenerateID("QL_MSTCAT1", CompnyCode)
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub FilterDDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLType.SelectedIndexChanged
        'If FilterDDLType.SelectedValue = "Raw" Then
        '    lblsepa.Visible = True : FilterDDLWip.Visible = True
        'Else
        '    lblsepa.Visible = False : FilterDDLWip.Visible = False
        'End If
        'FilterDDLWip.SelectedIndex = -1
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbType.Checked = False
        FilterDDLType.SelectedIndex = -1
        FilterDDLType_SelectedIndexChanged(Nothing, Nothing)
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
    End Sub

    Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvMst.DataSource = Session("TblMst")
            gvMst.DataBind()
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            If IsCodeExists() Then
                Exit Sub
            End If
            If IsDescExists() Then
                Exit Sub
            End If
            If Session("oid") = Nothing Or Session("oid") = "" Then
                cat1oid.Text = GenerateID("QL_MSTCAT1", CompnyCode)
            End If
            Dim sRes2 As String = ""
            If cat1res1.SelectedValue = "Raw" Then
                sRes2 = cat1res2.SelectedValue
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_mstcat1 (cmpcode, cat1oid, cat1code, cat1shortdesc, cat1longdesc, activeflag, cat1note, cat1res1, cat1res2, createuser, createtime, upduser, updtime) VALUES ('" & CompnyCode & "', " & cat1oid.Text & ", '" & Tchar(cat1code.Text) & "', '" & Tchar(cat1shortdesc.Text) & "', '" & Tchar(cat1longdesc.Text) & "', '" & activeflag.SelectedValue & "', '" & Tchar(cat1note.Text) & "', '" & cat1res1.SelectedValue & "', '" & sRes2 & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & cat1oid.Text & " WHERE tablename LIKE 'QL_MSTCAT1' AND cmpcode LIKE '%" & CompnyCode & "%'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_mstcat1 SET cat1code='" & Tchar(cat1code.Text) & "', cat1shortdesc='" & Tchar(cat1shortdesc.Text) & "', cat1longdesc='" & Tchar(cat1longdesc.Text) & "', activeflag='" & activeflag.SelectedValue & "', cat1note='" & Tchar(cat1note.Text) & "', cat1res1='" & cat1res1.SelectedValue & "', cat1res2='" & sRes2 & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & CompnyCode & "' AND cat1oid=" & cat1oid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                conn.Close()
            Catch ex As Exception
                objTrans.Rollback()
                conn.Close()
                showMessage(ex.Message, 1)
                Exit Sub
            End Try
            Response.Redirect("~\Master\mstCat1.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Master\mstCat1.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If cat1oid.Text = "" Then
            showMessage("Please select Category 1 first!", 1)
            Exit Sub
        End If
        'cek table mstitem
        If CheckDataExists("SELECT itemoid FROM QL_mstitem WHERE itemCat1 =" & cat1oid.Text) = True Then
            showMessage("KATEGORI TIDAK DAPAT DIHAPUS", 2)
            Exit Sub
        End If
        'cek tabel mstcat2 dan mstcat3
        Dim sCol() As String = {"cat1oid", "cat1oid"}
        Dim sTbl() As String = {"QL_mstcat2", "QL_mstcat3"}
        If CheckDataExists(CInt(cat1oid.Text), sCol, sTbl) = True Then
            showMessage("This data can't be deleted because it is being used by another data!", 2)
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE QL_mstcat1 WHERE cat1oid='" & cat1oid.Text & "' and cmpcode='" & CompnyCode & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try

        If DeleteData("QL_mstcat1", "cat1oid", Tchar(cat1oid.Text), CompnyCode) = True Then
            Response.Redirect("~\Master\mstCat1.aspx?awal=true")
        End If

        'If cat1res1.SelectedValue = "Raw" Then
        '    sSql = "SELECT COUNT(*) FROM QL_mstmatraw WHERE cmpcode='" & CompnyCode & "' AND SUBSTRING(matrawcode, 1, 2)='" & Tchar(cat1code.Text) & "'"
        'ElseIf cat1res1.SelectedValue = "General" Then
        '    sSql = "SELECT COUNT(*) FROM QL_mstmatgen WHERE cmpcode='" & CompnyCode & "' AND SUBSTRING(matgencode, 1, 2)='" & Tchar(cat1code.Text) & "'"
        'ElseIf cat1res1.SelectedValue = "Spare Part" Then
        '    sSql = "SELECT COUNT(*) FROM QL_mstsparepart WHERE cmpcode='" & CompnyCode & "' AND SUBSTRING(sparepartcode, 1, 2)='" & Tchar(cat1code.Text) & "'"
        'Else
        '    sSql = "SELECT COUNT(*) FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND SUBSTRING(itemcode, 1, 2)='" & Tchar(cat1code.Text) & "'"
        'End If
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        PrintReport()
    End Sub

    Protected Sub cat1res1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If cat1res1.SelectedValue = "WIP" Then
            cat1code.Text = ""
            cat1code.MaxLength = 2
        Else
            cat1code.Text = ""
            cat1code.MaxLength = 1
        End If
    End Sub

#End Region

End Class
