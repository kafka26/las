Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_Role
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        If rolename.Text = "" Then
            sError &= "- Please fill ROLE NAME field!<BR>"
        End If
        If roledesc.Text = "" Then
            sError &= "- Please fill DESCRIPTION field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindMstData(ByVal sFilter As String)
        sSql = "SELECT roleoid, rolename, roledesc FROM QL_mstrole WHERE cmpcode='" & CompnyCode & "' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(sFilter) & "%'"
        If checkPagePermission("~\Master\mstRole.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        sSql &= " ORDER BY rolename"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_mstrole")
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub FillTextBox()
        Try
            ' Fill Data Master
            sSql = "SELECT roleoid, rolename, roledesc, createuser, createtime, upduser, updtime FROM QL_mstrole WHERE cmpcode='" & CompnyCode & "' AND roleoid=" & Session("oid")
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            btnDelete.Enabled = False
            While xreader.Read
                roleoid.Text = xreader("roleoid").ToString
                rolename.Text = xreader("rolename").ToString
                roledesc.Text = xreader("roledesc").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
            End While
            ' Fill Data Detail
            sSql = "SELECT formtype, formname, formaddress, createuser, upduser, updtime FROM QL_mstroledtl WHERE cmpcode='" & CompnyCode & "' AND roleoid=" & Session("oid") & " ORDER BY formtype, formname"
            Dim objTable As DataTable = cKon.ambiltabel(sSql, "QL_mstroledtl")
            Session("TabelDtlRole") = objTable
            gvDtl.DataSource = Session("TabelDtlRole")
            gvDtl.DataBind()
            imbRemove.Visible = (objTable.Rows.Count > 0)

            ' Fill Data User Role
            If Session("TblDtlUserRole") Is Nothing Then
                CreateTblDetailUser()
            End If
            sSql = "SELECT DISTINCT ur.cmpcode, ur.profoid, p.profname, ur.special, ur.createuser, ur.createtime, ur.upduser, ur.updtime FROM QL_mstuserrole ur INNER JOIN QL_mstprof p ON ur.cmpcode=p.cmpcode AND ur.profoid=p.profoid AND p.activeflag='ACTIVE' WHERE ur.roleoid=" & roleoid.Text & ""
            Dim objTableUser As DataTable = cKon.ambiltabel(sSql, "QL_mstuserrole")
            Session("TblDtlUserRole") = objTableUser
            gvUserRole.DataSource = Session("TblDtlUserRole")
            gvUserRole.DataBind()
        Catch ex As Exception
            showMessage(ex.Message, 1)
        Finally
            xreader.Close()
            conn.Close()
            btnDelete.Enabled = True
            TD1.Visible = True
            TD2.Visible = True
            TD3.Visible = True
        End Try
    End Sub

    Private Sub FillDDLFormName(ByVal sFilter As String)
        sSql = "SELECT gendesc As Value, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='FORMNAME' AND genother2='" & Tchar(sFilter) & "' and genother3='" & listformmodule.SelectedValue & "'"
        Dim sNone As String = ""
        If Session("TabelDtlRole") IsNot Nothing Then
            Dim dt As DataTable = Session("TabelDtlRole")
            For C1 As Integer = 0 To dt.Rows.Count - 1
                sNone &= "'" & dt.Rows(C1)("formname").ToString & "',"
            Next
        End If
        If sNone <> "" Then
            sSql &= " AND gendesc NOT IN (" & Left(sNone, sNone.Length - 1) & ")"
        End If
        sSql &= " ORDER BY gendesc"
        FillDDL(listformname, sSql)
    End Sub

    Private Sub FillDDLFormType()
        sSql = "SELECT DISTINCT genother2 As Value, genother2 FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='FORMNAME' ORDER BY genother2"
        FillDDL(listformtype, sSql)
    End Sub

    Private Sub FillDDLFormModule()
        sSql = "SELECT DISTINCT genother3 As Value, genother3 FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='FORMNAME' ORDER BY genother3"
        FillDDL(listformmodule, sSql)
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("TblDtlRole")
        dtlTable.Columns.Add("formtype", Type.GetType("System.String"))
        dtlTable.Columns.Add("formname", Type.GetType("System.String"))
        dtlTable.Columns.Add("formaddress", Type.GetType("System.String"))
        dtlTable.Columns.Add("createuser", Type.GetType("System.String"))
        dtlTable.Columns.Add("upduser", Type.GetType("System.String"))
        dtlTable.Columns.Add("updtime", Type.GetType("System.DateTime"))
        Session("TabelDtlRole") = dtlTable
    End Sub

    Private Sub CreateTblDetailUser()
        Dim dtlTable As DataTable = New DataTable("TblDtlUserRole")
        dtlTable.Columns.Add("cmpcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("profoid", Type.GetType("System.String"))
        dtlTable.Columns.Add("profname", Type.GetType("System.String"))
        dtlTable.Columns.Add("special", Type.GetType("System.String"))
        dtlTable.Columns.Add("createuser", Type.GetType("System.String"))
        dtlTable.Columns.Add("createtime", Type.GetType("System.DateTime"))
        dtlTable.Columns.Add("upduser", Type.GetType("System.String"))
        dtlTable.Columns.Add("updtime", Type.GetType("System.DateTime"))
        Session("TblDtlUserRole") = dtlTable
    End Sub

    Private Sub PrintReport(ByVal sOid As String)
        Try
            report.Load(Server.MapPath(folderReport & "rptRole.rpt"))
            Dim sWhere As String
            sWhere = " WHERE r.cmpcode='" & CompnyCode & "'"
            If sOid = "" Then
                sWhere &= " AND r." & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If checkPagePermission("~\Master\mstRole.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND r.createuser='" & Session("UserID") & "'"
                End If
            Else
                sWhere &= " AND r.roleoid=" & sOid
            End If
            sWhere &= " ORDER BY r.rolename"
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "RolePrintOut_" & Format(GetServerTime(), "yyyyMMddHHmmss"))
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
        Response.Redirect("~\Master\mstRole.aspx?awal=true")
    End Sub

    Private Sub RemoveList(ByVal checkCol As Integer)
        Dim objTable As DataTable
        Dim objRow() As DataRow
        objTable = Session("TabelDtlRole")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length() > 0 Then
            For i As Integer = objRow.Length() - 1 To 0 Step -1
                If cKon.getCheckBoxValue(i, checkCol, gvDtl) = True Then
                    objTable.Rows.Remove(objRow(i))
                End If
            Next
            Session("TabelDtlRole") = objTable
            gvDtl.DataSource = Session("TabelDtlRole")
            gvDtl.DataBind()
        End If
        imbRemove.Visible = (objTable.Rows.Count > 0)
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Master\mstRole.aspx")
        End If
        If checkPagePermission("~\Master\mstRole.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Role"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            I_U.Text = "New Data"
        Else
            I_U.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox()
                TabContainer1.ActiveTabIndex = 1
            Else
                btnDelete.Visible = False
                btnPrint2.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
            FillDDLFormModule()
            listformmodule.SelectedIndex = -1
            FillDDLFormType()
            listformtype.SelectedIndex = -1
            FillDDLFormName(listformtype.SelectedValue)
            listformname.SelectedIndex = -1
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        FilterText.Text = ""
        FilterDDL.SelectedIndex = 0
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        PrintReport("")
    End Sub

    Protected Sub btnPrint2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint2.Click
        PrintReport(roleoid.Text)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            If Session("oid") = Nothing Or Session("oid") = "" Then
                roleoid.Text = GenerateID("QL_MSTROLE", CompnyCode)
            End If
            Dim iOid As Int64 = GenerateID("QL_MSTROLEDTL", CompnyCode)
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_mstrole (cmpcode, roleoid, rolename, roledesc, activeflag, createuser, upduser, updtime) VALUES('" & CompnyCode & "', " & roleoid.Text & ", '" & Tchar(rolename.Text.Trim) & "', '" & Tchar(roledesc.Text.Trim) & "', 'ACTIVE', '" & Session("UserID") & "', '" & Session("UserID") & "', current_timestamp)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & roleoid.Text & " where tablename='QL_MSTROLE' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_mstrole SET rolename='" & Tchar(rolename.Text.Trim) & "', roledesc='" & Tchar(roledesc.Text.Trim) & "', upduser='" & Session("UserID") & "', updtime=current_timestamp WHERE cmpcode='" & CompnyCode & "' AND roleoid=" & roleoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE QL_mstroledtl WHERE cmpcode='" & CompnyCode & "' AND roleoid=" & roleoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TabelDtlRole") Is Nothing Then
                    Dim objTable As DataTable = Session("TabelDtlRole")
                    For C1 As Integer = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_mstroledtl (cmpcode, roledtloid, roleoid, formtype, formname, formaddress, createuser, upduser, updtime) VALUES ('" & CompnyCode & "', " & iOid & ", " & roleoid.Text & ", '" & Tchar(objTable.Rows(C1)("formtype").ToString.ToUpper) & "', '" & Tchar(objTable.Rows(C1)("formname").ToString.ToUpper) & "', '" & Tchar(objTable.Rows(C1)("formaddress").ToString.ToUpper) & "', '" & Session("UserID") & "', '" & Session("UserID") & "', current_timestamp)"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iOid += 1
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iOid - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_MSTROLEDTL'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                Dim iOidUser As Int64 = GenerateID("QL_mstuserrole", CompnyCode)
                If Not Session("oid") Is Nothing And Session("oid") <> "" Then
                    sSql = "DELETE QL_mstuserrole WHERE roleoid='" & roleoid.Text & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtlUserRole") Is Nothing Then
                    Dim objTableUser As DataTable = Session("TblDtlUserRole")
                    For C1 As Integer = 0 To objTableUser.Rows.Count - 1
                        sSql = "INSERT INTO QL_mstuserrole (cmpcode, userroleoid, roleoid, profoid, special, createuser, createtime, upduser, updtime) VALUES ('" & objTableUser.Rows(C1)("cmpcode").ToString & "', " & iOidUser & ", " & roleoid.Text & ", '" & objTableUser.Rows(C1)("profoid").ToString & "', '" & Tchar(objTableUser.Rows(C1)("special").ToString) & "', '" & objTableUser.Rows(C1)("createuser").ToString & "', '" & objTableUser.Rows(C1)("createtime").ToString & "', '" & objTableUser.Rows(C1)("upduser").ToString & "', '" & objTableUser.Rows(C1)("updtime").ToString & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iOidUser += 1
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iOidUser - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_mstuserrole'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                conn.Close()
            Catch ex As Exception
                objTrans.Rollback()
                conn.Close()
                showMessage(ex.Message, 1)
                Exit Sub
            End Try
            Response.Redirect("~\Master\mstRole.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Master\mstRole.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If roleoid.Text = "" Then
            showMessage("Please select role data first!", 1)
            Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE QL_mstuserrole WHERE roleoid='" & roleoid.Text & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        If DeleteData("QL_mstrole", "roleoid", roleoid.Text, CompnyCode) = True Then
            DeleteData("QL_mstroledtl", "roleoid", roleoid.Text, CompnyCode)
            Response.Redirect("~\Master\mstRole.aspx?awal=true")
        End If
    End Sub

    Protected Sub imbTambah_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbTambah.Click
        lblListFormError.Text = ""
        listformname_SelectedIndexChanged(Nothing, Nothing)
        cProc.SetModalPopUpExtender(btnhiddenListForm, pnlListForm, mpeListForm, True)
    End Sub

    Protected Sub imbRemove_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbRemove.Click
        RemoveList(3)
    End Sub

    Protected Sub LBCancelRole_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LBCancelRole.Click
        lblListFormError.Text = ""
        cProc.SetModalPopUpExtender(btnhiddenListForm, pnlListForm, mpeListForm, False)
    End Sub

    Protected Sub LBAddRole_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LBAddRole.Click
        If listformurl.Text = "" Then
            lblListFormError.Text = "Please fill Form URL field"
            cProc.SetModalPopUpExtender(btnhiddenListForm, pnlListForm, mpeListForm, True)
            Exit Sub
        End If
        If Session("TabelDtlRole") Is Nothing Then
            CreateTblDetail()
        End If
        Dim objTbl As DataTable = Session("TabelDtlRole")
        Dim objRow As DataRow
        Dim objView As DataView = objTbl.DefaultView
        objView.RowFilter = "formname='" & listformname.SelectedValue & "'"
        If objView.Count > 0 Then
            lblListFormError.Text = "This data has been added before"
            objView.RowFilter = ""
            cProc.SetModalPopUpExtender(btnhiddenListForm, pnlListForm, mpeListForm, True)
            Exit Sub
        End If
        objView.RowFilter = ""
        objRow = objTbl.NewRow
        objRow("formtype") = listformtype.SelectedValue
        objRow("formname") = listformname.SelectedValue
        objRow("formaddress") = listformurl.Text
        objRow("createuser") = Session("UserID")
        objRow("upduser") = Session("UserID")
        objRow("updtime") = GetServerTime()
        objTbl.Rows.Add(objRow)
        Session("TabelDtlRole") = objTbl
        gvDtl.DataSource = Session("TabelDtlRole")
        gvDtl.DataBind()
        imbRemove.Visible = (objTbl.Rows.Count > 0)
        listformmodule.SelectedIndex = -1
        listformtype.SelectedIndex = -1
        listformtype_SelectedIndexChanged(Nothing, Nothing)
        lblListFormError.Text = ""
        cProc.SetModalPopUpExtender(btnhiddenListForm, pnlListForm, mpeListForm, True)
    End Sub

    Protected Sub listformname_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles listformname.SelectedIndexChanged
        lblListFormError.Text = ""
        sSql = "SELECT genother1 FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='FORMNAME' AND gendesc='" & listformname.SelectedValue & "' AND genother2='" & listformtype.SelectedValue & "' AND genother3='" & listformmodule.SelectedValue & "'"
        If GetStrData(sSql) <> "" Then
            listformurl.Text = GetStrData(sSql)
        Else
            listformurl.Text = ""
        End If
        cProc.SetModalPopUpExtender(btnhiddenListForm, pnlListForm, mpeListForm, True)
    End Sub

    Protected Sub listformtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles listformtype.SelectedIndexChanged, listformmodule.SelectedIndexChanged
        FillDDLFormName(listformtype.SelectedValue)
        listformname_SelectedIndexChanged(Nothing, Nothing)
        lblListFormError.Text = ""
        cProc.SetModalPopUpExtender(btnhiddenListForm, pnlListForm, mpeListForm, True)
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
    End Sub

    Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvMst.DataSource = Session("TblMst")
            gvMst.DataBind()
        End If
    End Sub

    Protected Sub gvUserRole_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvUserRole.RowDeleting
        Dim objTable As DataTable = Session("TblDtlUserRole")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        Session("TblDtlUserRole") = objTable
        gvUserRole.DataSource = objTable
        gvUserRole.DataBind()
    End Sub
#End Region

End Class
