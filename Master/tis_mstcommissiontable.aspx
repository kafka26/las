<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="tis_mstcommissiontable.aspx.vb" Inherits="tis_mstcommissiontable" title="Untitled Page" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">

 <table id="tableutama" align="center" border="1" cellpadding="3" cellspacing="0"
        width="100%" class="tabelhias">
        <tr style="font-size: 8pt; color: #000099">
            <th class="header" colspan="3" valign="middle">
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Maroon" Text=".: Insentive Table"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 15px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
<TABLE style="WIDTH: 752px"><TBODY><TR><TD style="WHITE-SPACE: nowrap" id="TD1" class="Label" vAlign=top align=left runat="server" visible="false">Filter</TD><TD style="WHITE-SPACE: nowrap" id="TD10" class="Label" vAlign=top align=left runat="server" visible="false">:</TD><TD style="WHITE-SPACE: nowrap" id="TD6" class="Label" vAlign=top align=left colSpan=3 runat="server" visible="false"><asp:DropDownList id="ddlfilter" runat="server" Font-Size="X-Small" CssClass="inpText" __designer:wfdid="w11">
    <asp:ListItem Value="commissioncode">Code</asp:ListItem>
                                        </asp:DropDownList> <asp:TextBox id="textfilter" runat="server" Font-Size="X-Small" Width="150px" MaxLength="30" CssClass="inpText" __designer:wfdid="w12"></asp:TextBox> </TD></TR><TR><TD style="WHITE-SPACE: nowrap" id="TD16" class="Label" vAlign=top align=left visible="false">Status</TD><TD style="WHITE-SPACE: nowrap" id="TD13" class="Label" vAlign=top align=left visible="false">:</TD><TD style="WHITE-SPACE: nowrap" id="TD21" class="Label" vAlign=top align=left colSpan=3 visible="false"><asp:DropDownList id="filterstatus" runat="server" Font-Size="X-Small" Width="100px" CssClass="inpText" __designer:wfdid="w13">
                <asp:ListItem>ACTIVE</asp:ListItem>
                <asp:ListItem>NOT ACTIVE</asp:ListItem>
                <asp:ListItem Value="%">ALL</asp:ListItem>
            </asp:DropDownList><asp:ImageButton style="MARGIN-LEFT: 5px" id="btnsearchlist" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w14"></asp:ImageButton><asp:ImageButton style="MARGIN-LEFT: 5px" id="btnalllist" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w15"></asp:ImageButton></TD></TR><TR><TD style="WHITE-SPACE: nowrap" class="Label" vAlign=top align=left></TD><TD style="WHITE-SPACE: nowrap" class="Label" vAlign=top align=left></TD><TD style="WHITE-SPACE: nowrap" class="Label" vAlign=top align=left></TD><TD style="WHITE-SPACE: nowrap" class="Label" vAlign=top align=left></TD><TD style="WHITE-SPACE: nowrap" class="Label" vAlign=top align=left></TD></TR><TR><TD style="WHITE-SPACE: nowrap" class="Label" vAlign=top align=left colSpan=5><asp:GridView id="GVmst" runat="server" BorderColor="#DEDFDE" Width="100%" __designer:wfdid="w16" AutoGenerateColumns="False" CellPadding="4" BackColor="White" AllowPaging="True" EmptyDataText="No data in database." BorderStyle="Solid" AllowSorting="True" BorderWidth="1px">
<FooterStyle BackColor="#F25407"></FooterStyle>
<Columns>
    <asp:HyperLinkField DataNavigateUrlFields="commissionoid" DataNavigateUrlFormatString="tis_mstcommissiontable.aspx?oid={0}"
        DataTextField="commissioncode" HeaderText="History Code">
        <HeaderStyle HorizontalAlign="Center" Width="100px" Wrap="False" />
        <ItemStyle HorizontalAlign="Center" Width="100px" Wrap="False" />
    </asp:HyperLinkField>
    <asp:BoundField DataField="validdate" HeaderText="Valid Date" DataFormatString="{0:dd/MM/yyyy}" />
    <asp:BoundField DataField="teamname" HeaderText="Sales" >
        <HeaderStyle Width="100px" />
        <ItemStyle Width="100px" />
    </asp:BoundField>
    <asp:BoundField DataField="commissionnote" HeaderText="Insentive Note" >
        <HeaderStyle Width="100px" />
        <ItemStyle Width="100px" />
    </asp:BoundField>
    <asp:BoundField DataField="commissionstatus" HeaderText="Insentive Status" >
        <HeaderStyle Width="100px" />
        <ItemStyle Width="100px" />
    </asp:BoundField>
</Columns>

<RowStyle BackColor="#F7F7DE"></RowStyle>
<EmptyDataTemplate>
                                        <asp:Label ID="lblmsg" runat="server" ForeColor="Red" Text="No Data Found !"></asp:Label>
                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#F25407" ForeColor="White" Font-Bold="True"></SelectedRowStyle>

<PagerStyle ForeColor="Black" HorizontalAlign="Right"></PagerStyle>

<HeaderStyle BackColor="#124E81" ForeColor="White" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="WHITE-SPACE: nowrap" class="Label" vAlign=top align=left colSpan=5><asp:ImageButton style="MARGIN-RIGHT: 5px" id="ibexcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w17"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 5px" id="ibpdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w18"></asp:ImageButton></TD></TR><TR><TD style="WHITE-SPACE: nowrap" class="Label" vAlign=top align=left></TD><TD style="WHITE-SPACE: nowrap" class="Label" vAlign=top align=left></TD><TD style="WHITE-SPACE: nowrap" class="Label" vAlign=top align=left></TD><TD style="WHITE-SPACE: nowrap" class="Label" vAlign=top align=left></TD><TD style="WHITE-SPACE: nowrap" class="Label" vAlign=top align=left></TD></TR></TBODY></TABLE>
</ContentTemplate>
                            </asp:UpdatePanel>
                            &nbsp;
                        </ContentTemplate>
                        <HeaderTemplate><img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <span style="font-size: 9pt"><strong>
                            :: List of Insentive Table</strong></span>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate><img align="absMiddle" alt="" src="../Images/Form.gif" id="Img2" />
                            <span style="font-size: 9pt"><strong>:: Form of Insentive</strong><strong> Table</strong></span>&nbsp;
                        </HeaderTemplate>
                        <ContentTemplate>
<asp:UpdatePanel id="UpdatePanel1" runat="server"><ContentTemplate>
<TABLE style="TEXT-ALIGN: left" width="100%"><TBODY><TR><TD style="WIDTH: 106px; WHITE-SPACE: nowrap" class="Label" align=left>Insentive Table History No.</TD><TD style="WHITE-SPACE: nowrap" class="Label" align=left>:</TD><TD style="WHITE-SPACE: nowrap" class="Label" align=left colSpan=5><asp:TextBox id="commissioncode" runat="server" Width="81px" __designer:wfdid="w70" CssClass="inpTextDisabled" MaxLength="30" ReadOnly="True" size="20"></asp:TextBox> <asp:Label id="oid" runat="server" __designer:wfdid="w71" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 106px; WHITE-SPACE: nowrap" class="Label" align=left>Sales Name</TD><TD style="WHITE-SPACE: nowrap" class="Label" align=left>:</TD><TD style="WHITE-SPACE: nowrap" class="Label" align=left colSpan=5><asp:DropDownList id="salesteam" runat="server" Width="200px" __designer:wfdid="w77" CssClass="inpText">
            </asp:DropDownList> <asp:Label id="tmpsales" runat="server" __designer:wfdid="w78" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 106px; WHITE-SPACE: nowrap" class="Label" align=left>Note</TD><TD style="WHITE-SPACE: nowrap" class="Label" align=left>:</TD><TD style="WHITE-SPACE: nowrap" class="Label" align=left colSpan=5><asp:TextBox id="commissionnote" runat="server" Width="400px" __designer:wfdid="w79" CssClass="inpText" MaxLength="200" size="20"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 106px; WHITE-SPACE: nowrap" class="Label" align=left>Status</TD><TD style="WHITE-SPACE: nowrap" class="Label" align=left>:</TD><TD style="WHITE-SPACE: nowrap" class="Label" align=left colSpan=5><asp:DropDownList id="commissionstatus" runat="server" Width="85px" __designer:wfdid="w80" CssClass="inpText"><asp:ListItem>ACTIVE</asp:ListItem>
<asp:ListItem>NOT ACTIVE</asp:ListItem>
</asp:DropDownList> <asp:Label id="tmpstatus" runat="server" __designer:wfdid="w81" Visible="False"></asp:Label> <asp:Label id="savestatus" runat="server" __designer:wfdid="w82" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 106px; WHITE-SPACE: nowrap" id="TD36" class="Label" align=left runat="server" visible="false">Commission Sales Type</TD><TD style="WHITE-SPACE: nowrap" id="TD32" class="Label" align=left runat="server" visible="false">:</TD><TD style="WHITE-SPACE: nowrap" id="TD35" class="Label" align=left colSpan=5 runat="server" visible="false"><asp:DropDownList id="commissionflag" runat="server" Width="85px" __designer:wfdid="w83" CssClass="inpText"><asp:ListItem>S1</asp:ListItem>
<asp:ListItem>S2</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="WIDTH: 106px; WHITE-SPACE: nowrap" class="Label" align=left></TD><TD style="WHITE-SPACE: nowrap" class="Label" align=left></TD><TD style="WHITE-SPACE: nowrap" class="Label" align=left></TD><TD style="WIDTH: 10px" class="Label" align=left></TD><TD style="WHITE-SPACE: nowrap" class="Label" align=left></TD><TD style="WHITE-SPACE: nowrap" class="Label" align=left></TD><TD style="WHITE-SPACE: nowrap" class="Label" align=left></TD></TR><TR><TD style="WHITE-SPACE: nowrap" id="TD8" class="Label" align=left colSpan=7 runat="server" visible="true"><asp:Label id="Label6" runat="server" Font-Size="13px" Font-Bold="True" Text="Insentive Detail :" __designer:wfdid="w84" Font-Underline="True"></asp:Label></TD></TR><TR><TD style="WIDTH: 106px; WHITE-SPACE: nowrap" id="Td34" class="Label" align=left runat="server" visible="true">Commision&nbsp;Rate <asp:Label id="Label5" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w92" CssClass="Important"></asp:Label></TD><TD style="WHITE-SPACE: nowrap" id="Td41" class="Label" align=left runat="server" visible="true">:</TD><TD style="WHITE-SPACE: nowrap" id="Td42" class="Label" align=left colSpan=5 runat="server" visible="true"><asp:TextBox id="insentiverate" runat="server" Width="50px" __designer:wfdid="w93" CssClass="inpText" MaxLength="6" size="20"></asp:TextBox> %</TD></TR><TR><TD style="WIDTH: 106px; WHITE-SPACE: nowrap" class="Label" align=left runat="server" visible="true">Kode Komisi</TD><TD style="WHITE-SPACE: nowrap" class="Label" align=left runat="server" visible="true">:</TD><TD style="WHITE-SPACE: nowrap" class="Label" align=left colSpan=5 runat="server" visible="true"><asp:DropDownList id="kode_komisi" runat="server" Width="260px" __designer:wfdid="w5" CssClass="inpText"><asp:ListItem Value="JPU">Jual Putus (JPU)</asp:ListItem>
<asp:ListItem Value="KTS">Konsinasi Tanpa SPG (KTS)</asp:ListItem>
<asp:ListItem Value="KSG">Konsinasi SPG
 (KSG)</asp:ListItem>
<asp:ListItem Value="LP">Luar Pulau (LP)</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="WIDTH: 106px; WHITE-SPACE: nowrap" class="Label" align=left runat="server" visible="true">Payment Time Range <asp:Label id="Label9" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w94" CssClass="Important"></asp:Label></TD><TD style="WHITE-SPACE: nowrap" class="Label" align=left runat="server" visible="true">:</TD><TD style="WHITE-SPACE: nowrap" class="Label" align=left colSpan=5 runat="server" visible="true"><asp:TextBox id="PayRangeOne" runat="server" Width="50px" __designer:wfdid="w95" CssClass="inpText" MaxLength="6" size="20"></asp:TextBox>&nbsp;-&nbsp;<asp:TextBox id="PayRangeTwo" runat="server" Width="50px" __designer:wfdid="w96" CssClass="inpText" MaxLength="6" size="20"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 106px; WHITE-SPACE: nowrap" id="Td56" class="Label" align=left runat="server" visible="false"></TD><TD style="WHITE-SPACE: nowrap" id="Td57" class="Label" align=left runat="server" visible="false"></TD><TD style="WHITE-SPACE: nowrap" id="Td58" class="Label" align=left colSpan=5 runat="server" visible="false"><asp:DropDownList id="paymentrate" runat="server" Width="150px" __designer:wfdid="w100" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:DropDownList> <asp:DropDownList id="paytermrange1" runat="server" Width="100px" __designer:wfdid="w101" CssClass="inpText" Visible="False"></asp:DropDownList> <asp:DropDownList id="paytermrange2" runat="server" Width="100px" __designer:wfdid="w102" CssClass="inpText" Visible="False"></asp:DropDownList> <asp:DropDownList id="region" runat="server" Width="150px" __designer:wfdid="w103" CssClass="inpText" AutoPostBack="True"><asp:ListItem Value="1">JAWA</asp:ListItem>
<asp:ListItem Value="2">LUAR JAWA</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="WHITE-SPACE: nowrap" id="TD26" class="Label" align=left colSpan=7 runat="server" visible="false"><asp:TextBox id="amountrange1" runat="server" Width="150px" __designer:wfdid="w86" CssClass="inpText" MaxLength="18" size="20" AutoPostBack="True">0</asp:TextBox> <asp:TextBox id="amountrange2" runat="server" Width="150px" __designer:wfdid="w87" CssClass="inpText" MaxLength="18" size="20" AutoPostBack="True">0</asp:TextBox> <asp:TextBox id="commissionrate" runat="server" Width="50px" __designer:wfdid="w98" CssClass="inpText" MaxLength="6" size="20">0</asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftberange1" runat="server" __designer:wfdid="w88" ValidChars="0123456789.," TargetControlID="amountrange1">
        </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FtePayRangeOne" runat="server" __designer:wfdid="w90" ValidChars="0123456789" TargetControlID="PayRangeOne"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftberange2" runat="server" __designer:wfdid="w89" ValidChars="0123456789.," TargetControlID="amountrange2">
        </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FtePayRangeTwo" runat="server" __designer:wfdid="w91" ValidChars="0123456789" TargetControlID="PayRangeTwo"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftberatevalue" runat="server" __designer:wfdid="w99" ValidChars="0123456789.," TargetControlID="commissionrate"></ajaxToolkit:FilteredTextBoxExtender> <asp:TextBox id="validdate" runat="server" Width="81px" __designer:wfdid="w73" CssClass="inpText" MaxLength="30" size="20"></asp:TextBox> <asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnvaliddate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w74"></asp:ImageButton> <asp:Label id="Label8" runat="server" ForeColor="Red" __designer:wfdid="w75">(MM/dd/yyyy)</asp:Label> <ajaxToolkit:CalendarExtender id="cevalid" runat="server" __designer:wfdid="w76" TargetControlID="validdate" PopupButtonID="btnvaliddate" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD style="WHITE-SPACE: nowrap" id="Td29" class="Label" align=left colSpan=7 runat="server" visible="true"><asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnaddvalue" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w104"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnclearadd" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w105"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 106px; WHITE-SPACE: nowrap" id="TD5" class="Label" align=left runat="server" visible="true"><asp:Label id="editaddseq" runat="server" __designer:wfdid="w106" Visible="False"></asp:Label></TD><TD style="WHITE-SPACE: nowrap" id="TD25" class="Label" align=left runat="server" visible="true"></TD><TD style="WHITE-SPACE: nowrap" id="TD20" class="Label" align=left runat="server" visible="true"></TD><TD style="WIDTH: 10px" id="TD15" class="Label" align=left runat="server" visible="true"></TD><TD style="WHITE-SPACE: nowrap" id="TD12" class="Label" align=left runat="server" visible="true"></TD><TD style="WHITE-SPACE: nowrap" id="TD9" class="Label" align=left runat="server" visible="true"></TD><TD style="WHITE-SPACE: nowrap" id="TD4" class="Label" align=left runat="server" visible="true"></TD></TR><TR><TD style="WHITE-SPACE: nowrap" id="TD24" class="Label" align=left colSpan=7 runat="server" visible="true"><asp:GridView style="BACKGROUND-COLOR: transparent" id="GVadddetail" runat="server" BorderColor="#DEDFDE" ForeColor="Black" Width="100%" __designer:wfdid="w57" BackColor="White" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="seq" OnRowDataBound="GVadddetail_RowDataBound">
<RowStyle BackColor="#F7F7DE"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Omzet Range" Visible="False"><ItemTemplate>
                            <asp:Label ID="amountrange" runat="server" Text='<%# String.Format("{0:#,##0.00###} - {1:#,##0.00###}", Eval("rangeone"), Eval("rangetwo")) %>'></asp:Label>
                        
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="kode_komisi" HeaderText="Kode Komisi">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="insentiverate" DataFormatString="{0:#,##0.00} %" HeaderText="Insentive Rate">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="paymentrate" DataFormatString="{0:#,##0}" HeaderText="Payment Type" Visible="False">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="region" HeaderText="Region" Visible="False">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Payment Time Range"><ItemTemplate>
                            <asp:Label ID="amountrange" runat="server" Text='<%# String.Format("{0:#,##0} - {1:#,##0}", Eval("payrangeone"), Eval("payrangetwo")) %>'></asp:Label>
                        
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="commissionrate" DataFormatString="{0:#,##0.00} %" HeaderText="Additional Commision" Visible="False">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" EditText="edit" ShowDeleteButton="True">
<ControlStyle ForeColor="Red" Width="10px"></ControlStyle>

<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" Width="30px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Width="30px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#CCCC99"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#F7F7DE" ForeColor="Black"></PagerStyle>
<EmptyDataTemplate>
                    <asp:Label ID="lblNodataShipping" runat="server" CssClass="Important" Text="No data found  !!"></asp:Label>
                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White"></SelectedRowStyle>

<HeaderStyle BackColor="#6B696B" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="WIDTH: 106px; WHITE-SPACE: nowrap" id="TD19" class="Label" align=left runat="server" visible="true"></TD><TD style="WHITE-SPACE: nowrap" id="TD14" class="Label" align=left runat="server" visible="true"></TD><TD style="WHITE-SPACE: nowrap" id="TD11" class="Label" align=left runat="server" visible="true"></TD><TD style="WIDTH: 10px" id="TD7" class="Label" align=left runat="server" visible="true"></TD><TD style="WHITE-SPACE: nowrap" id="TD2" class="Label" align=left runat="server" visible="true"></TD><TD style="WHITE-SPACE: nowrap" id="TD22" class="Label" align=left runat="server" visible="true"></TD><TD style="WHITE-SPACE: nowrap" id="TD17" class="Label" align=left runat="server" visible="true"></TD></TR><TR><TD style="WHITE-SPACE: nowrap" class="Label" align=left colSpan=7>Last Update by&nbsp;<asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w108"></asp:Label>&nbsp;on&nbsp;&nbsp;<asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w109"></asp:Label></TD></TR><TR><TD style="WHITE-SPACE: nowrap" class="Label" align=left colSpan=7></TD></TR><TR><TD style="WHITE-SPACE: nowrap" class="Label" align=left colSpan=7><asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnsave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w110" AlternateText="Save"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnsaveas" runat="server" ImageUrl="~/Images/saveas.png" ImageAlign="AbsMiddle" __designer:wfdid="w111" Visible="False" AlternateText="Save"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 5px" id="btncancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w112" AlternateText="Cancel"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 5px" id="btndelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w113" Visible="False" AlternateText="Delete"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 5px" id="btntoexcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w114" Visible="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 5px" id="btntopdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w115" Visible="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnupdatestatus" runat="server" ImageUrl="~/Images/updatestatus.png" ImageAlign="AbsMiddle" __designer:wfdid="w116" Visible="False" AlternateText="Save"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 106px; WHITE-SPACE: nowrap" class="Label" align=left></TD><TD style="WHITE-SPACE: nowrap" class="Label" align=left></TD><TD style="WHITE-SPACE: nowrap" class="Label" align=left></TD><TD style="WIDTH: 10px" class="Label" align=left></TD><TD style="WHITE-SPACE: nowrap" class="Label" align=left></TD><TD style="WHITE-SPACE: nowrap" class="Label" align=left></TD><TD style="WHITE-SPACE: nowrap" class="Label" align=left></TD></TR></TBODY></TABLE><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w117"><ProgressTemplate>
<asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w118"></asp:Image> 
</ProgressTemplate>
</asp:UpdateProgress> 
</ContentTemplate>
</asp:UpdatePanel> &nbsp; &nbsp;&nbsp; 
</ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upPopUpMsg" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False">
                <table>
                    <tbody>
                        <tr>
                            <td colspan="2" style="background-color: #cc0000; text-align: left">
                                <asp:Label ID="lblCaption" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="White"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="height: 10px">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Image ID="imIcon" runat="server" Height="24px" ImageUrl="~/Images/error.jpg"
                                    Width="24px" /></td>
                            <td class="Label" style="text-align: left">
                                <asp:Label ID="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="height: 10px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center">
                                &nbsp;<asp:ImageButton ID="imbOKPopUpMsg" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/ok.png" /></td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpePopUpMsg" runat="server" BackgroundCssClass="modalBackground"
                Drag="True" DropShadow="True" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption"
                TargetControlID="bePopUpMsg">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="bePopUpMsg" runat="server" CausesValidation="False" Visible="False" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

