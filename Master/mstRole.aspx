﻿<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="mstRole.aspx.vb" Inherits="Master_Role" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Role" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Role :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" __designer:wfdid="w2" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD style="WIDTH: 8%" class="Label" align=left>Filter </TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=4><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w3"><asp:ListItem Value="rolename">Role Name</asp:ListItem>
<asp:ListItem Value="roledesc">Description</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="150px" __designer:wfdid="w4"></asp:TextBox></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w5"></asp:ImageButton> <asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w6"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w7"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:GridView id="gvMst" runat="server" ForeColor="#333333" Width="100%" __designer:wfdid="w8" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True" PageSize="8">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="roleoid" DataNavigateUrlFormatString="~/Master/mstRole.aspx?oid={0}" DataTextField="rolename" HeaderText="Role Name" SortExpression="rolename">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="25%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="25%"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="roledesc" HeaderText="Description" SortExpression="roledesc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="Sienna" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="There is no data !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w9"></asp:Label> </TD></TR></TBODY></TABLE></asp:Panel> &nbsp; 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 11%" class="Label" align=left><asp:Label id="I_U" runat="server" CssClass="Important" Text="New Data" __designer:wfdid="w42"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=right></TD><TD style="WIDTH: 50%" class="Label" align=left><asp:Label id="roleoid" runat="server" __designer:wfdid="w43" Visible="False"></asp:Label> <asp:Label id="cmpcode" runat="server" __designer:wfdid="w44" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblNmRole" runat="server" Text="Role Name" __designer:wfdid="w45"></asp:Label></TD><TD class="Label" align=right>:</TD><TD class="Label" align=left><asp:TextBox id="rolename" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w46" MaxLength="30"></asp:TextBox></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblDeskripsi" runat="server" Text="Description" __designer:wfdid="w47"></asp:Label></TD><TD class="Label" align=right>:</TD><TD class="Label" align=left><asp:TextBox id="roledesc" runat="server" CssClass="inpText" Width="150px" __designer:wfdid="w48" MaxLength="50"></asp:TextBox></TD><TD class="Label" align=left></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:Label id="Label2" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Role Detail :" __designer:wfdid="w49"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 11%" class="Label" align=left colSpan=1><asp:ImageButton id="imbTambah" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w50"></asp:ImageButton></TD><TD class="Label" align=left colSpan=3><asp:ImageButton id="imbRemove" runat="server" ImageUrl="~/Images/remove.png" ImageAlign="AbsMiddle" __designer:wfdid="w51" Visible="False"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:Panel id="pnlDtl" runat="server" CssClass="inpText" Width="100%" __designer:wfdid="w52" ScrollBars="Vertical" Height="200px"><asp:GridView id="gvDtl" runat="server" ForeColor="#333333" Width="98%" __designer:wfdid="w53" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
<RowStyle BackColor="#EFF3FB"></RowStyle>
<Columns>
<asp:BoundField DataField="formtype" HeaderText="Form Type">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="15%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="formname" HeaderText="Form Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="30%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="formaddress" HeaderText="Form URL">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbDelData" runat="server"></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="5%"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmptyDetail" runat="server" CssClass="Important" Text="No Role Detail !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=center colSpan=4></TD></TR><TR><TD style="HEIGHT: 10px" id="TD1" class="Label" align=left colSpan=4 runat="server" Visible="false"><asp:Label id="Label3" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="User Role :" __designer:wfdid="w54"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" id="TD2" class="Label" align=center colSpan=4 runat="server" Visible="false"></TD></TR><TR><TD style="HEIGHT: 10px" id="TD3" class="Label" align=left colSpan=4 runat="server" Visible="false"><asp:Panel id="PnlUserRole" runat="server" CssClass="inpText" Width="100%" __designer:wfdid="w55" ScrollBars="Vertical" Height="200px"><asp:GridView id="gvUserRole" runat="server" ForeColor="#333333" Width="98%" __designer:wfdid="w56" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
<RowStyle BackColor="#EFF3FB"></RowStyle>
<Columns>
<asp:BoundField DataField="profoid" HeaderText="User ID">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="30%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="profname" HeaderText="User Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmptyDetail" runat="server" CssClass="Important" Text="No User Role Detail !!" __designer:wfdid="w255"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=center colSpan=4></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w57"></asp:Label>&nbsp;On <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w58"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update&nbsp;By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w59"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w60"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w61" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w62" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w63" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton id="btnPrint2" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w64"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w65" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w66"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </DIV><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2><asp:UpdatePanel id="updpnlListForm" runat="server" __designer:wfdid="w67"><ContentTemplate>
<asp:Panel id="pnlListForm" runat="server" CssClass="modalBox" __designer:wfdid="w68" Visible="False"><TABLE style="WIDTH: 400px"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="lblListForm" runat="server" Font-Size="Medium" Font-Bold="True" Text="FORM LIST" __designer:wfdid="w69"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" align=left colSpan=2></TD></TR><TR><TD class="Label" align=left>Form Module</TD><TD align=left><asp:DropDownList id="listformmodule" runat="server" CssClass="inpText" Width="150px" __designer:wfdid="w70" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Form Type</TD><TD align=left><asp:DropDownList id="listformtype" runat="server" CssClass="inpText" Width="150px" __designer:wfdid="w71" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Form Name</TD><TD align=left><asp:DropDownList id="listformname" runat="server" CssClass="inpText" Width="200px" __designer:wfdid="w72" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Form URL</TD><TD align=left><asp:TextBox id="listformurl" runat="server" CssClass="inpTextDisabled" Width="250px" __designer:wfdid="w73" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD class="Label" align=left></TD><TD style="HEIGHT: 5px" align=left><asp:Label id="lblListFormError" runat="server" CssClass="Important" __designer:wfdid="w74"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" class="Label" align=left colSpan=2><asp:LinkButton id="LBAddRole" runat="server" CausesValidation="False" __designer:wfdid="w75">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="LBCancelRole" runat="server" CausesValidation="False" __designer:wfdid="w76">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnhiddenListForm" runat="server" CausesValidation="False" __designer:wfdid="w77" Visible="False"></asp:Button><ajaxToolkit:ModalPopupExtender id="mpeListForm" runat="server" __designer:wfdid="w78" TargetControlID="btnhiddenListForm" Drag="True" PopupDragHandleControlID="lblListForm" BackgroundCssClass="modalBackground" PopupControlID="pnlListForm"></ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
</asp:UpdatePanel></TD></TR></TBODY></TABLE>
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint2"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Role :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

