<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="mstPerson.aspx.vb" Inherits="Master_Person" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header"  colspan="2">
                <asp:Label ID="Label1" runat="server" Text=".: Employee" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="2"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Employee :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 15%" class="Label" align=left>Filter</TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="p.nip">NIK</asp:ListItem>
<asp:ListItem Value="Name">Name</asp:ListItem>
<asp:ListItem>Address</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="200px"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbDiv" runat="server" Text="Business Unit"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLDiv" runat="server" CssClass="inpText" Width="200px" AutoPostBack="True">
            </asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbDept" runat="server" Text="Department"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLDept" runat="server" CssClass="inpText" Width="200px">
            </asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status" Checked="True"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="ALL">All</asp:ListItem>
<asp:ListItem Selected="True" Value="ACTIVE">ACTIVE</asp:ListItem>
<asp:ListItem Value="INACTIVE">INACTIVE</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvTRN" runat="server" ForeColor="#333333" Width="100%" GridLines="None" AllowPaging="True" PageSize="8" CellPadding="4" AutoGenerateColumns="False" AllowSorting="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="personoid,salaryoid" DataNavigateUrlFormatString="~\Master\mstPerson.aspx?oid={0}&amp;iOid={1}" DataTextField="nip" HeaderText="NIK" SortExpression="nip">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="name" HeaderText="Name" SortExpression="name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="divname" HeaderText="Business Unit" SortExpression="divname">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="deptname" HeaderText="Department" SortExpression="deptname">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="address" HeaderText="Address" SortExpression="address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="phone" HeaderText="Phone" SortExpression="phone">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="activeflag" HeaderText="Status" SortExpression="activeflag">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" CssClass="gvhdr" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data"></asp:Label> </TD></TR></TBODY></TABLE></asp:Panel> &nbsp; 
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD style="WIDTH: 15%" class="Label" align=left><asp:Label id="i_u" runat="server" CssClass="Important" Text="New Data"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="personoid" runat="server" Visible="False"></asp:Label> <asp:Label id="divcode" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 15%" class="Label" align=left><asp:Label id="Label43" runat="server" Text="Business Unit" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD style="WIDTH: 203px" class="Label" align=left><asp:Label id="lastdiv" runat="server" Visible="False"></asp:Label> <asp:Label id="lastdept" runat="server" Visible="False"></asp:Label> <asp:Label id="lastflag" runat="server" Visible="False"></asp:Label> <asp:DropDownList id="divisioid" runat="server" CssClass="inpText" Visible="False" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label10" runat="server" Text="NIK"></asp:Label> <asp:Label id="Label34" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="nip" runat="server" CssClass="inpText" Width="100px" MaxLength="10"></asp:TextBox></TD><TD id="tdKode1" class="Label" align=left runat="server" Visible="false">Kode Sales</TD><TD id="TDkode2" class="Label" align=center runat="server" Visible="false">:</TD><TD style="WIDTH: 203px" id="TDkode3" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="salesCode" runat="server" CssClass="inpText" Width="169px" MaxLength="20"></asp:TextBox> <asp:Label id="Label11" runat="server" ForeColor="Red" Text="*"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label27" runat="server" Text="Department"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="deptoid" runat="server" CssClass="inpText" Width="175px"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label2" runat="server" Text="Position"></asp:Label></TD><TD class="Label" align=center>:</TD><TD style="WIDTH: 203px" class="Label" align=left><asp:DropDownList id="leveloid" runat="server" CssClass="inpText" Width="123px" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label7" runat="server" Text="Gender"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="personsex" runat="server" CssClass="inpText" Width="105px"><asp:ListItem>LAKI-LAKI</asp:ListItem>
<asp:ListItem>PEREMPUAN</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label26" runat="server" Text="Status"></asp:Label></TD><TD class="Label" align=center>:</TD><TD style="WIDTH: 203px" class="Label" align=left><asp:DropDownList id="activeflag" runat="server" CssClass="inpText" Width="105px"><asp:ListItem>ACTIVE</asp:ListItem>
<asp:ListItem>INACTIVE</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 44px" class="Label" align=left><asp:Label id="Label4" runat="server" Text="Employee Name"></asp:Label> <asp:Label id="Label35" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD style="HEIGHT: 44px" class="Label" align=center>:</TD><TD style="HEIGHT: 44px" class="Label" align=left><asp:DropDownList id="persontitleoid" runat="server" CssClass="inpText" Width="75px">
            </asp:DropDownList> - <asp:TextBox id="personname" runat="server" CssClass="inpText" Width="200px" MaxLength="50"></asp:TextBox></TD><TD style="HEIGHT: 44px" class="Label" align=left><asp:Label id="Label18" runat="server" Text="Employee Picture"></asp:Label></TD><TD style="HEIGHT: 44px" class="Label" align=center>:</TD><TD style="WIDTH: 203px; HEIGHT: 44px" class="Label" align=left><asp:FileUpload id="personpicture" runat="server" Font-Size="X-Small" CssClass="inpText" Width="200px" Font-Overline="False"></asp:FileUpload> <asp:ImageButton id="btnUpload" runat="server" ImageUrl="~/Images/upload.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="lblpicloc" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label23" runat="server" Text="City And Date Of Birth" Width="94px"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="cityofbirth" runat="server" CssClass="inpText" Width="105px"></asp:DropDownList>&nbsp;- <asp:TextBox id="dateofbirth" runat="server" CssClass="inpText" Width="60px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbcal2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label41" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD style="WIDTH: 203px" class="Label" vAlign=top align=left rowSpan=5><asp:Image id="imgperson" runat="server" ImageUrl="~/Images/no_image.jpg" ImageAlign="AbsMiddle" Width="150px"></asp:Image></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text="Current Address"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="curraddr" runat="server" CssClass="inpText" Width="250px" MaxLength="200"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="curraddrcityoid" runat="server" CssClass="inpText" Width="105px"></asp:DropDownList></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label9" runat="server" Text="Religion"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="religionoid" runat="server" CssClass="inpText" Width="105px"></asp:DropDownList></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label8" runat="server" Text="Marital Status"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="maritalstatus" runat="server" CssClass="inpText" Width="105px"><asp:ListItem>KAWIN</asp:ListItem>
<asp:ListItem>TIDAK KAWIN</asp:ListItem>
<asp:ListItem>CERAI</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6><ajaxToolkit:CalendarExtender id="ceBirthDate" runat="server" Format="MM/dd/yyyy" PopupButtonID="imbcal2" TargetControlID="dateofbirth"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeBirthDate" runat="server" TargetControlID="dateofbirth" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD class="Label" align=left><asp:Label id="Label6" runat="server" Text="ID Card No."></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cardno" runat="server" CssClass="inpText" Width="100px" MaxLength="30"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label46" runat="server" Text="ID Card Picture" Width="84px"></asp:Label></TD><TD class="Label" align=center>:</TD><TD style="WIDTH: 203px" class="Label" align=left><asp:FileUpload id="CardUpload" runat="server" Font-Size="X-Small" CssClass="inpText" Width="200px" Font-Overline="False"></asp:FileUpload> <asp:ImageButton id="btnUploadCard" runat="server" ImageUrl="~/Images/upload.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="cardpicture" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left rowSpan=1><asp:Label id="Label12" runat="server" Text="Original Address"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="origaddr" runat="server" CssClass="inpText" Width="250px" MaxLength="200"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD style="WIDTH: 203px" class="Label" vAlign=top align=left rowSpan=2><asp:Image id="imgcard" runat="server" ImageUrl="~/Images/no_image.jpg" ImageAlign="AbsMiddle" Width="150px"></asp:Image></TD></TR><TR><TD class="Label" align=left rowSpan=1></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="origaddrcityoid" runat="server" CssClass="inpText" Width="105px"></asp:DropDownList></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label22" runat="server" Text="Phone 1"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="personphone1" runat="server" CssClass="inpText" Width="100px" MaxLength="20"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label21" runat="server" Text="Phone 2"></asp:Label></TD><TD class="Label" align=center>:</TD><TD style="WIDTH: 203px" class="Label" align=left><asp:TextBox id="personphone2" runat="server" CssClass="inpText" Width="100px" MaxLength="20"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label20" runat="server" Text="Emergency Phone"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="emergencyphone" runat="server" CssClass="inpText" Width="100px" MaxLength="20"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label13" runat="server" Text="Komisi Sales"></asp:Label></TD><TD class="Label" align=center><asp:Label id="Label14" runat="server" Text=":"></asp:Label></TD><TD style="WIDTH: 203px" class="Label" align=left><asp:TextBox id="komisi" runat="server" CssClass="inpText" Width="30px" MaxLength="5">0</asp:TextBox> <asp:Label id="Label15" runat="server" Text="%"></asp:Label></TD></TR>
    <tr>
        <td align="left" class="Label">
            Payment Type</td>
        <td align="center" class="Label">
            :</td>
        <td align="left" class="Label">
            <asp:DropDownList id="payment_type" runat="server" CssClass="inpText" Width="105px" AutoPostBack="True">
                <asp:ListItem>TRANSFER</asp:ListItem>
                <asp:ListItem>CASH</asp:ListItem>
            </asp:DropDownList></td>
        <td align="left" class="Label">
            Nama Bank</td>
        <td align="center" class="Label">
            :</td>
        <td align="left" class="Label" style="width: 203px">
            <asp:TextBox ID="bank_name" runat="server" CssClass="inpText" MaxLength="50" Width="150px"></asp:TextBox></td>
    </tr>
    <tr>
        <td align="left" class="Label">
            No Account</td>
        <td align="center" class="Label">
            :</td>
        <td align="left" class="Label">
            <asp:TextBox ID="account_no" runat="server" CssClass="inpText" MaxLength="50" Width="150px"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="fteaccount_no" runat="server" TargetControlID="account_no"
                ValidChars="1234567890.,">
            </ajaxToolkit:FilteredTextBoxExtender>
        </td>
        <td align="left" class="Label">
            Nama Account</td>
        <td align="center" class="Label">
            :</td>
        <td align="left" class="Label" style="width: 203px">
            <asp:TextBox ID="account_name" runat="server" CssClass="inpText" MaxLength="50" Width="150px"></asp:TextBox></td>
    </tr>
    <TR><TD class="Label" align=left><asp:Label id="Label5" runat="server" Text="Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="personnote" runat="server" CssClass="inpText" Width="250px" MaxLength="50"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD style="WIDTH: 203px" class="Label" align=left></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="ftbIDCard" runat="server" TargetControlID="cardno" ValidChars="1234567890"></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="ftbPhone1" runat="server" TargetControlID="personphone1" ValidChars="1234567890"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbPhone2" runat="server" TargetControlID="personphone2" ValidChars="1234567890"></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="ftbEmegPhone" runat="server" TargetControlID="emergencyphone" ValidChars="1234567890"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbnip" runat="server" TargetControlID="nip" FilterMode="InvalidChars" InvalidChars=" "></ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="lblWarning" runat="server" CssClass="Important"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=6></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton>&nbsp;</TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnUpload"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnUploadCard"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Employee :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel3" runat="server" HeaderText="TabPanel3">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" Visible="False">
                                <ContentTemplate>
<TABLE width=800><TBODY><TR><TD colSpan=7><asp:Label id="salaryoid" runat="server" Visible="False">0</asp:Label></TD><TD colSpan=1></TD></TR><TR><TD style="WIDTH: 104px"></TD><TD></TD><TD><asp:DropDownList id="ddlperiodtype" runat="server" CssClass="inpText" Width="100px" Visible="False"><asp:ListItem>BULANAN</asp:ListItem>
<asp:ListItem Value="HARIAN">HARIAN</asp:ListItem>
</asp:DropDownList></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD></TR><TR><TD style="WIDTH: 104px">Premi Hadir</TD><TD>:</TD><TD><asp:TextBox id="salary_premi_hadir" runat="server" CssClass="inpText" Width="100px" MaxLength="10" AutoPostBack="True" OnTextChanged="salary_gaji_bln_TextChanged"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftePremiHadir" runat="server" TargetControlID="salary_premi_hadir" ValidChars="1234567890.,">
                                                </ajaxToolkit:FilteredTextBoxExtender> </TD><TD><asp:DropDownList id="premi_hadir_acctgoid" runat="server" CssClass="inpText" Width="250px">
                                                    </asp:DropDownList></TD><TD>Tunjangan Jabatan</TD><TD>:</TD><TD><asp:TextBox id="salary_tunjangan_jabatan" runat="server" CssClass="inpText" Width="100px" MaxLength="10" AutoPostBack="True" OnTextChanged="salary_gaji_bln_TextChanged"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="fteTunjangan" runat="server" TargetControlID="salary_tunjangan_jabatan" ValidChars="1234567890.,">
                                                </ajaxToolkit:FilteredTextBoxExtender> </TD><TD><asp:DropDownList id="salary_tunjangan_acctgoid" runat="server" CssClass="inpText" Width="250px">
                                            </asp:DropDownList></TD></TR><TR><TD style="WIDTH: 104px">Premi Extra</TD><TD>:</TD><TD><asp:TextBox id="salary_premi_extra" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" OnTextChanged="salary_gaji_bln_TextChanged"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftePremiExtra" runat="server" TargetControlID="salary_premi_extra" ValidChars="1234567890.,">
                                                </ajaxToolkit:FilteredTextBoxExtender> </TD><TD><asp:DropDownList id="premi_extra_acctgoid" runat="server" CssClass="inpText" Width="250px">
                                                </asp:DropDownList></TD><TD>Bonus</TD><TD>:</TD><TD><asp:TextBox id="salary_bonus" runat="server" CssClass="inpText" Width="100px" MaxLength="10" AutoPostBack="True" OnTextChanged="salary_gaji_bln_TextChanged"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="fteBonus" runat="server" TargetControlID="salary_bonus" ValidChars="1234567890.,">
                                                </ajaxToolkit:FilteredTextBoxExtender> </TD><TD><asp:DropDownList id="salary_bonus_acctgoid" runat="server" CssClass="inpText" Width="250px">
                                                </asp:DropDownList></TD></TR><TR><TD style="WIDTH: 104px">Gaji Bulanan</TD><TD>:</TD><TD><asp:TextBox id="salary_gaji_bln" runat="server" CssClass="inpText" Width="100px" MaxLength="10" AutoPostBack="True"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftegajibulanan" runat="server" TargetControlID="salary_gaji_bln" ValidChars="1234567890.,">
                                                </ajaxToolkit:FilteredTextBoxExtender> </TD><TD><asp:DropDownList id="salary_gaji_bln_acctgoid" runat="server" CssClass="inpText" Width="250px">
                                                </asp:DropDownList></TD><TD>Lembur</TD><TD>:</TD><TD><asp:TextBox id="salary_lembur" runat="server" CssClass="inpText" Width="100px" MaxLength="10" AutoPostBack="True" OnTextChanged="salary_gaji_bln_TextChanged"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="fteLembur" runat="server" TargetControlID="salary_lembur" ValidChars="1234567890.,">
                                                </ajaxToolkit:FilteredTextBoxExtender> </TD><TD><asp:DropDownList id="salary_lembur_acctgoid" runat="server" CssClass="inpText" Width="250px">
                                                </asp:DropDownList></TD></TR><TR><TD style="WIDTH: 104px">Gaji Harian</TD><TD>:</TD><TD><asp:TextBox id="salary_gaji_harian" runat="server" CssClass="inpText" Width="100px"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftesalary_gaji_harian" runat="server" TargetControlID="salary_gaji_harian" ValidChars="1234567890.,">
            </ajaxToolkit:FilteredTextBoxExtender> </TD><TD><asp:DropDownList id="salary_gaji_harian_acctgoid" runat="server" CssClass="inpText" Width="250px">
            </asp:DropDownList></TD><TD>Kerajinan</TD><TD>:</TD><TD><asp:TextBox id="salary_kerajinan" runat="server" CssClass="inpText" Width="100px" MaxLength="10" AutoPostBack="True" OnTextChanged="salary_gaji_bln_TextChanged">0</asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftesalary_kerajinan" runat="server" TargetControlID="salary_kerajinan" ValidChars="1234567890.,">
            </ajaxToolkit:FilteredTextBoxExtender> </TD><TD><asp:DropDownList id="salary_kerajinan_acctgoid" runat="server" CssClass="inpText" Width="250px">
        </asp:DropDownList></TD></TR><TR><TD style="WIDTH: 104px"><asp:Label id="TxtHarianluarkota" runat="server" Text="Harian luar kota" Width="100px"></asp:Label></TD><TD>:</TD><TD><asp:TextBox id="salary_harian_lk" runat="server" CssClass="inpText" Width="100px" MaxLength="10" AutoPostBack="True" OnTextChanged="salary_gaji_bln_TextChanged"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="fteLuarKota" runat="server" TargetControlID="salary_harian_lk" ValidChars="1234567890.," BehaviorID="ctl02_ftePremiExtra">
                                                </ajaxToolkit:FilteredTextBoxExtender> </TD><TD><asp:DropDownList id="salary_harian_lk_acctgoid" runat="server" CssClass="inpText" Width="250px">
                                                </asp:DropDownList></TD><TD><asp:Label id="TxtHariandalamkota" runat="server" Text="Harian dalam kota" Width="150px"></asp:Label></TD><TD>:</TD><TD><asp:TextBox id="salary_harian_dk" runat="server" CssClass="inpText" Width="100px" MaxLength="10" AutoPostBack="True" OnTextChanged="salary_gaji_bln_TextChanged"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="fteDalamKota" runat="server" TargetControlID="salary_harian_dk" ValidChars="1234567890.,">
                                                </ajaxToolkit:FilteredTextBoxExtender> </TD><TD><asp:DropDownList id="salary_harian_dk_acctgoid" runat="server" CssClass="inpText" Width="250px">
                                                </asp:DropDownList></TD></TR><TR><TD style="WIDTH: 104px">Lain - Lain</TD><TD>:</TD><TD><asp:TextBox id="salary_other" runat="server" CssClass="inpText" Width="100px" MaxLength="10" AutoPostBack="True" OnTextChanged="salary_gaji_bln_TextChanged">0</asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftesalary_other" runat="server" TargetControlID="salary_other" ValidChars="1234567890.,">
            </ajaxToolkit:FilteredTextBoxExtender> </TD><TD><asp:DropDownList id="salary_other_acctgoid" runat="server" CssClass="inpText" Width="250px">
            </asp:DropDownList></TD><TD>Lembur Luar Kota</TD><TD></TD><TD><asp:TextBox id="salary_lembur_lk" runat="server" CssClass="inpText" Width="100px" MaxLength="10" AutoPostBack="True" OnTextChanged="salary_gaji_bln_TextChanged" __designer:wfdid="w1"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftlemburlk" runat="server" TargetControlID="salary_lembur_lk" ValidChars="1234567890.," __designer:wfdid="w2"></ajaxToolkit:FilteredTextBoxExtender></TD><TD><asp:DropDownList id="salary_lembur_lk_acctgoid" runat="server" CssClass="inpText" Width="250px" __designer:wfdid="w3"></asp:DropDownList></TD></TR><TR><TD colSpan=8><asp:ImageButton id="btnSaveSalary" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancelSalary" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDeleteSalary" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton></TD></TR></TBODY></TABLE>
</ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <strong><span style="font-size: 9pt">Form Salary :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <br />
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

