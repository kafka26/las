Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_Person
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim xreader1 As SqlDataReader
    Dim Odbxc As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
    Dim scek As Integer = 0
#End Region

#Region "Functions"
    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        If nip.Text.Trim = "" Then
            sError &= "- Please fill NIK field!<BR>"
        End If
        If personname.Text.Trim = "" Then
            sError &= "- Please fill EMPLOYEE NAME field!<BR>"
        End If
        Dim sErr As String = ""
        If dateofbirth.Text <> "" Then
            If Not IsValidDate(dateofbirth.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- DATE OF BIRTH is invalid. " & sErr & "<BR>"
            End If
        End If
        If divisioid.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If deptoid.SelectedValue = "" Then
            sError &= "- Please select DEPARTMENT field!<BR>"
        End If
        If leveloid.SelectedValue = "" Then
            sError &= "- Please select POSITION field!<BR>"
        End If
        If persontitleoid.SelectedValue = "" Then
            sError &= "- Please select EMPLOYEE TITLE field!<BR>"
        End If
        If cityofbirth.SelectedValue = "" Then
            sError &= "- Please select CITY field!<BR>"
        End If
        If religionoid.SelectedValue = "" Then
            sError &= "- Please select RELIGION field!<BR>"
        End If
        If leveloid.SelectedItem.Text.ToUpper = "SALES" Then
            If salesCode.Text.Trim = "" Then
                sError &= "- Please fill SALES CODE field!<BR>"
            End If
        End If

        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function checkTypeFile(ByVal filename As String) As Boolean
        Dim fileExt As String = Path.GetExtension(filename).ToLower()
        Dim allowedExt As String() = {".jpg", ".jpeg", ".bmp", ".gif", ".png"}
        Dim fileOK As Boolean = False
        For i As Integer = 0 To allowedExt.Length - 1
            If fileExt = allowedExt(i) Then
                fileOK = True
            End If
        Next
        Return fileOK
    End Function

    Private Function IsDataExists() As Boolean
        sSql = "SELECT COUNT(*) FROM QL_mstperson WHERE nip='" & Tchar(nip.Text) & "'"
        If Not Session("oid") Is Nothing And Session("oid") <> "" Then
            sSql &= " AND personoid<>" & Session("oid")
        End If
        If ToDouble(cKon.ambilscalar(sSql).ToString) > 0 Then
            showMessage("NIK has been used by another data. Please fill another NIK!", 2)
            Return True
        End If
        Return False
    End Function

    Private Function IsPersonExists(ByVal sType As String) As Boolean
        Dim bReturn As Boolean = True
        If sType = "input" Then
            sSql = "Select count(*) From QL_mstsalary where personoid=" & personoid.Text & " AND cmpcode='" & CompnyCode & "'"
        Else
            sSql = "Select count(*) From QL_r_salary where personoid=" & personoid.Text & " AND cmpcode='" & CompnyCode & "'"
        End If

        If cKon.ambilscalar(sSql) = 0 Then
            bReturn = False
        End If
        Return bReturn
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitAllDDL()
        ' Init DDL City
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode LIKE '%" & CompnyCode & "%' AND gengroup='CITY' AND activeflag='ACTIVE' ORDER BY genoid"
        FillDDL(cityofbirth, sSql)
        FillDDL(curraddrcityoid, sSql)
        FillDDL(origaddrcityoid, sSql)
        If cityofbirth.Items.Count = 0 Then
            lblWarning.Text &= "- Please fill <STRONG>City of Birth, City of Current Address</STRONG> and <STRONG>City of Original Address</STRONG> data in <STRONG>General Form</STRONG> with selected group is CITY!<BR>"
        End If
        ' Init DDL Religion
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode LIKE '%" & CompnyCode & "%' AND gengroup='RELIGION' AND activeflag='ACTIVE' ORDER BY genoid"
        FillDDL(religionoid, sSql)
        If religionoid.Items.Count = 0 Then
            lblWarning.Text &= "- Please fill <STRONG>Religion</STRONG> data in <STRONG>General Form</STRONG> with selected group is RELIGION!<BR>"
        End If
        ' Init DDL Person Title
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode LIKE '%" & CompnyCode & "%' AND gengroup='PERSON TITLE' AND activeflag='ACTIVE' ORDER BY genoid"
        FillDDL(persontitleoid, sSql)
        If persontitleoid.Items.Count = 0 Then
            lblWarning.Text &= "- Please fill <STRONG>Title of Name</STRONG> data in <STRONG>General Form</STRONG> with selected group is PERSON TITLE!<BR>"
        End If
        ' Init DDL Level
        sSql = "SELECT leveloid, leveldesc FROM QL_mstlevel WHERE cmpcode LIKE '%" & CompnyCode & "%' AND activeflag='ACTIVE' ORDER BY leveloid"
        FillDDL(leveloid, sSql)
        If leveloid.Items.Count = 0 Then
            lblWarning.Text &= "- Please fill <STRONG>Position</STRONG> data in <STRONG>Position Form</STRONG>!<BR>"
        Else
            If leveloid.SelectedItem.Text.ToUpper = "SALES" Then
                tdKode1.Visible = True
                TDkode2.Visible = True
                TDkode3.Visible = True
            Else
                tdKode1.Visible = False
                TDkode2.Visible = False
                TDkode3.Visible = False
            End If
        End If
        ' Init DDL Division
        sSql = "SELECT di.divoid, di.divname FROM QL_mstdivision di WHERE di.activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND di.cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(divisioid, sSql)
        FillDDL(FilterDDLDiv, sSql)
        If divisioid.Items.Count = 0 Then
            lblWarning.Text &= "- Please fill <STRONG>Business Unit</STRONG> data in <STRONG>Business Unit Form</STRONG>!<BR>"
            lblWarning.Text &= "- Please fill <STRONG>Department</STRONG> data in <STRONG>Department Form</STRONG> in every Business Unit you have created!<BR>"
        Else
            SetDivCode(divisioid.SelectedValue)
            ' Init DDL Department Filter By Division
            sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE divoid=" & divisioid.SelectedValue & " AND activeflag='ACTIVE'"
            FillDDL(deptoid, sSql)
            FillDDL(FilterDDLDept, sSql)
            If deptoid.Items.Count = 0 Then
                lblWarning.Text &= "- Please fill <STRONG>Department</STRONG> data in <STRONG>Department Form</STRONG> in every Business Unit you have created!<BR>"
            End If
        End If

        FillDDLAcctg(premi_hadir_acctgoid, "VAR_EXPENSE", CompnyCode)
        FillDDLAcctg(premi_extra_acctgoid, "VAR_EXPENSE", CompnyCode)
        FillDDLAcctg(salary_gaji_bln_acctgoid, "VAR_EXPENSE", CompnyCode)
        FillDDLAcctg(salary_gaji_harian_acctgoid, "VAR_EXPENSE", CompnyCode)
        FillDDLAcctg(salary_harian_lk_acctgoid, "VAR_EXPENSE", CompnyCode)
        FillDDLAcctg(salary_harian_dk_acctgoid, "VAR_EXPENSE", CompnyCode)
        FillDDLAcctg(salary_bonus_acctgoid, "VAR_EXPENSE", CompnyCode)
        FillDDLAcctg(salary_lembur_acctgoid, "VAR_EXPENSE", CompnyCode)
        FillDDLAcctg(salary_lembur_lk_acctgoid, "VAR_EXPENSE", CompnyCode)
        FillDDLAcctg(salary_tunjangan_acctgoid, "VAR_EXPENSE", CompnyCode)
        FillDDLAcctg(salary_kerajinan_acctgoid, "VAR_EXPENSE", CompnyCode)
        FillDDLAcctg(salary_other_acctgoid, "VAR_EXPENSE", CompnyCode)

    End Sub

    Private Sub InitDDLDept(ByVal sDiv As String)
        ' Init DDL Department Filter By Division
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE divoid=" & sDiv & " AND activeflag='ACTIVE'"
        FillDDL(deptoid, sSql)
    End Sub

    Private Sub InitDDLFilterDept(ByVal sDiv As String)
        ' Init DDL Department Filter By Division
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE divoid=" & sDiv & " AND activeflag='ACTIVE'"
        FillDDL(FilterDDLDept, sSql)
    End Sub

    Private Sub BindPersonData(ByVal sFilter As String)
        sSql = "SELECT p.personoid, p.nip, (CASE g1.gendesc WHEN 'No Title' THEN '' ELSE g1.gendesc END) + ' ' + p.personname AS name, di.divname, de.deptname, p.curraddr + ' ' + g2.gendesc AS address, ISNULL(p.personphone1, ISNULL(p.personphone2, p.emergencyphone)) AS phone, p.activeflag, Isnull((SELECT salaryoid from QL_mstsalary s Where s.personoid=p.personoid AND s.cmpcode=p.cmpcode), 0) salaryoid FROM QL_mstperson p INNER JOIN QL_mstgen g1 ON p.persontitleoid=g1.genoid AND g1.activeflag='ACTIVE' INNER JOIN QL_mstgen g2 ON p.curraddrcityoid=g2.genoid AND g2.activeflag='ACTIVE' INNER JOIN QL_mstdivision di ON p.divisioid=di.divoid AND di.activeflag='ACTIVE' INNER JOIN QL_mstdept de ON p.deptoid=de.deptoid AND de.activeflag='ACTIVE' INNER JOIN QL_mstgen g4 ON g4.genoid=p.origaddrcityoid AND g4.activeflag='ACTIVE' INNER JOIN QL_mstgen g5 ON g5.genoid=p.religionoid AND g5.activeflag='ACTIVE' INNER JOIN QL_mstlevel l ON l.leveloid=p.leveloid AND l.activeflag='ACTIVE'"
        If FilterDDL.SelectedValue = "Name" Then
            sSql &= " WHERE (p.personname LIKE '%" & Tchar(sFilter) & "%' OR g1.gendesc LIKE '%" & Tchar(sFilter) & "%')"
        ElseIf FilterDDL.SelectedValue = "Address" Then
            sSql &= " WHERE (p.curraddr LIKE '%" & Tchar(sFilter) & "%' OR g2.gendesc LIKE '%" & Tchar(sFilter) & "%')"
        Else
            sSql &= " WHERE " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(sFilter) & "%'"
        End If
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND p.cmpcode='" & Session("CompnyCode") & "'"
        End If
        If cbDiv.Checked Then
            If FilterDDLDiv.Items.Count > 0 Then
                sSql &= " AND p.divisioid=" & FilterDDLDiv.SelectedValue
            Else
                cbDiv.Checked = False
            End If
        End If
        If cbDept.Checked Then
            If FilterDDLDept.Items.Count > 0 Then
                sSql &= " AND p.deptoid=" & FilterDDLDept.SelectedValue
            Else
                cbDept.Checked = False
            End If
        End If
        If cbStatus.Checked Then
            If FilterDDLStatus.SelectedValue <> "ALL" Then
                sSql &= " AND p.activeflag='" & FilterDDLStatus.SelectedValue & "'"
            End If
        End If
        If checkPagePermission("~\Master\mstPerson.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND p.createuser='" & Session("UserID") & "'"
        End If
        sSql &= " ORDER BY p.personoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_mstperson")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub uploadFileGambar(ByVal proses As String, ByVal fname As String, ByVal iWidth As Integer, ByVal iHeight As Integer, ByVal sFolder As String)
        Dim savePath As String = Server.MapPath(sFolder)
        Dim sbmpW As Integer = iWidth
        Dim sbmpH As Integer = iHeight
        If personpicture.HasFile Then
            If checkTypeFile(personpicture.FileName) Then
                Dim divideBy, divideByH, divideByW As Double
                Dim newWidth, newHeight As Integer
                savePath &= fname
                Dim uploadBmp As Bitmap = Bitmap.FromStream(personpicture.PostedFile.InputStream)
                divideByW = uploadBmp.Width / sbmpW
                divideByH = uploadBmp.Height / sbmpH
                If divideByW > 1 Or divideByH > 1 Then
                    If divideByW > divideByH Then
                        divideBy = divideByW
                    Else
                        divideBy = divideByH
                    End If
                    newWidth = CInt(CDbl(uploadBmp.Width) / divideBy)
                    newHeight = CInt(CDbl(uploadBmp.Height) / divideBy)
                Else
                    newWidth = uploadBmp.Width
                    newHeight = uploadBmp.Height
                End If
                Dim newBmp As Bitmap = New Bitmap(newWidth, newHeight)
                newBmp.SetResolution(uploadBmp.HorizontalResolution, uploadBmp.VerticalResolution)
                Dim newGraphic As Graphics = Graphics.FromImage(newBmp)
                Try
                    newGraphic.Clear(Color.White)
                    newGraphic.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
                    newGraphic.DrawImage(uploadBmp, New Rectangle(0, 0, newWidth, newHeight), 0, 0, uploadBmp.Width, uploadBmp.Height, GraphicsUnit.Pixel)
                    newGraphic.Dispose()
                    newBmp.Save(savePath, Imaging.ImageFormat.Jpeg)
                    imgperson.ImageUrl = sFolder & fname
                Catch ex As Exception
                End Try
            Else
                showMessage("File can't be uploaded. File type must be .jpg/.jpeg/.gif/.png/.bmp!", 2)
                imgperson.ImageUrl = sFolder & "no_image.jpg"
                TabContainer1.ActiveTabIndex = 1
            End If
        Else
            showMessage("Please choose uploaded file first !", 2)
            imgperson.ImageUrl = sFolder & "no_image.jpg"
        End If
    End Sub

    Private Sub uploadFileGambar2(ByVal proses As String, ByVal fname As String, ByVal iWidth As Integer, ByVal iHeight As Integer, ByVal sFolder As String)
        Dim savePath As String = Server.MapPath(sFolder)
        Dim sbmpW As Integer = iWidth
        Dim sbmpH As Integer = iHeight
        If CardUpload.HasFile Then
            If checkTypeFile(CardUpload.FileName) Then
                Dim divideBy, divideByH, divideByW As Double
                Dim newWidth, newHeight As Integer
                savePath &= fname
                Dim uploadBmp As Bitmap = Bitmap.FromStream(CardUpload.PostedFile.InputStream)
                divideByW = uploadBmp.Width / sbmpW
                divideByH = uploadBmp.Height / sbmpH
                If divideByW > 1 Or divideByH > 1 Then
                    If divideByW > divideByH Then
                        divideBy = divideByW
                    Else
                        divideBy = divideByH
                    End If
                    newWidth = CInt(CDbl(uploadBmp.Width) / divideBy)
                    newHeight = CInt(CDbl(uploadBmp.Height) / divideBy)
                Else
                    newWidth = uploadBmp.Width
                    newHeight = uploadBmp.Height
                End If
                Dim newBmp As Bitmap = New Bitmap(newWidth, newHeight)
                newBmp.SetResolution(uploadBmp.HorizontalResolution, uploadBmp.VerticalResolution)
                Dim newGraphic As Graphics = Graphics.FromImage(newBmp)
                Try
                    newGraphic.Clear(Color.White)
                    newGraphic.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
                    newGraphic.DrawImage(uploadBmp, New Rectangle(0, 0, newWidth, newHeight), 0, 0, uploadBmp.Width, uploadBmp.Height, GraphicsUnit.Pixel)
                    newGraphic.Dispose()
                    newBmp.Save(savePath, Imaging.ImageFormat.Jpeg)
                    imgcard.ImageUrl = sFolder & fname
                Catch ex As Exception
                End Try
            Else
                showMessage("File can't be uploaded. File type must be .jpg/.jpeg/.gif/.png/.bmp!", 2)
                imgcard.ImageUrl = sFolder & "no_image.jpg"
                TabContainer1.ActiveTabIndex = 1
            End If
        Else
            showMessage("Please choose uploaded file first !", 2)
            imgcard.ImageUrl = sFolder & "no_image.jpg"
        End If
    End Sub

    Private Sub FillTextBox(ByVal sOid As String, ByVal iOid As Integer)
        ' Select Person General Data
        Try
            sSql = "SELECT p.cmpcode, p.personoid, p.nip, p.personname, p.persontitleoid, p.personsex, p.maritalstatus, p.divisioid, p.deptoid, p.leveloid, p.cardno, p.cityofbirth, p.dateofbirth, p.curraddr, p.curraddrcityoid, p.origaddr, p.origaddrcityoid, p.personphone1, p.personphone2, p.emergencyphone, p.personnote, p.religionoid, p.cardpicture, p.personpicture, p.activeflag, p.createuser, p.createtime, p.upduser, p.updtime, salescode, komisi, p.payment_type, p.account_no, p.account_name, p.bank_name FROM QL_mstperson p WHERE p.personoid=" & sOid
            xCmd.CommandText = sSql
            btnDelete.Visible = False
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            While xreader.Read
                personoid.Text = Trim(xreader("personoid").ToString)
                nip.Text = Trim(xreader("nip").ToString)
                personname.Text = Trim(xreader("personname").ToString)

                payment_type.SelectedValue = Trim(xreader("payment_type").ToString)
                account_name.Text = Trim(xreader("account_name").ToString)
                account_no.Text = Trim(xreader("account_no").ToString)
                bank_name.Text = Trim(xreader("bank_name").ToString)

                persontitleoid.SelectedValue = xreader("persontitleoid")
                personsex.SelectedValue = Trim(xreader("personsex").ToString)
                maritalstatus.SelectedValue = Trim(xreader("maritalstatus").ToString)
                divisioid.SelectedValue = xreader("divisioid").ToString
                lastdiv.Text = Trim(xreader("divisioid").ToString)
                InitDDLDept(divisioid.SelectedValue)
                deptoid.SelectedValue = xreader("deptoid").ToString
                lastdept.Text = Trim(xreader("deptoid").ToString)
                leveloid.SelectedValue = xreader("leveloid").ToString
                cardno.Text = Trim(xreader("cardno").ToString)
                cityofbirth.SelectedItem.Text = Trim(xreader("cityofbirth").ToString)
                dateofbirth.Text = Format(xreader("dateofbirth"), "MM/dd/yyyy")
                If dateofbirth.Text = "01/01/1900" Then
                    dateofbirth.Text = ""
                End If
                curraddr.Text = Trim(xreader("curraddr").ToString)
                curraddrcityoid.SelectedValue = xreader("curraddrcityoid").ToString
                origaddr.Text = Trim(xreader("origaddr").ToString)
                origaddrcityoid.SelectedValue = xreader("origaddrcityoid").ToString
                personphone1.Text = Trim(xreader("personphone1").ToString)
                personphone2.Text = Trim(xreader("personphone2").ToString)
                emergencyphone.Text = Trim(xreader("emergencyphone").ToString)
                personnote.Text = Trim(xreader("personnote").ToString)
                religionoid.SelectedValue = xreader("religionoid").ToString
                cardpicture.Text = Trim(xreader("cardpicture").ToString)
                If cardpicture.Text <> "" Then
                    imgcard.ImageUrl = "~\Images\CardImg\" & cardpicture.Text
                Else
                    imgcard.ImageUrl = "~\Images\CardImg\no_image.jpg"
                End If
                lblpicloc.Text = Trim(xreader("personpicture").ToString)
                If lblpicloc.Text <> "" Then
                    imgperson.ImageUrl = "~\Images\PersonImg\" & lblpicloc.Text
                Else
                    imgperson.ImageUrl = "~\Images\PersonImg\no_image.jpg"
                End If
                activeflag.SelectedValue = Trim(xreader("activeflag").ToString)
                lastflag.Text = Trim(xreader("activeflag").ToString)
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
                divcode.Text = xreader("cmpcode").ToString
                salesCode.Text = xreader("salescode").ToString
                komisi.Text = ToMaskEdit(ToDouble(xreader("komisi").ToString), 2)
                If leveloid.SelectedItem.Text.ToUpper = "SALES" Then
                    tdKode1.Visible = True
                    TDkode2.Visible = True
                    TDkode3.Visible = True
                Else
                    tdKode1.Visible = False
                    TDkode2.Visible = False
                    TDkode3.Visible = False
                End If 
            End While

            xreader.Close()
            conn.Close()

            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            sSql = "SELECT cmpcode, salaryoid, personoid, salary_premi_hadir, premi_hadir_acctgoid, salary_premi_extra, premi_extra_acctgoid, salary_gaji_bln, salary_gaji_bln_acctgoid, salary_harian_lk, salary_harian_lk_acctgoid, salary_harian_dk, salary_harian_dk_acctgoid, salary_bonus, salary_bonus_acctgoid, salary_lembur, salary_lembur_acctgoid, salary_lembur_lk, salary_lembur_lk_acctgoid, salary_tunjangan_jabatan, salary_tunjangan_acctgoid, createuser, createtime, salary_type, salary_gaji_harian_acctgoid, salary_gaji_harian, salary_kerajinan, salary_kerajinan_acctgoid, salary_other, salary_other_acctgoid from QL_mstsalary where personoid=" & sOid & " and salaryoid= " & iOid
            xCmd.CommandText = sSql
            xreader = xCmd.ExecuteReader
            While xreader.Read
                salaryoid.Text = iOid
                ddlperiodtype.SelectedValue = xreader("salary_type").ToString
                salary_premi_hadir.Text = ToMaskEdit(xreader("salary_premi_hadir").ToString, 2)
                salary_premi_extra.Text = ToMaskEdit(xreader("salary_premi_extra").ToString, 2)
                salary_gaji_bln.Text = ToMaskEdit(xreader("salary_gaji_bln").ToString, 2)
                salary_gaji_harian.Text = ToMaskEdit(xreader("salary_gaji_harian").ToString, 2)
                salary_harian_lk.Text = ToMaskEdit(xreader("salary_harian_lk").ToString, 2)
                salary_harian_dk.Text = ToMaskEdit(xreader("salary_harian_dk").ToString, 2)
                salary_bonus.Text = ToMaskEdit(xreader("salary_bonus").ToString, 2)
                salary_lembur.Text = ToMaskEdit(xreader("salary_lembur").ToString, 2)
                salary_lembur_lk.Text = ToMaskEdit(xreader("salary_lembur_lk").ToString, 2)
                salary_tunjangan_jabatan.Text = ToMaskEdit(xreader("salary_tunjangan_jabatan").ToString, 2)

                salary_kerajinan.Text = ToMaskEdit(xreader("salary_kerajinan").ToString, 2)
                salary_other.Text = ToMaskEdit(xreader("salary_other").ToString, 2)

                premi_hadir_acctgoid.SelectedValue = xreader("premi_hadir_acctgoid").ToString
                premi_extra_acctgoid.SelectedValue = xreader("premi_extra_acctgoid").ToString
                salary_tunjangan_acctgoid.SelectedValue = xreader("salary_tunjangan_acctgoid").ToString
                salary_gaji_bln_acctgoid.SelectedValue = xreader("salary_gaji_bln_acctgoid").ToString
                salary_gaji_harian_acctgoid.SelectedValue = xreader("salary_gaji_harian_acctgoid").ToString
                salary_harian_lk_acctgoid.SelectedValue = xreader("salary_harian_lk_acctgoid").ToString
                salary_harian_dk_acctgoid.SelectedValue = xreader("salary_harian_dk_acctgoid").ToString
                salary_lembur_acctgoid.SelectedValue = xreader("salary_lembur_acctgoid").ToString
                salary_lembur_lk_acctgoid.SelectedValue = xreader("salary_lembur_lk_acctgoid").ToString
                salary_bonus_acctgoid.SelectedValue = xreader("salary_bonus_acctgoid").ToString
                salary_kerajinan_acctgoid.SelectedValue = xreader("salary_kerajinan_acctgoid").ToString
                salary_other_acctgoid.SelectedValue = xreader("salary_other_acctgoid").ToString

            End While
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        Finally
            xreader.Close()
            conn.Close()
            btnDelete.Visible = True
        End Try
    End Sub 

    Private Sub SetDivCode(ByVal sOid As String)
        sSql = "SELECT DISTINCT TOP 1 divcode FROM QL_mstdivision WHERE divoid=" & sOid
        divcode.Text = cKon.ambilscalar(sSql).ToString
    End Sub

    Private Sub Hiddenfield()
        If payment_type.SelectedValue = "TRANSFER" Then
            account_no.Enabled = True
            account_no.CssClass = "inpText"
            account_name.Enabled = True
            account_name.CssClass = "inpText"
            bank_name.Enabled = True
            bank_name.CssClass = "inpText"
        Else
            account_no.Text = ""
            account_no.Enabled = False
            account_no.CssClass = "inpTextDisabled"
            account_name.Text = ""
            account_name.Enabled = False
            account_name.CssClass = "inpTextDisabled"
            bank_name.Text = ""
            bank_name.Enabled = False
            bank_name.CssClass = "inpTextDisabled"
        End If
    End Sub
 
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Master\mstPerson.aspx")
        End If
        If checkPagePermission("~\Master\mstPerson.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Employee"
        Session("oid") = Request.QueryString("oid")
        Session("iOid") = Request.QueryString("iOid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-" : InitAllDDL() 
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"), Session("iOid"))
                Hiddenfield()
                TabContainer1.ActiveTabIndex = 1
                UpdatePanel3.Visible = True
            Else
                personoid.Text = GenerateID("QL_MSTPERSON", CompnyCode)
                TabContainer1.ActiveTabIndex = 0
                UpdatePanel3.Visible = False
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindPersonData(FilterText.Text)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click

        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        cbDiv.Checked = False
        cbDept.Checked = False
        FilterDDLDiv.SelectedIndex = -1
        If FilterDDLDiv.Items.Count > 0 Then
            InitDDLFilterDept(FilterDDLDiv.SelectedValue)
        End If
        FilterDDLDept.SelectedIndex = -1
        BindPersonData(FilterText.Text)        '
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnUpload.Click
        Dim fname As String = "ImgPerson" & IIf(personoid.Text = "", GenerateID("QL_mstperson", CompnyCode), personoid.Text).ToString & Format(GetServerTime(), "_yyyyMMddhhmmss") & ".jpg"
        uploadFileGambar("insert", fname, 80, 120, "~/Images/PersonImg/")
        lblpicloc.Text = fname
    End Sub

    Protected Sub btnUploadCard_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnUploadCard.Click
        Dim fname As String = "ImgIDCard" & IIf(personoid.Text = "", GenerateID("QL_mstperson", CompnyCode), personoid.Text).ToString & Format(GetServerTime(), "_yyyyMMddhhmmss") & ".jpg"
        uploadFileGambar2("insert", fname, 80, 50, "~/Images/CardImg/")
        cardpicture.Text = fname
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Master\mstPerson.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If personoid.Text = "" Then
            showMessage("Please select employee data first!", 1)
            Exit Sub
        End If
        
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_mstdept SET depttotalperson = depttotalperson - 1 WHERE deptoid=" & lastdept.Text & " AND divoid=" & lastdiv.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_mstdivision SET divtotalperson = divtotalperson - 1 WHERE divoid=" & lastdiv.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE QL_mstperson WHERE personoid=" & personoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            showMessage(ex.Message, 1)
            xCmd.Connection.Close()
            Exit Sub
        End Try
        Response.Redirect("~\Master\mstPerson.aspx?awal=true")
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() And Not IsDataExists() Then
            If Session("oid") = Nothing Or Session("oid") = "" Then
                personoid.Text = GenerateID("QL_MSTPERSON", CompnyCode)
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_mstperson (cmpcode, personoid, nip, personname, persontitleoid, personsex, maritalstatus, divisioid, deptoid, leveloid, cardno, cityofbirth, dateofbirth, curraddr, curraddrcityoid, origaddr, origaddrcityoid, personphone1, personphone2, emergencyphone, personnote, religionoid, cardpicture, personpicture, activeflag, createuser, createtime, upduser, updtime, salescode, komisi, payment_type, account_no, account_name, bank_name ) VALUES ('" & divcode.Text & "', " & personoid.Text & ", '" & Tchar(nip.Text.Trim) & "', '" & Tchar(personname.Text.Trim) & "', " & persontitleoid.SelectedValue & ", '" & personsex.SelectedValue & "', '" & maritalstatus.SelectedValue & "', " & divisioid.SelectedValue & ", " & deptoid.SelectedValue & ", " & leveloid.SelectedValue & ", '" & Tchar(cardno.Text.Trim) & "', '" & cityofbirth.SelectedItem.Text & "', '" & IIf(dateofbirth.Text = "", "01/01/1900", dateofbirth.Text) & "', '" & Tchar(curraddr.Text.Trim) & "', " & curraddrcityoid.SelectedValue & ", '" & Tchar(origaddr.Text.Trim) & "', " & origaddrcityoid.SelectedValue & ", '" & Tchar(personphone1.Text.Trim) & "', '" & Tchar(personphone2.Text.Trim) & "', '" & Tchar(emergencyphone.Text.Trim) & "', '" & Tchar(personnote.Text.Trim) & "', " & religionoid.SelectedValue & ", '" & cardpicture.Text & "', '" & lblpicloc.Text & "', '" & activeflag.SelectedValue & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & upduser.Text & "', CURRENT_TIMESTAMP, '" & Tchar(salesCode.Text) & "', " & ToDouble(komisi.Text) & ", '" & payment_type.SelectedValue & "', '" & Tchar(account_no.Text) & "', '" & Tchar(account_name.Text) & "', '" & Tchar(bank_name.Text) & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & personoid.Text & " WHERE tablename='QL_MSTPERSON' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_mstperson SET cmpcode='" & divcode.Text & "', nip='" & Tchar(nip.Text.Trim) & "', personname='" & Tchar(personname.Text.Trim) & "', persontitleoid=" & persontitleoid.SelectedValue & ", personsex='" & personsex.SelectedValue & "', maritalstatus='" & maritalstatus.SelectedValue & "', divisioid=" & divisioid.SelectedValue & ", deptoid=" & deptoid.SelectedValue & ", leveloid=" & leveloid.SelectedValue & ", cardno='" & Tchar(cardno.Text.Trim) & "', cityofbirth='" & cityofbirth.SelectedItem.Text & "', dateofbirth='" & IIf(dateofbirth.Text = "", "01/01/1900", dateofbirth.Text) & "', curraddr='" & Tchar(curraddr.Text.Trim) & "', curraddrcityoid=" & curraddrcityoid.SelectedValue & ", origaddr='" & Tchar(origaddr.Text.Trim) & "', origaddrcityoid=" & origaddrcityoid.SelectedValue & ", personphone1='" & Tchar(personphone1.Text.Trim) & "', personphone2='" & Tchar(personphone2.Text.Trim) & "', emergencyphone='" & Tchar(emergencyphone.Text.Trim) & "', personnote='" & Tchar(personnote.Text.Trim) & "', religionoid=" & religionoid.SelectedValue & ", cardpicture='" & cardpicture.Text & "', personpicture='" & lblpicloc.Text & "', activeflag='" & activeflag.SelectedValue & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, salescode='" & Tchar(salesCode.Text) & "', komisi=" & ToDouble(komisi.Text) & ", payment_type='" & Tchar(payment_type.SelectedValue) & "', account_no='" & Tchar(account_no.Text) & "', account_name='" & Tchar(account_name.Text) & "', bank_name='" & Tchar(bank_name.Text) & "' WHERE personoid=" & personoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    If lastflag.Text = "ACTIVE" Then
                        sSql = "UPDATE QL_mstdept SET depttotalperson = depttotalperson - 1 WHERE deptoid=" & lastdept.Text & " AND divoid=" & lastdiv.Text
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        sSql = "UPDATE QL_mstdivision SET divtotalperson = divtotalperson - 1 WHERE divoid=" & lastdiv.Text
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                End If
                If activeflag.SelectedValue = "ACTIVE" Then
                    sSql = "UPDATE QL_mstdept SET depttotalperson = ISNULL(depttotalperson, 0) + 1 WHERE deptoid=" & deptoid.SelectedValue & " AND divoid=" & divisioid.SelectedValue
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstdivision SET divtotalperson = ISNULL(divtotalperson, 0) + 1 WHERE divoid=" & divisioid.SelectedValue
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                xCmd.Connection.Close()
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 1)
                xCmd.Connection.Close()
                Exit Sub
            End Try
            Response.Redirect("~\Master\mstPerson.aspx?awal=true")
        End If
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub divisioid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles divisioid.SelectedIndexChanged
        If divisioid.Items.Count > 0 Then
            InitDDLDept(divisioid.SelectedValue)
            SetDivCode(divisioid.SelectedValue)
        End If
    End Sub

    Protected Sub FilterDDLDiv_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLDiv.SelectedIndexChanged
        If FilterDDLDiv.Items.Count > 0 Then
            InitDDLFilterDept(FilterDDLDiv.SelectedValue)
        End If
    End Sub

    Protected Sub leveloid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles leveloid.SelectedIndexChanged
        If leveloid.SelectedItem.Text.ToUpper = "SALES" Then
            tdKode1.Visible = True
            TDkode2.Visible = True
            TDkode3.Visible = True
            Label13.Visible = True
            Label14.Visible = True
            Label15.Visible = True
            komisi.Visible = True
        Else
            tdKode1.Visible = False
            TDkode2.Visible = False
            TDkode3.Visible = False
            Label13.Visible = False
            Label14.Visible = False
            Label15.Visible = False
            komisi.Visible = False
        End If
    End Sub
#End Region 

#Region "Event Save Salary"

    Protected Sub btnSaveSalary_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSaveSalary.Click
        Dim sMsg As String = ""
        'If Session("iOid").ToString <> "0" Then
        '    Dim salary_type As String = ""
        '    sSql = "select * from QL_r_salary where personoid=" & personoid.Text
        '    Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_r_salary")
        '    If dtTbl.Rows.Count > 0 Then
        '        For i As Int16 = 0 To dtTbl.Rows.Count - 1
        '            salary_type = dtTbl.Rows(i)("salary_type").ToString()
        '        Next
        '        If ddlperiodtype.SelectedValue.ToString() <> salary_type.ToString() Then
        '            sMsg &= "Maaf periode tipe tidak bisa di ganti karena sudah ada di perhitungan gaji dengan tipe " & salary_type.ToUpper() & "..! <br>"
        '        End If
        '    End If
        'End If

        If Session("iOid").ToString = "0" Then
            If IsPersonExists("input") = True Then
                sMsg &= "Data sudah terinput " & personname.Text.ToUpper() & " ..! <br>"
            End If
        End If

        If sMsg <> "" Then
            showMessage(sMsg, 3)
            Exit Sub
        Else
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("iOid").ToString = "0" Then
                    sSql = "INSERT INTO QL_mstsalary ( cmpcode, personoid, salary_premi_hadir, premi_hadir_acctgoid, salary_premi_extra, premi_extra_acctgoid, salary_gaji_bln, salary_gaji_bln_acctgoid, salary_harian_lk, salary_harian_lk_acctgoid, salary_harian_dk, salary_harian_dk_acctgoid, salary_bonus, salary_bonus_acctgoid, salary_lembur, salary_lembur_acctgoid, salary_lembur_lk, salary_lembur_lk_acctgoid, salary_tunjangan_jabatan, salary_tunjangan_acctgoid, createuser, createtime, salary_type, salary_gaji_harian_acctgoid, salary_gaji_harian, salary_kerajinan, salary_kerajinan_acctgoid, salary_other, salary_other_acctgoid ) " & _
                    "VALUES ( '" & CompnyCode & "', " & personoid.Text & ", " & ToDouble(salary_premi_hadir.Text) & ", " & premi_hadir_acctgoid.SelectedValue & ", " & ToDouble(salary_premi_extra.Text) & ", " & premi_extra_acctgoid.SelectedValue & ", " & ToDouble(salary_gaji_bln.Text) & ", " & salary_gaji_bln_acctgoid.SelectedValue & ", " & ToDouble(salary_harian_lk.Text) & ", " & salary_harian_lk_acctgoid.SelectedValue & ", " & ToDouble(salary_harian_dk.Text) & ", " & salary_harian_dk_acctgoid.SelectedValue & ", " & ToDouble(salary_bonus.Text) & ", " & salary_bonus_acctgoid.SelectedValue & ", " & ToDouble(salary_lembur.Text) & ", " & salary_lembur_acctgoid.SelectedValue & ", " & ToDouble(salary_lembur_lk.Text) & ", " & salary_lembur_lk_acctgoid.SelectedValue & ", " & ToDouble(salary_tunjangan_jabatan.Text) & ", " & salary_tunjangan_acctgoid.SelectedValue & ", '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & ddlperiodtype.SelectedValue & "', " & salary_gaji_harian_acctgoid.SelectedValue & ", " & ToDouble(salary_gaji_harian.Text) & ", " & ToDouble(salary_kerajinan.Text) & ", " & salary_kerajinan_acctgoid.SelectedValue & ", " & ToDouble(salary_other.Text) & ", " & salary_other_acctgoid.SelectedValue & ") "
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_mstsalary SET salary_premi_hadir = " & ToDouble(salary_premi_hadir.Text) & ", premi_hadir_acctgoid = " & premi_hadir_acctgoid.SelectedValue & ", salary_premi_extra = " & ToDouble(salary_premi_extra.Text) & ", premi_extra_acctgoid = " & premi_extra_acctgoid.SelectedValue & ", salary_gaji_bln = " & ToDouble(salary_gaji_bln.Text) & ", salary_gaji_bln_acctgoid = " & salary_gaji_bln_acctgoid.SelectedValue & ", salary_harian_lk = " & ToDouble(salary_harian_lk.Text) & ", salary_harian_lk_acctgoid = " & salary_harian_lk_acctgoid.Text & ", salary_harian_dk = " & ToDouble(salary_harian_dk.Text) & ", salary_harian_dk_acctgoid = " & salary_harian_dk_acctgoid.SelectedValue & ", salary_bonus = " & ToDouble(salary_bonus.Text) & ", salary_bonus_acctgoid = " & salary_bonus_acctgoid.SelectedValue & ", salary_lembur = " & ToDouble(salary_lembur.Text) & ", salary_lembur_acctgoid = " & salary_lembur_acctgoid.SelectedValue & ", salary_lembur_lk = " & ToDouble(salary_lembur_lk.Text) & ", salary_lembur_lk_acctgoid = " & salary_lembur_lk_acctgoid.SelectedValue & ", salary_tunjangan_jabatan=" & ToDouble(salary_tunjangan_jabatan.Text) & ", salary_tunjangan_acctgoid=" & salary_tunjangan_acctgoid.SelectedValue & ", salary_type='" & ddlperiodtype.SelectedValue & "', salary_gaji_harian=" & ToDouble(salary_gaji_harian.Text) & ", salary_gaji_harian_acctgoid=" & salary_gaji_harian_acctgoid.SelectedValue & ", salary_kerajinan = " & ToDouble(salary_kerajinan.Text) & ", salary_kerajinan_acctgoid = " & salary_kerajinan_acctgoid.SelectedValue & ", salary_other = " & ToDouble(salary_other.Text) & ", salary_other_acctgoid = " & salary_other_acctgoid.SelectedValue & " WHERE salaryoid = " & salaryoid.Text & " AND personoid = " & personoid.Text & ""
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                xCmd.Connection.Close()
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message & sSql, 1)
                xCmd.Connection.Close()
                Exit Sub
            End Try
            Response.Redirect("~\Master\mstPerson.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnCancelSalary_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelSalary.Click
        Response.Redirect("~\Master\mstPerson.aspx?awal=true")
    End Sub

    Protected Sub btnDeleteSalary_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDeleteSalary.Click
        Dim sMsg As String = ""
        If Session("iOid").ToString = "0" Then
            If IsPersonExists("hapus") = True Then
                sMsg &= "Data sudah terinput di perhitungan gaji..! <br>"
            End If
        End If

        If personoid.Text = "" Then
            sMsg &= "Please select employee data first! <br>"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, 3)
            Exit Sub
        Else
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                sSql = "Delete QL_mstsalary where salaryoid=" & salaryoid.Text
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                objTrans.Commit()
                xCmd.Connection.Close()
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 1)
                xCmd.Connection.Close()
                Exit Sub
            End Try
            Response.Redirect("~\Master\mstPerson.aspx?awal=true")
        End If
    End Sub

    Protected Sub salary_gaji_bln_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles salary_gaji_bln.TextChanged
        If sender.Equals(salary_premi_hadir) Then
            salary_premi_hadir.Text = ToMaskEdit(ToDouble(salary_premi_hadir.Text), 2)
        ElseIf sender.Equals(salary_premi_extra) Then
            salary_premi_extra.Text = ToMaskEdit(ToDouble(salary_premi_extra.Text), 2)
        ElseIf sender.Equals(salary_gaji_bln) Then
            salary_gaji_bln.Text = ToMaskEdit(ToDouble(salary_gaji_bln.Text), 2)
        ElseIf sender.Equals(salary_harian_lk) Then
            salary_harian_lk.Text = ToMaskEdit(ToDouble(salary_harian_lk.Text), 2)
        ElseIf sender.Equals(salary_harian_dk) Then
            salary_harian_dk.Text = ToMaskEdit(ToDouble(salary_harian_dk.Text), 2)
        ElseIf sender.Equals(salary_lembur) Then
            salary_lembur.Text = ToMaskEdit(ToDouble(salary_lembur.Text), 2)
        ElseIf sender.Equals(salary_lembur_lk) Then
            salary_lembur_lk.Text = ToMaskEdit(ToDouble(salary_lembur_lk.Text), 2)
        ElseIf sender.Equals(salary_bonus) Then
            salary_bonus.Text = ToMaskEdit(ToDouble(salary_bonus.Text), 2)
        ElseIf sender.Equals(salary_tunjangan_jabatan) Then
            salary_tunjangan_jabatan.Text = ToMaskEdit(ToDouble(salary_tunjangan_jabatan.Text), 2) 
        End If
    End Sub

    Protected Sub payment_type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles payment_type.SelectedIndexChanged
        Hiddenfield()
    End Sub

#End Region
End Class
