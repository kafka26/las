Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports Microsoft.VisualBasic

Partial Class Master_mstitem
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public MaterialImageUrl As String = "~/Images/MatImages/"
    Public MaterialImageUrl1 As String = "~/Images/MatImages1/"
    Public MaterialImageUrl3 As String = "~/Images/MatImages3/"
    Public MaterialImageUrl4 As String = "~/Images/MatImages4/"
    Public MaterialImageUrl5 As String = "~/Images/MatImages5/"
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim CProc As New ClassProcedure
    Dim report As New ReportDocument
    Dim folderReport As String = "~/Report/"
#End Region

#Region "Procedure"

    Private Sub showMessage(ByVal message As String, ByVal caption As String)
        Validasi.Text = message
        lblCaption.Text = caption
        Image1.ImageUrl = "~/Images/warn.png"
        btnErrOK.ImageUrl = "~/Images/ok.png"
        PanelValidasi.Visible = True
        ButtonExtendervalidasi.Visible = True
        ModalPopupExtenderValidasi.Show()
    End Sub

    Private Sub showImage(ByVal imageUrl As String)
        'Validasi.Text = ""
        'lblCaption.Text = "Image Full View"
        'Image1.ImageUrl = imageUrl
        'btnErrOK.ImageUrl = "~/Images/ok.png"
        'PanelValidasi.Visible = True
        'ButtonExtendervalidasi.Visible = True
        'ModalPopupExtenderValidasi.Show()

    End Sub

    Public Sub BindData(ByVal sCompCode As String)
        If sCompCode <> "" Then
            sSql = "SELECT i.cmpcode, itemoid, itemType, itemgroupoid, itemGroup, itemCat1, itemCat2, itemCat3, itemCat4, itemCode, itemoldcode, itemShortDescription, itemLongDescription, itemBarcode, itemUnit1, itemUnit2, itemUnit3,Unit2Unit1Conversion, Unit3Unit1Conversion, itemMinStock, itemMaxStock, itemRecordStatus, itemNote1,itemNote2,itemNote3,itemMinPrice, itemMaxPrice, itemPriceList, itemLastSalesPrice,itemLastAverageValue,i.CreateUser,i.CreateTime,i.updUser,i.updTime, itemGroup AS gendesc, 0.00 AS pricepercentage, suppname  FROM QL_mstitem i INNER JOIN QL_mstgen g ON g.genoid=i.itemgroupoid LEFT JOIN QL_mstsupp s ON s.suppoid=i.suppoid WHERE " & FilterDDL.SelectedValue & " like '%" & Tchar(FilterText.Text) & "%'"
        End If

        sSql &= " AND itemType='" & FilterType.SelectedValue & "'"
        If Chkcat1.Checked Then
            sSql &= IIf(FilterGroup.SelectedValue = "ALL", "", " AND itemCat1= " & DDLFilterCat1.SelectedValue & "")
        End If
        If Chkcat2.Checked Then
            sSql &= IIf(FilterGroup.SelectedValue = "ALL", "", " AND itemCat2= " & DDLFilterCat2.SelectedValue & "")
        End If
        If chkGroup.Checked Then
            sSql &= IIf(FilterGroup.SelectedValue = "ALL", "", " AND itemGroup= '" & FilterGroup.SelectedValue & "'")
        End If
        If checkPagePermission("~\Master\mstitem.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND i.createuser='" & Session("UserID") & "'"
        End If
        sSql &= " ORDER BY itemcode DESC"
        Dim dtData As DataTable = cKon.ambiltabel(sSql, "QL_mstitem")
        Dim dPercent As Double = 0
        For C1 As Integer = 0 To dtData.Rows.Count - 1
            'dtData.Rows(C1)("pricepercentage") = CalculatePricePercentage2(dtData.Rows(C1)("itemoid").ToString, dPercent)
        Next
        dtData.AcceptChanges()
        Session("TblMst") = dtData
        GVmstmat.DataSource = Session("TblMst")
        GVmstmat.DataBind()
    End Sub

    Public Sub InitAllDDL()
        If matinvtype.SelectedValue = "INV" Then
            sSql = "select gencode,gendesc from QL_mstgen where gengroup='GROUPITEM' and activeflag='ACTIVE' order by gendesc"
            FillDDL(matgroup, sSql)
        Else
            sSql = "select gencode,gendesc from QL_mstgen where gengroup='ASSETTYPE' and activeflag='ACTIVE' order by gendesc"
            FillDDL(matgroup, sSql)
        End If
        If FilterType.SelectedValue = "INV" Then
            sSql = "select gencode,gendesc from QL_mstgen where gengroup='GROUPITEM' and activeflag='ACTIVE' order by gendesc"
            FillDDL(FilterGroup, sSql)
        Else
            sSql = "select gencode,gendesc from QL_mstgen where gengroup='ASSETTYPE' and activeflag='ACTIVE' order by gendesc"
            FillDDL(FilterGroup, sSql)
        End If
        sSql = "select genoid,gendesc from QL_mstgen where gengroup='UNIT' and activeflag='ACTIVE' order by gendesc"
        FillDDL(matunitoid1, sSql)
        'GET LIST OF BRAND
        sSql = "select genoid,gendesc from QL_mstgen where gengroup='BRAND' and activeflag='ACTIVE' order by gendesc"
        FillDDLWithAdditionalText(merk, sSql, "-", 0)
        'Fill DDL Model
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE gengroup='MODEL 2' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDLWithAdditionalText(DDLModel, sSql, "NONE", 0)
        ' Fill DDL Supplier
        sSql = "SELECT suppoid, suppname FROM QL_mstsupp WHERE activeflag='ACTIVE' ORDER BY suppname"
        FillDDLWithAdditionalText(suppoid, sSql, "NONE", 0)
        'GET LIST OF BAHAN
        sSql = "select genoid,gendesc from QL_mstgen where gengroup='BAHAN' and activeflag='ACTIVE' order by gendesc"
        FillDDLWithAdditionalText(ddlJenisBahan, sSql, "-", 0)
    End Sub

    Private Sub InitDDLCat1()
        'Fill DDL Category1
        sSql = "SELECT cat1oid, cat1code+' - '+cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat1res1='" & matgroup.SelectedValue & "' order by cat1shortdesc"
        FillDDL(Cat1, sSql)
        If Cat1.Items.Count > 0 Then
            InitDDLCat2()
        Else
            Cat2.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat2()
        'Fill DDL Category2
        sSql = "SELECT cat2oid, case when cat2res2<>'' then (cat2res2+cat2code+' - '+cat2shortdesc) else (cat2code+' - '+cat2shortdesc) end FROM QL_mstcat2 cat2 INNER JOIN QL_mstcat1 cat1 on cat1.cat1oid=cat2.cat1oid WHERE cat2.cmpcode='" & CompnyCode & "' AND cat2.cat1oid=" & Cat1.SelectedValue & " AND cat2.activeflag='ACTIVE' order by cat2shortdesc"
        FillDDL(Cat2, sSql)
        If Cat2.Items.Count > 0 Then
            InitDDLCat3()
        Else
            cat3.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat3()
        'Fill DDL Category2
        sSql = "SELECT cat3oid, case when cat3res2<>'' then (cat3res2+cat3code+' - '+cat3shortdesc) else (cat3code+' - '+cat3shortdesc) end FROM QL_mstcat3 cat2 INNER JOIN QL_mstcat2 cat1 on cat1.cat2oid=cat2.cat2oid WHERE cat2.cmpcode='" & CompnyCode & "' AND cat2.cat2oid=" & Cat2.SelectedValue & " AND cat2.cat1oid=" & Cat1.SelectedValue & " AND cat2.activeflag='ACTIVE' order by cat3shortdesc"
        FillDDLWithAdditionalText(cat3, sSql, "NONE", 0)
    End Sub

    Private Sub InitFilterDDLCat1()
        'Fill DDL Category1
        sSql = "SELECT cat1oid, cat1code+' - '+cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat1res1='" & FilterGroup.SelectedValue & "' order by cat1shortdesc"
        FillDDL(DDLFilterCat1, sSql)
        If DDLFilterCat1.Items.Count > 0 Then
            InitFilterDDLCat2()
        Else
            DDLFilterCat2.Items.Clear()
        End If
    End Sub

    Private Sub InitFilterDDLCat2()
        'Fill DDL Category2
        sSql = "SELECT cat2oid, cat2code+' - '+cat2shortdesc FROM QL_mstcat2 cat2 INNER JOIN QL_mstcat1 cat1 on cat1.cat1oid=cat2.cat1oid AND cat1res1='" & FilterGroup.SelectedValue & "' WHERE cat2.cmpcode='" & CompnyCode & "' AND cat2.cat1oid=" & DDLFilterCat1.SelectedValue & " AND cat2res1='" & FilterGroup.SelectedValue & "' AND cat2.activeflag='ACTIVE' order by cat2shortdesc"
        FillDDL(DDLFilterCat2, sSql)
    End Sub

    Public Sub FillTextBox(ByVal vMatoid As String)
        sSql = "SELECT cmpcode, itemoid, itemType, itemGroup, itemCat1, itemCat2, itemCat3, itemCat4, backupcode+itemlastcode AS backupcode, itemcode, itemoldcode,itemShortDescription, itemLongDescription, itemBarcode, itemUnit1, itemUnit2, itemUnit3,Unit2Unit1Conversion, Unit3Unit1Conversion, itemMinStock, itemMaxStock, itemRecordStatus, itemNote1, itemNote2, itemNote3, itemMinPrice, itemMaxPrice, itemPriceList, roundQty, pictureItem1, pictureItem2, pictureItem3, pictureItem4, pictureItem5,itemLastSalesPrice, itemLastAverageValue, CreateUser,CreateTime, updUser, updTime, unitprice, itembrand, modeloid, suppoid, pictureItem1desc, pictureItem2desc, pictureItem3desc, pictureItem4desc, pictureItem5desc, (select dbo.getUploadFileName(pictureItem1)) lblPic1, (select dbo.getUploadFileName(pictureItem2)) lblPic2, (select dbo.getUploadFileName(pictureItem3)) lblPic3, (select dbo.getUploadFileName(pictureItem4)) lblPic4, (select dbo.getUploadFileName(pictureItem5)) lblPic5, jenis_bahan_id FROM QL_mstitem where cmpcode='" & CompnyCode & "' and itemoid = '" & vMatoid & "'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        While xreader.Read
            matoid.Text = Trim(xreader("itemoid").ToString)
            CodeItem.Text = Trim(xreader("itemcode").ToString)
            backupcode.Text = Trim(xreader("backupcode").ToString)
            oldcode.Text = Trim(xreader("itemoldcode").ToString)
            matinvtype.Text = Trim(xreader("itemType").ToString)
            cekType()
            matgroup.SelectedValue = Trim(xreader("itemGroup").ToString)
            If Trim(xreader("itemType").ToString) = "INV" Then
                InitDDLCat1()
                Cat1.Text = Trim(xreader("itemCat1").ToString)
                InitDDLCat2()
                Cat2.Text = Trim(xreader("itemCat2").ToString)
                InitDDLCat3()
                cat3.Text = Trim(xreader("itemCat3").ToString)
            Else
                InitAllDDL()
                InitDDLCat1()
            End If
            matlongdesc.Text = Trim(xreader("itemLongDescription").ToString)
            matunitoid1.Text = Trim(xreader("itemUnit1").ToString)
            note1.Text = Trim(xreader("itemNote1").ToString)
            matpicture.ImageUrl = Trim(xreader("pictureItem1").ToString)
            matpicture1.ImageUrl = Trim(xreader("pictureItem2").ToString)
            matpicture3.ImageUrl = Trim(xreader("pictureItem3").ToString)
            matpicture4.ImageUrl = Trim(xreader("pictureItem4").ToString)
            matpicture5.ImageUrl = Trim(xreader("pictureItem5").ToString)

            lblpictureloc.Text = Trim(xreader("pictureItem1").ToString)
            lblpictureloc1.Text = Trim(xreader("pictureItem2").ToString)
            lblpictureloc3.Text = Trim(xreader("pictureItem3").ToString)
            lblpictureloc4.Text = Trim(xreader("pictureItem4").ToString)
            lblpictureloc5.Text = Trim(xreader("pictureItem5").ToString)

            pictureItem1desc.Text = Trim(xreader("pictureItem1desc").ToString)
            pictureItem2desc.Text = Trim(xreader("pictureItem2desc").ToString)
            pictureItem3desc.Text = Trim(xreader("pictureItem3desc").ToString)
            pictureItem4desc.Text = Trim(xreader("pictureItem4desc").ToString)
            pictureItem5desc.Text = Trim(xreader("pictureItem5desc").ToString)
            Price.Text = ToMaskEdit(Trim(xreader("itemPriceList").ToString), 2)
            minprice.Text = ToMaskEdit(Trim(xreader("itemMinPrice").ToString), 2)
            ddlStatus.SelectedValue = xreader("itemRecordStatus")
            merk.SelectedValue = xreader("itembrand")
            ddlJenisBahan.SelectedValue = xreader("jenis_bahan_id")
            DDLModel.SelectedValue = xreader("modeloid")
            createuser.Text = Trim(xreader("createUser").ToString)
            createtime.Text = CDate(Trim(xreader("createTime").ToString)).ToString("dd/MM/yyyy HH:mm:ss tt")
            Upduser.Text = Trim(xreader("updUser").ToString)
            Updtime.Text = CDate(Trim(xreader("updTime").ToString)).ToString("dd/MM/yyyy HH:mm:ss tt")
            suppoid.SelectedValue = xreader("suppoid")

            lblPic1.Text = Trim(xreader("lblPic1").ToString)
            lblPic2.Text = Trim(xreader("lblPic2").ToString)
            lblPic3.Text = Trim(xreader("lblPic3").ToString)
            lblPic4.Text = Trim(xreader("lblPic4").ToString)
            lblPic5.Text = Trim(xreader("lblPic5").ToString)
        End While
        xreader.Close()
        conn.Close()
        CalculatePricePercentage()
    End Sub

    Public Sub GenerateGenID()
        matoid.Text = GenerateID("QL_mstitem", CompnyCode)
        'CodeItem.Text = matoid.Text
    End Sub

    Private Sub generateCode()
        If matinvtype.SelectedValue = "INV" Then
            sSql = "SELECT cat1code FROM QL_mstcat1 WHERE cat1oid=" & Cat1.SelectedValue & ""
            Dim g1 As String = GetStrData(sSql)
            sSql = "SELECT cat2res2 FROM QL_mstcat2 WHERE cat2oid=" & Cat2.SelectedValue & ""
            Dim g2 As String = GetStrData(sSql)
            sSql = "SELECT cat2code FROM QL_mstcat2 WHERE cat2oid=" & Cat2.SelectedValue & ""
            Dim g3 As String = GetStrData(sSql)
            Dim g4 As String = "0"
            If matgroup.SelectedValue.ToString.ToUpper = "RAW" Then
                g4 = ""
            End If
            sSql = "SELECT cat3code FROM QL_mstcat3 WHERE cat3oid=" & cat3.SelectedValue & ""
            If Not String.IsNullOrEmpty(GetStrData(sSql)) Then
                g4 = GetStrData(sSql)
            Else
				If matgroup.SelectedValue.ToString.ToUpper = "FG" Then
					g4 = "0"
				End If
            End If

            sSql = "SELECT itemmidcode FROM QL_mstitemmidcode WHERE cat1code='" & g1 & "' and cat2code='" & g2 & g3 & "' --AND cat3code='" & g4 & "'"
            Dim gmid As String = GetStrData(sSql)
            If gmid = "" Then
                gmid = "0"
            End If

            Dim sNo As String = ""
            If g1 <> "" Then
                sNo = g1 & gmid
            End If
            If g2 <> "" Then
                sNo &= g2
            End If
            If matgroup.SelectedValue.ToString.ToUpper = "FG" Then
                If g3 <> "" Then
                    sNo &= g3
                End If
            End If
            Dim sLastCode As String = ""
            If matgroup.SelectedValue.ToString.ToUpper = "RAW" Then
                If g3 <> "" Then
                    sLastCode &= g3
                End If
            End If
            If g4 <> "" Then
                sLastCode &= g4
            End If
            Dim sCounter As String = "1"
            If matlongdesc.Text <> "" Then
                If ToDouble(GetStrData("SELECT COUNT(*) FROM QL_mstitem i WHERE REPLACE(itemshortdescription, ' ','')=REPLACE('" & Tchar(matlongdesc.Text) & "', ' ','')")) > 0 Then
                    sSql = "SELECT ISNULL(MAX(CAST(SUBSTRING(itemCode, 7,3) AS INTEGER)), 1) AS IDNEW FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemgroup='FG' AND itemCode LIKE '" & sNo & "%' AND REPLACE(itemshortdescription, ' ', '')=REPLACE('" & Tchar(matlongdesc.Text) & "', ' ','')"
                    sCounter = GetStrData(sSql)
                Else
                    sSql = "SELECT ISNULL(MAX(CAST(SUBSTRING(itemCode, 7,3) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemgroup='FG' AND itemCode LIKE '" & sNo & "%' "
                    sCounter = GetStrData(sSql)
                End If
                If matgroup.SelectedValue.ToString.ToUpper = "RAW" Then
                    sSql = "SELECT ISNULL(MAX(CAST(SUBSTRING(itemCode, 5,3) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemCode LIKE '" & sNo & "___" & sLastCode & "%'"
                    sCounter = GetStrData(sSql)
                End If
                CodeItem.Text = GenNumberString(sNo, "", sCounter, 3)
                If matgroup.SelectedValue.ToString.ToUpper = "RAW" Then
                    If g3 <> "" Then
                        CodeItem.Text &= g3
                    End If
                End If
                If g4 <> "" Then
                    CodeItem.Text &= g4
                End If
            End If
            'sSql = "SELECT ISNULL(MAX(CAST(RIGHT(backupcode, 3) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND backupcode LIKE '" & sNo & "%'"
            'CodeItem.Text = GenNumberString(sNo, "", GetStrData(sSql), 3)

            backupcode.Text = CodeItem.Text
            CodeItem.CssClass = "inpTextDisabled"
            CodeItem.Enabled = False
            'matlongdesc.Text = ""
        Else
            Dim sNo As String = "ITEM-"
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(itemcode, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemcode LIKE '" & sNo & "%'"
            backupcode.Text = GenNumberString(sNo, "", cKon.ambilscalar(sSql), 4)
            CodeItem.Text = backupcode.Text
            CodeItem.CssClass = "inpTextDisabled"
            CodeItem.Enabled = False
        End If
    End Sub

    Private Sub cekType()
        If matinvtype.SelectedValue = "INV" Then
            If i_u.Text = "New Data" Then
                Cat1.Enabled = True
                Cat1.CssClass = "inpText"
                Cat2.Enabled = True
                Cat2.CssClass = "inpText"
            End If
        Else
            Cat1.Enabled = False
            Cat1.CssClass = "inpTextDisabled"
            Cat2.Enabled = False
            Cat2.CssClass = "inpTextDisabled"
        End If
    End Sub

    Private Sub clearItem()
        createuser.Text = Session("UserID")
        createtime.Text = Date.Now.ToString("dd/MM/yyyy HH:mm:ss tt")
        Upduser.Text = Session("UserID")
        Updtime.Text = Date.Now.ToString("dd/MM/yyyy HH:mm:ss tt")
    End Sub

    Private Sub CalculatePricePercentage()
        Dim dPercent As Double = 0, dStartValue As Double = 0, dEndValue As Double = 0, dBalanceValue As Double = 0
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        If Session("oid_mstmat") <> Nothing Or Session("oid_mstmat") <> "" Then
            dStartValue = ToDouble(GetStrData("select dbo.getStockValue(" & Session("oid_mstmat") & ")"))
        End If
        If dStartValue > 0 Then
            dEndValue = ToMaskEdit(ToDouble(Price.Text) / dStartValue, 2)
        End If
        If dEndValue > 0 Then
            dBalanceValue = dEndValue * 100
        End If
        If dBalanceValue > 0 Then
            dPercent = dBalanceValue - 100
        End If
        pricepercentage.Text = dPercent
    End Sub

    Private Function CalculatePricePercentage2(ByVal sOid As String, ByRef dPercent As Double)
        Dim dStartValue As Double = 0, dEndValue As Double = 0, dBalanceValue As Double = 0
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        If sOid <> Nothing Or sOid <> "" Then
            dStartValue = ToDouble(GetStrData("select dbo.getStockValue(" & sOid & ")"))
        End If
        If dStartValue > 0 Then
            dEndValue = ToMaskEdit(ToDouble(Price.Text) / dStartValue, 2)
        End If
        If dEndValue > 0 Then
            dBalanceValue = dEndValue * 100
        End If
        If dBalanceValue > 0 Then
            dPercent = dBalanceValue - 100
        End If
        Return dPercent
    End Function

#End Region

#Region "Function"

    Private Function GenerateMatCode(ByVal codecat As String, ByVal codesubcat As String, ByVal codesubdtl As String) As String
        'Update:30.06.2010; disabled generatematcode, karena matcode tidak terpakai. sementara ubah jadi matoid
        GenerateMatCode = matoid.Text
        Return GenerateMatCode
    End Function

    Private Function GetStockValue(ByVal sOid As String, Optional ByVal sType As String = "IDR") As Double
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        Try
            sSql = "SELECT SUM(ISNULL(stockqty, 0) * ISNULL(stockvalue" & sType.ToLower & ", 0)) / SUM(ISNULL(stockqty, 0)) FROM QL_stockvalue WHERE cmpcode='" & CompnyCode & "' AND periodacctg IN ('" & sPeriod & "', '" & GetLastPeriod(sPeriod) & "') AND refoid=" & sOid & " AND closeflag=''"
            GetStockValue = ToDouble(GetStrData(sSql))
        Catch ex As Exception
            GetStockValue = 0
        End Try
    End Function

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx?target=~/Master/mstitem.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("mstitem.aspx")
        End If
        If checkPagePermission("~\Master\mstitem.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        Page.Title = CompnyName & " - Item/Material Data"
        Session("oid_mstmat") = Request.QueryString("oid")
        Me.BtnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');")

        If Not Page.IsPostBack Then
            matinvtype_SelectedIndexChanged(Nothing, Nothing)
            matgroup_SelectedIndexChanged(Nothing, Nothing)
            InitAllDDL()
            InitDDLCat1()
            InitFilterDDLCat1()
            If Session("oid_mstmat") IsNot Nothing And Session("oid_mstmat") <> "" Then
                i_u.Text = "Update Data"
                BtnDelete.Visible = True
                btnNew.Visible = False

                matgroup.Enabled = False
                matgroup.CssClass = "inpTextDisabled"
                matinvtype.Enabled = False
                matinvtype.CssClass = "inpTextDisabled"
                Cat1.Enabled = False
                Cat1.CssClass = "inpTextDisabled"
                Cat2.Enabled = False
                Cat2.CssClass = "inpTextDisabled"
                FillTextBox(Session("oid_mstmat"))
                TabContainer1.ActiveTabIndex = 1
                matlongdesc.CssClass = "inpTextDisabled" : matlongdesc.Enabled = False
                If cKon.ambilscalar("select count(*) from QL_trnpodtl where matoid=" & matoid.Text) > 0 Then
                    Price.Enabled = True
                    Price.CssClass = "inpText"
                End If
            Else
                i_u.Text = "New Data"
                GenerateGenID()
                'generateCode()
                BtnDelete.Visible = False
                Upduser.Text = Session("UserID")
                Updtime.Text = Date.Now.ToString("dd/MM/yyyy HH:mm:ss tt")
                TabContainer1.ActiveTabIndex = 0
            End If
            generateCode()
            Link_1.Target = "_blank" : Link_2.Target = "_blank" : Link_3.Target = "_blank" : Link_4.Target = "_blank" : Link_5.Target = "_blank"
            If Session("uploadImage_mstmat") IsNot Nothing Then
                Link_1.NavigateUrl = MaterialImageUrl & "temp"
            Else
                Link_1.NavigateUrl = lblpictureloc.Text
            End If
            If Session("uploadImage_mstmat1") IsNot Nothing Then
                Link_2.NavigateUrl = MaterialImageUrl1 & "temp"
            Else
                Link_2.NavigateUrl = lblpictureloc1.Text
            End If
            If Session("uploadImage_mstmat3") IsNot Nothing Then
                Link_3.NavigateUrl = MaterialImageUrl3 & "temp"
            Else
                Link_3.NavigateUrl = lblpictureloc3.Text
            End If
            If Session("uploadImage_mstmat4") IsNot Nothing Then
                Link_4.NavigateUrl = MaterialImageUrl4 & "temp"
            Else
                Link_4.NavigateUrl = lblpictureloc4.Text
            End If
            If Session("uploadImage_mstmat5") IsNot Nothing Then
                Link_5.NavigateUrl = MaterialImageUrl5 & "temp"
            Else
                Link_5.NavigateUrl = lblpictureloc5.Text
            End If
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindData(CompnyCode)
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        FilterText.Text = ""
        chkGroup.Checked = False
        Chkcat1.Checked = False
        Chkcat2.Checked = False
        BindData(CompnyCode)
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        clearItem()
        Session.Remove("uploadImage_mstmat")
        Session.Remove("uploadImage_mstmat1")
        Session.Remove("uploadImage_mstmat3")
        Session.Remove("uploadImage_mstmat4")
        Session.Remove("uploadImage_mstmat5")
        Response.Redirect("~\Master\mstitem.aspx?awal=true")
    End Sub

    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnDelete.Click
        sSql = "SELECT ta.name,col.name colum FROM sys.tables ta INNER JOIN sys.columns col ON col.object_id=ta.object_id WHERE col.name IN ('itemoid', 'matoid') AND ta.name NOT IN ('QL_mstitem')"
        Dim objTblUsage As DataTable = cKon.ambiltabel(sSql, "QL_tables")
        For C1 As Integer = 0 To objTblUsage.Rows.Count - 1
            If CheckDataExists("SELECT COUNT(*) FROM " & objTblUsage.Rows(C1).Item("name").ToString & " WHERE " & objTblUsage.Rows(C1).Item("colum").ToString & "=" & matoid.Text) Then
                showMessage("This data can't be Deleted because it is being used by another data!", "WARNING")
                Exit Sub
            End If
        Next

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            'cek table crdstock
            If CheckDataExists("SELECT crdstockoid FROM QL_crdstock WHERE refoid =" & matoid.Text) = True Then
                showMessage("ITEM TIDAK DAPAT DIHAPUS", "WARNING")
                Exit Sub
            End If
            'cek table crdhpp
            If CheckDataExists("SELECT hppoid FROM QL_crdhpp WHERE refoid =" & matoid.Text) = True Then
                showMessage("ITEM TIDAK DAPAT DIHAPUS", "WARNING")
                Exit Sub
            End If
            'cek table podtl
            If CheckDataExists("SELECT pomstoid FROM QL_trnpodtl WHERE matoid =" & matoid.Text) = True Then
                showMessage("ITEM TIDAK DAPAT DIHAPUS", "WARNING")
                Exit Sub
            End If
            'cek table mrdtl
            If CheckDataExists("SELECT mrmstoid FROM QL_trnmrdtl WHERE matoid =" & matoid.Text) = True Then
                showMessage("ITEM TIDAK DAPAT DIHAPUS", "WARNING")
                Exit Sub
            End If
            'cek table mstitemprice
            If CheckDataExists("SELECT itempriceoid FROM QL_mstitemprice WHERE itemoid =" & matoid.Text) = True Then
                showMessage("ITEM TIDAK DAPAT DIHAPUS", "WARNING")
                Exit Sub
            End If

            sSql = "DELETE QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemoid=" & matoid.Text & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Session.Remove("oid_mstmat")
        Session.Remove("uploadImage_mstmat")
        Session.Remove("uploadImage_mstmat1")
        Response.Redirect("~\Master\mstitem.aspx?awal=true")
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim errMessage As String = ""
        If matoid.Text.Trim = "" Then
            errMessage &= "Please Fill Item ID!<BR>"
        End If
        If matlongdesc.Text.Trim = "" Then
            errMessage &= "Please Fill Description Field!<BR>"
        End If
        If matunitoid1.SelectedItem.Text = "SELECT UNIT 1" Then
            errMessage &= "Please Select Unit 1!<BR>"
        End If
        If note1.Text <> "" Then
            If Tchar(note1.Text.Length) > 100 Then
                errMessage &= "Maximum 100 characters for note !<BR>"
            End If
        End If
        If matlongdesc.Text <> "" Then
            If Tchar(matlongdesc.Text.Length) > 1000 Then
                errMessage &= "Maximum 100 characters for description!<BR>"
            End If
        End If
        'cek code matcode [QL_mstgen] yang kembar (code tidak boleh kembar)
        Dim sSqlCheck As String = "SELECT COUNT(-1) FROM ql_mstitem WHERE itemCode = '" & Tchar(CodeItem.Text.Trim) & "'"
        If Session("oid_mstmat") IsNot Nothing And Session("oid_mstmat") <> "" Then
            sSqlCheck &= " AND itemoid <> " & matoid.Text
        End If
        If cKon.ambilscalar(sSqlCheck) > 0 Then
            showMessage("Code already used!", "WARNING")
            Exit Sub
        End If
        If matgroup.SelectedItem.Text = "FINISH GOOD" Then
            If DDLModel.SelectedValue = 0 Then
                errMessage &= "Silakan pilih model terlebih dahulu!<BR>"
            End If
            If cat3.SelectedValue = 0 Then
                errMessage &= "Silakan pilih kategori 3 terlebih dahulu!<BR>"
            End If
        End If
        If oldcode.Text <> "" And matgroup.SelectedItem.Text = "RAW MATERIAL" Then
            Dim sSqlCheck1 As String = "SELECT COUNT(-1) FROM ql_mstitem WHERE itemoldcode = '" & oldcode.Text & "'"
            If Session("oid_mstmat") IsNot Nothing And Session("oid_mstmat") <> "" Then
                sSqlCheck1 &= " AND itemoid <> " & matoid.Text.Trim
            End If
            If cKon.ambilscalar(sSqlCheck1) > 0 Then
                errMessage &= "Old Code" & oldcode.Text & " sudah dipakai di kode material lain, silakan cek kembali!<br>"
            End If

        End If
        If errMessage <> "" Then
            showMessage(errMessage, "WARNING")
            Exit Sub
        End If
        Dim sMidCodeOid As Int32 = GenerateID("QL_MSTITEMMIDCODE", CompnyCode)
        'Get code for change middle code
        sSql = "SELECT cat1code FROM QL_mstcat1 WHERE cat1oid=" & Cat1.SelectedValue & ""
        Dim sCode1 As String = GetStrData(sSql)
        sSql = "SELECT cat2res2 FROM QL_mstcat2 WHERE cat2oid=" & Cat2.SelectedValue & ""
        Dim sCodeRes2 As String = GetStrData(sSql)
        sSql = "SELECT cat2code FROM QL_mstcat2 WHERE cat2oid=" & Cat2.SelectedValue & ""
        Dim sCOde2 As String = GetStrData(sSql)
        'Dim sCode3 As String = 0
        'If matgroup.SelectedValue.ToString.ToUpper = "RAW" Then
        '    sCode3 = ""
        'End If
        'sSql = "SELECT cat3code FROM QL_mstcat3 WHERE cat3oid=" & cat3.SelectedValue & ""
        'If Not String.IsNullOrEmpty(GetStrData(sSql)) Then
        '    sCode3 = GetStrData(sSql)
        'End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            Upduser.Text = Session("UserID")
            Updtime.Text = FormatDateTime(Now(), DateFormat.GeneralDate)
            Dim genoid As Integer = 0
            If matinvtype.SelectedValue = "INV" Then
                sSql = "select genoid from QL_mstgen WHERE gencode like '" & matgroup.SelectedValue & "%' and gengroup='GROUPITEM'"
                genoid = cKon.ambilscalar(sSql)
            Else
                sSql = "select genoid from QL_mstgen WHERE gencode like '" & matgroup.SelectedValue & "%' and gengroup='ASSETTYPE'"
                genoid = cKon.ambilscalar(sSql)
            End If
            Dim tmpbarcode As String = ""
            If matinvtype.SelectedValue = "INV" Then
                tmpbarcode = CodeItem.Text.Replace(".", "")
            Else
                tmpbarcode = "0"
            End If
            Dim sLastCode As String = GetStrData("SELECT cat3code FROM QL_mstcat3 WHERE cat3oid=" & cat3.SelectedValue & "")
            If matgroup.SelectedValue.ToString.ToUpper = "RAW" Then
                sLastCode = GetStrData("SELECT cat2code FROM QL_mstcat2 WHERE cat2oid=" & Cat2.SelectedValue & "")
            End If
            Dim sMiddleCode As String = ""
            If matgroup.SelectedValue.ToString.ToUpper = "FG" Then
                If Mid(CodeItem.Text, 7, 3) = "999" Then
                    sMiddleCode = GetStrData("SELECT ISNULL(MAX(itemmidcode)+1,1) FROM QL_mstitemmidcode WHERE cat1code='" & sCode1 & "' AND cat2code='" & sCodeRes2 & sCOde2 & "'")
                End If
            ElseIf matgroup.SelectedValue.ToString.ToUpper = "RAW" Then
                If Mid(CodeItem.Text, 5, 3) = "999" Then
                    sMiddleCode = GetStrData("SELECT ISNULL(MAX(itemmidcode)+1,1) FROM QL_mstitemmidcode WHERE cat1code='" & sCode1 & "' AND cat2code='" & sCodeRes2 & sCOde2 & "'")
                End If
            End If
            'mode insert
            If Session("oid_mstmat") Is Nothing Or Session("oid_mstmat") = "" Then
                GenerateGenID()
                If matinvtype.SelectedValue = "INV" Then
                    sSql = "INSERT INTO QL_mstitem (cmpcode, itemoid, itemtype, itemgroupoid, itemgroup, itemcat1, itemcat2, itemcat3, itemcode, itemoldcode, itemshortdescription, itemlongdescription, itembarcode, itemunit1, itemunit2, itemunit3, unit2unit1conversion, unit3unit1conversion, itemminstock, itemmaxstock, itemrecordstatus, itemnote1, itemnote2, itemnote3, itemminprice, itemmaxprice, itempricelist, roundqty, pictureitem1, pictureitem2, pictureitem3, pictureitem4, pictureitem5, createuser, createtime, upduser, updtime, unitprice, itembrand, modeloid, itemlastcode, backupcode, suppoid, pictureItem1desc, pictureItem2desc, pictureItem3desc, pictureItem4desc, pictureItem5desc, jenis_bahan_id) VALUES " & _
                    "('" & CompnyCode & "', " & matoid.Text & ", '" & Tchar(matinvtype.Text) & "'," & genoid & ",'" & Tchar(matgroup.SelectedValue) & "'," & Cat1.SelectedValue & "," & Cat2.SelectedValue & "," & cat3.SelectedValue & ", '" & Tchar(CodeItem.Text) & "','" & Tchar(oldcode.Text) & "','" & Tchar(matlongdesc.Text) & "','" & Tchar(matlongdesc.Text) & "','" & Tchar(tmpbarcode) & "'," & matunitoid1.Text & "," & matunitoid1.Text & "," & matunitoid1.Text & ",1,1,0.0,0.0,'" & ddlStatus.SelectedValue & "','" & Tchar(note1.Text) & "','',''," & ToDecimal(minprice.Text) & "," & ToDecimal(Price.Text) & "," & ToDecimal(Price.Text) & ", 0.0001,'" & Tchar(matpicture.ImageUrl) & "','" & Tchar(matpicture1.ImageUrl) & "','" & Tchar(matpicture3.ImageUrl) & "','" & Tchar(matpicture4.ImageUrl) & "','" & Tchar(matpicture5.ImageUrl) & "','" & Session("UserID") & "',current_timestamp,'" & Upduser.Text & "',current_timestamp, " & matunitoid1.SelectedValue & ", " & merk.SelectedValue & ", " & DDLModel.SelectedValue & ", '" & sLastCode & "', '" & Left(Tchar(CodeItem.Text), CodeItem.Text.Length - 1) & "', " & suppoid.SelectedValue & ", '" & Tchar(pictureItem1desc.Text) & "', '" & Tchar(pictureItem2desc.Text) & "', '" & Tchar(pictureItem3desc.Text) & "', '" & Tchar(pictureItem4desc.Text) & "', '" & Tchar(pictureItem5desc.Text) & "', " & ddlJenisBahan.SelectedValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                Else
                    sSql = "INSERT INTO QL_mstitem (cmpcode, itemoid, itemtype, itemgroupoid, itemgroup, itemcode, itemoldcode, itemshortdescription, itemlongdescription, itembarcode, itemunit1,itemunit2, itemunit3, unit2unit1conversion, unit3unit1conversion, itemminstock, itemmaxstock,itemrecordstatus, itemnote1, itemnote2, itemnote3, itemminprice, itemmaxprice, itempricelist, roundqty, pictureitem1, pictureitem2, pictureitem3, pictureitem4, pictureitem5, createuser, createtime, upduser, updtime, unitprice, itembrand, modeloid, itemlastcode, backupcode, suppoid, pictureItem1desc, pictureItem2desc, pictureItem3desc, pictureItem4desc, pictureItem5desc, jenis_bahan_id) VALUES " & _
                    "('" & CompnyCode & "', " & matoid.Text & ", '" & Tchar(matinvtype.Text) & "'," & genoid & ",'" & Tchar(matgroup.SelectedValue) & "','" & Tchar(CodeItem.Text) & "','" & Tchar(oldcode.Text) & "','" & Tchar(matlongdesc.Text) & "','" & Tchar(matlongdesc.Text) & "','" & Tchar(tmpbarcode) & "'," & matunitoid1.Text & "," & matunitoid1.Text & "," & matunitoid1.Text & ",1.0,1.0,0.0,0.0,'" & ddlStatus.SelectedValue & "','" & Tchar(note1.Text) & "','',''," & ToDecimal(Price.Text) & "," & ToDecimal(Price.Text) & "," & ToDecimal(Price.Text) & ", 0.0001,'" & Tchar(matpicture.ImageUrl) & "','" & Tchar(matpicture1.ImageUrl) & "','" & Tchar(matpicture3.ImageUrl) & "','" & Tchar(matpicture4.ImageUrl) & "','" & Tchar(matpicture5.ImageUrl) & "','" & Session("UserID") & "',current_timestamp,'" & Upduser.Text & "',current_timestamp, " & matunitoid1.SelectedValue & ", " & merk.SelectedValue & ", " & DDLModel.SelectedValue & ", '" & sLastCode & "', '', " & suppoid.SelectedValue & ", '" & Tchar(pictureItem1desc.Text) & "', '" & Tchar(pictureItem2desc.Text) & "', '" & Tchar(pictureItem3desc.Text) & "', '" & Tchar(pictureItem4desc.Text) & "', '" & Tchar(pictureItem5desc.Text) & "', " & ddlJenisBahan.SelectedValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                End If

                sSql = "UPDATE QL_mstoid set lastoid=" & matoid.Text & " WHERE tablename='QL_mstitem' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

            Else 'mode update
                If matinvtype.SelectedValue = "INV" Then
                    sSql = "UPDATE QL_mstitem SET cmpcode='" & CompnyCode & "', itemcode='" & Tchar(CodeItem.Text) & "', itemoldcode='" & Tchar(oldcode.Text) & "', itemtype='" & Tchar(matinvtype.Text) & "', itemgroupoid=" & genoid & ", itemgroup='" & Tchar(matgroup.SelectedValue) & "',itemcat1=" & Cat1.SelectedValue & ", itemcat2=" & Cat2.SelectedValue & ", itemcat3=" & cat3.SelectedValue & ",itemlongdescription='" & Tchar(matlongdesc.Text) & "',itemshortdescription='" & Tchar(matlongdesc.Text) & "', itemunit1=" & matunitoid1.Text & ", itemunit2=" & matunitoid1.Text & ", itemunit3=" & matunitoid1.Text & ", itembarcode='" & tmpbarcode & "', itemnote1='" & Tchar(note1.Text) & "',itemminprice=" & ToDecimal(minprice.Text) & ", itemmaxprice=" & ToDecimal(Price.Text) & ", itempricelist=" & ToDecimal(Price.Text) & ", upduser='" & Upduser.Text & "', updtime=current_timestamp" & ", itemrecordstatus='" & ddlStatus.SelectedValue & "', pictureitem1='" & Tchar(matpicture.ImageUrl) & "', pictureitem2='" & Tchar(matpicture1.ImageUrl) & "', pictureitem3='" & Tchar(matpicture3.ImageUrl) & "', pictureitem4='" & Tchar(matpicture4.ImageUrl) & "', pictureitem5='" & Tchar(matpicture5.ImageUrl) & "', unitprice=" & matunitoid1.SelectedValue & ", itembrand=" & merk.SelectedValue & ", itemlastcode='" & sLastCode & "', backupcode='" & Left(Tchar(CodeItem.Text), CodeItem.Text.Length - 1) & "', suppoid=" & suppoid.SelectedValue & ", pictureItem1desc='" & Tchar(pictureItem1desc.Text) & "', pictureItem2desc='" & Tchar(pictureItem2desc.Text) & "', pictureItem3desc='" & Tchar(pictureItem3desc.Text) & "', pictureItem4desc='" & Tchar(pictureItem4desc.Text) & "', pictureItem5desc='" & Tchar(pictureItem5desc.Text) & "', jenis_bahan_id=" & ddlJenisBahan.SelectedValue & ", modeloid=" & DDLModel.SelectedValue & " where cmpcode='" & CompnyCode & "' and itemoid=" & matoid.Text & ""
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_mstitem SET cmpcode='" & CompnyCode & "', itemcode='" & Tchar(CodeItem.Text) & "', itemoldcode='" & Tchar(oldcode.Text) & "', itemtype='" & Tchar(matinvtype.Text) & "', itemgroupoid=" & genoid & ", itemgroup='" & Tchar(matgroup.SelectedValue) & "', itemlongdescription='" & Tchar(matlongdesc.Text) & "',itemshortdescription='" & Tchar(matlongdesc.Text) & "', itemunit1=" & matunitoid1.Text & ", itemunit2=" & matunitoid1.Text & ", itemunit3=" & matunitoid1.Text & ", itembarcode='" & tmpbarcode & "', itemnote1='" & Tchar(note1.Text) & "',itemminprice=" & ToDecimal(Price.Text) & ", itemmaxprice=" & ToDecimal(Price.Text) & ", itempricelist=" & ToDecimal(Price.Text) & ", upduser='" & Upduser.Text & "', updtime=current_timestamp" & ", itemrecordstatus='" & ddlStatus.SelectedValue & "', pictureitem1='" & Tchar(matpicture.ImageUrl) & "', pictureitem2='" & Tchar(matpicture1.ImageUrl) & "', pictureitem3='" & Tchar(matpicture3.ImageUrl) & "', pictureitem4='" & Tchar(matpicture4.ImageUrl) & "', pictureitem5='" & Tchar(matpicture5.ImageUrl) & "', unitprice=" & matunitoid1.SelectedValue & ", itembrand=" & merk.SelectedValue & ", itemlastcode='" & sLastCode & "', backupcode='', suppoid=" & suppoid.SelectedValue & ", pictureItem1desc='" & Tchar(pictureItem1desc.Text) & "', pictureItem2desc='" & Tchar(pictureItem2desc.Text) & "', pictureItem3desc='" & Tchar(pictureItem3desc.Text) & "', pictureItem4desc='" & Tchar(pictureItem4desc.Text) & "', pictureItem5desc='" & Tchar(pictureItem5desc.Text) & "', jenis_bahan_id=" & ddlJenisBahan.SelectedValue & ", modeloid=" & DDLModel.SelectedValue & " where cmpcode='" & CompnyCode & "' and itemoid=" & matoid.Text & ""
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
            End If

            If sMiddleCode <> "" Then
                sSql = "UPDATE QL_mstitemmidcode SET itemmidcode=" & sMiddleCode & ", upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & CompnyCode & "' AND cat1code='" & sCode1 & "' AND cat2code='" & sCodeRes2 & sCOde2 & "' "
                xCmd.CommandText = sSql
                If xCmd.ExecuteNonQuery() <= 0 Then
                    sSql = "INSERT INTO QL_mstitemmidcode (cmpcode, itemmidcodeoid, itemmidcode, cat1oid, cat2oid, cat3oid, createuser, createtime, upduser, updtime, cat1code, cat2code, cat3code) VALUES ('" & CompnyCode & "', " & sMidCodeOid & ", " & sMiddleCode & ", " & Cat1.SelectedValue & ", " & Cat2.SelectedValue & ", " & cat3.SelectedValue & ", '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & sCode1 & "', '" & sCodeRes2 & sCOde2 & "', '') "
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_mstoid SET lastoid=" & sMidCodeOid & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_MSTITEMMIDCODE'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
            End If

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.ToString & sSql, "ERROR")
            Exit Sub
        End Try
        Session("oid_mstmat") = Nothing
        Session("uploadImage_mstmat") = Nothing
        Session("uploadImage_mstmat1") = Nothing
        Session("uploadImage_mstmat3") = Nothing
        Session("uploadImage_mstmat4") = Nothing
        Session("uploadImage_mstmat5") = Nothing
        Response.Redirect("~\Master\mstitem.aspx?awal=true")
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnUpload.Click
        Dim imgext, imgname As String
        imgext = "Photo" & matpictureloc.FileName.LastIndexOf(".") + 1 & "_"
        imgname = imgext + Path.GetFileName(matpictureloc.FileName)
        Session("uploadImage_mstmat") = imgname
        matpictureloc.SaveAs(Server.MapPath(MaterialImageUrl + imgname))
        matpicture.ImageUrl = MaterialImageUrl + imgname
        matpicture.Visible = True
    End Sub

    Protected Sub btnUpload1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnUpload1.Click
        Dim errMessage As String = ""
        If matpictureloc1.FileName = "" Then
            errMessage &= "Silakan pilih foto nomer 2 terlebih dahulu!<BR>"
        End If
        If errMessage <> "" Then
            showMessage(errMessage, "WARNING")
            Exit Sub
        End If

        Dim imgext1, imgname1 As String
        imgext1 = "Photo" & matpictureloc1.FileName.LastIndexOf(".") + 1 & "_"
        imgname1 = imgext1 + Path.GetFileName(matpictureloc1.FileName)
        Session("uploadImage_mstmat1") = imgname1
        matpictureloc1.SaveAs(Server.MapPath(MaterialImageUrl1 + imgname1))
        matpicture1.ImageUrl = MaterialImageUrl1 + imgname1
        matpicture1.Visible = True
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrOK.Click
        CProc.SetModalPopUpExtender(ButtonExtendervalidasi, PanelValidasi, ModalPopupExtenderValidasi, False)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        Try
            report.Load(Server.MapPath(folderReport & "rptitem.rpt"))
            Dim swhere As String
            swhere = "WHERE itemRecordStatus <> 'INACTIVE' AND " & FilterDDL.SelectedValue & " like '%" & Tchar(FilterText.Text) & "%'"
            If Chkcat1.Checked Then
                swhere &= IIf(FilterGroup.SelectedValue = "ALL", "", " AND itemCat1= " & DDLFilterCat1.SelectedValue & "")
            End If
            If Chkcat2.Checked Then
                swhere &= IIf(FilterGroup.SelectedValue = "ALL", "", " AND itemCat2= " & DDLFilterCat2.SelectedValue & "")
            End If
            If chkGroup.Checked Then
                swhere &= IIf(FilterGroup.SelectedValue = "ALL", "", " AND itemGroup= '" & FilterGroup.SelectedValue & "'")
            End If

            report.SetParameterValue("sWhere", swhere)

            CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), _
                System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

            report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()

            Try
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "MstItem_" & Format(GetServerTime(), "dd_MM_yy"))
            Catch ex As Exception
                report.Close() : report.Dispose()
            End Try

        Catch ex As Exception
            showMessage(ex.ToString, "WARNING")
            Exit Sub
        End Try
        Response.Redirect("~\Master\mstitem.aspx?awal=true")
    End Sub

    Protected Sub btnexcell_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnexcell.Click
        Dim sSql As String = ""
        Dim sWhere As String = ""
        If chkGroup.Checked Then
            sSql &= " AND i.itemGroup ='" & FilterGroup.SelectedValue & "'"
        End If
        If Chkcat1.Checked Then
            sSql &= " AND i.itemCat1 =" & DDLFilterCat1.SelectedValue & ""
        End If
        If Chkcat2.Checked Then
            sSql &= " AND i.itemCat2 =" & DDLFilterCat2.SelectedValue & ""
        End If
        Response.Clear()
        Response.AddHeader("content-disposition", "inline;filename=Material_" & Format(Now, "ddMMyy") & ".xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"

        sSql = "SELECT i.cmpcode, itemoid, c1.cat1longdesc, c2.cat2longdesc, c3.cat3longdesc, itembarcode As Itemcode, itemLongDescription, u1.gendesc AS Unit1,  itemMinPrice, itemMaxPrice, itemPriceList, itemLastSalesPrice,i.CreateUser,i.CreateTime,i.updUser,i.updTime FROM QL_mstitem i INNER JOIN QL_mstcat1 c1 ON i.itemCat1=c1.cat1oid  INNER JOIN QL_mstcat2 c2 ON i.itemCat2=c2.cat2oid INNER JOIN QL_mstcat3 c3 ON i.itemCat3=c3.cat3oid INNER JOIN QL_mstgen u1 ON u1.genoid=i.itemUnit1 INNER JOIN QL_mstgen u2 ON u2.genoid=i.itemUnit2 INNER JOIN QL_mstgen u3 ON u3.genoid=i.itemUnit3 WHERE i.cmpcode='" & CompnyCode & "' " & sSql & " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"

        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
        da.SelectCommand = xcmd
        da.Fill(ds)
        Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
        dt = ds.Tables(0)
        Response.Write(ConvertDtToTDF(dt))
        conn.Close()
        Response.End()
    End Sub

    Protected Sub GVmstmat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GVmstmat.PageIndex = e.NewPageIndex
        GVmstmat.DataSource = Session("TblMst")
        GVmstmat.DataBind()
    End Sub

    Protected Sub Cat1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Cat1.Items.Count > 0 Then
            InitDDLCat2()
            generateCode()
        Else
            Cat2.Items.Clear()
        End If
    End Sub

    Protected Sub Cat2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Cat2.Items.Count > 0 Then
            InitDDLCat3()
            generateCode()
        Else
            cat3.Items.Clear()
        End If
    End Sub

    Protected Sub matgroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitDDLCat1()
        generateCode()
    End Sub

    Protected Sub FilterGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitFilterDDLCat1()
    End Sub

    Protected Sub DDLFilterCat1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitFilterDDLCat2()
    End Sub

    Protected Sub matinvtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        cekType()
        InitAllDDL()
        matgroup_SelectedIndexChanged(sender, e)
    End Sub

    Protected Sub FilterType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If FilterType.SelectedValue = "INV" Then
            Chkcat1.Visible = True
            Chkcat2.Visible = True
            DDLFilterCat1.Visible = True
            DDLFilterCat2.Visible = True
        Else
            Chkcat1.Visible = False
            Chkcat2.Visible = False
            DDLFilterCat1.Visible = False
            DDLFilterCat2.Visible = False
        End If
        InitAllDDL()
        FilterGroup_SelectedIndexChanged(sender, e)
    End Sub

    Protected Sub btngendesc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btngendesc.Click
        matlongdesc.Text = ""
        If Cat1.SelectedValue > 0 Then
            matlongdesc.Text = GetStrData("select cat1shortdesc from QL_mstcat1 where cat1oid=" & Cat1.SelectedValue & "")
        End If
        If Cat2.SelectedValue > 0 Then
            matlongdesc.Text &= " " & GetStrData("select cat2shortdesc from QL_mstcat2 where cat2oid=" & Cat2.SelectedValue & "")
        End If
        If cat3.SelectedValue > 0 Then
            matlongdesc.Text &= " " & GetStrData("select cat3shortdesc from QL_mstcat3 where cat3oid=" & cat3.SelectedValue & "")
        End If
        If merk.SelectedValue <> 0 Then
            matlongdesc.Text &= " " & merk.SelectedItem.Text
        End If
        If DDLModel.SelectedValue <> 0 Then
            matlongdesc.Text &= " " & DDLModel.SelectedItem.Text
        End If
        matlongdesc_TextChanged(Nothing, Nothing)
    End Sub

    Protected Sub DDLModel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'matlongdesc.Text = ""
    End Sub

    Protected Sub cat3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If cat3.Items.Count > 0 Then
            generateCode()
        Else
            cat3.Items.Clear()
        End If
    End Sub

    Protected Sub matlongdesc_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        generateCode()
    End Sub

    Protected Sub Price_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Price.Text = ToMaskEdit(ToDouble(Price.Text), 2)
        CalculatePricePercentage()
    End Sub

    Protected Sub GVmstmat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
        End If
    End Sub

    Protected Sub ImbPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            report.Load(Server.MapPath(folderReport & "rptitem.rpt"))
            Dim swhere As String
            swhere = "WHERE itemoid=" & sender.Tooltip
            report.SetParameterValue("sWhere", swhere)
            CProc.SetDBLogonForReport(report)
            report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()

            Try
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "MstItem_" & Format(GetServerTime(), "dd_MM_yy"))
            Catch ex As Exception
                report.Close() : report.Dispose()
            End Try

        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR")
            Exit Sub
        End Try
    End Sub

#End Region

    Protected Sub btnUpload3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnUpload3.Click
        Dim errMessage As String = ""
        If matpictureloc3.FileName = "" Then
            errMessage &= "Silakan pilih foto nomer 3 terlebih dahulu!<BR>"
        End If
        If errMessage <> "" Then
            showMessage(errMessage, "WARNING")
            Exit Sub
        End If

        Dim imgext3, imgname3 As String
        imgext3 = "Photo" & matpictureloc3.FileName.LastIndexOf(".") + 1 & "_"
        imgname3 = imgext3 + Path.GetFileName(matpictureloc3.FileName)
        Session("uploadImage_mstmat3") = imgname3
        matpictureloc3.SaveAs(Server.MapPath(MaterialImageUrl3 + imgname3))
        matpicture3.ImageUrl = MaterialImageUrl3 + imgname3
        matpicture3.Visible = True
    End Sub

    Protected Sub btnUpload4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnUpload4.Click
        Dim imgext4, imgname4 As String
        imgext4 = "Photo" & matpictureloc4.FileName.LastIndexOf(".") + 1 & "_"
        imgname4 = imgext4 + Path.GetFileName(matpictureloc4.FileName)
        Session("uploadImage_mstmat4") = imgname4
        matpictureloc4.SaveAs(Server.MapPath(MaterialImageUrl4 + imgname4))
        matpicture4.ImageUrl = MaterialImageUrl4 + imgname4
        matpicture4.Visible = True
    End Sub

    Protected Sub btnUpload5_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnUpload5.Click
        Dim imgext5, imgname5 As String
        imgext5 = "Photo" & matpictureloc5.FileName.LastIndexOf(".") + 1 & "_"
        imgname5 = imgext5 + Path.GetFileName(matpictureloc5.FileName)
        Session("uploadImage_mstmat5") = imgname5
        matpictureloc5.SaveAs(Server.MapPath(MaterialImageUrl5 + imgname5))
        matpicture5.ImageUrl = MaterialImageUrl5 + imgname5
        matpicture5.Visible = True
    End Sub

    Protected Sub ImbPriceList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            report.Load(Server.MapPath(folderReport & "rptPriceList.rpt"))
            sSql = "select c1.cat1shortdesc, br.gendesc merek, itemCode, itemLongDescription, itemPriceList harga_agen, itemPriceList + (itemPriceList * 25.00/100.00) harga_ecer, stok, age stok_1_30, case when age between 31 and 60 then stok else 0.0 end stok_31_60, case when age between 61 and 90 then stok else 0.0 end stok_61_90, case when age between 91 and 120 then stok else 0.0 end stok_91_120, case when age > 120 then stok else 0.0 end lebih_120, g.gendesc satuan from QL_mstitem i inner join QL_mstcat1 c1 on c1.cat1oid=i.itemCat1 inner join QL_mstgen g on g.genoid=i.itemUnit1 inner join QL_mstgen br on br.genoid=i.itembrand cross apply ((select sum(stok) stok, sum(agesales) age from (select sum(qtyin - qtyout) stok, case formaction when 'QL_trnprodresconfirm' then datediff(day, (select top 1 j.trndate from QL_constock j where j.refoid=cs.refoid and formaction='QL_trnprodresconfirm' order by j.trndate),getdate()) when 'QL_trnmrdtl' then datediff(day, (select top 1 j.trndate from QL_constock j where j.refoid=cs.refoid and formaction='QL_trnmrdtl' order by j.trndate),getdate()) when 'Material Init Stock' then datediff(day, (select top 1 j.trndate from QL_constock j where j.refoid=cs.refoid and formaction='Material Init Stock' order by j.trndate),getdate()) else 0 end agesales, datediff(day,isnull((select top 1 j.trndate from QL_constock j where j.refoid=cs.refoid and formaction='QL_trndodtl' order by j.trndate desc),getdate()),getdate()) agepurchase from QL_constock cs where refoid=itemoid and mtrlocoid=147 group by refoid, formaction) yy)) st where i.itemGroup='fg' and i.itemRecordStatus='active' and stok>0"

            If Chkcat1.Checked Then
                sSql &= IIf(FilterGroup.SelectedValue = "ALL", "", " and itemCat1= " & DDLFilterCat1.SelectedValue & "")
            End If
            sSql &= " order by c1.cat1shortdesc, age desc, merek, itemCode"
            Dim data As DataTable = cKon.ambiltabel(sSql, "data")
            report.SetDataSource(data)
            CProc.SetDBLogonForReport(report)

            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()

            Try
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "MstItem_" & Format(GetServerTime(), "dd_MM_yy"))
                report.Close()
                report.Dispose()
            Catch ex As Exception
                report.Close() : report.Dispose()
            End Try

        Catch ex As Exception
            showMessage(ex.ToString, "WARNING")
            Exit Sub
        End Try
        Response.Redirect("~\Master\mstitem.aspx?awal=true")
    End Sub
End Class
