Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_Division
  Inherits System.Web.UI.Page

#Region "Variables"
  Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
  Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
  Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
  Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
  Dim xCmd As New SqlCommand("", conn)
  Dim xreader As SqlDataReader
  Dim sSql As String = ""
  Dim cKon As New Koneksi
  Dim cProc As New ClassProcedure
  Dim report As New ReportDocument
  Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
  Private Function IsInputValid() As Boolean
    Dim sError As String = ""
    If divcode.Text = "" Then
      sError &= "- Please fill CODE field!<BR>"
    End If
    If divname.Text = "" Then
      sError &= "- Please fill NAME field!<BR>"
    End If
    Dim serr As String = ""
    If divcreationdate.Text = "" Then
      sError &= "- Please fill CREATION DATE field!<BR>"
    Else
      If Not IsValidDate(divcreationdate.Text, "MM/dd/yyyy", serr) Then
        sError &= "- CREATION DATE is invalid. " & serr & "<BR>"
      End If
    End If
    Dim oMatches As MatchCollection
    If divemail.Text <> "" Then
      oMatches = Regex.Matches(divemail.Text, "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")
      If oMatches.Count <= 0 Then
        sError &= "- EMAIL is invalid. Format should like '<STRONG>mail@sample.com</STRONG>'.<BR>"
      End If
    End If
    If divcityoid.SelectedValue = "" Then
      sError &= "- Please select CITY field!<br>"
    End If
    If sError <> "" Then
      showMessage(sError, 2)
      Return False
    End If
    Return True
  End Function

  Private Function IsInputExists() As Boolean
    Dim sError As String = ""
    sSql = "SELECT COUNT(*) FROM QL_mstdivision WHERE divcode='" & Tchar(divcode.Text.Trim) & "'"
    If Not Session("oid") Is Nothing And Session("oid") <> "" Then
      sSql &= " AND divoid<>" & Session("oid")
    End If
    If ToDouble(GetStrData(sSql).ToString) > 0 Then
      showMessage("- CODE has been used by another data. Please fill another CODE!<br>", 2)
      Return True
    End If
    sSql = "SELECT COUNT(*) FROM QL_mstdivision WHERE divname='" & Tchar(divname.Text.Trim) & "'"
    If Not Session("oid") Is Nothing And Session("oid") <> "" Then
      sSql &= " AND divoid<>" & Session("oid")
    End If
    If ToDouble(GetStrData(sSql).ToString) > 0 Then
      showMessage("- NAME has been used by another data. Please fill another NAME!<br>", 2)
      Return True
    End If
    Return False
  End Function
#End Region

#Region "Procedures"
  Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
    Dim strCaption As String = CompnyName
    If iType = 1 Then ' Error
      imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
    ElseIf iType = 2 Then ' Warning
      imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
    ElseIf iType = 3 Then ' Information
      imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
    Else
      imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
    End If
    lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
    cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
  End Sub

  Private Sub InitAllDDL()
    FillDivCountryOID()
  End Sub

  Private Sub BindMstData()
    sSql = "SELECT divoid, divcode, divname, CONVERT(varchar(10), divcreationdate, 101) AS creationdate, divpic, (divaddress + ' ' + gendesc + (CASE divphone WHEN NULL THEN '' WHEN '' THEN '' ELSE '. Phone 1: ' + divphone END) + (CASE divfax1 WHEN NULL THEN '' WHEN '' THEN '' ELSE '. Fax 1: ' + divfax1 END) + (CASE divphone2 WHEN NULL THEN '' WHEN '' THEN '' ELSE '. Phone 2: ' + divphone2 END) + (CASE divfax2 WHEN NULL THEN '' WHEN '' THEN '' ELSE '. Fax 2: ' + divfax2 END) + (CASE divpostcode WHEN NULL THEN '' WHEN '' THEN '' ELSE '. Postal Code: ' + divpostcode END) + (CASE divemail WHEN NULL THEN '' WHEN '' THEN '' ELSE '. Email: ' + divemail END)) AS divaddress, divtotalperson FROM QL_mstdivision d INNER JOIN QL_mstgen g ON genoid=divcityoid AND g.activeflag='ACTIVE'"
    If FilterDDL.SelectedValue = "Address" Then
      sSql &= " WHERE (divaddress LIKE '%" & Tchar(FilterText.Text.Trim) & "%' OR gendesc LIKE '%" & Tchar(FilterText.Text.Trim) & "%' OR divphone LIKE '%" & Tchar(FilterText.Text.Trim) & "%' OR divemail LIKE '%" & Tchar(FilterText.Text.Trim) & "%')"
    Else
      sSql &= " WHERE " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text.Trim) & "%'"
    End If
    If Session("CompnyCode") <> CompnyCode Then
      sSql &= " AND d.cmpcode='" & Session("CompnyCode") & "'"
    End If
    If cbStatus.Checked Then
      If FilterDDLStatus.SelectedValue <> "All" Then
        sSql &= " AND d.activeflag='" & FilterDDLStatus.SelectedValue & "'"
      End If
    End If
    If checkPagePermission("~\Master\mstDivision.aspx", Session("SpecialAccess")) = False Then
      sSql &= " AND d.createuser='" & Session("UserID") & "'"
    End If
    sSql &= " ORDER BY divoid DESC"
    Session("TblMst") = cKon.ambiltabel(sSql, "QL_mstdivision")
    gvMst.DataSource = Session("TblMst")
    gvMst.DataBind()
    lblViewInfo.Visible = False
  End Sub

  Private Sub FillTextBox()
    Try
            sSql = "SELECT divoid, divcode, divname, divcreationdate, divmaxperson, divmaxbasesalary, divtotalperson, divtotalbasesalary, divaddress,divaddress1,divaddress2, divcityoid, divphone, divemail, divpic, divnote, activeflag, createuser, createtime, upduser, updtime, divjkkvalue, divphone2, divpostcode, divfax1, divfax2 FROM QL_mstdivision WHERE divoid=" & Session("oid")
      If conn.State = ConnectionState.Closed Then
        conn.Open()
      End If
      xCmd = New SqlCommand(sSql, conn)
      xreader = xCmd.ExecuteReader
      btnDelete.Visible = False
      Dim city As Integer
      While xreader.Read
        divoid.Text = xreader("divoid").ToString
        divcode.Text = xreader("divcode").ToString
        divname.Text = xreader("divname").ToString.Trim
        divcreationdate.Text = Format(xreader("divcreationdate"), "MM/dd/yyyy")
        divmaxperson.Text = xreader("divmaxperson")
        divmaxbasesalary.Text = ToMaskEdit(xreader("divmaxbasesalary"), 4)
        divtotalperson.Text = xreader("divtotalperson")
        divtotalbasesalary.Text = ToMaskEdit(xreader("divtotalbasesalary"), 4)
                divaddress.Text = Trim(xreader("divaddress").ToString)
                divaddress1.Text = Trim(xreader("divaddress1").ToString)
                divaddress2.Text = Trim(xreader("divaddress2").ToString)
        divphone.Text = xreader("divphone").ToString
        divemail.Text = xreader("divemail").ToString
        divpic.Text = xreader("divpic").ToString
        divnote.Text = xreader("divnote").ToString
        activeflag.SelectedValue = xreader("activeflag")
        createuser.Text = xreader("createuser").ToString
        createtime.Text = xreader("createtime").ToString
        upduser.Text = xreader("upduser").ToString
        updtime.Text = xreader("updtime").ToString
        divjkkvalue.Text = ToMaskEdit(xreader("divjkkvalue"), 4)
        divphone2.Text = xreader("divphone2").ToString
        divpostcode.Text = xreader("divpostcode").ToString
        divFax1.Text = xreader("divfax1").ToString
        divFax2.Text = xreader("divfax2").ToString
        city = xreader("divcityoid").ToString
      End While
      xreader.Close()
      If divcountryoid.Items.Count > 0 Then
        xCmd.CommandText = "SELECT genother2 FROM QL_mstgen WHERE gengroup='CITY' AND genoid=" & city
        divcountryoid.SelectedValue = xCmd.ExecuteScalar
        divcountryoid_SelectedIndexChanged(Nothing, Nothing)
      End If
      If divprovinceoid.Items.Count > 0 Then
        xCmd.CommandText = "SELECT genother1 FROM QL_mstgen WHERE gengroup='CITY' AND genoid=" & city
        divprovinceoid.SelectedValue = xCmd.ExecuteScalar
        divprovinceoid_SelectedIndexChanged(Nothing, Nothing)
        divcityoid.SelectedValue = city
      End If
      conn.Close()
    Catch ex As Exception
      showMessage(ex.Message, 1)
    Finally
      divcode.CssClass = "inpTextDisabled"
      divcode.Enabled = True
      btnDelete.Visible = True
    End Try
  End Sub

  Private Sub ShowReport()
    'Try
    '    report.Load(Server.MapPath(folderReport & "rptDivision.rpt"))
    '    Dim sWhere As String = " WHERE cmpcode='" & CompnyCode & "' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
    '    If cbStatus.Checked Then
    '        If FilterDDLStatus.SelectedValue <> "All" Then
    '            sWhere &= " AND activeflag='" & FilterDDLStatus.SelectedValue & "'"
    '        End If
    '    End If
    '    If checkPagePermission("~\Master\mstDivision.aspx", Session("SpecialAccess")) = False Then
    '        sWhere &= " AND createuser='" & Session("UserID") & "'"
    '    End If
    '    sWhere &= " ORDER BY divoid"
    '    report.SetParameterValue("sWhere", sWhere)
    '    cProc.SetDBLogonForReport(report)
    '    report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
    '    Response.Buffer = False
    '    Response.ClearContent()
    '    Response.ClearHeaders()
    '    report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "DivisionReport_" & Format(GetServerTime(), "yyyyMMddHHmmss"))
    '    report.Close()
    '    report.Dispose()
    'Catch ex As Exception
    '    report.Close()
    '    report.Dispose()
    '    showMessage(ex.Message, 1)
    'End Try
  End Sub
#End Region

#Region "Events"
  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    If Session("UserID") = "" Then
      Response.Redirect("~/Other/login.aspx")
    End If
    Session.Timeout = 60
    If Request.QueryString("awal") = "true" Then
      ' Simpan session ke variabel temporary supaya tidak hilang
      Dim userId As String = Session("UserID")
      Dim xsetAcc As DataTable = Session("SpecialAccess")
      Dim appLimit As Decimal = Session("ApprovalLimit")
      Dim xsetRole As DataTable = Session("Role")
      Dim cmpcode As String = Session("CompnyCode")
      ' Clear all session
      Session.Clear()
      ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
      Session("UserID") = userId
      Session("SpecialAccess") = xsetAcc
      Session("ApprovalLimit") = appLimit
      Session("Role") = xsetRole
      Session("CompnyCode") = cmpcode
      ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
      Response.Redirect("~\Master\mstDivision.aspx")
    End If
    If checkPagePermission("~\Master\mstDivision.aspx", Session("Role")) = False Then
      Response.Redirect("~\Other\NotAuthorize.aspx")
    End If
    Page.Title = CompnyName & " - Business Unit"
    Session("oid") = Request.QueryString("oid")
    If Session("oid") = "" Or Session("oid") = Nothing Then
      I_U.Text = "New Data"
    Else
      I_U.Text = "Update Data"
    End If
    btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
    If Not Page.IsPostBack Then
      InitAllDDL()
      createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
      upduser.Text = "-" : updtime.Text = "-"
      If Session("oid") <> Nothing And Session("oid") <> "" Then
        FillTextBox()
        TabContainer1.ActiveTabIndex = 1
      Else
        divoid.Text = GenerateID("QL_MSTDIVISION", CompnyCode)
        divcreationdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
        btnDelete.Visible = False
        TabContainer1.ActiveTabIndex = 0
      End If
    End If
  End Sub

  Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
    BindMstData()
  End Sub

  Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
    FilterText.Text = ""
    FilterDDL.SelectedIndex = -1
    cbStatus.Checked = False
    FilterDDLStatus.SelectedIndex = -1
    BindMstData()
  End Sub

  Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
    If IsInputValid() And Not IsInputExists() Then
      If Session("oid") = Nothing Or Session("oid") = "" Then
        divoid.Text = GenerateID("QL_MSTDIVISION", CompnyCode)
      End If
      Dim objTrans As SqlClient.SqlTransaction
      If conn.State = ConnectionState.Closed Then
        conn.Open()
      End If
      objTrans = conn.BeginTransaction()
      xCmd.Transaction = objTrans
      Try
        If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_mstdivision (cmpcode, divoid, divcode, divname, divcreationdate, divmaxperson, divmaxbasesalary, divtotalperson, divtotalbasesalary, divaddress,divaddress1,divaddress2, divcityoid, divphone, divemail, divpic, divnote, activeflag, createuser, upduser, updtime, divjkkvalue, createtime, divphone2, divpostcode, divfax1, divfax2) VALUES ('" & Tchar(divcode.Text.Trim) & "', " & divoid.Text & ", '" & Tchar(divcode.Text.Trim) & "', '" & Tchar(divname.Text.Trim) & "', '" & divcreationdate.Text & "', " & ToDouble(divmaxperson.Text) & ", " & ToDouble(divmaxbasesalary.Text) & ", " & ToDouble(divtotalperson.Text) & ", " & ToDouble(divtotalbasesalary.Text) & ", '" & Tchar(divaddress.Text.Trim) & "','" & Tchar(divaddress1.Text.Trim) & "','" & Tchar(divaddress2.Text.Trim) & "', " & divcityoid.SelectedValue & ", '" & Tchar(divphone.Text.Trim) & "', '" & Tchar(divemail.Text.Trim) & "', '" & Tchar(divpic.Text.Trim) & "', '" & Tchar(divnote.Text.Trim) & "', '" & activeflag.SelectedValue & "', '" & Session("UserId") & "', '" & Session("UserId") & "', CURRENT_TIMESTAMP, " & ToDouble(divjkkvalue.Text) & ", CURRENT_TIMESTAMP, '" & Tchar(divphone2.Text.Trim) & "', '" & Tchar(divpostcode.Text.Trim) & "', '" & Tchar(divFax1.Text.Trim) & "', '" & Tchar(divFax2.Text.Trim) & "')"
          xCmd.CommandText = sSql
          xCmd.ExecuteNonQuery()
          sSql = "UPDATE QL_mstoid SET lastoid=" & divoid.Text & " WHERE tablename LIKE 'QL_mstdivision' AND cmpcode='" & CompnyCode & "'"
          xCmd.CommandText = sSql
          xCmd.ExecuteNonQuery()
        Else
                    sSql = "UPDATE QL_mstdivision SET divname='" & Tchar(divname.Text.Trim) & "', divcreationdate='" & divcreationdate.Text & "', divmaxperson=" & ToDouble(divmaxperson.Text) & ", divmaxbasesalary=" & ToDouble(divmaxbasesalary.Text) & ", divtotalperson=" & ToDouble(divtotalperson.Text) & ", divtotalbasesalary=" & ToDouble(divtotalbasesalary.Text) & ", divaddress='" & Tchar(divaddress.Text.Trim) & "',divaddress1='" & Tchar(divaddress1.Text.Trim) & "',divaddress2='" & Tchar(divaddress2.Text.Trim) & "', divcityoid=" & divcityoid.SelectedValue & ", divphone='" & Tchar(divphone.Text.Trim) & "', divemail='" & Tchar(divemail.Text.Trim) & "', divpic='" & Tchar(divpic.Text.Trim) & "', divnote='" & Tchar(divnote.Text.Trim) & "', activeflag='" & activeflag.SelectedValue & "', upduser='" & Session("UserId") & "', updtime=CURRENT_TIMESTAMP, divjkkvalue=" & ToDouble(divjkkvalue.Text) & ", divphone2='" & Tchar(divphone2.Text.Trim) & "', divpostcode='" & Tchar(divpostcode.Text.Trim) & "', divfax1= '" & Tchar(divFax1.Text.Trim) & "', divfax2= '" & Tchar(divFax2.Text.Trim) & "' WHERE divoid=" & divoid.Text
          xCmd.CommandText = sSql
          xCmd.ExecuteNonQuery()
        End If
        objTrans.Commit()
        conn.Close()
      Catch ex As Exception
        objTrans.Rollback()
        conn.Close()
        showMessage(ex.Message, 1)
        Exit Sub
      End Try
      Response.Redirect("~\Master\mstDivision.aspx?awal=true")
    End If
  End Sub

  Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
    Response.Redirect("~\Master\mstDivision.aspx?awal=true")
  End Sub

  Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
    If divoid.Text = "" Then
      showMessage("Please select division data first!", 1)
      Exit Sub
    End If
    showMessage("This data can't be deleted because it is being used by another data!", 2)
    'sSql = "SELECT tblusage, colusage FROM QL_oidusage WHERE cmpcode='" & CompnyCode & "' AND tblname='QL_MSTDIVISION'"
    'Dim objTblUsage As DataTable = cKon.ambiltabel(sSql, "QL_oidusage")
    'Dim sColomnName(objTblUsage.Rows.Count - 1) As String
    'Dim sTable(objTblUsage.Rows.Count - 1) As String
    'For c1 As Integer = 0 To objTblUsage.Rows.Count - 1
    '    sColomnName(c1) = objTblUsage.Rows(c1).Item("colusage").ToString
    '    sTable(c1) = objTblUsage.Rows(c1).Item("tblusage").ToString
    'Next
    'If CheckDataExists(divoid.Text, sColomnName, sTable) = True Then
    '    showMessage("This data can't be deleted because it is being used by another data!", 2)
    '    Exit Sub
    'End If
    'If DeleteData("QL_mstdivision", "divoid", divoid.Text, CompnyCode) = True Then
    '    Response.Redirect("~\Master\mstDivision.aspx?awal=true")
    'End If
  End Sub

  Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
    gvMst.PageIndex = e.NewPageIndex
    gvMst.DataSource = Session("TblMst")
    gvMst.DataBind()
  End Sub

  Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
    'Retrieve the table from the session object.
    Dim dt = TryCast(Session("TblMst"), DataTable)
    If dt IsNot Nothing Then
      'Sort the data.
      dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
      gvMst.DataSource = Session("TblMst")
      gvMst.DataBind()
    End If
  End Sub

  Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
    cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
  End Sub
#End Region

  Private Sub FillDivCountryOID()
    sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='COUNTRY' AND activeflag='ACTIVE' ORDER BY gendesc"
    FillDDL(divcountryoid, sSql)
    If divcountryoid.Items.Count > 0 Then
      divcountryoid_SelectedIndexChanged(Nothing, Nothing)
    Else
      lblWarning.Text &= "- Please fill <STRONG>COUNTRY</STRONG> data in <STRONG>General Form</STRONG> with selected group is COUNTRY!<BR>"
    End If
  End Sub

  Protected Sub divcountryoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles divcountryoid.SelectedIndexChanged
    sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='PROVINCE' AND activeflag='ACTIVE' AND genother1='" & divcountryoid.SelectedValue & "' ORDER BY gendesc"
    FillDDL(divprovinceoid, sSql)
    If divprovinceoid.Items.Count > 0 Then
      divprovinceoid_SelectedIndexChanged(Nothing, Nothing)
    ElseIf createuser.Text <> "" Then
      showMessage("- Please fill <STRONG>PROVINCE</STRONG> data in <STRONG>General Form</STRONG> with selected group is PROVINCE and COUNTRY is " & divcountryoid.SelectedItem.Text & "!<BR>", 2)
      divcityoid.Items.Clear()
    End If
  End Sub

  Protected Sub divprovinceoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles divprovinceoid.SelectedIndexChanged
    sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='CITY' AND activeflag='ACTIVE' AND genother1='" & divprovinceoid.SelectedValue & "' AND genother2='" & divcountryoid.SelectedValue & "' ORDER BY gendesc"
    FillDDL(divcityoid, sSql)
    If divcityoid.Items.Count < 1 Then
      showMessage("- Please fill <STRONG>CITY</STRONG> data in <STRONG>General Form</STRONG> with selected group is CITY, COUNTRY is " & divcountryoid.SelectedItem.Text & " and PROVINCE is " & divprovinceoid.SelectedItem.Text & "!<BR>", 2)
    End If
  End Sub
End Class
