<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="mstAppPerson.aspx.vb" Inherits="Master_ApprovalPerson" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Approval User" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Approval User :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD style="WIDTH: 8%" class="Label" align=left><asp:Label id="Label2" runat="server" Text="Filter"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="app.approvaluser">User ID</asp:ListItem>
<asp:ListItem Value="p.profname">User Name</asp:ListItem>
<asp:ListItem Value="app.apppersonnote">Table Name</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="150px"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status"></asp:CheckBox></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:DropDownList id="ddlFilterStatus" runat="server" CssClass="inpText" Width="100px">
    <asp:ListItem>All</asp:ListItem>
    <asp:ListItem>Active</asp:ListItem>
<asp:ListItem>Non Active</asp:ListItem>
<asp:ListItem>Suspended</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbType" runat="server" Text="Type" Checked="True" Visible="False"></asp:CheckBox></TD><TD class="Label" align=left></TD><TD class="Label" align=left colSpan=4><asp:DropDownList id="ddlFilterType" runat="server" CssClass="inpText" Width="100px" Visible="False"><asp:ListItem Enabled="False">Process</asp:ListItem>
<asp:ListItem Selected="True">Final</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:GridView id="gvMst" runat="server" ForeColor="#333333" Width="100%" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True" PageSize="8" GridLines="None">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="apppersonoid" DataNavigateUrlFormatString="~/Master/mstAppPerson.aspx?oid={0}" DataTextField="approvaluser" HeaderText="User ID" SortExpression="approvaluser">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="profname" HeaderText="User Name" SortExpression="profname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="apppersonnote" HeaderText="Table Name" SortExpression="apppersonnote">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="activeflag" HeaderText="Status" SortExpression="activeflag">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="Sienna" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" Text="Data can't be found !!" CssClass="Important"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data"></asp:Label> </TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3></TD></TR><TR><TD style="WIDTH: 11%" class="Label" align=left><asp:Label id="I_U" runat="server" CssClass="Important" Text="New Data"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=right></TD><TD class="Label" align=left><asp:Label id="apppersonoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Label id="lblWarn" runat="server" CssClass="Important" Text="You can't edit or delete this data because this data still have transaction must be approved." Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblUserID" runat="server" Text="User ID"></asp:Label></TD><TD class="Label" align=right>:</TD><TD class="Label" align=left><asp:DropDownList id="ddlapprovaluser" runat="server" CssClass="inpText" Width="150px"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblTableName" runat="server" Text="Table Name"></asp:Label></TD><TD class="Label" align=right>:</TD><TD class="Label" align=left><asp:DropDownList id="ddltablename" runat="server" CssClass="inpText" Width="200px"><asp:ListItem Value="QL_prrawmst">PR RAW MATERIAL</asp:ListItem>
<asp:ListItem Value="QL_prgenmst">PR GENERAL MATERIAL</asp:ListItem>
<asp:ListItem Value="QL_prspmst">PR SPARE PART</asp:ListItem>
<asp:ListItem Value="QL_prassetmst">PR FIXED ASSETS</asp:ListItem>
<asp:ListItem Value="QL_prwipmst">PR WIP LOG</asp:ListItem>
<asp:ListItem Value="QL_pritemmst">PR FINISH GOOD</asp:ListItem>
<asp:ListItem Value="QL_prsawnmst">PR SAWN TIMBER</asp:ListItem>
<asp:ListItem Value="QL_trnporawmst">PO RAW MATERIAL</asp:ListItem>
<asp:ListItem Value="QL_trnpogenmst">PO GENERAL MATERIAL</asp:ListItem>
<asp:ListItem Value="QL_trnpospmst">PO SPARE PART</asp:ListItem>
<asp:ListItem Value="QL_trnpoassetmst">PO FIXED ASSETS</asp:ListItem>
<asp:ListItem Value="QL_trnpowipmst">PO WIP LOG</asp:ListItem>
<asp:ListItem Value="QL_trnpoitemmst">PO FINISH GOOD</asp:ListItem>
<asp:ListItem Value="QL_trnposervicemst">PO SERVICE</asp:ListItem>
<asp:ListItem Value="QL_trnposubconmst">PO SUBCON</asp:ListItem>
<asp:ListItem Value="QL_trnposawnmst">PO SAWN TIMBER</asp:ListItem>
<asp:ListItem Value="QL_trnsorawmst">SO RAW MATERIAL</asp:ListItem>
<asp:ListItem Value="QL_trnsogenmst">SO GENERAL MATERIAL</asp:ListItem>
<asp:ListItem Value="QL_trnsospmst">SO SPARE PART</asp:ListItem>
<asp:ListItem Value="QL_trnsoitemmst">SO FINISH GOOD</asp:ListItem>
<asp:ListItem Value="QL_trnsoassetmst">SO FIXED ASSETS</asp:ListItem>
<asp:ListItem Value="QL_trnsowipmst">SO WIP LOG</asp:ListItem>
<asp:ListItem Value="QL_trnshipmentrawmst">SHIPMENT RAW MATERIAL</asp:ListItem>
<asp:ListItem Value="QL_trnshipmentgenmst">SHIPMENT GENERAL MATERIAL</asp:ListItem>
<asp:ListItem Value="QL_trnshipmentspmst">SHIPMENT SPARE PART</asp:ListItem>
<asp:ListItem Value="QL_trnshipmentitemmst">SHIPMENT FINISH GOOD</asp:ListItem>
<asp:ListItem Value="QL_trnbrtacaramst">BERITA ACARA</asp:ListItem>
    <asp:ListItem Value="QL_trnbrtacaraprodmst">BERITA ACARA PRODUCT</asp:ListItem>
<asp:ListItem Value="QL_trnaprawmst">A/P RAW MATERIAL</asp:ListItem>
<asp:ListItem Value="QL_trnapgenmst">A/P GENERAL MATERIAL</asp:ListItem>
<asp:ListItem Value="QL_trnapspmst">A/P SPARE PART</asp:ListItem>
    <asp:ListItem Value="QL_trnapitemmst">A/P FINISH GOOD</asp:ListItem>
<asp:ListItem Value="QL_trnapassetmst">A/P FIXED ASSETS</asp:ListItem>
    <asp:ListItem Value="QL_trnapservicemst">A/P SERVICE</asp:ListItem>
    <asp:ListItem Value="QL_trnapsubconmst">A/P SUBCON</asp:ListItem>
    <asp:ListItem Value="QL_trnapwipmst">A/P LOG</asp:ListItem>
    <asp:ListItem Value="QL_trnapsawnmst">A/P SAWN TIMBER</asp:ListItem>
    <asp:ListItem Value="QL_trnapproductmst">A/P PRODUCTION RESULT</asp:ListItem>
    <asp:ListItem Value="QL_trnapjoresultmst">A/P PROCESS COSTING MO RESULT</asp:ListItem>
    <asp:ListItem Value="QL_trnaptransformmst">A/P MATERIAL TRANSFORMATION</asp:ListItem>
<asp:ListItem Value="QL_trnpretrawmst">PURCHASE RETURN RAW MATERIAL</asp:ListItem>
<asp:ListItem Value="QL_trnpretgenmst">PURCHASE RETURN GENERAL MATERIAL</asp:ListItem>
<asp:ListItem Value="QL_trnpretspmst">PURCHASE RETURN SPARE PART</asp:ListItem>
<asp:ListItem Value="QL_trnpretassetmst">PURCHASE RETURN FIXED ASSETS</asp:ListItem>
    <asp:ListItem Value="QL_trnpretsubconmst">PURCHASE RETURN SUBCON</asp:ListItem>
<asp:ListItem Value="QL_trnarrawmst">ACCOUNT RECEIVABLE - RM</asp:ListItem>
<asp:ListItem Value="QL_trnargenmst">ACCOUNT RECEIVABLE - GM</asp:ListItem>
<asp:ListItem Value="QL_trnarspmst">ACCOUNT RECEIVABLE - SP</asp:ListItem>
<asp:ListItem Value="QL_trnaritemmst">ACCOUNT RECEIVABLE - FG</asp:ListItem>
<asp:ListItem Value="QL_trnarassetmst">ACCOUNT RECEIVABLE - ASSETS</asp:ListItem>
<asp:ListItem Value="QL_trnsretrawmst">SALES RETURN - RM</asp:ListItem>
<asp:ListItem Value="QL_trnsretgenmst">SALES RETURN - GM</asp:ListItem>
<asp:ListItem Value="QL_trnsretspmst">SALES RETURN - SP</asp:ListItem>
<asp:ListItem Value="QL_trnsretitemmst">SALES RETURN - FG</asp:ListItem>
<asp:ListItem Value="QL_trnsretassetmst">SALES RETURN - ASSETS</asp:ListItem>
    <asp:ListItem Value="QL_trnplanmst">KIK PLANNING DATE</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblType" runat="server" Text="Type" Visible="False"></asp:Label></TD><TD class="Label" align=right></TD><TD class="Label" align=left><asp:DropDownList id="ddlapppersontype" runat="server" CssClass="inpText" Width="75px" Visible="False" AutoPostBack="True"><asp:ListItem Enabled="False">Process</asp:ListItem>
<asp:ListItem Selected="True">Final</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text="Level" Visible="False"></asp:Label></TD><TD class="Label" align=right></TD><TD class="Label" align=left><asp:DropDownList id="apppersonlevel" runat="server" CssClass="inpText" Width="75px" Visible="False"><asp:ListItem Value="1">Level 1</asp:ListItem>
<asp:ListItem Value="2">Level 2</asp:ListItem>
<asp:ListItem Value="3">Level 3</asp:ListItem>
<asp:ListItem Value="4">Level 4</asp:ListItem>
<asp:ListItem Value="5">Level 5</asp:ListItem>
<asp:ListItem Value="6">Level 6</asp:ListItem>
<asp:ListItem Value="7">Level 7</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblStatus" runat="server" Text="Status"></asp:Label></TD><TD class="Label" align=right>:</TD><TD class="Label" align=left><asp:DropDownList id="ddlapppersonstatus" runat="server" CssClass="inpText" Width="75px"><asp:ListItem>Active</asp:ListItem>
<asp:ListItem>Non Active</asp:ListItem>
<asp:ListItem>Suspended</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=right></TD><TD class="Label" align=left><asp:Label id="lastuser" runat="server" Visible="False"></asp:Label> <asp:Label id="lasttable" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Label id="lblWarning" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=3></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update&nbsp;By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Approval User :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

