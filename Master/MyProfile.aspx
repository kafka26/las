<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="MyProfile.aspx.vb" Inherits="Master_MyProfile" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tableutama" align="center"
        class="tabelhias" width="100%">
        <tr>
            <th class="header" colspan="5" valign="top" align="left">
                <asp:Label ID="Label1" runat="server" CssClass="Title" Font-Bold="False"
                    Text=".: My Profile"></asp:Label></th>
        </tr>
        <tr>
            <th colspan="5" valign="top" align="left">
                <table>
                    <tr>
                        <td align="left" style="height: 10px" colspan="5">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="font-weight: bold; font-size: 9pt;" colspan="5">
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Menu.gif" />
                            Data Profile :.</td>
                    </tr>
                    <tr>
                        <td align="left">
                            User ID</td>
                        <td align="left">
                            :</td>
                        <td align="left">
                            <asp:Label ID="userid" runat="server"></asp:Label></td>
                        <td align="left">
                        </td>
                        <td align="left">
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            User Name</td>
                        <td align="left">
                            :</td>
                        <td align="left">
                            <asp:TextBox ID="username" runat="server" CssClass="inpText" MaxLength="50" Width="150px"></asp:TextBox></td>
                        <td align="left">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="username"
                                ErrorMessage="* Fill your user name !!"></asp:RequiredFieldValidator></td>
                        <td align="left">
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="username" ValidChars="1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM -_">
                            </ajaxToolkit:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr style="color: #000099">
                        <td align="left" style="height: 10px" colspan="5">
                        </td>
                    </tr>
                    <tr style="color: #000099">
                        <td align="left" style="font-weight: bold; font-size: 9pt;" colspan="5">
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Menu.gif" />
                            Ganti Password :.</td>
                    </tr>
                    <tr>
                        <td align="left">
                            Old Password</td>
                        <td align="left">
                            :</td>
                        <td align="left">
                            <asp:TextBox ID="oldPassword" runat="server" CssClass="inpText" MaxLength="20" Width="150px" TextMode="Password"></asp:TextBox></td>
                        <td align="left">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="oldPassword"
                                ErrorMessage="* Fill your old password !!"></asp:RequiredFieldValidator></td>
                        <td align="left"><ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="oldPassword" ValidChars="1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM ">
                        </ajaxToolkit:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            New Password</td>
                        <td align="left">
                            :</td>
                        <td align="left">
                            <asp:TextBox ID="Password1" runat="server" CssClass="inpText" MaxLength="20" Width="150px" TextMode="Password"></asp:TextBox></td>
                        <td align="left">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="Password1"
                                ErrorMessage="* Fill your new password !!"></asp:RequiredFieldValidator></td>
                        <td align="left"><ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="Password1" ValidChars="1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM ">
                        </ajaxToolkit:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Confirm New Password&nbsp;</td>
                        <td align="left">
                            :</td>
                        <td align="left">
                            <asp:TextBox ID="Password2" runat="server" CssClass="inpText" MaxLength="20" Width="150px" TextMode="Password"></asp:TextBox></td>
                        <td align="left">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="Password2"
                                ErrorMessage="* Fill again your new password !!"></asp:RequiredFieldValidator></td>
                        <td align="left"><ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="Password2" ValidChars="1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM ">
                        </ajaxToolkit:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                        </td>
                        <td align="left">
                        </td>
                        <td align="left" colspan="3">
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="Password2"
                                ControlToValidate="Password1" ErrorMessage="* Your new password don't match with the confirmation password !!"></asp:CompareValidator></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="5">
                            <asp:Label ID="lblWarning" runat="server" ForeColor="Red"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="5" style="height: 10px">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="5">
                            <asp:ImageButton ID="imbSave" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/btnsave.bmp" />
                            <asp:ImageButton ID="imbBack" runat="server" CausesValidation="False" ImageAlign="AbsMiddle"
                                ImageUrl="~/Images/Back.png" />
                            <asp:Button ID="btnUpdateInitStock" runat="server" CausesValidation="False" Text="UPDATE INIT STOCK VALUE"
                                Width="200px" /></td>
                    </tr>
                </table>
            </th>
        </tr>
    </table>
</asp:Content>

