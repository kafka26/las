Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO

Partial Class Master_BillOfMaterial
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
    Const iRoundDigit = 4
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsDetailProcessInputValid() As Boolean
        Dim sError As String = ""
        If bomdtl1res1.Text = "" Then
            sError &= "- Please fill SEQUENCE field!<BR>"
        End If
        If bomdtl1deptoid.SelectedValue = "" Then
            sError &= "- Please select FROM DEPARTMENT field!<BR>"
        End If
        If bomdtl1todeptoid.SelectedValue = "" Then
            sError &= "- Please select TO DEPARTMENT field!<BR>"
        End If
        'If bomdtl1refoid.Text = "" Then
        '    sError &= "- Please select PROCESS OUTPUT field!<BR>"
        'End If
        'If bomdtl1refqty.Text = "" Then
        '    sError &= "- Please fill QUANTITY of PROCESS OUTPUT field!<BR>"
        'Else
        '    If ToDouble(bomdtl1refqty.Text) <= 0 Then
        '        sError &= "- QUANTITY of PROCESS OUTPUT must be more than 0!<BR>"
        '    Else
        '        If Not IsQtyRounded(ToDouble(bomdtl1refqty.Text), ToDouble(roundqtydtl1.Text)) Then
        '            sError &= "- QUANTITY of PROCESS OUTPUT must be rounded by ROUNDING QTY!<BR>"
        '        Else
        '            Dim sErrReply As String = ""
        '            If Not isLengthAccepted("bomdtl1refqty", "QL_mstbomdtl1", ToDouble(bomdtl1refqty.Text), sErrReply) Then
        '                sError &= "- QUANTITY of PROCESS OUTPUT must be less than MAX QUANTITY (" & sErrReply & ") allowed stored in database!<BR>"
        '            End If
        '        End If
        '    End If
        'End If
        'If bomdtl1refunitoid.SelectedValue = "" Then
        '    sError &= "- Please select UNIT of PROCESS OUTPUT field!<BR>"
        'End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsDetailMatInputValid() As Boolean
        Dim sError As String = ""
        If DDLProcess.SelectedValue = "" Then
            sError &= "- Please select PROCESS field!<BR>"
        End If
        If bomdtl2refoid.Text = "" Then
            sError &= "- Please select MATERIAL field!<BR>"
        End If
        If bomdtl2refqty.Text = "" Then
            sError &= "- Please fill QUANTITY of MATERIAL field!<BR>"
        Else
            If ToDouble(bomdtl2refqty.Text) <= 0 Then
                sError &= "- QUANTITY of MATERIAL must be more than 0!<BR>"
            Else
                Dim sErrReply As String = ""
                If Not isLengthAccepted("bomdtl2refqty", "QL_mstbomdtl2", ToDouble(bomdtl2refqty.Text), sErrReply) Then
                    sError &= "- QUANTITY of MATERIAL must be less than MAX QUANTITY (" & sErrReply & ") allowed stored in database!<BR>"
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsDetailMatInputValid2() As Boolean
        Dim sError As String = ""
        If DDLProcess.SelectedValue = "" Then
            sError &= "- Please select PROCESS field!<BR>"
        End If
        If bomdtl3refoid.Text = "" Then
            sError &= "- Please select MATERIAL field!<BR>"
        End If
        If bomdtl3refqty.Text = "" Then
            sError &= "- Please fill QUANTITY of MATERIAL field!<BR>"
        Else
            If ToDouble(bomdtl3refqty.Text) <= 0 Then
                sError &= "- QUANTITY of MATERIAL must be more than 0!<BR>"
            Else
                Dim sErrReply As String = ""
                If Not isLengthAccepted("bomdtl3refqty", "QL_mstbomdtl3", ToDouble(bomdtl3refqty.Text), sErrReply) Then
                    sError &= "- QUANTITY of MATERIAL must be less than MAX QUANTITY (" & sErrReply & ") allowed stored in database!<BR>"
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If bomdate.Text = "" Then
            sError &= "- Please fill BOM DATE field!<BR>"
        Else
            If Not IsValidDate(bomdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- BOM DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If itemoid.Text = "" Then
            sError &= "- Please select FINISH GOOD field!<BR>"
        End If
        'If itemunit.Text = "" Then
        '    sError &= "- Please select UNIT of FINISH GOOD field!<BR>"
        'End If
        'If nomorlot.Text = "" Then
        '    sError &= "- Please Fill Nomor Lot Field!<BR>"
        'End If
        'If bomqty.Text = "" Then
        '    sError &= "- Please Fill Qty Field!<BR>"
        'Else
        '    If ToDouble(bomqty.Text) <= 0 Then
        '        sError &= "- BOM Qty Must more than 0!<BR>"
        '    End If
        'End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill DETAIL PROCESS!<BR>"
        Else
            Dim objTbl As DataTable = Session("TblDtl")
            If objTbl.Rows.Count <= 0 Then
                sError &= "- Please fill DETAIL PROCESS!<BR>"
            Else
                objTbl.DefaultView.RowFilter = "bomdtl1reftype='FG'"
                If objTbl.DefaultView.Count <> 1 Then
                    sError &= "- Please define only 1 detail process where Process Type='FG'!<BR>"
                End If
                objTbl.DefaultView.RowFilter = ""
                Dim iSelectedDiv As Integer = ToInteger(GetStrData("SELECT groupoid FROM QL_mstdeptgroupdtl WHERE deptoid=" & objTbl.Rows(0)("bomdtl1deptoid") & ""))
                If objTbl.Rows.Count > 1 Then
                    For C1 As Integer = 1 To objTbl.Rows.Count - 1
                        If iSelectedDiv <> ToInteger(GetStrData("SELECT groupoid FROM QL_mstdeptgroupdtl WHERE deptoid=" & objTbl.Rows(C1)("bomdtl1deptoid") & "")) Then
                            sError &= "- Every selected FROM DEPARTMENT in DETAIL PROCESS must be in one Division!<BR>" : Exit For
                        End If
                    Next
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetTextValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function GetDDLValue(ByVal cc As System.Web.UI.ControlCollection, Optional ByVal sText As String = "") As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                If sText = "" Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
                Else
                    sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedItem.Text
                End If
            End If
        Next
        Return sReturn
    End Function

    Private Function IsProcessExistsInDLC(ByVal sDept As String) As Boolean
        sSql = "SELECT COUNT(*) FROM QL_mstdlcdtl WHERE deptoid=" & sDept & " AND dlcoid IN (SELECT dlcoid FROM QL_mstdlc WHERE bomoid=" & Session("oid") & ")"
        If ToDouble(GetStrData(sSql).ToString) > 0 Then
            Return True
        End If
        Return False
    End Function

    Private Function IsFolderExists(ByVal sFolder As String) As Boolean
        Dim fso = CreateObject("Scripting.FileSystemObject")
        If (fso.FolderExists(sFolder)) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Overloads Function CheckMaterial(ByRef sOid As String, ByVal sCode As String, ByRef sDesc As String, ByRef iUnitID As Integer, ByRef sUnit As String, ByVal sType As String, ByRef dRoundQty As Double) As Boolean
        If sType = "raw" Then
            sType = "matraw"
        ElseIf sType = "general" Then
            sType = "matgen"
        Else
            sType = "sparepart"
        End If
        Try
            sSql = "SELECT TOP 1 " & sType & "oid matoid, " & sType & "longdesc matlongdesc, " & sType & "unitoid matunitoid, gendesc matunit, " & sType & "limitqty matlimitqty FROM QL_mst" & sType & " m INNER JOIN QL_mstgen g ON genoid=" & sType & "unitoid WHERE m.cmpcode='" & CompnyCode & "' AND " & sType & "code='" & sCode & "' AND m.activeflag='ACTIVE'"
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            While xreader.Read
                sOid = xreader("matoid").ToString
                sDesc = xreader("matlongdesc").ToString
                iUnitID = ToInteger(xreader("matunitoid").ToString)
                sUnit = xreader("matunit").ToString
                dRoundQty = ToDouble(xreader("matlimitqty").ToString)
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            Return False
        End Try
        If sOid = "" Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Function GetPreCostDtlID(ByVal sMatOid As String, ByVal sMatType As String) As Integer
        If sMatType = "raw" Then
            sSql = "SELECT precostdtlrefoid FROM QL_mstprecostdtl prd INNER JOIN QL_mstcat1 c1 ON c1.cat1oid=precostdtlrefoid AND cat1res1='Raw' AND cat1res2 IN ('WIP', 'Non WIP') INNER JOIN QL_mstmatraw m ON LEFT(matrawcode, 2)=cat1code WHERE prd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND prd.precostoid=" & precostoid.Text & " AND matrawoid=" & sMatOid & ""
        ElseIf sMatType = "general" Then
            sSql = "SELECT precostdtlrefoid FROM QL_mstprecostdtl prd INNER JOIN QL_mstcat1 c1 ON c1.cat1oid=precostdtlrefoid AND cat1res1='General' AND cat1res2 IN ('WIP', 'Non WIP') INNER JOIN QL_mstmatgen m ON LEFT(matgencode, 2)=cat1code WHERE prd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND prd.precostoid=" & precostoid.Text & " AND matgenoid=" & sMatOid & ""
        Else
            sSql = "SELECT precostdtlrefoid FROM QL_mstprecostdtl prd INNER JOIN QL_mstcat1 c1 ON c1.cat1oid=precostdtlrefoid AND cat1res1='Spare Part' AND cat1res2 IN ('WIP', 'Non WIP') INNER JOIN QL_mstsparepart m ON LEFT(sparepartcode, 2)=cat1code WHERE prd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND prd.precostoid=" & precostoid.Text & " AND sparepartoid=" & sMatOid & ""
        End If
        Return ToInteger(GetStrData(sSql))
    End Function

    Private Function CheckDepartment(ByVal sDept As String, ByRef iSeq As Integer, ByRef sProcess As String) As String
        Dim sReply As String = ""
        Dim iDeptID As Integer = ToInteger(GetStrData("SELECT TOP 1 deptoid FROM QL_mstdept WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND deptcode='" & Tchar(sDept) & "' AND activeflag='ACTIVE'"))
        If iDeptID <> 0 Then
            If Session("TblDtl") IsNot Nothing Then
                Dim dv As DataView = Session("TblDtl").DefaultView
                dv.RowFilter = "bomdtl1deptoid=" & iDeptID
                If dv.Count > 0 Then
                    iSeq = ToInteger(dv(0)("bomdtl1seq").ToString)
                    sProcess = dv(0)("bomdtl1desc").ToString
                Else
                    sReply = "Department can't be found in detail process, "
                End If
                dv.RowFilter = ""
            End If
        Else
            sReply = "No department, "
        End If
        Return sReply
    End Function

    Public Function GetParameterID() As String
        Return Eval("matrefoid") & "," & Eval("matrefunitoid")
    End Function

#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitAllDDL()
        ' Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If FillDDL(DDLBusUnit, sSql) Then
            InitDDLDept()
        End If
        ' Fill DDL Unit Material
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='UNIT' AND activeflag='ACTIVE'"
        FillDDL(bomdtl2refunitoid, sSql)
        FillDDL(bomdtl3refunitoid, sSql)
    End Sub

    Private Sub InitDDLUnit2()
        'InitDDLUnit()
        ' Fill DDL Unit Finish Good
        sSql = "select itemUnit1, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit1 where itemoid=" & itemoid.Text & " union all select itemUnit2, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit2 where itemoid=" & itemoid.Text & " union all select itemUnit3, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit3 where itemoid=" & itemoid.Text & ""
        FillDDL(itemunitoid, sSql)
    End Sub

    Private Sub InitDDLDept()
        ' Fill DDL Department
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE activeflag='ACTIVE' AND cmpcode='" & DDLBusUnit.SelectedValue & "'"
        If FillDDL(bomdtl1deptoid, sSql) Then
            InitDDLToDept()
        End If
    End Sub

    Private Sub InitDDLToDept(Optional ByVal sDeptEdit As String = "")
        ' Fill DDL Department
        If bomdtl1reftype.SelectedValue = "FG" Then
            sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE activeflag='ACTIVE' AND cmpcode='" & DDLBusUnit.SelectedValue & "' AND deptoid=" & bomdtl1deptoid.SelectedValue
        Else
            sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE activeflag='ACTIVE' AND cmpcode='" & DDLBusUnit.SelectedValue & "' AND deptoid<>" & bomdtl1deptoid.SelectedValue
            Dim sOid As String = ""
            If Session("TblDtl") IsNot Nothing Then
                Dim dt As DataTable = Session("TblDtl")
                For C1 As Integer = 0 To dt.Rows.Count - 1
                    If sDeptEdit <> dt.Rows(C1)("bomdtl1deptoid").ToString Then
                        If ToInteger(bomdtl1res1.Text) > ToInteger(dt.Rows(C1)("bomdtl1res1").ToString) Then
                            sOid &= dt.Rows(C1)("bomdtl1deptoid").ToString & ","
                        End If
                    End If
                Next
            End If
            If sOid <> "" Then
                sOid = Left(sOid, sOid.Length - 1)
                sSql &= " AND deptoid NOT IN (" & sOid & ")"
            End If
        End If
        FillDDL(bomdtl1todeptoid, sSql)
    End Sub

    Private Sub InitDDLUnit()
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        If bomdtl1reftype.SelectedValue = "FG" Then
            sSql &= " AND gengroup='UNIT'"
            bomdtl1refqty.Text = "1.0000"
            bomdtl1refqty.CssClass = "inpTextDisabled" : bomdtl1refqty.Enabled = False
            roundqtydtl1.Text = "1.0000"
        Else
            sSql &= " AND gengroup='UNIT'"
            bomdtl1refqty.Text = ""
            bomdtl1refqty.CssClass = "inpText" : bomdtl1refqty.Enabled = True
        End If
        FillDDL(bomdtl1refunitoid, sSql)
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT * FROM( SELECT bom.cmpcode, bom.bomoid, bom.bomdesc, CONVERT(CHAR(10), bom.bomdate, 101) AS bomdate, i.itemcode AS itemcode, i.itemlongdescription AS itemshortdesc, ISNULL(bom.dlcstatus, '') AS dlcstatus, bom.activeflag, bom.bomnote, 'False' AS checkvalue FROM QL_mstbom bom INNER JOIN QL_mstitem i ON i.itemoid=bom.itemoid WHERE bom.bomres1='FG' UNION ALL SELECT bom.cmpcode, bom.bomoid, bom.bomdesc, CONVERT(CHAR(10), bom.bomdate, 101) AS bomdate, i.itemcode AS itemcode, i.itemlongdescription AS itemshortdesc, ISNULL(bom.dlcstatus, '') AS dlcstatus, bom.activeflag, bom.bomnote, 'False' AS checkvalue FROM QL_mstbom bom INNER JOIN QL_mstitem i ON i.itemoid=bom.itemoid WHERE bom.bomres1='WIP') AS tbl"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " WHERE tbl.cmpcode='" & Session("CompnyCode") & "'"
        Else
            sSql &= " WHERE tbl.cmpcode='" & Session("CompnyCode") & "'"
        End If
        sSql &= sSqlPlus & " ORDER BY CAST(tbl.bomdate AS DATETIME) DESC, tbl.bomoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_mstbom")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "bomoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub BindPreCostingData()
        sSql = "SELECT precostoid, precostdesc, CONVERT(VARCHAR(10), precostdate, 101) AS precostdate, i.itemoid, itemcode, itemlongdescription AS itemlongdesc, itemunit1 AS itemunitoid, precostnote, pr.updtime , pr.upduser FROM QL_mstprecost pr INNER JOIN QL_mstitem i ON i.itemoid=pr.itemoid WHERE pr.cmpcode='" & DDLBusUnit.SelectedValue & "' AND " & FilterDDLListPC.SelectedValue & " LIKE '%" & Tchar(FilterTextListPC.Text) & "%' AND pr.activeflag='ACTIVE' AND precostoid NOT IN (SELECT precostoid FROM QL_mstbom WHERE cmpcode='" & DDLBusUnit.SelectedValue & "') ORDER BY precostoid"
        FillGV(gvListPC, sSql, "QL_mstprecost")
    End Sub

    Private Sub InitBOMDesc()
        If DDLBusUnit.SelectedValue <> "" Then
            sSql = "SELECT ISNULL(MAX(bomcounter), 0) FROM QL_mstbom WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND itemoid=" & itemoid.Text
            bomcounter.Text = ToDouble(CStr(GetStrData(sSql))) + 1
            bomdesc.Text = "BOM " & itemshortdesc.Text & " " & bomcounter.Text
        End If
    End Sub

    Private Sub InitDDLCat01WIP()
        'Filter WIP di tutup
        sSql = "SELECT cat1oid, (cat1code + ' - ' + cat1shortdesc) AS cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat1res1='Raw' /*AND cat1res2='WIP'*/ ORDER BY cat1code"
        If FillDDL(FilterDDLCat01ListWIP, sSql) Then
            InitDDLCat02WIP()
        Else
            FilterDDLCat02ListWIP.Items.Clear()
            FilterDDLCat03ListWIP.Items.Clear()
            FilterDDLCat04ListWIP.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat02WIP()
        sSql = "SELECT cat2oid, (cat2code + ' - ' + cat2shortdesc) AS cat2shortdest FROM QL_mstcat2 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat2res1='Raw' AND cat1oid=" & FilterDDLCat01ListWIP.SelectedValue & " ORDER BY cat2code"
        If FillDDL(FilterDDLCat02ListWIP, sSql) Then
            InitDDLCat03WIP()
        Else
            FilterDDLCat03ListWIP.Items.Clear()
            FilterDDLCat04ListWIP.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat03WIP()
        sSql = "SELECT cat3oid, (cat3code + ' - ' + cat3shortdesc) AS cat3shortdest FROM QL_mstcat3 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat3res1='Raw' AND cat1oid=" & FilterDDLCat01ListWIP.SelectedValue & " AND cat2oid=" & FilterDDLCat02ListWIP.SelectedValue & " ORDER BY cat3code"
        If FillDDL(FilterDDLCat03ListWIP, sSql) Then
            InitDDLCat04WIP()
        Else
            FilterDDLCat04ListWIP.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat04WIP()
        sSql = "SELECT cat4oid, (cat4code + ' - ' + cat4shortdesc) AS cat4shortdest FROM QL_mstcat4 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat4res1='Raw' AND cat1oid=" & FilterDDLCat01ListWIP.SelectedValue & " AND cat2oid=" & FilterDDLCat02ListWIP.SelectedValue & " AND cat3oid=" & FilterDDLCat03ListWIP.SelectedValue & " ORDER BY cat4code"
        FillDDL(FilterDDLCat04ListWIP, sSql)
    End Sub

    Private Sub BindWIPData()
        sSql = "SELECT itemoid AS matrawoid, itemcode AS matrawcode, itemlongdescription AS matrawlongdesc, itemunit1 AS matrawunitoid, g.gendesc AS matrawunit, roundqty AS matrawlimitqty FROM QL_mstitem m INNER JOIN QL_mstgen g ON g.cmpcode=m.cmpcode AND genoid=itemunit1 WHERE m.cmpcode='" & CompnyCode & "' AND m.itemgroup='WIP' AND m.itemrecordstatus='ACTIVE' AND " & FilterDDLListWIP.SelectedValue & " LIKE '%" & Tchar(FilterTextListWIP.Text) & "%'"
        If cbCat01ListWIP.Checked Then
            If FilterDDLCat01ListWIP.SelectedValue <> "" Then
                sSql &= " AND SUBSTRING(itemcode, 1, 2)='" & Left(FilterDDLCat01ListWIP.SelectedItem.Text, 2) & "'"
            End If
        End If
        If cbCat02ListWIP.Checked Then
            If FilterDDLCat02ListWIP.SelectedValue <> "" Then
                sSql &= IIf(cbCat01ListWIP.Checked, "", " AND SUBSTRING(itemcode, 1, 2)='" & Left(FilterDDLCat01ListWIP.SelectedItem.Text, 2) & "'") & " AND SUBSTRING(itemcode, 4, 3)='" & Left(FilterDDLCat02ListWIP.SelectedItem.Text, 3) & "'"
            End If
        End If
        If cbCat03ListWIP.Checked Then
            If FilterDDLCat03ListWIP.SelectedValue <> "" Then
                sSql &= IIf(cbCat01ListWIP.Checked, "", " AND SUBSTRING(itemcode, 1, 2)='" & Left(FilterDDLCat01ListWIP.SelectedItem.Text, 2) & "'") & IIf(cbCat02ListWIP.Checked, "", " AND SUBSTRING(itemcode, 4, 3)='" & Left(FilterDDLCat02ListWIP.SelectedItem.Text, 3) & "'") & " AND SUBSTRING(itemcode, 8, 4)='" & Left(FilterDDLCat03ListWIP.SelectedItem.Text, 4) & "'"
            End If
        End If
        If cbCat04ListWIP.Checked Then
            If FilterDDLCat04ListWIP.SelectedValue <> "" Then
                sSql &= IIf(cbCat01ListWIP.Checked, "", " AND SUBSTRING(itemcode, 1, 2)='" & Left(FilterDDLCat01ListWIP.SelectedItem.Text, 2) & "'") & IIf(cbCat02ListWIP.Checked, "", " AND SUBSTRING(itemcode, 4, 3)='" & Left(FilterDDLCat02ListWIP.SelectedItem.Text, 3) & "'") & IIf(cbCat03ListWIP.Checked, "", " AND SUBSTRING(itemcode, 8, 4)='" & Left(FilterDDLCat03ListWIP.SelectedItem.Text, 4) & "'") & " AND SUBSTRING(itemcode, 13, 4)='" & Left(FilterDDLCat04ListWIP.SelectedItem.Text, 4) & "'"
            End If
        End If
        sSql &= " ORDER BY itemcode"
        FillGV(gvListWIP, sSql, "QL_mstmatraw")
    End Sub

    Private Sub InitDDLCat01ListMat()
        'Fill DDL Category 1
        sSql = "SELECT c1.cat1oid, c1.cat1code +' - '+ c1.cat1shortdesc FROM QL_mstcat1 c1 WHERE c1.cmpcode='" & CompnyCode & "' AND c1.activeflag='ACTIVE'"
        If bomdtl2reftype.SelectedValue = "Raw" Then
            sSql &= " AND cat1res1='Raw' /*AND cat1res2 IN ('WIP', 'Non WIP')*/"
        Else
            sSql &= " AND cat1res1='" & bomdtl2reftype.SelectedValue & "'"
        End If
        sSql &= " ORDER BY cat1code"
        If FillDDL(FilterDDLCat01ListMat, sSql) Then
            InitDDLCat02ListMat()
        Else
            FilterDDLCat02ListMat.Items.Clear()
            FilterDDLCat03ListMat.Items.Clear()
            FilterDDLCat04ListMat.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat02ListMat()
        'Fill DDL Category 2
        sSql = "SELECT cat2oid, cat2code +' - '+ cat2shortdesc FROM QL_mstcat2 c2 INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c1.cat1oid=c2.cat1oid WHERE c2.cmpcode='" & CompnyCode & "' AND c2.activeflag='ACTIVE' AND c1.cat1oid='" & FilterDDLCat01ListMat.SelectedValue & "'"
        If bomdtl2reftype.SelectedValue = "Raw" Then
            sSql &= " AND cat2res1='Raw' AND cat1res1='Raw' /*AND cat1res2 IN ('WIP', 'Non WIP')*/"
        Else
            sSql &= " AND cat2res1='" & bomdtl2reftype.SelectedValue & "' AND cat1res1='" & bomdtl2reftype.SelectedValue & "'"
        End If
        sSql &= " ORDER BY cat2code"
        If FillDDL(FilterDDLCat02ListMat, sSql) Then
            InitDDLCat03ListMat()
        Else
            FilterDDLCat03ListMat.Items.Clear()
            'FilterDDLCat04ListMat.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat03ListMat()
        'Fill DDL Category 3
        sSql = "SELECT cat3oid, cat3code +' - '+ cat3shortdesc FROM QL_mstcat3 c3 INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c3.cmpcode AND c1.cat1oid=c3.cat1oid WHERE c3.cmpcode='" & CompnyCode & "' AND c3.activeflag='ACTIVE' AND c2.cat2oid='" & FilterDDLCat02ListMat.SelectedValue & "'"
        If bomdtl2reftype.SelectedValue = "Raw" Then
            sSql &= " AND cat3res1='Raw' AND cat2res1='Raw' AND cat1res1='Raw' /*AND cat1res2 IN ('WIP', 'Non WIP')*/"
        Else
            sSql &= " AND cat3res1='" & bomdtl2reftype.SelectedValue & "' AND cat2res1='" & bomdtl2reftype.SelectedValue & "' AND cat1res1='" & bomdtl2reftype.SelectedValue & "'"
        End If
        sSql &= " ORDER BY cat3code"
        If FillDDL(FilterDDLCat03ListMat, sSql) Then
            InitDDLCat04ListMat()
        Else
            FilterDDLCat04ListMat.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat04ListMat()
        'Fill DDL Category 4
        sSql = "SELECT genoid, gencode +' - '+ gendesc FROM QL_mstgen WHERE gengroup='CAT4' AND activeflag='ACTIVE' ORDER BY gencode"
        FillDDL(FilterDDLCat04ListMat, sSql)
    End Sub

    Private Sub BindMatData(ByVal sRef As String)
        sSql = "SELECT 'False' AS checkvalue, itemcat1, itemcat2, itemcat3, itemcat4, m.itemoid AS matrefoid, m.itemCode AS matrefcode, m.itemLongDescription AS matrefshortdesc, m.itemUnit1 AS matrefunitoid, g.gendesc AS matrefunit, 0.0 AS bomdtl2qty, '' AS bomdtl2note, roundQty AS roundqtydtl2, 0 AS precostdtlrefoid FROM QL_mstcat1 c1 /*AND cat1res2 IN ('WIP', 'Non WIP')*/ INNER JOIN QL_mstitem m ON m.itemCat1=c1.cat1oid AND cat1res1='" & bomdtl2reftype.SelectedValue & "' INNER JOIN QL_mstgen g ON genoid=itemUnit1 WHERE m.cmpcode='" & DDLBusUnit.SelectedValue & "' ORDER BY m.itemCode"
      
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstmat")
        If dt.Rows.Count > 0 Then
            Session("TblListMat") = dt
            Session("TblListMatView") = dt
            gvListMat2.DataSource = Session("TblListMatView")
            gvListMat2.DataBind()
        Else
            Session("ErrMat") = "Material data can't be found!"
            showMessage(Session("ErrMat"), 2)
        End If
    End Sub

    Private Sub InitDDLCat01ListMat2()
        'Fill DDL Category 1
        sSql = "SELECT cat1code, cat1shortdesc FROM QL_mstcat1 c1 INNER JOIN QL_mstprecostdtl prd ON precostdtlrefoid=c1.cat1oid WHERE c1.cmpcode='" & CompnyCode & "' AND c1.activeflag='ACTIVE' AND prd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND precostoid=" & precostoid.Text & " AND cat1res1='" & bomdtl3reftype.SelectedValue & "' AND cat1res2 IN ('Log', 'Sawn Timber')"
        If FillDDL(FilterDDLCat01ListMat2, sSql) Then
            InitDDLCat02ListMat2()
        Else
            FilterDDLCat02ListMat2.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat02ListMat2()
        'Fill DDL Category 2
        sSql = "SELECT cat1code + '.' + cat2code, cat2shortdesc FROM QL_mstcat2 c2 INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c1.cat1oid=c2.cat1oid WHERE c2.cmpcode='" & CompnyCode & "' AND c2.activeflag='ACTIVE' AND cat1code='" & FilterDDLCat01ListMat2.SelectedValue & "' AND cat2res1='" & bomdtl3reftype.SelectedValue & "' AND cat1res1='" & bomdtl3reftype.SelectedValue & "' AND cat1res2 IN ('Log', 'Sawn Timber')"
        FillDDL(FilterDDLCat02ListMat2, sSql)
    End Sub

    Private Sub BindMatData2()
        sSql = "SELECT 'False' AS checkvalue, cat2oid AS matrefoid2, (cat1code + '.' + cat2code) AS matrefcode2, RTRIM((CASE WHEN (LTRIM(cat1shortdesc)='None' OR LTRIM(cat1shortdesc)='') THEN '' ELSE cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat2shortdesc)='None' OR LTRIM(cat2shortdesc)='') THEN '' ELSE cat2shortdesc + ' ' END)) AS matrefshortdesc2, (SELECT TOP 1 genoid FROM QL_mstgen g WHERE g.cmpcode='" & CompnyCode & "' AND g.activeflag='ACTIVE' AND gengroup='MATERIAL UNIT') AS matrefunitoid2, (SELECT TOP 1 gendesc FROM QL_mstgen g WHERE g.cmpcode='" & CompnyCode & "' AND g.activeflag='ACTIVE' AND gengroup='MATERIAL UNIT') AS matrefunit2, 0.00 AS bomdtl3qty, '' AS bomdtl3note, precostdtlrefoid FROM QL_mstprecostdtl prd INNER JOIN QL_mstcat1 c1 ON c1.cat1oid=precostdtlrefoid AND cat1res1='Raw' AND cat1res2 IN ('Log', 'Sawn Timber') INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c1.cmpcode AND c2.cat1oid=c1.cat1oid WHERE prd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND prd.precostoid=" & precostoid.Text & " /*AND precostdtlreftype='" & bomdtl3reftype.SelectedValue & "'*/ ORDER BY matrefcode2"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstmat2")
        If dt.Rows.Count > 0 Then
            Session("TblListMat2") = dt
            Session("TblListMatView2") = dt
            gvListMat3.DataSource = Session("TblListMatView2")
            gvListMat3.DataBind()
        Else
            Session("ErrMat2") = "Material data can't be found!"
            showMessage(Session("ErrMat2"), 2)
        End If
    End Sub

    Private Sub CreateTblDetailProcess()
        Dim dtlTable As DataTable = New DataTable("QL_mstbomdtl1")
        dtlTable.Columns.Add("bomdtl1seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("bomdtl1desc", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl1deptoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("bomdtl1dept", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl1todeptoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("bomdtl1todept", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl1reftype", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl1note", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl1res1", Type.GetType("System.Int32"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub UpdateDetailMat(ByVal sDesc As String)
        If Not Session("TblDtl2") Is Nothing Then
            Dim dt As DataTable = Session("TblDtl2")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "bomdtl1seq=" & bomdtl1seq.Text
            For C1 As Integer = 0 To dv.Count - 1
                dv(C1)("bomdtl1process") = sDesc
            Next
            dv.RowFilter = ""
            Session("TblDtl2") = dv.ToTable
            GVDtl2.DataSource = Session("TblDtl2")
            GVDtl2.DataBind()
        End If
        If Not Session("TblDtl3") Is Nothing Then
            Dim dt As DataTable = Session("TblDtl3")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "bomdtl1seq=" & bomdtl1seq.Text
            For C1 As Integer = 0 To dv.Count - 1
                dv(C1)("bomdtl1process") = sDesc
            Next
            dv.RowFilter = ""
            Session("TblDtl3") = dv.ToTable
            GVDtl3.DataSource = Session("TblDtl3")
            GVDtl3.DataBind()
        End If
    End Sub

    Private Sub ClearDetail()
        bomdtl1seq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                EnableHeader(False)
            Else
                EnableHeader(True)
            End If
            bomdtl1seq.Text = objTable.Rows.Count + 1
        Else
            EnableHeader(True)
        End If
        i_u2.Text = "New Detail"
        bomdtl1oid.Text = ""
        bomdtl1deptoid.SelectedIndex = -1
        bomdtl1reftype.SelectedIndex = -1
        bomdtl1reftype_SelectedIndexChanged(Nothing, Nothing)
        InitDDLToDept()
        bomdtl1todeptoid.SelectedIndex = -1
        bomdtl1note.Text = ""
        bomdtl1res1.Text = ""
        bomdtl1res1_TextChanged(Nothing, Nothing)
        GVDtl1.SelectedIndex = -1
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal
        DDLBusUnit.CssClass = sCss
        'btnSearchPC.Visible = bVal
        'btnClearPC.Visible = bVal
    End Sub

    Private Sub UpdateDetailMat(ByVal iLastSeq As Integer, ByVal iNewSeq As Integer)
        If Not Session("TblDtl2") Is Nothing Then
            Dim dt As DataTable = Session("TblDtl2")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "bomdtl1seq=" & iLastSeq
            dv.AllowEdit = True
            For C1 As Integer = 0 To dv.Count - 1
                dv(0)("bomdtl1seq") = iNewSeq
            Next
            dv.RowFilter = ""
            Session("TblDtl2") = dv.ToTable
            GVDtl2.DataSource = Session("TblDtl2")
            GVDtl2.DataBind()
        End If
        If Not Session("TblDtl3") Is Nothing Then
            Dim dt As DataTable = Session("TblDtl3")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "bomdtl1seq=" & iLastSeq
            dv.AllowEdit = True
            For C1 As Integer = 0 To dv.Count - 1
                dv(0)("bomdtl1seq") = iNewSeq
            Next
            dv.RowFilter = ""
            Session("TblDtl3") = dv.ToTable
            GVDtl3.DataSource = Session("TblDtl3")
            GVDtl3.DataBind()
        End If
    End Sub

    Private Sub DeleteDetailMat(ByVal iSeq As Integer)
        If Not Session("TblDtl2") Is Nothing Then
            Dim dt As DataTable = Session("TblDtl2")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "bomdtl1seq=" & iSeq
            dv.AllowDelete = True
            For C1 As Integer = 0 To dv.Count - 1
                dv.Delete(0)
            Next
            dv.RowFilter = ""
            dv.AllowEdit = True
            For C1 As Integer = 0 To dv.Count - 1
                dv(C1)("bomdtl2seq") = C1 + 1
            Next
            Session("TblDtl2") = dv.ToTable
            GVDtl2.DataSource = Session("TblDtl2")
            GVDtl2.DataBind()
        End If
        If Not Session("TblDtl3") Is Nothing Then
            Dim dt As DataTable = Session("TblDtl3")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "bomdtl1seq=" & iSeq
            dv.AllowDelete = True
            For C1 As Integer = 0 To dv.Count - 1
                dv.Delete(0)
            Next
            dv.RowFilter = ""
            dv.AllowEdit = True
            For C1 As Integer = 0 To dv.Count - 1
                dv(C1)("bomdtl3seq") = C1 + 1
            Next
            Session("TblDtl3") = dv.ToTable
            GVDtl3.DataSource = Session("TblDtl3")
            GVDtl3.DataBind()
        End If
    End Sub

    Private Sub CreateTblDetailMat()
        Dim dtlTable As DataTable = New DataTable("QL_mstbomdtl2")
        dtlTable.Columns.Add("bomdtl2seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("bomdtl2reftype", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl2refoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("bomdtl2refqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("bomdtl2refqty_unitkecil", Type.GetType("System.Double"))
        dtlTable.Columns.Add("bomdtl2refunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("bomdtl2note", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl2refcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl2refdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl2refunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl1seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("bomdtl1process", Type.GetType("System.String"))
        dtlTable.Columns.Add("roundqtydtl2", Type.GetType("System.Double"))
        dtlTable.Columns.Add("precostdtlrefoid", Type.GetType("System.Int32"))
        Session("TblDtl2") = dtlTable
    End Sub

    Private Sub CreateTblDetailMat2()
        Dim dtlTable As DataTable = New DataTable("QL_mstbomdtl3")
        dtlTable.Columns.Add("bomdtl3seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("bomdtl3reftype", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl3refoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("bomdtl3refqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("bomdtl3refunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("bomdtl3note", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl3refcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl3refdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl3refunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl1seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("bomdtl1process", Type.GetType("System.String"))
        dtlTable.Columns.Add("precostdtlrefoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("selectedunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("selectedunitoid", Type.GetType("System.Int32"))
        Session("TblDtl3") = dtlTable
    End Sub

    Private Sub ClearDetail2()
        bomdtl2seq.Text = "1"
        If Session("TblDtl2") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("TblDtl2")
            bomdtl2seq.Text = objTable.Rows.Count + 1
        End If
        i_u3.Text = "New Detail"
        bomdtl2reftype.SelectedIndex = -1
        bomdtl2refdesc.Text = ""
        bomdtl2refoid.Text = ""
        bomdtl2refqty.Text = ""
        bomdtl2refunitoid.SelectedIndex = -1
        bomdtl2note.Text = ""
        roundqtydtl2.Text = ""
        precostdtlrefoid.Text = ""
        GVDtl2.SelectedIndex = -1
        bomdtl2reftype.CssClass = "inpText" : bomdtl2reftype.Enabled = True : btnSearchMat.Visible = True
    End Sub

    Private Sub ClearDetail3()
        bomdtl3seq.Text = "1"
        If Session("TblDtl3") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("TblDtl3")
            bomdtl3seq.Text = objTable.Rows.Count + 1
        End If
        i_u4.Text = "New Detail"
        bomdtl3reftype.SelectedIndex = -1
        bomdtl3refdesc.Text = ""
        bomdtl3refoid.Text = ""
        bomdtl3refqty.Text = ""
        bomdtl3refunitoid.SelectedIndex = -1
        bomdtl3note.Text = ""
        precostdtlrefoid2.Text = ""
        GVDtl3.SelectedIndex = -1
        btnSearchMat2.Visible = True
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        sSql = "SELECT bom.cmpcode, bom.bomoid, bom.bomdate, bom.bomdesc, bom.bomcounter, bom.itemoid, i.itemcode AS itemcode, i.itemlongdescription AS itemshortdesc, bom.itemunitoid, bom.bomnote, bom.activeflag, bom.createuser, bom.createtime, bom.upduser, bom.updtime, bom.updtime AS bomres2, ISNULL(RIGHT(bomres3,3),'') AS bomres3, nomorlot, bomtype, itemqty FROM QL_mstbom bom INNER JOIN QL_mstitem i ON i.itemoid=bom.itemoid WHERE bomoid=" & sOid
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        While xreader.Read
            DDLBusUnit.SelectedValue = xreader("cmpcode").ToString
            InitDDLDept()
            bomoid.Text = Trim(xreader("bomoid").ToString)
            bomdate.Text = Format(xreader("bomdate"), "MM/dd/yyyy")
            bomdesc.Text = Trim(xreader("bomdesc").ToString)
            bomcounter.Text = Trim(xreader("bomcounter").ToString)
            itemoid.Text = Trim(xreader("itemoid").ToString)
            InitDDLUnit2()
            itemunitoid.SelectedValue = (xreader("itemunitoid").ToString)
            itemcode.Text = Trim(xreader("itemcode").ToString)
            itemshortdesc.Text = Trim(xreader("itemshortdesc").ToString)
            precostoid.Text = 0
            lastprecostoid.Text = 0
            bomnote.Text = Trim(xreader("bomnote").ToString)
            activeflag.SelectedValue = Trim(xreader("activeflag").ToString)
            createuser.Text = Trim(xreader("createuser").ToString)
            createtime.Text = Trim(xreader("createtime").ToString)
            upduser.Text = Trim(xreader("upduser").ToString)
            updtime.Text = Trim(xreader("updtime").ToString)
            bomres2.Text = Trim(xreader("bomres2").ToString)
            formulaseq.Text = Trim(xreader("bomres3").ToString)
            nomorlot.Text = Trim(xreader("nomorlot").ToString)
            ddlbomtype.SelectedValue = Trim(xreader("bomtype").ToString)
            bomqty.Text = ToMaskEdit(ToDouble(Trim(xreader("itemqty").ToString)), iRoundDigit)
        End While
        xreader.Close()
        conn.Close()
        EnableHeader(False)
        ' Generate Detail Process
        sSql = "SELECT bod1.bomdtl1seq, bod1.bomdtl1desc, bod1.bomdtl1deptoid, de1.deptname AS bomdtl1dept, (CASE bod1.bomdtl1reftype WHEN 'WIP' THEN bod1.bomdtl1todeptoid ELSE bod1.bomdtl1deptoid END) AS bomdtl1todeptoid, (CASE bod1.bomdtl1reftype WHEN 'WIP' THEN de2.deptname ELSE 'END' END) AS bomdtl1todept, bod1.bomdtl1reftype, bod1.bomdtl1note, CAST(bod1.bomdtl1res1 AS INT) AS bomdtl1res1 FROM QL_mstbomdtl1 bod1 INNER JOIN QL_mstdept de1 ON de1.cmpcode=bod1.cmpcode AND de1.deptoid=bomdtl1deptoid INNER JOIN QL_mstdept de2 ON de2.cmpcode=bod1.cmpcode AND de2.deptoid=bomdtl1todeptoid WHERE bod1.bomoid=" & sOid & " ORDER BY bod1.bomdtl1seq"
        Session("TblDtl") = cKon.ambiltabel(sSql, "QL_mstbomdtl1")
        GVDtl1.DataSource = Session("TblDtl")
        GVDtl1.DataBind()
        DisableGridViewCell()
        sSql = "SELECT bomdtl1seq, bomdtl1desc FROM QL_mstbomdtl1 WHERE bomoid=" & sOid
        FillDDL(DDLProcess, sSql)
        ' Generate Detail Material Level 5
        sSql = "SELECT DISTINCT bod2.bomdtl2seq, bod2.bomdtl2reftype, bod2.bomdtl2refoid, bod2.bomdtl2refqty, bod2.bomdtl2refunitoid, bod2.bomdtl2note, m.itemCode AS bomdtl2refcode, m.itemLongDescription AS bomdtl2refdesc, g.gendesc AS bomdtl2refunit, bod1.bomdtl1seq, bod1.bomdtl1desc AS bomdtl1process, roundQty AS roundqtydtl2, bod2.precostdtlrefoid, bomdtl2refqty_unitkecil FROM QL_mstbomdtl2 bod2 INNER JOIN QL_mstbomdtl1 bod1 ON bod1.cmpcode=bod2.cmpcode AND bod1.bomoid=bod2.bomoid AND bod1.bomdtl1oid=bod2.bomdtl1oid INNER JOIN QL_mstbom bom ON bom.bomoid=bod2.bomoid INNER JOIN QL_mstitem m ON m.itemoid=bod2.bomdtl2refoid INNER JOIN QL_mstgen g ON g.genoid=bod2.bomdtl2refunitoid WHERE bod2.bomoid=" & sOid & " ORDER BY bod2.bomdtl2seq"
        Dim dtMat As DataTable = cKon.ambiltabel(sSql, "QL_mstbomdtl2")
        Dim dvx As DataView = dtMat.DefaultView
        dvx.Sort = "bomdtl1seq, bomdtl2seq"
        dtMat = dvx.ToTable
        For C1 As Integer = 0 To dtMat.Rows.Count - 1
            dtMat.Rows(C1)("bomdtl2seq") = C1 + 1
        Next
        Session("TblDtl2") = dtMat
        GVDtl2.DataSource = Session("TblDtl2")
        GVDtl2.DataBind()
        ' Generate Detail Material Level 2
        sSql = "SELECT DISTINCT bod3.bomdtl3seq, bod3.bomdtl3reftype, bod3.bomdtl3refoid, bod3.bomdtl3refqty, bod3.bomdtl3refunitoid, bod3.bomdtl3note, (cat1code + '.' + cat2code) AS bomdtl3refcode, RTRIM((CASE WHEN (LTRIM(cat1shortdesc)='None' OR LTRIM(cat1shortdesc)='') THEN '' ELSE cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(cat2shortdesc)='None' OR LTRIM(cat2shortdesc)='') THEN '' ELSE cat2shortdesc + ' ' END)) AS bomdtl3refdesc, g.gendesc AS bomdtl3refunit, bod1.bomdtl1seq, bod1.bomdtl1desc AS bomdtl1process, bod3.precostdtlrefoid FROM QL_mstbomdtl3 bod3 INNER JOIN QL_mstbomdtl1 bod1 ON bod1.cmpcode=bod3.cmpcode AND bod1.bomoid=bod3.bomoid AND bod1.bomdtl1oid=bod3.bomdtl1oid INNER JOIN QL_mstbom bom ON bom.bomoid=bod3.bomoid INNER JOIN QL_mstgen g ON g.genoid=bod3.bomdtl3refunitoid INNER JOIN QL_mstcat2 c2 ON cat2oid=bomdtl3refoid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c1.cat1oid=c2.cat1oid WHERE bod3.bomdtl3reftype='Raw' AND bod3.bomoid=" & sOid
        Dim dtMat2 As DataTable = cKon.ambiltabel(sSql, "QL_mstbomdtl3")
        Dim dvx2 As DataView = dtMat2.DefaultView
        dvx2.Sort = "bomdtl1seq, bomdtl3seq"
        dtMat2 = dvx2.ToTable
        For C1 As Integer = 0 To dtMat2.Rows.Count - 1
            dtMat2.Rows(C1)("bomdtl3seq") = C1 + 1
        Next
        Session("TblDtl3") = dtMat2
        GVDtl3.DataSource = Session("TblDtl3")
        GVDtl3.DataBind()
        ClearDetail()
        ClearDetail2()
        ClearDetail3()
    End Sub

    Private Sub PrintReport()
        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("bomoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            report.Load(Server.MapPath(folderReport & "rptBOM2.rpt"))
            Dim sWhere As String = ""
            If sOid = "" Then
                sWhere &= " WHERE " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If Session("CompnyCode") <> CompnyCode Then
                    sWhere &= " AND bom.cmpcode='" & Session("CompnyCode") & "'"
                End If
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND bom.bomdate>='" & FilterPeriod1.Text & " 00:00:00' AND bom.bomdate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    If FilterDDLStatus.SelectedValue <> "ALL" Then
                        sWhere &= " AND bom.activeflag='" & FilterDDLStatus.SelectedValue & "'"
                    End If
                End If
                If checkPagePermission("~\Master\mstBOM.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND bom.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sWhere &= " WHERE bom.bomoid IN (" & sOid & ")"
            End If
            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            cProc.SetDBLogonForReport(report)
            report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "BOMPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
        Response.Redirect("~\Master\mstBOM.aspx?awal=true")
    End Sub

    Private Sub UpdateCheckedMat()
        Dim dv As DataView = Session("TblListMat").DefaultView
        dv.AllowEdit = True
        For C1 As Integer = 0 To gvListMat2.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat2.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        dv.RowFilter = "matrefoid=" & sOid
                        If cbcheck = True Then
                            dv(0)("checkvalue") = "True"
                            Dim cc2 As System.Web.UI.ControlCollection = row.Cells(3).Controls
                            dv(0)("bomdtl2qty") = ToDouble(GetTextValue(cc2))
                            cc2 = row.Cells(5).Controls
                            dv(0)("bomdtl2note") = GetTextValue(cc2)
                            cc2 = row.Cells(4).Controls
                            dv(0)("matrefunit") = GetDDLValue(cc2, "Text")
                            dv(0)("matrefunitoid") = GetDDLValue(cc2)
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                        dv.RowFilter = ""
                    End If
                Next
            End If
        Next
    End Sub

    Private Sub UpdateCheckedMat2()
        Dim dv As DataView = Session("TblListMat2").DefaultView
        dv.AllowEdit = True
        For C1 As Integer = 0 To gvListMat3.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat3.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        dv.RowFilter = "matrefoid2=" & sOid
                        If cbcheck = True Then
                            dv(0)("checkvalue") = "True"
                            Dim cc2 As System.Web.UI.ControlCollection = row.Cells(3).Controls
                            dv(0)("bomdtl3qty") = ToDouble(GetTextValue(cc2))
                            cc2 = row.Cells(4).Controls
                            dv(0)("selectedunitoid") = ToDouble(GetDDLValue(cc2))
                            dv(0)("selectedunit") = ToDouble(GetDDLValue(cc2, "Text"))
                            cc2 = row.Cells(5).Controls
                            dv(0)("bomdtl3note") = GetTextValue(cc2)
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                        dv.RowFilter = ""
                    End If
                Next
            End If
        Next
    End Sub

    Private Sub UpdateCheckedMatView()
        Dim dv As DataView = Session("TblListMatView").DefaultView
        dv.AllowEdit = True
        For C1 As Integer = 0 To gvListMat2.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat2.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        dv.RowFilter = "matrefoid=" & sOid
                        If cbcheck = True Then
                            dv(0)("checkvalue") = "True"
                            Dim cc2 As System.Web.UI.ControlCollection = row.Cells(3).Controls
                            dv(0)("bomdtl2qty") = ToDouble(GetTextValue(cc2))
                            cc2 = row.Cells(5).Controls
                            dv(0)("bomdtl2note") = GetTextValue(cc2)
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                        dv.RowFilter = ""
                    End If
                Next
            End If
        Next
    End Sub

    Private Sub UpdateCheckedMatView2()
        Dim dv As DataView = Session("TblListMatView2").DefaultView
        dv.AllowEdit = True
        For C1 As Integer = 0 To gvListMat3.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat3.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        dv.RowFilter = "matrefoid2=" & sOid
                        If cbcheck = True Then
                            dv(0)("checkvalue") = "True"
                            Dim cc2 As System.Web.UI.ControlCollection = row.Cells(3).Controls
                            dv(0)("bomdtl3qty") = ToDouble(GetTextValue(cc2))
                            cc2 = row.Cells(4).Controls
                            dv(0)("matrefunitoid2") = ToDouble(GetDDLValue(cc2))
                            dv(0)("matrefunit2") = ToDouble(GetDDLValue(cc2, "Text"))
                            cc2 = row.Cells(5).Controls
                            dv(0)("bomdtl3note") = GetTextValue(cc2)
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                        dv.RowFilter = ""
                    End If
                Next
            End If
        Next
    End Sub

    Private Sub DisableGridViewCell()
        If Not Session("oid") Is Nothing And Session("oid") <> "" Then
            If Not Session("TblDtl") Is Nothing Then
                Dim dt As DataTable = Session("TblDtl")
                If dt.Rows.Count > 0 Then
                    If dt.Rows.Count = GVDtl1.Rows.Count Then
                        For C1 As Integer = 0 To dt.Rows.Count - 1
                            If IsProcessExistsInDLC(dt.Rows(C1)("bomdtl1deptoid").ToString) Then
                                GVDtl1.Rows(C1).Cells(0).Enabled = False
                                GVDtl1.Rows(C1).Cells(GVDtl1.Columns.Count - 1).Enabled = False
                            End If
                        Next
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub AddNewFolder(ByVal sFolder As String)
        Dim fso, f, fc, nf
        fso = CreateObject("Scripting.FileSystemObject")
        f = fso.GetFolder(Server.MapPath("~/"))
        fc = f.SubFolders
        nf = fc.Add(sFolder)
    End Sub

    Private Sub BindListItem()
        Dim sType As String = "FG"
        If ddlbomtype.SelectedValue = "WIP" Then
            sType = "WIP"
        End If
        sSql = "SELECT i.itemoid, i.itemcode, i.itemlongdescription AS itemshortdesc, /*(i.itempackvolume*1000000000 / 28316846.59)*/ 0.0 AS cbf, i.itemunit1 AS itemunitoid FROM QL_mstitem i WHERE i.cmpcode='" & CompnyCode & "' /*AND i.itemoid NOT IN (SELECT pr.itemoid FROM QL_mstbom pr WHERE pr.cmpcode='" & DDLBusUnit.SelectedValue & "')*/ AND i.itemrecordstatus='ACTIVE' AND itemgroup='" & sType & "' AND " & FilterDDLListItem.SelectedValue & " LIKE '%" & Tchar(FilterTextListItem.Text) & "%' ORDER BY i.itemcode"
        FillGV(gvListItem, sSql, "QL_mstitem")
    End Sub

    Private Sub GenerateFormulaNumber()
        If DDLBusUnit.SelectedValue <> "" Then
            sSql = "SELECT ISNULL(MAX(bomcounter), 0) FROM QL_mstbom WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND itemoid=" & itemoid.Text
            bomcounter.Text = ToDouble(CStr(GetStrData(sSql))) + 1
            formulaseq.Text = Right(GenNumberString(itemcode.Text, "", bomcounter.Text, 3), 3)
        End If
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Master\mstBOM.aspx")
        End If
        'If checkPagePermission("~\Master\mstBOM.aspx", Session("Role")) = False Then
        '    Response.Redirect("~\Other\NotAuthorize.aspx")
        'End If
        Page.Title = CompnyName & " - Bill Of Material"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        bomdtl2refqty.Attributes.Add("onfocus", "this.select();")
        bomdtl3refqty.Attributes.Add("onfocus", "this.select();")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            InitAllDDL()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            MultiView1.SetActiveView(View1)
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
                ddlbomtype.CssClass = "inpTextDisabled" : ddlbomtype.Enabled = False
            Else
                bomoid.Text = GenerateID("QL_MSTBOM", CompnyCode)
                bomdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                bomcounter.Text = "1"
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
                ddlbomtype_SelectedIndexChanged(Nothing, Nothing)
                ddlbomtype.CssClass = "inpText" : ddlbomtype.Enabled = True
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            GVDtl1.DataSource = dt
            GVDtl1.DataBind()
            DisableGridViewCell()
        End If
        If Not Session("TblDtl2") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl2")
            GVDtl2.DataSource = dt
            GVDtl2.DataBind()
        End If
        If Not Session("TblDtl3") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl3")
            GVDtl3.DataSource = dt
            GVDtl3.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("ErrMat") Is Nothing And Session("ErrMat") <> "" Then
            Session("ErrMat") = Nothing
            cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, True)
        End If
        If Not Session("ErrMat2") Is Nothing And Session("ErrMat2") <> "" Then
            Session("ErrMat2") = Nothing
            cProc.SetModalPopUpExtender(btnHideListMat3, pnlListMat3, mpeListMat3, True)
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Master\mstBOM.aspx?awal=true")
            End If
        End If
        If Not Session("MsgUpload") Is Nothing And Session("MsgUpload") <> "" Then
            If lblPopUpMsg.Text = Session("MsgUpload") Then
                mpeListUpload.Show()
            End If
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND tbl.bomdate>='" & FilterPeriod1.Text & " 00:00:00' AND tbl.bomdate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND tbl.activeflag='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Master\mstBOM.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND tbl.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Master\mstBOM.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND bom.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnPrintHdr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrintHdr.Click
        UpdateCheckedValue()
        PrintReport()
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLDept()
    End Sub

    Protected Sub btnSearchPC_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchPC.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        FilterDDLListPC.SelectedIndex = -1 : FilterTextListPC.Text = "" : gvListPC.SelectedIndex = -1
        BindPreCostingData()
        cProc.SetModalPopUpExtender(btnHideListPC, pnlListPC, mpeListPC, True)
    End Sub

    Protected Sub btnClearPC_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearPC.Click
        bomdesc.Text = ""
        itemcode.Text = ""
        itemshortdesc.Text = ""
        itemoid.Text = ""
        itemunitoid.SelectedIndex = -1
        itemunit.Text = ""
        precostoid.Text = ""
        bomres2.Text = ""
        formulaseq.Text = ""
    End Sub

    Protected Sub btnFindListPC_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListPC.Click
        BindPreCostingData()
        mpeListPC.Show()
    End Sub

    Protected Sub btnAllListPC_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListPC.Click
        FilterDDLListPC.SelectedIndex = -1
        FilterTextListPC.Text = ""
        BindPreCostingData()
        mpeListPC.Show()
    End Sub

    Protected Sub gvListPC_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListPC.PageIndexChanging
        gvListPC.PageIndex = e.NewPageIndex
        BindPreCostingData()
        mpeListPC.Show()
    End Sub

    Protected Sub gvListPC_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListPC.SelectedIndexChanged
        If precostoid.Text <> gvListPC.SelectedDataKey("precostoid").ToString().Trim Then
            btnClearPC_Click(Nothing, Nothing)
        End If
        itemoid.Text = gvListPC.SelectedDataKey("itemoid").ToString().Trim
        itemcode.Text = gvListPC.SelectedDataKey("itemcode").ToString().Trim
        itemshortdesc.Text = gvListPC.SelectedDataKey("itemlongdesc").ToString().Trim
        itemunit.Text = gvListPC.SelectedDataKey("itemunitoid").ToString
        precostoid.Text = gvListPC.SelectedDataKey("precostoid").ToString().Trim
        bomres2.Text = gvListPC.SelectedDataKey("updtime").ToString
        InitBOMDesc()
        cProc.SetModalPopUpExtender(btnHideListPC, pnlListPC, mpeListPC, False)
    End Sub 'Not Used

    Protected Sub lbCloseListPC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListPC.Click
        cProc.SetModalPopUpExtender(btnHideListPC, pnlListPC, mpeListPC, False)
    End Sub

    Protected Sub lkbDetMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbDetMat.Click
        MultiView1.SetActiveView(View2) : pnlDtlLevel5.Visible = True : pnlDtlLevel3.Visible = False
    End Sub

    Protected Sub bomdtl1res1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles bomdtl1res1.TextChanged
        Dim bVal As Boolean = False
        Dim sCss As String = "inpTextDisabled"
        If bomdtl1res1.Text <> "" Then
            bVal = True : sCss = "inpText"
        End If
        bomdtl1deptoid.Enabled = bVal : bomdtl1deptoid.CssClass = sCss
        bomdtl1todeptoid.Enabled = bVal : bomdtl1todeptoid.CssClass = sCss
    End Sub

    Protected Sub bomdtl1reftype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles bomdtl1reftype.SelectedIndexChanged
        'InitDDLUnit()
        'btnClearOutput_Click(Nothing, Nothing)
        If bomdtl1reftype.SelectedValue = "FG" Then
            lbltodept.Visible = False : septtodept.Visible = False : bomdtl1todeptoid.Visible = False
        Else
            lbltodept.Visible = True : septtodept.Visible = True : bomdtl1todeptoid.Visible = True
        End If
        InitDDLToDept()
    End Sub

    Protected Sub bomdtl1deptoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles bomdtl1deptoid.SelectedIndexChanged
        If i_u2.Text = "Update Detail" Then
            InitDDLToDept(bomdtl1todeptoid.SelectedValue)
        Else
            InitDDLToDept()
        End If
    End Sub

    Protected Sub btnSearchOutput_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchOutput.Click
        If bomdtl1reftype.SelectedValue = "FG" Then
            If precostoid.Text = "" Then
                showMessage("Please select Pre Costing first!", 2)
                Exit Sub
            End If
            bomdtl1refoid.Text = itemoid.Text
            bomdtl1refdesc.Text = itemshortdesc.Text
            bomdtl1refunitoid.SelectedValue = itemunit.Text
            roundqtydtl1.Text = "1.0000"
        Else
            FilterDDLListWIP.SelectedIndex = -1 : FilterTextListWIP.Text = "" : gvListWIP.SelectedIndex = -1 : InitDDLCat01WIP()
            cbCat01ListWIP.Checked = False : cbCat02ListWIP.Checked = False : cbCat03ListWIP.Checked = False : cbCat04ListWIP.Checked = False
            BindWIPData()
            cProc.SetModalPopUpExtender(btnHideListWIP, pnlListWIP, mpeListWIP, True)
        End If
    End Sub

    Protected Sub btnClearOutput_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearOutput.Click
        bomdtl1refoid.Text = ""
        bomdtl1refdesc.Text = ""
        bomdtl1refunitoid.SelectedIndex = -1
        roundqtydtl1.Text = ""
    End Sub

    Protected Sub FilterDDLCat01ListWIP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat01ListWIP.SelectedIndexChanged
        InitDDLCat02WIP()
        mpeListWIP.Show()
    End Sub

    Protected Sub FilterDDLCat02ListWIP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat02ListWIP.SelectedIndexChanged
        InitDDLCat03WIP()
        mpeListWIP.Show()
    End Sub

    Protected Sub FilterDDLCat03ListWIP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat03ListWIP.SelectedIndexChanged
        InitDDLCat04WIP()
        mpeListWIP.Show()
    End Sub

    Protected Sub btnFindListWIP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListWIP.Click
        BindWIPData()
        mpeListWIP.Show()
    End Sub

    Protected Sub btnAllListWIP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListWIP.Click
        FilterDDLListWIP.SelectedIndex = -1 : FilterTextListWIP.Text = "" : gvListWIP.SelectedIndex = -1 : InitDDLCat01WIP()
        cbCat01ListWIP.Checked = False : cbCat02ListWIP.Checked = False : cbCat03ListWIP.Checked = False : cbCat04ListWIP.Checked = False
        BindWIPData()
        mpeListWIP.Show()
    End Sub

    Protected Sub gvListWIP_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListWIP.PageIndexChanging
        gvListWIP.PageIndex = e.NewPageIndex
        BindWIPData()
        mpeListWIP.Show()
    End Sub

    Protected Sub gvListWIP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListWIP.SelectedIndexChanged
        If bomdtl1refoid.Text <> gvListWIP.SelectedDataKey("matrawoid").ToString().Trim Then
            btnClearOutput_Click(Nothing, Nothing)
        End If
        bomdtl1refoid.Text = gvListWIP.SelectedDataKey("matrawoid").ToString().Trim
        bomdtl1refdesc.Text = gvListWIP.SelectedDataKey("matrawlongdesc").ToString().Trim
        bomdtl1refunitoid.SelectedValue = gvListWIP.SelectedDataKey("matrawunitoid").ToString
        roundqtydtl1.Text = ToMaskEdit(ToDouble(gvListWIP.SelectedDataKey("matrawlimitqty").ToString), 4)
        cProc.SetModalPopUpExtender(btnHideListWIP, pnlListWIP, mpeListWIP, False)
    End Sub

    Protected Sub lbCloseListWIP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListWIP.Click
        cProc.SetModalPopUpExtender(btnHideListWIP, pnlListWIP, mpeListWIP, False)
    End Sub

    Protected Sub btnAddToList1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList1.Click
        If IsDetailProcessInputValid() Then
            If Session("TblDtl") Is Nothing Then
                CreateTblDetailProcess()
            End If
            Dim objTable As DataTable
            objTable = Session("TblDtl")
            Dim dv As DataView = objTable.DefaultView
            If i_u2.Text = "New Detail" Then
                dv.RowFilter = "bomdtl1deptoid=" & bomdtl1deptoid.SelectedValue
            Else
                dv.RowFilter = "bomdtl1deptoid=" & bomdtl1deptoid.SelectedValue & " AND bomdtl1seq<>" & bomdtl1seq.Text
            End If
            If dv.Count > 0 Then
                showMessage("This Data has been added before!", 2)
                dv.RowFilter = ""
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                bomdtl1seq.Text = objTable.Rows.Count + 1
                objRow("bomdtl1seq") = objTable.Rows.Count + 1
            Else
                objRow = objTable.Rows(bomdtl1seq.Text - 1)
                objRow.BeginEdit()
            End If
            Dim str As String = bomdtl1deptoid.SelectedItem.Text & " - " & IIf(bomdtl1reftype.SelectedValue = "FG", "END", bomdtl1todeptoid.SelectedItem.Text)
            objRow("bomdtl1desc") = str
            objRow("bomdtl1deptoid") = bomdtl1deptoid.SelectedValue
            objRow("bomdtl1dept") = bomdtl1deptoid.SelectedItem.Text
            objRow("bomdtl1todeptoid") = bomdtl1todeptoid.SelectedValue
            If bomdtl1reftype.SelectedValue = "FG" Then
                objRow("bomdtl1todept") = "END"
            Else
                objRow("bomdtl1todept") = bomdtl1todeptoid.SelectedItem.Text
            End If
            objRow("bomdtl1reftype") = bomdtl1reftype.SelectedValue
            objRow("bomdtl1note") = bomdtl1note.Text
            objRow("bomdtl1res1") = bomdtl1res1.Text
            If i_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
                DDLProcess.Items.Add(str)
                DDLProcess.Items.Item(DDLProcess.Items.Count - 1).Value = bomdtl1seq.Text
            Else
                objRow.EndEdit()
                DDLProcess.Items.Item(CInt(bomdtl1seq.Text) - 1).Text = str
                UpdateDetailMat(str)
            End If
            ClearDetail()
            Session("TblDtl") = objTable
            GVDtl1.DataSource = Session("TblDtl")
            GVDtl1.DataBind()
            DisableGridViewCell()
        End If
    End Sub

    Protected Sub btnClear1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear1.Click
        ClearDetail()
    End Sub

    Protected Sub GVDtl1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVDtl1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            'e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
        End If
    End Sub

    Protected Sub GVDtl1_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVDtl1.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        Dim iSeq As Integer = objTable.Rows(e.RowIndex)("bomdtl1seq")
        If Session("TblDtl2") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtl2")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "bomdtl1seq=" & iSeq
            If dv.Count > 0 Then
                showMessage("Please remove all material that using this data first before removing this data!", 2) : dv.RowFilter = "" : Exit Sub
            End If
            dv.RowFilter = ""
        End If
        If Session("TblDtl3") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtl3")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "bomdtl1seq=" & iSeq
            If dv.Count > 0 Then
                showMessage("Please remove all material that using this data first before removing this data!", 2) : dv.RowFilter = "" : Exit Sub
            End If
            dv.RowFilter = ""
        End If
        objTable.Rows.Remove(objRow(e.RowIndex))
        DeleteDetailMat(iSeq)
        DDLProcess.Items.RemoveAt(e.RowIndex)
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            DDLProcess.Items(C1).Value = C1 + 1
            UpdateDetailMat(CInt(objTable.Rows(C1)("bomdtl1seq").ToString), C1 + 1)
            dr.BeginEdit()
            dr("bomdtl1seq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        GVDtl1.DataSource = Session("TblDtl")
        GVDtl1.DataBind()
        DisableGridViewCell()
        ClearDetail()
    End Sub

    Protected Sub GVDtl1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDtl1.SelectedIndexChanged
        Try
            bomdtl1seq.Text = GVDtl1.SelectedDataKey.Item("bomdtl1seq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "bomdtl1seq=" & bomdtl1seq.Text
                bomdtl1deptoid.SelectedValue = dv.Item(0).Item("bomdtl1deptoid").ToString
                bomdtl1reftype.SelectedValue = dv.Item(0).Item("bomdtl1reftype").ToString
                bomdtl1reftype_SelectedIndexChanged(Nothing, Nothing)
                InitDDLToDept(dv.Item(0).Item("bomdtl1todeptoid").ToString)
                bomdtl1todeptoid.SelectedValue = dv.Item(0).Item("bomdtl1todeptoid").ToString
                'bomdtl1refoid.Text = dv.Item(0).Item("bomdtl1refoid").ToString
                'bomdtl1refdesc.Text = dv.Item(0).Item("bomdtl1refdesc").ToString
                'bomdtl1refqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("bomdtl1refqty").ToString), 4)
                'bomdtl1refunitoid.SelectedValue = dv.Item(0).Item("bomdtl1refunitoid").ToString
                bomdtl1note.Text = dv.Item(0).Item("bomdtl1note").ToString
                bomdtl1res1.Text = dv.Item(0).Item("bomdtl1res1").ToString
                bomdtl1res1_TextChanged(Nothing, Nothing)
                'roundqtydtl1.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("roundqtydtl1").ToString), 4)
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub lkbDetItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbDetItem.Click
        MultiView1.SetActiveView(View1)
    End Sub

    Protected Sub lbLevel05_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbLevel05.Click
        MultiView1.SetActiveView(View2) : pnlDtlLevel5.Visible = True : pnlDtlLevel3.Visible = False
    End Sub

    Protected Sub lbLevel03_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbLevel03.Click
        MultiView1.SetActiveView(View2) : pnlDtlLevel5.Visible = False : pnlDtlLevel3.Visible = True
    End Sub

    Protected Sub bomdtl2reftype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles bomdtl2reftype.SelectedIndexChanged
        bomdtl2refoid.Text = ""
        bomdtl2refdesc.Text = ""
        bomdtl2refcode.Text = ""
        bomdtl2refqty.Text = ""
        bomdtl2refunitoid.SelectedIndex = -1
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        If itemoid.Text = "" Then
            showMessage("Please select Finish Good first!", 2)
            Exit Sub
        End If
        If DDLProcess.SelectedValue = "" Then
            showMessage("Please select Process first!", 2)
            Exit Sub
        End If
        InitDDLCat01ListMat() : cbCat01ListMat.Checked = False : cbCat02ListMat.Checked = False : cbCat03ListMat.Checked = False : cbCat04ListMat.Checked = False
        FilterDDLListMat2.SelectedIndex = -1 : FilterTextListMat2.Text = "" : Session("TblListMat") = Nothing : Session("TblListMatView") = Nothing : gvListMat2.DataSource = Session("TblListMatView") : gvListMat2.DataBind()
        cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, True)
    End Sub

    Protected Sub btnFindListMat2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat2.Click
        If Session("TblListMat") Is Nothing Then
            BindMatData(bomdtl2reftype.SelectedValue)
        End If
        If Not Session("TblListMat") Is Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                UpdateCheckedMat()
                Dim dv As DataView = dt.DefaultView
                Dim sFilter As String = FilterDDLListMat2.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat2.Text) & "%'"
                If cbCat04ListMat.Checked And FilterDDLCat04ListMat.SelectedValue <> "" Then
                    sFilter &= " AND itemcat4=" & FilterDDLCat04ListMat.SelectedValue & ""
                Else
                    If cbCat03ListMat.Checked And FilterDDLCat03ListMat.SelectedValue <> "" Then
                        sFilter &= " AND itemcat3=" & FilterDDLCat03ListMat.SelectedValue & ""
                    Else
                        If cbCat02ListMat.Checked And FilterDDLCat02ListMat.SelectedValue <> "" Then
                            sFilter &= " AND itemcat2=" & FilterDDLCat02ListMat.SelectedValue & ""
                        Else
                            If cbCat01ListMat.Checked And FilterDDLCat01ListMat.SelectedValue <> "" Then
                                sFilter &= " AND itemcat1=" & FilterDDLCat01ListMat.SelectedValue & ""
                            End If
                        End If
                    End If
                End If
                dv.RowFilter = sFilter
                If dv.Count > 0 Then
                    Session("TblListMatView") = dv.ToTable
                    gvListMat2.DataSource = Session("TblListMatView")
                    gvListMat2.DataBind()
                    dv.RowFilter = ""
                    mpeListMat2.Show()
                Else
                    dv.RowFilter = ""
                    Session("ErrMat") = "Material data can't be found!"
                    showMessage(Session("ErrMat"), 2)
                End If
            Else
                Session("ErrMat") = "Material data can't be found!"
                showMessage(Session("ErrMat"), 2)
            End If
        End If
    End Sub

    Protected Sub btnAllListMat2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat2.Click
        If Session("TblListMat") Is Nothing Then
            BindMatData(bomdtl2reftype.SelectedValue)
        End If
        If Not Session("TblListMat") Is Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                UpdateCheckedMat()
                FilterDDLListMat2.SelectedIndex = -1 : FilterTextListMat2.Text = ""
                InitDDLCat01ListMat() : cbCat01ListMat.Checked = False : cbCat02ListMat.Checked = False : cbCat03ListMat.Checked = False : cbCat04ListMat.Checked = False
                Session("TblListMatView") = dt
                gvListMat2.DataSource = Session("TblListMatView")
                gvListMat2.DataBind()
                mpeListMat2.Show()
            Else
                Session("ErrMat") = "Material data can't be found!"
                showMessage(Session("ErrMat"), 2)
            End If
        End If
    End Sub

    Protected Sub FilterDDLCat01ListMat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat01ListMat.SelectedIndexChanged
        InitDDLCat02ListMat()
        mpeListMat2.Show()
    End Sub

    Protected Sub FilterDDLCat02ListMat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat02ListMat.SelectedIndexChanged
        InitDDLCat03ListMat()
        mpeListMat2.Show()
    End Sub

    Protected Sub FilterDDLCat03ListMat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat03ListMat.SelectedIndexChanged
        InitDDLCat04ListMat()
        mpeListMat2.Show()
    End Sub

    Protected Sub gvListMat2_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat2.PageIndexChanging
        If Not Session("TblListMat") Is Nothing Then
            UpdateCheckedMat()
            UpdateCheckedMatView()
            gvListMat2.PageIndex = e.NewPageIndex
            gvListMat2.DataSource = Session("TblListMatView")
            gvListMat2.DataBind()
        End If
        mpeListMat2.Show()
    End Sub

    Protected Sub gvListMat2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat2.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(3).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    End If
                End If
            Next
            Dim str As String = e.Row.Cells(1).Text
            e.Row.Cells(1).Text = Mid(str, 1, 7) & vbCrLf & Mid(str, 8, 10) & vbCrLf & Mid(str, 18, 5)

            Dim iItemOid As Integer = 0
            cc = e.Row.Cells(0).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    iItemOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                End If
            Next

            cc = e.Row.Cells(4).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                    FillDDL(CType(myControl, System.Web.UI.WebControls.DropDownList), "select itemUnit1, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit1 where itemoid=" & iItemOid & " union all select itemUnit2, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit2 where itemoid=" & iItemOid & " union all select itemUnit3, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit3 where itemoid=" & iItemOid & "")
                    CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue = CType(myControl, System.Web.UI.WebControls.DropDownList).ToolTip
                End If
            Next
        End If
    End Sub

    Protected Sub lkbAddToList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToList.Click
        If Not Session("TblListMat") Is Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                UpdateCheckedMat()
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "checkvalue='True'"
                If dv.Count = 0 Then
                    Session("ErrMat") = "Please select Material data!"
                    showMessage(Session("ErrMat"), 2)
                    dv.RowFilter = ""
                    Exit Sub
                End If
                dv.RowFilter = ""
                dv.RowFilter = "checkvalue='True' AND bomdtl2qty=0"
                If dv.Count > 0 Then
                    Session("ErrMat") = "BOM Qty for every selected Material must be more than 0!"
                    showMessage(Session("ErrMat"), 2)
                    dv.RowFilter = ""
                    Exit Sub
                End If
                dv.RowFilter = ""
                If Session("TblDtl2") Is Nothing Then
                    CreateTblDetailMat()
                End If
                Dim dtMat As DataTable = Session("TblDtl2")
                Dim dvMat As DataView = dtMat.DefaultView
                Dim dQty_unitkecil, dQty_unitbesar As Double
                Dim iSeq As Integer = dvMat.Count + 1
                dvMat.AllowEdit = True
                dvMat.AllowNew = True
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    dvMat.RowFilter = "bomdtl1seq=" & DDLProcess.SelectedValue & " AND bomdtl2reftype='" & bomdtl2reftype.SelectedValue & "' AND bomdtl2refoid=" & dv(C1)("matrefoid").ToString
                    If dvMat.Count > 0 Then
                        dvMat(0)("bomdtl2refqty") = ToDouble(dv(C1)("bomdtl2qty").ToString)
                        dvMat(0)("bomdtl2refunitoid") = dv(C1)("matrefunitoid").ToString
                        GetUnitConverter(dv(C1)("matrefoid").ToString, dv(C1)("matrefunitoid").ToString, ToDouble(dv(C1)("bomdtl2qty").ToString), dQty_unitkecil, dQty_unitbesar)
                        dvMat(0)("bomdtl2refqty_unitkecil") = dQty_unitkecil
                        dvMat(0)("bomdtl2note") = dv(C1)("bomdtl2note").ToString
                    Else
                        Dim rv As DataRowView = dvMat.AddNew()
                        rv.BeginEdit()
                        rv("bomdtl2seq") = iSeq
                        rv("bomdtl2reftype") = bomdtl2reftype.SelectedValue
                        rv("bomdtl2refoid") = dv(C1)("matrefoid").ToString
                        rv("bomdtl2refqty") = ToDouble(dv(C1)("bomdtl2qty").ToString)
                        GetUnitConverter(dv(C1)("matrefoid").ToString, dv(C1)("matrefunitoid").ToString, ToDouble(dv(C1)("bomdtl2qty").ToString), dQty_unitkecil, dQty_unitbesar)
                        rv("bomdtl2refqty_unitkecil") = dQty_unitkecil
                        rv("bomdtl2refunitoid") = dv(C1)("matrefunitoid").ToString
                        rv("bomdtl2note") = dv(C1)("bomdtl2note").ToString
                        rv("bomdtl2refcode") = dv(C1)("matrefcode").ToString
                        rv("bomdtl2refdesc") = dv(C1)("matrefshortdesc").ToString
                        rv("bomdtl2refunit") = dv(C1)("matrefunit").ToString
                        rv("bomdtl1seq") = DDLProcess.SelectedValue
                        rv("bomdtl1process") = DDLProcess.SelectedItem.Text
                        rv("roundqtydtl2") = ToDouble(dv(C1)("roundqtydtl2").ToString)
                        rv("precostdtlrefoid") = dv(C1)("precostdtlrefoid").ToString
                        rv.EndEdit()
                        iSeq += 1
                    End If
                    dvMat.RowFilter = ""
                Next
                dv.RowFilter = ""
                Session("TblDtl2") = dvMat.ToTable
                GVDtl2.DataSource = Session("TblDtl2")
                GVDtl2.DataBind()
                ClearDetail2()
            End If
            cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, False)
        Else

        End If
    End Sub

    Protected Sub lkbCloseListMat2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat2.Click
        cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, False)
    End Sub

    Protected Sub btnAddToList2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList2.Click
        If IsDetailMatInputValid() Then
            If Session("TblDtl2") Is Nothing Then
                CreateTblDetailMat()
            End If
            Dim dtMat As DataTable = Session("TblDtl2")
            Dim dQty_unitkecil, dQty_unitbesar As Double
            Dim dv As DataView = dtMat.DefaultView
            If i_u3.Text = "New Detail" Then
                dv.RowFilter = "bomdtl1seq=" & DDLProcess.SelectedValue & " AND bomdtl2reftype='" & bomdtl2reftype.SelectedValue & "' AND bomdtl2refoid=" & bomdtl2refoid.Text
            Else
                dv.RowFilter = "bomdtl1seq=" & DDLProcess.SelectedValue & " AND bomdtl2reftype='" & bomdtl2reftype.SelectedValue & "' AND bomdtl2refoid=" & bomdtl2refoid.Text & " AND bomdtl2seq<>" & bomdtl2seq.Text
            End If
            If dv.Count > 0 Then
                showMessage("This Data has been added before!", 2)
                dv.RowFilter = ""
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u3.Text = "New Detail" Then
                objRow = dtMat.NewRow()
                objRow("bomdtl2seq") = dtMat.Rows.Count + 1
            Else
                objRow = dtMat.Rows(bomdtl2seq.Text - 1)
                objRow.BeginEdit()
            End If
            objRow("bomdtl1seq") = DDLProcess.SelectedValue
            objRow("bomdtl1process") = DDLProcess.SelectedItem.Text
            objRow("bomdtl2reftype") = bomdtl2reftype.SelectedValue
            objRow("bomdtl2refoid") = bomdtl2refoid.Text
            objRow("bomdtl2refqty") = ToDouble(bomdtl2refqty.Text)
            GetUnitConverter(bomdtl2refoid.Text, bomdtl2refunitoid.SelectedValue, ToDouble(bomdtl2refqty.Text), dQty_unitkecil, dQty_unitbesar)
            objRow("bomdtl2refqty_unitkecil") = dQty_unitkecil
            objRow("bomdtl2refunitoid") = bomdtl2refunitoid.SelectedValue
            objRow("bomdtl2note") = bomdtl2note.Text
            objRow("bomdtl2refcode") = bomdtl2refcode.Text
            objRow("bomdtl2refdesc") = bomdtl2refdesc.Text
            objRow("bomdtl2refunit") = bomdtl2refunitoid.SelectedItem.Text
            objRow("precostdtlrefoid") = precostdtlrefoid.Text
            If i_u3.Text = "New Detail" Then
                dtMat.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            Session("TblDtl2") = dtMat
            GVDtl2.DataSource = dtMat
            GVDtl2.DataBind()
            ClearDetail2()
        End If
    End Sub

    Protected Sub btnClear2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear2.Click
        ClearDetail2()
    End Sub

    Protected Sub GVDtl2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVDtl2.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
        End If
    End Sub

    Protected Sub GVDtl2_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVDtl2.RowDeleting
        Dim objTable As DataTable = Session("TblDtl2")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("bomdtl2seq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl2") = objTable
        GVDtl2.DataSource = Session("TblDtl2")
        GVDtl2.DataBind()
        ClearDetail2()
    End Sub

    Protected Sub GVDtl2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDtl2.SelectedIndexChanged
        Try
            bomdtl2seq.Text = GVDtl2.SelectedDataKey.Item("bomdtl2seq").ToString
            If Session("TblDtl2") Is Nothing = False Then
                i_u3.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl2")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "bomdtl2seq=" & bomdtl2seq.Text
                DDLProcess.SelectedValue = dv.Item(0).Item("bomdtl1seq").ToString
                bomdtl2refdesc.Text = dv.Item(0).Item("bomdtl2refdesc").ToString
                bomdtl2refoid.Text = dv.Item(0).Item("bomdtl2refoid").ToString
                bomdtl2reftype.SelectedValue = dv.Item(0).Item("bomdtl2reftype").ToString
                bomdtl2refcode.Text = dv.Item(0).Item("bomdtl2refcode").ToString
                bomdtl2refqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("bomdtl2refqty").ToString), 4)
                bomdtl2refunitoid.SelectedValue = dv.Item(0).Item("bomdtl2refunitoid").ToString
                bomdtl2note.Text = dv.Item(0).Item("bomdtl2note").ToString
                roundqtydtl2.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("roundqtydtl2").ToString), 4)
                precostdtlrefoid.Text = dv.Item(0).Item("precostdtlrefoid").ToString
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        Finally
            bomdtl2reftype.CssClass = "inpTextDisabled" : bomdtl2reftype.Enabled = False : btnSearchMat.Visible = False
            cProc.SetFocusToControl(Me.Page, bomdtl2refqty)
        End Try
    End Sub

    Protected Sub btnSearchMat2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat2.Click
        If precostoid.Text = "" Then
            showMessage("Please select Pre Costing first!", 2)
            Exit Sub
        End If
        If DDLProcess.SelectedValue = "" Then
            showMessage("Please select Process first!", 2)
            Exit Sub
        End If
        InitDDLCat01ListMat2() : cbCat01ListMat2.Checked = False : cbCat02ListMat2.Checked = False
        FilterDDLListMat3.SelectedIndex = -1 : FilterTextListMat3.Text = "" : Session("TblListMat2") = Nothing : Session("TblListMatView2") = Nothing : gvListMat3.DataSource = Session("TblListMatView2") : gvListMat3.DataBind()
        cProc.SetModalPopUpExtender(btnHideListMat3, pnlListMat3, mpeListMat3, True)
    End Sub

    Protected Sub btnFindListMat3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat3.Click
        If Session("TblListMat2") Is Nothing Then
            BindMatData2()
        End If
        If Not Session("TblListMat2") Is Nothing Then
            Dim dt As DataTable = Session("TblListMat2")
            If dt.Rows.Count > 0 Then
                UpdateCheckedMat2()
                Dim dv As DataView = dt.DefaultView
                Dim sFilter As String = FilterDDLListMat3.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat3.Text) & "%'"
                If cbCat02ListMat2.Checked And FilterDDLCat02ListMat2.SelectedValue <> "" Then
                    sFilter &= " AND matrefcode2 LIKE '" & FilterDDLCat02ListMat2.SelectedValue & "%'"
                Else
                    If cbCat01ListMat2.Checked And FilterDDLCat01ListMat2.SelectedValue <> "" Then
                        sFilter &= " AND matrefcode2 LIKE '" & FilterDDLCat01ListMat2.SelectedValue & "%'"
                    End If
                End If
                dv.RowFilter = sFilter
                If dv.Count > 0 Then
                    Session("TblListMatView2") = dv.ToTable
                    gvListMat3.DataSource = Session("TblListMatView2")
                    gvListMat3.DataBind()
                    dv.RowFilter = ""
                    mpeListMat3.Show()
                Else
                    dv.RowFilter = ""
                    Session("ErrMat2") = "Material data can't be found!"
                    showMessage(Session("ErrMat2"), 2)
                End If
            Else
                Session("ErrMat2") = "Material data can't be found!"
                showMessage(Session("ErrMat2"), 2)
            End If
        End If
    End Sub

    Protected Sub btnAllListMat3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat3.Click
        If Not Session("TblListMat2") Is Nothing Then
            Dim dt As DataTable = Session("TblListMat2")
            If dt.Rows.Count > 0 Then
                UpdateCheckedMat2()
                FilterDDLListMat3.SelectedIndex = -1 : FilterTextListMat3.Text = ""
                InitDDLCat01ListMat2() : cbCat01ListMat2.Checked = False : cbCat02ListMat2.Checked = False
                Session("TblListMatView2") = dt
                gvListMat3.DataSource = Session("TblListMatView2")
                gvListMat3.DataBind()
                mpeListMat3.Show()
            Else
                Session("ErrMat2") = "Material data can't be found!"
                showMessage(Session("ErrMat2"), 2)
            End If
        End If
    End Sub

    Protected Sub FilterDDLCat01ListMat2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat01ListMat2.SelectedIndexChanged
        InitDDLCat02ListMat2()
        mpeListMat3.Show()
    End Sub

    Protected Sub gvListMat3_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat3.PageIndexChanging
        If Not Session("TblListMat2") Is Nothing Then
            UpdateCheckedMat2()
            UpdateCheckedMatView2()
            gvListMat3.PageIndex = e.NewPageIndex
            gvListMat3.DataSource = Session("TblListMatView2")
            gvListMat3.DataBind()
        End If
        mpeListMat3.Show()
    End Sub

    Protected Sub gvListMat3_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat3.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(3).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    End If
                End If
            Next
            cc = e.Row.Cells(4).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                    If CType(myControl, System.Web.UI.WebControls.DropDownList).Items.Count = 0 Then
                        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND gengroup='MATERIAL UNIT'"
                        FillDDL(CType(myControl, System.Web.UI.WebControls.DropDownList), sSql)
                    End If
                    If CType(myControl, System.Web.UI.WebControls.DropDownList).ToolTip <> "0" Then
                        CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue = CType(myControl, System.Web.UI.WebControls.DropDownList).ToolTip
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub lkbAddToList2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToList2.Click
        If Not Session("TblListMat2") Is Nothing Then
            Dim dt As DataTable = Session("TblListMat2")
            If dt.Rows.Count > 0 Then
                UpdateCheckedMat2()
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "checkvalue='True'"
                If dv.Count = 0 Then
                    Session("ErrMat2") = "Please select Material data!"
                    showMessage(Session("ErrMat2"), 2)
                    dv.RowFilter = ""
                    Exit Sub
                End If
                dv.RowFilter = ""
                dv.RowFilter = "checkvalue='True' AND bomdtl3qty=0"
                If dv.Count > 0 Then
                    Session("ErrMat2") = "BOM Qty for every selected Material must be more than 0!"
                    showMessage(Session("ErrMat2"), 2)
                    dv.RowFilter = ""
                    Exit Sub
                End If
                dv.RowFilter = ""
                dv.RowFilter = "checkvalue='True'"
                If Session("TblDtl3") Is Nothing Then
                    CreateTblDetailMat2()
                End If
                Dim dtMat As DataTable = Session("TblDtl3")
                Dim dvMat As DataView = dtMat.DefaultView
                Dim iSeq As Integer = dvMat.Count + 1
                dvMat.AllowEdit = True
                dvMat.AllowNew = True
                For C1 As Integer = 0 To dv.Count - 1
                    dvMat.RowFilter = "bomdtl1seq=" & DDLProcess.SelectedValue & " AND bomdtl3reftype='" & bomdtl3reftype.SelectedValue & "' AND bomdtl3refoid=" & dv(C1)("matrefoid2").ToString
                    If dvMat.Count > 0 Then
                        dvMat(0)("bomdtl3refqty") = ToDouble(dv(C1)("bomdtl3qty").ToString)
                        dvMat(0)("bomdtl3note") = dv(C1)("bomdtl3note").ToString
                    Else
                        Dim rv As DataRowView = dvMat.AddNew()
                        rv.BeginEdit()
                        rv("bomdtl3seq") = iSeq
                        rv("bomdtl3reftype") = bomdtl3reftype.SelectedValue
                        rv("bomdtl3refoid") = dv(C1)("matrefoid2").ToString
                        rv("bomdtl3refqty") = ToDouble(dv(C1)("bomdtl3qty").ToString)
                        rv("bomdtl3refunitoid") = dv(C1)("selectedunitoid")
                        rv("bomdtl3note") = dv(C1)("bomdtl3note").ToString
                        rv("bomdtl3refcode") = dv(C1)("matrefcode2").ToString
                        rv("bomdtl3refdesc") = dv(C1)("matrefshortdesc2").ToString
                        rv("bomdtl3refunit") = dv(C1)("selectedunit").ToString
                        rv("bomdtl1seq") = DDLProcess.SelectedValue
                        rv("bomdtl1process") = DDLProcess.SelectedItem.Text
                        rv("precostdtlrefoid") = dv(C1)("precostdtlrefoid").ToString
                        rv.EndEdit()
                        iSeq += 1
                    End If
                    dvMat.RowFilter = ""
                Next
                dv.RowFilter = ""
                Session("TblDtl3") = dvMat.ToTable
                GVDtl3.DataSource = Session("TblDtl3")
                GVDtl3.DataBind()
                ClearDetail3()
            End If
            cProc.SetModalPopUpExtender(btnHideListMat3, pnlListMat3, mpeListMat3, False)
        End If
    End Sub

    Protected Sub lkbCloseListMat3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat3.Click
        cProc.SetModalPopUpExtender(btnHideListMat3, pnlListMat3, mpeListMat3, False)
    End Sub

    Protected Sub btnAddToList3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList3.Click
        If IsDetailMatInputValid2() Then
            If Session("TblDtl3") Is Nothing Then
                CreateTblDetailMat2()
            End If
            Dim dtMat As DataTable = Session("TblDtl3")
            Dim dv As DataView = dtMat.DefaultView
            If i_u4.Text = "New Detail" Then
                dv.RowFilter = "bomdtl1seq=" & DDLProcess.SelectedValue & " AND bomdtl3reftype='" & bomdtl3reftype.SelectedValue & "' AND bomdtl3refoid=" & bomdtl3refoid.Text
            Else
                dv.RowFilter = "bomdtl1seq=" & DDLProcess.SelectedValue & " AND bomdtl3reftype='" & bomdtl3reftype.SelectedValue & "' AND bomdtl3refoid=" & bomdtl3refoid.Text & " AND bomdtl3seq<>" & bomdtl3seq.Text
            End If
            If dv.Count > 0 Then
                showMessage("This Data has been added before!", 2)
                dv.RowFilter = ""
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u4.Text = "New Detail" Then
                objRow = dtMat.NewRow()
                objRow("bomdtl3seq") = dtMat.Rows.Count + 1
            Else
                objRow = dtMat.Rows(bomdtl3seq.Text - 1)
                objRow.BeginEdit()
            End If
            objRow("bomdtl1seq") = DDLProcess.SelectedValue
            objRow("bomdtl1process") = DDLProcess.SelectedItem.Text
            objRow("bomdtl3reftype") = bomdtl3reftype.SelectedValue
            objRow("bomdtl3refoid") = bomdtl3refoid.Text
            objRow("bomdtl3refqty") = ToDouble(bomdtl3refqty.Text)
            objRow("bomdtl3refunitoid") = bomdtl3refunitoid.SelectedValue
            objRow("bomdtl3note") = bomdtl3note.Text
            objRow("bomdtl3refcode") = bomdtl3refcode.Text
            objRow("bomdtl3refdesc") = bomdtl3refdesc.Text
            objRow("bomdtl3refunit") = bomdtl3refunitoid.SelectedItem.Text
            objRow("precostdtlrefoid") = precostdtlrefoid2.Text
            If i_u4.Text = "New Detail" Then
                dtMat.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            Session("TblDtl3") = dtMat
            GVDtl3.DataSource = dtMat
            GVDtl3.DataBind()
            ClearDetail3()
        End If
    End Sub

    Protected Sub btnClear3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear3.Click
        ClearDetail3()
    End Sub

    Protected Sub GVDtl3_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVDtl3.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
        End If
    End Sub

    Protected Sub GVDtl3_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVDtl3.RowDeleting
        Dim objTable As DataTable = Session("TblDtl3")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("bomdtl3seq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl3") = objTable
        GVDtl3.DataSource = Session("TblDtl3")
        GVDtl3.DataBind()
        ClearDetail3()
    End Sub

    Protected Sub GVDtl3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDtl3.SelectedIndexChanged
        Try
            bomdtl3seq.Text = GVDtl3.SelectedDataKey.Item("bomdtl3seq").ToString
            If Session("TblDtl3") Is Nothing = False Then
                i_u4.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl3")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "bomdtl3seq=" & bomdtl3seq.Text
                DDLProcess.SelectedValue = dv.Item(0).Item("bomdtl1seq").ToString
                bomdtl3refdesc.Text = dv.Item(0).Item("bomdtl3refdesc").ToString
                bomdtl3refoid.Text = dv.Item(0).Item("bomdtl3refoid").ToString
                bomdtl3reftype.SelectedValue = dv.Item(0).Item("bomdtl3reftype").ToString
                bomdtl3refcode.Text = dv.Item(0).Item("bomdtl3refcode").ToString
                bomdtl3refqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("bomdtl3refqty").ToString), 4)
                bomdtl3refunitoid.SelectedValue = dv.Item(0).Item("bomdtl3refunitoid").ToString
                bomdtl3note.Text = dv.Item(0).Item("bomdtl3note").ToString
                precostdtlrefoid2.Text = dv.Item(0).Item("precostdtlrefoid").ToString
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        Finally
            btnSearchMat.Visible = False
            cProc.SetFocusToControl(Me.Page, bomdtl3refqty)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            bomdtl1oid.Text = GenerateID("QL_MSTBOMDTL1", CompnyCode)
            bomdtl2oid.Text = GenerateID("QL_MSTBOMDTL2", CompnyCode)
            bomdtl3oid.Text = GenerateID("QL_MSTBOMDTL3", CompnyCode)
            If Session("oid") = Nothing Or Session("oid") = "" Then
                If CheckDataExists("SELECT COUNT(*) FROM QL_mstbom WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND itemoid=" & itemoid.Text) Then
                    showMessage("Selected Finish Good already used by another data!", 2)
                    Exit Sub
                End If
                bomoid.Text = GenerateID("QL_MSTBOM", CompnyCode)
                InitBOMDesc()
            End If
            bomqty.Text = 0
            Dim dQty_kecil, dDty_besar As Double
            GetUnitConverter(itemoid.Text, itemunitoid.SelectedValue, bomqty.Text, dQty_kecil, dDty_besar)
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_mstbom (cmpcode, bomoid, bomdate, bomdesc, bomcounter, itemoid, itemqty, itemunitoid, precostoid, bomnote, activeflag, dlcstatus, createuser, createtime, upduser, updtime, bomres1, bomres2, bomres3, nomorlot, bomtype, bomqty_unitkecil) VALUES ('" & DDLBusUnit.SelectedValue & "', " & bomoid.Text & ", '" & bomdate.Text & "', '" & Tchar(bomdesc.Text) & "', " & bomcounter.Text & ", " & itemoid.Text & ", " & ToDouble(bomqty.Text) & ", " & itemunitoid.SelectedValue & ", 0, '" & Tchar(bomnote.Text.Trim) & "', '" & activeflag.SelectedValue & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & IIf(ddlbomtype.SelectedValue = "Finish Good", "FG", "WIP") & "', '" & bomres2.Text & "', '" & formulaseq.Text & "', '" & Tchar(nomorlot.Text) & "', '" & ddlbomtype.SelectedValue & "', " & dQty_kecil & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & bomoid.Text & " WHERE tablename='QL_MSTBOM' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_mstbom SET bomdate='" & bomdate.Text & "', bomdesc='" & Tchar(bomdesc.Text) & "', bomcounter=" & bomcounter.Text & ", itemoid=" & itemoid.Text & ", itemunitoid=" & itemunitoid.SelectedValue & ", bomnote='" & Tchar(bomnote.Text.Trim) & "', activeflag='" & activeflag.SelectedValue & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, bomres2='" & bomres2.Text & "', bomres3='" & formulaseq.Text & "', nomorlot='" & Tchar(nomorlot.Text) & "', bomtype='" & ddlbomtype.SelectedValue & "', itemqty=" & ToDouble(bomqty.Text) & ", bomres1='" & IIf(ddlbomtype.SelectedValue = "Finish Good", "FG", "WIP") & "', bomqty_unitkecil=" & dQty_kecil & " WHERE bomoid=" & bomoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_mstbomdtl3 WHERE bomoid=" & bomoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_mstbomdtl2 WHERE bomoid=" & bomoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_mstbomdtl1 WHERE bomoid=" & bomoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If

                If Not Session("TblDtl") Is Nothing Then
                    Dim dtProc As DataTable = SortingDataTable(Session("TblDtl"), "bomdtl1res1")
                    For C1 As Int16 = 0 To dtProc.Rows.Count - 1
                        sSql = "INSERT INTO QL_mstbomdtl1 (cmpcode, bomdtl1oid, bomoid, bomdtl1seq, bomdtl1desc, bomdtl1deptoid, bomdtl1todeptoid, bomdtl1reftype, bomdtl1refoid, bomdtl1refqty, bomdtl1refunitoid, bomdtl1note, bomdtl1res1, upduser, updtime) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C1 + CInt(bomdtl1oid.Text)) & ", " & bomoid.Text & ", " & C1 + 1 & ", '" & Tchar(dtProc.Rows(C1).Item("bomdtl1desc").ToString) & "', " & dtProc.Rows(C1).Item("bomdtl1deptoid") & ", " & dtProc.Rows(C1).Item("bomdtl1todeptoid") & ", '" & dtProc.Rows(C1).Item("bomdtl1reftype").ToString & "', 0, 1, " & itemunitoid.SelectedValue & ", '" & Tchar(dtProc.Rows(C1).Item("bomdtl1note").ToString) & "', '" & dtProc.Rows(C1).Item("bomdtl1res1") & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        If Not Session("TblDtl2") Is Nothing Then
                            Dim drMat() As DataRow = Session("TblDtl2").Select("bomdtl1seq=" & dtProc.Rows(C1).Item("bomdtl1seq"))
                            For C2 As Integer = 0 To drMat.Length - 1
                                sSql = "INSERT INTO QL_mstbomdtl2 (cmpcode, bomdtl2oid, bomdtl1oid, bomoid, bomdtl2seq, bomdtl2reftype, bomdtl2refoid, bomdtl2refqty, bomdtl2refunitoid, bomdtl2note, upduser, updtime, precostdtlrefoid, bomdtl2refqty_unitkecil) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C2 + CInt(bomdtl2oid.Text)) & ", " & (C1 + CInt(bomdtl1oid.Text)) & ", " & bomoid.Text & ", " & C2 + 1 & ", '" & drMat(C2).Item("bomdtl2reftype") & "', " & drMat(C2).Item("bomdtl2refoid") & ", " & ToDouble(drMat(C2).Item("bomdtl2refqty").ToString) & ", " & drMat(C2).Item("bomdtl2refunitoid") & ", '" & Tchar(drMat(C2).Item("bomdtl2note")) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & drMat(C2).Item("precostdtlrefoid") & ", " & ToDouble(drMat(C2).Item("bomdtl2refqty_unitkecil").ToString) & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                            Next
                            bomdtl2oid.Text = CInt(bomdtl2oid.Text) + drMat.Length
                        End If
                        If Not Session("TblDtl3") Is Nothing Then
                            Dim drMat() As DataRow = Session("TblDtl3").Select("bomdtl1seq=" & dtProc.Rows(C1).Item("bomdtl1seq"))
                            For C2 As Integer = 0 To drMat.Length - 1
                                sSql = "INSERT INTO QL_mstbomdtl3 (cmpcode, bomdtl3oid, bomdtl1oid, bomoid, bomdtl3seq, bomdtl3reftype, bomdtl3refoid, bomdtl3refqty, bomdtl3refunitoid, bomdtl3note, upduser, updtime, precostdtlrefoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C2 + CInt(bomdtl3oid.Text)) & ", " & (C1 + CInt(bomdtl1oid.Text)) & ", " & bomoid.Text & ", " & C2 + 1 & ", '" & drMat(C2).Item("bomdtl3reftype") & "', " & drMat(C2).Item("bomdtl3refoid") & ", " & ToDouble(drMat(C2).Item("bomdtl3refqty").ToString) & ", " & drMat(C2).Item("bomdtl3refunitoid") & ", '" & Tchar(drMat(C2).Item("bomdtl3note")) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & drMat(C2).Item("precostdtlrefoid") & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                            Next
                            bomdtl3oid.Text = CInt(bomdtl3oid.Text) + drMat.Length
                        End If
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (dtProc.Rows.Count - 1 + CInt(bomdtl1oid.Text)) & " WHERE tablename='QL_MSTBOMDTL1' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & CInt(bomdtl2oid.Text) - 1 & " WHERE tablename='QL_MSTBOMDTL2' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & CInt(bomdtl3oid.Text) - 1 & " WHERE tablename='QL_MSTBOMDTL3' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message & sSql, 1)
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message & sSql, 1)
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message & sSql, 1)
                conn.Close()
                Exit Sub
            End Try
            Response.Redirect("~\Master\mstBOM.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Master\mstBOM.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If bomoid.Text.Trim = "" Then
            showMessage("Please select Bill Of Material data first!", 2)
            Exit Sub
        End If
        If CheckDataExists("SELECT COUNT(*) FROM QL_mstdlc WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND bomoid=" & bomoid.Text) = True Then
            showMessage("This data can't be deleted because it is being used by another data!", 2)
            Exit Sub
        End If
        If CheckDataExists("SELECT COUNT(*) FROM QL_trnwodtl1 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND bomoid=" & bomoid.Text) = True Then
            showMessage("This data can't be deleted because it is being used by another data!", 2)
            Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_mstprecost SET bomstatus='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND precostoid=" & lastprecostoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_mstbomdtl3 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND bomoid=" & bomoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_mstbomdtl2 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND bomoid=" & bomoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_mstbomdtl1 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND bomoid=" & bomoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_mstbom WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND bomoid=" & bomoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Master\mstBOM.aspx?awal=true")
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnUpload.Click
        Session("TblDtlUpload") = Nothing
        gvListUpload.DataSource = Session("TblDtlUpload")
        gvListUpload.DataBind()
        cProc.SetModalPopUpExtender(btnHideListUpload, pnlListUpload, mpeListUpload, True)
    End Sub

    Protected Sub btnUploadList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnUploadList.Click
        If fuListUpload.HasFile Then
            Dim sExt As String = Path.GetExtension(fuListUpload.PostedFile.FileName).ToLower
            If sExt = ".xls" Then
                showMessage("Excel file type must be *.xlsx!", 2) : Exit Sub
            End If
            If sExt = ".xlsx" Then
                uploaddedfile.Text = "BOMFileUpload_" & Format(GetServerTime(), "MMddyyyyHHmmss") & sExt
                Dim sFile As String = ""
                If IsFolderExists(Server.MapPath("~/UploadFiles/")) Then
                    sFile = Server.MapPath("~/UploadFiles/" & uploaddedfile.Text)
                Else
                    AddNewFolder("UploadFiles")
                    sFile = Server.MapPath("~/UploadFiles/" & uploaddedfile.Text)
                End If
                fuListUpload.SaveAs(sFile)
                Dim sConnString As String = ""
                If sExt = ".xls" Then
                    sConnString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sFile & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=1"""
                ElseIf sExt = ".xlsx" Then
                    sConnString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & sFile & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=1"""
                End If
                Dim conExcel As New OleDbConnection(sConnString)
                Dim cmdExcel As New OleDbCommand()
                cmdExcel.CommandType = System.Data.CommandType.Text
                cmdExcel.Connection = conExcel
                Dim dAdapter As New OleDbDataAdapter(cmdExcel)
                Dim dtExcelRecords As New DataTable()
                conExcel.Open()
                Dim dtExcelSheetName As DataTable = conExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                Dim getExcelSheetName As String = dtExcelSheetName.Rows(0)("Table_Name").ToString()
                cmdExcel.CommandText = "SELECT * FROM [" & getExcelSheetName & "]"
                dAdapter.SelectCommand = cmdExcel
                dAdapter.Fill(dtExcelRecords)
                conExcel.Close()
                If dtExcelRecords.Columns.Count > 0 Then
                    Dim arrColName() As String = {"jenis material", "kode material", "nama material", "qty", "satuan", "dept from", "detil note"}
                    For C1 As Integer = 0 To arrColName.Length - 1
                        Dim bFound As Boolean = False
                        For C2 As Integer = 0 To dtExcelRecords.Columns.Count - 1
                            If arrColName(C1).ToUpper = dtExcelRecords.Columns(C2).ColumnName.ToUpper Then
                                bFound = True : Exit For
                            End If
                        Next
                        If bFound = False Then
                            Session("MsgUpload") = "Column " & arrColName(C1).ToUpper & " is not found in your Excel file! "
                            showMessage(Session("MsgUpload"), 2)
                            Exit Sub
                        End If
                    Next
                Else
                    Session("MsgUpload") = "No column found in your Excel file!"
                    showMessage(Session("MsgUpload"), 2)
                    Exit Sub
                End If
                Dim sCol() As String = {"bomdtl2seq", "bomdtl2reftype", "bomdtl2refoid", "bomdtl2refqty", "bomdtl2refunitoid", "bomdtl2note", "bomdtl2refcode", "bomdtl2refdesc", "bomdtl2refunit", "bomdtl1seq", "bomdtl1process", "roundqtydtl2", "precostdtlrefoid", "bomdtl2deptfrom", "validation"}
                Dim sType() As String = {"System.Int32", "System.String", "System.Int32", "System.Double", "System.Int32", "System.String", "System.String", "System.String", "System.String", "System.Int32", "System.String", "System.Double", "System.Int32", "System.String", "System.String"}
                Dim dtData As DataTable = SetTableDetail("tbldata", sCol, sType)
                Dim iNmr As Integer = 1
                Dim sValid As String = "", sMatType As String = "", sCode As String = "", sMatOid As String = "", sDesc As String = "", iUnitID As Integer = 0, sUnit As String = "", dRoundQty As Double = 0, iPreCostID As Integer, sErrReply As String = "", dQty As Double = 0, sDept As String = "", iSeq As Integer = 0, sProcess As String = ""
                For C1 As Integer = 0 To dtExcelRecords.Rows.Count - 1
                    sValid = "" : sMatType = "" : sCode = "" : sMatOid = "" : sDesc = "" : iUnitID = 0 : sUnit = "" : dRoundQty = 0 : iPreCostID = 0 : dQty = 0 : sDept = "" : iSeq = 0 : sProcess = ""
                    If dtExcelRecords.Rows(C1)(0).ToString <> "" Then
                        sMatType = dtExcelRecords.Rows(C1)("jenis material").ToString.ToLower
                        If sMatType = "" Then
                            sValid &= "No type, "
                        Else
                            If Not (sMatType = "raw" Or sMatType = "general" Or sMatType = "spare part") Then
                                sValid &= "Type is invalid, "
                            Else
                                sCode = dtExcelRecords.Rows(C1)("kode material").ToString
                                If sCode = "" Then
                                    sValid &= "No code, "
                                Else
                                    If Not CheckMaterial(sMatOid, sCode, sDesc, iUnitID, sUnit, sMatType, dRoundQty) Then
                                        sValid &= "No code found, "
                                    Else
                                        If dtExcelRecords.Rows(C1)("satuan").ToString.ToLower <> sUnit.ToLower Then
                                            sValid &= "Unit is invalid, "
                                        End If
                                        iPreCostID = GetPreCostDtlID(sMatOid, sMatType)
                                        If iPreCostID = 0 Then
                                            sValid &= "Material is not available in Pre Costing, "
                                        End If
                                    End If
                                End If
                            End If
                        End If
                        If Not isLengthAccepted("bomdtl2refqty", "QL_mstbomdtl2", ToDouble(dtExcelRecords.Rows(C1)("qty").ToString), sErrReply) Then
                            sValid &= "Qty < " & sErrReply & ", "
                        Else
                            dQty = ToDouble(dtExcelRecords.Rows(C1)("qty").ToString)
                        End If
                        sDept = dtExcelRecords.Rows(C1)("dept from").ToString
                        sValid &= CheckDepartment(sDept, iSeq, sProcess)

                        If sValid <> "" Then
                            sValid = Left(sValid, sValid.Length - 2)
                        End If
                        If sMatType = "raw" Then
                            sMatType = "Raw"
                        ElseIf sMatType = "general" Then
                            sMatType = "General"
                        Else
                            sMatType = "Spare Part"
                        End If
                        Dim dr As DataRow = dtData.NewRow
                        dr("bomdtl2seq") = iNmr
                        dr("bomdtl2reftype") = sMatType
                        dr("bomdtl2refoid") = sMatOid
                        dr("bomdtl2refqty") = dQty
                        dr("bomdtl2refunitoid") = iUnitID
                        dr("bomdtl2note") = dtExcelRecords.Rows(C1)("detil note").ToString
                        dr("bomdtl2refcode") = sCode
                        dr("bomdtl2refdesc") = sDesc
                        dr("bomdtl2refunit") = sUnit
                        dr("bomdtl1seq") = iSeq
                        dr("bomdtl1process") = sProcess
                        dr("roundqtydtl2") = dRoundQty
                        dr("precostdtlrefoid") = iPreCostID
                        dr("bomdtl2deptfrom") = sDept
                        dr("validation") = sValid
                        dtData.Rows.Add(dr)
                        dtData.AcceptChanges()
                        iNmr += 1
                    End If
                Next
                Session("TblDtlUpload") = dtData
                gvListUpload.DataSource = Session("TblDtlUpload")
                gvListUpload.DataBind()
                If File.Exists(sFile) Then
                    File.Delete(sFile)
                End If
                mpeListUpload.Show()
            Else
                Session("MsgUpload") = "File can't be uploaded. File type must be in Excel Extension (*.xlsx)."
                showMessage(Session("MsgUpload"), 2)
            End If
        Else
            Session("MsgUpload") = "Please choose uploaded file first !"
            showMessage(Session("MsgUpload"), 2)
        End If
    End Sub

    Protected Sub btnAddUpload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddUpload.Click
        If Not Session("TblDtlUpload") Is Nothing Then
            Dim dt As DataTable = Session("TblDtlUpload")
            If dt.Rows.Count > 0 Then
                Dim dv As DataView = dt.DefaultView
                If Session("TblDtl2") Is Nothing Then
                    CreateTblDetailMat()
                End If
                Dim dtMat As DataTable = Session("TblDtl2")
                Dim dvMat As DataView = dtMat.DefaultView
                Dim iSeq As Integer = dvMat.Count + 1
                For C1 As Integer = 0 To dv.Count - 1
                    If dv(C1)("validation").ToString = "" Then
                        dvMat.RowFilter = "bomdtl1seq=" & dv(C1)("bomdtl1seq") & " AND bomdtl2reftype='" & dv(C1)("bomdtl2reftype").ToString & "' AND bomdtl2refoid=" & dv(C1)("bomdtl2refoid")
                        If dvMat.Count > 0 Then
                            dvMat(0)("bomdtl2refqty") = ToDouble(dv(C1)("bomdtl2refqty").ToString)
                            dvMat(0)("bomdtl2refunitoid") = ToInteger(dv(C1)("bomdtl2refunitoid").ToString)
                            dvMat(0)("bomdtl2note") = dv(C1)("bomdtl2note").ToString
                            dvMat(0)("bomdtl2refcode") = dv(C1)("bomdtl2refcode").ToString
                            dvMat(0)("bomdtl2refdesc") = dv(C1)("bomdtl2refdesc").ToString
                            dvMat(0)("bomdtl2refunit") = dv(C1)("bomdtl2refunit").ToString
                            dvMat(0)("bomdtl1process") = dv(C1)("bomdtl1process").ToString
                            dvMat(0)("roundqtydtl2") = ToDouble(dv(C1)("roundqtydtl2").ToString)
                            dvMat(0)("precostdtlrefoid") = ToInteger(dv(C1)("precostdtlrefoid").ToString)
                        Else
                            Dim rv As DataRowView = dvMat.AddNew()
                            rv.BeginEdit()
                            rv("bomdtl2seq") = iSeq
                            rv("bomdtl2reftype") = dv(C1)("bomdtl2reftype").ToString
                            rv("bomdtl2refoid") = ToInteger(dv(C1)("bomdtl2refoid").ToString)
                            rv("bomdtl2refqty") = ToDouble(dv(C1)("bomdtl2refqty").ToString)
                            rv("bomdtl2refunitoid") = ToInteger(dv(C1)("bomdtl2refunitoid").ToString)
                            rv("bomdtl2note") = dv(C1)("bomdtl2note").ToString
                            rv("bomdtl2refcode") = dv(C1)("bomdtl2refcode").ToString
                            rv("bomdtl2refdesc") = dv(C1)("bomdtl2refdesc").ToString
                            rv("bomdtl2refunit") = dv(C1)("bomdtl2refunit").ToString
                            rv("bomdtl1seq") = ToInteger(dv(C1)("bomdtl1seq").ToString)
                            rv("bomdtl1process") = dv(C1)("bomdtl1process").ToString
                            rv("roundqtydtl2") = ToDouble(dv(C1)("roundqtydtl2").ToString)
                            rv("precostdtlrefoid") = ToInteger(dv(C1)("precostdtlrefoid").ToString)
                            rv.EndEdit()
                            iSeq += 1
                        End If
                        dvMat.RowFilter = ""
                    End If
                Next
                dv.RowFilter = ""
                Session("TblDtl2") = dvMat.ToTable
                GVDtl2.DataSource = Session("TblDtl2")
                GVDtl2.DataBind()
                ClearDetail2()
                cProc.SetModalPopUpExtender(btnHideListUpload, pnlListUpload, mpeListUpload, False)
            Else
                Session("MsgUpload") = "Please choose uploaded file first !"
                showMessage(Session("MsgUpload"), 2)
            End If
        Else
            Session("MsgUpload") = "Please choose uploaded file first !"
            showMessage(Session("MsgUpload"), 2)
        End If
    End Sub

    Protected Sub btnCancelUpload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelUpload.Click
        cProc.SetModalPopUpExtender(btnHideListUpload, pnlListUpload, mpeListUpload, False)
    End Sub

    Protected Sub gvListUpload_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListUpload.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
        End If
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2) : Exit Sub
        End If
        FilterDDLListItem.SelectedIndex = -1 : FilterTextListItem.Text = "" : gvListItem.SelectedIndex = -1
        BindListItem()
        cProc.SetModalPopUpExtender(btnHideListItem, pnlListItem, mpeListItem, True)
    End Sub

    Protected Sub btnClearItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        itemoid.Text = ""
        itemshortdesc.Text = ""
        itemcode.Text = ""
        bomdesc.Text = ""
        itemunitoid.Items.Clear()
    End Sub

    Protected Sub btnFindListItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindListItem()
        mpeListItem.Show()
    End Sub

    Protected Sub btnAllListItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListItem.SelectedIndex = -1 : FilterTextListItem.Text = "" : gvListItem.SelectedIndex = -1
        BindListItem()
        mpeListItem.Show()
    End Sub

    Protected Sub lkbCloseListItem_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListItem, pnlListItem, mpeListItem, False)
    End Sub

    Protected Sub gvListItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If itemoid.Text <> gvListItem.SelectedDataKey("itemoid").ToString().Trim Then
            btnClearPC_Click(Nothing, Nothing)
        End If
        itemoid.Text = gvListItem.SelectedDataKey.Item("itemoid").ToString
        InitDDLUnit2()
        itemcode.Text = gvListItem.SelectedDataKey.Item("itemcode").ToString
        itemshortdesc.Text = gvListItem.SelectedDataKey.Item("itemshortdesc").ToString
        itemunitoid.SelectedValue = gvListItem.SelectedDataKey("itemunitoid").ToString
        InitBOMDesc()
        GenerateFormulaNumber()
        cProc.SetModalPopUpExtender(btnHideListItem, pnlListItem, mpeListItem, False)
    End Sub

    Protected Sub gvListItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListItem.PageIndexChanging
        gvListItem.PageIndex = e.NewPageIndex
        BindListItem()
        mpeListItem.Show()
    End Sub

    Protected Sub ddlbomtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlbomtype.SelectedValue = "Finish Good" Then
            Label10.Text = "Finish Good"
        Else
            Label10.Text = "WIP"
        End If
    End Sub

    Protected Sub bomqty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        bomqty.Text = ToMaskEdit(ToDouble(bomqty.Text), iRoundDigit)
    End Sub
#End Region

End Class