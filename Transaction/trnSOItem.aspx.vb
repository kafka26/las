Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_SOItem
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Dim pkSize = New System.Drawing.Printing.PaperSize
    Dim PrintDoc1 As New System.Drawing.Printing.PrintDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetTaxValue() As Double
        GetTaxValue = 0
        Try
            sSql = "SELECT TOP 1 gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND gengroup='DEFAULT TAX' ORDER BY updtime DESC"
            GetTaxValue = ToDouble(GetStrData(sSql))
        Catch ex As Exception
            GetTaxValue = 0
        End Try
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid_sel=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("soqty") = ToDouble(GetTextBoxValue(row.Cells(8).Controls))
                                    dtView(0)("matunitoid") = GetDDLValue(row.Cells(9).Controls)
                                    dtView(0)("matunit") = GetDDLText(row.Cells(9).Controls)
                                    dtView(0)("soprice") = ToDouble(GetTextBoxValue(row.Cells(10).Controls))
                                    dtView(0)("sodtlnote") = GetTextBoxValue(row.Cells(11).Controls)
                                    dtView(0)("old_code") = GetTextBoxValue(row.Cells(13).Controls)
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl = dtView.ToTable
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat_price() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matoid=" & cbOid
                                dtView2.RowFilter = "matoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("soqty") = ToDouble(GetTextBoxValue(row.Cells(5).Controls))
                                    dtView(0)("matunitoid") = GetDDLValue(row.Cells(6).Controls)
                                    dtView(0)("matunit") = GetDDLText(row.Cells(6).Controls)
                                    If dtView(0)("matunitoid") = dtView(0)("matunit2oid") Then
                                        dtView(0)("soprice") = ToDouble(GetStrData("select itemminprice from ql_mstitem where itemoid=" & cbOid & ""))
                                    Else
                                        dtView(0)("soprice") = ToDouble(GetStrData("select itemminprice from ql_mstitem where itemoid=" & cbOid & "")) * dtView(0)("itemconvertunit")
                                    End If
                                    dtView(0)("panjang") = ToDouble(GetTextBoxValue(row.Cells(3).Controls))
                                    dtView(0)("roll") = ToDouble(GetTextBoxValue(row.Cells(7).Controls))
                                    dtView(0)("sodtlnote") = GetTextBoxValue(row.Cells(10).Controls)
                                    dtView(0)("dppprice") = (ToDouble(dtView(0)("soprice")) / GetNewTaxValueInclude(sodate.Text))
                                    If dtView2.Count > 0 Then
                                        dtView2(0)("soqty") = ToDouble(GetTextBoxValue(row.Cells(5).Controls))
                                        dtView2(0)("soprice") = ToDouble(GetTextBoxValue(row.Cells(7).Controls))
                                        dtView2(0)("matunitoid") = GetDDLValue(row.Cells(6).Controls)
                                        If dtView2(0)("matunitoid") = dtView2(0)("matunit2oid") Then
                                            dtView2(0)("soprice") = ToDouble(GetStrData("select itemminprice from ql_mstitem where itemoid=" & cbOid & "")) 'ToDouble(GetTextBoxValue(row.Cells(8).Controls))
                                        Else
                                            dtView2(0)("soprice") = ToDouble(GetStrData("select itemminprice from ql_mstitem where itemoid=" & cbOid & "")) * dtView2(0)("itemconvertunit")
                                        End If
                                        dtView2(0)("matunit") = GetDDLText(row.Cells(6).Controls)
                                        dtView2(0)("panjang") = ToDouble(GetTextBoxValue(row.Cells(3).Controls))
                                        dtView2(0)("roll") = ToDouble(GetTextBoxValue(row.Cells(7).Controls))
                                        dtView2(0)("sodtlnote") = GetTextBoxValue(row.Cells(10).Controls)
                                        dtView2(0)("dppprice") = (ToDouble(dtView2(0)("soprice")) / GetNewTaxValueInclude(sodate.Text))
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                dtView(0)("matunit") = GetDDLText(row.Cells(6).Controls)
                                dtView(0)("matunitoid") = GetDDLValue(row.Cells(6).Controls)
                                If dtView(0)("matunitoid") = dtView(0)("matunit2oid") Then
                                    dtView(0)("soprice") = ToDouble(GetStrData("select itemminprice from ql_mstitem where itemoid=" & cbOid & ""))
                                Else
                                    dtView(0)("soprice") = ToDouble(GetStrData("select itemminprice from ql_mstitem where itemoid=" & cbOid & "")) * dtView(0)("itemconvertunit")
                                End If
                                dtView(0)("roll") = ToDouble(GetTextBoxValue(row.Cells(7).Controls))
                                dtView(0)("sodtlnote") = GetTextBoxValue(row.Cells(10).Controls)
                                dtView(0)("dppprice") = (ToDouble(dtView(0)("soprice")) / GetNewTaxValueInclude(sodate.Text))
                                dtView(0)("panjang") = ToDouble(GetTextBoxValue(row.Cells(3).Controls))
                                dtView(0)("soqty") = ToDouble(GetTextBoxValue(row.Cells(5).Controls))

                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                    dtView2(0)("matunit") = GetDDLText(row.Cells(6).Controls)
                                    dtView2(0)("matunitoid") = GetDDLValue(row.Cells(6).Controls)
                                    If dtView2(0)("matunitoid") = dtView2(0)("matunit2oid") Then
                                        dtView2(0)("soprice") = ToDouble(GetStrData("select itemminprice from ql_mstitem where itemoid=" & cbOid & ""))
                                    Else
                                        dtView2(0)("soprice") = ToDouble(GetStrData("select itemminprice from ql_mstitem where itemoid=" & cbOid & "")) * dtView2(0)("itemconvertunit")
                                    End If
                                    dtView2(0)("roll") = ToDouble(GetTextBoxValue(row.Cells(7).Controls))
                                    dtView2(0)("sodtlnote") = GetTextBoxValue(row.Cells(10).Controls)
                                    dtView2(0)("dppprice") = (ToDouble(dtView2(0)("soprice")) / GetNewTaxValueInclude(sodate.Text))
                                    dtView2(0)("panjang") = ToDouble(GetTextBoxValue(row.Cells(3).Controls))
                                    dtView2(0)("soqty") = ToDouble(GetTextBoxValue(row.Cells(5).Controls))
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid_sel=" & cbOid
                                dtView2.RowFilter = "itemoid_sel=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("soqty") = ToDouble(GetTextBoxValue(row.Cells(8).Controls))
                                    dtView(0)("matunitoid") = GetDDLValue(row.Cells(9).Controls)
                                    dtView(0)("matunit") = GetDDLText(row.Cells(9).Controls)
                                    dtView(0)("soprice") = ToDouble(GetTextBoxValue(row.Cells(10).Controls))
                                    dtView(0)("sodtlnote") = GetTextBoxValue(row.Cells(11).Controls)
                                    dtView(0)("old_code") = GetTextBoxValue(row.Cells(13).Controls)
                                    If dtView2.Count > 0 Then
                                        dtView2(0)("soqty") = ToDouble(GetTextBoxValue(row.Cells(8).Controls))
                                        dtView2(0)("matunitoid") = GetDDLValue(row.Cells(9).Controls)
                                        dtView2(0)("matunit") = GetDDLText(row.Cells(9).Controls)
                                        dtView2(0)("soprice") = ToDouble(GetTextBoxValue(row.Cells(10).Controls))
                                        dtView2(0)("sodtlnote") = GetTextBoxValue(row.Cells(11).Controls)
                                        dtView2(0)("old_code") = GetTextBoxValue(row.Cells(13).Controls)
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function GetDDLValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
            End If
        Next
        Return sReturn
    End Function

    Private Function GetDDLText(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedItem.Text
            End If
        Next
        Return sReturn
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If matoid.Text = "" Then
            sError &= "- Please select MATERIAL field!<BR>"
        End If
        'If ToDouble(soqty.Text) > ToDouble(stockQty.Text) Then
        '    sError &= "- SO qty mus be <= stock Qty ..!!<BR>"
        'End If
        If soqty.Text = "" Then
            sError &= "- Please fill QUANTITY field!<BR>"
        Else
            If ToDouble(soqty.Text) <= 0 Then
                sError &= "- QUANTITY must be more than 0!<BR>"
            End If
        End If
        If soprice.Text = "" Then
            sError &= "- Please fill PRICE PER UNIT field!<BR>"
        Else
            If ToDouble(soprice.Text) <= 0 Then
                sError &= "- PRICE PER UNIT must be more than 0!<BR>"
            End If
        End If
        If sodtldisctype1.SelectedValue = "P" Then
            If sodtldiscvalue1.Text <> "" Then
                If ToDouble(sodtldiscvalue1.Text) < 0 Or ToDouble(sodtldiscvalue1.Text) > 100 Then
                    sError &= "- Percentage of DETAIL DISC 1 must be between 0 and 100.<BR>"
                End If
            End If
        Else
            If sodtldiscvalue1.Text <> "" Then
                If ToDouble(sodtldiscvalue1.Text) < 0 Or ToDouble(sodtldiscvalue1.Text) > ToDouble(sodtlamt.Text) Then
                    sError &= "- Amount of DETAIL DISC 1 must be between 0 and " & ToMaskEdit(ToDouble(sodtlamt.Text), 4) & " (DETAIL AMOUNT).<BR>"
                End If
            End If
        End If
        If sodtldisctype2.SelectedValue = "P" Then
            If sodtldiscvalue1.Text <> "" Then
                If ToDouble(sodtldiscvalue2.Text) < 0 Or ToDouble(sodtldiscvalue2.Text) > 100 Then
                    sError &= "- Percentage of DETAIL DISC 2 must be between 0 and 100.<BR>"
                End If
            End If
        Else
            If sodtldiscvalue2.Text <> "" Then
                If ToDouble(sodtldiscvalue2.Text) < 0 Or ToDouble(sodtldiscvalue2.Text) > ToDouble(sodtlamt.Text) Then
                    sError &= "- Amount of DETAIL DISC 2 must be between 0 and " & ToMaskEdit(ToDouble(sodtlamt.Text), 4) & " (DETAIL AMOUNT).<BR>"
                End If
            End If
        End If
        If sodtldisctype3.SelectedValue = "P" Then
            If sodtldiscvalue3.Text <> "" Then
                If ToDouble(sodtldiscvalue3.Text) < 0 Or ToDouble(sodtldiscvalue3.Text) > 100 Then
                    sError &= "- Percentage of DETAIL DISC 3 must be between 0 and 100.<BR>"
                End If
            End If
        Else
            If sodtldiscvalue3.Text <> "" Then
                If ToDouble(sodtldiscvalue3.Text) < 0 Or ToDouble(sodtldiscvalue3.Text) > ToDouble(sodtlamt.Text) Then
                    sError &= "- Amount of DETAIL DISC 3 must be between 0 and " & ToMaskEdit(ToDouble(sodtlamt.Text), 4) & " (DETAIL AMOUNT).<BR>"
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If groupoid.SelectedValue = "" Then
            sError &= "- Please select DIVISION field!<BR>"
        End If
        If sodate.Text = "" Then
            sError &= "- Please fill SO DATE field!<BR>"
        Else
            If Not IsValidDate(sodate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- SO DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If delivDate.Text = "" Then
            sError &= "- Please fill Deliv. DATE field!<BR>"
        Else
            If Not IsValidDate(delivDate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- Deliv. DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If CDate(sodate.Text) > CDate(delivDate.Text) Then
            sError &= "- SO DATE > Deliv. DATE" & sErr & "<BR>"
        End If
        If custoid.Text = "" Then
            sError &= "- Please select CUSTOMER field!<BR>"
        End If
        If sopaytypeoid.SelectedValue = "" Then
            sError &= "- Please select PAYMENT TYPE field!<BR>"
        End If
       
        If somstdisctype.SelectedValue = "P" Then
            If somstdiscvalue.Text <> "" Then
                If ToDouble(somstdiscvalue.Text) < 0 Or ToDouble(somstdiscvalue.Text) > 100 Then
                    sError &= "- Percentage of HEADER DISC must be between 0 and 100.<BR>"
                End If
            End If
        Else
            If somstdiscvalue.Text <> "" Then
                If ToDouble(somstdiscvalue.Text) < 0 Or ToDouble(somstdiscvalue.Text) > (ToDouble(sototalamt.Text) - ToDouble(sototaldiscdtl.Text)) Then
                    sError &= "- Amount of HEADER DISC must be between 0 and " & ToMaskEdit((ToDouble(sototalamt.Text) - ToDouble(sototaldiscdtl.Text)), 4) & " (TOTAL AMT - TOTAL DISC. DTL AMT).<BR>"
                End If
            End If
        End If
        If sopaytypeoid.SelectedItem.Text.ToUpper <> "CASH" Then
            If ToDouble(sototalnetto.Text) > ToDouble(CRFreeIDR.Text) Then
                'sError &= "- Total Netto > Limit Credit Customer! Netto : <strong>" & ToMaskEdit(ToDouble(sototalnetto.Text), 2) & "</strong>, Sisa Limit: <strong>" & ToMaskEdit(ToDouble(CRFreeIDR.Text), 2) & "</strong><BR>"
            End If
        End If
        If somstnote.Text.Length > 100 Then
            sError &= "- HEADER NOTE must be less than 100 characters!<BR>"
        End If
        If soitemsalescode.SelectedValue = "" Then
            sError &= "- Please select SALES field "
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill DETAIL DATA!<BR>"
        Else
            Dim objTbl As DataTable = Session("TblDtl")
            If objTbl.Rows.Count <= 0 Then
                sError &= "- Please fill DETAIL DATA!<BR>"
            End If
        End If
        Dim kode_komisi As String = cKon.ambilscalar("select kode_komisi from QL_mstcust where custoid=" & custoid.Text)
        If kode_komisi = "" Then
            sError &= "- Silakan set kode komisi di master customer!<BR>"
        End If
        If ddlPerson.SelectedValue = 0 And somststatus.Text <> "In Process" Then
            sError &= "- Pilih employee terlebih dahulu!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            somststatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function GetOidDetail() As String
        Dim sReturn As String = ""
        Dim dv As DataView = Session("TblMst").DefaultView
        dv.RowFilter = "CheckValue='True'"
        For C1 As Integer = 0 To dv.Count - 1
            sReturn &= dv(C1)("somstoid").ToString & ","
        Next
        dv.RowFilter = ""
        If sReturn <> "" Then
            sReturn = Left(sReturn, sReturn.Length - 1)
        End If
        Return sReturn
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckSOStatus()
        'Dim nDays As Integer = 7
        'sSql = "SELECT COUNT(*) FROM QL_trnsoitemmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND soitemmststatus='In Process'"
        'If Session("CompnyCode") <> CompnyCode Then
        '    sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        'End If
        'If checkPagePermission("~\Transaction\trnSOItem.aspx", Session("SpecialAccess")) = False Then
        '    sSql &= " AND createuser='" & Session("UserID") & "'"
        'End If
        'If ToDouble(GetStrData(sSql)) > 0 Then
        '    lbSOInProcess.Visible = True
        '    lbSOInProcess.Text = "You have " & GetStrData(sSql) & " In Process SO Finish Good that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        'End If
        'sSql = "SELECT COUNT(*) FROM QL_trnsoitemmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND soitemmststatus='In Approval'"
        'If Session("CompnyCode") <> CompnyCode Then
        '    sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        'End If
        'If checkPagePermission("~\Transaction\trnSOItem.aspx", Session("SpecialAccess")) = False Then
        '    sSql &= " AND createuser='" & Session("UserID") & "'"
        'End If
        'If ToDouble(GetStrData(sSql)) > 0 Then
        '    lbSOInApproval.Visible = True
        '    lbSOInApproval.Text = "You have " & GetStrData(sSql) & " In Approval SO Finish Good that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        'End If
    End Sub

    Private Sub InitAllDDL()
        'Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If FillDDL(DDLBusUnit, sSql) Then
            InitDDLDiv()
        End If
        'Fill DDL SO Type
        sSql = "select gencode,gendesc from QL_mstgen where cmpcode='" & CompnyCode & "' AND gengroup='GROUPITEM' order by gendesc"
        FillDDL(sotype, sSql)
        'Fill DDL Payment Type
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='PAYMENT TYPE' AND activeflag='ACTIVE'"
        FillDDL(sopaytypeoid, sSql)
        ' Fill DDL Currency
        sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        FillDDL(curroid, sSql)
        ' Fill DDL Tag
        sSql = "SELECT cat1oid, cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND cat1res1='Finish Good' AND activeflag='ACTIVE' AND ISNULL(cat1res2, '') IN ('Non WIP', '') ORDER BY cat1shortdesc"
        FillDDL(FilterDDLTag, sSql)
        ' Fill DDL Negotiating Bank
        FillDDLAcctg(soitembank, "VAR_BANK_SO", DDLBusUnit.SelectedValue)
        'Isi Kode sales
        sSql = "select distinct salescode,salescode+'-'+personname from ql_mstperson p inner join QL_mstlevel g on g.leveloid =p.leveloid where upper(g.leveldesc)='SALES' and p.activeflag='ACTIVE'"
        FillDDLWithAdditionalText(soitemsalescode, sSql, "-- Pilih Sales --", "")
        'Fill DDl Cat 4
        sSql = "select genoid,gencode+' - '+gendesc from QL_mstgen where cmpcode='" & CompnyCode & "' AND gengroup='CAT4' order by gendesc"
        FillDDL(FilterDDLCat04, sSql)
        'SO type 2
        sSql = "select genoid,gendesc from QL_mstgen where cmpcode='" & CompnyCode & "' AND gengroup='SOTYPE' order by gendesc"
        FillDDL(sotype2, sSql)
        'SO type Deliv
        sSql = "select genoid,gendesc from QL_mstgen where cmpcode='" & CompnyCode & "' AND gengroup='DELIVTYPE' order by gendesc"
        FillDDL(delivType, sSql)
        'Promo
        sSql = "select genoid,gendesc from QL_mstgen where cmpcode='" & CompnyCode & "' AND gengroup='PROMO' order by gendesc"
        FillDDL(promo, sSql)
        'Set Unit
        sSql = "select DISTINCT genoid,gendesc from QL_trnsodtl sod INNER JOIN QL_mstgen g ON sod.sodtlunitoid=g.genoid WHERE g.gengroup='UNIT'"
        FillDDL(sounitoid, sSql)
        'Isi Kode sales
        sSql = "select personoid, personname from ql_mstperson where activeflag='ACTIVE' order by personname"
        FillDDLWithAdditionalText(ddlPerson, sSql, "-- Pilih Employee --", "0")
    End Sub

    Private Sub InitDDLDiv()
        sSql = "SELECT 0 AS groupoid, '' groupdesc "
        FillDDL(groupoid, sSql)
        'sSql = "SELECT groupoid, groupdesc FROM QL_mstdeptgroup WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND activeflag='ACTIVE'"
        'FillDDL(groupoid, sSql)
    End Sub

    Private Sub BindGvContainer()
        'sSql = "SELECT genoid AS containeroid, gendesc AS containertype, 0.0 AS soitemcontqty, 0.0 AS soitemcontpct FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='CONTAINER TYPE' AND activeflag='ACTIVE' ORDER BY gendesc"
        'Session("TblCont") = cKon.ambiltabel(sSql, "QL_trnsocontmst")
        'gvContainer.DataSource = Session("TblCont")
        'gvContainer.DataBind()
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT som.somstoid, som.sono, CONVERT(VARCHAR(10), som.sodate, 101) AS sodate, c.custname, c.custtype, som.somststatus, som.somstnote, div.divname, case upper(som.somststatus) when 'REJECTED' then rejectreason when 'REVISED' then revisereason else cancelclosedreason end reason, 'False' AS checkvalue, case iskonsinyasi when 'true' then 'Konsinyasi' else '' end konsi FROM QL_trnsomst som INNER JOIN QL_mstcust c ON c.custoid=som.custoid INNER JOIN QL_mstdivision div ON div.cmpcode=som.cmpcode"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " WHERE som.cmpcode='" & Session("CompnyCode") & "'"
        Else
            sSql &= " WHERE som.cmpcode LIKE '%'"
        End If
        sSql &= sSqlPlus & " AND sotype<>'Buffer' ORDER BY CONVERT(DATETIME, som.sodate) DESC, som.somstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trnsomst")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "somstoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub BindCustomer()
        sSql = "SELECT custoid, custcode, custname, custpaymentoid, custaddr, ISNULL(custres3, '') AS custres3, custtype, custtaxable, (cg.custcreditlimitrupiah-cg.custcreditlimitusagerupiah) CRFreeIDR, (cg.custcreditlimitusd-cg.custcreditlimitusageusd) CRFreeUSD, custgroupdischeader1, custgroupdischeader2, custgroupdischeader3,custpaymentoid, custheaderdisc, cg.custgroupname, c.person_id, c.warehouse_id, isnull(pr.salescode, '') salescode FROM QL_mstcust c INNER JOIN QL_mstcustgroup cg ON c.custgroupoid=cg.custgroupoid left join QL_mstperson pr on pr.personoid=c.person_id WHERE c.cmpcode='" & CompnyCode & "' AND " & FilterDDLListCust.SelectedValue & " LIKE '%" & Tchar(FilterTextListCust.Text) & "%' AND c.activeflag='ACTIVE'"
        If cbTag.Checked Then
            If FilterDDLTag.SelectedValue <> "" Then
                sSql &= " AND (custres3 LIKE '%" & FilterDDLTag.SelectedItem.Text & "%' OR custres3 IS NULL OR custres3='')"
            End If
        End If
        sSql &= " ORDER BY custname"
        FillGV(gvListCust, sSql, "QL_mstcust")
    End Sub

    Private Sub GenerateCustPO(ByVal sCode As String)
        Dim sNo As String = "" & sCode & "-" & Format(GetServerTime(), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(soitemcustref, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnsoitemmst WHERE cmpcode='CORP' AND soitemcustref LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), 6)
        socustref.Text = sNo
    End Sub

    Private Sub CountHdrAmount()
        Dim ttx As Double = 0
        CountDtlAmount()
        If ToDouble(sototalamt.Text) > 0 Or sototalamt.Text <> "" Then
            If somstdisctype.SelectedValue = "P" Then
                somstdiscamt.Text = ToMaskEdit(((ToDouble(sototalamt.Text) - ToDouble(sototaldiscdtl.Text)) * ToDouble(somstdiscvalue.Text)) / 100, 2)
            Else
                somstdiscamt.Text = ToMaskEdit(ToDouble(somstdiscvalue.Text), 2)
            End If
            If somstdisctype2.SelectedValue = "P" Then
                somstdiscamt2.Text = ToMaskEdit(((ToDouble(sototalamt.Text) - ToDouble(somstdiscamt.Text) - ToDouble(sototaldiscdtl.Text)) * ToDouble(somstdiscvalue2.Text)) / 100, 2)
            Else
                somstdiscamt2.Text = ToMaskEdit(ToDouble(somstdiscvalue2.Text), 2)
            End If
            If DDLinex.SelectedValue = "INC" Then
                sototalnetto.Text = ToMaskEdit(ToDouble(sototalamt.Text) - ToDouble(somstdiscamt.Text) - ToDouble(somstdiscamt2.Text), 2)
                sodppamt.Text = ToMaskEdit(ToDouble(sototalnetto.Text) / GetNewTaxValueInclude(sodate.Text), 2)
                sovat.Text = ToMaskEdit(ToDouble(sodppamt.Text) * 11 / 100, 2)
            ElseIf DDLinex.SelectedValue = "EXC" Then
                sodppamt.Text = ToMaskEdit(ToDouble(sototalamt.Text) - ToDouble(somstdiscamt.Text) - ToDouble(somstdiscamt2.Text), 2)
                'sovat.Text = ToMaskEdit(ToDouble(sodppamt.Text) * 0.1, 2)
                sototalnetto.Text = ToMaskEdit(ToDouble(sodppamt.Text) + ToDouble(sovat.Text), 2)
            ElseIf DDLinex.SelectedValue = "NT" Then
                sodppamt.Text = ToMaskEdit(ToDouble(sototalamt.Text) - ToDouble(somstdiscamt.Text) - ToDouble(somstdiscamt2.Text), 2)
                sovat.Text = 0
                sototalnetto.Text = ToMaskEdit(ToDouble(sodppamt.Text), 2)
            End If
        End If
    End Sub

    Private Sub CountHdrAmount2()
        CountDtlAmount()
        If somstdisctype2.SelectedValue = "P" Then
            somstdiscamt2.Text = ToMaskEdit(((ToDouble(sototalamt.Text) - ToDouble(sototaldiscdtl.Text) - ToDouble(somstdiscamt.Text)) * ToDouble(somstdiscvalue2.Text)) / 100, 4)
        Else
            somstdiscamt2.Text = ToMaskEdit(ToDouble(somstdiscvalue2.Text), 4)
        End If
        sototaldisc.Text = ToMaskEdit(ToDouble(sototaldiscdtl.Text) + ToDouble(somstdiscamt.Text) + ToDouble(somstdiscamt2.Text), 4)
        sototalnetto.Text = ToMaskEdit(ToDouble(sototalamt.Text) - ToDouble(sototaldisc.Text), 4)
        sovat.Text = ToMaskEdit((ToDouble(sototalnetto.Text) * ToDouble(sotaxamt.Text)) / 100, 4)
        'sograndtotalamt.Text = ToMaskEdit(Math.Round((ToDouble(sototalnetto.Text) + ToDouble(sovat.Text)), 2), 4)
    End Sub

    Private Sub CountHdrAmount3()
        CountDtlAmount()
        If somstdisctype3.SelectedValue = "P" Then
            somstdiscamt3.Text = ToMaskEdit(((ToDouble(sototalamt.Text) - ToDouble(sototaldiscdtl.Text) - ToDouble(somstdiscamt.Text) - ToDouble(somstdiscamt2.Text)) * ToDouble(somstdiscvalue3.Text)) / 100, 4)
        Else
            somstdiscamt3.Text = ToMaskEdit(ToDouble(somstdiscvalue3.Text), 4)
        End If
        sototaldisc.Text = ToMaskEdit(ToDouble(sototaldiscdtl.Text) + ToDouble(somstdiscamt.Text) + ToDouble(somstdiscamt2.Text) + ToDouble(somstdiscamt3.Text), 4)
        sototalnetto.Text = ToMaskEdit(ToDouble(sototalamt.Text) - ToDouble(sototaldisc.Text), 4)
        sovat.Text = ToMaskEdit((ToDouble(sototalnetto.Text) * ToDouble(sotaxamt.Text)) / 100, 4)
        'sograndtotalamt.Text = ToMaskEdit(Math.Round((ToDouble(sototalnetto.Text) + ToDouble(sovat.Text)), 2), 4)
    End Sub

    Private Sub CountDtlAmount()
        RegenerateDtl()
        Dim dAmt As Double = 0
        Dim tax As Double = 0
        If Not Session("TblDtl") Is Nothing Then
            Dim objTable As DataTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                For C1 As Int16 = 0 To objTable.Rows.Count - 1
                    dAmt += ToDouble(objTable.Rows(C1).Item("sodtlnetto").ToString)
                    If DDLinex.SelectedValue = "INC" Then
                        tax += ToDouble(objTable.Rows(C1).Item("sodtltaxamt").ToString)
                    ElseIf DDLinex.SelectedValue = "EXC" Then
                        tax += ToDouble(objTable.Rows(C1).Item("sodtltaxamt").ToString)
                    End If
                Next
            End If
        End If
        sototalamt.Text = ToMaskEdit(dAmt, 2)
        sovat.Text = ToMaskEdit(tax, 2)
    End Sub

    Private Sub RegenerateDtl()
        If Session("TblDtl") Is Nothing = False Then
            Dim d, t As Double
            Dim objTable As DataTable
            objTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                Dim dv As DataView = objTable.DefaultView
                Dim objrow As DataRow = objTable.Rows(dv.Item(0).Item("sodtlseq") - 1)
                If objrow("inex") <> DDLinex.SelectedValue Then
                    If DDLinex.SelectedValue = "EXC" Then
                        For C1 As Integer = 0 To dv.Count - 1
                            objrow = objTable.Rows(dv.Item(C1).Item("sodtlseq") - 1)
                            objrow.BeginEdit()
                            objrow("sodtlprice") = ToMaskEdit(ToDouble(objrow("sodtlprice")) * GetNewTaxValueInclude(sodate.Text), 4)
                            d = ToDouble(objrow("sodtldiscamt1")) + ToDouble(objrow("sodtldiscamt2")) + ToDouble(objrow("sodtldiscamt3"))
                            t = ToDouble(objrow("sodtlprice")) * objrow("sodtlqty")
                            objrow("sodtlamt") = ToMaskEdit(t - d, 4)
                            objrow("sodtlnetto") = ToMaskEdit(t - d, 4)
                            objrow("inex") = DDLinex.SelectedValue
                            objrow.EndEdit()
                        Next
                    ElseIf DDLinex.SelectedValue = "INC" Then
                        For C1 As Integer = 0 To dv.Count - 1
                            objrow = objTable.Rows(dv.Item(C1).Item("sodtlseq") - 1)
                            objrow.BeginEdit()
                            objrow("sodtlprice") = ToMaskEdit(ToDouble(objrow("sodtlprice")) / GetNewTaxValueInclude(sodate.Text), 4)
                            d = ToDouble(objrow("sodtldiscamt1")) + ToDouble(objrow("sodtldiscamt2")) + ToDouble(objrow("sodtldiscamt3"))
                            t = ToDouble(objrow("sodtlprice")) * objrow("sodtlqty")
                            objrow("sodtlamt") = ToMaskEdit(t - d, 4)
                            objrow("sodtlnetto") = ToMaskEdit(t - d, 4)
                            objrow("inex") = DDLinex.SelectedValue
                            objrow.EndEdit()
                        Next
                    ElseIf DDLinex.SelectedValue = "NT" Then
                        If objrow("inex") = "EXC" Then
                            For C1 As Integer = 0 To dv.Count - 1
                                objrow = objTable.Rows(dv.Item(C1).Item("sodtlseq") - 1)
                                objrow.BeginEdit()
                                objrow("sodtlprice") = ToMaskEdit(ToDouble(objrow("sodtlprice")), 4)
                                d = ToDouble(objrow("sodtldiscamt1")) + ToDouble(objrow("sodtldiscamt2")) + ToDouble(objrow("sodtldiscamt3"))
                                t = ToDouble(objrow("sodtlprice")) * objrow("sodtlqty")
                                objrow("sodtlamt") = ToMaskEdit(t - d, 4)
                                objrow("sodtlnetto") = ToMaskEdit(t - d, 4)
                                objrow("inex") = DDLinex.SelectedValue
                                objrow.EndEdit()
                            Next
                        ElseIf objrow("inex") = "INC" Then
                            For C1 As Integer = 0 To dv.Count - 1
                                objrow = objTable.Rows(dv.Item(C1).Item("sodtlseq") - 1)
                                objrow.BeginEdit()
                                objrow("sodtlprice") = ToMaskEdit(ToDouble(objrow("sodtlprice")) * GetNewTaxValueInclude(sodate.Text), 4)
                                d = ToDouble(objrow("sodtldiscamt1")) + ToDouble(objrow("sodtldiscamt2")) + ToDouble(objrow("sodtldiscamt3"))
                                t = ToDouble(objrow("sodtlprice")) * objrow("sodtlqty")
                                objrow("sodtlamt") = ToMaskEdit(t - d, 4)
                                objrow("sodtlnetto") = ToMaskEdit(t - d, 4)
                                objrow("inex") = DDLinex.SelectedValue
                                objrow.EndEdit()
                            Next
                        End If
                    End If
                End If
                Session("TblDtl") = objTable
                gvTblDtl.DataSource = objTable
                gvTblDtl.DataBind()
            End If
        End If
    End Sub

    Private Sub InitDDLCat01()
        'Fill DDL Category 1
        sSql = "SELECT cat1oid, cat1code + ' - ' + cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat1res1='FG' "
        If custres3.Text <> "" Then
            sSql &= " AND ("
            Dim sData() As String = custres3.Text.Split(";")
            For C1 As Integer = 0 To sData.Length - 1
                sSql &= "cat1shortdesc='" & LTrim(sData(C1)) & "'"
                If C1 < sData.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If
        sSql &= " ORDER BY cat1code"
        FillDDL(FilterDDLCat01, sSql)
        If FilterDDLCat01.Items.Count > 0 Then
            InitDDLCat02()
        Else
            FilterDDLCat02.Items.Clear()
            FilterDDLCat03.Items.Clear()
            FilterDDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat02()
        'Fill DDL Category 2
        sSql = "SELECT cat2oid, cat2code + ' - ' + cat2shortdesc FROM QL_mstcat2 WHERE cmpcode='" & CompnyCode & "' AND cat1oid=" & FilterDDLCat01.SelectedValue & " AND activeflag='ACTIVE' ORDER BY cat2code"
        FillDDL(FilterDDLCat02, sSql)
        If FilterDDLCat02.Items.Count > 0 Then
            InitDDLCat03()
        Else
            FilterDDLCat03.Items.Clear()
            FilterDDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat03()
        'Fill DDL Category 3
        sSql = "SELECT cat3oid, cat3code + ' - ' + cat3shortdesc FROM QL_mstcat3 WHERE cmpcode='" & CompnyCode & "' AND cat1oid=" & FilterDDLCat01.SelectedValue & " AND cat2oid=" & FilterDDLCat02.SelectedValue & " AND activeflag='ACTIVE' ORDER BY cat3code"
        FillDDL(FilterDDLCat03, sSql)
        'If FilterDDLCat03.Items.Count > 0 Then
        '    InitDDLCat04()
        'Else
        '    FilterDDLCat04.Items.Clear()
        'End If
    End Sub

    Private Sub InitDDLCat04()
        'Fill DDL Category 4
        sSql = "SELECT cat4oid, cat4code + ' - ' + cat4shortdesc FROM QL_mstcat4 WHERE cmpcode='" & CompnyCode & "' AND cat1oid=" & FilterDDLCat01.SelectedValue & " AND cat2oid=" & FilterDDLCat02.SelectedValue & " AND cat3oid=" & FilterDDLCat03.SelectedValue & " AND activeflag='ACTIVE' ORDER BY cat4code"
        FillDDL(FilterDDLCat04, sSql)
    End Sub

    Private Sub InitDDLUnit(ByVal Oid As String)
        'Fill DDL Unit
        sSql = "select genoid,gendesc from QL_mstgen g inner join QL_mstitem m on m.itemUnit1 = g.genoid where m.itemoid=" & Oid
        FillDDL(unitkeciloid, sSql)
        sSql = "select genoid,gendesc from QL_mstgen g inner join QL_mstitem m on m.itemUnit2 = g.genoid where m.itemoid=" & Oid
        FillDDL(unitbesaroid, sSql)
        'sSql = "select genoid,gendesc from QL_mstgen WHERE gengroup='UNIT'"
        'FillDDL(sounitoid, sSql)
    End Sub

    Private Sub BindMaterial(ByVal sAct As String)
        Dim mtrwhoid As String = "147", sJoin As String = "", sSlct As String = ", '' transitemno, 0 transitemmstoid, 0 transitemdtloid, 0.0 cust_disc_1, 0.0 cust_disc_2, 0.0 cust_disc_3, m.itemoid itemoid_sel", sAdd As String = ""
        If cbCons.Checked Then
            mtrwhoid = "transitemtowhoid"
            sJoin = " INNER JOIN QL_trntransitemdtl d ON d.itemoid=m.itemoid INNER JOIN QL_trntransitemmst trm ON trm.transitemmstoid=d.transitemmstoid "
            sSlct = ", trm.transitemno, d.transitemmstoid, d.transitemdtloid, d.cust_disc_1, d.cust_disc_2, d.cust_disc_3, d.transitemdtloid itemoid_sel"
            sAdd = " AND transitemmststatus='Post' AND cons_type='TRANSFER' and trm.transitemno like 'SJK%' /*and (d.transitemqty - d.qty_return - isnull((select sum(dd.sodtlqty) from QL_trnsodtl dd where dd.itemoid=d.itemoid and dd.transitemmstoid=d.transitemmstoid), 0.0)) > 0*/ AND trm.transitemmstres3=" & custoid.Text
        End If
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sSql = "SELECT 'False' AS CheckValue, m.itemoid, itemcode AS matcode, itemoldcode AS oldcode, itemLongDescription AS matlongdesc, (select dbo.getstockqty(m.itemoid, " & mtrwhoid & ")) AS stockqty, (select dbo.getstockqty(m.itemoid, " & mtrwhoid & ")) AS stockqty2, '' AS [qty], 0 AS [unitoid], '' AS unitdesc, isnull(m.itemPriceList,0.0) AS [amt], '' AS [note], '' AS soqty, itemUnit1 AS matunitoid, itemUnit3 AS matunit2oid, Unit2Unit1Conversion AS itemconvertunit, g.gendesc AS matunit, g.gendesc AS matunit2, isnull(" & IIf(cbCons.Checked, "salesprice", "m.itemPriceList") & ",0.0) AS soprice, m.itemPriceList price_list, '' AS sodtlnote, itemNote1 AS itemnote, 0.0 dppprice, '' panjang,'' roll, replace(itemLongDescription,' ','') as matlongdesctrim, g.gendesc AS unit1, g1.gendesc AS unit3, " & IIf(cbCons.Checked, "salesprice", 0.0) & " soprice_kons " & sSlct & ", (select dbo.getStockValue(m.itemoid)) stockvalue, 0.0 stockvalue_pct, '' old_code FROM QL_mstitem m INNER JOIN QL_mstgen g ON g.genoid=m.itemUnit1 INNER JOIN QL_mstgen g1 ON g1.genoid=m.itemUnit3 " & sJoin & " WHERE m.cmpcode='" & CompnyCode & "' AND m.itemRecordStatus='ACTIVE' AND " & FilterDDLListMatEdit.SelectedValue & " LIKE '%" & Tchar(FilterTextListMatEdit.Text) & "%'" & sAdd
        If sAct = "edit" Then
            'sSql &= " AND " & FilterDDLListMatEdit.SelectedValue & " LIKE '%" & Tchar(FilterTextListMatEdit.Text) & "%'"
        End If
        sSql &= " and (select dbo.getstockqty(m.itemoid, " & mtrwhoid & ")) > 0 ORDER BY itemcode"
        If cbCons.Checked Then
            sSql &= ", trm.transitemno desc"
        End If
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstitem")
        If dtTbl.Rows.Count > 0 Then
            If cbCons.Checked Then
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    Dim harga As Double = ToDouble(dtTbl.Rows(C1)("soprice").ToString)
                    If ToDouble(dtTbl.Rows(C1)("cust_disc_1")) > 0 Then
                        harga -= harga * ToDouble(dtTbl.Rows(C1)("cust_disc_1")) / 100
                    End If
                    If ToDouble(dtTbl.Rows(C1)("cust_disc_2")) > 0 Then
                        harga -= harga * ToDouble(dtTbl.Rows(C1)("cust_disc_2")) / 100
                    End If
                    If ToDouble(dtTbl.Rows(C1)("cust_disc_3")) > 0 Then
                        harga -= harga * ToDouble(dtTbl.Rows(C1)("cust_disc_3")) / 100
                    End If
                    dtTbl.Rows(C1)("soprice") = harga
                Next
            End If
            Session("TblMat") = dtTbl
            Session("TblMatView") = dtTbl
            If sAct = "edit" Then
                gvListMatEdit.DataSource = Session("TblMatView")
                gvListMatEdit.DataBind()
                cProc.SetModalPopUpExtender(btnHideListMatEdit, pnlListMatEdit, mpeListMatEdit, True)
            Else
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        Else
            showMessage("Material data can't be found!", 2)
        End If
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("QL_trnsodtl")
        dtlTable.Columns.Add("sodtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("oldcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("matlongdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("sodtlqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("sodtlunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("sodtlunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("sodtlunit2oid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("sodtlunit2", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("stockqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("itemconvertunit", Type.GetType("System.Double"))
        dtlTable.Columns.Add("sodtlprice", Type.GetType("System.Double"))
        dtlTable.Columns.Add("price_list", Type.GetType("System.Double"))
        dtlTable.Columns.Add("sodtlamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("sodtldisctype1", Type.GetType("System.String"))
        dtlTable.Columns.Add("sodtldiscvalue1", Type.GetType("System.Double"))
        dtlTable.Columns.Add("sodtldiscamt1", Type.GetType("System.Double"))
        dtlTable.Columns.Add("sodtldisctype2", Type.GetType("System.String"))
        dtlTable.Columns.Add("sodtldiscvalue2", Type.GetType("System.Double"))
        dtlTable.Columns.Add("sodtldiscamt2", Type.GetType("System.Double"))
        dtlTable.Columns.Add("sodtldisctype3", Type.GetType("System.String"))
        dtlTable.Columns.Add("sodtldiscvalue3", Type.GetType("System.Double"))
        dtlTable.Columns.Add("sodtldiscamt3", Type.GetType("System.Double"))
        dtlTable.Columns.Add("sodtldpp", Type.GetType("System.Double"))
        dtlTable.Columns.Add("sodtltaxamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("sodtlnetto", Type.GetType("System.Double"))
        dtlTable.Columns.Add("sodtlnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("inex", Type.GetType("System.String"))
        dtlTable.Columns.Add("qtyunitkecil", Type.GetType("System.Double"))
        dtlTable.Columns.Add("qtyunitbesar", Type.GetType("System.Double"))
        dtlTable.Columns.Add("soprice_kons", Type.GetType("System.Double"))
        dtlTable.Columns.Add("transitemno", Type.GetType("System.String"))
        dtlTable.Columns.Add("transitemmstoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("transitemdtloid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("stockvalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("stockvalue_pct", Type.GetType("System.Double"))
        dtlTable.Columns.Add("old_code", Type.GetType("System.String"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub ClearDetail()
        sodtlseq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                sodtlseq.Text = objTable.Rows.Count + 1
                EnableHeader(False)
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        i_u2.Text = "New Detail"
        matoid.Text = ""
        matcode.Text = ""
        oldcode.Text = ""
        matlongdesc.Text = ""
        stockQty.Text = ""
        soqty.Text = ""
        sounitoid.SelectedIndex = -1
        soprice.Text = ""
        sodtlamt.Text = "0.00"
        sodtldisctype1.SelectedIndex = -1
        sodtldiscvalue1.Text = ""
        sodtldiscamt1.Text = ""
        sodtldisctype2.SelectedIndex = -1
        sodtldiscvalue2.Text = ""
        sodtldiscamt2.Text = ""
        sodtldisctype3.SelectedIndex = -1
        sodtldiscvalue3.Text = ""
        sodtldiscamt3.Text = ""
        sodtlnetto.Text = ""
        qtyunitkecil.Text = 0
        qtyunitbesar.Text = 0
        sodtlnote.Text = ""
        gvTblDtl.SelectedIndex = -1
        transitemdtloid.Text = ""
        'btnSearchMat.Visible = True
        sounitoid.Items.Clear()
        btnSearchMat.Visible = True
        btnSearchMatEdit.Visible = False
        btnClearMat.Visible = False
        stockvalue.Text = ""
        old_code.Text = ""
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        btnSearchCust.Visible = bVal : btnClearCust.Visible = bVal
        DDLinex.Enabled = bVal : DDLinex.CssClass = sCss
        cbCons.Enabled = bVal
        'transitemno.Enabled = bVal : transitemno.CssClass = sCss
    End Sub

    Private Sub CheckTax()
        'If sotaxtype.Text = "TAX" Then
        '    DDLinex.Visible = True
        '    sotaxamt.Visible = True
        '    lblSepTax.Visible = True
        'Else
        '    DDLinex.Visible = False
        '    sotaxamt.Visible = False
        '    lblSepTax.Visible = False
        'End If
        'CheckINEX()
    End Sub

    Private Sub CheckINEX()
        If DDLinex.SelectedValue = "INC" Then
            lblvarprice.Visible = True
            lblSepvarprice.Visible = True
            varprice.Visible = True
            soprice.Enabled = False
            soprice.CssClass = "inpTextDisabled"
            sodtldisctype1.Enabled = False
            sodtldisctype1.CssClass = "inpTextDisabled"
            sodtldiscvalue1.Enabled = False
            sodtldiscvalue1.CssClass = "inpTextDisabled"

            'somstdiscvalue.Enabled = False
            'somstdiscvalue.CssClass = "inpTextDisabled"
            'somstdisctype.Enabled = False
            'somstdisctype.CssClass = "inpTextDisabled"
        Else
            lblvarprice.Visible = False
            lblSepvarprice.Visible = False
            varprice.Visible = False
            soprice.Enabled = True
            soprice.CssClass = "inpText"
            sodtldisctype1.Enabled = True
            sodtldisctype1.CssClass = "inpText"
            sodtldiscvalue1.Enabled = True
            sodtldiscvalue1.CssClass = "inpText"

            'somstdiscvalue.Enabled = True
            'somstdiscvalue.CssClass = "inpText"
            'somstdisctype.Enabled = True
            'somstdisctype.CssClass = "inpText"
        End If
    End Sub

    Private Sub CountAllDtlAmount()
        Dim disc1Dtl, disc2Dtl, disc3Dtl As Double
        sodtlamt.Text = Math.Round(ToDouble(soqty.Text) * ToDouble(soprice.Text), 0, MidpointRounding.AwayFromZero)
        If sodtldiscvalue1.Text <> "" Then
            If sodtldisctype1.SelectedValue = "P" Then
                disc1Dtl = (ToDouble(sodtlamt.Text) * ToDouble(sodtldiscvalue1.Text)) / 100
                sodtldiscamt1.Text = ToMaskEdit(disc1Dtl, 2)
            Else
                disc1Dtl = ToDouble(sodtldiscvalue1.Text)
                sodtldiscamt1.Text = ToMaskEdit(disc1Dtl * ToDouble(soqty.Text), 2)
            End If
            sodtlnetto.Text = ToMaskEdit(ToDouble(sodtlamt.Text) - ToDouble(sodtldiscamt1.Text), 2)
            If sodtldiscvalue2.Text <> "" Then
                If sodtldisctype2.SelectedValue = "P" Then
                    disc2Dtl = (ToDouble(sodtlnetto.Text) * ToDouble(sodtldiscvalue2.Text)) / 100
                    sodtldiscamt2.Text = ToMaskEdit(disc2Dtl, 2)
                Else
                    disc2Dtl = ToDouble(sodtldiscvalue2.Text)
                    sodtldiscamt2.Text = ToMaskEdit(disc2Dtl * ToDouble(soqty.Text), 2)
                End If
                sodtlnetto.Text = ToMaskEdit(ToDouble(sodtlnetto.Text) - ToDouble(sodtldiscamt2.Text), 2)
                If sodtldiscvalue3.Text <> "" Then
                    If sodtldisctype3.SelectedValue = "P" Then
                        disc3Dtl = (ToDouble(sodtlnetto.Text) * ToDouble(sodtldiscvalue3.Text)) / 100
                        sodtldiscamt3.Text = ToMaskEdit(disc3Dtl, 2)
                    Else
                        disc3Dtl = ToDouble(sodtldiscvalue3.Text)
                        sodtldiscamt3.Text = ToMaskEdit(disc3Dtl * ToDouble(soqty.Text), 2)
                    End If
                    sodtlnetto.Text = ToMaskEdit(ToDouble(sodtlnetto.Text) - ToDouble(sodtldiscamt3.Text), 2)
                End If
            End If
            sodtlamt.Text = ToMaskEdit(Math.Round(ToDouble(sodtlnetto.Text), 0, MidpointRounding.AwayFromZero), 2)
        Else
            sodtldiscvalue1.Text = ""
            sodtldiscvalue2.Text = ""
            sodtldiscvalue3.Text = ""
            sodtldiscamt1.Text = ""
            sodtldiscamt2.Text = ""
            sodtldiscamt3.Text = ""
        End If
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT som.cmpcode, somstoid AS somstoid, som.periodacctg, somstres1 AS sotype, sotype2, sodelivtype, somstpromo, somstrefno, sono AS sono, sodate AS sodate, soetd, som.custoid, sopaytypeoid AS sopaytypeoid, sototalamt AS sototalamt, somstdisctype, somstdiscvalue, somstdiscamt, sototalnetto AS sototalnetto, sotaxtype AS sotaxtype, sotaxamt AS sotaxamt, somstnote AS somstnote, somststatus AS somststatus, custname, som.createuser, som.createtime, som.upduser, som.updtime, som.curroid, (cg.custcreditlimitrupiah-cg.custcreditlimitusagerupiah) CRFreeIDR, (cg.custcreditlimitusd-cg.custcreditlimitusageusd) CRFreeUSD, somstdisctype2, somstdiscvalue2, somstdiscamt2, sosalescode, somstdpp, iskonsinyasi, transitemmstoid, custgroupname, employee_id, som.warehouse_id FROM QL_trnsomst som INNER JOIN QL_mstcust c ON c.custoid=som.custoid INNER JOIN QL_mstcustgroup cg ON c.custgroupoid=cg.custgroupoid WHERE somstoid=" & sOid
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = xreader("cmpcode").ToString
                somstoid.Text = Trim(xreader("somstoid").ToString)
                periodacctg.Text = Trim(xreader("periodacctg").ToString)
                periodSO.Text = Trim(xreader("periodacctg").ToString)
                sotype.SelectedValue = xreader("sotype").ToString
                CRFreeIDR.Text = xreader("CRFreeIDR").ToString
                CRFreeUSD.Text = xreader("CRFreeUSD").ToString
                delivDate.Text = Format(xreader("soetd"), "MM/dd/yyyy")
                sono.Text = Trim(xreader("sono").ToString)
                sodate.Text = Format(xreader("sodate"), "MM/dd/yyyy")
                custoid.Text = Trim(xreader("custoid").ToString)
                delivType.SelectedValue = xreader("sodelivtype")
                promo.SelectedValue = xreader("somstpromo")
                sotype2.SelectedValue = xreader("sotype2")
                sopaytypeoid.SelectedValue = xreader("sopaytypeoid")
                noRef.Text = Trim(xreader("somstrefno").ToString)
                sodppamt.Text = ToMaskEdit(ToDouble(Trim(xreader("somstdpp").ToString)), 2)
                sototalamt.Text = ToMaskEdit(ToDouble(Trim(xreader("sototalamt").ToString)), 2)
                somstdisctype.SelectedValue = Trim(xreader("somstdisctype").ToString)
                somstdiscvalue.Text = ToMaskEdit(ToDouble(Trim(xreader("somstdiscvalue").ToString)), 2)
                somstdiscamt.Text = ToMaskEdit(ToDouble(Trim(xreader("somstdiscamt").ToString)), 2)
                somstdisctype2.SelectedValue = Trim(xreader("somstdisctype2").ToString)
                somstdiscvalue2.Text = ToMaskEdit(ToDouble(Trim(xreader("somstdiscvalue2").ToString)), 2)
                somstdiscamt2.Text = ToMaskEdit(ToDouble(Trim(xreader("somstdiscamt2").ToString)), 2)
                DDLinex.SelectedValue = Trim(xreader("sotaxtype").ToString)
                sovat.Text = ToMaskEdit(ToDouble(Trim(xreader("sotaxamt").ToString)), 2)
                sototalnetto.Text = ToMaskEdit(ToDouble(Trim(xreader("sototalnetto").ToString)), 2)
                somstnote.Text = Trim(xreader("somstnote").ToString)
                somststatus.Text = Trim(xreader("somststatus").ToString)
                custname.Text = Trim(xreader("custname").ToString)
                'DDLBusUnit_SelectedIndexChanged(Nothing, Nothing)
                createuser.Text = Trim(xreader("createuser").ToString)
                createtime.Text = Trim(xreader("createtime").ToString)
                upduser.Text = Trim(xreader("upduser").ToString)
                updtime.Text = Trim(xreader("updtime").ToString)
                curroid.SelectedValue = xreader("curroid").ToString
                curroid_SelectedIndexChanged(Nothing, Nothing)
                soitemsalescode.SelectedValue = Trim(xreader("sosalescode").ToString)
                cbCons.Checked = xreader("iskonsinyasi").ToString
                cbCons_CheckedChanged(Nothing, Nothing)
                'transitemno.SelectedValue = xreader("transitemmstoid").ToString
                custname2.Text = Trim(xreader("custgroupname").ToString)
                ddlPerson.SelectedValue = xreader("employee_id").ToString
                warehouse_id.Text = xreader("warehouse_id").ToString
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False : btnSendApproval.Visible = False
            Exit Sub
        End Try
        DDLBusUnit.CssClass = "inpTextDisabled"
        DDLBusUnit.Enabled = False
        btnSearchCust.Visible = False
        btnClearCust.Visible = False
        sotype.CssClass = "inpTextDisabled"
        sotype.Enabled = False
        curroid.CssClass = "inpTextDisabled"
        curroid.Enabled = False
        If somststatus.Text <> "In Process" And somststatus.Text <> "Revised" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnSendApproval.Visible = False
            btnAddToList.Visible = False
            gvTblDtl.Columns(0).Visible = False
            gvTblDtl.Columns(gvTblDtl.Columns.Count - 1).Visible = False
            lblTrnNo.Text = "SO No."
            somstoid.Visible = False
            sono.Visible = True
        End If
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sSql = "SELECT sod.sodtlseq AS sodtlseq, sod.itemoid AS itemoid, itemcode AS matcode, old_code AS oldcode, m.itemLongDescription AS matlongdesc, (select dbo.getstockqty(sod.itemoid, ISNULL(transitemtowhoid, 147))) AS stockqty, (select dbo.getstockqty(sod.itemoid, ISNULL(transitemtowhoid, 147))) AS stockqty2,  sod.sodtlqty AS sodtlqty, sod.sodtlunitoid AS sodtlunitoid, g.gendesc AS sodtlunit, sod.sodtlprice AS sodtlprice, sod.sodtlamt AS sodtlamt, sod.sodtldisctype1 AS sodtldisctype1, sod.sodtldiscvalue1 AS sodtldiscvalue1, sod.sodtldiscamt1 AS sodtldiscamt1, sod.sodtldisctype2 AS sodtldisctype2, sod.sodtldiscvalue2 AS sodtldiscvalue2, sod.sodtldiscamt2 AS sodtldiscamt2, sod.sodtldisctype3 AS sodtldisctype3, sod.sodtldiscvalue3 AS sodtldiscvalue3, sod.sodtldiscamt3 AS sodtldiscamt3, sod.sodtlnetto AS sodtlnetto, sod.sodtlnote AS sodtlnote, m.itemUnit1 sounitoid, m.Unit2Unit1Conversion AS itemconvertunit, soqty_unitkecil AS qtyunitkecil, soqty_unitbesar AS qtyunitbesar,(SELECT sotaxtype FROM QL_trnsomst som where som.somstoid=sod.somstoid) AS inex, sodtldpp, sodtltaxamt, soprice_kons, ISNULL(sod.transitemmstoid, 0) transitemmstoid, ISNULL(sod.transitemdtloid, 0) transitemdtloid, ISNULL(transitemno,'') transitemno, (select dbo.getStockValue(sod.itemoid)) stockvalue, 0.0 stockvalue_pct, old_code, itemPriceList price_list FROM QL_trnsodtl sod INNER JOIN QL_mstitem m ON m.itemoid=sod.itemoid INNER JOIN QL_mstgen g ON g.genoid=sod.sodtlunitoid LEFT JOIN (SELECT transitemno, m.transitemmstoid, d.transitemdtloid, d.itemoid, transitemtowhoid FROM QL_trntransitemdtl d INNER JOIN QL_trntransitemmst m ON m.transitemmstoid=d.transitemmstoid WHERE iskonsinyasi='True') trn ON trn.itemoid=sod.itemoid AND trn.transitemmstoid=sod.transitemmstoid WHERE sod.somstoid=" & sOid & " ORDER BY sod.sodtlseq"
        Dim objTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnsodtl")
        For C1 As Integer = 0 To objTbl.Rows.Count - 1
            Dim dSelisih As Double = ToDouble(objTbl.Rows(C1)("sodtlprice")) - ToDouble(objTbl.Rows(C1)("stockvalue"))
            objTbl.Rows(C1)("stockvalue_pct") = 0
            If ToDouble(objTbl.Rows(C1)("stockvalue")) > 0 Then
                objTbl.Rows(C1)("stockvalue_pct") = dSelisih / ToDouble(objTbl.Rows(C1)("stockvalue")) * 100
            End If
        Next
        Session("TblDtl") = objTbl
        gvTblDtl.DataSource = objTbl
        gvTblDtl.DataBind()
        CountHdrAmount()
        ClearDetail()
    End Sub

    Private Sub PrintReport(ByVal sOid As String)
        Try
            Dim sWhere As String
            Dim dv As DataView = Session("TblMst").DefaultView
            dv.RowFilter = "checkvalue='True'"
            Dim rptFile As String = "rptSOItem"
            If ddlPrintOption.SelectedValue = "so2" Then
                rptFile = "rptSOItem2"
            End If

            report.Load(Server.MapPath(folderReport & rptFile & ".rpt"))
            If Session("CompnyCode") <> CompnyCode Then
                sWhere = " WHERE som.cmpcode='" & Session("CompnyCode") & "'"
            Else
                sWhere = " WHERE som.cmpcode LIKE '%'"
            End If
            If sOid = "" Then
                sWhere &= " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND som.sodate>='" & FilterPeriod1.Text & " 00:00:00' AND som.sodate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sWhere &= " AND som.somststatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Transaction\trnSOItem.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND som.createuser='" & Session("UserID") & "'"
                End If
            Else
                sWhere &= " AND som.somstoid IN (" & sOid & ")"
            End If
            report.SetParameterValue("userID", Session("UserID"))
            report.SetParameterValue("userName", GetStrData("SELECT profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "SO_PrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
        Response.Redirect("~\Transaction\trnSOItem.aspx?awal=true")
    End Sub

    Private Sub PrintReport2(ByVal sOid As String)
        Try
            If cbprice.Checked = False Then
                If DDLTypeCust.SelectedIndex = 0 Then
                    report.Load(Server.MapPath(folderReport & "rptSOItemGD.rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptSOItemGD.rpt"))
                End If
            Else
                report.Load(Server.MapPath(folderReport & "rptSOItem2.rpt"))
            End If
            Dim sWhere As String
            If Session("CompnyCode") <> CompnyCode Then
                sWhere = " WHERE som.cmpcode='" & Session("CompnyCode") & "'"
            Else
                sWhere = " WHERE som.cmpcode LIKE '%'"
            End If
            If sOid = "" Then
                sWhere &= " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND som.soitemdate>='" & FilterPeriod1.Text & " 00:00:00' AND som.soitemdate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sWhere &= " AND som.soitemmststatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Transaction\trnSOItem.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND som.createuser='" & Session("UserID") & "'"
                End If
            Else
                sWhere &= " AND som.soitemmstoid IN (" & sOid & ")"
            End If
            report.SetParameterValue("userID", Session("UserID"))
            report.SetParameterValue("userName", GetStrData("SELECT profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)

            PrintDoc1.PrinterSettings.PrinterName = "Microsoft XPS Document Writer"
            Dim k As Integer
            For k = 0 To PrintDoc1.PrinterSettings.PaperSizes.Count - 1
                If PrintDoc1.PrinterSettings.PaperSizes.Item(k).PaperName = "tjkNew" Then
                    pkSize = PrintDoc1.PrinterSettings.PaperSizes.Item(k)
                End If
            Next
            report.PrintOptions.PrinterName = "Microsoft XPS Document Writer"
            report.PrintOptions.PaperSize = CType(pkSize.RawKind, CrystalDecisions.Shared.PaperSize)
            report.PrintOptions.PaperSource = CrystalDecisions.Shared.PaperSource.Manual

            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "SOFinishGoodPrintOutGD")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
        Response.Redirect("~\Transaction\trnSOItem.aspx?awal=true")
    End Sub

    Private Sub calcKonversi()
        If sounitoid.SelectedValue = GetStrData("select itemunitoid from QL_mstitem where itemoid=" & matoid.Text & "") Then
            If ToDouble(panjang.Text) > 0 Then
                qtyunitkecil.Text = Math.Round(ToDouble(soqty.Text) * ToDouble(panjang.Text), 2)
            Else
                qtyunitkecil.Text = Math.Round(ToDouble(soqty.Text) * ToDouble(GetStrData("select itemconvertunit from QL_mstitem where itemoid=" & matoid.Text & "")), 2)
            End If
            qtyunitbesar.Text = ToDouble(soqty.Text)
        Else
            qtyunitkecil.Text = ToDouble(soqty.Text)
            If ToDouble(panjang.Text) > 0 Then
                qtyunitbesar.Text = Math.Round(ToDouble(soqty.Text) / ToDouble(panjang.Text), 2)
            Else
                qtyunitbesar.Text = Math.Round(ToDouble(soqty.Text) / ToDouble(GetStrData("select itemconvertunit from QL_mstitem where itemoid=" & matoid.Text & "")), 2)
            End If
        End If
    End Sub

#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim scode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = scode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnSOItem.aspx")
        End If
        If checkPagePermission("~\Transaction\trnSOItem.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - SO Finish Good"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnSendApproval.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to send this data for APPROVAL?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            CheckSOStatus()
            InitAllDDL()
            CheckTax()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                BindGvContainer()
                somstoid.Text = GenerateID("QL_TRNSOMST", CompnyCode)
                sodate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                periodSO.Text = GetDateToPeriodAcctg(GetServerTime())
                delivDate.Text = Format(DateAdd(DateInterval.Day, 3, GetServerTime()), "MM/dd/yyyy")
                soitemetd.Text = Format(DateAdd(DateInterval.Day, 7, GetServerTime()), "MM/dd/yyyy")
                somststatus.Text = "In Process"
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
                curroid_SelectedIndexChanged(Nothing, Nothing)
            End If
            'Check Rate Pajak & Standart
            Dim cRate As New ClassRate()
            cRate.SetRateValue(1, Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, 2) : Exit Sub
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            gvTblDtl.DataSource = dt
            gvTblDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnSOItem.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lkbSOInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSOInProcess.Click
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim nDays As Integer = 7
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, som.updtime, GETDATE()) > " & nDays & " AND som.soitemmststatus='In Process' "
        If checkPagePermission("~\Transaction\trnSOItem.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND som.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub lbSOInApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSOInApproval.Click
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 1
        Dim nDays As Integer = 7
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, som.updtime, GETDATE()) > " & nDays & " AND som.soitemmststatus='In Approval' "
        If checkPagePermission("~\Transaction\trnSOItem.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND som.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND som.sodate>='" & FilterPeriod1.Text & " 00:00:00' AND som.sodate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND som.somststatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Transaction\trnSOItem.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND som.createuser='" & Session("UserID") & "'"
        End If
        If cb_so_type.Checked Then
            sSqlPlus &= " AND som.iskonsinyasi='" & IIf(ddl_so_type.SelectedValue.ToLower() = "konsinyasi", "true", "false") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        Dim sSqlPlus As String = ""
        If checkPagePermission("~\Transaction\trnSOItem.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND som.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData((sSqlPlus))
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        ' Fill DDL Account
        sSql = "SELECT a.accountoid, b.bankname+' - '+a.accountno  FROM QL_mstaccount a INNER JOIN QL_mstbank b ON b.bankoid = a.bankoid WHERE a.cmpcode='" & DDLBusUnit.SelectedValue & "' AND a.activeflag='ACTIVE'"
        FillDDL(accountoid, sSql)
        ' Fill DDL Negotiating Bank
        FillDDLAcctg(soitembank, "VAR_BANK_SO", DDLBusUnit.SelectedValue)
        InitDDLDiv()
    End Sub

    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchCust.Click
        FilterDDLListCust.SelectedIndex = -1 : FilterTextListCust.Text = "" : gvListCust.SelectedIndex = -1 : cbTag.Checked = False : FilterDDLTag.SelectedIndex = -1 : FilterDDLType.SelectedIndex = -1
        BindCustomer()
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
    End Sub

    Protected Sub btnClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearCust.Click
        custoid.Text = ""
        custname.Text = ""
        custname2.Text = ""
        CRFreeIDR.Text = ""
        CRFreeUSD.Text = ""
        somstdiscvalue.Text = ""
        cbCons.Checked = False
        'transitemno.Items.Clear()
    End Sub

    Protected Sub btnFindListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListCust.Click
        BindCustomer()
        mpeListCust.Show()
    End Sub

    Protected Sub btnAllListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListCust.Click
        FilterDDLListCust.SelectedIndex = -1 : FilterTextListCust.Text = "" : gvListCust.SelectedIndex = -1 : cbTag.Checked = False : FilterDDLTag.SelectedIndex = -1 : FilterDDLType.SelectedIndex = -1
        BindCustomer()
        mpeListCust.Show()
    End Sub

    Protected Sub gvListCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListCust.PageIndexChanging
        gvListCust.PageIndex = e.NewPageIndex
        BindCustomer()
        mpeListCust.Show()
    End Sub

    Protected Sub gvListCust_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListCust.SelectedIndexChanged
        If custoid.Text <> gvListCust.SelectedDataKey.Item("custoid").ToString Then
            btnClearCust_Click(Nothing, Nothing)
        End If
        custoid.Text = gvListCust.SelectedDataKey.Item("custoid").ToString
        custname.Text = gvListCust.SelectedDataKey.Item("custname").ToString
        CRFreeIDR.Text = ToMaskEdit(gvListCust.SelectedDataKey.Item("CRFreeIDR").ToString, 2)
        CRFreeUSD.Text = ToMaskEdit(gvListCust.SelectedDataKey.Item("CRFreeUSD").ToString, 2)
        sopaytypeoid.SelectedValue = gvListCust.SelectedDataKey.Item("custpaymentoid").ToString
        somstdiscvalue.Text = ToMaskEdit(gvListCust.SelectedDataKey.Item("custheaderdisc").ToString, 2)
        custname2.Text = gvListCust.SelectedDataKey.Item("custgroupname").ToString
        If cbCons.Checked Then
            warehouse_id.Text = gvListCust.SelectedDataKey.Item("warehouse_id").ToString
        End If
        soitemsalescode.SelectedValue = gvListCust.SelectedDataKey.Item("salescode").ToString
        somstdiscvalue_TextChanged(Nothing, Nothing)
        If gvListCust.SelectedDataKey.Item("custtaxable").ToString = "1" Then
            DDLinex.SelectedIndex = 1
        Else
            DDLinex.SelectedIndex = 0
        End If
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub

    Protected Sub lbCloseListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListCust.Click
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub

    Protected Sub curroid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles curroid.SelectedIndexChanged
        Dim sErr As String = ""
        If sodate.Text = "" Then
            showMessage("Please fill SO Date first!", 2)
            Exit Sub
        Else
            If Not IsValidDate(sodate.Text, "MM/dd/yyyy", sErr) Then
                showMessage("SO Date is invalid. " & sErr, 2)
                Exit Sub
            End If
        End If
        Dim cRate As New ClassRate()
        If curroid.SelectedValue <> "" Then
            cRate.SetRateValue(CInt(curroid.SelectedValue), sodate.Text)
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, 2)
                Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, 2)
                Exit Sub
            End If
            rateoid.Text = cRate.GetRateDailyOid
            rateidrvalue.Text = ToMaskEdit(cRate.GetRateDailyIDRValue, GetRoundValue(cRate.GetRateDailyIDRValue.ToString))
            soratetousd.Text = ToMaskEdit(cRate.GetRateDailyUSDValue, GetRoundValue(cRate.GetRateDailyUSDValue.ToString))
            rate2oid.Text = cRate.GetRateMonthlyOid
            rate2idrvalue.Text = ToMaskEdit(cRate.GetRateMonthlyIDRValue, GetRoundValue(cRate.GetRateMonthlyIDRValue.ToString))
            sorate2tousd.Text = ToMaskEdit(cRate.GetRateMonthlyUSDValue, GetRoundValue(cRate.GetRateMonthlyUSDValue.ToString))
        End If
    End Sub

    Protected Sub somstdisctype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles somstdisctype.SelectedIndexChanged
        CountHdrAmount()
    End Sub

    Protected Sub somstdiscvalue_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles somstdiscvalue.TextChanged
        CountHdrAmount()
    End Sub

    Protected Sub sotaxtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles sotaxtype.SelectedIndexChanged
        CheckTax()
        DDLinex.SelectedIndex = 0
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        Dim sErr As String = ""
        If sodate.Text = "" Then
            showMessage("Please fill SO Date first!", 2)
            Exit Sub
        Else
            If Not IsValidDate(sodate.Text, "MM/dd/yyyy", sErr) Then
                showMessage("SO Date is invalid. " & sErr, 2)
                Exit Sub
            End If
        End If
        If custoid.Text = "" Then
            showMessage("Please select Customer first!", 2)
            Exit Sub
        End If
        cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
        Session("TblMat") = Nothing : Session("TblMatView") = Nothing : gvListMat.DataSource = Nothing : gvListMat.DataBind()
        BindMaterial("new")
        InitDDLCat01()
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(10).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = 0
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 4)
                    End If
                End If
            Next

            Dim iUnitID As String = e.Row.Cells(9).ToolTip
            Dim cc2 As System.Web.UI.ControlCollection = e.Row.Cells(8).Controls
            For Each myControl As System.Web.UI.Control In cc2
                If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                    iUnitID = CType(myControl, System.Web.UI.WebControls.DropDownList).ToolTip
                End If
            Next

            '    'Isi untuk dropdownlist unit
            sSql = "select genoid,gendesc FROM( select genoid,gendesc from QL_mstitem sod INNER JOIN QL_mstgen g ON sod.itemUnit1=g.genoid WHERE g.gengroup='UNIT' AND sod.itemcode='" & e.Row.Cells(1).Text & "' UNION ALL select genoid,gendesc from QL_mstitem sod INNER JOIN QL_mstgen g ON sod.itemUnit2=g.genoid WHERE g.gengroup='UNIT' AND sod.itemcode='" & e.Row.Cells(1).Text & "' UNION ALL select genoid,gendesc from QL_mstitem sod INNER JOIN QL_mstgen g ON sod.itemUnit3=g.genoid WHERE g.gengroup='UNIT' AND sod.itemcode='" & e.Row.Cells(1).Text & "' )AS dt"

            Dim ods As New SqlDataAdapter(sSql, conn)
            Dim objTablee As New DataTable
            ods.Fill(objTablee)

            Dim cc3 As System.Web.UI.ControlCollection = e.Row.Cells(9).Controls
            'Dim price As String = e.Row.Cells(8).Text
            Dim tempString As String = ""
            For Each myControl As System.Web.UI.Control In cc3
                If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                    For x As Integer = 0 To objTablee.Rows.Count - 1
                        Dim textTemp As String = objTablee.Rows(x).Item("gendesc")
                        Dim oidTemp As String = objTablee.Rows(x).Item("genoid")
                        CType(myControl, System.Web.UI.WebControls.DropDownList).Items.Add(New ListItem(textTemp, oidTemp))
                    Next
                    CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue = iUnitID
                    tempString = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
                End If
            Next
        End If
    End Sub

    Protected Sub FilterDDLCat01_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat01.SelectedIndexChanged
        InitDDLCat02()
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat02_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat02.SelectedIndexChanged
        InitDDLCat03()
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat03_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat03.SelectedIndexChanged
        InitDDLCat04()
        mpeListMat.Show()
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim sFilter As String = ""
            Dim sSplit() As String = FilterTextListMat.Text.Split(" ")
            For C1 As Integer = 0 To sSplit.Length - 1
                sFilter &= FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(sSplit(C1)) & "%'"
                If C1 < sSplit.Length - 1 Then
                    sFilter &= " AND "
                End If
            Next
            dtView.RowFilter = sFilter
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : FilterDDLStock.SelectedIndex = -1 : FilterTextStock.Text = "0"
        cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
        FilterDDLCat01.SelectedIndex = -1 : InitDDLCat01()
        If UpdateCheckedMat() Then
            BindMaterial("new")
            Session("TblMatView") = Session("TblMat")
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("CheckValue") = "True"
                    dtTbl.Rows(C1)("CheckValue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                dtTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "No material data can't be selected!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "No material data can't be selected!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("CheckValue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                dtTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "No material data can't be selected!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "No material data can't be selected!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "CheckValue='True'"
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListMat") = "No material data have been selected before!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub lbShowData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbShowData.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Please find material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If tbData.Text = "" Then
            Session("WarningListMat") = "Please fill total data to be shown!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        Else
            If ToDouble(tbData.Text) <= 0 Then
                Session("WarningListMat") = "Total data to be shown must be more than 0!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat2() Then
            gvListMat.PageSize = Int(tbData.Text)
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub lbAddListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddListMat.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "CheckValue='True'"
            Dim iCheck As Integer = dtView.Count
            If iCheck > 0 Then
                dtView.RowFilter = ""
                dtView.RowFilter = "CheckValue='True' AND soqty>0"
                If dtView.Count <> iCheck Then
                    Session("WarningListMat") = "SO qty for every checked material data must be more than 0!"
                    showMessage(Session("WarningListMat"), 2)
                    dtView.RowFilter = ""
                    Exit Sub
                End If
                dtView.RowFilter = ""
                dtView.RowFilter = "CheckValue='True' AND soprice>0"
                If dtView.Count <> iCheck Then
                    Session("WarningListMat") = "SO price for every checked material data must be more than 0!"
                    showMessage(Session("WarningListMat"), 2)
                    dtView.RowFilter = ""
                    Exit Sub
                End If
                If Session("TblDtl") Is Nothing Then
                    CreateTblDetail()
                End If
                Dim objTable As DataTable
                objTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                Dim objRow As DataRow
                Dim iSeq As Integer = dv.Count + 1
                Dim dQty_unitkecil, dQty_unitbesar As Double
                For C1 As Integer = 0 To dtView.Count - 1
                    dv.RowFilter = " transitemmstoid=" & dtView(C1)("transitemmstoid") & " and transitemdtloid=" & dtView(C1)("transitemdtloid") & " AND itemoid=" & dtView(C1)("itemoid")
                    If dv.Count > 0 Then
                        dv(0)("sodtlqty") = ToDouble(dtView(C1)("soqty").ToString)
                        dv(0)("sodtldisctype1") = sodtldisctype1.SelectedValue
                        dv(0)("sodtldiscvalue1") = ToDouble(sodtldiscvalue1.Text)
                        dv(0)("sodtldiscamt1") = ToDouble(sodtldiscamt1.Text)
                        dv(0)("sodtldisctype2") = sodtldisctype2.SelectedValue
                        dv(0)("sodtldiscvalue2") = ToDouble(sodtldiscvalue2.Text)
                        dv(0)("sodtldiscamt2") = ToDouble(sodtldiscamt2.Text)
                        dv(0)("sodtldisctype3") = sodtldisctype3.SelectedValue
                        dv(0)("sodtldiscvalue3") = ToDouble(sodtldiscvalue3.Text)
                        dv(0)("sodtldiscamt3") = ToDouble(sodtldiscamt3.Text)
                        Dim dDisc, dValue, dNett As Double
                        dv(0)("sodtlprice") = ToDouble(dtView(C1)("soprice"))
                        If DDLinex.SelectedValue = "INC" Then
                            dv(0)("sodtlprice") = ToDouble(dtView(C1)("soprice")) / GetNewTaxValueInclude(sodate.Text)
                        End If
                        dDisc = (dv(0)("sodtldiscamt1") + dv(0)("sodtldiscamt2") + dv(0)("sodtldiscamt3"))
                        dValue = ToDouble(dv(0)("sodtlprice"))
                        dv(0)("sodtldpp") = (dValue - dDisc) * ToDouble(dtView(C1)("soqty"))
                        dv(0)("sodtltaxamt") = 0
                        If DDLinex.SelectedValue = "INC" Then
                            dv(0)("sodtltaxamt") = dv(0)("sodtldpp") * GetNewTaxValue(sodate.Text) / 100
                        End If
                        dNett = Math.Round(ToDouble(dv(0)("sodtldpp")) + ToDouble(dv(0)("sodtltaxamt")), 0, MidpointRounding.AwayFromZero)
                        dv(0)("sodtlamt") = ToDouble(dNett)
                        dv(0)("sodtlnetto") = ToDouble(dNett)
                        GetUnitConverter(dtView(C1)("itemoid"), dtView(C1)("matunitoid"), ToDouble(dtView(C1)("soqty")), dQty_unitkecil, dQty_unitbesar)
                        dv(0)("qtyunitkecil") = dQty_unitkecil
                        dv(0)("qtyunitbesar") = dQty_unitbesar
                        dv(0)("soprice_kons") = ToDouble(dtView(C1)("soprice_kons"))
                        dv(0)("transitemno") = dtView(C1)("transitemno")
                        dv(0)("transitemmstoid") = dtView(C1)("transitemmstoid")
                        dv(0)("transitemdtloid") = dtView(C1)("transitemdtloid")
                        dv(0)("stockvalue") = dtView(C1)("stockvalue")
                        Dim dSelisih As Double = ToDouble(dv(0)("sodtlprice")) - ToDouble(dtView(C1)("stockvalue"))
                        dv(0)("stockvalue_pct") = 0
                        If ToDouble(dtView(C1)("stockvalue")) > 0 Then
                            dv(0)("stockvalue_pct") = dSelisih / ToDouble(dv(0)("stockvalue")) * 100
                        End If
                        dv(0)("price_list") = ToDouble(dtView(C1)("price_list"))
                    Else
                        objRow = objTable.NewRow()
                        objRow("sodtlseq") = iSeq
                        objRow("itemoid") = dtView(C1)("itemoid")
                        objRow("matcode") = dtView(C1)("matcode")
                        objRow("oldcode") = dtView(C1)("old_code")
                        objRow("matlongdesc") = dtView(C1)("matlongdesc")
                        objRow("sodtlqty") = ToDouble(dtView(C1)("soqty"))
                        objRow("sodtlunitoid") = dtView(C1)("matunitoid")
                        objRow("sodtlunit") = dtView(C1)("matunit")
                        objRow("stockqty") = ToDouble(dtView(C1)("stockqty"))
                        objRow("itemconvertunit") = ToDouble(dtView(C1)("itemconvertunit"))
                        objRow("sodtldisctype1") = sodtldisctype1.SelectedValue
                        objRow("sodtldiscvalue1") = ToDouble(sodtldiscvalue1.Text)
                        objRow("sodtldiscamt1") = ToDouble(sodtldiscamt1.Text)
                        objRow("sodtldisctype2") = sodtldisctype2.SelectedValue
                        objRow("sodtldiscvalue2") = ToDouble(sodtldiscvalue2.Text)
                        objRow("sodtldiscamt2") = ToDouble(sodtldiscamt2.Text)
                        objRow("sodtldisctype3") = sodtldisctype3.SelectedValue
                        objRow("sodtldiscvalue3") = ToDouble(sodtldiscvalue3.Text)
                        objRow("sodtldiscamt3") = ToDouble(sodtldiscamt3.Text)
                        objRow("soprice_kons") = ToDouble(dtView(C1)("soprice_kons"))
                        Dim dDisc, dValue, dNett As Double
                        objRow("sodtlprice") = ToDouble(dtView(C1)("soprice"))
                        If DDLinex.SelectedValue = "INC" Then
                            objRow("sodtlprice") = ToDouble(dtView(C1)("soprice")) / GetNewTaxValueInclude(sodate.Text)
                        End If
                        dDisc = (objRow("sodtldiscamt1") + objRow("sodtldiscamt2") + objRow("sodtldiscamt3"))
                        dValue = ToDouble(objRow("sodtlprice"))
                        objRow("sodtldpp") = (dValue - dDisc) * ToDouble(dtView(C1)("soqty"))
                        objRow("sodtltaxamt") = 0
                        If DDLinex.SelectedValue = "INC" Then
                            objRow("sodtltaxamt") = objRow("sodtldpp") * GetNewTaxValue(sodate.Text) / 100
                        End If
                        dNett = Math.Round(ToDouble(objRow("sodtldpp")) + ToDouble(objRow("sodtltaxamt")), 0, MidpointRounding.AwayFromZero)
                        objRow("sodtlamt") = ToDouble(dNett)
                        objRow("sodtlnetto") = ToDouble(dNett)
                        objRow("sodtlnote") = dtView(C1)("sodtlnote")
                        objRow("old_code") = dtView(C1)("old_code")
                        objRow("inex") = DDLinex.SelectedValue
                        GetUnitConverter(dtView(C1)("itemoid"), dtView(C1)("matunitoid"), ToDouble(dtView(C1)("soqty")), dQty_unitkecil, dQty_unitbesar)
                        objRow("qtyunitkecil") = dQty_unitkecil
                        objRow("qtyunitbesar") = dQty_unitbesar
                        objRow("transitemno") = dtView(C1)("transitemno")
                        objRow("transitemmstoid") = dtView(C1)("transitemmstoid")
                        objRow("transitemdtloid") = dtView(C1)("transitemdtloid")
                        objRow("stockvalue") = dtView(C1)("stockvalue")
                        Dim dSelisih As Double = ToDouble(objRow("sodtlprice")) - ToDouble(dtView(C1)("stockvalue"))
                        objRow("stockvalue_pct") = 0
                        If ToDouble(dtView(C1)("stockvalue")) > 0 Then
                            objRow("stockvalue_pct") = dSelisih / ToDouble(objRow("stockvalue")) * 100
                        End If
                        objRow("price_list") = ToDouble(dtView(C1)("price_list"))
                        objTable.Rows.Add(objRow)
                        iSeq += 1
                    End If
                    dv.RowFilter = ""
                Next
                dtView.RowFilter = ""
                Session("TblDtl") = objTable
                gvTblDtl.DataSource = objTable
                gvTblDtl.DataBind()
                CountHdrAmount()
                ClearDetail()
                If cbOpenListMatUsage.Checked Then
                    btnSearchMat_Click(Nothing, Nothing)
                Else
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                End If
            Else
                Session("WarningListMat") = "Please select material to add to list!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        End If
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub soqty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles soqty.TextChanged
        If matlongdesc.Text = "" Then
            showMessage("Isi Item Dulu ..!!", 1)
            Exit Sub
        End If
        soqty.Text = ToMaskEdit(ToDouble(soqty.Text), 2)
        qtyunitkecil.Text = ToDouble(soqty.Text) / ToDouble(itemconvertunit.Text)
        qtyunitbesar.Text = ToDouble(soqty.Text)
        CountAllDtlAmount()
        'calcKonversi()
    End Sub

    Protected Sub DDLinex_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CountHdrAmount()
        'CheckINEX()
    End Sub

    Protected Sub varprice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If DDLinex.Text = "INC" Then
            soprice.Text = ToDouble(varprice.Text / GetNewTaxValueInclude(sodate.Text))
        End If
        CountAllDtlAmount()
    End Sub

    Protected Sub soprice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles soprice.TextChanged
        If matlongdesc.Text = "" Then
            showMessage("Isi Item Dulu ..!!", 1)
            Exit Sub
        End If
        soprice.Text = ToDouble(soprice.Text)
        CountAllDtlAmount()
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            Dim objTable As DataTable
            objTable = Session("TblDtl")
            Dim dv As DataView = objTable.DefaultView
            Dim objRow As DataRow = objTable.Rows(sodtlseq.Text - 1)
            Dim dQty_unitkecil, dQty_unitbesar As Double
            objRow.BeginEdit()
            objRow("itemoid") = matoid.Text
            objRow("matcode") = matcode.Text
            objRow("oldcode") = old_code.Text
            objRow("matlongdesc") = matlongdesc.Text
            objRow("sodtlqty") = ToDouble(soqty.Text)
            If DDLinex.SelectedValue = "INC" Then
                objRow("sodtlprice") = ToDouble(soprice.Text) / GetNewTaxValueInclude(sodate.Text)
            Else
                objRow("sodtlprice") = ToDouble(soprice.Text)
            End If
            objRow("sodtlamt") = ToDouble(sodtlamt.Text)
            objRow("sodtlunitoid") = sounitoid.SelectedValue
            objRow("sodtlunit") = sounitoid.SelectedItem.Text
            objRow("stockqty") = ToDouble(stockQty.Text)
            objRow("itemconvertunit") = ToDouble(itemconvertunit.Text)
            objRow("sodtldisctype1") = sodtldisctype1.SelectedValue
            objRow("sodtldiscvalue1") = ToDouble(sodtldiscvalue1.Text)
            objRow("sodtldiscamt1") = ToDouble(sodtldiscamt1.Text)
            objRow("sodtldisctype2") = sodtldisctype2.SelectedValue
            objRow("sodtldiscvalue2") = ToDouble(sodtldiscvalue2.Text)
            objRow("sodtldiscamt2") = ToDouble(sodtldiscamt2.Text)
            objRow("sodtldisctype3") = sodtldisctype3.SelectedValue
            objRow("sodtldiscvalue3") = ToDouble(sodtldiscvalue3.Text)
            objRow("sodtldiscamt3") = ToDouble(sodtldiscamt3.Text)
            objRow("transitemdtloid") = transitemdtloid.Text
            Dim dDisc, dDisc1, dDisc2, dDisc3, dValue As Double
            dValue = ToDouble(objRow("sodtlprice"))
            dDisc1 = ToDouble(sodtldiscamt1.Text) / ToDouble(soqty.Text)
            dDisc2 = ToDouble(sodtldiscamt2.Text) / ToDouble(soqty.Text)
            dDisc3 = ToDouble(sodtldiscamt3.Text) / ToDouble(soqty.Text)
            dDisc = dDisc1 + dDisc2 + dDisc3
            objRow("sodtldpp") = ToDouble(sodtlamt.Text) '(dValue - dDisc) * ToDouble(soqty.Text)
            objRow("sodtltaxamt") = 0
            If DDLinex.SelectedValue = "INC" Then
                objRow("sodtldpp") = ToDouble(sodtlamt.Text) / GetNewTaxValueInclude(sodate.Text)
                objRow("sodtltaxamt") = objRow("sodtldpp") * GetNewTaxValue(sodate.Text) / 100
            End If
            objRow("sodtlnetto") = Math.Round(ToDouble(objRow("sodtldpp")) + ToDouble(objRow("sodtltaxamt")), 0, MidpointRounding.AwayFromZero)
            objRow("sodtlnote") = sodtlnote.Text
            objRow("old_code") = old_code.Text
            objRow("inex") = DDLinex.SelectedValue
            GetUnitConverter(matoid.Text, sounitoid.SelectedValue, ToDouble(soqty.Text), dQty_unitkecil, dQty_unitbesar)
            objRow("qtyunitkecil") = dQty_unitkecil
            objRow("qtyunitbesar") = dQty_unitbesar
            Dim dSelisih As Double = ToDouble(objRow("sodtlprice")) - ToDouble(stockvalue.Text)
            objRow("stockvalue_pct") = 0
            If ToDouble(stockvalue.Text) > 0 Then
                objRow("stockvalue_pct") = dSelisih / ToDouble(stockvalue.Text) * 100
            End If
            objRow("price_list") = ToDouble(price_list.Text)

            objRow.EndEdit()
            objTable.AcceptChanges()
            Session("TblDtl") = objTable
            gvTblDtl.DataSource = objTable
            gvTblDtl.DataBind()
            CountHdrAmount()
            ClearDetail()
            btnSearchMat.Visible = True
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        CountHdrAmount()
        ClearDetail()
        btnSearchMat.Visible = True
    End Sub

    Protected Sub gvTblDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTblDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 2)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 2)
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 2)
            e.Row.Cells(10).Text = ToMaskEdit(ToDouble(e.Row.Cells(10).Text), 2)
            e.Row.Cells(11).Text = ToMaskEdit(ToDouble(e.Row.Cells(11).Text), 2)
            e.Row.Cells(12).Text = ToMaskEdit(ToDouble(e.Row.Cells(12).Text), 2)
            e.Row.Cells(13).Text = ToMaskEdit(ToDouble(e.Row.Cells(13).Text), 2)
            e.Row.Cells(14).Text = ToMaskEdit(ToDouble(e.Row.Cells(14).Text), 2)
            e.Row.Cells(15).Text = ToMaskEdit(ToDouble(e.Row.Cells(15).Text), 2)
        End If
    End Sub

    Protected Sub gvTblDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvTblDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        'Dim objRows As DataRow = objRow(e.RowIndex)
        'objRows.BeginEdit()
        'sototalamt.Text = ToMaskEdit(ToDouble(sototalamt.Text) - (ToDouble(objRows("sodtlamt"))), 4)
        'objRows.EndEdit()
        'objTable.AcceptChanges()
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("sodtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvTblDtl.DataSource = objTable
        gvTblDtl.DataBind()
        CountHdrAmount()
        ClearDetail()
        btnSearchMat.Visible = True
    End Sub

    Protected Sub gvTblDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvTblDtl.SelectedIndexChanged
        Try
            sodtlseq.Text = gvTblDtl.SelectedDataKey.Item("sodtlseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable
                objTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "sodtlseq=" & sodtlseq.Text
                matoid.Text = dv.Item(0).Item("itemoid")
                matcode.Text = dv.Item(0).Item("matcode").ToString.Trim
                oldcode.Text = dv.Item(0).Item("oldcode").ToString.Trim
                matlongdesc.Text = dv.Item(0).Item("matlongdesc").ToString.Trim
                InitDDLUnit(matoid.Text)
                sSql = "select genoid,gendesc from QL_mstgen WHERE gengroup='UNIT'"
                FillDDL(sounitoid, sSql)
                soqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("sodtlqty")), 2)
                sounitoid.SelectedValue = dv.Item(0).Item("sodtlunitoid")
                stockQty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("stockqty")), 2)
                itemconvertunit.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("itemconvertunit")), 2)
                If DDLinex.SelectedValue = "INC" Then
                    soprice.Text = ToDouble(dv.Item(0).Item("sodtlprice")) * GetNewTaxValueInclude(sodate.Text)
                Else
                    soprice.Text = ToDouble(dv.Item(0).Item("sodtlprice"))
                End If
                sodtlamt.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("sodtlamt").ToString), 2)
                sodtldisctype1.SelectedValue = dv.Item(0).Item("sodtldisctype1").ToString
                sodtldiscvalue1.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("sodtldiscvalue1")), 2)
                sodtldiscamt1.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("sodtldiscamt1")), 2)
                sodtldisctype2.SelectedValue = dv.Item(0).Item("sodtldisctype2").ToString
                sodtldiscvalue2.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("sodtldiscvalue2")), 2)
                sodtldiscamt2.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("sodtldiscamt2")), 2)
                sodtldisctype3.SelectedValue = dv.Item(0).Item("sodtldisctype3").ToString
                sodtldiscvalue3.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("sodtldiscvalue3")), 2)
                sodtldiscamt3.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("sodtldiscamt3")), 2)
                sodtlnetto.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("sodtlnetto")), 2)
                sodtlnote.Text = dv.Item(0).Item("sodtlnote")
                old_code.Text = dv.Item(0).Item("old_code")
                transitemdtloid.Text = dv.Item(0).Item("transitemdtloid")
                stockvalue.Text = ToDouble(dv.Item(0).Item("stockvalue"))
                price_list.Text = ToDouble(dv.Item(0).Item("price_list"))
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
            Exit Sub
        Finally
            btnSearchMat.Visible = False
            btnSearchMatEdit.Visible = True
            btnClearMat.Visible = True
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        curroid_SelectedIndexChanged(Nothing, Nothing)
        If IsInputValid() Then
            sodtloid.Text = GenerateID("QL_TRNSODTL", CompnyCode)
            'socontoid.Text = GenerateID("QL_TRNSOITEMCONT", CompnyCode)
            Dim iAppOid As Integer = GenerateID("QL_APPROVAL", CompnyCode)
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                If CheckDataExists("SELECT COUNT(*) FROM QL_trnsomst WHERE somstoid=" & somstoid.Text) Then
                    somstoid.Text = GenerateID("QL_TRNSOMST", CompnyCode)
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNSOMST", "somstoid", somstoid.Text, "somststatus", updtime.Text, "Approved")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    somststatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            periodacctg.Text = GetDateToPeriodAcctg(CDate(sodate.Text))
            If somststatus.Text = "Revised" Then
                somststatus.Text = "In Process"
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trnsomst (cmpcode, somstoid, periodacctg, sotype, sotype2, sodelivtype, somstpromo, somstrefno, sodate, sono, custoid, sopaytypeoid, curroid, rateoid, sototalamt, somstdisctype, somstdiscvalue, somstdiscamt, somstdisctype2, somstdiscvalue2, somstdiscamt2, somstdpp, sototalnetto, sotaxtype, sotaxamt, somstnote, somststatus, somstres1, somstres2, somstres3, createuser, createtime, upduser, updtime, soratetoidr, sototalamtidr, somstdiscamtidr, sototalnettoidr, soratetousd, sototalamtusd, somstdiscamtusd, sototalnettousd, soetd, sosalescode, iskonsinyasi, transitemmstoid, employee_id, warehouse_id) VALUES ('" & DDLBusUnit.SelectedValue & "', " & somstoid.Text & ", '" & periodacctg.Text & "', 'SO', " & sotype2.SelectedValue & ", " & delivType.SelectedValue & ", " & promo.SelectedValue & ", '" & Tchar(noRef.Text) & "', '" & sodate.Text & "', '" & sono.Text & "'," & custoid.Text & ",  " & sopaytypeoid.SelectedValue & ", " & curroid.SelectedValue & ", " & rateoid.Text & ", " & ToDouble(sototalamt.Text) & ", '" & somstdisctype.SelectedValue & "', " & ToDouble(somstdiscvalue.Text) & ", " & ToDouble(somstdiscamt.Text) & ", '" & somstdisctype2.SelectedValue & "', " & ToDouble(somstdiscvalue2.Text) & ", " & ToDouble(somstdiscamt2.Text) & ", " & ToDouble(sodppamt.Text) & ", " & ToDouble(sototalnetto.Text) & ", '" & DDLinex.SelectedValue & "', " & ToDouble(sovat.Text) & ", '" & Tchar(somstnote.Text.Trim) & "', '" & somststatus.Text & "', '" & sotype.SelectedValue & "', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & rateidrvalue.Text & ", " & ToDouble(sototalamt.Text) & ", " & ToDouble(somstdiscamt.Text) & ", " & ToDouble(sototalnetto.Text) & ", " & soratetousd.Text & ", " & ToDouble(sototalamt.Text) * soratetousd.Text & ", " & ToDouble(somstdiscamt.Text) * soratetousd.Text & ", " & ToDouble(sototalnetto.Text) * soratetousd.Text & ", '" & delivDate.Text & "', '" & soitemsalescode.SelectedValue & "', '" & cbCons.Checked & "', 0, " & ddlPerson.SelectedValue & ", " & ToInteger(warehouse_id.Text) & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & somstoid.Text & " WHERE tablename='QL_TRNSOMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trnsomst SET periodacctg='" & periodacctg.Text & "', sotype='SO', sotype2=" & sotype2.SelectedValue & ", sodelivtype=" & delivType.SelectedValue & ", somstpromo=" & promo.SelectedValue & ", somstrefno='" & Tchar(noRef.Text) & "', sodate='" & sodate.Text & "', sono='" & sono.Text & "', custoid=" & custoid.Text & ", sopaytypeoid=" & sopaytypeoid.SelectedValue & ", curroid=" & curroid.SelectedValue & ", rateoid=" & rateoid.Text & ", sototalamt=" & ToDouble(sototalamt.Text) & ", somstdisctype='" & somstdisctype.SelectedValue & "', somstdiscvalue=" & ToDouble(somstdiscvalue.Text) & ", somstdiscamt=" & ToDouble(somstdiscamt.Text) & ", somstdisctype2='" & somstdisctype2.SelectedValue & "', somstdiscvalue2=" & ToDouble(somstdiscvalue2.Text) & ", somstdiscamt2=" & ToDouble(somstdiscamt2.Text) & ", somstdpp=" & ToDouble(sodppamt.Text) & ", sototalnetto=" & ToDouble(sototalnetto.Text) & ", sotaxtype='" & DDLinex.SelectedValue & "', sotaxamt=" & ToDouble(sovat.Text) & ", somstnote='" & Tchar(somstnote.Text.Trim) & "', somststatus='" & somststatus.Text & "', somstres1='" & sotype.SelectedValue & "', somstres2='', somstres3='', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, soratetoidr=" & rateidrvalue.Text & ", sototalamtidr=" & ToDouble(sototalamt.Text) & ", somstdiscamtidr=" & ToDouble(somstdiscamt.Text) & ", sototalnettoidr=" & ToDouble(sototalnetto.Text) & ", soratetousd=" & soratetousd.Text & ", sototalamtusd=" & ToDouble(sototalamt.Text) * soratetousd.Text & ", somstdiscamtusd=" & ToDouble(somstdiscamt.Text) * soratetousd.Text & ", sototalnettousd=" & ToDouble(sototalnetto.Text) * soratetousd.Text & ", sosalescode='" & soitemsalescode.SelectedValue & "', iskonsinyasi='" & cbCons.Checked & "', employee_id=" & ddlPerson.SelectedValue & ", warehouse_id=" & ToInteger(warehouse_id.Text) & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND somstoid=" & somstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    sSql = "DELETE FROM QL_trnsodtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND somstoid=" & somstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    If cbCons.Checked Then
                        sSql = "update QL_mstcust set limit_usage=limit_usage - " & ToDouble(sototalnetto.Text) & " where custoid=" & custoid.Text
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                End If

                If cbCons.Checked Then
                    sSql = "update QL_mstcust set limit_usage=limit_usage + " & ToDouble(sototalnetto.Text) & " where custoid=" & custoid.Text
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = SortingDataTable(Session("TblDtl"), "sodtlseq")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trnsodtl (cmpcode, sodtloid, somstoid, sodtlseq, itemoid, sodtlqty, sodtlunitoid, sodtlprice, sodtlamt, sodtldisctype1, sodtldiscvalue1, sodtldiscamt1, sodtldisctype2, sodtldiscvalue2, sodtldiscamt2, sodtldisctype3, sodtldiscvalue3, sodtldiscamt3, sodtlnetto, sodtlnote, sodtlstatus, sodtlres1, sodtlres2, sodtlres3, upduser, updtime, sopriceidr, sodtlamtidr, sodtlnettoidr, sopriceusd, sodtlamtusd, sodtlnettousd, soqty_unitkecil, soqty_unitbesar, sodtldpp, sodtltaxamt, soprice_kons, transitemmstoid, transitemdtloid, old_code) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C1 + CInt(sodtloid.Text)) & ", " & somstoid.Text & ",  " & C1 + 1 & ", " & objTable.Rows(C1).Item("itemoid") & ", " & ToDouble(objTable.Rows(C1).Item("sodtlqty").ToString) & ", " & objTable.Rows(C1).Item("sodtlunitoid") & ", " & ToDouble(objTable.Rows(C1).Item("sodtlprice").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("sodtlamt").ToString) & ", '" & objTable.Rows(C1).Item("sodtldisctype1").ToString & "', " & ToDouble(objTable.Rows(C1).Item("sodtldiscvalue1").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("sodtldiscamt1").ToString) & ", '" & objTable.Rows(C1).Item("sodtldisctype2").ToString & "', " & ToDouble(objTable.Rows(C1).Item("sodtldiscvalue2").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("sodtldiscamt2").ToString) & ", '" & objTable.Rows(C1).Item("sodtldisctype3").ToString & "', " & ToDouble(objTable.Rows(C1).Item("sodtldiscvalue3").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("sodtldiscamt3").ToString) & "," & ToDouble(objTable.Rows(C1).Item("sodtlnetto").ToString) & ", '" & Tchar(objTable.Rows(C1).Item("sodtlnote")) & "', '', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(C1).Item("sodtlprice").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("sodtlamt").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("sodtlnetto").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("sodtlprice").ToString) * soratetousd.Text & ", " & ToDouble(objTable.Rows(C1).Item("sodtlamt").ToString) * soratetousd.Text & ", " & ToDouble(objTable.Rows(C1).Item("sodtlnetto").ToString) * soratetousd.Text & ", " & ToDouble(objTable.Rows(C1).Item("qtyunitkecil").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("qtyunitbesar").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("sodtldpp").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("sodtltaxamt").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("soprice_kons").ToString) & ", " & objTable.Rows(C1).Item("transitemmstoid").ToString & ", " & objTable.Rows(C1).Item("transitemdtloid").ToString & ", '" & Tchar(objTable.Rows(C1).Item("old_code")) & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(sodtloid.Text)) & " WHERE tablename='QL_TRNSODTL' AND cmpcode='" & CompnyCode & "' "
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                'If Not Session("TblCont") Is Nothing Then
                '    Dim objTableCont As DataTable
                '    objTableCont = Session("TblCont")
                '    For C1 As Int16 = 0 To objTableCont.Rows.Count - 1
                '        sSql = "INSERT INTO QL_trnsoitemcont (cmpcode, soitemcontoid, soitemmstoid, containeroid, soitemcontqty, soitemcontpct, soitemcontstatus, upduser, updtime) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C1 + CInt(socontoid.Text)) & ", " & somstoid.Text & ", " & objTableCont.Rows(C1).Item("containeroid") & ", " & ToDouble(objTableCont.Rows(C1).Item("soitemcontqty").ToString) & ", " & ToDouble(objTableCont.Rows(C1).Item("soitemcontpct").ToString) & ",'', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                '        xCmd.CommandText = sSql
                '        xCmd.ExecuteNonQuery()
                '    Next
                '    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTableCont.Rows.Count - 1 + CInt(socontoid.Text)) & " WHERE tablename='QL_TRNSOITEMCONT' AND cmpcode='" & CompnyCode & "' "
                '    xCmd.CommandText = sSql
                '    xCmd.ExecuteNonQuery()
                'End If
                If somststatus.Text = "In Approval" Then
                    Dim dt As DataTable = Session("AppPerson")
                    Dim tempAppCode As String = ""
                    For C1 As Integer = 0 To dt.Rows.Count - 1
                        If C1 = 0 Then
                            tempAppCode = dt.Rows(C1)("apppersonlevel").ToString
                        End If
                        sSql = "INSERT INTO QL_approval (cmpcode, approvaloid, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvalnote, posisiLevel) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iAppOid + C1 & ", 'SO-" & somstoid.Text & "_" & (iAppOid + C1) & "', '" & Session("UserID") & "', '" & sodate.Text & "', 'New', 'QL_trnsomst', " & somstoid.Text & ", 'In Approval', '" & dt.Rows(C1)("apppersonlevel").ToString & "', '" & dt.Rows(C1)("approvaluser").ToString & "', '1/1/1900', '','" & IIf(tempAppCode = dt.Rows(C1)("apppersonlevel").ToString, "1", "") & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iAppOid + dt.Rows.Count - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_APPROVAL'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    somststatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.ToString, 1)
                conn.Close()
                somststatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & somstoid.Text & ".<BR>"
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trnSOItem.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnSOItem.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If somstoid.Text.Trim = "" Then
            showMessage("Please select Sales Order data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNSOMST", "somstoid", somstoid.Text, "somststatus", updtime.Text, "Approved")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                somststatus.Text = "In Process"
                Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            'sSql = "DELETE FROM QL_trnsoitemcont WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND soitemmstoid=" & somstoid.Text
            'xCmd.CommandText = sSql
            'xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnsodtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND somstoid=" & somstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnsomst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND somstoid=" & somstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnSOItem.aspx?awal=true")
    End Sub

    Protected Sub btnSendApproval_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSendApproval.Click
        Try
            Dim is_admin As Boolean = False
            If Not Session("TblDtl") Is Nothing Then
                Dim objTable As DataTable = Session("TblDtl")
                For C1 As Int16 = 0 To objTable.Rows.Count - 1
                    is_admin = objTable.Rows(C1).Item("old_code") <> ""
                    If Not is_admin Then
                        If objTable.Rows(C1).Item("transitemno") = "" Then
                            Dim harga_inc As Double = ToDouble(objTable.Rows(C1).Item("sodtlamt")) / ToDouble(objTable.Rows(C1).Item("sodtlqty"))
                            is_admin = harga_inc <> ToDouble(objTable.Rows(C1).Item("price_list"))
                        End If
                    End If 'transitemno
                Next
            End If
            sSql = "SELECT approvaluser, apppersontype, (case when upper(apppersontype)='FINAL' then 0 else apppersonlevel end) apppersonlevel FROM QL_approvalperson WHERE tablename='QL_trnsomst' AND activeflag='ACTIVE' AND cmpcode IN ('" & CompnyCode & "', '" & DDLBusUnit.SelectedValue & "') "
            If is_admin Then
                sSql &= " and approvaluser in ('admin', 'ISTI')"
            End If
            sSql &= " order by apppersontype desc,apppersonlevel asc"
            Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_approvalperson")
            If dt.Rows.Count = 0 Then
                showMessage("There is no user can't approve this transaction. Please define approval user in <STRONG>APPROVAL USER</STRONG> form.", 2)
            Else
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "apppersontype='Final'"
                If dv.Count = 0 Then
                    showMessage("Final approval user is not defined for this transaction. Please define final approval user in <STRONG>APPROVAL USER</STRONG> form.", 2)
                    dv.RowFilter = ""
                Else
                    dv.RowFilter = ""
                    Session("AppPerson") = dt
                    somststatus.Text = "In Approval"
                    btnSave_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        If Session("TblMst") IsNot Nothing Then
            UpdateCheckedValue()
            lblRptOid.Text = GetOidDetail()
            PrintReport(lblRptOid.Text)
        End If
    End Sub

    Protected Sub btnCancelPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelPrint.Click
        cProc.SetModalPopUpExtender(btnPnlApproval, pnlApproval, mpeApproval, False)
    End Sub

    Protected Sub btnContinue_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnContinue.Click
        cProc.SetModalPopUpExtender(btnPnlApproval, pnlApproval, mpeApproval, False)
        PrintReport(lblRptOid.Text)
    End Sub

    Protected Sub somstdisctype2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles somstdisctype2.SelectedIndexChanged
        If somstdiscvalue2.Text = "" Then
            somstdiscvalue2.Text = 0
        End If
        CountHdrAmount()
    End Sub

    Protected Sub somstdiscvalue2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles somstdiscvalue2.TextChanged
        If somstdiscvalue2.Text = "" Then
            somstdiscvalue2.Text = 0
        End If
        CountHdrAmount()
    End Sub

    Protected Sub somstdisctype3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles somstdisctype3.SelectedIndexChanged
        If somstdiscvalue3.Text = "" Then
            somstdiscvalue3.Text = 0
        End If
        CountHdrAmount()
    End Sub

    Protected Sub somstdiscvalue3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles somstdiscvalue3.TextChanged
        If somstdiscvalue3.Text = "" Then
            somstdiscvalue3.Text = 0
        End If
        CountHdrAmount()
    End Sub

    Protected Sub gvListMat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        matoid.Text = gvListMat.SelectedDataKey.Item("itemoid").ToString
        matcode.Text = gvListMat.SelectedDataKey.Item("matcode").ToString
        matlongdesc.Text = gvListMat.SelectedDataKey.Item("matlongdesc").ToString
        stockQty.Text = ToDouble(gvListMat.SelectedDataKey.Item("stockqty").ToString)
        sounit1oid.Text = gvListMat.SelectedDataKey.Item("matunitoid").ToString
        sounit2oid.Text = gvListMat.SelectedDataKey.Item("matunit2oid").ToString
        itemconvertunit.Text = gvListMat.SelectedDataKey.Item("itemconvertunit").ToString
        InitDDLUnit(matoid.Text)
        qtyunitkecil.Text = ToDouble(soqty.Text) / ToDouble(itemconvertunit.Text)
        qtyunitbesar.Text = ToDouble(soqty.Text)
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub DDLUnit_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Session("TblMat") Is Nothing Then
            'If UpdateCheckedMat() Then
            UpdateCheckedMat_price()

            '        Dim dtTbl_olah As DataTable = Session("TblMat")
            '        Dim dtView_olah As DataView = dtTbl_olah.DefaultView
            '        dtView_olah.AllowEdit = True
            '        dtView_olah.RowFilter = "matoid=" & Session("tempMatoid") & ""
            '        For C1 As Integer = 0 To dtView_olah.Count - 1
            '            If dtView_olah.Count > 0 Then
            '                MsgBox(dtView_olah(0)("matunitoid"))
            '                If dtView_olah(0)("matunitoid") = dtView_olah(0)("matunit2oid") Then
            '                    dtView_olah(0)("soprice") = dtView_olah(0)("soprice")
            '                Else
            '                    dtView_olah(0)("soprice") = dtView_olah(0)("soprice") * GetStrData("select isnull(itemconvertunit,1) FROM ql_mstitem m WHERE m.itemoid=" & Session("tempMatoid") & " ")
            '                End If
            '                dtView_olah.RowFilter = ""
            '            End If
            '        Next
            '        dtTbl_olah.AcceptChanges()
            '        Session("TblMat") = dtTbl_olah

            'Dim dtTbl As DataTable = Session("TblMat")
            'Dim dtView As DataView = dtTbl.DefaultView
            'Dim sFilter As String = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%' AND stockqty " & FilterDDLStock.SelectedValue & " " & ToDouble(FilterTextStock.Text)
            'If cbCat01.Checked Then
            '    If FilterDDLCat01.SelectedValue <> "" Then
            '        sFilter &= " AND SUBSTRING(matcode, 1, 2)='" & Left(FilterDDLCat01.SelectedItem.Text, 2) & "'"
            '    Else
            '        cbCat01.Checked = False
            '    End If
            'End If
            'If cbCat02.Checked Then
            '    If FilterDDLCat02.SelectedValue <> "" Then
            '        sFilter &= IIf(cbCat01.Checked, "", " AND SUBSTRING(matcode, 1, 2)='" & Left(FilterDDLCat01.SelectedItem.Text, 2) & "'") & " AND SUBSTRING(matcode, 4, 3)='" & Left(FilterDDLCat02.SelectedItem.Text, 3) & "'"
            '    Else
            '        cbCat02.Checked = False
            '    End If
            'End If
            'If cbCat03.Checked Then
            '    If FilterDDLCat03.SelectedValue <> "" Then
            '        sFilter &= IIf(cbCat01.Checked, "", " AND SUBSTRING(matcode, 1, 2)='" & Left(FilterDDLCat01.SelectedItem.Text, 2) & "'") & IIf(cbCat02.Checked, "", " AND SUBSTRING(matcode, 4, 3)='" & Left(FilterDDLCat02.SelectedItem.Text, 3) & "'") & " AND SUBSTRING(matcode, 8, 4)='" & Tchar(Left(FilterDDLCat03.SelectedItem.Text, 4)) & "'"
            '    Else
            '        cbCat03.Checked = False
            '    End If
            'End If
            'If cbCat04.Checked Then
            '    If FilterDDLCat04.SelectedValue <> "" Then
            '        sFilter &= IIf(cbCat01.Checked, "", " AND SUBSTRING(matcode, 1, 2)='" & Left(FilterDDLCat01.SelectedItem.Text, 2) & "'") & IIf(cbCat02.Checked, "", " AND SUBSTRING(matcode, 4, 3)='" & Left(FilterDDLCat02.SelectedItem.Text, 3) & "'") & IIf(cbCat03.Checked, "", " AND SUBSTRING(matcode, 8, 4)='" & Tchar(Left(FilterDDLCat03.SelectedItem.Text, 4)) & "'") & " AND SUBSTRING(matcode, 13, 4)='" & Left(FilterDDLCat04.SelectedItem.Text, 4) & "'"
            '    Else
            '        cbCat04.Checked = False
            '    End If
            'End If
            'dtView.RowFilter = sFilter
            'If dtView.Count > 0 Then
            '    Session("TblMatView") = dtView.ToTable
            '    gvListMat.DataSource = Session("TblMatView")
            '    gvListMat.DataBind()
            '    dtView.RowFilter = ""
            '    mpeListMat.Show()
            'Else
            '    dtView.RowFilter = ""
            '    Session("TblMatView") = Nothing
            '    gvListMat.DataSource = Session("TblMatView")
            '    gvListMat.DataBind()
            '    Session("WarningListMat") = "Material data can't be found!"
            '    showMessage(Session("WarningListMat"), 2)
            'End If
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()

    End Sub

    Protected Sub printso2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Session("TblMst") IsNot Nothing Then
            UpdateCheckedValue()
            lblRptOid.Text = GetOidDetail()
            Dim dt As DataTable = Session("TblMst")
            Dim sSOFilter As String = ""
            If lblRptOid.Text <> "" Then
                sSOFilter = " AND somstoid IN (" & lblRptOid.Text & ")"
            End If
            If cbprice.Checked = False Then
                If dt.Select("custtype='LOCAL'" & sSOFilter).Length > 0 Then
                    If dt.Select("custtype='EXPORT'" & sSOFilter).Length > 0 Then
                        cProc.SetModalPopUpExtender(btnPnlApproval, pnlApproval, mpeApproval, True)
                    Else
                        DDLTypeCust.SelectedValue = "LOCAL"
                        PrintReport2(lblRptOid.Text)
                    End If
                Else
                    DDLTypeCust.SelectedValue = "EXPORT"
                    PrintReport2(lblRptOid.Text)
                End If
            Else
                PrintReport2(lblRptOid.Text)
            End If
        Else
            cProc.SetModalPopUpExtender(btnPnlApproval, pnlApproval, mpeApproval, True)
        End If
    End Sub

    Protected Sub sounitoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles sounitoid.SelectedIndexChanged
        'If sounitoid.SelectedValue = GetStrData("select itemunit2oid from ql_mstitem where itemoid=" & matoid.Text & "") Then
        '    varprice.Text = GetStrData("select itemminprice from ql_mstitem where itemoid=" & matoid.Text & "")
        'Else
        '    varprice.Text = GetStrData("select itemminprice from ql_mstitem where itemoid=" & matoid.Text & "") * ToDouble(itemconvertunit.Text)
        'End If
        'varprice_TextChanged(Nothing, Nothing)
    End Sub

    Protected Sub panjang_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles panjang.TextChanged
        calcKonversi()
    End Sub

    Protected Sub soitemsalescode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'GenerateCustPO(soitemsalescode.SelectedItem.Text)
    End Sub

    Protected Sub gvListCust_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 2)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 2)
        End If
    End Sub

    Protected Sub sodtldisctype1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CountAllDtlAmount()
    End Sub

    Protected Sub sodtldiscvalue1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sodtldiscvalue1.Text = ToMaskEdit(ToDouble(sodtldiscvalue1.Text), 4)
        CountAllDtlAmount()
    End Sub

    Protected Sub btnClearMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        matoid.Text = ""
        matlongdesc.Text = ""
        stockQty.Text = ""
        soqty.Text = ""
        soprice.Text = ""
        qtyunitkecil.Text = ""
        qtyunitbesar.Text = ""
    End Sub

    Protected Sub sodtldiscvalue2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sodtldiscvalue2.Text = ToMaskEdit(ToDouble(sodtldiscvalue2.Text), 4)
        CountAllDtlAmount()
    End Sub

    Protected Sub sodtldiscvalue3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sodtldiscvalue3.Text = ToMaskEdit(ToDouble(sodtldiscvalue3.Text), 4)
        CountAllDtlAmount()
    End Sub

    Protected Sub sodtldisctype2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CountAllDtlAmount()
    End Sub

    Protected Sub sodtldisctype3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CountAllDtlAmount()
    End Sub

    Protected Sub sototalamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CountHdrAmount()
    End Sub

    Protected Sub btnSearchMatEdit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListMatEdit.SelectedIndex = -1 : FilterTextListMatEdit.Text = "" : gvListMatEdit.SelectedIndex = -1
        BindMaterial("edit")
        cProc.SetModalPopUpExtender(btnHideListMatEdit, pnlListMatEdit, mpeListMatEdit, True)
    End Sub

    Protected Sub btnFindListMatEdit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindMaterial("edit")
        mpeListMatEdit.Show()
    End Sub

    Protected Sub btnAllListMatEdit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindMaterial("edit")
        cProc.SetModalPopUpExtender(btnHideListMatEdit, pnlListMatEdit, mpeListMatEdit, True)
        mpeListMatEdit.Show()
    End Sub

    Protected Sub lbCloseListMatEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListMatEdit, pnlListMatEdit, mpeListMatEdit, False)
    End Sub

    Protected Sub gvListMatEdit_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 2)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 2)
        End If
    End Sub

    Protected Sub gvListMatEdit_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvListMatEdit.PageIndex = e.NewPageIndex
        BindMaterial("edit")
        mpeListMatEdit.Show()
    End Sub

    Protected Sub gvListMatEdit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        matoid.Text = gvListMatEdit.SelectedDataKey.Item("itemoid").ToString
        matcode.Text = gvListMatEdit.SelectedDataKey.Item("matcode").ToString
        oldcode.Text = gvListMatEdit.SelectedDataKey.Item("oldcode").ToString
        matlongdesc.Text = gvListMatEdit.SelectedDataKey.Item("matlongdesc").ToString
        stockQty.Text = ToMaskEdit(ToDouble(gvListMatEdit.SelectedDataKey.Item("stockqty").ToString), 4)
        sSql = "select genoid,gendesc from QL_mstgen WHERE gengroup='UNIT'"
        FillDDL(sounitoid, sSql)
        sounitoid.SelectedValue = gvListMatEdit.SelectedDataKey.Item("matunitoid").ToString
        soprice.Text = ToMaskEdit(ToDouble(gvListMatEdit.SelectedDataKey.Item("soprice").ToString), 4)
        CountAllDtlAmount()
        cProc.SetModalPopUpExtender(btnHideListMatEdit, pnlListMatEdit, mpeListMatEdit, False)
    End Sub

#End Region

    Protected Sub cbCons_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If custoid.Text <> "" Then
            'InitDDLTransfer()
        End If
    End Sub
End Class