Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class Transaction_CancelSO
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Function"
    Private Function GetOidDetail() As String
        Dim sReturn As String = ""
        For C1 As Integer = 0 To gvPODtl.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvPODtl.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        If cbCheck Then
                            sReturn &= CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip & ","
                        End If
                    End If
                Next
            End If
        Next
        If sReturn <> "" Then
            sReturn = Left(sReturn, sReturn.Length - 1)
        End If
        Return sReturn
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitDDL(ByVal gencode As String)
        ' Init DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(pobusinessunit, sSql)
    End Sub

    Private Sub BindListPO()
        Dim sType As String = ddlType.SelectedValue
        sSql = "SELECT * FROM ( SELECT som.cmpcode,som.sotype,som.somstoid AS pomstoid,ISNULL(som.sono,som.somstoid) AS pono,CONVERT(VARCHAR(10), som.sodate, 101) AS podate,som.somstres1 AS pogroup, somstnote as pomstnote,som.somststatus AS pomststatus,p.personname AS PIC,'' AS suppname FROM QL_trnsomst som INNER JOIN QL_mstperson p ON p.personoid=som.approvalpic WHERE som.cmpcode='" & CompnyCode & "' AND som.somststatus='Post' AND ISNULL(som.somstres3, '')='' AND som.sotype='" & ddlType.SelectedValue & "' AND som.somstoid NOT IN (SELECT wod.soitemmstoid FROM QL_trnwodtl1 wod WHERE som.somstoid=wod.soitemmstoid AND som.cmpcode=wod.cmpcode ) " & _
        " UNION " & _
        " SELECT som.cmpcode,som.sotype,som.somstoid AS pomstoid,ISNULL(som.sono,som.somstoid) AS pono,CONVERT(VARCHAR(10), som.sodate, 101) AS podate,som.somstres1 AS pogroup, somstnote as pomstnote,som.somststatus AS pomststatus,'' AS PIC,c.custname AS suppname FROM QL_trnsomst som INNER JOIN QL_mstcust c ON c.custoid=som.custoid WHERE som.cmpcode='" & CompnyCode & "' AND som.somststatus='Approved' AND ISNULL(som.somstres3, '')='' AND som.sotype='" & ddlType.SelectedValue & "' AND som.somstoid NOT IN (SELECT dom.somstoid FROM QL_trndomst dom WHERE dom.cmpcode=som.cmpcode AND dom.somstoid=som.somstoid)) AS dt ORDER BY podate DESC, pomstoid DESC"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnpomst")
        If dt.Rows.Count = 0 Then
            showMessage("There is no SO can be Cancel for this time.", 2)
            Exit Sub
        End If
        If ddlType.SelectedValue = "Buffer" Then
            gvListPO.Columns(3).Visible = False
            gvListPO.Columns(4).Visible = True
        Else
            gvListPO.Columns(3).Visible = True
            gvListPO.Columns(4).Visible = False
        End If
        Session("Tblpomst") = dt
        gvListPO.DataSource = Session("Tblpomst")
        gvListPO.DataBind()
        gvListPO.SelectedIndex = -1
        cProc.SetModalPopUpExtender(btnHiddenListPO, PanelListPO, mpeListPO, True)
    End Sub

    Private Sub ViewDetailPO(ByVal iOid As Integer)
        Dim sql As String = ""
        If ddlType.SelectedValue = "Buffer" Then
            sql &= " AND sod.sodtloid NOT IN (SELECT wo.soitemdtloid FROM QL_trnwodtl1 wo WHERE sod.sodtloid=wo.soitemdtloid AND sod.somstoid=som.somstoid AND sod.cmpcode=wo.cmpcode)"
        Else
            sql &= " AND sod.sodtloid NOT IN (SELECT do.sodtloid FROM QL_trndodtl do WHERE sod.sodtloid=do.sodtloid AND sod.somstoid=som.somstoid AND do.cmpcode=sod.cmpcode) "
        End If
        sSql = "SELECT sod.sodtlseq AS podtlseq,sod.sodtloid AS podtloid,som.cmpcode,som.somstoid AS pomstoid,m.itemoid,m.itemLongDescription,m.itemCode,sod.sodtlstatus AS podtlstatus,g.gendesc AS gendesc,sod.sodtlnote AS podtlnote,ISNULL(sod.sodtlqty,0.00) AS PoQty FROM QL_trnsodtl sod INNER JOIN QL_trnsomst som ON som.cmpcode=sod.cmpcode AND som.somstoid=sod.somstoid INNER JOIN QL_mstitem m ON m.itemoid=sod.itemoid INNER JOIN QL_mstgen g ON g.genoid=sod.sodtlunitoid WHERE som.cmpcode='" & CompnyCode & "' AND som.somstoid=" & pomstoid.Text & " AND som.sotype='" & ddlType.SelectedValue & "' " & sql & "ORDER BY sod.sodtlseq "
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnpodtl")
        Session("TblDtlPO") = dt
        gvPODtl.DataSource = dt
        gvPODtl.DataBind()
    End Sub

    Private Sub ClearData()
        pomstoid.Text = ""
        pono.Text = ""
        podate.Text = ""
        suppname.Text = ""
        pomstnote.Text = ""
        pomststatus.Text = ""
        gvPODtl.DataSource = Nothing
        gvPODtl.DataBind()
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnSOcancel.aspx")
        End If
        If checkPagePermission("~\Transaction\trnSOcancel.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - SO Closing"
        btnClosing.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to CLOSE this selected data?');")
        btnClosingAll.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to CLOSE this data?');")
        If Not Page.IsPostBack Then
            upduser.Text = Session("UserID") : updtime.Text = GetServerTime().ToString
            InitDDL("")
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("Success") Is Nothing And Session("Success") <> "" Then
            Response.Redirect("~\Transaction\trnSOcancel.aspx?awal=true")
        End If
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        'ClearData()
        If ddlType.SelectedValue = "Buffer" Then
            Name.Text = "PIC"
        Else
            Name.Text = "Customer"
        End If
    End Sub

    Protected Sub pobusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles pobusinessunit.SelectedIndexChanged
        ClearData()
    End Sub

    Protected Sub imbFindPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindPO.Click
        DDLFilterListPO.SelectedIndex = -1
        txtFilterListPO.Text = ""
        Session("Tblpomst") = Nothing
        gvListPO.DataSource = Session("Tblpomst")
        gvListPO.DataBind()
        BindListPO()
    End Sub

    Protected Sub imbErasePO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbErasePO.Click
        ClearData()
        InitDDL("")
        ddlType.Enabled = True
        ddlType.CssClass = "inpText"
    End Sub

    Protected Sub btnFindListPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListPO.Click
        Dim dv As DataView = Session("Tblpomst").DefaultView
        dv.RowFilter = DDLFilterListPO.SelectedValue & " LIKE '%" & Tchar(txtFilterListPO.Text) & "%'"
        gvListPO.DataSource = dv.ToTable
        gvListPO.DataBind()
        dv.RowFilter = ""
        mpeListPO.Show()
    End Sub

    Protected Sub btnViewAllListPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListPO.Click
        txtFilterListPO.Text = ""
        DDLFilterListPO.SelectedIndex = -1
        gvListPO.SelectedIndex = -1
        gvListPO.DataSource = Session("Tblpomst")
        gvListPO.DataBind()
        mpeListPO.Show()
    End Sub

    Protected Sub gvListPO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListPO.PageIndexChanging
        gvListPO.PageIndex = e.NewPageIndex
        Dim dv As DataView = Session("Tblpomst").DefaultView
        dv.RowFilter = DDLFilterListPO.SelectedValue & " LIKE '%" & Tchar(txtFilterListPO.Text) & "%'"
        gvListPO.DataSource = dv.ToTable
        gvListPO.DataBind()
        dv.RowFilter = ""
        mpeListPO.Show()
    End Sub

    Protected Sub gvListPO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListPO.SelectedIndexChanged
        If pomstoid.Text = gvListPO.SelectedDataKey.Item("pomstoid").ToString Then
            ClearData()
        End If
        pono.Text = gvListPO.SelectedDataKey.Item("pono").ToString
        pomstoid.Text = gvListPO.SelectedDataKey.Item("pomstoid").ToString
        podate.Text = Format(CDate(gvListPO.SelectedDataKey.Item("podate").ToString), "MM/dd/yyyy")
        If ddlType.SelectedValue = "Buffer" Then
            gvListPO.Columns(3).Visible = False
            suppname.Text = gvListPO.SelectedDataKey.Item("PIC").ToString
        Else
            suppname.Text = gvListPO.SelectedDataKey.Item("suppname").ToString
        End If
        pomststatus.Text = gvListPO.SelectedDataKey.Item("pomststatus").ToString
        pomstnote.Text = gvListPO.SelectedDataKey.Item("pomstnote").ToString
        ViewDetailPO(CInt(pomstoid.Text))
        ddlType.Enabled = False
        ddlType.CssClass = "inpTextDisabled"
        cProc.SetModalPopUpExtender(btnHiddenListPO, PanelListPO, mpeListPO, False)
    End Sub

    Protected Sub lkbListPO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListPO.Click
        cProc.SetModalPopUpExtender(btnHiddenListPO, PanelListPO, mpeListPO, False)
    End Sub

    Protected Sub gvpoDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPODtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnSOcancel.aspx?awal=true")
    End Sub

    Protected Sub btnClosingAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClosingAll.Click
        Dim sType As String = ddlType.SelectedValue
        Dim sError As String = ""
        If pomstnote.Text = "" Then
            sError &= "Please fill NOTE field as a reason for cancel this data!<BR>"
        Else

        End If
        If pomstoid.Text = "" Then
            sError &= "Please select SO DATA field!<BR>"
        Else
            sSql = "SELECT COUNT(*) FROM QL_trnsomst WHERE somstoid=" & pomstoid.Text & " AND somststatus='Cancel' AND cmpcode='" & pobusinessunit.SelectedValue & "'"
            If CheckDataExists(sSql) Then
                sError &= "This data has been canceled by another user. Please CANCEL this transaction and try again!<BR>"
            End If
        End If

        If sError <> "" Then
            showMessage(sError, 2)
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trnsomst SET somststatus='Cancel', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, cancelclosedreason='" & Tchar(pomstnote.Text) & "' WHERE cmpcode='" & CompnyCode & "' AND somstoid=" & pomstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If pomststatus.Text = "In Approval" Then
                sSql = "UPDATE QL_approval SET statusrequest='Cancel', event='Cancel' WHERE cmpcode='" & pobusinessunit.SelectedValue & "' AND tablename='QL_trnsomst' AND oid=" & pomstoid.Text
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Session("Success") = "SO No : " & pono.Text & " have been cancel successfully."
        showMessage(Session("Success"), 3)
    End Sub

    Protected Sub gvListPO_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "MM/dd/yyyy")
        End If
    End Sub
#End Region

    Protected Sub btnClosing_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub
End Class