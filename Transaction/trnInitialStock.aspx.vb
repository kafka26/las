﻿Imports System.IO
Imports System.Drawing
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Data
Imports ClassFunction
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Partial Class Transaction_trnInitialStock
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim kumpulanSN As String
    Dim has_sn As String
    '@@@@@@@@@@@ Konek Exel @@@@@@@@@
    Dim KodeITemExcel As String
    Dim JumItemExcel As Integer
    Dim adap As OleDbDataAdapter
    Dim SNExcel As String
    Dim cRate As New ClassRate
#End Region

#Region "Functions"

    Private Function IsFilterValid()
        Dim sError As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select GROUP field!<BR>"
        End If
        If DDLLocation.SelectedValue = "" Then
            sError &= "- Please select WAREHOUSE field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, CompnyName & " - Warning", 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetTextValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function GetDDLValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
            End If
        Next
        Return sReturn
    End Function

    Private Function GetDDLText(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedItem.Text
            End If
        Next
        Return sReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("matqty") = ToDouble(GetTextValue(row.Cells(4).Controls))
                                    dtView(0)("Harga") = ToDouble(GetTextValue(row.Cells(5).Controls))
                                    dtView(0)("unitoid") = GetDDLValue(row.Cells(6).Controls)
                                    dtView(0)("unit") = GetDDLText(row.Cells(6).Controls)
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl = dtView.ToTable
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matoid=" & cbOid
                                dtView2.RowFilter = "matoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("matqty") = ToDouble(GetTextValue(row.Cells(4).Controls))
                                    dtView(0)("Harga") = ToDouble(GetTextValue(row.Cells(5).Controls))
                                    dtView(0)("unitoid") = GetDDLValue(row.Cells(6).Controls)
                                    dtView(0)("unit") = GetDDLText(row.Cells(6).Controls)
                                    If dtView2.Count > 0 Then
                                        dtView2(0)("matqty") = ToDouble(GetTextValue(row.Cells(4).Controls))
                                        dtView2(0)("Harga") = ToDouble(GetTextValue(row.Cells(5).Controls))
                                        dtView2(0)("unitoid") = GetDDLValue(row.Cells(6).Controls)
                                        dtView2(0)("unit") = GetDDLText(row.Cells(6).Controls)
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function GetHppPercentage(ByVal sOid As String) As Double
        Dim dReturn As Double = 0
        Try
            dReturn = ToDouble(GetStrData("SELECT TOP 1 gendesc FROM QL_mstgen WHERE activeflag='ACTIVE' AND genother1='" & sOid & "' AND gengroup='HPP PERCENTAGE (%)' ORDER BY updtime DESC"))
        Catch ex As Exception
            dReturn = 0
        End Try
        Return dReturn
    End Function

#End Region

#Region "Procedures"

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            strCaption &= " - INFORMATION"
            lblCaption.ForeColor = Drawing.Color.White
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"

        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Public Sub BindData(ByVal sqlPlus As String)
        sSql = "select s.crdstockoid * (-1) AS nomor, i.itemCode, i.itemoldcode, i.itemShortDescription,i.itemLongDescription,s.saldoawal, amtawal_idr AS refhpp,amtawal_usd refhppusd,g1.gendesc AS unit,g2.gendesc AS mtr,'' hppstatus from QL_crdstock s inner join QL_mstitem i ON i.cmpcode=s.cmpcode AND i.itemoid=s.refoid AND i.itemRecordStatus='ACTIVE' inner join QL_mstgen g1 ON g1.genoid=i.itemUnit1 inner join QL_mstgen g2 ON g2.genoid=s.mtrlocoid WHERE s.crdstockoid < 0"
        If itemname.Text <> "" Then
            sSql &= IIf(DDLfilterGroup.SelectedValue = "ALL", "", " AND i." & DDLitemname.SelectedValue & " like '%" & itemname.Text & "%'")
        End If
        If chkGroup.Checked Then
            sSql &= IIf(DDLfilterGroup.SelectedValue = "ALL", "", " AND itemGroup= '" & DDLfilterGroup.SelectedValue & "'")
        End If
        If chkWH.Checked Then
            sSql &= IIf(DDLfilterWH.SelectedValue = "ALL", "", " AND mtrlocoid= " & DDLfilterWH.SelectedValue & "")
        End If
        sSql &= " order by s.crdstockoid"
        Dim dtab As DataTable = cKon.ambiltabel(sSql, "GVlist")
        gvTblData.DataSource = dtab
        gvTblData.DataBind()
        Session("List") = dtab
    End Sub

    Private Sub InitAllDDL()
        sSql = "select gencode,gendesc from QL_mstgen where gengroup='GROUPITEM' order by gendesc"
        FillDDL(DDLBusUnit, sSql)
        FillDDL(DDLfilterGroup, sSql)
        sSql = "select genoid,gendesc from QL_mstgen where gengroup='WAREHOUSE' order by gendesc"
        FillDDL(DDLLocation, sSql)
        FillDDL(DDLfilterWH, sSql)
        sSql = "select genoid,gencode+' - '+gendesc from QL_mstgen where gengroup='CAT4' order by gendesc"
        FillDDL(DDLCat04, sSql)
    End Sub

    Private Sub InitDDLLocation()
        'Fill DDL Init Location
        'sSql = "select a.genoid, (SELECT gendesc FROM QL_mstgen WHERE gengroup = 'WAREHOUSE' AND genoid = a.genother1) +' - '+a.gendesc gendesc from QL_mstgen a WHERE a.gengroup = 'Location' and a.genother2 = (SELECT genoid FROM QL_mstgen WHERE gencode = '" & DDLBusUnit.SelectedValue & "' and gengroup = 'cabang')"
        'FillDDL(DDLLocation, sSql)
    End Sub

    Private Sub InitDDLLocationForm()
        'sSql = "select a.genoid, (SELECT gendesc FROM QL_mstgen WHERE gengroup = 'WAREHOUSE' AND genoid = a.genother1) +' - '+a.gendesc gendesc from QL_mstgen a WHERE a.gengroup = 'Location' and a.genother2 = (SELECT genoid FROM QL_mstgen WHERE gencode = '" & dd_branch.SelectedValue & "' and gengroup = 'cabang')"
        'FillDDL(DDLocation, sSql)
        'DDLocation.Items.Add(New ListItem("SEMUA LOKASI", "ALL LOCATION"))
        'DDLocation.SelectedValue = "ALL LOCATION"
    End Sub

    Private Sub BindMaterial()
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
        Dim sType As String = "", sRef As String = ""

        sSql = "select i.itemoid AS matoid,i.itemcode AS matcode, i.itemcat2 itemcat22,i.itemoldcode AS oldcode, i.itemShortDescription AS shortdesc, i.itemLongDescription AS longdesc ,'' AS matqty, '' AS Harga, gen2.gendesc AS unit, gen2.genoid AS unitoid, gen3.gendesc AS warehouse, gen3.genoid AS warehouseoid, 'False' AS checkvalue,'True' AS enableqty, ISNULL((SELECT cat2res2 FROM QL_mstcat2 cat2 WHERE cat2.cat2oid=i.itemcat2),'') AS itemcat2 from  QL_mstitem i INNER JOIN QL_mstgen gen ON gen.genoid= i.itemgroupoid AND gencode='" & DDLBusUnit.SelectedValue & "' AND  gen.gengroup='GROUPITEM' INNER JOIN QL_mstgen gen2 on gen2.genoid=i.itemUnit1 AND gen2.gengroup='UNIT' LEFT JOIN ql_mstgen gen3 ON gen3.cmpcode=i.cmpcode AND gen3.genoid=" & DDLLocation.SelectedValue & " AND gen3.gengroup='WAREHOUSE' WHERE i.itemoid NOT IN (SELECT ini.refoid FROM QL_crdstock ini WHERE ini.cmpcode='" & CompnyCode & "' AND ini.mtrlocoid=" & DDLLocation.SelectedValue & ")"
        
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstitem")
        Session("TblMat") = dt
        Session("TblMatView") = Session("TblMat")
        gvListMat.DataSource = Session("TblMatView")
        gvListMat.DataBind()
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Private Sub InitDDLCat1(ByVal group As String)
        'Fill DDL Category 1
        sSql = "SELECT cat1oid, cat1code+' - '+cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat1res1='" & group & "' order by cat1shortdesc"
        FillDDL(DDLCat01, sSql)
        If DDLCat01.Items.Count > 0 Then
            InitDDLCat2()
        End If
    End Sub

    Private Sub InitDDLCat2()
        'Fill DDL Category 2
        sSql = "SELECT cat2oid, cat2code+' - '+cat2shortdesc FROM QL_mstcat2 cat2 INNER JOIN QL_mstcat1 cat1 on cat1.cat1oid=cat2.cat1oid WHERE cat2.cmpcode='" & CompnyCode & "' AND cat2.cat1oid=" & DDLCat01.SelectedValue & " AND cat2.activeflag='ACTIVE' order by cat2shortdesc"
        FillDDL(DDLCat02, sSql)
        If DDLCat02.Items.Count > 0 Then
            InitDDLCat3()
        End If
    End Sub

    Private Sub InitDDLCat3()
        'Fill DDL Category 3
        sSql = "SELECT cat3oid, cat3code+' - '+cat3shortdesc FROM QL_mstcat3 cat3 INNER JOIN QL_mstcat1 cat1 on cat1.cat1oid=cat3.cat1oid INNER JOIN QL_mstcat2 cat2 ON cat3.cat2oid=cat2.cat2oid WHERE cat3.cmpcode='" & CompnyCode & "' AND cat3.cat1oid=" & DDLCat01.SelectedValue & " AND cat3.cat2oid=" & DDLCat02.SelectedValue & " AND cat3.activeflag='ACTIVE' order by cat3shortdesc"
        FillDDL(DDLCat03, sSql)
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("TableDetail")
        dtlTable.Columns.Add("seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matwhoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matwh", Type.GetType("System.String"))
        dtlTable.Columns.Add("matoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("oldcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("shortdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("longdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("matqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("Harga", Type.GetType("System.Double"))
        dtlTable.Columns.Add("unitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("unit", Type.GetType("System.String"))
        dtlTable.Columns.Add("warehouseoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemcat2", Type.GetType("System.String"))
        dtlTable.Columns.Add("warehouse", Type.GetType("System.String"))
        dtlTable.Columns.Add("enableqty", Type.GetType("System.String"))
        dtlTable.Columns.Add("qtyunitkecil", Type.GetType("System.Double"))
        dtlTable.Columns.Add("qtyunitbesar", Type.GetType("System.Double"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal
        DDLBusUnit.CssClass = sCss
        DDLLocation.Enabled = bVal
        DDLLocation.CssClass = sCss
    End Sub

#End Region

#Region "Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnInitialStock.aspx")
        End If
        If checkPagePermission("~\Transaction\trnInitialStock.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        Page.Title = CompnyName & " - Initial Stock"
        Session("oid") = Request.QueryString("oid")
        btnPost.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POSTING this data?');")
        upduser.Text = Session("UserID") : updtime.Text = GetServerTime().ToString
        Page.Title = CompnyName & " - Initial Stock"
        btnPost.Visible = True
        btnCancel.Visible = True
        btnLookUpMat.Visible = True
        If Not Page.IsPostBack Then
            dateAwal.Text = Format(GetServerTime(), "MM/01/yyyy")
            dateAkhir.Text = Format(GetServerTime(), "MM/dd/yyyy")
            sSql = "select genother1 from QL_mstgen where gengroup='CUTOFDATE' AND activeflag='ACTIVE'"
            cutof.Text = GetStrData(sSql)
            InitAllDDL()
            cRate.SetRateValue(1, Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
            End If
            cbInfo.Visible = True
            TabContainer1.ActiveTabIndex = 0
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                mpeListMat.Show()
            End If
        End If
    End Sub

    Private Sub CreateTblDetailSN()
        Dim dtlTableSN As DataTable = New DataTable("TableDetailSN")
        dtlTableSN.Columns.Add("matoid", Type.GetType("System.Int32"))
        dtlTableSN.Columns.Add("matno", Type.GetType("System.String"))
        dtlTableSN.Columns.Add("matcode", Type.GetType("System.String"))
        dtlTableSN.Columns.Add("matlongdesc", Type.GetType("System.String"))
        dtlTableSN.Columns.Add("matqty", Type.GetType("System.Int32"))
        dtlTableSN.Columns.Add("Harga", Type.GetType("System.String"))
        dtlTableSN.Columns.Add("matunit", Type.GetType("System.String"))
        dtlTableSN.Columns.Add("unitoid", Type.GetType("System.Int32"))
        dtlTableSN.Columns.Add("checkvalue", Type.GetType("System.String"))
        dtlTableSN.Columns.Add("enableqty", Type.GetType("System.String"))
        Session("TblDtlSN") = dtlTableSN
    End Sub

    Private Sub BindMaterialSN()

        has_sn = "Yes"
        Session("Y") = has_sn
        Dim SaveLocation As String = FileExelInitStokSN.Text
        Dim ConnStrExe As String = "provider=Microsoft.jet.OLEDB.4.0; Data Source=" & SaveLocation & "; Extended Properties=Excel 8.0;"
        Dim connexc As New OleDbConnection(ConnStrExe)
        Dim commecel As OleDbCommand
        Dim TbSNAda As New DataTable
        Dim TbSNAda1 As New DataTable
        Dim TBCekSN As New DataTable
        Dim jumlahSnSama As Integer = 0
        Dim TampungSnSama As String
        Dim SNDami As Integer = 0
        connexc.Open()
        Try
            sSql = "select itemcode, sn from [SN_Uplaod$]"
            adap = New OleDbDataAdapter(sSql, connexc)
            adap.Fill(TbSNAda)
            adap.Dispose()
        Catch ex As Exception
            showMessage("Data Serial Init Stock Tidak Sesuai ", CompnyName & " - Warning", 2)
            Exit Sub
        End Try

        '@@@@@@@@@@@@@ Cek Item Di Gudang Init @@@@@@@@@@@@@@@
        sSql = "select DISTINCT(itemcode) from [SN_Uplaod$] "
        adap = New OleDbDataAdapter(sSql, connexc)
        adap.Fill(TbSNAda1)
        For itemDataSn As Integer = 0 To TbSNAda1.Rows.Count - 1
            sSql = "SELECT count(itemoid) FROM QL_mstItem where itemcode = '" & TbSNAda1.Rows(itemDataSn).Item("itemcode") & "'"
            Dim ItemID As Integer = cKon.ambilscalar(sSql)

            sSql = "SELECT count(*) FROM QL_initstock where initrefoid = '" & ItemID & "' and initbrach = '" & DDLBusUnit.SelectedValue & "' and initlocation = '" & DDLLocation.SelectedValue & "'"
            Dim ItemData As Integer = cKon.ambilscalar(sSql)
            If Not ItemData = 0 Then
                showMessage("Init Stock Sudah Ada Di Gudang or </br> Material data can't be found!, <strong>check Data Excel</strong> ", CompnyName & " - INFORMATION", 3)
                Exit Sub
            End If
        Next


        '@@@@@@@@@@@@ Cek SN Per Item @@@@@@@@@@@@@@@@@@@@@@@
        sSql = "select Sn from [SN_Uplaod$] "
        adap = New OleDbDataAdapter(sSql, connexc)
        adap.Fill(TBCekSN)
        For SNup As Integer = 0 To TBCekSN.Rows.Count - 1
            sSql = "SELECT count(SN) FROM QL_mstitemDtl WHERE sn = '" & TBCekSN.Rows(SNup).Item("Sn") & "'"
            Dim JumSn As Integer = cKon.ambilscalar(sSql)
            If JumSn = 1 Then
                sSql = "SELECT SN FROM QL_mstitemDtl WHERE sn = '" & TBCekSN.Rows(SNup).Item("Sn") & "'"
                TampungSnSama = cKon.ambilscalar(sSql)
                kumpulanSN = kumpulanSN & " " & TampungSnSama
                jumlahSnSama = jumlahSnSama + 1
                If jumlahSnSama < TBCekSN.Rows.Count - 1 Then
                    kumpulanSN &= ","
                    If (jumlahSnSama / 2) = 5 Then
                        kumpulanSN &= "<br/>"
                    End If
                End If
            End If
        Next

        If Not jumlahSnSama = 0 Then
            showMessage("<strong>Ada SN Sama Yang Di Upload </strong> <br/> <br> " & kumpulanSN & " ", CompnyName & " - INFORMATION", 3)
            Exit Sub
        End If

        For TamSN As Integer = 0 To TbSNAda1.Rows.Count - 1
            SNExcel &= "'" & TbSNAda1.Rows(TamSN).Item("itemcode") & "'"
            If TamSN < TbSNAda1.Rows.Count - 1 Then
                SNExcel &= ","
            End If
        Next

        UpdateCheckedMat()
        If Session("TblListMatView") Is Nothing Then
            sSql = "select i.itemoid  AS matoid, '' AS matno,i.itemcode matcode,  gen.gendesc +' '+ i.itemdesc +' '+  i.merk matlongdesc ,'' AS matqty, '' AS Harga, gen2.gendesc matunit, gen2.genoid unitoid , 'True' AS checkvalue,'True' AS enableqty   from  QL_mstitem i   INNER JOIN QL_mstgen gen ON gen.genoid= i.itemgroupoid  INNER JOIN QL_mstgen gen2 on gen2.genoid = i.satuan3   where i.has_SN = 1 and i.itemoid NOT IN (SELECT initrefoid FROM QL_initstock init WHERE init.cmpcode='" & CompnyCode & "' AND init.initbrach='" & DDLBusUnit.SelectedValue & "' AND initlocation ='" & DDLLocation.SelectedValue & "') and i.itemcode in (" & SNExcel & ")"
            Dim dtSN As DataTable = cKon.ambiltabel(sSql, "QL_mstmat")

            If dtSN.Rows.Count > 0 Then
                Session("TblListMatView") = dtSN
            End If
        End If

        If Not Session("TblListMatView") Is Nothing Then

            Dim TabelItem As DataTable = Session("TblListMatView")
            Dim dv As DataView = TabelItem.DefaultView

            If Session("TblDtlSN") Is Nothing Then
                CreateTblDetailSN()
                Dim objTblSN As DataTable = Session("TblDtlSN")
                Dim objView As DataView = objTblSN.DefaultView
                For iSN As Integer = 0 To TabelItem.Rows.Count - 1
                    Dim rv As DataRowView = objView.AddNew()
                    rv.BeginEdit()

                    sSql = "select count(sn) from [SN_Uplaod$] where itemcode = '" & TabelItem.Rows(iSN).Item("matcode") & "'"
                    commecel = New OleDbCommand(sSql, connexc)
                    JumItemExcel = commecel.ExecuteScalar()

                    rv("matoid") = dv(iSN)("matoid").ToString
                    rv("matcode") = dv(iSN)("matcode").ToString

                    rv("matqty") = JumItemExcel
                    rv("Harga") = ""
                    rv("matunit") = dv(iSN)("matunit").ToString
                    rv("unitoid") = dv(iSN)("unitoid").ToString
                    rv("checkvalue") = dv(iSN)("enableqty").ToString
                    rv("enableqty") = dv(iSN)("enableqty").ToString
                    rv("matlongdesc") = dv(iSN)("matlongdesc").ToString
                    rv.EndEdit()
                Next
                objTblSN.AcceptChanges()
                Session("TblListMatView") = objTblSN
                Session("TblListMat") = objTblSN

                gvListMat.DataSource = Session("TblListMatView")
                gvListMat.DataBind()
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            Else

                gvListMat.DataSource = Session("TblListMatView")
                gvListMat.DataBind()
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)

            End If
            sSql = "select * from [SN_Uplaod$]"
            adap = New OleDbDataAdapter(sSql, connexc)
            Dim dtecelSN As New DataTable
            adap.Fill(dtecelSN)
            Session("TabelSN") = dtecelSN
        Else
            showMessage("Init Stock Sudah Ada Di Gudang or </br> Material data can't be found! ", CompnyName & " - INFORMATION", 3)
            Exit Sub
        End If

        connexc.Close()
    End Sub

    Protected Sub btnLookUpMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLookUpMat.Click
        If IsFilterValid() Then
            FilterDDLListMat.SelectedIndex = 0
            FilterTextListMat.Text = ""
            InitDDLCat1(DDLBusUnit.SelectedValue)
            cbCat01.Checked = False
            cbCat02.Checked = False
            cbCat03.Checked = False
            cbCat04.Checked = False
            Session("TblMat") = Nothing : Session("TblMatView") = Nothing : gvListMat.DataSource = Nothing : gvListMat.DataBind()
            BindMaterial()
        End If
    End Sub

    Protected Sub cbHdrLM_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        For C1 As Integer = 0 To gvListMat.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub lbAddToListMat_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbAddToListMat.Click
        UpdateCheckedMat()
        If Session("TblMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMat")
            If dt.Rows.Count > 0 Then
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "checkvalue='True'"
                If dv.Count > 0 Then
                    Dim sErr As String = ""
                    For C1 As Integer = 0 To dv.Count - 1
                        If ToDouble(dv(C1)("matqty").ToString) <= 0 Then
                            sErr = "Every Qty of selected data must be more than 0!"
                            Exit For
                        End If
                        If ToDouble(dv(C1)("harga").ToString) <= 0 Then
                            sErr = "Every Init Value of selected data must be more than 0!"
                            Exit For
                        End If
                    Next
                    If sErr <> "" Then
                        Session("WarningListMat") = sErr
                        showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2) : Exit Sub
                    End If
                    If Session("TblDtl") Is Nothing Then
                        CreateTblDetail()
                    End If
                    Dim objTbl As DataTable = Session("TblDtl")
                    Dim iSeq As Integer = objTbl.Rows.Count + 1
                    Dim objView As DataView = objTbl.DefaultView
                    For C1 As Integer = 0 To dv.Count - 1
                        objView.RowFilter = " matoid=" & dv(C1)("matoid")
                        If objView.Count > 0 Then
                            objView(0)("matqty") = ToDouble(dv(C1)("matqty").ToString)
                            objView(0)("Harga") = ToDouble(dv(C1)("Harga").ToString)
                        Else
                            Dim rv As DataRowView = objView.AddNew()
                            rv.BeginEdit()
                            rv("seq") = iSeq
                            rv("matwhoid") = DDLLocation.SelectedValue
                            rv("matwh") = DDLLocation.SelectedItem.Text
                            rv("matoid") = dv(C1)("matoid").ToString
                            rv("matcode") = dv(C1)("matcode").ToString
                            rv("oldcode") = dv(C1)("oldcode").ToString
                            rv("shortdesc") = dv(C1)("shortdesc").ToString
                            rv("longdesc") = dv(C1)("longdesc").ToString
                            rv("matqty") = ToDouble(dv(C1)("matqty").ToString)
                            rv("unit") = dv(C1)("unit").ToString
                            rv("unitoid") = dv(C1)("unitoid").ToString
                            rv("Harga") = ToMaskEdit(ToDouble(dv(C1)("Harga").ToString), 4)
                            rv("warehouse") = dv(C1)("warehouse").ToString
                            rv("warehouseoid") = dv(C1)("warehouseoid").ToString
                            rv("enableqty") = dv(C1)("enableqty").ToString
                            rv("itemcat2") = dv(C1)("itemcat2").ToString
                            rv.EndEdit()
                            iSeq += 1
                        End If
                        objView.RowFilter = ""
                    Next
                    objTbl.AcceptChanges()
                    Session("TblDtl") = objTbl
                    gvDtl.DataSource = Session("TblDtl")
                    gvDtl.DataBind()
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)

                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
                End If
            Else
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
            End If
        Else
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
        End If
    End Sub

    Protected Sub lbSelectAllToList_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbSelectAllToList.Click
        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dtTbl As DataTable = Session("TblListMat")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblListMat")
                Dim objView As DataView = objTbl.DefaultView
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matoid=" & dtTbl.Rows(C1)("matoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblListMat") = objTbl
                gvListMat.DataSource = Session("TblListMat")
                gvListMat.DataBind()
                lbAddToListMat_Click(Nothing, Nothing)
                btnHideListMat.Visible = False
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            Else
                Session("WarningListMat") = "Please show material data first!"
                showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
            End If
        Else
            Session("WarningListMat") = "Please show material data first!"
            showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
        End If
    End Sub

    Protected Sub gvDtl_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDtl.PageIndexChanging
        gvDtl.PageIndex = e.NewPageIndex
        gvDtl.DataSource = Session("List")
        gvDtl.DataBind()
    End Sub

    Protected Sub gvDtl_RowCancelingEdit(ByVal sender As Object, ByVal e As GridViewCancelEditEventArgs) Handles gvDtl.RowCancelingEdit
        e.Cancel = True
        gvDtl.EditIndex = -1
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If e.Row.Cells(5).Text = "Yes" Then
                e.Row.Cells(6).Enabled = False
            Else
                e.Row.Cells(6).Enabled = True
            End If
        End If
    End Sub

    Protected Sub gvDtl_RowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs) Handles gvDtl.RowDeleting
        e.Cancel = True
        gvDtl.EditIndex = -1
        Dim objTable As DataTable = Session("TblDtl")

        Dim objRow() As DataRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("seq") = C1 + 1
            dr.EndEdit()
        Next
        objTable.AcceptChanges()
        Session("TblDtl") = objTable
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
    End Sub

    Protected Sub gvDtl_RowEditing(ByVal sender As Object, ByVal e As GridViewEditEventArgs) Handles gvDtl.RowEditing
        gvDtl.EditIndex = e.NewEditIndex
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
    End Sub

    Protected Sub gvDtl_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gvDtl.RowUpdating
        Dim row As GridViewRow = gvDtl.Rows(e.RowIndex)
        Dim dQty As Double = ToDouble(CType(row.FindControl("tbQty"), System.Web.UI.WebControls.TextBox).Text)
        Dim dHarga As Double = ToDouble(CType(row.FindControl("tbAmount"), System.Web.UI.WebControls.TextBox).Text)
        Dim iOid As Integer = ToInteger(gvDtl.DataKeys(e.RowIndex).Value.ToString())
        Dim dQty_unitkecil, dQty_unitbesar As Double
        Session("TblDtl").DefaultView.RowFilter = "seq=" & iOid
        Session("TblDtl").DefaultView(0)("matqty") = dQty
        Session("TblDtl").DefaultView(0)("harga") = dHarga
        GetUnitConverter(Session("TblDtl").DefaultView(0)("matoid"), Session("TblDtl").DefaultView(0)("unitoid"), dQty, dQty_unitkecil, dQty_unitbesar)
        Session("TblDtl").DefaultView(0)("qtyunitkecil") = dQty_unitkecil
        Session("TblDtl").DefaultView(0)("qtyunitbesar") = dQty_unitbesar
        Session("TblDtl").DefaultView.RowFilter = ""
        e.Cancel = True
        gvDtl.EditIndex = -1
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnFindListMat.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim sFilter As String = ""
            Dim sSplit() As String = FilterTextListMat.Text.Split(" ")
            For C1 As Integer = 0 To sSplit.Length - 1
                sFilter &= FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(sSplit(C1)) & "%'"
                If C1 < sSplit.Length - 1 Then
                    sFilter &= " AND "
                End If
            Next
            dtView.RowFilter = sFilter
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), "WARNING", 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = 0
        FilterTextListMat.Text = ""
        InitDDLCat1(DDLBusUnit.SelectedValue)
        cbCat01.Checked = False
        cbCat02.Checked = False
        cbCat03.Checked = False
        cbCat04.Checked = False
        If UpdateCheckedMat() Then
            BindMaterial()
            Session("TblMatView") = Session("TblMat")
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnPost.Click
        If Session("TblDtl") Is Nothing Then
            showMessage("Please initiate some material data first!", CompnyName & " - Warning", 2)
            Exit Sub
        End If
        Dim objTable As DataTable = Session("TblDtl")
        If objTable.Rows.Count = 0 Then
            showMessage("Please initiate some material data first!", CompnyName & " - Warning", 2)
            Exit Sub
        End If
      
        Dim iStockValOid As Integer = GenerateID("QL_STOCKVALUE", CompnyCode)
        'Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        Dim sDate As String = Format(CDate(cutof.Text), "MM/dd/yyyy")
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
        sSql = "SELECT curroid FROM QL_mstcurr WHERE currcode='IDR'"
        cRate.SetRateValue(cKon.ambilscalar(sSql), sDate)
        If cRate.GetRateDailyLastError <> "" Then
            showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
        End If
        If cRate.GetRateMonthlyLastError <> "" Then
            showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
        End If

        If isPeriodClosed(CompnyCode, sDate) Then
            showMessage("This Period Has been already Closed, Please CANCEL this transaction!", CompnyName & " - Warning", 2)
            Exit Sub
        End If

        Dim stockOid As String = GetStrData("SELECT isnull(MIN(crdstockoid)-1,-1) FROM ql_crdstock WHERE cmpcode='" & CompnyCode & "' AND crdstockoid < 0")
        Dim iConStockOid As String = GetStrData("SELECT ISNULL(MIN(constockoid)-1,-1) FROM QL_constock WHERE cmpcode='" & CompnyCode & "' AND constockoid < 0")
        Dim hppOid As String = GetStrData("SELECT isnull(MIN(hppoid)-1,-1) FROM ql_crdhpp WHERE cmpcode='" & CompnyCode & "' AND hppoid < 0")

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            For C1 As Int16 = 0 To objTable.Rows.Count - 1
                Dim Harga As Double = ToDouble(objTable.Rows(C1).Item("Harga"))
                Dim RateIDR As Double = Harga * cRate.GetRateMonthlyIDRValue
                Dim RateUSD As Double = Harga * cRate.GetRateMonthlyUSDValue

                sSql = "INSERT INTO QL_constock (cmpcode, constockoid, contype, trndate, periodacctg, formaction, formoid, refoid, refname, mtrlocoid, qtyin, qtyout, reason, upduser, updtime, typemin, note, valueidr, valueusd, createuser, createtime) VALUES ('" & CompnyCode & "', " & iConStockOid & ", 'MIS', '" & sDate & "', '" & sPeriod & "', 'Initial Stock', 0, " & objTable.Rows(C1)("matoid") & ", '" & DDLBusUnit.SelectedItem.ToString & "', " & objTable.Rows(C1)("matwhoid") & ", " & objTable.Rows(C1).Item("matqty") & ", 0, 'MATERIAL INIT STOCK', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 1, 'Material Init Stock', " & RateIDR & ", " & RateIDR & ", '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                iConStockOid += -1
                ' Update QL_crdstock
                sSql = "UPDATE QL_crdstock SET saldoawal=" & objTable.Rows(C1).Item("matqty") & ", saldoakhir=saldoakhir + " & ToDouble(objTable.Rows(C1).Item("matqty")) & ", amtawal_idr=amtawal_idr + " & RateIDR & ", amtawal_usd=amtawal_usd + " & RateUSD & ", lasttranstype='MATERIAL INIT STOCK', lasttransdate='" & sDate & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & CompnyCode & "' AND refoid=" & objTable.Rows(C1).Item("matoid") & " AND mtrlocoid=" & objTable.Rows(C1).Item("matwhoid") & " AND periodacctg='" & sPeriod & "'"
                xCmd.CommandText = sSql
                If xCmd.ExecuteNonQuery() = 0 Then
                    ' Insert QL_crdstock
                    sSql = "INSERT INTO QL_crdstock (cmpcode, crdstockoid, periodacctg, refname, refoid, mtrlocoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir, lasttranstype, lasttransdate, createuser, createtime, upduser, updtime, amtawal_idr,amtawal_usd) VALUES ('" & CompnyCode & "', " & stockOid & ", '" & sPeriod & "', '" & DDLBusUnit.SelectedItem.ToString & "', " & objTable.Rows(C1).Item("matoid") & ", " & objTable.Rows(C1).Item("matwhoid") & ", 0, 0, 0, 0, " & objTable.Rows(C1).Item("matqty") & ", " & objTable.Rows(C1).Item("matqty") & ",'MATERIAL INIT STOCK','" & sDate & "','" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & RateIDR & ", " & RateUSD & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    stockOid += -1
                End If

                'Insert STockvalue
                sSql = GetQueryUpdateStockValue(ToDouble(objTable.Rows(C1)("matqty").ToString), ToDouble(objTable.Rows(C1)("matqty").ToString), RateIDR, RateUSD, "INITIAL STOCK", sDate, Session("UserID"), CompnyCode, sPeriod, objTable.Rows(C1)("matoid"))
                xCmd.CommandText = sSql
                If xCmd.ExecuteNonQuery() = 0 Then
                    sSql = GetQueryInsertStockValue(ToDouble(objTable.Rows(C1)("matqty").ToString), ToDouble(objTable.Rows(C1)("matqty").ToString), RateIDR, RateUSD, "INITIAL STOCK", sDate, Session("UserID"), CompnyCode, sPeriod, objTable.Rows(C1)("matoid"), iStockValOid)
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iStockValOid += 1
                End If

                Dim price As Double = Harga
                Dim dPercentage As Double = GetHppPercentage(objTable.Rows(C1)("itemcat2").ToString)
                Dim hpp As Double = ToMaskEdit(price * (dPercentage / 100), 2)
                'update minStock di tabel mstitem
                sSql = "UPDATE QL_mstitem SET itemMinPrice = " & price + hpp & ", itemMinPrice1 = " & price + hpp & ", itemMinPrice2 = " & price + hpp & ", itemMinPrice3 = " & price + hpp & " WHERE itemoid = '" & objTable.Rows(C1).Item("matoid") & "' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            Next
            sSql = "UPDATE QL_mstoid SET lastoid=" & iStockValOid - 1 & " WHERE tablename='QL_STOCKVALUE' AND cmpcode='" & CompnyCode & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            showMessage(ex.Message, CompnyName & " - ERORR", 1)
            conn.Close()
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnInitialStock.aspx?awal=true")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnInitialStock.aspx?awal=true")
    End Sub

    Protected Sub CheckBox1_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CheckBox1.CheckedChanged
        If Page.IsPostBack Then
            If CheckBox1.Checked = True Then
                FileUpload1.Visible = True
                BtnUplaodFile.Visible = True
                TabContainer1.ActiveTabIndex = 1
            Else
                FileUpload1.Visible = False
                BtnUplaodFile.Visible = False
                FileExelInitStokSN.Text = ""
                TabContainer1.ActiveTabIndex = 1
            End If
            btnPost.Visible = True
            btnCancel.Visible = True
            btnLookUpMat.Visible = True
        End If

    End Sub

    Public Function getFileName(ByVal backID As String) As String
        Dim fname As String = ""
        Dim ID As String = Trim(backID)
        fname = "InitStok_" & ID & ".xls"
        Return fname
    End Function

    Function checkTypeFile(ByVal filename As String) As Boolean
        Dim fileExt As String = Path.GetExtension(filename).ToLower()
        Dim allowedExt As String() = {".xls"}
        Dim fileOK As Boolean = False
        For i As Integer = 0 To allowedExt.Length - 1
            If fileExt = allowedExt(i) Then
                fileOK = True
            End If
        Next
        Return fileOK
    End Function

    Public Sub uploadFileGambar(ByVal proses As String, ByVal fname As String)
        Dim savePath As String = Server.MapPath("~/FileExcel/")

        If FileUpload1.HasFile Then
            If checkTypeFile(FileUpload1.FileName) Then
                savePath &= fname
                Try
                    FileUpload1.PostedFile.SaveAs(savePath)
                    FileExelInitStokSN.Text = savePath
                    Session("LinkSaveFile") = FileExelInitStokSN.Text
                Catch ex As Exception
                End Try
            Else
                showMessage("Tidak dapat diupload. Format file harus .xls!", CompnyName & " - WARNING", 2)
            End If
        Else
            'showMessage("Tidak Ada File  .xls! Yang Di Pilih", CompnyName & " - WARNING", 2)
        End If
    End Sub

    Protected Sub BtnUplaodFile_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnUplaodFile.Click
        Dim fname As String = getFileName(Trim("File Exel Init Stok SN"))
        uploadFileGambar("insert", fname)
        btnPost.Visible = True
        btnCancel.Visible = True
        btnLookUpMat.Visible = True
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbCloseListMat.Click
        btnHideListMat.Visible = False
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
        btnPost.Visible = True
        btnCancel.Visible = True
        btnLookUpMat.Visible = True
    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gvDtl.SelectedIndexChanged
        If gvDtl.SelectedDataKey("SN").ToString().Trim = "Yes" Then
            showMessage("Data Tidak Bisa Di Edit Adau Del", CompnyCode, 3)
        End If
    End Sub

    Public Sub BindDataListItem()
        'Dim sWhare As String = ""
        'sSql = "select DISTINCT m.itemoid, m.itemcode, m.itemdesc,m.itempriceunit1,m.itempriceunit2,m.itempriceunit3,m.itemoid, g.gendesc satuan1,g2.gendesc satuan2, g3.gendesc satuan3,m.konversi1_2, m.konversi2_3 , m.merk, (SELECT gendesc FROM QL_mstgen WHERE gencode =  ik.initbrach AND gengroup = 'cabang') branch_code    from ql_mstitem m  INNER JOIN QL_initstock ik ON m.itemoid = ik.initrefoid  inner join ql_mstgen g on g.genoid=m.satuan1 and m.itemflag='AKTIF'   inner join ql_mstgen g2 on g2.genoid=m.satuan2   inner join ql_mstgen g3 on g3.genoid=m.satuan3     WHERE m.cmpcode = '" & CompnyCode & "' " & sWhare & "  AND (m.itemdesc like '%" & Tchar(itemname.Text) & "%' or m.itemcode like '%" & Tchar(itemname.Text) & "%' or m.merk like '%" & Tchar(itemname.Text) & "%')"

        'Dim DTItem As DataTable = cKon.ambiltabel(sSql, "listofitem")
        'gvItem.DataSource = DTItem
        'gvItem.DataBind()
        'Session("listofitem") = DTItem
        'gvItem.Visible = True
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnSearchItem.Click
        BindDataListItem()
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If has_sn = "Yes" Then
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(3).Enabled = False
            End If
        Else
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(3).Enabled = True
            End If
        End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim iUnitID As String = e.Row.Cells(6).ToolTip
            Dim cc2 As System.Web.UI.ControlCollection = e.Row.Cells(6).Controls
            For Each myControl As System.Web.UI.Control In cc2
                If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                    iUnitID = CType(myControl, System.Web.UI.WebControls.DropDownList).ToolTip
                End If
            Next

            '    'Isi untuk dropdownlist unit
            sSql = "select genoid,gendesc FROM( select genoid,gendesc from QL_mstitem sod INNER JOIN QL_mstgen g ON sod.itemUnit1=g.genoid WHERE g.gengroup='UNIT' AND sod.itemcode='" & e.Row.Cells(1).Text & "' UNION ALL select genoid,gendesc from QL_mstitem sod INNER JOIN QL_mstgen g ON sod.itemUnit2=g.genoid WHERE g.gengroup='UNIT' AND sod.itemcode='" & e.Row.Cells(1).Text & "' UNION ALL select genoid,gendesc from QL_mstitem sod INNER JOIN QL_mstgen g ON sod.itemUnit3=g.genoid WHERE g.gengroup='UNIT' AND sod.itemcode='" & e.Row.Cells(1).Text & "' )AS dt"

            Dim ods As New SqlDataAdapter(sSql, conn)
            Dim objTablee As New DataTable
            ods.Fill(objTablee)

            Dim cc3 As System.Web.UI.ControlCollection = e.Row.Cells(6).Controls
            'Dim price As String = e.Row.Cells(8).Text
            Dim tempString As String = ""
            For Each myControl As System.Web.UI.Control In cc3
                If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                    For x As Integer = 0 To objTablee.Rows.Count - 1
                        Dim textTemp As String = objTablee.Rows(x).Item("gendesc")
                        Dim oidTemp As String = objTablee.Rows(x).Item("genoid")
                        CType(myControl, System.Web.UI.WebControls.DropDownList).Items.Add(New ListItem(textTemp, oidTemp))
                    Next
                    CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue = iUnitID
                    tempString = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
                End If
            Next
        End If
    End Sub

    Protected Sub gvTblData_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs) Handles gvTblData.PageIndexChanging
        gvTblData.PageIndex = e.NewPageIndex
        gvTblData.DataSource = Session("List")
        gvTblData.DataBind()
        gvTblData.Visible = True
    End Sub

    Protected Sub btnfind_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnfind.Click
        BindData("")
        cbInfo.Visible = False
    End Sub

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Protected Sub btnPreview_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnPreview.Click

        Try

            Dim path As String = Server.MapPath("~/report/FileExel_SN.xls") 'get file object as FileInfo
            Dim file As System.IO.FileInfo = New System.IO.FileInfo(path) '-- if the file exists on the server
            Response.AddHeader("Content-Disposition", "attachment; filename=FileExel_SN.xls")
            Response.AddHeader("Content-Length", file.Length.ToString())
            Response.ContentType = "application/octet-stream"
            Response.WriteFile(file.FullName)
            Response.End() 'if file does not exist
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Protected Sub btnEraseItem_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnEraseItem.Click
        itemname.Text = ""
        itemoid.Text = ""
        'gvItem.Visible = False
    End Sub

    Protected Sub btnviewall_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnviewall.Click
        dateAwal.Text = Format(Now, "01/MM/yyyy")
        dateAkhir.Text = Format(Now, "dd/MM/yyyy")
        chkPeriod.Checked = False
        chkGroup.Checked = False
        chkWH.Checked = False
        itemname.Text = ""
        itemoid.Text = ""
        BindData("")
        cbInfo.Visible = False
    End Sub

    Protected Sub gvTblData_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvTblData.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 4)
        End If
    End Sub

    Protected Sub DDLCat01_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitDDLCat2()
        mpeListMat.Show()
    End Sub

    Protected Sub DDLCat02_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitDDLCat3()
        mpeListMat.Show()
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matoid=" & dtTbl.Rows(C1)("matoid")
                    objView(0)("CheckValue") = "True"
                    dtTbl.Rows(C1)("CheckValue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                dtTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "No material data can't be selected!"
                showMessage(Session("WarningListMat"), "WARNING", 2)
            End If
        Else
            Session("WarningListMat") = "No material data can't be selected!"
            showMessage(Session("WarningListMat"), "WARNING", 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matoid=" & dtTbl.Rows(C1)("matoid")
                    objView(0)("CheckValue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                dtTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "No material data can't be selected!"
                showMessage(Session("WarningListMat"), "WARNING", 2)
            End If
        Else
            Session("WarningListMat") = "No material data can't be selected!"
            showMessage(Session("WarningListMat"), "WARNING", 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "CheckValue='True'"
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListMat") = "No material data have been selected before!"
                showMessage(Session("WarningListMat"), "WARNING", 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

#End Region

End Class
