<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnMatUsage.aspx.vb" Inherits="Transaction_MaterialUsageNonKIK" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2" style="height: 3px; width: 927px;">
                <asp:Label ID="Label1" runat="server" ForeColor="#804000" Text=".: Material Usage Non KIK" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Material Usage Non KIK :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" DefaultButton="btnSearch" __designer:wfdid="w83"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="Label15" runat="server" Text="Filter" __designer:wfdid="w84"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w85"><asp:ListItem Value="mum.matusagemstoid">Draft No.</asp:ListItem>
<asp:ListItem Value="mum.matusageno">Usage No.</asp:ListItem>
<asp:ListItem Value="de.deptname">Department</asp:ListItem>
<asp:ListItem Value="mum.matusagemstnote">Note</asp:ListItem>
<asp:ListItem Value="mum.createuser">Create User</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="150px" __designer:wfdid="w86"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text="Period" __designer:wfdid="w87"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="60px" __designer:wfdid="w88"></asp:TextBox> <asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w89"></asp:ImageButton> <asp:Label id="Label5" runat="server" Text="to" __designer:wfdid="w90"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="60px" __designer:wfdid="w91"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w92"></asp:ImageButton> <asp:Label id="lbldate1" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w93"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status" __designer:wfdid="w94"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w95"><asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w96" PopupButtonID="imbDate1" Format="MM/dd/yyyy" TargetControlID="FilterPeriod1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w97" PopupButtonID="imbDate2" Format="MM/dd/yyyy" TargetControlID="FilterPeriod2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender3" runat="server" __designer:wfdid="w98" TargetControlID="FilterPeriod1" UserDateFormat="MonthDayYear" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender4" runat="server" __designer:wfdid="w99" TargetControlID="FilterPeriod2" UserDateFormat="MonthDayYear" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w100"></asp:ImageButton> <asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w101"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w102"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:LinkButton id="lkbUsageInProcess" runat="server" __designer:wfdid="w103"></asp:LinkButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvTRN" runat="server" ForeColor="#333333" Width="100%" __designer:wfdid="w104" AllowSorting="True" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" DataKeyNames="matusagemstoid">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="matusagemstoid" DataNavigateUrlFormatString="~\Transaction\trnMatUsage.aspx?oid={0}" DataTextField="matusagemstoid" HeaderText="Draft No." SortExpression="matusagemstoid">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="matusageno" HeaderText="Usage No." SortExpression="matusageno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matusagedate" HeaderText="Usage Date" SortExpression="matusagedate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="deptname" HeaderText="Department" SortExpression="deptname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matusagemststatus" HeaderText="Status" SortExpression="matusagemststatus">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matusagemstnote" HeaderText="Note" SortExpression="matusagemstnote">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="divname" HeaderText="Business Unit" SortExpression="divname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="createuser" HeaderText="Create User" SortExpression="createuser">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField SelectImageUrl="~/Images/print.gif" ShowSelectButton="True" ButtonType="Image">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w105"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=4><asp:UpdateProgress id="UpdateProgress2" runat="server" __designer:wfdid="w106" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image21" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w107"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="gvTRN"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
<asp:UpdatePanel id="UpdatePanel1" runat="server"><ContentTemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label3" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Material Usage Non KIK Header :" __designer:wfdid="w183" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="i_u" runat="server" CssClass="Important" Text="New Data" __designer:wfdid="w184"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="periodacctg" runat="server" __designer:wfdid="w185" Visible="False"></asp:Label> <asp:Label id="matoid_mix" runat="server" __designer:wfdid="w186" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label10" runat="server" Text="Business Unit" __designer:wfdid="w187" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w188" Visible="False" AutoPostBack="True" Enabled="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblspuno" runat="server" Text="Draft No" __designer:wfdid="w189"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:Label id="matusagemstoid" runat="server" __designer:wfdid="w190"></asp:Label><asp:TextBox id="matusageno" runat="server" CssClass="inpTextDisabled" Width="160px" __designer:wfdid="w191" Visible="False" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label4" runat="server" Text="Department" __designer:wfdid="w194"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="deptoid" runat="server" CssClass="inpText" Width="205px" __designer:wfdid="w195"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label6" runat="server" Text="Usage Date" __designer:wfdid="w192"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="matusagedate" runat="server" CssClass="inpTextDisabled" Width="80px" __designer:wfdid="w193" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label8" runat="server" Text="Header Note" __designer:wfdid="w198"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="matusagemstnote" runat="server" CssClass="inpText" Width="200px" __designer:wfdid="w199" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label9" runat="server" Text="Status" __designer:wfdid="w196"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="matusagemststatus" runat="server" CssClass="inpTextDisabled" Width="80px" __designer:wfdid="w197" Enabled="False"></asp:TextBox></TD></TR><TR><TD id="TD4" class="Label" align=left runat="server" Visible="false"><asp:Label id="lblMatMix" runat="server" Text="Material Mixing" __designer:wfdid="w200" Visible="False"></asp:Label></TD><TD id="TD2" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD3" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="matlongdesc_mix" runat="server" CssClass="inpTextDisabled" Width="160px" __designer:wfdid="w201" Visible="False" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSearchMatMix" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w202" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnClearMatMix" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w203" Visible="False"></asp:ImageButton></TD><TD id="TD5" class="Label" align=left runat="server" Visible="false"><asp:Label id="lblQtyMix" runat="server" Text="Qty Mixing" __designer:wfdid="w204" Visible="False"></asp:Label></TD><TD id="TD1" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD6" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="qty_mix" runat="server" CssClass="inpTextDisabled" Width="80px" __designer:wfdid="w205" Visible="False" Enabled="False"></asp:TextBox> <asp:DropDownList id="unitoid_mix" runat="server" CssClass="inpTextDisabled" Width="105px" __designer:wfdid="w206" Visible="False" Enabled="False"></asp:DropDownList></TD></TR><TR><TD id="TD11" class="Label" align=left colSpan=6 runat="server" Visible="false"><ajaxToolkit:FilteredTextBoxExtender id="ftbQtyMix" runat="server" __designer:wfdid="w207" TargetControlID="qty_mix" ValidChars="1234567890,.">
            </ajaxToolkit:FilteredTextBoxExtender> <asp:Label id="Label18" runat="server" Text="Request Qty" __designer:wfdid="w228"></asp:Label> <asp:TextBox id="matreqqty" runat="server" CssClass="inpTextDisabled" Width="80px" __designer:wfdid="w229" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="HEIGHT: 5px" id="TD10" class="Label" align=left colSpan=6 runat="server" Visible="false"><asp:TextBox id="matreqno" runat="server" CssClass="inpTextDisabled" Width="160px" __designer:wfdid="w217" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchMatReq" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w1"></asp:ImageButton> <asp:ImageButton id="btnClearMatReq" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w2"></asp:ImageButton>&nbsp;<asp:Label id="Label17" runat="server" Text="Request No." __designer:wfdid="w3"></asp:Label> <asp:Label id="Label22" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w4"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label25" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Material Usage Non KIK Detail :" __designer:wfdid="w208" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="i_u2" runat="server" Text="New Detail" __designer:wfdid="w209" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="matusagedtlseq" runat="server" __designer:wfdid="w210" Visible="False">1</asp:Label> <asp:Label id="matusagedtloid" runat="server" __designer:wfdid="w211"></asp:Label> <asp:Label id="matreqmstoid" runat="server" __designer:wfdid="w212" Visible="False"></asp:Label> </TD><TD class="Label" align=left><asp:Label id="matreqdate" runat="server" __designer:wfdid="w213" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="matreqdtloid" runat="server" __designer:wfdid="w214" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label12" runat="server" Text="WareHouse" __designer:wfdid="w5"></asp:Label>&nbsp; </TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="matusagewhoid" runat="server" CssClass="inpText" Width="205px" __designer:wfdid="w221"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label11" runat="server" Text="Material" __designer:wfdid="w6"></asp:Label> <asp:Label id="Label23" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w225"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="matusagereflongdesc" runat="server" CssClass="inpTextDisabled" Width="180px" __designer:wfdid="w7" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchMat" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w8"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label16" runat="server" Text="Usage Qty" __designer:wfdid="w232"></asp:Label> <asp:Label id="Label24" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w233"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="matusageqty" runat="server" CssClass="inpText" Width="80px" __designer:wfdid="w234"></asp:TextBox> <asp:DropDownList id="matusageunitoid" runat="server" CssClass="inpTextDisabled" Width="115px" __designer:wfdid="w235" Enabled="False"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label20" runat="server" Text="Stock Qty" __designer:wfdid="w230"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="stockqty" runat="server" CssClass="inpTextDisabled" Width="80px" __designer:wfdid="w231" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label13" runat="server" Text="Detail Note" __designer:wfdid="w236"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="matusagedtlnote" runat="server" CssClass="inpText" Width="200px" __designer:wfdid="w237" MaxLength="100"></asp:TextBox></TD><TD id="TD7" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label26" runat="server" Text="Type" __designer:wfdid="w222" Visible="False"></asp:Label></TD><TD id="TD9" class="Label" align=center runat="server" Visible="false"></TD><TD id="TD8" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="matusagereftype" runat="server" CssClass="inpText" __designer:wfdid="w223" Visible="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="matusagerefoid" runat="server" __designer:wfdid="w238" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="matusagerefcode" runat="server" __designer:wfdid="w239" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="ftbQty" runat="server" __designer:wfdid="w240" TargetControlID="matusageqty" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w241"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w242"></asp:ImageButton> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:GridView id="gvDtl" runat="server" ForeColor="#333333" Width="98%" __designer:wfdid="w243" AutoGenerateColumns="False" CellPadding="4" GridLines="None" DataKeyNames="matusagedtlseq">
<RowStyle BackColor="#EFF3FB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="matreqno" HeaderText="Request No." Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matreqdate" HeaderText="Request Date" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matusagewh" HeaderText="Warehouse">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matusagereftype" HeaderText="Type" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matusagerefcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matusagerefoldcode" HeaderText="Old Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matusagereflongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matusageqty" HeaderText="Usage Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matusageunit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matusagedtlnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=6></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w244"></asp:Label>&nbsp;On <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w245"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w246"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w247"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w248" AlternateText="Save"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancel" onclick="btnCancel_Click" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w249" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w250" AlternateText="Delete"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnposting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w251"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w252" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div3" class="progressBackgroundFilter"></DIV><DIV id="Div4" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w253"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</ContentTemplate>
</asp:UpdatePanel> 
</ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Material Usage Non KIK :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upListMatReq" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlListMatReq" runat="server" CssClass="modalBox" Width="750px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 20px" class="Label" align=center colSpan=3><asp:Label id="lblListMatReq" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Material Request Non KIK"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMatReq" runat="server" Width="100%" DefaultButton="btnFindListMatReq"><asp:Label id="lblFilterListMatReq" runat="server" Text="Filter :"></asp:Label> <asp:DropDownList id="FilterDDLListMatReq" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="reqm.matreqno">Request No.</asp:ListItem>
<asp:ListItem Value="g.gendesc">Warehouse</asp:ListItem>
<asp:ListItem Value="reqm.matreqmstnote">Req. Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMatReq" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListMatReq" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMatReq" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMatReq" runat="server" BorderColor="Black" ForeColor="#333333" Width="99%" AutoGenerateColumns="False" CellPadding="4" GridLines="None" DataKeyNames="matreqmstoid,matreqdate,matreqno,matreqwhoid" PageSize="5" BorderStyle="None" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="matreqno" HeaderText="Request No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matreqdate" HeaderText="Request Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matreqwh" HeaderText="Warehouse">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matreqmstnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                                        <asp:Label ID="lblInfoOnListGV" runat="server" CssClass="Important" Text="There is no data !!"></asp:Label>
                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbCloseListMatReq" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMatReq" runat="server" TargetControlID="btnHideListMatReq" PopupDragHandleControlID="lblListMatReq" PopupControlID="pnlListMatReq" Drag="True" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMatReq" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upListMat" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListMat" runat="server" CssClass="modalBox" Width="900px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Medium" Font-Bold="True" Font-Underline="False">List Of Material</asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMat" runat="server" Width="100%" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Enabled="False" Value="m.itemcode">Log/Pallet No.</asp:ListItem>
<asp:ListItem Value="matreqrefcode">Code</asp:ListItem>
<asp:ListItem Value="itemoldcode">Old Code</asp:ListItem>
<asp:ListItem Value="matreqreflongdesc">Description</asp:ListItem>
<asp:ListItem Value="matrequnit">Unit</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterTextListMat" runat="server" CssClass="inpText" Width="200px"></asp:TextBox> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNone" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewChecked" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:UpdateProgress id="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat"><ProgressTemplate>
<TABLE style="WIDTH: 200px"><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple">Load Data, Please Wait ...</TD></TR><TR><TD><asp:Image id="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif"></asp:Image></TD></TR></TABLE>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD class="Label" align=left colSpan=3>Show <asp:TextBox id="tbData" runat="server" CssClass="inpText" Width="25px"></asp:TextBox> Data Per Page <asp:LinkButton id="lbShowData" runat="server">>> View</asp:LinkButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat" runat="server" ForeColor="#333333" Width="99%" PageSize="5" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbSelectPR" runat="server" __designer:wfdid="w1" ToolTip='<%# eval("matreqrefoid") %>' Checked='<%# eval("CheckValue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="matreqrefcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemoldcode" HeaderText="Old Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matreqreflongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stockqty" HeaderText="Stock Qty">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Usage Qty"><EditItemTemplate>
<asp:TextBox id="TextBox1" runat="server" Text='<%# Bind("registerqty") %>'></asp:TextBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:TextBox id="tbRegQty" runat="server" CssClass="inpText" Text='<%# eval("matusageqty") %>' Width="75px" MaxLength="10" Enabled='<%# eval("enableqty") %>'></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftbeQty" runat="server" TargetControlID="tbRegQty" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="matrequnit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Note"><ItemTemplate>
<asp:TextBox id="TextBox1" runat="server" CssClass="inpText" Text='<%# eval("matusagedtlnote") %>' Width="150px" MaxLength="100"></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
&nbsp;
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListMat" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlListMat" PopupDragHandleControlID="lblTitleListMat"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upListMatMix" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlListMatMix" runat="server" CssClass="modalBox" Visible="False"
                Width="750px">
                <table style="width: 100%">
                    <tbody>
                        <tr>
                            <td align="center" class="Label" colspan="3" style="height: 20px">
                                <asp:Label ID="lblListMatMix" runat="server" Font-Bold="True" Font-Size="Medium"
                                    Text="List of Material Mixing"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="center" class="Label" colspan="3">
                                <asp:Panel ID="pnlFindListMatMix" runat="server" DefaultButton="btnFindListMatMix"
                                    Width="100%">
                                    <asp:Label ID="lblFilterListMatMix" runat="server" Text="Filter :"></asp:Label>
                                    <asp:DropDownList ID="FilterDDLListMatMix" runat="server" CssClass="inpText" Width="100px">
                                        <asp:ListItem Value="matcode_mix">Code</asp:ListItem>
                                        <asp:ListItem Value="matlongdesc_mix">Description</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:TextBox ID="FilterTextListMatMix" runat="server" CssClass="inpText" Width="150px"></asp:TextBox>
                                    <asp:ImageButton ID="btnFindListMatMix" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/btnfind.bmp" />
                                    <asp:ImageButton ID="btnAllListMatMix" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewall.png" /></asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="Label" colspan="3">
                                <asp:GridView ID="gvListMatMix" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                    BorderColor="Black" BorderStyle="None" CellPadding="4" DataKeyNames="matoid_mix,matlongdesc_mix,unitoid_mix"
                                    ForeColor="#333333" GridLines="None" PageSize="5" Width="99%">
                                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="True">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:CommandField>
                                        <asp:BoundField DataField="matcode_mix" HeaderText="Code">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="matlongdesc_mix" HeaderText="Description">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="matunit_mix" HeaderText="Unit">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                    </Columns>
                                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle CssClass="gvpopup" Font-Bold="True" ForeColor="White" HorizontalAlign="Right" />
                                    <EmptyDataTemplate>
                                        <asp:Label ID="lblInfoOnListGV" runat="server" CssClass="Important" Text="There is no data !!"></asp:Label>
                                    </EmptyDataTemplate>
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3">
                                <asp:LinkButton ID="lbCloseListMatMix" runat="server">[ Cancel & Close ]</asp:LinkButton></td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpeListMatMix" runat="server" BackgroundCssClass="modalBackground"
                Drag="True" PopupControlID="pnlListMatMix" PopupDragHandleControlID="lblListMatMix"
                TargetControlID="btnHideListMatMix">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="btnHideListMatMix" runat="server" Visible="False" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True" Drag="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>