Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_MaterialRequestNonKIKCancellation
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitDDL()
        ' Init DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)
    End Sub

	Private Sub BindListMatReq()
		sSql = "SELECT req.matreqmstoid AS reqmstoid ,req.matreqno AS reqno,CONVERT(VARCHAR(10), req.matreqdate, 101) AS reqdate, deptname, gendesc AS matreqwh,req.matreqmstnote AS reqmstnote FROM QL_trnmatreqmst req INNER JOIN QL_mstdept de ON de.cmpcode=req.cmpcode AND de.deptoid=req.deptoid INNER JOIN QL_mstgen g ON genoid=req.matreqwhoid WHERE req.cmpcode='" & DDLBusUnit.SelectedValue & "' AND req.matreqmststatus='Post' AND  " & FilterDDLListMatReq.SelectedValue & " LIKE '%" & Tchar(FilterTextListMatReq.Text) & "%' AND req.matreqmstoid NOT IN (SELECT matreqmstoid FROM QL_trnmatusagedtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "') ORDER BY matreqmstoid"
		FillGV(gvListMatReq, sSql, "QL_trnreqmst")
	End Sub

    Private Sub GenerateDetailData()
		sSql = "SELECT req.matreqdtlseq,req.matreqreftype,req.matreqrefoid,(SELECT m.itemcode FROM QL_mstitem m WHERE m.itemoid=req.matreqrefoid) AS matreqrefcode, (SELECT m.itemLongDescription FROM QL_mstitem m WHERE itemoid=req.matreqrefoid) AS matreqreflongdesc,req.matreqqty,req.matrequnitoid, gendesc AS matrequnit,req.matreqdtlnote AS matreqdtlnote FROM QL_trnmatreqdtl req INNER JOIN QL_mstgen ON genoid=matrequnitoid WHERE req.cmpcode='" & DDLBusUnit.SelectedValue & "' AND matreqmstoid=" & matreqmstoid.Text & " ORDER BY matreqdtlseq"
		FillGV(gvDtl, sSql, "QL_trnmatreqdtl")
	End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnMatRequestCancel.aspx")
        End If
        If checkPagePermission("~\Transaction\trnMatRequestCancel.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Material Request Non KIK Cancellation"
        btnProcess.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to CANCEL this data?');")
        If Not Page.IsPostBack Then
            upduser.Text = Session("UserID") : updtime.Text = GetServerTime().ToString
            InitDDL()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("Success") Is Nothing And Session("Success") <> "" Then
            Response.Redirect("~\Transaction\trnMatRequestCancel.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnSearchMatReq_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMatReq.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        FilterDDLListMatReq.SelectedIndex = -1
        FilterTextListMatReq.Text = ""
        gvListMatReq.SelectedIndex = -1
        BindListMatReq()
        cProc.SetModalPopUpExtender(btnHideListMatReq, pnlListMatReq, mpeListMatReq, True)
    End Sub

    Protected Sub btnFindListMatReq_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMatReq.Click
        BindListMatReq()
        mpeListMatReq.Show()
    End Sub

    Protected Sub btnAllListMatReq_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMatReq.Click
        FilterDDLListMatReq.SelectedIndex = -1 : FilterTextListMatReq.Text = "" : gvListMatReq.SelectedIndex = -1
        BindListMatReq()
        mpeListMatReq.Show()
    End Sub

    Protected Sub gvListMatReq_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMatReq.PageIndexChanging
        gvListMatReq.PageIndex = e.NewPageIndex
        BindListMatReq()
        mpeListMatReq.Show()
    End Sub

    Protected Sub gvListMatReq_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListMatReq.SelectedIndexChanged
        matreqmstoid.Text = gvListMatReq.SelectedDataKey.Item("reqmstoid").ToString
        matreqno.Text = gvListMatReq.SelectedDataKey.Item("reqno").ToString
        matreqmstnote.Text = gvListMatReq.SelectedDataKey.Item("reqmstnote").ToString
        cProc.SetModalPopUpExtender(btnHideListMatReq, pnlListMatReq, mpeListMatReq, False)
        DDLBusUnit.Enabled = False : DDLBusUnit.CssClass = "inpTextDisabled"
        GenerateDetailData()
    End Sub

    Protected Sub lbCloseListMatReq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMatReq.Click
        cProc.SetModalPopUpExtender(btnHideListMatReq, pnlListMatReq, mpeListMatReq, False)
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnMatRequestCancel.aspx?awal=true")
    End Sub

    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnProcess.Click
        If matreqmstoid.Text = "" Then
            showMessage("Please select Request No. first!", 2)
            Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
			sSql = "UPDATE QL_trnmatreqdtl SET matreqdtlstatus='Cancel', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matreqmstoid=" & matreqmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
			sSql = "UPDATE QL_trnmatreqmst SET matreqmststatus='Cancel', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matreqmstoid=" & matreqmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Session("Success") = "Request No. : " & matreqno.Text & " have been cancelled successfully."
        showMessage(Session("Success"), 3)
    End Sub
#End Region

End Class