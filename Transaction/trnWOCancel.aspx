<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnWOCancel.aspx.vb" Inherits="Transaction_WorkOrderCancellation" title="" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Job Costing MO Cancellation" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Job Costing MO Cancellation :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                <asp:UpdatePanel id="upWOCancel" runat="server">
                    <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Label id="Label4" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Job Costing MO Header" __designer:wfdid="w27"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 15%" class="Label"></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label"><asp:Label id="womstoid" runat="server" __designer:wfdid="w26" Visible="False"></asp:Label></TD><TD style="WIDTH: 15%" class="Label"></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label"></TD></TR><TR><TD class="Label"><asp:Label id="Label8" runat="server" Text="Division" __designer:wfdid="w27" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label"><asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpText" Width="205px" __designer:wfdid="w28" Visible="False" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label"></TD><TD class="Label" align=center></TD><TD class="Label"></TD></TR><TR><TD class="Label"><asp:Label id="Label2" runat="server" Text="Job No." __designer:wfdid="w31"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label"><asp:TextBox id="wono" runat="server" CssClass="inpTextDisabled" Width="160px" __designer:wfdid="w32" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnFindWO" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w33"></asp:ImageButton> <asp:ImageButton id="btnClearWO" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w34"></asp:ImageButton></TD><TD class="Label"><asp:Label id="Label5" runat="server" Text="Job Date" __designer:wfdid="w35"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label"><asp:TextBox id="wodate" runat="server" CssClass="inpTextDisabled" Width="75px" __designer:wfdid="w36" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label"><asp:Label id="Label3" runat="server" Text="Doc. Ref. No." __designer:wfdid="w37"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label"><asp:TextBox id="wodocrefno" runat="server" CssClass="inpTextDisabled" Width="160px" __designer:wfdid="w38" Enabled="False"></asp:TextBox></TD><TD class="Label"><asp:Label id="Label7" runat="server" Text="Status" __designer:wfdid="w39"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label"><asp:TextBox id="womststatus" runat="server" CssClass="inpTextDisabled" Width="75px" __designer:wfdid="w40" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label"><asp:Label id="Label6" runat="server" Text="Note" __designer:wfdid="w41"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label"><asp:TextBox id="womstnote" runat="server" CssClass="inpText" Width="200px" __designer:wfdid="w42" MaxLength="100"></asp:TextBox></TD><TD class="Label"></TD><TD class="Label" align=center></TD><TD class="Label"></TD></TR></TBODY></TABLE></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Label id="Label9" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Job Costing MO Detail Finish Good" __designer:wfdid="w27"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Panel id="pnlGVDtl1" runat="server" __designer:dtid="562949953421585" CssClass="inpText" Width="100%" __designer:wfdid="w55" Height="150px" ScrollBars="Vertical"><asp:GridView id="GVDtl1" runat="server" ForeColor="#333333" __designer:dtid="562949953421586" Width="98%" CellPadding="4" AutoGenerateColumns="False" __designer:wfdid="w276" PageSize="5" GridLines="None">
<RowStyle BackColor="#E3EAEB" __designer:dtid="562949953421587"></RowStyle>
<Columns __designer:dtid="562949953421588">
<asp:BoundField DataField="wodtl1seq" HeaderText="No." __designer:dtid="562949953421592">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" __designer:dtid="562949953421593"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" __designer:dtid="562949953421594"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="soitemno" HeaderText="SO No." __designer:dtid="562949953421595">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" __designer:dtid="562949953421596"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" __designer:dtid="562949953421597"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Code" __designer:dtid="562949953421598">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" __designer:dtid="562949953421599"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" __designer:dtid="562949953421600"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdesc" HeaderText="Description" __designer:dtid="562949953421601">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" __designer:dtid="562949953421602"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" __designer:dtid="562949953421603"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl1qty" HeaderText="Qty" __designer:dtid="562949953421604">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" __designer:dtid="562949953421605"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" __designer:dtid="562949953421606"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl1unit" HeaderText="Unit" __designer:dtid="562949953421607">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" __designer:dtid="562949953421608"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" __designer:dtid="562949953421609"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomdesc" HeaderText="BOM" __designer:dtid="562949953421610">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" __designer:dtid="562949953421611"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" __designer:dtid="562949953421612"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl1note" HeaderText="Detail Note" __designer:dtid="562949953421613">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" __designer:dtid="562949953421614"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" __designer:dtid="562949953421615"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" __designer:dtid="562949953421619"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#666666" ForeColor="White" __designer:dtid="562949953421620"></PagerStyle>

<SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" __designer:dtid="562949953421621"></SelectedRowStyle>

<HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" __designer:dtid="562949953421622"></HeaderStyle>

<EditRowStyle BackColor="#7C6F57" __designer:dtid="562949953421623"></EditRowStyle>

<AlternatingRowStyle BackColor="White" __designer:dtid="562949953421624"></AlternatingRowStyle>
</asp:GridView></asp:Panel></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Label id="Label10" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Job Costing MO Detail Process" __designer:wfdid="w27"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Panel id="Panel2" runat="server" __designer:dtid="562949953421713" CssClass="inpText" Width="100%" __designer:wfdid="w57" Height="150px" ScrollBars="Vertical"><asp:GridView id="GVDtl2" runat="server" ForeColor="#333333" __designer:dtid="562949953421714" Width="98%" __designer:wfdid="w319" GridLines="None" PageSize="5" AutoGenerateColumns="False" CellPadding="4">
<RowStyle BackColor="#E3EAEB" __designer:dtid="562949953421715"></RowStyle>
<Columns __designer:dtid="562949953421716">
<asp:BoundField DataField="wodtl2seq" HeaderText="No." __designer:dtid="562949953421720">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" __designer:dtid="562949953421721"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" __designer:dtid="562949953421722"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl1desc" HeaderText="Det. Finish Good" __designer:dtid="562949953421723">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" __designer:dtid="562949953421724"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" __designer:dtid="562949953421725"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="deptname" HeaderText="From Department" __designer:dtid="562949953421726">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" __designer:dtid="562949953421727"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" __designer:dtid="562949953421728"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl2desc" HeaderText="To Department" __designer:dtid="562949953421729">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" __designer:dtid="562949953421730"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" __designer:dtid="562949953421731"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl2type" HeaderText="Output Type" __designer:dtid="562949953421732">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" __designer:dtid="562949953421733"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" __designer:dtid="562949953421734"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl2reflongdesc" HeaderText="Output" __designer:dtid="562949953421735">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" __designer:dtid="562949953421736"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" __designer:dtid="562949953421737"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl2qty" HeaderText="Quantity" __designer:dtid="562949953421738">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" __designer:dtid="562949953421739"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" __designer:dtid="562949953421740"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl2unit" HeaderText="Unit" __designer:dtid="562949953421741">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" __designer:dtid="562949953421742"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" __designer:dtid="562949953421743"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl2note" HeaderText="Note" __designer:dtid="562949953421744">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" __designer:dtid="562949953421745"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" __designer:dtid="562949953421746"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" __designer:dtid="562949953421750"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#666666" ForeColor="White" __designer:dtid="562949953421751"></PagerStyle>

<SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" __designer:dtid="562949953421752"></SelectedRowStyle>

<HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" __designer:dtid="562949953421753"></HeaderStyle>

<EditRowStyle BackColor="#7C6F57" __designer:dtid="562949953421754"></EditRowStyle>

<AlternatingRowStyle BackColor="White" __designer:dtid="562949953421755"></AlternatingRowStyle>
</asp:GridView></asp:Panel></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Label id="Label11" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Job Costing MO Detail Material" __designer:wfdid="w27"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Panel id="pnlGVDtl2" runat="server" __designer:dtid="562949953421837" CssClass="inpText" Width="100%" __designer:wfdid="w59" Height="150px" ScrollBars="Vertical"><asp:GridView id="GVDtl3" runat="server" ForeColor="#333333" __designer:dtid="562949953421838" Width="98%" CellPadding="4" AutoGenerateColumns="False" __designer:wfdid="w353" PageSize="5" GridLines="None">
<RowStyle BackColor="#E3EAEB" __designer:dtid="562949953421839"></RowStyle>
<Columns __designer:dtid="562949953421840">
<asp:BoundField DataField="wodtl3seq" HeaderText="No." __designer:dtid="562949953421844">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" __designer:dtid="562949953421845"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" __designer:dtid="562949953421846"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl2desc" HeaderText="Det. Process" __designer:dtid="562949953421847">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" __designer:dtid="562949953421848"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" __designer:dtid="562949953421849"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl3reftype" HeaderText="Type" __designer:dtid="562949953421850">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" __designer:dtid="562949953421851"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" __designer:dtid="562949953421852"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl3refcode" HeaderText="Mat. Code" __designer:dtid="562949953421853">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" __designer:dtid="562949953421854"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" __designer:dtid="562949953421855"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl3reflongdesc" HeaderText="Mat. Description" __designer:dtid="562949953421856">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" __designer:dtid="562949953421857"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" __designer:dtid="562949953421858"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl3qty" HeaderText="Qty" __designer:dtid="562949953421859">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" __designer:dtid="562949953421860"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" __designer:dtid="562949953421861"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl3unit" HeaderText="Unit" __designer:dtid="562949953421862">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" __designer:dtid="562949953421863"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" __designer:dtid="562949953421864"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl3note" HeaderText="Detail Note" __designer:dtid="562949953421865">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" __designer:dtid="562949953421866"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" __designer:dtid="562949953421867"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" __designer:dtid="562949953421871"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#666666" ForeColor="White" __designer:dtid="562949953421872"></PagerStyle>

<SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" __designer:dtid="562949953421873"></SelectedRowStyle>

<HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" __designer:dtid="562949953421874"></HeaderStyle>

<EditRowStyle BackColor="#7C6F57" __designer:dtid="562949953421875"></EditRowStyle>

<AlternatingRowStyle BackColor="White" __designer:dtid="562949953421876"></AlternatingRowStyle>
</asp:GridView></asp:Panel></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=3></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left colSpan=3>Processed By <asp:Label id="upduser" runat="server" __designer:wfdid="w45"></asp:Label>&nbsp;On <asp:Label id="updtime" runat="server" __designer:wfdid="w46"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:ImageButton id="btnProcess" runat="server" ImageUrl="~/Images/process.png" ImageAlign="AbsMiddle" __designer:wfdid="w47"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w48"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w49" AssociatedUpdatePanelID="upWOCancel"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w1"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upListWO" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListWO" runat="server" CssClass="modalBox" Width="750px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListWO" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Job Costing MO"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListWO" runat="server" Width="100%" DefaultButton="btnFindListWO"><asp:Label id="Label24" runat="server" Text="Filter :"></asp:Label> <asp:DropDownList id="FilterDDLListWO" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="wom.wono">Job No.</asp:ListItem>
<asp:ListItem Value="wom.wodocrefno">Doc. Ref. No.</asp:ListItem>
<asp:ListItem Value="wom.womststatus">Status</asp:ListItem>
<asp:ListItem Value="wom.womstnote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListWO" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListWO" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListWO" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 175px; BORDER-BOTTOM-STYLE: none" id="Fieldset1"><DIV id="Div1"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="gvListWO" runat="server" BorderColor="Black" ForeColor="#333333" Width="96%" GridLines="None" AutoGenerateColumns="False" CellPadding="4" BorderStyle="None" DataKeyNames="womstoid,wono,wodate,wodocrefno,womststatus,womstnote">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="wono" HeaderText="Job No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodate" HeaderText="Job Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodocrefno" HeaderText="Doc. Ref. No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="womststatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="womstnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                            <asp:Label ID="lblInfoOnListGV" runat="server" CssClass="Important" Text="There is no data !!"></asp:Label>
                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> &nbsp;</DIV>&nbsp;</FIELDSET> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListWO" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE><ajaxToolkit:ModalPopupExtender id="mpeListWO" runat="server" PopupControlID="pnlListWO" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListWO" TargetControlID="btnHiddenListWO"></ajaxToolkit:ModalPopupExtender> </asp:Panel> &nbsp; <asp:Button id="btnHiddenListWO" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

