<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnFGR.aspx.vb" Inherits="Transaction_trnFGR" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
  <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label4" runat="server" Font-Bold="False" Text=".: FG Received Non KIK " CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
            <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%" >
            
        <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
            <ContentTemplate>
                            
                                <ContentTemplate></ContentTemplate>
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
                <asp:Panel ID="Panel1" runat="server" >
                    <table>
                        <tr>
                            <td align="left" class="Label">
                                Filter :</td><TD align=center colSpan=1>:</TD>
                            <td align="left" colspan="3">
                                <asp:DropDownList ID="FilterDDL" runat="server" CssClass="inpText"
                                    Width="80px">
                                    <asp:ListItem Value="prodresno">FGR No.</asp:ListItem>
                                    <asp:ListItem Value="personname">PIC</asp:ListItem>
                                </asp:DropDownList>
                                &nbsp;<asp:TextBox ID="FilterText" runat="server" CssClass="inpText" MaxLength="30"
                                    Width="136px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" class="Label">
                                <asp:CheckBox ID="cbPeriode" runat="server" Text="Period" AutoPostBack="True" /></td><TD class="Label" align=center colSpan=1>:</TD>
                            <td align="left" class="Label" colspan="3">
                                <asp:TextBox ID="FilterPeriod1" runat="server" CssClass="inpText" Width="75px"></asp:TextBox>
                                &nbsp;<asp:ImageButton ID="imbPeriod1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/oCalendar.gif" />
                                &nbsp;<asp:Label ID="lblTo" runat="server" Text="to"></asp:Label>
                                &nbsp;<asp:TextBox ID="FilterPeriod2" runat="server" CssClass="inpText" Width="75px"></asp:TextBox>
                                <asp:ImageButton ID="imbPeriod2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/oCalendar.gif" />
                                &nbsp;<asp:Label ID="lblFormat" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label>
                                <ajaxToolkit:CalendarExtender ID="cePer1" runat="server" Enabled="True" Format="MM/dd/yyyy"
                                    PopupButtonID="imbPeriod1" TargetControlID="FilterPeriod1">
                                </ajaxToolkit:CalendarExtender>
                                <ajaxToolkit:CalendarExtender ID="cePer2" runat="server" Enabled="True" Format="MM/dd/yyyy"
                                    PopupButtonID="imbPeriod2" TargetControlID="FilterPeriod2">
                                </ajaxToolkit:CalendarExtender>
                                <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender3" runat="server" CultureAMPMPlaceholder=""
                                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                                    CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                                    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="FilterPeriod1"
                                    UserDateFormat="MonthDayYear">
                                </ajaxToolkit:MaskedEditExtender>
                                <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender4" runat="server" CultureAMPMPlaceholder=""
                                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                                    CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                                    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="FilterPeriod2"
                                    UserDateFormat="MonthDayYear">
                                </ajaxToolkit:MaskedEditExtender>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="Label">
                                <asp:Label ID="Label1" runat="server" Text="Status"></asp:Label></td><TD align=center colSpan=1>:</TD>
                            <td align="left" colspan="3">
                                <asp:DropDownList ID="DDLStatus" runat="server" CssClass="inpText" Width="80px">
                                    <asp:ListItem>All</asp:ListItem>
                                    <asp:ListItem Value="In Process">In Process</asp:ListItem>
                                    <asp:ListItem>Post</asp:ListItem>
                                </asp:DropDownList>
                                &nbsp;
                                <asp:ImageButton ID="btnSearch" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/btnfind.bmp" />&nbsp;<asp:ImageButton
                                    ID="btnAll" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewall.png" />&nbsp;<asp:ImageButton
                                        ID="btnPrint" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/print.png" /></td>
                        </tr><TR><TD class="Label" align=left></TD><TD align=center colSpan=1></TD><TD align=left colSpan=3></TD></TR>
                    </table><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TR><TD align=left>
                                <asp:GridView ID="gvListOfTemp" runat="server" AllowPaging="True" AllowSorting="True"
                                    AutoGenerateColumns="False" CellPadding="4" DataKeyNames="prodresmstoid" ForeColor="#333333"
                                    GridLines="None" PageSize="8" Width="100%">
                                    <PagerSettings FirstPageText="First" LastPageText="Last" />
                                    <RowStyle BackColor="#EFF3FB" Font-Size="X-Small" />
                                    <EmptyDataRowStyle BackColor="#FFFFC0" />
                                    <Columns>
                                        <asp:HyperLinkField DataNavigateUrlFields="prodresmstoid" DataNavigateUrlFormatString="~\Transaction\trnFGR.aspx?oid={0}"
                                            DataTextField="prodresmstoid" HeaderText="Draft No." SortExpression="prodresmstoid">
                                            <HeaderStyle CssClass="gvhdr" Font-Bold="True" HorizontalAlign="Center" Width="70px" />
                                            <ItemStyle HorizontalAlign="Center" Width="70px" />
                                        </asp:HyperLinkField>
                                        <asp:BoundField DataField="prodresno" HeaderText="FGR No." SortExpression="prodresno">
                                            <HeaderStyle CssClass="gvhdr" Font-Bold="True" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="prodresdate" HeaderText="FGR Date" SortExpression="prodresdate">
                                            <HeaderStyle CssClass="gvhdr" Font-Bold="True" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="suppname" HeaderText="PIC" SortExpression="suppname">
                                            <HeaderStyle CssClass="gvhdr" Font-Bold="True" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="prodresmststatus" HeaderText="Status" SortExpression="prodresmststatus">
                                            <HeaderStyle CssClass="gvhdr" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbGVMst" runat="server" Checked='<%# eval("checkvalue") %>' />
                                                <asp:Label ID="lblOidGVMst" runat="server" Text='<%# eval("prodresmstoid") %>' Visible="False"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="gvhdr" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle CssClass="gvhdr" Font-Bold="True" ForeColor="White" HorizontalAlign="Right" />
                                    <EmptyDataTemplate>
                                        <asp:Label ID="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label>
                                    </EmptyDataTemplate>
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White" />
                                    <EditRowStyle BackColor="#2461BF" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
                                <asp:Label ID="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data"></asp:Label></TD></TR></TABLE>
                </asp:Panel></contenttemplate>
                </asp:UpdatePanel>
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp;&nbsp;<asp:BoundField DataField="iomstoid">
                    <itemstyle forecolor="White" width="1px" />
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                <asp:BoundField DataField="iomstoid">
                    <itemstyle forecolor="White" width="1px" />
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                <asp:BoundField DataField="iomstoid">
                    <itemstyle forecolor="White" width="1px" />
                                <triggers></triggers>
                    <asp:POSTBACKTRIGGER ControlID="tbldata">
                           
            </ContentTemplate>
           <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of FG Received Non KIK :.</span></strong>
                        </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
           <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">Form FG Received Non KIK :.</span></strong>
                        </HeaderTemplate>
            <ContentTemplate>
                
                <asp:UpdatePanel id="UpdatePanel7" runat="server"><ContentTemplate>
<asp:MultiView id="MultiView1" runat="server" ActiveViewIndex="0"><asp:View id="View1" runat="server"><DIV style="VERTICAL-ALIGN: top; TEXT-ALIGN: left"><TABLE width="97%"><TBODY><TR><TD style="FONT-WEIGHT: bold; COLOR: black; HEIGHT: 15px; TEXT-DECORATION: underline" align=left colSpan=4>FGR&nbsp;Header :</TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 68px" align=left>FGR&nbsp;No.</TD><TD align=left><asp:TextBox id="bpbjno" runat="server" CssClass="inpTextDisabled" Width="127px" MaxLength="20" AutoPostBack="True" ReadOnly="True"></asp:TextBox> <asp:Label id="i_u" runat="server" ForeColor="Red" Text="N E W" Visible="False"></asp:Label> <asp:Label id="oid" runat="server" Font-Size="X-Small" Font-Bold="True" Visible="False"></asp:Label></TD><TD align=left>FGR&nbsp;Date</TD><TD style="FONT-SIZE: 8pt" align=left><asp:TextBox id="pritemexpdate" runat="server" CssClass="inpText" Width="60px" ToolTip="MM/dd/yyyy" __designer:wfdid="w20"></asp:TextBox> <asp:ImageButton id="imbExpDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w21"></asp:ImageButton> <asp:Label id="Label19" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w22"></asp:Label><ajaxToolkit:CalendarExtender id="ceDate2" runat="server" __designer:wfdid="w23" TargetControlID="pritemexpdate" PopupButtonID="imbExpDate" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeDate2" runat="server" __designer:wfdid="w24" TargetControlID="pritemexpdate" MaskType="Date" Mask="99/99/9999">
            </ajaxToolkit:MaskedEditExtender></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WHITE-SPACE: nowrap" align=left>PIC (Receiver)<asp:Label id="Label5" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD align=left><asp:DropDownList id="userid" runat="server" Font-Size="X-Small" CssClass="inpText" Width="254px" __designer:wfdid="w1"></asp:DropDownList></TD><TD align=left>Status</TD><TD align=left><asp:TextBox id="bpbjstatus" runat="server" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 68px" align=left>FGR&nbsp;Note</TD><TD align=left><asp:TextBox id="bpbjnote" runat="server" CssClass="inpText" Width="250px" MaxLength="50" TextMode="MultiLine"></asp:TextBox><BR /><asp:Label id="Label255" runat="server" CssClass="Important" Text="* 50 characters"></asp:Label></TD><TD align=left></TD><TD align=left></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 68px" align=left><asp:Label id="Label3" runat="server" Text="Type" Visible="False"></asp:Label></TD><TD align=left><asp:DropDownList id="bpbjtype" runat="server" CssClass="inpText" Width="97px" Visible="False"><asp:ListItem>Finish Good</asp:ListItem>
<asp:ListItem>WIP</asp:ListItem>
</asp:DropDownList></TD><TD align=left></TD><TD align=left></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=4></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="FONT-WEIGHT: bold; COLOR: black; HEIGHT: 15px; TEXT-DECORATION: underline" align=left colSpan=4>FGR&nbsp;Detail :</TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 68px; HEIGHT: 21px" align=left>Location</TD><TD style="HEIGHT: 21px" align=left colSpan=3><asp:DropDownList id="mtrloc" runat="server" Font-Size="X-Small" CssClass="inpText" Width="150px"></asp:DropDownList> <asp:Label id="seq" runat="server" Font-Size="XX-Small" Visible="False"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 68px" align=left>Item</TD><TD align=left colSpan=3><asp:TextBox id="itemlocaldesc" runat="server" CssClass="inpText" Width="519px"></asp:TextBox>&nbsp;<asp:ImageButton id="imbSearchItem" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Width="16px" Height="16px"></asp:ImageButton> <asp:ImageButton id="imbEraseItem" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton>&nbsp;<asp:Label id="itemoid" runat="server" Visible="False"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 68px" align=left>Qty</TD><TD align=left><asp:TextBox id="qty1" runat="server" CssClass="inpText" Width="95px" MaxLength="20"></asp:TextBox> <asp:DropDownList id="unit1" runat="server" CssClass="inpTextDisabled" Enabled="False"></asp:DropDownList> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender1" runat="server" TargetControlID="qty1" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender></TD><TD align=left><asp:Label id="Label6" runat="server" Text="Value" Visible="False" __designer:wfdid="w13"></asp:Label></TD><TD align=left><asp:TextBox id="value" runat="server" CssClass="inpText" __designer:wfdid="w14"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 68px" align=left>Detail Note</TD><TD align=left colSpan=3><asp:TextBox id="dtlnote" runat="server" CssClass="inpText" Width="747px" MaxLength="50" TextMode="MultiLine" Height="29px"></asp:TextBox><BR /><asp:Label id="Label2" runat="server" CssClass="Important" Text="* 50 characters"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 68px; HEIGHT: 25px" align=left></TD><TD style="HEIGHT: 25px" align=left><asp:ImageButton id="imbAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbClearDtl" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD style="HEIGHT: 25px" align=left><asp:Label id="i_u4" runat="server" ForeColor="Red" Text="New Detail" Visible="False"></asp:Label></TD><TD style="HEIGHT: 25px" align=left><asp:TextBox id="panjang" runat="server" CssClass="inpText" Width="67px" MaxLength="4" Visible="False" __designer:wfdid="w1"></asp:TextBox> <asp:TextBox id="roll" runat="server" CssClass="inpText" Width="67px" MaxLength="4" Visible="False" __designer:wfdid="w2"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=4><FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 173px; BORDER-BOTTOM-STYLE: none" id="Fieldset3"><DIV id="Div3"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="tbldtl" runat="server" ForeColor="#333333" Width="100%" __designer:wfdid="w1" PageSize="5" GridLines="None" DataKeyNames="prodresdtlseq" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#E3EAEB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="10px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="10px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="prodresdtlseq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="5px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="5px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="loc" HeaderText="Location">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlocaldesc" HeaderText="Item">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prodresqty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit1" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="value" HeaderText="Value">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prodresdtlnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteImageUrl="~/Images/del.jpeg" DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#666666" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#7C6F57"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></FIELDSET></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="HEIGHT: 15px" title="WIDTH: 30%" align=left colSpan=4>&nbsp; </TD></TR></TBODY></TABLE></DIV></asp:View></asp:MultiView>&nbsp; 
</ContentTemplate>
                    <triggers>
</triggers>
</asp:UpdatePanel>
                <br />
                <table>
                    <tr>
                        <td align="left" class="Label" style="color: #585858">
                            Created By
                            <asp:Label ID="createuser" runat="server" Font-Bold="True"></asp:Label>
                            On
                            <asp:Label ID="createtime" runat="server" Font-Bold="True"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" class="Label" style="color: #585858">
                            Last Update By
                            <asp:Label ID="upduser" runat="server" Font-Bold="True"></asp:Label>
                            On
                            <asp:Label ID="updtime" runat="server" Font-Bold="True"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:ImageButton ID="btnSave" runat="server" AlternateText="Save" ImageAlign="AbsMiddle"
                                ImageUrl="~/Images/btnsave.bmp" />
                            <asp:ImageButton ID="btnCancel" runat="server" AlternateText="Cancel" ImageAlign="AbsMiddle"
                                ImageUrl="~/Images/btncancel.bmp" />
                            <asp:ImageButton ID="btnDelete" runat="server"
                                    AlternateText="Delete" ImageAlign="AbsMiddle" ImageUrl="~/Images/btndelete.bmp" />
                            <asp:ImageButton ID="btnPost" runat="server" AlternateText="Delete" ImageAlign="AbsMiddle"
                                ImageUrl="~/Images/posting.png" />&nbsp;
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    &nbsp;<asp:UpdatePanel id="upPopUpMsg" runat="server"><contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Height="24px" ImageUrl="~/Images/error.jpg" Width="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" DropShadow="True" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    &nbsp;<asp:UpdatePanel id="upListMat" runat="server"><contenttemplate>
<asp:Panel id="pnlListMat" runat="server" CssClass="modalBox" Width="900px" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListMat" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Finish Good"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListMat" runat="server" Width="100%" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblFilterInListMat" runat="server" Text="Filter :"></asp:Label> <asp:DropDownList id="FilterDDLListMat" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="itemlongdesc">Description</asp:ListItem>
<asp:ListItem Value="itemcode">Code</asp:ListItem>
<asp:ListItem Value="matunit">Unit</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:CheckBox id="cbCat01" runat="server" Text="Category 1 :"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat01" runat="server" CssClass="inpText" Width="200px" AutoPostBack="True"></asp:DropDownList> <asp:CheckBox id="cbCat02" runat="server" Text="Category 2 :"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat02" runat="server" CssClass="inpText" Width="200px" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:CheckBox id="cbCat03" runat="server" Text="Category 3 :"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat03" runat="server" CssClass="inpText" Width="200px" AutoPostBack="True"></asp:DropDownList> <asp:CheckBox id="cbCat04" runat="server" Text="Category 4 :"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat04" runat="server" CssClass="inpText" Width="200px"></asp:DropDownList></TD></TR></TBODY></TABLE></asp:Panel></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnSelectNone" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewChecked" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:UpdateProgress id="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat"><ProgressTemplate>
<TABLE style="WIDTH: 200px"><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple">Load Data, Please Wait ...</TD></TR><TR><TD><asp:Image id="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif"></asp:Image></TD></TR></TABLE>
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR><TR><TD class="Label" align=left colSpan=3>Show <asp:TextBox id="tbData" runat="server" CssClass="inpText" Width="25px"></asp:TextBox> Data Per Page <asp:LinkButton id="lbShowData" runat="server">>> View</asp:LinkButton></TD></TR><TR><TD class="Label" align=center colSpan=3>&nbsp;<asp:GridView id="gvListMat" runat="server" ForeColor="#333333" Width="98%" PageSize="5" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbSelectMat" runat="server" __designer:wfdid="w1" ToolTip='<%# eval("itemoid") %>' Checked='<%# eval("CheckValue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Qty Received"><ItemTemplate>
<asp:TextBox id="tbMatQty" runat="server" CssClass="inpText" Text='<%# eval("fgrqty") %>' Width="50px" MaxLength="18" __designer:wfdid="w1"></asp:TextBox><BR /><ajaxToolkit:FilteredTextBoxExtender id="ftbeQty" runat="server" __designer:wfdid="w2" TargetControlID="tbMatQty" ValidChars="1234567890."></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Value"><ItemTemplate>
<asp:TextBox id="value" runat="server" CssClass="inpText" Text='<%# eval("value") %>' MaxLength="16" __designer:wfdid="w9"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="FteValue" runat="server" __designer:wfdid="w12" TargetControlID="value" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Note"><ItemTemplate>
<asp:TextBox id="tbMatNote" runat="server" CssClass="inpText" Text='<%# eval("fgrdtlnote") %>' Width="150px" MaxLength="100"></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD colSpan=3><asp:LinkButton id="lbAddListMat" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lbCloseListMat" onclick="lbCloseListMat_Click" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" PopupDragHandleControlID="lblListMat" PopupControlID="pnlListMat" BackgroundCssClass="modalBackground" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>