Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Drawing.Printing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_trnRetur
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Dim pkSize = New System.Drawing.Printing.PaperSize
    Dim PrintDoc1 As New System.Drawing.Printing.PrintDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim iCell As Integer = 7, isInit As Boolean = False
        If retType.SelectedValue.ToUpper = "INITIAL AP" Then
            iCell = 8 : isInit = True
        ElseIf retType.SelectedValue.ToUpper = "NOTA LAMA" Then
            iCell = 9 : isInit = True
        End If
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "dtloid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    If isInit Then
                                        dtView(0)("retqty") = ToDouble(GetTextBoxValue(row.Cells(6).Controls))
                                        dtView(0)("retdtlnote") = GetTextBoxValue(row.Cells(8).Controls)
                                        dtView(0)("amt") = ToDouble(GetTextBoxValue(row.Cells(9).Controls))
                                        If retType.SelectedValue.ToUpper = "NOTA LAMA" Then
                                            dtView(0)("notalama") = GetTextBoxValue(row.Cells(10).Controls)
                                        End If
                                    Else
                                        dtView(0)("retqty") = ToDouble(GetTextBoxValue(row.Cells(6).Controls))
                                        dtView(0)("retdtlnote") = GetTextBoxValue(row.Cells(8).Controls)
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl = dtView.ToTable
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim iCell As Integer = 7, isInit As Boolean = False
        If retType.SelectedValue.ToUpper = "INITIAL AP" Then
            iCell = 8 : isInit = True
        ElseIf retType.SelectedValue.ToUpper = "NOTA LAMA" Then
            iCell = 9 : isInit = True
        End If
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "dtloid=" & cbOid
                                dtView2.RowFilter = "dtloid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    If isInit Then
                                        dtView(0)("retqty") = ToDouble(GetTextBoxValue(row.Cells(6).Controls))
                                        dtView(0)("retdtlnote") = GetTextBoxValue(row.Cells(8).Controls)
                                        dtView(0)("amt") = ToDouble(GetTextBoxValue(row.Cells(9).Controls))
                                        If retType.SelectedValue.ToUpper = "NOTA LAMA" Then
                                            dtView(0)("notalama") = GetTextBoxValue(row.Cells(10).Controls)
                                        End If
                                    Else
                                        dtView(0)("retqty") = ToDouble(GetTextBoxValue(row.Cells(6).Controls))
                                        dtView(0)("retdtlnote") = GetTextBoxValue(row.Cells(8).Controls)
                                    End If
                                    If dtView2.Count > 0 Then
                                        If isInit Then
                                            dtView2(0)("retqty") = ToDouble(GetTextBoxValue(row.Cells(6).Controls))
                                            dtView2(0)("retdtlnote") = GetTextBoxValue(row.Cells(8).Controls)
                                            dtView2(0)("amt") = ToDouble(GetTextBoxValue(row.Cells(9).Controls))
                                            If retType.SelectedValue.ToUpper = "NOTA LAMA" Then
                                                dtView2(0)("notalama") = GetTextBoxValue(row.Cells(10).Controls)
                                            End If
                                        Else
                                            dtView2(0)("retqty") = ToDouble(GetTextBoxValue(row.Cells(6).Controls))
                                            dtView2(0)("retdtlnote") = GetTextBoxValue(row.Cells(8).Controls)
                                        End If
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function GetDropDownValue(ByVal cc As System.Web.UI.ControlCollection, Optional ByVal sText As String = "") As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                If sText = "" Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
                Else
                    sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedItem.Text
                End If
            End If
        Next
        Return sReturn
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If mrrawmstoid.Text = "" Then
            sError &= "- Please select MR NO. field!<BR>"
        End If
        If mrrawdtloid.Text = "" Then
            sError &= "- Please select MATERIAL field!<BR>"
        End If
        If pretrawqty.Text = "" Then
            sError &= "- Please fill QTY field!<BR>"
        Else
            If ToDouble(pretrawqty.Text) <= 0 Then
                sError &= "- QTY must be more than 0!<BR>"
            Else
                If ToDouble(pretrawqty.Text) >= 100000 Then
                    sError &= "- QTY must be less than 100,000!<BR>"
                Else
                    If ToDouble(pretrawqty.Text) > ToDouble(mrrawqty.Text) Then
                        sError &= "- QTY must be less than MR QTY!<BR>"
                    End If
                End If
            End If
        End If
        If pretrawunitoid.SelectedValue = "" Then
            sError &= "- Please select UNIT field!<BR>"
        End If
        If reasonoid.SelectedValue = "" Then
            sError &= "- Please select REASON field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If suppoid.Text = "" Then
            sError &= "- Please select SUPPLIER field!<BR>"
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill DETAIL DATA!<BR>"
        Else
            Dim objTbl As DataTable = Session("TblDtl")
            If objTbl.Rows.Count <= 0 Then
                sError &= "- Please fill DETAIL DATA!<BR>"
            Else
                If i_u.Text = "New Data" Then
                    If retType.SelectedValue = "MR" Then
                        For C1 As Int16 = 0 To objTbl.Rows.Count - 1
                            sSql = "SELECT (mrqty - ISNULL((SELECT SUM(retqty) FROM QL_trnreturdtl pretd WHERE pretd.refoidmst=" & objTbl.Rows(C1)("mrmstoid").ToString & " AND pretd.refoiddtl=mrd.mrdtloid AND pretd.retdtlres1='QL_trnmrmst' AND pretd.retdtlres2='QL_trnmrdtl'), 0)) AS mrqty FROM QL_trnmrdtl mrd WHERE mrd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrd.mrmstoid=" & objTbl.Rows(C1)("mrmstoid").ToString & " AND mrd.mrdtloid=" & objTbl.Rows(C1)("mrdtloid").ToString
                            If ToDouble(objTbl.Rows(C1)("qty").ToString) <> ToDouble(GetStrData(sSql)) Then
                                objTbl.Rows(C1)("qty") = ToDouble(GetStrData(sSql))
                            End If
                        Next
                    ElseIf retType.SelectedValue = "PI" Then
                        For C1 As Int16 = 0 To objTbl.Rows.Count - 1
                            sSql = "SELECT (trnbelidtlqty - ISNULL((SELECT SUM(retqty) FROM QL_trnreturdtl pretd WHERE pretd.refoidmst=" & objTbl.Rows(C1)("mrmstoid").ToString & " AND pretd.refoiddtl=bld.trnbelidtloid AND pretd.retdtlres1='QL_trnbelimst' AND pretd.retdtlres2='QL_trnbelidtl'), 0)) AS beliqty FROM QL_trnbelidtl bld WHERE bld.cmpcode='" & DDLBusUnit.SelectedValue & "' AND bld.trnbelimstoid=" & objTbl.Rows(C1)("mrmstoid").ToString & " AND bld.trnbelidtloid=" & objTbl.Rows(C1)("mrdtloid").ToString
                            If ToDouble(objTbl.Rows(C1)("qty").ToString) <> ToDouble(GetStrData(sSql)) Then
                                objTbl.Rows(C1)("qty") = ToDouble(GetStrData(sSql))
                            End If
                        Next
                    End If
                    objTbl.AcceptChanges()
                    For C1 As Int16 = 0 To objTbl.Rows.Count - 1
                        If ToDouble(objTbl.Rows(C1)("qty").ToString) < ToDouble(objTbl.Rows(C1)("retqty").ToString) Then
                            sError &= "- QTY for some detail data has been updated by another user. Please check that every detail MR QTY must be more or equal than RETURN QTY!<BR>"
                            Exit For
                        End If
                    Next
                    Session("TblDtl") = objTbl
                    gvListDtl.DataSource = Session("TblDtl")
                    gvListDtl.DataBind()
                End If
                For C1 As Integer = 0 To objTbl.Rows.Count - 1
                    If Not IsStockAvailable(DDLBusUnit.SelectedValue, Format(GetServerTime(), "yyyyMM"), objTbl.Rows(C1)("itemoid"), objTbl.Rows(C1)("retwhoid").ToString, ToDouble((objTbl.Compute("SUM(retqty)", "itemoid='" & objTbl.Rows(C1)("itemoid") & "'")).ToString)) Then
                        sError &= "- Material Code <B><STRONG>" & objTbl.Rows(C1)("itemcode") & "</STRONG></B> must be equal or less than Stock Qty !<BR> "
                        Exit For
                    End If
                Next
                For C1 As Integer = 0 To objTbl.Rows.Count - 1
                    If retType.SelectedValue = "Initial AP" Then
                        If ToDouble(ToDouble(objTbl.Rows(C1)("amtbalance").ToString)) = 0 Then
                            If ToDouble(ToDouble(objTbl.Compute("SUM(retamt)", "mrmstoid=" & objTbl.Rows(C1)("mrmstoid").ToString))) > ToDouble(objTbl.Rows(C1)("amttrans").ToString) Then
                                sError &= "- Total Retur Invoice " & objTbl.Rows(C1)("mrno").ToString & " Harus Kurang dari atau sama dengan total Invoice! #(Rp. " & ToMaskEdit(ToDouble(objTbl.Rows(C1)("amttrans").ToString), 2) & ")#<BR>"
                                Exit For
                            End If
                        Else
                            If ToDouble(ToDouble(objTbl.Compute("SUM(retamt)", "mrmstoid=" & objTbl.Rows(C1)("mrmstoid").ToString))) > ToDouble(objTbl.Rows(C1)("amtbalance").ToString) Then
                                sError &= "- Total Retur Invoice " & objTbl.Rows(C1)("mrno").ToString & " Harus Kurang dari atau sama dengan total Balance! #(Rp. " & ToDouble(objTbl.Rows(C1)("amtbalance").ToString) & ")#<BR>"
                            End If
                        End If
                    End If
                Next
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            pretrawmststatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trnpretrawmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND pretrawmststatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnRetur.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql).ToString) > 0 Then
            lbInProcess.Visible = True
            lbInProcess.Text = "You have " & cKon.ambilscalar(sSql).ToString & " In Process Purchase Return Raw Material data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
        sSql = "SELECT COUNT(*) FROM QL_trnpretrawmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND pretrawmststatus='In Approval'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnRetur.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql).ToString) > 0 Then
            lbInApproval.Visible = True
            lbInApproval.Text = "You have " & cKon.ambilscalar(sSql).ToString & " In Approval Purchase Return Raw Material data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitAllDDL()
        'Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)
        'Fill DDL Reason
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='PURCHASE RETURN REASON' AND activeflag='ACTIVE'"
        FillDDL(reasonoid, sSql)
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='WAREHOUSE' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDL(mtrwhoid, sSql)
        mtrwhoid.SelectedValue = 629
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT retmstoid, retno, CONVERT(VARCHAR(10), retdate, 101) AS retdate, suppname, rettype, retmststatus, retmstnote, divname, ISNULL(retmstres2, 'Retur Ganti Barang') AS retmstres2, case upper(pretm.retmststatus) when 'REJECTED' then rejectreason when 'REVISED' then revisereason else '' end reason FROM QL_trnreturmst pretm INNER JOIN QL_mstsupp s ON s.suppoid=pretm.suppoid INNER JOIN QL_mstdivision div ON div.cmpcode=pretm.cmpcode WHERE"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " pretm.cmpcode='" & Session("CompnyCode") & "'"
        Else
            sSql &= " pretm.cmpcode LIKE '%'"
        End If
        sSql &= sSqlPlus & " ORDER BY CONVERT(DATETIME, pretm.retdate) DESC, pretm.retmstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trnretmrmst")
        gvList.DataSource = Session("TblMst")
        gvList.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub BindSupplierData()
        If retType.SelectedValue = "MR" Then
            sSql = "SELECT suppoid, suppcode, suppname, suppaddr FROM QL_mstsupp WHERE cmpcode='" & CompnyCode & "' AND " & FilterDDLListSupp.SelectedValue & " LIKE '%" & Tchar(FilterTextListSupp.Text) & "%' AND suppoid IN (SELECT DISTINCT suppoid FROM QL_trnmrmst WHERE cmpcode='" & CompnyCode & "' AND mrmststatus='Post' AND ISNULL(mrmstres1,'')='') ORDER BY suppcode, suppname"
        Else
            sSql = "SELECT suppoid, suppcode, suppname, suppaddr FROM QL_mstsupp WHERE cmpcode='" & CompnyCode & "' AND " & FilterDDLListSupp.SelectedValue & " LIKE '%" & Tchar(FilterTextListSupp.Text) & "%' AND suppoid IN (SELECT DISTINCT suppoid FROM QL_trnbelimst WHERE cmpcode='" & CompnyCode & "' AND trnbelimststatus IN('Approved','Closed')) ORDER BY suppcode, suppname"
        End If
        Dim objTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstsupp")
        Session("mstSupp") = objTbl
        gvListSupp.DataSource = objTbl
        gvListSupp.DataBind()
    End Sub

    Private Sub BindRegisterData()
        sSql = "SELECT registermstoid, registerno, CONVERT(VARCHAR(10), registerdate, 101) AS registerdate, registerdocrefno, (CASE WHEN CONVERT(VARCHAR(10), registerdocrefdate, 101)='01/01/1900' THEN '' ELSE CONVERT(VARCHAR(10), registerdocrefdate, 101) END) AS registerdocrefdate, registermstnote FROM QL_trnregistermst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND suppoid=" & suppoid.Text & " AND registertype='RAW' AND " & FilterDDLListReg.SelectedValue & " LIKE '%" & Tchar(FilterTextListReg.Text) & "%' AND registermstoid IN (SELECT registermstoid FROM QL_trnmrrawmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrrawmststatus='Post' AND ISNULL(mrrawmstres1, '')<>'Closed') AND registermstoid NOT IN (SELECT registermstoid FROM QL_trnaprawdtl apd INNER JOIN QL_trnaprawmst apm ON apm.cmpcode=apd.cmpcode AND apm.aprawmstoid=apd.aprawmstoid WHERE apd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND aprawmststatus<>'Rejected') ORDER BY registermstoid"
        'FillGV(gvListReg, sSql, "QL_trnregistermst")
        Dim objTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnregistermst")
        Session("Register") = objTbl
        gvListReg.DataSource = objTbl
        gvListReg.DataBind()
    End Sub

    Private Sub BindMRData()
        Dim tipeTrn As String = ""
        Dim sListType As String = "List Of Material Received"
        lblListMR.Visible = True : lblListPI.Visible = False : lblListPay.Visible = False
        If retType.SelectedValue = "MR" Then
            tipeTrn = "MR"
        ElseIf retType.SelectedValue = "PI" Then
            sListType = "List Of Purchase Invoice"
            tipeTrn = "PI"
        Else
            sListType = "List Of Initial AP"
            tipeTrn = "Initial AP"
        End If
        lblListMR.Text = sListType

        sSql = "SELECT mstoid,nomor,tgl,whoid,whdesc,note,suppoid,tipe, amttrans, amtbalance FROM("
        sSql &= " SELECT mrmstoid AS mstoid, mrno AS nomor , CONVERT(VARCHAR(10), mrdate, 101) AS tgl, 0 AS whoid, '' AS whdesc, mrmstnote AS note,suppoid AS suppoid,'MR' AS tipe, 0 amttrans, 0 amtbalance FROM QL_trnmrmst mrm INNER JOIN QL_mstgen ON genoid=mrwhoid WHERE mrm.cmpcode='" & CompnyCode & "' AND mrmststatus='Post' AND ISNULL(mrmstres1,'')=''"
        sSql &= " UNION ALL"
        sSql &= " SELECT mrm.trnbelimstoid AS mstoid, trnbelino AS nomor , CONVERT(VARCHAR(10), trnbelidate, 101) AS tgl, 0 AS whoid, '' AS whdesc, trnbelimstnote AS note,suppoid AS suppoid,'PI' AS tipe, 0 amttrans, 0 amtbalance FROM QL_trnbelimst mrm WHERE mrm.cmpcode='" & CompnyCode & "' AND mrm.trnbelimststatus IN('Approved','Closed') AND mrm.trnbelimstoid > 0"
        sSql &= " UNION ALL"
        sSql &= " SELECT DISTINCT mrm.trnbelimstoid AS mstoid, trnbelino AS nomor , CONVERT(VARCHAR(10), trnbelidate, 101) AS tgl, 0 AS whoid, '' AS whdesc, trnbelimstnote AS note,mrm.suppoid AS suppoid, 'Initial AP' tipe, amttrans, ISNULL(ap.amttrans - ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 LEFT JOIN QL_trnpayap pay ON pay.cmpcode=ap2.cmpcode AND pay.payapoid=ap2.payrefoid AND pay.refoid=ap2.refoid AND pay.reftype=ap2.reftype WHERE ap2.cmpcode=ap.cmpcode AND ap2.refoid=ap.refoid AND ap2.reftype=ap.reftype AND ap2.payrefoid<>0 AND ap2.suppoid=ap.suppoid AND ISNULL(pay.payapres1,'')<>'Lebih Bayar'), 0), 0) amtbalance FROM QL_trnbelimst mrm INNER JOIN QL_conap ap ON ap.cmpcode=mrm.cmpcode AND ap.refoid=mrm.trnbelimstoid WHERE mrm.cmpcode='" & CompnyCode & "' AND mrm.trnbelimststatus IN('Approved','Closed') AND mrm.trnbelimstoid < 0 AND ap.payrefoid=0 AND ap.reftype='QL_trnbelimst' AND ap.suppoid=mrm.suppoid"
        sSql &= " ) AS dtTbl"
        sSql &= " WHERE suppoid=" & suppoid.Text & " AND tipe='" & tipeTrn & "'"
        If Tchar(FilterTextListMR.Text) <> "" Then
            sSql &= "AND " & FilterDDLListMR.SelectedValue & " LIKE '%" & Tchar(FilterTextListMR.Text) & "%' ORDER BY mstoid"
        End If
        Dim objTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnmrmst")
        Session("tblMR") = objTbl
        gvListMR.DataSource = objTbl
        gvListMR.DataBind()
    End Sub

    Private Sub BindMaterialData()
        Dim sAdd As String = ""
        Dim sAdd2 As String = ""
        If Not Session("oid") Is Nothing And Session("oid") <> "" Then
            sAdd = " AND pretd.retmstoid<>" & Session("oid")
        End If
        Dim tipeTrn As String = ""
        Dim tableTrn As String = ""
        If retType.SelectedValue = "MR" Then
            tipeTrn &= "MR"
            tableTrn &= "QL_trnmrdtl"
        ElseIf retType.SelectedValue = "PI" Then
            tipeTrn &= "PI"
            tableTrn &= "QL_trnbelidtl"
        Else
            tipeTrn &= "Initial AP"
            sAdd2 = ""
        End If
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sSql = "SELECT checkvalue, mstoid, dtloid, itemoid, itemcode, oldcode, itemdesc, unitoid, unitdesc, qty, retqty, amt, retamt, retdtlnote, seq, reasonoid, reasondesc, value, valueidr, valueusd, mrmstoid, disc, tax, tipe, initvalue, notalama, ref_no mrno FROM ("
        sSql &= " Select DISTINCT 'False' AS checkvalue, mrdtloid AS mstoid, mrdtloid AS dtloid, mrd.matoid AS itemoid, m.itemCode AS itemcode, m.itemoldcode AS oldcode, m.itemLongDescription AS itemdesc, mrunitoid AS unitoid, gendesc AS unitdesc, (mrqty - ISNULL((SELECT SUM(retqty) FROM QL_trnreturdtl pretd WHERE pretd.refoidmst=mrm.mrmstoid AND pretd.refoiddtl=mrd.mrdtloid AND pretd.retdtlres1='QL_trnmrmst' AND pretd.retdtlres2='QL_trnmrdtl'" & sAdd & "), 0)) AS qty, 0.0 AS retqty, mrd.mrvalue AS amt, 0.0 AS retamt, '' AS retdtlnote, mrdtlseq AS seq, 0 AS reasonoid, '' AS reasondesc,mrd.mrvalue AS value,mrd.mrvalueidr valueidr,mrd.mrvalueusd AS valueusd, mrm.mrmstoid AS mrmstoid, (pod.podtldiscamt/pod.poqty) AS disc, 0 AS tax, 'MR' AS tipe, 0.00 initvalue, '' notalama, mrno ref_no FROM QL_trnmrdtl mrd INNER JOIN QL_mstitem m ON m.itemoid=mrd.matoid INNER JOIN QL_mstgen g ON genoid=mrunitoid INNER JOIN QL_trnmrmst mrm ON mrm.mrmstoid=mrd.mrmstoid AND mrm.cmpcode=mrd.cmpcode INNER JOIN QL_trnpodtl pod ON pod.podtloid=mrd.podtloid WHERE mrd.cmpcode='" & CompnyCode & "' AND mrm.mrmststatus='Post' AND ISNULL(mrdtlres1, '')<>'Complete'"
        sSql &= " UNION ALL"
        sSql &= " Select DISTINCT 'False' AS checkvalue, bld.trnbelidtloid AS mstoid, mrd.mrdtloid AS dtloid, bld.itemoid AS itemoid, m.itemCode AS itemcode, m.itemoldcode AS oldcode, m.itemLongDescription AS itemdesc, trnbelidtlunitoid AS unitoid, gendesc AS unitdesc, (trnbelidtlqty - ISNULL((SELECT SUM(retqty) FROM QL_trnreturdtl pretd WHERE pretd.refoidmst=blm.trnbelimstoid AND pretd.refoiddtl=bld.trnbelidtloid AND pretd.retdtlres1='QL_trnbelimst' AND pretd.retdtlres2='QL_trnbelidtl'" & sAdd & "), 0)) AS qty, 0.0 AS retqty, mrd.mrvalue AS amt, 0.0 AS retamt, '' AS retdtlnote, trnbelidtlseq AS seq, 0 AS reasonoid, '' AS reasondesc, mrd.mrvalue AS value, mrd.mrvalueidr AS valueidr, mrd.mrvalueusd AS valueusd, blm.trnbelimstoid AS mrmstoid, (pod.podtldiscamt/pod.poqty) AS disc, blm.trnbelimsttaxpct AS tax, 'PI' AS tipe, 0.00 initvalue, '' notalama, trnbelino ref_no FROM QL_trnbelidtl bld INNER JOIN QL_trnbelimst blm ON blm.cmpcode=bld.cmpcode AND blm.trnbelimstoid=bld.trnbelimstoid INNER JOIN QL_mstitem m ON m.itemoid=bld.itemoid INNER JOIN QL_mstgen g ON genoid=trnbelidtlunitoid INNER JOIN QL_trnmrdtl mrd ON mrd.mrdtloid=bld.mrdtloid INNER JOIN QL_trnpodtl pod ON pod.podtloid=mrd.podtloid WHERE bld.cmpcode='" & CompnyCode & "' AND blm.trnbelimststatus IN ('Approved','Closed') AND ISNULL(trnbelidtlres1, '')<>'Complete' AND bld.trnbelimstoid > 0"
        sSql &= " UNION ALL"
        sSql &= " Select DISTINCT 'False' AS checkvalue, 0 mstoid, itemoid dtloid, itemoid, m.itemCode AS itemcode, m.itemoldcode AS oldcode, m.itemLongDescription AS itemdesc, itemUnit1 AS unitoid, gendesc AS unitdesc, ISNULL((SELECT SUM(saldoakhir) FROM QL_crdstock crd WHERE crd.cmpcode=m.cmpcode AND crd.refoid=m.itemoid AND crd.mtrlocoid=" & mtrwhoid.SelectedValue & " AND crd.periodacctg IN ('" & sPeriod & "', '" & GetLastPeriod(sPeriod) & "') AND crd.closeuser=''),0.00) AS qty, 0.00 AS retqty, 0.00 AS amt, 0.00 AS retamt, '' AS retdtlnote, 0 seq, 0 AS reasonoid, '' AS reasondesc, 0.00 AS value, 0.00 AS valueidr, 0.00 AS valueusd, 0 AS mrmstoid, 0.00 AS disc, 0.00 AS tax, 'Initial AP' AS tipe, 0.00 initvalue, '" & Tchar(mrrawno.Text.Trim) & "' notalama, '' ref_no FROM QL_mstitem m INNER JOIN QL_mstgen g ON g.genoid=itemUnit1 WHERE m.cmpcode='" & CompnyCode & "'"
        sSql &= " ) AS tbl"
        sSql &= " WHERE " & sAdd2 & " tipe='" & tipeTrn & "'"

        If Tchar(FilterTextListMat.Text) <> "" Then
            sSql &= "AND " & FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        End If
        sSql &= " ORDER BY itemcode, ref_no desc"

        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstitem")
        Dim dvData As DataView = dtTbl.DefaultView
        dvData.RowFilter = "qty > 0"
        For C1 As Integer = 0 To dvData.Count - 1
            dvData(C1)("retqty") = dvData(C1)("qty").ToString
        Next
        If dvData.Count > 0 Then
            Session("TblMat") = dvData.ToTable
            Session("TblMatView") = dvData.ToTable
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            showMessage("Material data can't be found!", 2)
        End If
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("QL_trnreturdtl")
        dtlTable.Columns.Add("retdtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("mrmstoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("mrno", Type.GetType("System.String"))
        dtlTable.Columns.Add("retwhoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("warehouse", Type.GetType("System.String"))
        dtlTable.Columns.Add("mrdtloid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("dtloid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("oldcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("qty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("retqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("price", Type.GetType("System.Double"))
        dtlTable.Columns.Add("amt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("retamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("retamtnetto", Type.GetType("System.Double"))
        dtlTable.Columns.Add("retunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("retunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("retdtlnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("reasonoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("reasondesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("retvalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("retvalueidr", Type.GetType("System.Double"))
        dtlTable.Columns.Add("initvalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("retvalueusd", Type.GetType("System.Double"))
        dtlTable.Columns.Add("disc", Type.GetType("System.Double"))
        dtlTable.Columns.Add("retdtltaxamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("qtyunitkecil", Type.GetType("System.Double"))
        dtlTable.Columns.Add("qtyunitbesar", Type.GetType("System.Double"))
        dtlTable.Columns.Add("amttrans", Type.GetType("System.Double"))
        dtlTable.Columns.Add("amtbalance", Type.GetType("System.Double"))
        dtlTable.Columns.Add("tax", Type.GetType("System.Double"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub ClearDetail()
        pretrawdtlseq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                EnableHeader(False)
                pretrawdtlseq.Text = objTable.Rows.Count + 1
                btnSearchSupp.Visible = False
                btnClearSupp.Visible = False
            Else
                EnableHeader(True)
                btnSearchSupp.Visible = True
                btnClearSupp.Visible = True
            End If
        Else
            EnableHeader(True)
        End If
        i_u2.Text = "New Detail"
        mrrawmstoid.Text = ""
        mrrawno.Text = ""
        pretrawwhoid.Text = ""
        mrrawdtloid.Text = ""
        dtloid.Text = ""
        price.Text = ""
        matrawoid.Text = ""
        matrawcode.Text = ""
        matrawlongdesc.Text = ""
        oldcode.Text = ""
        mrrawqty.Text = ""
        pretrawqty.Text = ""
        pretrawdtlnote.Text = ""
        qtyunitbesar.Text = ""
        qtyunitkecil.Text = ""
        amount.Text = ""
        pretrawunitoid.Items.Clear()
        reasonoid.SelectedIndex = -1
        gvListDtl.SelectedIndex = -1
        btnSearchMR.Visible = True : btnClearMR.Visible = True
        'btnSearchMat.Visible = False
        retType_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Private Sub ClearType()
        suppoid.Text = ""
        suppname.Text = ""
        mrrawmstoid.Text = ""
        mrrawno.Text = ""
        pretrawwhoid.Text = ""
        mrrawdtloid.Text = ""
        matrawoid.Text = ""
        matrawcode.Text = ""
        matrawlongdesc.Text = ""
        matrawlimitqty.Text = ""
        mrrawqty.Text = ""
        pretrawqty.Text = ""
        pretrawunitoid.Items.Clear()
        pretrawdtlnote.Text = ""
        reasonoid.SelectedIndex = -1
        gvListDtl.SelectedIndex = -1
        btnSearchMR.Visible = True : btnClearMR.Visible = True
        'btnSearchMat.Visible = False
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal : DDLBusUnit.CssClass = sCss
        btnSearchSupp.Visible = bVal : btnClearSupp.Visible = bVal
        'btnSearchReg.Visible = bVal : btnClearReg.Visible = bVal
        retType.Enabled = bVal : retType.CssClass = sCss
        DDLTaxType.Enabled = bVal : DDLTaxType.CssClass = sCss
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT pretm.cmpcode, retmstoid, pretm.periodacctg, pretm.retdate, pretm.retno, pretm.suppoid, suppname, pretm.rettype, pretm.retmstamt, retmststatus, retmstnote, pretm.createuser, pretm.createtime, pretm.upduser, pretm.updtime, ISNULL(retmstres2, '') AS retmstres2 FROM QL_trnreturmst pretm INNER JOIN QL_mstsupp s ON s.suppoid=pretm.suppoid WHERE pretm.retmstoid=" & sOid
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = xreader("cmpcode").ToString
                retType.SelectedValue = xreader("retmstres2").ToString
                pretrawmstoid.Text = xreader("retmstoid").ToString
                periodacctg.Text = xreader("periodacctg").ToString
                pretrawdate.Text = Format(xreader("retdate"), "MM/dd/yyyy")
                pretrawno.Text = xreader("retno").ToString
                suppoid.Text = xreader("suppoid").ToString
                suppname.Text = xreader("suppname").ToString
                pretrawmstnote.Text = xreader("retmstnote").ToString
                pretrawmststatus.Text = xreader("retmststatus").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
                pretrawmstres2.SelectedValue = xreader("retmstres2").ToString
                TotalAmt.Text = ToMaskEdit(ToDouble(xreader("retmstamt").ToString), 2)
                InitAllDDL()
                retType_SelectedIndexChanged(Nothing, Nothing)
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False : btnSendApproval.Visible = False
            Exit Sub
        End Try
        btnSearchSupp.Visible = False
        btnClearSupp.Visible = False
        retType.Enabled = False
        retType.CssClass = "inpTextDisabled"
        If pretrawmststatus.Text <> "In Process" And pretrawmststatus.Text <> "Revised" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnSendApproval.Visible = False
            btnAddToList.Visible = False
            gvListDtl.Columns(0).Visible = False
            gvListDtl.Columns(gvListDtl.Columns.Count - 1).Visible = False
            If pretrawmststatus.Text <> "Rejected" Then
                lblTrnNo.Text = "Return No."
                pretrawmstoid.Visible = False
                pretrawno.Visible = True
            End If
        End If

        If retType.SelectedValue = "MR" Then
            sSql = "SELECT retdtlseq, pretd.refoidmst AS mrmstoid, pretd.refoiddtl AS mrdtloid, pretd.mrdtloid AS dtloid, mrno, retwhoid, gx.gendesc warehouse, pretd.matoid AS itemoid, itemCode, itemoldcode AS oldcode, itemLongDescription AS itemdesc, (mrqty - ISNULL((SELECT SUM(pretdx.retqty) FROM QL_trnreturdtl pretdx WHERE pretdx.cmpcode=pretd.cmpcode AND pretd.mrdtloid=pretdx.mrdtloid AND pretd.retmstoid<>pretdx.retmstoid), 0)) AS qty, retqty, mrd.mrdtlamt AS amt, retdtlamt AS retamt, retunitoid, g.gendesc AS retunit, retdtlnote, reasonoid, ISNULL(gres.gendesc, '') AS reasondesc, retqty_unitkecil AS qtyunitkecil,retqty_unitbesar AS qtyunitbesar,(retdtlamt/retqty) price,retvalue retvalue,retvalueidr retvalueidr,retvalueusd retvalueusd, retdtltaxamt, 0 amttrans, 0 amtbalance, 0.0 AS tax, retdtlamtnetto AS retamtnetto, mrno ref_no FROM QL_trnreturdtl pretd INNER JOIN QL_trnmrmst mrm ON mrm.cmpcode=pretd.cmpcode AND mrm.mrmstoid=pretd.refoidmst INNER JOIN QL_mstitem m ON m.itemoid=pretd.matoid INNER JOIN QL_mstgen g ON g.genoid=retunitoid INNER JOIN QL_trnmrdtl mrd ON mrd.cmpcode=pretd.cmpcode AND mrd.mrdtloid=pretd.refoiddtl AND mrd.mrmstoid=pretd.refoidmst INNER JOIN QL_mstgen gx ON gx.genoid=retwhoid LEFT JOIN QL_mstgen gres ON gres.genoid=reasonoid WHERE retmstoid=" & sOid & " ORDER BY retdtlseq"
        ElseIf retType.SelectedValue = "PI" Then
            sSql = "select retdtlseq, retmstoid, rd.refoidmst AS mrmstoid, rd.refoiddtl AS mrdtloid, rd.mrdtloid AS dtloid, trnbelino AS mrno, retwhoid, gx.gendesc warehouse, rd.matoid AS itemoid, itemCode, itemoldcode AS oldcode, itemLongDescription AS itemdesc, (trnbelidtlqty - ISNULL((SELECT SUM(retqty) FROM QL_trnreturdtl pretd WHERE pretd.refoidmst=blm.trnbelimstoid AND pretd.refoiddtl=bld.trnbelidtloid AND pretd.retdtlres1='QL_trnbelimst' AND pretd.retdtlres2='QL_trnbelidtl' AND rd.retmstoid<>pretd.retmstoid), 0)) AS qty, retqty, mrd.mrdtlamt AS amt, retdtlamt AS retamt, retunitoid, g.gendesc AS retunit, retdtlnote, reasonoid, ISNULL(gres.gendesc, '') AS reasondesc, retqty_unitkecil AS qtyunitkecil,retqty_unitbesar AS qtyunitbesar,(retdtlamt/retqty) price,retvalue retvalue,retvalueidr retvalueidr,retvalueusd retvalueusd, rd.retdtltaxamt, 0 amttrans, 0 amtbalance, blm.trnbelimsttaxpct AS tax, retdtlamtnetto AS retamtnetto, trnbelino ref_no from ql_trnreturdtl rd INNER JOIN QL_trnbelimst blm on blm.trnbelimstoid=rd.refoidmst INNER JOIN QL_trnbelidtl bld ON bld.trnbelidtloid=rd.refoiddtl INNER JOIN QL_mstitem i ON i.itemoid=rd.matoid INNER JOIN QL_mstgen gres ON gres.genoid=rd.reasonoid INNER JOIN QL_mstgen g ON g.genoid=rd.retunitoid INNER JOIN QL_mstgen gx ON gx.genoid=retwhoid INNER JOIN QL_trnmrdtl mrd ON mrd.cmpcode=bld.cmpcode AND mrd.mrdtloid=bld.mrdtloid WHERE retmstoid=" & sOid & " ORDER BY retdtlseq"
        Else
            sSql = "select retdtlseq, retmstoid, rd.refoidmst AS mrmstoid, rd.refoiddtl AS mrdtloid, rd.mrdtloid AS dtloid, case when rd.refoidmst=0 then ISNULL(rd.retdtlres2,'') else blm.trnbelino end AS mrno, retwhoid, gx.gendesc warehouse, rd.matoid AS itemoid, itemCode, itemoldcode AS oldcode, itemLongDescription AS itemdesc, (ISNULL((SELECT SUM(retqty) FROM QL_trnreturdtl pretd WHERE rd.retmstoid<>pretd.retmstoid), 0)) AS qty, retqty, CONVERT(DECIMAL (18,2), (retdtlamt/retqty)) AS amt, retdtlamt AS retamt, retunitoid, g.gendesc AS retunit, retdtlnote, reasonoid, ISNULL(gres.gendesc, '') AS reasondesc, retqty_unitkecil AS qtyunitkecil,retqty_unitbesar AS qtyunitbesar,(retdtlamt/retqty) price,retvalue retvalue,retvalueidr,retvalueusd retvalueusd, (rd.retdtltaxamt/rd.retqty) AS retdtltaxamt, trnbeliamtnetto amttrans, ISNULL(blm.trnbeliamtnetto - ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 LEFT JOIN QL_trnpayap pay ON pay.cmpcode=ap2.cmpcode AND pay.payapoid=ap2.payrefoid AND pay.refoid=ap2.refoid AND pay.reftype=ap2.reftype WHERE ap2.cmpcode=blm.cmpcode AND ap2.refoid=blm.trnbelimstoid AND ap2.reftype='QL_trnbelimst' AND ap2.payrefoid<>0 AND ap2.suppoid=blm.suppoid AND ISNULL(pay.payapres1,'')<>'Lebih Bayar'), 0), 0) amtbalance, 0.0 AS tax, retdtlamtnetto AS retamtnetto, '' ref_no from ql_trnreturdtl rd INNER JOIN QL_mstitem i ON i.itemoid=rd.matoid INNER JOIN QL_mstgen gres ON gres.genoid=rd.reasonoid INNER JOIN QL_mstgen g ON g.genoid=rd.retunitoid INNER JOIN QL_mstgen gx ON gx.genoid=retwhoid LEFT JOIN QL_trnbelimst blm on blm.trnbelimstoid=rd.refoidmst AND retdtlres1='QL_trnbelimst' WHERE retmstoid=" & sOid & " ORDER BY retdtlseq"
        End If
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_TRNRETURDTL")
        Session("TblDtl") = dtTbl
        gvListDtl.DataSource = dtTbl
        gvListDtl.DataBind()
        CountHdrAmount()
        ClearDetail()
        Dim bVal As Boolean = False
        If retType.SelectedValue.ToUpper = "INITIAL AP" Then
            bVal = True
        End If
        'gvListDtl.Columns(gvListDtl.Columns.Count - 3).Visible = bVal
    End Sub

    Private Sub PrintReport(ByVal sOid As String)
        Try
            report.Load(Server.MapPath(folderReport & "rptRetur.rpt"))
            Dim sWhere As String = " WHERE pretm.cmpcode='" & CompnyCode & "'"
            'If Session("CompnyCode") <> CompnyCode Then
            'sWhere = " WHERE pretm.cmpcode='" & Session("CompnyCode") & "'"
            'Else
            '    sWhere = " WHERE pretm.cmpcode LIKE '%'"
            'End If
            If sOid = "" Then
                If Tchar(FilterText.Text) <> "" Then
                    sWhere &= " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                End If
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND retdate>='" & FilterPeriod1.Text & " 00:00:00' AND retdate<='" & FilterPeriod2.Text & " 23:59:59'"
                        'Else
                        '    Exit Sub
                    End If
                Else
                    showMessage("Checked Periode Before Print", 2)
                    Exit Sub
                End If
                If cbStatus.Checked Then
                    If FilterDDLStatus.SelectedValue <> "All" Then
                        sWhere &= " AND retmststatus='" & FilterDDLStatus.SelectedValue & "'"
                    End If
                End If
            Else
                sWhere &= " AND pretm.retmstoid=" & sOid
            End If
            report.SetParameterValue("sWhere", sWhere)
            'PrintDoc1.PrinterSettings.PrinterName = "Microsoft XPS Document Writer"
            'Dim k As Integer
            'For k = 0 To PrintDoc1.PrinterSettings.PaperSizes.Count - 1
            '    If PrintDoc1.PrinterSettings.PaperSizes.Item(k).PaperName = "Return_" Then
            '        pkSize = PrintDoc1.PrinterSettings.PaperSizes.Item(k)
            '    End If
            'Next
            'report.PrintOptions.PrinterName = "Microsoft XPS Document Writer"
            'report.PrintOptions.PaperSize = CType(pkSize.RawKind, CrystalDecisions.Shared.PaperSize)
            'report.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Portrait
            'report.PrintOptions.PaperSource = CrystalDecisions.Shared.PaperSource.Manual
            cProc.SetDBLogonForReport(report)
            'report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "PurchaseReturnPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            report.Close()
            report.Dispose()
        End Try
        'Response.Redirect("~\Transaction\trnRetur.aspx?awal=true")
    End Sub

    Private Sub cekEnabled()
        If GetStrData("select isnull(convertable,0) from QL_mstitem where itemoid=" & matrawoid.Text & "") = 1 Then
            qtyunitkecil.Enabled = False
            qtyunitbesar.Enabled = False
            qtyunitkecil.CssClass = "inpText"
            qtyunitbesar.CssClass = "inpText"
        Else
            qtyunitkecil.Enabled = True
            qtyunitbesar.Enabled = True
            qtyunitkecil.CssClass = "inpTextDisabled"
            qtyunitbesar.CssClass = "inpTextDisabled"
        End If
    End Sub

    Private Sub calcKonversi()
        If pretrawunitoid.SelectedValue = GetStrData("select itemUnit1 from QL_mstitem where itemoid=" & matrawoid.Text & "") Then
            qtyunitbesar.Text = ToDouble(pretrawqty.Text)
            qtyunitkecil.Text = Math.Round(ToDouble(pretrawqty.Text) / ToDouble(GetStrData("select Unit3Unit1Conversion from QL_mstitem where itemoid=" & matrawoid.Text & "")), 2)
        Else
            qtyunitbesar.Text = Math.Round(ToDouble(pretrawqty.Text) * ToDouble(GetStrData("select Unit3Unit1Conversion from QL_mstitem where itemoid=" & matrawoid.Text & "")), 2)
            qtyunitkecil.Text = ToDouble(pretrawqty.Text)
        End If
        amount.Text = ToMaskEdit(ToDouble(pretrawqty.Text) * ToDouble(price.Text), 2)
    End Sub

    Private Sub CountHdrAmount()
        Dim dAmt As Double = 0 : Dim dTaxAmt As Double = 0
        Dim dDefTax As Double = 10
        If Not Session("TblDtl") Is Nothing Then
            Dim objTable As DataTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                For C1 As Int16 = 0 To objTable.Rows.Count - 1
                    dAmt += ToDouble(objTable.Rows(C1).Item("retamt").ToString)
                    dTaxAmt += ToDouble(objTable.Rows(C1).Item("retdtltaxamt").ToString)
                Next
            End If
        End If
        totalamount.Text = ToMaskEdit(dAmt, 2)
        taxamount.Text = ToMaskEdit(dTaxAmt, 2)
        If ToDouble(taxamount.Text) > 0 Then
            DDLTaxType.SelectedValue = "TAX" : msttaxpct.Text = dDefTax
        Else
            DDLTaxType.SelectedValue = "NON TAX" : msttaxpct.Text = 0
        End If
        TotalAmt.Text = ToMaskEdit(Math.Round(ToDouble(totalamount.Text) + ToDouble(taxamount.Text), 0, MidpointRounding.AwayFromZero), 2)
    End Sub

#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnRetur.aspx")
        End If
        If checkPagePermission("~\Transaction\trnRetur.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Purchase Return MR"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnSendApproval.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to send this data for APPROVAL?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            CheckStatus()
            InitAllDDL()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            Dim cRate As New ClassRate()
            cRate.SetRateValue(1, Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, 2) : Exit Sub
            End If
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                pretrawmstoid.Text = GenerateID("QL_TRNRETURMST", CompnyCode)
                pretrawmststatus.Text = "In Process"
                pretrawdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime)
                TabContainer1.ActiveTabIndex = 0
                btnDelete.Visible = False
                retType_SelectedIndexChanged(Nothing, Nothing)
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvListDtl.DataSource = dt
            gvListDtl.DataBind()
        End If
        If ToDouble(Format(GetServerTime(), "MM")) = 1 Then
            If ToDouble(Format(GetServerTime(), "dd")) <= 10 Then
                imbretdate.Visible = True
            End If
        Else
            If ToDouble(Format(GetServerTime(), "dd")) <= 3 Then
                imbretdate.Visible = True
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnRetur.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lbInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, pretm.updtime, GETDATE()) > " & nDays & " AND pretrawmststatus='In Process' "
        If checkPagePermission("~\Transaction\trnRetur.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND pretm.createuser='" & Session("UserID") & "' "
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub lbInApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbInApproval.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 1
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, pretm.updtime, GETDATE()) > " & nDays & " AND pretrawmststatus='In Approval' "
        If checkPagePermission("~\Transaction\trnRetur.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND pretm.createuser='" & Session("UserID") & "' "
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND retdate>='" & FilterPeriod1.Text & " 00:00:00' AND retdate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            If FilterDDLStatus.SelectedValue <> "All" Then
                sSqlPlus &= " AND retmststatus='" & FilterDDLStatus.SelectedValue & "'"
            End If
        End If
        If checkPagePermission("~\Transaction\trnRetur.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND pretm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Transaction\trnRetur.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND pretm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub gvList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvList.PageIndexChanging
        gvList.PageIndex = e.NewPageIndex
        gvList.DataSource = Session("TblMst")
        gvList.DataBind()
    End Sub

    Protected Sub gvList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvList.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvList.DataSource = Session("TblMst")
            gvList.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        btnClearSupp_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupp.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        FilterDDLListSupp.SelectedIndex = -1 : FilterTextListSupp.Text = "" : gvListSupp.SelectedIndex = -1
        BindSupplierData()
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
    End Sub

    Protected Sub btnClearSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupp.Click
        suppoid.Text = ""
        suppname.Text = ""
        btnClearReg_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnFindListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSupp.Click
        BindSupplierData()
        mpeListSupp.Show()
    End Sub

    Protected Sub btnAllListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListSupp.Click
        FilterDDLListSupp.SelectedIndex = -1 : FilterTextListSupp.Text = "" : gvListSupp.SelectedIndex = -1
        BindSupplierData()
        mpeListSupp.Show()
    End Sub

    Protected Sub gvListSupp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSupp.PageIndexChanging
        gvListSupp.PageIndex = e.NewPageIndex
        BindSupplierData()
        mpeListSupp.Show()
    End Sub

    Protected Sub gvListSupp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListSupp.SelectedIndexChanged
        If suppoid.Text <> gvListSupp.SelectedDataKey.Item("suppoid").ToString Then
            btnClearSupp_Click(Nothing, Nothing)
        End If
        suppoid.Text = gvListSupp.SelectedDataKey.Item("suppoid").ToString
        suppname.Text = gvListSupp.SelectedDataKey.Item("suppname").ToString
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub lkbCloseListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListSupp.Click
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub btnSearchReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchReg.Click
        If suppoid.Text = "" Then
            showMessage("Please select Supplier first!", 2)
            Exit Sub
        End If
        FilterDDLListReg.SelectedIndex = -1 : FilterTextListReg.Text = "" : gvListReg.SelectedIndex = -1
        BindRegisterData()
        cProc.SetModalPopUpExtender(btnHideListReg, pnlListReg, mpeListReg, True)
    End Sub

    Protected Sub btnClearReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearReg.Click
        registermstoid.Text = "" : registerno.Text = ""
    End Sub

    Protected Sub btnFindListReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListReg.Click
        'BindRegisterData()
        'mpeListReg.Show()
    End Sub

    Protected Sub btnAllListReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListReg.Click
        FilterDDLListReg.SelectedIndex = -1 : FilterTextListReg.Text = "" : gvListReg.SelectedIndex = -1
        'BindRegisterData()
        'mpeListReg.Show()
    End Sub

    Protected Sub gvListReg_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListReg.PageIndexChanging
        gvListReg.PageIndex = e.NewPageIndex
        BindRegisterData()
        mpeListReg.Show()
    End Sub

    Protected Sub gvListReg_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListReg.SelectedIndexChanged
        'If registermstoid.Text <> gvListReg.SelectedDataKey.Item("registermstoid").ToString Then
        '    btnClearReg_Click(Nothing, Nothing)
        'End If
        registermstoid.Text = gvListReg.SelectedDataKey.Item("mrmstoid").ToString
        registerno.Text = gvListReg.SelectedDataKey.Item("mrno").ToString
        cProc.SetModalPopUpExtender(btnHideListReg, pnlListReg, mpeListReg, False)
    End Sub

    Protected Sub lkbCloseListReg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListReg.Click
        cProc.SetModalPopUpExtender(btnHideListReg, pnlListReg, mpeListReg, False)
    End Sub

    Protected Sub btnSearchMR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMR.Click
        If suppoid.Text = "" Or suppname.Text = "" Then
            showMessage("Please select Supplier first!", 2)
            Exit Sub
        End If 
        FilterDDLListMR.SelectedIndex = -1 : FilterTextListMR.Text = "" : gvListMR.SelectedIndex = -1
        BindMRData()
        cProc.SetModalPopUpExtender(btnHideListMR, pnlListMR, mpeListMR, True)
    End Sub

    Protected Sub btnClearMR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearMR.Click
        mrrawmstoid.Text = "" : mrrawno.Text = "" : pretrawwhoid.Text = "" : amttrans.Text = "" : lblMax.Text = "" : amtbalance.Text = ""
    End Sub

    Protected Sub btnFindListMR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMR.Click
        BindMRData()
        mpeListMR.Show()
    End Sub

    Protected Sub btnAllListMR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMR.Click
        FilterDDLListMR.SelectedIndex = -1 : FilterTextListMR.Text = "" : gvListMR.SelectedIndex = -1
        BindMRData()
        mpeListMR.Show()
    End Sub

    Protected Sub gvListMR_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMR.PageIndexChanging
        gvListMR.PageIndex = e.NewPageIndex
        BindMRData()
        mpeListMR.Show()
    End Sub

    Protected Sub gvListMR_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListMR.SelectedIndexChanged
        If mrrawmstoid.Text <> gvListMR.SelectedDataKey.Item("mstoid").ToString Then
            btnClearMR_Click(Nothing, Nothing)
        End If
        mrrawmstoid.Text = gvListMR.SelectedDataKey.Item("mstoid").ToString
        mrrawno.Text = gvListMR.SelectedDataKey.Item("nomor").ToString
        amttrans.Text = ToDouble(gvListMR.SelectedDataKey.Item("amttrans").ToString)
        amtbalance.Text = ToDouble(gvListMR.SelectedDataKey.Item("amtbalance").ToString)
        cProc.SetModalPopUpExtender(btnHideListMR, pnlListMR, mpeListMR, False)
        btnSearchMat_Click(Nothing, Nothing)
    End Sub

    Protected Sub lkbCloseListMR_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMR.Click
        mrrawmstoid.Text = 0
        mrrawmstoid.Visible = False
        mrrawno.Text = ""
        pretrawwhoid.Text = 0
        cProc.SetModalPopUpExtender(btnHideListMR, pnlListMR, mpeListMR, False)
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        If suppoid.Text = "" Or suppname.Text = "" Then
            showMessage("Please select Supplier first!", 2)
            Exit Sub
        End If
        If Tchar(mrrawno.Text.Trim) = "" Then
            'showMessage("Please Fill Nota No. first!", 2)
            'Exit Sub
        End If
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : Session("TblMat") = Nothing : Session("TblMatView") = Nothing : gvListMat.DataSource = Nothing : gvListMat.DataBind()
        BindMaterialData()
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If UpdateCheckedMat() Then
            Session("TblMatView") = Session("TblMat")
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "mstoid=" & dtTbl.Rows(C1)("mstoid")
                    objView(0)("CheckValue") = "True"
                    dtTbl.Rows(C1)("CheckValue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                dtTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "No material data can't be selected!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "No material data can't be selected!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "mstoid=" & dtTbl.Rows(C1)("mstoid")
                    objView(0)("CheckValue") = "False"
                    objView(0)("retqty") = 0
                    objView(0)("retdtlnote") = ""
                    dtTbl.Rows(C1)("CheckValue") = "False"
                    dtTbl.Rows(C1)("retqty") = 0
                    dtTbl.Rows(C1)("retdtlnote") = ""
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                dtTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "No material data can't be selected!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "No material data can't be selected!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "CheckValue='True'"
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListMat") = "No material data have been selected before!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 2)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 2)
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(6).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 2)
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        'mrrawmstoid.Text = 0
        mrrawmstoid.Visible = False
        'mrrawno.Text = ""
        pretrawwhoid.Text = 0
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub lbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListListMat.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "CheckValue='True'"
            Dim iCheck As Integer = dtView.Count
            If iCheck > 0 Then
                dtView.RowFilter = ""
                dtView.RowFilter = "CheckValue='True' AND retqty>0"
                If dtView.Count <> iCheck Then
                    Session("WarningListMat") = "Qty for every checked material data must be more than 0!"
                    showMessage(Session("WarningListMat"), 2)
                    dtView.RowFilter = ""
                    Exit Sub
                End If
                dtView.RowFilter = ""
                dtView.RowFilter = "CheckValue='True' AND retqty<100000"
                If dtView.Count <> iCheck Then
                    Session("WarningListMat") = "Qty for every checked material data must be less than 100,000!"
                    showMessage(Session("WarningListMat"), 2)
                    dtView.RowFilter = ""
                    Exit Sub
                End If
                dtView.RowFilter = ""
                dtView.RowFilter = "CheckValue='True' AND retqty<=qty"
                If dtView.Count <> iCheck Then
                    Session("WarningListMat") = "Qty for every checked material data must be less than MR Qty!"
                    showMessage(Session("WarningListMat"), 2)
                    dtView.RowFilter = ""
                    Exit Sub
                End If
                If retType.SelectedValue.ToUpper = "NOTA LAMA" Then
                    dtView.RowFilter = ""
                    dtView.RowFilter = "CheckValue='True' AND notalama=''"
                    If dtView.Count > 0 Then
                        Session("WarningListMat") = "Nota Lama for every checked material data must be more than Zero!"
                        showMessage(Session("WarningListMat"), 2)
                        dtView.RowFilter = ""
                        Exit Sub
                    End If
                End If
                If retType.SelectedValue.ToUpper = "INITIAL AP" Or retType.SelectedValue.ToUpper = "NOTA LAMA" Then
                    dtView.RowFilter = ""
                    dtView.RowFilter = "CheckValue='True' AND amt>0"
                    If dtView.Count <> iCheck Then
                        Session("WarningListMat") = "Price for every checked material data must be more than 0!"
                        showMessage(Session("WarningListMat"), 2)
                        dtView.RowFilter = ""
                        Exit Sub
                    End If
                End If
                If Session("TblDtl") Is Nothing Then
                    CreateTblDetail()
                End If
                Dim objTable As DataTable
                objTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                Dim objRow As DataRow
                Dim counter As Integer = objTable.Rows.Count
                Dim dQty_unitkecil, dQty_unitbesar As Double
                For C1 As Integer = 0 To dtView.Count - 1
                    If retType.SelectedValue.ToUpper = "NOTA LAMA" Then
                        dv.RowFilter = " itemoid=" & dtView(C1)("itemoid")
                    Else
                        dv.RowFilter = " dtloid=" & dtView(C1)("dtloid") & " AND itemoid=" & dtView(C1)("itemoid")
                    End If
                    If dv.Count > 0 Then
                        Dim dQty As Double = ToDouble(dtView(C1)("retqty"))
                        dv(0)("retqty") = dQty
                        Dim dPrice As Double = ToDouble(dtView(C1)("amt"))
                        dv(0)("price") = dPrice
                        Dim dDisc As Double = ToDouble(dtView(C1)("disc"))
                        Dim dDiscAmt As Double = dDisc * dQty
                        Dim dPriceNett As Double = dPrice - dDisc
                        dv(0)("amt") = dPriceNett
                        Dim dAmt As Double = Math.Round(dPriceNett * dQty, 2, MidpointRounding.AwayFromZero)
                        dv(0)("retamt") = dAmt
                        Dim dTaxPct As Double = ToDouble(dtView(C1)("tax"))
                        If dTaxPct <= 0 Then
                            dv(0)("retdtltaxamt") = 0
                        Else
                            dv(0)("retdtltaxamt") = ((dAmt - dDiscAmt) * dTaxPct) / 100
                        End If
                        dv(0)("retdtltaxamt") = ToDouble(dtView(C1)("tax"))
                        GetUnitConverter(dtView(C1)("itemoid"), dtView(C1)("unitoid"), ToDouble(dtView(C1)("retqty")), dQty_unitkecil, dQty_unitbesar)
                        dv(0)("qtyunitkecil") = dQty_unitkecil
                        dv(0)("qtyunitbesar") = dQty_unitbesar
                        Dim dValIDR As Double = ToDouble(dtView(C1)("valueidr"))
                        If retType.SelectedValue.ToUpper = "INITIAL AP" Or retType.SelectedValue.ToUpper = "NOTA LAMA" Then
                            dValIDR = ToDouble(dtView(C1)("initvalue"))
                        End If
                        dv(0)("retvalue") = dValIDR
                        dv(0)("retvalueidr") = dValIDR
                        dv(0)("retvalueusd") = dValIDR
                        dv(0)("amttrans") = ToDouble(amttrans.Text)
                        dv(0)("amtbalance") = ToDouble(amtbalance.Text)
                        dv(0)("tax") = ToDouble(dtView(C1)("tax"))
                        dv(0)("retamtnetto") = dAmt + (((dAmt - dDiscAmt) * dTaxPct) / 100)
                    Else
                        counter += 1
                        objRow = objTable.NewRow()
                        objRow("retdtlseq") = counter
                        objRow("retwhoid") = mtrwhoid.SelectedValue
                        objRow("warehouse") = mtrwhoid.SelectedItem.Text
                        If retType.SelectedValue.ToUpper = "NOTA LAMA" Then
                            objRow("mrmstoid") = 0
                            objRow("mrdtloid") = 0
                            objRow("mrno") = dtView(C1)("notalama")
                            objRow("amttrans") = 0
                            objRow("amtbalance") = 0
                        Else
                            objRow("mrmstoid") = dtView(C1)("mrmstoid")
                            objRow("mrdtloid") = dtView(C1)("mstoid")
                            objRow("mrno") = dtView(C1)("mrno").ToString
                            objRow("amttrans") = ToDouble(amttrans.Text)
                            objRow("amtbalance") = ToDouble(amtbalance.Text)
                        End If
                        objRow("dtloid") = dtView(C1)("dtloid")
                        objRow("itemoid") = dtView(C1)("itemoid")
                        objRow("itemcode") = dtView(C1)("itemcode").ToString
                        objRow("oldcode") = dtView(C1)("oldcode").ToString
                        objRow("itemdesc") = dtView(C1)("itemdesc").ToString
                        objRow("qty") = ToDouble(dtView(C1)("qty"))
                        Dim dQty As Double = ToDouble(dtView(C1)("retqty"))
                        objRow("retqty") = dQty
                        Dim dPrice As Double = ToDouble(dtView(C1)("amt"))
                        objRow("price") = dPrice
                        Dim dDisc As Double = ToDouble(dtView(C1)("disc"))
                        Dim dDiscAmt As Double = dDisc * dQty
                        Dim dNettoPrice As Double = dPrice - dDisc
                        objRow("amt") = dNettoPrice
                        Dim dAmt As Double = Math.Round(dNettoPrice * dQty, 2, MidpointRounding.AwayFromZero)
                        objRow("retamt") = dAmt
                        objRow("retunitoid") = dtView(C1)("unitoid")
                        objRow("retunit") = dtView(C1)("unitdesc").ToString
                        objRow("retdtlnote") = dtView(C1)("retdtlnote")
                        objRow("reasonoid") = reasonoid.SelectedValue
                        objRow("reasondesc") = reasonoid.SelectedItem.Text
                        Dim dValIDR As Double = ToDouble(dtView(C1)("amt"))
                        objRow("retvalue") = dValIDR
                        objRow("retvalueidr") = dValIDR
                        objRow("retvalueusd") = dValIDR
                        Dim dTaxPct As Double = ToDouble(dtView(C1)("tax"))
                        If dTaxPct <= 0 Then
                            objRow("retdtltaxamt") = 0
                        Else
                            objRow("retdtltaxamt") = ((dAmt - dDiscAmt) * dTaxPct) / 100
                        End If
                        GetUnitConverter(dtView(C1)("itemoid"), dtView(C1)("unitoid"), ToDouble(dtView(C1)("retqty")), dQty_unitkecil, dQty_unitbesar)
                        objRow("tax") = ToDouble(dtView(C1)("tax"))
                        objRow("qtyunitkecil") = dQty_unitkecil
                        objRow("qtyunitbesar") = dQty_unitbesar
                        objRow("retamtnetto") = dAmt + (((dAmt - dDiscAmt) * dTaxPct) / 100)
                        objTable.Rows.Add(objRow)
                    End If
                    dv.RowFilter = ""
                Next
                dtView.RowFilter = ""
                objTable.AcceptChanges()
                Session("TblDtl") = objTable
                gvListDtl.DataSource = objTable
                gvListDtl.DataBind()
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                CountHdrAmount()
                ClearDetail()
            Else
                Session("WarningListMat") = "Please select material to add to list!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
            retType.Enabled = False
            retType.CssClass = "inpTextDisabled"
        Else
            Session("WarningListMat") = "Please select material to add to list!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            Dim objTable As DataTable = Session("TblDtl")
            Dim objRow As DataRow = objTable.Rows(pretrawdtlseq.Text - 1)
            Dim dQty_unitkecil, dQty_unitbesar As Double
            objRow.BeginEdit()
            objRow("mrmstoid") = mrrawmstoid.Text
            objRow("mrno") = mrrawno.Text
            objRow("retwhoid") = mtrwhoid.SelectedValue
            objRow("warehouse") = mtrwhoid.SelectedItem.Text
            objRow("mrdtloid") = mrrawdtloid.Text
            objRow("dtloid") = dtloid.Text
            objRow("itemoid") = matrawoid.Text
            objRow("itemcode") = matrawcode.Text
            objRow("oldcode") = oldcode.Text
            objRow("itemdesc") = matrawlongdesc.Text
            'objRow("matrawlimitqty") = ToDouble(matrawlimitqty.Text)
            objRow("qty") = ToDouble(mrrawqty.Text)
            objRow("retqty") = ToDouble(pretrawqty.Text)
            objRow("price") = ToMaskEdit(ToDouble(price.Text), 2)
            objRow("retunitoid") = pretrawunitoid.SelectedValue
            objRow("retunit") = pretrawunitoid.SelectedItem.Text
            objRow("retdtlnote") = pretrawdtlnote.Text
            objRow("reasonoid") = reasonoid.SelectedValue
            objRow("reasondesc") = reasonoid.SelectedItem.Text
            GetUnitConverter(matrawoid.Text, pretrawunitoid.SelectedValue, ToDouble(pretrawqty.Text), dQty_unitkecil, dQty_unitbesar)
            objRow("qtyunitkecil") = dQty_unitkecil
            objRow("qtyunitbesar") = dQty_unitbesar
            Dim dAmt As Double = ToDouble(pretrawqty.Text) * ToDouble(price.Text)
            objRow("retamt") = dAmt
            Dim dTaxPct As Double = ToDouble(taxpct.Text)
            If dTaxPct <= 0 Then
                objRow("retdtltaxamt") = 0
            Else
                objRow("retdtltaxamt") = ((dAmt) * dTaxPct) / 100
            End If
            If ToDouble(TotalAmt.Text) = 0 Then
                TotalAmt.Text = ""
            End If
            objRow("retamtnetto") = dAmt + (((dAmt) * dTaxPct) / 100)
            objRow.EndEdit()
            objTable.AcceptChanges()
            Session("TblDtl") = objTable
            gvListDtl.DataSource = objTable
            gvListDtl.DataBind()
            CountHdrAmount()
            ClearDetail()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvListDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 2)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 2)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 2)
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 2)
        End If
    End Sub

    Protected Sub gvListDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvListDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        Dim sOid As String = objRow(e.RowIndex)("retdtlseq").ToString
        Dim dv As DataView = objTable.DefaultView
        dv.RowFilter = "retdtlseq=" & sOid
        For C1 As Int16 = 0 To dv.Count - 1
            dv.Delete(0)
        Next
        dv.RowFilter = ""
        objTable.AcceptChanges()
        Session("TblDtl") = objTable
        gvListDtl.DataSource = Session("TblDtl")
        gvListDtl.DataBind()
        CountHdrAmount()
        ClearDetail()
    End Sub

    Protected Sub gvListDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListDtl.SelectedIndexChanged
        Try
            pretrawdtlseq.Text = gvListDtl.SelectedDataKey.Item("retdtlseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "retdtlseq=" & pretrawdtlseq.Text
                mrrawmstoid.Text = dv.Item(0).Item("mrmstoid").ToString
                mrrawno.Text = dv.Item(0).Item("mrno").ToString
                mtrwhoid.SelectedValue = dv.Item(0).Item("retwhoid").ToString
                mrrawdtloid.Text = dv.Item(0).Item("mrdtloid").ToString
                dtloid.Text = dv.Item(0).Item("dtloid").ToString
                matrawoid.Text = dv.Item(0).Item("itemoid").ToString
                matrawcode.Text = dv.Item(0).Item("itemcode").ToString
                oldcode.Text = dv.Item(0).Item("oldcode").ToString
                matrawlongdesc.Text = dv.Item(0).Item("itemdesc").ToString
                mrrawqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("qty").ToString), 2)
                pretrawqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("retqty").ToString), 2)
                amtUnit.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("amt").ToString), 2)
                price.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("price").ToString), 2)
                amount.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("retamt").ToString), 2)
                pretrawdtlnote.Text = dv.Item(0).Item("retdtlnote").ToString
                If dv.Item(0).Item("reasonoid").ToString = "0" Then
                    reasonoid.SelectedIndex = -1
                Else
                    reasonoid.SelectedValue = dv.Item(0).Item("reasonoid").ToString
                End If

                'Fill DDL Unit
                sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='UNIT' AND activeflag='ACTIVE'"
                FillDDL(pretrawunitoid, sSql)
                FillDDL(unitbesaroid, sSql)
                FillDDL(unitkeciloid, sSql)
                pretrawunitoid.SelectedValue = dv.Item(0).Item("retunitoid").ToString
                unitbesaroid.SelectedValue = GetStrData("select itemUnit1 from ql_mstitem where itemoid=" & matrawoid.Text & "")
                unitkeciloid.SelectedValue = GetStrData("select itemUnit3 from ql_mstitem where itemoid=" & matrawoid.Text & "")
                qtyunitbesar.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("qtyunitkecil").ToString), 2)
                qtyunitkecil.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("qtyunitbesar").ToString), 2)
                taxpct.Text = dv.Item(0).Item("tax").ToString

                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        Finally
            btnSearchMR.Visible = False : btnClearMR.Visible = False
            If retType.SelectedValue.ToUpper = "NOTA LAMA" Then
                mrrawno.CssClass = "inpText" : mrrawno.Enabled = True
            Else
                mrrawno.CssClass = "inpTextDisabled" : mrrawno.Enabled = False
            End If
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                If CheckDataExists("SELECT COUNT(*) FROM QL_trnreturmst WHERE retmstoid=" & pretrawmstoid.Text) Then
                    pretrawmstoid.Text = GenerateID("QL_TRNRETURMST", CompnyCode)
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNRETURMST", "retmstoid", pretrawmstoid.Text, "retmststatus", updtime.Text, "Approved")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    pretrawmststatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            pretrawdtloid.Text = GenerateID("QL_TRNRETURDTL", CompnyCode)
            Dim iAppOid As Integer = GenerateID("QL_APPROVAL", CompnyCode)
            If pretrawmststatus.Text = "Revised" Then
                pretrawmststatus.Text = "In Process"
            End If
            Dim refNameMst As String = ""
            Dim refNameDtl As String = ""
            If retType.SelectedValue = "MR" Then
                refNameMst &= "QL_trnmrmst"
                refNameDtl &= "QL_trnmrdtl"
            ElseIf retType.SelectedValue = "PI" Then
                refNameMst &= "QL_trnbelimst"
                refNameDtl &= "QL_trnbelidtl"
            ElseIf retType.SelectedValue.ToUpper = "NOTA LAMA" Then
                refNameMst &= "Nota Lama"
            Else
                refNameMst &= "QL_trnbelimst"
                refNameDtl &= "Initial AP"
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                'insert retur mst
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trnreturmst (cmpcode, retmstoid, periodacctg, retdate, retno, suppoid, rettype, retmstamt, retmstnote, retmststatus, createuser, createtime, upduser, updtime, retmstres2, posisi_appcode, retmsttaxamt, retmstamtnetto) VALUES ('" & DDLBusUnit.SelectedValue & "', " & pretrawmstoid.Text & ", '" & periodacctg.Text & "', '" & pretrawdate.Text & "', '" & pretrawno.Text & "', " & suppoid.Text & ", '" & retType.SelectedItem.Text & "', " & ToDouble(totalamount.Text) & ", '" & Tchar(pretrawmstnote.Text) & "', '" & pretrawmststatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & retType.SelectedValue & "', '" & IIf(pretrawmststatus.Text = "In Approval", "1", "") & "', " & ToDouble(taxamount.Text) & ", " & ToDouble(TotalAmt.Text) & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & pretrawmstoid.Text & " WHERE tablename='QL_TRNRETURMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trnreturmst SET periodacctg='" & periodacctg.Text & "', retdate='" & pretrawdate.Text & "', retno='" & pretrawno.Text & "', suppoid=" & suppoid.Text & ", rettype='" & retType.SelectedItem.Text & "', retmstamt=" & ToDouble(totalamount.Text) & ", retmstnote='" & Tchar(pretrawmstnote.Text) & "', retmststatus='" & pretrawmststatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, retmstres2='" & retType.SelectedValue & "', posisi_appcode='" & IIf(pretrawmststatus.Text = "In Approval", "1", "") & "', retmsttaxamt=" & ToDouble(taxamount.Text) & ", retmstamtnetto=" & ToDouble(TotalAmt.Text) & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND retmstoid=" & pretrawmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    'Update status MR dan PI
                    If retType.SelectedValue = "MR" Then
                        sSql = "UPDATE QL_trnmrdtl SET mrdtlres1='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrdtloid IN (SELECT refoiddtl FROM QL_trnreturdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND retdtlres1='QL_trnmrmst' AND retdtlres2='QL_trnmrdtl' AND retmstoid=" & pretrawmstoid.Text & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        sSql = "UPDATE QL_trnmrmst SET mrmstres1='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrmstoid IN (SELECT refoidmst FROM QL_trnreturdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND retdtlres1='QL_trnmrmst' AND retdtlres2='QL_trnmrdtl' AND retmstoid=" & pretrawmstoid.Text & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Else
                        sSql = "UPDATE QL_trnbelidtl SET trnbelidtlres1='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnbelidtloid IN (SELECT refoiddtl FROM QL_trnreturdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND retdtlres1='QL_trnmrmst' AND retdtlres2='QL_trnmrdtl' AND retmstoid=" & pretrawmstoid.Text & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        sSql = "UPDATE QL_trnbelimst SET trnbelimstres1='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnbelimstoid IN (SELECT refoidmst FROM QL_trnreturdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND retdtlres1='QL_trnmrmst' AND retdtlres2='QL_trnmrdtl' AND retmstoid=" & pretrawmstoid.Text & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                    sSql = "DELETE FROM QL_trnreturdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND retmstoid=" & pretrawmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If

                'insert retur detail
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trnreturdtl (cmpcode, retdtloid, retmstoid, retdtlseq, refoidmst, refoiddtl, retwhoid, matoid, retqty, retunitoid, retdtlamt, retdtlstatus, retdtlnote, retdtlres1, retdtlres2, upduser, updtime, reasonoid, retvalue, retvalueidr, retvalueusd, retqty_unitkecil, retqty_unitbesar, mrdtloid, retdtltaxamt, retdtlamtnetto) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C1 + CInt(pretrawdtloid.Text)) & ", " & pretrawmstoid.Text & ", " & C1 + 1 & ", " & objTable.Rows(C1)("mrmstoid") & ", " & objTable.Rows(C1)("mrdtloid") & ", " & objTable.Rows(C1)("retwhoid") & ", " & objTable.Rows(C1)("itemoid") & ", " & ToDouble(objTable.Rows(C1)("retqty").ToString) & ", " & objTable.Rows(C1)("retunitoid") & ", " & ToDouble(objTable.Rows(C1)("retamt")) & ", '', '" & Tchar(objTable.Rows(C1)("retdtlnote")) & "', '" & refNameMst & "', '" & IIf(retType.SelectedValue.ToUpper = "NOTA LAMA", Tchar(objTable.Rows(C1)("mrno").ToString), refNameDtl) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & objTable.Rows(C1)("reasonoid") & "," & ToDouble(objTable.Rows(C1).Item("retvalue").ToString) & "," & ToDouble(objTable.Rows(C1).Item("retvalueidr").ToString) & "," & ToDouble(objTable.Rows(C1).Item("retvalueusd").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("qtyunitkecil").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("qtyunitbesar").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("dtloid").ToString) & ", " & ToDouble(objTable.Rows(C1)("retdtltaxamt")) & ", " & ToDouble(objTable.Rows(C1)("retamtnetto")) & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()

                        'Cek Qty yg sudah terpenuhi
                        If ToDouble(objTable.Rows(C1)("retqty").ToString) >= ToDouble(objTable.Rows(C1)("qty").ToString) Then
                            If retType.SelectedValue = "MR" Then
                                sSql = "UPDATE QL_trnmrdtl SET mrdtlres1='Complete' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrdtloid=" & objTable.Rows(C1)("mrdtloid")
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                sSql = "UPDATE QL_trnmrmst SET mrmstres1='Closed' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrmstoid=" & objTable.Rows(C1)("mrmstoid") & " AND (SELECT COUNT(*) FROM QL_trnmrdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrmstoid=" & objTable.Rows(C1)("mrmstoid") & " AND mrdtloid<>" & objTable.Rows(C1)("mrdtloid") & " AND ISNULL(mrdtlres1, '')<>'Complete')=0"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                            ElseIf retType.SelectedValue = "PI" Then
                                sSql = "UPDATE QL_trnbelidtl SET trnbelidtlres1='Complete' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnbelidtloid=" & objTable.Rows(C1)("mrdtloid")
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                sSql = "UPDATE QL_trnbelimst SET trnbelimstres1='Closed' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnbelimstoid=" & objTable.Rows(C1)("mrmstoid") & " AND (SELECT COUNT(*) FROM QL_trnbelidtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnbelimstoid=" & objTable.Rows(C1)("mrmstoid") & " AND trnbelidtloid<>" & objTable.Rows(C1)("mrdtloid") & " AND ISNULL(trnbelidtlres1, '')<>'Complete')=0"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                            End If
                        End If
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(pretrawdtloid.Text)) & " WHERE tablename='QL_TRNRETURDTL' AND cmpcode='" & CompnyCode & "' "
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If

                'Status In Approval
                If pretrawmststatus.Text = "In Approval" Then
                    Dim dt As DataTable = Session("AppPerson")
                    Dim tempAppCode As String = ""
                    For C1 As Integer = 0 To dt.Rows.Count - 1
                        If C1 = 0 Then
                            tempAppCode = dt.Rows(C1)("apppersonlevel").ToString
                        End If
                        sSql = "INSERT INTO QL_approval (cmpcode, approvaloid, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvalnote, posisiLevel) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iAppOid + C1 & ", 'RETUR" & pretrawmstoid.Text & "_" & (iAppOid + C1) & "', '" & Session("UserID") & "', '" & pretrawdate.Text & "', 'New', 'QL_trnreturmst', " & pretrawmstoid.Text & ", 'In Approval', '" & dt.Rows(C1)("apppersonlevel").ToString & "', '" & dt.Rows(C1)("approvaluser").ToString & "', '1/1/1900', '', '" & IIf(tempAppCode = dt.Rows(C1)("apppersonlevel").ToString, "1", "") & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iAppOid + dt.Rows.Count - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_APPROVAL'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                xCmd.Connection.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    pretrawmststatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 1)
                xCmd.Connection.Close()
                pretrawmststatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & pretrawmstoid.Text & ".<BR>"
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trnRetur.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnRetur.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If pretrawmstoid.Text.Trim = "" Then
            showMessage("Please select Purchase Return Raw Material data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNRETURMST", "retmstoid", pretrawmstoid.Text, "retmststatus", updtime.Text, "Approved")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                pretrawmststatus.Text = "In Process"
                Exit Sub
            End If
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            If retType.SelectedValue = "MR" Then
                sSql = "UPDATE QL_trnmrdtl SET mrdtlres1='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrdtloid IN (SELECT mrdtloid FROM QL_trnreturdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND retmstoid=" & pretrawmstoid.Text & ")"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_trnmrmst SET mrmstres1='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrmstoid IN (SELECT mrmstoid FROM QL_trnmrdtl WHERE mrdtloid IN (SELECT mrdtloid FROM QL_trnreturdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND retmstoid=" & pretrawmstoid.Text & "))"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            ElseIf retType.SelectedValue = "PI" Then

            End If
          
            sSql = "DELETE FROM QL_trnreturdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND retmstoid=" & pretrawmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnreturmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND retmstoid=" & pretrawmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnRetur.aspx?awal=true")
    End Sub

    Protected Sub btnSendApproval_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSendApproval.Click
        sSql = "SELECT approvaluser, apppersontype, case when upper(apppersontype)='FINAL' then 0 else apppersonlevel end apppersonlevel FROM QL_approvalperson WHERE tablename='QL_trnreturmst' AND activeflag='ACTIVE' AND cmpcode IN ('" & CompnyCode & "', '" & DDLBusUnit.SelectedValue & "') order by apppersontype desc,apppersonlevel asc"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_approvalperson")
        If dt.Rows.Count = 0 Then
            showMessage("There is no user can't approve this transaction. Please define approval user in <STRONG>APPROVAL USER</STRONG> form.", 2)
        Else
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "apppersontype='Final'"
            If dv.Count = 0 Then
                showMessage("Final approval user is not defined for this transaction. Please define final approval user in <STRONG>APPROVAL USER</STRONG> form.", 2)
                dv.RowFilter = ""
            Else
                dv.RowFilter = ""
                Session("AppPerson") = dt
                pretrawmststatus.Text = "In Approval"
                btnSave_Click(Nothing, Nothing)
            End If
        End If
    End Sub

    Protected Sub btnPrintHdr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrintHdr.Click
        PrintReport("")
    End Sub

    Protected Sub gvList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvList.SelectedIndexChanged
        PrintReport(gvList.SelectedDataKey.Item("retmstoid").ToString)
    End Sub

    Protected Sub gvListSupp_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvListSupp.Sorting
        Dim dt = TryCast(Session("mstSupp"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvListSupp.DataSource = Session("mstSupp")
            gvListSupp.DataBind()
        End If
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
    End Sub

    Protected Sub gvListreg_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvListReg.Sorting
        Dim dt = TryCast(Session("Register"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvListReg.DataSource = Session("Register")
            gvListReg.DataBind()
        End If
        mpeListReg.Show()
    End Sub

    Protected Sub gvListMat_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvListMat.Sorting
        UpdateCheckedMat()
        Dim dt = TryCast(Session("TblMat"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvListMat.DataSource = Session("TblMat")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMR_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvListMR.Sorting
        Dim dt = TryCast(Session("tblMR"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvListMR.DataSource = Session("tblMR")
            gvListMR.DataBind()
        End If
        mpeListMR.Show()
    End Sub

    Protected Sub pretrawqty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles pretrawqty.TextChanged
        calcKonversi()
    End Sub

    Protected Sub retType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim bVal As Boolean = True
        Dim bVal2 As Boolean = False
        Dim sType As String = "Material Received"
        btnSearchMR.Visible = True : btnClearMR.Visible = True
        mrrawno.Enabled = False : mrrawno.CssClass = "inpTextDisabled"
        'btnSearchMat.Visible = False
        If retType.SelectedValue = "PI" Then
            sType = "Purchase Invoice"
        ElseIf retType.SelectedValue = "Initial AP" Then
            sType = "Initial AP" : bVal = False
        ElseIf retType.SelectedValue.ToUpper = "NOTA LAMA" Then
            sType = "Nota No" : bVal = False : bVal2 = True
            btnSearchMR.Visible = False : btnClearMR.Visible = False
            mrrawno.Enabled = True : mrrawno.CssClass = "inpText"
            btnSearchMat.Visible = True
        End If
        lblmr.Visible = True : lblpi.Visible = False : lblpayment.Visible = False 'ClearType()
        lblmr.Text = sType
        gvListMat.Columns(7).Visible = bVal : gvListMat.Columns(9).Visible = Not bVal
        lblMax.Visible = Not bVal : gvListMat.Columns(10).Visible = bVal2
        If i_u.Text = "New Data" Then
            btnClearMR_Click(Nothing, Nothing)
        End If
    End Sub

#End Region

End Class