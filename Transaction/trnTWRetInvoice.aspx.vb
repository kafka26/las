Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Structure VarUsageValue
    Public Val_IDR As Double
    Public Val_USD As Double
End Structure

Partial Class Transaction_trnTWRetInvoice
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Dim cRate As New ClassRate
    Public folderReport As String = "~/Report/"
#End Region

#Region "Function"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetID() As Int64
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As New SqlCommand("", conn)
        Dim sSql As String = ""
        xCmd.CommandTimeout = 60
        sSql = "select Count(*) from t_retitem_h"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        GetID = xCmd.ExecuteScalar + 1
        conn.Close()
        Return GetID
    End Function

    Private Function GetDropDownListValue(ByVal cc As System.Web.UI.ControlCollection, Optional ByVal sText As String = "") As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                If sText = "" Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
                Else
                    sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedItem.Text
                End If
            End If
        Next
        Return sReturn
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If retitemfromwhoid.SelectedValue = "" Then
            sError &= "- Please select FROM WH field!<BR>"
        End If
        If retitemtowhoid.SelectedValue = "" Then
            sError &= "- Please select TO WH field!<BR>"
        End If
        If itemoid.Text = "" Then
            sError &= "- Please select MATERIAL field!<BR>"
        End If
        If retitemqty.Text = "" Then
            sError &= "- Please fill QTY field!<BR>"
        Else
            If ToDouble(retitemqty.Text) <= 0 Then
                sError &= "- QTY field must be more than 0!<BR>"
            Else
                If ToDouble(retitemqty.Text) > ToDouble(stockqty.Text) Then
                    sError &= "- QTY field must be less than STOCK QTY!<BR>"
                End If
            End If
        End If
        If transunitoid.SelectedValue = "" Then
            sError &= "- Please select UNIT field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If retitemmstdate.Text = "" Then
            sError &= "- Please fill TRANSFER DATE field!<BR>"
        Else
            If Not IsValidDate(retitemmstdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- TRANSFER DATE is invalid. " & sErr & "<BR>"
            End If
        End If

        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill Detail Data!<BR>"
        Else
            Dim dtTbl As DataTable = Session("TblDtl")
            If dtTbl.Rows.Count <= 0 Then
                sError &= "- Please fill Detail Data!<BR>"
            Else
                If sError = "" Then
                    For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                        If Not IsStockAvailable(CompnyCode, Format(GetServerTime(), "yyyyMM"), dtTbl.Rows(C1)("itemoid"), dtTbl.Rows(C1)("retitemfromwhoid"), ToDouble((dtTbl.Compute("SUM(retitemqty)", "itemoid=" & dtTbl.Rows(C1)("itemoid") & " AND retitemfromwhoid=" & dtTbl.Rows(C1)("retitemfromwhoid") & "")).ToString), "") Then
                            sError &= "- Transfer Qty for Item!<BR><B><STRONG>" & dtTbl.Rows(C1).Item("itemLongDescription").ToString & "</STRONG></B> must less or equal than stock QTY !<BR>"
                            Exit For
                        End If
                    Next
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            retitemmststatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Public Function GetParameterID() As String
        Return Eval("itemoid") & "," & Eval("selectedunitoid")
    End Function

    Private Function GetStockValue(ByVal sOid As String, Optional ByVal sType As String = "IDR") As Double
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        Try
            GetStockValue = ToDouble(GetStrData("select dbo.getStockValue(" & sOid & ")"))
        Catch ex As Exception
            GetStockValue = 0
        End Try
    End Function
#End Region

#Region "Procedure"
    Private Sub UpdateCheckedListMat()
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("retitemqty") = ToDouble(GetTextBoxValue(row.Cells(6).Controls))
                                    'dtView(0)("retitemprice") = GetTextBoxValue(row.Cells(7).Controls)
                                    dtView(0)("retitemdtlnote") = GetTextBoxValue(row.Cells(8).Controls)
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
            End If
        End If
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("retitemqty") = ToDouble(GetTextBoxValue(row.Cells(6).Controls))
                                    'dtView(0)("retitemprice") = GetTextBoxValue(row.Cells(7).Controls)
                                    dtView(0)("retitemdtlnote") = GetTextBoxValue(row.Cells(8).Controls)
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMatView") = dtTbl
            End If
        End If
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvMst.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvMst.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvMst.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "transmstoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub InitDDLToWH()
        ' Fill DDL Warehouse
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='WAREHOUSE' AND genoid > 0 "
        'AND genoid <> " & retitemfromwhoid.SelectedValue
        If Session("oid") Is Nothing Or Session("oid") = "" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
        FillDDL(retitemtowhoid, sSql)
    End Sub

    Private Sub InitDDLFromWH()
        ' Fill DDL Warehouse
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='WAREHOUSE' AND genoid > 0"
        If Session("oid") Is Nothing Or Session("oid") = "" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
        If FillDDL(retitemfromwhoid, sSql) Then
            InitDDLToWH()
        End If
    End Sub

    Private Sub InitDDLUnit()
        ' Fill DDL Warehouse
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='UNIT'"
        If Session("oid") Is Nothing Or Session("oid") = "" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
        FillDDL(transunitoid, sSql)
        FillDDL(retitemunitoid, sSql)
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub DrawTable(ByVal gView As GridView, ByVal HdrText As String(), ByVal DtField As String(), ByVal arKey As String(), ByVal dtTbl As DataTable, ByVal action As String)
        gView.DataKeyNames = arKey
        Dim cField As New CommandField
        cField.ShowSelectButton = True
        cField.SelectText = "Select"
        cField.HeaderStyle.CssClass = "gvhdr"
        cField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
        gView.Columns.Add(cField)

        For C1 As Int16 = 0 To DtField.Length - 1 
            Dim sfield As New BoundField
            sfield.HeaderStyle.CssClass = "gvhdr"
            sfield.HeaderText = HdrText(C1)
            sfield.DataField = DtField(C1)
            sfield.HeaderStyle.Font.Size = FontUnit.XSmall
            sfield.ItemStyle.Font.Size = FontUnit.XSmall
            gView.Columns.Add(sfield)
        Next

        Dim tb As New TextBox

        If action = "dtl" Then
            Dim dField As New CommandField
            dField.ShowDeleteButton = True
            dField.HeaderStyle.CssClass = "gvhdr"
            dField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            dField.ItemStyle.Font.Bold = True
            dField.ItemStyle.ForeColor.R.ToString()
            gView.Columns.Add(dField)
        End If
        gView.DataSource = dtTbl
        gView.DataBind()
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "select h.retitemmstoid, h.periodacctg, CONVERT(VARCHAR(10), retitemmstdate, 101) retitemmstdate, h.retitemmstno, h.transitemmstoid, tw.transitemno, tw.transitemdocrefno, h.retitemdocrefno, h.retitemmstnote, h.retitemmststatus, h.retitemmstres1, h.retitemmstres2, h.retitemmstres3, h.createuser, h.createtime, h.upduser, h.updtime, div.divname from t_retitem_h h Inner Join QL_trntransitemmst tw ON tw.transitemmstoid=h.transitemmstoid INNER JOIN QL_mstdivision div ON div.cmpcode=h.cmpcode WHERE h.cmpcode='" & CompnyCode & "'"
        If checkPagePermission("~\Transaction\trnTWRetInvoice.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        sSql &= sSqlPlus & " ORDER BY CAST(h.retitemmstdate AS DATETIME) DESC, retitemmstoid DESC"

        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trntransmst")
        If dtTbl.Rows.Count > 0 Then
            Dim HdrText() As String = {"No. Draft", "No. Retur TW", "Tanggal", "No. TW", "Doc. Ref. No.", "Businee Unit", "Catatan", "Status"}
            Dim DtField() As String = {"retitemmstoid", "retitemmstno", "retitemmstdate", "transitemno", "transitemdocrefno", "divname", "retitemmstnote", "retitemmststatus"}
            Dim arKey() As String = {"retitemmstoid"}
            gvMst.Columns.Clear()
            DrawTable(gvMst, HdrText, DtField, arKey, dtTbl, "")
        End If
        Session("TblMst") = dtTbl
        'lblViewInfo.Visible = False
    End Sub

    Private Sub BindDataTW()
        Try
            sSql = "SELECT h.transitemmstoid, transitemno, CONVERT(VARCHAR(10), transitemdate, 101) AS transitemdate, transitemdocrefno, transitemmststatus, transitemmstnote, transitemmstres3, SUM(d.transitemqty) transitemqty, Isnull(SUM(r.retitemqty), 0.0) retitemqty, SUM(d.transitemqty) - Isnull(SUM(r.retitemqty), 0.0) transosqty FROM QL_trntransitemmst h Inner Join QL_trntransitemdtl d ON d.transitemmstoid=h.transitemmstoid Inner Join ( Select refoid, mtrlocoid, SUM(qtyin) qtyin, SUM(qtyout) qtyout, Isnull(SUM(qtyin), 0.0)-Isnull(SUM(qtyout), 0.0) qtysa from QL_constock Group By refoid, mtrlocoid HAVING Isnull(SUM(qtyin), 0.0)-Isnull(SUM(qtyout), 0.0) > 0) c ON c.refoid=d.itemoid AND c.mtrlocoid=d.transitemtowhoid Left Join t_retitem_d r ON r.transitemdtloid=d.transitemdtloid AND r.itemoid=d.itemoid Where h.transitemmststatus='Post' AND h.cmpcode='" & CompnyCode & "' AND Isnull(r.retitemmstoid, 0) != " & retitemmstoid.Text & " AND Isnull(qtysa, 0.0) > 0 AND transitemno LIKE '%" & Tchar(transitemno.Text.Trim) & "%' Group By h.transitemmstoid, transitemno, transitemdate, transitemdocrefno, transitemmststatus, transitemmstnote, transitemmstres3 Having SUM(d.transitemqty) - Isnull(SUM(r.retitemqty), 0.0)>0 and transitemno like 'SJPROD%' Order By transitemno"
            Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trntransmst")
            If dtTbl.Rows.Count > 0 Then
                gvTW.Columns.Clear()
                Dim HdrText() As String = {"No. Draft", "No. Transfer", "Tanggal", "Doc. Ref. No.", "Catatan"}
                Dim DtField() As String = {"transitemmstoid", "transitemno", "transitemdate", "transitemdocrefno", "transitemmstnote"}
                Dim arKey() As String = {"transitemmstoid", "transitemno", "transitemdocrefno", "transitemmstnote", "transitemmstres3"}
                DrawTable(gvTW, HdrText, DtField, arKey, dtTbl, "")
            End If
            Session("TblTwView") = dtTbl
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
        
    End Sub

    Private Sub BindMaterialData()
        sSql = "SELECT 'False' AS Checkvalue, 'True' AS enableqty, m.itemoid, itemoldcode, Isnull(CONVERT(DECIMAL(18, 2), itempricelist), 0.0) AS salesprice2, d.transitemdtloid, Isnull(d.salesprice, 0.0) retitemprice, 0.0 registerqty, itemcode, m.itemLongDescription, d.transitemunitoid AS retitemunitoid, d.transitemtowhoid retitemfromwhoid, j.gendesc retitemfromwhdesc, g.gendesc AS retitemunitname, ISNULL(( SELECT SUM(crd.qtyin) - SUm(crd.qtyout) FROM QL_constock crd WHERE crd.cmpcode = d.cmpcode AND crd.mtrlocoid = d.transitemtowhoid AND crd.refoid=m.itemoid ), 0.0) AS stockqty, d.transitemqty AS retitemqty, '' AS retitemdtlnote, d.transitemvalueidr AS retitemvalue, d.transitemvalueusd AS ValueUsd, m.itemUnit1 AS selectedunitoid, g.gendesc AS selectedunit, ( SELECT ISNULL(genother1, '') acctgoid FROM QL_mstgen gx WHERE gx.cmpcode = d.cmpcode AND gx.gengroup = 'GROUPITEM' AND gx.gencode = m.itemgroup AND activeflag = 'ACTIVE' ) AS stockacctgoid, m.itemunit1 AS stockunitoid, ( CASE m.itemGroup WHEN 'RAW' THEN 'RAW MATERIAL' WHEN 'FG' THEN 'FINISH GOOD' WHEN 'GEN' THEN 'GENERAL MATERIAL' WHEN 'WIP' THEN 'WIP' END ) AS refname FROM QL_mstitem m Inner Join QL_trntransitemdtl d ON d.itemoid=m.itemoid AND d.transitemunitoid=m.itemUnit1 Inner Join QL_mstgen j ON j.genoid=d.transitemtowhoid Inner Join QL_mstgen g ON g.genoid = d.transitemunitoid WHERE m.cmpcode = '" & CompnyCode & "' AND d.transitemmstoid=" & transitemmstoid.Text & " ORDER BY itemcode"
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstitem")
        If dtTbl.Rows.Count > 0 Then
            For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                Dim qty As Integer = dtTbl.Rows(C1)("retitemqty")
                dtTbl.Rows(C1)("retitemqty") = ToDouble(dtTbl.Rows(C1)("stockqty").ToString)
                dtTbl.Rows(C1)("retitemprice") = ToDouble(dtTbl.Rows(C1)("salesprice2").ToString)
            Next
            dtTbl.AcceptChanges()
            Dim dv As DataView = dtTbl.DefaultView
            dv.RowFilter = "stockqty>0"
            Session("TblMat") = dv.ToTable
            Session("TblMatView") = dv.ToTable
            gvListMat.DataSource = Session("TblMat")
            gvListMat.DataBind()
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            showMessage("Material data can't be found!", 2)
        End If
    End Sub

    Private Sub CreateTblDetail() 
        Dim dtlTable As DataTable = New DataTable("TblTransferDtl")
        dtlTable.Columns.Add("retitemdtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("transitemdtloid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("retitemfromwhoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("retitemfromwhdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("retitemtowhoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("retitemtowhdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemoldcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemLongDescription", Type.GetType("System.String"))
        dtlTable.Columns.Add("retitemqty", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("retitemprice", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("stockqty", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("retitemunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("retitemunitname", Type.GetType("System.String"))
        dtlTable.Columns.Add("retitemdtlnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("retitemvalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("stockacctgoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("refname", Type.GetType("System.String"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub DrawTblDtl(ByVal dtTbl As DataTable, ByVal gView As GridView) 
        If dtTbl.Rows.Count > 0 Then
            gvDtl.Columns.Clear()
            Dim HdrText() As String = {"No", "From WH", "To WH", "Code", "Material", "Unit", "Qty", "Harga Jual", "Keterangan"}
            Dim DtField() As String = {"retitemdtlseq", "retitemfromwhdesc", "retitemtowhdesc", "itemcode", "itemLongDescription", "retitemunitname", "retitemqty", "retitemprice", "retitemdtlnote"}
            Dim arKey() As String = {"itemoid", "retitemunitoid", "retitemdtlseq", "retitemfromwhoid", "retitemfromwhdesc", "retitemtowhoid", "retitemtowhdesc", "itemcode", "itemLongDescription", "retitemqty", "retitemunitname", "retitemprice", "retitemdtlnote", "stockqty", "transitemdtloid"}
            DrawTable(gView, HdrText, DtField, arKey, dtTbl, "dtl")
        End If
    End Sub

    Private Sub CountHdrAmount()
        Dim dTotalAmt As Double = 0
        If Session("TblDtl") IsNot Nothing Then
            Dim objTbl As DataTable = Session("TblDtl")
            If objTbl.Rows.Count > 0 Then
                For C1 As Integer = 0 To objTbl.Rows.Count - 1
                    dTotalAmt += ToDouble(objTbl.Rows(C1)("retitemprice")) * ToDouble(objTbl.Rows(C1)("retitemqty"))
                Next
            End If
        End If
        transtotalamt.Text = ToMaskEdit(Math.Round(dTotalAmt, 0, MidpointRounding.AwayFromZero), 4)
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
            sSql = "select h.retitemmstoid, h.periodacctg, h.retitemmstdate, h.retitemmstno, h.transitemmstoid, t.transitemno, t.transitemdocrefno, h.retitemmstnote, h.retitemmststatus, h.createuser, h.createtime, h.upduser, h.updtime, h.retitemmstres3 from t_retitem_h h Inner Join QL_trntransitemmst t ON t.transitemmstoid=h.transitemmstoid AND t.cmpcode=h.cmpcode Where h.retitemmstoid=" & sOid
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                InitDDLFromWH()
                retitemmstoid.Text = xreader("retitemmstoid").ToString
                custoid.Text = xreader("retitemmstres3").ToString
                periodacctg.Text = xreader("periodacctg").ToString
                retitemmstdate.Text = Format(xreader("retitemmstdate"), "MM/dd/yyyy")
                retitemmstno.Text = xreader("retitemmstno").ToString
                transitemno.Text = xreader("transitemno").ToString
                transitemmstoid.Text = xreader("transitemmstoid").ToString
                retitemdocrefno.Text = xreader("transitemdocrefno").ToString
                retitemmstnote.Text = xreader("retitemmstnote").ToString
                retitemmststatus.Text = xreader("retitemmststatus").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
            btnSave.Visible = False
            btnDelete.Visible = False
            btnposting.Visible = False
        End Try

        sSql = "Select retitemdtlseq, f.gendesc retitemfromwhdesc, k.gendesc retitemtowhdesc, i.itemoid, itemcode, itemLongDescription, u.gendesc retitemunitname, retitemqty, retitemprice, d.transitemdtloid, retitemdtlnote, d.retitemunitoid, d.retitemtowhoid, d.retitemfromwhoid, ISNULL(( SELECT SUM(crd.qtyin) - SUm(crd.qtyout) FROM QL_constock crd WHERE crd.cmpcode = d.cmpcode AND crd.mtrlocoid = d.retitemfromwhoid AND crd.refoid=i.itemoid ), 0.0) AS stockqty, d.retitemvalue, ( SELECT ISNULL(genother1, '') acctgoid FROM QL_mstgen gx WHERE gx.cmpcode = d.cmpcode AND gx.gengroup = 'GROUPITEM' AND gx.gencode = i.itemgroup AND activeflag = 'ACTIVE' ) AS stockacctgoid, ( CASE i.itemGroup WHEN 'RAW' THEN 'RAW MATERIAL' WHEN 'FG' THEN 'FINISH GOOD' WHEN 'GEN' THEN 'GENERAL MATERIAL' WHEN 'WIP' THEN 'WIP' END ) AS refname from t_retitem_d d Inner Join QL_trntransitemdtl t ON t.transitemdtloid=d.transitemdtloid Inner Join QL_mstitem i ON i.itemoid=d.itemoid Inner Join QL_mstgen f ON f.genoid=d.retitemfromwhoid Inner Join QL_mstgen k ON k.genoid=d.retitemtowhoid Inner Join QL_mstgen u ON u.genoid=d.retitemunitoid where d.retitemmstoid=" & sOid & " Order By retitemdtlseq"
        Dim objTable As DataTable = cKon.ambiltabel(sSql, "QL_trntransitemdtl")
        Session("TblDtl") = objTable
        DrawTblDtl(objTable, gvDtl)
        'gvDtl.DataSource = Session("TblDtl")
        'gvDtl.DataBind()
        ClearDetail()
        If retitemmststatus.Text <> "In Process" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnposting.Visible = False
            lblspuno.Text = "Transfer No."
            retitemmstoid.Visible = False
            retitemmstno.Visible = True
            btnAddToList.Visible = False
            gvDtl.Columns(0).Visible = False
            gvDtl.Columns(gvDtl.Columns.Count - 1).Visible = False
        End If
        'EnableGridDetail()
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
    End Sub

    Private Sub ClearDetail()
        retitemdtlseq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                retitemdtlseq.Text = objTable.Rows.Count + 1
            End If
        End If
        i_u2.Text = "New Detail"
        retitemfromwhoid.SelectedIndex = -1
        retitemtowhoid.SelectedIndex = -1
        itemoid.Text = "" : itemcode.Text = ""
        matlongdesc.Text = "" : retitemqty.Text = ""
        stockqty.Text = "" : transunitoid.SelectedIndex = -1
        retitemdtlnote.Text = "" : retitemprice.Text = ""
        gvDtl.SelectedIndex = -1 : btnSearchMat.Visible = True
        retitemfromwhoid.Enabled = True : retitemfromwhoid.CssClass = "inpText"
        CountHdrAmount()
    End Sub

    Private Sub SetUsageValue(ByRef objVal As VarUsageValue)
        objVal.Val_IDR = 0
        objVal.Val_USD = 0
        Dim dt As DataTable = Session("TblDtl")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            Dim dValIDR As Double = GetStockValue(dt.Rows(C1)("itemoid").ToString)
            Dim dValUSD As Double = GetStockValue(dt.Rows(C1)("itemoid").ToString, "USD")
            dt.Rows(C1)("retitemvalue") = dValIDR
            'dt.Rows(C1)("transvalueusd") = dValUSD
            objVal.Val_IDR += dValIDR '* ToDouble(dt.Rows(C1)("transqty").ToString)
            objVal.Val_USD += dValUSD '* ToDouble(dt.Rows(C1)("transqty").ToString)
        Next
        Session("TblDtl") = dt
    End Sub

    Private Sub GenerateNo()
        Dim sPrefixCode As String = "TRI"
        Dim sNo As String = sPrefixCode & "-" & Format(GetServerTime(), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(retitemmstno, " & DefaultFormatCounter & ") AS INTEGER)) + 1, 1) AS IDNEW FROM t_retitem_h WHERE cmpcode='" & CompnyCode & "' AND retitemmstno LIKE '" & sNo & "%'"
        If GetStrData(sSql) = "" Then
            retitemmstno.Text = GenNumberString(sNo, "", 1, DefaultFormatCounter)
        Else
            retitemmstno.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        End If
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnTWRetInvoice.aspx")
        End If
        If checkPagePermission("~\Transaction\trnTWRetInvoice.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Finish Good Transfer Retur (Invoice)"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnposting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")

        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            'lblTitleListMat.Text = "List Of Material"
            'CheckStatus()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitDDLToWH()
            InitDDLFromWH()
            InitDDLUnit()
            'cbCons_CheckedChanged(Nothing, Nothing)
            'suppoid_SelectedIndexChanged(Nothing, Nothing)
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                'BindTrnData("")
                retitemmstoid.Text = GetID()
                retitemmstdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                retitemmststatus.Text = "In Process"
                periodacctg.Text = GetDateToPeriodAcctg(CDate(retitemmstdate.Text))
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvDtl.DataSource = dt
            gvDtl.DataBind()
        End If
    End Sub

    Protected Sub btnSearchTW_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchTW.Click
        BindDataTW() : gvTW.Visible = True : ClearDetail()
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            Dim dQty_unitkecil As Double = 0
            Dim dQty_unitbesar As Double = 0
            Dim objTable As DataTable = Session("TblDtl")
            Dim objRow As DataRow = objTable.Rows(retitemdtlseq.Text - 1)
            objRow.BeginEdit()
            objRow("retitemtowhoid") = retitemfromwhoid.SelectedValue
            objRow("retitemfromwhdesc") = retitemfromwhoid.SelectedItem.Text
            objRow("retitemqty") = ToDouble(retitemqty.Text)
            objRow("retitemprice") = ToDouble(retitemprice.Text)
            objRow("retitemdtlnote") = retitemdtlnote.Text

            objRow.EndEdit()
            Session("TblDtl") = objTable
            gvDtl.DataSource = Session("TblDtl")
            gvDtl.DataBind()
            ClearDetail()
            'EnableGridDetail()
        End If
    End Sub

    Protected Sub gvTW_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTW.PageIndexChanging
        gvTW.PageIndex = e.NewPageIndex
        gvTW.DataSource = Session("TblTwView")
        gvTW.DataBind()
        gvTW.Visible = True
    End Sub

    Protected Sub gvTW_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvTW.SelectedIndexChanged
        transitemmstoid.Text = gvTW.SelectedDataKey.Item("transitemmstoid")
        transitemno.Text = gvTW.SelectedDataKey.Item("transitemno")
        retitemdocrefno.Text = gvTW.SelectedDataKey.Item("transitemdocrefno")
        retitemmstnote.Text = gvTW.SelectedDataKey.Item("transitemmstnote")
        custoid.Text = gvTW.SelectedDataKey.Item("transitemmstres3")
        gvTW.Visible = False
        If Session("TblDtl") Is Nothing = False Then
            i_u2.Text = "New Detail"
            Session("TblDtl") = Nothing
            gvDtl.Visible = False
        End If 
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        Session("TblMat") = Nothing : Session("TblMatView") = Nothing
        gvListMat.DataSource = Nothing : gvListMat.DataBind()
        BindMaterialData()
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        UpdateCheckedListMat()
        If Session("TblMat") IsNot Nothing Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        UpdateCheckedListMat()
        FilterTextListMat.Text = ""
        If Session("TblMat") IsNot Nothing Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = ""
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        UpdateCheckedListMat()
        gvListMat.PageIndex = e.NewPageIndex
        gvListMat.DataSource = Session("TblMatView")
        gvListMat.DataBind()
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(6).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 2)
                    End If
                End If
            Next
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 2)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 2)
            Dim str As String = e.Row.Cells(1).Text
            e.Row.Cells(1).Text = Mid(str, 1, 7) & vbCrLf & Mid(str, 8, 10) & vbCrLf & Mid(str, 18, 5)

            Dim iItemOid As Integer = 0
            cc = e.Row.Cells(0).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    iItemOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                End If
            Next
        End If
    End Sub

    Protected Sub cbHdrListMat_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For C1 As Integer = 0 To gvListMat.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        mpeListMat.Show()
    End Sub

    Protected Sub lkbSelectAllToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbSelectAllToListListMat.Click
        If Session("TblMatView") IsNot Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                lkbAddToListListMat_Click(Nothing, Nothing)
            Else
                Session("WarningListMat") = "Please show Material data first!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Please show Material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        Try
            UpdateCheckedListMat()
            If Session("TblMat") IsNot Nothing Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    dtView.RowFilter = ""
                    dtView.RowFilter = "checkvalue='True' AND retitemqty>0"
                    If dtView.Count <> iCheck Then
                        Session("WarningListMat") = "Transfer Qty for every checked material data must be more than 0!"
                        showMessage(Session("WarningListMat"), 2)
                        dtView.RowFilter = ""
                        Exit Sub
                    End If
                    dtView.RowFilter = ""
                    dtView.RowFilter = "checkvalue='True' AND retitemqty<=stockqty"
                    If dtView.Count <> iCheck Then
                        Session("WarningListMat") = "Transfer Qty for every checked material data must be less than Stock Qty!"
                        showMessage(Session("WarningListMat"), 2)
                        dtView.RowFilter = ""
                        Exit Sub
                    End If
                    dtView.RowFilter = ""
                    dtView.RowFilter = "checkvalue='True'"
                    If Session("TblDtl") Is Nothing Then
                        CreateTblDetail()
                    End If
                    Dim objTable As DataTable = Session("TblDtl")
                    Dim dv As DataView = objTable.DefaultView
                    Dim dQty_unitkecil As Double = 0
                    Dim dQty_unitbesar As Double = 0
                    Dim objRow As DataRow
                    Dim counter As Integer = objTable.Rows.Count
                    For C1 As Integer = 0 To dtView.Count - 1
                        dv.RowFilter = "retitemtowhoid=" & retitemtowhoid.SelectedValue & " AND itemoid=" & dtView(C1)("itemoid")
                        If dv.Count > 0 Then
                            dv(0)("retitemqty") = ToDouble(dtView(C1)("retitemqty").ToString)
                            dv(0)("retitemdtlnote") = dtView(C1)("retitemdtlnote").ToString
                            dv(0)("retitemprice") = ToDouble(dtView(C1)("retitemprice").ToString)
                        Else
                            counter += 1
                            objRow = objTable.NewRow()
                            objRow("retitemdtlseq") = counter
                            objRow("transitemdtloid") = dtView(C1)("transitemdtloid")
                            objRow("retitemfromwhoid") = dtView(C1)("retitemfromwhoid")
                            objRow("retitemfromwhdesc") = dtView(C1)("retitemfromwhdesc").ToString
                            objRow("retitemtowhoid") = retitemtowhoid.SelectedValue
                            objRow("retitemtowhdesc") = retitemtowhoid.SelectedItem.Text
                            objRow("itemoid") = dtView(C1)("itemoid")
                            objRow("itemcode") = dtView(C1)("itemcode").ToString
                            objRow("stockqty") = dtView(C1)("stockqty").ToString
                            objRow("itemLongDescription") = dtView(C1)("itemLongDescription").ToString
                            objRow("stockqty") = ToDouble(dtView(C1)("stockqty").ToString)
                            objRow("retitemqty") = ToDouble(dtView(C1)("retitemqty").ToString)
                            objRow("retitemprice") = ToDouble(dtView(C1)("retitemprice").ToString)
                            objRow("retitemunitoid") = dtView(C1)("retitemunitoid")
                            objRow("retitemunitname") = dtView(C1)("retitemunitname").ToString
                            objRow("retitemdtlnote") = dtView(C1)("retitemdtlnote").ToString
                            objRow("retitemvalue") = ToDouble(dtView(C1)("retitemvalue").ToString)
                            objRow("stockacctgoid") = dtView(C1)("stockacctgoid").ToString
                            objRow("refname") = dtView(C1)("refname").ToString
                            objTable.Rows.Add(objRow)
                        End If
                        dv.RowFilter = ""
                    Next
                    dtView.RowFilter = ""
                    Session("TblDtl") = objTable
                    DrawTblDtl(objTable, gvDtl) 
                    CountHdrAmount() : ClearDetail()
                    gvDtl.Visible = True
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                End If
            Else
                Session("WarningListMat") = "Please show some material data first!"
                showMessage(Session("WarningListMat"), 2)
            End If
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 2)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 2)
        End If
    End Sub

    Protected Sub gvDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("retitemdtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind() : ClearDetail()
        CountHdrAmount()
        'EnableGridDetail()
    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDtl.SelectedIndexChanged  
        Try
            retitemdtlseq.Text = gvDtl.SelectedDataKey.Item("retitemdtlseq").ToString().Trim
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim obj As DataTable = Session("TblDtl")
                Dim dv As DataView = obj.DefaultView
                dv.RowFilter = "retitemdtlseq=" & retitemdtlseq.Text
                retitemfromwhoid.SelectedValue = dv(0)("retitemfromwhoid").ToString
                stockqty.Text = dv(0)("itemoid").ToString
                'retitemfromwhoid_SelectedIndexChanged(Nothing, Nothing)
                retitemtowhoid.SelectedValue = dv(0)("retitemtowhoid").ToString
                itemoid.Text = dv(0)("itemoid").ToString
                itemcode.Text = dv(0)("itemcode").ToString
                matlongdesc.Text = dv(0)("itemLongDescription").ToString
                stockqty.Text = ToMaskEdit(ToDouble(dv(0)("stockqty").ToString), 2)
                retitemqty.Text = ToMaskEdit(ToDouble(dv(0)("retitemqty").ToString), 2)
                transunitoid.SelectedValue = dv(0)("retitemunitoid").ToString
                retitemunitoid.SelectedValue = dv(0)("retitemunitoid").ToString
                retitemdtlnote.Text = dv(0)("retitemdtlnote").ToString
                transitemdtloid.Text = dv(0)("transitemdtloid").ToString
                'stockunitoid.Text = dv(0)("stockunitoid").ToString
                retitemprice.Text = ToMaskEdit(ToDouble(dv(0)("retitemprice").ToString), 2)
                dv.RowFilter = ""
                'transfromwhoid.Enabled = False : transfromwhoid.CssClass = "inpTextDisabled"
                btnSearchMat.Visible = False
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnTWRetInvoice.aspx?awal=true")
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                'sSql = "SELECT COUNT(*) FROM QL_trntransitemmst WHERE transitemmstoid=" & retitemmstoid.Text
                'If CheckDataExists(sSql) Then
                '    retitemmstoid.Text = GenerateID("QL_TRNTRANSITEMMST", CompnyCode)
                '    isRegenOid = True
                'End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("t_retitem_h", "retitemmstoid", retitemmstoid.Text, "retitemmststatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    retitemmststatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            'retitemdtloid.Text = GenerateID("QL_TRNTRANSITEMDTL", CompnyCode)
            Dim iConAROid As Int32 = GenerateID("QL_CONAR", CompnyCode)
            Dim conmtroid As Integer = GenerateID("QL_CONSTOCK", CompnyCode) 
            Dim glmstoid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
            Dim gldtloid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
            Dim iStockValOid As Integer = GenerateID("QL_STOCKVALUE", CompnyCode)
            Dim iPiutangAcctgOid As Integer = GetAcctgOID(GetVarInterface("VAR_AR_OTHER", CompnyCode), CompnyCode)
            Dim iHppAcctgOid As Integer = GetAcctgOID(GetVarInterface("VAR_HPP_OTHER", CompnyCode), CompnyCode)
            Dim iJualAcctgOid As Integer = GetAcctgOID(GetVarInterface("VAR_PENJUALAN_OTHER", CompnyCode), CompnyCode)
            Dim dTotalIDR As Double, dTotalUSD As Double, dGrandTotalAmtIDR As Double = 0
            Dim cRate As New ClassRate
            Dim sTgl As String = Format(GetServerTime(), "MM/dd/yyyy")
            periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
            'sDim objValue As VarUsageValue

            Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sTgl))

            If retitemmststatus.Text = "Post" Then
                If isPeriodClosed(CompnyCode, sPeriod) Then
                    showMessage("Cannot continue this transaction anymore because the period : " & MonthName(CInt(sPeriod.Substring(4, 2))) & " " & sPeriod.Substring(0, 2) & " have been closed.", 3)
                    retitemmststatus.Text = "In Process"
                    Exit Sub
                End If
                cRate.SetRateValue("IDR", sTgl)
                If cRate.GetRateDailyLastError <> "" Then
                    showMessage(cRate.GetRateDailyLastError, 2)
                    retitemmststatus.Text = "In Process"
                    Exit Sub
                End If
                If cRate.GetRateMonthlyLastError <> "" Then
                    showMessage(cRate.GetRateMonthlyLastError, 2)
                    retitemmststatus.Text = "In Process"
                    Exit Sub
                End If
                Dim sVarErr As String = ""
                If Not IsInterfaceExists("VAR_STOCK", CompnyCode) Then
                    sVarErr = "VAR_STOCK"
                End If
                If sVarErr <> "" Then
                    showMessage(GetInterfaceWarning(sVarErr, "posting"), 2)
                    retitemmststatus.Text = "In Process"
                    Exit Sub
                End If
                GenerateNo()
                'SetUsageValue(objValue)
            End If

            ' Define Tabel Strucure utk Auto Jurnal 
            sSql = "SELECT '' tipe, 0 acctgoid, 0.0 debet, 0.0 credit, '' refno, '' note, '' AS glother1 "
            Dim tbPostGL As DataTable = cKon.ambiltabel(sSql, "PostGL")
            tbPostGL.Rows.Clear()
            Dim oRow As DataRow
            Dim dvCek As DataView = tbPostGL.DefaultView

            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                Dim mstoid As Integer = 0
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO t_retitem_h (cmpcode, periodacctg, retitemmstdate, retitemmstno, transitemmstoid, retitemdocrefno, retitemmstnote, retitemmststatus, retitemmstres1, retitemmstres2, retitemmstres3, createuser, createtime, upduser, updtime) VALUES ('" & CompnyCode & "', '" & periodacctg.Text & "', '" & retitemmstdate.Text & "', '" & Tchar(retitemmstno.Text) & "', " & transitemmstoid.Text & ", '" & Tchar(retitemdocrefno.Text) & "', '" & Tchar(retitemmstnote.Text) & "', '" & retitemmststatus.Text & "', '', '', '" & custoid.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP) SELECT SCOPE_IDENTITY()"
                    xCmd.CommandText = sSql
                    mstoid = ToInteger(xCmd.ExecuteScalar()) 
                Else
                    sSql = "UPDATE t_retitem_h SET periodacctg = '" & periodacctg.Text & "', retitemmstdate = '" & retitemmstdate.Text & "', retitemmstno = '" & Tchar(retitemmstno.Text) & "', retitemdocrefno = '" & Tchar(retitemdocrefno.Text) & "', retitemmstnote = '" & Tchar(retitemmstnote.Text) & "', retitemmstres2='" & Tchar(custoid.Text) & "', retitemmstres3='" & custoid.Text & "', retitemmststatus = '" & retitemmststatus.Text & "', upduser = '" & Session("UserID") & "', updtime = CURRENT_TIMESTAMP WHERE retitemmstoid=" & retitemmstoid.Text
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM t_retitem_d WHERE cmpcode='" & CompnyCode & "' AND retitemmstoid=" & retitemmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    mstoid = retitemmstoid.Text
                End If

                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO t_retitem_d ( cmpcode, retitemmstoid, retitemdtlseq, transitemdtloid, retitemfromwhoid, retitemtowhoid, itemoid, stockacctgoid, retitemqty, retitemprice, retitemvalue, retitemunitoid, retitemdtlnote, retitemdtlstatus, retitemdtlres1, retitemdtlres2, retitemdtlres3) VALUES ( '" & CompnyCode & "', " & mstoid & ", " & C1 + 1 & ", " & objTable.Rows(C1)("transitemdtloid") & ", " & objTable.Rows(C1)("retitemfromwhoid") & ", " & objTable.Rows(C1)("retitemtowhoid") & ", " & objTable.Rows(C1)("itemoid") & ", " & objTable.Rows(C1)("stockacctgoid") & ", " & ToDouble(objTable.Rows(C1)("retitemqty")) & ", " & ToDouble(objTable.Rows(C1)("retitemprice")) & ", " & ToDouble(objTable.Rows(C1)("retitemvalue").ToString) & ", " & ToDouble(objTable.Rows(C1)("retitemunitoid").ToString) & ", '" & Tchar(objTable.Rows(C1)("retitemdtlnote")) & "', '', '', '', '')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        ' Hitung Value Item
                        dTotalIDR += ToDouble(objTable.Rows(C1)("retitemvalue").ToString) * ToDouble(objTable.Rows(C1)("retitemqty").ToString)
                        'dTotalUSD += ToDouble(objTable.Rows(C1)("transvalueusd").ToString) * ToDouble(objTable.Rows(C1)("retitemqty").ToString)
                        dGrandTotalAmtIDR += Math.Round((ToDouble(objTable.Rows(C1)("retitemprice").ToString) * ToDouble(objTable.Rows(C1)("retitemqty").ToString)), 0, MidpointRounding.AwayFromZero)

                        If retitemmststatus.Text = "Post" Then
                            ' Insert QL_conmat Out
                            sSql = "INSERT INTO QL_constock (cmpcode ,constockoid ,contype ,trndate ,formaction ,formoid ,periodacctg ,refname ,refoid ,mtrlocoid ,qtyin ,qtyout ,amount ,hpp ,typemin ,note ,reason ,createuser ,createtime ,upduser ,updtime ,refno ,deptoid, valueidr, valueusd) VALUES ('" & CompnyCode & "'," & conmtroid & ", 'RETOUT','" & sTgl & "', 't_retitem_d','" & retitemmstoid.Text & "', '" & sPeriod & "', '" & objTable.Rows(C1)("refname") & "', " & objTable.Rows(C1)("itemoid") & ", " & objTable.Rows(C1)("retitemfromwhoid") & ", 0, " & ToDouble(objTable.Rows(C1)("retitemqty").ToString) & ", 0, 0, -1,'" & retitemmstno.Text & " # KE " & objTable.Rows(C1)("retitemtowhdesc") & "', 'Transfer Retur INV (Invoice)','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & retitemmstno.Text & "',0," & ToDouble(objTable.Rows(C1).Item("retitemvalue").ToString) & "," & ToDouble(objTable.Rows(C1).Item("retitemvalue").ToString) & ")"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            conmtroid += 1

                            ' Insert QL_conmat In
                            sSql = "INSERT INTO QL_constock (cmpcode ,constockoid ,contype ,trndate ,formaction ,formoid ,periodacctg ,refname ,refoid ,mtrlocoid ,qtyin ,qtyout ,amount ,hpp ,typemin ,note ,reason ,createuser ,createtime ,upduser ,updtime ,refno ,deptoid, valueidr, valueusd) VALUES ('" & CompnyCode & "'," & conmtroid & ",'RETIN','" & sTgl & "', 't_retitem_d','" & retitemmstoid.Text & "','" & sPeriod & "','" & objTable.Rows(C1)("refname") & "'," & objTable.Rows(C1)("itemoid") & "," & objTable.Rows(C1)("retitemtowhoid") & ", " & ToDouble(objTable.Rows(C1)("retitemqty").ToString) & ", 0, 0, 0, 1,'" & retitemmstno.Text & " # DARI " & objTable.Rows(C1)("retitemfromwhdesc") & "', 'Transfer Retur INV (Invoice)','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & retitemmstno.Text & "',0," & ToDouble(objTable.Rows(C1).Item("retitemvalue").ToString) & "," & ToDouble(objTable.Rows(C1).Item("retitemvalue").ToString) & ")"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            conmtroid += 1 
                        End If

                        dvCek.RowFilter = "acctgoid=" & objTable.Rows(C1).Item("stockacctgoid").ToString & " "
                        If dvCek.Count > 0 Then 
                            dvCek(0)("credit") += ToDouble(objTable.Rows(C1).Item("retitemvalue").ToString) * ToDouble(objTable.Rows(C1).Item("retitemqty").ToString)
                            dvCek(0)("debet") += ToDouble(objTable.Rows(C1).Item("retitemvalue").ToString) * ToDouble(objTable.Rows(C1).Item("retitemqty").ToString)
                            dvCek.RowFilter = ""
                        Else
                            dvCek.RowFilter = ""
                            oRow = tbPostGL.NewRow
                            oRow("acctgoid") = objTable.Rows(C1).Item("stockacctgoid")
                            oRow("debet") = ToDouble(objTable.Rows(C1).Item("retitemvalue").ToString) * ToDouble(objTable.Rows(C1).Item("retitemqty").ToString)
                            oRow("credit") = ToDouble(objTable.Rows(C1).Item("retitemvalue").ToString) * ToDouble(objTable.Rows(C1).Item("retitemqty").ToString)

                            tbPostGL.Rows.Add(oRow)
                        End If
                        tbPostGL.AcceptChanges()

                        'Insert(STockvalue)
                        'sSql = GetQueryUpdateStockValue(-ToDouble(objTable.Rows(C1)("retitemqty").ToString), -ToDouble(objTable.Rows(C1)("retitemqty").ToString), ToDouble(objTable.Rows(C1).Item("retitemvalue").ToString), ToDouble(objTable.Rows(C1).Item("retitemvalue").ToString), "QL_trntransitemdtl", sTgl, Session("UserID"), CompnyCode, sPeriod, objTable.Rows(C1)("itemoid"))
                        'xCmd.CommandText = sSql
                        'If xCmd.ExecuteNonQuery() = 0 Then
                        '    sSql = GetQueryInsertStockValue(-ToDouble(objTable.Rows(C1)("retitemqty").ToString), -ToDouble(objTable.Rows(C1)("retitemqty").ToString), ToDouble(objTable.Rows(C1).Item("retitemvalue").ToString), ToDouble(objTable.Rows(C1).Item("retitemvalue").ToString), "QL_trntransitemdtl", sTgl, Session("UserID"), CompnyCode, sPeriod, objTable.Rows(C1)("itemoid"), iStockValOid)
                        '    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        '    iStockValOid += 1
                        'End If
                    Next

                    sSql = "UPDATE QL_mstoid SET lastoid=" & conmtroid - 1 & " WHERE tablename='QL_CONSTOCK' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    'sSql = "UPDATE QL_mstoid SET lastoid=" & iStockValOid - 1 & " WHERE tablename='QL_stockvalue' AND cmpcode='" & CompnyCode & "'"
                    'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()                  

                    If retitemmststatus.Text = "Post" Then
                        'Insert conar
                        dGrandTotalAmtIDR = Math.Round(dGrandTotalAmtIDR, 0, MidpointRounding.AwayFromZero)
                        Dim sDueDate As String = DateAdd(DateInterval.Day, 30, CDate(sTgl))
                        sSql = "INSERT INTO QL_conar (cmpcode, conaroid, reftype, refoid, payrefoid, custoid, acctgoid, trnarstatus, trnartype, trnardate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans, amtbayar, trnarnote, trnarres1, trnarres2, trnarres3, createuser, createtime, upduser, updtime, amttransidr, amtbayaridr, amttransusd, amtbayarusd) VALUES ('" & CompnyCode & "', " & iConAROid & ", 't_retitem_h', " & retitemmstoid.Text & ", 0, " & custoid.Text & ", " & iPiutangAcctgOid & ", 'Post', 'AR', '" & sTgl & "', '" & sPeriod & "', " & iHppAcctgOid & ", CURRENT_TIMESTAMP, '" & Tchar(retitemmstno.Text) & "', 0, '" & sDueDate & "', 0, " & dGrandTotalAmtIDR & ", '', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, " & dGrandTotalAmtIDR * cRate.GetRateMonthlyIDRValue & ", 0, " & dGrandTotalAmtIDR * cRate.GetRateMonthlyUSDValue & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()

                        If dTotalIDR > 0 Or dTotalUSD > 0 Then
                            ' Insert GL MST
                            sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & CompnyCode & "', " & glmstoid & ", '" & sTgl & "', '" & sPeriod & "', 'Transfer Retur INV|No. " & retitemmstno.Text & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 0, 0, 1, 1, ISNULL((" & dTotalUSD & "/NULLIF(" & dTotalIDR & ", 0)), 0), ISNULL((" & dTotalUSD & "/NULLIF(" & dTotalIDR & ", 0)), 0))"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            ' Insert GL DTL
                            Dim iSeq As Integer = 1
                            For C1 As Integer = 0 To tbPostGL.Rows.Count - 1
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & CompnyCode & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & tbPostGL.Rows(C1)("acctgoid") & ", 'D', " & ToDouble(tbPostGL.Rows(C1)("credit").ToString) & ", '" & retitemmstno.Text & "', 'Transfer Retur INV|No. " & retitemmstno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(tbPostGL.Rows(C1)("credit").ToString) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(tbPostGL.Rows(C1)("credit").ToString) * cRate.GetRateMonthlyUSDValue & ", 't_retitem_h " & retitemmstoid.Text & "')"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                gldtloid += 1 : iSeq += 1
                            Next
                            glmstoid += 1

                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & CompnyCode & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & iHppAcctgOid & ", 'C', " & dTotalIDR & ", '" & retitemmstno.Text & "', 'Transfer Retur INV|No. " & retitemmstno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalIDR * cRate.GetRateMonthlyIDRValue & ", " & dTotalIDR * cRate.GetRateMonthlyUSDValue & ", 't_retitem_h " & retitemmstoid.Text & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            gldtloid += 1 : iSeq += 1

                            ' Insert GL MST 2nd
                            sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & CompnyCode & "', " & glmstoid & ", '" & sTgl & "', '" & sPeriod & "', 'Transfer Retur INV|No. " & retitemmstno.Text & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 0, 0, 1, 1, ISNULL((" & dTotalUSD & "/NULLIF(" & dTotalIDR & ", 0)), 0), ISNULL((" & dTotalUSD & "/NULLIF(" & dTotalIDR & ", 0)), 0))"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & CompnyCode & "', " & gldtloid & ", 1, " & glmstoid & ", " & iPiutangAcctgOid & ", 'D', " & dGrandTotalAmtIDR & ", '" & retitemmstno.Text & "', 'Transfer Retur INV|No. " & retitemmstno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dGrandTotalAmtIDR * cRate.GetRateMonthlyIDRValue & ", " & dGrandTotalAmtIDR * cRate.GetRateMonthlyUSDValue & ", 't_retitem_h " & retitemmstoid.Text & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            gldtloid += 1

                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & CompnyCode & "', " & gldtloid & ", 2, " & glmstoid & ", " & iJualAcctgOid & ", 'C', " & dGrandTotalAmtIDR & ", '" & retitemmstno.Text & "', 'Transfer Retur INV|No. " & retitemmstno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dGrandTotalAmtIDR * cRate.GetRateMonthlyIDRValue & ", " & dGrandTotalAmtIDR * cRate.GetRateMonthlyUSDValue & ", 't_retitem_h " & retitemmstoid.Text & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            gldtloid += 1

                            sSql = "UPDATE QL_mstoid SET lastoid=" & iConAROid & " WHERE tablename='QL_CONAR' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_mstoid SET lastoid=" & glmstoid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_mstoid SET lastoid=" & gldtloid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        End If
                    End If
                End If
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.ToString & sSql, 1)
                        retitemmststatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.ToString & sSql, 1)
                    retitemmststatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.ToString & sSql, 2)
                conn.Close()
                retitemmststatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & retitemmstoid.Text & ".<BR>"
            End If

            If retitemmststatus.Text = "Post" Then
                Session("SavedInfo") &= "Data have been posted with Transfer No. = " & retitemmstno.Text & "."
            End If

            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trnTWRetInvoice.aspx?awal=true")
            End If
            Response.Redirect("~\Transaction\trnTWRetInvoice.aspx?awal=true")
        End If
    End Sub

    Protected Sub gvMst_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvMst.SelectedIndexChanged
        Response.Redirect("~\Transaction\trnTWRetInvoice.aspx?oid=" & gvMst.SelectedDataKey("retitemmstoid").ToString & "") 
    End Sub

    Protected Sub btnEraseTW_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseTW.Click
        transitemno.Text = "" : custoid.Text = "0"
        ClearDetail() : i_u2.Text = "New Detail"
        Session("TblDtl") = Nothing : gvDtl.Columns.Clear()
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
    End Sub

    Protected Sub btnposting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnposting.Click
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        If isPeriodClosed(CompnyCode, CDate(sDate)) Then
            showMessage("Period " & GetDateToPeriodAcctg(CDate(sDate)) & " Have been Already CLOSED, Please CANCEL this transaction and try again!", 2)
            Exit Sub
        End If
        retitemmststatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub 

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail() : gvTW.Visible = False
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND CAST(retitemmstdate AS DATETIME)>=CAST('" & FilterPeriod1.Text & " 00:00:00' AS DATETIME) AND CAST(retitemmstdate AS DATETIME)<=CAST('" & FilterPeriod2.Text & " 23:59:59' AS DATETIME)"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND transmststatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        cbStatus.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        FilterDDLStatus.SelectedIndex = -1
        BindTrnData("")
    End Sub
#End Region 

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If retitemmststatus.Text = "" Then
            showMessage("Please select Finish Good Transfer data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("t_retitem_h", "retitemmstoid", retitemmstoid.Text, "retitemmststatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                retitemmststatus.Text = "In Process"
                Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE FROM t_retitem_d WHERE cmpcode='" & CompnyCode & "' AND retitemmstoid=" & retitemmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM t_retitem_h WHERE cmpcode='" & CompnyCode & "' AND retitemmstoid=" & retitemmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnTWRetInvoice.aspx?awal=true")
    End Sub
End Class

