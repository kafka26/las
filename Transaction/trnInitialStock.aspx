﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnInitialStock.aspx.vb" Inherits="Transaction_trnInitialStock" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">

    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text=".: Initial Stock" CssClass="Title" ForeColor="Maroon" Font-Names="Verdana" Font-Size="21px"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="center"></td>
        </tr>
        <tr>
            <td align="center" valign="top" style="text-align: left;">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%"><ajaxToolkit:TabPanel runat="server" HeaderText="TabPanel2" ID="TabPanel1"><HeaderTemplate>
<asp:Image id="Image111esrdftybhjk" runat="server" ImageUrl="~/Images/List.gif" ImageAlign="AbsMiddle"></asp:Image> <STRONG><SPAN style="FONT-SIZE: 9pt">List of Init Stock :.</SPAN></STRONG>&nbsp; 
</HeaderTemplate>
<ContentTemplate>
    <div style="text-align: left">
        <table style="width: 100%">
            <tr>
                <td style="width: 100%">
<asp:UpdatePanel id="UpdatePanel1" runat="server"><ContentTemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 100px" id="tdPeriod1" align=left runat="server" visible="true"><asp:CheckBox id="chkPeriod" runat="server" Text="Periode" __designer:wfdid="w31" Visible="False"></asp:CheckBox></TD><TD id="tdperiod2" align=left colSpan=3 runat="server" visible="true"><asp:TextBox id="dateAwal" runat="server" CssClass="inpText" Width="75px" __designer:wfdid="w32" Visible="False"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w33" Visible="False"></asp:ImageButton>&nbsp;<asp:TextBox id="dateAkhir" runat="server" CssClass="inpText" Width="75px" __designer:wfdid="w34" Visible="False"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w35" Visible="False"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" ForeColor="Red" __designer:wfdid="w36" Visible="False">(MM/DD/YYYY)</asp:Label><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w37" Format="MM/dd/yyyy" PopupButtonID="imageButton1" TargetControlID="dateAwal"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w38" Format="MM/dd/yyyy" PopupButtonID="imageButton2" TargetControlID="dateAkhir"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" __designer:wfdid="w39" TargetControlID="dateAwal" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" __designer:wfdid="w40" TargetControlID="dateAkhir" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="WIDTH: 100px" id="Td1" align=left runat="server" visible="true"><asp:CheckBox id="chkGroup" runat="server" Text="Group" __designer:wfdid="w41"></asp:CheckBox></TD><TD id="Td2" align=left colSpan=3 runat="server" visible="true"><asp:DropDownList id="DDLfilterGroup" runat="server" CssClass="inpText" Width="230px" __designer:wfdid="w42"></asp:DropDownList>&nbsp; </TD></TR><TR><TD style="WIDTH: 100px" id="Td3" align=left runat="server" visible="true"><asp:CheckBox id="chkWH" runat="server" Text="Warehouse" __designer:wfdid="w43"></asp:CheckBox></TD><TD id="Td4" align=left colSpan=3 runat="server" visible="true"><asp:DropDownList id="DDLfilterWH" runat="server" CssClass="inpText" Width="230px" __designer:wfdid="w44"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 100px" id="Td5" align=left runat="server" visible="true"><asp:Label id="Label3" runat="server" Text="Filter Item :." __designer:wfdid="w45"></asp:Label></TD><TD id="Td6" align=left colSpan=3 runat="server" visible="true"><asp:DropDownList id="DDLitemname" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w46"><asp:ListItem Value="itemCode">Code</asp:ListItem>
<asp:ListItem Value="itemShortDescription">Short Desc</asp:ListItem>
<asp:ListItem Value="itemLongDescription">Long Desc</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="itemname" runat="server" CssClass="inpText" Width="250px" __designer:wfdid="w47"></asp:TextBox> <asp:ImageButton id="btnSearchItem" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="Top" __designer:wfdid="w48" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnEraseItem" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" __designer:wfdid="w49" Visible="False"></asp:ImageButton> <asp:Label id="itemoid" runat="server" __designer:wfdid="w50" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 100px" id="Td7" align=left runat="server" visible="true"></TD><TD id="Td8" align=left colSpan=3 runat="server" visible="true"><asp:ImageButton id="btnfind" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w51"></asp:ImageButton> <asp:ImageButton id="btnviewall" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w52"></asp:ImageButton> <asp:ImageButton id="imbLastSearch" runat="server" ImageUrl="~/Images/viewlast.png" ImageAlign="AbsMiddle" __designer:wfdid="w53" Visible="False"></asp:ImageButton></TD></TR><TR><TD id="Td9" align=left colSpan=4 runat="server" visible="true"><asp:GridView id="gvTblData" runat="server" Width="100%" __designer:wfdid="w26" GridLines="None" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" EnableModelValidation="True" BorderWidth="1px" BorderStyle="None">
<RowStyle BackColor="#E3EAEB"></RowStyle>

<EmptyDataRowStyle BackColor="DarkGray"></EmptyDataRowStyle>
<Columns>
<asp:BoundField DataField="nomor">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" ForeColor="#FF8000"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemCode" HeaderText="Code" HtmlEncode="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemoldcode" HeaderText="Old Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemLongDescription" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="saldoawal" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="refhpp" HeaderText="Amout IDR">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="refhppusd" HeaderText="Amount USD" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="mtr" HeaderText="Warehouse">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:ImageButton id="imbPrintFromList" runat="server" ImageUrl="~/Images/print.gif" ImageAlign="AbsMiddle" __designer:wfdid="w11" ToolTip=""></asp:ImageButton><ajaxToolkit:ConfirmButtonExtender id="cbePrintFromList" runat="server" TargetControlID="imbPrintFromList" __designer:wfdid="w12" ConfirmText="Are You Sure Print This Data ?">
                                                    </ajaxToolkit:ConfirmButtonExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="OrangeRed"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label3" runat="server" ForeColor="Red" Font-Bold="True" Text="No Data Found !!" __designer:wfdid="w10"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#C5BBAF" BorderStyle="None" ForeColor="#333333"></SelectedRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="cbInfo" runat="server" ForeColor="Red" Font-Bold="True" Text="Click Find to List Data !!" __designer:wfdid="w55"></asp:Label></TD></TR></TBODY></TABLE>
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="gvTblData"></asp:PostBackTrigger>
</Triggers>
</asp:UpdatePanel> 
                </td>
            </tr>
        </table>
    </div>
</ContentTemplate>
</ajaxToolkit:TabPanel>
<ajaxToolkit:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel2"><HeaderTemplate>
                            <asp:Image ID="Image221" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Initial Stock :.</span></strong>
                        
</HeaderTemplate>
<ContentTemplate>
                            <div style="WIDTH: 100%; TEXT-ALIGN: center">
                                <div style="width: 100%; text-align: left">
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="width: 100%">
                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                    <contenttemplate>
<TABLE style="WIDTH: 100%" __designer:dtid="562949953421338"><TBODY __designer:dtid="562949953421339"><TR><TD style="WIDTH: 100px" class="Label" align=left><asp:CheckBox id="CheckBox1" runat="server" __designer:dtid="562949953421343" __designer:wfdid="w25" Visible="False" AutoPostBack="True"></asp:CheckBox></TD><TD class="Label" align=left><asp:FileUpload id="FileUpload1" runat="server" __designer:dtid="562949953421344" Width="222px" __designer:wfdid="w26" Visible="False" Height="30px"></asp:FileUpload><asp:Button id="BtnUplaodFile" runat="server" __designer:dtid="562949953421345" Text="Upload" __designer:wfdid="w27" Visible="False"></asp:Button> <asp:Label id="FileExelInitStokSN" runat="server" __designer:dtid="562949953421346" __designer:wfdid="w28" Visible="False"></asp:Label> <asp:Label id="Label2" runat="server" __designer:dtid="562949953421342" Text="Init SN" __designer:wfdid="w29" Visible="False"></asp:Label></TD></TR><TR __designer:dtid="562949953421347"><TD style="WIDTH: 100px" class="Label" align=left __designer:dtid="562949953421348"><asp:Label id="Label9" runat="server" __designer:dtid="562949953421349" Text="Warehouse" __designer:wfdid="w30"></asp:Label> </TD><TD class="Label" align=left __designer:dtid="562949953421351"><asp:DropDownList id="DDLLocation" runat="server" __designer:dtid="562949953421352" CssClass="inpText" Width="300px" __designer:wfdid="w31"></asp:DropDownList> <asp:Label id="cutof" runat="server" Text="cutof" __designer:wfdid="w32" Visible="False"></asp:Label></TD></TR><TR __designer:dtid="562949953421353"><TD style="WIDTH: 100px" class="Label" align=left __designer:dtid="562949953421354"><asp:Label id="Label5" runat="server" __designer:dtid="562949953421355" Text="Group" __designer:wfdid="w33"></asp:Label> </TD><TD class="Label" align=left __designer:dtid="562949953421357"><asp:DropDownList id="DDLBusUnit" runat="server" __designer:dtid="562949953421358" CssClass="inpText" Width="300px" __designer:wfdid="w34"></asp:DropDownList> </TD></TR><TR __designer:dtid="562949953421359"><TD style="WIDTH: 100px" class="Label" align=left __designer:dtid="562949953421360"></TD><TD class="Label" align=left __designer:dtid="562949953421362"><asp:ImageButton id="btnLookUpMat" runat="server" ImageUrl="~/Images/btnfind.bmp" __designer:dtid="562949953421363" __designer:wfdid="w35"></asp:ImageButton>&nbsp; </TD></TR><TR><TD style="WIDTH: 100px" class="Label" align=left></TD><TD class="Label" align=left><asp:Label id="Label6" runat="server" ForeColor="Red" Font-Size="8pt" Font-Bold="True" Text="-- Click Find to List Item !!" __designer:wfdid="w36"></asp:Label></TD></TR><TR __designer:dtid="562949953421364"><TD class="Label" align=left colSpan=2 __designer:dtid="562949953421365"><asp:GridView id="gvDtl" runat="server" ForeColor="#333333" __designer:dtid="562949953421366" Width="100%" __designer:wfdid="w37" GridLines="None" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" EnableModelValidation="True" BorderWidth="1px" BorderStyle="None" DataKeyNames="seq,matqty,harga">
<PagerSettings FirstPageText="First" LastPageText="Last" __designer:dtid="562949953421367"></PagerSettings>

<RowStyle BackColor="#E3EAEB" __designer:dtid="562949953421368"></RowStyle>
<Columns __designer:dtid="562949953421369">
<asp:CommandField ShowEditButton="True" __designer:dtid="562949953421370">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" __designer:dtid="562949953421371"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" ForeColor="Sienna" __designer:dtid="562949953421372"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No." ReadOnly="True" __designer:dtid="562949953421373">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" __designer:dtid="562949953421374"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" __designer:dtid="562949953421375"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matcode" HeaderText="Code" ReadOnly="True" __designer:dtid="562949953421376">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" __designer:dtid="562949953421377"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" __designer:dtid="562949953421378"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="oldcode" HeaderText="Old Code" ReadOnly="True" __designer:dtid="562949953421379">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" __designer:dtid="562949953421380"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" __designer:dtid="562949953421381"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="longdesc" HeaderText="Description" ReadOnly="True" __designer:dtid="562949953421382">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" __designer:dtid="562949953421383"></HeaderStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Qty" __designer:dtid="562949953421384"><EditItemTemplate __designer:dtid="562949953421385">
                                                            <asp:TextBox __designer:dtid="562949953421386" ID="tbQty" runat="server" CssClass="inpText" Text='<%# eval("matqty") %>' Width="75px" MaxLength="16" Enabled='<%# eval("enableqty") %>'></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender __designer:dtid="562949953421387" ID="ftbeQtyLM" runat="server" TargetControlID="tbQty" ValidChars="1234567890.,">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        
</EditItemTemplate>
<ItemTemplate __designer:dtid="562949953421388">
<asp:Label id="lblQty" runat="server" __designer:dtid="562949953421389" Text='<%# eval("matqty") %>' __designer:wfdid="w7"></asp:Label> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" __designer:dtid="562949953421390"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" __designer:dtid="562949953421391"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Amount" __designer:dtid="562949953421392"><EditItemTemplate __designer:dtid="562949953421393">
<asp:TextBox id="tbAmount" runat="server" __designer:dtid="562949953421394" CssClass="inpText" Text='<%# eval("Harga") %>' Width="75px" __designer:wfdid="w9" Enabled='<%# eval("enableqty") %>' MaxLength="8"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftbeAmount" runat="server" __designer:dtid="562949953421395" __designer:wfdid="w10" TargetControlID="tbAmount" ValidChars="1234567890.,">
                                                            </ajaxToolkit:FilteredTextBoxExtender> 
</EditItemTemplate>
<ItemTemplate __designer:dtid="562949953421396">
<asp:Label id="Harga" runat="server" __designer:dtid="562949953421397" Text='<%# Eval("Harga") %>' __designer:wfdid="w8"></asp:Label> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" __designer:dtid="562949953421399"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" __designer:dtid="562949953421398"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="unit" HeaderText="Unit" ReadOnly="True" __designer:dtid="562949953421400">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" __designer:dtid="562949953421401"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" __designer:dtid="562949953421402"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unitoid" HeaderText="unitoid" ReadOnly="True" Visible="False" __designer:dtid="562949953421403"></asp:BoundField>
<asp:BoundField DataField="matwh" HeaderText="Warehouse" ReadOnly="True" __designer:dtid="562949953421404">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" __designer:dtid="562949953421405"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" __designer:dtid="562949953421406"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matwhoid" HeaderText="Warehouseoid" ReadOnly="True" Visible="False" __designer:dtid="562949953421407">
<HeaderStyle CssClass="gvhdr" __designer:dtid="562949953421409"></HeaderStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True" __designer:dtid="562949953421410">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" __designer:dtid="562949953421411"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red" __designer:dtid="562949953421412"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#1C5E55" ForeColor="White" __designer:dtid="562949953421413"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="White" __designer:dtid="562949953421414"></PagerStyle>

<SelectedRowStyle BackColor="#333333" Font-Bold="True" __designer:dtid="562949953421415"></SelectedRowStyle>

<HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" __designer:dtid="562949953421416"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR __designer:dtid="562949953421417"><TD style="COLOR: #585858" class="Label" align=left colSpan=2 __designer:dtid="562949953421418">Processed By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:dtid="562949953421419" __designer:wfdid="w38"></asp:Label> &nbsp;On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:dtid="562949953421420" __designer:wfdid="w39"></asp:Label> </TD></TR><TR __designer:dtid="562949953421421"><TD class="Label" align=left colSpan=2 __designer:dtid="562949953421422"><asp:ImageButton id="btnPost" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:dtid="562949953421423" __designer:wfdid="w40"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btnCancel.bmp" ImageAlign="AbsMiddle" __designer:dtid="562949953421424" __designer:wfdid="w41"></asp:ImageButton> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:ImageButton style="FONT-WEIGHT: 700" id="btnPreview" runat="server" ImageAlign="AbsMiddle" __designer:dtid="562949953421425" __designer:wfdid="w42" Visible="False" AlternateText=">> Upload excel example <<"></asp:ImageButton> </TD></TR><TR><TD class="Label" align=center colSpan=2><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w43" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w44"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE>&nbsp; 
</contenttemplate>
                                                    <triggers>
<asp:AsyncPostBackTrigger ControlID="btnFindListMat"></asp:AsyncPostBackTrigger>
</triggers>
                                                </asp:UpdatePanel></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        
</ContentTemplate>
</ajaxToolkit:TabPanel>
</ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <%-- <asp:UpdatePanel ID="upListMat" runat="server">
        <ContentTemplate>--%><asp:UpdatePanel ID="UPListMat" runat="server">
            <contenttemplate>
<asp:Panel id="pnlListMat" runat="server" __designer:dtid="1407374883553395" CssClass="modalBox" Width="800px" Visible="False" __designer:wfdid="w1"><TABLE style="WIDTH: 100%" __designer:dtid="1407374883553396"><TBODY __designer:dtid="1407374883553397"><TR __designer:dtid="1407374883553398"><TD class="Label" align=center colSpan=3 __designer:dtid="1407374883553399"><asp:Panel id="pnlFindListMat" runat="server" __designer:dtid="1407374883553403" Width="100%" __designer:wfdid="w19" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%" __designer:dtid="1407374883553404"><TBODY __designer:dtid="1407374883553405"><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Medium" Font-Bold="True" __designer:dtid="1407374883553400" __designer:wfdid="w20" Font-Underline="False">List Of Item</asp:Label></TD></TR><TR __designer:dtid="1407374883553406"><TD class="Label" align=center colSpan=3 __designer:dtid="1407374883553407">Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" __designer:dtid="1407374883553408" CssClass="inpText" Width="100px" __designer:wfdid="w21"><asp:ListItem Value="matcode" __designer:dtid="1407374883553410">Code</asp:ListItem>
<asp:ListItem Value="oldcode" __designer:dtid="1407374883553411">Old Code</asp:ListItem>
<asp:ListItem Value="longdesc" __designer:dtid="1407374883553412">Description</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" __designer:dtid="1407374883553413" CssClass="inpText" Width="200px" __designer:wfdid="w22"></asp:TextBox> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:dtid="1407374883553414" __designer:wfdid="w23"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:dtid="1407374883553415" __designer:wfdid="w24"></asp:ImageButton></TD></TR><TR __designer:dtid="1407374883553416"><TD class="Label" align=center colSpan=3 __designer:dtid="1407374883553417">&nbsp;&nbsp;&nbsp; <asp:ImageButton id="btnSelectAll" onclick="btnSelectAll_Click" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" __designer:wfdid="w27"></asp:ImageButton> <asp:ImageButton id="btnSelectNone" onclick="btnSelectNone_Click" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" __designer:wfdid="w28"></asp:ImageButton>&nbsp;&nbsp;<asp:ImageButton id="btnViewChecked" onclick="btnViewChecked_Click" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" __designer:wfdid="w29"></asp:ImageButton>&nbsp;<BR /><asp:UpdateProgress id="uprogListMat" runat="server" __designer:wfdid="w45" AssociatedUpdatePanelID="upListMat"><ProgressTemplate>
<TABLE style="WIDTH: 200px"><TBODY><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple" align=center>Load Data, Please Wait ...</TD></TR><TR><TD align=center><asp:Image id="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif" __designer:wfdid="w46"></asp:Image></TD></TR></TBODY></TABLE>
</ProgressTemplate>
</asp:UpdateProgress> <asp:CheckBox id="cbCat01" runat="server" __designer:dtid="1407374883553418" Text="Cat 1" Visible="False" __designer:wfdid="w25"></asp:CheckBox><asp:DropDownList id="DDLCat01" runat="server" __designer:dtid="1407374883553419" CssClass="inpText" Width="200px" AutoPostBack="True" Visible="False" __designer:wfdid="w26" OnSelectedIndexChanged="DDLCat01_SelectedIndexChanged"></asp:DropDownList> <asp:CheckBox id="cbCat02" runat="server" __designer:dtid="1407374883553420" Text="Cat 2" Visible="False" __designer:wfdid="w30"></asp:CheckBox> <asp:DropDownList id="DDLCat02" runat="server" __designer:dtid="1407374883553421" CssClass="inpText" Width="200px" AutoPostBack="True" Visible="False" __designer:wfdid="w31" OnSelectedIndexChanged="DDLCat02_SelectedIndexChanged"></asp:DropDownList> <asp:CheckBox id="cbCat03" runat="server" __designer:dtid="1407374883553424" Text="Cat 3" Visible="False" __designer:wfdid="w32"></asp:CheckBox> <asp:DropDownList id="DDLCat03" runat="server" __designer:dtid="1407374883553425" CssClass="inpText" Width="200px" AutoPostBack="True" Visible="False" __designer:wfdid="w33"></asp:DropDownList> <asp:CheckBox id="cbCat04" runat="server" __designer:dtid="1407374883553426" Text="Cat  4" Visible="False" __designer:wfdid="w36"></asp:CheckBox> <asp:DropDownList id="DDLCat04" runat="server" __designer:dtid="1407374883553427" CssClass="inpText" Width="200px" Visible="False" __designer:wfdid="w37"></asp:DropDownList></TD></TR></TBODY></TABLE>&nbsp; </asp:Panel></TD></TR><TR __designer:dtid="1407374883553428"><TD class="Label" align=center colSpan=3 __designer:dtid="1407374883553429"><asp:GridView id="gvListMat" runat="server" ForeColor="#333333" __designer:dtid="1407374883553430" Width="99%" __designer:wfdid="w16" BorderStyle="None" BorderWidth="1px" EnableModelValidation="True" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" GridLines="None" PageSize="8">
<PagerSettings FirstPageText="First" LastPageText="Last" __designer:dtid="1407374883553431"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333" __designer:dtid="1407374883553432"></RowStyle>
<Columns __designer:dtid="1407374883553433">
<asp:TemplateField __designer:dtid="1407374883553434"><HeaderTemplate __designer:dtid="1407374883553435">
<asp:CheckBox id="cbHdrLM" runat="server" __designer:dtid="1407374883553436" AutoPostBack="true" Visible="False" __designer:wfdid="w13" OnCheckedChanged="cbHdrLM_CheckedChanged"></asp:CheckBox> 
</HeaderTemplate>
<ItemTemplate __designer:dtid="1407374883553437">
<asp:CheckBox id="cbLM" runat="server" __designer:dtid="1407374883553438" __designer:wfdid="w1" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("matoid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" __designer:dtid="1407374883553439"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" __designer:dtid="1407374883553440"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="matcode" HeaderText="Code" __designer:dtid="1407374883553441">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" __designer:dtid="1407374883553442"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" __designer:dtid="1407374883553443"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="oldcode" HeaderText="Old Code">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="longdesc" HeaderText="Description" __designer:dtid="1407374883553447">
<HeaderStyle CssClass="gvpopup" __designer:dtid="1407374883553448"></HeaderStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Qty" __designer:dtid="1407374883553449"><ItemTemplate __designer:dtid="1407374883553450">
<asp:TextBox id="tbQty" runat="server" __designer:dtid="1407374883553451" CssClass="inpText" Text='<%# eval("matqty") %>' Width="100px" __designer:wfdid="w2" Enabled='<%# eval("enableqty") %>' MaxLength="6"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftbeQtyLM" runat="server" __designer:dtid="1407374883553452" __designer:wfdid="w3" TargetControlID="tbQty" ValidChars="1234567890.,">
                                        </ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup" __designer:dtid="1407374883553453"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" __designer:dtid="1407374883553454"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Init Value (IDR / ITEM)" __designer:dtid="1407374883553455"><ItemTemplate __designer:dtid="1407374883553456">
<asp:TextBox id="tbHarga" runat="server" __designer:dtid="1407374883553457" CssClass="inpText" Text='<%# Eval("Harga")%>' Width="100px" __designer:wfdid="w4" MaxLength="9" Enabled='<%# eval("enableqty") %>'></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftbeHargaLM" runat="server" __designer:dtid="1407374883553458" __designer:wfdid="w5" TargetControlID="tbHarga" ValidChars="1234567890.,">
                                        </ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup" __designer:dtid="1407374883553459"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" __designer:dtid="1407374883553460"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Unit"><ItemTemplate>
<asp:DropDownList id="DDLunit" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w2" ToolTip='<%# eval("unitoid") %>' Enabled="False"></asp:DropDownList> 
</ItemTemplate>

<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="warehouse" HeaderText="Warehouse" __designer:dtid="1407374883553466">
<HeaderStyle CssClass="gvpopup" __designer:dtid="1407374883553467"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="warehouseoid" HeaderText="Warehouseoid" Visible="False" __designer:dtid="1407374883553468">
<HeaderStyle CssClass="gvpopup" __designer:dtid="1407374883553469"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#CCCCCC" __designer:dtid="1407374883553470"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" ForeColor="White" __designer:dtid="1407374883553471"></PagerStyle>
<EmptyDataTemplate __designer:dtid="1407374883553474">
                                <asp:Label __designer:dtid="1407374883553475" ID="Label10" runat="server" Font-Bold="True" ForeColor="Red" Text="No Data Found !!"></asp:Label>
                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" __designer:dtid="1407374883553472"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" __designer:dtid="1407374883553473"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> &nbsp;</TD></TR><TR __designer:dtid="1407374883553476"><TD align=center colSpan=3 __designer:dtid="1407374883553477"><asp:LinkButton id="lbAddToListMat" runat="server" __designer:dtid="1407374883553478" __designer:wfdid="w17">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lbSelectAllToList" runat="server" __designer:dtid="1407374883553479" Visible="False" __designer:wfdid="w18">[ Select All To List ]</asp:LinkButton> <asp:LinkButton id="lbCloseListMat" runat="server" __designer:dtid="1407374883553480" __designer:wfdid="w19">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE><ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" __designer:dtid="562949953421431" __designer:wfdid="w20" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlListMat" PopupDragHandleControlID="lblTitleListMat" TargetControlID="btnHideListMat">
    </ajaxToolkit:ModalPopupExtender> </asp:Panel> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" __designer:dtid="562949953421432" Visible="False" __designer:wfdid="w21"></asp:Button> 
</contenttemplate>
            <triggers>
<asp:AsyncPostBackTrigger ControlID="btnFindListMat"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="DDLCat01"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="DDLCat02"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="DDLCat03"></asp:AsyncPostBackTrigger>
</triggers>
        </asp:UpdatePanel>
    &nbsp;<br />
    <%--  </ContentTemplate>
    </asp:UpdatePanel>--%>
    <asp:UpdatePanel ID="upPopUpMsg" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False" __designer:wfdid="w180"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True" __designer:wfdid="w181"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" __designer:wfdid="w182" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red" __designer:wfdid="w183"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle" __designer:wfdid="w184"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption" TargetControlID="bePopUpMsg" __designer:wfdid="w185" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False" __designer:wfdid="w186"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

