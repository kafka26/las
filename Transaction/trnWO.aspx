<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnWO.aspx.vb" Inherits="Transaction_WorkOrder" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Job Costing MO" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Job Costing MO :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="lblFilter" runat="server" Text="Filter"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="105px"><asp:ListItem Value="wom.womstoid">Draft No.</asp:ListItem>
<asp:ListItem Value="wom.wono">SPK No.</asp:ListItem>
<asp:ListItem Value="som.soitemno">SO No.</asp:ListItem>
<asp:ListItem Value="wom.womstres1">FG Code</asp:ListItem>
<asp:ListItem Value="wom.womstres2">FG Description</asp:ListItem>
<asp:ListItem Value="wom.upduser">Posted By</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="250px"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text="Period"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="80px" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label5" runat="server" Text="to"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="80px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="lblLabelPeriod" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbBusUnit" runat="server" Text="Business Unit"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLBusUnit" runat="server" CssClass="inpText" Width="360px"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbQtyKIK" runat="server" Text="Qty SPK"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLQtyKIK" runat="server" CssClass="inpText" Width="50px"><asp:ListItem>&gt;</asp:ListItem>
<asp:ListItem>&gt;=</asp:ListItem>
<asp:ListItem>=</asp:ListItem>
<asp:ListItem>&lt;&gt;</asp:ListItem>
<asp:ListItem>&lt;</asp:ListItem>
<asp:ListItem>&lt;=</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextQtyKIK" runat="server" CssClass="inpText" Width="100px" MaxLength="16"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status Doc."></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="105px"><asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
<asp:ListItem>Closed</asp:ListItem>
<asp:ListItem>Cancel</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbsts" runat="server" Text="Status Update" __designer:wfdid="w3"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="ddlstatus" runat="server" CssClass="inpText" Width="105px" __designer:wfdid="w4"><asp:ListItem>In Approval</asp:ListItem>
<asp:ListItem Value="Post">Approved</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" Format="MM/dd/yyyy" PopupButtonID="imbDate1" TargetControlID="FilterPeriod1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="FilterPeriod1" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" Format="MM/dd/yyyy" PopupButtonID="imbDate2" TargetControlID="FilterPeriod2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" TargetControlID="FilterPeriod2" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbKIKQty" runat="server" TargetControlID="FilterTextQtyKIK" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:LinkButton id="lkbWOInProcess" runat="server" Visible="False"></asp:LinkButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvTRN" runat="server" ForeColor="#333333" Width="100%" AllowSorting="True" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="8">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="womstoid" DataNavigateUrlFormatString="~\Transaction\trnWO.aspx?oid={0}" DataTextField="womstoid" HeaderText="Draft No." SortExpression="womstoid">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="wodate" HeaderText="Date" SortExpression="wodate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wono" HeaderText="SPK No." SortExpression="wono">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="soitemno" HeaderText="SO No." SortExpression="soitemno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdesc" HeaderText="FG Description" SortExpression="itemlongdesc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl1qty" HeaderText="Qty SPK" SortExpression="wodtl1qty" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="postuser" HeaderText="Posted By" SortExpression="postuser">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="womststatus" HeaderText="Status Doc." SortExpression="womststatus">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="wotempstatus" HeaderText="Status Update" SortExpression="wotempstatus">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbGVMst" runat="server" Checked='<%# eval("checkvalue") %>'></asp:CheckBox> <asp:Label id="lblOidGVMst" runat="server" Text='<%# eval("womstoid") %>' Visible="False"></asp:Label> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=4><asp:UpdateProgress id="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image12" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label166" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Job Costing MO Header" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="i_u" runat="server" CssClass="Important" Text="New Data" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="PeriodAcctg" runat="server" Visible="False"></asp:Label> <asp:Label id="cmpcode" runat="server" Visible="False"></asp:Label> <asp:RadioButton id="rbtipe1" runat="server" Text="Buffer Stock" Visible="False" Checked="True" GroupName="rb"></asp:RadioButton> <asp:RadioButton id="rbtipe2" runat="server" Text="Customer Order" Visible="False" GroupName="rb"></asp:RadioButton></TD><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="Label63" runat="server" Text="Tolerance"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="makloonoid" runat="server" Visible="False"></asp:Label> <asp:Label id="suppoid_bordir" runat="server" Visible="False" __designer:wfdid="w7"></asp:Label> <asp:TextBox id="wotolerance" runat="server" CssClass="inpText" Width="33px" MaxLength="6"></asp:TextBox></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:CheckBox id="cbAutoGenerate" runat="server" Text="Auto Generate Buffer" Checked="True" AutoPostBack="True" OnCheckedChanged="cbAutoGenerate_CheckedChanged"></asp:CheckBox></TD><TD class="Label" align=left><asp:Label id="Label6" runat="server" Text="Date"></asp:Label>&nbsp;<asp:Label id="Label7" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="wodate" runat="server" CssClass="inpText" Width="80px" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="imbPRDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label3" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblTrnNo" runat="server" Text="Draft No."></asp:Label>&nbsp;<asp:Label id="Label43" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:Label id="womstoid" runat="server"></asp:Label><asp:TextBox id="wono" runat="server" CssClass="inpTextDisabled" Width="260px" Visible="False" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label11" runat="server" Text="Ongkos Jahit"></asp:Label> <asp:Label id="Label64" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="dlcamt" runat="server" CssClass="inpText" Width="100px" MaxLength="16" AutoPostBack="True" OnTextChanged="dlcamt_TextChanged"></asp:TextBox> <asp:TextBox id="dlcamtPlus" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w1" Enabled="False"></asp:TextBox>&nbsp;Per Unit</TD></TR><TR><TD class="Label" align=left><asp:Label id="Label83" runat="server" Text="Jml. Kancing" __designer:wfdid="w2"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="txtJumlahKancing" runat="server" CssClass="inpText" Width="48px" __designer:wfdid="w3" MaxLength="16" AutoPostBack="True" OnTextChanged="txtJumlahKancing_TextChanged">7</asp:TextBox> Rp. <asp:TextBox id="txtRefPriceKancing" runat="server" CssClass="inpText" Width="128px" __designer:wfdid="w4" AutoPostBack="True" OnTextChanged="txtRefPriceKancing_TextChanged"></asp:TextBox>&nbsp;Per Pcs</TD><TD class="Label" align=left><asp:Label id="Label65" runat="server" Text="Total Kancing"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="kancingamt" runat="server" CssClass="inpTextDisabled" Width="100px" MaxLength="16" AutoPostBack="True" Enabled="False" OnTextChanged="kancingamt_TextChanged"></asp:TextBox>&nbsp;Per Unit&nbsp;<asp:CheckBox id="cbKancing_use_ap" runat="server" Text="Hutang"></asp:CheckBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label27" runat="server" Text="Bordir/Sablon"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="ohdamt" runat="server" CssClass="inpText" Width="100px" MaxLength="16" AutoPostBack="True" OnTextChanged="ohdamt_TextChanged"></asp:TextBox>&nbsp;Per Unit <asp:CheckBox id="cbBordir_use_ap" runat="server" Text="Inc. Hutang" __designer:wfdid="w1"></asp:CheckBox></TD><TD class="Label" align=left><asp:Label id="Label66" runat="server" Text="Kain"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="kainamt" runat="server" CssClass="inpText" Width="100px" MaxLength="16" AutoPostBack="True" OnTextChanged="kainamt_TextChanged"></asp:TextBox>&nbsp;Per Unit <asp:CheckBox id="cbKain_use_ap" runat="server" Text="Inc. Hutang"></asp:CheckBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label67" runat="server" Text="Label"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="labelamt" runat="server" CssClass="inpText" Width="100px" MaxLength="16" AutoPostBack="True" OnTextChanged="labelamt_TextChanged"></asp:TextBox> <asp:TextBox id="txtRefPriceLabel" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w20" Enabled="False"></asp:TextBox> <asp:CheckBox id="cbLabel_use_ap" runat="server" Text="Hutang"></asp:CheckBox></TD><TD class="Label" align=left><asp:Label id="Label68" runat="server" Text="Other"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="othervalue" runat="server" CssClass="inpText" Width="100px" MaxLength="16" AutoPostBack="True" OnTextChanged="othervalue_TextChanged"></asp:TextBox> Per Unit <asp:CheckBox id="cbOther_use_ap" runat="server" Text="Inc. Hutang"></asp:CheckBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label4" runat="server" Text="Ref. No."></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="wodocrefno" runat="server" CssClass="inpText" Width="300px" MaxLength="50"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label57" runat="server" Text="Penjahit"></asp:Label> <asp:Label id="Label58" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="makloonname" runat="server" CssClass="inpTextDisabled" Width="182px" MaxLength="50" Enabled="False"></asp:TextBox> <asp:ImageButton id="ImbSearchMakloon" onclick="ImbSearchMakloon_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="ImbEraseMakloon" onclick="ImbEraseMakloon_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="Label84" runat="server" Text="No. Transfer" __designer:wfdid="w2"></asp:Label>&nbsp;<asp:Label id="Label85" runat="server" CssClass="Important" Text="*" __designer:wfdid="w3"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="ddlNoTransfer" runat="server" CssClass="inpText" Width="305px" __designer:wfdid="w4"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label69" runat="server" Text="Bordir" __designer:wfdid="w1"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="bordiramt" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w2" MaxLength="16" AutoPostBack="True" OnTextChanged="bordiramt_TextChanged"></asp:TextBox>&nbsp;Per Unit</TD><TD class="Label" align=left><asp:Label id="Label70" runat="server" Text="Supplier Bordir" __designer:wfdid="w3"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="suppname_bordir" runat="server" CssClass="inpTextDisabled" Width="182px" __designer:wfdid="w4" MaxLength="50" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSearchSupp" onclick="btnSearchSupp_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w5"></asp:ImageButton> <asp:ImageButton id="btnEraseSupp" onclick="btnEraseSupp_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w6"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label59" runat="server" Text="Start Date"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="startdate" runat="server" CssClass="inpText" Width="80px" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="ImbStartDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label61" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label60" runat="server" Text="Finish Date"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="finishdate" runat="server" CssClass="inpText" Width="80px" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="ImbEndDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label62" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label8" runat="server" Text="Header Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="womstnote" runat="server" CssClass="inpText" Width="300px" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label9" runat="server" Text="Status"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="womststatus" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox> <asp:TextBox id="wotempstatus" runat="server" CssClass="inpTextDisabled" Width="100px" Visible="False" __designer:wfdid="w2" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label71" runat="server" ForeColor="Sienna" Font-Size="8pt" Font-Bold="False" Text="Simulasi Perhitungan HPP" Font-Underline="False" __designer:wfdid="w1" Font-Italic="True"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label72" runat="server" Text="Total Barang Jadi" __designer:wfdid="w2"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="txtQtyFG" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w3" Enabled="False"></asp:TextBox>&nbsp;<asp:Label id="Label75" runat="server" Text="Potong" __designer:wfdid="w2"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label73" runat="server" Text="Material + Jasa" __designer:wfdid="w2"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="txtTotalBahan" runat="server" CssClass="inpTextDisabled" Width="182px" __designer:wfdid="w5" MaxLength="50" Enabled="False"></asp:TextBox>&nbsp;<asp:Label id="Label80" runat="server" Text="Rupiah" __designer:wfdid="w14"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label74" runat="server" Text="HPP" __designer:wfdid="w2"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="txtHPP" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w8" Enabled="False"></asp:TextBox>&nbsp;<asp:Label id="Label76" runat="server" Text="Potong" __designer:wfdid="w9"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label77" runat="server" Text="Margin" __designer:wfdid="w2"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="txtMargin" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w11" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label78" runat="server" Text="Harga Jual Std" __designer:wfdid="w2"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="txtHargaJual" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w13" Enabled="False"></asp:TextBox>&nbsp;<asp:Label id="Label79" runat="server" Text="Potong" __designer:wfdid="w14"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label81" runat="server" Text="Harga Jual Final" __designer:wfdid="w2"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="txtHargaJualFinal" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w17" MaxLength="16" AutoPostBack="True" OnTextChanged="txtHargaJualFinal_TextChanged"></asp:TextBox>&nbsp;<asp:Label id="Label82" runat="server" Text="Potong" __designer:wfdid="w18"></asp:Label></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label21" runat="server" Text="Business Unit" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpText" Width="305px" Visible="False" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><ajaxToolkit:CalendarExtender id="ceDate" runat="server" TargetControlID="wodate" PopupButtonID="imbPRDate" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeDate" runat="server" TargetControlID="wodate" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="FtbDlc" runat="server" TargetControlID="dlcamt" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FtbOhd" runat="server" TargetControlID="ohdamt" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:CalendarExtender id="CeStartDate" runat="server" TargetControlID="startdate" PopupButtonID="ImbStartDate" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CeFinDate" runat="server" TargetControlID="finishdate" PopupButtonID="ImbEndDate" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meestartdate" runat="server" TargetControlID="startdate" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftetolerance" runat="server" TargetControlID="wotolerance" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:MaskedEditExtender id="meefindate" runat="server" TargetControlID="finishdate" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:FilteredTextBoxExtender id="FtbKancing" runat="server" TargetControlID="kancingamt" ValidChars="1234567890,.">
    </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FtbKain" runat="server" TargetControlID="kainamt" ValidChars="1234567890,.">
        </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FtbLabel" runat="server" TargetControlID="labelamt" ValidChars="1234567890,.">
        </ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6 rowSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><asp:Label id="Label173" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Job Costing MO Detail" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><asp:MultiView id="MultiView1" runat="server"><asp:View id="View1" runat="server"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label2" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Detail Finish Good" Font-Underline="False"></asp:Label>&nbsp;<asp:Label id="Label12" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="|" Font-Underline="False"></asp:Label>&nbsp;<asp:LinkButton id="lkbDetProc1" runat="server" ForeColor="SteelBlue" Font-Size="8pt" Font-Bold="True">Detail Process</asp:LinkButton>&nbsp;<asp:Label id="Label13" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="|" Font-Underline="False"></asp:Label>&nbsp;<asp:LinkButton id="lkbDetMat1" runat="server" ForeColor="SteelBlue" Font-Size="8pt" Font-Bold="True">Detail Material</asp:LinkButton></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="i_u2" runat="server" Text="New Detail" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="wodtl1seq" runat="server" Visible="False">1</asp:Label> <asp:Label id="wodtl1oid" runat="server" Visible="False"></asp:Label> <asp:Label id="soitemmstoid" runat="server" Visible="False"></asp:Label> <asp:Label id="soitemdtloid" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="itemoid" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="itemcode" runat="server" Visible="False"></asp:Label> <asp:Label id="bomoid" runat="server" Visible="False"></asp:Label> <asp:Label id="groupoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label36" runat="server" Text="SO No."></asp:Label>&nbsp;<asp:Label id="Label37" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="soitemno" runat="server" CssClass="inpTextDisabled" Width="260px" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchSO" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearSO" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="Label52" runat="server" Text="Finish Good"></asp:Label>&nbsp;<asp:Label id="Label56" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="itemshortdesc" runat="server" CssClass="inpTextDisabled" Width="260px" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearItem" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label14" runat="server" Text="Qty"></asp:Label>&nbsp;<asp:Label id="Label28" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="wodtl1qty" runat="server" CssClass="inpText" Width="100px" MaxLength="16"></asp:TextBox>&nbsp;- <asp:DropDownList id="wodtl1unitoid" runat="server" CssClass="inpTextDisabled" Width="145px" Enabled="False"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label38" runat="server" Text="SO Qty"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="soitemqty" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label10" runat="server" Text="Area" Visible="False"></asp:Label> <asp:Label id="Label54" runat="server" CssClass="Important" Text="*" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="wodtl1area" runat="server" CssClass="inpText" Width="100px" Visible="False" MaxLength="16"></asp:TextBox> <asp:Label id="Label55" runat="server" CssClass="Important" Text="M2" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label40" runat="server" Text="Rounding Qty"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="itemlimitqty" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label152" runat="server" Text="Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="wodtl1note" runat="server" CssClass="inpText" Width="300px" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label29" runat="server" Text="Division"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="groupdesc" runat="server" CssClass="inpTextDisabled" Width="300px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="wodtl1totalmatamt" runat="server" Visible="False"></asp:Label> <asp:Label id="wodtl1dlcamt" runat="server" Visible="False"></asp:Label> <asp:Label id="wodtl1ohdamt" runat="server" Visible="False"></asp:Label> <asp:Label id="curroid_ohd" runat="server" Visible="False"></asp:Label> <asp:Label id="wodtl1ohdamtidr" runat="server" Visible="False"></asp:Label> <asp:Label id="wodtl1marginamt" runat="server" Visible="False"></asp:Label> <asp:Label id="wodtl1precostamt" runat="server" Visible="False"></asp:Label> <asp:Label id="curroid_precost" runat="server" Visible="False"></asp:Label> <asp:Label id="wodtl1precostamtidr" runat="server" Visible="False"></asp:Label> <asp:Label id="lastupduser_precost" runat="server" Visible="False"></asp:Label> <asp:Label id="lastupdtime_precost" runat="server" Visible="False"></asp:Label> <asp:Label id="lastupduser_bom" runat="server" Visible="False"></asp:Label> <asp:Label id="lastupdtime_bom" runat="server" Visible="False"></asp:Label> <asp:Label id="lastupduser_dlc" runat="server" Visible="False"></asp:Label> <asp:Label id="lastupdtime_dlc" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="ftbDtl1Qty" runat="server" TargetControlID="wodtl1qty" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbArea" runat="server" TargetControlID="wodtl1area" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnAddToList1" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClear1" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:GridView id="GVDtl1" runat="server" ForeColor="#333333" Width="100%" PageSize="5" GridLines="None" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="wodtl1seq">
<RowStyle BackColor="#E3EAEB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="soitemno" HeaderText="SO No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="FG Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemshortdesc" HeaderText="FG Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl1qty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl1unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl1area" HeaderText="Area" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl1note" HeaderText="Detail Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#666666" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#7C6F57"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR></TBODY></TABLE></asp:View><asp:View id="View2" runat="server"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left colSpan=6><asp:LinkButton id="lkbDetItem1" runat="server" ForeColor="SteelBlue" Font-Size="8pt" Font-Bold="True">Detail Finish Good</asp:LinkButton>&nbsp;<asp:Label id="Label24" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="|" Font-Underline="False"></asp:Label>&nbsp;<asp:Label id="Label32" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Detail Process" Font-Underline="False"></asp:Label>&nbsp;<asp:Label id="Label31" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="|" Font-Underline="False"></asp:Label> <asp:LinkButton id="lkbDetMat2" onclick="lkbDetMat_Click" runat="server" ForeColor="SteelBlue" Font-Size="8pt" Font-Bold="True">Detail Material</asp:LinkButton></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="i_u4" runat="server" Text="New Detail" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="wodtl2seq" runat="server" Visible="False">1</asp:Label> <asp:Label id="wodtl2oid" runat="server" Visible="False"></asp:Label> <asp:Label id="bomdtl1oid" runat="server" Visible="False"></asp:Label> <asp:Label id="wodtl2desc" runat="server" Visible="False"></asp:Label> <asp:Label id="wodtl2code" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 14%" class="Label" align=left></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label33" runat="server" Text="Sequence"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="wodtl2procseq" runat="server" CssClass="inpTextDisabled" Width="100px" AutoPostBack="True" Enabled="False" OnTextChanged="wodtl2procseq_TextChanged">1</asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label34" runat="server" Text="Process Type"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="wodtl2type" runat="server" CssClass="inpTextDisabled" Width="105px" AutoPostBack="True" Enabled="False" OnSelectedIndexChanged="wodtl2type_SelectedIndexChanged">
<asp:ListItem Value="FG">FG</asp:ListItem>
<asp:ListItem Enabled="False">WIP</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label46" runat="server" Text="From Department"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="deptoid" runat="server" CssClass="inpTextDisabled" Width="305px" Enabled="False"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="lbltodept" runat="server" Text="To Department"></asp:Label></TD><TD class="Label" align=center><asp:Label id="septtodept" runat="server" Text=":"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="todeptoid" runat="server" CssClass="inpTextDisabled" Width="305px" Enabled="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label35" runat="server" Text="Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="wodtl2note" runat="server" CssClass="inpText" Width="300px" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label42" runat="server" Text="Rounding Qty"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="processlimitqty" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD><TD class="Label" align=left><asp:Label id="Label44" runat="server" Text="Qty" Visible="False"></asp:Label><asp:Label id="Label45" runat="server" CssClass="Important" Text="*" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="wodtl2unitoid" runat="server" CssClass="inpTextDisabled" Width="105px" Visible="False" Enabled="False"></asp:DropDownList> <asp:TextBox id="wodtl2qty" runat="server" CssClass="inpTextDisabled" Width="100px" Visible="False" MaxLength="16" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="ftbDtl2Qty" runat="server" TargetControlID="wodtl2qty" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnAddToList2" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnClear2" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:GridView id="GVDtl2" runat="server" ForeColor="#333333" Width="100%" PageSize="5" GridLines="None" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="wodtl2seq">
<RowStyle BackColor="#E3EAEB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="wodtl2procseq" HeaderText="Sequence">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl2fgcode" HeaderText="FG Code">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl2type" HeaderText="Process Type">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="deptname" HeaderText="From Department">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="todeptname" HeaderText="To Department">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl2qty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl2unit" HeaderText="Unit" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl2note" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#666666" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#7C6F57"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR></TBODY></TABLE></asp:View><asp:View id="View3" runat="server"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left colSpan=6><asp:LinkButton id="lkbDetItem2" onclick="lkbDetItem_Click" runat="server" ForeColor="SteelBlue" Font-Size="8pt" Font-Bold="True">Detail Finish Good</asp:LinkButton>&nbsp;<asp:Label id="Label23" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="|" Font-Underline="False"></asp:Label>&nbsp;<asp:LinkButton id="lkbDetProc2" onclick="lkbDetProc_Click" runat="server" ForeColor="SteelBlue" Font-Size="8pt" Font-Bold="True">Detail Process</asp:LinkButton>&nbsp;<asp:Label id="Label19" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="|" Font-Underline="False"></asp:Label> <asp:Label id="Label18" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Detail Material" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Panel id="pnlMatDtl5" runat="server" BorderColor="Silver" Width="100%" BorderStyle="Outset" BorderWidth="1px"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label41" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Detail Material Level 5" Font-Underline="False"></asp:Label> <asp:Label id="Label15" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="|" Font-Underline="False" Visible="False"></asp:Label> <asp:LinkButton id="lbLevel03" runat="server" ForeColor="SteelBlue" Font-Size="8pt" Font-Bold="True" Visible="False">Detail Material Level 2</asp:LinkButton></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="i_u3" runat="server" Text="New Detail" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="wodtl3seq" runat="server" Visible="False">1</asp:Label> <asp:Label id="wodtl3oid" runat="server" Visible="False"></asp:Label> <asp:Label id="bomdtl2oid" runat="server" Visible="False"></asp:Label> <asp:Label id="wodtl3refoid" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 14%" class="Label" align=left></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="wodtl3refcode" runat="server" Visible="False"></asp:Label> <asp:Label id="wodtl3qtyref" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label39" runat="server" Text="Process"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLProcess" runat="server" CssClass="inpTextDisabled" Width="260px" Enabled="False" EnableTheming="True"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label20" runat="server" Text="Mat. Type"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="wodtl3reftype" runat="server" CssClass="inpText" Width="105px"><asp:ListItem>Raw</asp:ListItem>
<asp:ListItem Value="FG">Finish Good</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label22" runat="server" Text="Material"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="wodtl3refshortdesc" runat="server" CssClass="inpTextDisabled" Width="255px" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchMat" onclick="btnSearchMat_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseMat" onclick="btnEraseMat_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="Label25" runat="server" Text="Qty"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="wodtl3qty" runat="server" CssClass="inpText" Width="100px"></asp:TextBox>&nbsp;- <asp:DropDownList id="wodtl3unitoid" runat="server" CssClass="inpTextDisabled" Width="105px" Enabled="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label26" runat="server" Text="Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="wodtl3note" runat="server" CssClass="inpText" Width="255px" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label30" runat="server" Text="Finih Good"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLbom" runat="server" CssClass="inpTextDisabled" Width="260px" Enabled="False"></asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6><asp:Label id="itemcat1" runat="server" Visible="False"></asp:Label> <asp:Label id="itemcat2" runat="server" Visible="False"></asp:Label> <asp:Label id="itemcat3" runat="server" Visible="False"></asp:Label>&nbsp;<asp:Label id="roundqtydtl3" runat="server" Visible="False"></asp:Label> <asp:Label id="txtRefValue" runat="server" Visible="False" __designer:wfdid="w5"></asp:Label> <asp:Label id="txtRefAmt" runat="server" Visible="False" __designer:wfdid="w5"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnAddToList" onclick="btnAddToList_Click" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClear3" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:GridView id="GVDtl3" runat="server" ForeColor="#333333" Width="100%" PageSize="5" GridLines="None" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="wodtl3seq">
<RowStyle BackColor="#E3EAEB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="wodtl3fgcode" HeaderText="FG Code" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl2desc" HeaderText="Process" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl3reftype" HeaderText="Mat. Type">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl3seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl3refcode" HeaderText="Mat. Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl3refshortdesc" HeaderText="Mat. Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl3qty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl3unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl3stockqty" HeaderText="Stock Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl3note" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl3value" HeaderText="Price">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl3amt" HeaderText="Total">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#666666" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#7C6F57"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR></TBODY></TABLE></asp:Panel></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Panel id="pnlMatDtl3" runat="server" BorderColor="Silver" Width="100%" BorderStyle="Outset" BorderWidth="1px"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left colSpan=6><asp:LinkButton id="lbLevel05" runat="server" ForeColor="SteelBlue" Font-Size="8pt" Font-Bold="True">Detail Material Level 5</asp:LinkButton> <asp:Label id="Label17" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="|" Font-Underline="False"></asp:Label> <asp:Label id="Label16" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Detail Material Level 2" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="i_u5" runat="server" Text="New Detail" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="wodtl4seq" runat="server" Visible="False">1</asp:Label> <asp:Label id="wodtl4oid" runat="server" Visible="False"></asp:Label> <asp:Label id="bomdtl3oid" runat="server" Visible="False"></asp:Label> <asp:Label id="wodtl4refoid" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 14%" class="Label" align=left></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="wodtl4refcode" runat="server" Visible="False"></asp:Label> <asp:Label id="wodtl4qtyref" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label47" runat="server" Text="Process"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLProcess2" runat="server" CssClass="inpTextDisabled" Width="305px" Enabled="False"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label48" runat="server" Text="Mat. Type"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="wodtl4reftype" runat="server" CssClass="inpTextDisabled" Width="105px" Enabled="False"><asp:ListItem>Raw</asp:ListItem>
<asp:ListItem>General</asp:ListItem>
<asp:ListItem>Spare Part</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label49" runat="server" Text="Material"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="wodtl4refshortdesc" runat="server" CssClass="inpTextDisabled" Width="300px" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label51" runat="server" Text="Qty"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="wodtl4qty" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox>&nbsp;- <asp:DropDownList id="wodtl4unitoid" runat="server" CssClass="inpTextDisabled" Width="105px" Enabled="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label53" runat="server" Text="Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="wodtl4note" runat="server" CssClass="inpText" Width="300px" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnClear4" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:GridView id="GVDtl4" runat="server" ForeColor="#333333" Width="100%" PageSize="5" GridLines="None" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="wodtl4seq">
<RowStyle BackColor="#E3EAEB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="wodtl2desc" HeaderText="Process">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl4reftype" HeaderText="Mat. Type">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl4refcode" HeaderText="Mat. Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl4refshortdesc" HeaderText="Mat. Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl4qty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl4unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl4note" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#666666" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#7C6F57"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR></TBODY></TABLE></asp:Panel></TD></TR></TBODY></TABLE></asp:View> </asp:MultiView></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=6></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPost" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton id="btnUpdate" onclick="btnUpdate_Click" runat="server" ImageUrl="~/Images/Update.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w1" AlternateText="Delete"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSendApp" runat="server" ImageUrl="~/Images/sendapproval.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w1" AlternateText="Delete" OnClick="btnSendApp_Click"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Job Costing MO :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upListSO" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListSO" runat="server" CssClass="modalBox" Width="700px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSO" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Sales Order" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListSO" runat="server" Width="100%" DefaultButton="btnFindListSO">Filter : <asp:DropDownList id="FilterDDLListSO" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="som.sono">SO No.</asp:ListItem>
<asp:ListItem Value="c.custname">Customer</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListSO" runat="server" CssClass="inpText" Width="200px"></asp:TextBox> <asp:ImageButton id="btnFindListSO" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListSO" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListSO" runat="server" ForeColor="#333333" Width="99%" PageSize="5" GridLines="None" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="soitemmstoid,soitemno,groupoid,groupdesc" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="soitemno" HeaderText="SO No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="soitemdate" HeaderText="SO Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="soitemtype" HeaderText="SO Type">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="somstnote" HeaderText="Header Note">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                                                <asp:Label ID="Label114" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbCloseListSO" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSO" runat="server" TargetControlID="btnHideListSO" Drag="True" PopupControlID="pnlListSO" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListSO"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListSO" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upListSupp" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListSupp" runat="server" CssClass="modalBox" Width="650px" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSupp" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Makloon"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListSupp" runat="server" Width="100%" DefaultButton="btnFindListSupp">Filter : <asp:DropDownList id="FilterDDLListSupp" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="suppname">Name</asp:ListItem>
<asp:ListItem Value="suppcode">Code</asp:ListItem>
<asp:ListItem Value="suppaddr">Address</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListSupp" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> &nbsp;<asp:ImageButton id="btnFindListSupp" onclick="btnFindListSupp_Click" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListSupp" onclick="btnAllListSupp_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListSupp" runat="server" ForeColor="#333333" Width="98%" PageSize="5" GridLines="None" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="suppoid,suppcode,suppname,supppaymentoid,supptaxable" AllowPaging="True" OnSelectedIndexChanged="gvListSupp_SelectedIndexChanged" AllowSorting="True" OnPageIndexChanging="gvListSupp_PageIndexChanging">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Code" SortExpression="suppcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Name" SortExpression="suppname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Width="40%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="40%"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label50" runat="server" CssClass="Important" Text="Data Not Found!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListSupp" onclick="lkbCloseListSupp_Click" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSupp" runat="server" TargetControlID="btnHideListSupp" PopupDragHandleControlID="lblListSupp" BackgroundCssClass="modalBackground" PopupControlID="pnlListSupp" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListSupp" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel><asp:UpdatePanel id="upListSupp2" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListSupp2" runat="server" CssClass="modalBox" Width="650px" Visible="False" __designer:wfdid="w24"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSupp2" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Makloon" __designer:wfdid="w3"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListSupp2" runat="server" Width="100%" __designer:wfdid="w26" DefaultButton="btnFindListSupp2">Filter : <asp:DropDownList id="FilterDDLListSupp2" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w27"><asp:ListItem Value="suppname">Name</asp:ListItem>
<asp:ListItem Value="suppcode">Code</asp:ListItem>
<asp:ListItem Value="suppaddr">Address</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListSupp2" runat="server" CssClass="inpText" Width="150px" __designer:wfdid="w28"></asp:TextBox> &nbsp;<asp:ImageButton id="btnFindListSupp2" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w29" OnClick="btnFindListSupp2_Click"></asp:ImageButton> <asp:ImageButton id="btnAllListSupp2" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w30" OnClick="btnAllListSupp2_Click"></asp:ImageButton></asp:Panel></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListSupp2" runat="server" ForeColor="#333333" Width="98%" __designer:wfdid="w31" DataKeyNames="suppoid,suppcode,suppname,supppaymentoid,supptaxable" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="5" OnSelectedIndexChanged="gvListSupp2_SelectedIndexChanged" AllowPaging="True" OnPageIndexChanging="gvListSupp2_PageIndexChanging" AllowSorting="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Code" SortExpression="suppcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Name" SortExpression="suppname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Width="40%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="40%"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label50" runat="server" CssClass="Important" Text="Data Not Found!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListSupp2" runat="server" __designer:wfdid="w32" OnClick="lkbCloseListSupp2_Click">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSupp2" runat="server" __designer:wfdid="w33" TargetControlID="btnHideListSupp2" PopupDragHandleControlID="lblListSupp2" BackgroundCssClass="modalBackground" PopupControlID="pnlListSupp2" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListSupp2" runat="server" Visible="False" __designer:wfdid="w34"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upListItem" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListItem" runat="server" CssClass="modalBox" Width="800px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListItem" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Finish Good" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListItem" runat="server" Width="100%" DefaultButton="btnFindListItem">Filter : <asp:DropDownList id="FilterDDLListItem" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="i.itemcode">Code</asp:ListItem>
<asp:ListItem Value="i.itemlongdescription">Description</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListItem" runat="server" CssClass="inpText" Width="200px"></asp:TextBox> <asp:ImageButton id="btnFindListItem" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListItem" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListItem" runat="server" ForeColor="#333333" Width="99%" PageSize="5" GridLines="None" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="soitemdtloid,itemoid,itemcode,itemlongdesc,soitemunitoid,soitemqty,bomoid,itemlimitqty,itemres2,precosttotalmatamt,precostdlcamt,precostoverheadamt,precostmarginamt,precostsalesprice,precostoverheadamtidr,precostsalespriceidr,curroid_overhead,curroid_salesprice,lastupduser_precost,lastupdtime_precost,lastupduser_bom,lastupdtime_bom,lastupduser_dlc,lastupdtime_dlc,formula" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbSelectMat" runat="server" ToolTip='<%# eval("soitemdtloid") %>' Checked='<%# eval("CheckValue") %>'></asp:CheckBox>
</ItemTemplate>

<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="SO Qty"><ItemTemplate>
<asp:TextBox id="tbMatQty" runat="server" CssClass="inpText" Text='<%# eval("soitemqty") %>' Width="50px" MaxLength="18"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftbeQty" runat="server" TargetControlID="tbMatQty" ValidChars="1234567890."></ajaxToolkit:FilteredTextBoxExtender>
</ItemTemplate>

<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="soitemunit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="formula" HeaderText="BOM">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="sodtlnote" HeaderText="Detail Note">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="stockqty1" HeaderText="Stock Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label114" runat="server" CssClass="Important" Text="No data found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbAddListMat" runat="server">[ Add To List ]</asp:LinkButton>&nbsp;- <asp:LinkButton id="lkbCloseListItem" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListItem" runat="server" TargetControlID="btnHideListItem" PopupDragHandleControlID="lblTitleListItem" BackgroundCssClass="modalBackground" PopupControlID="pnlListItem" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListItem" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upListMat" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListMat2" runat="server" CssClass="modalBox" Width="900px" Visible="False" DefaultButton="btnFindListMat2"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat2" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Material" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat2" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="matrefcode">Code</asp:ListItem>
<asp:ListItem Value="matrefshortdesc">Description</asp:ListItem>
<asp:ListItem Value="matrefunit">Unit</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat2" runat="server" CssClass="inpText" Width="200px"></asp:TextBox> <asp:ImageButton id="btnFindListMat2" onclick="btnFindListMat2_Click" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat2" onclick="btnAllListMat2_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:CheckBox id="cbCat01ListMat" runat="server" Text="Cat. 1 :"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat01ListMat" runat="server" CssClass="inpText" Width="200px" AutoPostBack="True"></asp:DropDownList> <asp:CheckBox id="cbCat02ListMat" runat="server" Text="Cat. 2 :"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat02ListMat" runat="server" CssClass="inpText" Width="200px" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:CheckBox id="cbCat03ListMat" runat="server" Text="Cat. 3 :"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat03ListMat" runat="server" CssClass="inpText" Width="200px" AutoPostBack="True"></asp:DropDownList> <asp:CheckBox id="cbCat04ListMat" runat="server" Text="Cat. 4 :"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat04ListMat" runat="server" CssClass="inpText" Width="200px"></asp:DropDownList></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:UpdateProgress id="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat"><ProgressTemplate>
<TABLE style="WIDTH: 200px"><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple">Load Data, Please Wait ...</TD></TR><TR><TD><asp:Image id="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif"></asp:Image></TD></TR></TABLE>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat2" runat="server" ForeColor="#333333" Width="99%" DataKeyNames="matrefoid,matrefcode,matrefshortdesc,bomdtl2oid" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="5" AllowPaging="True" OnSelectedIndexChanged="gvListMat2_SelectedIndexChanged">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
    <asp:TemplateField>
        <ItemTemplate>
            <asp:CheckBox ID="cbListMat2" runat="server" Checked='<%# eval("checkvalue") %>'
                ToolTip='<%# eval("matrefoid") %>' />
        </ItemTemplate>
        <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center" Width="5%" />
        <ItemStyle HorizontalAlign="Center" Width="5%" />
    </asp:TemplateField>
<asp:BoundField DataField="matrefcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matrefshortdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Qty/FG"><ItemTemplate>
<asp:TextBox id="tbBOMQty" runat="server" CssClass="inpText" Text='<%# eval("bomdtl2qty") %>' Width="50px" MaxLength="16"></asp:TextBox>&nbsp;<ajaxToolkit:FilteredTextBoxExtender id="ftbQtyGV" runat="server" TargetControlID="tbBOMQty" ValidChars="1234567890,.">
</ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Unit"><ItemTemplate>
<asp:DropDownList id="DDLUnit" runat="server" CssClass="inpTextDisabled" Width="80px" ToolTip="<%# GetParameterID() %>" Enabled="False"></asp:DropDownList>
</ItemTemplate>

<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Note"><ItemTemplate>
<asp:TextBox id="tbBOMNote" runat="server" CssClass="inpText" Text='<%# eval("bomdtl2note") %>' Width="150px" MaxLength="100"></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="roundqtydtl2" HeaderText="Round Qty" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 15px" align=center colSpan=3><asp:LinkButton id="lkbAddToList" onclick="lkbAddToList_Click" runat="server">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lkbCloseListMat2" onclick="lkbCloseListMat2_Click" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat2" runat="server" TargetControlID="btnHideListMat2" PopupDragHandleControlID="lblTitleListMat2" BackgroundCssClass="modalBackground" PopupControlID="pnlListMat2" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat2" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

