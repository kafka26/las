<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnSOItem.aspx.vb" Inherits="Transaction_SOItem" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Sales Order" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Sales Order :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 10%" class="Label" align=left><asp:Label id="Label42a" runat="server" Text="Filter"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="105px"><asp:ListItem Value="som.somstoid">Draft No.</asp:ListItem>
<asp:ListItem Value="som.sono">SO No.</asp:ListItem>
<asp:ListItem Value="c.custname">Customer</asp:ListItem>
<asp:ListItem Value="som.somstnote">Note</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="250px"></asp:TextBox></TD></TR><TR><TD style="HEIGHT: 22px" class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text="Periode"></asp:CheckBox></TD><TD style="HEIGHT: 22px" class="Label" align=center>:</TD><TD style="HEIGHT: 22px" class="Label" align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="60px" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label5" runat="server" Text="to"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="60px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="lblFormatTgl" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="105px"><asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>In Approval</asp:ListItem>
<asp:ListItem>Approved</asp:ListItem>
<asp:ListItem>Rejected</asp:ListItem>
<asp:ListItem>Revised</asp:ListItem>
<asp:ListItem>Closed</asp:ListItem>
<asp:ListItem>Force Closed</asp:ListItem>
<asp:ListItem>Cancel</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cb_so_type" runat="server" Text="SO Type" __designer:wfdid="w20"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="ddl_so_type" runat="server" CssClass="inpText" Width="105px" __designer:wfdid="w21"><asp:ListItem>Standard</asp:ListItem>
<asp:ListItem>Konsinyasi</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label74" runat="server" Text="Print Option" __designer:wfdid="w3"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="ddlPrintOption" runat="server" CssClass="inpText" Width="105px" __designer:wfdid="w4"><asp:ListItem Value="so">Sales Order</asp:ListItem>
<asp:ListItem Value="so2">Tugas Pilih Order</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" UserDateFormat="MonthDayYear" Mask="99/99/9999" MaskType="Date" TargetControlID="FilterPeriod1"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" TargetControlID="FilterPeriod1" Format="MM/dd/yyyy" PopupButtonID="imbDate1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" Mask="99/99/9999" MaskType="Date" TargetControlID="FilterPeriod2"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" TargetControlID="FilterPeriod2" Format="MM/dd/yyyy" PopupButtonID="imbDate2"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Button id="printso2" onclick="printso2_Click" runat="server" Text="Print FPB" Visible="False"></asp:Button> <asp:CheckBox id="cbprice" runat="server" Text="Print Without Price" Visible="False"></asp:CheckBox></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:LinkButton id="lbSOInProcess" runat="server" Visible="False"></asp:LinkButton> <asp:LinkButton id="lbSOInApproval" runat="server" Visible="False"></asp:LinkButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvTRN" runat="server" ForeColor="#333333" Width="100%" AllowSorting="True" AllowPaging="True" DataKeyNames="somstoid" GridLines="None" CellPadding="4" AutoGenerateColumns="False" PageSize="8">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="somstoid" DataNavigateUrlFormatString="~\Transaction\trnSOItem.aspx?oid={0}" DataTextField="somstoid" HeaderText="Draft No." SortExpression="somstoid">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="sono" HeaderText="SO No." SortExpression="sono">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sodate" HeaderText="Date" SortExpression="sodate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer" SortExpression="custname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="konsi" HeaderText="SO Type">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="somststatus" HeaderText="Status" SortExpression="somststatus">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="somstnote" HeaderText="Note" SortExpression="somstnote">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reason" HeaderText="Reason">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbGVMst" runat="server" Checked='<%# eval("checkvalue") %>'></asp:CheckBox> <asp:Label id="lblOidGVMst" runat="server" Text='<%# eval("somstoid") %>' Visible="False"></asp:Label> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="printso2"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label40" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Sales Order Header" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 15%" class="Label" align=left><asp:Label id="i_u" runat="server" CssClass="Important" Text="New Data"></asp:Label></TD><TD class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="custoid" runat="server" Visible="False"></asp:Label> <asp:Label id="periodacctg" runat="server" Visible="False"></asp:Label> <asp:Label id="warehouse_id" runat="server" Visible="False" __designer:wfdid="w7"></asp:Label> </TD><TD style="WIDTH: 15%" class="Label" align=left></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left runat="server" visible="true"></TD><TD class="Label" align=center runat="server" visible="true"></TD><TD class="Label" align=left runat="server" visible="true"><asp:CheckBox id="cbCons" runat="server" Text="SO Konsinyasi" __designer:wfdid="w4" AutoPostBack="True" OnCheckedChanged="cbCons_CheckedChanged"></asp:CheckBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD id="BusinessUnit1" class="Label" align=left runat="server" visible="false"><asp:Label id="Label10" runat="server" Text="Business Unit"></asp:Label></TD><TD id="BusinessUnit2" class="Label" align=center runat="server" visible="false">:</TD><TD id="BusinessUnit3" class="Label" align=left runat="server" visible="false"><asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpText" Width="205px" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="lbDivision" runat="server" Text="Division" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="groupoid" runat="server" CssClass="inpText" Width="205px" Visible="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblTrnNo" runat="server" Text="Draft No."></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:Label id="somstoid" runat="server"></asp:Label> <asp:TextBox id="sono" runat="server" CssClass="inpTextDisabled" Width="160px" Visible="False" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label67" runat="server" Text="Period Acctg" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="periodSO" runat="server" CssClass="inpTextDisabled" Width="100px" Visible="False" Enabled="False" MaxLength="20"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label8" runat="server" Text="SO Date"></asp:Label> <asp:Label id="Label43" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="sodate" runat="server" CssClass="inpText" Width="100px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="ibsodate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Width="16px" Height="16px"></asp:ImageButton> <asp:Label id="Label55" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" Height="16px"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label66" runat="server" Text="Plan Deliv. Date"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="delivDate" runat="server" CssClass="inpText" Width="100px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="btnETD" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Width="16px" Height="16px"></asp:ImageButton>&nbsp;<asp:Label id="Label29" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" Height="16px"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label6" runat="server" Text="Customer"></asp:Label> <asp:Label id="Label44" runat="server" CssClass="Important" Text="*" Visible="False"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custname" runat="server" CssClass="inpTextDisabled" Width="160px" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchCust" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearCust" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="Label65" runat="server" Text="Deliv. Type"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="delivType" runat="server" CssClass="inpText" Width="120px" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label72" runat="server" Text="PKP Name" __designer:wfdid="w1"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custname2" runat="server" CssClass="inpTextDisabled" Width="160px" __designer:wfdid="w2" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label64" runat="server" Text="SO Type"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="sotype2" runat="server" CssClass="inpText" Width="120px" AutoPostBack="True"></asp:DropDownList> </TD><TD class="Label" align=left><asp:Label id="Label68" runat="server" Text="No. Ref"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="noRef" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" MaxLength="18"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label69" runat="server" Text="Promo"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="promo" runat="server" CssClass="inpText" Width="120px" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left>&nbsp;<asp:Label id="Label12" runat="server" Text="Currency"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="curroid" runat="server" CssClass="inpText" Width="50px" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label16" runat="server" Text="Payment Type"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="sopaytypeoid" runat="server" CssClass="inpText" Width="50px"></asp:DropDownList> <asp:Label id="Label70" runat="server" CssClass="Important" Text="Days" Height="16px"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label45" runat="server" Text="Daily Rate (IDR)"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="rateidrvalue" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label56" runat="server" Text="Sales"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="soitemsalescode" runat="server" CssClass="inpText" Width="205px" OnSelectedIndexChanged="soitemsalescode_SelectedIndexChanged"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label13" runat="server" Text="Daily Rate (USD)"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="soratetousd" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label17" runat="server" Text="Total Amt"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="sototalamt" runat="server" CssClass="inpTextDisabled" Width="100px" AutoPostBack="True" Enabled="False" OnTextChanged="sototalamt_TextChanged"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label18" runat="server" Text="Header Disc 1"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="somstdisctype" runat="server" CssClass="inpText" Width="85px" AutoPostBack="True"><asp:ListItem Value="P">Percentage</asp:ListItem>
</asp:DropDownList>&nbsp;- <asp:TextBox id="somstdiscvalue" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" MaxLength="18"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label27" runat="server" Text="Header Disc. Amt 1"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="somstdiscamt" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label75" runat="server" Text="Header Disc 2" __designer:wfdid="w1"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="somstdisctype2" runat="server" CssClass="inpText" Width="85px" AutoPostBack="True"><asp:ListItem Value="P">Percentage</asp:ListItem>
</asp:DropDownList>&nbsp;- <asp:TextBox id="somstdiscvalue2" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" MaxLength="18"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label76" runat="server" Text="Header Disc. Amt 2" __designer:wfdid="w4"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="somstdiscamt2" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label23" runat="server" Text="DPP Amount"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="sodppamt" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label20" runat="server" Text="Taxable"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLinex" runat="server" CssClass="inpText" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="DDLinex_SelectedIndexChanged"><asp:ListItem Value="NT">NON TAX</asp:ListItem>
<asp:ListItem Value="INC">INCLUDE</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label31" runat="server" Text="Tax Amount"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="sovat" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label4" runat="server" Text="Status"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="somststatus" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False" MaxLength="20"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label19" runat="server" Text="Total Netto"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="sototalnetto" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label73" runat="server" Text="Employee" __designer:wfdid="w1"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="ddlPerson" runat="server" CssClass="inpText" Width="205px" __designer:wfdid="w2"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label24" runat="server" Text="Header Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="somstnote" runat="server" CssClass="inpText" Width="200px" MaxLength="100"></asp:TextBox></TD></TR><TR><TD class="Label" align=left rowSpan=1><asp:Label id="Label3" runat="server" Text="SO Group" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="sotype" runat="server" CssClass="inpText" Width="120px" Visible="False" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="rateoid" runat="server" Visible="False"></asp:Label> <asp:Label id="rate2oid" runat="server" Visible="False"></asp:Label> <asp:Label id="custres3" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD id="TD2" class="Label" align=left colSpan=6 rowSpan=1 runat="server" Visible="false"><asp:GridView id="gvContainer" runat="server" ForeColor="#333333" Width="50%" GridLines="None" CellPadding="4" AutoGenerateColumns="False" PageSize="5">
<RowStyle BackColor="#E3EAEB"></RowStyle>
<Columns>
<asp:BoundField DataField="containertype" HeaderText="Type">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Qty"><ItemTemplate>
<asp:TextBox id="tbContQty" runat="server" CssClass="inpText" Text='<%# eval("soitemcontqty") %>' Width="60px" ToolTip='<%# eval("containeroid") %>' MaxLength="16"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftbContQty" runat="server" TargetControlID="tbContQty" ValidChars="1234567890,.">
                            </ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="%"><ItemTemplate>
<asp:TextBox id="tbContPct" runat="server" CssClass="inpText" Text='<%# eval("soitemcontpct") %>' Width="60px" MaxLength="16"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftbContPct" runat="server" ValidChars="1234567890,." TargetControlID="tbContPct">
                                </ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#666666" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#7C6F57"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:TextBox id="soitemportship" runat="server" CssClass="inpText" Width="200px" Visible="False" MaxLength="100"></asp:TextBox> <asp:DropDownList id="soiteminvoiceval" runat="server" CssClass="inpText" Width="105px" Visible="False"><asp:ListItem>FOB</asp:ListItem>
<asp:ListItem>CIF</asp:ListItem>
<asp:ListItem>CNF</asp:ListItem>
<asp:ListItem>FCA</asp:ListItem>
</asp:DropDownList> <asp:DropDownList id="soitembank" runat="server" CssClass="inpText" Width="205px" Visible="False"></asp:DropDownList> <asp:DropDownList id="accountoid" runat="server" CssClass="inpText" Width="165px" Visible="False"></asp:DropDownList> <asp:TextBox id="socustref" runat="server" CssClass="inpText" Width="160px" Visible="False" MaxLength="50"></asp:TextBox> <asp:TextBox id="soitemetd" runat="server" CssClass="inpText" Width="80px" Visible="False" ToolTip="MM/DD/YYYY"></asp:TextBox> <asp:TextBox id="sotaxamt" runat="server" CssClass="inpTextDisabled" Width="100px" Visible="False" Enabled="False"></asp:TextBox> <asp:DropDownList id="sotaxtype" runat="server" CssClass="inpTextDisabled" Width="85px" Visible="False" Enabled="False"><asp:ListItem>TAX</asp:ListItem>
<asp:ListItem Value="NON TAX">Non TAX</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="sototaldisc" runat="server" CssClass="inpTextDisabled" Width="100px" Visible="False" Enabled="False"></asp:TextBox> <asp:TextBox id="sototaldiscdtl" runat="server" CssClass="inpTextDisabled" Width="100px" Visible="False" Enabled="False"></asp:TextBox> <asp:TextBox id="somstdiscamt3" runat="server" CssClass="inpTextDisabled" Width="100px" Visible="False" Enabled="False"></asp:TextBox>&nbsp; <asp:TextBox id="sorate2tousd" runat="server" CssClass="inpTextDisabled" Width="100px" Visible="False" Enabled="False"></asp:TextBox> <asp:TextBox id="CRFreeUSD" runat="server" CssClass="inpTextDisabled" Width="100px" Visible="False" Enabled="False"></asp:TextBox> <asp:TextBox id="somstdiscvalue3" runat="server" CssClass="inpText" Width="100px" Visible="False" AutoPostBack="True" MaxLength="18"></asp:TextBox> <asp:DropDownList id="somstdisctype3" runat="server" CssClass="inpText" Width="85px" Visible="False" AutoPostBack="True"><asp:ListItem Value="P">Percentage</asp:ListItem>
<asp:ListItem Value="A">Amount</asp:ListItem>
</asp:DropDownList>&nbsp;&nbsp; <asp:TextBox id="rate2idrvalue" runat="server" CssClass="inpTextDisabled" Width="100px" Visible="False" Enabled="False"></asp:TextBox> <asp:TextBox id="CRFreeIDR" runat="server" CssClass="inpTextDisabled" Width="100px" Visible="False" Enabled="False"></asp:TextBox><asp:TextBox id="soitemportdischarge" runat="server" CssClass="inpText" Width="200px" Visible="False" MaxLength="100"></asp:TextBox><asp:DropDownList id="sopaymethod" runat="server" CssClass="inpText" Width="105px" Visible="False"><asp:ListItem>TT</asp:ListItem>
<asp:ListItem>LC</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="Td1" class="Label" align=left colSpan=6 rowSpan=1 runat="server" Visible="false"><asp:Label id="socontoid" runat="server"></asp:Label> <asp:Label id="Label30" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Container Available" Font-Underline="False"></asp:Label> <asp:Label id="Label32" runat="server" Text="Payment Method" Visible="False"></asp:Label> <asp:Label id="Label41" runat="server" Text="Credit Limit Free (IDR)" Visible="False"></asp:Label> <asp:Label id="Label42" runat="server" Text="Rate Standard (IDR)" Visible="False"></asp:Label> <asp:Label id="Label50" runat="server" Text="Header Disc 2" Visible="False"></asp:Label> <asp:Label id="Label51" runat="server" Text="Header Disc 3" Visible="False"></asp:Label> <asp:Label id="Label28" runat="server" Text="Total Disc. Amt" Visible="False"></asp:Label> <asp:Label id="Label25" runat="server" Text="ETD" Visible="False"></asp:Label>&nbsp;&nbsp; <asp:Label id="Label22" runat="server" Text="Bill To" Visible="False"></asp:Label> <asp:Label id="Label49" runat="server" CssClass="Important" Text="*" Visible="False"></asp:Label> <asp:Label id="Label47" runat="server" Text="Credit Limit Free (USD)" Visible="False"></asp:Label> <asp:Label id="Label46" runat="server" Text="Rate Standard (USD)" Visible="False"></asp:Label> <asp:Label id="Label52" runat="server" Text="Header Disc. Amt 2" Visible="False"></asp:Label> <asp:Label id="Label53" runat="server" Text="Header Disc. Amt 3" Visible="False"></asp:Label> <asp:Label id="Label26" runat="server" Text="Total Disc. Dtl Amt" Visible="False"></asp:Label> <asp:Label id="Label9" runat="server" Text="Customer PO" Visible="False"></asp:Label> <asp:Label id="lblSepTax" runat="server" Text="-" Visible="False"></asp:Label> <asp:Label id="Label2" runat="server" Text="Invoice Value" Visible="False"></asp:Label> <asp:Label id="Label21" runat="server" Text="Ship To" Visible="False"></asp:Label> <asp:Label id="Label48" runat="server" CssClass="Important" Text="*" Visible="False"></asp:Label> <asp:Label id="Label33" runat="server" Text="Account No" Visible="False"></asp:Label> <asp:Label id="Label11" runat="server" Text="Negotiating Bank" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><ajaxToolkit:FilteredTextBoxExtender id="ftbDisc" runat="server" TargetControlID="somstdiscvalue" ValidChars="0123456789,."></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="ftbDisc2" runat="server" TargetControlID="somstdiscvalue2" ValidChars="0123456789,."></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="ftbDisc3" runat="server" TargetControlID="somstdiscvalue3" ValidChars="0123456789,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:CalendarExtender id="ceDate2" runat="server" TargetControlID="delivDate" Format="MM/dd/yyyy" PopupButtonID="btnETD"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeDate2" runat="server" TargetControlID="delivDate" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="ceDate3" runat="server" TargetControlID="sodate" Format="MM/dd/yyyy" PopupButtonID="ibsodate"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeDate3" runat="server" TargetControlID="sodate" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><asp:Label id="Label7" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Sales Order Detail" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="i_u2" runat="server" Text="New Detail" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="sodtloid" runat="server" Visible="False"></asp:Label> <asp:Label id="sodtlseq" runat="server" Visible="False">1</asp:Label> <asp:Label id="Seq" runat="server" Text="Label" Visible="False"></asp:Label> <asp:Label id="oldcode" runat="server" Visible="False"></asp:Label> <asp:Label id="transitemdtloid" runat="server" Visible="False" __designer:wfdid="w10"></asp:Label> <asp:Label id="price_list" runat="server" Visible="False" __designer:wfdid="w10"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="matoid" runat="server" Visible="False"></asp:Label> <asp:Label id="matcode" runat="server" Visible="False"></asp:Label><asp:Label id="itemconvertunit" runat="server" Visible="False"></asp:Label><asp:Label id="sounit2oid" runat="server" Visible="False"></asp:Label> <asp:Label id="sounit1oid" runat="server" Visible="False"></asp:Label> <asp:Label id="stockvalue" runat="server" Visible="False" __designer:wfdid="w1"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label34" runat="server" Text="Item"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="matlongdesc" runat="server" CssClass="inpTextDisabled" Width="180px" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSearchMat" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnSearchMatEdit" onclick="btnSearchMatEdit_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnClearMat" onclick="btnClearMat_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="Label57" runat="server" Text="Stock Qty"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="stockQty" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label14" runat="server" Text="Quantity"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="soqty" runat="server" CssClass="inpText" Width="75px" AutoPostBack="True" MaxLength="18"></asp:TextBox>&nbsp;- <asp:DropDownList id="sounitoid" runat="server" Font-Bold="True" CssClass="inpTextDisabled" Width="100px" AutoPostBack="True" Enabled="False"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label35" runat="server" Text="Price Per Unit"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="soprice" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" MaxLength="18"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label58" runat="server" Text="Qty 1" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="qtyunitbesar" runat="server" CssClass="inpTextDisabled" Width="75px" Visible="False" MaxLength="12"></asp:TextBox><asp:DropDownList id="unitkeciloid" runat="server" CssClass="inpTextDisabled" Width="100px" Visible="False" Enabled="False"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label59" runat="server" Text="Qty 2" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="qtyunitkecil" runat="server" CssClass="inpTextDisabled" Width="75px" Visible="False" MaxLength="12"></asp:TextBox><asp:DropDownList id="unitbesaroid" runat="server" CssClass="inpTextDisabled" Width="100px" Visible="False" Enabled="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label36" runat="server" Text="Detail Disc 1"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="sodtldisctype1" runat="server" CssClass="inpText" Width="85px" AutoPostBack="True" OnSelectedIndexChanged="sodtldisctype1_SelectedIndexChanged"><asp:ListItem Value="P">Percentage</asp:ListItem>
<asp:ListItem Value="A">Amount</asp:ListItem>
</asp:DropDownList> - <asp:TextBox id="sodtldiscvalue1" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" MaxLength="18" OnTextChanged="sodtldiscvalue1_TextChanged"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label37" runat="server" Text="Detail Disc 1 Amt"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="sodtldiscamt1" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label60" runat="server" Text="Detail Disc 2"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="sodtldisctype2" runat="server" CssClass="inpText" Width="85px" AutoPostBack="True" OnSelectedIndexChanged="sodtldisctype2_SelectedIndexChanged"><asp:ListItem Value="P">Percentage</asp:ListItem>
<asp:ListItem Value="A">Amount</asp:ListItem>
</asp:DropDownList>&nbsp;- <asp:TextBox id="sodtldiscvalue2" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" MaxLength="18" OnTextChanged="sodtldiscvalue2_TextChanged"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label61" runat="server" Text="Detail Disc 2 Amt"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="sodtldiscamt2" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label39" runat="server" Text="Detail Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="sodtlnote" runat="server" CssClass="inpText" Width="200px" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label15" runat="server" Text="Detail Total Amt"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="sodtlamt" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label71" runat="server" Text="Code 2" __designer:wfdid="w5"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="old_code" runat="server" CssClass="inpText" Width="200px" __designer:wfdid="w6" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD id="TD3" class="Label" align=left colSpan=6 runat="server" Visible="false"><asp:Label id="Label62" runat="server" Text="Detail Disc 3"></asp:Label> <asp:DropDownList id="sodtldisctype3" runat="server" CssClass="inpText" Width="85px" AutoPostBack="True" OnSelectedIndexChanged="sodtldisctype3_SelectedIndexChanged"><asp:ListItem Value="P">Percentage</asp:ListItem>
<asp:ListItem Value="A">Amount</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="sodtldiscvalue3" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" MaxLength="18" OnTextChanged="sodtldiscvalue3_TextChanged"></asp:TextBox> <asp:Label id="Label63" runat="server" Text="Detail Disc 3 Amt"></asp:Label> <asp:TextBox id="sodtldiscamt3" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblvarprice" runat="server" Text="Var Price" Visible="False"></asp:Label> <asp:Label id="Label38" runat="server" Text="Detail Netto " Visible="False"></asp:Label></TD><TD class="Label" align=center><asp:Label id="lblSepvarprice" runat="server" Text=":" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="varprice" runat="server" CssClass="inpText" Width="100px" Visible="False" AutoPostBack="True" MaxLength="18" OnTextChanged="varprice_TextChanged"></asp:TextBox><asp:TextBox id="sodtlnetto" runat="server" CssClass="inpTextDisabled" Width="100px" Visible="False" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label54" runat="server" Text="Panjang" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="panjang" runat="server" CssClass="inpText" Width="60px" Visible="False" AutoPostBack="True" MaxLength="18"></asp:TextBox><asp:TextBox id="roll" runat="server" CssClass="inpText" Width="60px" Visible="False" AutoPostBack="True" MaxLength="18"></asp:TextBox></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="ftbDD" runat="server" TargetControlID="sodtldiscvalue1" ValidChars="1234567890,.">
            </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbQty" runat="server" TargetControlID="soqty" ValidChars="1234567890,.">
            </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbPrice" runat="server" TargetControlID="soprice" ValidChars="1234567890,.">
            </ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Panel id="Panel2" runat="server" Width="100%" Height="250px" ScrollBars="Vertical"><asp:GridView id="gvTblDtl" runat="server" ForeColor="#333333" Width="98%" DataKeyNames="sodtlseq" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
<RowStyle Wrap="True" BackColor="#E3EAEB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="sodtlseq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transitemno" HeaderText="Transfer No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="oldcode" HeaderText="Old Code">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="matlongdesc" HeaderText="Long Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sodtlqty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sodtlunit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sodtlprice" HeaderText="Price">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sodtldiscamt1" HeaderText="Disc 1">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sodtldiscamt1" HeaderText="Disc 2">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stockvalue" HeaderText="HPP" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stockvalue_pct" HeaderText="Kenaikan %" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sodtltaxamt" HeaderText="Tax Amt" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sodtldpp" HeaderText="Total Amt">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sodtlnetto" HeaderText="netto" Visible="False"></asp:BoundField>
<asp:BoundField DataField="sodtlnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stockqty" HeaderText="stockqty" Visible="False"></asp:BoundField>
<asp:BoundField DataField="itemconvertunit" HeaderText="itemconvertunit" Visible="False"></asp:BoundField>
<asp:BoundField DataField="sodtlunitoid" HeaderText="unitoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="inex" HeaderText="inex" Visible="False"></asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#666666" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle Wrap="True" BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#7C6F57"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </asp:Panel> </TD></TR><TR><TD class="Label" align=left colSpan=6></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left colSpan=6>Created By <asp:Label id="createuser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left colSpan=6>Last Update&nbsp;By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton> <asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton id="btnSendApproval" runat="server" ImageUrl="~/Images/sendapproval.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=6><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center">&nbsp;</DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Sales Order :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upListCust" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlListCust" runat="server" CssClass="modalBox" Width="650px" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListCust" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Customer"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListCust" runat="server" Width="100%" DefaultButton="btnFindListCust">Filter : <asp:DropDownList id="FilterDDLListCust" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="custname">Name</asp:ListItem>
<asp:ListItem Value="custcode">Code</asp:ListItem>
<asp:ListItem Value="custaddr">Address</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListCust" runat="server" CssClass="inpText" Width="200px"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListCust" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListCust" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton><BR /><asp:CheckBox id="cbTag" runat="server" Text="Tag :" Visible="False"></asp:CheckBox>&nbsp;<asp:DropDownList id="FilterDDLTag" runat="server" CssClass="inpText" Width="200px" Visible="False"></asp:DropDownList><asp:DropDownList id="FilterDDLType" runat="server" CssClass="inpText" Width="100px" Visible="False"><asp:ListItem>LOCAL</asp:ListItem>
<asp:ListItem>EXPORT</asp:ListItem>
</asp:DropDownList></asp:Panel> </TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListCust" runat="server" ForeColor="#333333" Width="98%" PageSize="5" AutoGenerateColumns="False" CellPadding="4" GridLines="None" DataKeyNames="custoid,custcode,custname,CRFreeIDR,CRFreeUSD,custpaymentoid,custtaxable,custheaderdisc,custgroupname,person_id,warehouse_id,salescode" AllowPaging="True" OnRowDataBound="gvListCust_RowDataBound">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="custcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="CRFreeIDR" HeaderText="Credit Limit(IDR)">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="CRFreeUSD" HeaderText="Credit Limit(USD)">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbCloseListCust" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListCust" runat="server" TargetControlID="btnHideListCust" PopupDragHandleControlID="lblListCust" PopupControlID="pnlListCust" Drag="True" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListCust" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
    &nbsp;<asp:UpdatePanel id="upListMat" runat="server"><contenttemplate>
<asp:Panel id="pnlListMat" runat="server" CssClass="modalBox" Width="900px" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListMat" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Finish Good"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListMat" runat="server" Width="100%" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblFilterInListMat" runat="server" Text="Filter :"></asp:Label> <asp:DropDownList id="FilterDDLListMat" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="matlongdesc">Description</asp:ListItem>
<asp:ListItem Value="matcode">Code</asp:ListItem>
<asp:ListItem Value="oldcode">Old Code</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" CssClass="inpText" Width="150px"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="lblStockQtyInListMat" runat="server" Text="And Stock Qty :" Visible="False"></asp:Label> <asp:DropDownList id="FilterDDLStock" runat="server" CssClass="inpText" Width="40px" Visible="False"><asp:ListItem>&gt;=</asp:ListItem>
<asp:ListItem>=</asp:ListItem>
<asp:ListItem>&lt;=</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextStock" runat="server" CssClass="inpText" Width="50px" Visible="False">0</asp:TextBox></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:CheckBox id="cbCat01" runat="server" Text="Category 1 :" Visible="False"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat01" runat="server" CssClass="inpText" Width="200px" Visible="False" AutoPostBack="True"></asp:DropDownList> <asp:CheckBox id="cbCat02" runat="server" Text="Category 2 :" Visible="False"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat02" runat="server" CssClass="inpText" Width="200px" Visible="False" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:CheckBox id="cbCat03" runat="server" Text="Category 3 :" Visible="False"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat03" runat="server" CssClass="inpText" Width="200px" Visible="False" AutoPostBack="True"></asp:DropDownList> <asp:CheckBox id="cbCat04" runat="server" Text="Category 4 :" Visible="False"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat04" runat="server" CssClass="inpText" Width="200px" Visible="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=center colSpan=3><ajaxToolkit:FilteredTextBoxExtender id="ftbStock" runat="server" TargetControlID="FilterTextStock" ValidChars="1234567890,.">
            </ajaxToolkit:FilteredTextBoxExtender></TD></TR></TBODY></TABLE></asp:Panel></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnSelectNone" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewChecked" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:TextBox id="tbData" runat="server" CssClass="inpText" Width="25px" Visible="False"></asp:TextBox> <asp:LinkButton id="lbShowData" runat="server" Visible="False">>> View</asp:LinkButton> <asp:UpdateProgress id="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat"><ProgressTemplate>
<TABLE style="WIDTH: 200px"><TBODY><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple" align=center>Load Data, Please Wait ...</TD></TR><TR><TD align=center><asp:Image id="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif"></asp:Image></TD></TR></TBODY></TABLE>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat" runat="server" ForeColor="#333333" Width="98%" PageSize="8" AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowPaging="True" OnSelectedIndexChanged="gvListMat_SelectedIndexChanged">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbOnListMat" runat="server" ToolTip='<%# eval("itemoid_sel") %>' __designer:wfdid="w24" Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="matcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="oldcode" HeaderText="Old Code" Visible="False">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matlongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stockqty" HeaderText="Stock Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit1" HeaderText="Unit Qty">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stockqty2" HeaderText="stock Qty 3" Visible="False">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit3" HeaderText="Unit Qty 3" Visible="False">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Qty"><ItemTemplate>
<asp:TextBox id="tbQtyOnListMat" runat="server" CssClass="inpText" Text='<%# eval("soqty") %>' Width="30px" __designer:wfdid="w8" MaxLength="12"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftbQtyOnListMat" runat="server" __designer:wfdid="w9" ValidChars="1234567890,." TargetControlID="tbQtyOnListMat">
            </ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Unit"><ItemTemplate>
<asp:DropDownList id="DDLunit" runat="server" CssClass="inpTextDisabled" Width="64px" __designer:wfdid="w1" Enabled="False" ToolTip='<%# eval("unitoid") %>'></asp:DropDownList> 
</ItemTemplate>

<HeaderStyle CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Price"><ItemTemplate>
<asp:TextBox id="tbAmtOnListMat" runat="server" CssClass="inpText" Text='<%# eval("soprice") %>' Width="70px" __designer:wfdid="w6" MaxLength="12"></asp:TextBox><BR /><ajaxToolkit:FilteredTextBoxExtender id="ftbAmtOnListMat" runat="server" __designer:wfdid="w7" ValidChars="1234567890,." TargetControlID="tbAmtOnListMat"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Note"><ItemTemplate>
<asp:TextBox id="tbNoteOnListMat" runat="server" CssClass="inpText" Text='<%# eval("note") %>' Width="100px" __designer:wfdid="w3" MaxLength="100"></asp:TextBox> 
</ItemTemplate>

<HeaderStyle CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="transitemno" HeaderText="TW No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Code 2"><ItemTemplate>
<asp:TextBox id="code_2" runat="server" CssClass="inpText" Text='<%# eval("old_code") %>' Width="100px" __designer:wfdid="w2" MaxLength="100"></asp:TextBox>
</ItemTemplate>

<HeaderStyle CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="matunitoid" HeaderText="Unitoid" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matunit2oid" HeaderText="Unitoid2" Visible="False"></asp:BoundField>
<asp:BoundField DataField="itemconvertunit" HeaderText="itemconvertunit" Visible="False"></asp:BoundField>
<asp:BoundField DataField="sodtlseq" HeaderText="seq" Visible="False"></asp:BoundField>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="soprice" HeaderText="price" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:CheckBox id="cbOpenListMatUsage" runat="server" Font-Size="8pt" Font-Bold="True" CssClass="Important" Text="Open List Of Material again after add data to list" Checked="True"></asp:CheckBox></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbAddListMat" runat="server" Font-Bold="True">[ Add To List ]</asp:LinkButton>&nbsp;- <asp:LinkButton id="lbCloseListMat" runat="server" Font-Bold="True">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlListMat" PopupDragHandleControlID="lblListMat"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <br />
    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListMatEdit" runat="server" CssClass="modalBox" Width="650px" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListMatEdit" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Material"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListMatEdit" runat="server" Width="100%" DefaultButton="btnFindListCust">Filter : <asp:DropDownList id="FilterDDLListMatEdit" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="m.itemCode">Code</asp:ListItem>
<asp:ListItem Value="m.itemoldcode">Old Code</asp:ListItem>
<asp:ListItem Value="m.itemLongDescription">Description</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMatEdit" runat="server" CssClass="inpText" Width="200px"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListMatEdit" onclick="btnFindListMatEdit_Click" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMatEdit" onclick="btnAllListMatEdit_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListMatEdit" runat="server" ForeColor="#333333" Width="98%" OnSelectedIndexChanged="gvListMatEdit_SelectedIndexChanged" PageSize="5" AutoGenerateColumns="False" CellPadding="4" GridLines="None" DataKeyNames="itemoid,matcode,oldcode,matlongdesc,stockqty,matunit,soprice,matunitoid" OnRowDataBound="gvListMatEdit_RowDataBound" AllowPaging="True" OnPageIndexChanging="gvListMatEdit_PageIndexChanging">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="matcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="oldcode" HeaderText="Old Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matlongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stockqty" HeaderText="Stock Qty">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="matunit" HeaderText="Unit">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="soprice" HeaderText="Price">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbCloseListMatEdit" onclick="lbCloseListMatEdit_Click" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMatEdit" runat="server" TargetControlID="btnHideListMatEdit" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlListMatEdit" PopupDragHandleControlID="lblListMatEdit"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMatEdit" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <br />
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upApproval" runat="server">
        <contenttemplate>
<asp:Panel id="pnlApproval" runat="server" CssClass="modalBox" Width="250px" Visible="False"><TABLE width="100%"><TBODY><TR><TD align=center colSpan=2><asp:Label id="lblApproval" runat="server" Font-Size="Medium" Font-Bold="True" Text="Choose Language"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" align=center colSpan=2><asp:Label id="lblRptOid" runat="server" Visible="False"></asp:Label> </TD></TR><TR><TD class="Label" align=center colSpan=2><asp:DropDownList id="DDLTypeCust" runat="server" CssClass="inpText" Width="150px"><asp:ListItem Value="LOCAL">Indonesia</asp:ListItem>
<asp:ListItem Value="EXPORT">English</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 5px" align=center colSpan=2></TD></TR><TR><TD align=center colSpan=2><asp:ImageButton id="btnContinue" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsBottom" AlternateText="Continue"></asp:ImageButton> <asp:ImageButton id="btnCancelPrint" runat="server" ImageUrl="~/Images/Close.png" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeApproval" runat="server" TargetControlID="btnPnlApproval" PopupDragHandleControlID="lblApproval" PopupControlID="pnlApproval" Drag="True" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnPnlApproval" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="btnContinue"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>
</asp:Content>

