Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_BeritaAcararReturn
    Inherits System.Web.UI.Page

    Structure VarReturnValue
        Public ValRaw_IDR As Double
        Public ValRaw_USD As Double
        Public ValGen_IDR As Double
        Public ValGen_USD As Double
        Public ValSP_IDR As Double
        Public ValSP_USD As Double
        Public ValLog_IDR As Double
        Public ValLog_USD As Double
        Public ValST_IDR As Double
        Public ValST_USD As Double
    End Structure

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "acaradtloid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("retbaqty") = ToDouble(GetTextBoxValue(row.Cells(4).Controls))
                                    dtView(0)("retbadtlnote") = GetTextBoxValue(row.Cells(6).Controls)
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "acaradtloid=" & cbOid
                                dtView2.RowFilter = "acaradtloid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("retbaqty") = ToDouble(GetTextBoxValue(row.Cells(4).Controls))
                                    dtView(0)("retbadtlnote") = GetTextBoxValue(row.Cells(6).Controls)
                                    If dtView2.Count > 0 Then
                                        dtView2(0)("retbaqty") = ToDouble(GetTextBoxValue(row.Cells(4).Controls))
                                        dtView2(0)("retbadtlnote") = GetTextBoxValue(row.Cells(6).Controls)
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If acaramstoid.Text = "" Then
            sError &= "- Please select BA NO. field!<BR>"
        End If
        If retbawhoid.SelectedValue = "" Then
            sError &= "- Please select WAREHOUSE field!<BR>"
        End If
        If acaradtloid.Text = "" Then
            sError &= "- Please select MATERIAL field!<BR>"
        End If
        If retbaqty.Text = "" Then
            sError &= "- Please fill RETURN QTY field!<BR>"
        Else
            If ToDouble(retbaqty.Text) <= 0 Then
                sError &= "- RETURN QTY field must be more than 0!<BR>"
            Else
                If ToDouble(retbaqty.Text) > ToDouble(acaraqty.Text) Then
                    sError &= "- RETURN QTY field must be less than BA QTY!<BR>"
                End If
            End If
        End If
        If retbaunitoid.SelectedValue = "" Then
            sError &= "- Please select UNIT field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If retbadate.Text = "" Then
            sErr = "None"
            sError &= "- Please fill RETURN DATE field!<BR>"
        Else
            If Not IsValidDate(retbadate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- RETURN DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If deptoid.SelectedValue = "" Then
            sError &= "- Please select DEPARTMENT field!<BR>"
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill Detail Data!<BR>"
        Else
            Dim dtTbl As DataTable = Session("TblDtl")
            If dtTbl.Rows.Count <= 0 Then
                sError &= "- Please fill Detail Data!<BR>"
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            retbamststatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckReturnStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trnreturnbamst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND retbamststatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnReturnBA.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql).ToString) > 0 Then
            lkbReturnInProcess.Visible = True
            lkbReturnInProcess.Text = "You have " & GetStrData(sSql) & " In Process Process Berita Acara Return data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitAllDDL()
        ' Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        FillDDL(DDLBusUnit, sSql)
        If DDLBusUnit.Items.Count > 0 Then
            InitDDLDept()
        End If
        ' Fill DDL Warehouse
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='WAREHOUSE' AND genoid>0"
        If i_u.Text = "New Data" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
        FillDDL(retbawhoid, sSql)
        ' Fill DDL Unit
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='UNIT'"
        If i_u.Text = "New Data" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
        FillDDL(retbaunitoid, sSql)
    End Sub

    Private Sub InitDDLDept()
        ' Fill DDL Department
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND deptoid IN (SELECT deptoid FROM QL_trnbrtacaramst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND acaramststatus='Approved') ORDER BY deptname"
        FillDDL(deptoid, sSql)
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT 'False' AS checkvalue, divname, retbamstoid, retbano, CONVERT(VARCHAR(10), retbadate, 101) AS retbadate, deptname, retbamststatus, retbamstnote, retm.createuser FROM QL_trnreturnbamst retm INNER JOIN QL_mstdept de ON de.deptoid=retm.deptoid AND de.cmpcode=retm.cmpcode INNER JOIN QL_mstdivision div ON div.cmpcode=retm.cmpcode WHERE"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " retm.cmpcode='" & Session("CompnyCode") & "'"
        Else
            sSql &= " retm.cmpcode LIKE '%'"
        End If
        sSql &= sSqlPlus & " ORDER BY CONVERT(DATETIME, retbadate) DESC, retbamstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trnreturnbamst")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "retbamstoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub PrintReport()
        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("retbamstoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            report.Load(Server.MapPath(folderReport & "rptReturnBA.rpt"))
            Dim sWhere As String = "WHERE"
            If Session("CompnyCode") <> CompnyCode Then
                sWhere &= " retm.cmpcode='" & Session("CompnyCode") & "'"
            Else
                sWhere &= " retm.cmpcode LIKE '%'"
            End If
            If sOid = "" Then
                sWhere &= " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND retbadate>=CAST('" & FilterPeriod1.Text & " 00:00:00' AS DATETIME) AND retbadate<=CAST('" & FilterPeriod2.Text & " 23:59:59' AS DATETIME)"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sWhere &= " AND retbamststatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Transaction\trnReturnBA.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND retm.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sWhere &= " AND retm.retbamstoid IN (" & sOid & ")"
            End If
            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            report.PrintOptions.PaperSize = PaperSize.PaperA4
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "BeritaAcaraReturnPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
        End Try
    End Sub

    Private Sub BindListUsage()
        sSql = "SELECT acaramstoid, acarano, CONVERT(VARCHAR(10), acaradate, 101) AS acaradate, acaramstnote FROM QL_trnbrtacaramst bam INNER JOIN QL_trnwomst wom ON wom.cmpcode=bam.cmpcode AND wom.womstoid=bam.womstoid WHERE bam.cmpcode='" & DDLBusUnit.SelectedValue & "' AND acaramststatus='Approved' AND acaratype='Material Usage' AND " & FilterDDLListUsage.SelectedValue & " LIKE '%" & Tchar(FilterTextListUsage.Text) & "%' AND bam.deptoid=" & deptoid.SelectedValue & " AND womststatus='Post' ORDER BY acaramstoid"
        FillGV(gvListUsage, sSql, "QL_trnbrtacaramst")
    End Sub

    Private Sub BindMatData()
        Dim sStr As String = ""
        If Session("oid") IsNot Nothing And Session("oid") <> "" Then
            sStr = " AND retd.retbamstoid<>" & Session("oid")
        End If
        Dim sType As String = "item", sType2 As String = "item", sRef As String = "Material", sPeriod As String = GetDateToPeriodAcctg(GetServerTime()), sNo As String = "", sAddJoin As String = "", sEnable As String = "True", sCss As String = "inpText"
      
        lblTitleListMat.Text = "List Of " & sRef
        sSql = "SELECT 'False' AS checkvalue, acaradtlseq, acaradtloid, acararefoid, " & sType & "code" & sNo & " AS acararefcode, " & sType & "longdescription AS acarareflongdesc, (acaraqty - ISNULL((SELECT SUM(retbaqty) FROM QL_trnreturnbadtl retd WHERE retd.cmpcode=bad.cmpcode AND retd.acaradtloid=bad.acaradtloid" & sStr & "), 0.0)) AS acaraqty, acaraunitoid, g.gendesc AS acaraunit, 0.0 AS retbaqty, acaradtlnote AS retbadtlnote, acarawhoid, gwh.gendesc AS acarawh, acaravalueidr, acaravalueusd, '" & sEnable & "' enableqty, '" & sCss & "' cssqty, (CASE WHEN itemgroup='RAW' THEN 'RAW MATERIAL' WHEN itemgroup='WIP' THEN 'WIP' WHEN itemgroup='FG' THEN 'FINISH GOOD' END) AS refname, (SELECT ISNULL(genother1,'') acctgoid FROM QL_mstgen gx WHERE gx.cmpcode=bad.cmpcode AND gx.gengroup='GROUPITEM' AND gx.genoid=m.itemgroupoid AND activeflag='ACTIVE') AS stockacctgoid FROM QL_trnbrtacaradtl bad INNER JOIN QL_trnbrtacaramst bam ON bam.cmpcode=bad.cmpcode AND bam.acaramstoid=bad.acaramstoid INNER JOIN QL_mst" & sType2 & " m ON " & sType2 & "oid=acararefoid" & sAddJoin & " INNER JOIN QL_mstgen g ON g.genoid=acaraunitoid INNER JOIN QL_mstgen gwh ON gwh.genoid=acarawhoid WHERE bad.cmpcode='" & DDLBusUnit.SelectedValue & "' AND bad.acaramstoid=" & acaramstoid.Text & " AND acaradtlstatus='' ORDER BY acaradtlseq"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstmat")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(C1)("retbaqty") = ToDouble(dt.Rows(C1)("acaraqty").ToString)
        Next
        dt.AcceptChanges()
        Session("TblMat") = dt
    End Sub

    Private Sub CreateTblDetail()
        If Session("TblDtl") Is Nothing Then
            Dim dtlTable As DataTable = New DataTable("QL_trnreturnbadtl")
            dtlTable.Columns.Add("retbadtlseq", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("acaramstoid", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("acarano", Type.GetType("System.String"))
            dtlTable.Columns.Add("retbareftype", Type.GetType("System.String"))
            dtlTable.Columns.Add("acaradtloid", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("retbarefoid", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("retbarefcode", Type.GetType("System.String"))
            dtlTable.Columns.Add("retbareflongdesc", Type.GetType("System.String"))
            dtlTable.Columns.Add("retbawhoid", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("retbawh", Type.GetType("System.String"))
            dtlTable.Columns.Add("acaraqty", Type.GetType("System.Double"))
            dtlTable.Columns.Add("retbaqty", Type.GetType("System.Double"))
            dtlTable.Columns.Add("retbaunitoid", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("retbaunit", Type.GetType("System.String"))
            dtlTable.Columns.Add("retbadtlnote", Type.GetType("System.String"))
            dtlTable.Columns.Add("retbavalueidr", Type.GetType("System.Double"))
            dtlTable.Columns.Add("retbavalueusd", Type.GetType("System.Double"))
            dtlTable.Columns.Add("retbaqty_unitkecil", Type.GetType("System.Double"))
            dtlTable.Columns.Add("retbaqty_unitbesar", Type.GetType("System.Double"))
            dtlTable.Columns.Add("stockacctgoid", Type.GetType("System.Int32"))
            Session("TblDtl") = dtlTable
        End If
    End Sub

    Private Sub ClearDetail()
        retbadtlseq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                retbadtlseq.Text = objTable.Rows.Count + 1
                EnableHeader(False)
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        i_u2.Text = "New Detail"
        acaramstoid.Text = ""
        acarano.Text = ""
        acaradtloid.Text = ""
        retbarefoid.Text = ""
        retbarefcode.Text = ""
        retbareflongdesc.Text = ""
        retbawhoid.SelectedIndex = -1
        acaraqty.Text = ""
        retbaqty.Text = ""
        retbaunitoid.SelectedIndex = -1
        retbadtlnote.Text = ""
        gvDtl.SelectedIndex = -1
        btnSearchUsage.Visible = True : btnClearUsage.Visible = True
        retbareftype.Enabled = True : retbareftype.CssClass = "inpText"
        btnSearchMat.Visible = True
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal : DDLBusUnit.CssClass = sCss
        deptoid.Enabled = bVal : deptoid.CssClass = sCss
    End Sub

    Private Sub generateReturnNo()
        Dim sNo As String = "RETBA-" & Format(GetServerTime(), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(retbano, " & DefaultFormatCounter & ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnreturnbamst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND retbano LIKE '%" & sNo & "%'"
        retbano.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT cmpcode, retbamstoid, periodacctg, retbadate, retbano, deptoid, retbamstnote, retbamststatus, createuser, createtime, upduser, updtime FROM QL_trnreturnbamst WHERE retbamstoid=" & sOid
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = xreader("cmpcode").ToString
                DDLBusUnit_SelectedIndexChanged(Nothing, Nothing)
                retbamstoid.Text = xreader("retbamstoid").ToString
                periodacctg.Text = xreader("periodacctg").ToString
                retbadate.Text = Format(xreader("retbadate"), "MM/dd/yyyy")
                retbano.Text = xreader("retbano").ToString
                deptoid.SelectedValue = xreader("deptoid").ToString
                retbamstnote.Text = xreader("retbamstnote").ToString
                retbamststatus.Text = xreader("retbamststatus").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            xreader.Close()
            conn.Close()
            btnSave.Visible = False : btnDelete.Visible = False : btnposting.Visible = False
            Exit Sub
        End Try
        If retbamststatus.Text = "Post" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnposting.Visible = False
            lblspuno.Text = "Return No."
            retbamstoid.Visible = False
            retbano.Visible = True
            btnAddToList.Visible = False
            gvDtl.Columns(0).Visible = False
            gvDtl.Columns(gvDtl.Columns.Count - 1).Visible = False
        End If
        sSql = "SELECT DISTINCT retbadtlseq, retd.acaramstoid, acarano, retbareftype, retd.acaradtloid, retbarefoid, itemCode AS retbarefcode, itemLongDescription AS retbareflongdesc, retbawhoid, g1.gendesc AS retbawh, retbaqty, (acaraqty - (ISNULL((SELECT SUM(retdx.retbaqty) FROM QL_trnreturnbadtl retdx WHERE retdx.cmpcode=retd.cmpcode AND retdx.acaradtloid=retd.acaradtloid AND retdx.retbamstoid<>retd.retbamstoid), 0.0))) AS acaraqty, retbaunitoid, g2.gendesc AS retbaunit, retbadtlnote, retbavalueidr, retbavalueusd, retbaqty_unitkecil, retbaqty_unitbesar, (SELECT ISNULL(genother1,'') acctgoid FROM QL_mstgen gx WHERE gx.cmpcode=retd.cmpcode AND gx.gengroup='GROUPITEM' AND gx.genoid=m.itemgroupoid AND activeflag='ACTIVE') AS stockacctgoid FROM QL_trnreturnbadtl retd INNER JOIN QL_trnbrtacaramst bam ON bam.cmpcode=retd.cmpcode AND bam.acaramstoid=retd.acaramstoid INNER JOIN QL_trnbrtacaradtl bad ON bad.cmpcode=retd.cmpcode AND bad.acaradtloid=retd.acaradtloid INNER JOIN QL_mstitem m ON m.itemoid=retd.retbarefoid INNER JOIN QL_mstgen g1 ON g1.genoid=retd.retbawhoid INNER JOIN QL_mstgen g2 ON g2.genoid=retd.retbaunitoid WHERE retbamstoid=" & sOid & " ORDER BY retbadtlseq"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnreturnbadtl")
        Session("TblDtl") = dt
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        ClearDetail()
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnReturnBA.aspx")
        End If
        If checkPagePermission("~\Transaction\trnReturnBA.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Berita Acara Return"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnposting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            CheckReturnStatus()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitAllDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                retbamstoid.Text = GenerateID("QL_TRNRETURNBAMST", CompnyCode)
                retbadate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                retbamststatus.Text = "In Process"
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvDtl.DataSource = dt
            gvDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnReturnBA.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lkbReturnInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbReturnInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = 0
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, retm.updtime, GETDATE()) > " & nDays & " AND retbamststatus='In Process'"
        If checkPagePermission("~\Transaction\trnReturnBA.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND retm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND retbadate>=CAST('" & FilterPeriod1.Text & " 00:00:00' AS DATETIME) AND retbadate<=CAST('" & CDate(FilterPeriod2.Text) & " 23:59:59' AS DATETIME)"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND retbamststatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Transaction\trnReturnBA.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND retm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        cbStatus.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Transaction\trnReturnBA.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND retm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        UpdateCheckedValue()
        PrintReport()
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLDept()
    End Sub

    Protected Sub btnSearchUsage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchUsage.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        If deptoid.SelectedValue = "" Then
            showMessage("Please select Department first!", 2)
            Exit Sub
        End If
        FilterDDLListUsage.SelectedIndex = -1 : FilterTextListUsage.Text = "" : gvListUsage.SelectedIndex = -1
        BindListUsage()
        cProc.SetModalPopUpExtender(btnHideListUsage, pnlListUsage, mpeListUsage, True)
    End Sub

    Protected Sub btnClearUsage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearUsage.Click
        acaramstoid.Text = "" : acarano.Text = ""
    End Sub

    Protected Sub btnFindListUsage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListUsage.Click
        BindListUsage()
        mpeListUsage.Show()
    End Sub

    Protected Sub btnAllListUsage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListUsage.Click
        FilterDDLListUsage.SelectedIndex = -1 : FilterTextListUsage.Text = "" : gvListUsage.SelectedIndex = -1
        BindListUsage()
        mpeListUsage.Show()
    End Sub

    Protected Sub gvListUsage_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListUsage.PageIndexChanging
        gvListUsage.PageIndex = e.NewPageIndex
        BindListUsage()
        mpeListUsage.Show()
    End Sub

    Protected Sub gvListUsage_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListUsage.SelectedIndexChanged
        If acaramstoid.Text <> gvListUsage.SelectedDataKey.Item("acaramstoid").ToString Then
            btnClearUsage_Click(Nothing, Nothing)
        End If
        acaramstoid.Text = gvListUsage.SelectedDataKey.Item("acaramstoid").ToString
        acarano.Text = gvListUsage.SelectedDataKey.Item("acarano").ToString
        cProc.SetModalPopUpExtender(btnHideListUsage, pnlListUsage, mpeListUsage, False)
    End Sub

    Protected Sub lbCloseListUsage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListUsage.Click
        cProc.SetModalPopUpExtender(btnHideListUsage, pnlListUsage, mpeListUsage, False)
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        If acaramstoid.Text = "" Then
            showMessage("Please select BA No. first!", 2)
            Exit Sub
        End If
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        Session("TblMat") = Nothing : Session("TblMatView") = Nothing : gvListMat.DataSource = Nothing : gvListMat.DataBind() : tbData.Text = "5" : gvListMat.PageSize = CInt(tbData.Text)
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblMat") Is Nothing Then
            BindMatData()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If Session("TblMat") Is Nothing Then
            BindMatData()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "acaradtloid=" & dtTbl.Rows(C1)("acaradtloid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "acaradtloid=" & dtTbl.Rows(C1)("acaradtloid")
                    objView(0)("checkvalue") = "False"
                    objView(0)("retbaqty") = 0
                    objView(0)("retbadtlnote") = ""
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    dtTbl.Rows(C1)("retbaqty") = 0
                    dtTbl.Rows(C1)("retbadtlnote") = ""
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Selected material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub lbShowData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbShowData.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Please find material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If tbData.Text = "" Then
            Session("WarningListMat") = "Please fill total data to be shown!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        Else
            If ToDouble(tbData.Text) <= 0 Then
                Session("WarningListMat") = "Total data to be shown must be more than 0!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat2() Then
            gvListMat.PageSize = Int(tbData.Text)
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(4).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 4)
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    dtView.RowFilter = ""
                    dtView.RowFilter = "checkvalue='True' AND retbaqty>0"
                    If dtView.Count <> iCheck Then
                        Session("WarningListMat") = "Return Qty for every checked material data must be more than 0!"
                        showMessage(Session("WarningListMat"), 2)
                        dtView.RowFilter = ""
                        Exit Sub
                    End If
                    dtView.RowFilter = ""
                    dtView.RowFilter = "CheckValue='True' AND retbaqty<=acaraqty"
                    If dtView.Count <> iCheck Then
                        Session("WarningListMat") = "Return Qty for every checked material data must be less than BA Qty!"
                        showMessage(Session("WarningListMat"), 2)
                        dtView.RowFilter = ""
                        Exit Sub
                    End If
                    If Session("TblDtl") Is Nothing Then
                        CreateTblDetail()
                    End If
                    Dim objTable As DataTable
                    objTable = Session("TblDtl")
                    Dim dv As DataView = objTable.DefaultView
                    Dim objRow As DataRow
                    Dim dQty_unitkecil, dQty_unitbesar As Double
                    Dim counter As Integer = objTable.Rows.Count
                    For C1 As Integer = 0 To dtView.Count - 1
                        Dim dMaxQty As Double = 0, dReturnQty As Double = 0
                        dv.RowFilter = "acaradtloid=" & dtView(C1)("acaradtloid")
                        dReturnQty = ToDouble(dtView(C1)("retbaqty").ToString)
                        dMaxQty = ToDouble(dtView(C1)("acaraqty").ToString)
                        If dv.Count > 0 Then
                            dv.AllowEdit = True
                            GetUnitConverter(dtView(C1)("acararefoid"), dtView(C1)("acaraunitoid"), dReturnQty, dQty_unitkecil, dQty_unitbesar)
                            dv(0)("retbaqty") = dReturnQty
                            dv(0)("acaraqty") = dMaxQty
                            dv(0)("retbadtlnote") = dtView(C1)("retbadtlnote")
                            dv(0)("retbavalueidr") = ToDouble(dtView(C1)("acaravalueidr").ToString)
                            dv(0)("retbavalueusd") = ToDouble(dtView(C1)("acaravalueusd").ToString)
                            dv(0)("retbaqty_unitkecil") = dQty_unitkecil
                            dv(0)("retbaqty_unitbesar") = dQty_unitbesar
                            dv(0)("stockacctgoid") = dtView(C1)("stockacctgoid").ToString
                        Else
                            counter += 1
                            objRow = objTable.NewRow()
                            objRow("retbadtlseq") = counter
                            objRow("acaramstoid") = acaramstoid.Text
                            objRow("acarano") = acarano.Text
                            objRow("retbareftype") = dtView(C1)("refname")
                            objRow("acaradtloid") = dtView(C1)("acaradtloid")
                            objRow("retbarefoid") = dtView(C1)("acararefoid")
                            objRow("retbarefcode") = dtView(C1)("acararefcode")
                            objRow("retbareflongdesc") = dtView(C1)("acarareflongdesc")
                            objRow("retbawhoid") = dtView(C1)("acarawhoid")
                            objRow("retbawh") = dtView(C1)("acarawh").ToString
                            objRow("acaraqty") = dMaxQty
                            objRow("retbaqty") = dReturnQty
                            objRow("retbaunitoid") = dtView(C1)("acaraunitoid")
                            objRow("retbaunit") = dtView(C1)("acaraunit")
                            objRow("retbadtlnote") = dtView(C1)("retbadtlnote")
                            objRow("retbavalueidr") = ToDouble(dtView(C1)("acaravalueidr").ToString)
                            objRow("retbavalueusd") = ToDouble(dtView(C1)("acaravalueusd").ToString)
                            GetUnitConverter(dtView(C1)("acararefoid"), dtView(C1)("acaraunitoid"), dReturnQty, dQty_unitkecil, dQty_unitbesar)
                            objRow("retbaqty_unitkecil") = dQty_unitkecil
                            objRow("retbaqty_unitbesar") = dQty_unitbesar
                            objRow("stockacctgoid") = dtView(C1)("stockacctgoid").ToString
                            objTable.Rows.Add(objRow)
                        End If
                        dv.RowFilter = ""
                    Next
                    dtView.RowFilter = ""
                    Session("TblDtl") = objTable
                    gvDtl.DataSource = objTable
                    gvDtl.DataBind()
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                    ClearDetail()
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            If Session("TblDtl") Is Nothing Then
                CreateTblDetail()
            End If
            Dim objTable As DataTable = Session("TblDtl")
            Dim dv As DataView = objTable.DefaultView
            If i_u2.Text = "New Detail" Then
                dv.RowFilter = "acaradtloid=" & acaradtloid.Text
            Else
                dv.RowFilter = "acaradtloid=" & acaradtloid.Text & " AND retbadtlseq<>" & retbadtlseq.Text
            End If
            If dv.Count > 0 Then
                showMessage("This Data has been added before, please check!", 2)
                dv.RowFilter = ""
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            Dim dQty_unitkecil, dQty_unitbesar As Double
            If i_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                objRow("retbadtlseq") = objTable.Rows.Count + 1
            Else
                objRow = objTable.Rows(retbadtlseq.Text - 1)
                objRow.BeginEdit()
            End If
            GetUnitConverter(retbarefoid.Text, retbaunitoid.SelectedValue, ToDouble(retbaqty.Text), dQty_unitkecil, dQty_unitbesar)
            objRow("acaramstoid") = acaramstoid.Text
            objRow("acarano") = acarano.Text
            objRow("acaradtloid") = acaradtloid.Text
            objRow("retbarefoid") = retbarefoid.Text
            objRow("retbarefcode") = retbarefcode.Text
            objRow("retbareflongdesc") = retbareflongdesc.Text
            objRow("retbawhoid") = retbawhoid.SelectedValue
            objRow("retbawh") = retbawhoid.SelectedItem.Text
            objRow("acaraqty") = ToDouble(acaraqty.Text)
            objRow("retbaqty") = ToDouble(retbaqty.Text)
            objRow("retbaqty_unitkecil") = dQty_unitkecil
            objRow("retbaqty_unitbesar") = dQty_unitbesar
            objRow("retbaunitoid") = retbaunitoid.SelectedValue
            objRow("retbaunit") = retbaunitoid.SelectedItem.Text
            objRow("retbadtlnote") = retbadtlnote.Text
            If i_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            objTable.AcceptChanges()
            Session("TblDtl") = objTable
            gvDtl.DataSource = Session("TblDtl")
            gvDtl.DataBind()
            ClearDetail()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
        End If
    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDtl.SelectedIndexChanged
        Try
            retbadtlseq.Text = gvDtl.SelectedDataKey.Item("retbadtlseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "retbadtlseq=" & retbadtlseq.Text
                acaramstoid.Text = dv.Item(0).Item("acaramstoid").ToString
                acarano.Text = dv.Item(0).Item("acarano").ToString
                acaradtloid.Text = dv.Item(0).Item("acaradtloid").ToString
                retbarefoid.Text = dv.Item(0).Item("retbarefoid").ToString
                retbarefcode.Text = dv.Item(0).Item("retbarefcode").ToString
                retbareflongdesc.Text = dv.Item(0).Item("retbareflongdesc").ToString
                retbawhoid.SelectedValue = dv.Item(0).Item("retbawhoid").ToString
                acaraqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("acaraqty").ToString), 4)
                retbaqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("retbaqty").ToString), 4)
                retbaunitoid.SelectedValue = dv.Item(0).Item("retbaunitoid").ToString
                retbadtlnote.Text = dv.Item(0).Item("retbadtlnote").ToString
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        Finally
            btnSearchUsage.Visible = False : btnClearUsage.Visible = False
            retbareftype.Enabled = False : retbareftype.CssClass = "inpTextDisabled"
            btnSearchMat.Visible = False
        End Try
    End Sub

    Protected Sub gvDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("retbadtlseq") = C1 + 1
            dr.EndEdit()
        Next
        objTable.AcceptChanges()
        Session("TblDtl") = objTable
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        ClearDetail()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_trnreturnbamst WHERE retbamstoid=" & retbamstoid.Text
                If CheckDataExists(sSql) Then
                    retbamstoid.Text = GenerateID("QL_TRNRETURNBAMST", CompnyCode)
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNRETURNBAMST", "retbamstoid", retbamstoid.Text, "retbamststatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    retbamststatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            retbadtloid.Text = GenerateID("QL_TRNRETURNBADTL", CompnyCode)
            Dim conmtroid As Integer = 0, crdmatoid As Integer = 0, glmstoid As Integer = 0, gldtloid As Integer = 0, stockvalueoid As Integer = 0
            Dim iReturnAcctgOid As Integer = 0, iStockRawAcctgOid As Integer = 0, iStockGenAcctgOid As Integer = 0
            Dim iStockSPAcctgOid As Integer = 0, iStockLogAcctgOid As Integer = 0, iStockSTAcctgOid As Integer = 0
            Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
            Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
            periodacctg.Text = GetDateToPeriodAcctg(CDate(retbadate.Text))
            Dim objValue As VarReturnValue
            Dim iGroupOid As Integer = 0
            If retbamststatus.Text = "Post" Then
                If isPeriodClosed(DDLBusUnit.SelectedValue, sPeriod) Then
                    showMessage("Cannot continue this transaction anymore because the period : " & MonthName(CInt(sPeriod.Substring(4, 2))) & " " & sPeriod.Substring(0, 4) & " have been closed.", 3)
                    Exit Sub
                End If
                conmtroid = GenerateID("QL_CONSTOCK", CompnyCode) : crdmatoid = GenerateID("QL_CRDSTOCK", CompnyCode)
                glmstoid = GenerateID("QL_TRNGLMST", CompnyCode) : gldtloid = GenerateID("QL_TRNGLDTL", CompnyCode)
                stockvalueoid = GenerateID("QL_STOCKVALUE", CompnyCode)
                Dim sVarErr As String = ""
                If Not IsInterfaceExists("VAR_STOCK_WIP", DDLBusUnit.SelectedValue) Then
                    sVarErr = "VAR_STOCK_WIP"
                End If
              
                If sVarErr <> "" Then
                    showMessage(GetInterfaceWarning(sVarErr, "posting"), 2)
                    retbamststatus.Text = "In Process"
                    Exit Sub
                End If
                iReturnAcctgOid = GetAcctgOID(GetVarInterface("VAR_STOCK_WIP", DDLBusUnit.SelectedValue), CompnyCode)
            
                generateReturnNo()
                iGroupOid = ToInteger(GetStrData("SELECT TOP 1 groupoid FROM QL_mstdeptgroupdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND deptoid=" & deptoid.SelectedValue & " ORDER BY updtime DESC"))

            End If

            ' Define Tabel Strucure utk Auto Jurnal 
            sSql = "SELECT '' tipe, 0 acctgoid, 0.0 debetidr, 0.0 creditidr, 0.0 debetusd, 0.0 creditusd, '' refno, '' note, '' AS glother1 "
            Dim tbPostGL As DataTable = cKon.ambiltabel(sSql, "PostGL")
            tbPostGL.Rows.Clear()
            Dim oRow As DataRow
            Dim dvCek As DataView = tbPostGL.DefaultView

            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trnreturnbamst (cmpcode, retbamstoid, periodacctg, retbadate, retbano, deptoid, retbamstnote, retbamststatus, createuser, createtime, upduser, updtime) VALUES ('" & DDLBusUnit.SelectedValue & "', " & retbamstoid.Text & ", '" & periodacctg.Text & "', '" & retbadate.Text & "', '" & retbano.Text & "', " & deptoid.SelectedValue & ", '" & Tchar(retbamstnote.Text) & "', '" & retbamststatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & retbamstoid.Text & " WHERE tablename='QL_TRNRETURNBAMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trnreturnbamst SET periodacctg='" & periodacctg.Text & "', retbadate='" & retbadate.Text & "', retbano='" & retbano.Text & "', deptoid=" & deptoid.SelectedValue & ", retbamstnote='" & Tchar(retbamstnote.Text) & "', retbamststatus='" & retbamststatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE retbamstoid=" & retbamstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnbrtacaradtl SET acaradtlstatus='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND acaradtloid IN (SELECT acaradtloid FROM QL_trnreturnbadtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND retbamstoid=" & retbamstoid.Text & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnbrtacaramst SET acaramststatus='Post' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND acaramstoid IN (SELECT acaramstoid FROM QL_trnreturnbadtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND retbamstoid=" & retbamstoid.Text & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trnreturnbadtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND retbamstoid=" & retbamstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trnreturnbadtl (cmpcode, retbadtloid, retbamstoid, retbadtlseq, acaramstoid, acaradtloid, retbareftype, retbarefoid, retbaqty, retbaunitoid, retbawhoid, retbadtlnote, retbadtlstatus, upduser, updtime, retbavalueidr, retbavalueusd, retbaqty_unitkecil, retbaqty_unitbesar) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C1 + CInt(retbadtloid.Text)) & ", " & retbamstoid.Text & ", " & C1 + 1 & ", " & objTable.Rows(C1).Item("acaramstoid") & ", " & objTable.Rows(C1).Item("acaradtloid") & ", '" & objTable.Rows(C1).Item("retbareftype").ToString & "', " & objTable.Rows(C1).Item("retbarefoid") & ", " & ToDouble(objTable.Rows(C1).Item("retbaqty").ToString) & ", " & objTable.Rows(C1).Item("retbaunitoid") & ", " & objTable.Rows(C1).Item("retbawhoid") & ", '" & Tchar(objTable.Rows(C1).Item("retbadtlnote")) & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(C1).Item("retbavalueidr").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("retbavalueusd").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("retbaqty_unitkecil").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("retbaqty_unitbesar").ToString) & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        If objTable.Rows(C1).Item("retbaqty") >= objTable.Rows(C1).Item("acaraqty") Then
                            sSql = "UPDATE QL_trnbrtacaradtl SET acaradtlstatus='Complete' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND acaradtloid=" & objTable.Rows(C1).Item("acaradtloid")
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_trnbrtacaramst SET acaramststatus='Closed' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND acaramstoid=" & objTable.Rows(C1).Item("acaramstoid") & " AND (SELECT COUNT(*) FROM QL_trnbrtacaradtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND acaramstoid=" & objTable.Rows(C1).Item("acaramstoid") & " AND acaradtloid<>" & objTable.Rows(C1).Item("acaradtloid") & " AND acaradtlstatus='')=0"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                        If retbamststatus.Text = "Post" Then
                            Dim sRef As String = ""
                            objValue.ValRaw_IDR += ToDouble(objTable.Rows(C1).Item("retbavalueidr").ToString) * ToDouble(objTable.Rows(C1).Item("retbaqty"))
                            objValue.ValRaw_USD += ToDouble(objTable.Rows(C1).Item("retbavalueusd").ToString) * ToDouble(objTable.Rows(C1).Item("retbaqty"))
                            ' Insert QL_conmat
                            sSql = "INSERT INTO QL_constock (cmpcode, constockoid, contype, typemin, trndate, periodacctg, formaction, formoid, refoid, refname, mtrlocoid, qtyin, qtyout, reason, note, upduser, updtime, valueidr, valueusd, deptoid, qtyin_unitbesar, qtyout_unitbesar) VALUES ('" & DDLBusUnit.SelectedValue & "', " & conmtroid & ", 'RETBA', 1, '" & sDate & "', '" & sPeriod & "', 'QL_trnreturnbadtl', " & retbamstoid.Text & ", " & objTable.Rows(C1).Item("retbarefoid") & ", '" & objTable.Rows(C1).Item("retbareftype").ToString & "', " & objTable.Rows(C1).Item("retbawhoid") & ", " & ToDouble(objTable.Rows(C1).Item("retbaqty")) & ", 0, 'Berita Acara Return', '" & retbano.Text & "(#" & objTable.Rows(C1).Item("acarano") & "#)" & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(C1).Item("retbavalueidr").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("retbavalueusd").ToString) & ", " & deptoid.SelectedValue & ", " & ToDouble(objTable.Rows(C1).Item("retbaqty_unitbesar")) & ", 0)"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            conmtroid += 1
                            ' Insert QL_crdmtr
                            sSql = "UPDATE QL_crdstock SET qtyin=qtyin + " & ToDouble(objTable.Rows(C1).Item("retbaqty")) & ", saldoakhir=saldoakhir + " & ToDouble(objTable.Rows(C1).Item("retbaqty")) & ", qtyin_unitbesar=qtyin_unitbesar + " & ToDouble(objTable.Rows(C1).Item("retbaqty_unitbesar")) & ", saldoakhir_unitbesar=saldoakhir_unitbesar + " & ToDouble(objTable.Rows(C1).Item("retbaqty_unitbesar")) & ", lasttranstype='QL_trnreturnbadtl', lasttransdate='" & sDate & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND refoid=" & objTable.Rows(C1).Item("retbarefoid") & " AND mtrlocoid=" & objTable.Rows(C1).Item("retbawhoid") & " AND periodacctg='" & sPeriod & "'"
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() = 0 Then
                                sSql = "INSERT INTO QL_crdstock (cmpcode, crdstockoid, periodacctg, refoid, refname, mtrlocoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir, lasttranstype, lasttransdate, upduser, updtime, createuser, createtime, closeuser, closingdate, qtyin_unitbesar, saldoakhir_unitbesar) VALUES ('" & DDLBusUnit.SelectedValue & "', " & crdmatoid & ", '" & sPeriod & "', " & objTable.Rows(C1).Item("retbarefoid") & ", '" & objTable.Rows(C1).Item("retbareftype").ToString & "', " & objTable.Rows(C1).Item("retbawhoid") & ", " & ToDouble(objTable.Rows(C1).Item("retbaqty")) & ", 0, 0, 0, 0, " & ToDouble(objTable.Rows(C1).Item("retbaqty")) & ", 'QL_trnreturnbadtl', '" & sDate & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', '1/1/1900', " & ToDouble(objTable.Rows(C1).Item("retbaqty_unitbesar")) & ", " & ToDouble(objTable.Rows(C1).Item("retbaqty_unitbesar")) & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                crdmatoid += 1
                            End If
                            sSql = GetQueryUpdateStockValue(ToDouble(objTable.Rows(C1).Item("retbaqty")), ToDouble(objTable.Rows(C1).Item("retbaqty_unitbesar")), ToDouble(objTable.Rows(C1).Item("retbavalueidr").ToString), ToDouble(objTable.Rows(C1).Item("retbavalueusd").ToString), "QL_trnreturnbadtl", sDate, Session("UserID"), DDLBusUnit.SelectedValue, sPeriod, objTable.Rows(C1).Item("retbarefoid"))
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() = 0 Then
                                sSql = GetQueryInsertStockValue(ToDouble(objTable.Rows(C1).Item("retbaqty")), ToDouble(objTable.Rows(C1).Item("retbaqty_unitbesar")), ToDouble(objTable.Rows(C1).Item("retbavalueidr").ToString), ToDouble(objTable.Rows(C1).Item("retbavalueusd").ToString), "QL_trnreturnbadtl", sDate, Session("UserID"), DDLBusUnit.SelectedValue, sPeriod, objTable.Rows(C1).Item("retbarefoid"), stockvalueoid)
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                stockvalueoid += 1
                            End If
                        End If

                        dvCek.RowFilter = "acctgoid=" & objTable.Rows(C1).Item("stockacctgoid").ToString & " "
                        If dvCek.Count > 0 Then
                            dvCek(0)("debetidr") += ToDouble(objTable.Rows(C1).Item("retbavalueidr").ToString) * ToDouble(objTable.Rows(C1).Item("retbaqty").ToString)
                            dvCek(0)("debetusd") += ToDouble(objTable.Rows(C1).Item("retbavalueusd").ToString) * ToDouble(objTable.Rows(C1).Item("retbaqty").ToString)
                            dvCek.RowFilter = ""
                        Else
                            dvCek.RowFilter = ""

                            oRow = tbPostGL.NewRow
                            oRow("acctgoid") = objTable.Rows(C1).Item("stockacctgoid")
                            oRow("debetidr") = ToDouble(objTable.Rows(C1).Item("retbavalueidr").ToString) * ToDouble(objTable.Rows(C1).Item("retbaqty").ToString)
                            oRow("debetusd") = ToDouble(objTable.Rows(C1).Item("retbavalueusd").ToString) * ToDouble(objTable.Rows(C1).Item("retbaqty").ToString)
                            tbPostGL.Rows.Add(oRow)
                        End If
                        tbPostGL.AcceptChanges()
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(retbadtloid.Text)) & " WHERE tablename='QL_TRNRETURNBADTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    If retbamststatus.Text = "Post" Then
                        Dim dTotalIDR As Double = tbPostGL.Compute("SUM(debetidr)", "").ToString
                        Dim dTotalUSD As Double = tbPostGL.Compute("SUM(debetusd)", "").ToString
                        If dTotalIDR > 0 Or dTotalUSD > 0 Then
                            ' Insert GL MST
                            sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & glmstoid & ", '" & sDate & "', '" & sPeriod & "', 'BA Return|No. " & retbano.Text & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 1, 1, 1, 1, " & dTotalUSD / dTotalIDR & ", " & dTotalUSD / dTotalIDR & ")"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            ' Insert GL DTL
                            Dim iSeq As Integer = 1
                            For C1 As Integer = 0 To tbPostGL.Rows.Count - 1
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, groupoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & tbPostGL.Rows(C1)("acctgoid").ToString & ", 'D', " & ToDouble(tbPostGL.Rows(C1)("debetidr").ToString) & ", '" & retbano.Text & "', 'BA Return|No. " & retbano.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(tbPostGL.Rows(C1)("debetidr").ToString) & ", " & ToDouble(tbPostGL.Rows(C1)("debetusd").ToString) & ", 'QL_trnreturnbamst " & retbamstoid.Text & "', " & iGroupOid & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iSeq += 1
                                gldtloid += 1
                            Next

                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, groupoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & iReturnAcctgOid & ", 'C', " & dTotalIDR & ", '" & retbano.Text & "', 'BA Return|No. " & retbano.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalIDR & ", " & dTotalUSD & ", 'QL_trnreturnbamst " & retbamstoid.Text & "', " & iGroupOid & ")"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_mstoid SET lastoid=" & glmstoid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_mstoid SET lastoid=" & gldtloid & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                        sSql = "UPDATE QL_mstoid SET lastoid=" & conmtroid - 1 & " WHERE tablename='QL_CONSTOCK' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        sSql = "UPDATE QL_mstoid SET lastoid=" & crdmatoid - 1 & " WHERE tablename='QL_CRDSTOCK' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()

                        xCmd.ExecuteNonQuery()
                        sSql = "UPDATE QL_mstoid SET lastoid=" & stockvalueoid - 1 & " WHERE tablename='QL_STOCKVALUE' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                End If
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.ToString, 1)
                        retbamststatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.ToString, 1)
                    retbamststatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.ToString, 2)
                conn.Close()
                retbamststatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & retbamstoid.Text & ".<BR>"
            End If
            If retbamststatus.Text = "Post" Then
                Session("SavedInfo") &= "Data have been posted with Return No. = " & retbano.Text & "."
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trnReturnBA.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnReturnBA.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If retbamstoid.Text = "" Then
            showMessage("Please select Berita Acara Return data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNRETURNBAMST", "retbamstoid", retbamstoid.Text, "retbamststatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                retbamststatus.Text = "In Process"
                Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trnbrtacaradtl SET acaradtlstatus='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND acaradtloid IN (SELECT DISTINCT acaradtloid FROM QL_trnreturnbadtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND retbamstoid=" & retbamstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_trnbrtacaramst SET acaramststatus='Post' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND acaramstoid IN (SELECT DISTINCT acaramstoid FROM QL_trnreturnbadtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND retbamstoid=" & retbamstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnreturnbadtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND retbamstoid=" & retbamstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnreturnbamst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND retbamstoid=" & retbamstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnReturnBA.aspx?awal=true")
    End Sub

    Protected Sub btnposting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnposting.Click
        retbamststatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub
#End Region

End Class
