Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_RawMaterialReceived
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Dim report1 As New ReportDocument
    Public folderReport As String = "~/Report/"
    Const iRoundDigit = 2

    Structure VarRate
        Public VarRateOid As Integer
        Public VarRateIDR As Double
        Public VarRateUSD As Double
    End Structure
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "podtloid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("mrqty") = ToDouble(GetTextBoxValue(row.Cells(4).Controls))
                                    dtView(0)("mrdtlnote") = GetTextBoxValue(row.Cells(6).Controls)
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl = dtView.ToTable
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "podtloid=" & cbOid
                                dtView2.RowFilter = "podtloid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("mrqty") = ToDouble(GetTextBoxValue(row.Cells(4).Controls))
                                    dtView(0)("mrdtlnote") = GetTextBoxValue(row.Cells(6).Controls)
                                    If dtView2.Count > 0 Then
                                        dtView2(0)("mrqty") = ToDouble(GetTextBoxValue(row.Cells(4).Controls))
                                        dtView2(0)("mrdtlnote") = GetTextBoxValue(row.Cells(6).Controls)
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If registerdtloid.Text = "" Then
            sError &= "- Please select MATERIAL field!<BR>"
        End If
        If mrqty.Text = "" Then
            sError &= "- Please fill RECEIVED QTY field!<BR>"
        Else
            If ToDouble(registerqty.Text) <= 0 Then
                sError &= "- RECEIVED QTY  must be more than 0!<BR>"
            Else
                If ToDouble(mrqty.Text) > (ToDouble(registerqty.Text) + (ToDouble(registerqty.Text) * (ToDouble(potolerance.Text) / 100))) Then
                    sError &= "- RECEIVED QTY  must be equal or less than PO. Qty <BR>"
                End If
                'If Not IsQtyRounded(ToDouble(mrqty.Text), ToDouble(matrawlimitqty.Text)) Then
                '    sError &= "- Quantity must be rounded by Round Qty!<BR>"
                'End If
            End If
        End If
       
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        Dim sDate As String = Format(CDate(mrrawdate.Text), "MM/dd/yyyy")
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
     
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If suppoid.Text = "" Then
            sError &= "- Please select SUPPLIER field!<BR>"
        End If
        If registermstoid.Text = "" Then
            sError &= "- Please select REG. NO. field!<BR>"
        End If
        If mrrawwhoid.SelectedValue = "" Then
            sError &= "- Please select WAREHOUSE field!<BR>"
        End If
        If receiver.Text = "" Then
            sError &= "- Please Fill RECEIVER field!<BR>"
        End If
        If sjno.Text = "" Then
            sError &= "- Please Fill SJ. No field!<BR>"
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill DETAIL DATA!<BR>"
        Else
            Dim objTbl As DataTable = Session("TblDtl")
            If objTbl.Rows.Count <= 0 Then
                sError &= "- Please fill DETAIL DATA!<BR>"
            Else
                If DDLBusUnit.SelectedValue <> "" Then
                    For C1 As Int16 = 0 To objTbl.Rows.Count - 1
                        sSql = "SELECT (poqty - ISNULL((SELECT SUM(mrqty) FROM QL_trnmrdtl mrd WHERE mrd.cmpcode=regd.cmpcode AND mrd.podtloid=regd.podtloid AND mrmstoid<>" & mrrawmstoid.Text & "), 0) /*- ISNULL((SELECT SUM(retd.retrawqty) FROM QL_trnretrawdtl retd WHERE retd.cmpcode=regd.cmpcode AND retd.registerdtloid=regd.registerdtloid), 0*/) AS poqty FROM QL_trnpodtl regd WHERE regd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND regd.podtloid=" & objTbl.Rows(C1)("podtloid").ToString
                        If ToDouble(objTbl.Rows(C1)("poqty").ToString) <> ToDouble(GetStrData(sSql)) Then
                            objTbl.Rows(C1)("poqty") = ToDouble(GetStrData(sSql))
                        End If
                    Next
                    objTbl.AcceptChanges()
                    For C1 As Int16 = 0 To objTbl.Rows.Count - 1
                        If objTbl.Rows(C1)("poqty") = 0 Then
                            sError &= "- PO. QTY for some detail data has been updated by another user. Please check that every detail PO. QTY must be more than 0!<BR>"
                            Exit For
                        End If
                    Next
                    Session("TblDtl") = objTbl
                    gvListDtl.DataSource = Session("TblDtl")
                    gvListDtl.DataBind()
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            mrrawmststatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function IsRegisterWillBeClosed() As Boolean
        Dim bRet As Boolean = False
        If Session("TblDtl") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            Dim sOid As String = ""
            For C1 As Integer = 0 To dt.Rows.Count - 1
                If ToDouble(dt.Rows(C1)("mrqty").ToString) >= ToDouble(dt.Rows(C1)("poqty").ToString) Then
                    sOid &= dt.Rows(C1)("podtloid").ToString & ","
                End If
            Next
            If sOid <> "" Then
                sOid = Left(sOid, sOid.Length - 1)
                sSql = "SELECT COUNT(*) FROM QL_trnpodtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND pomstoid=" & registermstoid.Text & " AND podtlstatus='' AND podtloid NOT IN (" & sOid & ")"
                bRet = Not CheckDataExists(sSql)
            End If
        End If
        Return bRet
    End Function

    Private Function GetImportCost() As Double
        sSql = "SELECT icd.curroid, SUM(icd.importvalue) AS importvalue FROM QL_trnimportdtl icd INNER JOIN QL_trnimportmst icm ON icm.cmpcode=icd.cmpcode AND icm.importmstoid=icd.importmstoid WHERE icd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND icm.registermstoid=" & registermstoid.Text & " GROUP BY icd.curroid"
        Dim dtImport As DataTable = cKon.ambiltabel(sSql, "QL_trnimportdtl")
        Dim dRet As Double = 0
        Dim cRateTmp As New ClassRate
        For C1 As Integer = 0 To dtImport.Rows.Count - 1
            cRateTmp.SetRateValue(dtImport.Rows(C1)("curroid"), registerdate.Text)
            dRet += ToDouble(dtImport.Rows(C1)("importvalue").ToString) * cRateTmp.GetRateMonthlyIDRValue
        Next
        Return dRet
    End Function

    Private Function GetAcctgStock(ByVal sType As String) As String
        GetAcctgStock = ""
        sSql = "SELECT ISNULL(genother1,'') acctgoid FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='GROUPITEM' AND gendesc='" & sType & "' AND activeflag='ACTIVE'"
        GetAcctgStock = GetStrData(sSql)
        Return GetAcctgStock
    End Function

    Private Function GetTextValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function UpdateBatcnumber() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblDtl") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDtl")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListDtl.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListDtl.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(8).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = True
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                Dim sNote As String = GetTextBoxValue(row.Cells(5).Controls)
                                dtView.RowFilter = "mrdtlseq=" & cbOid
                                If cbCheck Then
                                    If dtView.Count > 0 Then
                                        dtView(0)("batchnumber") = sNote
                                    End If
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                Session("TblDtl") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function GetHppPercentage(ByVal sOid As String) As Double
        Dim dReturn As Double = 0
        Try
            dReturn = ToDouble(GetStrData("SELECT TOP 1 gendesc FROM QL_mstgen WHERE activeflag='ACTIVE' AND genother1='" & sOid & "' AND gengroup='HPP PERCENTAGE (%)' ORDER BY updtime DESC"))
        Catch ex As Exception
            dReturn = 0
        End Try
        Return dReturn
    End Function

#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckMRStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM ql_TRNMRMST WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND mrmststatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnMR.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(cKon.ambilscalar(sSql).ToString) > 0 Then
            lbMRInProcess.Visible = True
            lbMRInProcess.Text = "You have " & cKon.ambilscalar(sSql).ToString & " In Process Raw Material Received data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitAllDDL()
        'Fill DDL Business Unit
        sSql = "SELECT '" & CompnyCode & "' divcode, '" & CompnyName & "' divname "

        FillDDL(DDLBusUnit, sSql)
        'Fill DDL Warehouse
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='WAREHOUSE' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDL(mrrawwhoid, sSql)
        'Fill DDL Unit
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='UNIT' AND activeflag='ACTIVE'"
        FillDDL(unit1, sSql)
        FillDDL(unit2, sSql)
        FillDDL(mrrawunitoid, sSql)
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT mrmstoid, mrno, CONVERT(VARCHAR(10), mrdate, 101) AS mrdate, suppname, pono, mrmststatus, mrmstnote, 'False' AS checkvalue FROM QL_trnmrmst mrm INNER JOIN QL_trnpomst reg ON reg.cmpcode=mrm.cmpcode AND reg.pomstoid=mrm.pomstoid INNER JOIN QL_mstsupp s ON s.suppoid=mrm.suppoid WHERE mrm.cmpcode='" & CompnyCode & "' "
        sSql &= sSqlPlus & " ORDER BY CONVERT(DATETIME, mrm.mrdate) DESC, mrm.mrmstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_TRNMRMST")
        gvList.DataSource = Session("TblMst")
        gvList.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvList.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvList.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvList.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "mrmstoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub BindSupplierData()
        sSql = "SELECT DISTINCT suppoid, suppcode, suppname, suppaddr FROM QL_mstsupp WHERE cmpcode='" & CompnyCode & "' AND " & FilterDDLListSupp.SelectedValue & " LIKE '%" & Tchar(FilterTextListSupp.Text) & "%' AND suppoid IN (SELECT suppoid FROM QL_trnregistermst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND registertype='RAW' AND registermststatus='Post' /*AND (CASE registerflag WHEN 'IMPORT' THEN registermstres1 ELSE 'Closed' END)='Closed'*/ AND ISNULL(registermstres2, '')<>'Closed') ORDER BY suppcode, suppname"
        'FillGV(gvListSupp, sSql, "QL_mstsupp")
        Dim objTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstsupp")
        Session("mstSupp") = objTbl
        gvListSupp.DataSource = objTbl
        gvListSupp.DataBind()
    End Sub

    Private Sub BindRegisterData()
        sSql = "SELECT pom.pomstoid, pom.pono, CONVERT(VARCHAR(10), pom.podate, 101) AS podate, pom.posuppref, pom.potaxtype, g.gendesc AS pogroup, pom.pomstnote, pom.curroid, s.suppoid, s.suppname, pom.potype AS poflag FROM QL_trnpomst pom INNER JOIN QL_mstgen g ON g.gencode=pom.pogroup INNER JOIN QL_mstsupp s ON s.suppoid=pom.suppoid WHERE pom.cmpcode='" & CompnyCode & "' AND " & FilterDDLListReg.SelectedValue & " LIKE '%" & Tchar(FilterTextListReg.Text) & "%' AND pom.pomststatus='Approved '/*AND (CASE registerflag WHEN 'IMPORT' THEN registermstres1 ELSE 'Closed' END)='Closed'*/  ORDER BY pomstoid"
        Dim objTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnregistermst")
        Session("Register") = objTbl
        gvListReg.DataSource = objTbl
        gvListReg.DataBind()
    End Sub

    Private Sub BindMaterialData()
        Dim sAdd As String = ""
        If Not Session("oid") Is Nothing And Session("oid") <> "" Then
            sAdd = "AND mrmstoid<>" & Session("oid")
        End If
        sSql = "SELECT 'False' AS checkvalue, podtloid, pono, regd.matoid AS matoid, itemCode AS matcode, itemLongDescription AS matlongdesc, roundQty AS matlimitqty, pounitoid, g.gendesc AS pounit, ((poqty-ISNULL(closeqty,0)) - (ISNULL((SELECT SUM(mrqty) FROM QL_trnmrdtl mrd WHERE mrd.cmpcode=regd.cmpcode AND mrd.podtloid=regd.podtloid " & sAdd & "), 0) - ISNULL((SELECT SUM(retd.retqty) FROM QL_trnreturdtl retd INNER JOIN QL_trnmrdtl mrd ON mrd.cmpcode=retd.cmpcode AND mrd.mrdtloid=retd.mrdtloid WHERE mrd.podtloid=regd.podtloid AND retd.retmstoid IN (SELECT DISTINCT retm.retmstoid FROM QL_trnreturmst retm WHERE retmststatus='Approved')), 0))) AS poqty, 0.0 AS mrqty, '' AS mrdtlnote, podtlseq, (CONVERT(DECIMAL(18,2), (CASE podtldiscvalue WHEN 0 THEN (podtlamt/poqty) ELSE (podtlnetto/poqty) END))) AS mrvalue, podtlnetto, pomstdiscamt, pomstdiscvalue, pomstdisctype,g2.gendesc AS pounit1, g3.gendesc AS pounit2, g2.genoid AS pounit1oid, g3.genoid AS pounit2oid, regd.poqty_unitkecil, regd.poqty_unitbesar, regm.potolerance,'' AS toleransi, regd.poqty AS porealqty, unit2unit1conversion AS unit2to1, unit3unit1conversion AS unit3to1, itemunit1 AS unit1, itemunit2 AS unit2, itemunit3 AS unit3, '' AS batchnumber, regd.podtlnote, ISNULL((SELECT cat2res2 FROM QL_mstcat2 cat2 WHERE cat2.cat2oid=m.itemcat2),'') AS itemcat2, poprice, regm.pomstoid, ISNULL((SELECT SUM(podx.podtlnetto) FROM QL_trnpodtl podx WHERE podx.pomstoid=regm.pomstoid),0) AS totaldtlnetto FROM QL_trnpodtl regd INNER JOIN QL_trnpomst regm ON regd.pomstoid=regm.pomstoid INNER JOIN QL_mstitem m ON m.itemoid=regd.matoid INNER JOIN QL_mstgen g ON genoid=pounitoid INNER JOIN QL_mstgen g2 ON g2.genoid=m.itemUnit1 INNER JOIN QL_mstgen g3 ON g3.genoid=m.itemUnit3 WHERE regd.cmpcode='" & CompnyCode & "' AND regd.pomstoid=" & registermstoid.Text & " AND podtlstatus='' /*AND ISNULL(registerdtlres2, '')<>'Complete'*/ ORDER BY podtlseq"
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnregisterdtl")
        Dim dSelisih1 As Double = 0
        Dim dSelisih2 As Double = 0
        For C1 As Integer = 0 To dtTbl.Rows.Count - 1
            dtTbl.Rows(C1).Item("mrqty") = ToMaskEdit(ToDouble(dtTbl.Rows(C1).Item("poqty").ToString), 2)
            dtTbl.Rows(C1).Item("toleransi") = ToMaskEdit(ToDouble(dtTbl.Rows(C1).Item("potolerance").ToString), 2) & "%"
            dtTbl.Rows(C1).Item("mrdtlnote") = dtTbl.Rows(C1).Item("podtlnote").ToString
            Dim dNetto As Double = ToDouble(dtTbl.Rows(C1).Item("mrvalue").ToString)
            Dim dDiscProp As Double
            If ToDouble(dtTbl.Rows(C1).Item("pomstdiscamt").ToString) > 0 Then
                If dtTbl.Rows(C1).Item("pomstdisctype").ToString = "P" Then
                    dDiscProp = ((dNetto * ToDouble(dtTbl.Rows(C1).Item("pomstdiscvalue").ToString)) / 100)
                Else
                    dDiscProp = dNetto * (ToDouble(dtTbl.Rows(C1).Item("pomstdiscamt").ToString) / ToDouble(dtTbl.Rows(C1).Item("totaldtlnetto").ToString))
                End If
            Else
                dDiscProp = 0
            End If
            dtTbl.Rows(C1).Item("mrvalue") = dNetto - dDiscProp
        Next
        dtTbl.AcceptChanges()
        If dtTbl.Rows.Count > 0 Then
            Session("TblMat") = dtTbl
            Session("TblMatView") = dtTbl
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            showMessage("Material data can't be found!", 2)
        End If
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("QL_trnmrrawdtl")
        dtlTable.Columns.Add("mrdtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("podtloid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("pono", Type.GetType("System.String"))
        dtlTable.Columns.Add("matoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("matlongdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("matlimitqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("poqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("porealqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("pounitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("poqty_unitkecil", Type.GetType("System.Double"))
        dtlTable.Columns.Add("poqty_unitbesar", Type.GetType("System.Double"))
        dtlTable.Columns.Add("mrqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("mrbonusqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("mrunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("mrunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("mrunit1oid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("mrunit1", Type.GetType("System.String"))
        dtlTable.Columns.Add("mrunit2oid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("mrunit2", Type.GetType("System.String"))
        dtlTable.Columns.Add("mrdtlnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("mrrawprice", Type.GetType("System.Double"))
        dtlTable.Columns.Add("mrvalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("mrdtlamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("potolerance", Type.GetType("System.Double"))
        dtlTable.Columns.Add("mrqty_unitkecil", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("mrqty_unitbesar", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("itemcat2", Type.GetType("System.String"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub ClearDetail()
        mrrawdtlseq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                EnableHeader(False)
                mrrawdtlseq.Text = objTable.Rows.Count + 1
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        i_u2.Text = "New Detail"
        registerdtloid.Text = ""
        matrawoid.Text = ""
        matrawcode.Text = ""
        matrawlongdesc.Text = ""
        matrawlimitqty.Text = ""
        porawno.Text = ""
        registerqty.Text = ""
        mrqty.Text = ""
        mrrawqty.Text = ""
        mrrawbonusqty.Text = ""
        mrrawunitoid.Items.Clear()
        mrrawdtlnote.Text = ""
        mrrawvalue.Text = ""
        qtyunitkecil.Text = 0
        qtyunitbesar.Text = 0
        unit1.SelectedIndex = -1
        unit2.SelectedIndex = -1
        gvListDtl.SelectedIndex = -1
        btnSearchMat.Visible = True
        maxqty1.Text = "0.00"
        maxqty2.Text = "0.00"
        potolerance.Text = ""
        qtyunitkecil.Text = ""
        qtyunitbesar.Text = ""
        porealqty.Text = ""
        unit2to1.Text = ""
        unit3to1.Text = ""
        batchnumber.Text = ""
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal : DDLBusUnit.CssClass = sCss
        btnSearchSupp.Visible = bVal : btnClearSupp.Visible = bVal
        btnSearchReg.Visible = bVal : btnClearReg.Visible = bVal
    End Sub

    Private Sub GenerateRMRNo()
        Dim sNo As String = "LPB-" & Format(CDate(mrrawdate.Text), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(mrno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnmrmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrno LIKE '" & sNo & "%'"
        mrrawno.Text = GenNumberString(sNo, "", cKon.ambilscalar(sSql), DefaultFormatCounter)
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT mrm.cmpcode, mrmstoid, mrm.periodacctg, mrm.mrdate, mrm.mrno, mrm.suppoid, suppname, reg.pomstoid, pono, reg.potype AS registerflag, podate, mrwhoid, mrmststatus, mrmstnote, mrm.createuser, mrm.createtime, mrm.upduser, mrm.updtime, mrm.curroid, receiver, sjno, sjdate, podate, g.gendesc AS pogroup FROM QL_trnmrmst mrm INNER JOIN QL_trnpomst reg ON reg.cmpcode=mrm.cmpcode AND reg.pomstoid=mrm.pomstoid INNER JOIN QL_mstsupp s ON s.suppoid=mrm.suppoid INNER JOIN QL_mstgen g ON g.gencode=reg.pogroup WHERE mrm.mrmstoid=" & sOid
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = xreader("cmpcode").ToString
                mrrawmstoid.Text = xreader("mrmstoid").ToString
                periodacctg.Text = xreader("periodacctg").ToString
                mrrawdate.Text = Format(xreader("mrdate"), "MM/dd/yyyy")
                podate.Text = Format(xreader("podate"), "MM/dd/yyyy")
                mrrawno.Text = xreader("mrno").ToString
                suppoid.Text = xreader("suppoid").ToString
                suppname.Text = xreader("suppname").ToString
                registermstoid.Text = xreader("pomstoid").ToString
                registerno.Text = xreader("pono").ToString
                registerflag.Text = xreader("registerflag").ToString
                mrrawwhoid.SelectedValue = xreader("mrwhoid").ToString
                mrrawmstnote.Text = xreader("mrmstnote").ToString
                mrrawmststatus.Text = xreader("mrmststatus").ToString
                receiver.Text = xreader("receiver").ToString
                sjno.Text = xreader("sjno").ToString
                sjdate.Text = Format(xreader("sjdate"), "MM/dd/yyyy")
                potype.Text = xreader("pogroup").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
                curroid.Text = xreader("curroid").ToString
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False : btnPost.Visible = False
            Exit Sub
        End Try
        DDLBusUnit.Enabled = False : DDLBusUnit.CssClass = "inpTextDisabled"
        If mrrawmststatus.Text = "Post" Or mrrawmststatus.Text = "Closed" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnPost.Visible = False
            btnAddToList.Visible = False
            gvListDtl.Columns(0).Visible = False
            gvListDtl.Columns(gvListDtl.Columns.Count - 1).Visible = False
            lblTrnNo.Text = "MR No."
            mrrawmstoid.Visible = False
            mrrawno.Visible = True
        End If
        sSql = "SELECT mrdtlseq, mrd.podtloid, pono, mrd.matoid, itemCode AS matcode, itemLongDescription AS matlongdesc, roundQty AS  matlimitqty, (poqty - (ISNULL((SELECT SUM(mrqty) FROM QL_trnmrdtl x WHERE x.cmpcode=reg.cmpcode AND x.podtloid=reg.podtloid AND x.mrmstoid<>mrd.mrmstoid), 0) - ISNULL((SELECT SUM(retd.retqty) FROM QL_trnreturdtl retd INNER JOIN QL_trnmrdtl mrd ON mrd.cmpcode=retd.cmpcode AND mrd.mrdtloid=retd.mrdtloid WHERE mrd.cmpcode=mrd.cmpcode AND mrd.podtloid=reg.podtloid AND retd.retmstoid IN (SELECT DISTINCT retm.retmstoid FROM QL_trnreturmst retm WHERE retmststatus IN ('Approved', 'Closed'))), 0))) AS poqty, mrqty, mrbonusqty, mrunitoid, g.gendesc AS mrunit, mrdtlnote, mrvalue, mrdtlamt, mrqty_unitkecil, mrqty_unitbesar, g1.gendesc AS mrunit1, g2.gendesc AS mrunit2, g1.genoid AS mrunit1oid, g2.genoid AS mrunit2oid, poqty_unitkecil, poqty_unitbesar, pom.potolerance,'' AS toleransi, mrrawprice, reg.poqty AS porealqty, unit2unit1conversion AS unit2to1, unit3unit1conversion AS unit3to1, itemunit1 AS unit1, itemunit2 AS unit2, itemunit3 AS unit3, ISNULL((SELECT cat2res2 FROM QL_mstcat2 cat2 WHERE cat2.cat2oid=m.itemcat2),'') AS itemcat2 FROM QL_trnmrdtl mrd INNER JOIN QL_mstitem m ON m.itemoid=mrd.matoid INNER JOIN QL_mstgen g ON genoid=mrunitoid INNER JOIN QL_trnpodtl reg ON reg.cmpcode=mrd.cmpcode AND reg.podtloid=mrd.podtloid INNER JOIN QL_trnpomst pom ON pom.cmpcode=reg.cmpcode AND pom.pomstoid=reg.pomstoid INNER JOIN QL_mstgen g1 ON g1.genoid=m.itemunit1 INNER JOIN QL_mstgen g2 ON g2.genoid=m.itemunit3 WHERE mrmstoid=" & sOid & " ORDER BY mrdtlseq"
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnmwdtl")
        For C1 As Integer = 0 To dtTbl.Rows.Count - 1
            dtTbl.Rows(C1).Item("toleransi") = ToMaskEdit(ToDouble(dtTbl.Rows(C1).Item("potolerance").ToString), 2) & "%"
        Next
        Session("TblDtl") = dtTbl
        gvListDtl.DataSource = dtTbl
        gvListDtl.DataBind()
        ClearDetail()
    End Sub

    Private Sub PrintReport()

        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("mrmstoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            report.Load(Server.MapPath(folderReport & "rptMR.rpt"))
            'Dim sWhere As String

            sSql = "SELECT mrm.cmpcode,div.divname,mrm.mrmstoid,mrm.mrno AS [No LPB],pom.pono AS [No PO],mrm.sjno,pom.podate,i.itemCode AS itemCode,i.itemLongDescription AS itemDesc,mrd.mrqty,un.gendesc AS unit,sp.suppoid,gp.gendesc,sp.suppname,sp.suppaddr AS SupAdress,ct.gendesc AS City,gp.gendesc+'. '+sp.suppname AS [PREFIX NAME],mrm.mrmststatus,mrm.createuser,pom.pogroup AS pGroup ," & _
            " CASE WHEN DATENAME(dw ,mrm.mrdate)='Sunday' Then 'Minggu'" & _
            " WHEN DATENAME(dw, mrm.mrdate)='Monday' Then 'Senin' WHEN DATENAME(dw, mrm.mrdate)='Tuesday' Then 'Selasa'" & _
            " WHEN DATENAME(dw, mrm.mrdate)='Wednesday' Then 'Rabu' WHEN DATENAME(dw, mrm.mrdate)='Thursday' Then 'Kamis'" & _
            " WHEN DATENAME(dw, mrm.mrdate)='Friday' Then 'Jumat' ELSE 'Sabtu' END as sDay,Day(mrm.mrdate) AS sDate," & _
            " CASE WHEN MONTH(mrm.mrdate)=1 Then 'Januari' WHEN MONTH(mrm.mrdate)=2 Then 'Februari' WHEN MONTH(mrm.mrdate)=3 Then 'Maret'" & _
            " WHEN MONTH(mrm.mrdate)=4 Then 'April' WHEN MONTH(mrm.mrdate)=5 Then 'Mei' WHEN MONTH(mrm.mrdate)=6 Then 'Juni'" & _
            " WHEN MONTH(mrm.mrdate)=7 Then 'Juli' WHEN MONTH(mrm.mrdate)=8 Then 'Agustus' WHEN MONTH(mrm.mrdate)=9 Then 'September'" & _
            " WHEN MONTH(mrm.mrdate)=10 Then 'Oktober' WHEN MONTH(mrm.mrdate)=11 Then 'November' WHEN MONTH(mrm.mrdate)=12 Then 'Desember' " & _
            " END sMonth, mrm.mrdate AS sYears, pom.potaxtype AS TypeTax,div.divaddress,div.divaddress1,cd.gendesc AS kotadiv,div.divphone AS Phone,div.divfax1 AS Fax, '' AS [Batch Number] FROM QL_trnmrmst mrm  " & _
            " INNER JOIN QL_trnmrdtl mrd ON mrm.cmpcode=mrd.cmpcode AND mrm.mrmstoid=mrd.mrmstoid" & _
            " INNER JOIN QL_trnpomst pom ON pom.pomstoid=mrm.pomstoid AND pom.cmpcode=mrm.cmpcode" & _
            " INNER JOIN QL_mstdivision div ON div.divcode=mrm.cmpcode " & _
            " INNER JOIN QL_mstitem i ON i.itemoid=mrd.matoid " & _
            " INNER JOIN QL_mstsupp sp ON sp.suppoid=mrm.suppoid " & _
            " INNER JOIN QL_mstgen ct ON ct.genoid=sp.suppcityOid AND ct.gengroup='CITY'" & _
            " INNER JOIN QL_mstgen un ON un.genoid=mrd.mrunitoid AND un.gengroup='UNIT'" & _
            " INNER JOIN QL_mstgen gp ON gp.genoid=sp.suppprefixoid AND gp.gengroup='PREFIX NAME'" & _
            " INNER JOIN QL_mstgen cd ON div.divcityoid=cd.genoid AND cd.gengroup='CITY'" & _
            " WHERE mrm.cmpcode='" & Session("CompnyCode") & "' "
            'If Session("CompnyCode") <> CompnyCode Then
            '    sSql &= " WHERE mrm.cmpcode='" & Session("CompnyCode") & "'"
            'Else
            '    sSql &= " WHERE mrm.cmpcode LIKE '%'"
            'End If
            If sOid = "" Then
                sSql &= " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sSql &= " AND mrm.mrdate>='" & FilterPeriod1.Text & " 00:00:00' AND mrm.mrdate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    If FilterDDLStatus.SelectedValue <> "All" Then
                        sSql &= " AND mrm.mrmststatus='" & FilterDDLStatus.SelectedValue & "'"
                    End If
                End If
                If checkPagePermission("~\Transaction\trnMR.aspx", Session("SpecialAccess")) = False Then
                    sSql &= " AND mrm.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sSql &= " AND mrm.mrmstoid IN (" & sOid & ")"
            End If
            sSql &= " ORDER BY mrm.mrmstoid"

            Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_TRNMRMST")
            Dim dvTbl As DataView = dtTbl.DefaultView
            report.SetDataSource(dvTbl.ToTable)
            'report.SetParameterValue("Header", "RAW MATERIAL RECEIVED PRINT OUT")
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))

            'report.PrintOptions.PaperSize = PaperSize.PaperStatement
            'report.PrintOptions.PaperOrientation = PaperOrientation.Landscape

            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "MATERIAL_RECEIVED_PRINT_OUT")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            report.Close()
            report.Dispose()
        End Try
        'Response.Redirect("~\Transaction\trnMR.aspx?awal=true")
    End Sub

    Private Sub GetLastData(ByRef dtHdr As DataTable, ByRef dtDtl As DataTable)
        sSql = "SELECT mrmstoid, mrdate, mrno, mrwhoid FROM QL_trnmrmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrmststatus='Post' AND mrno NOT IN (SELECT noref FROM QL_trngldtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND noref LIKE 'MR-%') AND pomstoid=" & registermstoid.Text
        If Session("oid") IsNot Nothing And Session("oid") <> "" Then
            sSql &= " AND mrmstoid<>" & Session("oid")
        End If
        sSql &= " ORDER BY mrmstoid"
        dtHdr = cKon.ambiltabel(sSql, "QL_trnmrmstlast")
        sSql = "SELECT mrm.mrmstoid, mrdtloid, mrdtlseq, matoid, mrqty, mrvalue, (mrqty * mrvalue) AS mrdtlamt, mrqty_unitkecil AS mrqty_unitkecil FROM QL_trnmrmst mrm INNER JOIN QL_trnmrdtl mrd ON mrd.cmpcode=mrm.cmpcode AND mrd.mrmstoid=mrm.mrmstoid WHERE mrm.cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrmststatus='Post' AND mrno NOT IN (SELECT noref FROM QL_trngldtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND noref LIKE 'MR-%') AND pomstoid=" & registermstoid.Text
        If Session("oid") IsNot Nothing And Session("oid") <> "" Then
            sSql &= " AND mrm.mrmstoid<>" & Session("oid")
        End If
        sSql &= " ORDER BY mrm.mrmstoid, mrdtlseq"
        dtDtl = cKon.ambiltabel(sSql, "QL_trnmrdtllast")
    End Sub

    Sub cekEnabled()
        If GetStrData("select isnull(convertable,0) from QL_mstmatraw where matrawoid=" & matrawoid.Text & "") = 1 Then
            qtyunitkecil.Enabled = True
            qtyunitbesar.Enabled = True
            qtyunitkecil.CssClass = "inpText"
            qtyunitbesar.CssClass = "inpText"
        Else
            qtyunitkecil.Enabled = False
            qtyunitbesar.Enabled = False
            qtyunitkecil.CssClass = "inpTextDisabled"
            qtyunitbesar.CssClass = "inpTextDisabled"
        End If
    End Sub

    Sub calcKonversi()
        If mrrawunitoid.SelectedValue = GetStrData("SELECT itemunit1 FROM QL_mstitem WHERE itemoid='" & matrawoid.Text & "'") Then
            qtyunitkecil.Text = ToDouble(mrqty.Text)
            qtyunitbesar.Text = ToMaskEdit(ToDouble(mrqty.Text) / ToDouble(GetStrData("SELECT unit3unit1conversion FROM QL_mstitem WHERE itemoid=" & matrawoid.Text & "")), GetRoundValue(ToDouble(mrqty.Text) / ToDouble(GetStrData("SELECT unit3unit1conversion FROM QL_mstitem WHERE itemoid=" & matrawoid.Text & ""))))
        ElseIf mrrawunitoid.SelectedValue = GetStrData("SELECT itemunit2 FROM QL_mstitem WHERE itemoid='" & matrawoid.Text & "'") Then
            qtyunitkecil.Text = ToMaskEdit(ToDouble(mrqty.Text) * ToDouble(GetStrData("SELECT unit2unit1conversion FROM QL_mstitem WHERE itemoid=" & matrawoid.Text & "")), 4)
            qtyunitbesar.Text = ToMaskEdit(ToDouble(qtyunitkecil.Text) / ToDouble(GetStrData("SELECT unit3unit1conversion FROM QL_mstitem WHERE itemoid=" & matrawoid.Text & "")), GetRoundValue(ToDouble(qtyunitkecil.Text) / ToDouble(GetStrData("SELECT unit3unit1conversion FROM QL_mstitem WHERE itemoid=" & matrawoid.Text & ""))))
        Else
            qtyunitkecil.Text = ToMaskEdit(ToDouble(mrqty.Text) * ToDouble(GetStrData("SELECT unit3unit1conversion FROM QL_mstitem WHERE itemoid=" & matrawoid.Text & "")), 4)
            qtyunitbesar.Text = ToDouble(mrqty.Text)
        End If
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnMR.aspx")
        End If
        If checkPagePermission("~\Transaction\trnMR.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Material Received"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPost.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            CheckMRStatus()
            InitAllDDL()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                mrrawmstoid.Text = GenerateID("QL_TRNMRMST", CompnyCode)
                mrrawmststatus.Text = "In Process"
                mrrawdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                sjdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime)
                TabContainer1.ActiveTabIndex = 0
                btnDelete.Visible = False
                btnShowCOA.Visible = False
            End If
            If Not Session("TblDtl") Is Nothing Then
                Dim dt As New DataTable
                dt = Session("TblDtl")
                gvListDtl.DataSource = dt
                gvListDtl.DataBind()
            End If
            mrqty.Enabled = True : mrqty.CssClass = "inpText"
            batchnumber.Visible = False : Label12.Visible = False
        End If
       
        If ToDouble(Format(GetServerTime(), "MM")) = 1 Then
            If ToDouble(Format(GetServerTime(), "dd")) <= 10 Then
                imbprdate.Visible = True
            End If
        Else
            If ToDouble(Format(GetServerTime(), "dd")) <= 3 Then
                imbprdate.Visible = True
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnMR.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lbMRInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbMRInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, mrm.updtime, GETDATE()) > " & nDays & " AND mrrawmststatus='In Process' "
        If checkPagePermission("~\Transaction\trnMR.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND mrm.createuser='" & Session("UserID") & "' "
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND mrdate>='" & FilterPeriod1.Text & " 00:00:00' AND mrdate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            If FilterDDLStatus.SelectedValue <> "All" Then
                sSqlPlus &= " AND mrmststatus='" & FilterDDLStatus.SelectedValue & "'"
            End If
        End If
        If checkPagePermission("~\Transaction\trnMR.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND mrm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Transaction\trnMR.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND mrm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub gvList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvList.PageIndexChanging
        UpdateCheckedValue()
        gvList.PageIndex = e.NewPageIndex
        gvList.DataSource = Session("TblMst")
        gvList.DataBind()
    End Sub

    Protected Sub gvList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvList.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvList.DataSource = Session("TblMst")
            gvList.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        btnClearSupp_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupp.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        FilterDDLListSupp.SelectedIndex = -1 : FilterTextListSupp.Text = "" : gvListSupp.SelectedIndex = -1
        BindSupplierData()
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
    End Sub

    Protected Sub btnClearSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupp.Click
        suppoid.Text = ""
        suppname.Text = ""
        btnClearReg_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnFindListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSupp.Click
        BindSupplierData()
        mpeListSupp.Show()
    End Sub

    Protected Sub btnAllListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListSupp.Click
        FilterDDLListSupp.SelectedIndex = -1 : FilterTextListSupp.Text = "" : gvListSupp.SelectedIndex = -1
        BindSupplierData()
        mpeListSupp.Show()
    End Sub

    Protected Sub gvListSupp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSupp.PageIndexChanging
        gvListSupp.PageIndex = e.NewPageIndex
        BindSupplierData()
        mpeListSupp.Show()
    End Sub

    Protected Sub gvListSupp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListSupp.SelectedIndexChanged
        If suppoid.Text <> gvListSupp.SelectedDataKey.Item("suppoid").ToString Then
            btnClearSupp_Click(Nothing, Nothing)
        End If
        suppoid.Text = gvListSupp.SelectedDataKey.Item("suppoid").ToString
        suppname.Text = gvListSupp.SelectedDataKey.Item("suppname").ToString
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub lkbCloseListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListSupp.Click
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub btnSearchReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchReg.Click
        'If suppoid.Text = "" Then
        '    showMessage("Please select Supplier first!", 2)
        '    Exit Sub
        'End If
        FilterDDLListReg.SelectedIndex = -1 : FilterTextListReg.Text = "" : gvListReg.SelectedIndex = -1
        BindRegisterData()
        cProc.SetModalPopUpExtender(btnHideListReg, pnlListReg, mpeListReg, True)
    End Sub

    Protected Sub btnClearReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearReg.Click
        registermstoid.Text = "" : registerno.Text = "" : registerflag.Text = "" : curroid.Text = "" : potype.Text = ""
    End Sub

    Protected Sub btnFindListReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListReg.Click
        BindRegisterData()
        mpeListReg.Show()
    End Sub

    Protected Sub btnAllListReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListReg.Click
        FilterDDLListReg.SelectedIndex = -1 : FilterTextListReg.Text = "" : gvListReg.SelectedIndex = -1
        BindRegisterData()
        mpeListReg.Show()
    End Sub

    Protected Sub gvListReg_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListReg.PageIndexChanging
        gvListReg.PageIndex = e.NewPageIndex
        BindRegisterData()
        mpeListReg.Show()
    End Sub

    Protected Sub gvListReg_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListReg.SelectedIndexChanged
        If registermstoid.Text <> gvListReg.SelectedDataKey.Item("pomstoid").ToString Then
            btnClearReg_Click(Nothing, Nothing)
        End If
        registermstoid.Text = gvListReg.SelectedDataKey.Item("pomstoid").ToString
        registerno.Text = gvListReg.SelectedDataKey.Item("pono").ToString
        registerflag.Text = gvListReg.SelectedDataKey.Item("poflag").ToString
        registerdate.Text = gvListReg.SelectedDataKey.Item("podate").ToString
        curroid.Text = gvListReg.SelectedDataKey.Item("curroid").ToString
        suppoid.Text = gvListReg.SelectedDataKey.Item("suppoid").ToString
        suppname.Text = gvListReg.SelectedDataKey.Item("suppname").ToString
        potype.Text = gvListReg.SelectedDataKey.Item("pogroup").ToString
        podate.Text = gvListReg.SelectedDataKey.Item("podate").ToString
        cProc.SetModalPopUpExtender(btnHideListReg, pnlListReg, mpeListReg, False)
    End Sub

    Protected Sub lkbCloseListReg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListReg.Click
        cProc.SetModalPopUpExtender(btnHideListReg, pnlListReg, mpeListReg, False)
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        If registermstoid.Text = "" Then
            showMessage("Please Select PO No. first!", 2)
            Exit Sub
        End If
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : Session("TblMat") = Nothing : Session("TblMatView") = Nothing : gvListMat.DataSource = Nothing : gvListMat.DataBind()
        BindMaterialData()
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If UpdateCheckedMat() Then
            Session("TblMatView") = Session("TblMat")
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "podtloid=" & dtTbl.Rows(C1)("podtloid")
                    objView(0)("CheckValue") = "True"
                    dtTbl.Rows(C1)("CheckValue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                dtTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "No material data can't be selected!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "No material data can't be selected!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "podtloid=" & dtTbl.Rows(C1)("podtloid")
                    objView(0)("CheckValue") = "False"
                    objView(0)("mrqty") = dtTbl.Rows(C1)("poqty")
                    objView(0)("mrdtlnote") = ""
                    dtTbl.Rows(C1)("CheckValue") = "False"
                    dtTbl.Rows(C1)("mrqty") = dtTbl.Rows(C1)("poqty")
                    dtTbl.Rows(C1)("mrdtlnote") = ""
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                dtTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "No material data can't be selected!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "No material data can't be selected!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "CheckValue='True'"
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListMat") = "No material data have been selected before!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
        End If
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub lbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListListMat.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "CheckValue='True'"
            Dim iCheck As Integer = dtView.Count
            If iCheck > 0 Then
                dtView.RowFilter = ""
                dtView.RowFilter = "CheckValue='True' AND mrqty>0"
                If dtView.Count <> iCheck Then
                    Session("WarningListMat") = "Qty for every checked material data must be more than 0!"
                    showMessage(Session("WarningListMat"), 2)
                    dtView.RowFilter = ""
                    Exit Sub
                End If
                dtView.RowFilter = ""
                dtView.RowFilter = "CheckValue='True' AND mrqty<1000000"
                If dtView.Count <> iCheck Then
                    Session("WarningListMat") = "Qty for every checked material data must be less than 1,000,000!"
                    showMessage(Session("WarningListMat"), 2)
                    dtView.RowFilter = ""
                    Exit Sub
                End If
                dtView.RowFilter = ""
                dtView.RowFilter = "CheckValue='True' AND mrqty>(poqty+(poqty*(potolerance/100)))"
                If dtView.Count > 0 Then
                    Session("WarningListMat") = "Qty for every checked material data must be less or equal than PO QTY + TOLERANCE!"
                    showMessage(Session("WarningListMat"), 2)
                    dtView.RowFilter = ""
                    Exit Sub
                End If
                'If Not IsQtyRounded(dtView, "mrqty", "matlimitqty") Then
                '    Session("WarningListMat") = "Quantity for every checked material data must be rounded by Round Qty!"
                '    showMessage(Session("WarningListMat"), 2)
                '    dtView.RowFilter = ""
                '    Exit Sub
                'End If
                dtView.RowFilter = "CheckValue='True'"
                If Session("TblDtl") Is Nothing Then
                    CreateTblDetail()
                End If
                Dim objTable As DataTable
                objTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                Dim objRow As DataRow
                Dim counter As Integer = objTable.Rows.Count
                For C1 As Integer = 0 To dtView.Count - 1
                    dv.RowFilter = "podtloid=" & dtView(C1)("podtloid")
                    If dv.Count > 0 Then
                        dv.AllowEdit = True
                        dv(0)("mrqty") = dtView(C1)("mrqty")
                        dv(0)("mrdtlnote") = dtView(C1)("mrdtlnote")
                        dv(0)("mrrawprice") = dtView(C1)("mrvalue")
                        dv(0)("mrvalue") = ToDouble(dtView(C1)("mrvalue"))
                        dv(0)("mrdtlamt") = ToMaskEdit(ToDouble(dtView(C1)("mrvalue")) * dtView(C1)("mrqty"), iRoundDigit)
                    Else
                        counter += 1
                        objRow = objTable.NewRow()
                        objRow("mrdtlseq") = counter
                        objRow("podtloid") = dtView(C1)("podtloid")
                        objRow("matoid") = dtView(C1)("matoid")
                        objRow("matcode") = dtView(C1)("matcode").ToString
                        objRow("matlongdesc") = dtView(C1)("matlongdesc").ToString
                        objRow("matlimitqty") = dtView(C1)("matlimitqty")
                        objRow("mrqty") = dtView(C1)("mrqty")
                        objRow("poqty") = dtView(C1)("poqty")
                        objRow("mrunitoid") = dtView(C1)("pounitoid")
                        objRow("mrunit") = dtView(C1)("pounit").ToString
                        objRow("mrdtlnote") = dtView(C1)("mrdtlnote")
                        objRow("potolerance") = dtView(C1)("potolerance")
                        objRow("mrrawprice") = dtView(C1)("mrvalue")
                        objRow("mrvalue") = dtView(C1)("mrvalue")
                        objRow("mrdtlamt") = ToMaskEdit(ToDouble(dtView(C1)("mrvalue")) * ToDouble(dtView(C1)("mrqty")), iRoundDigit)
                        objRow("itemcat2") = dtView(C1)("itemcat2")

                        objTable.Rows.Add(objRow)
                    End If
                    dv.RowFilter = ""
                Next
                dtView.RowFilter = ""
                objTable.AcceptChanges()
                Session("TblDtl") = objTable
                gvListDtl.DataSource = objTable
                gvListDtl.DataBind()
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                ClearDetail()
            Else
                Session("WarningListMat") = "Please select material to add to list!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        Else
            Session("WarningListMat") = "Please select material to add to list!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub mrqty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles mrqty.TextChanged
        mrqty.Text = ToDouble(mrqty.Text)
        Dim dQty As Double = ToDouble(mrqty.Text) - ToDouble(registerqty.Text)
        calcKonversi()
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            Dim objTable As DataTable = Session("TblDtl")
            Dim objRow As DataRow = objTable.Rows(mrrawdtlseq.Text - 1)
            objRow.BeginEdit()
            objRow("podtloid") = registerdtloid.Text
            objRow("matoid") = matrawoid.Text
            objRow("matcode") = matrawcode.Text
            objRow("matlongdesc") = matrawlongdesc.Text
            objRow("mrqty") = ToDouble(mrqty.Text)
            objRow("mrdtlnote") = mrrawdtlnote.Text
            objRow("potolerance") = ToDouble(potolerance.Text)
            objRow("mrrawprice") = ToDouble(mrrawprice.Text)
            objRow("mrvalue") = ToDouble(mrrawprice.Text)
            objRow("mrdtlamt") = ToMaskEdit(ToDouble(mrrawprice.Text) * ToDouble(mrqty.Text), iRoundDigit)

            objRow.EndEdit()
            objTable.AcceptChanges()
            Session("TblDtl") = objTable
            gvListDtl.DataSource = objTable
            gvListDtl.DataBind()
            ClearDetail()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvListDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), iRoundDigit)
        End If
    End Sub

    Protected Sub gvListDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvListDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        Dim iOid As Integer = objRow(e.RowIndex)("matoid")
        Dim dv As DataView = objTable.DefaultView
        dv.RowFilter = "matoid=" & iOid
        For C1 As Int16 = 0 To dv.Count - 1
            dv.Delete(0)
        Next
        dv.RowFilter = ""
        Session("TblDtl") = dv.ToTable
        gvListDtl.DataSource = Session("TblDtl")
        gvListDtl.DataBind()
        ClearDetail()
    End Sub

    Protected Sub gvListDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListDtl.SelectedIndexChanged
        Try
            mrrawdtlseq.Text = gvListDtl.SelectedDataKey.Item("mrdtlseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "mrdtlseq=" & mrrawdtlseq.Text
                registerdtloid.Text = dv.Item(0).Item("podtloid").ToString
                matrawoid.Text = dv.Item(0).Item("matoid").ToString
                matrawcode.Text = dv.Item(0).Item("matcode").ToString
                matrawlongdesc.Text = dv.Item(0).Item("matlongdesc").ToString
                mrqty.Text = dv.Item(0).Item("mrqty").ToString
                registerqty.Text = dv.Item(0).Item("poqty").ToString
                InitAllDDL()
                mrrawunitoid.SelectedValue = dv.Item(0).Item("mrunitoid").ToString
                mrrawdtlnote.Text = dv.Item(0).Item("mrdtlnote").ToString
                mrrawvalue.Text = dv.Item(0).Item("mrvalue").ToString
                mrrawprice.Text = dv.Item(0).Item("mrrawprice").ToString
                matrawlimitqty.Text = dv.Item(0).Item("matlimitqty").ToString
                potolerance.Text = dv.Item(0).Item("potolerance").ToString
                porealqty.Text = ToDouble(dv.Item(0).Item("porealqty").ToString)
             
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        Finally
            btnSearchMat.Visible = False
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                If CheckDataExists("SELECT COUNT(*) FROM QL_TRNMRMST WHERE mrmstoid=" & mrrawmstoid.Text) Then
                    mrrawmstoid.Text = GenerateID("QL_TRNMRMST", CompnyCode)
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNMRMST", "mrmstoid", mrrawmstoid.Text, "mrmststatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    mrrawmststatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            mrrawdtloid.Text = GenerateID("QL_TRNMRDTL", CompnyCode)
            Dim iConMatOid, iCrdMtrOid, iGLMstOid, iGLDtlOid, iStockAcctgOid, iRecAcctgOid, iStockValOid As Integer
            Dim dTotalAmt, dMRVal As Double
            Dim iConAPOid As Int64
            Dim iSeq As Int16 = 1
            Dim dtLastHdrData As DataTable = Nothing, dtLastDtlData As DataTable = Nothing
            Dim dvLastDtlData As DataView = Nothing
            Dim isRegClosed As Boolean = False
            Dim dImportCost As Double = 0, dSumMR As Double = 0
            Dim cRate As New ClassRate
            periodacctg.Text = GetDateToPeriodAcctg(CDate(mrrawdate.Text))
            Dim sDate As String = Format(CDate(mrrawdate.Text), "MM/dd/yyyy")
            Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
            iConAPOid = GenerateID("QL_CONAP", CompnyCode)
            If mrrawmststatus.Text = "Post" Then
                cRate.SetRateValue(CInt(curroid.Text), podate.Text)
                If cRate.GetRateDailyLastError <> "" Then
                    showMessage(cRate.GetRateDailyLastError, 2)
                    mrrawmststatus.Text = "In Process"
                    Exit Sub
                End If
                If cRate.GetRateMonthlyLastError <> "" Then
                    showMessage(cRate.GetRateMonthlyLastError, 2)
                    mrrawmststatus.Text = "In Process"
                    Exit Sub
                End If
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
                If registerflag.Text.ToUpper = "IMPORT" Then
                    isRegClosed = IsRegisterWillBeClosed()
                End If
                If registerflag.Text.ToUpper = "IMPORT" And isRegClosed = True Then
                    dImportCost = 0
                    GetLastData(dtLastHdrData, dtLastDtlData)
                    dSumMR += ToDouble(dtLastDtlData.Compute("SUM(mrdtlamt)", "").ToString)
                    dvLastDtlData = dtLastDtlData.DefaultView
                End If
                If registerflag.Text.ToUpper = "LOCAL" Or registerflag.Text.ToUpper = "IMPORT" Then
                    iConMatOid = GenerateID("QL_constock", CompnyCode)
                    iCrdMtrOid = GenerateID("QL_crdstock", CompnyCode)
                    iGLMstOid = GenerateID("QL_TRNGLMST", CompnyCode)
                    iGLDtlOid = GenerateID("QL_TRNGLDTL", CompnyCode)
                    iStockValOid = GenerateID("QL_STOCKVALUE", CompnyCode)
                    Dim sVarErr As String = ""
                    If Not IsInterfaceExists("VAR_PURC_RECEIVED", DDLBusUnit.SelectedValue) Then
                        sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_PURC_RECEIVED"
                    End If
                    If sVarErr <> "" Then
                        showMessage(GetInterfaceWarning(sVarErr, "posting"), 2)
                        mrrawmststatus.Text = "In Process"
                        Exit Sub
                    End If
                    iStockAcctgOid = GetAcctgStock(potype.Text)
                    iRecAcctgOid = GetAcctgOID(GetVarInterface("VAR_PURC_RECEIVED", DDLBusUnit.SelectedValue), CompnyCode)
                End If
                GenerateRMRNo()
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                Dim totalMR As Decimal = 0.0 : Dim totalPrice As Decimal = 0.0 : Dim totalPajak As Decimal = 0.0
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    totalMR += ToDouble(objTable.Compute("SUM(mrdtlamt)", "").ToString)
                    totalPrice += ToDouble(objTable.Compute("SUM(mrvalue)", "").ToString)
                End If
                totalPajak = (ToDouble(GetStrData("select top 1 potaxamt from QL_trnpomst regmst inner join QL_trnpodtl regdtl on regmst.pomstoid=regdtl.pomstoid where regmst.pomstoid=" & registermstoid.Text & "")) / 100) * totalMR
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trnmrmst (cmpcode, mrmstoid, periodacctg, mrdate, mrno, suppoid, pomstoid, mrwhoid, mrmstnote, mrmststatus, createuser, createtime, upduser, updtime, curroid, potype, receiver, sjno, sjdate) VALUES ('" & DDLBusUnit.SelectedValue & "', " & mrrawmstoid.Text & ", '" & periodacctg.Text & "', '" & mrrawdate.Text & "', '" & mrrawno.Text & "', " & suppoid.Text & ", " & registermstoid.Text & ", " & mrrawwhoid.SelectedValue & ", '" & Tchar(mrrawmstnote.Text) & "', '" & mrrawmststatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & curroid.Text & ", '" & potype.Text.ToUpper & "', '" & Tchar(receiver.Text) & "', '" & Tchar(sjno.Text) & "', '" & CDate(sjdate.Text) & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & mrrawmstoid.Text & " WHERE tablename='QL_TRNMRMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trnmrmst SET periodacctg='" & periodacctg.Text & "', mrdate='" & mrrawdate.Text & "', mrno='" & mrrawno.Text & "', suppoid=" & suppoid.Text & ", pomstoid=" & registermstoid.Text & ", mrwhoid=" & mrrawwhoid.SelectedValue & ", mrmstnote='" & Tchar(mrrawmstnote.Text) & "', mrmststatus='" & mrrawmststatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, curroid=" & curroid.Text & ", receiver='" & Tchar(receiver.Text) & "', sjno='" & Tchar(sjno.Text) & "', sjdate='" & CDate(sjdate.Text) & "', potype= '" & potype.Text.ToUpper & "' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrmstoid=" & mrrawmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnpodtl SET podtlstatus='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND podtloid IN (SELECT podtloid FROM QL_trnmrdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrmstoid=" & mrrawmstoid.Text & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnpomst SET pomststatus='Approved' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND pomstoid=" & registermstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trnmrdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrmstoid=" & mrrawmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If mrrawmststatus.Text = "Post" Then
                    ' Insert GL MST
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGLMstOid & ", '" & sDate & "', '" & sPeriod & "', 'LPB |No. " & mrrawno.Text & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Insert GL DTL
                    Dim dMRAmtIDR As Double = ToMaskEdit((ToDouble(totalMR) * cRate.GetRateMonthlyIDRValue), iRoundDigit)
                    Dim dMRAmtUSD As Double = ToMaskEdit((ToDouble(totalMR) * cRate.GetRateMonthlyUSDValue), iRoundDigit)

                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGLDtlOid & ", " & iSeq & ", " & iGLMstOid & ", " & iStockAcctgOid & ", 'D', " & totalMR & ", '" & mrrawno.Text & "', 'LPB |No. " & mrrawno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dMRAmtIDR & ", " & dMRAmtUSD & ", 'QL_trnmrmst " & mrrawmstoid.Text & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGLDtlOid += 1
                    iSeq += 1

                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGLDtlOid & ", " & iSeq & ", " & iGLMstOid & ", " & iRecAcctgOid & ", 'C', " & totalMR & ", '" & mrrawno.Text & "', 'LPB |No. " & mrrawno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dMRAmtIDR & ", " & dMRAmtUSD & ", 'QL_trnmrmst " & mrrawmstoid.Text & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGLDtlOid += 1
                    iSeq += 1
                    iGLMstOid += 1
                End If

                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    dSumMR += ToDouble(objTable.Compute("SUM(mrdtlamt)", "").ToString)
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        If dSumMR > 0 Then
                            dMRVal = ToDouble(objTable.Rows(C1)("mrvalue").ToString)
                        End If
                        sSql = "INSERT INTO QL_trnmrdtl (cmpcode, mrdtloid, mrmstoid, mrdtlseq, podtloid, matoid, mrqty, mrbonusqty, mrunitoid, mrdtlnote, upduser, updtime, mrvalue, mrvalueidr, mrvalueusd, mrdtlamt, mrrawprice) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C1 + CInt(mrrawdtloid.Text)) & ", " & mrrawmstoid.Text & ", " & C1 + 1 & ", " & objTable.Rows(C1)("podtloid") & ", " & objTable.Rows(C1)("matoid") & ", " & ToDouble(objTable.Rows(C1)("mrqty").ToString) & ", 0, " & objTable.Rows(C1)("mrunitoid") & ", '" & Tchar(objTable.Rows(C1)("mrdtlnote")) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(C1)("mrvalue").ToString) & ", " & ToDouble(objTable.Rows(C1)("mrvalue").ToString) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(objTable.Rows(C1)("mrvalue").ToString) * cRate.GetRateMonthlyUSDValue & ", " & ToDouble(objTable.Rows(C1).Item("mrdtlamt").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("mrrawprice").ToString) & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        If ToDouble(objTable.Rows(C1)("mrqty").ToString) >= ToDouble(objTable.Rows(C1)("poqty").ToString) Then
                            sSql = "UPDATE QL_trnpodtl SET podtlstatus='COMPLETE' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND podtloid=" & objTable.Rows(C1)("podtloid")
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_trnpomst SET pomststatus='Closed' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND pomstoid=" & registermstoid.Text & " AND (SELECT COUNT(*) FROM QL_trnpodtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND pomstoid=" & registermstoid.Text & " AND podtloid<>" & objTable.Rows(C1)("podtloid") & " AND podtlstatus='')=0"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(mrrawdtloid.Text)) & " WHERE tablename='QL_TRNMRDTL' AND cmpcode='" & CompnyCode & "' "
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    If mrrawmststatus.Text = "Post" Then
                        If registerflag.Text.ToUpper = "LOCAL" Or registerflag.Text.ToUpper = "IMPORT" Then
                            Dim dTotalMR As Double = 0
                            For C1 As Int16 = 0 To objTable.Rows.Count - 1
                                Dim dFactor As Double = 0
                                If dSumMR > 0 Then
                                    dFactor = ToDouble(objTable.Rows(C1)("mrvalue").ToString) / dSumMR
                                End If
                                sSql = "INSERT INTO QL_constock (cmpcode, constockoid, contype, trndate, formaction, formoid, periodacctg, refname, refoid, mtrlocoid, qtyin, qtyout, typemin, reason, note, createuser, createtime, upduser, updtime, valueidr, valueusd, qtyin_unitbesar, qtyout_unitbesar) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iConMatOid & ", 'MR', '" & sDate & "', 'QL_trnmrdtl',  " & mrrawmstoid.Text & ", '" & sPeriod & "', '" & potype.Text.ToUpper & "', " & objTable.Rows(C1)("matoid") & ", " & mrrawwhoid.SelectedValue & ", " & ToDouble(objTable.Rows(C1)("mrqty").ToString) & ", 0, 1, 'Material Received', '" & mrrawno.Text & " # " & suppname.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(C1)("mrvalue").ToString) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(objTable.Rows(C1)("mrvalue").ToString) * cRate.GetRateMonthlyUSDValue & ", " & ToDouble(objTable.Rows(C1)("mrqty_unitbesar").ToString) & ", 0)"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iConMatOid += 1
                                sSql = "UPDATE QL_crdstock SET qtyin=qtyin + " & ToDouble(objTable.Rows(C1)("mrqty").ToString) & ", saldoakhir=saldoakhir + " & ToDouble(objTable.Rows(C1)("mrqty").ToString) & ", qtyin_unitbesar=qtyin_unitbesar + " & ToDouble(objTable.Rows(C1)("mrqty_unitbesar").ToString) & ", saldoakhir_unitbesar=saldoakhir_unitbesar + " & ToDouble(objTable.Rows(C1)("mrqty_unitbesar").ToString) & ", lasttranstype='QL_trnmrdtl', lasttransdate='" & sDate & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND refoid=" & objTable.Rows(C1)("matoid") & " AND mtrlocoid=" & mrrawwhoid.SelectedValue & " AND periodacctg='" & sPeriod & "' "
                                xCmd.CommandText = sSql
                                If xCmd.ExecuteNonQuery() = 0 Then
                                    sSql = "INSERT INTO QL_crdstock (cmpcode, crdstockoid, periodacctg, refoid, refname, mtrlocoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir, lasttranstype, lasttransdate, createuser, createtime, upduser, updtime, closeuser, closingdate, refno, qtyin_unitbesar, qtyout_unitbesar, qtyadjin_unitbesar, qtyadjout_unitbesar, saldoawal_unitbesar, saldoakhir_unitbesar) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iCrdMtrOid & ", '" & sPeriod & "', " & objTable.Rows(C1)("matoid") & ", '" & potype.Text.ToUpper & "', " & mrrawwhoid.SelectedValue & ", " & ToDouble(objTable.Rows(C1)("mrqty").ToString) & ", 0, 0, 0, 0, " & ToDouble(objTable.Rows(C1)("mrqty").ToString) & ", 'QL_trnmrdtl', '" & sDate & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', '1/1/1900', '', " & ToDouble(objTable.Rows(C1)("mrqty_unitbesar").ToString) & ", 0, 0, 0, 0, " & ToDouble(objTable.Rows(C1)("mrqty_unitbesar").ToString) & ")"
                                    xCmd.CommandText = sSql
                                    xCmd.ExecuteNonQuery()
                                    iCrdMtrOid += 1
                                End If
                                dTotalMR += (ToDouble(objTable.Rows(C1)("mrdtlamt").ToString))

                                'Insert STockvalue
                                sSql = GetQueryUpdateStockValue(ToDouble(objTable.Rows(C1)("mrqty").ToString), 0, ToDouble(objTable.Rows(C1)("mrvalue").ToString), 0, "QL_trnmrdtl", sDate, Session("UserID"), DDLBusUnit.SelectedValue, sPeriod, objTable.Rows(C1)("matoid"))
                                xCmd.CommandText = sSql
                                If xCmd.ExecuteNonQuery() = 0 Then
                                    sSql = GetQueryInsertStockValue(ToDouble(objTable.Rows(C1)("mrqty").ToString), 0, ToDouble(objTable.Rows(C1)("mrvalue").ToString), 0, "QL_trnmrdtl", sDate, Session("UserID"), DDLBusUnit.SelectedValue, sPeriod, objTable.Rows(C1)("matoid"), iStockValOid)
                                    xCmd.CommandText = sSql
                                    xCmd.ExecuteNonQuery()
                                    iStockValOid += 1
                                End If

                                Dim price As Double = ToDouble(objTable.Rows(C1)("mrvalue").ToString)
                                Dim dPercentage As Double = GetHppPercentage(objTable.Rows(C1)("itemcat2").ToString)
                                Dim hpp As Double = ToMaskEdit(price * (dPercentage / 100), 2)
                                'update minStock di tabel mstitem
                                sSql = "UPDATE QL_mstitem SET itemMinPrice = " & price + hpp & ", itemMinPrice1 = " & price + hpp & ", itemMinPrice2 = " & price + hpp & ", itemMinPrice3 = " & price + hpp & " WHERE itemoid = '" & objTable.Rows(C1).Item("matoid") & "' and cmpcode = '" & CompnyCode & "' "
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                            Next
                            dTotalAmt = dSumMR '+ dImportCost
                            If mrrawmststatus.Text = "Post" Then
                                sSql = "UPDATE QL_mstoid SET lastoid=" & iConMatOid - 1 & " WHERE tablename='QL_constock' AND cmpcode='" & CompnyCode & "'"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                sSql = "UPDATE QL_mstoid SET lastoid=" & iCrdMtrOid - 1 & " WHERE tablename='QL_crdstock' AND cmpcode='" & CompnyCode & "'"

                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()

                                sSql = "UPDATE QL_mstoid SET lastoid=" & iGLMstOid - 1 & " WHERE tablename='QL_trnglmst' AND cmpcode='" & CompnyCode & "'"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                sSql = "UPDATE QL_mstoid SET lastoid=" & iGLDtlOid - 1 & " WHERE tablename='QL_trngldtl' AND cmpcode='" & CompnyCode & "'"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                sSql = "UPDATE QL_mstoid SET lastoid=" & iStockValOid - 1 & " WHERE tablename='QL_STOCKVALUE' AND cmpcode='" & CompnyCode & "'"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                            End If
                        End If
                    End If
                End If
                objTrans.Commit()
                xCmd.Connection.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    mrrawmststatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.ToString, 1)
                xCmd.Connection.Close()
                mrrawmststatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & mrrawmstoid.Text & ".<BR>"
            End If
            If mrrawmststatus.Text = "Post" Then
                Session("SavedInfo") &= "Data have been posted with MR No. = " & mrrawno.Text & "."
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trnMR.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnMR.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If mrrawmstoid.Text.Trim = "" Then
            showMessage("Please select Material Received data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_trnmrmst", "mrmstoid", mrrawmstoid.Text, "mrmststatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                mrrawmststatus.Text = "In Process"
                Exit Sub
            End If
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trnpodtl SET podtlstatus='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND podtloid IN (SELECT podtloid FROM QL_trnmrdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrmstoid=" & mrrawmstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_trnpomst SET pomststatus='Approved' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND pomstoid=" & registermstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnmrdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrmstoid=" & mrrawmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnmrmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrmstoid=" & mrrawmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnMR.aspx?awal=true")
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPost.Click
        mrrawmststatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnShowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnShowCOA.Click
        DDLRateType.SelectedIndex = -1
        ShowCOAPosting(mrrawno.Text, DDLBusUnit.SelectedValue, gvCOAPosting, DDLRateType.SelectedValue)
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, True)
    End Sub

    Protected Sub lkbCloseCOAPosting_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseCOAPosting.Click
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, False)
    End Sub

    Protected Sub gvCOAPosting_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCOAPosting.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 4)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
        End If
    End Sub

    Protected Sub DDLRateType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLRateType.SelectedIndexChanged
        ShowCOAPosting(mrrawno.Text, DDLBusUnit.SelectedValue, gvCOAPosting, DDLRateType.SelectedValue)
        mpeCOAPosting.Show()
    End Sub

    Protected Sub btnPrintHdr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrintHdr.Click
        UpdateCheckedValue()
        PrintReport()
    End Sub

    Protected Sub gvListSupp_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvListSupp.Sorting
        Dim dt = TryCast(Session("mstSupp"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvListSupp.DataSource = Session("mstSupp")
            gvListSupp.DataBind()
        End If
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
    End Sub

    Protected Sub gvListreg_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvListReg.Sorting
        Dim dt = TryCast(Session("Register"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvListReg.DataSource = Session("Register")
            gvListReg.DataBind()
        End If
        mpeListReg.Show()
    End Sub

    Protected Sub gvListMat_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvListMat.Sorting
        UpdateCheckedMat()
        Dim dt = TryCast(Session("TblMat"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvListMat.DataSource = Session("TblMat")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub mrrawunitoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles mrrawunitoid.SelectedIndexChanged
        calcKonversi()
    End Sub
#End Region

End Class