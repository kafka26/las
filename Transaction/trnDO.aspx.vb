Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_DO
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Dim pkSize = New System.Drawing.Printing.PaperSize
    Dim PrintDoc1 As New System.Drawing.Printing.PrintDocument
    Public folderReport As String = "~/Report/"
#End Region

    Structure VarUsageValue
        Public Val_IDR As Double
        Public Val_USD As Double
    End Structure

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "sodtloid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("doqty") = ToDouble(GetTextBoxValue(row.Cells(10).Controls))
                                    dtView(0)("note") = GetTextBoxValue(row.Cells(12).Controls)
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "sodtloid=" & cbOid
                                dtView2.RowFilter = "sodtloid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("doqty") = ToDouble(GetTextBoxValue(row.Cells(10).Controls))
                                    dtView(0)("note") = GetTextBoxValue(row.Cells(12).Controls)
                                    If dtView2.Count > 0 Then
                                        dtView2(0)("doqty") = ToDouble(GetTextBoxValue(row.Cells(10).Controls))
                                        dtView2(0)("note") = GetTextBoxValue(row.Cells(12).Controls)
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        'If sorawmstoid.Text = "" Then
        '    sError &= "- Please select SO NO. field!<BR>"
        'End If
        'If dowhoid.SelectedValue = "" Then
        '    sError &= "- Please select WAREHOUSE field!<BR>"
        'End If
        If sorawdtloid.Text = "" Then
            sError &= "- Please select MATERIAL field!<BR>"
        End If
        If dorawqty.Text = "" Then
            sError &= "- Please fill QTY field!<BR>"
        Else
            If ToDouble(dorawqty.Text) <= 0 Then
                sError &= "- QTY must be more than 0!<BR>"
            Else
                Dim sErrReply As String = ""
                If Not isLengthAccepted("", "", ToDouble(dorawqty.Text), sErrReply) Then
                    sError &= "- QTY must be less than MAX QTY (" & sErrReply & ") allowed stored in database!<BR>"
                Else
                    'If ToDouble(dorawqty.Text) > ToDouble(sorawqty.Text) Then
                    '    sError &= "- QTY must be less than SO QTY!<BR>"
                    'Else
                    '    If Not IsQtyRounded(ToDouble(dorawqty.Text), ToDouble(matrawlimitqty.Text)) Then
                    '        sError &= "- QTY must be rounded by ROUNDING QTY!<BR>"
                    '    End If
                    'End If
                    If dorawunitoid.SelectedValue <> dounit2oid.Text Then
                        If ToDouble(dorawqty.Text) * ToDouble(matrawconvertunit.Text) > ToDouble(stockqty.Text) Then
                            sError &= "- QTY must be less than Stock QTY!<BR>"
                        End If
                    Else
                        If ToDouble(dorawqty.Text) > ToDouble(stockqty.Text) Then
                            sError &= "- QTY must be less than Stock QTY!<BR>"
                        End If
                    End If
                End If
            End If
        End If
       
        If dorawprice.Text = "" Then
            sError &= "- Please fill PRICE field!<BR>"
        Else
            If ToDouble(dorawprice.Text) <= 0 Then
                sError &= "- PRICE must be more than 0!<BR>"
            End If
        End If
        If dorawunitoid.SelectedValue = "" Then
            sError &= "- Please select UNIT field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        Dim sLastSOStatus As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If dorawdate.Text = "" Then
            sError &= "- Please fill DO DATE field!<BR>"
            sErr = "Empty"
        Else
            If Not IsValidDate(dorawdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- DO DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If custoid.Text = "" Then
            sError &= "- Please select CUSTOMER field!<BR>"
        End If
        If expedisioid.SelectedValue = "" Then
            sError &= "- Please select EXPEDISI field!<BR>"
        End If
        Dim sErr2 As String = ""
        If dorawreqdate.Text <> "" Then
            If Not IsValidDate(dorawreqdate.Text, "MM/dd/yyyy", sErr2) Then
                sError &= "- DATE is invalid. " & sErr2 & "<BR>"
            End If
        End If

        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill Detail Data!<BR>"
        Else
            Dim dtTbl As DataTable = Session("TblDtl")
            For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                'cek stock ada atau tidak
                If Not IsStockAvailable(CompnyCode, GetDateToPeriodAcctg(GetServerTime()), dtTbl.Rows(C1)("itemoid"), dowhoid.SelectedValue, ToDouble(dtTbl.Compute("SUM(doqty)", "itemoid='" & dtTbl.Rows(C1)("itemoid") & "'"))) Then
                    sError &= "- Material Code " & dtTbl.Rows(C1)("itemcode") & " Must be less or equal than Stock Qty!<BR>"
                End If
            Next

            'cek qty DO dan qty SO
            For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                If i_u.Text = "New Data" Then
                    sSql = "SELECT (sod.sodtlqty - (ISNULL((SELECT SUM(d.doqty) FROM ql_trndodtl d INNER JOIN ql_trndomst m ON m.domstoid=d.domstoid WHERE d.cmpcode=sod.cmpcode AND m.domststatus NOT IN('Rejected','Revised') AND d.sodtloid=sod.sodtloid),0.0) - ISNULL((SELECT SUM(rd.sretqty) FROM QL_trnsalesreturdtl rd INNER JOIN QL_trnjualdtl jd ON jd.trnjualdtloid=rd.trnjualdtloid INNER JOIN QL_trndodtl dd ON dd.dodtloid=jd.dodtloid WHERE dd.cmpcode=sod.cmpcode AND dd.sodtloid=sod.sodtloid),0.0))) AS doqty FROM QL_trnsodtl sod WHERE sod.cmpcode='" & DDLBusUnit.SelectedValue & "' AND sod.sodtloid=" & dtTbl.Rows(C1)("sodtloid").ToString
                    Dim dQty As Double = ToDouble(GetStrData(sSql).ToString)
                    If ToDouble(dtTbl.Rows(C1)("doqty").ToString) > dQty Then
                        sError &= "- Please check that every DO Qty must be less than SO Qty!<BR>"
                        Exit For
                    End If
                Else
                    sSql = "SELECT sod.sodtlqty AS doqty FROM QL_trnsodtl sod WHERE sod.cmpcode='" & DDLBusUnit.SelectedValue & "' AND sod.sodtloid=" & dtTbl.Rows(C1)("sodtloid").ToString
                    Dim dQty As Double = ToDouble(GetStrData(sSql).ToString)
                    If ToDouble(dtTbl.Rows(C1)("doqty").ToString) > dQty Then
                        sError &= "- Please check that every DO Qty must be less than SO Qty!<BR>"
                        Exit For
                    End If
                End If
            Next
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            dorawmststatus.Text = "In Process"
            Return False
        End If
            Return True
    End Function

    Private Function GetStockValue(ByVal sOid As String, Optional ByVal sType As String = "IDR") As Double
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        Try
            sSql = "SELECT SUM(ISNULL(stockqty, 0) * ISNULL(stockvalue" & sType.ToLower & ", 0)) / SUM(ISNULL(stockqty, 0)) FROM QL_stockvalue WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND periodacctg IN ('" & sPeriod & "', '" & GetLastPeriod(sPeriod) & "') AND refoid=" & sOid & " AND closeflag=''"
            GetStockValue = ToDouble(GetStrData(sSql))
        Catch ex As Exception
            GetStockValue = 0
        End Try
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trndomst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND domststatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trndo.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lbInProcess.Visible = True
            lbInProcess.Text = "You have " & GetStrData(sSql) & " In Process DO Raw Material data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitAllDDL()
        'Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)
        'Fill DDL Currency
        sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' ORDER BY currcode"
        FillDDL(curroid, sSql)
        'Fill DDL Delivery Type
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='EXPEDISI' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDL(expedisioid, sSql)
        'fill DDL Warehouse
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='WAREHOUSE' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDLWithAdditionalText(dowhoid, sSql, "-- Pilih Gudang --", "0")
        'fill DDL so type
        sSql = "SELECT gencode, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='GROUPITEM' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDL(sotype, sSql)
    End Sub

    Private Sub InitDDLUnit()
        'Fill DDL Unit
        sSql = "select genoid,gendesc from QL_mstgen where gengroup='UNIT'"
        FillDDL(dorawunitoid, sSql)
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT domstoid, dono, CONVERT(VARCHAR(10), dodate, 101) AS dodate, custname, domststatus, domstnote, divname, 'False' AS checkvalue,(case upper(dom.domststatus) when 'Closed' then '' when 'Rejected' then dom.rejectreason when 'Revised' then dom.revisereason else '' end) reason, som.sono FROM QL_trndomst dom INNER JOIN QL_trnsomst som ON som.somstoid=dom.somstoid INNER JOIN QL_mstcust c ON c.custoid=dom.custoid INNER JOIN QL_mstdivision div ON div.cmpcode=dom.cmpcode"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " WHERE dom.cmpcode='" & Session("CompnyCode") & "'"
        Else
            sSql &= " WHERE dom.cmpcode LIKE '%'"
        End If
        sSql &= sSqlPlus & " ORDER BY CONVERT(DATETIME, dom.dodate) DESC, dom.domstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trndomst")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "domstoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub BindCustomer()
        sSql = "SELECT DISTINCT c.custoid, custcode, custname, som.curroid, currcode FROM QL_mstcust c INNER JOIN QL_trnsomst som ON som.custoid=c.custoid INNER JOIN QL_mstcurr cu ON cu.curroid=som.curroid WHERE som.cmpcode='" & DDLBusUnit.SelectedValue & "' AND " & FilterDDLListCust.SelectedValue & " LIKE '%" & Tchar(FilterTextListCust.Text) & "%' AND somststatus='Approved' ORDER BY custname"
        FillGV(gvListCust, sSql, "QL_mstcust")
    End Sub

    Private Sub BindSOData()
        sSql = "SELECT somstoid, sono, CONVERT(VARCHAR(10), sodate, 101) AS sodate, somstnote, somstres1, soetd, som.warehouse_id FROM QL_trnsomst som WHERE som.cmpcode='" & DDLBusUnit.SelectedValue & "' AND somststatus='Approved' AND custoid=" & custoid.Text & " AND " & FilterDDLListSO.SelectedValue & " LIKE '%" & Tchar(FilterTextListSO.Text) & "%' AND curroid=" & curroid.SelectedValue & " ORDER BY somstoid"
        FillGV(gvListSO, sSql, "QL_trnsomst")
    End Sub

    Private Sub BindMaterial()
        Dim sAdd As String = ""
        If Session("oid") IsNot Nothing And Session("oid") <> "" Then
            sAdd = " AND d.domstoid<>" & Session("oid")
        End If
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sSql = "SELECT 'False' AS CheckValue, sodtloid, i.itemoid AS itemoid, i.itemCode AS itemcode,i.itemLongDescription AS itemdesc, sod.sodtlunitoid AS unitoid, g1.gendesc AS unitdesc, (select dbo.getstockqty(i.itemoid, " & dowhoid.SelectedValue & ")) AS stockqty, 0.0 AS stockqtybesar, sod.sodtlqty AS soqty2, (sod.sodtlqty - (ISNULL((SELECT SUM(d.doqty) FROM ql_trndodtl d INNER JOIN ql_trndomst m ON m.domstoid=d.domstoid WHERE d.cmpcode=sod.cmpcode AND m.domststatus NOT IN ('Rejected','Revised') AND d.sodtloid=sod.sodtloid " & sAdd & "),0.0) - ISNULL((SELECT SUM(rd.sretqty) FROM QL_trnsalesreturdtl rd INNER JOIN QL_trnjualdtl jd ON jd.trnjualdtloid=rd.trnjualdtloid INNER JOIN QL_trndodtl dd ON dd.dodtloid=jd.dodtloid WHERE dd.cmpcode=sod.cmpcode AND dd.sodtloid=sod.sodtloid),0.0))) AS soqty, '' AS note, 0.00 doqty, sod.sodtlprice AS price, sod.old_code itemoldcode, g2.gendesc AS unit1, g3.gendesc AS unit3, sod.soprice_kons, sod.old_code FROM QL_trnsodtl sod INNER JOIN QL_mstitem i ON i.itemoid=sod.itemoid INNER JOIN QL_mstgen g1 ON g1.genoid=sod.sodtlunitoid INNER JOIN QL_mstgen g2 ON g2.genoid=i.itemunit1 INNER JOIN QL_mstgen g3 ON g3.genoid=i.itemunit3 WHERE sod.cmpcode='" & CompnyCode & "' AND sod.somstoid=" & sorawmstoid.Text & " AND sod.sodtlstatus<>'Complete' ORDER BY sodtlseq"
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnsodtl")
        For C1 As Integer = 0 To dtTbl.Rows.Count - 1
            dtTbl.Rows(C1)("doqty") = dtTbl.Rows(C1)("soqty").ToString
        Next
        dtTbl.AcceptChanges()
        If dtTbl.Rows.Count > 0 Then
            Session("TblMat") = dtTbl
            Session("TblMatView") = dtTbl
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            showMessage("Material data can't be found!", 2)
        End If
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("QL_trndodtl")
        dtlTable.Columns.Add("dodtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("sodtloid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("oldcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("stockqty", Type.GetType("System.String"))
        dtlTable.Columns.Add("soqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("doqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("dounitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("dounit", Type.GetType("System.String"))
        dtlTable.Columns.Add("dodtlnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("doprice", Type.GetType("System.Double"))
        dtlTable.Columns.Add("soprice_kons", Type.GetType("System.Double"))
        dtlTable.Columns.Add("dodtlamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("qtyunitkecil", Type.GetType("System.Double"))
        dtlTable.Columns.Add("qtyunitbesar", Type.GetType("System.Double"))
        dtlTable.Columns.Add("valueidr", Type.GetType("System.Double"))
        dtlTable.Columns.Add("valueusd", Type.GetType("System.Double"))
        dtlTable.Columns.Add("old_code", Type.GetType("System.String"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub ClearDetail()
        dorawdtlseq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                dorawdtlseq.Text = objTable.Rows.Count + 1
                EnableHeader(False)
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        i_u2.Text = "New Detail"
        sorawdtloid.Text = ""
        matrawoid.Text = ""
        matrawcode.Text = ""
        oldcode.Text = ""
        matrawlongdesc.Text = ""
        stock.Text = ""
        sorawqty.Text = ""
        dorawqty.Text = ""
        dorawunitoid.SelectedIndex = -1
        dorawprice.Text = ""
        dorawdtlamt.Text = ""
        dorawdtlnote.Text = ""
        gvDtl.SelectedIndex = -1
        btnSearchSO.Visible = True : btnClearSO.Visible = True
        btnSearchMat.Visible = True
        delivQty.Text = ""
        stockqty.Text = ""
    End Sub

    Private Sub SumTotalAmount()
        Dim dVal As Double = 0
        If Session("TblDtl") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            For C1 As Integer = 0 To dt.Rows.Count - 1
                dVal += ToDouble(dt.Rows(C1)("dodtlamt").ToString)
            Next
        End If
        dorawtotalamt.Text = ToMaskEdit(Math.Ceiling(dVal), 2)
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal : DDLBusUnit.CssClass = sCss
        btnSearchCust.Visible = bVal : btnClearCust.Visible = bVal
    End Sub

    Private Sub GenerateDONo()
        Dim sNo As String = "DORAW-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(dorawno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trndorawmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dorawno LIKE '" & sNo & "%'"
        dorawno.Text = GenNumberString(sNo, "", cKon.ambilscalar(sSql), DefaultFormatCounter)
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT dom.cmpcode, dom.periodacctg, domstoid, dono, dodate, dom.custoid, custname, domststatus, domstnote, dom.createuser, dom.createtime, dom.upduser, dom.updtime, dom.curroid, doreqdate, domstres1, sono, expedisioid, nopolisi, som.sodate, dom.dowhoid, som.somstoid, som.warehouse_id FROM QL_trndomst dom INNER JOIN QL_mstcust c ON c.custoid=dom.custoid INNER JOIN QL_trnsomst som ON som.somstoid=dom.somstoid WHERE domstoid=" & sOid
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = xreader("cmpcode").ToString
                periodacctg.Text = xreader("periodacctg").ToString
                dorawmstoid.Text = xreader("domstoid").ToString
                dorawno.Text = xreader("dono").ToString
                sorawno.Text = xreader("sono").ToString
                sorawmstoid.Text = xreader("somstoid").ToString
                dorawdate.Text = Format(xreader("dodate"), "MM/dd/yyyy")
                custoid.Text = xreader("custoid").ToString
                custname.Text = xreader("custname").ToString
                sotype.SelectedValue = xreader("domstres1").ToString
                sodate.Text = Format(xreader("sodate"), "MM/dd/yyyy")
                nopolisi.Text = xreader("nopolisi").ToString
                expedisioid.SelectedValue = xreader("expedisioid").ToString
                dorawmststatus.Text = xreader("domststatus").ToString
                dorawmstnote.Text = xreader("domstnote").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
                curroid.SelectedValue = xreader("curroid").ToString
                dowhoid.SelectedValue = xreader("dowhoid").ToString
                dorawreqdate.Text = Format(xreader("doreqdate"), "MM/dd/yyyy")
                If dorawreqdate.Text = "01/01/1900" Then
                    dorawreqdate.Text = ""
                End If
                If CInt(xreader("warehouse_id").ToString) > 0 Then
                    dowhoid.Enabled = False : dowhoid.CssClass = "inpTextDisabled"
                End If
                'dorawcustpono.Text = xreader("dorawcustpono").ToString
                'dorawdest.Text = xreader("dorawdest").ToString
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False
        End Try
        If dorawmststatus.Text <> "In Process" And dorawmststatus.Text <> "Revised" Then
            lblTrnNo.Text = "DO No."
            dorawmstoid.Visible = False
            dorawno.Visible = True
            btnSave.Visible = False
            btnDelete.Visible = False
            btnPost.Visible = False
            btnAddToList.Visible = False
            gvDtl.Columns(0).Visible = False
            gvDtl.Columns(gvDtl.Columns.Count - 1).Visible = False
            btnSearchCust.Visible = False
            btnClearCust.Visible = False
        End If
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sSql = "SELECT dodtlseq, dod.somstoid, sono, dod.sodtloid, dod.matoid AS itemoid, itemCode, dod.old_code AS oldcode, itemLongDescription AS itemdesc, (sod.sodtlqty - ISNULL((SELECT SUM(dodxx.doqty) FROM QL_trndodtl dodxx INNER JOIN QL_trndomst domxx ON domxx.cmpcode=dodxx.cmpcode AND domxx.domstoid=dodxx.domstoid WHERE dodxx.cmpcode=sod.cmpcode AND domxx.domststatus<>'Rejected' AND dodxx.sodtloid=sod.sodtloid AND dodxx.domstoid<>" & dorawmstoid.Text & "), 0.0) - ISNULL((SELECT SUM(rd.sretqty) FROM QL_trnsalesreturdtl rd INNER JOIN QL_trnjualdtl jd ON jd.trnjualdtloid=rd.trnjualdtloid INNER JOIN QL_trndodtl dd ON dd.dodtloid=jd.dodtloid WHERE dd.cmpcode=sod.cmpcode AND dd.sodtloid=sod.sodtloid),0.0)) AS soqty, dod.doqty, (select dbo.getstockqty(m.itemoid, " & dowhoid.SelectedValue & ")) AS stockqty, dod.dounitoid, g2.gendesc AS dounit, dod.dodtlnote, dod.doprice, dod.dodtlamt, doqty_unitkecil AS qtyunitkecil, doqty_unitbesar AS qtyunitbesar, valueidr, valueusd, sod.soprice_kons, dod.old_code FROM QL_trndodtl dod INNER JOIN QL_trnsomst som ON som.cmpcode=dod.cmpcode AND som.somstoid=dod.somstoid INNER JOIN QL_trnsodtl sod ON sod.cmpcode=dod.cmpcode AND sod.sodtloid=dod.sodtloid INNER JOIN QL_mstitem m ON m.itemoid=dod.matoid INNER JOIN QL_mstgen g2 ON g2.genoid=dounitoid WHERE domstoid=" & sOid & "  ORDER BY dodtlseq"
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trndodtl")
        Session("TblDtl") = dtTbl
        gvDtl.DataSource = dtTbl
        gvDtl.DataBind()
        ClearDetail()
        SumTotalAmount()
    End Sub

    Private Sub PrintReport()
        Try
            Dim sOid As String = ""
            report.Load(Server.MapPath(folderReport & "rptDO_Trn.rpt"))
            Dim sRptName As String = "DO_PrintOut"
            Dim sSql As String = " WHERE dom.cmpcode='" & Session("CompnyCode") & "'"
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("domstoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If

            'If Not cbShowPrice.Checked Then       
            'Else
            'report.Load(Server.MapPath(folderReport & "rptDO_TrnPrice.rpt"))
            'End If
            'Dim Slct As String = ""
            'Dim Join As String = ""
            'If cbShowPrice.Checked Then
            '    Slct = ", currcode AS [Curr. Code], currsymbol AS [Curr. Symbol], soprice AS [SO Price], dod.doprice AS [DO Price], dod.dodtlamt AS [DO Amount] "
            '    Join = "INNER JOIN QL_trnsodtl sod ON sod.cmpcode=dod.cmpcode AND sod.sodtloid=dod.sodtloid INNER JOIN QL_mstcurr cu ON cu.curroid=dom.curroid "
            'End If
            'sSql = "SELECT dom.cmpcode, [BU Name], [BU Address], [BU City], [BU Province], [BU Country], [BU Telp. 1], [BU Telp. 2], [BU Fax 1], [BU Fax 2], [BU Email], [BU Post Code], dom.domstoid AS [MstOid], CONVERT(VARCHAR(20), dom.domstoid) AS [Draft No.], dom.dono AS [DO No.], dom.dodate AS [DO Date], dom.domstnote AS [Header Note], dom.domststatus AS [Status], dom.custoid, [cust Name], [cust Code], [cust Address], [cust City], [cust Province], [cust Email], [cust Phone1], [cust Phone2], [cust Phone3], [cust Fax1], [cust Fax2], [cust Country], g1.gendesc AS [Delivery Type], doreqdate AS [Req. Date],  dom.createuser AS [Created By], dom.createtime AS [Created Time], dom.upduser AS [Posted By], dom.updtime AS [Posted Time], dod.dodtloid AS [DtlOid], dod.dodtlseq AS [No.], som.sono AS [SO No.], soetd AS [SO ETD], m.matoid AS [MatOid], m.itemCode AS [Code], m.itemLongDescription AS [Material], dod.dodtlqty AS [Qty], g2.gendesc AS [Unit], dod.dodtlnote AS [Note], m.Unit2Unit1Conversion [Konversi], doqty_unitkecil [Qty Kecil] " & Slct & " FROM QL_trndomst dom INNER JOIN QL_trndodtl dod ON dod.cmpcode=dom.cmpcode AND dod.domstoid=dom.domstoid INNER JOIN QL_trnsomst som ON som.cmpcode=dod.cmpcode AND som.somstoid=dod.sorawmstoid INNER JOIN QL_mstitem m ON m.itemoid=dod.matoid INNER JOIN QL_mstgen g2 ON g2.genoid=dod.dorawunitoid " & Join & " "
            ''Tbl Division
            'sSql &= " INNER JOIN (SELECT di.cmpcode, divname AS [BU Name], divaddress AS [BU Address], g1.gendesc AS [BU City], g2.gendesc AS [BU Province], g3.gendesc AS [BU Country], ISNULL(divphone, '') AS [BU Telp. 1], ISNULL(divphone2, '') AS [BU Telp. 2], ISNULL(divfax1, '') AS [BU Fax 1], ISNULL(divfax2, '') AS [BU Fax 2], ISNULL(divemail, '') AS [BU Email], ISNULL(divpostcode, '') AS [BU Post Code] FROM QL_mstdivision di INNER JOIN QL_mstgen g1 ON g1.genoid=divcityoid INNER JOIN QL_mstgen g2 ON g2.cmpcode=g1.cmpcode AND g2.genoid=CONVERT(INT, g1.genother1) INNER JOIN QL_mstgen g3 ON g3.cmpcode=g1.cmpcode AND g3.genoid=CONVERT(INT, g1.genother2)) TblDiv ON TblDiv.cmpcode=dom.cmpcode"
            ''Tbl Customer
            'sSql &= " INNER JOIN (SELECT c.custoid, c.custname AS [cust Name], c.custcode AS [cust Code], c.custaddr AS [cust Address], g1.gendesc AS [cust City], g2.gendesc AS [cust Province], c.custemail AS [cust Email], c.custphone1 AS [cust Phone1], c.custphone2 AS [cust Phone2], c.custphone3 AS [cust Phone3], c.custfax1 AS [cust Fax1], c.custfax2 AS [cust Fax2], g3.gendesc AS [cust Country] FROM QL_mstcust c INNER JOIN QL_mstgen g1 ON g1.cmpcode=c.cmpcode AND g1.gengroup='CITY' AND g1.genoid=c.custcityOid INNER JOIN QL_mstgen g2 ON g2.cmpcode=g1.cmpcode AND g2.genoid=CONVERT(INT, g1.genother1) INNER JOIN QL_mstgen g3 ON g3.cmpcode=g1.cmpcode AND g3.genoid=CONVERT(INT, g1.genother2)) tblCust ON tblCust.custoid=dom.custoid "


            'If Session("CompnyCode") <> CompnyCode Then
            '    sSql &= " WHERE dom.cmpcode='" & Session("CompnyCode") & "'"
            'Else
            '    sSql &= " WHERE dom.cmpcode='" & Session("CompnyCode") & "'"
            'End If
            If sOid = "" Then
                If Tchar(FilterText.Text) <> "" Then
                    sSql &= " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                End If
                If cbPeriod.Checked Then
                    If IsValidPeriod() Then
                        sSql &= " AND dodate>='" & FilterPeriod1.Text & " 00:00:00' AND dodate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sSql &= " AND domststatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Transaction\trndo.aspx", Session("SpecialAccess")) = False Then
                    sSql &= " AND dom.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sSql &= " AND dom.domstoid IN (" & sOid & ")"
            End If
            sSql &= " ORDER BY dom.domstoid, dod.dodtlseq"
            'Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trndomst")
            'Dim dvTbl As DataView = dtTbl.DefaultView
            'report.SetDataSource(dvTbl.ToTable)
            'report.SetParameterValue("Header", "DO PRINT OUT")
            'report.SetParameterValue("PrintUserID", Session("UserID"))
            'report.SetParameterValue("PrintUserName", GetStrData("SELECT profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            report.SetParameterValue("sWhere", sSql)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            cProc.SetDBLogonForReport(report)

            'PrintDoc1.PrinterSettings.PrinterName = "Microsoft XPS Document Writer"
            'Dim k As Integer
            'For k = 0 To PrintDoc1.PrinterSettings.PaperSizes.Count - 1
            '    If PrintDoc1.PrinterSettings.PaperSizes.Item(k).PaperName = "SJ" Then
            '        pkSize = PrintDoc1.PrinterSettings.PaperSizes.Item(k)
            '    End If
            'Next
            'report.PrintOptions.PrinterName = "Microsoft XPS Document Writer"
            'report.PrintOptions.PaperSize = CType(pkSize.RawKind, CrystalDecisions.Shared.PaperSize)
            ''report.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Portrait
            'report.PrintOptions.PaperSource = CrystalDecisions.Shared.PaperSource.Manual

            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, sRptName)
            report.Close()
            report.Dispose()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            report.Close()
            report.Dispose()
        End Try
        Response.Redirect("~\Transaction\trndo.aspx?awal=true")
    End Sub

    Private Sub SetUsageValue(ByRef objVal As VarUsageValue)
        objVal.Val_IDR = 0
        objVal.Val_USD = 0
        Dim dt As DataTable = Session("TblDtl")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            Dim dValIDR As Double = GetStockValue(dt.Rows(C1)("itemoid").ToString)
            Dim dValUSD As Double = GetStockValue(dt.Rows(C1)("itemoid").ToString, "USD")
            dt.Rows(C1)("valueidr") = dValIDR
            dt.Rows(C1)("valueusd") = dValUSD
            objVal.Val_IDR += dValIDR
            objVal.Val_USD += dValUSD
        Next
        Session("TblDtl") = dt
    End Sub

#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trndo.aspx")
        End If
        If checkPagePermission("~\Transaction\trndo.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Delivery Order"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPost.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to SEND Approval this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            CheckStatus()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitAllDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                dorawmstoid.Text = GenerateID("QL_TRNDOMST", CompnyCode)
                dorawmststatus.Text = "In Process"
                dorawdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                dorawreqdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
                dowhoid.SelectedValue = 147
            End If
            'Check Rate Pajak & Standart
            Dim cRate As New ClassRate()
            cRate.SetRateValue(1, Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, 2) : Exit Sub
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvDtl.DataSource = dt
            gvDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trndo.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lbInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriod.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, dom.updtime, GETDATE()) > " & nDays & " AND dorawmststatus='In Process'"
        If checkPagePermission("~\Transaction\trndo.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND dom.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriod.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND dodate>='" & FilterPeriod1.Text & " 00:00:00' AND dodate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND domststatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Transaction\trndo.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND dom.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriod.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Transaction\trndo.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND dom.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        btnClearCust_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchCust.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        FilterDDLListCust.SelectedIndex = -1 : FilterTextListCust.Text = "" : gvListCust.SelectedIndex = -1
        BindCustomer()
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
    End Sub

    Protected Sub btnClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearCust.Click
        custoid.Text = ""
        custname.Text = ""
        curroid.SelectedIndex = -1
        btnClearSO_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnFindListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListCust.Click
        BindCustomer()
        mpeListCust.Show()
    End Sub

    Protected Sub btnAllListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListCust.Click
        FilterDDLListCust.SelectedIndex = -1 : FilterTextListCust.Text = "" : gvListCust.SelectedIndex = -1
        BindCustomer()
        mpeListCust.Show()
    End Sub

    Protected Sub gvListCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListCust.PageIndexChanging
        gvListCust.PageIndex = e.NewPageIndex
        BindCustomer()
        mpeListCust.Show()
    End Sub

    Protected Sub gvListCust_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListCust.SelectedIndexChanged
        If custoid.Text <> gvListCust.SelectedDataKey.Item("custoid").ToString Then
            btnClearCust_Click(Nothing, Nothing)
        End If
        custoid.Text = gvListCust.SelectedDataKey.Item("custoid").ToString
        custname.Text = gvListCust.SelectedDataKey.Item("custname").ToString
        curroid.SelectedValue = gvListCust.SelectedDataKey.Item("curroid").ToString
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub

    Protected Sub lbCloseListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListCust.Click
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub

    Protected Sub btnSearchSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSO.Click
        If custoid.Text = "" Then
            showMessage("Please select Customer first!", 2)
            Exit Sub
        End If
        FilterDDLListSO.SelectedIndex = -1 : FilterTextListSO.Text = "" : gvListSO.SelectedIndex = -1
        BindSOData()
        cProc.SetModalPopUpExtender(btnHideListSO, pnlListSO, mpeListSO, True)
    End Sub

    Protected Sub btnClearSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSO.Click
        sorawmstoid.Text = ""
        sorawno.Text = ""
        dorawreqdate.Text = ""
        sodate.Text = ""
    End Sub

    Protected Sub btnFindListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSO.Click
        BindSOData()
        mpeListSO.Show()
    End Sub

    Protected Sub btnAllListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListSO.Click
        FilterDDLListSO.SelectedIndex = -1 : FilterTextListSO.Text = "" : gvListSO.SelectedIndex = -1
        BindSOData()
        mpeListSO.Show()
    End Sub

    Protected Sub gvListSO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSO.PageIndexChanging
        gvListSO.PageIndex = e.NewPageIndex
        BindSOData()
        mpeListSO.Show()
    End Sub

    Protected Sub gvListSO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListSO.SelectedIndexChanged
        If sorawmstoid.Text <> gvListSO.SelectedDataKey.Item("somstoid").ToString Then
            btnClearSO_Click(Nothing, Nothing)
        End If
        sorawmstoid.Text = gvListSO.SelectedDataKey.Item("somstoid").ToString
        sorawno.Text = gvListSO.SelectedDataKey.Item("sono").ToString
        sodate.Text = gvListSO.SelectedDataKey.Item("sodate").ToString
        sotype.SelectedValue = gvListSO.SelectedDataKey.Item("somstres1").ToString
        dorawreqdate.Text = Format(gvListSO.SelectedDataKey.Item("soetd"), "MM/dd/yyyy")
        dowhoid.SelectedValue = gvListSO.SelectedDataKey.Item("warehouse_id").ToString
        cProc.SetModalPopUpExtender(btnHideListSO, pnlListSO, mpeListSO, False)

        dowhoid.Enabled = True : dowhoid.CssClass = "inpText"
        If CInt(gvListSO.SelectedDataKey.Item("warehouse_id").ToString) > 0 Then
            dowhoid.Enabled = False : dowhoid.CssClass = "inpTextDisabled"
        End If
        'btnSearchMat_Click(Nothing, Nothing)
    End Sub

    Protected Sub lbCloseListSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListSO.Click
        cProc.SetModalPopUpExtender(btnHideListSO, pnlListSO, mpeListSO, False)
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        If sorawmstoid.Text = "" Then
            showMessage("Please select SO No. first!", 2)
            Exit Sub
        End If
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : Session("TblMat") = Nothing : Session("TblMatView") = Nothing : gvListMat.DataSource = Nothing : gvListMat.DataBind()
        BindMaterial()
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 2)
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 2)
            'Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(10).Controls
            'For Each myControl As System.Web.UI.Control In cc
            '    If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
            '        If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
            '            CType(myControl, System.Web.UI.WebControls.TextBox).Text = 0
            '        Else
            '            CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 2)
            '        End If
            '    End If
            'Next
        End If
    End Sub

    Protected Sub cbHdrMat_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For C1 As Integer = 0 To gvListMat.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        mpeListMat.Show()
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim sFilter As String = ""
            Dim sSplit() As String = FilterTextListMat.Text.Split(" ")
            For C1 As Integer = 0 To sSplit.Length - 1
                sFilter &= FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(sSplit(C1)) & "%'"
                If C1 < sSplit.Length - 1 Then
                    sFilter &= " AND "
                End If
            Next
            dtView.RowFilter = sFilter
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If Session("TblMat") Is Nothing Then
            BindMaterial()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Session("TblMatView") = Session("TblMat")
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub lbAddListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddListMat.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "CheckValue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    dtView.RowFilter = ""
                    dtView.RowFilter = "CheckValue='True' AND doqty>0"
                    If dtView.Count <> iCheck Then
                        Session("WarningListMat") = "DO qty for every checked material data must be more than 0!"
                        showMessage(Session("WarningListMat"), 2)
                        dtView.RowFilter = ""
                        Exit Sub
                    End If
                    dtView.RowFilter = ""
                    dtView.RowFilter = "CheckValue='True' AND doqty<=soqty"
                    If dtView.Count <> iCheck Then
                        Session("WarningListMat") = "DO qty for every checked material data must be less than SO Qty!"
                        showMessage(Session("WarningListMat"), 2)
                        dtView.RowFilter = ""
                        Exit Sub
                    End If

                    If Session("TblDtl") Is Nothing Then
                        CreateTblDetail()
                    End If
                    Dim objTable As DataTable
                    objTable = Session("TblDtl")
                    Dim dv As DataView = objTable.DefaultView
                    Dim objRow As DataRow
                    Dim counter As Integer = objTable.Rows.Count
                    Dim dQty_unitkecil, dQty_unitbesar As Double
                    For C1 As Integer = 0 To dtView.Count - 1
                        dv.RowFilter = "sodtloid=" & dtView(C1)("sodtloid")
                        If dv.Count > 0 Then
                            dv.AllowEdit = True
                            dv(0)("doqty") = dtView(C1)("doqty")
                            dv(0)("doprice") = dtView(C1)("price")
                            Dim dQty As Double = ToDouble(dtView(C1)("doqty"))
                            Dim dPrice As Double = ToDouble(dtView(C1)("price"))
                            Dim dAmt As Double = dQty * dPrice
                            dv(0)("dodtlamt") = dAmt
                            GetUnitConverter(dtView(C1)("itemoid"), dtView(C1)("unitoid"), ToDouble(dtView(C1)("doqty")), dQty_unitkecil, dQty_unitbesar)
                            dv(0)("qtyunitkecil") = dQty_unitkecil
                            dv(0)("qtyunitbesar") = dQty_unitbesar
                            dv(0)("soprice_kons") = dtView(C1)("soprice_kons")
                        Else
                            counter += 1
                            objRow = objTable.NewRow()
                            objRow("dodtlseq") = counter
                            objRow("sodtloid") = dtView(C1)("sodtloid")
                            objRow("itemoid") = dtView(C1)("itemoid")
                            objRow("itemcode") = dtView(C1)("itemcode").ToString
                            objRow("oldcode") = dtView(C1)("itemoldcode").ToString
                            objRow("itemdesc") = dtView(C1)("itemdesc").ToString
                            objRow("stockqty") = dtView(C1)("stockqty")
                            objRow("soqty") = dtView(C1)("soqty")
                            objRow("doqty") = dtView(C1)("doqty")
                            objRow("dounitoid") = dtView(C1)("unitoid")
                            objRow("dounit") = dtView(C1)("unitdesc").ToString
                            objRow("dodtlnote") = dtView(C1)("note").ToString
                            objRow("old_code") = dtView(C1)("old_code").ToString
                            objRow("doprice") = dtView(C1)("price")
                            objRow("soprice_kons") = dtView(C1)("soprice_kons")
                            Dim dQty As Double = ToDouble(dtView(C1)("doqty"))
                            Dim dPrice As Double = ToDouble(dtView(C1)("price"))
                            Dim dAmt As Double = dQty * dPrice
                            objRow("dodtlamt") = dAmt
                            GetUnitConverter(dtView(C1)("itemoid"), dtView(C1)("unitoid"), ToDouble(dtView(C1)("doqty")), dQty_unitkecil, dQty_unitbesar)
                            objRow("qtyunitkecil") = dQty_unitkecil
                            objRow("qtyunitbesar") = dQty_unitbesar
                            objTable.Rows.Add(objRow)
                        End If
                        dv.RowFilter = ""
                    Next
                    dtView.RowFilter = ""
                    Session("TblDtl") = objTable
                    gvDtl.DataSource = objTable
                    gvDtl.DataBind()
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                    ClearDetail()
                    SumTotalAmount()
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub doqtyprice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dorawprice.TextChanged, dorawqty.TextChanged
        dorawdtlamt.Text = ToMaskEdit(ToDouble(dorawqty.Text) * ToDouble(dorawprice.Text), 2)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            If Session("TblDtl") Is Nothing Then
                CreateTblDetail()
            End If
            Dim objTable As DataTable = Session("TblDtl")
            Dim dv As DataView = objTable.DefaultView
            Dim dQty_unitkecil, dQty_unitbesar As Double
            If i_u2.Text = "New Detail" Then
                dv.RowFilter = "sodtloid=" & sorawdtloid.Text
            Else
                dv.RowFilter = "sodtloid=" & sorawdtloid.Text & " AND dodtlseq<>" & dorawdtlseq.Text
            End If
            If dv.Count > 0 Then
                showMessage("This Data has been added before. Please check!", 2)
                dv.RowFilter = ""
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                objRow("dodtlseq") = objTable.Rows.Count + 1
            Else
                objRow = objTable.Rows(dorawdtlseq.Text - 1)
                objRow.BeginEdit()
            End If
            objRow("sodtloid") = sorawdtloid.Text
            objRow("itemoid") = matrawoid.Text
            objRow("itemcode") = matrawcode.Text
            objRow("oldcode") = oldcode.Text
            objRow("itemdesc") = matrawlongdesc.Text
            objRow("stockqty") = ToDouble(stock.Text)
            objRow("soqty") = ToDouble(sorawqty.Text)
            objRow("doqty") = ToDouble(dorawqty.Text)
            objRow("dounitoid") = dorawunitoid.SelectedValue
            objRow("dounit") = dorawunitoid.SelectedItem.Text
            objRow("dodtlnote") = dorawdtlnote.Text
            objRow("doprice") = ToDouble(dorawprice.Text)
            Dim dAmt As Double = Math.Round(ToDouble(dorawqty.Text) * ToDouble(dorawprice.Text), 2, MidpointRounding.AwayFromZero)
            objRow("dodtlamt") = dAmt
            GetUnitConverter(matrawoid.Text, dorawunitoid.SelectedValue, ToDouble(dorawqty.Text), dQty_unitkecil, dQty_unitbesar)
            objRow("qtyunitkecil") = dQty_unitkecil
            objRow("qtyunitbesar") = dQty_unitbesar
            If i_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            Session("TblDtl") = objTable
            gvDtl.DataSource = objTable
            gvDtl.DataBind()
            ClearDetail()
            SumTotalAmount()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 2)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 2)
        End If
    End Sub

    Protected Sub gvDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("dodtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvDtl.DataSource = objTable
        gvDtl.DataBind()
        ClearDetail()
        SumTotalAmount()
    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDtl.SelectedIndexChanged
        Try
            dorawdtlseq.Text = gvDtl.SelectedDataKey.Item("dodtlseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "dodtlseq=" & dorawdtlseq.Text
                sorawdtloid.Text = dv.Item(0).Item("sodtloid").ToString
                matrawoid.Text = dv.Item(0).Item("itemoid").ToString
                matrawcode.Text = dv.Item(0).Item("itemcode").ToString
                oldcode.Text = dv.Item(0).Item("oldcode").ToString
                matrawlongdesc.Text = dv.Item(0).Item("itemdesc").ToString
                stock.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("stockqty").ToString), 2)
                sorawqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("soqty").ToString), 2)
                dorawqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("doqty").ToString), 2)
                stockqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("doprice").ToString), 2)
                sSql = "SELECT ISNULL(d.doqty, 0) FROM ql_trndodtl d WHERE d.cmpcode='" & CompnyCode & "' AND d.sodtloid=" & sorawdtloid.Text & ""
                delivQty.Text = ToMaskEdit(ToDouble(cKon.ambilscalar(sSql)), 2)
                InitDDLUnit()
                dorawunitoid.SelectedValue = dv.Item(0).Item("dounitoid").ToString
                dorawprice.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("doprice").ToString), 2)
                dorawdtlamt.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("dodtlamt").ToString), 2)
                dorawdtlnote.Text = dv.Item(0).Item("dodtlnote").ToString
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        Finally
            btnSearchSO.Visible = False : btnClearSO.Visible = False
            btnSearchMat.Visible = False
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            dorawdtloid.Text = GenerateID("QL_TRNDODTL", CompnyCode)
            Dim iAppOid As Integer = GenerateID("QL_APPROVAL", CompnyCode)
            Dim isRegenOid As Boolean = False
            Dim objValue As VarUsageValue
            SetUsageValue(objValue)
            If Session("oid") = Nothing Or Session("oid") = "" Then
                If CheckDataExists("SELECT COUNT(*) FROM QL_trndomst WHERE domstoid=" & dorawmstoid.Text) Then
                    dorawmstoid.Text = GenerateID("QL_TRNDOMST", CompnyCode)
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNDOMST", "domstoid", dorawmstoid.Text, "domststatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    dorawmststatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            periodacctg.Text = GetDateToPeriodAcctg(CDate(dorawdate.Text()))
            Dim DueDate As Integer
            sSql = "SELECT gendesc FROM QL_trnsomst som inner join ql_mstgen g ON g.genoid=som.sopaytypeoid WHERE som.cmpcode='" & CompnyCode & "' AND gengroup='PAYMENT TYPE' AND som.somstoid=" & sorawmstoid.Text
            If GetStrData(sSql).ToUpper <> "CASH" Then
                DueDate = CInt(GetStrData(sSql))
                doDueDate.Text = Format(CDate(dorawdate.Text).AddDays(DueDate), "MM/dd/yyyy")
            Else
                doDueDate.Text = Format(CDate(dorawdate.Text), "MM/dd/yyyy")
            End If

            If dorawmststatus.Text = "Post" Then
                GenerateDONo()
            End If
            If dorawmststatus.Text = "Revised" Then
                dorawmststatus.Text = "In Process"
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trndomst (cmpcode, domstoid, periodacctg, dono, dotype, dodate, custoid , somstoid, dowhoid, expedisioid, nopolisi, dodelivtypeoid, doplandelivdate, domstnote, domstres1, domstres2, domstres3, domststatus, dototalamt, createuser, createtime, upduser, updtime, curroid, doreqdate) VALUES ('" & DDLBusUnit.SelectedValue & "', " & dorawmstoid.Text & ", '" & periodacctg.Text & "', '" & dorawno.Text & "', 'DO', '" & dorawdate.Text & "', " & custoid.Text & ", " & sorawmstoid.Text & ", " & dowhoid.SelectedValue & ", " & expedisioid.SelectedValue & ", '" & Tchar(nopolisi.Text) & "', 0, '" & doDueDate.Text & "', '" & Tchar(dorawmstnote.Text.Trim) & "', '" & sotype.SelectedValue & "','','', '" & dorawmststatus.Text & "', " & ToDouble(dorawtotalamt.Text) & ", '" & createuser.Text & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & curroid.SelectedValue & ", '" & IIf(dorawreqdate.Text = "", "01/01/1900", dorawreqdate.Text) & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & dorawmstoid.Text & " WHERE tablename='QL_TRNDOMST' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trndomst SET periodacctg= '" & periodacctg.Text & "', dono='" & dorawno.Text & "', dotype='DO', dodate='" & dorawdate.Text & "', custoid=" & custoid.Text & ", somstoid=" & sorawmstoid.Text & ", dowhoid=" & dowhoid.SelectedValue & ", expedisioid=" & expedisioid.SelectedValue & ", nopolisi='" & Tchar(nopolisi.Text) & "', doplandelivdate='" & doDueDate.Text & "', domstnote='" & Tchar(dorawmstnote.Text.Trim) & "', domstres1='" & sotype.SelectedValue & "', domstres2='', domstres3='', domststatus='" & dorawmststatus.Text & "', dototalamt=" & ToDouble(dorawtotalamt.Text) & ", upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, curroid=" & curroid.SelectedValue & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND domstoid=" & dorawmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnsodtl SET sodtlstatus='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND sodtloid IN (SELECT sodtloid FROM QL_trndodtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND domstoid=" & dorawmstoid.Text & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnsomst SET somststatus='Approved' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND somstoid IN (SELECT somstoid FROM QL_trndodtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND domstoid=" & dorawmstoid.Text & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE QL_trndodtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND domstoid=" & dorawmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = SortingDataTable(Session("TblDtl"), "dodtlseq")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trndodtl (cmpcode, dodtloid, domstoid, dodtlseq, somstoid, sodtloid, matoid, doqty, dounitoid, doprice, dodtlamt, dodtlnote, dodtlres1, dodtlres2, dodtlres3, dodtlstatus, doqty_unitkecil, doqty_unitbesar, upduser, updtime, valueidr, valueusd, old_code) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C1 + CInt(dorawdtloid.Text)) & ", " & dorawmstoid.Text & ", " & C1 + 1 & ", " & sorawmstoid.Text & ", " & objTable.Rows(C1)("sodtloid").ToString & ", " & objTable.Rows(C1)("itemoid").ToString & ", " & ToDouble(objTable.Rows(C1)("doqty").ToString) & ", " & objTable.Rows(C1)("dounitoid").ToString & ", " & ToDouble(objTable.Rows(C1)("doprice").ToString) & ", " & ToDouble(objTable.Rows(C1)("dodtlamt").ToString) & ", '" & Tchar(objTable.Rows(C1)("dodtlnote").ToString) & "', '','','', 'Post', " & ToDouble(objTable.Rows(C1)("qtyunitkecil").ToString) & ", " & ToDouble(objTable.Rows(C1)("qtyunitbesar").ToString) & ", '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(C1)("valueidr").ToString) & ", " & ToDouble(objTable.Rows(C1)("valueusd").ToString) & ", '" & Tchar(objTable.Rows(C1)("old_code").ToString) & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()

                        If ToDouble((objTable.Compute("SUM(doqty)", "sodtloid='" & objTable.Rows(C1)("sodtloid") & "'")).ToString) >= ToDouble(objTable.Rows(C1).Item("soqty").ToString) Then
                            sSql = "UPDATE QL_trnsodtl SET sodtlstatus='COMPLETE', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND sodtloid=" & objTable.Rows(C1)("sodtloid").ToString
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_trnsomst SET somststatus='Closed', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND somstoid=" & sorawmstoid.Text & " AND (SELECT COUNT(*) FROM QL_trnsodtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND sodtlstatus='' AND somstoid=" & sorawmstoid.Text & " AND sodtloid<>" & objTable.Rows(C1)("sodtloid").ToString & ")=0"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(dorawdtloid.Text)) & " WHERE tablename='QL_TRNDODTL' AND cmpcode='" & CompnyCode & "' "
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    If dorawmststatus.Text = "In Approval" Then
                        Dim dt As DataTable = Session("AppPerson")
                        Dim tempAppCode As String = ""
                        For C1 As Integer = 0 To dt.Rows.Count - 1
                            If C1 = 0 Then
                                tempAppCode = dt.Rows(C1)("apppersonlevel").ToString
                            End If
                            sSql = "INSERT INTO QL_approval (cmpcode, approvaloid, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvalnote, posisiLevel) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iAppOid + C1 & ", 'DO-" & dorawmstoid.Text & "_" & (iAppOid + C1) & "', '" & Session("UserID") & "', '" & dorawdate.Text & "', 'New', 'QL_trndomst', " & dorawmstoid.Text & ", 'In Approval', '" & dt.Rows(C1)("apppersonlevel").ToString & "', '" & dt.Rows(C1)("approvaluser").ToString & "', '1/1/1900', '','" & IIf(tempAppCode = dt.Rows(C1)("apppersonlevel").ToString, "1", "") & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        Next
                        sSql = "UPDATE QL_mstOid SET lastoid=" & iAppOid + dt.Rows.Count - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_APPROVAL'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                End If
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        dorawmststatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    dorawmststatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                conn.Close()
                showMessage(ex.Message, 1)
                dorawmststatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & dorawmstoid.Text & ".<BR>"
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trndo.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trndo.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If dorawmstoid.Text.Trim = "" Then
            showMessage("Please select DO data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNDOMST", "domstoid", dorawmstoid.Text, "domststatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                dorawmststatus.Text = "In Process"
                Exit Sub
            End If
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trnsodtl SET sodtlstatus='', soclosingqty=0 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND sodtloid IN (SELECT sodtloid FROM QL_trndodtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND domstoid=" & dorawmstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_trnsomst SET somststatus='Approved' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND somstoid IN (SELECT somstoid FROM QL_trndodtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND domstoid=" & dorawmstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE QL_trndodtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND domstoid=" & dorawmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE QL_trndomst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND domstoid=" & dorawmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trndo.aspx?awal=true")
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPost.Click
        sSql = "SELECT approvaluser, apppersontype, (case when upper(apppersontype)='FINAL' then 0 else apppersonlevel end) apppersonlevel FROM QL_approvalperson WHERE tablename='QL_trndomst' AND activeflag='ACTIVE' AND cmpcode IN ('" & CompnyCode & "', '" & DDLBusUnit.SelectedValue & "') order by apppersontype desc,apppersonlevel asc"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_approvalperson")
        If dt.Rows.Count = 0 Then
            showMessage("There is no user can't approve this transaction. Please define approval user in <STRONG>APPROVAL USER</STRONG> form.", 2)
        Else
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "apppersontype='Final'"
            If dv.Count = 0 Then
                showMessage("Final approval user is not defined for this transaction. Please define final approval user in <STRONG>APPROVAL USER</STRONG> form.", 2)
                dv.RowFilter = ""
            Else
                dv.RowFilter = ""
                Session("AppPerson") = dt
                dorawmststatus.Text = "In Approval"
                btnSave_Click(Nothing, Nothing)
            End If
        End If
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        UpdateCheckedValue()
        PrintReport()
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterTextListMat.Text = ""
        If UpdateCheckedMat() Then
            BindMaterial()
            Session("TblMatView") = Session("TblMat")
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "sodtloid=" & dtTbl.Rows(C1)("sodtloid")
                    objView(0)("CheckValue") = "True"
                    dtTbl.Rows(C1)("CheckValue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                dtTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "No material data can't be selected!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "No material data can't be selected!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "sodtloid=" & dtTbl.Rows(C1)("sodtloid")
                    objView(0)("CheckValue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                dtTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "No material data can't be selected!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "No material data can't be selected!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "CheckValue='True'"
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListMat") = "No material data have been selected before!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

#End Region

End Class