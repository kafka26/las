'Pgmr: ANDIKA ===> 140424
Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_StockOpname
    Inherits System.Web.UI.Page

#Region "Variables"
    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"

    Private Function IsValidDate(ByVal sDate As String, ByVal sFormat As String, ByRef sError As String) As Boolean
        If sFormat.Trim <> "MM/dd/yyyy" And sFormat.Trim <> "MM/dd/yyyy" Then
            sError = "Date format should be '<STRONG>MM/dd/yyyy</STRONG>' or '<STRONG>MM/dd/yyyy</STRONG>'."
            Return False
        End If
        If sFormat = "MM/dd/yyyy" Then
            sDate = CDate(sDate)
        End If
        Dim splitdate() As String = sDate.Split("/")
        If splitdate.Length < 3 Then
            sError = "Date is incomplete. Date component must be consisted of day, month and year value."
            Return False
        End If
        If splitdate.Length > 3 Then
            sError = "Date is out of range. Date component must be consisted of day, month and year value only."
            Return False
        End If
        If ToDouble(splitdate(2)) >= 1900 And ToDouble(splitdate(2)) <= 9999 Then
            If ToDouble(splitdate(0)) > 0 And ToDouble(splitdate(0)) < 13 Then
                Dim iNDay As Integer = Date.DaysInMonth(CInt(splitdate(2)), CInt(splitdate(0)))
                If ToDouble(splitdate(1)) < 1 Or ToDouble(splitdate(1)) > CDbl(iNDay) Then
                    sError = "Day value is invalid. Day value must be between 1 and " & iNDay.ToString & "."
                    Return False
                End If
            Else
                sError = "Month value is invalid. Month value must be between 1 and 12."
                Return False
            End If
        Else
            sError = "Year value is invalid. Year value must be between 1900 and 9999."
            Return False
        End If
        Return True
    End Function

    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If stockopdate.Text = "" Then
            sError &= "- Please fill DATE field!<BR>"
        Else
            If Not IsValidDate(stockopdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If Session("QL_trnStockOpnameDtl") Is Nothing Then
            sError &= "- Please fill DETAIL DATA!<BR>"
        End If
        If Not Session("QL_trnStockOpnameDtl") Is Nothing Then
            Dim objTbl As DataTable = Session("QL_trnStockOpnameDtl")
            If objTbl.Rows.Count <= 0 Then
                sError &= "- Please fill DETAIL DATA!<BR>"
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            stockstatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub showprint(ByVal type As String)
        Try
            Dim sSql As String = ""
            Dim sWhere As String = ""
            Dim WHoid As String = ""
            Dim cat1 As String = ""
            Dim cat2 As String = ""
            Dim cat3 As String = ""
            Dim LocStock As String = ""
            Dim sType As String = ddlType.SelectedItem.Text
            periodacctg.Text = GetDateToPeriodAcctg(CDate(stockopdate.Text))
            If stockopwarehouseoid.SelectedValue <> "ALL" Then
                If stockopwarehouseoid.SelectedValue <> "NULL" Then
                    WHoid &= " AND g2.genoid= " & stockopwarehouseoid.SelectedValue & " AND g2.gengroup='WAREHOUSE'"
                Else
                    WHoid &= ""
                End If
            Else
                WHoid &= " AND g2.genoid= " & stockopwarehouseoid.SelectedValue & " AND g2.gengroup='WAREHOUSE'"
            End If
            If itemcat.SelectedValue <> "" Then
                cat1 &= " AND c1.cat1code='" & itemcat.SelectedValue & "'"
            Else
                cat1 &= " AND c1.cat1code='%'"
            End If
            If ddlGenCat.SelectedValue <> "" Then
                cat2 &= " AND c2.cat2code='" & ddlGenCat.SelectedValue & "'"
            Else
                cat2 &= " AND c2.cat2code='%'"
            End If
            If ddlGenSubCat.SelectedValue <> "" Then
                cat3 &= " AND c3.cat3code='" & ddlGenSubCat.SelectedValue & "'"
            Else
                cat3 &= " AND c3.cat3code='%'"
            End If

            If txtFilter.Text.Trim <> "" Then
                sWhere &= " AND " & ddlFilter.SelectedValue & " like '%" & Tchar(txtFilter.Text) & "%' "
            End If

            Dim namaPDF As String = ""
            vReport = New ReportDocument

            If type = "EXCEL" Then
                vReport.Load(Server.MapPath("~\Report\PrintlistMat.rpt"))
                namaPDF = "List_Material_"
            ElseIf type = "PDF" Then
                vReport.Load(Server.MapPath("~\Report\PrintlistMat.rpt"))
                namaPDF = "List_Material_"
            End If
            sWhere &= " WHERE i.cmpcode='" & busunit.SelectedValue & "' " & sWhere & "" & WHoid & " AND i.itemgroup='" & ddlType.SelectedValue & "'"

            'sSql = "SELECT 0 as dtlseq, 0 as isApply, i.cmpcode,w.gendesc warehouse,i.itemcode,i.itemoid,i.itemLongDescription itemlocaldesc, 0.0 Qty1, i.itemunit1, i.itemunit2 ,g1.gendesc unit1,s.saldoakhir LastQty, 0.0 Qty2 , g2.gendesc unit2 ,g3.gendesc unit3 FROM QL_mstitem i INNER JOIN QL_mstcat1 c1 ON c1.cat1oid=i.itemCat1 INNER JOIN QL_mstcat2 c2 ON c2.cat2oid=i.itemCat2 INNER JOIN QL_mstcat3 c3 ON c3.cat3oid=i.itemCat3 INNER JOIN QL_mstgen g1 ON g1.genoid=i.itemUnit1 INNER JOIN QL_mstgen g2 ON g2.genoid=i.itemUnit2 INNER JOIN QL_mstgen g3 ON g3.genoid=i.itemUnit3 INNER JOIN QL_crdstock s ON s.refoid=i.itemoid INNER JOIN QL_mstgen w ON w.genoid=s.mtrlocoid " & sWhere & " AND NOT EXISTS (select stockoprefoid from QL_TRNSTOCKOPNAMEDTL odtl INNER JOIN QL_TRNSTOCKOPNAME omst on odtl.cmpcode=omst.cmpcode and odtl.stockopoid=omst.stockopoid where odtl.stockoprefoid=i.itemoid and odtl.stockoprefname='QL_mstitem' and odtl.stockopareaoid=g1.genoid and odtl.stockopwarehouseoid=w.genoid and odtl.stockoplocationoid=g3.genoid and omst.stockopdate='" & Date.ParseExact(stockopdate.Text, "MM/dd/yyyy", Nothing, Nothing) & "') order by i.itemLongDescription"

            vReport.SetParameterValue("sWhere1", sWhere)
            vReport.SetParameterValue("sType", sType)

            Response.Buffer = False
            'Clear the response content and headers
            Response.ClearContent()
            Response.ClearHeaders()
            ' Export the Report to Response stream in Excel format and file name Customers
            vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, GetServerTime().ToString("MM_dd_yyyy"))
            If type = "EXCEL" Then
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, namaPDF)

            Else
                If type = "PDF" Then
                    vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, namaPDF)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub UpdateCheckedValue()
        'If Session("TblMst") IsNot Nothing Then
        '    Dim dt As DataTable = Session("TblMst")
        '    Dim dv As DataView = dt.DefaultView
        '    For C1 As Integer = 0 To gvTRN.Rows.Count - 1
        '        Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
        '        If (row.RowType = DataControlRowType.DataRow) Then
        '            Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
        '            Dim cbCheck As Boolean = False
        '            Dim sOid As String = ""
        '            For Each myControl As System.Web.UI.Control In cc
        '                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
        '                    cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
        '                    sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
        '                End If
        '            Next

        '            Dim sID() As String = sOid.Split(",")
        '            dv.RowFilter = "m.cmpcode='" & sID(0) & "' AND m.stockopoid='" & sID(1) & "'"
        '            If cbCheck = True Then
        '                dv(0)("checkvalue") = "True"
        '            Else
        '                dv(0)("checkvalue") = "False"
        '            End If
        '            dv.RowFilter = ""
        '        End If
        '    Next
        '    dt.AcceptChanges()
        '    Session("TblMst") = dt
        'End If
    End Sub

    Private Sub WareHouseDDL()
        sSql = "SELECT DISTINCT g.genoid,g.gendesc FROM QL_mstgen g INNER JOIN QL_crdstock crd ON crd.mtrlocoid=g.genoid AND gengroup='WAREHOUSE' INNER JOIN QL_mstitem i ON i.itemoid=crd.refoid WHERE g.cmpcode='" & CompnyCode & "' AND i.itemGroup LIKE '%" & ddlType.SelectedValue & "%'"
        FillDDL(stockopwarehouseoid, sSql)
        stockopwarehouseoid.Items.Add("ALL")
        stockopwarehouseoid.SelectedValue = "ALL"
    End Sub

    Private Sub InitAllDDL()
        sSql = "SELECT gencode,gendesc from QL_mstgen where gengroup='GROUPITEM' AND cmpcode = '" & CompnyCode & "'"
        FillDDL(ddlType, sSql)
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE' "
        Dim x As String = Session("CompnyCode")
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
            busunit.Enabled = False
            busunit.CssClass = "inpTextDisabled"
        Else
            busunit.Enabled = True
            busunit.CssClass = "inpText"
        End If
        FillDDL(busunit, sSql)

    End Sub

    Private Sub InitFilterDDLCat1(ByVal sType As String)
        'Fill DDL Category1
        Dim OidWh As String = ""
        If stockopwarehouseoid.SelectedValue <> "ALL" Then
            OidWh &= " AND crd.mtrlocoid LIKE '%" & stockopwarehouseoid.SelectedValue & "%'"
        End If
        sSql = "SELECT DISTINCT cat1code, cat1shortdesc FROM QL_mstcat1 c1 INNER JOIN QL_mstitem i ON i.itemCat1=c1.cat1oid INNER JOIN QL_crdstock crd ON i.itemoid=crd.refoid WHERE c1.cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat1res1='" & sType & "' " & OidWh & ""
        '"SELECT cat1code, cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat1res1='" & sType & "'"

        FillDDL(itemcat, sSql)
        If itemcat.Items.Count > 0 Then
            InitFilterDDLCat2(sType, itemcat.SelectedValue)
        Else
            ddlGenCat.Items.Clear()
            ddlGenSubCat.Items.Clear()
        End If
    End Sub

    Private Sub InitFilterDDLCat2(ByVal sType As String, ByVal CatCode As String)
        'Fill DDL Category1
        sSql = "SELECT DISTINCT cat2code, cat2shortdesc FROM QL_mstcat2 cat2 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat2.cmpcode AND cat1.cat1oid=cat2.cat1oid WHERE cat2.cmpcode='" & CompnyCode & "' AND cat2.activeflag='ACTIVE' AND cat1res1='" & sType & "' AND cat1.cat1code='" & CatCode & "'"

        FillDDL(ddlGenCat, sSql)
        If ddlGenCat.Items.Count > 0 Then
            InitFilterDDLCat3(sType, itemcat.SelectedValue, ddlGenCat.SelectedValue)
        Else
            ddlGenSubCat.Items.Clear()
        End If
    End Sub

    Private Sub InitFilterDDLCat3(ByVal sType As String, ByVal CatSatu As String, ByVal Catcode As String)
        'Fill DDL Category3
        sSql = "SELECT cat3code, cat3shortdesc FROM QL_mstcat3 cat3 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat3.cmpcode AND cat1.cat1oid=cat3.cat1oid AND cat1.cat1code LIKE '%" & CatSatu & "%' INNER JOIN QL_mstcat2 cat2 ON cat2.cmpcode=cat3.cmpcode AND cat2.cat2oid=cat3.cat2oid AND cat2.cat2code LIKE '%" & Catcode & "%' WHERE cat3.cmpcode='" & CompnyCode & "' AND cat3.activeflag='ACTIVE' AND cat3res1='" & sType & "'"
        FillDDL(ddlGenSubCat, sSql)
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT 0 AS checkvalue, m.cmpcode,m.stockopoid, m.stockopno, CONVERT(CHAR(10), m.stockopdate, 103) AS stockopdate, m.stockopstatus FROM QL_TRNSTOCKOPNAME m " & sSqlPlus & " ORDER BY CONVERT(DATETIME, m.stockopdate) DESC, m.stockopoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_TRNSTOCKOPNAME")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub ClearDetail()
        ddlType.SelectedIndex = 0
        ddlType_SelectedIndexChanged(Nothing, Nothing)
        'stockopareaoid.SelectedIndex = -1
        stockopwarehouseoid.SelectedValue = "ALL"
        itemcat.Items.Add("NONE")
        ddlGenCat.Items.Add("NONE")
        ddlGenSubCat.Items.Add("NONE")
        ddlFilter.SelectedIndex = -1
        Cat1CB.Checked = False
        Cat2CB.Checked = False
        Cat3CB.Checked = False
        txtFilter.Text = ""
        Session("QL_trnStockOpnameDtl") = Nothing
        gvStockOpname.DataSource = Session("QL_trnStockOpnameDtl")
        gvStockOpname.Visible = False
    End Sub

    Private Sub FillTextBox(ByVal sCmpcode As String, ByVal sOid As String)
        sSql = "SELECT m.stockopoid, m.stockopdate, m.stockopno, m.stockoptype, m.stockopstatus, m.createuser, m.createdate, m.upduser, m.updtime,(SELECT dt.stockopwarehouseoid FROM QL_trnstockopnamedtl dt WHERE dt.cmpcode=m.cmpcode AND dt.stockopoid=m.stockopoid) AS OidWh FROM QL_TRNSTOCKOPNAME m WHERE m.stockopoid=" & sOid
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        While xreader.Read
            stockopoid.Text = Trim(xreader("stockopoid").ToString)
            stockopdate.Text = Format(xreader("stockopdate"), "MM/dd/yyyy")
            stockno.Text = Trim(xreader("stockopno").ToString)
            stockopwarehouseoid.SelectedValue = Trim(xreader("OidWh").ToString)
            stockstatus.Text = Trim(xreader("stockopstatus").ToString)
            ddlType.SelectedValue = Trim(xreader("stockoptype").ToString)
            ddlType_SelectedIndexChanged(Nothing, Nothing)
            createuser.Text = Trim(xreader("createuser").ToString)
            createtime.Text = Format(xreader("createdate"), "MM/dd/yyyy HH:mm:ss")
            upduser.Text = Trim(xreader("upduser").ToString)
            updtime.Text = Format(xreader("updtime"), "MM/dd/yyyy HH:mm:ss")
            ddlType.Enabled = False
            ddlType.CssClass = "inpTextDisabled"
        End While
        xreader.Close()
        conn.Close()

        sSql = "SELECT Distinct 0 dtlseq, 1 as isApply, d.stockopareaoid areaoid, '-' area, d.stockopwarehouseoid whoid, g2.gendesc warehouse, d.stockoplocationoid locoid, '-' location, d.stockopqty1 Qty1, g4.gendesc AS unit1, d.stockopqty2 Qty2, g5.gendesc AS unit2, d.stockopunit1 itemunitoid1, d.stockopunit2 itemunitoid2, i.itemcode, i.itemLongDescription AS itemlocaldesc, d.stockoprefoid itemoid  FROM QL_TRNSTOCKOPNAMEDTL d " & _
        " INNER JOIN QL_mstitem i ON i.itemoid=d.stockoprefoid" & _
        " AND d.stockoprefname='QL_MSTITEM' AND i.cmpcode=d.cmpcode " & _
        " INNER JOIN QL_mstgen g2 ON g2.cmpcode=d.cmpcode AND g2.genoid=d.stockopwarehouseoid " & _
        " INNER JOIN QL_mstgen g4 ON g4.cmpcode=i.cmpcode AND g4.genoid=i.itemunit1" & _
        " INNER JOIN QL_mstgen g5 ON g5.cmpcode=i.cmpcode AND g5.genoid=i.itemunit2" & _
        " LEFT JOIN QL_crdstock crd ON crd.refoid=d.stockoprefoid AND crd.cmpcode=d.cmpcode /*AND crd.refname=d.stockoprefname*/" & _
        " AND crd.periodacctg=d.periodacctg AND crd.mtrlocoid=d.stockopwarehouseoid" & _
        " WHERE i.cmpcode='" & sCmpcode & "' AND d.stockopoid=" & sOid & " AND d.stockoprefname='QL_MSTITEM' ORDER BY dtlseq"


        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnStockOpnameDtl")
        For C1 As Integer = 0 To dtTbl.Rows.Count - 1
            dtTbl.Rows(C1)("dtlseq") = C1 + 1
        Next
        Session("QL_trnStockOpnameDtl") = dtTbl
        gvStockOpname.DataSource = Session("QL_trnStockOpnameDtl")
        gvStockOpname.DataBind()
        gvStockOpname.Visible = True
        If stockstatus.Text = "In Process" Then
            btnPrintHdr.Visible = False
            imbFindForm.Visible = False
            btnClear.Visible = False
        Else
            btnSave.Visible = False
            btnDelete.Visible = False
            btnPosting.Visible = False
            imbFindForm.Visible = False
            btnClear.Visible = False
            gvStockOpname.Columns(0).Visible = False
            'gvStockOpname.Columns(gvStockOpname.Columns.Count - 1).Visible = False
            If stockstatus.Text = "Post" Then
                lblTrnNo.Text = "Stock Opname No."
                stockopoid.Visible = False
                stockno.Visible = True
            End If
        End If
    End Sub

#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then Response.Redirect("~/Other/login.aspx")
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnstockopname.aspx")
        End If
        If checkPagePermission("~\Transaction\trnstockopname.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Stock Opname"
        Session("oid") = Request.QueryString("oid")
        Session("CompnyCode") = CompnyCode
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            'CheckPRStatus()
            InitAllDDL()
            WareHouseDDL()
            ddlType_SelectedIndexChanged(Nothing, Nothing)
            'stockopareaoid_SelectedIndexChanged(Nothing, Nothing)
            FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")

            Dim sSqlPlus As String = " WHERE 1=1 "
            FilterDDL.SelectedIndex = 0
            FilterText.Text = ""
            cbPeriode.Checked = False
            FilterPeriod1.Text = Format(GetServerTime(), "MM/dd/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            cbStatus.Checked = False
            FilterDDLStatus.SelectedIndex = -1
            sSqlPlus &= " AND m.cmpcode='" & CompnyCode & "' "
            BindTrnData(sSqlPlus)
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("CompnyCode"), Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                stockopoid.Text = GenerateID("QL_TRNSTOCKOPNAME", CompnyCode)
                stockno.Text = stockopoid.Text
                stockopdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                periodacctg.Text = GetDateToPeriodAcctg(CDate(stockopdate.Text))
                stockstatus.Text = "In Process"
                btnDelete.Visible = False
                btnPrintHdr.Visible = True
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnstockopname.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lkbPRInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbPRInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = 0
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 1
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, prm.updtime, GETDATE()) > " & nDays & " AND prm.prrawmststatus='In Process' "

        sSqlPlus &= "AND prm.cmpcode='" & CompnyCode & "' "


        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub lkbPRInApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbPRInApproval.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = 0
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 2
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, prm.updtime, GETDATE()) > " & nDays & " AND prm.prrawmststatus='In Approval' "

        sSqlPlus &= "AND prm.cmpcode='" & CompnyCode & "'"


        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " WHERE " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        sSqlPlus &= " AND m.cmpcode='" & CompnyCode & "'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND m.stockopdate>='" & CDate(FilterPeriod1.Text) & " 00:00:00' AND m.stockopdate<='" & CDate(FilterPeriod2.Text) & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            If FilterDDLStatus.SelectedValue <> "All" Then
                sSqlPlus &= " AND m.stockopstatus='" & FilterDDLStatus.SelectedValue & "'"
            End If
        End If

        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = " WHERE 1=1 "
        FilterDDL.SelectedIndex = 0
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1

        sSqlPlus &= " AND m.cmpcode='" & CompnyCode & "' "


        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnPrintHdr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrintHdr.Click
        showprint("EXCEL")
    End Sub

    Protected Sub gvTRN_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvTRN.SelectedIndexChanged
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            sSql = "select count(-1) from QL_trnstockopname where stockopoid=" & gvTRN.SelectedDataKey("stockopoid") & " and stockopstatus='Post'"
            xCmd.CommandText = sSql
            If xCmd.ExecuteScalar <= 0 Then
                showMessage("Please post your transaction before print !", 2)
                conn.Close()
                Exit Sub
            End If
            conn.Close()

            report = New ReportDocument
            report.Load(Server.MapPath("~/report/rptprintstockopname.rpt"))
            report.SetParameterValue("swhere", "and m.cmpcode='" & CompnyCode & "' and m.stockopoid=" & gvTRN.SelectedDataKey("stockopoid") & "")
            report.SetParameterValue("sWhere", "")
            report.SetParameterValue("sUserID", Session("UserID"))

            cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "STOCK_OPNAME_" & gvTRN.SelectedDataKey("stockopno"))
        Catch ex As Exception
            showMessage(ex.Message, 2)
            Exit Sub
        End Try
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Sub UpdateDetail()
        If Not Session("QL_trnStockOpnameDtl") Is Nothing Then
            Dim tmpQty1 As Double = 0.0
            Dim tmpQty2 As Double = 0.0
            Dim iError As Int16 = 0
            Dim objTable As DataTable
            objTable = Session("QL_trnStockOpnameDtl")
            Try
                For c1 As Integer = 0 To objTable.Rows.Count - 1
                    iError = c1

                    Dim row As System.Web.UI.WebControls.GridViewRow = gvStockOpname.Rows(c1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(6).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                                If CType(myControl, System.Web.UI.WebControls.TextBox).ID = "Qty1" Then
                                    tmpQty1 = ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text)
                                End If
                            End If
                        Next
                        Dim dr As DataRow = objTable.Rows(c1)
                        dr.BeginEdit()
                        dr("Qty1") = tmpQty1
                        dr.EndEdit()

                        Dim ccc As System.Web.UI.ControlCollection = row.Cells(8).Controls
                        For Each myControl As System.Web.UI.Control In ccc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                                If CType(myControl, System.Web.UI.WebControls.TextBox).ID = "Qty2" Then
                                    tmpQty2 = ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text)
                                End If
                            End If
                        Next
                        dr = objTable.Rows(c1)
                        dr.BeginEdit()
                        dr("Qty2") = tmpQty2
                        dr.EndEdit()

                    End If
                Next

                Session("QL_trnStockOpnameDtl") = objTable
                gvStockOpname.DataSource = objTable
                gvStockOpname.DataBind()

            Catch ex As Exception
                showMessage(ex.ToString, 1)
                Exit Sub
            End Try
        End If
    End Sub

    Private Sub UpdateTblDtl(ByRef dtRef As DataTable)
        Dim bVal As Integer, sOid As String, dQty1 As Double, dQty2 As Double
        For C1 As Integer = 0 To gvStockOpname.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvStockOpname.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                bVal = 0 : sOid = "" : dQty1 = 0 : dQty2 = 0
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                    End If
                Next
                cc = row.Cells(5).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                        dQty1 = ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text)
                    End If
                Next
                cc = row.Cells(7).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                        dQty2 = ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text)
                    End If
                Next
                dtRef.DefaultView.RowFilter = "dtlseq=" & sOid
                If dtRef.DefaultView.Count > 0 Then
                    If dQty1 > 0.0 Or dQty2 > 0.0 Then
                        dtRef.DefaultView(0)("isApply") = 1
                        dtRef.DefaultView(0)("Qty1") = dQty1
                        dtRef.DefaultView(0)("Qty2") = dQty2
                    Else
                        dtRef.DefaultView(0)("isApply") = 0
                        dtRef.DefaultView(0)("Qty1") = 0.0
                        dtRef.DefaultView(0)("Qty2") = 0.0
                    End If
                End If
                dtRef.DefaultView.RowFilter = ""
            End If
        Next
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            If Session("QL_trnStockOpnameDtl") Is Nothing Then
                showMessage("Please define detail data first! <BR>", 2)
                stockstatus.Text = "In Process"
                Exit Sub
            End If
            Dim objTable As DataTable = Session("QL_trnStockOpnameDtl")
            UpdateTblDtl(objTable)
            objTable.DefaultView.RowFilter = " isApply = 1"
            If objTable.DefaultView.Count <= 0 Then
                showMessage("Detail has '" & objTable.DefaultView.Count & "' selected data with qty is equal to zero ! <BR>", 2)
                stockstatus.Text = "In Process"
                Exit Sub
            End If
            objTable.DefaultView.RowFilter = " isApply = 1 AND (Qty1 = 0 OR Qty2 = 0)"
            If objTable.DefaultView.Count > 0 Then
                showMessage("Detail has '" & objTable.DefaultView.Count & "' selected data with qty is equal to zero ! <BR>", 2)
                stockstatus.Text = "In Process"
                Exit Sub
            End If
            Dim stokopno As String = ""
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                If CheckDataExists("SELECT COUNT(*) FROM QL_TRNSTOCKOPNAME WHERE stockopoid=" & stockopoid.Text) Then
                    stockopoid.Text = GenerateID("QL_TRNSTOCKOPNAME", CompnyCode)
                    stockno.Text = stockopoid.Text
                    isRegenOid = True
                End If
                'Else
                '    Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNSTOCKOPNAME", "stockopoid", stockopoid.Text, "prrawmststatus", CDate(updtime.Text), "Approved")
                '    If sStatusInfo <> "" Then
                '        showMessage(sStatusInfo, 2)
                '        stockstatus.Text = "In Process"
                '        Exit Sub
                '    End If
            End If

            If stockstatus.Text = "Post" Then
                'Generate No. Document.
                Dim sNo As String = "OP/" & Format(GetServerTime(), "yyMM") & "/"
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(stockopno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnstockopname WHERE cmpcode='" & CompnyCode & "' AND stockopno LIKE '" & sNo & "%'"
                stockno.Text = GenNumberString(sNo, "", GetStrData(sSql), 4)
            End If
            sSql = "SELECT stockopstatus FROM QL_trnstockopname WHERE stockopoid = " & stockopoid.Text & ""
            xCmd.CommandText = sSql
            Dim currentstatus As String = xCmd.ExecuteScalar
            If Not currentstatus Is Nothing Then
                If currentstatus = "Post" Then
                    showMessage("Stock Opname cannot be Posted !<br />Stock Opname is already in Post state !", 2)
                    objTrans.Rollback()
                    If conn.State = ConnectionState.Open Then
                        conn.Close()
                    End If
                    Exit Sub
                End If
            End If
            Dim dtloid As Integer = GenerateID("QL_TRNSTOCKOPNAMEDTL", CompnyCode)
            If stockstatus.Text = "Post" Then
                stockopdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
            End If
            Try
                periodacctg.Text = GetDateToPeriodAcctg(CDate(stockopdate.Text))
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_TRNSTOCKOPNAME (cmpcode, stockopoid, periodacctg, stockopno, stockoptype, stockopdate, stockopstatus, createuser, createdate, upduser, updtime) VALUES ('" & CompnyCode & "', " & stockopoid.Text & ", '" & periodacctg.Text & "','" & stockno.Text & "', '" & ddlType.SelectedValue & "', '" & CDate(stockopdate.Text) & "', '" & stockstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & stockopoid.Text & " WHERE tablename='QL_TRNSTOCKOPNAME' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_TRNSTOCKOPNAME SET cmpcode='" & CompnyCode & "', periodacctg='" & periodacctg.Text & "', stockopno='" & stockno.Text & "', stockopdate='" & CDate(stockopdate.Text) & "', stockopstatus='" & stockstatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE stockopoid=" & stockopoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_TRNSTOCKOPNAMEDTL WHERE stockopoid=" & stockopoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("QL_trnStockOpnameDtl") Is Nothing Then
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        If objTable.Rows(C1).Item("isApply") = 1 Then
                            If ((ToDouble(objTable.Rows(C1).Item("Qty1").ToString) <> 0)) Or ((ToDouble(objTable.Rows(C1).Item("Qty2").ToString) <> 0)) Then
                                Dim sType As String = ""

                                Dim qty1 As Decimal = 0.0
                                Dim qty2 As Decimal = 0.0
                                Dim sysqty1 As Decimal = 0.0
                                Dim sysqty2 As Decimal = 0.0

                                If objTable.Rows(C1).Item("Qty1") > 0.0 Then
                                    qty1 = ToDouble(objTable.Rows(C1).Item("Qty1").ToString)
                                End If

                                If objTable.Rows(C1).Item("Qty2") > 0.0 Then
                                    qty2 = ToDouble(objTable.Rows(C1).Item("Qty2").ToString)
                                End If

                                If stockstatus.Text = "Post" Then
                                    sSql = "Select ISNULL(crd.saldoakhir,0.0) From QL_crdstock crd Where crd.refoid=" & objTable.Rows(C1).Item("itemoid") & " AND crd.cmpcode='" & CompnyCode & "' AND upper(crd.refname)='" & ddlType.SelectedItem.Text & "' AND crd.mtrlocoid=" & objTable.Rows(C1).Item("whoid") & "  AND crd.periodacctg='" & periodacctg.Text & "'"
                                    xCmd.CommandText = sSql
                                    sysqty1 = ToDouble(xCmd.ExecuteScalar)

                                    sSql = "Select ISNULL(crd.saldoakhir,0.0)*i.Unit3Unit1Conversion From QL_crdstock crd INNER JOIN QL_mstitem I on I.itemoid=crd.refoid Where crd.cmpcode='" & CompnyCode & "' AND upper(crd.refname)='" & ddlType.SelectedItem.Text & "' AND crd.mtrlocoid=" & objTable.Rows(C1).Item("whoid") & " AND crd.periodacctg='" & periodacctg.Text & "'"
                                    xCmd.CommandText = sSql
                                    sysqty2 = ToDouble(xCmd.ExecuteScalar)
                                End If

                                'stocksysqty2
                                dtloid = dtloid + 1

                                sSql = "INSERT INTO QL_TRNSTOCKOPNAMEDTL (cmpcode, stockopdtloid, stockopoid, periodacctg, stockopareaoid, stockopwarehouseoid, stockoplocationoid, stockoprefname, stockoprefoid, stockopqty1, stocksysqty1, stockopunit1, stockopqty2, stocksysqty2, stockopunit2, createuser, createdate, upduser, updtime) VALUES ('" & CompnyCode & "', " & dtloid & ", " & stockopoid.Text & ", '" & periodacctg.Text & "', 0, " & objTable.Rows(C1).Item("whoid") & ", 0, 'QL_MSTITEM', " & objTable.Rows(C1).Item("itemoid") & ", " & qty1 & ", " & sysqty1 & ", " & objTable.Rows(C1).Item("itemunitoid1") & ", " & qty2 & ", " & sysqty2 & ", " & objTable.Rows(C1).Item("itemunitoid2") & ", '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                            End If
                        End If
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & dtloid & " WHERE tablename='QL_TRNSTOCKOPNAMEDTL' AND cmpcode='" & CompnyCode & "' "
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                End If

                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    stockstatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.ToString, 1)
                conn.Close()
                stockstatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Stock Opname No. have been regenerated because being used by another data. Your new PR No. is " & stockopoid.Text & ".<BR>"
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trnstockopname.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnstockopname.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If stockopoid.Text.Trim = "" Then
            showMessage("Please select Stock Opname data first!", 2)
            Exit Sub
            'Else
            '    Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNSTOCKOPNAME", "stockopoid", stockopoid.Text, "prrawmststatus", updtime.Text, "Approved")
            '    If sStatusInfo <> "" Then
            '        showMessage(sStatusInfo, 2)
            '        stockstatus.Text = "In Process"
            '        Exit Sub
            '    End If
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE FROM QL_TRNSTOCKOPNAME WHERE stockopoid=" & stockopoid.Text
            xCmd.CommandText = sSql
            Dim res As Integer = xCmd.ExecuteNonQuery()
            If res > 0 Then
                sSql = "DELETE FROM QL_TRNSTOCKOPNAMEDTL WHERE stockopoid=" & stockopoid.Text
                xCmd.CommandText = sSql
                res = xCmd.ExecuteNonQuery()
            Else
                objTrans.Rollback() : conn.Close()
                showMessage("Delete failed !<br />Please check if Stock Opname is already deleted by another user .", 2)
                Exit Sub
            End If

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnstockopname.aspx?awal=true")
    End Sub

#End Region

    Protected Sub btnClearMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDetail()
    End Sub

    Protected Sub imbFindForm_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindDataMat(busunit.SelectedValue)
        gvStockOpname.Visible = True
    End Sub

    Private Sub FillDDLWarehouse()

    End Sub

    Private Sub FillDDLLocation()

    End Sub

    Public Sub BindDataMat(ByVal sCompCode As String)
        Dim sSql As String = ""
        Dim sWhere As String = ""
        Dim WHoid As String = ""
        Dim cat1 As String = ""
        Dim cat2 As String = ""
        Dim cat3 As String = ""
        Dim LocStock As String = ""
        periodacctg.Text = GetDateToPeriodAcctg(CDate(stockopdate.Text))

        If stockopwarehouseoid.SelectedValue <> "ALL" Then
            If stockopwarehouseoid.SelectedItem.Text <> "" Then
                WHoid &= " AND w.genoid= " & stockopwarehouseoid.SelectedValue & " AND w.gengroup='WAREHOUSE'"
            Else
                WHoid &= ""
            End If
        Else
            WHoid &= " AND w.gengroup='WAREHOUSE'"
        End If
        If Cat1CB.Checked Then
            cat1 &= " AND c1.cat1code='" & itemcat.SelectedValue & "'"
        End If
        If Cat2CB.Checked Then
            cat2 &= " AND c2.cat2code='" & ddlGenCat.SelectedValue & "'"
        End If
        If Cat3CB.Checked Then
            cat3 &= " AND c3.cat3code='" & ddlGenSubCat.SelectedValue & "'"
        End If
        If txtFilter.Text.Trim <> "" Then
            sWhere &= " AND " & ddlFilter.SelectedValue & " ='" & Tchar(txtFilter.Text) & "'"
        End If

        sSql = "SELECT 0 as dtlseq, 0 as isApply, i.cmpcode,w.genoid AS whoid ,w.gendesc AS warehouse,i.itemcode,i.itemoid,i.itemLongDescription itemlocaldesc, 0.0 Qty1, i.itemunit1 AS itemunitoid1, i.itemunit3 AS itemunitoid2,g1.gendesc unit1,s.saldoakhir LastQty, 0.0 Qty2 , g3.gendesc unit2 ,g2.gendesc unit FROM QL_mstitem i INNER JOIN QL_mstcat1 c1 ON c1.cat1oid=i.itemCat1 INNER JOIN QL_mstcat2 c2 ON c2.cat2oid=i.itemCat2 INNER JOIN QL_mstcat3 c3 ON c3.cat3oid=i.itemCat3 INNER JOIN QL_mstgen g1 ON g1.genoid=i.itemUnit1 INNER JOIN QL_mstgen g2 ON g2.genoid=i.itemUnit2 INNER JOIN QL_mstgen g3 ON g3.genoid=i.itemUnit3 INNER JOIN QL_crdstock s ON s.refoid=i.itemoid INNER JOIN QL_mstgen w ON w.genoid=s.mtrlocoid WHERE i.cmpcode='" & busunit.SelectedValue & "' " & sWhere & "" & WHoid & " " & cat1 & " " & cat2 & " " & cat3 & " AND i.itemgroup='" & ddlType.SelectedValue & "' AND NOT EXISTS (select stockoprefoid from QL_TRNSTOCKOPNAMEDTL odtl INNER JOIN QL_TRNSTOCKOPNAME omst on odtl.cmpcode=omst.cmpcode and odtl.stockopoid=omst.stockopoid where odtl.stockoprefoid=i.itemoid and odtl.stockoprefname='QL_mstitem' and odtl.stockopareaoid=g1.genoid and odtl.stockopwarehouseoid=w.genoid and odtl.stockoplocationoid=g3.genoid and omst.stockopdate='" & Date.ParseExact(stockopdate.Text, "MM/dd/yyyy", Nothing, Nothing) & "') order by i.itemLongDescription"

        Dim tbDtl As DataTable = cKon.ambiltabel(sSql, "QL_trnStockOpnameDtl")
        If tbDtl.Rows.Count > 0 Then
            For C1 As Integer = 0 To tbDtl.Rows.Count - 1
                tbDtl.Rows(C1)("dtlseq") = C1 + 1
            Next
            Session("QL_trnStockOpnameDtl") = tbDtl
            gvStockOpname.DataSource = Session("QL_trnStockOpnameDtl")
            gvStockOpname.DataBind()
        Else
            showMessage("Material data can't be found!", 2)
        End If
    End Sub

    Protected Sub btnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        stockstatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub gvStockOpname_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStockOpname.PageIndexChanging
        Dim dtTbl As DataTable = Session("QL_trnStockOpnameDtl")
        UpdateTblDtl(dtTbl)
        Session("QL_trnStockOpnameDtl") = dtTbl
        gvStockOpname.PageIndex = e.NewPageIndex
        gvStockOpname.DataSource = Session("QL_trnStockOpnameDtl")
        gvStockOpname.DataBind()
    End Sub

    Protected Sub gvStockOpname_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStockOpname.RowDataBound

    End Sub

    Protected Sub stockopwarehouseoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitFilterDDLCat1(ddlType.SelectedValue)
    End Sub

    Protected Sub stockoplocationoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub stockopdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        periodacctg.Text = GetDateToPeriodAcctg(CDate(stockopdate.Text))
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitFilterDDLCat1(ddlType.SelectedValue)
        Session("QL_trntockOpnameDtl") = Nothing
        gvStockOpname.DataSource = Session("QL_trnStockOpnameDtl")
        gvStockOpname.DataBind()
        WareHouseDDL()
        'If ddlType.SelectedValue.ToUpper = "QL_MSTITEM" Then
        '    'Label7.Visible = True
        '    'itemgroup.Visible = True
        '    'Label8.Visible = True
        '    'itemtype.Visible = True
        '    'Label10.Visible = True
        '    'itemclass.Visible = True
        '    'Label19.Visible = True
        '    'itemcat.Visible = True
        '    'Label11.Visible = False
        '    'ddlGenCat.Visible = False
        '    'Label13.Visible = False
        '    'ddlGenSubCat.Visible = False
        '    ddlFilter.Items.Clear()
        '    ddlFilter.Dispose()
        '    ddlFilter.Items.Insert(0, New ListItem("Code", "itemcode"))
        '    ddlFilter.Items.Insert(1, New ListItem("Material/Item", "itemlocaldesc"))
        '    Session("QL_trnStockOpnameDtl") = Nothing
        '    gvStockOpname.DataSource = Session("QL_trnStockOpnameDtl")
        '    gvStockOpname.DataBind()
        'ElseIf ddlType.SelectedValue.ToUpper = "QL_MSTSPAREPART" Then
        '    Label7.Visible = False
        '    itemgroup.Visible = False
        '    Label8.Visible = False
        '    itemtype.Visible = False
        '    Label10.Visible = False
        '    itemclass.Visible = False
        '    Label19.Visible = False
        '    itemcat.Visible = False
        '    Label11.Visible = False
        '    ddlGenCat.Visible = False
        '    Label13.Visible = False
        '    ddlGenSubCat.Visible = False
        '    ddlFilter.Items.Clear()
        '    ddlFilter.Dispose()
        '    ddlFilter.Items.Insert(0, New ListItem("Code", "sparepartcode"))
        '    ddlFilter.Items.Insert(1, New ListItem("Spare Part", "sparepartLongdesc"))
        '    Session("QL_trnStockOpnameDtl") = Nothing
        '    gvStockOpname.DataSource = Session("QL_trnStockOpnameDtl")
        '    gvStockOpname.DataBind()
        'ElseIf ddlType.SelectedValue.ToUpper = "QL_MSTMATGEN" Then
        '    Label7.Visible = False
        '    itemgroup.Visible = False
        '    Label8.Visible = False
        '    itemtype.Visible = False
        '    Label10.Visible = False
        '    itemclass.Visible = False
        '    Label19.Visible = False
        '    itemcat.Visible = False
        '    Label11.Visible = True
        '    ddlGenCat.Visible = True
        '    Label13.Visible = True
        '    ddlGenSubCat.Visible = True
        '    ddlFilter.Items.Clear()
        '    ddlFilter.Dispose()
        '    ddlFilter.Items.Insert(0, New ListItem("Code", "matgencode"))
        '    ddlFilter.Items.Insert(1, New ListItem("General", "matgenlongdesc"))
        'Session("QL_trntockOpnameDtl") = Nothing
        'gvStockOpname.DataSource = Session("QL_trnStockOpnameDtl")
        'gvStockOpname.DataBind()
        'End If
        'FillDDLWarehouse()
    End Sub

    Protected Sub itemcat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Cat1CB.Checked Then
            InitFilterDDLCat2(ddlType.SelectedValue, itemcat.SelectedValue)
        End If
    End Sub

    Protected Sub ddlGenCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Cat2CB.Checked Then
            InitFilterDDLCat3(ddlType.SelectedValue, itemcat.SelectedValue, ddlGenCat.SelectedValue)
        End If
    End Sub
End Class