Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports ClassProcedure
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Partial Class Transaction_trnFGR
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Procedure"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub PrintReport(ByVal oid As String)
        'untuk print
        Try

            report.Load(Server.MapPath(folderReport & "printFGR.rpt"))
            report.SetParameterValue("oid", oid)

            cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), _
                System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))


            report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize

            Response.Buffer = False

            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, oid & "_" & Format(GetServerTime(), "dd_MM_yy"))
            report.Close() : report.Dispose()
            Response.Redirect("~\Transaction\trnFGR.aspx")
        Catch ex As Exception
            showMessage(ex.ToString & ex.Message, 1)
        End Try

    End Sub

    Sub initDDL()
        initDDLLoc()
        sSql = "SELECT genoid, gendesc FROM QL_MSTGEN WHERE gengroup='UNIT' ORDER BY gendesc"
        FillDDL(unit1, sSql)
        'Init PIC
        sSql = "select distinct personoid,personname from ql_mstperson where activeflag='ACTIVE' order by personname"
        FillDDL(userid, sSql)
    End Sub

    Sub initDDLLoc()
        sSql = "SELECT genoid, gendesc FROM QL_MSTGEN WHERE gengroup='WAREHOUSE' ORDER BY gendesc"
        FillDDL(mtrloc, sSql)
        If mtrloc.Items.Count = 0 Then
            mtrloc.Items.Add("None")
            mtrloc.Items(0).Value = 0
        End If
    End Sub

    Private Sub FillTextBox(ByVal id As Int64)
        Try
            sSql = "SELECT resm.prodresmstoid, resm.prodresno, resm.prodresdate, p.personname suppname, resm.prodresmststatus, resm.prodresmstnote, suppoid,resm.createuser,resm.createtime,resm.upduser,resm.updtime FROM QL_trnprodresmst resm INNER JOIN QL_mstperson p on p.personoid=resm.suppoid and p.cmpcode=resm.cmpcode WHERE prodresmstoid=" & id
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                oid.Text = Trim(xreader("prodresmstoid").ToString)
                pritemexpdate.Text = Format(xreader("prodresdate"), "MM/dd/yyyy")
                bpbjno.Text = Trim(xreader("prodresno").ToString)
                userid.SelectedValue = Trim(xreader("suppoid").ToString)
                bpbjnote.Text = Trim(xreader("prodresmstnote").ToString)
                bpbjstatus.Text = Trim(xreader("prodresmststatus").ToString)
                createuser.Text = Trim(xreader("createuser").ToString)
                createtime.Text = Trim(xreader("createtime").ToString)
                upduser.Text = Trim(xreader("upduser").ToString)
                updtime.Text = Trim(xreader("updtime").ToString)
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False : btnPost.Visible = False
            Exit Sub
        End Try
        If bpbjstatus.Text = "Post" Or bpbjstatus.Text = "Closed" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnPost.Visible = False
        End If

        sSql = "SELECT prodresdtlseq,i.itemoid,i.itemlongdescription AS itemlocaldesc,prodresqty,g.gendesc unit1,prodresunitoid,prodresdtlnote,whoid,g2.gendesc loc, prodrestype, (CASE WHEN prodrestype='FG' THEN 'FINISH GOOD' ELSE 'WIP' END) typemat, prodresvalue AS value, prodresvalueusd AS valueusd, (prodresqty*prodresvalue) AS amount FROM QL_trnprodresdtl resd INNER JOIN QL_mstitem i on i.itemoid=resd.itemoid INNER JOIN QL_mstgen g ON g.genoid=resd.prodresunitoid INNER JOIN QL_mstgen g2 on g2.genoid=resd.whoid WHERE resd.prodresmstoid=" & id & " ORDER BY resd.prodresdtlseq"
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnprodresdtl")
        Session("TblDtl") = dtTbl
        tbldtl.DataSource = dtTbl
        tbldtl.DataBind()
        ClearDetail()
    End Sub

    Public Sub BindMatData(ByVal sqlPlus As String)
        sSql = "SELECT m.itemoid, m.itemcode, m.itemlongdescription AS itemlongdesc, m.roundqty AS itemlimitqty, m.itemunit1 AS itemunitoid, g.gendesc AS unit, 0.0 AS fgrqty, '' AS fgrdtlnote, 'False' AS checkvalue, '' AS prarrdatereq,m.itemunit3 unitoid,g.gendesc unitdesc," & mtrloc.SelectedValue & " AS whoid,'" & mtrloc.SelectedItem.Text & "' AS loc, itemcat1, itemcat2, itemcat3, itemcat4, 0.0 AS value, itemgroup AS prodrestype FROM QL_mstitem m INNER JOIN QL_mstgen g ON m.cmpcode=g.cmpcode AND m.itemunit1=g.genoid WHERE m.cmpcode='" & CompnyCode & "' AND m.itemrecordstatus='ACTIVE' AND itemgroup IN ('FG', 'WIP') ORDER BY m.itemcode"
        Session("TblMat") = cKon.ambiltabel(sSql, "QL_mstitem")
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT resm.prodresmstoid, resm.prodresno, CONVERT(VARCHAR(10), resm.prodresdate, 101) prodresdate, p.personname suppname, resm.prodresmststatus, resm.prodresmstnote, 'False' AS checkvalue FROM QL_trnprodresmst resm INNER JOIN QL_mstperson p on p.personoid=resm.suppoid and p.cmpcode=resm.cmpcode WHERE prodresmstres1='FGR-IN' "
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND resm.cmpcode='" & Session("CompnyCode") & "'"
        Else
            sSql &= " AND resm.cmpcode LIKE '%'"
        End If
        sSql &= sSqlPlus & " ORDER BY CONVERT(DATETIME, resm.prodresdate) DESC, resm.prodresmstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trnprodresmst")
        gvListOfTemp.DataSource = Session("TblMst")
        gvListOfTemp.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Sub GenerateNo(ByVal sPrefik As String)
        sSql = "SELECT   isnull(max(abs(replace(prodresno,'" & sPrefik & "',''))),0)+1  FROM QL_TRNPRODRESMST WHERE prodresno LIKE '" & sPrefik & "%'"
        bpbjno.Text = GenNumberString(sPrefik, "", GetStrData(sSql), DefaultFormatCounter)
    End Sub

    Private Sub InitFilterDDLCat1()
        'Fill DDL Category1
        sSql = "SELECT cat1oid, cat1code+' - '+cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat1res1 IN ('FG', 'WIP') Order By cat1code"
        FillDDL(FilterDDLCat01, sSql)
        If FilterDDLCat01.Items.Count > 0 Then
            InitFilterDDLCat2()
        Else
            FilterDDLCat02.Items.Clear()
            FilterDDLCat03.Items.Clear()
            FilterDDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitFilterDDLCat2()
        'Fill DDL Category2
        sSql = "SELECT cat2oid, cat2code+' - '+cat2shortdesc FROM QL_mstcat2 cat2 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat2.cmpcode AND cat1.cat1oid=cat2.cat1oid AND cat1.cat1oid='" & FilterDDLCat01.SelectedValue & "' WHERE cat2.cmpcode='" & CompnyCode & "' AND cat2.activeflag='ACTIVE' AND cat2res1 IN ('FG', 'WIP') Order By cat2code"
        FillDDL(FilterDDLCat02, sSql)
        If FilterDDLCat02.Items.Count > 0 Then
            InitFilterDDLCat3()
        Else
            FilterDDLCat03.Items.Clear()
            FilterDDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitFilterDDLCat3()
        'Fill DDL Category3
        sSql = "SELECT cat3oid, cat3code+' - '+cat3shortdesc FROM QL_mstcat3 cat3 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat3.cmpcode AND cat1.cat1oid=cat3.cat1oid AND cat1.cat1oid='" & FilterDDLCat01.SelectedValue & "' INNER JOIN QL_mstcat2 cat2 ON cat2.cmpcode=cat3.cmpcode AND cat2.cat2oid=cat3.cat2oid AND cat2.cat2oid='" & FilterDDLCat02.SelectedValue & "' WHERE cat3.cmpcode='" & CompnyCode & "' AND cat3.activeflag='ACTIVE' AND cat3res1 IN ('FG', 'WIP') Order By cat3code"
        FillDDL(FilterDDLCat03, sSql)
        If FilterDDLCat03.Items.Count > 0 Then
            InitFilterDDLCat4()
        Else
            FilterDDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitFilterDDLCat4()
        'Fill DDL Category4
        sSql = "SELECT genoid, gencode+' - '+gendesc FROM QL_mstgen WHERE gengroup='CAT4' AND activeflag='ACTIVE' ORDER BY gencode"
        FillDDL(FilterDDLCat04, sSql)
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("QL_pritemdtl")
        dtlTable.Columns.Add("prodresdtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemlocaldesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("prodresqty", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("unit1", Type.GetType("System.String"))
        dtlTable.Columns.Add("prodresunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("prodresdtlnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("whoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("loc", Type.GetType("System.String"))
        dtlTable.Columns.Add("prodrestype", Type.GetType("System.String"))
        dtlTable.Columns.Add("typemat", Type.GetType("System.String"))
        dtlTable.Columns.Add("value", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("amount", Type.GetType("System.Decimal"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub ClearDetail()
        seq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("TblDtl")
            seq.Text = objTable.Rows.Count + 1
        End If
        i_u4.Text = "New Detail"
        itemlocaldesc.Text = ""
        itemoid.Text = ""
        qty1.Text = ""
        panjang.Text = 0
        roll.Text = 0
        unit1.Items.Clear()
        dtlnote.Text = ""
        tbldtl.SelectedIndex = -1
        imbSearchItem.Visible = True
        value.Text = ""
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListOfTemp.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListOfTemp.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvListOfTemp.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "prodresmstoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub PrintReport()
        Try
            report.Load(Server.MapPath(folderReport & "rptFGR_IN.rpt"))
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("prodresmstoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            If sOid <> "" Then
                sOid = Left(sOid, sOid.Length - 1)
                sSql &= " AND prm.prodresmstoid IN (" & sOid & ")"
            End If

            report.SetParameterValue("sWhere", sSql)
            report.SetParameterValue("sUserID", Session("userID"))
            report.PrintOptions.PaperSize = PaperSize.PaperFolio
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "FinishGoodReceivedNonKIK")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            report.Close()
            report.Dispose()
        End Try
        Response.Redirect("~\Transaction\trnFGR.aspx?awal=true")
    End Sub
#End Region

#Region "Function"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnFGR.aspx")
        End If
        'If checkPagePermission("~\Transaction\trnFGR.aspx", Session("Role")) = False Then
        '    Response.Redirect("~\Other\NotAuthorize.aspx")
        'End If
        Page.Title = CompnyName & " - FG Received Non KIK"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPost.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to post this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            Me.FilterPeriod1.Text = Format(New Date(Now.Year, Now.Month, 1), "MM/dd/yyyy")
            Me.FilterPeriod2.Text = Format(Now, "MM/dd/yyyy")
            initDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                pritemexpdate.Text = Format(Now, "MM/dd/yyyy")
                Me.bpbjno.Text = GetStrData("SELECT ISNULL(MAX(abs(prodresno)),0) + 1 FROM QL_TRNPRODRESMST WHERE(cmpcode LIKE '%" & CompnyCode & "%') AND periodacctg='" & GetActivePeriode() & "' and prodresmststatus in ('In Process', 'In Approval' ) ")
                bpbjstatus.Text = "In Process"
                btnDelete.Visible = False
                btnPrint.Visible = True
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            tbldtl.DataSource = dt
            tbldtl.DataBind()
        End If
    End Sub

    Protected Sub imbSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSearchItem.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        InitFilterDDLCat1()
        cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
        Session("TblMat") = Nothing : Session("TblMatView") = Nothing : gvListMat.DataSource = Nothing : gvListMat.DataBind() : tbData.Text = "5" : gvListMat.PageSize = CInt(tbData.Text)
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Protected Sub imbEraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseItem.Click

    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblMat") Is Nothing Then
            BindMatData("")
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        If cbCat01.Checked Then
            If FilterDDLCat01.SelectedValue <> "" Then
                sPlus &= " AND itemcat1='" & FilterDDLCat01.SelectedValue & "'"
            Else
                cbCat01.Checked = False
            End If
        End If
        If cbCat02.Checked Then
            If FilterDDLCat02.SelectedValue <> "" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND itemcat1='" & FilterDDLCat01.SelectedValue & "'") & " AND itemcat2='" & FilterDDLCat02.SelectedValue & "'"
            Else
                cbCat02.Checked = False
            End If
        End If
        If cbCat03.Checked Then
            If FilterDDLCat03.SelectedValue <> "" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND itemcat1='" & FilterDDLCat01.SelectedValue & "'") & IIf(cbCat02.Checked, "", " AND itemcat2='" & FilterDDLCat02.SelectedValue & "'") & " AND itemcat3='" & FilterDDLCat03.SelectedValue & "'"
            Else
                cbCat03.Checked = False
            End If
        End If
        If cbCat04.Checked Then
            If FilterDDLCat04.SelectedValue <> "" Then
                sPlus &= IIf(cbCat01.Checked, "", " AND itemcat1='" & FilterDDLCat01.SelectedValue & "'") & IIf(cbCat02.Checked, "", " AND itemcat2='" & FilterDDLCat02.SelectedValue & "'") & IIf(cbCat03.Checked, "", " AND itemcat3='" & FilterDDLCat03.SelectedValue & "'") & " AND itemcat4='" & FilterDDLCat04.SelectedValue & "'"
            Else
                cbCat04.Checked = False
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        InitFilterDDLCat1()
        cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
        If Session("TblMat") Is Nothing Then
            BindMatData("")
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "False"
                    objView(0)("fgrqty") = 0
                    objView(0)("fgrdtlnote") = ""
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    dtTbl.Rows(C1)("fgrqty") = 0
                    dtTbl.Rows(C1)("fgrdtlnote") = ""
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                InitFilterDDLCat1()
                cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Selected material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("fgrqty") = ToDouble(GetTextBoxValue(row.Cells(3).Controls))
                                    dtView(0)("value") = ToDouble(GetTextBoxValue(row.Cells(5).Controls))
                                    dtView(0)("fgrdtlnote") = GetTextBoxValue(row.Cells(6).Controls)
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                dtView2.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("fgrqty") = ToDouble(GetTextBoxValue(row.Cells(3).Controls))
                                    dtView(0)("fgrdtlnote") = GetTextBoxValue(row.Cells(5).Controls)
                                    dtView(0)("value") = ToDouble(GetTextBoxValue(row.Cells(6).Controls))
                                    If dtView2.Count > 0 Then
                                        dtView2(0)("fgrqty") = ToDouble(GetTextBoxValue(row.Cells(3).Controls))
                                        dtView2(0)("fgrdtlnote") = GetTextBoxValue(row.Cells(5).Controls)
                                        dtView(0)("value") = ToDouble(GetTextBoxValue(row.Cells(6).Controls))
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Protected Sub imbAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbAddToList.Click
        If mtrloc.SelectedValue = 0 Then
            showMessage("Please choose Material Location!", 2)
            Exit Sub
        End If
        If itemoid.Text = "" Then
            showMessage("Please choose Item!", 2)
            Exit Sub
        End If
        If Tchar(dtlnote.Text).Length > 50 Then
            showMessage("Detail Note Can't more than 50 character", 2)
            Exit Sub
        End If
        If ToDouble(qty1.Text) = 0 Then
            showMessage("quantity 1 must > 0 ", 2)
            Exit Sub
        End If
        If ToDouble(value.Text) = 0 Then
            showMessage("Value Must > 0 ", 2)
            Exit Sub
        End If

        If ToDouble(panjang.Text) > 0 Then
            Dim tempTotal As Decimal = ToDouble(qty1.Text) / ToDouble(panjang.Text)
            If ToDouble(tempTotal) - CInt(tempTotal) <> 0 Then
                'sError &= "- Item FG " & itemlocaldesc.Text & " rounded tidak bulat <BR>"
                showMessage("- Item FG " & itemlocaldesc.Text & " rounded tidak bulat <BR>", 2)
                Exit Sub
            End If
        End If

        If Session("TblDtl") Is Nothing Then
            CreateTblDetail()
        End If

        Dim objTable As DataTable
        objTable = Session("TblDtl")
        'Cek apa sudah ada item yang sama dalam Tabel Detail
        Dim dv As DataView = objTable.DefaultView
        If i_u4.Text = "New Detail" Then
            dv.RowFilter = "itemoid = " & itemoid.Text & " AND whoid=" & mtrloc.SelectedValue & ""
        Else
            dv.RowFilter = "itemoid = " & itemoid.Text & " AND whoid=" & mtrloc.SelectedValue & " AND prodresdtlseq<>" & seq.Text
        End If
        If dv.Count > 0 Then
            dv.RowFilter = ""
            showMessage("Data has been added !!", 2)
            Exit Sub
        End If

        dv.RowFilter = ""

        'insert/update to list data
        Dim objRow As DataRow
        If i_u4.Text = "New Detail" Then
            objRow = objTable.NewRow()
            objRow("prodresdtlseq") = objTable.Rows.Count + 1
        Else 'update
            objRow = objTable.Rows(seq.Text - 1)
            objRow.BeginEdit()
        End If
        objRow("itemoid") = itemoid.Text
        objRow("itemlocaldesc") = itemlocaldesc.Text
        objRow("prodresqty") = ToDouble(qty1.Text)
        objRow("unit1") = unit1.SelectedItem.Text
        objRow("prodresunitoid") = unit1.SelectedValue
        objRow("prodresdtlnote") = dtlnote.Text
        objRow("whoid") = mtrloc.SelectedValue
        objRow("loc") = mtrloc.SelectedItem.Text
        objRow("value") = ToDouble(value.Text)
        objRow("amount") = ToDouble(value.Text) * ToDouble(qty1.Text)
        If i_u4.Text = "New Detail" Then
            objTable.Rows.Add(objRow)
            'update
        Else
            objRow.EndEdit()
        End If
        Session("TblDtl") = objTable
        tbldtl.Visible = True
        tbldtl.DataSource = objTable
        tbldtl.DataBind()
        seq.Text = objTable.Rows.Count + 1
        tbldtl.SelectedIndex = -1
        ClearDetail()
        i_u4.Text = "New Detail"
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If bpbjstatus.Text = "" Then
            bpbjstatus.Text = "In Process"
        End If

        If Tchar(bpbjnote.Text).Length > 50 Then
            showMessage("Note can't have more than 50 Char", 2)
            Exit Sub
        End If
        If Not IsValidDate(pritemexpdate.Text, "MM/dd/yyyy", "") Then
            showMessage("Invalid  Date !", 2)
            bpbjstatus.Text = "In Process"
            Exit Sub
        End If

        If CheckYearIsValid((pritemexpdate.Text)) Then
            showMessage("FGR Date Year can't Less than 2000 and can't More than 2072 <br />", CompnyName & " - WARNING")
            Exit Sub
        End If
        If Session("TblDtl") Is Nothing Then
            showMessage("Can not save data!<br>No Detail Data !!", 2)
            bpbjstatus.Text = "In Process"
            Exit Sub
        Else
            Dim objTableCek As DataTable = Session("TblDtl")
            If objTableCek.Rows.Count = 0 Then
                showMessage("Can not save data!<br>No Detail Data !!", 2)
                bpbjstatus.Text = "In Process"
                Exit Sub
            End If
        End If

        'checking Current Last No to prevent redundancy
        If Session("oid") = Nothing Or Session("oid") = "" Then
            If bpbjstatus.Text = "In Process" Then
                Me.bpbjno.Text = GetStrData("SELECT ISNULL(MAX(abs(prodresno)),0) + 1 FROM QL_TRNPRODRESMST WHERE(cmpcode LIKE '%" & CompnyCode & "%') AND periodacctg='" & GetActivePeriode() & "' and prodresmststatus in ('In Process', 'In Approval' ) ")
            Else
                Dim sPrefik As String = "FGRIN-" & Format(GetServerTime(), "yyMM") & "-"
                GenerateNo(sPrefik)
            End If
        Else
        End If
        If bpbjstatus.Text = "Post" Then
            Dim sPrefik As String = "FGR-IN/" & Format(GetServerTime(), "yyMM")
            GenerateNo(sPrefik)
        End If
        Dim bpbjdtloid As Int64 = GenerateID("QL_TRNPRODRESDTL", CompnyCode)
        Dim iConMatOid As Integer = GenerateID("QL_CONSTOCK", CompnyCode)
        Dim iCrdMtrOid As Integer = GenerateID("QL_CRDSTOCK", CompnyCode)
        Dim iCrdHpp As Integer = GenerateID("QL_CRDHPP", CompnyCode)
        Dim iGlMstOid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
        Dim iGlDtlOid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
        Dim iStockValOid As Integer = GenerateID("QL_STOCKVALUE", CompnyCode)
        Dim sDate As String = Format(CDate(pritemexpdate.Text), "MM/dd/yyyy")
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
        Dim cRate As New ClassRate

        If bpbjstatus.Text = "Post" Then
            cRate.SetRateValue(1, sDate)
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, 2)
                bpbjstatus.Text = "In Process"
                Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, 2)
                bpbjstatus.Text = "In Process"
                Exit Sub
            End If
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            oid.Text = GenerateID("QL_TRNPRODRESMST", CompnyCode)
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "INSERT INTO QL_TRNPRODRESMST (cmpcode, prodresmstoid, periodacctg, prodresdate, prodresno, deptoid, suppoid, prodresmstnote, prodresmststatus, createuser, createtime, upduser, updtime, deptreplaceoid,prodresmstres1) VALUES ('" & CompnyCode & "', " & oid.Text & ",'" & GetActivePeriode() & "','" & pritemexpdate.Text & "', '" & bpbjno.Text & "',0," & userid.SelectedValue & ",'" & Tchar(bpbjnote.Text) & "','" & Tchar(bpbjstatus.Text) & "','" & Session("userId") & "',CURRENT_TIMESTAMP,'" & Session("userId") & "',CURRENT_TIMESTAMP,0,'FGR-IN')"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & oid.Text & " WHERE tablename ='QL_TRNPRODRESMST' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

            Else 'update
                sSql = "UPDATE QL_TRNPRODRESMST SET periodacctg='" & GetActivePeriode() & "', prodresdate='" & pritemexpdate.Text & "', prodresno='" & bpbjno.Text & "', deptoid=0, suppoid=" & userid.SelectedValue & ", prodresmstnote='" & Tchar(bpbjnote.Text) & "', prodresmststatus='" & Tchar(bpbjstatus.Text) & "', upduser='" & Session("userId") & "', updtime=CURRENT_TIMESTAMP, deptreplaceoid=0 WHERE prodresmstoid=" & oid.Text
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                sSql = "DELETE QL_TRNPRODRESDTL where prodresmstoid=" & oid.Text
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If

            If Not Session("TblDtl") Is Nothing Then
                Dim objTable As DataTable = Session("TblDtl")
                For C1 As Int16 = 0 To objTable.Rows.Count - 1
                    sSql = "INSERT INTO QL_TRNPRODRESDTL (cmpcode, prodresdtloid, prodresmstoid, prodresdtlseq, womstoid, wodtl2oid, prodresprocseq, todeptoid, prodrestype, prodresqty, prodresunitoid, prodresdtlstatus, prodresdtlnote, upduser, updtime, prodresdlcvalue, prodresdlcamt, prodresohdvalue, prodresohdamt, prodresdlcvalueusd, prodresdlcamtusd, prodresohdvalueusd, prodresohdamtusd,itemoid,whoid, prodresvalue, prodresvalueusd) VALUES ('" & CompnyCode & "', " & (C1 + CInt(bpbjdtloid)) & ", " & oid.Text & ", " & C1 + 1 & ",0,0, " & objTable.Rows(C1).Item("prodresdtlseq") & ", 0, '" & Tchar(objTable.Rows(C1).Item("prodrestype").ToString) & "', " & ToDouble(objTable.Rows(C1).Item("prodresqty").ToString) & ", " & objTable.Rows(C1).Item("prodresunitoid") & ", '', '" & Tchar(objTable.Rows(C1).Item("prodresdtlnote").ToString) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP,0,0,0,0,0,0,0,0," & ToDouble(objTable.Rows(C1).Item("itemoid").ToString) & "," & ToDouble(objTable.Rows(C1).Item("whoid").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("value").ToString) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(objTable.Rows(C1).Item("value").ToString) * cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    bpbjdtloid += 1

                    If bpbjstatus.Text = "Post" Then
                        ' Insert Into QL_conmat
                        sSql = "INSERT INTO QL_constock (cmpcode, constockoid, contype, typemin, trndate, periodacctg, formaction, formoid, refoid, refname, mtrlocoid, qtyin, qtyout, reason, note, upduser, updtime, refno, valueidr, valueusd) VALUES ('" & CompnyCode & "', " & iConMatOid & ", 'FGR-IN', 1, '" & sDate & "', '" & sPeriod & "', 'QL_TRNPRODRESMST', " & oid.Text & "," & objTable.Rows(C1).Item("itemoid").ToString & ", '" & Tchar(objTable.Rows(C1).Item("typemat").ToString) & "'," & ToDouble(objTable.Rows(C1).Item("whoid").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("prodresqty").ToString) & ", 0, 'FGR-IN', 'FGR-IN : " & bpbjno.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & bpbjno.Text & "', " & ToDouble(objTable.Rows(C1).Item("value").ToString) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(objTable.Rows(C1).Item("value").ToString) * cRate.GetRateMonthlyUSDValue & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iConMatOid += 1
                        ' Insert Into QL_crdmtr
                        sSql = "UPDATE QL_crdstock SET qtyin = qtyin + " & ToDouble(objTable.Rows(C1).Item("prodresqty").ToString) & ", saldoakhir = saldoakhir + " & ToDouble(objTable.Rows(C1).Item("prodresqty").ToString) & ", lasttranstype='QL_TRNPRODRESMST', lasttransdate='" & sDate & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & CompnyCode & "' AND refoid=" & objTable.Rows(C1).Item("itemoid").ToString & " AND refname='" & Tchar(objTable.Rows(C1).Item("typemat").ToString) & "' AND mtrlocoid=" & objTable.Rows(C1).Item("whoid").ToString & " AND periodacctg='" & sPeriod & "' "
                        xCmd.CommandText = sSql
                        If xCmd.ExecuteNonQuery() = 0 Then
                            sSql = "INSERT INTO QL_crdstock (cmpcode, crdstockoid, periodacctg, refoid, refname, mtrlocoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir, lasttranstype, lasttransdate, upduser, updtime, createuser, createtime, closeuser, closingdate) VALUES ('" & CompnyCode & "', " & iCrdMtrOid & ", '" & sPeriod & "', " & objTable.Rows(C1).Item("itemoid").ToString & ", '" & Tchar(objTable.Rows(C1).Item("typemat").ToString) & "', " & objTable.Rows(C1).Item("whoid").ToString & ", " & ToDouble(objTable.Rows(C1).Item("prodresqty").ToString) & ", 0, 0, 0, 0, " & ToDouble(objTable.Rows(C1).Item("prodresqty").ToString) & ", 'QL_TRNPRODRESMST', '" & pritemexpdate.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', '1/1/1900')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iCrdMtrOid += 1
                        End If

                        ' Update QL_stockvalue
                        sSql = "UPDATE QL_stockvalue SET stockqty=stockqty + " & ToDouble(objTable.DefaultView(C1)("prodresqty").ToString) & ", stockvalueidr=ISNULL((((stockvalueidr * stockqty) + " & (ToDouble(objTable.Rows(C1).Item("value").ToString) * ToDouble(objTable.Rows(C1).Item("prodresqty").ToString)) * cRate.GetRateMonthlyIDRValue & ") / NULLIF((stockqty + " & ToDouble(objTable.DefaultView(C1)("prodresqty").ToString) & "), 0)), 0), stockvalueusd=ISNULL((((stockvalueusd * stockqty) + " & (ToDouble(objTable.Rows(C1).Item("value").ToString) * ToDouble(objTable.Rows(C1).Item("prodresqty").ToString)) * cRate.GetRateMonthlyUSDValue & ") / NULLIF((stockqty + " & ToDouble(objTable.DefaultView(C1)("prodresqty").ToString) & "), 0)), 0), lasttranstype='QL_trnprodresmst', lasttransdate='" & sDate & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & CompnyCode & "' AND periodacctg='" & sPeriod & "' AND refoid=" & objTable.DefaultView(C1)("itemoid") & ""
                        xCmd.CommandText = sSql
                        If xCmd.ExecuteNonQuery() = 0 Then
                            ' Insert QL_stockvalue
                            sSql = "INSERT INTO QL_stockvalue (cmpcode, stockvalueoid, periodacctg, refoid, stockqty, stockvalueidr, stockvalueusd, lasttranstype, lasttransdate, note, upduser, updtime) VALUES ('" & CompnyCode & "', " & iStockValOid & ", '" & sPeriod & "', " & objTable.DefaultView(C1)("itemoid") & ", " & ToDouble(objTable.DefaultView(C1)("prodresqty").ToString) & ", " & (ToDouble(objTable.Rows(C1).Item("value").ToString) * ToDouble(objTable.Rows(C1).Item("prodresqty").ToString)) * cRate.GetRateMonthlyIDRValue & ", " & (ToDouble(objTable.Rows(C1).Item("value").ToString) * ToDouble(objTable.Rows(C1).Item("prodresqty").ToString)) * cRate.GetRateMonthlyUSDValue & ", 'QL_trnprodresmst', '" & sDate & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iStockValOid += 1
                        End If
                    End If
                Next

                'Insert Jurnal Accounting
                '===========================================
                'Persediaan         10000
                'Hpp                    10000
                '===========================================
                If bpbjstatus.Text = "Post" Then
                    '' Insert GL MST
                    'sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlMstOid & ", '" & sDate & "', '" & sPeriod & "', 'LPB |No. " & mrrawno.Text & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                    'xCmd.CommandText = sSql
                    'xCmd.ExecuteNonQuery()
                    '' Insert GL DTL
                    'Dim dMRAmtIDR As Double = (ToDouble(totalMR) * cRate.GetRateMonthlyIDRValue)
                    'Dim dMRAmtUSD As Double = (ToDouble(totalMR) * cRate.GetRateMonthlyUSDValue)

                    'sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iStockAcctgOid & ", 'D', " & totalMR & ", '" & mrrawno.Text & "', 'LPB |No. " & mrrawno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dMRAmtIDR & ", " & dMRAmtUSD & ", 'QL_trnmrmst " & mrrawmstoid.Text & "')"
                    'xCmd.CommandText = sSql
                    'xCmd.ExecuteNonQuery()
                    'iGlDtlOid += 1
                    'iSeq += 1

                    'sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iRecAcctgOid & ", 'C', " & totalMR & ", '" & mrrawno.Text & "', 'LPB |No. " & mrrawno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dMRAmtIDR & ", " & dMRAmtUSD & ", 'QL_trnmrmst " & mrrawmstoid.Text & "')"
                    'xCmd.CommandText = sSql
                    'xCmd.ExecuteNonQuery()
                    'iGlDtlOid += 1
                    'iSeq += 1
                    'iGlMstOid += 1
                End If

                sSql = "UPDATE  QL_mstoid SET lastoid=" & bpbjdtloid & " WHERE tablename = 'QL_TRNPRODRESDTL' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & iConMatOid - 1 & " WHERE tablename='QL_CONSTOCK' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & iCrdMtrOid - 1 & " WHERE tablename='QL_CRDSTOCK' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & iCrdMtrOid - 1 & " WHERE tablename='QL_CRDHPP' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            showMessage(ex.ToString & sSql, 2)
            xCmd.Connection.Close()
            bpbjstatus.Text = "In Process"
            Exit Sub
        End Try

        Session("TblDtl") = Nothing
        If Session("oid") = Nothing Or Session("oid") = "" Then
            Response.Redirect("~\Transaction\trnFGR.aspx?awal=true")
        Else
            Response.Redirect("~\Transaction\trnFGR.aspx")
        End If

    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPost.Click
        bpbjstatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnFGR.aspx?awal=true")
    End Sub

    Protected Sub tbldtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles tbldtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
        End If
    End Sub

    Protected Sub tbldtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbldtl.SelectedIndexChanged
        Try
            If unit1.SelectedValue <> "" Then
                unit1.Items.Clear()
            End If
            seq.Text = tbldtl.SelectedDataKey.Item("prodresdtlseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u4.Text = "Update Detail"
                Dim objTable As DataTable
                objTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "prodresdtlseq=" & seq.Text
                itemlocaldesc.Text = dv.Item(0).Item("itemlocaldesc").ToString
                itemoid.Text = dv.Item(0).Item("itemoid").ToString
                qty1.Text = NewMaskEdit(ToDouble(dv.Item(0).Item("prodresqty").ToString))
                dtlnote.Text = dv.Item(0).Item("prodresdtlnote").ToString
                mtrloc.SelectedValue = dv.Item(0).Item("whoid").ToString
                'fill ddl unit
                sSql = "select genoid,gendesc from QL_mstgen g inner join QL_mstitem m on m.itemunitoid = g.genoid where m.itemoid='" & dv.Item(0).Item("itemoid").ToString & "' UNION select genoid,gendesc from QL_mstgen g inner join QL_mstitem m on m.itemunit2oid = g.genoid where m.itemoid='" & dv.Item(0).Item("itemoid").ToString & "' "
                FillDDL(unit1, sSql)
                unit1.SelectedValue = dv.Item(0).Item("prodresunitoid").ToString
              
                dv.RowFilter = ""
            End If
            imbSearchItem.Visible = False
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub tbldtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles tbldtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("prodresdtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        tbldtl.DataSource = objTable
        tbldtl.DataBind()
        ClearDetail()
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If oid.Text.Trim = "" Then
            showMessage("FGR ID can't be empty!", CompnyName & " - WARNING")
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "Delete QL_trnprodresdtl where prodresmstoid = " & Session("oid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "Delete from QL_trnprodresmst where prodresmstoid = " & Session("oid")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback() : xCmd.Connection.Close()
            showMessage(ex.Message, CompnyName & " - Error") : Exit Sub
        End Try

        Session("tbldtl") = Nothing
        Response.Redirect("~\transaction\trnFGR.aspx?awal=true")
    End Sub

    Protected Sub imbClearDtl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClearDtl.Click
        ClearDetail()
    End Sub

    Protected Sub lbAddListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddListMat.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim dtView2 As DataView = dtTbl2.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            Dim iCheck As Integer = dtView.Count
            Dim iCheck2 As Integer = dtView2.Count

            'cek apakah ada item yg mengandung panjang tetapi jumlah qty1 tidak bulat
            'If iCheck2 > 0 Then
            '    dtView2.RowFilter = ""
            '    dtView2.RowFilter = "checkvalue='True' AND panjang>0"
            '    Dim sWarn As String = ""
            '    Dim tempTotal As Decimal = 0.0
            '    If dtView2.Count > 0 Then
            '        For c1 As Int64 = 0 To dtView2.Count - 1
            '            tempTotal = ToDouble(dtView2(c1)("fgrqty").ToString) / ToDouble(dtView2(c1)("panjang").ToString)
            '            If tempTotal - CInt(tempTotal) <> 0 Then
            '                sWarn = sWarn & "(*) " & dtView2(c1)("itemlongdesc").ToString & " --> Panjang (" & NewMaskEdit(ToDouble(dtView2(c1)("panjang").ToString)) & "),Qty (" & NewMaskEdit(ToDouble(dtView2(c1)("fgrqty").ToString)) & " " & dtView2(c1)("unit").ToString & ")</BR>"
            '            End If
            '        Next
            '        If sWarn.Trim <> "" Then
            '            'sWarn = sWarn.Remove(0, 1)
            '            Session("WarningListMat") = "Item FG dibawah ini mempunyai rounding yg tidak bulat : </BR></BR>" & sWarn
            '            showMessage(Session("WarningListMat"), 2)
            '            dtView2.RowFilter = ""
            '            Exit Sub
            '        End If
            '    End If
            'End If

            If iCheck > 0 Then
                dtView.RowFilter = ""
                dtView.RowFilter = "checkvalue='True' AND fgrqty>0"
                If dtView.Count <> iCheck Then
                    Session("WarningListMat") = "FG Received qty for every checked material data must be more than 0!"
                    showMessage(Session("WarningListMat"), 2)
                    dtView.RowFilter = ""
                    Exit Sub
                End If
                dtView.RowFilter = ""
                dtView.RowFilter = "checkvalue='True' AND value>0"
                If dtView.Count <> iCheck Then
                    Session("WarningListMat") = "FG Received Value for every checked material data must be more than 0!"
                    showMessage(Session("WarningListMat"), 2)
                    dtView.RowFilter = ""
                    Exit Sub
                End If
                Dim sErr = ""
                If Session("TblDtl") Is Nothing Then
                    CreateTblDetail()
                End If
                Dim objTable As DataTable
                objTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                Dim objRow As DataRow
                Dim counter As Integer = objTable.Rows.Count
                For C1 As Integer = 0 To dtView.Count - 1
                    dv.RowFilter = "itemoid=" & dtView(C1)("itemoid") & " AND whoid=" & mtrloc.SelectedValue & " "
                    If dv.Count > 0 Then
                        dv.AllowEdit = True
                        dv(0)("prodresqty") = dtView(C1)("fgrqty")
                        dv(0)("prodresdtlnote") = dtView(C1)("fgrdtlnote")
                        dv(0)("value") = dtView(C1)("value")
                        dv(0)("amount") = dtView(C1)("value") * dtView(C1)("fgrqty")
                        dv(0)("prodrestype") = dtView(C1)("prodrestype").ToString
                        dv(0)("typemat") = IIf(dtView(C1)("prodrestype").ToString.ToUpper = "FG", "FINISH GOOD", "WIP")
                    Else
                        counter += 1
                        objRow = objTable.NewRow()
                        objRow("prodresdtlseq") = counter
                        objRow("itemoid") = dtView(C1)("itemoid")
                        objRow("prodresqty") = dtView(C1)("fgrqty")
                        objRow("prodresunitoid") = dtView(C1)("unitoid")
                        objRow("prodresdtlnote") = dtView(C1)("fgrdtlnote")
                        objRow("loc") = dtView(C1)("loc").ToString
                        objRow("itemlocaldesc") = dtView(C1)("itemlongdesc").ToString
                        objRow("prodrestype") = dtView(C1)("prodrestype").ToString
                        objRow("typemat") = IIf(dtView(C1)("prodrestype").ToString.ToUpper = "FG", "FINISH GOOD", "WIP")
                        objRow("whoid") = dtView(C1)("whoid")
                        objRow("unit1") = dtView(C1)("unit").ToString
                        objRow("value") = dtView(C1)("value")
                        objRow("amount") = dtView(C1)("value") * dtView(C1)("fgrqty")
                        objTable.Rows.Add(objRow)
                    End If
                    dv.RowFilter = ""
                Next
                dtView.RowFilter = ""
                Session("TblDtl") = objTable
                tbldtl.DataSource = objTable
                tbldtl.DataBind()
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                ClearDetail()
            Else
                Session("WarningListMat") = "Please select material to add to list!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        End If
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(3).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    End If
                End If
            Next

            Dim cc2 As System.Web.UI.ControlCollection = e.Row.Cells(5).Controls
            For Each myControl As System.Web.UI.Control In cc2
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnFGR.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If Session("CompnyCode") <> CompnyCode Then
            sSqlPlus &= " AND resm.cmpcode='" & Session("CompnyCode") & "' "
        End If
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND resm.prodresdate>='" & FilterPeriod1.Text & " 00:00:00' AND resm.prodresdate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If

        If DDLStatus.SelectedValue <> "All" Then
            sSqlPlus &= " AND resm.prodresmststatus='" & DDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Transaction\trnFGR.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND resm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = ""
        FilterDDL.SelectedIndex = 0
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        DDLStatus.SelectedIndex = -1
        If Session("CompnyCode") <> CompnyCode Then
            sSqlPlus &= " AND resm.cmpcode='" & Session("CompnyCode") & "' "
        End If
        If checkPagePermission("~\Transaction\trnFGR.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND resm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        UpdateCheckedValue()
        PrintReport()
    End Sub

    Protected Sub FilterDDLCat01_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat01.SelectedIndexChanged
        'btnFindListMat_Click(Nothing, Nothing)
        InitFilterDDLCat2()
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat02_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat02.SelectedIndexChanged
        InitFilterDDLCat3()
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat03_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat03.SelectedIndexChanged
        InitFilterDDLCat4()
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat04_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat04.SelectedIndexChanged

    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub lbShowData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbShowData.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Please find material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If tbData.Text = "" Then
            Session("WarningListMat") = "Please fill total data to be shown!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        Else
            If ToDouble(tbData.Text) <= 0 Then
                Session("WarningListMat") = "Total data to be shown must be more than 0!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat2() Then
            gvListMat.PageSize = Int(tbData.Text)
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListOfTemp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListOfTemp.PageIndexChanging
        UpdateCheckedValue()
        gvListOfTemp.PageIndex = e.NewPageIndex
        gvListOfTemp.DataSource = Session("TblMst")
        gvListOfTemp.DataBind()
    End Sub
#End Region


End Class
