Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Structure VarUsageValue
    Public ValRaw_IDR As Double
    Public ValRaw_USD As Double
    Public ValST_IDR As Double
    Public ValST_USD As Double
End Structure

Partial Class Transaction_MaterialReturnNonKIK
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cRate As New ClassRate
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matusagedtloid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("matretqty") = ToDouble(GetTextBoxValue(row.Cells(4).Controls))
                                    dtView(0)("matretdtlnote") = GetTextBoxValue(row.Cells(6).Controls)
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matusagedtloid=" & cbOid
                                dtView2.RowFilter = "matusagedtloid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("matretqty") = ToDouble(GetTextBoxValue(row.Cells(4).Controls))
                                    dtView(0)("matretdtlnote") = GetTextBoxValue(row.Cells(6).Controls)
                                    If dtView2.Count > 0 Then
                                        dtView2(0)("matretqty") = ToDouble(GetTextBoxValue(row.Cells(4).Controls))
                                        dtView2(0)("matretdtlnote") = GetTextBoxValue(row.Cells(6).Controls)
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If matusagemstoid.Text = "" Then
            sError &= "- Please select RETURN NO. field!<BR>"
        End If
        If matusagedtloid.Text = "" Then
            sError &= "- Please select MATERIAL field!<BR>"
        End If
        If matretqty.Text = "" Then
            sError &= "- Please fill RETURN QTY field!<BR>"
        Else
            If ToDouble(matretqty.Text) <= 0 Then
                sError &= "- RETURN QTY field must be more than 0!<BR>"
            Else
                If ToDouble(matretqty.Text) > ToDouble(matusageqty.Text) Then
                    sError &= "- RETURN QTY field must be less than USAGE QTY!<BR>"
                End If
            End If
        End If
        If matretunitoid.SelectedValue = "" Then
            sError &= "- Please select UNIT field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If matretdate.Text = "" Then
            sErr = "None"
            sError &= "- Please fill RETURN DATE field!<BR>"
        Else
            If Not IsValidDate(matretdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- RETURN DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If matretwhoid.SelectedValue = "" Then
            sError &= "- Please select WAREHOUSE field!<BR>"
        End If
        If deptoid.SelectedValue = "" Then
            sError &= "- Please select DEPARTMENT field!<BR>"
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill Detail Data!<BR>"
        Else
            Dim dtTbl As DataTable = Session("TblDtl")
            If dtTbl.Rows.Count <= 0 Then
                sError &= "- Please fill Detail Data!<BR>"
            Else
                If sErr = "" Then
                    For C1 As Integer = 0 To dtTbl.Rows.Count - 1

                    Next
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            matretmststatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function GetStockValue(ByVal sOid As String, ByVal sRef As String, Optional ByVal sType As String = "IDR") As Double
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        Try
            sSql = "SELECT SUM(ISNULL(stockqty, 0) * ISNULL(stockvalue" & sType.ToLower & ", 0)) / SUM(ISNULL(stockqty, 0)) FROM QL_stockvalue WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND periodacctg IN ('" & sPeriod & "', '" & GetLastPeriod(sPeriod) & "') AND refoid=" & sOid & " AND refname='" & sRef & "' AND closeflag=''"
            GetStockValue = ToDouble(GetStrData(sSql))
        Catch ex As Exception
            GetStockValue = 0
        End Try
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckUsageStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_TRNMATRETMST WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND matretmststatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnmatreturn.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql).ToString) > 0 Then
            lkbUsageInProcess.Visible = True
            lkbUsageInProcess.Text = "You have " & GetStrData(sSql) & " In Process Process Material Return Non KIK data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitAllDDL()
        ' Fill DDL Business Unit
		sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'  AND cmpcode='" & Session("CompnyCode") & "'"
		'If Session("CompnyCode") <> CompnyCode Then
		'    sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
		'End If
        FillDDL(DDLBusUnit, sSql)
        If DDLBusUnit.Items.Count > 0 Then
            InitDDLDept()
            InitDDLWH()
        End If
        ' Fill DDL Unit
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='UNIT'"
        If i_u.Text = "New Data" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
        FillDDL(matretunitoid, sSql)
    End Sub

    Private Sub InitDDLDept()
        ' Fill DDL Department
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE cmpcode='" & DDLBusUnit.SelectedValue & "'"
        If i_u.Text = "New Data" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
        sSql &= " ORDER BY deptname"
        FillDDL(deptoid, sSql)
    End Sub

    Private Sub InitDDLWH()
        ' Fill DDL Warehouse
		sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='WAREHOUSE'"
        If i_u.Text = "New Data" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
		FillDDL(matretwhoid, sSql)
    End Sub

	Private Sub IniTypeDDL()
		sSql = "SELECT gencode,gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='GROUPITEM'"
		FillDDL(matretreftype, sSql)
	End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT div.divname, mum.matretmstoid, mum.matretno, CONVERT(VARCHAR(10), mum.matretdate, 101) AS matretdate, de.deptname, mum.matretmststatus, mum.matretmstnote FROM QL_TRNMATRETMST mum INNER JOIN QL_mstdept de ON de.deptoid=mum.deptoid AND de.cmpcode=mum.cmpcode INNER JOIN QL_mstdivision div ON div.cmpcode=mum.cmpcode WHERE"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " mum.cmpcode='" & Session("CompnyCode") & "'"
        Else
            sSql &= " mum.cmpcode LIKE '%'"
        End If
        sSql &= sSqlPlus & " ORDER BY CONVERT(DATETIME, mum.matretdate) DESC, mum.matretmstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_TRNMATRETMST")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

	Private Sub BindListmatusage()
		sSql = "SELECT req.matusagemstoid,req.matusageno, CONVERT(VARCHAR(10), matusagedate, 101) AS matusagedate,req.matusagemstnote FROM QL_trnmatusagemst req WHERE req.cmpcode='" & DDLBusUnit.SelectedValue & "' AND matusagemststatus='Post' AND " & FilterDDLListmatusage.SelectedValue & " LIKE '%" & Tchar(FilterTextListmatusage.Text) & "%' AND deptoid=" & deptoid.SelectedValue & " AND matusagemstoid NOT IN (SELECT a.matusagemstoid FROM QL_trnmatusagemst a WHERE a.cmpcode='" & DDLBusUnit.SelectedValue & "' AND a.matusagemststatus='Closed')  ORDER BY matusagemstoid"
		FillGV(gvListmatusage, sSql, "QL_trnmatusagemst")
	End Sub

    Private Sub BindMatData()
		Dim sStr As String = ""
		If Session("oid") IsNot Nothing And Session("oid") <> "" Then
			sStr = " AND matretmstoid <> " & Session("oid")
		End If
        sSql = "SELECT 'False' AS checkvalue,reqd.matusagedtlseq,reqd.matusagedtloid,reqd.matusagerefoid, m.itemCode AS matusagerefcode, m.itemLongDescription AS matusagereflongdesc, (reqd.matusageqty - ISNULL((SELECT SUM(matretqty) FROM QL_TRNMATRETDTL mud WHERE mud.cmpcode=reqd.cmpcode AND mud.matusagedtloid=reqd.matusagedtloid " & sStr & " ), 0.0)) AS matusageqty,reqd.matusageunitoid, gendesc AS matusageunit, 0.0 AS matretqty,reqd.matusagedtlnote AS matretdtlnote FROM QL_trnmatusagedtl reqd INNER JOIN QL_mstitem m ON m.itemoid=reqd.matusagerefoid INNER JOIN QL_mstgen g ON genoid=reqd.matusageunitoid WHERE reqd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqd.matusagemstoid=" & matusagemstoid.Text & " AND matusagedtlstatus='' AND m.itemgroup='" & matretreftype.SelectedValue & "' ORDER BY matusagedtlseq"

        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstmat")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(C1)("matretqty") = ToDouble(dt.Rows(C1)("matusageqty").ToString)
        Next
        dt.AcceptChanges()
        Session("TblMat") = dt
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("QL_TRNMATRETDTL")
        dtlTable.Columns.Add("matretdtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matusagemstoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matusageno", Type.GetType("System.String"))
        dtlTable.Columns.Add("matretreftype", Type.GetType("System.String"))
        dtlTable.Columns.Add("matusagedtloid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matretrefoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("stockacctgoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matretrefcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("matretreflongdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("matusageqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("matretqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("matretunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matretunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("matretdtlnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("matretvalueidr", Type.GetType("System.Double"))
        dtlTable.Columns.Add("matretvalueusd", Type.GetType("System.Double"))
        dtlTable.Columns.Add("matretqty_unitkecil", Type.GetType("System.Double"))
        dtlTable.Columns.Add("matretqty_unitbesar", Type.GetType("System.Double"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub ClearDetail()
        matretdtlseq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                matretdtlseq.Text = objTable.Rows.Count + 1
                EnableHeader(False)
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        i_u2.Text = "New Detail"
        matusagemstoid.Text = ""
        matusageno.Text = ""
        matretwhoid.SelectedIndex = -1
        matusagedtloid.Text = ""
        matretrefoid.Text = ""
        matretrefcode.Text = ""
        matretreflongdesc.Text = ""
        matusageqty.Text = ""
        matretqty.Text = ""
        matretunitoid.SelectedIndex = -1
        matretdtlnote.Text = ""
        gvDtl.SelectedIndex = -1
        btnSearchmatusage.Visible = True : btnClearmatusage.Visible = True
        matretreftype.Enabled = True : matretreftype.CssClass = "inpText"
        btnSearchMat.Visible = True
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal : DDLBusUnit.CssClass = sCss
        deptoid.Enabled = bVal : deptoid.CssClass = sCss
    End Sub

    Private Sub generateReturnNo()
        Dim sNo As String = "RET-" & Format(GetServerTime(), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(matretno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_TRNMATRETMST WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matretno LIKE '%" & sNo & "%'"
        matretno.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT cmpcode, matretmstoid, periodacctg, matretdate, matretno, deptoid, matretwhoid, matretmstnote, matretmststatus, createuser, createtime, upduser, updtime FROM QL_TRNMATRETMST WHERE cmpcode='" & CompnyCode & "' AND matretmstoid=" & sOid
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = xreader("cmpcode").ToString
                DDLBusUnit_SelectedIndexChanged(Nothing, Nothing)
                matretmstoid.Text = xreader("matretmstoid").ToString
                periodacctg.Text = xreader("periodacctg").ToString
                matretdate.Text = Format(xreader("matretdate"), "MM/dd/yyyy")
                matretno.Text = xreader("matretno").ToString
                deptoid.SelectedValue = xreader("deptoid").ToString
                matretmstnote.Text = xreader("matretmstnote").ToString
                matretmststatus.Text = xreader("matretmststatus").ToString
                matretwhoid.SelectedValue = xreader("matretwhoid")
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False : btnposting.Visible = False
            Exit Sub
        End Try
        If matretmststatus.Text = "Post" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnposting.Visible = False
            lblspuno.Text = "RETURN No."
            matretmstoid.Visible = False
            matretno.Visible = True
            btnAddToList.Visible = False
            gvDtl.Columns(0).Visible = False
            gvDtl.Columns(gvDtl.Columns.Count - 1).Visible = False
        End If
		Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sSql = "SELECT DISTINCT matretdtlseq, mud.matusagemstoid,reqm.matusageno, matretreftype, mud.matusagedtloid, matretrefoid,itemCode AS matretrefcode, itemLongDescription AS matretreflongdesc, matretqty, (reqd.matusageqty - (ISNULL((SELECT SUM(mudx.matretqty) FROM QL_TRNMATRETDTL mudx WHERE mudx.cmpcode=mud.cmpcode AND mudx.matusagedtloid=mud.matusagedtloid AND mudx.matretmstoid<>mud.matretmstoid), 0.0))) AS matusageqty,matretqty_unitkecil,matretqty_unitbesar matretunitoid, g2.gendesc AS matretunit, matretdtlnote, 0.00 AS matretvalueidr, 0.00 AS matretvalueusd,mud.matretqty_unitkecil,mud.matretqty_unitbesar, ISNULL(gx.genother1,0) stockacctgoid FROM QL_trnmatretdtl mud INNER JOIN QL_trnmatusagemst reqm ON reqm.cmpcode=mud.cmpcode AND reqm.matusagemstoid=mud.matusagemstoid INNER JOIN QL_trnmatusagedtl reqd ON reqd.cmpcode=mud.cmpcode AND reqd.matusagedtloid=mud.matusagedtloid INNER JOIN QL_mstgen g2 ON g2.genoid=mud.matretunitoid INNER JOIN QL_mstitem i ON i.itemoid=mud.matretrefoid INNER JOIN QL_mstgen gx ON gx.genoid=i.itemgroupoid WHERE mud.cmpcode='" & CompnyCode & "' AND matretmstoid=" & sOid & " ORDER BY matretdtlseq"

        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_TRNMATRETDTL")
        Session("TblDtl") = dt
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        ClearDetail()
    End Sub

    Private Sub PrintReport(ByVal sOid As String)
        Try
            report.Load(Server.MapPath(folderReport & "rptmatret.rpt"))
            Dim sWhere As String = "WHERE"
            sWhere &= " mum.cmpcode='" & CompnyCode & "'"
            If sOid = "" Then
                sWhere &= " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " and mum.matretdate>='" & FilterPeriod1.Text & " 00:00:00' AND mum.matretdate<='" & FilterPeriod2.Text & "'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sWhere &= " and mum.matretmststatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Transaction\trnmatreturn.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND mum.createuser='" & Session("UserID") & "'"
                End If
            Else
                sWhere &= " AND mum.matretmstoid=" & sOid
            End If
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "MaterialReturnNonKIKPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
        End Try
        Response.Redirect("~\Transaction\trnmatreturn.aspx?awal=true")
    End Sub

    Private Sub SetUsageValue(ByRef objVal As VarUsageValue)
        objVal.ValRaw_IDR = 0
        objVal.ValRaw_USD = 0
        Dim dt As DataTable = Session("TblDtl")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            Dim dValIDR As Double = GetStockValue(dt.Rows(C1)("matretrefoid").ToString, "")
            Dim dValUSD As Double = GetStockValue(dt.Rows(C1)("matretrefoid").ToString, "USD")
            dt.Rows(C1)("matretvalueidr") = dValIDR
            dt.Rows(C1)("matretvalueusd") = dValUSD
            objVal.ValRaw_IDR += dValIDR * ToDouble(dt.Rows(C1)("matretqty").ToString)
            objVal.ValRaw_USD += dValUSD * ToDouble(dt.Rows(C1)("matretqty").ToString)
        Next
        Session("TblDtl") = dt
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnmatreturn.aspx")
        End If
        If checkPagePermission("~\Transaction\trnmatreturn.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Material Return Non KIK"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
		btnposting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
		IniTypeDDL()
        If Not Page.IsPostBack Then
			createuser.Text = Session("UserID")
			createtime.Text = GetServerTime().ToString
			upduser.Text = "-"
			updtime.Text = "-"
            CheckUsageStatus()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitAllDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                matretmstoid.Text = GenerateID("QL_TRNMATRETMST", CompnyCode)
                matretdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                matretmststatus.Text = "In Process"
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvDtl.DataSource = dt
            gvDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnmatreturn.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lkbUsageInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbUsageInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = 0
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, mum.updtime, GETDATE()) > " & nDays & " AND mum.matretmststatus='In Process'"
        If checkPagePermission("~\Transaction\trnmatreturn.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND mum.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND mum.matretdate>='" & FilterPeriod1.Text & " 00:00:00' AND mum.matretdate<='" & FilterPeriod2.Text & "'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND mum.matretmststatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Transaction\trnmatreturn.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND mum.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        cbStatus.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Transaction\trnmatreturn.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND mum.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLDept()
        InitDDLWH()
    End Sub

    Protected Sub btnSearchmatusage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchmatusage.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        If deptoid.SelectedValue = "" Then
            showMessage("Please select Department first!", 2)
            Exit Sub
        End If
		FilterDDLListmatusage.SelectedIndex = -1
		FilterTextListmatusage.Text = ""
		gvListmatusage.SelectedIndex = -1
        BindListmatusage()
        cProc.SetModalPopUpExtender(btnHideListmatusage, pnlListmatusage, mpeListmatusage, True)
    End Sub

    Protected Sub btnClearmatusage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearmatusage.Click
        matusagemstoid.Text = "" : matusageno.Text = ""
    End Sub

    Protected Sub btnFindListmatusage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListmatusage.Click
        BindListmatusage()
        mpeListmatusage.Show()
    End Sub

    Protected Sub btnAllListmatusage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListmatusage.Click
        FilterDDLListmatusage.SelectedIndex = -1 : FilterTextListmatusage.Text = "" : gvListmatusage.SelectedIndex = -1
        BindListmatusage()
        mpeListmatusage.Show()
    End Sub

    Protected Sub gvListmatusage_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListmatusage.PageIndexChanging
        gvListmatusage.PageIndex = e.NewPageIndex
        BindListmatusage()
        mpeListmatusage.Show()
    End Sub

    Protected Sub gvListmatusage_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListmatusage.SelectedIndexChanged
        If matusagemstoid.Text <> gvListmatusage.SelectedDataKey.Item("matusagemstoid").ToString Then
            btnClearmatusage_Click(Nothing, Nothing)
        End If
        matusagemstoid.Text = gvListmatusage.SelectedDataKey.Item("matusagemstoid").ToString
        matusageno.Text = gvListmatusage.SelectedDataKey.Item("matusageno").ToString
        cProc.SetModalPopUpExtender(btnHideListmatusage, pnlListmatusage, mpeListmatusage, False)
    End Sub

    Protected Sub lbCloseListmatusage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListmatusage.Click
        cProc.SetModalPopUpExtender(btnHideListmatusage, pnlListmatusage, mpeListmatusage, False)
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        If matusagemstoid.Text = "" Then
            showMessage("Please select RETURN No. first!", 2)
            Exit Sub
        End If
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
		Session("TblMat") = Nothing : Session("TblMatView") = Nothing
		gvListMat.DataSource = Nothing : gvListMat.DataBind()
		tbData.Text = "5" : gvListMat.PageSize = CInt(tbData.Text)
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblMat") Is Nothing Then
            BindMatData()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If Session("TblMat") Is Nothing Then
            BindMatData()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matusagedtloid=" & dtTbl.Rows(C1)("matusagedtloid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matusagedtloid=" & dtTbl.Rows(C1)("matusagedtloid")
                    objView(0)("checkvalue") = "False"
                    objView(0)("matretqty") = 0
                    objView(0)("matretdtlnote") = ""
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    dtTbl.Rows(C1)("matretqty") = 0
                    dtTbl.Rows(C1)("matretdtlnote") = ""
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Selected material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub lbShowData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbShowData.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Please find material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If tbData.Text = "" Then
            Session("WarningListMat") = "Please fill total data to be shown!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        Else
            If ToDouble(tbData.Text) <= 0 Then
                Session("WarningListMat") = "Total data to be shown must be more than 0!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat2() Then
            gvListMat.PageSize = Int(tbData.Text)
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(4).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 2)
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    dtView.RowFilter = ""
                    dtView.RowFilter = "checkvalue='True' AND matretqty>0"
                    If dtView.Count <> iCheck Then
                        Session("WarningListMat") = "RETURN Qty for every checked material data must be more than 0!"
                        showMessage(Session("WarningListMat"), 2)
                        dtView.RowFilter = ""
                        Exit Sub
                    End If
                    dtView.RowFilter = ""
                    dtView.RowFilter = "CheckValue='True' AND matretqty<=matusageqty"
                    If dtView.Count <> iCheck Then
                        Session("WarningListMat") = "RETURN Qty for every checked material data must be less than Usage Qty!"
                        showMessage(Session("WarningListMat"), 2)
                        dtView.RowFilter = ""
                        Exit Sub
                    End If
                    If Session("TblDtl") Is Nothing Then
                        CreateTblDetail()
                    End If
                    Dim objTable As DataTable
                    objTable = Session("TblDtl")
                    Dim dv As DataView = objTable.DefaultView
                    Dim dQty_unitkecil As Double = 0
                    Dim dQty_unitbesar As Double = 0
                    Dim objRow As DataRow
                    Dim counter As Integer = objTable.Rows.Count
                    For C1 As Integer = 0 To dtView.Count - 1
                        dv.RowFilter = "matusagedtloid=" & dtView(C1)("matusagedtloid")
                        If dv.Count > 0 Then
                            dv.AllowEdit = True
                            GetUnitConverter(dtView(C1)("matusagerefoid"), dtView(C1)("matusageunitoid"), ToDouble(dtView(C1)("matretqty")), dQty_unitkecil, dQty_unitbesar)
                            dv(0)("matretqty") = dtView(C1)("matretqty")
                            dv(0)("matretdtlnote") = dtView(C1)("matretdtlnote")
                        Else
                            counter += 1
                            objRow = objTable.NewRow()
                            GetUnitConverter(dtView(C1)("matusagerefoid"), dtView(C1)("matusageunitoid"), ToDouble(dtView(C1)("matretqty")), dQty_unitkecil, dQty_unitbesar)
                            objRow("matretdtlseq") = counter
                            objRow("matusagemstoid") = matusagemstoid.Text
                            objRow("matusageno") = matusageno.Text
                            objRow("matretreftype") = matretreftype.SelectedValue
                            objRow("matusagedtloid") = dtView(C1)("matusagedtloid")
                            objRow("matretrefoid") = dtView(C1)("matusagerefoid")
                            objRow("matretrefcode") = dtView(C1)("matusagerefcode")
                            objRow("matretreflongdesc") = dtView(C1)("matusagereflongdesc")
                            objRow("matusageqty") = ToDouble(dtView(C1)("matusageqty"))
                            objRow("matretqty") = ToDouble(dtView(C1)("matretqty"))
                            objRow("matretqty_unitkecil") = dQty_unitkecil
                            objRow("matretqty_unitbesar") = dQty_unitbesar
                            objRow("matretunitoid") = dtView(C1)("matusageunitoid")
                            objRow("matretunit") = dtView(C1)("matusageunit")
                            objRow("matretdtlnote") = dtView(C1)("matretdtlnote")
                            objTable.Rows.Add(objRow)
                        End If
                        dv.RowFilter = ""
                    Next
                    dtView.RowFilter = ""
                    Session("TblDtl") = objTable
                    gvDtl.DataSource = objTable
                    gvDtl.DataBind()
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                    ClearDetail()
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            If Session("TblDtl") Is Nothing Then
                CreateTblDetail()
            End If
            Dim objTable As DataTable = Session("TblDtl")
            Dim dv As DataView = objTable.DefaultView
            If i_u2.Text = "New Detail" Then
                dv.RowFilter = "matusagedtloid=" & matusagedtloid.Text
            Else
                dv.RowFilter = "matusagedtloid=" & matusagedtloid.Text & " AND matretdtlseq<>" & matretdtlseq.Text
            End If
            If dv.Count > 0 Then
                showMessage("This Data has been added before, please check!", 2)
                dv.RowFilter = ""
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                objRow("matretdtlseq") = objTable.Rows.Count + 1
            Else
                objRow = objTable.Rows(matretdtlseq.Text - 1)
                objRow.BeginEdit()
            End If
            objRow("matusagemstoid") = matusagemstoid.Text
            objRow("matusageno") = matusageno.Text
            objRow("matretreftype") = matretreftype.SelectedValue
            objRow("matusagedtloid") = matusagedtloid.Text
            objRow("matretrefoid") = matretrefoid.Text
            objRow("matretrefcode") = matretrefcode.Text
            objRow("matretreflongdesc") = matretreflongdesc.Text
            objRow("matusageqty") = ToDouble(matusageqty.Text)
            objRow("matretqty") = ToDouble(matretqty.Text)
            objRow("matretunitoid") = matretunitoid.SelectedValue
            objRow("matretunit") = matretunitoid.SelectedItem.Text
            objRow("matretdtlnote") = matretdtlnote.Text
            If i_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            Session("TblDtl") = objTable
            gvDtl.DataSource = Session("TblDtl")
            gvDtl.DataBind()
            ClearDetail()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
        End If
    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDtl.SelectedIndexChanged
        Try
            matretdtlseq.Text = gvDtl.SelectedDataKey.Item("matretdtlseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "matretdtlseq=" & matretdtlseq.Text
                matusagemstoid.Text = dv.Item(0).Item("matusagemstoid").ToString
                matusageno.Text = dv.Item(0).Item("matusageno").ToString
                matretreftype.SelectedValue = dv.Item(0).Item("matretreftype").ToString
                matusagedtloid.Text = dv.Item(0).Item("matusagedtloid").ToString
                matretrefoid.Text = dv.Item(0).Item("matretrefoid").ToString
                matretrefcode.Text = dv.Item(0).Item("matretrefcode").ToString
                matretreflongdesc.Text = dv.Item(0).Item("matretreflongdesc").ToString
                matusageqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("matusageqty").ToString), 4)
                matretqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("matretqty").ToString), 4)
                matretunitoid.SelectedValue = dv.Item(0).Item("matretunitoid").ToString
                matretdtlnote.Text = dv.Item(0).Item("matretdtlnote").ToString
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        Finally
            btnSearchmatusage.Visible = False : btnClearmatusage.Visible = False
            matretreftype.Enabled = False : matretreftype.CssClass = "inpTextDisabled"
            btnSearchMat.Visible = False
        End Try
    End Sub

    Protected Sub gvDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("matretdtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        ClearDetail()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_TRNMATRETMST WHERE matretmstoid=" & matretmstoid.Text
                If CheckDataExists(sSql) Then
                    matretmstoid.Text = GenerateID("QL_TRNMATRETMST", CompnyCode)
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNMATRETMST", "matretmstoid", matretmstoid.Text, "matretmststatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    matretmststatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            matretdtloid.Text = GenerateID("QL_TRNMATRETDTL", CompnyCode)
            Dim conmtroid As Integer = 0, crdmatoid As Integer = 0, glmstoid As Integer = 0, gldtloid As Integer = 0
            Dim iDeptAcctgOid As Integer = 0, iStockRawAcctgOid As Integer = 0, iStockGenAcctgOid As Integer = 0
            Dim iStockSPAcctgOid As Integer = 0, iStockLogAcctgOid As Integer = 0, iStockSTAcctgOid As Integer = 0, stockvalueoid As Integer = 0
            Dim objValue As VarUsageValue
            Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
            Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
            periodacctg.Text = GetDateToPeriodAcctg(CDate(matretdate.Text))
            Dim iGroupOid As Integer = 0
            If matretmststatus.Text = "Post" Then
                If isPeriodClosed(DDLBusUnit.SelectedValue, sPeriod) Then
                    showMessage("Cannot continue this transaction anymore because the period : " & MonthName(CInt(sPeriod.Substring(4, 2))) & " " & sPeriod.Substring(0, 4) & " have been closed.", 3)
                    Exit Sub
                End If
                conmtroid = GenerateID("QL_CONSTOCK", CompnyCode)
                crdmatoid = GenerateID("QL_CRDSTOCK", CompnyCode)
                glmstoid = GenerateID("QL_TRNGLMST", CompnyCode)
                gldtloid = GenerateID("QL_TRNGLDTL", CompnyCode)
                stockvalueoid = GenerateID("QL_STOCKVALUE", CompnyCode)
                Dim sVarErr As String = ""
                sSql = "SELECT ISNULL(deptacctgoid, 0) AS deptacctgoid FROM QL_mstdept WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND deptoid=" & deptoid.SelectedValue
                iDeptAcctgOid = CInt(GetStrData(sSql))
                If iDeptAcctgOid = 0 Then
                    showMessage("Please select default COA Usage for department " & deptoid.SelectedItem.Text & " in Department form!", 2)
                    matretmststatus.Text = "In Process"
                    Exit Sub
                End If
                If Not IsInterfaceExists("VAR_STOCK", DDLBusUnit.SelectedValue) Then
					sVarErr = "VAR_STOCK"
                End If
           
                If sVarErr <> "" Then
                    showMessage(GetInterfaceWarning(sVarErr, "posting"), 2)
                    matretmststatus.Text = "In Process"
                    Exit Sub
                End If
                iStockRawAcctgOid = GetAcctgOID(GetVarInterface("VAR_STOCK", DDLBusUnit.SelectedValue), CompnyCode)
                SetUsageValue(objValue)
                generateReturnNo()
                iGroupOid = ToInteger(GetStrData("SELECT TOP 1 groupoid FROM QL_mstdeptgroupdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND deptoid=" & deptoid.SelectedValue & " ORDER BY updtime DESC"))
            End If
            ' Define Tabel Strucure utk Auto Jurnal 
            sSql = "SELECT 0 acctgoid, 0.0 debet, 0.0 credit"
            Dim tbPostGL As DataTable = cKon.ambiltabel(sSql, "PostGL")
            tbPostGL.Rows.Clear()
            Dim oRow As DataRow
            Dim dvCek As DataView = tbPostGL.DefaultView

            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_TRNMATRETMST (cmpcode, matretmstoid, periodacctg, matretdate, matretno, deptoid, matretmstnote, matretmststatus, createuser, createtime, upduser, updtime, matretwhoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & matretmstoid.Text & ", '" & periodacctg.Text & "', '" & matretdate.Text & "', '" & matretno.Text & "', " & deptoid.SelectedValue & ", '" & Tchar(matretmstnote.Text) & "', '" & matretmststatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & matretwhoid.SelectedValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & matretmstoid.Text & " WHERE tablename='QL_TRNMATRETMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_TRNMATRETMST SET periodacctg='" & periodacctg.Text & "', matretdate='" & matretdate.Text & "', matretno='" & matretno.Text & "', deptoid=" & deptoid.SelectedValue & ", matretmstnote='" & Tchar(matretmstnote.Text) & "', matretmststatus='" & matretmststatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, matretwhoid=" & matretwhoid.SelectedValue & " WHERE matretmstoid=" & matretmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
					sSql = "UPDATE QL_trnmatusagedtl SET matusagedtlstatus='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matusagedtloid IN (SELECT matusagedtloid FROM QL_TRNMATRETDTL WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matretmstoid=" & matretmstoid.Text & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
					sSql = "UPDATE QL_trnmatusagemst SET matusagemststatus='Post' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matusagemstoid IN (SELECT matusagemstoid FROM QL_TRNMATRETDTL WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matretmstoid=" & matretmstoid.Text & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_TRNMATRETDTL WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matretmstoid=" & matretmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_TRNMATRETDTL (cmpcode, matretdtloid, matretmstoid, matretdtlseq, matusagemstoid, matusagedtloid, matretreftype, matretrefoid, matretqty, matretunitoid, matretdtlnote, matretdtlstatus, upduser, updtime, matretvalueidr, matretvalueusd,matretqty_unitkecil,matretqty_unitbesar) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C1 + CInt(matretdtloid.Text)) & ", " & matretmstoid.Text & ", " & C1 + 1 & ", " & objTable.Rows(C1).Item("matusagemstoid") & ", " & objTable.Rows(C1).Item("matusagedtloid") & ", '" & objTable.Rows(C1).Item("matretreftype").ToString & "', " & objTable.Rows(C1).Item("matretrefoid") & ", " & ToDouble(objTable.Rows(C1).Item("matretqty").ToString) & ", " & objTable.Rows(C1).Item("matretunitoid") & ", '" & Tchar(objTable.Rows(C1).Item("matretdtlnote")) & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(C1).Item("matretvalueidr").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("matretvalueusd").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("matretqty_unitkecil").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("matretqty_unitbesar").ToString) & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        If objTable.Rows(C1).Item("matretqty") >= objTable.Rows(C1).Item("matusageqty") Then
							sSql = "UPDATE QL_trnmatusagedtl SET matusagedtlstatus='Complete' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matusagedtloid=" & objTable.Rows(C1).Item("matusagedtloid")
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
							sSql = "UPDATE QL_trnmatusagemst SET matusagemststatus='Closed' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matusagemstoid=" & objTable.Rows(C1).Item("matusagemstoid") & " AND (SELECT COUNT(*) FROM QL_trnmatusagedtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matusagemstoid=" & objTable.Rows(C1).Item("matusagemstoid") & " AND matusagedtloid <> " & objTable.Rows(C1).Item("matusagedtloid") & " AND matusagedtlstatus='')=0"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                        If matretmststatus.Text = "Post" Then
                            Dim sRef As String = ""
                            If objTable.Rows(C1)("matretreftype").ToString.ToUpper = "RAW" Then
                                sRef = "RAW MATERIAL"
                            ElseIf objTable.Rows(C1)("matretreftype").ToString.ToUpper = "GEN" Then
                                sRef = "GENERAL MATERIAL"
                            ElseIf objTable.Rows(C1)("matretreftype").ToString.ToUpper = "FG" Then
                                sRef = "FINISH GOOD"
                            ElseIf objTable.Rows(C1)("matretreftype").ToString.ToUpper = "WIP" Then
                                sRef = "WIP"
                            End If
                            ' Insert QL_constock
                            sSql = "INSERT INTO QL_constock(cmpcode,constockoid,contype,trndate,formaction,formoid,periodacctg,refname,refoid,mtrlocoid,qtyin,qtyout,amount,hpp,typemin,note,reason,createuser,createtime,upduser,updtime,refno,deptoid,valueidr,valueusd,qtyin_unitbesar,qtyout_unitbesar) VALUES" & _
                            " ('" & DDLBusUnit.SelectedValue & "'," & conmtroid & ",'MRNONKIK','" & sDate & "','QL_TRNMATRETDTL'," & matretmstoid.Text & ",'" & sPeriod & "','" & sRef & "'," & objTable.Rows(C1).Item("matretrefoid") & "," & matretwhoid.SelectedValue & "," & ToDouble(objTable.Rows(C1).Item("matretqty")) & ",0,0,0, 1,'" & matretno.Text & "','Material Return Non KIK','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,''," & deptoid.SelectedValue & "," & ToDouble(objTable.Rows(C1).Item("matretvalueidr").ToString) & "," & ToDouble(objTable.Rows(C1).Item("matretvalueusd").ToString) & "," & ToDouble(objTable.Rows(C1).Item("matretqty_unitbesar").ToString) & ",0)"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            conmtroid += 1
                            ' Insert QL_crdstock
                            sSql = "UPDATE QL_crdstock SET qtyin=qtyin + " & ToDouble(objTable.Rows(C1).Item("matretqty")) & ", saldoakhir=saldoakhir + " & ToDouble(objTable.Rows(C1).Item("matretqty")) & ", saldoakhir_unitbesar=saldoakhir_unitbesar + " & ToDouble(objTable.Rows(C1).Item("matretqty_unitbesar")) & ", lasttranstype='QL_TRNMATRETDTL', lasttransdate='" & sDate & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND refoid=" & objTable.Rows(C1).Item("matretrefoid") & " AND mtrlocoid=" & matretwhoid.SelectedValue & " AND periodacctg='" & sPeriod & "'"
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() = 0 Then
                                sSql = "INSERT INTO QL_crdstock (cmpcode, crdstockoid, periodacctg, refoid, refname, mtrlocoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir, lasttranstype, lasttransdate, upduser, updtime, createuser, createtime, closeuser, closingdate, qtyin_unitbesar, qtyout_unitbesar, qtyadjin_unitbesar, qtyadjout_unitbesar, saldoawal_unitbesar, saldoakhir_unitbesar) VALUES ('" & DDLBusUnit.SelectedValue & "', " & crdmatoid & ", '" & sPeriod & "', " & objTable.Rows(C1).Item("matretrefoid") & ", '" & sRef & "', " & matretwhoid.SelectedValue & ", " & ToDouble(objTable.Rows(C1).Item("matretqty")) & ",0, 0, 0, 0, " & ToDouble(objTable.Rows(C1).Item("matretqty")) & ", 'QL_TRNMATRETDTL', '" & sDate & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', '1/1/1900', " & ToDouble(objTable.Rows(C1).Item("matretqty_unitbesar")) & ",0, 0, 0, 0, " & ToDouble(objTable.Rows(C1).Item("matretqty_unitbesar")) & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                crdmatoid += 1
                            End If
                            sSql = GetQueryUpdateStockValue(ToDouble(objTable.Rows(C1).Item("matretqty")), ToDouble(objTable.Rows(C1).Item("matretqty_unitbesar")), ToDouble(objTable.Rows(C1).Item("matretvalueidr")), ToDouble(objTable.Rows(C1).Item("matretvalueusd")), "QL_TRNMATRETDTL", sDate, Session("UserID"), DDLBusUnit.SelectedValue, sPeriod, objTable.Rows(C1).Item("matretrefoid"))
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() = 0 Then
                                sSql = GetQueryInsertStockValue(ToDouble(objTable.Rows(C1).Item("matretqty")), ToDouble(objTable.Rows(C1).Item("matretqty_unitbesar")), ToDouble(objTable.Rows(C1).Item("matretvalueidr")), ToDouble(objTable.Rows(C1).Item("matretvalueusd")), "QL_TRNMATRETDTL", sDate, Session("UserID"), DDLBusUnit.SelectedValue, sPeriod, objTable.Rows(C1).Item("matretrefoid"), stockvalueoid)
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                stockvalueoid += 1
                            End If

                            dvCek.RowFilter = "acctgoid=" & objTable.Rows(C1).Item("stockacctgoid").ToString & " "
                            If dvCek.Count > 0 Then
                                dvCek(0)("debet") += ToDouble(objTable.Rows(C1).Item("matretvalueidr").ToString) * ToDouble(objTable.Rows(C1).Item("matretqty").ToString)
                                dvCek.RowFilter = ""
                            Else
                                dvCek.RowFilter = ""

                                oRow = tbPostGL.NewRow
                                oRow("acctgoid") = objTable.Rows(C1).Item("stockacctgoid")
                                oRow("debet") = ToDouble(objTable.Rows(C1).Item("matretvalueidr").ToString) * ToDouble(objTable.Rows(C1).Item("matretqty").ToString)
                                tbPostGL.Rows.Add(oRow)
                            End If
                            tbPostGL.AcceptChanges()
                        End If
                    Next

                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(matretdtloid.Text)) & " WHERE tablename='QL_TRNMATRETDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & stockvalueoid - 1 & " WHERE tablename='QL_STOCKVALUE' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    If matretmststatus.Text = "Post" Then
                        Dim dTotalIDR As Double = objValue.ValRaw_IDR * cRate.GetRateMonthlyIDRValue
                        Dim dTotalUSD As Double = objValue.ValRaw_USD * cRate.GetRateMonthlyUSDValue
                        If dTotalIDR > 0 And dTotalUSD > 0 Then
                            ' Insert GL MST
                            sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & glmstoid & ", '" & sDate & "', '" & sPeriod & "', 'Retur Non KIK|No. " & matretno.Text & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 1, 1, 1, 1, " & dTotalUSD & ", " & dTotalUSD & ")"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            ' Insert GL DTL
                            Dim iSeq As Integer = 1
                            For C1 As Integer = 0 To tbPostGL.Rows.Count - 1
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, groupoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & tbPostGL.Rows(C1)("stockacctgoid").ToString & ", 'D', " & ToDouble(tbPostGL.Rows(C1)("debet")) & ", '" & matretno.Text & "', 'Retur Non KIK|No. " & matretno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(tbPostGL.Rows(C1)("debet")) & ", " & ToDouble(tbPostGL.Rows(C1)("debet")) & ", 'QL_trnmatretmst " & matretmstoid.Text & "', " & iGroupOid & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iSeq += 1
                                gldtloid += 1
                            Next
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, groupoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & iDeptAcctgOid & ", 'C', " & dTotalIDR & ", '" & matretno.Text & "', 'Retur Non KIK|No. " & matretno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalIDR * cRate.GetRateMonthlyIDRValue & ", " & dTotalUSD * cRate.GetRateMonthlyIDRValue & ", 'QL_trnmatretmst " & matretmstoid.Text & "', " & iGroupOid & ")"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iSeq += 1
                            gldtloid += 1
                            sSql = "UPDATE QL_mstoid SET lastoid=" & glmstoid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_mstoid SET lastoid=" & gldtloid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                        sSql = "UPDATE QL_mstoid SET lastoid=" & conmtroid - 1 & " WHERE tablename='QL_CONSTOCK' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        sSql = "UPDATE QL_mstoid SET lastoid=" & crdmatoid - 1 & " WHERE tablename='QL_CRDSTOCK' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()

                    End If
                End If
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message & sSql, 1)
                        matretmststatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    matretmststatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 2)
                conn.Close()
                matretmststatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & matretmstoid.Text & ".<BR>"
            End If
            If matretmststatus.Text = "Post" Then
                Session("SavedInfo") &= "Data have been posted with RETURN No. = " & matretno.Text & "."
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trnmatreturn.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnmatreturn.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If matretmstoid.Text = "" Then
            showMessage("Please select Process Material RETURN data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNMATRETMST", "matretmstoid", matretmstoid.Text, "matretmststatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                matretmststatus.Text = "In Process"
                Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
			sSql = "UPDATE QL_trnmatusagedtl SET matusagedtlstatus='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matusagedtloid IN (SELECT DISTINCT matusagedtloid FROM QL_TRNMATRETDTL WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matretmstoid=" & matretmstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
			sSql = "UPDATE QL_trnmatusagemst SET matusagemststatus='Post' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matusagemstoid IN (SELECT DISTINCT matusagemstoid FROM QL_TRNMATRETDTL WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matretmstoid=" & matretmstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_TRNMATRETDTL WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matretmstoid=" & matretmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_TRNMATRETMST WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matretmstoid=" & matretmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnmatreturn.aspx?awal=true")
    End Sub

    Protected Sub btnposting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnposting.Click
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        If isPeriodClosed(CompnyCode, CDate(sDate)) Then
            showMessage("Period " & GetDateToPeriodAcctg(CDate(sDate)) & " Have been Already CLOSED, Please CANCEL this transaction and try again!", 2)
            Exit Sub
        End If
        matretmststatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        PrintReport("")
    End Sub

    Protected Sub gvTRN_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvTRN.SelectedIndexChanged
        PrintReport(gvTRN.SelectedDataKey.Item("matretmstoid").ToString)
    End Sub
#End Region

End Class
