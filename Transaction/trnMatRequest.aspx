<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnMatRequest.aspx.vb" Inherits="Transaction_MaterialRequestNonKIK" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" ForeColor="SaddleBrown" Text=".: Material Request Non KIK" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Material Request Non KIK :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" DefaultButton="btnSearch" __designer:wfdid="w192"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 13%" class="Label" align=left><asp:Label id="Label19" runat="server" Text="Filter" __designer:wfdid="w193"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w194"><asp:ListItem Value="reqm.matreqmstoid">Draft No.</asp:ListItem>
<asp:ListItem Value="reqm.matreqno">Request No.</asp:ListItem>
<asp:ListItem Value="de.deptname">Department</asp:ListItem>
<asp:ListItem Value="g1.gendesc">Warehouse</asp:ListItem>
<asp:ListItem Value="reqm.matreqmstnote">Note</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="200px" __designer:wfdid="w195"></asp:TextBox></TD><TD align=left></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text=" Period" __designer:wfdid="w196"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="60px" __designer:wfdid="w197"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w198"></asp:ImageButton>&nbsp;<asp:Label id="Label5" runat="server" Text="to" __designer:wfdid="w199"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="60px" __designer:wfdid="w200"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w201"></asp:ImageButton>&nbsp;<asp:Label id="lbldate1" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w202"></asp:Label></TD><TD align=left colSpan=1></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text=" Status" __designer:wfdid="w203"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w204">
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
<asp:ListItem>Cancel</asp:ListItem>
<asp:ListItem>Closed</asp:ListItem>
</asp:DropDownList></TD><TD align=left colSpan=1></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w205" Format="MM/dd/yyyy" PopupButtonID="imbDate1" TargetControlID="FilterPeriod1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender3" runat="server" __designer:wfdid="w206" TargetControlID="FilterPeriod1" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w207" Format="MM/dd/yyyy" PopupButtonID="imbDate2" TargetControlID="FilterPeriod2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender4" runat="server" __designer:wfdid="w208" TargetControlID="FilterPeriod2" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender></TD><TD align=left colSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w209"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w210"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w211"></asp:ImageButton></TD><TD align=left colSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:LinkButton id="lkbReqInProcess" runat="server" __designer:wfdid="w212"></asp:LinkButton></TD><TD align=left colSpan=1></TD></TR><TR><TD style="HEIGHT: 5pt" class="Label" align=left colSpan=4></TD><TD align=left colSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvTRN" runat="server" ForeColor="#333333" Width="100%" __designer:wfdid="w213" AllowSorting="True" AllowPaging="True" DataKeyNames="matreqmstoid" PageSize="8" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="matreqmstoid" DataNavigateUrlFormatString="~\Transaction\trnMatRequest.aspx?oid={0}" DataTextField="matreqmstoid" HeaderText="Draft No." SortExpression="matreqmstoid">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="matreqno" HeaderText="Request No." SortExpression="matreqno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matreqdate" HeaderText="Date" SortExpression="matreqdate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="deptname" HeaderText="Department" SortExpression="deptname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matreqwh" HeaderText="Warehouse" SortExpression="matreqwh">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matreqmststatus" HeaderText="Status" SortExpression="matreqmststatus">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matreqmstnote" HeaderText="Note" SortExpression="matreqmstnote">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="divname" HeaderText="Business Unit" SortExpression="divname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="createuser" HeaderText="Create User" SortExpression="createuser">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField SelectImageUrl="~/Images/print.gif" ShowSelectButton="True" ButtonType="Image">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="Sienna" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !!" __designer:wfdid="w57"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w214"></asp:Label></TD><TD align=left colSpan=1></TD></TR><TR><TD class="Label" align=center colSpan=4><asp:UpdateProgress id="UpdateProgress2" runat="server" __designer:wfdid="w215" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image21" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w216"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress></TD><TD align=left colSpan=1></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="gvTRN"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label17" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Material Request Non KIK Header" Font-Underline="False" __designer:wfdid="w513"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="i_u" runat="server" CssClass="Important" Text="New Data" __designer:wfdid="w514"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="periodacctg" runat="server" __designer:wfdid="w515" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label2" runat="server" Text="Business Unit" Width="80px" __designer:wfdid="w463" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w517" Visible="False" Enabled="False" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblsprno" runat="server" Text="Draft No." __designer:wfdid="w518"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:Label id="matreqmstoid" runat="server" __designer:wfdid="w519"></asp:Label><asp:TextBox id="matreqno" runat="server" CssClass="inpTextDisabled" Width="160px" __designer:wfdid="w520" Visible="False" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label6" runat="server" Text="Request Date" __designer:wfdid="w521"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="matreqdate" runat="server" CssClass="inpTextDisabled" Width="80px" __designer:wfdid="w522" Enabled="False" ToolTip="MM/DD/YYYY"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label4" runat="server" Text="Department" __designer:wfdid="w523"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="deptoid" runat="server" CssClass="inpText" Width="205px" __designer:wfdid="w524" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text="Warehouse" __designer:wfdid="w525"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="matreqwhoid" runat="server" CssClass="inpText" Width="205px" __designer:wfdid="w526"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label8" runat="server" Text="Header Note" __designer:wfdid="w527"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="matreqmstnote" runat="server" CssClass="inpText" Width="200px" __designer:wfdid="w528" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label9" runat="server" Text="Status" __designer:wfdid="w529"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="matreqmststatus" runat="server" CssClass="inpTextDisabled" Width="80px" __designer:wfdid="w530" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6 rowSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><asp:Label id="Label18" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Material Request Non KIK Detail" Font-Underline="False" __designer:wfdid="w531"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="i_u2" runat="server" Text="New Detail" __designer:wfdid="w532" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="matreqdtlseq" runat="server" __designer:wfdid="w533" Visible="False">1</asp:Label> <asp:Label id="matreqdtloid" runat="server" __designer:wfdid="w534" Visible="False"></asp:Label> <asp:Label id="matreqrefoid" runat="server" __designer:wfdid="w535" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:Label id="matreqrefcode" runat="server" __designer:wfdid="w536" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD id="TD5" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label12" runat="server" Text="Type" __designer:wfdid="w537"></asp:Label></TD><TD id="TD4" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD3" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="matreqreftype" runat="server" CssClass="inpText" __designer:wfdid="w538"></asp:DropDownList> <asp:Label id="TypeRef" runat="server" __designer:wfdid="w1" Visible="False"></asp:Label></TD><TD id="TD2" class="Label" align=left runat="server" Visible="false"></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label11" runat="server" Text="Material" __designer:wfdid="w539"></asp:Label> <asp:Label id="Label20" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w540"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="matreqreflongdesc" runat="server" CssClass="inpTextDisabled" Width="180px" __designer:wfdid="w541" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchMat" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w542"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="Label16" runat="server" Text="Qty" __designer:wfdid="w543"></asp:Label> <asp:Label id="Label21" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w544"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="matreqqty" runat="server" CssClass="inpText" Width="80px" __designer:wfdid="w545" MaxLength="10"></asp:TextBox>&nbsp;<asp:DropDownList id="matrequnitoid" runat="server" CssClass="inpTextDisabled" Width="115px" __designer:wfdid="w546" Enabled="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label13" runat="server" Text="Detail Note" __designer:wfdid="w547"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="matreqdtlnote" runat="server" CssClass="inpText" Width="200px" __designer:wfdid="w548" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="ftbQty" runat="server" __designer:wfdid="w549" ValidChars="1234567890,." TargetControlID="matreqqty">
            </ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w550"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w551"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Panel id="pnlGVDetail" runat="server" CssClass="inpText" Width="100%" __designer:wfdid="w552" ScrollBars="Vertical" Height="150px"><asp:GridView id="gvReqDtl" runat="server" ForeColor="#333333" Width="98%" __designer:wfdid="w553" GridLines="None" AutoGenerateColumns="False" CellPadding="4" PageSize="5" DataKeyNames="matreqdtlseq">
<RowStyle BackColor="#EFF3FB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="matreqreftype" HeaderText="Type">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matreqrefcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matreqreflongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matreqqty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matrequnit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matreqdtlnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label7" runat="server" ForeColor="Red" Text="No detail data !!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=6></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w554"></asp:Label>&nbsp;On <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w555"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update&nbsp;By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w556"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w557"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w558" AlternateText="Save"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w559" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w560" AlternateText="Delete"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnposting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w561"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w562" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w563"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Material Request Non KIK :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel id="upListMat" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListMat" runat="server" CssClass="modalBox" Width="850px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Medium" Font-Bold="True" Font-Underline="False">List Of Material</asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMat" runat="server" Width="100%" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3>Filter :&nbsp;<asp:DropDownList id="FilterDDLListMat" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w2"><asp:ListItem Value="itemcode">Item Code</asp:ListItem>
<asp:ListItem Value="itemLongDescription">Description</asp:ListItem>
<asp:ListItem Value="gendesc">Unit</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" CssClass="inpText" Width="200px"></asp:TextBox> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD id="TD1" class="Label" align=center colSpan=3 runat="server" Visible="false"><asp:CheckBox id="cbCat01" runat="server" Text="Cat 1 :"></asp:CheckBox> <asp:DropDownList id="DDLCat01" runat="server" CssClass="inpText" Width="150px" AutoPostBack="True"></asp:DropDownList> <asp:CheckBox id="cbCat02" runat="server" Text="Cat 2 :"></asp:CheckBox> <asp:DropDownList id="DDLCat02" runat="server" CssClass="inpText" Width="150px" AutoPostBack="True"></asp:DropDownList><BR /><asp:CheckBox id="cbCat03" runat="server" Text="Cat 3 :"></asp:CheckBox> <asp:DropDownList id="DDLCat03" runat="server" CssClass="inpText" Width="150px" AutoPostBack="True"></asp:DropDownList> <asp:CheckBox id="cbCat04" runat="server" Text="Cat 4 :"></asp:CheckBox> <asp:DropDownList id="DDLCat04" runat="server" CssClass="inpText" Width="150px" AutoPostBack="True"></asp:DropDownList></TD></TR></TBODY></TABLE></asp:Panel></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNone" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewChecked" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:UpdateProgress id="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat"><ProgressTemplate>
<TABLE style="WIDTH: 200px"><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple">Load Data, Please Wait ...</TD></TR><TR><TD><asp:Image id="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif"></asp:Image></TD></TR></TABLE>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD class="Label" align=left colSpan=3>Show <asp:TextBox id="tbData" runat="server" CssClass="inpText" Width="25px"></asp:TextBox> Data Per Page <asp:LinkButton id="lbShowData" runat="server">>> View</asp:LinkButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat" runat="server" ForeColor="#333333" Width="99%" PageSize="5" CellPadding="4" AutoGenerateColumns="False" GridLines="None" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbSelectPR" runat="server" __designer:wfdid="w3" ToolTip='<%# eval("itemoid") %>' Checked='<%# eval("CheckValue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="matreqrefcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemLongDescription" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Qty"><EditItemTemplate>
<asp:TextBox id="TextBox1" runat="server" Text='<%# Bind("registerqty") %>' __designer:wfdid="w328"></asp:TextBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:TextBox id="tbRegQty" runat="server" CssClass="inpText" Text='<%# eval("matreqqty") %>' Width="75px" __designer:wfdid="w326" Enabled='<%# eval("enableqty") %>' MaxLength="10"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftbeQty" runat="server" __designer:wfdid="w327" TargetControlID="tbRegQty" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="gendesc" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Note"><ItemTemplate>
<asp:TextBox id="TextBox1" runat="server" CssClass="inpText" Text='<%# eval("matreqdtlnote") %>' MaxLength="100" Width="150px"></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Yunit" Visible="False"><ItemTemplate>
<asp:DropDownList id="DDLUnit" runat="server" CssClass="inpText" Width="80px" __designer:wfdid="w4" ToolTip="<%# GetParameterID() %>"></asp:DropDownList>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
&nbsp;
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListMat" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" PopupDragHandleControlID="lblTitleListMat" BackgroundCssClass="modalBackground" PopupControlID="pnlListMat" Drag="True"></ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

