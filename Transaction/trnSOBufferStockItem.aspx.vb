Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_trnSOBufferStockItem
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetTaxValue() As Double
        GetTaxValue = 0
        Try
            sSql = "SELECT TOP 1 gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND gengroup='DEFAULT TAX' ORDER BY updtime DESC"
            GetTaxValue = ToDouble(GetStrData(sSql))
        Catch ex As Exception
            GetTaxValue = 0
        End Try
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Session("tempMatoid") = "0"
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matoid=" & cbOid
                                Session("tempMatoid") = cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("soqty") = ToDouble(GetTextBoxValue(row.Cells(4).Controls))
                                    dtView(0)("matunitoid") = GetDDLValue(row.Cells(5).Controls)
                                    dtView(0)("matunit") = GetDDLText(row.Cells(5).Controls)
                                    dtView(0)("sodtlnote") = GetTextBoxValue(row.Cells(6).Controls)
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matoid=" & cbOid
                                dtView2.RowFilter = "matoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("soqty") = ToDouble(GetTextBoxValue(row.Cells(4).Controls))
                                    dtView(0)("matunitoid") = GetDDLValue(row.Cells(5).Controls)
                                    dtView(0)("matunit") = GetDDLText(row.Cells(5).Controls)
                                    dtView(0)("sodtlnote") = GetTextBoxValue(row.Cells(6).Controls)
                                    If dtView2.Count > 0 Then
                                        dtView2(0)("soqty") = ToDouble(GetTextBoxValue(row.Cells(4).Controls))
                                        dtView2(0)("matunitoid") = GetDDLValue(row.Cells(5).Controls)
                                        dtView2(0)("matunit") = GetDDLText(row.Cells(5).Controls)
                                        dtView2(0)("sodtlnote") = GetTextBoxValue(row.Cells(6).Controls)
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function GetDDLValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
            End If
        Next
        Return sReturn
    End Function

    Private Function GetDDLText(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedItem.Text
            End If
        Next
        Return sReturn
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If matoid.Text = "" Then
            sError &= "- Please select MATERIAL field!<BR>"
        End If
        If soqty.Text = "" Then
            sError &= "- Please fill QUANTITY field!<BR>"
        Else
            If ToDouble(soqty.Text) <= 0 Then
                sError &= "- QUANTITY must be more than 0!<BR>"
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If groupoid.SelectedValue = "" Then
            sError &= "- Please select DIVISION field!<BR>"
        End If
        If sodate.Text = "" Then
            sError &= "- Please fill SO DATE field!<BR>"
        Else
            If Not IsValidDate(sodate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- SO DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        'If pic.Text = "" Then
        '    sError &= "- Please fill PIC field!<BR>"
        'End If
        If somstnote.Text.Length > 100 Then
            sError &= "- HEADER NOTE must be less than 100 characters!<BR>"
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill DETAIL DATA!<BR>"
        Else
            Dim objTbl As DataTable = Session("TblDtl")
            If objTbl.Rows.Count <= 0 Then
                sError &= "- Please fill DETAIL DATA!<BR>"
            End If
        End If

        If sError <> "" Then
            showMessage(sError, 2)
            somststatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function GetOidDetail() As String
        Dim sReturn As String = ""
        Dim dv As DataView = Session("TblMst").DefaultView
        dv.RowFilter = "CheckValue='True'"
        For C1 As Integer = 0 To dv.Count - 1
            sReturn &= dv(C1)("somstoid").ToString & ","
        Next
        dv.RowFilter = ""
        If sReturn <> "" Then
            sReturn = Left(sReturn, sReturn.Length - 1)
        End If
        Return sReturn
    End Function

    Private Function GenerateSOFGNo() As String
        Dim sNo As String = "BS-" & Format(GetServerTime(), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(sono, " & DefaultFormatCounter & ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnsomst WHERE cmpcode='" & CompnyCode & "' AND sono LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckSOStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trnsomst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND somststatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnSOBufferStockItem.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lbSOInProcess.Visible = True
            lbSOInProcess.Text = "You have " & GetStrData(sSql) & " In Process SO Finish Good that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
        sSql = "SELECT COUNT(*) FROM QL_trnsomst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND somststatus='In Approval'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnSOBufferStockItem.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lbSOInApproval.Visible = True
            lbSOInApproval.Text = "You have " & GetStrData(sSql) & " In Approval SO Finish Good that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitAllDDL()
        'Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If FillDDL(DDLBusUnit, sSql) Then
            InitDDLDiv()
        End If
        'PIC
        sSql = "SELECT personoid, UPPER(personname) AS personname FROM QL_mstperson WHERE activeflag='ACTIVE' ORDER BY personname"
        FillDDL(DDLPIC, sSql)
    End Sub

    Private Sub InitDDLDiv()
        sSql = "SELECT 0 AS groupoid, '' groupdesc "
        FillDDL(groupoid, sSql)
        'sSql = "SELECT groupoid, groupdesc FROM QL_mstdeptgroup WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND activeflag='ACTIVE'"
        'FillDDL(groupoid, sSql)
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT som.somstoid AS somstoid, som.sono AS sono, CONVERT(VARCHAR(10), som.sodate, 101) AS sodate, p.personname AS pic, som.somststatus AS somststatus, som.somstnote AS somstnote, div.divname, 'False' AS checkvalue FROM QL_trnsomst som INNER JOIN QL_mstdivision div ON div.cmpcode=som.cmpcode LEFT JOIN QL_mstperson p ON p.personoid=som.approvalpic WHERE ISNULL(sotype,'')='Buffer' AND som.cmpcode='" & CompnyCode & "'"

        sSql &= sSqlPlus & " ORDER BY CONVERT(DATETIME, som.sodate) DESC, som.somstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trnsomst")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "somstoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub InitDDLCat01()
        'Fill DDL Category 1
        sSql = "SELECT cat1oid, cat1code + ' - ' + cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat1res1 IN ('FG', 'WIP') "
        If custres3.Text <> "" Then
            sSql &= " AND ("
            Dim sData() As String = custres3.Text.Split(";")
            For C1 As Integer = 0 To sData.Length - 1
                sSql &= "cat1shortdesc='" & LTrim(sData(C1)) & "'"
                If C1 < sData.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If
        sSql &= " ORDER BY cat1code"
        FillDDL(FilterDDLCat01, sSql)
        If FilterDDLCat01.Items.Count > 0 Then
            InitDDLCat02()
        Else
            FilterDDLCat02.Items.Clear()
            FilterDDLCat03.Items.Clear()
            FilterDDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat02()
        'Fill DDL Category 2
        sSql = "SELECT cat2oid, cat2code + ' - ' + cat2shortdesc FROM QL_mstcat2 WHERE cmpcode='" & CompnyCode & "' AND cat1oid=" & FilterDDLCat01.SelectedValue & " AND activeflag='ACTIVE' ORDER BY cat2code"
        FillDDL(FilterDDLCat02, sSql)
        If FilterDDLCat02.Items.Count > 0 Then
            InitDDLCat03()
        Else
            FilterDDLCat03.Items.Clear()
            FilterDDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat03()
        'Fill DDL Category 3
        sSql = "SELECT cat3oid, cat3code + ' - ' + cat3shortdesc FROM QL_mstcat3 WHERE cmpcode='" & CompnyCode & "' AND cat1oid=" & FilterDDLCat01.SelectedValue & " AND cat2oid=" & FilterDDLCat02.SelectedValue & " AND activeflag='ACTIVE' ORDER BY cat3code"
        FillDDL(FilterDDLCat03, sSql)
        If FilterDDLCat03.Items.Count > 0 Then
            InitDDLCat04()
        Else
            FilterDDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat04()
        'Fill DDL Category 4
        sSql = "SELECT genoid, gencode + ' - ' + gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND gengroup='CAT4' ORDER BY gencode"
        FillDDL(FilterDDLCat04, sSql)
    End Sub

    Private Sub InitDDLUnit(ByVal Oid As String)
        'Fill DDL Unit
        sSql = "SELECT genoid,gendesc FROM QL_mstgen g INNER JOIN QL_mstitem m ON m.itemunit1=g.genoid WHERE m.itemoid='" & Oid & "' UNION ALL SELECT genoid,gendesc FROM QL_mstgen g INNER JOIN QL_mstitem m ON m.itemunit2=g.genoid WHERE m.itemoid='" & Oid & "' UNION ALL SELECT genoid,gendesc FROM QL_mstgen g INNER JOIN QL_mstitem m ON m.itemunit3=g.genoid WHERE m.itemoid='" & Oid & "'"
        FillDDL(sounitoid, sSql)
    End Sub

    Private Sub BindMaterial()
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sSql = "SELECT 'False' AS CheckValue, itemoid AS matoid, itemcode AS matcode, itemlongdescription AS matlongdesc, ISNULL((SELECT SUM(saldoakhir) FROM QL_crdstock crd WHERE crd.cmpcode='" & CompnyCode & "' AND periodacctg IN ('" & sPeriod & "', '" & GetLastPeriod(sPeriod) & "') AND refoid=itemoid AND closingdate='01/01/1900'), 0.0) AS stockqty, 0.0 AS soqty, itemunit1 AS matunitoid, itemunit3 AS matunit2oid, unit3unit1conversion AS itemconvertunit, gendesc AS matunit, '' AS sodtlnote, m.itemnote1 AS itemnote FROM QL_mstitem m INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=m.cmpcode AND c1.cat1oid=itemcat1 AND cat1res1 IN ('FG', 'WIP') INNER JOIN QL_mstgen g ON g.cmpcode=m.cmpcode AND genoid=itemunit1 WHERE m.cmpcode='" & CompnyCode & "' AND m.itemrecordstatus='ACTIVE'"
        If custres3.Text <> "" Then
            sSql &= " AND ("
            Dim sData() As String = custres3.Text.Split(";")
            For C1 As Integer = 0 To sData.Length - 1
                sSql &= "c1.cat1shortdesc='" & LTrim(sData(C1)) & "'"
                If C1 < sData.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If
        sSql &= " ORDER BY itemcode"
        Session("TblMat") = cKon.ambiltabel(sSql, "QL_mstmat")
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("QL_trnsodtl")
        dtlTable.Columns.Add("sodtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("matlongdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("soqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("sounitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("sounit", Type.GetType("System.String"))
        dtlTable.Columns.Add("soqty_unitkecil", Type.GetType("System.Double"))
        dtlTable.Columns.Add("sounit2oid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemconvertunit", Type.GetType("System.Double"))
        dtlTable.Columns.Add("sodtlnote", Type.GetType("System.String"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub ClearDetail()
        sodtlseq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                sodtlseq.Text = objTable.Rows.Count + 1
            Else
            End If
        Else
        End If
        i_u2.Text = "New Detail"
        matoid.Text = ""
        matcode.Text = ""
        matlongdesc.Text = ""
        soqty.Text = ""
        sounitoid.SelectedIndex = -1
        sodtlnote.Text = ""
        gvTblDtl.SelectedIndex = -1
        btnSearchMat.Visible = True
        sounitoid.Items.Clear()
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT som.cmpcode, somstoid AS somstoid, som.periodacctg, somstres1 AS sotype, sono AS sono, sodate AS sodate,  somstnote AS somstnote, somststatus AS somststatus, approvalpic AS pic, som.createuser, som.createtime, som.upduser, som.updtime FROM QL_trnsomst som WHERE somstoid=" & sOid
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = xreader("cmpcode").ToString
                somstoid.Text = Trim(xreader("somstoid").ToString)
                periodacctg.Text = Trim(xreader("periodacctg").ToString)
                sono.Text = Trim(xreader("sono").ToString)
                sodate.Text = Format(xreader("sodate"), "MM/dd/yyyy")
                somstnote.Text = Trim(xreader("somstnote").ToString)
                somststatus.Text = Trim(xreader("somststatus").ToString)
                DDLPIC.SelectedValue = Trim(xreader("pic").ToString)
                DDLBusUnit_SelectedIndexChanged(Nothing, Nothing)
                createuser.Text = Trim(xreader("createuser").ToString)
                createtime.Text = Trim(xreader("createtime").ToString)
                upduser.Text = Trim(xreader("upduser").ToString)
                updtime.Text = Trim(xreader("updtime").ToString)
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False : btnPosting.Visible = False
            Exit Sub
        End Try
        DDLBusUnit.CssClass = "inpTextDisabled"
        DDLBusUnit.Enabled = False
        If somststatus.Text <> "In Process" And somststatus.Text <> "Revised" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnPosting.Visible = False
            btnAddToList.Visible = False
            gvTblDtl.Columns(0).Visible = False
            gvTblDtl.Columns(gvTblDtl.Columns.Count - 1).Visible = False

            lblTrnNo.Text = "SO No."
            somstoid.Visible = False
            sono.Visible = True
        End If
        sSql = "SELECT sod.sodtlseq AS sodtlseq, sod.itemoid AS matoid, itemcode AS matcode, m.itemlongdescription AS matlongdesc, sod.sodtlqty AS soqty, sod.sodtlunitoid AS sounitoid, g.gendesc AS sounit, itemunit3 AS sounit2oid, sod.sodtlnote AS sodtlnote, soqty_unitkecil soqty_unitkecil FROM QL_trnsodtl sod INNER JOIN QL_mstitem m ON m.itemoid=sod.itemoid INNER JOIN QL_mstgen g ON g.genoid=sod.sodtlunitoid WHERE sod.somstoid=" & sOid & " ORDER BY sod.sodtlseq"
        Dim objTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnsodtl")
        Session("TblDtl") = objTbl
        gvTblDtl.DataSource = objTbl
        gvTblDtl.DataBind()
        ClearDetail()
    End Sub

    Private Sub PrintReport(ByVal sOid As String)
        Try
            If cbprice.Checked = False Then
                If DDLTypeCust.SelectedIndex = 0 Then
                    report.Load(Server.MapPath(folderReport & "rptSOBufferStockItem.rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptSOBufferStockItemEn.rpt"))
                End If
            Else
                report.Load(Server.MapPath(folderReport & "rptSOBufferStockItem2.rpt"))
            End If
            Dim sWhere As String
            If Session("CompnyCode") <> CompnyCode Then
                sWhere = " WHERE som.cmpcode='" & Session("CompnyCode") & "'"
            Else
                sWhere = " WHERE som.cmpcode LIKE '%'"
            End If
            If sOid = "" Then
                sWhere &= " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND som.sodate>='" & FilterPeriod1.Text & " 00:00:00' AND som.sodate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sWhere &= " AND som.somststatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Transaction\trnSOBufferStockItem.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND som.createuser='" & Session("UserID") & "'"
                End If
            Else
                sWhere &= " AND som.somstoid IN (" & sOid & ")"
            End If
            report.SetParameterValue("userID", Session("UserID"))
            report.SetParameterValue("userName", GetStrData("SELECT profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "SOFinishGoodPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
        Response.Redirect("~\Transaction\trnSOBufferStockItem.aspx?awal=true")
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim scode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = scode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnSOBufferStockItem.aspx")
        End If
        If checkPagePermission("~\Transaction\trnSOBufferStockItem.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - SO Buffer Stock"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            CheckSOStatus()
            InitAllDDL()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                somstoid.Text = GenerateID("QL_TRNSOMST", CompnyCode)
                sodate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                somststatus.Text = "In Process"
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            gvTblDtl.DataSource = dt
            gvTblDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnSOBufferStockItem.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lkbSOInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSOInProcess.Click
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim nDays As Integer = 7
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, som.updtime, GETDATE()) > " & nDays & " AND som.somststatus='In Process' "
        If checkPagePermission("~\Transaction\trnSOBufferStockItem.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND som.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub lbSOInApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSOInApproval.Click
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 1
        Dim nDays As Integer = 7
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, som.updtime, GETDATE()) > " & nDays & " AND som.somststatus='In Approval' "
        If checkPagePermission("~\Transaction\trnSOBufferStockItem.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND som.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND som.sodate>='" & FilterPeriod1.Text & " 00:00:00' AND som.sodate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND som.somststatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Transaction\trnSOBufferStockItem.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND som.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        Dim sSqlPlus As String = ""
        If checkPagePermission("~\Transaction\trnSOBufferStockItem.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND som.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData((sSqlPlus))
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLDiv()
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        Dim sErr As String = ""
        If sodate.Text = "" Then
            showMessage("Please fill SO Date first!", 2)
            Exit Sub
        Else
            If Not IsValidDate(sodate.Text, "MM/dd/yyyy", sErr) Then
                showMessage("SO Date is invalid. " & sErr, 2)
                Exit Sub
            End If
        End If
        InitDDLCat01()
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : Session("TblMat") = Nothing : Session("TblMatView") = Nothing : gvListMat.DataSource = Nothing : gvListMat.DataBind() : FilterDDLStock.SelectedIndex = -1 : FilterTextStock.Text = "0" : tbData.Text = "5" : gvListMat.PageSize = CInt(tbData.Text)
        cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(4).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    End If
                End If
            Next
            Dim iUnitID As String = e.Row.Cells(5).ToolTip
            Dim cc2 As System.Web.UI.ControlCollection = e.Row.Cells(5).Controls
            For Each myControl As System.Web.UI.Control In cc2
                If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                    iUnitID = CType(myControl, System.Web.UI.WebControls.DropDownList).ToolTip
                End If
            Next

            sSql = "SELECT genoid,gendesc FROM QL_mstgen g INNER JOIN QL_mstitem m ON m.itemunit1=g.genoid WHERE m.itemcode='" & e.Row.Cells(1).Text & "' UNION ALL SELECT genoid,gendesc FROM QL_mstgen g INNER JOIN QL_mstitem m ON m.itemunit2=g.genoid WHERE m.itemcode='" & e.Row.Cells(1).Text & "' UNION ALL SELECT genoid,gendesc FROM QL_mstgen g INNER JOIN QL_mstitem m ON m.itemunit3=g.genoid WHERE m.itemcode='" & e.Row.Cells(1).Text & "'"

            Dim ods As New SqlDataAdapter(sSql, conn)
            Dim objTablee As New DataTable
            ods.Fill(objTablee)

            Dim cc3 As System.Web.UI.ControlCollection = e.Row.Cells(5).Controls
            Dim tempString As String = ""
            For Each myControl As System.Web.UI.Control In cc3
                If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                    For x As Integer = 0 To objTablee.Rows.Count - 1
                        Dim textTemp As String = objTablee.Rows(x).Item("gendesc")
                        Dim oidTemp As String = objTablee.Rows(x).Item("genoid")
                        CType(myControl, System.Web.UI.WebControls.DropDownList).Items.Add(New ListItem(textTemp, oidTemp))
                    Next
                    CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue = iUnitID
                    tempString = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
                End If
            Next
        End If
    End Sub

    Protected Sub FilterDDLCat01_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat01.SelectedIndexChanged
        InitDDLCat02()
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat02_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat02.SelectedIndexChanged
        InitDDLCat03()
        mpeListMat.Show()
    End Sub

    Protected Sub FilterDDLCat03_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat03.SelectedIndexChanged
        InitDDLCat04()
        mpeListMat.Show()
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblMat") Is Nothing Then
            BindMaterial()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim sFilter As String = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%' AND stockqty " & FilterDDLStock.SelectedValue & " " & ToDouble(FilterTextStock.Text)
            If cbCat01.Checked Then
                If FilterDDLCat01.SelectedValue <> "" Then
                    sFilter &= " AND SUBSTRING(matcode, 1, 1)='" & Left(FilterDDLCat01.SelectedItem.Text, 2) & "'"
                Else
                    cbCat01.Checked = False
                End If
            End If
            If cbCat02.Checked Then
                If FilterDDLCat02.SelectedValue <> "" Then
                    sFilter &= IIf(cbCat01.Checked, "", " AND SUBSTRING(matcode, 1, 1)='" & Left(FilterDDLCat01.SelectedItem.Text, 2) & "'") & " AND SUBSTRING(matcode, 3, 2)='" & Left(FilterDDLCat02.SelectedItem.Text, 3) & "'"
                Else
                    cbCat02.Checked = False
                End If
            End If
            If cbCat03.Checked Then
                If FilterDDLCat03.SelectedValue <> "" Then
                    sFilter &= IIf(cbCat01.Checked, "", " AND SUBSTRING(matcode, 1, 1)='" & Left(FilterDDLCat01.SelectedItem.Text, 2) & "'") & IIf(cbCat02.Checked, "", " AND SUBSTRING(matcode, 3, 2)='" & Left(FilterDDLCat02.SelectedItem.Text, 3) & "'") & " AND SUBSTRING(matcode, 4, 1)='" & Tchar(Left(FilterDDLCat03.SelectedItem.Text, 4)) & "'"
                Else
                    cbCat03.Checked = False
                End If
            End If
            If cbCat04.Checked Then
                If FilterDDLCat04.SelectedValue <> "" Then
                    sFilter &= IIf(cbCat01.Checked, "", " AND SUBSTRING(matcode, 1, 2)='" & Left(FilterDDLCat01.SelectedItem.Text, 2) & "'") & IIf(cbCat02.Checked, "", " AND SUBSTRING(matcode, 4, 3)='" & Left(FilterDDLCat02.SelectedItem.Text, 3) & "'") & IIf(cbCat03.Checked, "", " AND SUBSTRING(matcode, 8, 4)='" & Tchar(Left(FilterDDLCat03.SelectedItem.Text, 4)) & "'") & " AND SUBSTRING(matcode, 13, 4)='" & Left(FilterDDLCat04.SelectedItem.Text, 4) & "'"
                Else
                    cbCat04.Checked = False
                End If
            End If
            dtView.RowFilter = sFilter
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : FilterDDLStock.SelectedIndex = -1 : FilterTextStock.Text = "0"
        cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
        FilterDDLCat01.SelectedIndex = -1 : InitDDLCat01()
        'If FilterDDLCat01.SelectedValue = "" Then
        '    Session("EmptyListMat") = "Please input category or check customer tag first!"
        '    showMessage(Session("EmptyListMat"), 2)
        '    Exit Sub
        'Else
        '    InitDDLCat01()
        'End If
        If Session("TblMat") Is Nothing Then
            BindMaterial()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Session("TblMatView") = Session("TblMat")
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matoid=" & dtTbl.Rows(C1)("matoid")
                    objView(0)("CheckValue") = "True"
                    dtTbl.Rows(C1)("CheckValue") = "True"
                    objView.RowFilter = ""
                Next
                Session("TblMat") = objView.ToTable
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matoid=" & dtTbl.Rows(C1)("matoid")
                    objView(0)("CheckValue") = "False"
                    objView(0)("soqty") = 0
                    objView(0)("sodtlnote") = ""
                    dtTbl.Rows(C1)("CheckValue") = "False"
                    dtTbl.Rows(C1)("soqty") = 0
                    dtTbl.Rows(C1)("sodtlnote") = ""
                    objView.RowFilter = ""
                Next
                Session("TblMat") = objView.ToTable
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "CheckValue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : FilterDDLStock.SelectedIndex = -1 : FilterTextStock.Text = "0"
                cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
                FilterDDLCat01.SelectedIndex = -1 : FilterDDLCat01_SelectedIndexChanged(Nothing, Nothing)
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Selected material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub lbShowData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbShowData.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Please find material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If tbData.Text = "" Then
            Session("WarningListMat") = "Please fill total data to be shown!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        Else
            If ToDouble(tbData.Text) <= 0 Then
                Session("WarningListMat") = "Total data to be shown must be more than 0!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat2() Then
            gvListMat.PageSize = Int(tbData.Text)
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub lbAddListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddListMat.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "CheckValue='True'"
            Dim iCheck As Integer = dtView.Count
            If iCheck > 0 Then
                dtView.RowFilter = ""
                dtView.RowFilter = "CheckValue='True' AND soqty>0"
                If dtView.Count <> iCheck Then
                    Session("WarningListMat") = "SO qty for every checked material data must be more than 0!"
                    showMessage(Session("WarningListMat"), 2)
                    dtView.RowFilter = ""
                    Exit Sub
                End If
                If Session("TblDtl") Is Nothing Then
                    CreateTblDetail()
                End If
                Dim objTable As DataTable
                objTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                Dim objRow As DataRow
                Dim counter As Integer = objTable.Rows.Count
                For C1 As Integer = 0 To dtView.Count - 1
                    dv.RowFilter = "matoid=" & dtView(C1)("matoid")
                    If dv.Count > 0 Then
                        dv.AllowEdit = True
                        dv(0)("soqty") = dtView(C1)("soqty")
                        dv(0)("sodtlnote") = dtView(C1)("sodtlnote")
                        dv(0)("sounitoid") = dtView(C1)("matunitoid")
                        dv(0)("sounit") = dtView(C1)("matunit")
                        If dtView(C1)("matunitoid") <> dtView(C1)("matunit2oid") Then
                            dv(0)("soqty_unitkecil") = ToDouble(dtView(C1)("soqty")) * ToDouble(dtView(C1)("itemconvertunit"))
                        Else
                            dv(0)("soqty_unitkecil") = dtView(C1)("soqty")
                        End If
                    Else
                        counter += 1
                        objRow = objTable.NewRow()
                        objRow("sodtlseq") = counter
                        objRow("matoid") = dtView(C1)("matoid")
                        objRow("matcode") = dtView(C1)("matcode").ToString
                        objRow("matlongdesc") = dtView(C1)("matlongdesc").ToString
                        objRow("soqty") = dtView(C1)("soqty")
                        objRow("sounitoid") = dtView(C1)("matunitoid")
                        objRow("sounit2oid") = dtView(C1)("matunit2oid")
                        objRow("sounit") = dtView(C1)("matunit").ToString
                        If dtView(C1)("matunitoid") <> dtView(C1)("matunit2oid") Then
                            objRow("soqty_unitkecil") = ToDouble(dtView(C1)("soqty")) * ToDouble(dtView(C1)("itemconvertunit"))
                        Else
                            objRow("soqty_unitkecil") = dtView(C1)("soqty")
                        End If
                        objRow("itemconvertunit") = dtView(C1)("itemconvertunit")
                        objRow("sodtlnote") = dtView(C1)("sodtlnote")
                        objTable.Rows.Add(objRow)
                    End If
                    dv.RowFilter = ""
                Next
                dtView.RowFilter = ""
                Session("TblDtl") = objTable
                gvTblDtl.DataSource = objTable
                gvTblDtl.DataBind()
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                ClearDetail()
            Else
                Session("WarningListMat") = "Please select material to add to list!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        End If
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            Dim objTable As DataTable
            objTable = Session("TblDtl")
            Dim objRow As DataRow
            objRow = objTable.Rows(sodtlseq.Text - 1)
            objRow.BeginEdit()
            objRow("soqty") = ToDouble(soqty.Text)
            objRow("sounitoid") = sounitoid.SelectedValue
            objRow("sounit") = sounitoid.SelectedItem.Text
            If sounitoid.SelectedValue <> sounit2oid.Text Then
                objRow("soqty_unitkecil") = ToDouble(soqty.Text) * ToDouble(itemconvertunit.Text)
            Else
                objRow("soqty_unitkecil") = ToDouble(soqty.Text)
            End If
            objRow("sodtlnote") = sodtlnote.Text
            objRow.EndEdit()
            Session("TblDtl") = objTable
            gvTblDtl.DataSource = objTable
            gvTblDtl.DataBind()
            ClearDetail()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvTblDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTblDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
        End If
    End Sub

    Protected Sub gvTblDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvTblDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("sodtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvTblDtl.DataSource = objTable
        gvTblDtl.DataBind()
        ClearDetail()
    End Sub

    Protected Sub gvTblDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvTblDtl.SelectedIndexChanged
        Try
            sodtlseq.Text = gvTblDtl.SelectedDataKey.Item("sodtlseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable
                objTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "sodtlseq=" & sodtlseq.Text
                matcode.Text = dv.Item(0).Item("matcode").ToString.Trim
                matlongdesc.Text = dv.Item(0).Item("matlongdesc").ToString.Trim
                matoid.Text = dv.Item(0).Item("matoid")
                InitDDLUnit(matoid.Text)
                soqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("soqty")), 4)
                sounitoid.SelectedValue = dv.Item(0).Item("sounitoid")
                'sounit2oid.Text = dv.Item(0).Item("sounit2oid")
                'itemconvertunit.Text = dv.Item(0).Item("itemconvertunit")
                sodtlnote.Text = dv.Item(0).Item("sodtlnote")
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
            Exit Sub
        Finally
            btnSearchMat.Visible = False
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
       If IsInputValid() Then
            sodtloid.Text = GenerateID("QL_TRNSODTL", CompnyCode)
            Dim iAppOid As Integer = GenerateID("QL_APPROVAL", CompnyCode)
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                If CheckDataExists("SELECT COUNT(*) FROM QL_trnsomst WHERE somstoid=" & somstoid.Text) Then
                    somstoid.Text = GenerateID("QL_TRNSOMST", CompnyCode)
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNSOMST", "somstoid", somstoid.Text, "somststatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    somststatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            periodacctg.Text = GetDateToPeriodAcctg(CDate(sodate.Text))
            If somststatus.Text = "Revised" Then
                somststatus.Text = "In Process"
            ElseIf somststatus.Text = "Post" Then
                sono.Text = GenerateSOFGNo()
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trnsomst (cmpcode, somstoid, periodacctg, sotype, sono, sodate,somstnote, somststatus, createuser, createtime, upduser, updtime, custoid, approvalpic) VALUES ('" & DDLBusUnit.SelectedValue & "', " & somstoid.Text & ", '" & periodacctg.Text & "', 'Buffer', '" & sono.Text & "', '" & sodate.Text & "', '" & Tchar(somstnote.Text.Trim) & "', '" & somststatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP,0, " & DDLPIC.SelectedValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & somstoid.Text & " WHERE tablename='QL_TRNSOMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trnsomst SET periodacctg='" & periodacctg.Text & "', sotype='Buffer', sono='" & sono.Text & "', sodate='" & sodate.Text & "', approvalpic=" & DDLPIC.SelectedValue & ", somstnote='" & Tchar(somstnote.Text.Trim) & "', somststatus='" & somststatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND somstoid=" & somstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trnsodtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND somstoid=" & somstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    'sSql = "DELETE FROM QL_trnsoitemcont WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND soitemmstoid=" & somstoid.Text
                    'xCmd.CommandText = sSql
                    'xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = SortingDataTable(Session("TblDtl"), "matcode")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trnsodtl (cmpcode, sodtloid, somstoid, sodtlseq, itemoid, sodtlqty, sodtlunitoid, sodtlnote, sodtlstatus, upduser, updtime, soqty_unitkecil) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C1 + CInt(sodtloid.Text)) & ", " & somstoid.Text & ",  " & C1 + 1 & ", " & objTable.Rows(C1).Item("matoid") & ", " & ToDouble(objTable.Rows(C1).Item("soqty").ToString) & ", " & objTable.Rows(C1).Item("sounitoid") & ", '" & Tchar(objTable.Rows(C1).Item("sodtlnote")) & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(C1).Item("soqty_unitkecil").ToString) & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(sodtloid.Text)) & " WHERE tablename='QL_TRNSODTL' AND cmpcode='" & CompnyCode & "' "
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    somststatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 1)
                conn.Close()
                somststatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & somstoid.Text & ".<BR>"
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trnSOBufferStockItem.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnSOBufferStockItem.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If somstoid.Text.Trim = "" Then
            showMessage("Please select SO Finish Good data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNSOMST", "somstoid", somstoid.Text, "somststatus", updtime.Text, "Approved")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                somststatus.Text = "In Process"
                Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE FROM QL_trnsodtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND somstoid=" & somstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnsomst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND somstoid=" & somstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnSOBufferStockItem.aspx?awal=true")
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        If Session("TblMst") IsNot Nothing Then
            UpdateCheckedValue()
            lblRptOid.Text = GetOidDetail()
            Dim dt As DataTable = Session("TblMst")
            Dim sSOFilter As String = ""
            If lblRptOid.Text <> "" Then
                sSOFilter = " AND somstoid IN (" & lblRptOid.Text & ")"
            End If
            PrintReport(lblRptOid.Text)
        Else
            cProc.SetModalPopUpExtender(btnPnlApproval, pnlApproval, mpeApproval, True)
        End If
    End Sub

    Protected Sub btnCancelPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelPrint.Click
        cProc.SetModalPopUpExtender(btnPnlApproval, pnlApproval, mpeApproval, False)
    End Sub

    Protected Sub btnContinue_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnContinue.Click
        cProc.SetModalPopUpExtender(btnPnlApproval, pnlApproval, mpeApproval, False)
        PrintReport(lblRptOid.Text)
    End Sub

    Protected Sub btnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosting.Click
        somststatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub
#End Region

End Class
