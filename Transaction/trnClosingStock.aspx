<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnClosingStock.aspx.vb" Inherits="Transaction_ClosingStock" title="" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Closing Stock" CssClass="Title" ForeColor="SaddleBrown"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Closing Stock :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                <asp:UpdatePanel id="upPRClosing" runat="server">
                    <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3></TD></TR><TR id="BusinessUnitA" runat="server" visible="true"><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="Label8" runat="server" Text="Business Unit"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="FilterDDLBusUnit" runat="server" CssClass="inpText" Width="300px" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label2" runat="server" Text="Closing Period"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLPeriod" runat="server" CssClass="inpText" Width="150px">
            </asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=3></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left colSpan=3>Processed By <asp:Label id="upduser" runat="server"></asp:Label>&nbsp;On <asp:Label id="updtime" runat="server"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:ImageButton id="imbClose" runat="server" ImageUrl="~/Images/Close.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" CausesValidation="False"></asp:ImageButton></TD></TR></TBODY></TABLE>
                        <div style="width: 100%; text-align: center">
                            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upPRClosing">
                                <ProgressTemplate>
                                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                    </div>
                                    <div id="processMessage" class="processMessage">
                                        <span style="font-weight: bold; font-size: 10pt; color: purple">
                                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />
                                        Please Wait .....</span><br />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </div>
</contenttemplate>
                </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
        <tr>
            <td align="left">
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td align="left">
    <asp:UpdatePanel id="upOutTrn" runat="server">
        <contenttemplate>
<asp:Panel id="pnlOutTrn" runat="server" CssClass="modalBox" Width="500px" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblOutTrn" runat="server" Font-Size="Small" Font-Bold="True" CssClass="Important" Text="There are several transaction below that must be posted or approved before closing stock" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3><asp:GridView id="gvOutTrn" runat="server" ForeColor="#333333" Width="99%" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" ForeColor="White" Width="5%"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnname" HeaderText="Form Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnqty" HeaderText="Total">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup" ForeColor="White" Width="15%"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" ForeColor="White" Width="20%"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                <asp:Label ID="Label114" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD style="TEXT-ALIGN: center" class="Label" colSpan=3><asp:ImageButton id="imbOKOutTrn" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHideOutTrn" runat="server" ForeColor="Transparent" Visible="False"></asp:Button><ajaxToolkit:ModalPopupExtender id="mpeOutTrn" runat="server" PopupControlID="pnlOutTrn" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblOutTrn" TargetControlID="btnHideOutTrn"></ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
    </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td align="left">
    <asp:UpdatePanel id="upPopUpConfirm" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpConfirm" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaptionConfirm" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIconConfirm" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpConfirm" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbYesPopUpConfirm" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbNoPopUpConfirm" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpConfirm" runat="server" DropShadow="True" PopupControlID="pnlPopUpConfirm" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaptionConfirm" Drag="True" TargetControlID="bePopUpConfirm"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpConfirm" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>

