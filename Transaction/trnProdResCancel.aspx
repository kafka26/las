<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnProdResCancel.aspx.vb" Inherits="Transaction_ProductionResultCancellation" title="" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Production Result Cancellation" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Production Result Cancellation :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                <asp:UpdatePanel id="upProdResCancel" runat="server">
                    <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Label id="Label4" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Production Result Header"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 14%" class="Label"></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label"><asp:Label id="prodresmstoid" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 14%" class="Label"></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label"><asp:Label id="posttime" runat="server" Visible="False" __designer:wfdid="w1"></asp:Label> <asp:Label id="deptoid" runat="server" Visible="False" __designer:wfdid="w1"></asp:Label></TD></TR><TR><TD class="Label"><asp:Label id="Label8" runat="server" Text="Business Unit"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label"><asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpText" Width="305px" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label"></TD><TD class="Label" align=center></TD><TD class="Label"></TD></TR><TR><TD class="Label"><asp:Label id="Label2" runat="server" Text="Result No."></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label"><asp:TextBox id="prodresno" runat="server" CssClass="inpTextDisabled" Width="260px" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnFindResult" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClearResult" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD class="Label"><asp:Label id="Label5" runat="server" Text="Result Date"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label"><asp:TextBox id="prodresdate" runat="server" CssClass="inpTextDisabled" Width="80px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label"><asp:Label id="Label3" runat="server" Text="Department"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label"><asp:TextBox id="deptname" runat="server" CssClass="inpTextDisabled" Width="300px" Enabled="False"></asp:TextBox></TD><TD class="Label"><asp:Label id="Label10" runat="server" Text="Group Name"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label"><asp:TextBox id="suppname" runat="server" CssClass="inpTextDisabled" Width="300px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label"><asp:Label id="Label6" runat="server" Text="Cancel Reason"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label"><asp:TextBox id="prodresmstnote" runat="server" CssClass="inpText" Width="300px" MaxLength="100"></asp:TextBox></TD><TD class="Label"><asp:Label id="Label7" runat="server" Text="Status"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label"><asp:TextBox id="prodresmststatus" runat="server" CssClass="inpTextDisabled" Width="80px" Enabled="False"></asp:TextBox></TD></TR></TBODY></TABLE></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Label id="Label9" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Production Result Detail"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:GridView id="gvDtl" runat="server" ForeColor="#333333" Width="100%" PageSize="5" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
                <RowStyle BackColor="#E3EAEB" />
                <Columns>
                    <asp:BoundField DataField="prodresdtlseq" HeaderText="No.">
                        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="wono" HeaderText="KIK No.">
                        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="wodate" HeaderText="KIK Date">
                        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="todeptname" HeaderText="To Department">
                        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="prodresqty" HeaderText="Result Qty">
                        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Right" />
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="prodresunit" HeaderText="Unit">
                        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="prodresdtlnote" HeaderText="Note">
                        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                </Columns>
                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#7C6F57" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView> </TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=3></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left colSpan=3>Processed By <asp:Label id="upduser" runat="server"></asp:Label>&nbsp;On <asp:Label id="updtime" runat="server"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:ImageButton id="btnProcess" runat="server" ImageUrl="~/Images/process.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE> <DIV style="WIDTH: 100%; TEXT-ALIGN: center" __designer:dtid="562949953421671"><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:dtid="562949953421672" AssociatedUpdatePanelID="upProdResCancel" __designer:wfdid="w3"><ProgressTemplate __designer:dtid="562949953421673">
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter" __designer:dtid="562949953421674"></DIV><DIV id="processMessage" class="processMessage" __designer:dtid="562949953421675"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple" __designer:dtid="562949953421676"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:dtid="562949953421677" __designer:wfdid="w5"></asp:Image><BR __designer:dtid="562949953421678" />Please Wait .....</SPAN><BR __designer:dtid="562949953421679" /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upListResult" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListResult" runat="server" CssClass="modalBox" Width="650px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListResult" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Production Result"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListResult" runat="server" Width="100%" DefaultButton="btnFindListResult"><asp:Label id="Label24" runat="server" Text="Filter :"></asp:Label> <asp:DropDownList id="FilterDDLListResult" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="prodresno">Result No.</asp:ListItem>
<asp:ListItem Value="deptname">Department</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListResult" runat="server" CssClass="inpText" Width="200px"></asp:TextBox> <asp:ImageButton id="btnFindListResult" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListResult" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" align=left colSpan=3><asp:GridView id="gvListResult" runat="server" BorderColor="Black" ForeColor="#333333" Width="99%" PageSize="5" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" DataKeyNames="prodresmstoid,prodresno,prodresdate,deptname,suppname,prodresmststatus,updtime,deptoid" BorderStyle="None">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="prodresno" HeaderText="Result No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prodresdate" HeaderText="Result Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="deptname" HeaderText="Department">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                            <asp:Label ID="lblInfoOnListGV" runat="server" CssClass="Important" Text="There is no data !!"></asp:Label>
                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListResult" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHideListResult" runat="server" Visible="False"></asp:Button><ajaxToolkit:ModalPopupExtender id="mpeListResult" runat="server" TargetControlID="btnHideListResult" PopupDragHandleControlID="lblListResult" BackgroundCssClass="modalBackground" PopupControlID="pnlListResult"></ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

