<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnMA.aspx.vb" Inherits="Transaction_MaterialAdjustment" title="" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" colspan="2" valign="middle">
                <asp:Label ID="Label1" runat="server" CssClass="Title" Font-Bold="False" ForeColor="SaddleBrown"
                    Text=".: Stock Adjustment"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="center" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Stock Adjustment :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
<asp:Panel id="pnlList" runat="server" Width="100%" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 10%" class="Label" align=left><asp:Label id="Label4x" runat="server" Text="Filter"></asp:Label></TD><TD style="WIDTH: 2%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLMst" runat="server" CssClass="inpText">
                                <asp:ListItem Value="a.resfield1">Draft No</asp:ListItem>
                                <asp:ListItem Value="a.stockadjno">Adj. No</asp:ListItem>
                            </asp:DropDownList> <asp:TextBox id="FilterTextMst" runat="server" CssClass="inpText" Width="200px"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text="Periode"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="60px" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label5x" runat="server" Text="to"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="60px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label9" runat="server" Text="(MM/dd/yyyy)" CssClass="Important"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label10" runat="server" Text="Status"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText">
                                        <asp:ListItem Value="%">ALL</asp:ListItem>
                                        <asp:ListItem>In Process</asp:ListItem>
                                        <asp:ListItem>In Approval</asp:ListItem>
                                        <asp:ListItem>Approved</asp:ListItem>
                                        <asp:ListItem>Rejected</asp:ListItem>
                                        <asp:ListItem>Revised</asp:ListItem>
                                    </asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" Enabled="True" Format="MM/dd/yyyy" PopupButtonID="imbDate1" TargetControlID="FilterPeriod1">
                            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" Enabled="True" TargetControlID="FilterPeriod1" Mask="99/99/9999" MaskType="Date" CultureTimePlaceholder="" CultureThousandsPlaceholder="" CultureDecimalPlaceholder="" CultureDatePlaceholder="" CultureDateFormat="" CultureCurrencySymbolPlaceholder="" CultureAMPMPlaceholder="">
                            </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" Enabled="True" Format="MM/dd/yyyy" PopupButtonID="imbDate2" TargetControlID="FilterPeriod2">
                            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" Enabled="True" TargetControlID="FilterPeriod2" Mask="99/99/9999" MaskType="Date" CultureTimePlaceholder="" CultureThousandsPlaceholder="" CultureDecimalPlaceholder="" CultureDatePlaceholder="" CultureDateFormat="" CultureCurrencySymbolPlaceholder="" CultureAMPMPlaceholder="">
                            </ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnPrintHdr" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvList" runat="server" Width="100%" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="8" DataKeyNames="stockadjno" AllowSorting="True">
                                <PagerSettings FirstPageText="First" LastPageText="Last" />
                                <RowStyle BackColor="#EFF3FB" Font-Size="X-Small" />
                                <EmptyDataRowStyle BackColor="#FFFFC0" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Draft No">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lkbSelect" runat="server" OnClick="lkbSelect_Click" Text='<%# Eval("resfield1") %>'
                                                ToolTip="<%# GetTransID() %>"></asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="stockadjno" HeaderText="Adj. No" SortExpression="stockadjno">
                                        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="stockadjdate" HeaderText="Adj. Date" SortExpression="stockadjdate">
                                        <HeaderStyle CssClass="gvhdr" Font-Bold="True" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="divname" HeaderText="Business Unit" SortExpression="divname">
                                        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="adjcount" HeaderText="Adj Count" SortExpression="adjcount">
                                        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Right" Width="100px" />
                                        <ItemStyle HorizontalAlign="Right" Width="100px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="stockadjstatus" HeaderText="Status" SortExpression="stockadjstatus">
                                        <HeaderStyle CssClass="gvhdr" Font-Bold="True" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="cbGVMst" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# GetTransID() %>' />
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Center" Width="25px" />
                                        <ItemStyle HorizontalAlign="Center" Width="25px" />
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="Sienna" Font-Bold="True" ForeColor="White" HorizontalAlign="Right" />
                                <EmptyDataTemplate>
                                    <asp:Label ID="lblEmpty" runat="server" CssClass="Important" Text="No data found."></asp:Label>
                                </EmptyDataTemplate>
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White" />
                                <EditRowStyle BackColor="#2461BF" />
                                <AlternatingRowStyle BackColor="White" />
                            </asp:GridView> </TD></TR><TR><TD class="Label" align=left colSpan=4><asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="btnPrintHdr"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="imgForm" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">Form Stock Adjustment :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
<asp:UpdatePanel id="UpdatePanel1" runat="server"><ContentTemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 14%" class="Label" align=left></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:RadioButton id="rbAdjust" runat="server" ForeColor="Black" Font-Size="Small" Font-Bold="True" Text="ADJUSTMENT" GroupName="AdjustGroup" Checked="True"></asp:RadioButton> <asp:RadioButton id="rbInit" runat="server" ForeColor="Black" Font-Size="Small" Font-Bold="True" Text="INITIAL" GroupName="AdjustGroup"></asp:RadioButton></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label21" runat="server" Text="Business Unit"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="busunit" runat="server" CssClass="inpText" AutoPostBack="True" Width="184px"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblNo" runat="server" Text="Draft No"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:Label id="resfield1" runat="server"></asp:Label><asp:TextBox id="stockadjno" runat="server" CssClass="inpTextDisabled" Width="125px" Enabled="False" Visible="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label11" runat="server" Text="Adj. Date"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="stockadjdate" runat="server" CssClass="inpTextDisabled" Width="60px" Enabled="False" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:Label id="Label8" runat="server" Text="(MM/dd/yyyy)" CssClass="Important" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label2" runat="server" Text="Status"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="stockadjstatus" runat="server" CssClass="inpTextDisabled" Width="75px" Enabled="False">In Process</asp:TextBox></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><ajaxToolkit:CalendarExtender id="CalendarExtender3" runat="server" TargetControlID="stockadjdate" PopupButtonID="imbDate" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" TargetControlID="stockadjdate" MaskType="Date" Mask="99/99/9999" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Label id="Label7" runat="server" ForeColor="Black" Font-Size="Small" Font-Bold="True" Text="Find Stock Filter :" Font-Underline="True"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label5" runat="server" Text="Type"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="ddlType" runat="server" CssClass="inpText" AutoPostBack="True">
<asp:ListItem Value="FINISH GOOD">Finish Good</asp:ListItem>
<asp:ListItem Value="RAW MATERIAL">Raw Material</asp:ListItem>
</asp:DropDownList> <asp:Label id="lblWarnType" runat="server" CssClass="Important"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text="Warehouse"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="FilterWarehouse" runat="server" CssClass="inpText" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label4" runat="server" Text="Filter"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><DIV style="TEXT-ALIGN: left"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 10%" class="Label" align=left><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText">
<asp:ListItem Value="code">Code</asp:ListItem>
    <asp:ListItem Value="oldcode">Old Code</asp:ListItem>
<asp:ListItem Value="longdescription">Description</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left><asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="300px"></asp:TextBox></TD></TR></TBODY></TABLE></DIV></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbCat01" runat="server" Text="Category 1"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="FilterDDLCat01" runat="server" CssClass="inpText" Width="250px" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbCat02" runat="server" Text="Category 2"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="FilterDDLCat02" runat="server" CssClass="inpText" Width="250px" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbCat03" runat="server" Text="Category 3"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="FilterDDLCat03" runat="server" CssClass="inpText" Width="250px" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbCat04" runat="server" Text="Category 4" Visible="False"></asp:CheckBox></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="FilterDDLCat04" runat="server" CssClass="inpText" Width="250px" AutoPostBack="True" Visible="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=right><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD class="Label" align=left><asp:ImageButton id="imbFind" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="lblCount" runat="server"></asp:Label></TD><TD class="Label" align=right><asp:ImageButton id="imbAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD></TR></TBODY></TABLE></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=right><asp:ImageButton id="btnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:GridView id="gvFindCrd" runat="server" Width="100%" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<PagerSettings Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
    <asp:BoundField DataField="itemoldcode" HeaderText="Old Code">
        <HeaderStyle CssClass="gvhdr" Font-Size="X-Small" />
        <ItemStyle Font-Size="X-Small" />
    </asp:BoundField>
<asp:BoundField DataField="matlongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="lastqty" HeaderText="Current Qty">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="New Qty"><ItemTemplate>
<asp:TextBox id="qtyin" runat="server" Text="<%# GetAdjQty() %>" CssClass="<%# GetCssClass() %>" Width="75px" Enabled="<%# GetEnabled() %>" ToolTip='<%# Eval("refoid") %>' MaxLength="13"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender1" runat="server" TargetControlID="qtyin" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Reason" Visible="False"><ItemTemplate>
<asp:DropDownList id="ddlReason" runat="server" CssClass="inpText" ToolTip='<%# Eval("reason") %>'></asp:DropDownList>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Note"><ItemTemplate>
<asp:TextBox id="Note" runat="server" Text='<%# Eval("note") %>' CssClass="inpText" Width="150px" MaxLength="50"></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="location" HeaderText="Location">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><EditItemTemplate>
<asp:TextBox id="TextBox1" runat="server"></asp:TextBox> 
</EditItemTemplate>
<HeaderTemplate>
<asp:CheckBox id="chkSelectAll" runat="server" AutoPostBack="True" ToolTip="Select/Unselect All for Current Page" OnCheckedChanged="chkSelectAll_CheckedChanged"></asp:CheckBox> 
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="chkSelected" runat="server" ToolTip='<%# Eval("seq") %>' Checked="<%# GetSelected() %>"></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Label id="lblTitleDtl" runat="server" Font-Bold="True" Text="Detail Stock Adjustment :" Font-Underline="True"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:GridView id="gvDtl" runat="server" Width="100%" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
<PagerSettings Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="Gainsboro"></EmptyDataRowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="refname" HeaderText="Type">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="refcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
    <asp:BoundField DataField="itemoldcode" HeaderText="Old Code">
        <HeaderStyle CssClass="gvhdr" />
    </asp:BoundField>
<asp:BoundField DataField="reflongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="location" HeaderText="Location">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reason" HeaderText="Reason" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="lastqty" HeaderText="Current Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="adjqty" HeaderText="New Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Note">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:LinkButton id="lkbDelete" onclick="lkbDelete_Click" runat="server" ForeColor="Red" CausesValidation="False" Text="X" ToolTip='<%# Eval("seq") %>' CommandName="Delete"></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Medium" ForeColor="Red" Width="25px"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label6" runat="server" Text="No Stock Adjustment detail." CssClass="Important"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left colSpan=3>Created By <asp:Label id="createuser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left colSpan=3>Last Updated By <asp:Label id="upduser" runat="server" Font-Bold="True">-</asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True">-</asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:ImageButton id="imbSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbClear" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbPosting" runat="server" ImageUrl="~/Images/sendapproval.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:ImageButton id="imbPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="imbPrint"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="imbSave"></asp:PostBackTrigger>
</Triggers>
</asp:UpdatePanel> 
</ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
                
            </td>
        </tr>
        <tr>
            <td align="left">
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Width="700px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 40px; TEXT-ALIGN: center"><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" Height="24px"></asp:Image></TD><TD style="WIDTH: 660px; TEXT-ALIGN: left" class="Label" align=left><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2><asp:Label id="lblState" runat="server" CssClass="Important" Visible="False"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" DropShadow="True" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True" TargetControlID="bePopUpMsg"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel></td>
        </tr>
    </table>
</asp:Content>

