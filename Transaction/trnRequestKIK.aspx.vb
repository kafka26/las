Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_RawMaterialRequest
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetTextValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If matoid.Text = "" Then
            sError &= "- Please select MATERIAL field!<BR>"
        End If
        If reqqty.Text = "" Then
            sError &= "- Please fill QTY field!<BR>"
        Else
            If ToDouble(reqqty.Text) <= 0 Then
                sError &= "- QTY field must be more than 0!<BR>"
            Else
                Dim sErrReply As String = ""
                If Not isLengthAccepted("reqrawqty", "QL_trnreqrawdtl", ToDouble(reqqty.Text), sErrReply) Then
                    sError &= "- QTY field must be less than MAX QTY (" & sErrReply & ") allowed stored in database!<BR>"
                Else
                    If ToDouble(reqqty.Text) > ToDouble(reqmaxqty.Text) Then
                        If reftype.Text = "Detail 3" Then
                            Dim dTol As Double = (GetQtyKIK(refoid.Text) * GetTolerance()) / 100
                            If ToDouble(reqqty.Text) > ToDouble(reqmaxqty.Text) + dTol Then
                                sError &= "- QTY field must be less than MAX QTY + TOLERANCE (" & ToMaskEdit(dTol, 4) & ")!<BR>"
                            End If
                        Else
                            sError &= "- QTY field must be less than MAX QTY!<BR>"
                        End If
                    Else
                        If Not IsQtyRounded(ToDouble(reqqty.Text), ToDouble(reqlimitqty.Text)) Then
                            sError &= "- QTY field must be rounded by ROUNDING QTY!<BR>"
                        End If
                    End If
                End If
            End If
        End If
        If requnitoid.SelectedValue = "" Then
            sError &= "- Please select UNIT field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If deptoid.SelectedValue = "" Then
            sError &= "- Please select DEPARTMENT field!<BR>"
        End If
        If reqdate.Text = "" Then
            sError &= "- Please fill REQUEST DATE field!<BR>"
            sErr = "None"
        Else
            If Not IsValidDate(reqdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- REQUEST DATE is invalid! " & sErr & "<BR>"
            End If
        End If
        If wodtl2oid.Text = "" Then
            sError &= "- Please select KIK NO. field!<BR>"
        Else
            If sErr = "" Then
                If CDate(wodate.Text) > CDate(reqdate.Text) Then
                    sError &= "- REQUEST DATE must be more or equal than KIK DATE!<BR>"
                End If
            End If
        End If
        If reqwhoid.SelectedValue = "" Then
            sError &= "- Please select WAREHOUSE field!<BR>"
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill Detail Data!<BR>"
        Else
            Dim dtTbl As DataTable = Session("TblDtl")
            If dtTbl.Rows.Count <= 0 Then
                sError &= "- Please fill Detail Data!<BR>"
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            reqmststatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function GetTolerance() As Double
        Try
            GetTolerance = ToDouble(GetStrData("SELECT TOP 1 gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND gengroup='REQUEST KIK TOLERANCE (%)' ORDER BY updtime DESC"))
        Catch ex As Exception
            GetTolerance = 0
        End Try
    End Function

    Private Function GetQtyKIK(ByVal sOid As String) As Double
        GetQtyKIK = ToDouble(GetStrData("SELECT wodtl4qty FROM QL_trnwodtl4 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl4oid=" & sOid & ""))
    End Function

    Private Function isMaterialSisaAvaliable(ByVal sOid As String()) As Boolean
        For C1 As Integer = 0 To sOid.Length

        Next
        Return True
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckRequestStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trnreqmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND reqmststatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnRequestKIK.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lkbReqInProcess.Visible = True
            lkbReqInProcess.Text = "You have " & GetStrData(sSql) & " In Process  Material Request data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitALLDDL()
        ' Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)
        ' Fill DDL Unit
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='UNIT' AND activeflag='ACTIVE'"
        FillDDL(requnitoid, sSql)
        FillDDL(stockunitoid, sSql)
        ' Fill DDL Warehouse
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='WAREHOUSE' AND activeflag='ACTIVE'"
        FillDDL(reqwhoid, sSql)
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT div.divname, reqm.reqmstoid AS reqrawmstoid, reqm.reqno AS reqrawno, CONVERT(VARCHAR(10), reqm.reqdate, 101) reqrawdate, wom.wono, d.deptname, reqm.reqmststatus AS reqrawmststatus, reqm.reqmstnote AS reqrawmstnote, g.gendesc AS reqrawwh, 'False' AS checkvalue, reqmstres3 AS itemlongdesc FROM QL_trnreqmst reqm INNER JOIN QL_trnwomst wom ON wom.cmpcode=reqm.cmpcode AND wom.womstoid=reqm.womstoid INNER JOIN QL_mstdept d ON d.cmpcode=reqm.cmpcode AND d.deptoid=reqm.deptoid INNER JOIN QL_mstgen g ON g.genoid=reqm.reqwhoid INNER JOIN QL_mstdivision div ON reqm.cmpcode=div.cmpcode WHERE"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " reqm.cmpcode='" & Session("CompnyCode") & "'"
        Else
            sSql &= " reqm.cmpcode LIKE '%'"
        End If
        sSql &= sSqlPlus & " ORDER BY reqm.reqmstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trnreqrawmst")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "reqrawmstoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub PrintReport()
        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("reqrawmstoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            report.Load(Server.MapPath(folderReport & "rptReqRaw.rpt"))
            Dim sWhere As String = " WHERE"
            If Session("CompnyCode") <> CompnyCode Then
                sWhere &= " reqm.cmpcode='" & Session("CompnyCode") & "'"
            Else
                sWhere &= " reqm.cmpcode LIKE '%'"
            End If
            If sOid = "" Then
                sWhere = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND reqm.reqdate>='" & FilterPeriod1.Text & " 00:00:00' AND reqm.reqdate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sWhere &= " And reqm.reqmststatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Transaction\trnRequestKIK.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND reqm.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sWhere &= " AND reqm.reqmstoid IN (" & sOid & ")"
            End If
            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "RawMatRequestPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
        Response.Redirect("~\Transaction\trnRequestKIK.aspx?awal=true")
    End Sub

    Private Sub BindListKIK()
        sSql = "SELECT DISTINCT wom.womstoid, wono, CONVERT(VARCHAR(10), wodate, 101) AS wodate,wom.womstres2 itemlongdesc FROM QL_trnwomst wom INNER JOIN QL_trnwodtl3 wod3 ON wod3.cmpcode=wom.cmpcode AND wod3.womstoid=wom.womstoid WHERE wom.cmpcode='" & DDLBusUnit.SelectedValue & "' AND womststatus='Post' AND " & FilterDDLListKIK.SelectedValue & " LIKE '%" & Tchar(FilterTextListKIK.Text) & "%' AND wod3.wodtl3reqflag='' ORDER BY wono"
        FillGV(gvListKIK, sSql, "QL_trnwomst")
    End Sub

    Private Sub InitDDLDept()
        ' Fill DDL Department
        Dim sAdd As String = ""
        Dim sAdd2 As String = ""
        If i_u.Text = "Update Data" Then
            sAdd = " OR deptoid IN (SELECT deptoid FROM QL_trnreqmst WHERE reqmstoid=" & reqmstoid.Text & " AND womstoid=" & womstoid.Text & ")"
        Else
            sAdd2 = " AND wodtl3reqflag=''"
        End If
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND (deptoid IN (SELECT deptoid FROM QL_trnwodtl2 wod2 INNER JOIN QL_trnwodtl3 wod3 ON wod3.cmpcode=wod2.cmpcode AND wod3.wodtl1oid=wod2.wodtl1oid WHERE wod2.cmpcode='" & DDLBusUnit.SelectedValue & "' AND wod2.womstoid=" & womstoid.Text & " " & sAdd2 & " AND wodtl2type='FG')" & sAdd & ") ORDER BY deptname"
        FillDDL(deptoid, sSql)
    End Sub

    Private Sub ClearDetail()
        i_u2.Text = "New Detail"
        reqdtlseq.Text = "1"
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            If dt.Rows.Count > 0 Then
                reqdtlseq.Text = dt.Rows.Count + 1
                EnableHeader(False)
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        matoid.Text = ""
        matcode.Text = ""
        matlongdesc.Text = ""
        refoid.Text = ""
        reftype.Text = ""
        reqqty.Text = ""
        reqmaxqty.Text = ""
        requnitoid.SelectedIndex = -1
        reqlimitqty.Text = ""
        reqdtlnote.Text = ""
        gvReqDtl.SelectedIndex = -1
        btnSearchMat.Visible = True
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal : DDLBusUnit.CssClass = sCss
        deptoid.Enabled = bVal : deptoid.CssClass = sCss
        btnSearchKIK.Visible = bVal : btnClearKIK.Visible = bVal
        reqwhoid.Enabled = bVal : reqwhoid.CssClass = sCss
    End Sub

    Private Sub BindMaterial()
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        Dim iWHSisaProd As String = GetStrData("SELECT TOP 1 genother1 FROM QL_mstgen WHERE gengroup='WH SISA PROD' AND activeflag='ACTIVE' ORDER BY updtime DESC")
        If iWHSisaProd = "" Then
            showMessage("- Please SETUP WAREHOUSE for SISA PRODUKSI in Master General Group (WH SISA PROD) First!", 2)
            Exit Sub
        End If
        Dim sError As String = ""
        Dim bCheck As Boolean = False
        Dim sAdd As String = ""
        If Session("oid") IsNot Nothing And Session("oid") <> "" Then
            sAdd = " AND reqd.reqmstoid<>" & Session("oid")
        End If
        sSql = "SELECT 'False' AS checkvalue, 'Detail 5' AS reftype, wod3.wodtl3oid AS refoid, m.itemoid AS matoid, m.itemCode AS matcode, m.itemLongDescription AS matlongdesc, 0.0 AS reqqty, (wod3.wodtl3qty - ISNULL((SELECT SUM(reqd.reqqty) FROM QL_trnreqdtl reqd INNER JOIN QL_trnreqmst reqm ON reqm.cmpcode=reqd.cmpcode AND reqm.reqmstoid=reqd.reqmstoid WHERE reqd.cmpcode=wod3.cmpcode AND reqd.reftype='Detail 5' AND reqd.refoid=wod3.wodtl3oid AND reqm.reqmststatus<>'Cancel'), 0)) AS reqmaxqty, wod3.wodtl3unitoid AS matunitoid, g.gendesc AS matunit, '' AS reqdtlnote, m.roundQty AS reqlimitqty, (CASE itemgroup WHEN 'RAW' THEN 'RAW MATERIAL' WHEN 'GEN' THEN 'GEENRAL' WHEN 'WIP' THEN 'WIP' ELSE 'FINISH GOOD' END ) AS itemgroup, CAST (ISNULL((SELECT SUM(crd.saldoakhir) FROM QL_crdstock crd WHERE crd.cmpcode=wod3.cmpcode AND crd.refoid=wod3.wodtl3refoid AND crd.mtrlocoid=" & reqwhoid.SelectedValue & " AND crd.periodacctg IN ('" & sPeriod & "', '" & GetLastPeriod(sPeriod) & "') AND crd.closingdate='01/01/1900'), 0.0) AS DECIMAL (18,2)) AS stockqty, gx.genoid AS stockunitoid, gx.gendesc AS stockunit FROM QL_trnwodtl3 wod3 INNER JOIN QL_mstitem m ON m.itemoid=wod3.wodtl3refoid INNER JOIN QL_mstgen g ON g.genoid=wod3.wodtl3unitoid INNER JOIN QL_mstgen gx ON gx.genoid=m.itemunit1 WHERE wod3.cmpcode='" & CompnyCode & "' AND wod3.wodtl1oid=" & wodtl2oid.Text & " AND wod3.wodtl3reqflag='' "
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstmatraw")
        dt.DefaultView.Sort = "reftype, matcode"
        For C1 As Integer = 0 To dt.Rows.Count - 1
			'If IsStockAvailable(CompnyCode, GetDateToPeriodAcctg(GetServerTime()), dt.Rows(C1)("matoid"), iWHSisaProd, 0.0001) Then
			'    sError &= "- Material <STRONG><B>" & dt.Rows(C1)("matcode") & " " & dt.Rows(C1)("matlongdesc") & "</B></STRONG> is Available In Warehouse SISA PRODUKSI, Please Transfer First and continue Using this Form !<BR>"
			'Else
			'    dt.Rows(C1)("reqqty") = ToDouble(dt.Rows(C1)("reqmaxqty").ToString)
			'End If
        Next
        If sError <> "" Then
            showMessage(sError, 2)
            Exit Sub
        Else
            dt.AcceptChanges()
            Session("TblListMat") = dt
            Session("TblListMatView") = dt
            gvListMat2.DataSource = Session("TblListMatView")
            gvListMat2.DataBind()
        End If
    End Sub

    Private Sub UpdateCheckedMat()
        Dim dv As DataView = Session("TblListMat").DefaultView
        dv.AllowEdit = True
        For C1 As Integer = 0 To gvListMat2.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat2.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        dv.RowFilter = "matoid=" & sOid
                        If cbcheck = True Then
                            dv(0)("checkvalue") = "True"
                            Dim cc2 As System.Web.UI.ControlCollection = row.Cells(3).Controls
                            dv(0)("reqqty") = ToDouble(GetTextValue(cc2))
                            cc2 = row.Cells(8).Controls
                            dv(0)("reqdtlnote") = GetTextValue(cc2)
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                        dv.RowFilter = ""
                    End If
                Next
            End If
        Next
    End Sub

    Private Sub UpdateCheckedMatView()
        Dim dv As DataView = Session("TblListMatView").DefaultView
        dv.AllowEdit = True
        For C1 As Integer = 0 To gvListMat2.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat2.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        dv.RowFilter = "matoid=" & sOid
                        If cbcheck = True Then
                            dv(0)("checkvalue") = "True"
                            Dim cc2 As System.Web.UI.ControlCollection = row.Cells(3).Controls
                            dv(0)("reqqty") = ToDouble(GetTextValue(cc2))
                            cc2 = row.Cells(8).Controls
                            dv(0)("reqdtlnote") = GetTextValue(cc2)
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                        dv.RowFilter = ""
                    End If
                Next
            End If
        Next
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("TabelReqRawDetail")
        dtlTable.Columns.Add("reqdtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("matlongdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("reqqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("requnitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("stockunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("requnit", Type.GetType("System.String"))
        dtlTable.Columns.Add("stockunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("reqdtlnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("refoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("reftype", Type.GetType("System.String"))
        dtlTable.Columns.Add("reqlimitqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("reqmaxqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("reqmaxqtyfix", Type.GetType("System.Double"))
        dtlTable.Columns.Add("stockqty", Type.GetType("System.Double"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub GenerateNoRequest()
        Dim sNo As String = "REQ-" & Format(CDate(reqdate.Text), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(reqno, " & DefaultFormatCounter & ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnreqmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqno LIKE '%" & sNo & "%'"
        reqno.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT DISTINCT reqm.cmpcode, reqm.reqmstoid AS reqrawmstoid, reqm.periodacctg, reqm.reqdate AS reqrawdate, reqm.reqno AS reqrawno, reqm.wodtl2oid, reqm.womstoid, wom.wodate, wom.wono, reqm.deptoid, reqm.reqmstnote AS reqrawmstnote, reqm.reqmststatus AS reqrawmststatus, reqm.createuser, reqm.createtime, reqm.upduser, reqm.updtime, reqm.reqwhoid AS reqrawwhoid, reqmstres3 FROM QL_trnreqmst reqm INNER JOIN QL_trnwodtl2 wod2 ON wod2.cmpcode=reqm.cmpcode AND wod2.wodtl1oid=reqm.wodtl2oid INNER JOIN QL_trnwomst wom ON wom.cmpcode=wod2.cmpcode AND wom.womstoid=wod2.womstoid WHERE reqm.reqmstoid=" & sOid
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = Trim(xreader("cmpcode").ToString)
                DDLBusUnit_SelectedIndexChanged(Nothing, Nothing)
                reqmstoid.Text = Trim(xreader("reqrawmstoid").ToString)
                periodacctg.Text = Trim(xreader("periodacctg").ToString)
                reqdate.Text = Format(xreader("reqrawdate"), "MM/dd/yyyy")
                reqno.Text = Trim(xreader("reqrawno").ToString)
                womstoid.Text = Trim(xreader("womstoid").ToString)
                wodtl2oid.Text = Trim(xreader("wodtl2oid").ToString)
                reqwhoid.SelectedValue = Trim(xreader("reqrawwhoid").ToString)
                wono.Text = Trim(xreader("wono").ToString)
                wodate.Text = Format(xreader("wodate"), "MM/dd/yyyy")
                InitDDLDept()
                deptoid.SelectedValue = Trim(xreader("deptoid").ToString)
                reqmstnote.Text = Trim(xreader("reqrawmstnote").ToString)
                reqmststatus.Text = Trim(xreader("reqrawmststatus").ToString)
                createuser.Text = Trim(xreader("createuser").ToString)
                createtime.Text = Trim(xreader("createtime").ToString)
                upduser.Text = Trim(xreader("upduser").ToString)
                updtime.Text = Trim(xreader("updtime").ToString)
                itemdesc.Text = Trim(xreader("reqmstres3").ToString)
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False : btnPost.Visible = False
        End Try
        If reqmststatus.Text = "Post" Or reqmststatus.Text = "Closed" Or reqmststatus.Text = "Cancel" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnPost.Visible = False
            btnAddToList.Visible = False
            gvReqDtl.Columns(0).Visible = False
            gvReqDtl.Columns(gvReqDtl.Columns.Count - 1).Visible = False
            lblsprno.Text = "Request No."
            reqmstoid.Visible = False
            reqno.Visible = True
        End If
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sSql = "SELECT reqd.reqdtlseq AS reqdtlseq, reqd.matoid AS matoid, m.itemcode AS matcode, m.itemlongdescription AS matlongdesc, reqd.reqqty AS reqqty, reqd.requnitoid AS requnitoid, g.gendesc AS requnit, reqd.reqdtlnote AS reqdtlnote, reqd.refoid, reqd.reftype, m.roundqty AS reqlimitqty, (wod3.wodtl3qty - ISNULL((SELECT SUM(x.reqqty) FROM QL_trnreqdtl x INNER JOIN QL_trnreqmst xx ON xx.cmpcode=x.cmpcode AND xx.reqmstoid=x.reqmstoid WHERE x.cmpcode=reqd.cmpcode AND x.reftype='Detail 5' AND x.refoid=reqd.refoid AND xx.reqmststatus<>'Cancel' AND x.reqmstoid<>reqd.reqmstoid), 0)) AS reqmaxqty, 0.0 AS reqmaxqtyfix, gx.genoid AS stockunitoid, gx.gendesc AS stockunit, ISNULL((SELECT SUM(crd.saldoakhir) FROM QL_crdstock crd WHERE crd.cmpcode=reqd.cmpcode AND crd.refoid=reqd.matoid AND crd.mtrlocoid=" & reqwhoid.SelectedValue & " AND crd.periodacctg IN ('" & sPeriod & "', '" & GetLastPeriod(sPeriod) & "') AND crd.closingdate='01/01/1900'), 0.0) AS stockqty FROM QL_trnreqdtl reqd INNER JOIN QL_trnwodtl3 wod3 ON wod3.cmpcode=reqd.cmpcode AND wod3.wodtl3oid=reqd.refoid INNER JOIN QL_mstitem m ON m.itemoid=reqd.matoid INNER JOIN QL_mstgen g ON g.genoid=reqd.requnitoid INNER JOIN QL_mstgen gx ON gx.genoid=m.itemunit1 WHERE reqd.reqmstoid=" & sOid & " AND reqd.reftype='Detail 5'"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnreqrawdtl")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(C1)("reqmaxqtyfix") = ToDouble(dt.Rows(C1)("reqmaxqty").ToString)
        Next
        Dim dv As DataView = dt.DefaultView
        dv.Sort = "reqdtlseq"
        dv.RowFilter = "reftype='Detail 3'"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                Dim dQty As Double = ToDouble(dv(C1)("reqmaxqtyfix").ToString)
                dv(C1)("reqmaxqty") = dQty - ToDouble(dt.Compute("SUM(reqqty)", "reftype='Detail 3' AND refoid=" & dv(C1)("refoid") & " AND matoid<>" & dv(C1)("matoid") & " AND reqdtlseq<" & dv(C1)("reqdtlseq")).ToString)
            Next
        End If
        dv.RowFilter = ""
        Session("TblDtl") = dv.ToTable
        gvReqDtl.DataSource = Session("TblDtl")
        gvReqDtl.DataBind()
        ClearDetail()
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnRequestKIK.aspx")
        End If
        If checkPagePermission("~\Transaction\trnRequestKIK.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Material Request"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPost.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            CheckRequestStatus()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitALLDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                reqmstoid.Text = GenerateID("QL_TRNREQMST", CompnyCode)
                reqdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                reqmststatus.Text = "In Process"
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvReqDtl.DataSource = dt
            gvReqDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("ErrMat") Is Nothing And Session("ErrMat") <> "" Then
            Session("ErrMat") = Nothing
            cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, True)
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnRequestKIK.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lkbReqInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbReqInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, reqm.updtime, GETDATE()) > " & nDays & " AND reqrawmststatus='In Process' "
        If checkPagePermission("~\Transaction\trnRequestKIK.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND reqm.createuser='" & Session("UserID") & "' "
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND reqm.reqdate>='" & FilterPeriod1.Text & " 00:00:00' AND reqm.reqdate<='" & CDate(FilterPeriod2.Text) & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND reqm.reqmststatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Transaction\trnRequestKIK.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND reqm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlplus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Transaction\trnRequestKIK.aspx", Session("SpecialAccess")) = False Then
            sSqlplus &= " AND reqm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlplus)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        UpdateCheckedValue()
        PrintReport()
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        btnClearKIK_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnSearchKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchKIK.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        FilterDDLListKIK.SelectedIndex = -1 : FilterTextListKIK.Text = "" : gvListKIK.SelectedIndex = -1
        BindListKIK()
        cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, True)
    End Sub

    Protected Sub btnClearKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearKIK.Click
        womstoid.Text = "" : wono.Text = "" : wodate.Text = "" : deptoid.Items.Clear() : deptoid_SelectedIndexChanged(Nothing, Nothing)
        itemdesc.Text = ""
    End Sub

    Protected Sub btnFindListKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListKIK.Click
        BindListKIK()
        mpeListKIK.Show()
    End Sub

    Protected Sub btnAllListKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListKIK.Click
        FilterDDLListKIK.SelectedIndex = -1 : FilterTextListKIK.Text = "" : gvListKIK.SelectedIndex = -1
        BindListKIK()
        mpeListKIK.Show()
    End Sub

    Protected Sub gvListKIK_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListKIK.PageIndexChanging
        gvListKIK.PageIndex = e.NewPageIndex
        BindListKIK()
        mpeListKIK.Show()
    End Sub

    Protected Sub gvListKIK_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListKIK.SelectedIndexChanged
        If womstoid.Text <> gvListKIK.SelectedDataKey.Item("womstoid").ToString Then
            btnClearKIK_Click(Nothing, Nothing)
        End If
        womstoid.Text = gvListKIK.SelectedDataKey.Item("womstoid").ToString
        wono.Text = gvListKIK.SelectedDataKey.Item("wono").ToString
        wodate.Text = gvListKIK.SelectedDataKey.Item("wodate").ToString
        itemdesc.Text = gvListKIK.SelectedDataKey.Item("itemlongdesc").ToString
        InitDDLDept() : deptoid_SelectedIndexChanged(Nothing, Nothing)
        cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, False)
    End Sub

    Protected Sub lkbCloseListKIK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListKIK.Click
        cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, False)
    End Sub

    Protected Sub deptoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles deptoid.SelectedIndexChanged
        If deptoid.SelectedValue <> "" Then
            wodtl2oid.Text = GetStrData("SELECT TOP 1 wodtl1oid FROM QL_trnwodtl2 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND deptoid=" & deptoid.SelectedValue & " AND womstoid=" & womstoid.Text)
        End If
        ClearDetail()
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        If deptoid.SelectedValue = "" Then
            showMessage("Please select Department first!", 2)
            Exit Sub
        End If
        FilterDDLListMat2.SelectedIndex = -1 : FilterTextListMat2.Text = "" : Session("TblListMat") = Nothing : Session("TblListMatView") = Nothing : gvListMat2.DataSource = Session("TblListMatView") : gvListMat2.DataBind()
        BindMaterial()
        cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, True)
    End Sub

    Protected Sub btnFindListMat2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat2.Click
        If Not Session("TblListMat") Is Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                UpdateCheckedMat()
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = FilterDDLListMat2.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat2.Text) & "%'"
                Session("TblListMatView") = dv.ToTable
                gvListMat2.DataSource = Session("TblListMatView")
                gvListMat2.DataBind()
                dv.RowFilter = ""
            End If
        End If
        mpeListMat2.Show()
    End Sub

    Protected Sub btnAllListMat2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat2.Click
        If Not Session("TblListMat") Is Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                UpdateCheckedMat()
                FilterDDLListMat2.SelectedIndex = -1 : FilterTextListMat2.Text = ""
                Session("TblListMatView") = dt
                gvListMat2.DataSource = Session("TblListMatView")
                gvListMat2.DataBind()
            End If
        End If
        mpeListMat2.Show()
    End Sub

    Protected Sub cbHdrListMat_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim bVal As Boolean = sender.Checked
        For C1 As Integer = 0 To gvListMat2.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat2.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = bVal
                    End If
                Next
            End If
        Next
        mpeListMat2.Show()
    End Sub

    Protected Sub gvListMat2_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat2.PageIndexChanging
        If Not Session("TblListMat") Is Nothing Then
            UpdateCheckedMat()
            UpdateCheckedMatView()
            gvListMat2.PageIndex = e.NewPageIndex
            gvListMat2.DataSource = Session("TblListMatView")
            gvListMat2.DataBind()
        End If
        mpeListMat2.Show()
    End Sub

    Protected Sub gvListMat2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat2.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            e.Row.Cells(10).Text = ToMaskEdit(ToDouble(e.Row.Cells(10).Text), 4)
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(3).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 4)
                    End If
                End If
            Next
            Dim str As String = e.Row.Cells(1).Text
            e.Row.Cells(1).Text = Mid(str, 1, 7) & vbCrLf & Mid(str, 8, 10) & vbCrLf & Mid(str, 18, 5)
        End If
    End Sub

    Protected Sub lkbAddToList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToList.Click
        If Not Session("TblListMat") Is Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                UpdateCheckedMat()
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "checkvalue='True'"
                If dv.Count = 0 Then
                    Session("ErrMat") = "Please select Material data!"
                    showMessage(Session("ErrMat"), 2)
                    dv.RowFilter = ""
                    Exit Sub
                End If
                dv.RowFilter = ""
                dv.RowFilter = "checkvalue='True' AND reqqty=0"
                If dv.Count > 0 Then
                    Session("ErrMat") = "Request Qty for every selected Material must be more than 0!"
                    showMessage(Session("ErrMat"), 2)
                    dv.RowFilter = ""
                    Exit Sub
                End If
                dv.RowFilter = ""
                dv.RowFilter = "checkvalue='True' AND reqqty>reqmaxqty"
                If dv.Count > 0 Then
                    Session("ErrMat") = "Request Qty for every selected Material must be less or equal than Max Qty!"
                    showMessage(Session("ErrMat"), 2)
                    dv.RowFilter = ""
                    Exit Sub
                End If
                dv.RowFilter = ""
                dv.RowFilter = "checkvalue='True'"
                If Not IsQtyRounded(dv, "reqqty", "reqlimitqty") Then
                    Session("ErrMat") = "Request Qty for every selected Material must be rounded by Round Qty!"
                    showMessage(Session("ErrMat"), 2)
                    dv.RowFilter = ""
                    Exit Sub
                End If
                dv.RowFilter = ""
                If Session("TblDtl") Is Nothing Then
                    CreateTblDetail()
                End If
                Dim dtMat As DataTable = Session("TblDtl")
                Dim dvMat As DataView = dtMat.DefaultView
                Dim iSeq As Integer = dvMat.Count + 1
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    dvMat.RowFilter = "reftype='" & dv(C1)("reftype").ToString & "' AND matoid=" & dv(C1)("matoid").ToString
                    If dvMat.Count > 0 Then
                        dvMat(0)("reqqty") = ToDouble(dv(C1)("reqqty").ToString)
                        dvMat(0)("reqdtlnote") = dv(C1)("reqdtlnote").ToString
                        dvMat(0)("reqmaxqty") = ToDouble(dvMat(0)("reqmaxqty").ToString)
                        dvMat(0)("reqmaxqtyfix") = ToDouble(dvMat(0)("reqmaxqty").ToString)
                    Else
                        Dim rv As DataRowView = dvMat.AddNew()
                        rv.BeginEdit()
                        rv("reqdtlseq") = iSeq
                        rv("matoid") = dv(C1)("matoid")
                        rv("matcode") = dv(C1)("matcode").ToString
                        rv("matlongdesc") = dv(C1)("matlongdesc").ToString
                        rv("reqqty") = ToDouble(dv(C1)("reqqty").ToString)
                        rv("requnitoid") = dv(C1)("matunitoid")
                        rv("stockunitoid") = dv(C1)("stockunitoid")
                        rv("requnit") = dv(C1)("matunit").ToString
                        rv("stockunit") = dv(C1)("stockunit").ToString
                        rv("reqdtlnote") = dv(C1)("reqdtlnote").ToString
                        rv("refoid") = dv(C1)("refoid")
                        rv("reftype") = dv(C1)("reftype").ToString
                        rv("reqlimitqty") = ToDouble(dv(C1)("reqlimitqty").ToString)
                        rv("reqmaxqty") = ToDouble(dv(C1)("reqmaxqty").ToString)
                        rv("reqmaxqtyfix") = ToDouble(dv(C1)("reqmaxqty").ToString)
                        rv("stockqty") = ToDouble(dv(C1)("stockqty").ToString)
                        rv.EndEdit()
                        iSeq += 1
                    End If
                    dvMat.RowFilter = ""
                Next
                dv.RowFilter = ""
                Session("TblDtl") = dvMat.ToTable
                gvReqDtl.DataSource = Session("TblDtl")
                gvReqDtl.DataBind()
                ClearDetail()
            End If
            cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, False)
        End If
    End Sub

    Protected Sub lkbCloseListMat2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat2.Click
        cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, False)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            If Session("TblDtl") Is Nothing Then
                CreateTblDetail()
            End If
            Dim objTable As DataTable = Session("TblDtl")
            Dim dv As DataView = objTable.DefaultView
            If i_u2.Text = "New Detail" Then
                dv.RowFilter = "matoid=" & matoid.Text & " AND reftype='" & reftype.Text & "'"
            Else
                dv.RowFilter = "matoid=" & matoid.Text & " AND reftype='" & reftype.Text & "' AND reqdtlseq<>" & reqdtlseq.Text
            End If
            If dv.Count > 0 Then
                dv.RowFilter = ""
                showMessage("This Data has been added before, please check!", 2)
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                reqdtlseq.Text = objTable.Rows.Count + 1
            Else
                objRow = objTable.Rows(reqdtlseq.Text - 1)
                objRow.BeginEdit()
            End If
            objRow("reqdtlseq") = reqdtlseq.Text
            objRow("matoid") = matoid.Text
            objRow("matcode") = matcode.Text
            objRow("matlongdesc") = matlongdesc.Text
            objRow("reqqty") = ToDouble(reqqty.Text)
            objRow("requnitoid") = requnitoid.SelectedValue
            objRow("requnit") = requnitoid.SelectedItem.Text
            objRow("stockunit") = stockunitoid.SelectedItem.Text
            objRow("reqdtlnote") = reqdtlnote.Text.Trim
            objRow("refoid") = refoid.Text
            objRow("reftype") = reftype.Text
            objRow("reqlimitqty") = ToDouble(reqlimitqty.Text)
            objRow("reqmaxqty") = ToDouble(reqmaxqty.Text)
            objRow("stockqty") = ToDouble(stockqty.Text)
            If i_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            Session("TblDtl") = objTable
            gvReqDtl.DataSource = Session("TblDtl")
            gvReqDtl.DataBind()
            ClearDetail()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvReqDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvReqDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
        End If
    End Sub

    Protected Sub gvReqDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvReqDtl.SelectedIndexChanged
        Try
            reqdtlseq.Text = gvReqDtl.SelectedDataKey.Item("reqdtlseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "reqdtlseq=" & reqdtlseq.Text
                matoid.Text = dv.Item(0).Item("matoid").ToString
                matcode.Text = dv.Item(0).Item("matcode").ToString
                matlongdesc.Text = dv.Item(0).Item("matlongdesc").ToString
                reqqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("reqqty").ToString), 4)
                requnitoid.SelectedValue = dv.Item(0).Item("requnitoid").ToString
                stockunitoid.SelectedValue = dv.Item(0).Item("stockunitoid").ToString
                reqdtlnote.Text = dv.Item(0).Item("reqdtlnote").ToString
                refoid.Text = dv.Item(0).Item("refoid").ToString
                reftype.Text = dv.Item(0).Item("reftype").ToString
                reqlimitqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("reqlimitqty").ToString), 4)
                reqmaxqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("reqmaxqty").ToString), 4)
                stockqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("stockqty").ToString), 4)
                dv.RowFilter = ""
                btnSearchMat.Visible = False
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub gvReqDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvReqDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("reqdtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvReqDtl.DataSource = objTable
        gvReqDtl.DataBind()
        ClearDetail()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_trnreqmst WHERE reqmstoid=" & reqmstoid.Text
                If CheckDataExists(sSql) Then
                    reqmstoid.Text = GenerateID("QL_TRNREQMST", CompnyCode)
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_trnreqmst", "reqmstoid", reqmstoid.Text, "reqmststatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    reqmststatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            reqdtloid.Text = GenerateID("QL_TRNREQDTL", CompnyCode)
            periodacctg.Text = GetDateToPeriodAcctg(CDate(reqdate.Text))
            If reqmststatus.Text = "Post" Then
                GenerateNoRequest()
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trnreqmst (cmpcode, reqmstoid, periodacctg, reqdate, reqno, womstoid, deptoid, wodtl2oid, reqwhoid, reqmstnote, reqmststatus, createuser, createtime, upduser, updtime, reqmstres3) VALUES ('" & DDLBusUnit.SelectedValue & "', " & reqmstoid.Text & ", '" & periodacctg.Text & "', '" & reqdate.Text & "', '" & reqno.Text & "', " & womstoid.Text & ", " & deptoid.SelectedValue & ", " & wodtl2oid.Text & ", " & reqwhoid.SelectedValue & ", '" & Tchar(reqmstnote.Text.Trim) & "', '" & reqmststatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & itemdesc.Text & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & reqmstoid.Text & " WHERE tablename='QL_TRNREQMST' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trnreqmst SET periodacctg='" & periodacctg.Text & "', reqdate='" & reqdate.Text & "', reqno='" & reqno.Text & "', womstoid=" & womstoid.Text & ", deptoid=" & deptoid.SelectedValue & ", wodtl2oid=" & wodtl2oid.Text & ", reqwhoid=" & reqwhoid.SelectedValue & ", reqmstnote='" & Tchar(reqmstnote.Text.Trim) & "', reqmststatus='" & reqmststatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, reqmstres3='" & itemdesc.Text & "' WHERE reqmstoid=" & reqmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnwodtl2 SET wodtl2reqflag='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl2oid=" & wodtl2oid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnwodtl3 SET wodtl3reqflag='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl3oid IN (SELECT refoid FROM QL_trnreqdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqmstoid=" & reqmstoid.Text & " AND reftype='Detail 5')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnwodtl4 SET wodtl4reqflag='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl4oid IN (SELECT refoid FROM QL_trnreqdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqmstoid=" & reqmstoid.Text & " AND reftype='Detail 3')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trnreqdtl WHERE reqmstoid=" & reqmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    For c1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trnreqdtl (cmpcode, reqdtloid, reqmstoid, reqdtlseq, reftype, refoid, matoid, reqqty, requnitoid, reqdtlnote, reqdtlstatus, upduser, updtime) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (c1 + CInt(reqdtloid.Text)) & ", " & reqmstoid.Text & ", " & c1 + 1 & ", '" & objTable.Rows(c1).Item("reftype").ToString & "', " & objTable.Rows(c1).Item("refoid") & ", " & objTable.Rows(c1).Item("matoid") & ", " & ToDouble(objTable.Rows(c1).Item("reqqty").ToString) & ", " & objTable.Rows(c1).Item("requnitoid") & ", '" & Tchar(objTable.Rows(c1).Item("reqdtlnote")) & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        If ToDouble(objTable.Rows(c1).Item("reqqty").ToString) >= ToDouble(objTable.Rows(c1).Item("reqmaxqty").ToString) Then
                            If objTable.Rows(c1).Item("reftype").ToString.ToUpper = "DETAIL 5" Then
                                sSql = "UPDATE QL_trnwodtl3 SET wodtl3reqflag='Complete' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl3oid=" & objTable.Rows(c1).Item("refoid")
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                            End If
                            sSql = "UPDATE QL_trnwodtl2 SET wodtl2reqflag='Complete' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl1oid=" & wodtl2oid.Text & " AND (SELECT COUNT(*) FROM QL_trnwodtl3 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl1oid=" & wodtl2oid.Text & " AND wodtl3reqflag='') + (SELECT COUNT(*) FROM QL_trnwodtl4 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl2oid=" & wodtl2oid.Text & " AND wodtl4reqflag='')=0"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(reqdtloid.Text)) & " WHERE tablename='QL_TRNREQDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    reqmststatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 1)
                conn.Close()
                reqmststatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & reqmstoid.Text & ".<BR>"
            End If
            If reqmststatus.Text = "Post" Then
                Session("SavedInfo") &= "Data have been posted with Request No. = " & reqno.Text & "."
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trnRequestKIK.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnRequestKIK.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If reqmstoid.Text = "" Then
            showMessage("Please select  Material Request data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_trnreqmst", "reqmstoid", reqmstoid.Text, "reqmststatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                reqmststatus.Text = "In Process"
                Exit Sub
            End If
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trnwodtl2 SET wodtl2reqflag='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl1oid=" & wodtl2oid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_trnwodtl3 SET wodtl3reqflag='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl3oid IN (SELECT refoid FROM QL_trnreqdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqmstoid=" & reqmstoid.Text & " AND reftype='Detail 5')"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_trnwodtl4 SET wodtl4reqflag='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl4oid IN (SELECT refoid FROM QL_trnreqdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqmstoid=" & reqmstoid.Text & " AND reftype='Detail 3')"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnreqdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqmstoid=" & reqmstoid.Text & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnreqmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqmstoid=" & reqmstoid.Text & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnRequestKIK.aspx?awal=true")
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPost.Click
        reqmststatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub
#End Region

End Class