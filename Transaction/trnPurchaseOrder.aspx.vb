Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Drawing.Printing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_PORawMaterial
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public POAllowNull As String = ConfigurationSettings.AppSettings("POAllowNull")
    Public POUsingTag As String = ConfigurationSettings.AppSettings("POUsingTag")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim cRate As New ClassRate
    Dim report As New ReportDocument
    Dim pkSize = New System.Drawing.Printing.PaperSize
    Dim PrintDoc1 As New System.Drawing.Printing.PrintDocument
    Public folderReport As String = "~/Report/"
    Const iRoundDigit = 2
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("poqty") = Math.Round(ToDouble(GetTextBoxValue(row.Cells(4).Controls)), iRoundDigit)
                                    dtView(0)("poprice") = Math.Round(ToDouble(GetTextBoxValue(row.Cells(5).Controls)), iRoundDigit)
                                    dtView(0)("podtlnote") = GetTextBoxValue(row.Cells(7).Controls)
                                    dtView(0)("selectedunitoid") = GetDropDownListValue(row.Cells(3).Controls)
                                    dtView(0)("selectedunit") = GetDropDownListValue(row.Cells(3).Controls, "Text")
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                dtView(0)("dppprice") = Math.Round(ToDouble(GetTextBoxValue(row.Cells(5).Controls)), iRoundDigit) / GetNewTaxValueInclude(porawdate.Text)
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl = dtView.ToTable
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function GetDropDownListValue(ByVal cc As System.Web.UI.ControlCollection, Optional ByVal sText As String = "") As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                If sText = "" Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
                Else
                    sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedItem.Text
                End If
            End If
        Next
        Return sReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                dtView2.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("poqty") = Math.Round(ToDouble(GetTextBoxValue(row.Cells(4).Controls)), 4)
                                    dtView(0)("poprice") = Math.Round(ToDouble(GetTextBoxValue(row.Cells(5).Controls)), iRoundDigit)
                                    dtView(0)("podtlnote") = GetTextBoxValue(row.Cells(7).Controls)
                                    dtView(0)("selectedunitoid") = GetDropDownListValue(row.Cells(3).Controls)
                                    dtView(0)("selectedunit") = GetDropDownListValue(row.Cells(3).Controls, "Text")
                                    If dtView2.Count > 0 Then
                                        dtView2(0)("poqty") = Math.Round(ToDouble(GetTextBoxValue(row.Cells(4).Controls)), 4)
                                        dtView2(0)("poprice") = Math.Round(ToDouble(GetTextBoxValue(row.Cells(5).Controls)), iRoundDigit)
                                        dtView2(0)("podtlnote") = GetTextBoxValue(row.Cells(7).Controls)
                                        dtView2(0)("selectedunitoid") = GetDropDownListValue(row.Cells(3).Controls)
                                        dtView2(0)("selectedunit") = GetDropDownListValue(row.Cells(3).Controls, "Text")
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
       
        If matrawoid.Text = "" Then
            sError &= "- Please select MATERIAL field!<BR>"
        End If
        If porawunitoid.SelectedValue = "" Then
            sError &= "- Please select QUANTITY UNIT field!<BR>"
        End If
        If porawqty.Text = "" Then
            sError &= "- Please fill QUANTITY field!<BR>"
        Else
            If ToDouble(porawqty.Text) <= 0 Then
                sError &= "- QUANTITY must be more than 0!<BR>"
            End If
        End If
        If porawprice.Text = "" Then
            sError &= "- Please fill PRICE PER UNIT field!<BR>"
        Else
            If ToDouble(porawprice.Text) <= 0 Then
                If POAllowNull = "False" Then
                    sError &= "- PRICE PER UNIT must be more than 0!<BR>"
                End If
            End If
        End If
        If porawdtldisctype.SelectedValue = "P" Then
            If porawdtldiscvalue.Text <> "" Then
                If ToDouble(porawdtldiscvalue.Text) < 0 Or ToDouble(porawdtldiscvalue.Text) > 100 Then
                    sError &= "- Percentage of DETAIL DISC must be between 0 and 100.<BR>"
                End If
            End If
        Else
            If porawdtldiscvalue.Text <> "" Then
                If ToDouble(porawdtldiscvalue.Text) < 0 Or ToDouble(porawdtldiscvalue.Text) > ToDouble(porawdtlamt.Text) Then
                    sError &= "- Amount of DETAIL DISC must be between 0 and " & ToMaskEdit(ToDouble(porawdtlamt.Text), iRoundDigit) & " (DETAIL AMOUNT).<BR>"
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        sError = GetHeaderValidation(sErr)
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill DETAIL DATA!<BR>"
        Else
            Dim objTbl As DataTable = Session("TblDtl")
            If objTbl.Rows.Count <= 0 Then
                sError &= "- Please fill DETAIL DATA!<BR>"
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            porawmststatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function CheckQtyIsUsedByAnotherUser() As String
        Dim sError As String = ""

        Return sError
    End Function

    Private Function GetHeaderValidation(ByRef sDateStatus As String) As String
        Dim sError As String = ""
        Dim sErr As String = ""
        If porawdate.Text = "" Then
            sError &= "- Please fill PO DATE field!<BR>"
            sErr = "None"
        Else
            If Not IsValidDate(porawdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- PO DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If suppoid.Text = "" Then
            sError &= "- Please select SUPPLIER field!<BR>"
        End If
        If curroid.SelectedValue = "" Then
            sError &= "- Please select CURRENCY field!<BR>"
        End If
        If porawpaytypeoid.SelectedValue = "" Then
            sError &= "- Please select PAYMENT TYPE field!<BR>"
        End If
        If rateoid.Text = "" Then
            sError &= "- Please define some Rate Daily data for Currency : '" & IIf(curroid.SelectedItem.Text = "IDR", "USD", curroid.SelectedItem.Text) & "' and Date : " & porawdate.Text & " first before continue using this form! <BR>"
        End If
        If rate2oid.Text = "" Then
            sError &= "- Please define some Rate Monthly data for Currency : '" & IIf(curroid.SelectedItem.Text = "IDR", "USD", curroid.SelectedItem.Text) & "' and Period : " & Format(CDate(porawdate.Text), "MMMM") & " " & Year(CDate(porawdate.Text)) & " first before continue using this form! <BR>"
        End If
        If tolerance.Text <> "" Then
            If ToDouble(tolerance.Text) > 10 Then
                sError &= "- Tolerance must be less than 10%!<BR>"
            End If
        End If
        If porawtaxtype.SelectedValue = "TAX" Then
            If porawtaxamt.Text <> "" Then
                If ToDouble(porawtaxamt.Text) < 0 Or ToDouble(porawtaxamt.Text) > 100 Then
                    sError &= "- TAX value must be between 0 and 100.<BR>"
                End If
            End If
        End If
        sDateStatus = sErr
        GetHeaderValidation = sError
    End Function

    Private Function IsInputValidForSaveAs() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        sError = GetHeaderValidation(sErr)
        Dim iTotalDtl As Integer = 0, iErrPRMst As Integer = 0, iErrPRDtl As Integer = 0
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill DETAIL DATA!<BR>"
        Else
            Dim objTbl As DataTable = Session("TblDtl")
            If objTbl.Rows.Count <= 0 Then
                sError &= "- Please fill DETAIL DATA!<BR>"
            Else
                iTotalDtl = objTbl.Rows.Count
                If sErr = "" Then
                    Dim bFound As Boolean = False
                    For C1 As Integer = 0 To iTotalDtl - 1
                        If CDate(porawdate.Text) < CDate(objTbl.Rows(C1)("prrawdate").ToString) Then
                            sError &= "- PO DATE must be more or equal than every PR Date!<BR>"
                            Exit For
                        End If
                    Next
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            porawmststatus.Text = "In Process"
            Return False
        Else
            If Not Session("RemovePRClosed") Is Nothing And Session("RemovePRClosed") <> "" Then
                showConfirmation("Some PR data have been closed their status. Do you want to remove the Closed PR data?")
                porawmststatus.Text = "In Process"
                Return False
            End If
            If Not Session("RemovePRCompleted") Is Nothing And Session("RemovePRCompleted") <> "" Then
                showConfirmation("Some material of PR data have been completed their status. Do you want to remove the Completed material of PR data?")
                porawmststatus.Text = "In Process"
                Return False
            End If
        End If
        Return True
    End Function

    Private Function GetTaxValue() As Double
        GetTaxValue = 0
        Try
            sSql = "SELECT TOP 1 gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND gengroup='DEFAULT TAX' ORDER BY updtime DESC"
            GetTaxValue = ToDouble(GetStrData(sSql))
        Catch ex As Exception
            GetTaxValue = 0
        End Try
    End Function

    Private Function GetPriceValue() As Double
        GetPriceValue = 0
        Try
            sSql = "SELECT TOP 1 gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND gengroup='PRICE TOLERANCE' ORDER BY updtime DESC"
            GetPriceValue = (ToDouble(GetStrData(sSql)) / 100)
        Catch ex As Exception
            GetPriceValue = 0
        End Try
    End Function

    Private Function IsQtyPOOverThanQtyPR() As Boolean
        Dim bRet As Boolean = False
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            For C1 As Integer = 0 To dt.Rows.Count - 1
                If ToDouble(dt.Rows(C1)("prrawqty").ToString) < ToDouble(dt.Rows(C1)("porawqty").ToString) Then
                    gvTblDtl.Rows(C1).Cells(5).ForeColor = Color.Red
                    gvTblDtl.Rows(C1).Cells(6).ForeColor = Color.Red
                    bRet = True
                End If
            Next
        End If
        Return bRet
    End Function

    Private Function GetOidDetail() As String
        Dim sReturn As String = ""
        Dim dv As DataView = Session("TblMst").DefaultView
        dv.RowFilter = "CheckValue='True'"
        For C1 As Integer = 0 To dv.Count - 1
            sReturn &= dv(C1)("POMSTOID").ToString & ","
        Next
        dv.RowFilter = ""
        If sReturn <> "" Then
            sReturn = Left(sReturn, sReturn.Length - 1)
        End If
        Return sReturn
    End Function

    Public Function GetParameterID() As String
        Return Eval("itemoid") & "," & Eval("selectedunitoid")
    End Function

    Private Function GetLastPrice(ByVal sOid As Integer, ByVal sReffID As Integer, ByVal sUnitOid As String) As Decimal
        sSql = "SELECT ISNULL(lastpurchaseprice,0) FROM QL_mstitemprice WHERE cmpcode='" & CompnyCode & "' AND itemoid=" & sOid & " AND refname='SUPPLIER' AND refoid=" & sReffID & " AND res1='" & sUnitOid & "'"
        Return GetLastPrice = ToDouble(GetStrData(sSql))
    End Function

#End Region

#Region "Procedures"
    Sub RegenerateDtl()
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                Dim dv As DataView = objTable.DefaultView
                Dim objrow As DataRow = objTable.Rows(dv.Item(0).Item("podtlseq") - 1)
                If objrow("inex") <> DDLinex.SelectedValue Then
                    If DDLinex.SelectedValue = "EXC" Then
                        For C1 As Integer = 0 To dv.Count - 1
                            objrow = objTable.Rows(dv.Item(C1).Item("podtlseq") - 1)
                            objrow.BeginEdit()
                            objrow("poprice") = ToMaskEdit(ToDouble(objrow("poprice")) * GetNewTaxValueInclude(porawdate.Text), iRoundDigit)
                            objrow("podtlamt") = ToMaskEdit(ToDouble(objrow("poprice")) * objrow("poqty"), iRoundDigit)
                            objrow("podtldiscamt") = 0
                            objrow("podtldiscvalue") = 0
                            objrow("podtlnetto") = ToMaskEdit(ToDouble(objrow("podtlamt")) - ToDouble(objrow("podtldiscamt")), iRoundDigit)
                            objrow("inex") = DDLinex.SelectedValue
                            objrow.EndEdit()
                        Next
                    Else
                        For C1 As Integer = 0 To dv.Count - 1
                            objrow = objTable.Rows(dv.Item(C1).Item("podtlseq") - 1)
                            objrow.BeginEdit()
                            objrow("poprice") = ToMaskEdit(ToDouble(objrow("poprice")) / GetNewTaxValueInclude(porawdate.Text), iRoundDigit)
                            objrow("podtlamt") = ToMaskEdit(ToDouble(objrow("poprice")) * ToDouble(objrow("poqty")), iRoundDigit)
                            objrow("podtldiscamt") = 0
                            objrow("podtldiscvalue") = 0
                            objrow("podtlnetto") = ToMaskEdit(ToDouble(objrow("podtlamt")) - ToDouble(objrow("podtldiscamt")), iRoundDigit)
                            objrow("inex") = DDLinex.SelectedValue
                            objrow.EndEdit()
                        Next
                    End If
                End If
                Session("TblDtl") = objTable
                gvTblDtl.DataSource = objTable
                gvTblDtl.DataBind()
                CountHdrAmount()
                ClearDetail()
            End If
        End If
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub showConfirmation(ByVal sMessage As String)
        lblCaptionConfirm.Text = CompnyName & " - CONFIRMATION" : lblPopUpConfirm.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpConfirm, pnlPopUpConfirm, mpePopUpConfirm, True)
    End Sub

    Private Sub ClearTempFile()
        Dim xDir As New IO.DirectoryInfo(Server.MapPath("~/Files/"))
        Dim xFileDir() As IO.FileInfo = xDir.GetFiles()
        For Each xFile As IO.FileInfo In xFileDir
            If File.Exists(xFile.FullName) Then
                If xFile.FullName.Contains("PORawMat") Then
                    Try
                        File.Delete(xFile.FullName)
                    Catch ex As Exception
                    End Try
                End If
            End If
        Next
    End Sub

    Private Sub CheckPOStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trnpomst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND pomststatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnPurchaseOrder.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lkbPOInProcess.Visible = True
            lkbPOInProcess.Text = "You have " & GetStrData(sSql) & " In Process PO Raw Material that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
        sSql = "SELECT COUNT(*) FROM QL_trnpomst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND pomststatus='In Approval'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnPurchaseOrder.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lkbPOInApproval.Visible = True
            lkbPOInApproval.Text = "You have " & GetStrData(sSql) & " In Approval PO Raw Material that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub CheckTax()
        If porawtaxtype.Text = "TAX" Then
            DDLinex.Visible = True
            porawtaxamt.Visible = True
            lbltax.Visible = True
            porawtaxamt.Text = GetNewTaxValue(porawdate.Text)
        Else
            DDLinex.Visible = False
            porawtaxamt.Visible = False
            lbltax.Visible = False
            porawtaxamt.Text = 0
        End If

        'If DDLinex.SelectedValue = "INC" Then
        '    lblvarprice.Visible = True
        '    lblvarprice2.Visible = True
        '    varprice.Visible = True
        '    varprice.CssClass = "inpTextDisabled"
        '    varprice.Enabled = False
        '    porawprice.Enabled = False
        '    porawprice.CssClass = "inpTextDisabled"
        '    porawdtldisctype.Enabled = False
        '    porawdtldisctype.CssClass = "inpTextDisabled"
        '    porawdtldiscvalue.Enabled = False
        '    porawdtldiscvalue.CssClass = "inpTextDisabled"
        'Else
        '    lblvarprice.Visible = False
        '    lblvarprice2.Visible = False
        '    varprice.Visible = False
        '    porawprice.Enabled = True
        '    porawprice.CssClass = "inpText"
        '    porawdtldisctype.Enabled = True
        '    porawdtldisctype.CssClass = "inpText"
        '    porawdtldiscvalue.Enabled = True
        '    porawdtldiscvalue.CssClass = "inpText"
        'End If

        If porawtaxtype.Text = "NON TAX" Then
            lblvarprice.Visible = False
            lblvarprice2.Visible = False
            varprice.Visible = False
            porawprice.Enabled = True
            porawprice.CssClass = "inpText"
            porawdtldisctype.Enabled = True
            porawdtldisctype.CssClass = "inpText"
            porawdtldiscvalue.Enabled = True
            porawdtldiscvalue.CssClass = "inpText"
        End If
    End Sub

    Private Sub InitAllDDL()
        ' Fill DDL Currency
        sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        FillDDL(curroid, sSql)
        'Fill DDL Payment Type
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='PAYMENT TYPE' AND activeflag='ACTIVE'"
        FillDDL(porawpaytypeoid, sSql)
        ' Fill DDL Tag
        sSql = "SELECT cat1oid, cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND cat1res1='Raw' AND cat1res2 LIKE '%WIP%' AND activeflag='ACTIVE' ORDER BY cat1shortdesc"
        FillDDL(FilterDDLTag, sSql)
        'Fill DDL Tipe PO
        sSql = "SELECT gencode, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='GROUPITEM' AND activeflag='ACTIVE' ORDER BY gencode"
        FillDDL(DDLTypePO, sSql)
    End Sub

    Private Sub InitDDLUnit(ByVal iItemOid As Integer)
        'Fill DDDL Unit
        sSql = "select itemUnit1, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit1 where itemoid=" & iItemOid & ""
        FillDDL(porawunitoid, sSql)
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT 'False' AS checkvalue, pom.cmpcode, pom.pomstoid, pom.pono, CONVERT(VARCHAR(10), pom.podate, 101) AS podate, s.suppname, s.supptype, pom.pomstnote, (CASE ISNULL(pom.pomstres1, '') WHEN '' THEN pom.pomststatus ELSE pom.pomstres1 END) pomststatus, '' AS divname, pom.createuser, pom.createtime, case upper(pom.pomststatus) when 'CLOSED' then pom.closereason when 'REJECTED' then rejectreason when 'REVISED' then revisereason else '' end reason, CONVERT(DATETIME, pom.podate) temp FROM QL_trnpomst pom INNER JOIN QL_mstsupp s ON pom.suppoid=s.suppoid WHERE pom.cmpcode='" & CompnyCode & "' "
        sSql &= sSqlPlus & " ORDER BY CONVERT(DATETIME, pom.podate) DESC, pom.pomstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trnpomst")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "pomstoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub BindSupplierData()
        sSql = "SELECT suppoid, suppcode, supptype, suppname, supppaymentoid, suppaddr, supptaxable, ISNULL(suppres3, '') AS suppres3, partnerflag FROM QL_mstsupp WHERE cmpcode='" & CompnyCode & "' AND " & FilterDDLListSupp.SelectedValue & " LIKE '%" & Tchar(FilterTextListSupp.Text) & "%' AND supptype='" & FilterDDLType.SelectedValue & "' AND activeflag='ACTIVE'"
        If cbTag.Checked Then
            If FilterDDLTag.SelectedValue <> "" Then
                sSql &= " AND (suppres3 LIKE '%" & FilterDDLTag.SelectedItem.Text & "%' OR suppres3 IS NULL OR suppres3='')"
            End If
        End If
        sSql &= " ORDER BY suppcode"
        Dim objTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstsupp")
        Session("mstSupp") = objTbl
        gvListSupp.DataSource = objTbl
        gvListSupp.DataBind()
    End Sub

    Private Sub CountHdrAmount()
        CountDtlAmount()
        If tbHD.Text = "" Then
            tbHD.Text = "0"
        End If
        If ddlHD.SelectedValue = "P" Then
            HDA.Text = ToMaskEdit((ToDouble(porawtotalamt.Text) - ToDouble(porawtotaldiscdtl.Text)) * ToDouble(tbHD.Text) / 100, 2)
        Else
            HDA.Text = ToMaskEdit(ToDouble(tbHD.Text), 2)
        End If
        pototaldiscamt.Text = ToMaskEdit(ToDouble(porawtotaldiscdtl.Text) + ToDouble(HDA.Text), 2)
        porawtotalnetto.Text = ToMaskEdit(ToDouble(porawtotalamt.Text) - ToDouble(pototaldiscamt.Text), iRoundDigit)
        If porawtaxtype.SelectedValue = "NON TAX" Then
            porawvat.Text = "0"
            porawtaxamt.Text = ""
        Else
            porawvat.Text = ToMaskEdit(ToDouble(porawtotalnetto.Text) * (ToDouble(porawtaxamt.Text) / 100), iRoundDigit)
        End If
        porawgrandtotalamt.Text = ToMaskEdit(Math.Round(ToDouble(porawtotalnetto.Text) + ToDouble(porawvat.Text), MidpointRounding.AwayFromZero), iRoundDigit)
    End Sub

    Private Sub CountDtlAmount()
        If Not Session("TblDtl") Is Nothing Then
            Dim TotalAmount As Double = 0
            Dim TotalDisc As Double = 0
            Dim objTable As DataTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                For C1 As Int16 = 0 To objTable.Rows.Count - 1
                    TotalAmount += ToDouble(objTable.Rows(C1).Item("podtlamt").ToString)
                    TotalDisc += ToDouble(objTable.Rows(C1).Item("podtldiscamt").ToString)
                Next
            End If
            porawtotalamt.Text = ToMaskEdit(TotalAmount, iRoundDigit)
            porawtotaldiscdtl.Text = ToMaskEdit(TotalDisc, iRoundDigit)
        End If
    End Sub

    Private Sub BindPRData()

        sSql = "SELECT DISTINCT prm.prmstoid, prno, CONVERT(VARCHAR(10), prdate, 101) AS prdate, CONVERT(VARCHAR(10), prexpdate, 101) AS prexpdate, deptname, prmstnote FROM QL_prmst prm INNER JOIN QL_prdtl prd ON prm.cmpcode=prd.cmpcode AND prm.prmstoid=prd.prmstoid INNER JOIN QL_mstmat m ON prd.matoid=m.matoid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=m.cmpcode AND c1.cat1code=SUBSTRING(m.matcode, 1, 2) AND cat1res1='Raw' INNER JOIN QL_mstdept d ON prm.cmpcode=d.cmpcode AND prm.deptoid=d.deptoid WHERE prm.cmpcode='" & CompnyCode & "' AND prm.prmststatus='Approved'"
        If POUsingTag = "True" Then
            If suppres3.Text <> "" Then
                sSql &= " AND ("
                Dim sData() As String = suppres3.Text.Split(";")
                For C1 As Integer = 0 To sData.Length - 1
                    sSql &= "c1.cat1shortdesc='" & sData(C1) & "'"
                    If C1 < sData.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            End If
        End If
        sSql &= " AND " & FilterDDLListPR.SelectedValue & " LIKE '%" & Tchar(FilterTextListPR.Text) & "%' ORDER BY prdate DESC, prm.prmstoid DESC"
        'FillGV(gvListPR, sSql, "QL_prmst")
        Dim objTbl As DataTable = cKon.ambiltabel(sSql, "QL_prmst")
        Session("listPR") = objTbl
        gvListPR.DataSource = objTbl
        gvListPR.DataBind()
    End Sub

    Private Sub BindMaterialData()
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sSql = "SELECT DISTINCT 'False' AS CheckValue, i.itemoid, i.itemCode, i.itemLongDescription AS itemlongdesc, i.itemMinStock, i.itemUnit1 unit1oid, unit1.gendesc AS unit1, i.itemUnit2 unit2oid, unit2.gendesc AS unit2, i.itemUnit3 unit3oid, unit3.gendesc AS unit3, i.itemUnit1 selectedunitoid, unit1.gendesc selectedunit, 0.00 AS poqty, '' AS podtlnote, 0 AS lastcurroid, 0.00 AS poprice, 0.00 dppprice,i.itemUnit1 UnitKecil,i.itemUnit3 unitBesar, roundqty, '' AS itemunit, CONVERT(DECIMAL(18, 2), ISNULL((SELECT SUM(saldoakhir) FROM QL_crdstock crd WHERE crd.refoid=i.itemoid AND periodacctg IN ('" & sPeriod & "', '" & GetLastPeriod(sPeriod) & "') AND closeuser=''),0)) AS stockqty, 0.00 AS lastpoprice FROM QL_mstitem i INNER JOIN QL_mstgen unit1 ON unit1.genoid=i.itemUnit1 INNER JOIN QL_mstgen unit2 ON unit2.genoid=i.itemUnit2 INNER JOIN QL_mstgen unit3 ON unit3.genoid=i.itemUnit3 WHERE i.cmpcode='" & CompnyCode & "' AND i.itemGroup='" & DDLTypePO.SelectedValue & "' AND i.itemRecordStatus='ACTIVE'"
        If POUsingTag = "True" Then
            If suppres3.Text <> "" Then
                sSql &= " AND ("
                Dim sData() As String = suppres3.Text.Split(";")
                For C1 As Integer = 0 To sData.Length - 1
                    sSql &= "c1.cat1shortdesc='" & sData(C1) & "'"
                    If C1 < sData.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            End If
        End If
        sSql &= " ORDER BY i.itemcode"
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstitem")
        If dtTbl.Rows.Count > 0 Then
            Dim cRateTmp As New ClassRate
            For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                dtTbl.Rows(C1)("dppprice") = ToDouble(dtTbl.Rows(C1)("poprice").ToString) / GetNewTaxValueInclude(porawdate.Text)
            Next
            dtTbl.AcceptChanges()
            Session("TblMat") = dtTbl
            Session("TblMatView") = dtTbl
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            showMessage("Material data can't be found!", 2)
        End If
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("QL_trnpodtl")
        dtlTable.Columns.Add("podtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemlongdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("poqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("pounitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("unit", Type.GetType("System.String"))
        dtlTable.Columns.Add("poprice", Type.GetType("System.Double"))
        dtlTable.Columns.Add("podtlamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("podtldisctype", Type.GetType("System.String"))
        dtlTable.Columns.Add("podtldiscvalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("podtldiscamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("podtlnetto", Type.GetType("System.Double"))
        dtlTable.Columns.Add("podtlnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemlimitqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("lastpoprice", Type.GetType("System.Double"))
        dtlTable.Columns.Add("lastcurroid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("poqty_unitkecil", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("dppprice", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("inex", Type.GetType("System.String"))
        dtlTable.Columns.Add("poqty_unitbesar", Type.GetType("System.Decimal"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub ClearDetail()
        porawdtlseq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("TblDtl")
            porawdtlseq.Text = objTable.Rows.Count + 1
            If objTable.Rows.Count > 0 Then
                EnableHeader(False)
                EnableType(False)
            Else
                EnableHeader(True)
                EnableType(True)
            End If
        Else
            EnableHeader(True)
            EnableType(True)
        End If
        i_u2.Text = "New Detail"
        prrawdate.Text = ""
        matrawoid.Text = ""
        matrawcode.Text = ""
        matrawlongdesc.Text = ""
        porawqty.Text = ""
        porawunitoid.Items.Clear()
        porawprice.Text = ""
        porawdtlamt.Text = "0"
        porawdtldisctype.SelectedIndex = -1
        porawdtldiscvalue.Text = ""
        porawdtldiscamt.Text = "0"
        porawdtlnetto.Text = "0"
        porawdtlnote.Text = ""
        cbPrice.Checked = True
        cbDisc.Checked = True
        gvTblDtl.SelectedIndex = -1
        btnSearchMat.Visible = True
        varprice.Text = ""
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        'DDLBusUnit.Enabled = bVal : DDLBusUnit.CssClass = sCss
        'btnSearchSupp.Visible = bVal : btnClearSupp.Visible = bVal
        curroid.Enabled = bVal : curroid.CssClass = sCss
        'DDLinex.Enabled = bVal : DDLinex.CssClass = sCss
    End Sub

    Private Sub CountAllDtlAmount()
        porawdtlamt.Text = ToMaskEdit(Math.Round(ToDouble(porawqty.Text) * ToDouble(porawprice.Text), MidpointRounding.AwayFromZero), iRoundDigit)
        If porawdtldisctype.SelectedValue = "P" Then
            porawdtldiscamt.Text = ToMaskEdit((ToDouble(porawdtlamt.Text) * ToDouble(porawdtldiscvalue.Text)) / 100, iRoundDigit)
        Else
            porawdtldiscamt.Text = ToMaskEdit(ToDouble(porawdtldiscvalue.Text), iRoundDigit)
        End If
        porawdtlnetto.Text = ToMaskEdit(ToDouble(porawdtlamt.Text) - ToDouble(porawdtldiscamt.Text), iRoundDigit)
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        sSql = "SELECT pom.cmpcode, pom.pomstoid, pom.periodacctg, pom.potype, pom.pono, pom.podate, pom.suppoid, pom.posuppref, pom.popaytypeoid, pom.pototalamt, pom.pototaldiscdtl, pom.pomstdisctype, pom.pomstdiscvalue, pom.pomstdiscamt, pom.pototaldisc, pom.pototalnetto, pom.potaxtype, pom.potaxamt, pom.povat, pom.poothercost, pom.pograndtotalamt, pom.pomstnote, pom.pomststatus, pom.pomstres1, s.suppname, s.supptype, pom.createuser, pom.createtime, pom.upduser, pom.updtime, pom.curroid, pom.rateoid, pom.rate2oid, pom.poratetoidrchar, pom.poratetousdchar, pom.porate2toidrchar, pom.porate2tousdchar, ISNULL(suppres3, '') AS suppres3, pom.potaxtype_inex, tolerance, pogroup, popartnerid, potolerance FROM QL_trnpomst pom INNER JOIN QL_mstsupp s ON pom.suppoid=s.suppoid WHERE pom.pomstoid=" & sOid
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        While xreader.Read
            CompnyCode = xreader("cmpcode").ToString
            porawmstoid.Text = Trim(xreader("pomstoid").ToString)
            periodacctg.Text = Trim(xreader("periodacctg").ToString)
            porawtype.SelectedValue = xreader("potype").ToString
            porawno.Text = Trim(xreader("pono").ToString)
            porawdate.Text = Format(xreader("podate"), "MM/dd/yyyy")
            suppoid.Text = Trim(xreader("suppoid").ToString)
            porawsuppref.Text = Trim(xreader("posuppref").ToString)
            porawpaytypeoid.SelectedValue = xreader("popaytypeoid")
            tolerance.Text = ToMaskEdit(ToDouble(Trim(xreader("potolerance").ToString)), iRoundDigit)
            porawtotalamt.Text = ToMaskEdit(ToDouble(Trim(xreader("pototalamt").ToString)), iRoundDigit)
            porawtotaldiscdtl.Text = ToMaskEdit(ToDouble(Trim(xreader("pototaldiscdtl").ToString)), iRoundDigit)
            porawtotalnetto.Text = ToMaskEdit(ToDouble(Trim(xreader("pototalnetto").ToString)), iRoundDigit)
            porawtaxtype.SelectedValue = Trim(xreader("potaxtype").ToString)
            porawtaxamt.Text = ToMaskEdit(ToDouble(Trim(xreader("potaxamt").ToString)), iRoundDigit)
            porawvat.Text = ToMaskEdit(ToDouble(Trim(xreader("povat").ToString)), iRoundDigit)
            porawgrandtotalamt.Text = ToMaskEdit(ToDouble(Trim(xreader("pograndtotalamt").ToString)), iRoundDigit)
            ddlHD.SelectedValue = Trim(xreader("pomstdisctype").ToString)
            tbHD.Text = ToMaskEdit(ToDouble(Trim(xreader("pomstdiscvalue").ToString)), 2)
            HDA.Text = ToMaskEdit(ToDouble(Trim(xreader("pomstdiscamt").ToString)), 2)
            pototaldiscamt.Text = ToMaskEdit(ToDouble(porawtotaldiscdtl.Text) + ToDouble(HDA.Text), 2)
            porawmstnote.Text = Trim(xreader("pomstnote").ToString)
            porawmststatus.Text = Trim(xreader("pomststatus").ToString)
            porawmstres1.Text = Trim(xreader("pomstres1").ToString)
            suppname.Text = Trim(xreader("suppname").ToString)
            suppres3.Text = Trim(xreader("suppres3").ToString)
            createuser.Text = Trim(xreader("createuser").ToString)
            createtime.Text = Trim(xreader("createtime").ToString)
            upduser.Text = Trim(xreader("upduser").ToString)
            updtime.Text = Trim(xreader("updtime").ToString)
            curroid.SelectedValue = xreader("curroid").ToString
            rateoid.Text = Trim(xreader("rateoid").ToString)
            rate2oid.Text = Trim(xreader("rate2oid").ToString)
            DDLinex.SelectedValue = Trim(xreader("potaxtype_inex").ToString)
            tolerance.Text = Trim(xreader("tolerance").ToString)
            DDLTypePO.SelectedValue = Trim(xreader("pogroup").ToString)
            DDLinex_SelectedIndexChanged(Nothing, Nothing)
            porawratetoidr.Text = ToMaskEdit(ToDouble(Trim(xreader("poratetoidrchar").ToString)), GetRoundValue(Trim(xreader("poratetoidrchar").ToString)))
            porawratetousd.Text = ToMaskEdit(ToDouble(Trim(xreader("poratetousdchar").ToString)), GetRoundValue(Trim(xreader("poratetousdchar").ToString)))
            porawrate2toidr.Text = ToMaskEdit(ToDouble(Trim(xreader("porate2toidrchar").ToString)), GetRoundValue(Trim(xreader("porate2toidrchar").ToString)))
            porawrate2tousd.Text = ToMaskEdit(ToDouble(Trim(xreader("porate2tousdchar").ToString)), GetRoundValue(Trim(xreader("porate2tousdchar").ToString)))
            partnerflag.Text = Trim(xreader("popartnerid").ToString)
        End While
        xreader.Close()
        conn.Close()
        btnGenerate.Visible = False
        If porawmststatus.Text = "In Process" Or porawmststatus.Text = "Revised" Then
            btnPrint2.Visible = False
        Else
            btnSave.Visible = False
            btnDelete.Visible = False
            btnSendApproval.Visible = False
            If porawmststatus.Text = "Cancel" Or porawmstres1.Text = "Force Closed" Then
                btnSaveAs.Visible = False
                lblTrnNo.Text = "PO No."
                porawmstoid.Visible = False
                porawno.Visible = True
            Else
                btnAddToList.Visible = False
                gvTblDtl.Columns(0).Visible = False
                gvTblDtl.Columns(gvTblDtl.Columns.Count - 1).Visible = False
                If porawmststatus.Text = "Approved" Or porawmststatus.Text = "Closed" Then
                    lblTrnNo.Text = "PO No."
                    porawmstoid.Visible = False
                    porawno.Visible = True
                End If
            End If
            If porawmststatus.Text = "Cancel" Or porawmststatus.Text = "In Approval" Or porawmststatus.Text = "Rejected" Then
                btnPrint2.Visible = False
            End If
        End If

        sSql = "SELECT pod.podtlseq, pod.matoid, m.itemCode, m.itemoid, m.itemLongDescription itemlongdesc, m.roundQty, pod.poqty,  ISNULL(pod.podtlres1, 0) AS poqtyuse,  pod.pounitoid, g.gendesc AS unit, pod.poprice, pod.podtlamt, pod.podtldisctype, pod.podtldiscvalue, pod.podtldiscamt, pod.podtlnetto, pod.podtlnote, ISNULL((SELECT TOP 1 crd.lastpurchaseprice FROM QL_mstitemprice crd WHERE crd.cmpcode=pod.cmpcode AND crd.refname='SUPPLIER' /*AND crd.refoid=" & suppoid.Text & " */AND crd.itemoid=pod.matoid AND " & sOid & ">lasttransoid AND lasttransoid<>" & sOid & " AND crd.res1=pod.pounitoid AND crd.curroid=" & curroid.SelectedValue & " ORDER BY crd.updtime DESC), 0.0) AS lastpoprice, ISNULL((SELECT TOP 1 crd.curroid FROM QL_mstitemprice crd WHERE crd.cmpcode=pod.cmpcode AND crd.refname='SUPPLIER' AND crd.refoid=" & suppoid.Text & " AND crd.itemoid=pod.matoid AND lasttransoid<>" & sOid & " AND crd.res1=pod.pounitoid ORDER BY crd.updtime DESC), 0.0) AS lastcurroid, poqty_unitbesar, poqty_unitkecil, itemUnit1 AS unitkecil, itemUnit3 AS unitbesar, '" & DDLinex.SelectedValue & "' AS inex, roundqty AS itemlimitqty FROM QL_trnpodtl pod INNER JOIN QL_mstitem m ON pod.matoid=m.itemoid INNER JOIN QL_mstgen g ON pod.pounitoid=g.genoid WHERE pod.pomstoid=" & sOid & " ORDER BY pod.podtlseq"
        Dim objTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnpodtl")
        Session("TblDtl") = objTbl
        gvTblDtl.DataSource = objTbl
        gvTblDtl.DataBind()
        CheckTax()
        EnableType(False)
        ClearDetail()
    End Sub

    Private Sub PrintReport(ByVal sOid As String)
        Try
            Dim sRptName As String = "PORawMatPrintOut"
            report.Load(Server.MapPath(folderReport & "rptPO_TrnEn.rpt"))

			sSql = "select '' as [NO],pom.cmpcode [CMPCODE],pom.pomstoid [OID],convert (char(10),pom.podate,103) [PO DATE],pom.pono [NO PO],supp.suppname [NAMA SUPPLIER],supp.suppaddr [ALAMAT],curr.currdesc [CURRENCY],CURR.CURRSYMBOL [SYMBOL],i.itemLongDescription [ITEM],dtl.poqty [QTY],(select gendesc from QL_mstgen where genoid=dtl.pounitoid)[UNIT],dtl.poprice [PRICE ITEM],dtl.podtldiscamt [DISC ITEM DTL],dtl.podtlnetto [AMT NETTO DTL],pom.potaxtype [TAXTYPE],potaxamt [PCT TAX],povat AS [AMT TAX],pom.pomstdiscamt[DISC HEADER], pograndtotalamt AS  [GRAND TOTAL],pom.updtime [UPDTIME],pom.upduser [UPDUSER],di.divname,di.divaddress,di.divaddress1,di.divaddress2,di.divfax1,di.divphone,gc.gendesc AS kota,gc1.gendesc as paytype, pomstnote AS [Header Note],pom.posuppref AS Up,di.divemail AS [Mail Office],di.divfax2 AS [Mail Faktur] from ql_trnpomst pom inner join QL_trnpodtl dtl on pom.pomstoid = dtl.pomstoid and pom.cmpcode = dtl.cmpcode inner join QL_mstitem i on i.itemoid = dtl.matoid inner join QL_mstcurr curr on curr.curroid = pom.curroid  inner join QL_mstsupp supp on supp.suppoid = pom.suppoid INNER JOIN QL_mstdivision di on di.divcode=pom.cmpcode INNER JOIN QL_mstgen gc ON gc.genoid=di.divcityoid INNER JOIN QL_mstgen gc1 ON gc1.genoid=pom.popaytypeoid"

            If Session("CompnyCode") <> CompnyCode Then
                sSql &= " WHERE pom.cmpcode='" & Session("CompnyCode") & "'"
            Else
                sSql &= " WHERE pom.cmpcode LIKE '%'"
            End If
            If sOid = "" Then
                sSql &= " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sSql &= " AND pom.podate>='" & FilterPeriod1.Text & " 00:00:00' AND pom.podate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sSql &= " AND pom.pomststatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Transaction\trnPurchaseOrder.aspx", Session("SpecialAccess")) = False Then
                    sSql &= " AND pom.createuser='" & Session("UserID") & "'"
                End If
            Else
                sSql &= " AND pom.pomstoid IN (" & sOid & ")"
            End If

            Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnpomst")
            Dim dvTbl As DataView = dtTbl.DefaultView
            report.SetDataSource(dvTbl.ToTable)
            report.SetParameterValue("userId", Session("UserID"))
            report.SetParameterValue("userName", GetStrData("SELECT profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, sRptName)
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Private Sub PrintDoc(ByVal sOid As String)
        Try
            Dim sRptName As String = "PORawMatPrintOut"
            'If DDLTypeSupp.SelectedIndex = 0 Then
            '    report.Load(Server.MapPath(folderReport & "rptPO_Trn.rpt"))
            'Else
            report.Load(Server.MapPath(folderReport & "rptPO_TrnEn.rpt"))
            'End If

            sSql = "select '' as [NO],pom.cmpcode [CMPCODE],pom.pomstoid [OID],convert (char(10),pom.podate,103) [PO DATE],pom.pono [NO PO],supp.suppname [NAMA SUPPLIER],supp.suppaddr [ALAMAT],curr.currdesc [CURRENCY],CURR.CURRSYMBOL [SYMBOL],i.itemLongDescription [ITEM],dtl.poqty [QTY],(select gendesc from QL_mstgen where genoid=dtl.pounitoid)[UNIT],dtl.poprice [PRICE ITEM],dtl.podtldiscamt [DISC ITEM DTL],dtl.podtlnetto [AMT NETTO DTL],pom.potaxtype [TAXTYPE],potaxamt [PCT TAX],povat AS [AMT TAX],pom.pomstdiscamt[DISC HEADER], pograndtotalamt AS [GRAND TOTAL],pom.updtime [UPDTIME],pom.upduser [UPDUSER], pomstnote AS [Header Note]  from ql_trnpomst pom inner join QL_trnpodtl dtl on pom.pomstoid = dtl.pomstoid and pom.cmpcode = dtl.cmpcode inner join QL_mstitem i on i.itemoid = dtl.matoid inner join QL_mstcurr curr on curr.curroid = pom.curroid  inner join QL_mstsupp supp on supp.suppoid = pom.suppoid "

            If Session("CompnyCode") <> CompnyCode Then
                sSql &= " WHERE pom.cmpcode='" & Session("CompnyCode") & "'"
            Else
                sSql &= " WHERE pom.cmpcode LIKE '%'"
            End If
            If sOid = "" Then
                sSql &= " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sSql &= " AND pom.podate>='" & FilterPeriod1.Text & " 00:00:00' AND pom.podate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sSql &= " AND pom.pomststatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Transaction\trnPurchaseOrder.aspx", Session("SpecialAccess")) = False Then
                    sSql &= " AND pom.createuser='" & Session("UserID") & "'"
                End If
            Else
                sSql &= " AND pom.pomstoid IN (" & sOid & ")"
            End If

            Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnpomst")
            Dim dvTbl As DataView = dtTbl.DefaultView
            report.SetDataSource(dvTbl.ToTable)
            report.SetParameterValue("userId", Session("UserID"))
            report.SetParameterValue("userName", GetStrData("SELECT profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            'PrintDoc1.PrinterSettings.PrinterName = "Microsoft XPS Document Writer"
            'Dim k As Integer
            'For k = 0 To PrintDoc1.PrinterSettings.PaperSizes.Count - 1
            '    If PrintDoc1.PrinterSettings.PaperSizes.Item(k).PaperName = "A4" Then
            '        pkSize = PrintDoc1.PrinterSettings.PaperSizes.Item(k)
            '    End If
            'Next
            'report.PrintOptions.PrinterName = "Microsoft XPS Document Writer"
            'report.PrintOptions.PaperSize = CType(pkSize.RawKind, CrystalDecisions.Shared.PaperSize)
            ''report.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Portrait
            'report.PrintOptions.PaperSource = CrystalDecisions.Shared.PaperSource.Manual
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, sRptName)
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Private Sub EnableType(ByVal bVal As Boolean)
        Dim sVal As String = "inpText"
        If bVal = False Then
            sVal = "inpTextDisabled"
        End If
        DDLTypePO.CssClass = sVal
        DDLTypePO.Enabled = bVal
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim scode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = scode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnPurchaseOrder.aspx")
        End If
        If checkPagePermission("~\Transaction\trnPurchaseOrder.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Purchase Order Material"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnSendApproval.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to send this data for APPROVAL?');")
        If Not Page.IsPostBack Then
            ClearTempFile()
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            CheckPOStatus()
            InitAllDDL()

            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            DDLinex_SelectedIndexChanged(Nothing, Nothing)
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                DDLTypePO.CssClass = "inpTextDisabled" : DDLTypePO.Enabled = False
                'DDLinex.CssClass = "inpTextDisabled" : DDLinex.Enabled = False
                TabContainer1.ActiveTabIndex = 1
            Else
                porawmstoid.Text = GenerateID("QL_TRNPOMST", CompnyCode)
                porawdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                curroid_SelectedIndexChanged(Nothing, Nothing)
                porawmststatus.Text = "In Process"
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
                btnDelete.Visible = False
                btnSaveAs.Visible = False
                btnPrint2.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
            CheckTax()
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvTblDtl.DataSource = dt
            gvTblDtl.DataBind()
        End If
        If ToDouble(Format(GetServerTime(), "MM")) = 1 Then
            If ToDouble(Format(GetServerTime(), "dd")) <= 10 Then
                imbpodate.Visible = True
            End If
        Else
            If ToDouble(Format(GetServerTime(), "dd")) <= 3 Then
                imbpodate.Visible = True
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnPurchaseOrder.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lkbPOInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbPOInProcess.Click
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim nDays As Integer = 7
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, pom.updtime, GETDATE()) > " & nDays & " AND pom.pomststatus='In Process' "
        If checkPagePermission("~\Transaction\trnPurchaseOrder.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND pom.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub lkbPOInApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbPOInApproval.Click
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 2
        Dim nDays As Integer = 7
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, pom.updtime, GETDATE()) > " & nDays & " AND pom.pomststatus='In Approval' "
        If checkPagePermission("~\Transaction\trnPurchaseOrder.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND pom.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND pom.podate>='" & FilterPeriod1.Text & " 00:00:00' AND pom.podate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            If FilterDDLStatus.SelectedValue.ToUpper = "FORCE CLOSED" Then
                sSqlPlus &= " AND pom.pomstres1='" & FilterDDLStatus.SelectedValue & "'"
            Else
                sSqlPlus &= " AND pom.pomststatus='" & FilterDDLStatus.SelectedValue & "'"
            End If
        End If
        If checkPagePermission("~\Transaction\trnPurchaseOrder.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND pom.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        Dim sSqlPlus As String = ""
        If checkPagePermission("~\Transaction\trnPurchaseOrder.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND pom.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupp.Click
        FilterDDLListSupp.SelectedIndex = -1 : FilterTextListSupp.Text = "" : gvListSupp.SelectedIndex = -1 : cbTag.Checked = False : FilterDDLTag.SelectedIndex = -1 : FilterDDLType.SelectedIndex = -1
        BindSupplierData()
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
    End Sub

    Protected Sub btnClearSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupp.Click
        suppoid.Text = ""
        suppname.Text = ""
        suppres3.Text = ""
        porawsuppref.Text = ""
        porawpaytypeoid.SelectedIndex = -1
        porawtaxtype.SelectedIndex = -1
        porawtaxamt.Text = ""
        porawtype.SelectedIndex = -1
        porawtaxtype_SelectedIndexChanged(Nothing, Nothing)
        ClearDetail()
        partnerflag.Text = ""
        'Reset DDL Tipe PO
        sSql = "SELECT gencode, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='GROUPITEM' AND activeflag='ACTIVE' ORDER BY gencode"
        FillDDL(DDLTypePO, sSql)
    End Sub

    Protected Sub btnFindListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSupp.Click
        BindSupplierData()
        mpeListSupp.Show()
    End Sub

    Protected Sub btnAllListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListSupp.Click
        FilterDDLListSupp.SelectedIndex = -1 : FilterTextListSupp.Text = "" : gvListSupp.SelectedIndex = -1 : cbTag.Checked = False : FilterDDLTag.SelectedIndex = -1 : FilterDDLType.SelectedIndex = -1
        BindSupplierData()
        mpeListSupp.Show()
    End Sub

    Protected Sub gvListSupp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSupp.PageIndexChanging
        gvListSupp.PageIndex = e.NewPageIndex
        BindSupplierData()
        mpeListSupp.Show()
    End Sub

    Protected Sub gvListSupp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListSupp.SelectedIndexChanged
        If suppoid.Text <> gvListSupp.SelectedDataKey.Item("suppoid").ToString Then
            btnClearSupp_Click(Nothing, Nothing)
        End If
        suppoid.Text = gvListSupp.SelectedDataKey.Item("suppoid").ToString
        suppname.Text = gvListSupp.SelectedDataKey.Item("suppname").ToString
        porawpaytypeoid.SelectedValue = gvListSupp.SelectedDataKey.Item("supppaymentoid").ToString
        suppres3.Text = gvListSupp.SelectedDataKey.Item("suppres3").ToString
        partnerflag.Text = gvListSupp.SelectedDataKey.Item("partnerflag").ToString
        If gvListSupp.SelectedDataKey.Item("supptaxable").ToString = "1" Then
            porawtaxtype.SelectedIndex = 0
            porawtaxamt.Text = GetNewTaxValue(porawdate.Text)
        Else
            porawtaxtype.SelectedIndex = 1
            porawtaxamt.Text = ""
        End If
        porawtaxtype_SelectedIndexChanged(Nothing, Nothing)
        porawtype.SelectedValue = gvListSupp.SelectedDataKey.Item("supptype").ToString
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)

        'Fill DDL Tipe PO
        If partnerflag.Text.ToUpper = "GSMR" Then
            sSql = "SELECT gencode, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='GROUPITEM' AND activeflag='ACTIVE'/* and gencode='RAW'*/ ORDER BY gencode"
            FillDDL(DDLTypePO, sSql)
            DDLTypePO.SelectedValue = "RAW"
        Else
            sSql = "SELECT gencode, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='GROUPITEM' AND activeflag='ACTIVE' ORDER BY gencode"
            FillDDL(DDLTypePO, sSql)
            DDLTypePO.SelectedValue = "FG"
        End If
    End Sub

    Protected Sub lkbCloseListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListSupp.Click
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub curroid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles curroid.SelectedIndexChanged
        Dim sErr As String = ""
        If porawdate.Text <> "" Then
            If IsValidDate(porawdate.Text, "MM/dd/yyyy", sErr) Then
                If curroid.SelectedValue <> "" Then
                    cRate.SetRateValue(CInt(curroid.SelectedValue), porawdate.Text)
                    If cRate.GetRateDailyLastError <> "" Then
                        showMessage(cRate.GetRateDailyLastError, 2)
                        rateoid.Text = "" : porawratetoidr.Text = "" : porawratetousd.Text = ""
                        Exit Sub
                    End If
                    If cRate.GetRateMonthlyLastError <> "" Then
                        showMessage(cRate.GetRateMonthlyLastError, 2)
                        rate2oid.Text = "" : porawrate2toidr.Text = "" : porawrate2tousd.Text = ""
                        Exit Sub
                    End If
                    rateoid.Text = cRate.GetRateDailyOid
                    porawratetoidr.Text = ToMaskEdit(cRate.GetRateDailyIDRValue, GetRoundValue(cRate.GetRateDailyIDRValue.ToString))
                    porawratetousd.Text = ToMaskEdit(cRate.GetRateDailyUSDValue, GetRoundValue(cRate.GetRateDailyUSDValue.ToString))
                    rate2oid.Text = cRate.GetRateMonthlyOid
                    porawrate2toidr.Text = ToMaskEdit(cRate.GetRateMonthlyIDRValue, GetRoundValue(cRate.GetRateMonthlyIDRValue.ToString))
                    porawrate2tousd.Text = ToMaskEdit(cRate.GetRateMonthlyUSDValue, GetRoundValue(cRate.GetRateMonthlyUSDValue.ToString))
                End If
            End If
        End If
    End Sub

    Protected Sub porawtaxtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles porawtaxtype.SelectedIndexChanged
        CheckTax()
        DDLinex.SelectedIndex = 0
        'RegenerateDtl()
        CountHdrAmount()
        If partnerflag.Text.ToUpper = "GSMR" Then
            'DDLinex.Enabled = False
            DDLinex.SelectedValue = "INC"
            'DDLinex.CssClass = "inpTextDisabled"
            'DDLinex_SelectedIndexChanged(Nothing, Nothing)
        Else
            'DDLinex.Enabled = True
            DDLinex.SelectedIndex = -1
            'DDLinex.CssClass = "inpText"
            'DDLinex_SelectedIndexChanged(Nothing, Nothing)
        End If
    End Sub

    Protected Sub porawtaxamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles porawtaxamt.TextChanged
        porawtaxamt.Text = ToMaskEdit(ToDouble(porawtaxamt.Text), iRoundDigit)
        CountHdrAmount()
    End Sub

    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGenerate.Click
        If CompnyCode = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        Dim sErr As String = ""
        If porawdate.Text = "" Then
            showMessage("Please fill PO Date first!", 2)
            Exit Sub
        Else
            If Not IsValidDate(porawdate.Text, "MM/dd/yyyy", sErr) Then
                showMessage("PO Date is invalid. " & sErr, 2)
                Exit Sub
            End If
        End If
        If suppoid.Text = "" Then
            showMessage("Please select Supplier first!", 2)
            Exit Sub
        End If
        sSql = "SELECT 0 AS podtlseq, prd.prmstoid, prm.prno, CONVERT(VARCHAR(10), prm.prdate, 101) AS prdate, prd.prdtloid, prd.matoid, m.matcode, m.matlongdesc, 0.0 AS poqty, ISNULL((prd.prqty - (SELECT ISNULL(SUM(pod.poqty), 0) FROM QL_trnpodtl pod INNER JOIN QL_trnpomst pom ON pom.cmpcode=pod.cmpcode AND pom.pomstoid=pod.pomstoid WHERE pod.cmpcode=prd.cmpcode AND pod.prdtloid=prd.prdtloid AND pom.pomststatus NOT IN ('Cancel', 'Rejected'))), 0) AS prqty, prd.prunitoid AS pounitoid, g2.gendesc AS unit, ISNULL((SELECT crd.lastpurchaseprice FROM QL_mstmatprice crd WHERE crd.cmpcode=prd.cmpcode AND crd.refname='SUPPLIER' AND crd.refoid=" & suppoid.Text & " AND crd.matoid=prd.matoid AND crd.curroid=" & curroid.SelectedValue & " and crd.res3=g2.gendesc), 0.0) AS poprice, 0.0 AS lastpoprice, 0.0 AS podtlamt, 'P' AS podtldisctype, 0.0 AS podtldiscvalue, 0.0 AS podtldiscamt, 0.0 AS podtlnetto, 'Generated Detail' AS podtlnote, m.matlimitqty FROM QL_prmst prm INNER JOIN QL_prdtl prd ON prm.cmpcode=prd.cmpcode AND prm.prmstoid=prd.prmstoid INNER JOIN QL_mstmat m ON prd.matoid=m.matoid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=m.cmpcode AND c1.cat1code=SUBSTRING(m.matcode, 1, 2) AND cat1res1='' INNER JOIN QL_mstgen g2 ON prd.prunitoid=g2.genoid WHERE prm.cmpcode='" & CompnyCode & "' AND prm.prmststatus='Approved' AND prd.prdtlstatus='' "
        If suppres3.Text <> "" Then
            sSql &= " AND ("
            Dim sData() As String = suppres3.Text.Split(";")
            For C1 As Integer = 0 To sData.Length - 1
                sSql &= "c1.cat1shortdesc='" & sData(C1) & "'"
                If C1 < sData.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_prdtl")
        If dtTbl.Rows.Count > 0 Then
            For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                dtTbl.Rows(C1)("podtlseq") = C1 + 1
                Dim dQty As Double = Math.Ceiling(dtTbl.Rows(C1)("prqty") / dtTbl.Rows(C1)("matlimitqty")) * dtTbl.Rows(C1)("matlimitqty")
                dtTbl.Rows(C1)("poqty") = dQty
                Dim dAmt As Double = dQty * dtTbl.Rows(C1)("poprice")
                dtTbl.Rows(C1)("podtlamt") = dAmt
                dtTbl.Rows(C1)("podtlnetto") = dAmt
                dtTbl.Rows(C1)("lastpoprice") = ToDouble(dtTbl.Rows(C1)("poprice").ToString)
            Next
            Session("TblDtl") = dtTbl
            gvTblDtl.DataSource = Session("TblDtl")
            gvTblDtl.DataBind()
            CountHdrAmount()
        Else
            showMessage("There is no data can't be generated for this time.", 2)
            Exit Sub
        End If
    End Sub

    Protected Sub btnFindListPR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListPR.Click
        BindPRData()
        mpeListPR.Show()
    End Sub

    Protected Sub btnAllListPR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListPR.Click
        FilterDDLListPR.SelectedIndex = -1 : FilterTextListPR.Text = "" : gvListPR.SelectedIndex = -1
        BindPRData()
        mpeListPR.Show()
    End Sub

    Protected Sub gvListPR_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListPR.PageIndexChanging
        gvListPR.PageIndex = e.NewPageIndex
        gvListPR.DataSource = Session("listPR")
        gvListPR.DataBind()
        mpeListPR.Show()
    End Sub

    Protected Sub lbCloseListPR_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListPR.Click
        cProc.SetModalPopUpExtender(btnHideListPR, pnlListPR, mpeListPR, False)
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        If suppoid.Text = "" Then
            showMessage("Please select Supplier first!", 2)
            Exit Sub
        End If

        Dim sErr As String = ""
        If porawdate.Text = "" Then
            showMessage("Please fill PO Date first!", 2)
            Exit Sub
        Else
            If Not IsValidDate(porawdate.Text, "MM/dd/yyyy", sErr) Then
                showMessage("PO Date is invalid. " & sErr, 2)
                Exit Sub
            End If
        End If
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : Session("TblMat") = Nothing : Session("TblMatView") = Nothing : gvListMat.DataSource = Nothing : gvListMat.DataBind() : tbData.Text = "5" : gvListMat.PageSize = CInt(tbData.Text)
        BindMaterialData()
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim sFilter As String = ""
            Dim sSplit() As String = FilterTextListMat.Text.Split(" ")
            For C1 As Integer = 0 To sSplit.Length - 1
                sFilter &= FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(sSplit(C1)) & "%'"
                If C1 < sSplit.Length - 1 Then
                    sFilter &= " AND "
                End If
            Next
            dtView.RowFilter = sFilter
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If UpdateCheckedMat() Then
            Session("TblMatView") = Session("TblMat")
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("CheckValue") = "True"
                    dtTbl.Rows(C1)("CheckValue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                dtTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "No material data can't be selected!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "No material data can't be selected!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("CheckValue") = "False"
                    objView(0)("poqty") = 0
                    objView(0)("poprice") = 0
                    objView(0)("podtlnote") = ""
                    dtTbl.Rows(C1)("CheckValue") = "False"
                    dtTbl.Rows(C1)("poqty") = 0
                    dtTbl.Rows(C1)("poprice") = 0
                    dtTbl.Rows(C1)("podtlnote") = ""
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                dtTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "No material data can't be selected!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "No material data can't be selected!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "CheckValue='True'"
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListMat") = "No material data have been selected before!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(4).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    End If
                End If
            Next
            cc = e.Row.Cells(5).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    End If
                End If
            Next
            Dim iItemOid As Integer = 0
            cc = e.Row.Cells(0).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    iItemOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                End If
            Next
            cc = e.Row.Cells(3).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                    FillDDL(CType(myControl, System.Web.UI.WebControls.DropDownList), "select itemUnit1, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit1 where itemoid=" & iItemOid & "")
                    CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue = CType(myControl, System.Web.UI.WebControls.DropDownList).ToolTip
                End If
            Next
        End If
    End Sub

    Protected Sub lbShowData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbShowData.Click
        If tbData.Text = "" Then
            Session("WarningListMat") = "Please fill total data to be shown!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        Else
            If ToDouble(tbData.Text) <= 0 Then
                Session("WarningListMat") = "Total data to be shown must be more than 0!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat2() Then
            gvListMat.PageSize = Int(tbData.Text)
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub lbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListListMat.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "CheckValue='True'"
            Dim iCheck As Integer = dtView.Count
            If iCheck > 0 Then
                dtView.RowFilter = ""
                dtView.RowFilter = "CheckValue='True' AND poqty>0"
                If dtView.Count <> iCheck Then
                    Session("WarningListMat") = "PO qty for every checked material data must be more than 0!"
                    showMessage(Session("WarningListMat"), 2)
                    dtView.RowFilter = ""
                    Exit Sub
                End If
                dtView.RowFilter = ""
                If POAllowNull = "False" Then
                    dtView.RowFilter = "CheckValue='True' AND poqty>0 AND poprice>0"
                Else
                    dtView.RowFilter = "CheckValue='True' AND poqty>0 AND poprice>=0"
                End If
                If dtView.Count <> iCheck Then
                    Session("WarningListMat") = "PO price for every checked material data must be more than 0!"
                    showMessage(Session("WarningListMat"), 2)
                    dtView.RowFilter = ""
                    Exit Sub
                End If
                'If Not IsQtyRounded(dtView, "poqty", "roundqty") Then
                '    Session("WarningListMat") = "Quantity for every checked material data must be rounded by Round Qty!"
                '    showMessage(Session("WarningListMat"), 2)
                '    dtView.RowFilter = ""
                '    Exit Sub
                'End If
                If Session("TblDtl") Is Nothing Then
                    CreateTblDetail()
                End If
                Dim objTable As DataTable
                objTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                Dim dQty_unitkecil, dQty_unitbesar As Double
                Dim objRow As DataRow
                Dim counter As Integer = objTable.Rows.Count
                Dim dValue, dDisc As Double
                For C1 As Integer = 0 To dtView.Count - 1
                    dv.RowFilter = "itemoid=" & dtView(C1)("itemoid")
                    If dv.Count > 0 Then
                        dv.AllowEdit = True
                        GetUnitConverter(dtView(C1)("itemoid"), dtView(C1)("selectedunitoid"), ToDouble(dtView(C1)("poqty")), dQty_unitkecil, dQty_unitbesar)
                        dv(0)("poqty") = dtView(C1)("poqty")
                        Dim dPriceInc As Double = 0
                        dv(0)("podtldiscamt") = dDisc
                        If (DDLinex.Text = "INC") Then
                            dv(0)("poprice") = dtView(C1)("poprice") / GetNewTaxValueInclude(porawdate.Text)
                            dPriceInc = dtView(C1)("poprice") / GetNewTaxValueInclude(porawdate.Text)
                            dValue = dPriceInc * dtView(C1)("poqty")
                            dv(0)("podtlamt") = dValue 'Math.Round(dValue, MidpointRounding.AwayFromZero)
                            dv(0)("podtlnetto") = dValue - dDisc 'Math.Round(dValue - dDisc, MidpointRounding.AwayFromZero)
                        Else
                            dv(0)("poprice") = dtView(C1)("poprice")
                            dPriceInc = dtView(C1)("poprice")
                            dValue = dPriceInc * dtView(C1)("poqty")
                            dv(0)("podtlamt") = Math.Round(dValue, MidpointRounding.AwayFromZero)
                            dv(0)("podtlnetto") = Math.Round(dValue - dDisc, MidpointRounding.AwayFromZero)
                        End If
                        If dv(0)("podtldisctype") = "P" Then
                            dDisc = (dValue * dv(0)("podtldiscvalue")) / 100
                        Else
                            dDisc = dv(0)("podtldiscvalue")
                        End If
                        dv(0)("podtlnote") = dtView(C1)("podtlnote")
                        dv(0)("poqty_unitkecil") = dQty_unitkecil
                        dv(0)("poqty_unitbesar") = dQty_unitbesar
                    Else
                        counter += 1
                        objRow = objTable.NewRow()
                        GetUnitConverter(dtView(C1)("itemoid"), dtView(C1)("selectedunitoid"), ToDouble(dtView(C1)("poqty")), dQty_unitkecil, dQty_unitbesar)
                        objRow("podtlseq") = counter
                        objRow("itemoid") = dtView(C1)("itemoid")
                        objRow("itemcode") = dtView(C1)("itemcode").ToString
                        objRow("itemlongdesc") = dtView(C1)("itemlongdesc").ToString
                        objRow("poqty") = dtView(C1)("poqty")
                        objRow("pounitoid") = dtView(C1)("selectedunitoid")
                        objRow("unit") = dtView(C1)("selectedunit").ToString
                        objRow("podtldiscamt") = dDisc
                        Dim dPriceInc As Double = 0
                        If (DDLinex.Text = "INC") Then
                            objRow("poprice") = dtView(C1)("poprice") / GetNewTaxValueInclude(porawdate.Text)
                            dPriceInc = dtView(C1)("poprice") / GetNewTaxValueInclude(porawdate.Text)
                            dValue = dPriceInc * dtView(C1)("poqty")
                            objRow("podtlamt") = dValue 'Math.Round(dValue, MidpointRounding.AwayFromZero)
                            objRow("podtlnetto") = dValue - dDisc 'Math.Round(dValue - dDisc, MidpointRounding.AwayFromZero)
                        Else
                            objRow("poprice") = dtView(C1)("poprice")
                            dPriceInc = dtView(C1)("poprice")
                            dValue = dPriceInc * dtView(C1)("poqty")
                            objRow("podtlamt") = Math.Round(dValue, MidpointRounding.AwayFromZero)
                            objRow("podtlnetto") = Math.Round(dValue - dDisc, MidpointRounding.AwayFromZero)
                        End If
                        objRow("podtldisctype") = porawdtldisctype.SelectedValue
                        objRow("podtldiscvalue") = ToDouble(porawdtldiscvalue.Text)
                        If porawdtldisctype.SelectedValue = "P" Then
                            dDisc = (dValue * ToDouble(porawdtldiscvalue.Text)) / 100
                        Else
                            dDisc = ToDouble(porawdtldiscvalue.Text)
                        End If
                        objRow("podtlnote") = dtView(C1)("podtlnote")
                        objRow("itemlimitqty") = dtView(C1)("roundqty")
                        Dim dVal As Double = ToDouble(GetStrData("SELECT TOP 1 ISNULL(crd.lastpurchaseprice,0.0) FROM QL_mstitemprice crd WHERE crd.cmpcode='" & CompnyCode & "' AND crd.refname='SUPPLIER' AND crd.itemoid=" & dtView(C1)("itemoid") & " AND crd.res1='" & dtView(C1)("selectedunitoid") & "' ORDER BY crd.updtime DESC"))
                        objRow("lastpoprice") = dVal
                        Dim iCurrOid As Integer = cKon.ambilscalar("SELECT TOP 1 curroid FROM QL_mstitemprice crd WHERE crd.cmpcode='" & CompnyCode & "' AND crd.refname='SUPPLIER' AND crd.itemoid=" & dtView(C1)("itemoid") & " AND crd.res1='" & dtView(C1)("selectedunitoid") & "' ORDER BY crd.updtime DESC")
                        objRow("lastcurroid") = iCurrOid
                        objRow("poqty_unitkecil") = dQty_unitkecil
                        objRow("poqty_unitbesar") = dQty_unitbesar
                        objRow("inex") = DDLinex.SelectedValue
                        objTable.Rows.Add(objRow)
                    End If
                    dv.RowFilter = ""
                Next
                dtView.RowFilter = ""
                Session("TblDtl") = objTable
                gvTblDtl.DataSource = objTable
                gvTblDtl.DataBind()
                CountHdrAmount()
                EnableType(False)
                If cbOpenListMatUsage.Checked Then
                    btnSearchMat_Click(Nothing, Nothing)
                Else
                    CountHdrAmount()
                    ClearDetail()
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                End If
            Else
                Session("WarningListMat") = "Please select material to add to list!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        Else
            Session("WarningListMat") = "Please select material to add to list!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub porawqty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles porawqty.TextChanged
        porawqty.Text = ToMaskEdit(ToDouble(porawqty.Text), 4)
        CountAllDtlAmount()
    End Sub

    Protected Sub porawprice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles porawprice.TextChanged
        porawprice.Text = ToMaskEdit(ToDouble(porawprice.Text), iRoundDigit)
        CountAllDtlAmount()
    End Sub

    Protected Sub porawdtldisctype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles porawdtldisctype.SelectedIndexChanged
        CountAllDtlAmount()
    End Sub

    Protected Sub porawdtldiscvalue_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles porawdtldiscvalue.TextChanged
        porawdtldiscvalue.Text = ToMaskEdit(ToDouble(porawdtldiscvalue.Text), iRoundDigit)
        CountAllDtlAmount()
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            Dim objTable As DataTable
            objTable = Session("TblDtl")
            Dim objRow As DataRow
            objRow = objTable.Rows(porawdtlseq.Text - 1)
            objRow.BeginEdit()
            Dim dPriceInc As Double = 0
            objRow("podtldiscamt") = porawdtldiscamt.Text
            If DDLinex.Text = "INC" Then
                dPriceInc = ToDouble(varprice.Text) / GetNewTaxValueInclude(porawdate.Text)
                objRow("poprice") = dPriceInc
                objRow("podtlamt") = dPriceInc * ToDouble(porawqty.Text)
                objRow("podtlnetto") = objRow("podtlamt") - ToDouble(porawdtldiscamt.Text)
            Else
                dPriceInc = ToDouble(porawprice.Text)
                objRow("poprice") = dPriceInc
                objRow("podtlamt") = Math.Round(dPriceInc * ToDouble(porawqty.Text), MidpointRounding.AwayFromZero)
                objRow("podtlnetto") = Math.Round(objRow("podtlamt") - ToDouble(porawdtldiscamt.Text), MidpointRounding.AwayFromZero)
            End If
            objRow("poqty") = porawqty.Text
            objRow("podtldisctype") = porawdtldisctype.SelectedValue
            objRow("podtldiscvalue") = porawdtldiscvalue.Text
            objRow("podtlnote") = porawdtlnote.Text
            objRow("poqty_unitkecil") = ToDouble(porawqty.Text)
            objRow("poqty_unitbesar") = ToDouble(porawqty.Text)
            objRow("pounitoid") = porawunitoid.SelectedValue
            objRow("unit") = porawunitoid.SelectedItem.Text
            objRow.EndEdit()
            If cbPrice.Checked Or cbDisc.Checked Then
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "itemoid=" & matrawoid.Text & " AND podtlseq<>" & porawdtlseq.Text
                If dv.Count > 0 Then
                    For C1 As Integer = 0 To dv.Count - 1
                        Dim dr As DataRow = objTable.Rows(dv.Item(C1).Item("podtlseq") - 1)
                        dr.BeginEdit()
                        If cbPrice.Checked Then
                            If DDLinex.Text = "INC" Then
                                dr("poprice") = dPriceInc
                                dr("podtlamt") = dPriceInc * ToDouble(dr("poqty")) 'Math.Round(ToDouble(porawprice.Text) * ToDouble(dr("poqty")), MidpointRounding.AwayFromZero)
                            Else
                                dr("poprice") = dPriceInc
                                dr("podtlamt") = Math.Round(ToDouble(porawprice.Text) * ToDouble(dr("poqty")), MidpointRounding.AwayFromZero)
                            End If
                        End If
                        If cbDisc.Checked Then
                            dr("podtldisctype") = porawdtldisctype.SelectedValue
                            dr("podtldiscvalue") = porawdtldiscvalue.Text
                        End If
                        If dr("podtldisctype") = "P" Then
                            dr("podtldiscamt") = dr("podtlamt") * dr("podtldiscvalue") / 100
                        Else
                            dr("podtldiscamt") = dr("podtldiscvalue")
                        End If
                        dr("podtlnetto") = dr("podtlamt") - dr("podtldiscamt")
                        dr.EndEdit()
                    Next
                End If
                dv.RowFilter = ""
            End If
            Session("TblDtl") = objTable
            gvTblDtl.DataSource = objTable
            gvTblDtl.DataBind()
            CountHdrAmount()
            CountAllDtlAmount()
            EnableType(False)
            ClearDetail()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvTblDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTblDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), iRoundDigit)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), iRoundDigit)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), iRoundDigit)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), iRoundDigit)
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), iRoundDigit)
            e.Row.Cells(10).Text = ToMaskEdit(ToDouble(e.Row.Cells(10).Text), iRoundDigit)
        End If
    End Sub

    Protected Sub gvTblDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvTblDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("podtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvTblDtl.DataSource = objTable
        gvTblDtl.DataBind()
        CountHdrAmount()
        ClearDetail()
    End Sub

    Protected Sub gvTblDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvTblDtl.SelectedIndexChanged
        Try
            porawdtlseq.Text = gvTblDtl.SelectedDataKey.Item("podtlseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable
                objTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "podtlseq=" & porawdtlseq.Text
                matrawoid.Text = dv.Item(0).Item("itemoid").ToString
                matrawcode.Text = dv.Item(0).Item("itemcode").ToString
                matrawlongdesc.Text = dv.Item(0).Item("itemlongdesc").ToString
                porawqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("poqty").ToString), iRoundDigit)
                porawprice.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("poprice").ToString), iRoundDigit)
                porawdtlamt.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("podtlamt").ToString), iRoundDigit)
                porawdtldisctype.SelectedValue = dv.Item(0).Item("podtldisctype").ToString
                porawdtldiscvalue.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("podtldiscvalue").ToString), iRoundDigit)
                porawdtldiscamt.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("podtldiscamt").ToString), iRoundDigit)
                porawdtlnetto.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("podtlnetto").ToString), iRoundDigit)
                porawdtlnote.Text = dv.Item(0).Item("podtlnote").ToString
                InitDDLUnit(dv.Item(0).Item("itemoid").ToString)
                porawunitoid.SelectedValue = dv.Item(0).Item("pounitoid").ToString
                If DDLinex.Text = "INC" Then
                    varprice.Text = ToMaskEdit(Math.Round(ToDouble(dv.Item(0).Item("poprice").ToString) * GetNewTaxValueInclude(porawdate.Text), MidpointRounding.AwayFromZero), iRoundDigit)
                End If
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
            Exit Sub
        Finally
            btnSearchMat.Visible = False
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        curroid_SelectedIndexChanged(Nothing, Nothing)
        If IsInputValid() Then
            porawdtloid.Text = GenerateID("QL_TRNPODTL", CompnyCode)
            Dim iAppOid As Integer = GenerateID("QL_APPROVAL", CompnyCode)
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                If CheckDataExists("SELECT COUNT(*) FROM QL_trnpomst WHERE pomstoid=" & porawmstoid.Text) Then
                    porawmstoid.Text = GenerateID("QL_TRNPOMST", CompnyCode)
                    Dim sError As String = CheckQtyIsUsedByAnotherUser()
                    If sError <> "" Then
                        showMessage(sError, 2)
                        porawmststatus.Text = "In Process"
                        Exit Sub
                    End If
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_trnpomst", "pomstoid", porawmstoid.Text, "pomststatus", updtime.Text, "Approved")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    porawmststatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            If porawmststatus.Text = "Revised" Then
                porawmststatus.Text = "In Process"
            End If
            Dim periodacctg As String = GetDateToPeriodAcctg(porawdate.Text)
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trnpomst (cmpcode, pomstoid, periodacctg, potype, pogroup, podate, pono, suppoid, posuppref, potolerance, popaytypeoid, curroid, rateoid, potaxtype_inex, pototalamt, pototaldiscdtl, pomstdisctype, pomstdiscvalue, pomstdiscamt, pototaldisc, pototalnetto, potaxtype, potaxamt, povat, podeliverycost, poothercost, pograndtotalamt, pomstnote, pomststatus, createuser, createtime, upduser, updtime, rate2oid, poratetoidr, porate2toidr, pototalamtidr, pototaldiscdtlidr, pomstdiscamtidr, pototaldiscidr, pototalnettoidr, povatidr, podeliverycostidr, poothercostidr, pograndtotalamtidr, poratetousd, porate2tousd, pototalamtusd, pototaldiscdtlusd, pomstdiscamtusd, pototaldiscusd, pototalnettousd, povatusd, podeliverycostusd, poothercostusd, pograndtotalamtusd, poratetoidrchar, poratetousdchar, porate2toidrchar, porate2tousdchar, tolerance, popartnerid) VALUES ('" & CompnyCode & "', " & porawmstoid.Text & ", '" & periodacctg & "', '" & porawtype.SelectedValue & "', '" & DDLTypePO.SelectedValue & "', '" & CDate(porawdate.Text) & "', '" & porawno.Text & "', " & suppoid.Text & ", '" & Tchar(porawsuppref.Text) & "', " & ToDouble(tolerance.Text) & ", " & porawpaytypeoid.SelectedValue & ", " & curroid.SelectedValue & ", " & rateoid.Text & ", '" & IIf(porawtaxtype.SelectedValue.ToUpper = "TAX", DDLinex.SelectedValue, "") & "', " & ToDouble(porawtotalamt.Text) & ", " & ToDouble(porawtotaldiscdtl.Text) & ", '" & ddlHD.SelectedValue & "', " & ToDouble(tbHD.Text) & ", " & ToDouble(HDA.Text) & ", " & ToDouble(pototaldiscamt.Text) & ", " & ToDouble(porawtotalnetto.Text) & ", '" & porawtaxtype.SelectedValue & "', " & ToDouble(porawtaxamt.Text) & ", " & ToDouble(porawvat.Text) & ", 0, 0, " & ToDouble(porawgrandtotalamt.Text) & ", '" & Tchar(porawmstnote.Text) & "', '" & porawmststatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & rate2oid.Text & ", " & ToDouble(porawratetoidr.Text) & ", " & ToDouble(porawrate2toidr.Text) & ", " & ToDouble(porawtotalamt.Text) * ToDouble(porawrate2toidr.Text) & ", " & ToDouble(porawtotaldiscdtl.Text) * ToDouble(porawrate2toidr.Text) & ", 0, 0, " & ToDouble(porawtotalnetto.Text) * ToDouble(porawrate2toidr.Text) & ", " & ToDouble(porawvat.Text) * ToDouble(porawrate2toidr.Text) & ", 0, 0, " & ToDouble(porawgrandtotalamt.Text) * ToDouble(porawrate2toidr.Text) & ", " & ToDouble(porawratetousd.Text) & ", " & ToDouble(porawrate2tousd.Text) & ", " & ToDouble(porawtotalamt.Text) * ToDouble(porawrate2tousd.Text) & ", " & ToDouble(porawtotaldiscdtl.Text) * ToDouble(porawrate2tousd.Text) & ", 0, 0, " & ToDouble(porawtotalnetto.Text) * ToDouble(porawrate2tousd.Text) & ", " & ToDouble(porawvat.Text) * ToDouble(porawrate2tousd.Text) & ", 0, 0, " & ToDouble(porawgrandtotalamt.Text) * ToDouble(porawrate2tousd.Text) & ", '" & ToDouble(Left(porawratetoidr.Text, 50)) & "', '" & ToDouble(Left(porawratetousd.Text, 50)) & "', '" & ToDouble(Left(porawrate2toidr.Text, 50)) & "', '" & ToDouble(Left(porawrate2tousd.Text, 50)) & "', " & ToDouble(tolerance.Text) & ", 0)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_mstoid SET lastoid=" & porawmstoid.Text & " WHERE tablename='QL_trnpomst' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trnpomst SET periodacctg='" & periodacctg & "', potype='" & porawtype.SelectedValue & "', pono='" & porawno.Text & "', podate='" & porawdate.Text & "', suppoid=" & suppoid.Text & ", posuppref='" & Tchar(porawsuppref.Text) & "', popaytypeoid=" & porawpaytypeoid.SelectedValue & ", pototalamt=" & ToDouble(porawtotalamt.Text) & ", pototaldiscdtl=" & ToDouble(porawtotaldiscdtl.Text) & ",  pototalnetto=" & ToDouble(porawtotalnetto.Text) & ", potaxtype='" & porawtaxtype.SelectedValue & "', potaxamt=" & ToDouble(porawtaxamt.Text) & ", povat=" & ToDouble(porawvat.Text) & ", pograndtotalamt=" & ToDouble(porawgrandtotalamt.Text) & ", pomstnote='" & Tchar(porawmstnote.Text.Trim) & "', pomststatus='" & porawmststatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, curroid=" & curroid.SelectedValue & ", rateoid=" & rateoid.Text & ", poratetoidr=" & ToDouble(porawratetoidr.Text) & ", pototalamtidr=" & ToDouble(porawtotalamt.Text) * ToDouble(porawrate2toidr.Text) & ", pototaldiscdtlidr=" & ToDouble(porawtotaldiscdtl.Text) * ToDouble(porawrate2toidr.Text) & ", pototalnettoidr=" & ToDouble(porawtotalnetto.Text) * ToDouble(porawrate2toidr.Text) & ", povatidr=" & ToDouble(porawvat.Text) * ToDouble(porawrate2toidr.Text) & ", pograndtotalamtidr=" & ToDouble(porawgrandtotalamt.Text) * ToDouble(porawrate2toidr.Text) & ", poratetousd=" & ToDouble(porawratetousd.Text) & ", pototalamtusd=" & ToDouble(porawtotalamt.Text) * ToDouble(porawrate2tousd.Text) & ", pototaldiscdtlusd=" & ToDouble(porawtotaldiscdtl.Text) * ToDouble(porawrate2tousd.Text) & ", pototalnettousd=" & ToDouble(porawtotalnetto.Text) * ToDouble(porawrate2tousd.Text) & ", povatusd=" & ToDouble(porawvat.Text) * ToDouble(porawrate2tousd.Text) & ",  pograndtotalamtusd=" & ToDouble(porawgrandtotalamt.Text) * ToDouble(porawrate2tousd.Text) & ",rate2oid=" & rate2oid.Text & ", porate2toidr=" & ToDouble(porawrate2toidr.Text) & ", porate2tousd=" & ToDouble(porawrate2tousd.Text) & ", poratetoidrchar='" & ToDouble(Left(porawratetoidr.Text, 50)) & "', poratetousdchar='" & ToDouble(Left(porawratetousd.Text, 50)) & "', porate2toidrchar='" & ToDouble(Left(porawrate2toidr.Text, 50)) & "', porate2tousdchar='" & ToDouble(Left(porawrate2tousd.Text, 50)) & "', potaxtype_inex='" & IIf(porawtaxtype.SelectedValue.ToUpper = "TAX", DDLinex.SelectedValue, "") & "', potolerance=" & ToDouble(tolerance.Text) & ", pomstdisctype='" & ddlHD.SelectedValue & "', pomstdiscvalue=" & ToDouble(tbHD.Text) & ", pomstdiscamt=" & ToDouble(HDA.Text) & ", pototaldisc=" & ToDouble(pototaldiscamt.Text) & " WHERE cmpcode='" & CompnyCode & "' AND pomstoid=" & porawmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    sSql = "DELETE FROM QL_trnpodtl WHERE cmpcode='" & CompnyCode & "' AND pomstoid=" & porawmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = SortingDataTable(Session("TblDtl"), "podtlseq")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trnpodtl (cmpcode, podtloid, pomstoid, podtlseq, matoid, poqty, pounitoid, poprice, podtlamt, podtldisctype, podtldiscvalue, podtldiscamt, podtlnetto, podtlnote, podtlstatus, upduser, updtime, popriceidr, podtlamtidr, podtldiscamtidr, podtlnettoidr, popriceusd, podtlamtusd, podtldiscamtusd, podtlnettousd, poqty_unitkecil, poqty_unitbesar, povarpriceidr, povarpriceusd) VALUES ('" & CompnyCode & "', " & (C1 + CInt(porawdtloid.Text)) & ", " & porawmstoid.Text & ",  " & C1 + 1 & ", " & objTable.Rows(C1).Item("itemoid") & ", " & ToDouble(objTable.Rows(C1).Item("poqty").ToString) & ", " & objTable.Rows(C1).Item("pounitoid") & ", " & ToDouble(objTable.Rows(C1).Item("poprice").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("podtlamt").ToString) & ", '" & objTable.Rows(C1).Item("podtldisctype").ToString & "', " & ToDouble(objTable.Rows(C1).Item("podtldiscvalue").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("podtldiscamt").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("podtlnetto").ToString) & ", '" & Tchar(objTable.Rows(C1).Item("podtlnote")) & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(C1).Item("poprice").ToString) * ToDouble(porawrate2toidr.Text) & ", " & ToDouble(objTable.Rows(C1).Item("podtlamt").ToString) * ToDouble(porawrate2toidr.Text) & ", " & ToDouble(objTable.Rows(C1).Item("podtldiscamt").ToString) * ToDouble(porawrate2toidr.Text) & ", " & ToDouble(objTable.Rows(C1).Item("podtlnetto").ToString) * ToDouble(porawrate2toidr.Text) & ", " & ToDouble(objTable.Rows(C1).Item("poprice").ToString) * ToDouble(porawrate2tousd.Text) & ", " & ToDouble(objTable.Rows(C1).Item("podtlamt").ToString) * ToDouble(porawrate2tousd.Text) & ", " & ToDouble(objTable.Rows(C1).Item("podtldiscamt").ToString) * ToDouble(porawrate2tousd.Text) & ", " & ToDouble(objTable.Rows(C1).Item("podtlnetto").ToString) * ToDouble(porawrate2tousd.Text) & ", " & ToDouble(objTable.Rows(C1).Item("poqty_unitkecil").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("poqty_unitbesar").ToString) & ", " & IIf(DDLinex.SelectedItem.Text = "INC", ToDouble(objTable.Rows(C1).Item("poprice").ToString) * ToDouble(porawrate2toidr.Text) * GetNewTaxValueInclude(porawdate.Text), ToDouble(objTable.Rows(C1).Item("poprice").ToString) * ToDouble(porawrate2toidr.Text)) & ", " & IIf(DDLinex.SelectedItem.Text = "INC", ToDouble(objTable.Rows(C1).Item("poprice").ToString) * ToDouble(porawrate2tousd.Text) * GetNewTaxValueInclude(porawdate.Text), ToDouble(objTable.Rows(C1).Item("poprice").ToString) * ToDouble(porawrate2tousd.Text)) & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()

                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(porawdtloid.Text)) & " WHERE tablename='QL_TRNPODTL' AND cmpcode='" & CompnyCode & "' "
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If porawmststatus.Text = "In Approval" Then
                    Dim dt As DataTable = Session("AppPerson")
                    Dim tempAppCode As String = ""
                    For C1 As Integer = 0 To dt.Rows.Count - 1
                        If C1 = 0 Then
                            tempAppCode = dt.Rows(C1)("apppersonlevel").ToString
                        End If
                        sSql = "INSERT INTO QL_approval (cmpcode, approvaloid, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvalnote, posisiLevel) VALUES ('" & CompnyCode & "', " & iAppOid + C1 & ", 'PO-" & DDLTypePO.SelectedValue.ToUpper & "" & porawmstoid.Text & "_" & (iAppOid + C1) & "', '" & Session("UserID") & "', '" & porawdate.Text & "', 'New', 'QL_trnpo" & DDLTypePO.SelectedValue.ToLower & "mst', " & porawmstoid.Text & ", 'In Approval', '" & dt.Rows(C1)("apppersonlevel").ToString & "', '" & dt.Rows(C1)("approvaluser").ToString & "', '1/1/1900', '', '" & IIf(tempAppCode = dt.Rows(C1)("apppersonlevel").ToString, "1", "") & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                    sSql = "UPDATE QL_mstOid SET lastoid=" & iAppOid + dt.Rows.Count - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_APPROVAL'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    porawmststatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 1)
                conn.Close()
                porawmststatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & porawmstoid.Text & ".<BR>"
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trnPurchaseOrder.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnPurchaseOrder.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If porawmstoid.Text.Trim = "" Then
            showMessage("Please select PO Material data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_trnpomst", "pomstoid", porawmstoid.Text, "pomststatus", updtime.Text, "Approved")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                porawmststatus.Text = "In Process"
                Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE FROM QL_trnpodtl WHERE cmpcode='" & CompnyCode & "' AND pomstoid=" & porawmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnpomst WHERE cmpcode='" & CompnyCode & "' AND pomstoid=" & porawmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnPurchaseOrder.aspx?awal=true")
    End Sub

    Protected Sub btnSendApproval_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSendApproval.Click
        Dim sTableName As String = "QL_trnpofgmst"
        If DDLTypePO.SelectedItem.ToString.ToUpper = "RAW MATERIAL" Then
            sTableName = "QL_trnporawmst"
        End If
        sSql = "SELECT approvaluser, apppersontype, case when upper(apppersontype)='FINAL' then 0 else apppersonlevel end apppersonlevel FROM QL_approvalperson WHERE tablename='" & sTableName & "' AND activeflag='ACTIVE' AND cmpcode IN ('" & CompnyCode & "', '" & CompnyCode & "') order by apppersontype desc,apppersonlevel asc"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_approvalperson")
        If dt.Rows.Count = 0 Then
            showMessage("There is no user can't approve this transaction. Please define approval user in <STRONG>APPROVAL USER</STRONG> form.", 2)
        Else
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "apppersontype='Final'"
            If dv.Count = 0 Then
                showMessage("Final approval user is not defined for this transaction. Please define final approval user in <STRONG>APPROVAL USER</STRONG> form.", 2)
                dv.RowFilter = ""
            Else
                dv.RowFilter = ""
                Session("AppPerson") = dt
                'If Not Session("TblDtl") Is Nothing Then
                '    Dim objTbl As DataTable = Session("TblDtl")
                '    Dim PercentPrice As Double = GetPriceValue()
                '    For C1 As Int16 = 0 To objTbl.Rows.Count - 1
                '        If ToDouble(objTbl.Rows(C1)("lastpoprice").ToString) > 0 Then
                '            If ToDouble(objTbl.Rows(C1)("poprice").ToString) > (ToDouble(objTbl.Rows(C1)("lastpoprice").ToString) + (ToDouble(objTbl.Rows(C1)("lastpoprice").ToString) * PercentPrice)) Or ToDouble(objTbl.Rows(C1)("poprice").ToString) < (ToDouble(objTbl.Rows(C1)("lastpoprice").ToString) - (ToDouble(objTbl.Rows(C1)("lastpoprice").ToString) * PercentPrice)) Then
                '                Session("CheckPrice") = "Exceed"
                '                gvTblDtl.Rows(C1).Cells(5).ForeColor = Color.Red
                '                gvTblDtl.Rows(C1).Cells(6).ForeColor = Color.Red
                '            End If
                '        End If
                '    Next
                '    If Session("CheckPrice") = "Exceed" Then
                '        showConfirmation("There are some detail data have price per unit more or less than " & (PercentPrice * 100).ToString & " % of the last PO price. Are you sure to continue?")
                '        Exit Sub
                '    End If
                'End If
                porawmststatus.Text = "In Approval"
                btnSave_Click(Nothing, Nothing)
            End If
        End If
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        'If Session("TblMst") IsNot Nothing Then
        '    UpdateCheckedValue()
        '    lblRptOid.Text = GetOidDetail()
        '    Dim dt As DataTable = Session("TblMst")
        '    Dim sPOFilter As String = ""
        '    If lblRptOid.Text <> "" Then
        '        sPOFilter = " AND porawmstoid IN (" & lblRptOid.Text & ")"
        '    End If
        '    If dt.Select("supptype='LOCAL'" & sPOFilter).Length > 0 Then
        '        If dt.Select("supptype='IMPORT'" & sPOFilter).Length > 0 Then
        '            'cProc.SetModalPopUpExtender(btnPnlApproval, pnlApproval, mpeApproval, True)
        '        Else
        '            DDLTypeSupp.SelectedValue = "LOCAL"
        '            PrintReport(lblRptOid.Text)
        '        End If
        '    Else
        '        DDLTypeSupp.SelectedValue = "IMPORT"
        '        PrintReport(lblRptOid.Text)
        '    End If
        'Else
        '    'cProc.SetModalPopUpExtender(btnPnlApproval, pnlApproval, mpeApproval, True)
        'End If


        If Session("TblMst") IsNot Nothing Then
            UpdateCheckedValue()
            lblRptOid.Text = GetOidDetail()
            Dim dt As DataTable = Session("TblMst")
            Dim sPOFilter As String = ""
            If lblRptOid.Text <> "" Then
                sPOFilter = " AND pomstoid IN (" & lblRptOid.Text & ")"
            End If
            If dt.Select("supptype='LOCAL'" & sPOFilter).Length > 0 Then
                If dt.Select("supptype='IMPORT'" & sPOFilter).Length > 0 Then
                    PrintReport(lblRptOid.Text)
                Else
                    DDLTypeSupp.SelectedValue = "LOCAL"
                    PrintReport(lblRptOid.Text)
                End If
            Else
                DDLTypeSupp.SelectedValue = "IMPORT"
                PrintReport(lblRptOid.Text)
            End If
        Else
            PrintReport(lblRptOid.Text)
        End If
    End Sub

    Protected Sub btnPrint2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint2.Click
        PrintReport(porawmstoid.Text)
    End Sub

    Protected Sub btnCancelPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelPrint.Click
        cProc.SetModalPopUpExtender(btnPnlApproval, pnlApproval, mpeApproval, False)
    End Sub

    Protected Sub btnContinue_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnContinue.Click
        cProc.SetModalPopUpExtender(btnPnlApproval, pnlApproval, mpeApproval, False)
        PrintReport(lblRptOid.Text)
    End Sub

    Protected Sub btnSaveAs_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSaveAs.Click

    End Sub

    Protected Sub imbYesConfirm_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbYesConfirm.Click
        If Not Session("TblDtl") Is Nothing Then
            If Not Session("CheckPrice") Is Nothing Or Session("CheckPrice") <> "" Then
                porawmststatus.Text = "In Approval"
                cProc.SetModalPopUpExtender(bePopUpConfirm, pnlPopUpConfirm, mpePopUpConfirm, False)
                btnSave_Click(Nothing, Nothing)
            ElseIf Not Session("POQtyMoreThanPRQty") Is Nothing And Session("POQtyMoreThanPRQty") = True Then
                cProc.SetModalPopUpExtender(bePopUpConfirm, pnlPopUpConfirm, mpePopUpConfirm, False)
                btnSave_Click(Nothing, Nothing)
            ElseIf Not Session("POQtyMoreThanPRQty2") Is Nothing And Session("POQtyMoreThanPRQty2") = True Then
                cProc.SetModalPopUpExtender(bePopUpConfirm, pnlPopUpConfirm, mpePopUpConfirm, False)
                btnSaveAs_Click(Nothing, Nothing)
            Else
                Dim dt As DataTable = Session("TblDtl")
                Dim dv As DataView = dt.DefaultView
                Dim sFilter As String = "podtlseq IN "
                If Not Session("RemovePRClosed") Is Nothing And Session("RemovePRClosed") <> "" Then
                    sFilter &= "(" & Session("RemovePRClosed") & ")"
                Else
                    If Not Session("RemovePRCompleted") Is Nothing And Session("RemovePRCompleted") <> "" Then
                        sFilter &= "(" & Session("RemovePRCompleted") & ")"
                    Else
                        sFilter = ""
                    End If
                End If
                dv.RowFilter = sFilter
                dv.AllowDelete = True
                For C1 As Integer = 0 To dv.Count - 1
                    dv.Delete(0)
                Next
                dv.RowFilter = ""
                dt.AcceptChanges()
                Session("TblDtl") = dt
                gvTblDtl.DataSource = Session("TblDtl")
                gvTblDtl.DataBind()
            End If
        End If
        Session("RemovePRClosed") = Nothing : Session("RemovePRCompleted") = Nothing
        cProc.SetModalPopUpExtender(bePopUpConfirm, pnlPopUpConfirm, mpePopUpConfirm, False)
    End Sub

    Protected Sub imbNoConfirm_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbNoConfirm.Click
        porawmststatus.Text = "In Process"
        Session("RemovePRClosed") = Nothing : Session("RemovePRCompleted") = Nothing : Session("CheckPrice") = Nothing : Session("POQtyMoreThanPRQty") = Nothing : Session("POQtyMoreThanPRQty2") = Nothing
        cProc.SetModalPopUpExtender(bePopUpConfirm, pnlPopUpConfirm, mpePopUpConfirm, False)
    End Sub

    Protected Sub DDLinex_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If DDLinex.SelectedValue = "INC" Then
            lblvarprice.Visible = True
            lblvarprice2.Visible = True
            varprice.Visible = True
            varprice.Enabled = True
            varprice.CssClass = "inpText"
            porawprice.Enabled = False
            porawprice.CssClass = "inpTextDisabled"
            porawdtldisctype.Enabled = False
            porawdtldisctype.CssClass = "inpTextDisabled"
            porawdtldiscvalue.Enabled = False
            porawdtldiscvalue.CssClass = "inpTextDisabled"
        Else
            lblvarprice.Visible = False
            lblvarprice2.Visible = False
            varprice.Visible = False
            porawprice.Enabled = True
            porawprice.CssClass = "inpText"
            porawdtldisctype.Enabled = True
            porawdtldisctype.CssClass = "inpText"
            porawdtldiscvalue.Enabled = True
            porawdtldiscvalue.CssClass = "inpText"
        End If
        If Session("TblDtl") IsNot Nothing Then
            gvTblDtl.DataSource = Nothing
            gvTblDtl.DataBind()
        End If
        'RegenerateDtl()
    End Sub

    Protected Sub varprice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If DDLinex.Text = "INC" Then
            porawprice.Text = ToMaskEdit(ToDouble(varprice.Text / GetNewTaxValueInclude(porawdate.Text)), iRoundDigit)
        End If
        CountAllDtlAmount()
    End Sub

    Protected Sub gvListSupp_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvListSupp.Sorting
        Dim dt = TryCast(Session("mstSupp"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvListSupp.DataSource = Session("mstSupp")
            gvListSupp.DataBind()
        End If
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
    End Sub

    Protected Sub gvListPR_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvListPR.Sorting
        Dim dt = TryCast(Session("listPR"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvListPR.DataSource = Session("listPR")
            gvListPR.DataBind()
        End If
        mpeListPR.Show()
    End Sub

    Protected Sub gvListMat_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvListMat.Sorting
        UpdateCheckedMat()
        Dim dt = TryCast(Session("TblMat"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvListMat.DataSource = Session("TblMat")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub tbHD_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Text = ToMaskEdit(ToDouble(tbHD.Text), 2)
        CountHdrAmount()
    End Sub

    Protected Sub ddlHD_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CountHdrAmount()
    End Sub

#End Region

End Class