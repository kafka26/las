Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_ProductionResultCancellation
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitDDL()
        ' Init DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)
    End Sub

    Private Sub ClearData()
        prodresmstoid.Text = "" : prodresno.Text = "" : prodresdate.Text = "" : deptname.Text = "" : suppname.Text = "" : prodresmststatus.Text = "" : posttime.Text = "" : deptoid.Text = ""
        Session("TblDtl") = Nothing
        gvDtl.DataSource = Nothing
        gvDtl.DataBind()
    End Sub

    Private Sub BindListResult()
        sSql = "SELECT DISTINCT resm.prodresmstoid, prodresno, CONVERT(VARCHAR(10), prodresdate, 101) AS prodresdate, deptname, ISNULL(suppname, 'None') AS suppname, prodresmststatus, resm.updtime, (CASE WHEN ISNULL(deptreplaceoid, 0)<>0 THEN deptreplaceoid ELSE resm.deptoid END) AS deptoid FROM QL_trnprodresmst resm INNER JOIN QL_trnprodresdtl resd ON resd.cmpcode=resm.cmpcode AND resd.prodresmstoid=resm.prodresmstoid INNER JOIN QL_mstdept de ON de.cmpcode=resm.cmpcode AND de.deptoid=resm.deptoid LEFT JOIN QL_mstsupp s ON s.suppoid=resm.suppoid WHERE resm.cmpcode='" & DDLBusUnit.SelectedValue & "' AND prodresmststatus='Post' AND " & FilterDDLListResult.SelectedValue & " LIKE '%" & Tchar(FilterTextListResult.Text) & "%' AND resm.prodresmstoid NOT IN (SELECT apd.prodresmstoid FROM QL_trnapproductdtl apd INNER JOIN QL_trnapproductmst apm ON apm.cmpcode=apd.cmpcode AND apm.approductmstoid=apd.approductmstoid WHERE apd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND approductmststatus<>'Rejected') AND resm.prodresmstoid NOT IN (SELECT resc.prodresmstoid FROM QL_trnprodresconfirm resc WHERE resc.cmpcode='" & DDLBusUnit.SelectedValue & "') AND resm.prodresmstoid NOT IN (SELECT resmx.prodresmstoid FROM QL_trnprodresmst resmx INNER JOIN QL_trnprodresdtl resdx ON resdx.cmpcode=resmx.cmpcode AND resdx.prodresmstoid=resmx.prodresmstoid WHERE resmx.cmpcode='" & DDLBusUnit.SelectedValue & "' AND resmx.deptoid=resd.todeptoid AND resdx.womstoid=resd.womstoid AND resmx.prodresmststatus<>'Cancel') AND resm.prodresmstoid NOT IN (SELECT resdx.prodresmstoid FROM QL_trnprodresdtl resdx INNER JOIN QL_trnwomst wom ON wom.cmpcode=resdx.cmpcode AND wom.womstoid=resdx.womstoid WHERE resdx.cmpcode='" & DDLBusUnit.SelectedValue & "' AND womststatus='Closed') ORDER BY resm.prodresdate DESC, resm.prodresmstoid DESC"
        FillGV(gvListResult, sSql, "QL_trnprodresmst")
    End Sub

    Private Sub ViewDetailResult(ByVal iOid As Integer)
        sSql = "SELECT prodresdtlseq, wono, CONVERT(VARCHAR(10), wodate, 101) AS wodate, (CASE prodrestype WHEN 'WIP' THEN deptname ELSE 'END' END) AS todeptname, prodresqty, gendesc AS prodresunit, prodresdtlnote, 0.0 AS prodresdlcamt, 0.0 AS prodresohdamt, 0.0 AS prodresdlcamtusd, 0.0 AS prodresohdamtusd FROM QL_trnprodresdtl resd INNER JOIN QL_trnwomst wom ON wom.cmpcode=resd.cmpcode AND wom.womstoid=resd.womstoid INNER JOIN QL_mstdept de ON de.cmpcode=resd.cmpcode AND de.deptoid=resd.todeptoid INNER JOIN QL_mstgen g ON genoid=prodresunitoid WHERE prodresmstoid=" & prodresmstoid.Text & " ORDER BY prodresdtlseq"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnprodresdtl")
        Session("TblDtl") = dt
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnProdResCancel.aspx")
        End If
        If checkPagePermission("~\Transaction\trnProdResCancel.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Production Result Cancellation"
        btnProcess.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to CANCEL this data?');")
        If Not Page.IsPostBack Then
            upduser.Text = Session("UserID") : updtime.Text = GetServerTime().ToString
            InitDDL()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("Success") Is Nothing And Session("Success") <> "" Then
            Response.Redirect("~\Transaction\trnProdResCancel.aspx?awal=true")
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        ClearData()
    End Sub

    Protected Sub btnFindResult_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindResult.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        FilterTextListResult.Text = "" : FilterDDLListResult.SelectedIndex = -1 : gvListResult.SelectedIndex = -1
        BindListResult()
        cProc.SetModalPopUpExtender(btnHideListResult, pnlListResult, mpeListResult, True)
    End Sub

    Protected Sub btnClearResult_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearResult.Click
        ClearData()
    End Sub

    Protected Sub btnFindListResult_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListResult.Click
        BindListResult()
        mpeListResult.Show()
    End Sub

    Protected Sub btnAllListResult_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListResult.Click
        FilterTextListResult.Text = "" : FilterDDLListResult.SelectedIndex = -1 : gvListResult.SelectedIndex = -1
        BindListResult()
        mpeListResult.Show()
    End Sub

    Protected Sub gvListResult_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListResult.PageIndexChanging
        gvListResult.PageIndex = e.NewPageIndex
        BindListResult()
        mpeListResult.Show()
    End Sub

    Protected Sub gvListResult_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListResult.SelectedIndexChanged
        prodresmstoid.Text = gvListResult.SelectedDataKey.Item("prodresmstoid").ToString
        prodresno.Text = gvListResult.SelectedDataKey.Item("prodresno").ToString
        prodresdate.Text = gvListResult.SelectedDataKey.Item("prodresdate").ToString
        deptname.Text = gvListResult.SelectedDataKey.Item("deptname").ToString
        suppname.Text = gvListResult.SelectedDataKey.Item("suppname").ToString
        prodresmststatus.Text = gvListResult.SelectedDataKey.Item("prodresmststatus").ToString
        posttime.Text = gvListResult.SelectedDataKey.Item("updtime").ToString
        deptoid.Text = gvListResult.SelectedDataKey.Item("deptoid").ToString
        cProc.SetModalPopUpExtender(btnHideListResult, pnlListResult, mpeListResult, False)
        ViewDetailResult(CInt(prodresmstoid.Text))
    End Sub

    Protected Sub lkbCloseListResult_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListResult.Click
        cProc.SetModalPopUpExtender(btnHideListResult, pnlListResult, mpeListResult, False)
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnProdResCancel.aspx?awal=true")
    End Sub

    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnProcess.Click
        If prodresmstoid.Text = "" Then
            showMessage("Please select Production Result Data first!", 2)
            Exit Sub
        End If
        If Session("TblDtl") Is Nothing Then
            showMessage("Please select Production Result Data first!", 2)
            Exit Sub
        End If
        Dim objTbl As DataTable = Session("TblDtl")
        If objTbl.Rows.Count <= 0 Then
            showMessage("Please select Production Result Data first!", 2)
            Exit Sub
        End If
        Dim iGLMstOid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
        Dim iGLDtlOid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
        Dim iStockWIPAcctgOid As Integer = GetAcctgOID(GetVarInterface("VAR_STOCK_WIP", DDLBusUnit.SelectedValue), CompnyCode)
        Dim iDLAssignAcctgOid As Integer = GetAcctgOID(GetVarInterface("VAR_DL_ASSIGNMENT", DDLBusUnit.SelectedValue), CompnyCode)
        Dim iBOPBebanAcctgOid As Integer = GetAcctgOID(GetVarInterface("VAR_BOP_BEBAN", DDLBusUnit.SelectedValue), CompnyCode)
        Dim iBOPRealAcctgOid As Integer = GetAcctgOID(GetVarInterface("VAR_BOP_REAL", DDLBusUnit.SelectedValue), CompnyCode)
        Dim dTotalDL As Double = ToDouble(objTbl.Compute("SUM(prodresdlcamt)", "").ToString)
        Dim dTotalDLIDR As Double = ToDouble(objTbl.Compute("SUM(prodresdlcamt)", "").ToString)
        Dim dTotalDLUSD As Double = ToDouble(objTbl.Compute("SUM(prodresdlcamtusd)", "").ToString)
        Dim dTotalOHD As Double = ToDouble(objTbl.Compute("SUM(prodresohdamt)", "").ToString)
        Dim dTotalOHDIDR As Double = ToDouble(objTbl.Compute("SUM(prodresohdamt)", "").ToString)
        Dim dTotalOHDUSD As Double = ToDouble(objTbl.Compute("SUM(prodresohdamtusd)", "").ToString)
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
        Dim iGroupOid As Integer = ToInteger(GetStrData("SELECT TOP 1 groupoid FROM QL_mstdeptgroupdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND deptoid=" & deptoid.Text & " ORDER BY updtime DESC"))
        Dim cRate As New ClassRate
        cRate.SetRateValue(1, Format(CDate(posttime.Text), "MM/dd/yyyy"))
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trnwodtl2 SET wodtl2resflag='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl2oid IN (SELECT wodtl2oid FROM QL_trnprodresdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND prodresmstoid=" & prodresmstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_trnprodresdtl SET prodresdtlstatus='Cancel' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND prodresmstoid=" & prodresmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_trnprodresmst SET prodresmststatus='Cancel', closeuser='" & Session("UserID") & "', closetime=CURRENT_TIMESTAMP, closereason='" & Tchar(prodresmstnote.Text) & "' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND prodresmstoid=" & prodresmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If dTotalDL > 0 Or dTotalDLIDR > 0 Or dTotalDLUSD > 0 Then
                ' Insert QL_trnglmst
                sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGLMstOid & ", '" & sDate & "', '" & sPeriod & "', 'Cancel ProdRes|No. " & prodresno.Text & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                ' Insert QL_trngldtl
                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, groupoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGLDtlOid & ", 1, " & iGLMstOid & ", " & iDLAssignAcctgOid & ", 'D', " & dTotalDL & ", '" & prodresno.Text & "', 'Cancel ProdRes|No. " & prodresno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalDLIDR & ", " & dTotalDLUSD & ", 'QL_trnprodresmst " & prodresmstoid.Text & "', " & iGroupOid & ")"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                iGLDtlOid += 1
                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, groupoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGLDtlOid & ", 2, " & iGLMstOid & ", " & iStockWIPAcctgOid & ", 'C', " & dTotalDL & ", '" & prodresno.Text & "', 'Cancel ProdRes|No. " & prodresno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalDLIDR & ", " & dTotalDLUSD & ", 'QL_trnprodresmst " & prodresmstoid.Text & "', " & iGroupOid & ")"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                iGLDtlOid += 1
                iGLMstOid += 1
            End If
            If dTotalOHD > 0 Or dTotalOHDIDR > 0 Or dTotalOHDUSD > 0 Then
                ' Insert QL_trnglmst
                sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGLMstOid & ", '" & sDate & "', '" & sPeriod & "', 'Cancel ProdRes|No. " & prodresno.Text & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                ' Insert QL_trngldtl
                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, groupoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGLDtlOid & ", 1, " & iGLMstOid & ", " & iBOPBebanAcctgOid & ", 'D', " & dTotalOHD & ", '" & prodresno.Text & "', 'Cancel ProdRes|No. " & prodresno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalOHDIDR & ", " & dTotalOHDUSD & ", 'QL_trnprodresmst " & prodresmstoid.Text & "', " & iGroupOid & ")"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                iGLDtlOid += 1
                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, groupoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGLDtlOid & ", 2, " & iGLMstOid & ", " & iStockWIPAcctgOid & ", 'C', " & dTotalOHD & ", '" & prodresno.Text & "', 'Cancel ProdRes|No. " & prodresno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalOHDIDR & ", " & dTotalOHDUSD & ", 'QL_trnprodresmst " & prodresmstoid.Text & "', " & iGroupOid & ")"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                iGLDtlOid += 1
                iGLMstOid += 1
                ' Insert QL_trnglmst
                sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGLMstOid & ", '" & sDate & "', '" & sPeriod & "', 'Cancel ProdRes|No. " & prodresno.Text & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                ' Insert QL_trngldtl
                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, groupoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGLDtlOid & ", 1, " & iGLMstOid & ", " & iBOPRealAcctgOid & ", 'D', " & dTotalOHD & ", '" & prodresno.Text & "', 'Cancel ProdRes|No. " & prodresno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalOHDIDR & ", " & dTotalOHDUSD & ", 'QL_trnprodresmst " & prodresmstoid.Text & "', " & iGroupOid & ")"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                iGLDtlOid += 1
                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, groupoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGLDtlOid & ", 2, " & iGLMstOid & ", " & iBOPBebanAcctgOid & ", 'C', " & dTotalOHD & ", '" & prodresno.Text & "', 'Cancel ProdRes|No. " & prodresno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalOHDIDR & ", " & dTotalOHDUSD & ", 'QL_trnprodresmst " & prodresmstoid.Text & "', " & iGroupOid & ")"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                iGLDtlOid += 1
                iGLMstOid += 1
            End If
            sSql = "UPDATE QL_mstoid SET lastoid=" & iGLMstOid - 1 & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_mstoid SET lastoid=" & iGLDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch exSql As SqlException
            objTrans.Rollback() : conn.Close()
            If ToDouble(Session("ErrorCounter")) < 5 Then
                If exSql.Number = 2627 Then
                    Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                    btnProcess_Click(sender, e)
                    Exit Sub
                Else
                    showMessage(exSql.Message, 1)
                    Exit Sub
                End If
            Else
                showMessage(exSql.Message, 1)
                Exit Sub
            End If
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Session("Success") = "Result No. : " & prodresno.Text & " have been cancel successfully."
        showMessage(Session("Success"), 3)
    End Sub
#End Region

End Class