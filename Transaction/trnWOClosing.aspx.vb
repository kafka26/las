Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_JobCostingMOClosing
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If womstoid.Text = "" Then
            sError &= "- Please select KIK NO. field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitAllDDL()
        'Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND divcode='" & Session("CompnyCode") & "'"
        End If
        If FillDDL(DDLBusUnit, sSql) Then
            InitDDLAppUser()
        End If
    End Sub

    Private Sub InitDDLAppUser()
        sSql = "SELECT approvaluser, approvaluser FROM QL_approvalperson WHERE tablename='QL_trnwoclose' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" & DDLBusUnit.SelectedValue & "', '" & CompnyCode & "') ORDER BY approvaluser"
        FillDDLWithAdditionalText(approvaluser, sSql, "-", "-")
        approvaluser.SelectedValue = SetDefaultAppUser("QL_trnwoclose", DDLBusUnit.SelectedValue, Session("UserID"), approvaluser)
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT wocloseoid, wocloseno, wono, requestuser, requestdatetime, (CASE woclosestatus WHEN 'Approved' THEN approvaluser ELSE approvalcode END) approvaluser, approvaldatetime, woclosestatus, woclosenote, divname FROM QL_trnwoclose woc INNER JOIN QL_trnwomst wom ON wom.cmpcode=woc.cmpcode AND wom.womstoid=woc.womstoid INNER JOIN QL_mstdivision div ON div.cmpcode=woc.cmpcode WHERE"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " woc.cmpcode='" & Session("CompnyCode") & "'"
        Else
            sSql &= " woc.cmpcode LIKE '%'"
        End If
        sSql &= sSqlPlus & " ORDER BY wocloseoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trnwoclose")
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub BindListKIK()
        sSql = "SELECT womstoid, wono, CONVERT(VARCHAR(10), wodate, 101) AS wodate, womstnote FROM QL_trnwomst wom WHERE wom.cmpcode='" & DDLBusUnit.SelectedValue & "' AND womststatus='Post' AND woforceclose='' AND " & FilterDDLListKIK.SelectedValue & " LIKE '%" & Tchar(FilterTextListKIK.Text) & "%' ORDER BY wono"
        FillGV(gvListKIK, sSql, "QL_trnwomst")
    End Sub

    Private Sub FillTextBox()
        Try
            sSql = "SELECT woc.*, wono FROM QL_trnwoclose woc INNER JOIN QL_trnwomst wom ON wom.cmpcode=woc.cmpcode AND wom.womstoid=woc.womstoid WHERE wocloseoid=" & Session("oid")
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = xreader("cmpcode").ToString
                DDLBusUnit_SelectedIndexChanged(Nothing, Nothing)
                wocloseoid.Text = xreader("wocloseoid").ToString
                wocloseno.Text = xreader("wocloseno").ToString
                womstoid.Text = xreader("womstoid").ToString
                wono.Text = xreader("wono").ToString
                requestuser.Text = xreader("requestuser").ToString
                requestdatetime.Text = xreader("requestdatetime").ToString
                woclosestatus.Text = xreader("woclosestatus").ToString
                woclosenote.Text = xreader("woclosenote").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
                If woclosestatus.Text = "Approved" Then
                    approvaluser.SelectedValue = xreader("approvaluser").ToString
                    approvaldatetime.Text = xreader("approvaldatetime").ToString
                Else
                    approvaluser.SelectedValue = xreader("approvalcode").ToString
                    approvaldatetime.Text = "-"
                End If
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
        If woclosestatus.Text <> "In Process" And woclosestatus.Text <> "Revised" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnSendApproval.Visible = False
            If woclosestatus.Text = "Approved" Then
                lbNo.Text = "KIK Close No."
                wocloseoid.Visible = False
                wocloseno.Visible = True
            End If
        Else
            requestuser.Text = Session("UserID")
            requestdatetime.Text = GetServerTime()
        End If
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnWOClosing.aspx")
        End If
        If checkPagePermission("~\Transaction\trnWOClosing.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Job Costing MO Closing"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            I_U.Text = "New Data"
        Else
            I_U.Text = "View Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnSendApproval.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to SEND this data for APPROVAL?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime()
            requestuser.Text = Session("UserID") : requestdatetime.Text = GetServerTime()
            approvaldatetime.Text = "-"
            upduser.Text = "-" : updtime.Text = "-"
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitAllDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox()
                TabContainer1.ActiveTabIndex = 1
            Else
                wocloseoid.Text = GenerateID("QL_TRNWOCLOSE", CompnyCode)
                woclosestatus.Text = "In Process"
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnWOClosing.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriod.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND " & FilterDDLPeriod.SelectedValue & ">=CAST('" & FilterPeriod1.Text & " 00:00:00' AS DATETIME) AND " & FilterDDLPeriod.SelectedValue & "<=CAST('" & FilterPeriod2.Text & " 23:59:59' AS DATETIME)"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND woclosestatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Transaction\trnWOClosing.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND requestuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriod.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        FilterDDLPeriod.SelectedIndex = -1
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Transaction\trnWOClosing.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND requestuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
    End Sub

    Protected Sub gvMst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvMst.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If e.Row.Cells(6).Text = "1/1/1900 12:00:00 AM" Then
                e.Row.Cells(6).Text = "-"
            End If
        End If
    End Sub

    Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvMst.DataSource = Session("TblMst")
            gvMst.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLAppUser()
        btnClearKIK_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnSearchKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchKIK.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        FilterDDLListKIK.SelectedIndex = -1 : FilterTextListKIK.Text = "" : gvListKIK.SelectedIndex = -1
        BindListKIK()
        cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, True)
    End Sub

    Protected Sub btnClearKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearKIK.Click
        womstoid.Text = "" : wono.Text = ""
    End Sub

    Protected Sub btnFindListKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListKIK.Click
        BindListKIK()
        mpeListKIK.Show()
    End Sub

    Protected Sub btnAllListKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListKIK.Click
        FilterDDLListKIK.SelectedIndex = -1 : FilterTextListKIK.Text = "" : gvListKIK.SelectedIndex = -1
        BindListKIK()
        mpeListKIK.Show()
    End Sub

    Protected Sub gvListKIK_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListKIK.PageIndexChanging
        gvListKIK.PageIndex = e.NewPageIndex
        BindListKIK()
        mpeListKIK.Show()
    End Sub

    Protected Sub gvListKIK_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListKIK.SelectedIndexChanged
        If womstoid.Text <> gvListKIK.SelectedDataKey.Item("womstoid").ToString Then
            btnClearKIK_Click(Nothing, Nothing)
        End If
        womstoid.Text = gvListKIK.SelectedDataKey.Item("womstoid").ToString
        wono.Text = gvListKIK.SelectedDataKey.Item("wono").ToString
        cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, False)
    End Sub

    Protected Sub lkbCloseListKIK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListKIK.Click
        cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, False)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnWOClosing.aspx?awal=true")
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                If CheckDataExists("SELECT COUNT(*) FROM QL_trnwoclose WHERE wocloseoid=" & wocloseoid.Text) Then
                    wocloseoid.Text = GenerateID("QL_TRNWOCLOSE", CompnyCode)
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNWOCLOSE", "wocloseoid", wocloseoid.Text, "woclosestatus", updtime.Text, "Approved")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    woclosestatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            Dim iAppOid As Integer = GenerateID("QL_APPROVAL", CompnyCode)
            If woclosestatus.Text = "Revised" Then
                woclosestatus.Text = "In Process"
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trnwoclose (cmpcode, wocloseoid, wocloseno, womstoid, woclosenote, woclosestatus, requestuser, requestdatetime, createuser, createtime, upduser, updtime, approvalcode) VALUES ('" & DDLBusUnit.SelectedValue & "', " & wocloseoid.Text & ", '" & wocloseno.Text & "', " & womstoid.Text & ", '" & Tchar(woclosenote.Text) & "', '" & woclosestatus.Text & "', '" & requestuser.Text & "', CURRENT_TIMESTAMP, '" & createuser.Text & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & approvaluser.SelectedValue & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & wocloseoid.Text & " WHERE tablename='QL_TRNWOCLOSE' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trnwoclose SET wocloseno='" & wocloseno.Text & "', womstoid=" & womstoid.Text & ", woclosenote='" & Tchar(woclosenote.Text) & "', woclosestatus='" & woclosestatus.Text & "', requestuser='" & requestuser.Text & "', requestdatetime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, approvalcode='" & approvaluser.SelectedValue & "' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wocloseoid=" & wocloseoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                sSql = "UPDATE QL_trnwomst SET woforceclose='Closed' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid=" & womstoid.Text
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                If woclosestatus.Text = "In Approval" Then
                    Dim dt As DataTable = Session("AppPerson")
                    For C1 As Integer = 0 To dt.Rows.Count - 1
                        sSql = "INSERT INTO QL_approval (cmpcode, approvaloid, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvalnote) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iAppOid + C1 & ", 'WOCLOSE" & wocloseoid.Text & "_" & (iAppOid + C1) & "', '" & Session("UserID") & "', '" & requestdatetime.Text & "', 'New', 'QL_trnwoclose', " & wocloseoid.Text & ", 'In Approval', '0', '" & dt.Rows(C1)("approvaluser").ToString & "', '1/1/1900', '')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                    sSql = "UPDATE QL_mstOid SET lastoid=" & iAppOid + dt.Rows.Count - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_APPROVAL'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        woclosestatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    woclosestatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                conn.Close()
                showMessage(ex.Message, 1)
                woclosestatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & wocloseoid.Text & ".<BR>"
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trnWOClosing.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If wocloseoid.Text.Trim = "" Then
            showMessage("Please select Job Costing MO Closing data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNWOCLOSE", "wocloseoid", wocloseoid.Text, "woclosestatus", updtime.Text, "Approved")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                woclosestatus.Text = "In Process"
                Exit Sub
            End If
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trnwomst SET woforceclose='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid=" & womstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnwoclose WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wocloseoid=" & wocloseoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnWOClosing.aspx?awal=true")
    End Sub

    Protected Sub btnSendApproval_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSendApproval.Click
        If approvaluser.SelectedValue = "-" Or approvaluser.SelectedValue = "" Then
            showMessage("Please select Approval User first!", 2) : Exit Sub
        End If
        sSql = "SELECT approvaluser, apppersontype FROM QL_approvalperson WHERE tablename='QL_trnwoclose' AND activeflag='ACTIVE' AND cmpcode IN ('" & CompnyCode & "', '" & DDLBusUnit.SelectedValue & "') AND (issuperuser='True' OR approvaluser='" & approvaluser.SelectedValue & "')"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_approvalperson")
        If dt.Rows.Count = 0 Then
            showMessage("There is no user can't approve this transaction. Please define approval user in <STRONG>APPROVAL USER</STRONG> form.", 2)
        Else
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "apppersontype='Final'"
            If dv.Count = 0 Then
                showMessage("Final approval user is not defined for this transaction. Please define final approval user in <STRONG>APPROVAL USER</STRONG> form.", 2)
                dv.RowFilter = ""
            Else
                dv.RowFilter = ""
                Session("AppPerson") = dt
                woclosestatus.Text = "In Approval"
                btnSave_Click(Nothing, Nothing)
            End If
        End If
    End Sub
#End Region

End Class
