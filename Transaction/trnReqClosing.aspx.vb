Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class Transaction_MaterialRequestClosing
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitDDL()
        ' Init DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)
    End Sub

    Private Sub BindListReq()
        Dim sType As String = ""
        sSql = "SELECT * FROM (SELECT req" & sType & "mstoid AS reqmstoid, req" & sType & "no AS reqno, CONVERT(VARCHAR(10), req" & sType & "date, 101) AS reqdate, " & IIf(DDLType.SelectedIndex <= 3, "ISNULL((SELECT wono FROM QL_trnwomst wom WHERE wom.womstoid=req.womstoid), '')", "ISNULL((SELECT soitemno FROM QL_trnsoitemmst som WHERE som.soitemmstoid=req.soitemmstoid), '')") & " AS wono, ISNULL((SELECT deptname FROM QL_mstdept de WHERE de.deptoid=req.deptoid), '') AS deptname, req" & sType & "mstnote AS reqmstnote, reqmstres3 FROM QL_trnreq" & sType & "mst req WHERE req.cmpcode='" & DDLBusUnit.SelectedValue & "' AND req" & sType & "mststatus='Post' AND req" & sType & "mstoid IN (SELECT req" & sType & "mstoid FROM QL_trnusage" & sType & "dtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "')) AS tbl_request WHERE " & FilterDDLListReq.SelectedValue & " LIKE '%" & Tchar(FilterTextListReq.Text) & "%' ORDER BY CONVERT(DATETIME, reqdate) DESC, reqmstoid DESC"
        FillGV(gvListReq, sSql, "tbl_request")
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnReqClosing.aspx")
        End If
        If checkPagePermission("~\Transaction\trnReqClosing.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Material Request Closing"
        btnClose.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to CLOSE this data?');")
        If Not Page.IsPostBack Then
            closeuser.Text = Session("UserID") : closetime.Text = GetServerTime().ToString
            InitDDL()
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvReqDtl.DataSource = dt
            gvReqDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("Success") Is Nothing And Session("Success") <> "" Then
            Response.Redirect("~\Transaction\trnReqClosing.aspx?awal=true")
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        DDLType_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub DDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLType.SelectedIndexChanged
        btnClearReq_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnSearchReq_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchReq.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2) : Exit Sub
        End If
        FilterDDLListReq.SelectedIndex = -1 : FilterTextListReq.Text = "" : gvListReq.SelectedIndex = -1
        If DDLType.SelectedIndex < 3 Then
            lblListReq.Text = "List Of " & DDLType.SelectedItem.Text & " Request"
            gvListReq.Columns(3).HeaderText = "KIK No."
            FilterDDLListReq.Items(1).Text = "KIK No."
        ElseIf DDLType.SelectedIndex = 3 Then
            lblListReq.Text = "List Of Material Request (Kayu)"
            gvListReq.Columns(3).HeaderText = "KIK No."
            FilterDDLListReq.Items(1).Text = "KIK No."
        ElseIf DDLType.SelectedIndex = 4 Then
            lblListReq.Text = "List Of Material Request By SO"
            gvListReq.Columns(3).HeaderText = "SO No."
            FilterDDLListReq.Items(1).Text = "SO No."
        ElseIf DDLType.SelectedIndex = 5 Then
            lblListReq.Text = "List Of Material Request By SO (Kayu)"
            gvListReq.Columns(3).HeaderText = "SO No."
            FilterDDLListReq.Items(1).Text = "SO No."
        End If
        BindListReq()
        cProc.SetModalPopUpExtender(btnHideListReq, pnlListReq, mpeListReq, True)
    End Sub

    Protected Sub btnClearReq_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearReq.Click
        reqmstoid.Text = "" : reqno.Text = "" : Session("TblDtl") = Nothing : gvReqDtl.DataSource = Session("TblDtl") : gvReqDtl.DataBind()
    End Sub

    Protected Sub btnFindListReq_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListReq.Click
        BindListReq()
        mpeListReq.Show()
    End Sub

    Protected Sub btnAllListReq_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListReq.Click
        FilterDDLListReq.SelectedIndex = -1 : FilterTextListReq.Text = "" : gvListReq.SelectedIndex = -1
        BindListReq()
        mpeListReq.Show()
    End Sub

    Protected Sub gvListReq_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListReq.PageIndexChanging
        gvListReq.PageIndex = e.NewPageIndex
        BindListReq()
        mpeListReq.Show()
    End Sub

    Protected Sub gvListReq_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListReq.SelectedIndexChanged
        reqmstoid.Text = gvListReq.SelectedDataKey.Item("reqmstoid").ToString
        reqno.Text = gvListReq.SelectedDataKey.Item("reqno").ToString
        ' Generate Detail Data
        Dim sMatCode As String = "itemcode", sMatLongDesc As String = "itemlongdescription", sType As String = "", sJoin As String = "INNER JOIN QL_mstitem m ON m.itemoid=req.matoid"
       
        sSql = "SELECT req" & sType & "dtloid AS reqdtloid, req" & sType & "dtlseq AS reqdtlseq, " & sMatCode & " AS matcode, " & sMatLongDesc & " AS matlongdesc, req" & sType & "qty AS reqqty, ISNULL((SELECT SUM(usage" & sType & "qty) FROM QL_trnusage" & sType & "dtl usg WHERE usg.cmpcode=req.cmpcode AND usg.req" & sType & "dtloid=req.req" & sType & "dtloid), 0.0) AS usageqty, 0.0 AS closeqty, gendesc AS requnit, req" & sType & "dtlnote AS reqdtlnote, " & IIf(DDLType.SelectedIndex <= 3, "req.refoid", "0") & " refoid FROM QL_trnreq" & sType & "dtl req " & sJoin & " INNER JOIN QL_mstgen g ON genoid=req" & sType & "unitoid WHERE req.cmpcode='" & DDLBusUnit.SelectedValue & "' AND req" & sType & "mstoid=" & reqmstoid.Text & " AND req" & sType & "dtlstatus='' ORDER BY reqdtlseq"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "tbl_requestdetail")
        dt.DefaultView.RowFilter = "(reqqty - usageqty) > 0"
        For C1 As Integer = 0 To dt.DefaultView.Count - 1
            dt.DefaultView(C1)("reqdtlseq") = C1 + 1
            dt.DefaultView(C1)("closeqty") = ToDouble(dt.DefaultView(C1)("reqqty").ToString) - ToDouble(dt.DefaultView(C1)("usageqty").ToString)
        Next
        Session("TblDtl") = dt.DefaultView.ToTable() : gvReqDtl.DataSource = Session("TblDtl") : gvReqDtl.DataBind()
        cProc.SetModalPopUpExtender(btnHideListReq, pnlListReq, mpeListReq, False)
    End Sub

    Protected Sub lbCloseListReq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListReq.Click
        cProc.SetModalPopUpExtender(btnHideListReq, pnlListReq, mpeListReq, False)
    End Sub

    Protected Sub gvReqDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvReqDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnReqClosing.aspx?awal=true")
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
        If reqmstoid.Text = "" Then
            showMessage("Please select Request No. first!", 2) : Exit Sub
        End If
        If closereason.Text = "" Then
            showMessage("Please fill Closing Reason first!", 2) : Exit Sub
        End If
        Dim sType As String = ""
     
        Dim dtTbl As DataTable = Session("TblDtl")
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                sSql = "UPDATE QL_trnreq" & sType & "dtl SET req" & sType & "dtlstatus='Complete', closeqty=" & ToDouble(dtTbl.Rows(C1)("closeqty").ToString) & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND req" & sType & "dtloid=" & dtTbl.Rows(C1)("reqdtloid")
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                If ToDouble(dtTbl.Rows(C1)("closeqty").ToString) > 0 Then
                    sSql = "UPDATE QL_trnwodtl3 SET wodtl3reqflag='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl3oid=" & dtTbl.Rows(C1)("refoid")
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            Next
            If DDLType.SelectedIndex <= 3 Then
                sSql = "UPDATE QL_trnwodtl2 SET wodtl2reqflag='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl2oid IN (SELECT wodtl2oid FROM QL_trnreq" & sType & "mst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND req" & sType & "mstoid=" & reqmstoid.Text & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If
            sSql = "UPDATE QL_trnreq" & sType & "mst SET req" & sType & "mststatus='Closed', closereason='" & Tchar(closereason.Text) & "', closeuser='" & closeuser.Text & "', closetime='" & closetime.Text & "' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND req" & sType & "mstoid=" & reqmstoid.Text
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Session("Success") = "Request No. : " & reqno.Text & " have been closed successfully."
        showMessage(Session("Success"), 3)
    End Sub
#End Region

End Class