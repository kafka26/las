Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Structure VarUsageValue
    Public Val_IDR As Double
    Public Val_USD As Double
End Structure

Partial Class Transaction_MaterialUsageNonKIK
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cRate As New ClassRate
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matreqrefoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("matusageqty") = ToDouble(GetTextBoxValue(row.Cells(5).Controls))
                                    dtView(0)("matusagedtlnote") = GetTextBoxValue(row.Cells(7).Controls)
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matreqrefoid=" & cbOid
                                dtView2.RowFilter = "matreqrefoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("matusageqty") = ToDouble(GetTextBoxValue(row.Cells(5).Controls))
                                    dtView(0)("matusagedtlnote") = GetTextBoxValue(row.Cells(7).Controls)
                                    If dtView2.Count > 0 Then
                                        dtView2(0)("matusageqty") = ToDouble(GetTextBoxValue(row.Cells(5).Controls))
                                        dtView2(0)("matusagedtlnote") = GetTextBoxValue(row.Cells(7).Controls)
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If matusagewhoid.SelectedValue = "" Then
            sError &= "- Please select WAREHOUSE field!<BR>"
        End If
        If matreqdtloid.Text = "" Then
            sError &= "- Please select MATERIAL field!<BR>"
        End If
        If matusageqty.Text = "" Then
            sError &= "- Please fill USAGE QTY field!<BR>"
        Else
            If ToDouble(matusageqty.Text) <= 0 Then
                sError &= "- USAGE QTY field must be more than 0!<BR>"
            Else
                If ToDouble(matusageqty.Text) > ToDouble(stockqty.Text) Then
                    sError &= "- USAGE QTY field must be less than STOCK QTY!<BR>"
                End If
            End If
        End If
        If matusageunitoid.SelectedValue = "" Then
            sError &= "- Please select UNIT field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If matusagedate.Text = "" Then
            sErr = "None"
            sError &= "- Please fill USAGE DATE field!<BR>"
        Else
            If Not IsValidDate(matusagedate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- USAGE DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If deptoid.SelectedValue = "" Then
            sError &= "- Please select DEPARTMENT field!<BR>"
        End If
        If ToInteger(matoid_mix.Text) <> 0 Then
            If qty_mix.Text = "" Then
                sError &= "- Please fill QTY MIXING field!<BR>"
            Else
                If ToDouble(qty_mix.Text) <= 0 Then
                    sError &= "- QTY MIXING must be more than 0!<BR>"
                End If
            End If
            If unitoid_mix.SelectedValue = "" Then
                sError &= "- Please select MATERIAL MIXING UNIT field!<BR>"
            End If
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill Detail Data!<BR>"
        Else
            Dim dtTbl As DataTable = Session("TblDtl")
            If dtTbl.Rows.Count <= 0 Then
                sError &= "- Please fill Detail Data!<BR>"
            Else
                If sErr = "" Then
                    'For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    '    If CDate(dtTbl.Rows(C1)("matreqdate").ToString) > CDate(matusagedate.Text) Then
                    '        sError &= "- Every Request Date must be less than Usage Date!<BR>"
                    '        Exit For
                    '    End If
                    'Next
                    For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                        If Not IsStockAvailable(DDLBusUnit.SelectedValue, Format(GetServerTime(), "yyyyMM"), dtTbl.Rows(C1)("matusagerefoid"), dtTbl.Rows(C1)("matusagewhoid"), ToDouble((dtTbl.Compute("SUM(matusageqty_unitkecil)", "matusagerefoid=" & dtTbl.Rows(C1)("matusagerefoid") & " AND matusagereftype='" & dtTbl.Rows(C1)("matusagereftype").ToString & "'")).ToString), dtTbl.Rows(C1)("matusagereftype").ToString.ToUpper) Then
                            sError &= "- Material <B><STRONG>" & dtTbl.Rows(C1).Item("matusagereflongdesc").ToString & "</STRONG></B> must less or equal than stock QTY, stock is <B><STRONG>" & ToDouble(dtTbl.Rows(C1).Item("matusageqty").ToString) & "</STRONG></B> !<BR>"
                            Exit For
                        End If
                    Next
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            matusagemststatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function GetStockValue(ByVal sOid As String, Optional ByVal sType As String = "") As Double
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        Dim s As String = ""
        Try
            If sType = "USD" Then
                s = "stockvalueusd"
            Else
                s = "stockvalueidr"
            End If

            sSql = "SELECT SUM(ISNULL(stockqty, 0) * ISNULL(" & s & ", 0)) / SUM(ISNULL(stockqty, 0)) FROM QL_stockvalue WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND periodacctg IN ('" & sPeriod & "', '" & GetLastPeriod(sPeriod) & "') AND refoid=" & sOid & " AND closeflag=''"
            GetStockValue = ToDouble(GetStrData(sSql))
        Catch ex As Exception
            GetStockValue = 0
        End Try
    End Function

    Private Function GetAmtByGroup(ByVal dtRef As DataTable) As DataTable
        Dim dtReturn As New DataTable()
        dtReturn.Columns.Add("acctgoid", Type.GetType("System.Int32"))
        dtReturn.Columns.Add("amount", Type.GetType("System.Double"))
        sSql = "SELECT DISTINCT genother1 AS acctgoid FROM QL_mstgen WHERE cmpcode='" & Session("CompnyCode") & "' AND gengroup='GROUPITEM' AND activeflag='ACTIVE'"
        Dim dtAcctg As DataTable = cKon.ambiltabel(sSql, "QL_mstacctg")
        For C1 As Integer = 0 To dtAcctg.Rows.Count - 1
            Dim obj As Object
            obj = dtRef.Compute("SUM(matusagevalueidr)", "stockacctgoid=" & dtAcctg.Rows(C1)("acctgoid").ToString)
            If Not IsDBNull(obj) Then
                Dim dr As DataRow = dtReturn.NewRow()
                dr("acctgoid") = CInt(dtAcctg.Rows(C1)("acctgoid").ToString.Trim)
                dr("amount") = ToDouble(obj.ToString)
                dtReturn.Rows.Add(dr)
            End If
        Next
        Return dtReturn
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckUsageStatus()
        Dim nDays As Integer = 7
		sSql = "SELECT COUNT(*) FROM QL_trnmatusagemst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND usagemststatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnMatUsage.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql).ToString) > 0 Then
            lkbUsageInProcess.Visible = True
            lkbUsageInProcess.Text = "You have " & GetStrData(sSql) & " In Process Process Material Usage Non KIK data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitAllDDL()
        ' Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)
        If DDLBusUnit.Items.Count > 0 Then
            InitDDLDept()
        End If
        ' Fill DDL Warehouse
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='WAREHOUSE' AND genoid>0"
        If i_u.Text = "New Data" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
        FillDDL(matusagewhoid, sSql)
        ' Fill DDL Unit
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='UNIT'"
        If i_u.Text = "New Data" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
        FillDDL(matusageunitoid, sSql)

        'Group item
        'sSql = "SELECT gencode,gendesc FROM QL_mstgen WHERE gengroup='GROUPITEM' AND cmpcode='" & CompnyCode & "'"
        'FillDDL(matusagereftype, sSql)
        ' Fill DDL Material Mixing Unit
        'sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='UNIT'"
        'If i_u.Text = "New Data" Then
        '    sSql &= " AND activeflag='ACTIVE'"
        'End If
        'FillDDL(unitoid_mix, sSql)
    End Sub

    Private Sub IniDDLType(ByVal sType As String)
        sSql = "SELECT gencode,gendesc FROM QL_mstgen WHERE gengroup='GROUPITEM' AND cmpcode='" & CompnyCode & "' AND gencode LIKE '%" & sType & "%'"
        FillDDL(matusagereftype, sSql)
    End Sub

    Private Sub InitDDLDept()
        ' Fill DDL Department
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE cmpcode='" & DDLBusUnit.SelectedValue & "'"
        If i_u.Text = "New Data" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
        sSql &= " ORDER BY deptname"
        FillDDL(deptoid, sSql)
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
		sSql = "SELECT div.divname, mum.matusagemstoid, mum.matusageno, CONVERT(VARCHAR(10), mum.matusagedate, 101) AS matusagedate, de.deptname, mum.matusagemststatus, mum.matusagemstnote, mum.createuser FROM QL_trnmatusagemst mum INNER JOIN QL_mstdept de ON de.deptoid=mum.deptoid AND de.cmpcode=mum.cmpcode INNER JOIN QL_mstdivision div ON div.cmpcode=mum.cmpcode WHERE mum.cmpcode='" & CompnyCode & "'"
        sSql &= sSqlPlus & " ORDER BY CONVERT(DATETIME, mum.matusagedate) DESC, mum.matusagemstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trnmatusagemst")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub BindListMatReq()
        sSql = "SELECT DISTINCT reqm.matreqmstoid,reqm.matreqno, CONVERT(VARCHAR(10), reqm.matreqdate, 101) AS matreqdate,reqm.matreqwhoid, gendesc AS matreqwh,reqm.matreqmstnote,'' AS reftype/*,reqd.matreqreftype AS reftype,(SELECT red.matreqreftype FROM QL_trnmatreqdtl red WHERE red.matreqmstoid=reqm.matreqmstoid AND red.cmpcode=reqm.cmpcode) reftypere*/FROM QL_trnmatreqmst reqm INNER JOIN QL_trnmatreqdtl reqd ON reqd.matreqmstoid=reqm.matreqmstoid INNER JOIN QL_mstgen g ON genoid=matreqwhoid WHERE reqm.cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqm.matreqmststatus='Post' AND " & FilterDDLListMatReq.SelectedValue & " LIKE '%" & Tchar(FilterTextListMatReq.Text) & "%' AND reqm.deptoid=" & deptoid.SelectedValue & " ORDER BY reqm.matreqmstoid"
        FillGV(gvListMatReq, sSql, "QL_trnmatreqmst")
    End Sub

    Private Sub BindMatData()
        Dim sStr As String = ""
        If Session("oid") IsNot Nothing And Session("oid") <> "" Then
			sStr = " AND matusagemstoid<>" & Session("oid")
        End If
        ' AND crd.mtrlocoid=" & matusagewhoid.SelectedValue & "
        Dim sType As String = ""
        Dim sRef As String = ""
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        Dim sPeriode As String = GetDateToPeriodAcctg(GetServerTime.AddMonths(-1))
        sSql = "SELECT 'False' AS checkvalue,0 AS reqdtlseq,Itemoid AS matreqdtloid,Itemoid AS matreqrefoid, '' AS matreqrefno, m.itemCode AS matreqrefcode, m.itemoldcode AS matreqrefoldcode, m.itemLongDescription AS matreqreflongdesc, m.itemgroup AS RefType, 0.0 AS matreqqty,m.itemUnit1 AS matrequnitoid, gendesc AS matrequnit, ISNULL((SELECT SUM(crd.saldoakhir) FROM QL_crdstock crd WHERE crd.cmpcode=m.cmpcode AND m.itemoid=crd.refoid AND crd.periodacctg IN ('" & sPeriode & "', '" & sPeriod & "') AND crd.mtrlocoid=" & matusagewhoid.SelectedValue & "), 0.0) AS stockqty, 0.0 AS matusageqty, '' AS matusagedtlnote, 'True' AS enableqty,(SELECT ISNULL(genother1,'') acctgoid FROM QL_mstgen gx WHERE gx.cmpcode='" & CompnyCode & "' AND gx.gengroup='GROUPITEM' AND gx.gencode=m.itemgroup AND activeflag='ACTIVE') AS stockacctgoid, 0.00 AS matusagevalueidr, 0.00 AS matusagevalueusd, itemoldcode FROM QL_mstitem m INNER JOIN QL_mstgen g ON g.genoid=m.itemUnit1 WHERE m.cmpcode='" & CompnyCode & "'"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstmat")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(C1)("matusageqty") = ToDouble(dt.Rows(C1)("matreqqty").ToString)
        Next
        dt.AcceptChanges()
        Session("TblMat") = dt
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("QL_trnmatusagedtl")
        dtlTable.Columns.Add("matusagedtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matreqmstoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matreqdate", Type.GetType("System.String"))
        dtlTable.Columns.Add("matreqno", Type.GetType("System.String"))
        dtlTable.Columns.Add("matusagewhoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matusagewh", Type.GetType("System.String"))
        dtlTable.Columns.Add("matusagereftype", Type.GetType("System.String"))
        dtlTable.Columns.Add("matreqdtloid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matusagerefoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matusagerefcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("matusagerefoldcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("matusagereflongdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("matreqqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("stockqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("matusageqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("matusageunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matusageunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("matusagedtlnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("matusagevalueidr", Type.GetType("System.Double"))
        dtlTable.Columns.Add("matusagevalueusd", Type.GetType("System.Double"))
        dtlTable.Columns.Add("usagevalueidr", Type.GetType("System.Double"))
        dtlTable.Columns.Add("stockacctgoid", Type.GetType("System.Double"))
        dtlTable.Columns.Add("matusageqty_unitkecil", Type.GetType("System.Double"))
        dtlTable.Columns.Add("matusageqty_unitbesar", Type.GetType("System.Double"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub ClearDetail()
        matusagedtlseq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                matusagedtlseq.Text = objTable.Rows.Count + 1
                EnableHeader(False)
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        i_u2.Text = "New Detail"
        matreqmstoid.Text = ""
        matreqdate.Text = ""
        matreqno.Text = ""
        matusagewhoid.SelectedIndex = -1
        matreqdtloid.Text = ""
        matusagerefoid.Text = ""
        matusagerefcode.Text = ""
        matusagereflongdesc.Text = ""
        matreqqty.Text = ""
        stockqty.Text = ""
        matusageqty.Text = ""
        matusageunitoid.SelectedIndex = -1
        matusagedtlnote.Text = ""
        gvDtl.SelectedIndex = -1
        btnSearchMatReq.Visible = True : btnClearMatReq.Visible = True
        matusagereftype.Enabled = True : matusagereftype.CssClass = "inpText"
        btnSearchMat.Visible = True
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal : DDLBusUnit.CssClass = sCss
        deptoid.Enabled = bVal : deptoid.CssClass = sCss
    End Sub

    Private Sub generateUsageNo()
		Dim sNo As String = "USGNK-" & Format(GetServerTime(), "yyMM") & "-"
		sSql = "SELECT ISNULL(MAX(CAST(RIGHT(matusageno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnmatusagemst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matusageno LIKE '%" & sNo & "%'"
		matusageno.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
    End Sub

    Private Sub FillTextBox(ByVal sOid As String, ByVal sCom As String)
        Try
			sSql = "SELECT cmpcode,matusagemstoid, periodacctg,matusagedate,matusageno, deptoid,matusagemstnote,matusagemststatus, createuser, createtime, upduser,updtime FROM QL_trnmatusagemst WHERE matusagemstoid=" & sOid & " AND cmpcode='" & sCom & "'"
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = xreader("cmpcode").ToString
                DDLBusUnit_SelectedIndexChanged(Nothing, Nothing)
                matusagemstoid.Text = xreader("matusagemstoid").ToString
                periodacctg.Text = xreader("periodacctg").ToString
                matusagedate.Text = Format(xreader("matusagedate"), "MM/dd/yyyy")
                matusageno.Text = xreader("matusageno").ToString
                deptoid.SelectedValue = xreader("deptoid").ToString
                matusagemstnote.Text = xreader("matusagemstnote").ToString
                matusagemststatus.Text = xreader("matusagemststatus").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
			End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            xreader.Close()
            conn.Close()
            btnSave.Visible = False
            btnDelete.Visible = False
            btnposting.Visible = False
            Exit Sub
        End Try
        If matusagemststatus.Text = "Post" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnposting.Visible = False
            lblspuno.Text = "Usage No."
            matusagemstoid.Visible = False
            matusageno.Visible = True
            btnAddToList.Visible = False
            gvDtl.Columns(0).Visible = False
            gvDtl.Columns(gvDtl.Columns.Count - 1).Visible = False
        End If
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sSql = "SELECT DISTINCT matusagedtlseq,mud.matusagedtlnote,mud.matreqmstoid,GETDATE() AS matreqdate,'' matreqno,mud.matusagewhoid, g1.gendesc AS matusagewh,mud.matreqdtloid,mud.matusagerefoid,matusagereftype,m.itemCode AS matusagerefcode,m.itemoldcode AS matusagerefoldcode,m.itemLongDescription AS matusagereflongdesc,matusageqty,0 AS matreqqty,ISNULL((SELECT SUM(crd.saldoakhir) FROM QL_crdstock crd WHERE crd.cmpcode=mud.cmpcode AND refoid=mud.matusagerefoid AND mtrlocoid=mud.matusagewhoid AND crd.periodacctg IN ('" & sPeriod & "', '" & GetLastPeriod(sPeriod) & "') AND closingdate='01/01/1900'), 0.0) AS stockqty,mud.matusageunitoid,g2.gendesc AS matusageunit, mud.matusagedtlnote AS usagedtlnote,mud.matusagevalueidr, mud.matusagevalueusd,(SELECT ISNULL(genother1,'') acctgoid FROM QL_mstgen gx WHERE gx.cmpcode='" & sCom & "' AND gx.gengroup='GROUPITEM' AND gx.gencode=m.itemgroup AND activeflag='ACTIVE') AS stockacctgoid, ISNULL((SELECT refhpp FROM QL_crdhpp h WHERE h.refoid=m.itemoid AND h.hppstatus='OPEN'), 0.0) AS usagevalueidr, ISNULL((SELECT refhppusd FROM QL_crdhpp h WHERE h.refoid=m.itemoid AND h.hppstatus='OPEN'), 0.0) AS usagevalueusd,matusageqty AS matusageqty_unitkecil,matusageqty AS matusageqty_unitbesar FROM QL_trnmatusagedtl mud" & _
        " INNER JOIN QL_mstitem m ON m.itemoid=mud.matusagerefoid AND m.cmpcode=mud.cmpcode" & _
        " INNER JOIN QL_mstgen g1 ON g1.genoid=mud.matusagewhoid" & _
        " INNER JOIN QL_mstgen g2 ON g2.genoid=mud.matusageunitoid" & _
        " WHERE mud.matusagemstoid=" & sOid & " AND mud.cmpcode='" & sCom & "'" & _
        " ORDER BY matusagedtlseq"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnmatusagedtl")
        Session("TblDtl") = dt
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        ClearDetail()
    End Sub

    Private Sub PrintReport(ByVal sOid As String)
        Try
            report.Load(Server.MapPath(folderReport & "rptMatUsage.rpt"))
            Dim sWhere As String = "WHERE 1=1 "
            If sOid = "" Then
                sWhere &= " AND mum.cmpcode='" & CompnyCode & "' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " and mum.matusagedate>='" & FilterPeriod1.Text & " 00:00:00' AND mum.matusagedate<='" & FilterPeriod2.Text & "'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sWhere &= " and mum.matusagemststatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Transaction\trnMatUsage.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND mum.createuser='" & Session("UserID") & "'"
                End If
            Else
                sWhere &= " AND mum.matusagemstoid=" & sOid
            End If
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "MaterialUsageNonKIKPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
        End Try
        Response.Redirect("~\Transaction\trnMatUsage.aspx?awal=true")
    End Sub

    Private Sub SetUsageValue(ByRef objVal As VarUsageValue)
        objVal.Val_IDR = 0
        objVal.Val_USD = 0
        Dim dt As DataTable = Session("TblDtl")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            Dim dValIDR As Double = GetStockValue(dt.Rows(C1)("matusagerefoid").ToString, "")
            Dim dValUSD As Double = GetStockValue(dt.Rows(C1)("matusagerefoid").ToString, "USD")
            dt.Rows(C1)("matusagevalueidr") = dValIDR
            dt.Rows(C1)("matusagevalueusd") = dValUSD
            objVal.Val_IDR += dValIDR * ToDouble(dt.Rows(C1)("matusageqty").ToString)
            objVal.Val_USD += dValUSD * ToDouble(dt.Rows(C1)("matusageqty").ToString)
        Next
        Session("TblDtl") = dt
    End Sub

    Private Sub EnableQtyMix(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        qty_mix.Enabled = bVal : qty_mix.CssClass = sCss
        unitoid_mix.Enabled = bVal : unitoid_mix.CssClass = sCss
    End Sub

    Private Sub BindListMatMix()
        sSql = "SELECT * FROM (SELECT matrawoid AS matoid_mix, matrawcode AS matcode_mix, matrawlongdesc AS matlongdesc_mix, gendesc AS matunit_mix, matrawunitoid AS unitoid_mix FROM QL_mstmatraw m INNER JOIN QL_mstgen g ON g.cmpcode=m.cmpcode AND genoid=matrawunitoid WHERE m.cmpcode='" & CompnyCode & "' AND LEFT(matrawcode, 2) IN (SELECT cat1code FROM QL_mstcat1 c1 WHERE c1.cmpcode='" & CompnyCode & "' AND cat1oid IN (SELECT c0d.cat1oid FROM QL_mstcat0dtl c0d INNER JOIN QL_mstoid oi ON oi.cmpcode=c0d.cmpcode AND lastoid=cat0oid WHERE c0d.cmpcode='" & CompnyCode & "' AND tablename='SET MATERIAL MIXING'))) AS tbl_MatMix WHERE " & FilterDDLListMatMix.SelectedValue & " LIKE '%" & Tchar(FilterTextListMatMix.Text) & "%' ORDER by matcode_mix"
        FillGV(gvListMatMix, sSql, "tbl_MatMix")
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnMatUsage.aspx")
        End If
        If checkPagePermission("~\Transaction\trnMatUsage.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Material Usage Non KIK"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnposting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            CheckUsageStatus()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            IniDDLType("")
            InitAllDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"), CompnyCode)
                TabContainer1.ActiveTabIndex = 1
            Else
				matusagemstoid.Text = GenerateID("QL_TRNMATUSAGEMST", CompnyCode)
                matusagedate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                matusagemststatus.Text = "In Process"
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvDtl.DataSource = dt
            gvDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnMatUsage.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lkbUsageInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbUsageInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = 0
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = 0
		Dim sSqlPlus As String = " AND DATEDIFF(DAY, mum.updtime, GETDATE()) > " & nDays & " AND mum.usagemststatus='In Process'"
        If checkPagePermission("~\Transaction\trnMatUsage.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND mum.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND mum.matusagedate>='" & FilterPeriod1.Text & " 00:00:00' AND mum.matusagedate<='" & FilterPeriod2.Text & "'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND mum.matusagemststatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Transaction\trnMatUsage.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND mum.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = "" : FilterDDL.SelectedIndex = -1
        FilterText.Text = "" : cbPeriode.Checked = False
        cbStatus.Checked = False : FilterDDLStatus.SelectedIndex = -1
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        If checkPagePermission("~\Transaction\trnMatUsage.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND mum.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLDept()
    End Sub

    Protected Sub btnSearchMatReq_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMatReq.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        If deptoid.SelectedValue = "" Then
            showMessage("Please select Department first!", 2)
            Exit Sub
        End If
        FilterDDLListMatReq.SelectedIndex = -1
        FilterTextListMatReq.Text = ""
        gvListMatReq.SelectedIndex = -1
        BindListMatReq()
        cProc.SetModalPopUpExtender(btnHideListMatReq, pnlListMatReq, mpeListMatReq, True)
    End Sub

    Protected Sub btnClearMatReq_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearMatReq.Click
        matreqmstoid.Text = "" : matreqdate.Text = ""
        matreqno.Text = "" : matusagewhoid.SelectedIndex = -1
        Dim tYpe As String = ""
        IniDDLType(tYpe)
    End Sub

    Protected Sub btnFindListMatReq_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMatReq.Click
        BindListMatReq()
        mpeListMatReq.Show()
    End Sub

    Protected Sub btnAllListMatReq_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMatReq.Click
        FilterDDLListMatReq.SelectedIndex = -1 : FilterTextListMatReq.Text = "" : gvListMatReq.SelectedIndex = -1
        BindListMatReq()
        mpeListMatReq.Show()
    End Sub

    Protected Sub gvListMatReq_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMatReq.PageIndexChanging
        gvListMatReq.PageIndex = e.NewPageIndex
        BindListMatReq()
        mpeListMatReq.Show()
    End Sub

    Protected Sub gvListMatReq_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListMatReq.SelectedIndexChanged
        If matreqmstoid.Text <> gvListMatReq.SelectedDataKey.Item("matreqmstoid").ToString Then
            btnClearMatReq_Click(Nothing, Nothing)
        End If

        matreqmstoid.Text = gvListMatReq.SelectedDataKey.Item("matreqmstoid").ToString
        matreqno.Text = gvListMatReq.SelectedDataKey.Item("matreqno").ToString
        matreqdate.Text = gvListMatReq.SelectedDataKey.Item("matreqdate").ToString
        matusagewhoid.SelectedValue = gvListMatReq.SelectedDataKey.Item("matreqwhoid").ToString
        'Dim type As String = gvListMatReq.SelectedDataKey.Item("reftype").ToString
        'IniDDLType(type)
        cProc.SetModalPopUpExtender(btnHideListMatReq, pnlListMatReq, mpeListMatReq, False)
    End Sub

    Protected Sub lbCloseListMatReq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMatReq.Click
        cProc.SetModalPopUpExtender(btnHideListMatReq, pnlListMatReq, mpeListMatReq, False)
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        FilterDDLListMat.SelectedIndex = -1
        FilterTextListMat.Text = ""
        Session("TblMat") = Nothing
        Session("TblMatView") = Nothing
        gvListMat.DataSource = Nothing
        gvListMat.DataBind()
        tbData.Text = "5"
        gvListMat.PageSize = CInt(tbData.Text)
        BindMatData()
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblMat") Is Nothing Then
			'BindMatData()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = ""
        If matusagereftype.SelectedIndex > 2 Then
            If FilterDDLListMat.SelectedIndex = 0 Then
                Dim sText() As String = FilterTextListMat.Text.Split(";")
                For C1 As Integer = 0 To sText.Length - 1
                    If sText(C1) <> "" Then
                        sPlus &= FilterDDLListMat.SelectedValue & " LIKE '" & Tchar(sText(C1)) & "' OR "
                    End If
                Next
                If sPlus <> "" Then
                    sPlus = Left(sPlus, sPlus.Length - 4)
                End If
            Else
                sPlus = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
            End If
        Else
            sPlus = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        End If
        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If Session("TblMat") Is Nothing Then
			'BindMatData()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matreqdtloid=" & dtTbl.Rows(C1)("matreqdtloid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matreqdtloid=" & dtTbl.Rows(C1)("matreqdtloid")
                    objView(0)("checkvalue") = "False"
                    objView(0)("matusageqty") = 0
                    objView(0)("matusagedtlnote") = ""
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    dtTbl.Rows(C1)("matusageqty") = 0
                    dtTbl.Rows(C1)("matusagedtlnote") = ""
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Selected material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub lbShowData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbShowData.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Please find material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If tbData.Text = "" Then
            Session("WarningListMat") = "Please fill total data to be shown!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        Else
            If ToDouble(tbData.Text) <= 0 Then
                Session("WarningListMat") = "Total data to be shown must be more than 0!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat2() Then
            gvListMat.PageSize = Int(tbData.Text)
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(5).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 2)
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    dtView.RowFilter = ""
                    dtView.RowFilter = "checkvalue='True' AND matusageqty > 0"
                    If dtView.Count <> iCheck Then
                        Session("WarningListMat") = "Usage Qty for every checked material data must be more than 0!"
                        showMessage(Session("WarningListMat"), 2)
                        dtView.RowFilter = ""
                        Exit Sub
                    End If
                    dtView.RowFilter = ""
                    dtView.RowFilter = "CheckValue='True' AND matusageqty <= stockqty"
                    If dtView.Count <> iCheck Then
                        Session("WarningListMat") = "Usage Qty for every checked material data must be less than Stock Qty!"
                        showMessage(Session("WarningListMat"), 2)
                        dtView.RowFilter = ""
                        Exit Sub
                    End If
                    If Session("TblDtl") Is Nothing Then
                        CreateTblDetail()
                    End If
                    Dim objTable As DataTable
                    objTable = Session("TblDtl")
                    Dim dv As DataView = objTable.DefaultView
                    Dim objRow As DataRow
                    Dim counter As Integer = objTable.Rows.Count
                    For C1 As Integer = 0 To dtView.Count - 1
                        dv.RowFilter = "matreqdtloid=" & dtView(C1)("matreqdtloid")
                        If dv.Count > 0 Then
                            dv.AllowEdit = True
                            dv(0)("matusageqty") = ToDouble(dtView(C1)("matusageqty"))
                            dv(0)("matusageqty_unitkecil") = ToDouble(dtView(C1)("matusageqty"))
                            dv(0)("matusageqty_unitbesar") = ToDouble(dtView(C1)("matusageqty"))
                            dv(0)("matusagedtlnote") = dtView(C1)("matusagedtlnote")
                        Else
                            counter += 1
                            objRow = objTable.NewRow()
                            objRow("matusagedtlseq") = counter
                            objRow("matreqdate") = matreqdate.Text
                            objRow("matusagewhoid") = matusagewhoid.SelectedValue
                            objRow("matusagewh") = matusagewhoid.SelectedItem.Text
                            objRow("matusagereftype") = dtView(C1)("RefType")
                            objRow("matusagerefoid") = dtView(C1)("matreqrefoid")
                            objRow("matusagerefcode") = dtView(C1)("matreqrefcode")
                            objRow("matusagerefoldcode") = dtView(C1)("matreqrefoldcode")
                            objRow("matusagereflongdesc") = dtView(C1)("matreqreflongdesc")
                            objRow("stockqty") = ToDouble(dtView(C1)("stockqty"))
                            objRow("matusageqty") = ToDouble(dtView(C1)("matusageqty"))
                            objRow("matusageqty_unitkecil") = ToDouble(dtView(C1)("matusageqty"))
                            objRow("matusageqty_unitbesar") = ToDouble(dtView(C1)("matusageqty"))
                            objRow("matusageunitoid") = dtView(C1)("matrequnitoid")
                            objRow("matusageunit") = dtView(C1)("matrequnit")
                            objRow("matusagedtlnote") = dtView(C1)("matusagedtlnote")
                            objRow("matusagevalueidr") = 0
                            objRow("matusagevalueusd") = 0
                            objRow("stockacctgoid") = dtView(C1)("stockacctgoid")
                            objTable.Rows.Add(objRow)
                        End If
                        dv.RowFilter = ""
                    Next
                    dtView.RowFilter = ""
                    Session("TblDtl") = objTable
                    gvDtl.DataSource = objTable
                    gvDtl.DataBind()
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                    ClearDetail()
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            If Session("TblDtl") Is Nothing Then
                CreateTblDetail()
            End If
            Dim objTable As DataTable = Session("TblDtl")
            Dim dv As DataView = objTable.DefaultView
            If i_u2.Text = "New Detail" Then
                dv.RowFilter = "matreqdtloid=" & matreqdtloid.Text
            Else
                dv.RowFilter = "matreqdtloid=" & matreqdtloid.Text & " AND matusagedtlseq <> " & matusagedtlseq.Text
            End If
            'If dv.Count > 0 Then
            '    showMessage("This Data has been added before, please check!", 2)
            '    dv.RowFilter = ""
            '    Exit Sub
            'End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                objRow("matusagedtlseq") = objTable.Rows.Count + 1
            Else
                objRow = objTable.Rows(matusagedtlseq.Text - 1)
                objRow.BeginEdit()
            End If
            objRow("matreqdate") = matreqdate.Text
            objRow("matusagewhoid") = matusagewhoid.SelectedValue
            objRow("matusagewh") = matusagewhoid.SelectedItem.Text
            objRow("matusagereftype") = matusagereftype.SelectedValue
            objRow("matreqdtloid") = matreqdtloid.Text
            objRow("matusagerefoid") = matusagerefoid.Text
            objRow("matusagerefcode") = matusagerefcode.Text
            objRow("matusagereflongdesc") = matusagereflongdesc.Text
            objRow("stockqty") = ToDouble(stockqty.Text)
            objRow("matusageqty") = ToDouble(matusageqty.Text)
            objRow("matusageunitoid") = matusageunitoid.SelectedValue
            objRow("matusageunit") = matusageunitoid.SelectedItem.Text
            objRow("matusagedtlnote") = matusagedtlnote.Text
            If i_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            Session("TblDtl") = objTable
            gvDtl.DataSource = Session("TblDtl")
            gvDtl.DataBind()
            ClearDetail()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 4)
        End If
    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDtl.SelectedIndexChanged
        Try
            matusagedtlseq.Text = gvDtl.SelectedDataKey.Item("matusagedtlseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "matusagedtlseq=" & matusagedtlseq.Text
                matreqmstoid.Text = dv.Item(0).Item("matreqmstoid").ToString
                matreqdate.Text = dv.Item(0).Item("matreqdate").ToString
                matreqno.Text = dv.Item(0).Item("matreqno").ToString
                matusagewhoid.SelectedValue = dv.Item(0).Item("matusagewhoid").ToString
                matusagereftype.SelectedValue = dv.Item(0).Item("matusagereftype").ToString
                matreqdtloid.Text = dv.Item(0).Item("matreqdtloid").ToString
                matusagerefoid.Text = dv.Item(0).Item("matusagerefoid").ToString
                matusagerefcode.Text = dv.Item(0).Item("matusagerefcode").ToString
                matusagereflongdesc.Text = dv.Item(0).Item("matusagereflongdesc").ToString
                matreqqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("matreqqty").ToString), 4)
                stockqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("stockqty").ToString), 4)
                matusageqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("matusageqty").ToString), 4)
                matusageunitoid.SelectedValue = dv.Item(0).Item("matusageunitoid").ToString
                matusagedtlnote.Text = dv.Item(0).Item("matusagedtlnote").ToString
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        Finally
            btnSearchMatReq.Visible = False : btnClearMatReq.Visible = False
            matusagereftype.Enabled = False : matusagereftype.CssClass = "inpTextDisabled"
            btnSearchMat.Visible = False
        End Try
    End Sub

    Protected Sub gvDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("matusagedtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        ClearDetail()
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If matusagemstoid.Text = "" Then
            showMessage("Please select Process Material Usage data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNMATUSAGEMST", "matusagemstoid", matusagemstoid.Text, "matusagemststatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                matusagemststatus.Text = "In Process"
                Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trnmatreqdtl SET matreqdtlstatus='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matreqdtloid IN (SELECT DISTINCT matreqdtloid FROM QL_trnmatusagedtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matusagemstoid=" & matusagemstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_trnmatreqmst SET matreqmststatus='Post' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matreqmstoid IN (SELECT DISTINCT matreqmstoid FROM QL_trnmatusagedtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matusagemstoid=" & matusagemstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnmatusagedtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matusagemstoid=" & matusagemstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnmatusagemst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matusagemstoid=" & matusagemstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnMatUsage.aspx?awal=true")
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_TRNMATUSAGEMST WHERE matusagemstoid=" & matusagemstoid.Text
                If CheckDataExists(sSql) Then
                    matusagemstoid.Text = GenerateID("QL_TRNMATUSAGEMST", CompnyCode)
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNMATUSAGEMST", "matusagemstoid", matusagemstoid.Text, "matusagemststatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    matusagemststatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            matusagedtloid.Text = GenerateID("QL_trnmatusagedtl", CompnyCode)
            Dim conmtroid As Integer = 0, crdmatoid As Integer = 0, glmstoid As Integer = 0, gldtloid As Integer = 0, conmtroid_mix As Integer = 0, crdmatoid_mix As Integer = 0, iStockValOid As Integer = 0
            Dim iStockSPAcctgOid As Integer = 0
            Dim iPostAcctgOid As Integer = 0 : Dim objValue As VarUsageValue
            Dim dtSumStock As DataTable = Nothing : Dim dtSumUsage As DataTable = Nothing
            Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
            Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
            periodacctg.Text = GetDateToPeriodAcctg(CDate(matusagedate.Text))
            Dim iGroupOid As Integer = 0
            If matusagemststatus.Text = "Post" Then
                cRate.SetRateValue("IDR", sDate)
                If isPeriodClosed(CompnyCode, sPeriod) Then
                    showMessage("Cannot continue this transaction anymore because the period : " & MonthName(CInt(sPeriod.Substring(4, 2))) & " " & sPeriod.Substring(0, 4) & " have been closed.", 3)
                    Exit Sub
                End If
                If cRate.GetRateDailyLastError <> "" Then
                    showMessage(cRate.GetRateDailyLastError, 2)
                    matusagemststatus.Text = "In Process"
                    Exit Sub
                End If
                If cRate.GetRateMonthlyLastError <> "" Then
                    showMessage(cRate.GetRateMonthlyLastError, 2)
                    matusagemststatus.Text = "In Process"
                    Exit Sub
                End If
                'conmtro()
                Dim sVarErr As String = ""
                If Not IsInterfaceExists("VAR_USAGE_NON_KIK", CompnyCode) Then
                    sVarErr = "VAR_USAGE_NON_KIK"
                End If
                If Not IsInterfaceExists("VAR_STOCK", CompnyCode) Then
                    sVarErr = "VAR_STOCK"
                End If
                If sVarErr <> "" Then
                    showMessage(GetInterfaceWarning(sVarErr, "posting"), 2)
                    matusagemststatus.Text = "In Process"
                    Exit Sub
                End If
                conmtroid = GenerateID("QL_CONSTOCK", CompnyCode)
                crdmatoid = GenerateID("QL_CRDSTOCK", CompnyCode)
                glmstoid = GenerateID("QL_TRNGLMST", CompnyCode)
                gldtloid = GenerateID("QL_TRNGLDTL", CompnyCode)
                iStockValOid = GenerateID("QL_STOCKVALUE", CompnyCode)
                iPostAcctgOid = GetAcctgOID(GetVarInterface("VAR_USAGE_NON_KIK", CompnyCode), CompnyCode)
                iStockSPAcctgOid = GetAcctgOID(GetVarInterface("VAR_STOCK", CompnyCode), CompnyCode)
                dtSumStock = GetAmtByGroup(Session("TblDtl"))
                SetUsageValue(objValue)
                generateUsageNo()
                iGroupOid = ToInteger(GetStrData("SELECT TOP 1 groupoid FROM QL_mstdeptgroupdtl WHERE cmpcode='" & CompnyCode & "' AND deptoid=" & deptoid.SelectedValue & " ORDER BY updtime DESC"))
            End If
            sSql = "SELECT '' tipe, 0 acctgoid, 0.0 debet, 0.0 credit, '' refno, '' note, '' AS glother1 "
            Dim tbPostGL As DataTable = cKon.ambiltabel(sSql, "PostGL")
            tbPostGL.Rows.Clear()
            Dim oRow As DataRow
            Dim dvCek As DataView = tbPostGL.DefaultView
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trnmatusagemst (cmpcode,matusagemstoid,periodacctg,matusagedate,matusageno,deptoid,matusagemstnote,matusagemststatus,createuser,createtime,upduser,updtime) " & _
                    " VALUES " & _
                    "('" & CompnyCode & "'," & matusagemstoid.Text & ",'" & periodacctg.Text & "','" & CDate(matusagedate.Text) & "','" & Tchar(matusageno.Text) & "', " & deptoid.SelectedValue & ",'" & Tchar(matusagemstnote.Text) & "','" & matusagemststatus.Text & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & matusagemstoid.Text & " WHERE tablename='QL_trnmatusagemst' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trnmatusagemst SET periodacctg='" & periodacctg.Text & "', matusagedate='" & matusagedate.Text & "', matusageno='" & matusageno.Text & "', deptoid=" & deptoid.SelectedValue & ", matusagemstnote='" & Tchar(matusagemstnote.Text) & "', matusagemststatus='" & matusagemststatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE matusagemstoid=" & matusagemstoid.Text
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    'sSql = "UPDATE QL_trnmatreqdtl SET matreqdtlstatus='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matreqdtloid IN (SELECT matreqdtloid FROM QL_trnmatusagedtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matusagemstoid=" & matusagemstoid.Text & ")"
                    'xCmd.CommandText = sSql
                    'xCmd.ExecuteNonQuery()
                    'sSql = "UPDATE QL_trnmatreqmst SET matreqmststatus='Post' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matreqmstoid IN (SELECT matreqmstoid FROM QL_trnmatusagedtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matusagemstoid=" & matusagemstoid.Text & ")"
                    'xCmd.CommandText = sSql
                    'xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trnmatusagedtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matusagemstoid=" & matusagemstoid.Text
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trnmatusagedtl (cmpcode,matusagedtloid,matusagemstoid,matusagedtlseq,matreqmstoid,matreqdtloid,matusagerefoid,matusageqty,matusageunitoid,matusagewhoid,matusagedtlnote,matusagedtlstatus,upduser,updtime,matusagevalueidr,matusagevalueusd,matusagereftype) " & _
      " VALUES " & _
      "('" & CompnyCode & "'," & (C1 + CInt(matusagedtloid.Text)) & "," & matusagemstoid.Text & "," & C1 + 1 & ",0,0," & objTable.Rows(C1).Item("matusagerefoid") & "," & ToDouble(objTable.Rows(C1).Item("matusageqty").ToString) & "," & objTable.Rows(C1).Item("matusageunitoid") & "," & objTable.Rows(C1).Item("matusagewhoid") & ",'" & Tchar(objTable.Rows(C1).Item("matusagedtlnote")) & "','', '" & Session("UserID") & "',CURRENT_TIMESTAMP," & ToDouble(objTable.Rows(C1).Item("matusagevalueidr").ToString) & "," & ToDouble(objTable.Rows(C1).Item("matusagevalueusd").ToString) & ",'" & matusagereftype.SelectedValue & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        'If objTable.Rows(C1).Item("matusageqty") >= objTable.Rows(C1).Item("matreqqty") Then
                        '    sSql = "UPDATE QL_trnmatreqdtl SET matreqdtlstatus='Complete' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matreqdtloid=" & objTable.Rows(C1).Item("matreqdtloid")
                        '    xCmd.CommandText = sSql
                        '    xCmd.ExecuteNonQuery()
                        '    sSql = "UPDATE QL_trnmatreqmst SET matreqmststatus='Closed' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matreqmstoid=" & objTable.Rows(C1).Item("matreqmstoid") & " AND (SELECT COUNT(*) FROM QL_trnmatreqdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matreqmstoid=" & objTable.Rows(C1).Item("matreqmstoid") & " AND matreqdtloid <> " & objTable.Rows(C1).Item("matreqdtloid") & " AND matreqdtlstatus='')=0"
                        '    xCmd.CommandText = sSql
                        '    xCmd.ExecuteNonQuery()
                        'End If
                    Next
                    sSql = " UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(matusagedtloid.Text)) & " WHERE tablename='QL_trnmatusagedtl' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
                If matusagemststatus.Text = "Post" Then
                    Dim objtbl As DataTable = Session("TblDtl")
                    For C1 As Int16 = 0 To objtbl.Rows.Count - 1
                        Dim sRef As String = ""
                        If objtbl.Rows(C1)("matusagereftype").ToString.ToUpper = "RAW" Then
                            sRef = "RAW MATERIAL"
                        ElseIf objtbl.Rows(C1)("matusagereftype").ToString.ToUpper = "GEN" Then
                            sRef = "GENERAL MATERIAL"
                        ElseIf objtbl.Rows(C1)("matusagereftype").ToString.ToUpper = "FG" Then
                            sRef = "FINISH GOOD"
                        ElseIf objtbl.Rows(C1)("matusagereftype").ToString.ToUpper = "WIP" Then
                            sRef = "WIP"
                        End If
                        sSql = "INSERT INTO QL_constock (cmpcode, constockoid, contype, typemin, trndate, periodacctg, formaction, formoid, refoid, refname, mtrlocoid, qtyin, qtyout, reason, note,upduser, updtime, refno, deptoid, valueidr, valueusd)" & _
                        " VALUES ('" & DDLBusUnit.SelectedValue & "', " & conmtroid & ", 'MUNONKIK', -1,'" & sDate & "', '" & sPeriod & "', 'QL_trnmatusagedtl', " & matusagemstoid.Text & ", " & objtbl.Rows(C1).Item("matusagerefoid") & ", '" & sRef & "', " & objtbl.Rows(C1).Item("matusagewhoid") & ", 0, " & ToDouble(objtbl.Rows(C1).Item("matusageqty")) & ", 'Material Usage Non KIK', '" & matusageno.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Tchar(matusageno.Text) & "', " & deptoid.SelectedValue & ", " & ToDouble(objtbl.Rows(C1).Item("matusagevalueidr").ToString) & "," & ToDouble(objtbl.Rows(C1).Item("matusagevalueusd").ToString) & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        conmtroid += 1
                        ' Insert QL_crdmtr

                        sSql = "UPDATE QL_crdstock SET qtyout=qtyout + " & ToDouble(objtbl.Rows(C1).Item("matusageqty").ToString) & ", saldoakhir=saldoakhir - " & ToDouble(objtbl.Rows(C1).Item("matusageqty")) & ", lasttranstype='QL_trnmatusagedtl', lasttransdate='" & sDate & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & CompnyCode & "' AND refoid=" & objtbl.Rows(C1).Item("matusagerefoid") & " AND mtrlocoid=" & objtbl.Rows(C1).Item("matusagewhoid") & " AND periodacctg='" & sPeriod & "'"
                        xCmd.CommandText = sSql
                        If xCmd.ExecuteNonQuery() = 0 Then
                            sSql = "INSERT INTO QL_crdstock (cmpcode, crdstockoid, periodacctg, refoid, refname, mtrlocoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir, lasttranstype, lasttransdate, upduser, updtime, createuser, createtime, closeuser, closingdate) VALUES ('" & DDLBusUnit.SelectedValue & "', " & crdmatoid & ",'" & sPeriod & "', " & objtbl.Rows(C1).Item("matusagerefoid") & ", '" & sRef & "', " & objtbl.Rows(C1).Item("matusagewhoid") & ", 0, " & ToDouble(objtbl.Rows(C1).Item("matusageqty_unitkecil")) & ", 0, 0, 0, " & -ToDouble(objtbl.Rows(C1).Item("matusageqty_unitkecil")) & ", 'QL_trnmatusagedtl', '" & sDate & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', '1/1/1900')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            crdmatoid += 1
                        End If
                        dvCek.RowFilter = "acctgoid=" & objtbl.Rows(C1).Item("stockacctgoid").ToString & " "
                        If dvCek.Count > 0 Then
                            dvCek(0)("credit") += ToDouble(objtbl.Rows(C1).Item("matusagevalueidr").ToString) * ToDouble(objtbl.Rows(C1).Item("matusageqty").ToString)
                            dvCek.RowFilter = ""
                        Else
                            dvCek.RowFilter = ""
                            oRow = tbPostGL.NewRow
                            oRow("acctgoid") = objtbl.Rows(C1).Item("stockacctgoid")
                            oRow("debet") = 0
                            oRow("credit") = ToDouble(objtbl.Rows(C1).Item("matusagevalueidr").ToString) * ToDouble(objtbl.Rows(C1).Item("matusageqty").ToString)
                            tbPostGL.Rows.Add(oRow)
                        End If
                        tbPostGL.AcceptChanges()
                        'Insert STockvalue
                        sSql = GetQueryUpdateStockValue(-ToDouble(objtbl.Rows(C1)("matusageqty").ToString), -ToDouble(objtbl.Rows(C1)("matusageqty").ToString), ToDouble(objtbl.Rows(C1).Item("matusagevalueidr").ToString), ToDouble(objtbl.Rows(C1).Item("matusagevalueusd").ToString), "QL_trnmatusagedtl", sDate, Session("UserID"), DDLBusUnit.SelectedValue, sPeriod, objtbl.Rows(C1)("matusagerefoid"))
                        xCmd.CommandText = sSql
                        If xCmd.ExecuteNonQuery() = 0 Then
                            sSql = GetQueryInsertStockValue(-ToDouble(objtbl.Rows(C1)("matusageqty").ToString), -ToDouble(objtbl.Rows(C1)("matusageqty").ToString), ToDouble(objtbl.Rows(C1).Item("matusagevalueidr").ToString), ToDouble(objtbl.Rows(C1).Item("matusagevalueusd").ToString), "QL_trnmatusagedtl", sDate, Session("UserID"), DDLBusUnit.SelectedValue, sPeriod, objtbl.Rows(C1)("matusagerefoid"), iStockValOid)
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            iStockValOid += 1
                        End If
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & crdmatoid - 1 & " WHERE tablename='QL_crdstock' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & conmtroid - 1 & " WHERE tablename='QL_constock' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iStockValOid - 1 & " WHERE tablename='QL_stockvalue' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
                If matusagemststatus.Text = "Post" Then
                    Dim dTotalIDR As Double = objValue.Val_IDR
                    Dim dTotalUSD As Double = objValue.Val_USD
                    Dim sVal As Double = tbPostGL.Compute("SUM(credit)", "").ToString
                    'Dim sValue As Double = dtSumStock.Compute("SUM(amount)", "").ToString
                    If dTotalIDR > 0 Then
                        ' Insert GL MST
                        sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & glmstoid & ", '" & sDate & "', '" & sPeriod & "', 'USG Non KIK|No. " & matusageno.Text & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        ' Insert GL DTL
                        Dim iSeq As Integer = 1
						' PEMAKAIAN
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, groupoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & iPostAcctgOid & ", 'D', " & dTotalIDR & ", '" & matusageno.Text & "', '" & matusageno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalIDR * cRate.GetRateMonthlyIDRValue & ", " & dTotalUSD * cRate.GetRateMonthlyUSDValue & ", 'QL_trnmatusagemst " & matusagemstoid.Text & "', " & iGroupOid & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        iSeq += 1 : gldtloid += 1

                        ' PERSEDIAAN
                        For C1 As Integer = 0 To tbPostGL.Rows.Count - 1
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, groupoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & tbPostGL.Rows(C1)("acctgoid") & " , 'C', " & dTotalIDR & ", '" & matusageno.Text & "', '" & matusageno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalIDR * cRate.GetRateMonthlyIDRValue & ", " & dTotalUSD * cRate.GetRateMonthlyUSDValue & ", 'QL_trnmatusagemst " & matusagemstoid.Text & "', " & iGroupOid & ")"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            iSeq += 1 : gldtloid += 1
                        Next
                        sSql = "UPDATE QL_mstoid SET lastoid=" & glmstoid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        sSql = "UPDATE QL_mstoid SET lastoid=" & gldtloid & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                End If
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        matusagemststatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    matusagemststatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 2)
                conn.Close()
                matusagemststatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & matusagemstoid.Text & ".<BR>"
            End If
            If matusagemststatus.Text = "Post" Then
                Session("SavedInfo") &= "Data have been posted with Usage No. = " & matusageno.Text & "."
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trnMatUsage.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("~\Transaction\trnMatUsage.aspx?awal=true")
    End Sub

    Protected Sub btnposting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnposting.Click
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        If isPeriodClosed(CompnyCode, CDate(sDate)) Then
            showMessage("Period " & GetDateToPeriodAcctg(CDate(sDate)) & " Have been Already CLOSED, Please CANCEL this transaction and try again!", 2)
            Exit Sub
        End If
        matusagemststatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        PrintReport("")
    End Sub

    Protected Sub gvTRN_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvTRN.SelectedIndexChanged
        PrintReport(gvTRN.SelectedDataKey.Item("matusagemstoid").ToString)
    End Sub

    Protected Sub btnSearchMatMix_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMatMix.Click
        FilterDDLListMatMix.SelectedIndex = -1 : FilterTextListMatMix.Text = "" : gvListMatMix.SelectedIndex = -1
        BindListMatMix()
        cProc.SetModalPopUpExtender(btnHideListMatMix, pnlListMatMix, mpeListMatMix, True)
    End Sub

    Protected Sub btnClearMatMix_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearMatMix.Click
        matoid_mix.Text = "" : matlongdesc_mix.Text = "" : qty_mix.Text = "" : unitoid_mix.SelectedIndex = -1 : EnableQtyMix(False)
    End Sub

    Protected Sub btnFindListMatMix_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMatMix.Click
        BindListMatMix()
        mpeListMatMix.Show()
    End Sub

    Protected Sub btnAllListMatMix_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMatMix.Click
        FilterDDLListMatMix.SelectedIndex = -1 : FilterTextListMatMix.Text = "" : gvListMatMix.SelectedIndex = -1
        BindListMatMix()
        mpeListMatMix.Show()
    End Sub

    Protected Sub gvListMatMix_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMatMix.PageIndexChanging
        gvListMatMix.PageIndex = e.NewPageIndex
        BindListMatMix()
        mpeListMatMix.Show()
    End Sub

    Protected Sub gvListMatMix_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListMatMix.SelectedIndexChanged
        matoid_mix.Text = gvListMatMix.SelectedDataKey.Item("matoid_mix").ToString
        matlongdesc_mix.Text = gvListMatMix.SelectedDataKey.Item("matlongdesc_mix").ToString
        unitoid_mix.SelectedValue = gvListMatMix.SelectedDataKey.Item("unitoid_mix").ToString
        EnableQtyMix(True)
        cProc.SetModalPopUpExtender(btnHideListMatMix, pnlListMatMix, mpeListMatMix, False)
    End Sub

    Protected Sub lbCloseListMatMix_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMatMix.Click
        cProc.SetModalPopUpExtender(btnHideListMatMix, pnlListMatMix, mpeListMatMix, False)
    End Sub
#End Region

End Class
