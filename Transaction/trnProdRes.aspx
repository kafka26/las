<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnProdRes.aspx.vb" Inherits="Transaction_ProductionResult" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Production Result" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Production Result :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="Label4" runat="server" Text="Filter"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="105px"><asp:ListItem Value="prodresmstoid">Draft No.</asp:ListItem>
<asp:ListItem Value="prodresno">Result No.</asp:ListItem>
    <asp:ListItem Value="approductno">AP No.</asp:ListItem>
<asp:ListItem Value="deptname">Department</asp:ListItem>
<asp:ListItem Value="prodresmstnote">Note</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="250px"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text="Period"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="80px" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label5" runat="server" Text="to"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="80px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label9" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR>
    <tr>
        <td align="left" class="Label">
            <asp:CheckBox ID="cbGroupName" runat="server" Text="Supplier" /></td>
        <td align="center" class="Label">
            :</td>
        <td align="left" class="Label" colspan="2">
            <asp:DropDownList id="FilterDDLGroupName" runat="server" CssClass="inpText" Width="360px">
            </asp:DropDownList></td>
    </tr>
    <TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="105px">
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" PopupButtonID="imbDate1" Format="MM/dd/yyyy" TargetControlID="FilterPeriod1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="FilterPeriod1" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" PopupButtonID="imbDate2" Format="MM/dd/yyyy" TargetControlID="FilterPeriod2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" TargetControlID="FilterPeriod2" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton>
    <asp:ImageButton ID="btnPrint" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/print.png" /></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:LinkButton id="lkbResultInProcess" runat="server" Visible="False"></asp:LinkButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvTRN" runat="server" ForeColor="#333333" Width="100%" AllowPaging="True" PageSize="8" AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowSorting="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="prodresmstoid" DataNavigateUrlFormatString="~\Transaction\trnProdRes.aspx?oid={0}" DataTextField="prodresmstoid" HeaderText="Draft No." SortExpression="prodresmstoid">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="prodresno" HeaderText="Result No." SortExpression="prodresno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prodresdate" HeaderText="Result Date" SortExpression="prodresdate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="deptname" HeaderText="Department" SortExpression="deptname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
    <asp:BoundField DataField="suppname" HeaderText="Supplier" SortExpression="suppname">
        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
    </asp:BoundField>
    <asp:BoundField DataField="approductno" HeaderText="AP No.">
        <HeaderStyle CssClass="gvhdr" />
    </asp:BoundField>
<asp:BoundField DataField="prodresmststatus" HeaderText="Status" SortExpression="prodresmststatus">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prodresmstnote" HeaderText="Note" SortExpression="prodresmstnote">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
    <asp:BoundField DataField="divname" HeaderText="Business Unit" SortExpression="divname">
        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
        <ItemStyle HorizontalAlign="Left" />
    </asp:BoundField>
    <asp:TemplateField>
        <ItemTemplate>
            <asp:CheckBox ID="cbGVMst" runat="server" Checked='<%# eval("checkvalue") %>' />
            <asp:Label ID="lblOidGVMst" runat="server" Text='<%# eval("prodresmstoid") %>' Visible="False"></asp:Label>
        </ItemTemplate>
        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Center" />
        <ItemStyle HorizontalAlign="Center" />
    </asp:TemplateField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" ForeColor="White" CssClass="gvhdr" Font-Bold="True"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="No Data Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data"></asp:Label></TD></TR>
    <tr>
        <td align="center" class="Label" colspan="4">
            <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel2">
                <ProgressTemplate>
                    <span></span><span style="font-weight: bold; font-size: 10pt; color: purple">
                        <asp:Image ID="Image12" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />
                        Please Wait .....</span><br />
                </ProgressTemplate>
            </asp:UpdateProgress>
        </td>
    </tr>
</TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnPrint" />
                                </Triggers>
                           
                           
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label16" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Production Result Header" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="i_u" runat="server" CssClass="Important" Text="New Data"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="periodacctg" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="Label2" runat="server" Text="Business Unit" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpText" Width="305px" Visible="False" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblTrnNo" runat="server" Text="Draft No."></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:Label id="prodresmstoid" runat="server"></asp:Label> <asp:TextBox id="prodresno" runat="server" CssClass="inpTextDisabled" Width="260px" Visible="False" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left rowSpan=1><asp:Label id="Label18" runat="server" Text="From Department"></asp:Label></TD><TD class="Label" align=center rowSpan=1>:</TD><TD class="Label" align=left rowSpan=1><asp:DropDownList id="deptoid" runat="server" CssClass="inpText" Width="305px" AutoPostBack="True">
            </asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label22" runat="server" Text="Supplier/Makloon"></asp:Label>&nbsp;<asp:Label id="Label26" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="suppoid" runat="server" CssClass="inpText" Width="305px" AutoPostBack="True" OnSelectedIndexChanged="suppoid_SelectedIndexChanged"></asp:DropDownList></TD><TD class="Label" align=left rowSpan=1><asp:Label id="Label6" runat="server" Text="Result Date"></asp:Label>&nbsp;<asp:Label id="Label7" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center rowSpan=1>:</TD><TD class="Label" align=left rowSpan=1><asp:TextBox id="prodresdate" runat="server" CssClass="inpText" Width="80px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbPRDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label3" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label27" runat="server" Text="Total AP"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="approductgrandtotal" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label28" runat="server" Text="AP No."></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="approductno" runat="server" CssClass="inpTextDisabled" Width="150px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label8" runat="server" Text="Header Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="prodresmstnote" runat="server" CssClass="inpText" Width="300px" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label25" runat="server" Text="Status"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="prodresmststatus" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="approductpaytypeoid" runat="server" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label11" runat="server" Text="Replacement Dept." Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="deptreplaceoid" runat="server" CssClass="inpText" Width="305px" Visible="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:CalendarExtender id="ceDate" runat="server" TargetControlID="prodresdate" Format="MM/dd/yyyy" PopupButtonID="imbPRDate"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeDate" runat="server" TargetControlID="prodresdate" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><asp:Label id="Label17" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Production Result Detail" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="i_u2" runat="server" Text="New Detail" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="prodresdtlseq" runat="server" Visible="False">1</asp:Label> <asp:Label id="prodresdtloid" runat="server" Visible="False"></asp:Label> <asp:Label id="womstoid" runat="server" Visible="False"></asp:Label> <asp:Label id="wodtl2oid" runat="server" Visible="False" ToolTip="wodtl2oid"></asp:Label> <asp:Label id="itemrefoid" runat="server" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="prodrestype" runat="server" Visible="False"></asp:Label> <asp:Label id="prodresdlcvalue" runat="server" Visible="False"></asp:Label> <asp:Label id="prodresohdvalue" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label23" runat="server" Text="SPK No."></asp:Label> <asp:Label id="Label19" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="wono" runat="server" CssClass="inpTextDisabled" Width="280px" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSearchKIK" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="Label13" runat="server" Text="SPK Date"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="wodate" runat="server" CssClass="inpTextDisabled" Width="80px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label10" runat="server" Text="Sequence"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="prodresprocseq" runat="server" CssClass="inpTextDisabled" Width="80px" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="lbltodept" runat="server" Text="To Department"></asp:Label></TD><TD class="Label" align=center><asp:Label id="septtodept" runat="server" Text=":"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="todeptoid" runat="server" CssClass="inpTextDisabled" Width="305px" Enabled="False">
            </asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label14" runat="server" Text="Result Qty"></asp:Label> <asp:Label id="Label12" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="prodresqty" runat="server" CssClass="inpText" Width="100px" MaxLength="16"></asp:TextBox>&nbsp;- <asp:DropDownList id="prodresunitoid" runat="server" CssClass="inpTextDisabled" Width="145px" Enabled="False"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label20" runat="server" Text="SPK Qty"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="wodtl2qty" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label21" runat="server" Text="Warehouse"></asp:Label>&nbsp;<asp:Label id="Label24" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="warehouseoid" runat="server" CssClass="inpText" Width="262px"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label29" runat="server" Text="Rounding Qty"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="resultlimitqty" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label15" runat="server" Text="Detail Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="prodresdtlnote" runat="server" CssClass="inpText" Width="300px" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="suppoid2" runat="server" Visible="False"></asp:Label> <asp:Label id="suppname2" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="sisaqtyunitoid" runat="server" CssClass="inpTextDisabled" Width="145px" Visible="False" Enabled="False"></asp:DropDownList></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="resultmaxqty" runat="server" CssClass="inpTextDisabled" Width="100px" Visible="False" Enabled="False"></asp:TextBox> <asp:TextBox id="prodresqty_sisa" runat="server" CssClass="inpText" Width="100px" Visible="False" MaxLength="16"></asp:TextBox> </TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="ftbQty" runat="server" TargetControlID="prodresqty" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbSisaQty" runat="server" TargetControlID="prodresqty_sisa" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <asp:Label id="prodreskancingvalue" runat="server" Visible="False"></asp:Label> <asp:Label id="prodreskainvalue" runat="server" Visible="False"></asp:Label> <asp:Label id="prodreslabelvalue" runat="server" Visible="False"></asp:Label> <asp:Label id="prodresothervalue" runat="server" Visible="False"></asp:Label> <asp:Label id="prodresbordirvalue" runat="server" Visible="False" __designer:wfdid="w8"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:GridView id="gvDtl" runat="server" ForeColor="#333333" Width="100%" GridLines="None" CellPadding="4" AutoGenerateColumns="False" PageSize="5" DataKeyNames="prodresdtlseq">
<RowStyle BackColor="#E3EAEB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="prodresdtlseq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wono" HeaderText="SPK No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodate" HeaderText="SPK Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Code FG">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdesc" HeaderText="Finish Good">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="todeptname" HeaderText="To Department">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prodresqty" HeaderText="Result Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prodresunit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="warehouse" HeaderText="Warehouse">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prodresdtlnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Makloon" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteImageUrl="~/Images/del.jpeg" DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#666666" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#7C6F57"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=6></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton id="btnPost" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div1" class="progressBackgroundFilter"></DIV><DIV id="Div2" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Production Result :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upListKIK" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlListKIK" runat="server" CssClass="modalBox" Width="800px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListKIK" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Job Costing MO" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFindListKIK" runat="server" CssClass="Label" Width="100%" DefaultButton="btnFindListKIK">Filter : <asp:DropDownList id="FilterDDLListKIK" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="wono">SPK No.</asp:ListItem>
<asp:ListItem Value="todeptname">To Department</asp:ListItem>
    <asp:ListItem Value="suppname">Makloon</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListKIK" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListKIK" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListKIK" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListKIK" runat="server" ForeColor="#333333" Width="99%" GridLines="None" CellPadding="4" AutoGenerateColumns="False" PageSize="5" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><HeaderTemplate>
<asp:CheckBox id="cbHdrListKIK" runat="server" AutoPostBack="True" OnCheckedChanged="cbHdrListKIK_CheckedChanged"></asp:CheckBox> 
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="cbListKIK" runat="server" ToolTip='<%# eval("wodtl2oid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="wono" HeaderText="SPK No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdesc" HeaderText="Finish Good">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodtl2qty" HeaderText="SPK Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Result Qty"><ItemTemplate>
<asp:TextBox id="tbResQtyListKIK" runat="server" CssClass="inpText" Text='<%# eval("prodresqty") %>' Width="75px" MaxLength="16"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftbResQty" runat="server" TargetControlID="tbResQtyListKIK" ValidChars="1234567890,.">
                                                </ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="wodtl2unit" HeaderText="Unit">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="resultlimitqty" HeaderText="Round Qty" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
    <asp:BoundField DataField="suppname" HeaderText="Makloon">
        <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
        <ItemStyle HorizontalAlign="Left" />
    </asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                                        <asp:Label ID="Label14" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD vAlign=top align=center colSpan=3><asp:LinkButton id="lkbAddToListListKIK" runat="server">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lkbCloseListKIK" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHideListKIK" runat="server" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="mpeListKIK" runat="server" TargetControlID="btnHideListKIK" PopupDragHandleControlID="lblListKIK" PopupControlID="pnlListKIK" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2><asp:LinkButton id="lbViewDetail" runat="server">[ View Detail ]</asp:LinkButton></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption" DropShadow="True" Drag="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

