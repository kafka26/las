Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Structure VarUsageValue
    Public ValRaw_IDR As Double
    Public ValRaw_USD As Double
    Public ValGen_IDR As Double
    Public ValGen_USD As Double
    Public ValSP_IDR As Double
    Public ValSP_USD As Double
    Public ValLog_IDR As Double
    Public ValLog_USD As Double
    Public ValST_IDR As Double
    Public ValST_USD As Double
End Structure

Partial Class Transaction_MaterialUsageBySO
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "reqdtloid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("usageqty") = ToDouble(GetTextBoxValue(row.Cells(5).Controls))
                                    dtView(0)("usagedtlnote") = GetTextBoxValue(row.Cells(7).Controls)
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "reqdtloid=" & cbOid
                                dtView2.RowFilter = "reqdtloid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("usageqty") = ToDouble(GetTextBoxValue(row.Cells(5).Controls))
                                    dtView(0)("usagedtlnote") = GetTextBoxValue(row.Cells(7).Controls)
                                    If dtView2.Count > 0 Then
                                        dtView2(0)("usageqty") = ToDouble(GetTextBoxValue(row.Cells(5).Controls))
                                        dtView2(0)("usagedtlnote") = GetTextBoxValue(row.Cells(7).Controls)
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If reqmstoid.Text = "" Then
            sError &= "- Please select REQUEST NO. field!<BR>"
        End If
        If usagewhoid.SelectedValue = "" Then
            sError &= "- Please select WAREHOUSE field!<BR>"
        End If
        If reqdtloid.Text = "" Then
            sError &= "- Please select MATERIAL field!<BR>"
        End If
        If usageqty.Text = "" Then
            sError &= "- Please fill USAGE QTY field!<BR>"
        Else
            If ToDouble(usageqty.Text) <= 0 Then
                sError &= "- USAGE QTY field must be more than 0!<BR>"
            Else
                If ToDouble(usageqty.Text) > ToDouble(reqqty.Text) Then
                    sError &= "- USAGE QTY field must be less than REQUEST QTY!<BR>"
                Else
                    If ToDouble(usageqty.Text) > ToDouble(stockqty.Text) Then
                        sError &= "- USAGE QTY field must be less than STOCK QTY!<BR>"
                    End If
                End If
            End If
        End If
        If usageunitoid.SelectedValue = "" Then
            sError &= "- Please select UNIT field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If usagedate.Text = "" Then
            sErr = "None"
            sError &= "- Please fill USAGE DATE field!<BR>"
        Else
            If Not IsValidDate(usagedate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- USAGE DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If deptoid.SelectedValue = "" Then
            sError &= "- Please select DEPARTMENT field!<BR>"
        End If
        If ToInteger(matoid_mix.Text) <> 0 Then
            If qty_mix.Text = "" Then
                sError &= "- Please fill QTY MIXING field!<BR>"
            Else
                If ToDouble(qty_mix.Text) <= 0 Then
                    sError &= "- QTY MIXING must be more than 0!<BR>"
                End If
            End If
            If unitoid_mix.SelectedValue = "" Then
                sError &= "- Please select MATERIAL MIXING UNIT field!<BR>"
            End If
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill Detail Data!<BR>"
        Else
            Dim dtTbl As DataTable = Session("TblDtl")
            If dtTbl.Rows.Count <= 0 Then
                sError &= "- Please fill Detail Data!<BR>"
            Else
                If sErr = "" Then
                    For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                        Dim sRef As String = ""
                        If dtTbl.Rows(C1)("usagereftype").ToString.ToUpper = "RAW" Then
                            sRef = "RAW MATERIAL"
                        ElseIf dtTbl.Rows(C1)("usagereftype").ToString.ToUpper = "GENERAL" Then
                            sRef = "GENERAL MATERIAL"
                        ElseIf dtTbl.Rows(C1)("usagereftype").ToString.ToUpper = "SPARE PART" Then
                            sRef = "SPARE PART"
                        ElseIf dtTbl.Rows(C1)("usagereftype").ToString.ToUpper = "LOG" Then
                            sRef = "LOG"
                        ElseIf dtTbl.Rows(C1)("usagereftype").ToString.ToUpper = "SAWN TIMBER" Then
                            sRef = "PALLET"
                        End If
                        If Not IsStockAvailable(DDLBusUnit.SelectedValue, Format(GetServerTime(), "yyyyMM"), dtTbl.Rows(C1)("usagerefoid"), dtTbl.Rows(C1)("usagewhoid"), ToDouble((dtTbl.Compute("SUM(usageqty)", "usagerefoid=" & dtTbl.Rows(C1)("usagerefoid") & " AND usagereftype='" & dtTbl.Rows(C1)("usagereftype").ToString & "'")).ToString), sRef) Then
                            sError &= "- Every Usage Qty must be less than Stock Qty!<BR>"
                            Exit For
                        End If
                    Next
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            usagemststatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function GetStockValue(ByVal sOid As String, ByVal sRef As String, Optional ByVal sType As String = "IDR") As Double
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        Try
            GetStockValue = ToDouble(GetStrData("select dbo.getStockValue(" & sOid & ")"))
        Catch ex As Exception
            GetStockValue = 0
        End Try
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckUsageStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trnusagemst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND usagemststatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnUsage.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql).ToString) > 0 Then
            lkbUsageInProcess.Visible = True
            lkbUsageInProcess.Text = "You have " & GetStrData(sSql) & " In Process Process Material Usage By SO data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitAllDDL()
        ' Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)
        If DDLBusUnit.Items.Count > 0 Then
            InitDDLDept()
        End If
        ' Fill DDL Warehouse
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='MATERIAL LOCATION' AND genoid>0"
        If i_u.Text = "New Data" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
        FillDDL(usagewhoid, sSql)
        ' Fill DDL Unit
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='MATERIAL UNIT'"
        If i_u.Text = "New Data" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
        FillDDL(usageunitoid, sSql)
        ' Fill DDL Material Mixing Unit
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='MATERIAL UNIT'"
        If i_u.Text = "New Data" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
        FillDDL(unitoid_mix, sSql)
    End Sub

    Private Sub InitDDLDept()
        ' Fill DDL Department
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND deptoid IN (SELECT deptoid FROM QL_trnreqmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqmststatus='Post') ORDER BY deptname"
        FillDDL(deptoid, sSql)
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT 'False' AS checkvalue, divname, usagemstoid, usageno, CONVERT(VARCHAR(10), usagedate, 101) AS usagedate, deptname, usagemststatus, usagemstnote, mum.createuser FROM QL_trnusagemst mum INNER JOIN QL_mstdept de ON de.deptoid=mum.deptoid AND de.cmpcode=mum.cmpcode INNER JOIN QL_mstdivision div ON div.cmpcode=mum.cmpcode WHERE"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " mum.cmpcode='" & Session("CompnyCode") & "'"
        Else
            sSql &= " mum.cmpcode LIKE '%'"
        End If
        sSql &= sSqlPlus & " ORDER BY CONVERT(DATETIME, usagedate) DESC, usagemstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trnusagemst")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "usagemstoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub PrintReport()
        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("usagemstoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            report.Load(Server.MapPath(folderReport & "rptUsageBySO.rpt"))
            Dim sWhere As String = "WHERE"
            If Session("CompnyCode") <> CompnyCode Then
                sWhere &= " mum.cmpcode='" & Session("CompnyCode") & "'"
            Else
                sWhere &= " mum.cmpcode LIKE '%'"
            End If
            If sOid = "" Then
                sWhere &= " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND usagedate>=CAST('" & FilterPeriod1.Text & " 00:00:00' AS DATETIME) AND usagedate<=CAST('" & FilterPeriod2.Text & " 23:59:59' AS DATETIME)"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sWhere &= " AND usagemststatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Transaction\trnUsage.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND mum.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sWhere &= " AND mum.usagemstoid IN (" & sOid & ")"
            End If
            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            report.PrintOptions.PaperSize = PaperSize.PaperA4
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "MaterialUsageBySOPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
        End Try
    End Sub

    Private Sub BindListRequest()
        sSql = "SELECT reqmstoid, reqno, CONVERT(VARCHAR(10), reqdate, 101) AS reqdate, reqwhoid, gendesc AS reqwh, reqmstnote FROM QL_trnreqmst req INNER JOIN QL_mstgen g ON genoid=reqwhoid WHERE req.cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqmststatus='Post' AND " & FilterDDLListRequest.SelectedValue & " LIKE '%" & Tchar(FilterTextListRequest.Text) & "%' AND deptoid=" & deptoid.SelectedValue & " ORDER BY reqmstoid"
        FillGV(gvListRequest, sSql, "QL_trnreqmst")
    End Sub

    Private Sub BindMatData()
        Dim sStr As String = ""
        If Session("oid") IsNot Nothing And Session("oid") <> "" Then
            sStr = " AND mud.usagemstoid<>" & Session("oid")
        End If
        Dim sType As String = ""
        Dim sRef As String = ""
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        If usagereftype.SelectedValue.ToUpper = "RAW" Then
            sRef = "Raw Material" : sType = "matraw"
        ElseIf usagereftype.SelectedValue.ToUpper = "GENERAL" Then
            sRef = "General Material" : sType = "matgen"
        ElseIf usagereftype.SelectedValue.ToUpper = "SPARE PART" Then
            sRef = "Spare Part" : sType = "sparepart"
        End If
        lblTitleListMat.Text = "List Of " & sRef
        sSql = "SELECT 'False' AS checkvalue, reqdtlseq, reqdtloid, reqrefoid, " & sType & "code AS reqrefcode, " & sType & "longdesc AS reqreflongdesc, (reqqty - ISNULL((SELECT SUM(usageqty) FROM QL_trnusagedtl mud WHERE mud.cmpcode=reqd.cmpcode AND mud.reqdtloid=reqd.reqdtloid" & sStr & "), 0.0)) AS reqqty, requnitoid, gendesc AS requnit, (select dbo.getstockqty(reqrefoid, " & usagewhoid.SelectedValue & ")) AS stockqty, 0.0 AS usageqty, reqdtlnote AS usagedtlnote FROM QL_trnreqdtl reqd INNER JOIN QL_mst" & sType & " m ON " & sType & "oid=reqrefoid INNER JOIN QL_mstgen g ON genoid=requnitoid WHERE reqd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqmstoid=" & reqmstoid.Text & " AND reqdtlstatus='' AND reqreftype='" & usagereftype.SelectedValue & "' ORDER BY reqdtlseq"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstmat")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            If ToDouble(dt.Rows(C1)("reqqty").ToString) < ToDouble(dt.Rows(C1)("stockqty").ToString) Then
                dt.Rows(C1)("usageqty") = ToDouble(dt.Rows(C1)("reqqty").ToString)
            Else
                dt.Rows(C1)("usageqty") = ToDouble(dt.Rows(C1)("stockqty").ToString)
            End If
        Next
        dt.AcceptChanges()
        Session("TblMat") = dt
        sSql = "SELECT reqd2.reqdtloid, (reqdtl2qty - ISNULL((SELECT SUM(usagedtl2qty) FROM QL_trnusagedtl2 mud WHERE mud.cmpcode=reqd2.cmpcode AND mud.reqdtl2oid=reqd2.reqdtl2oid " & sStr & "), 0.0)) AS usagedtl2maxqty, reqdtl2unitoid AS usagedtl2unitoid, reqdtl2oid FROM QL_trnreqdtl2 reqd2 INNER JOIN QL_trnreqdtl reqd ON reqd.cmpcode=reqd2.cmpcode AND reqd.reqdtloid=reqd2.reqdtloid WHERE reqd2.cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqd2.reqmstoid=" & reqmstoid.Text & " AND reqdtlstatus='' AND reqdtl2status='' AND reqreftype='" & usagereftype.SelectedValue & "' ORDER BY reqd2.reqdtloid, reqdtl2seq"
        Dim dt2 As DataTable = cKon.ambiltabel(sSql, "QL_mstmat2")
        Session("TblMat2") = dt2
    End Sub

    Private Sub CreateTblDetail()
        If Session("TblDtl") Is Nothing Then
            Dim dtlTable As DataTable = New DataTable("QL_trnusagedtl")
            dtlTable.Columns.Add("usagedtlseq", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("reqmstoid", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("reqno", Type.GetType("System.String"))
            dtlTable.Columns.Add("usagewhoid", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("usagewh", Type.GetType("System.String"))
            dtlTable.Columns.Add("usagereftype", Type.GetType("System.String"))
            dtlTable.Columns.Add("reqdtloid", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("usagerefoid", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("usagerefcode", Type.GetType("System.String"))
            dtlTable.Columns.Add("usagereflongdesc", Type.GetType("System.String"))
            dtlTable.Columns.Add("reqqty", Type.GetType("System.Double"))
            dtlTable.Columns.Add("stockqty", Type.GetType("System.Double"))
            dtlTable.Columns.Add("usageqty", Type.GetType("System.Double"))
            dtlTable.Columns.Add("usageunitoid", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("usageunit", Type.GetType("System.String"))
            dtlTable.Columns.Add("usagedtlnote", Type.GetType("System.String"))
            dtlTable.Columns.Add("usagevalueidr", Type.GetType("System.Double"))
            dtlTable.Columns.Add("usagevalueusd", Type.GetType("System.Double"))
            Session("TblDtl") = dtlTable
        End If
        If Session("TblDtl2") Is Nothing Then
            Dim dtlTable2 As DataTable = New DataTable("QL_trnreqdtl2")
            dtlTable2.Columns.Add("reqdtloid", Type.GetType("System.Int32"))
            dtlTable2.Columns.Add("usagedtl2qty", Type.GetType("System.Double"))
            dtlTable2.Columns.Add("usagedtl2maxqty", Type.GetType("System.Double"))
            dtlTable2.Columns.Add("usagedtl2unitoid", Type.GetType("System.Int32"))
            dtlTable2.Columns.Add("reqdtl2oid", Type.GetType("System.Int32"))
            Session("TblDtl2") = dtlTable2
        End If
    End Sub

    Private Sub ClearDetail()
        usagedtlseq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                usagedtlseq.Text = objTable.Rows.Count + 1
                EnableHeader(False)
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        i_u2.Text = "New Detail"
        reqmstoid.Text = ""
        reqno.Text = ""
        usagewhoid.SelectedIndex = -1
        reqdtloid.Text = ""
        usagerefoid.Text = ""
        usagerefcode.Text = ""
        usagereflongdesc.Text = ""
        reqqty.Text = ""
        stockqty.Text = ""
        usageqty.Text = ""
        usageunitoid.SelectedIndex = -1
        usagedtlnote.Text = ""
        gvDtl.SelectedIndex = -1
        btnSearchRequest.Visible = True : btnClearRequest.Visible = True
        usagereftype.Enabled = True : usagereftype.CssClass = "inpText"
        btnSearchMat.Visible = True
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal : DDLBusUnit.CssClass = sCss
        deptoid.Enabled = bVal : deptoid.CssClass = sCss
    End Sub

    Private Sub generateUsageNo()
        Dim sNo As String = "USGSO-" & Format(GetServerTime(), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(usageno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnusagemst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND usageno LIKE '%" & sNo & "%'"
        usageno.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT cmpcode, usagemstoid, periodacctg, usagedate, usageno, deptoid, usagemstnote, usagemststatus, createuser, createtime, upduser, updtime, matoid_mix, qty_mix, unitoid_mix FROM QL_trnusagemst WHERE usagemstoid=" & sOid
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = xreader("cmpcode").ToString
                DDLBusUnit_SelectedIndexChanged(Nothing, Nothing)
                usagemstoid.Text = xreader("usagemstoid").ToString
                periodacctg.Text = xreader("periodacctg").ToString
                usagedate.Text = Format(xreader("usagedate"), "MM/dd/yyyy")
                usageno.Text = xreader("usageno").ToString
                deptoid.SelectedValue = xreader("deptoid").ToString
                usagemstnote.Text = xreader("usagemstnote").ToString
                usagemststatus.Text = xreader("usagemststatus").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
                matoid_mix.Text = xreader("matoid_mix").ToString
                If ToInteger(matoid_mix.Text) <> 0 Then
                    EnableQtyMix(True)
                Else
                    EnableQtyMix(False)
                End If
                qty_mix.Text = ToMaskEdit(ToDouble(xreader("qty_mix").ToString), 4)
                If qty_mix.Text = "0" Then
                    qty_mix.Text = ""
                End If
                unitoid_mix.SelectedValue = xreader("unitoid_mix").ToString
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            xreader.Close()
            conn.Close()
            btnSave.Visible = False : btnDelete.Visible = False : btnposting.Visible = False
            Exit Sub
        End Try
        If usagemststatus.Text <> "In Process" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnposting.Visible = False
            lblspuno.Text = "Usage No."
            usagemstoid.Visible = False
            usageno.Visible = True
            btnAddToList.Visible = False
            gvDtl.Columns(0).Visible = False
            gvDtl.Columns(gvDtl.Columns.Count - 1).Visible = False
        End If
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sSql = "SELECT DISTINCT usagedtlseq, mud.reqmstoid, reqno, usagewhoid, g1.gendesc AS usagewh, usagereftype, mud.reqdtloid, usagerefoid, (CASE usagereftype WHEN 'Raw' THEN (SELECT matrawcode FROM QL_mstmatraw WHERE matrawoid=usagerefoid) WHEN 'General' THEN (SELECT matgencode FROM QL_mstmatgen WHERE matgenoid=usagerefoid) WHEN 'Spare Part' THEN (SELECT sparepartcode FROM QL_mstsparepart WHERE sparepartoid=usagerefoid) WHEN 'Log' THEN (SELECT m.logno FROM QL_mstlog m WHERE m.logoid=mud.usagerefoid) WHEN 'Sawn Timber' THEN (SELECT m.palletno FROM QL_mstpallet m WHERE m.palletoid=mud.usagerefoid) ELSE '' END) AS usagerefcode, (CASE usagereftype WHEN 'Raw' THEN (SELECT matrawlongdesc FROM QL_mstmatraw WHERE matrawoid=usagerefoid) WHEN 'General' THEN (SELECT matgenlongdesc FROM QL_mstmatgen WHERE matgenoid=usagerefoid) WHEN 'Spare Part' THEN (SELECT sparepartlongdesc FROM QL_mstsparepart WHERE sparepartoid=usagerefoid) WHEN 'Log' THEN (SELECT m.matrawlongdesc FROM QL_mstmatraw m INNER JOIN QL_mstlog m1 ON m1.refoid = m.matrawoid WHERE m1.logoid=mud.usagerefoid ) WHEN 'Sawn Timber' THEN (SELECT m.palletlongdesc FROM QL_mstpallet m WHERE m.palletoid=mud.usagerefoid) ELSE '' END) AS usagereflongdesc, usageqty, (reqqty - (ISNULL((SELECT SUM(mudx.usageqty) FROM QL_trnusagedtl mudx WHERE mudx.cmpcode=mud.cmpcode AND mudx.reqdtloid=mud.reqdtloid AND mudx.usagemstoid<>mud.usagemstoid), 0.0))) AS reqqty, (select dbo.getstockqty(usagerefoid, usagewhoid)) AS stockqty, usageunitoid, g2.gendesc AS usageunit, usagedtlnote, 0.0000 AS usagevalueidr, 0.0000 AS usagevalueusd FROM QL_trnusagedtl mud INNER JOIN QL_trnreqmst reqm ON reqm.cmpcode=mud.cmpcode AND reqm.reqmstoid=mud.reqmstoid INNER JOIN QL_trnreqdtl reqd ON reqd.cmpcode=mud.cmpcode AND reqd.reqdtloid=mud.reqdtloid INNER JOIN QL_mstgen g1 ON g1.genoid=mud.usagewhoid INNER JOIN QL_mstgen g2 ON g2.genoid=mud.usageunitoid WHERE usagemstoid=" & sOid & " ORDER BY usagedtlseq"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnusagedtl")
        Session("TblDtl") = dt
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        ClearDetail()
        sSql = "SELECT reqd2.reqdtloid, usagedtl2qty, (reqdtl2qty - ISNULL((SELECT SUM(mudx2.usagedtl2qty) FROM QL_trnusagedtl2 mudx2 WHERE mudx2.cmpcode=mud2.cmpcode AND mudx2.reqdtl2oid=mud2.reqdtl2oid AND mudx2.usagemstoid<>mud2.usagemstoid), 0.0)) AS usagedtl2maxqty, usagedtl2unitoid, mud2.reqdtl2oid FROM QL_trnusagedtl2 mud2 INNER JOIN QL_trnreqdtl2 reqd2 ON reqd2.cmpcode=mud2.cmpcode AND reqd2.reqdtl2oid=mud2.reqdtl2oid WHERE mud2.usagemstoid=" & Session("oid") & " ORDER BY reqd2.reqdtloid, usagedtl2seq"
        Session("TblDtl2") = cKon.ambiltabel(sSql, "QL_trnusagedtl2")
    End Sub

    Private Sub SetUsageValue(ByRef objVal As VarUsageValue)
        objVal.ValRaw_IDR = 0
        objVal.ValRaw_USD = 0
        objVal.ValGen_IDR = 0
        objVal.ValGen_USD = 0
        objVal.ValSP_IDR = 0
        objVal.ValSP_USD = 0
        objVal.ValLog_IDR = 0
        objVal.ValLog_USD = 0
        objVal.ValST_IDR = 0
        objVal.ValST_USD = 0
        Dim dt As DataTable = Session("TblDtl")
        Dim dValIDR As Double = 0, dValUSD As Double = 0
        For C1 As Integer = 0 To dt.Rows.Count - 1
            If dt.Rows(C1)("usagereftype").ToString.ToUpper = "RAW" Then
                dValIDR = GetStockValue(dt.Rows(C1)("usagerefoid").ToString, "RAW MATERIAL")
                dValUSD = GetStockValue(dt.Rows(C1)("usagerefoid").ToString, "RAW MATERIAL", "USD")
                dt.Rows(C1)("usagevalueidr") = dValIDR
                dt.Rows(C1)("usagevalueusd") = dValUSD
                objVal.ValRaw_IDR += dValIDR * ToDouble(dt.Rows(C1)("usageqty").ToString)
                objVal.ValRaw_USD += dValUSD * ToDouble(dt.Rows(C1)("usageqty").ToString)
            ElseIf dt.Rows(C1)("usagereftype").ToString.ToUpper = "GENERAL" Then
                dValIDR = GetStockValue(dt.Rows(C1)("usagerefoid").ToString, "GENERAL MATERIAL")
                dValUSD = GetStockValue(dt.Rows(C1)("usagerefoid").ToString, "GENERAL MATERIAL", "USD")
                dt.Rows(C1)("usagevalueidr") = dValIDR
                dt.Rows(C1)("usagevalueusd") = dValUSD
                objVal.ValGen_IDR += dValIDR * ToDouble(dt.Rows(C1)("usageqty").ToString)
                objVal.ValGen_USD += dValUSD * ToDouble(dt.Rows(C1)("usageqty").ToString)
            ElseIf dt.Rows(C1)("usagereftype").ToString.ToUpper = "SPARE PART" Then
                dValIDR = GetStockValue(dt.Rows(C1)("usagerefoid").ToString, "SPARE PART")
                dValUSD = GetStockValue(dt.Rows(C1)("usagerefoid").ToString, "SPARE PART", "USD")
                dt.Rows(C1)("usagevalueidr") = dValIDR
                dt.Rows(C1)("usagevalueusd") = dValUSD
                objVal.ValSP_IDR += dValIDR * ToDouble(dt.Rows(C1)("usageqty").ToString)
                objVal.ValSP_USD += dValUSD * ToDouble(dt.Rows(C1)("usageqty").ToString)
            ElseIf dt.Rows(C1)("usagereftype").ToString.ToUpper = "LOG" Then
                dValIDR = GetStockValue(dt.Rows(C1)("usagerefoid").ToString, "LOG")
                dValUSD = GetStockValue(dt.Rows(C1)("usagerefoid").ToString, "LOG", "USD")
                dt.Rows(C1)("usagevalueidr") = dValIDR
                dt.Rows(C1)("usagevalueusd") = dValUSD
                objVal.ValLog_IDR += dValIDR * ToDouble(dt.Rows(C1)("usageqty").ToString)
                objVal.ValLog_USD += dValUSD * ToDouble(dt.Rows(C1)("usageqty").ToString)
            ElseIf dt.Rows(C1)("usagereftype").ToString.ToUpper = "SAWN TIMBER" Then
                dValIDR = GetStockValue(dt.Rows(C1)("usagerefoid").ToString, "PALLET")
                dValUSD = GetStockValue(dt.Rows(C1)("usagerefoid").ToString, "PALLET", "USD")
                dt.Rows(C1)("usagevalueidr") = dValIDR
                dt.Rows(C1)("usagevalueusd") = dValUSD
                objVal.ValST_IDR += dValIDR * ToDouble(dt.Rows(C1)("usageqty").ToString)
                objVal.ValST_USD += dValUSD * ToDouble(dt.Rows(C1)("usageqty").ToString)
            End If
        Next
        Session("TblDtl") = dt
    End Sub

    Private Sub EnableQtyMix(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        qty_mix.Enabled = bVal : qty_mix.CssClass = sCss
    End Sub

    Private Sub BindListMatMix()
        sSql = "SELECT * FROM (SELECT matrawoid AS matoid_mix, matrawcode AS matcode_mix, matrawlongdesc AS matlongdesc_mix, gendesc AS matunit_mix, matrawunitoid AS unitoid_mix FROM QL_mstmatraw m INNER JOIN QL_mstgen g ON g.cmpcode=m.cmpcode AND genoid=matrawunitoid WHERE m.cmpcode='" & CompnyCode & "' AND LEFT(matrawcode, 2) IN (SELECT cat1code FROM QL_mstcat1 c1 WHERE c1.cmpcode='" & CompnyCode & "' AND cat1oid IN (SELECT c0d.cat1oid FROM QL_mstcat0dtl c0d INNER JOIN QL_mstoid oi ON oi.cmpcode=c0d.cmpcode AND lastoid=cat0oid WHERE c0d.cmpcode='" & CompnyCode & "' AND tablename='SET MATERIAL MIXING'))) AS tbl_MatMix WHERE " & FilterDDLListMatMix.SelectedValue & " LIKE '%" & Tchar(FilterTextListMatMix.Text) & "%' ORDER by matcode_mix"
        FillGV(gvListMatMix, sSql, "tbl_MatMix")
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnUsage.aspx")
        End If
        If checkPagePermission("~\Transaction\trnUsage.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Material Usage By SO"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnposting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            CheckUsageStatus()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitAllDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                usagemstoid.Text = GenerateID("QL_TRNUSAGEMST", CompnyCode)
                usagedate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                usagemststatus.Text = "In Process"
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvDtl.DataSource = dt
            gvDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnUsage.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lkbUsageInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbUsageInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = 0
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, mum.updtime, GETDATE()) > " & nDays & " AND usagemststatus='In Process'"
        If checkPagePermission("~\Transaction\trnUsage.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND mum.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND usagedate>=CAST('" & FilterPeriod1.Text & " 00:00:00' AS DATETIME) AND usagedate<=CAST('" & CDate(FilterPeriod2.Text) & " 23:59:59' AS DATETIME)"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND usagemststatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Transaction\trnUsage.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND mum.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        cbStatus.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Transaction\trnUsage.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND mum.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        UpdateCheckedValue()
        PrintReport()
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLDept()
    End Sub

    Protected Sub btnSearchRequest_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchRequest.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        If deptoid.SelectedValue = "" Then
            showMessage("Please select Department first!", 2)
            Exit Sub
        End If
        FilterDDLListRequest.SelectedIndex = -1 : FilterTextListRequest.Text = "" : gvListRequest.SelectedIndex = -1
        BindListRequest()
        cProc.SetModalPopUpExtender(btnHideListRequest, pnlListRequest, mpeListRequest, True)
    End Sub

    Protected Sub btnClearRequest_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearRequest.Click
        reqmstoid.Text = "" : reqno.Text = "" : usagewhoid.SelectedIndex = -1
    End Sub

    Protected Sub btnFindListRequest_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListRequest.Click
        BindListRequest()
        mpeListRequest.Show()
    End Sub

    Protected Sub btnAllListRequest_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListRequest.Click
        FilterDDLListRequest.SelectedIndex = -1 : FilterTextListRequest.Text = "" : gvListRequest.SelectedIndex = -1
        BindListRequest()
        mpeListRequest.Show()
    End Sub

    Protected Sub gvListRequest_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListRequest.PageIndexChanging
        gvListRequest.PageIndex = e.NewPageIndex
        BindListRequest()
        mpeListRequest.Show()
    End Sub

    Protected Sub gvListRequest_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListRequest.SelectedIndexChanged
        If reqmstoid.Text <> gvListRequest.SelectedDataKey.Item("reqmstoid").ToString Then
            btnClearRequest_Click(Nothing, Nothing)
        End If
        reqmstoid.Text = gvListRequest.SelectedDataKey.Item("reqmstoid").ToString
        reqno.Text = gvListRequest.SelectedDataKey.Item("reqno").ToString
        usagewhoid.SelectedValue = gvListRequest.SelectedDataKey.Item("reqwhoid").ToString
        cProc.SetModalPopUpExtender(btnHideListRequest, pnlListRequest, mpeListRequest, False)
    End Sub

    Protected Sub lbCloseListRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListRequest.Click
        cProc.SetModalPopUpExtender(btnHideListRequest, pnlListRequest, mpeListRequest, False)
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        If reqmstoid.Text = "" Then
            showMessage("Please select Request No. first!", 2)
            Exit Sub
        End If
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        Session("TblMat") = Nothing : Session("TblMatView") = Nothing : gvListMat.DataSource = Nothing : gvListMat.DataBind() : tbData.Text = "5" : gvListMat.PageSize = CInt(tbData.Text) : Session("TblMat2") = Nothing
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblMat") Is Nothing Then
            BindMatData()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If Session("TblMat") Is Nothing Then
            BindMatData()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "reqdtloid=" & dtTbl.Rows(C1)("reqdtloid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "reqdtloid=" & dtTbl.Rows(C1)("reqdtloid")
                    objView(0)("checkvalue") = "False"
                    objView(0)("usageqty") = 0
                    objView(0)("usagedtlnote") = ""
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    dtTbl.Rows(C1)("usageqty") = 0
                    dtTbl.Rows(C1)("usagedtlnote") = ""
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Selected material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub lbShowData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbShowData.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Please find material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If tbData.Text = "" Then
            Session("WarningListMat") = "Please fill total data to be shown!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        Else
            If ToDouble(tbData.Text) <= 0 Then
                Session("WarningListMat") = "Total data to be shown must be more than 0!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat2() Then
            gvListMat.PageSize = Int(tbData.Text)
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(5).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 4)
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtTbl2 As DataTable = Session("TblMat2")
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    dtView.RowFilter = ""
                    dtView.RowFilter = "checkvalue='True' AND usageqty>0"
                    If dtView.Count <> iCheck Then
                        Session("WarningListMat") = "Usage Qty for every checked material data must be more than 0!"
                        showMessage(Session("WarningListMat"), 2)
                        dtView.RowFilter = ""
                        Exit Sub
                    End If
                    dtView.RowFilter = ""
                    dtView.RowFilter = "CheckValue='True' AND usageqty<=reqqty"
                    If dtView.Count <> iCheck Then
                        Session("WarningListMat") = "Usage Qty for every checked material data must be less than Req. Qty!"
                        showMessage(Session("WarningListMat"), 2)
                        dtView.RowFilter = ""
                        Exit Sub
                    End If
                    dtView.RowFilter = ""
                    dtView.RowFilter = "CheckValue='True' AND usageqty<=stockqty"
                    If dtView.Count <> iCheck Then
                        Session("WarningListMat") = "Usage Qty for every checked material data must be less than Stock Qty!"
                        showMessage(Session("WarningListMat"), 2)
                        dtView.RowFilter = ""
                        Exit Sub
                    End If
                    If Session("TblDtl") Is Nothing Or Session("TblDtl2") Is Nothing Then
                        CreateTblDetail()
                    End If
                    Dim objTable As DataTable
                    objTable = Session("TblDtl")
                    Dim objTable2 As DataTable
                    objTable2 = Session("TblDtl2")
                    Dim dv As DataView = objTable.DefaultView
                    Dim dv2 As DataView = objTable2.DefaultView
                    Dim objRow As DataRow
                    Dim counter As Integer = objTable.Rows.Count
                    For C1 As Integer = 0 To dtView.Count - 1
                        Dim dMaxQty As Double = 0, dUsageQty As Double = 0
                        dv.RowFilter = "reqdtloid=" & dtView(C1)("reqdtloid")
                        dUsageQty = ToDouble(dtView(C1)("usageqty").ToString)
                        dMaxQty = ToDouble(dtView(C1)("reqqty").ToString)
                        If dv.Count > 0 Then
                            dv.AllowEdit = True
                            dv(0)("usageqty") = dUsageQty
                            dv(0)("reqqty") = dMaxQty
                            dv(0)("usagedtlnote") = dtView(C1)("usagedtlnote")
                        Else
                            counter += 1
                            objRow = objTable.NewRow()
                            objRow("usagedtlseq") = counter
                            objRow("reqmstoid") = reqmstoid.Text
                            objRow("reqno") = reqno.Text
                            objRow("usagewhoid") = usagewhoid.SelectedValue
                            objRow("usagewh") = usagewhoid.SelectedItem.Text
                            objRow("usagereftype") = usagereftype.SelectedValue
                            objRow("reqdtloid") = dtView(C1)("reqdtloid")
                            objRow("usagerefoid") = dtView(C1)("reqrefoid")
                            objRow("usagerefcode") = dtView(C1)("reqrefcode")
                            objRow("usagereflongdesc") = dtView(C1)("reqreflongdesc")
                            objRow("reqqty") = dMaxQty
                            objRow("stockqty") = ToDouble(dtView(C1)("stockqty"))
                            objRow("usageqty") = dUsageQty
                            objRow("usageunitoid") = dtView(C1)("requnitoid")
                            objRow("usageunit") = dtView(C1)("requnit")
                            objRow("usagedtlnote") = dtView(C1)("usagedtlnote")
                            objTable.Rows.Add(objRow)
                        End If
                        dv.RowFilter = ""
                        dtView2.RowFilter = "reqdtloid=" & dtView(C1)("reqdtloid")
                        Dim dTotalUsage As Double = 0
                        For C2 As Integer = 0 To dtView2.Count - 1
                            Dim dUsageQty2 As Double = Math.Round((ToDouble(dtView2(C2)("usagedtl2maxqty").ToString) / dMaxQty) * dUsageQty, 4)
                            dTotalUsage += Math.Round(dUsageQty2, 4)
                            If C2 = dtView2.Count - 1 Then
                                If dUsageQty <> dTotalUsage Then
                                    dUsageQty2 += (dUsageQty - dTotalUsage)
                                End If
                            End If
                            dv2.RowFilter = "reqdtl2oid=" & dtView2(C2)("reqdtl2oid")
                            If dv2.Count > 0 Then
                                dv2.AllowEdit = True
                                dv2(0)("usagedtl2qty") = dUsageQty2
                                dv2(0)("usagedtl2maxqty") = ToDouble(dtView2(C2)("usagedtl2maxqty").ToString)
                            Else
                                objRow = objTable2.NewRow()
                                objRow("reqdtloid") = dtView2(C2)("reqdtloid")
                                objRow("usagedtl2qty") = dUsageQty2
                                objRow("usagedtl2maxqty") = ToDouble(dtView2(C2)("usagedtl2maxqty").ToString)
                                objRow("usagedtl2unitoid") = dtView2(C2)("usagedtl2unitoid")
                                objRow("reqdtl2oid") = dtView2(C2)("reqdtl2oid")
                                objTable2.Rows.Add(objRow)
                            End If
                            dv2.RowFilter = ""
                        Next
                        dtView2.RowFilter = ""
                    Next
                    dtView.RowFilter = ""
                    Session("TblDtl") = objTable
                    Session("TblDtl2") = objTable2
                    gvDtl.DataSource = objTable
                    gvDtl.DataBind()
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                    ClearDetail()
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            If Session("TblDtl") Is Nothing Then
                CreateTblDetail()
            End If
            Dim objTable As DataTable = Session("TblDtl")
            Dim objTable2 As DataTable = Session("TblDtl2")
            Dim objView As DataView = objTable2.DefaultView
            Dim dv As DataView = objTable.DefaultView
            If i_u2.Text = "New Detail" Then
                dv.RowFilter = "reqdtloid=" & reqdtloid.Text
            Else
                dv.RowFilter = "reqdtloid=" & reqdtloid.Text & " AND usagedtlseq<>" & usagedtlseq.Text
            End If
            If dv.Count > 0 Then
                showMessage("This Data has been added before, please check!", 2)
                dv.RowFilter = ""
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                objRow("usagedtlseq") = objTable.Rows.Count + 1
            Else
                objRow = objTable.Rows(usagedtlseq.Text - 1)
                objRow.BeginEdit()
            End If
            objRow("reqmstoid") = reqmstoid.Text
            objRow("reqno") = reqno.Text
            objRow("usagewhoid") = usagewhoid.SelectedValue
            objRow("usagewh") = usagewhoid.SelectedItem.Text
            objRow("usagereftype") = usagereftype.SelectedValue
            objRow("reqdtloid") = reqdtloid.Text
            objRow("usagerefoid") = usagerefoid.Text
            objRow("usagerefcode") = usagerefcode.Text
            objRow("usagereflongdesc") = usagereflongdesc.Text
            objRow("reqqty") = ToDouble(reqqty.Text)
            objRow("stockqty") = ToDouble(stockqty.Text)
            objRow("usageqty") = ToDouble(usageqty.Text)
            objRow("usageunitoid") = usageunitoid.SelectedValue
            objRow("usageunit") = usageunitoid.SelectedItem.Text
            objRow("usagedtlnote") = usagedtlnote.Text
            If i_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            objView.RowFilter = "reqdtloid=" & reqdtloid.Text
            Dim dTotalUsage As Double = 0
            If objView.Count > 0 Then
                For C1 As Integer = 0 To objView.Count - 1
                    Dim dUsage As Double = Math.Round((ToDouble(objView(C1)("usagedtl2maxqty").ToString) / ToDouble(reqqty.Text)) * ToDouble(usageqty.Text), 4)
                    dTotalUsage += Math.Round(dUsage, 4)
                    If C1 = objView.Count - 1 Then
                        If ToDouble(reqqty.Text) <> dTotalUsage Then
                            dUsage += (ToDouble(reqqty.Text) - dTotalUsage)
                        End If
                    End If
                    objView(C1)("usagedtl2qty") = dUsage
                Next
            End If
            objView.RowFilter = ""
            objTable.AcceptChanges()
            Session("TblDtl") = objTable
            objTable2.AcceptChanges()
            Session("TblDtl2") = objTable2
            gvDtl.DataSource = Session("TblDtl")
            gvDtl.DataBind()
            ClearDetail()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
        End If
    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDtl.SelectedIndexChanged
        Try
            usagedtlseq.Text = gvDtl.SelectedDataKey.Item("usagedtlseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "usagedtlseq=" & usagedtlseq.Text
                reqmstoid.Text = dv.Item(0).Item("reqmstoid").ToString
                reqno.Text = dv.Item(0).Item("reqno").ToString
                usagewhoid.SelectedValue = dv.Item(0).Item("usagewhoid").ToString
                usagereftype.SelectedValue = dv.Item(0).Item("usagereftype").ToString
                reqdtloid.Text = dv.Item(0).Item("reqdtloid").ToString
                usagerefoid.Text = dv.Item(0).Item("usagerefoid").ToString
                usagerefcode.Text = dv.Item(0).Item("usagerefcode").ToString
                usagereflongdesc.Text = dv.Item(0).Item("usagereflongdesc").ToString
                reqqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("reqqty").ToString), 4)
                stockqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("stockqty").ToString), 4)
                usageqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("usageqty").ToString), 4)
                usageunitoid.SelectedValue = dv.Item(0).Item("usageunitoid").ToString
                usagedtlnote.Text = dv.Item(0).Item("usagedtlnote").ToString
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        Finally
            btnSearchRequest.Visible = False : btnClearRequest.Visible = False
            usagereftype.Enabled = False : usagereftype.CssClass = "inpTextDisabled"
            btnSearchMat.Visible = False
        End Try
    End Sub

    Protected Sub gvDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objTable2 As DataTable = Session("TblDtl2")
        Dim objView As DataView = objTable2.DefaultView
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        Dim sOid As String = objRow(e.RowIndex)("reqdtloid").ToString
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("usagedtlseq") = C1 + 1
            dr.EndEdit()
        Next
        objView.RowFilter = "reqdtloid=" & sOid
        If objView.Count > 0 Then
            For C1 As Integer = 0 To objView.Count - 1
                objView.Delete(0)
            Next
        End If
        objView.RowFilter = ""
        objTable.AcceptChanges()
        Session("TblDtl") = objTable
        objTable2.AcceptChanges()
        Session("TblDtl2") = objTable2
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        ClearDetail()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_trnusagemst WHERE usagemstoid=" & usagemstoid.Text
                If CheckDataExists(sSql) Then
                    usagemstoid.Text = GenerateID("QL_TRNUSAGEMST", CompnyCode)
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNUSAGEMST", "usagemstoid", usagemstoid.Text, "usagemststatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    usagemststatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            usagedtloid.Text = GenerateID("QL_TRNUSAGEDTL", CompnyCode)
            Dim usagedtl2oid As Integer = GenerateID("QL_TRNUSAGEDTL2", CompnyCode)
            Dim conmtroid As Integer = 0, crdmatoid As Integer = 0, glmstoid As Integer = 0, gldtloid As Integer = 0, conmtroid_mix As Integer = 0, crdmatoid_mix As Integer = 0
            Dim iUsageAcctgOid As Integer = 0, iStockRawAcctgOid As Integer = 0, iStockGenAcctgOid As Integer = 0
            Dim iStockSPAcctgOid As Integer = 0, iStockLogAcctgOid As Integer = 0, iStockSTAcctgOid As Integer = 0
            Dim objValue As VarUsageValue
            Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
            Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
            periodacctg.Text = GetDateToPeriodAcctg(CDate(usagedate.Text))
            Dim iGroupOid As Integer = 0
            If usagemststatus.Text = "Post" Then
                If isPeriodClosed(DDLBusUnit.SelectedValue, sPeriod) Then
                    showMessage("Cannot continue this transaction anymore because the period : " & MonthName(CInt(sPeriod.Substring(4, 2))) & " " & sPeriod.Substring(0, 4) & " have been closed.", 3)
                    Exit Sub
                End If
                conmtroid = GenerateID("QL_CONMAT", CompnyCode) : crdmatoid = GenerateID("QL_CRDMTR", CompnyCode)
                glmstoid = GenerateID("QL_TRNGLMST", CompnyCode) : gldtloid = GenerateID("QL_TRNGLDTL", CompnyCode)
                conmtroid_mix = GenerateID("QL_CONMAT_MIX", CompnyCode) : crdmatoid_mix = GenerateID("QL_CRDMTR_MIX", CompnyCode)
                Dim sVarErr As String = ""
                If Not IsInterfaceExists("VAR_STOCK_WIP", DDLBusUnit.SelectedValue) Then
                    sVarErr = "VAR_STOCK_WIP"
                End If
                If Not IsInterfaceExists("VAR_STOCK_RM", DDLBusUnit.SelectedValue) Then
                    sVarErr &= IIf(sVarErr = "", "", ", ") & "VAR_STOCK_RM"
                End If
                If Not IsInterfaceExists("VAR_STOCK_GM", DDLBusUnit.SelectedValue) Then
                    sVarErr &= IIf(sVarErr = "", "", ", ") & "VAR_STOCK_GM"
                End If
                If Not IsInterfaceExists("VAR_STOCK_SP", DDLBusUnit.SelectedValue) Then
                    sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_STOCK_SP"
                End If
                If Not IsInterfaceExists("VAR_STOCK_LOG", DDLBusUnit.SelectedValue) Then
                    sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_STOCK_LOG"
                End If
                If Not IsInterfaceExists("VAR_STOCK_ST", DDLBusUnit.SelectedValue) Then
                    sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_STOCK_ST"
                End If
                If sVarErr <> "" Then
                    showMessage(GetInterfaceWarning(sVarErr, "posting"), 2)
                    usagemststatus.Text = "In Process"
                    Exit Sub
                End If
                iUsageAcctgOid = GetAcctgOID(GetVarInterface("VAR_STOCK_WIP", DDLBusUnit.SelectedValue), CompnyCode)
                iStockRawAcctgOid = GetAcctgOID(GetVarInterface("VAR_STOCK_RM", DDLBusUnit.SelectedValue), CompnyCode)
                iStockGenAcctgOid = GetAcctgOID(GetVarInterface("VAR_STOCK_GM", DDLBusUnit.SelectedValue), CompnyCode)
                iStockSPAcctgOid = GetAcctgOID(GetVarInterface("VAR_STOCK_SP", DDLBusUnit.SelectedValue), CompnyCode)
                iStockLogAcctgOid = GetAcctgOID(GetVarInterface("VAR_STOCK_LOG", DDLBusUnit.SelectedValue), CompnyCode)
                iStockSTAcctgOid = GetAcctgOID(GetVarInterface("VAR_STOCK_ST", DDLBusUnit.SelectedValue), CompnyCode)
                SetUsageValue(objValue)
                generateUsageNo()
                iGroupOid = ToInteger(GetStrData("SELECT TOP 1 groupoid FROM QL_mstdeptgroupdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND deptoid=" & deptoid.SelectedValue & " ORDER BY updtime DESC"))
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trnusagemst (cmpcode, usagemstoid, periodacctg, usagedate, usageno, deptoid, usagemstnote, usagemststatus, createuser, createtime, upduser, updtime, matoid_mix, qty_mix, unitoid_mix) VALUES ('" & DDLBusUnit.SelectedValue & "', " & usagemstoid.Text & ", '" & periodacctg.Text & "', '" & usagedate.Text & "', '" & usageno.Text & "', " & deptoid.SelectedValue & ", '" & Tchar(usagemstnote.Text) & "', '" & usagemststatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToInteger(matoid_mix.Text) & ", " & ToDouble(qty_mix.Text) & ", " & unitoid_mix.SelectedValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & usagemstoid.Text & " WHERE tablename='QL_TRNUSAGEMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trnusagemst SET periodacctg='" & periodacctg.Text & "', usagedate='" & usagedate.Text & "', usageno='" & usageno.Text & "', deptoid=" & deptoid.SelectedValue & ", usagemstnote='" & Tchar(usagemstnote.Text) & "', usagemststatus='" & usagemststatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, matoid_mix=" & ToInteger(matoid_mix.Text) & ", qty_mix=" & ToDouble(qty_mix.Text) & ", unitoid_mix=" & unitoid_mix.SelectedValue & " WHERE usagemstoid=" & usagemstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnreqdtl SET reqdtlstatus='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqdtloid IN (SELECT reqdtloid FROM QL_trnusagedtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagemstoid=" & usagemstoid.Text & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnreqmst SET reqmststatus='Post' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqmstoid IN (SELECT reqmstoid FROM QL_trnusagedtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagemstoid=" & usagemstoid.Text & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trnusagedtl2 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagemstoid=" & usagemstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trnusagedtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagemstoid=" & usagemstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    Dim objTable2 As DataTable = Session("TblDtl2")
                    Dim objView As DataView = objTable2.DefaultView
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trnusagedtl (cmpcode, usagedtloid, usagemstoid, usagedtlseq, reqmstoid, reqdtloid, usagereftype, usagerefoid, usageqty, usageunitoid, usagewhoid, usagedtlnote, usagedtlstatus, upduser, updtime, usagevalueidr, usagevalueusd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C1 + CInt(usagedtloid.Text)) & ", " & usagemstoid.Text & ", " & C1 + 1 & ", " & objTable.Rows(C1).Item("reqmstoid") & ", " & objTable.Rows(C1).Item("reqdtloid") & ", '" & objTable.Rows(C1).Item("usagereftype").ToString & "', " & objTable.Rows(C1).Item("usagerefoid") & ", " & ToDouble(objTable.Rows(C1).Item("usageqty").ToString) & ", " & objTable.Rows(C1).Item("usageunitoid") & ", " & objTable.Rows(C1).Item("usagewhoid") & ", '" & Tchar(objTable.Rows(C1).Item("usagedtlnote")) & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(C1).Item("usagevalueidr").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("usagevalueusd").ToString) & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        If objTable.Rows(C1).Item("usageqty") >= objTable.Rows(C1).Item("reqqty") Then
                            sSql = "UPDATE QL_trnreqdtl SET reqdtlstatus='Complete' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqdtloid=" & objTable.Rows(C1).Item("reqdtloid")
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_trnreqmst SET reqmststatus='Closed' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqmstoid=" & objTable.Rows(C1).Item("reqmstoid") & " AND (SELECT COUNT(*) FROM QL_trnreqdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqmstoid=" & objTable.Rows(C1).Item("reqmstoid") & " AND reqdtloid<>" & objTable.Rows(C1).Item("reqdtloid") & " AND reqdtlstatus='')=0"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                        objView.RowFilter = "reqdtloid=" & objTable.Rows(C1)("reqdtloid")
                        If objView.Count > 0 Then
                            For C2 As Integer = 0 To objView.Count - 1
                                sSql = "INSERT INTO QL_trnusagedtl2 (cmpcode, usagedtl2oid, usagedtloid, usagemstoid, usagedtl2seq, reqdtl2oid, usagedtl2qty, usagedtl2unitoid, usagedtl2note, usagedtl2status, upduser, updtime) VALUES ('" & DDLBusUnit.SelectedValue & "', " & usagedtl2oid & ", " & (C1 + CInt(usagedtloid.Text)) & ", " & usagemstoid.Text & ", " & C2 + 1 & ", " & objView(C2)("reqdtl2oid") & ", " & ToDouble(objView(C2)("usagedtl2qty").ToString) & ", " & objView(C2)("usagedtl2unitoid") & ", '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                usagedtl2oid += 1
                                If ToDouble(objView(C2)("usagedtl2qty").ToString) >= ToDouble(objView(C2)("usagedtl2maxqty").ToString) Then
                                    sSql = "UPDATE QL_trnreqdtl2 SET reqdtl2status='Complete' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqdtl2oid=" & objView(C2)("reqdtl2oid")
                                    xCmd.CommandText = sSql
                                    xCmd.ExecuteNonQuery()
                                End If
                            Next
                        End If
                        objView.RowFilter = ""
                        If usagemststatus.Text = "Post" Then
                            Dim sRef As String = ""
                            If objTable.Rows(C1)("usagereftype").ToString.ToUpper = "RAW" Then
                                sRef = "Raw Material"
                            ElseIf objTable.Rows(C1)("usagereftype").ToString.ToUpper = "GENERAL" Then
                                sRef = "General Material"
                            ElseIf objTable.Rows(C1)("usagereftype").ToString.ToUpper = "SPARE PART" Then
                                sRef = "Spare Part"
                            ElseIf objTable.Rows(C1)("usagereftype").ToString.ToUpper = "LOG" Then
                                sRef = "LOG"
                            ElseIf objTable.Rows(C1)("usagereftype").ToString.ToUpper = "SAWN TIMBER" Then
                                sRef = "PALLET"
                            End If
                            ' Insert QL_conmat
                            sSql = "INSERT INTO QL_conmat (cmpcode, conmtroid, type, typemin, trndate, periodacctg, formaction, formoid, refoid, refname, mtrwhoid, qtyin, qtyout, reason, note, upduser, updtime, valueidr, valueusd, deptoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & conmtroid & ", 'MUSO', -1, '" & sDate & "', '" & sPeriod & "', 'QL_trnusagedtl', " & usagemstoid.Text & ", " & objTable.Rows(C1).Item("usagerefoid") & ", '" & sRef & "', " & objTable.Rows(C1).Item("usagewhoid") & ", 0, " & ToDouble(objTable.Rows(C1).Item("usageqty")) & ", 'Material Usage By SO', '" & usageno.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(C1).Item("usagevalueidr").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("usagevalueusd").ToString) & ", " & deptoid.SelectedValue & ")"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            conmtroid += 1
                            ' Insert QL_crdmtr
                            sSql = "UPDATE QL_crdmtr SET qtyout=qtyout + " & ToDouble(objTable.Rows(C1).Item("usageqty")) & ", saldoakhir=saldoakhir - " & ToDouble(objTable.Rows(C1).Item("usageqty")) & ", lasttranstype='QL_trnusagedtl', lasttransdate='" & sDate & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND refoid=" & objTable.Rows(C1).Item("usagerefoid") & " AND refname='" & sRef & "' AND mtrwhoid=" & objTable.Rows(C1).Item("usagewhoid") & " AND periodacctg='" & sPeriod & "'"
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() = 0 Then
                                sSql = "INSERT INTO QL_crdmtr (cmpcode, crdmatoid, periodacctg, refoid, refname, mtrwhoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir, lasttranstype, lasttransdate, upduser, updtime, createuser, createdate, closeuser, closingdate) VALUES ('" & DDLBusUnit.SelectedValue & "', " & crdmatoid & ", '" & sPeriod & "', " & objTable.Rows(C1).Item("usagerefoid") & ", '" & sRef & "', " & objTable.Rows(C1).Item("usagewhoid") & ", 0, " & ToDouble(objTable.Rows(C1).Item("usageqty")) & ", 0, 0, 0, " & -ToDouble(objTable.Rows(C1).Item("usageqty")) & ", 'QL_trnusagedtl', '" & sDate & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', '1/1/1900')"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                crdmatoid += 1
                            End If
                        End If
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(usagedtloid.Text)) & " WHERE tablename='QL_TRNUSAGEDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & usagedtl2oid - 1 & " WHERE tablename='QL_TRNUSAGEDTL2' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    If usagemststatus.Text = "Post" Then
                        If matoid_mix.Text <> "" Then
                            If ToInteger(matoid_mix.Text) <> 0 Then
                                ' Insert History Stock Mixing
                                sSql = "INSERT INTO QL_conmat_mix (cmpcode, conmtroid, typemin, trndate, periodacctg, formaction, formoid, refoid, deptoid, qtyin, qtyout, unitoid, reason, note, upduser, updtime) VALUES ('" & DDLBusUnit.SelectedValue & "', " & conmtroid_mix & ", 1, '" & sDate & "', '" & sPeriod & "', 'QL_trnusagemst', " & usagemstoid.Text & ", " & ToInteger(matoid_mix.Text) & ", " & deptoid.SelectedValue & ", " & ToDouble(qty_mix.Text) & ", 0, " & unitoid_mix.SelectedValue & ", 'Material Usage By SO', '" & usageno.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                conmtroid_mix += 1
                                ' Insert Stock Mixing
                                sSql = "UPDATE QL_crdmtr_mix SET qtyin=qtyin + " & ToDouble(qty_mix.Text) & ", saldoakhir=saldoakhir + " & ToDouble(qty_mix.Text) & ", lasttranstype='QL_trnusagemst', lasttransdate='" & sDate & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND refoid=" & ToInteger(matoid_mix.Text) & " AND periodacctg='" & sPeriod & "' AND unitoid=" & unitoid_mix.SelectedValue & " AND deptoid=" & deptoid.SelectedValue & ""
                                xCmd.CommandText = sSql
                                If xCmd.ExecuteNonQuery() = 0 Then
                                    sSql = "INSERT INTO QL_crdmtr_mix (cmpcode, crdmatoid, periodacctg, refoid, deptoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir, unitoid, lasttranstype, lasttransdate, upduser, updtime, createuser, createdate, closeuser, closingdate) VALUES ('" & DDLBusUnit.SelectedValue & "', " & crdmatoid_mix & ", '" & sPeriod & "', " & ToInteger(matoid_mix.Text) & ", " & deptoid.SelectedValue & ", " & ToDouble(qty_mix.Text) & ", 0, 0, 0, 0, " & ToDouble(qty_mix.Text) & ", " & unitoid_mix.SelectedValue & ", 'QL_trnusagemst', '" & sDate & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', '1/1/1900')"
                                    xCmd.CommandText = sSql
                                    xCmd.ExecuteNonQuery()
                                    crdmatoid_mix += 1
                                End If
                                sSql = "UPDATE QL_mstoid SET lastoid=" & conmtroid_mix - 1 & " WHERE tablename='QL_CONMAT_MIX' AND cmpcode='" & CompnyCode & "'"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                sSql = "UPDATE QL_mstoid SET lastoid=" & crdmatoid_mix - 1 & " WHERE tablename='QL_CRDMTR_MIX' AND cmpcode='" & CompnyCode & "'"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                            End If
                        End If
                        Dim dTotalIDR As Double = objValue.ValRaw_IDR + objValue.ValGen_IDR + objValue.ValSP_IDR + objValue.ValLog_IDR + objValue.ValST_IDR
                        Dim dTotalUSD As Double = objValue.ValRaw_USD + objValue.ValGen_USD + objValue.ValSP_USD + objValue.ValLog_USD + objValue.ValST_USD
                        If dTotalIDR > 0 And dTotalUSD > 0 Then
                            ' Insert GL MST
                            sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & glmstoid & ", '" & sDate & "', '" & sPeriod & "', 'MU By SO|No. " & usageno.Text & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 1, 1, 1, 1, " & dTotalUSD / dTotalIDR & ", " & dTotalUSD / dTotalIDR & ")"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            ' Insert GL DTL
                            Dim iSeq As Integer = 1
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, groupoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & iUsageAcctgOid & ", 'D', " & dTotalIDR & ", '" & usageno.Text & "', 'MU By SO|No. " & usageno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalIDR & ", " & dTotalUSD & ", 'QL_trnusagemst " & usagemstoid.Text & "', " & iGroupOid & ")"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iSeq += 1
                            gldtloid += 1
                            If objValue.ValRaw_IDR > 0 And objValue.ValRaw_USD > 0 Then
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, groupoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & iStockRawAcctgOid & ", 'C', " & objValue.ValRaw_IDR & ", '" & usageno.Text & "', 'MU By SO|No. " & usageno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & objValue.ValRaw_IDR & ", " & objValue.ValRaw_USD & ", 'QL_trnusagemst " & usagemstoid.Text & "', " & iGroupOid & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iSeq += 1
                                gldtloid += 1
                            End If
                            If objValue.ValGen_IDR > 0 And objValue.ValGen_USD > 0 Then
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, groupoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & iStockGenAcctgOid & ", 'C', " & objValue.ValGen_IDR & ", '" & usageno.Text & "', 'MU By SO|No. " & usageno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & objValue.ValGen_IDR & ", " & objValue.ValGen_USD & ", 'QL_trnusagemst " & usagemstoid.Text & "', " & iGroupOid & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iSeq += 1
                                gldtloid += 1
                            End If
                            If objValue.ValSP_IDR > 0 And objValue.ValSP_USD > 0 Then
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, groupoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & iStockSPAcctgOid & ", 'C', " & objValue.ValSP_IDR & ", '" & usageno.Text & "', 'MU By SO|No. " & usageno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & objValue.ValSP_IDR & ", " & objValue.ValSP_USD & ", 'QL_trnusagemst " & usagemstoid.Text & "', " & iGroupOid & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iSeq += 1
                                gldtloid += 1
                            End If
                            If objValue.ValLog_IDR > 0 And objValue.ValLog_USD > 0 Then
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, groupoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & iStockLogAcctgOid & ", 'C', " & objValue.ValLog_IDR & ", '" & usageno.Text & "', 'MU By SO|No. " & usageno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & objValue.ValLog_IDR & ", " & objValue.ValLog_USD & ", 'QL_trnusagemst " & usagemstoid.Text & "', " & iGroupOid & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iSeq += 1
                                gldtloid += 1
                            End If
                            If objValue.ValST_IDR > 0 And objValue.ValST_USD > 0 Then
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, groupoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & iStockSTAcctgOid & ", 'C', " & objValue.ValST_IDR & ", '" & usageno.Text & "', 'MU By SO|No. " & usageno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & objValue.ValST_IDR & ", " & objValue.ValST_USD & ", 'QL_trnusagemst " & usagemstoid.Text & "', " & iGroupOid & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iSeq += 1
                                gldtloid += 1
                            End If
                            sSql = "UPDATE QL_mstoid SET lastoid=" & glmstoid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_mstoid SET lastoid=" & gldtloid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                        sSql = "UPDATE QL_mstoid SET lastoid=" & conmtroid - 1 & " WHERE tablename='QL_CONMAT' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        sSql = "UPDATE QL_mstoid SET lastoid=" & crdmatoid - 1 & " WHERE tablename='QL_CRDMTR' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                End If
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        usagemststatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    usagemststatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 2)
                conn.Close()
                usagemststatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & usagemstoid.Text & ".<BR>"
            End If
            If usagemststatus.Text = "Post" Then
                Session("SavedInfo") &= "Data have been posted with Usage No. = " & usageno.Text & "."
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trnUsage.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnUsage.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If usagemstoid.Text = "" Then
            showMessage("Please select Material Usage By SO data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNUSAGEMST", "usagemstoid", usagemstoid.Text, "usagemststatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                usagemststatus.Text = "In Process"
                Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trnreqdtl2 SET reqdtl2status='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqdtl2oid IN (SELECT DISTINCT reqdtl2oid FROM QL_trnusagedtl2 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagemstoid=" & usagemstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_trnreqdtl SET reqdtlstatus='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqdtloid IN (SELECT DISTINCT reqdtloid FROM QL_trnusagedtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagemstoid=" & usagemstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_trnreqmst SET reqmststatus='Post' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqmstoid IN (SELECT DISTINCT reqmstoid FROM QL_trnusagedtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagemstoid=" & usagemstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnusagedtl2 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagemstoid=" & usagemstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnusagedtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagemstoid=" & usagemstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnusagemst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagemstoid=" & usagemstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnUsage.aspx?awal=true")
    End Sub

    Protected Sub btnposting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnposting.Click
        usagemststatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnSearchMatMix_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMatMix.Click
        FilterDDLListMatMix.SelectedIndex = -1 : FilterTextListMatMix.Text = "" : gvListMatMix.SelectedIndex = -1
        BindListMatMix()
        cProc.SetModalPopUpExtender(btnHideListMatMix, pnlListMatMix, mpeListMatMix, True)
    End Sub

    Protected Sub btnClearMatMix_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearMatMix.Click
        matoid_mix.Text = "" : matlongdesc_mix.Text = "" : qty_mix.Text = "" : unitoid_mix.SelectedIndex = -1 : EnableQtyMix(False)
    End Sub

    Protected Sub btnFindListMatMix_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMatMix.Click
        BindListMatMix()
        mpeListMatMix.Show()
    End Sub

    Protected Sub btnAllListMatMix_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMatMix.Click
        FilterDDLListMatMix.SelectedIndex = -1 : FilterTextListMatMix.Text = "" : gvListMatMix.SelectedIndex = -1
        BindListMatMix()
        mpeListMatMix.Show()
    End Sub

    Protected Sub gvListMatMix_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMatMix.PageIndexChanging
        gvListMatMix.PageIndex = e.NewPageIndex
        BindListMatMix()
        mpeListMatMix.Show()
    End Sub

    Protected Sub gvListMatMix_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListMatMix.SelectedIndexChanged
        matoid_mix.Text = gvListMatMix.SelectedDataKey.Item("matoid_mix").ToString
        matlongdesc_mix.Text = gvListMatMix.SelectedDataKey.Item("matlongdesc_mix").ToString
        unitoid_mix.SelectedValue = gvListMatMix.SelectedDataKey.Item("unitoid_mix").ToString
        EnableQtyMix(True)
        cProc.SetModalPopUpExtender(btnHideListMatMix, pnlListMatMix, mpeListMatMix, False)
    End Sub

    Protected Sub lbCloseListMatMix_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMatMix.Click
        cProc.SetModalPopUpExtender(btnHideListMatMix, pnlListMatMix, mpeListMatMix, False)
    End Sub
#End Region

End Class
