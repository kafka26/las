Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Structure VarUsageValue
    Public Val_IDR As Double
    Public Val_USD As Double
End Structure

Partial Class Transaction_TW
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Dim cRate As New ClassRate
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function GetDropDownListValue(ByVal cc As System.Web.UI.ControlCollection, Optional ByVal sText As String = "") As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                If sText = "" Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
                Else
                    sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedItem.Text
                End If
            End If
        Next
        Return sReturn
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If transfromwhoid.SelectedValue = "" Then
            sError &= "- Please select FROM WH field!<BR>"
        End If
        If transtowhoid.SelectedValue = "" Then
            sError &= "- Please select TO WH field!<BR>"
        End If
        If matoid.Text = "" Then
            sError &= "- Please select MATERIAL field!<BR>"
        End If
        If transqty.Text = "" Then
            sError &= "- Please fill QTY field!<BR>"
        Else
            If ToDouble(transqty.Text) <= 0 Then
                sError &= "- QTY field must be more than 0!<BR>"
            Else
                If ToDouble(transqty.Text) > ToDouble(stockqty.Text) Then
                    sError &= "- QTY field must be less than STOCK QTY!<BR>"
                End If
            End If
        End If
        If transunitoid.SelectedValue = "" Then
            sError &= "- Please select UNIT field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If transdate.Text = "" Then
            sError &= "- Please fill TRANSFER DATE field!<BR>"
        Else
            If Not IsValidDate(transdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- TRANSFER DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If transitemmstaddr.Text.Length > 500 Then
            sError &= "- Addres Max 500 Character!"
        End If

        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill Detail Data!<BR>"
        Else
			Dim dtTbl As DataTable = Session("TblDtl")
			If dtTbl.Rows.Count <= 0 Then
				sError &= "- Please fill Detail Data!<BR>"
			Else
				If sError = "" Then
					For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                        If Not IsStockAvailable(DDLBusUnit.SelectedValue, Format(GetServerTime(), "yyyyMM"), dtTbl.Rows(C1)("matoid"), dtTbl.Rows(C1)("transfromwhoid"), ToDouble((dtTbl.Compute("SUM(transqty)", "matoid=" & dtTbl.Rows(C1)("matoid") & " AND transfromwhoid=" & dtTbl.Rows(C1)("transfromwhoid") & "")).ToString), "") Then
                            sError &= "- Transfer Qty for Item!<BR><B><STRONG>" & dtTbl.Rows(C1).Item("matlongdesc").ToString & "</STRONG></B> must less or equal than stock QTY !<BR>"
                            Exit For
                        End If
                        If dtTbl.Rows(C1)("transfromwhoid").ToString = "0" Then
                            sError &= "- Silakan pilih gudang asal!<br>"
                            Exit For
                        End If
                        If dtTbl.Rows(C1)("transtowhoid").ToString = "0" Then
                            sError &= "- Silakan pilih gudang tujuan!<br>"
                            Exit For
                        End If
                    Next
				End If
			End If
        End If

        If cbCons.Checked And cons_type.SelectedValue.ToLower() = "transfer" Then
            Dim total_sales As Double = 0
            If Not Session("TblDtl") Is Nothing Then
                Dim objTable As DataTable = Session("TblDtl")
                For C1 As Int16 = 0 To objTable.Rows.Count - 1
                    total_sales += ToDouble(objTable.Rows(C1)("salesprice").ToString) * ToDouble(objTable.Rows(C1)("transqty").ToString)
                Next
            End If

            Dim balance_limit = cKon.ambilscalar("select credit_limit - limit_usage from QL_mstcust where custoid=" & custoid.Text)
            If total_sales > balance_limit Then
                sError &= "- Customer ini melebihi batas limit konsinyasi! Limit : Rp. " & ToMaskEdit(balance_limit, 2) & ", Kiriman : Rp. " & ToMaskEdit(total_sales, 2) & "<BR>"
            End If
        End If
        If cbCons.Checked And person_id.SelectedValue = "0" Then
            sError &= "- Silakan pilih sales terlebih dahulu!<BR>"
        End If

        If sError <> "" Then
            showMessage(sError, 2)
            transmststatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Public Function GetParameterID() As String
        Return Eval("matoid") & "," & Eval("selectedunitoid")
    End Function

    Private Function GetStockValue(ByVal sOid As String, Optional ByVal sType As String = "IDR") As Double
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        Try
            GetStockValue = ToDouble(GetStrData("select dbo.getStockValue(" & sOid & ")"))
        Catch ex As Exception
            GetStockValue = 0
        End Try
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trntransitemmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND transitemmststatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnTW.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lbInProcess.Visible = True
            lbInProcess.Text = "You have " & ToDouble(GetStrData(sSql)) & " In Process Finish Good Transfer data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitAllDDL()
        Dim sError As String = ""
        ' Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE 1=1"
        If Session("oid") Is Nothing Or Session("oid") = "" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If FillDDL(DDLBusUnit, sSql) Then
            InitDDLFromWH()
            InitDDLToWH()
        End If
        ' Fill DDL Unit
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='UNIT'"
        If Session("oid") Is Nothing Or Session("oid") = "" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
        FillDDL(transunitoid, sSql)
        FillDDL(stockunitoid, sSql)
        ' Fill DDL Sales
        sSql = "select personoid, personname from QL_mstperson p inner join QL_mstlevel g on g.leveloid =p.leveloid where salescode<>'' and upper(g.leveldesc)='SALES' and p.activeflag='active' order by personname"
        FillDDLWithAdditionalText(person_id, sSql, "-- Pilih Sales --", "0")
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='CITY' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDL(transitemmstcityOid, sSql)
        If transitemmstcityOid.Items.Count = 0 Then
            sError = "CITY data is empty. Please input CITY data first or contact your administrator!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            btnSave.Visible = False
            btnposting.Visible = False
        End If
    End Sub

    Private Sub InitDDLTransferTo(ByVal bVal As Boolean)
        If Not bVal Then
            'Fill DDL Supplier
            sSql = "SELECT suppoid, suppname FROM QL_mstsupp WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND suppgroup='Supplier Maklon' ORDER BY suppname"
        Else
            'Fill DDL Customer
            sSql = "SELECT custoid, custname FROM QL_mstcust WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' ORDER BY custname"
        End If
        FillDDLWithAdditionalText(suppoid, sSql, "NONE", 0)
    End Sub

    Private Sub InitDDLFromWH()
        ' Fill DDL Warehouse
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND gengroup='WAREHOUSE' AND genoid > 0"
        If Session("oid") Is Nothing Or Session("oid") = "" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
        If FillDDLWithAdditionalText(transfromwhoid, sSql, "-- Pilih Gudang Asal --", "0") Then
            InitDDLToWH()
        End If
    End Sub

    Private Sub InitDDLToWH()
        ' Fill DDL Warehouse
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND gengroup='WAREHOUSE' AND genoid > 0 AND genoid <> " & transfromwhoid.SelectedValue
        If Session("oid") Is Nothing Or Session("oid") = "" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
        FillDDLWithAdditionalText(transtowhoid, sSql, "-- Pilih Gudang Tujuan --", "0")
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT * FROM (SELECT tm.cmpcode, 'False' AS checkvalue, divname, transitemmstoid AS transmstoid, transitemno AS transno, CONVERT(VARCHAR(10), transitemdate, 101) AS transdate, transitemdocrefno AS transdocrefno, transitemmststatus AS transmststatus, transitemmstnote AS transmstnote, tm.updtime, tm.createuser, ISNULL(name,'') name FROM QL_trntransitemmst tm INNER JOIN QL_mstdivision div ON div.cmpcode=tm.cmpcode LEFT JOIN (SELECT custoid oid, custname name, 'QL_mstcust' reftype FROM QL_mstcust c UNION ALL SELECT suppoid oid, suppname name, 'QL_mstsupp' reftype FROM QL_mstsupp s) AS c ON c.oid=ISNULL(transitemmstres3,0) AND reftype=(CASE WHEN ISNULL(transitemmstres3, 0)<>0 AND iskonsinyasi='False' THEN 'QL_mstsupp' WHEN ISNULL(transitemmstres3, 0)<>0 AND iskonsinyasi='True' THEN 'QL_mstcust' ELSE '' END) WHERE tm.sssflag=0) AS QL_trntransmst WHERE cmpcode='" & CompnyCode & "'"
        If checkPagePermission("~\Transaction\trnTW.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND createuser='" & Session("UserID") & "'"
        End If
        sSql &= sSqlPlus & " ORDER BY CAST(transdate AS DATETIME) DESC, transmstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trntransmst")
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvMst.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvMst.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvMst.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "transmstoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub PrintReport()
        Try
            Dim sOid As String = ""
            Dim cekRes As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("transmstoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            If DDLPrintOption.SelectedIndex = 0 Then
                If sOid <> "" Then
                    sOid = Left(sOid, sOid.Length - 1)
                    sSql = "select distinct (case when transitemmstres3='0' then 'None' ELSE 'NotNone' End) res3 from QL_trntransitemmst where cmpcode='" & Session("CompnyCode") & "' and transitemmstoid IN (" & sOid & ")"
                    cekRes = GetStrData(sSql)
                    If cekRes = "None" Then
                        report.Load(Server.MapPath(folderReport & "rptTransferNone.rpt"))
                    Else
                        sSql = "SELECT iskonsinyasi FROM QL_trntransitemmst WHERE transitemmstoid IN (" & sOid & ")"
                        If GetStrData(sSql) = "True" Then
                            report.Load(Server.MapPath(folderReport & "rptDOCons_Trn.rpt"))
                        Else
                            report.Load(Server.MapPath(folderReport & "rptTransfer.rpt"))
                        End If
                    End If
                Else
                    report.Load(Server.MapPath(folderReport & "rptTransfer.rpt"))
                End If
            Else
                report.Load(Server.MapPath(folderReport & "rptARNotaCons.rpt"))
            End If
            If DDLPrintOption.SelectedIndex = 0 Then
                sSql = "SELECT iskonsinyasi FROM QL_trntransitemmst WHERE transitemmstoid IN (" & sOid & ")"
                If GetStrData(sSql) = "False" Then
                    sSql = "SELECT * FROM ( SELECT tm.cmpcode, ( SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=tm.cmpcode) [Business Unit], tm.transitemmstoid [ID Hdr], CAST(tm.transitemmstoid AS VARCHAR(10)) [Draft No.], transitemno [Transfer No.], transitemdate [Transfer Date], transitemdocrefno [Doc. Ref. No.], transitemmstnote [Header Note], transitemmststatus [Status], UPPER(tm.createuser) [Create User], tm.createtime [Create Time], (CASE transitemmststatus WHEN 'In Process' THEN '' ELSE UPPER(tm.upduser) END) [Post User],(CASE transitemmststatus WHEN 'In Process' THEN CAST('01/01/1900' AS DATETIME) ELSE UPPER(tm.updtime) END) [Post Time], transitemdtloid [ID Dtl], transitemdtlseq [No.], g1.gendesc [From WH], g2.gendesc [To WH], m.itemCode [Code], m.itemLongDescription [Description], transitemqty [Qty], g3.gendesc [Unit], transitemdtlnote [Detail Note], suppname [Transfer To], ISNULL(transitemmstaddr,'') [Address], transitemmstcityOid [City Oid], g4.gendesc [City], td.salesprice [value], m.itemoldcode [oldcode] " & _
                                " FROM QL_trntransitemmst tm " & _
                                " INNER JOIN QL_trntransitemdtl td ON td.cmpcode=tm.cmpcode" & _
                                " AND td.transitemmstoid=tm.transitemmstoid" & _
                                " INNER JOIN QL_mstgen g1 ON g1.genoid=transitemfromwhoid " & _
                                " INNER JOIN QL_mstgen g2 ON g2.genoid=transitemtowhoid " & _
                                " INNER JOIN QL_mstitem m ON m.itemoid=td.itemoid " & _
                                " INNER JOIN QL_mstgen g3 ON g3.genoid=transitemunitoid " & _
                                " LEFT JOIN QL_mstsupp sup ON sup.suppoid=ISNULL(transitemmstres3,0) " & _
                                " INNER JOIN QL_mstgen g4 ON g4.gengroup='CITY' " & _
                                " AND g4.genoid=transitemmstcityOid ) AS tblTransfer WHERE"
                    If Session("CompnyCode") <> CompnyCode Then
                        sSql &= " cmpcode='" & Session("CompnyCode") & "'"
                    Else
                        sSql &= " cmpcode LIKE '%'"
                    End If
                    If sOid = "" Then
                        sSql &= " AND " & FilterDDL2.Items(FilterDDL.SelectedIndex).Value & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                        If cbPeriode.Checked Then
                            If IsValidPeriod() Then
                                sSql &= " AND [Transfer Date]>=CAST('" & FilterPeriod1.Text & " 00:00:00' AS DATETIME) AND [Transfer Date]<=CAST('" & FilterPeriod2.Text & " 23:59:59' AS DATETIME)"
                            Else
                                Exit Sub
                            End If
                        End If
                        If cbStatus.Checked Then
                            sSql &= " AND [Status]='" & FilterDDLStatus.SelectedValue & "'"
                        End If
                        If checkPagePermission("~\Transaction\trnTW.aspx", Session("SpecialAccess")) = False Then
                            sSql &= " AND [Create User]=UPPER('" & Session("UserID") & "')"
                        End If
                    Else
                        sSql &= " AND [ID Hdr] IN (" & sOid & ")"
                    End If
                    Dim dtReport As DataTable = cKon.ambiltabel(sSql, "tblTransfer")
                    report.SetDataSource(dtReport)
                    report.SetParameterValue("MaterialType", "FINISH GOOD")
                    report.SetParameterValue("PrintUserID", Session("UserID"))
                    report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
                Else
                    Dim sWhere As String = " WHERE dom.transitemmstoid IN (" & sOid & ")"
                    report.SetParameterValue("Header", "FINISH GOOD")
                    report.SetParameterValue("sWhere", sWhere)
                    report.SetParameterValue("PrintUserID", Session("UserID"))
                    report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
                End If
            Else
                If sOid <> "" Then
                    sOid = Left(sOid, sOid.Length - 1)
                End If
                Dim sWhere As String = " WHERE arm.transitemmstoid IN (" & sOid & ")"
                report.SetParameterValue("Header", "FINISH GOOD")
                report.SetParameterValue("sWhere", sWhere)
                report.SetParameterValue("PrintUserID", Session("UserID"))
                report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            End If
            cProc.SetDBLogonForReport(report)
            report.PrintOptions.PaperSize = PaperSize.PaperA4
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "MaterialTransferPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
        Response.Redirect("~\Transaction\trnTW.aspx?awal=true")
    End Sub

    Private Sub SetUsageValue(ByRef objVal As VarUsageValue)
        objVal.Val_IDR = 0
        objVal.Val_USD = 0
        Dim dt As DataTable = Session("TblDtl")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            Dim dValIDR As Double = GetStockValue(dt.Rows(C1)("matoid").ToString)
            Dim dValUSD As Double = GetStockValue(dt.Rows(C1)("matoid").ToString, "USD")
            dt.Rows(C1)("transvalueidr") = dValIDR
            dt.Rows(C1)("transvalueusd") = dValUSD
            objVal.Val_IDR += dValIDR '* ToDouble(dt.Rows(C1)("transqty").ToString)
            objVal.Val_USD += dValUSD '* ToDouble(dt.Rows(C1)("transqty").ToString)
        Next
        Session("TblDtl") = dt
    End Sub

    Private Sub BindMaterialData()
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        Dim sel As String = "CONVERT (DECIMAL (18,2), itempricelist) salesprice2, 0 trans_ref_d_id, 0 trans_ref_id, '' trans_ref_no, null trans_ref_date, 0.0 trans_ref_qty, m.itemoid matoid_sel"
        If cbCons.Checked And cons_type.SelectedValue.ToLower() = "return" Then
            sel = "salesprice salesprice2, td.transitemdtloid trans_ref_d_id, td.transitemmstoid trans_ref_id, transitemno trans_ref_no, CONVERT(VARCHAR(10), transitemdate, 101) trans_ref_date, transitemqty - qty_return trans_ref_qty, td.transitemdtloid matoid_sel"
        End If
        sSql = "SELECT 'False' AS Checkvalue, 'True' AS enableqty, m.itemoid AS matoid, itemoldcode, " & sel & ", 0.00 AS salesprice, itemcode AS matcode, m.itemLongDescription AS matlongdesc, m.itemUnit1 AS transunitoid, gendesc AS stockunit, (select dbo.getstockqty(m.itemoid, " & transfromwhoid.SelectedValue & ")) AS stockqty, 0.0 AS transqty, '' AS transdtlnote, 0.0 AS ValueIdr, 0.0 AS ValueUsd, m.itemUnit1 AS selectedunitoid, g.gendesc AS selectedunit, (SELECT ISNULL(genother1,'') acctgoid FROM QL_mstgen gx WHERE gx.cmpcode='" & CompnyCode & "' AND gx.gengroup='GROUPITEM' AND gx.gencode=m.itemgroup AND activeflag='ACTIVE') AS stockacctgoid, m.itemunit1 AS stockunitoid, (CASE m.itemGroup When 'RAW' Then 'RAW MATERIAL' When 'FG' Then 'FINISH GOOD' When 'GEN' Then 'GENERAL MATERIAL' When 'WIP' Then 'WIP' End) AS refname FROM QL_mstitem m INNER JOIN QL_mstgen g ON genoid=itemUnit1"
        If cbCons.Checked And cons_type.SelectedValue.ToLower() = "return" Then
            sSql &= " inner join QL_trntransitemdtl td on td.itemoid=m.itemoid inner join QL_trntransitemmst tm on tm.transitemmstoid=td.transitemmstoid and tm.transitemno like 'sjk%'"
        End If
        sSql &= " WHERE m.cmpcode='" & CompnyCode & "'"
        sSql &= " ORDER BY matcode"
        If cbCons.Checked And cons_type.SelectedValue.ToLower() = "return" Then
            sSql &= ", tm.transitemdate desc"
        End If
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstitem")
        If dt.Rows.Count > 0 Then
            For C1 As Integer = 0 To dt.Rows.Count - 1
                Dim qty As Integer = dt.Rows(C1)("transqty")
                dt.Rows(C1)("transqty") = ToDouble(dt.Rows(C1)("stockqty").ToString)
                If cbCons.Checked And cons_type.SelectedValue.ToLower() = "return" Then
                    dt.Rows(C1)("transqty") = ToDouble(dt.Rows(C1)("trans_ref_qty").ToString)
                End If
                dt.Rows(C1)("salesprice") = ToDouble(dt.Rows(C1)("salesprice2").ToString)
                If Not cbCons.Checked Then
                    dt.Rows(C1)("salesprice") = "0.00"
                End If
            Next
            dt.AcceptChanges()
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "stockqty>0"
            Session("TblMat") = dv.ToTable
            Session("TblMatView") = dv.ToTable
            gvListMat.DataSource = Session("TblMat")
            gvListMat.DataBind()
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            showMessage("Material data can't be found!", 2)
        End If
    End Sub

    Private Sub UpdateCheckedListMat()
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matoid_sel=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("transqty") = ToDouble(GetTextBoxValue(row.Cells(6).Controls))
                                    dtView(0)("salesprice") = GetTextBoxValue(row.Cells(8).Controls)
                                    dtView(0)("transdtlnote") = GetTextBoxValue(row.Cells(9).Controls)
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
            End If
        End If
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matoid_sel=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("transqty") = ToDouble(GetTextBoxValue(row.Cells(6).Controls))
                                    dtView(0)("salesprice") = GetTextBoxValue(row.Cells(8).Controls)
                                    dtView(0)("transdtlnote") = GetTextBoxValue(row.Cells(9).Controls)
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMatView") = dtTbl
            End If
        End If
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("TblTransferDtl")
        dtlTable.Columns.Add("transdtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("transfromwhoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("transfromwh", Type.GetType("System.String"))
        dtlTable.Columns.Add("transtowhoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("transtowh", Type.GetType("System.String"))
        dtlTable.Columns.Add("matoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matoid_sel", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemoldcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("matlongdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("transqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("stockqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("transunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("transunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("transdtlnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("transvalueidr", Type.GetType("System.Double"))
        dtlTable.Columns.Add("transvalueusd", Type.GetType("System.Double"))
        dtlTable.Columns.Add("refname", Type.GetType("System.String"))
        dtlTable.Columns.Add("lastqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("transitemqty_unitkecil", Type.GetType("System.Double"))
        dtlTable.Columns.Add("transitemqty_unitbesar", Type.GetType("System.Double"))
        dtlTable.Columns.Add("stockunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("stockacctgoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("salesprice", Type.GetType("System.Double"))
        dtlTable.Columns.Add("totalamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("trans_ref_d_id", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("trans_ref_id", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("trans_ref_qty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("trans_ref_no", Type.GetType("System.String"))
        dtlTable.Columns.Add("trans_ref_date", Type.GetType("System.String"))
        dtlTable.Columns.Add("cust_disc_1", Type.GetType("System.Double"))
        dtlTable.Columns.Add("cust_disc_2", Type.GetType("System.Double"))
        dtlTable.Columns.Add("cust_disc_3", Type.GetType("System.Double"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub CountHdrAmt()
        Dim dVal As Double = 0
        If Session("TblDtl") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtl")

            dVal = ToDouble(dt.Compute("SUM(totalamt)", "salesprice>0").ToString)
        End If
        lblTotalInv.Text = ToMaskEdit(dVal, 4)
    End Sub

    Private Sub ClearDetail()
        transdtlseq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                transdtlseq.Text = objTable.Rows.Count + 1
                EnableHeader(False)
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        i_u2.Text = "New Detail"
        'transfromwhoid.SelectedIndex = -1
        'transtowhoid.SelectedIndex = -1
        matoid_sel.Text = ""
        matoid.Text = ""
        matcode.Text = ""
        matlongdesc.Text = ""
        transqty.Text = ""
        stockqty.Text = ""
        transunitoid.SelectedIndex = -1
        transdtlnote.Text = ""
        salesprice.Text = ""
        gvDtl.SelectedIndex = -1
        trans_ref_d_id.Text = ""
        trans_ref_id.Text = ""
        'transfromwhoid.Enabled = True : transfromwhoid.CssClass = "inpText"
        btnSearchMat.Visible = True
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal : DDLBusUnit.CssClass = sCss
        cbCons.Enabled = bVal
        suppoid.Enabled = bVal : suppoid.CssClass = sCss
        suppoid2.Enabled = bVal : suppoid2.CssClass = sCss
        btnSearchSupp.Visible = bVal : btnEraseSupp.Visible = bVal
        cons_type.Enabled = bVal : cons_type.CssClass = sCss
    End Sub

    Private Sub GenerateNo()
        Dim sPrefixCode As String = ""
        If Not cbCons.Checked Then
            sPrefixCode = "SJP"
            If suppoid.SelectedItem.Text.ToUpper = "NONE" Then
                sPrefixCode = "TW"
            End If
        Else
            sPrefixCode = "SJK"
            If cons_type.SelectedValue.ToLower() = "return" Then
                sPrefixCode = "SJK.R"
            End If
        End If
        Dim sNo As String = sPrefixCode & "-" & Format(GetServerTime(), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(transitemno, " & DefaultFormatCounter & ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trntransitemmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND transitemno LIKE '" & sNo & "%'"
        If GetStrData(sSql) = "" Then
            transno.Text = GenNumberString(sNo, "", 1, DefaultFormatCounter)
        Else
            transno.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        End If
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT t.cmpcode, transitemmstoid, periodacctg, transitemdate, transitemno, transitemdocrefno, transitemmstnote, transitemmststatus, t.createuser, t.createtime, t.upduser, t.updtime, transitemmstto, transitemmstaddr, transitemmstcityOid, ISNULL(transitemmstres3,0) AS suppoid, iskonsinyasi, cons_type, t.person_id, isnull(cc.warehouse_id, 0) warehouse_id FROM QL_trntransitemmst t left join QL_mstcust cc on cc.custoid=ISNULL(transitemmstres3,0) WHERE transitemmstoid=" & sOid
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = xreader("cmpcode").ToString
                InitDDLFromWH()
                transmstoid.Text = xreader("transitemmstoid").ToString
                periodacctg.Text = xreader("periodacctg").ToString
                transdate.Text = Format(xreader("transitemdate"), "MM/dd/yyyy")
                transno.Text = xreader("transitemno").ToString
                transdocrefno.Text = xreader("transitemdocrefno").ToString
                transitemmstto.Text = xreader("transitemmstto").ToString
                transitemmstaddr.Text = xreader("transitemmstaddr").ToString
                transitemmstcityOid.SelectedValue = xreader("transitemmstcityOid").ToString
                cbCons.Checked = xreader("iskonsinyasi").ToString
                cbCons_CheckedChanged(Nothing, Nothing)
                suppoid.SelectedValue = xreader("suppoid").ToString
                cons_type.SelectedValue = xreader("cons_type").ToString
                person_id.SelectedValue = xreader("person_id").ToString
                If cbCons.Checked Then
                    suppoid2.Text = GetStrData("SELECT custname FROM QL_mstcust WHERE custoid=" & xreader("suppoid").ToString)
                    custoid.Text = xreader("suppoid").ToString

                    If cons_type.SelectedValue = "TRANSFER" Then
                        transtowhoid.SelectedValue = xreader("warehouse_id").ToString
                    Else
                        transfromwhoid.SelectedValue = xreader("warehouse_id").ToString
                    End If
                End If
                transmstnote.Text = xreader("transitemmstnote").ToString
                transmststatus.Text = xreader("transitemmststatus").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            Exit Sub
            btnSave.Visible = False
            btnDelete.Visible = False
            btnposting.Visible = False
        End Try
        If transmststatus.Text = "Post" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnposting.Visible = False
            lblspuno.Text = "Transfer No."
            transmstoid.Visible = False
            transno.Visible = True
            btnAddToList.Visible = False
            gvDtl.Columns(0).Visible = False
            gvDtl.Columns(gvDtl.Columns.Count - 1).Visible = False
            ImbUpdSales.Visible = True
        End If
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sSql = "SELECT 0.0 AS lastqty,td.transitemdtlseq AS transdtlseq, td.transitemfromwhoid AS transfromwhoid, g1.gendesc AS transfromwh, td.transitemtowhoid AS transtowhoid, g2.gendesc AS transtowh, m.itemoid AS matoid, td.salesprice, (td.salesprice * td.transitemqty) totalamt, itemoldcode, m.itemcode AS matcode, m.itemLongDescription AS matlongdesc, td.transitemqty AS transqty, (select dbo.getstockqty(td.itemoid, td.transitemfromwhoid)) AS stockqty, td.transitemunitoid AS transunitoid, g3.gendesc AS transunit, td.transitemdtlnote AS transdtlnote, 0.0 AS transvalueidr, 0.0 AS transvalueusd, (CASE m.itemGroup When 'RAW' Then 'RAW MATERIAL' When 'FG' Then 'FINISH GOOD' When 'GEN' Then 'GENERAL MATERIAL' When 'WIP' Then 'WIP' End) AS refname,td.transitemqty_unitkecil,td.transitemqty_unitbesar, (SELECT ISNULL(genother1,'') acctgoid FROM QL_mstgen gx WHERE gx.cmpcode='" & CompnyCode & "' AND gx.gengroup='GROUPITEM' AND gx.gencode=m.itemgroup AND activeflag='ACTIVE') AS stockacctgoid, m.itemunit1 AS stockunitoid, td.trans_ref_d_id, td.trans_ref_id, isnull((select y.transitemno from QL_trntransitemmst y where y.transitemmstoid=td.trans_ref_id), '') trans_ref_no, isnull((select CONVERT(VARCHAR(10), transitemdate, 101) from QL_trntransitemmst y where y.transitemmstoid=td.trans_ref_id), '') trans_ref_date, isnull(rd.transitemqty - rd.qty_return, 0.0) trans_ref_qty, case td.trans_ref_d_id when 0 then m.itemoid else td.trans_ref_d_id end matoid_sel, td.cust_disc_1, td.cust_disc_2, td.cust_disc_3 FROM QL_trntransitemdtl td INNER JOIN QL_mstitem m ON m.itemoid=td.itemoid INNER JOIN QL_mstgen g1 ON g1.genoid=transitemfromwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=transitemtowhoid INNER JOIN QL_mstgen g3 ON g3.genoid=transitemunitoid left join QL_trntransitemdtl rd on rd.transitemdtloid=td.trans_ref_d_id WHERE td.transitemmstoid=" & sOid & " ORDER BY transdtlseq"
        Session("TblDtl") = cKon.ambiltabel(sSql, "QL_trntransitemdtl")
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        ClearDetail()
        CountHdrAmt()
        EnableGridDetail()
        cons_type_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Private Sub EnableGridDetail()
        gvDtl.Columns(gvDtl.Columns.Count - 3).Visible = True
        If Not cbCons.Checked Then
            If suppoid.SelectedItem.Text.ToUpper <> "NONE" Then
                gvDtl.Columns(gvDtl.Columns.Count - 3).Visible = False
            End If
        End If
    End Sub

    Private Sub BindDataCustomer()
        sSql = "SELECT custoid, custcode, custname, custaddr, person_id, warehouse_id, cust_disc_1, cust_disc_2, cust_disc_3 FROM QL_mstcust WHERE activeflag='ACTIVE' AND (custcode LIKE '%" & Tchar(suppoid2.Text) & "%' OR custname LIKE '%" & Tchar(suppoid2.Text) & "%' OR custaddr LIKE '%" & Tchar(suppoid2.Text) & "%') ORDER BY custname"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstcust")
        Session("TblCust") = dt
        gvCust.DataSource = dt
        gvCust.DataBind()
    End Sub

    Private Sub BindListMatMix()
        sSql = "SELECT itemoid AS matoid_mix, itemCode AS matcode_mix, itemLongDescription AS matlongdesc_mix, gendesc AS matunit_mix, itemUnit1 AS unitoid_mix, CONVERT (DECIMAL (18,2), itempricelist) salesprice, (select dbo.getstockqty(m.itemoid, " & transfromwhoid.SelectedValue & ")) stockqty FROM QL_mstitem m INNER JOIN QL_mstgen g ON g.cmpcode=m.cmpcode AND genoid=itemUnit1 WHERE itemgroup='FG' and itemRecordStatus='ACTIVE' and " & FilterDDLListMatMix.SelectedValue & " LIKE '%" & Tchar(FilterTextListMatMix.Text) & "%' ORDER by matcode_mix"
        FillGV(gvListMatMix, sSql, "tbl_MatMix")
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnTW.aspx")
        End If
        If checkPagePermission("~\Transaction\trnTW.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Finish Good Transfer"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnposting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            lblTitleListMat.Text = "List Of Material"
            CheckStatus()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitAllDDL()
            cbCons_CheckedChanged(Nothing, Nothing)
            suppoid_SelectedIndexChanged(Nothing, Nothing)
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                transmstoid.Text = GenerateID("QL_TRNTRANSITEMMST", CompnyCode)
                transdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                transmststatus.Text = "In Process"
                periodacctg.Text = GetDateToPeriodAcctg(CDate(transdate.Text))
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvDtl.DataSource = dt
            gvDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnTW.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lbInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = 0
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = 1
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND transmststatus='In Process'"
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND CAST(transdate AS DATETIME)>=CAST('" & FilterPeriod1.Text & " 00:00:00' AS DATETIME) AND CAST(transdate AS DATETIME)<=CAST('" & FilterPeriod2.Text & " 23:59:59' AS DATETIME)"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND transmststatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        cbStatus.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        FilterDDLStatus.SelectedIndex = -1
        BindTrnData("")
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        UpdateCheckedValue()
        gvMst.PageIndex = e.NewPageIndex
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
    End Sub

    Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvMst.DataSource = Session("TblMst")
            gvMst.DataBind()
        End If
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        UpdateCheckedValue()
        PrintReport()
    End Sub

    Protected Sub transfromwhoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitDDLToWH()
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        If transfromwhoid.SelectedValue = "" Then
            showMessage("Please select From WH first!", 2)
            Exit Sub
        End If
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        Session("TblMat") = Nothing : Session("TblMatView") = Nothing
        gvListMat.DataSource = Nothing : gvListMat.DataBind()
        BindMaterialData()
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        UpdateCheckedListMat()
        If Session("TblMat") IsNot Nothing Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        UpdateCheckedListMat()
        FilterTextListMat.Text = ""
        If Session("TblMat") IsNot Nothing Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = ""
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        UpdateCheckedListMat()
        gvListMat.PageIndex = e.NewPageIndex
        gvListMat.DataSource = Session("TblMatView")
        gvListMat.DataBind()
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(6).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 2)
                    End If
                End If
            Next
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 2)
            Dim str As String = e.Row.Cells(1).Text
            e.Row.Cells(1).Text = Mid(str, 1, 7) & vbCrLf & Mid(str, 8, 10) & vbCrLf & Mid(str, 18, 5)

            Dim iItemOid As Integer = 0
            cc = e.Row.Cells(0).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    iItemOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                End If
            Next
            e.Row.Cells(12).Text = ToMaskEdit(ToDouble(e.Row.Cells(12).Text), 2)
        End If
    End Sub

    Protected Sub cbHdrListMat_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For C1 As Integer = 0 To gvListMat.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        mpeListMat.Show()
    End Sub

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        UpdateCheckedListMat()
        If Session("TblMat") IsNot Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            Dim iCheck As Integer = dtView.Count
            If iCheck > 0 Then
                dtView.RowFilter = ""
                dtView.RowFilter = "checkvalue='True' AND transqty>0"
                If dtView.Count <> iCheck Then
                    Session("WarningListMat") = "Transfer Qty for every checked material data must be more than 0!"
                    showMessage(Session("WarningListMat"), 2)
                    dtView.RowFilter = ""
                    Exit Sub
                End If
                'dtView.RowFilter = ""
                'dtView.RowFilter = "checkvalue='True' AND transqty<=stockqty"
                'If dtView.Count <> iCheck Then
                '    Session("WarningListMat") = "Transfer Qty for every checked material data must be less than Stock Qty!"
                '    showMessage(Session("WarningListMat"), 2)
                '    dtView.RowFilter = ""
                '    Exit Sub
                'End If
                dtView.RowFilter = ""
                dtView.RowFilter = "checkvalue='True'"
                If Session("TblDtl") Is Nothing Then
                    CreateTblDetail()
                End If
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                Dim dQty_unitkecil As Double = 0
                Dim dQty_unitbesar As Double = 0
                Dim objRow As DataRow
                Dim counter As Integer = objTable.Rows.Count
                For C1 As Integer = 0 To dtView.Count - 1
                    dv.RowFilter = "transfromwhoid=" & transfromwhoid.SelectedValue & " AND trans_ref_d_id=" & dtView(C1)("trans_ref_d_id").ToString & " AND matoid=" & dtView(C1)("matoid_sel")
                    If dv.Count > 0 Then
                        dv(0)("transqty") = ToDouble(dtView(C1)("transqty").ToString)
                        dv(0)("transdtlnote") = dtView(C1)("transdtlnote").ToString
                        dv(0)("salesprice") = dtView(C1)("salesprice").ToString
                        dv(0)("totalamt") = ToDouble(dtView(C1)("transqty").ToString) * ToDouble(dtView(C1)("salesprice").ToString)
                        dv(0)("trans_ref_d_id") = dtView(C1)("trans_ref_d_id").ToString
                        dv(0)("trans_ref_id") = dtView(C1)("trans_ref_id").ToString
                        dv(0)("trans_ref_qty") = ToDouble(dtView(C1)("trans_ref_qty").ToString)
                        dv(0)("trans_ref_no") = dtView(C1)("trans_ref_no").ToString
                        dv(0)("trans_ref_date") = dtView(C1)("trans_ref_date").ToString
                    Else
                        counter += 1
                        objRow = objTable.NewRow()
                        objRow("transdtlseq") = counter
                        objRow("transfromwhoid") = transfromwhoid.SelectedValue
                        objRow("transfromwh") = transfromwhoid.SelectedItem.Text
                        objRow("transtowhoid") = transtowhoid.SelectedValue
                        objRow("transtowh") = transtowhoid.SelectedItem.Text
                        objRow("matoid") = dtView(C1)("matoid")
                        objRow("matoid_sel") = dtView(C1)("matoid_sel")
                        objRow("matcode") = dtView(C1)("matcode").ToString
                        objRow("itemoldcode") = dtView(C1)("itemoldcode").ToString
                        objRow("matlongdesc") = dtView(C1)("matlongdesc").ToString
                        objRow("stockqty") = ToDouble(dtView(C1)("stockqty").ToString)
                        objRow("transqty") = ToDouble(dtView(C1)("transqty").ToString)
                        objRow("lastqty") = ToDouble(dtView(C1)("stockqty").ToString) - ToDouble(dtView(C1)("transqty").ToString)
                        objRow("transunitoid") = dtView(C1)("selectedunitoid")
                        objRow("transunit") = dtView(C1)("selectedunit").ToString
                        objRow("transdtlnote") = dtView(C1)("transdtlnote").ToString
                        objRow("transvalueidr") = ToDouble(dtView(C1)("ValueIdr").ToString)
                        objRow("transvalueusd") = ToDouble(dtView(C1)("ValueUsd").ToString)
                        objRow("refname") = dtView(C1)("refname").ToString
                        objRow("stockunitoid") = dtView(C1)("stockunitoid").ToString
                        objRow("stockacctgoid") = dtView(C1)("stockacctgoid").ToString
                        objRow("salesprice") = dtView(C1)("salesprice").ToString
                        objRow("totalamt") = ToDouble(dtView(C1)("transqty").ToString) * ToDouble(dtView(C1)("salesprice").ToString)
                        objRow("trans_ref_d_id") = dtView(C1)("trans_ref_d_id").ToString
                        objRow("trans_ref_id") = dtView(C1)("trans_ref_id").ToString
                        objRow("trans_ref_no") = dtView(C1)("trans_ref_no").ToString
                        objRow("trans_ref_date") = dtView(C1)("trans_ref_date").ToString
                        objRow("trans_ref_qty") = ToDouble(dtView(C1)("trans_ref_qty").ToString)
                        objRow("cust_disc_1") = ToDouble(cust_disc_1.Text)
                        objRow("cust_disc_2") = ToDouble(cust_disc_2.Text)
                        objRow("cust_disc_3") = ToDouble(cust_disc_3.Text)
                        objTable.Rows.Add(objRow)
                    End If
                    dv.RowFilter = ""
                Next
                dtView.RowFilter = ""
                Session("TblDtl") = objTable
                gvDtl.DataSource = objTable
                gvDtl.DataBind()
                If cbOpenListMatUsage.Checked Then
                    btnSearchMat_Click(Nothing, Nothing)
                Else
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                End If
                EnableGridDetail()
                ClearDetail()
                CountHdrAmt()
            Else
                Session("WarningListMat") = "Please select material to add to list!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub lkbSelectAllToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbSelectAllToListListMat.Click
        If Session("TblMatView") IsNot Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matoid=" & dtTbl.Rows(C1)("matoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                lkbAddToListListMat_Click(Nothing, Nothing)
            Else
                Session("WarningListMat") = "Please show Material data first!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Please show Material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            Dim dQty_unitkecil As Double = 0
            Dim dQty_unitbesar As Double = 0
            Dim objTable As DataTable = Session("TblDtl")
            Dim objRow As DataRow = objTable.Rows(transdtlseq.Text - 1)
            objRow.BeginEdit()
            objRow("matcode") = matcode.Text
            objRow("matoid") = matoid.Text
            objRow("matoid_sel") = matoid_sel.Text
            objRow("itemoldcode") = oldcode.Text
            objRow("matlongdesc") = matlongdesc.Text
            objRow("transqty") = ToDouble(transqty.Text)
            objRow("salesprice") = ToDouble(salesprice.Text)
            objRow("totalamt") = ToDouble(salesprice.Text) * ToDouble(transqty.Text)
            objRow("transunitoid") = transunitoid.SelectedValue
            objRow("transunit") = transunitoid.SelectedItem.Text
            objRow("transdtlnote") = transdtlnote.Text
            objRow("trans_ref_d_id") = trans_ref_d_id.Text
            objRow("trans_ref_id") = trans_ref_id.Text
            objRow("cust_disc_1") = ToDouble(cust_disc_1.Text)
            objRow("cust_disc_2") = ToDouble(cust_disc_2.Text)
            objRow("cust_disc_3") = ToDouble(cust_disc_3.Text)

            objRow.EndEdit()
            Session("TblDtl") = objTable
            gvDtl.DataSource = Session("TblDtl")
            gvDtl.DataBind()
            ClearDetail()
            CountHdrAmt()
            EnableGridDetail()
            btnSearchMat.Visible = True : lbkGantiItem.Visible = False
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 2)
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 2)
            e.Row.Cells(13).Text = ToMaskEdit(ToDouble(e.Row.Cells(13).Text), 2)
        End If
    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDtl.SelectedIndexChanged
        Try
            transdtlseq.Text = gvDtl.SelectedDataKey.Item("transdtlseq").ToString().Trim
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "transdtlseq=" & transdtlseq.Text
                transfromwhoid.SelectedValue = dv(0)("transfromwhoid").ToString
                transfromwhoid_SelectedIndexChanged(Nothing, Nothing)
                transtowhoid.SelectedValue = dv(0)("transtowhoid").ToString
                matoid.Text = dv(0)("matoid").ToString
                matoid_sel.Text = dv(0)("matoid_sel").ToString
                matcode.Text = dv(0)("matcode").ToString
                oldcode.Text = dv(0)("itemoldcode").ToString
                matlongdesc.Text = dv(0)("matlongdesc").ToString
                stockqty.Text = ToMaskEdit(ToDouble(dv(0)("stockqty").ToString), 2)
                transqty.Text = ToMaskEdit(ToDouble(dv(0)("transqty").ToString), 2)
                transunitoid.SelectedValue = dv(0)("transunitoid").ToString
                transdtlnote.Text = dv(0)("transdtlnote").ToString
                stockunitoid.Text = dv(0)("stockunitoid").ToString
                salesprice.Text = ToMaskEdit(ToDouble(dv(0)("salesprice").ToString), 2)
                trans_ref_id.Text = dv(0)("trans_ref_id").ToString
                trans_ref_d_id.Text = dv(0)("trans_ref_d_id").ToString
                cust_disc_1.Text = dv(0)("cust_disc_1").ToString
                cust_disc_2.Text = dv(0)("cust_disc_2").ToString
                cust_disc_3.Text = dv(0)("cust_disc_3").ToString

                dv.RowFilter = ""
                'transfromwhoid.Enabled = False : transfromwhoid.CssClass = "inpTextDisabled"
                'transtowhoid.Enabled = False : transtowhoid.CssClass = "inpTextDisabled"
                btnSearchMat.Visible = False : lbkGantiItem.Visible = True
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub gvDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("transdtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        ClearDetail()
        CountHdrAmt()
        EnableGridDetail()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_trntransitemmst WHERE transitemmstoid=" & transmstoid.Text
                If CheckDataExists(sSql) Then
                    transmstoid.Text = GenerateID("QL_TRNTRANSITEMMST", CompnyCode)
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNTRANSITEMMST", "transitemmstoid", transmstoid.Text, "transitemmststatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    transmststatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            transdtloid.Text = GenerateID("QL_TRNTRANSITEMDTL", CompnyCode)
            Dim conmtroid As Integer = GenerateID("QL_CONSTOCK", CompnyCode)
            Dim crdmatoid As Integer = GenerateID("QL_CRDSTOCK", CompnyCode)
            Dim glmstoid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
            Dim gldtloid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
            Dim iStockValOid As Integer = GenerateID("QL_STOCKVALUE", CompnyCode)
            Dim dTotalIDR As Double, dTotalUSD As Double
            Dim cRate As New ClassRate
            Dim sTgl As String = Format(GetServerTime(), "MM/dd/yyyy")
            periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
            Dim objValue As VarUsageValue

            Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sTgl))

            If transmststatus.Text = "Post" Then
                If isPeriodClosed(DDLBusUnit.SelectedValue, sPeriod) Then
                    showMessage("Cannot continue this transaction anymore because the period : " & MonthName(CInt(sPeriod.Substring(4, 2))) & " " & sPeriod.Substring(0, 2) & " have been closed.", 3)
                    transmststatus.Text = "In Process"
                    Exit Sub
                End If
                cRate.SetRateValue("IDR", sTgl)
                If cRate.GetRateDailyLastError <> "" Then
                    showMessage(cRate.GetRateDailyLastError, 2)
                    transmststatus.Text = "In Process"
                    Exit Sub
                End If
                If cRate.GetRateMonthlyLastError <> "" Then
                    showMessage(cRate.GetRateMonthlyLastError, 2)
                    transmststatus.Text = "In Process"
                    Exit Sub
                End If
                Dim sVarErr As String = ""
                If Not IsInterfaceExists("VAR_STOCK", DDLBusUnit.SelectedValue) Then
                    sVarErr = "VAR_STOCK"
                End If
                If sVarErr <> "" Then
                    showMessage(GetInterfaceWarning(sVarErr, "posting"), 2)
                    transmststatus.Text = "In Process"
                    Exit Sub
                End If
                GenerateNo()
                SetUsageValue(objValue)
            End If

            ' Define Tabel Strucure utk Auto Jurnal 
            sSql = "SELECT '' tipe, 0 acctgoid, 0.0 debet, 0.0 credit, '' refno, '' note, '' AS glother1 "
            Dim tbPostGL As DataTable = cKon.ambiltabel(sSql, "PostGL")
            tbPostGL.Rows.Clear()
            Dim oRow As DataRow
            Dim dvCek As DataView = tbPostGL.DefaultView

            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                Dim total_sales As Double = 0
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        total_sales += ToDouble(objTable.Rows(C1)("salesprice").ToString) * ToDouble(objTable.Rows(C1)("transqty").ToString)
                    Next
                End If

                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trntransitemmst (cmpcode, transitemmstoid, periodacctg, transitemdate, transitemno, transitemdocrefno, transitemmstnote, transitemmststatus, createuser, createtime, upduser, updtime, transitemmstto, transitemmstaddr, transitemmstcityOid, transitemmstres3, iskonsinyasi, cons_type, person_id) VALUES ('" & DDLBusUnit.SelectedValue & "', " & transmstoid.Text & ", '" & periodacctg.Text & "', '" & transdate.Text & "', '" & transno.Text & "', '" & Tchar(transdocrefno.Text) & "', '" & Tchar(transmstnote.Text) & "', '" & transmststatus.Text & "', '" & createuser.Text & "', '" & createtime.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Tchar(transitemmstto.Text) & "', '" & Tchar(transitemmstaddr.Text) & "', " & transitemmstcityOid.SelectedValue & ", " & IIf(cbCons.Checked, custoid.Text, suppoid.SelectedValue) & ", '" & cbCons.Checked & "', '" & cons_type.SelectedValue & "', " & person_id.SelectedValue & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & transmstoid.Text & " WHERE tablename='QL_TRNTRANSITEMMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trntransitemmst SET periodacctg='" & periodacctg.Text & "', transitemdate='" & transdate.Text & "', transitemno='" & transno.Text & "', transitemdocrefno='" & Tchar(transdocrefno.Text) & "', transitemmstnote='" & Tchar(transmstnote.Text) & "', transitemmststatus='" & transmststatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, transitemmstto='" & Tchar(transitemmstto.Text) & "', transitemmstaddr='" & Tchar(transitemmstaddr.Text) & "', transitemmstcityOid=" & transitemmstcityOid.SelectedValue & ", transitemmstres3=" & IIf(cbCons.Checked, custoid.Text, suppoid.SelectedValue) & ", iskonsinyasi='" & cbCons.Checked & "', cons_type='" & cons_type.SelectedValue & "', person_id=" & person_id.SelectedValue & " WHERE transitemmstoid=" & transmstoid.Text
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trntransitemdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND transitemmstoid=" & transmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    If total_sales > 0 And cbCons.Checked Then
                        sSql = "update QL_mstcust set limit_usage=limit_usage " & IIf(cons_type.SelectedValue.ToLower() = "transfer", "+", "-") & ToDouble(total_sales) & " where custoid=" & custoid.Text
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                End If

                If total_sales > 0 And cbCons.Checked Then
                    sSql = "update QL_mstcust set limit_usage=limit_usage " & IIf(cons_type.SelectedValue.ToLower() = "transfer", "-", "+") & ToDouble(total_sales) & " where custoid=" & custoid.Text
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trntransitemdtl (cmpcode, transitemdtloid, transitemmstoid, transitemdtlseq, transitemfromwhoid, transitemtowhoid, itemoid, transitemqty, transitemunitoid, transitemdtlnote, transitemdtlstatus, upduser, updtime, transitemvalueidr, transitemvalueusd, salesprice, trans_ref_d_id, trans_ref_id, cust_disc_1, cust_disc_2, cust_disc_3) VALUES ('" & CompnyCode & "', " & (C1 + CInt(transdtloid.Text)) & ", " & transmstoid.Text & ", " & C1 + 1 & ", " & objTable.Rows(C1)("transfromwhoid") & ", " & objTable.Rows(C1)("transtowhoid") & ", " & objTable.Rows(C1)("matoid") & ", " & ToDouble(objTable.Rows(C1)("transqty").ToString) & ", " & objTable.Rows(C1)("transunitoid") & ", '" & Tchar(objTable.Rows(C1)("transdtlnote").ToString) & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(C1)("transvalueidr").ToString) & ", " & ToDouble(objTable.Rows(C1)("transvalueusd").ToString) & ", " & ToDouble(objTable.Rows(C1)("salesprice").ToString) & ", " & ToInteger(objTable.Rows(C1)("trans_ref_d_id").ToString) & ", " & ToInteger(objTable.Rows(C1)("trans_ref_id").ToString) & ", " & ToDouble(objTable.Rows(C1)("cust_disc_1").ToString) & ", " & ToDouble(objTable.Rows(C1)("cust_disc_2").ToString) & ", " & ToDouble(objTable.Rows(C1)("cust_disc_3").ToString) & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        ' Hitung Value Item
                        dTotalIDR += ToDouble(objTable.Rows(C1)("transvalueidr").ToString) * ToDouble(objTable.Rows(C1)("transqty").ToString)
                        dTotalUSD += ToDouble(objTable.Rows(C1)("transvalueusd").ToString) * ToDouble(objTable.Rows(C1)("transqty").ToString)
                        If transmststatus.Text = "Post" Then
                            If cbCons.Checked And cons_type.SelectedValue.ToLower() = "return" Then
                                sSql = "update QL_trntransitemdtl set qty_return=qty_return + " & ToDouble(objTable.Rows(C1)("transqty").ToString) & " where transitemdtloid=" & ToInteger(objTable.Rows(C1)("trans_ref_d_id").ToString)
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            End If

                            ' Insert QL_constock In
                            sSql = "INSERT INTO QL_constock (cmpcode ,constockoid ,contype ,trndate ,formaction ,formoid ,periodacctg ,refname ,refoid ,mtrlocoid ,qtyin ,qtyout ,amount ,hpp ,typemin ,note ,reason ,createuser ,createtime ,upduser ,updtime ,refno ,deptoid,valueidr,valueusd) VALUES ('" & CompnyCode & "'," & conmtroid & ",'TWIN','" & sTgl & "','QL_trntransitemdtl','" & transmstoid.Text & "','" & sPeriod & "','" & objTable.Rows(C1)("refname") & "'," & objTable.Rows(C1)("matoid") & "," & objTable.Rows(C1)("transtowhoid") & "," & ToDouble(objTable.Rows(C1)("transqty").ToString) & ", 0, 0, " & ToDouble(objTable.Rows(C1)("salesprice").ToString) & ", 1,'" & transno.Text & " # DARI " & objTable.Rows(C1)("transfromwh") & "', 'TRANSFER WAREHOUSE','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & transno.Text & "', 0," & ToDouble(objTable.Rows(C1).Item("transvalueidr").ToString) & "," & ToDouble(objTable.Rows(C1).Item("transvalueusd").ToString) & ")"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            conmtroid += 1

                            sSql = "UPDATE QL_mstoid SET lastoid=" & conmtroid & " WHERE tablename='QL_CONSTOCK' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                            ' Insert QL_conmat Out
                            sSql = "INSERT INTO QL_constock (cmpcode ,constockoid ,contype ,trndate ,formaction ,formoid ,periodacctg ,refname ,refoid ,mtrlocoid ,qtyin ,qtyout ,amount ,hpp ,typemin ,note ,reason ,createuser ,createtime ,upduser ,updtime ,refno ,deptoid,valueidr,valueusd) VALUES ('" & CompnyCode & "'," & conmtroid & ",'TWOUT','" & sTgl & "','QL_trntransitemdtl','" & transmstoid.Text & "','" & sPeriod & "','" & objTable.Rows(C1)("refname") & "'," & objTable.Rows(C1)("matoid") & "," & objTable.Rows(C1)("transfromwhoid") & ", 0, " & ToDouble(objTable.Rows(C1)("transqty").ToString) & ", 0, 0, -1,'" & transno.Text & " # KE " & objTable.Rows(C1)("transtowh") & "', 'TRANSFER WAREHOUSE','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & transno.Text & "',0," & ToDouble(objTable.Rows(C1).Item("transvalueidr").ToString) & "," & ToDouble(objTable.Rows(C1).Item("transvalueusd").ToString) & ")"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            conmtroid += 1
                            sSql = "UPDATE QL_mstoid SET lastoid=" & conmtroid & " WHERE tablename='QL_CONSTOCK' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        End If

                        dvCek.RowFilter = "acctgoid=" & objTable.Rows(C1).Item("stockacctgoid").ToString & " "
                        If dvCek.Count > 0 Then
                            dvCek(0)("credit") += ToDouble(objTable.Rows(C1).Item("transvalueidr").ToString) * ToDouble(objTable.Rows(C1).Item("transqty").ToString)
                            dvCek(0)("debet") += ToDouble(objTable.Rows(C1).Item("transvalueidr").ToString) * ToDouble(objTable.Rows(C1).Item("transqty").ToString)
                            dvCek.RowFilter = ""
                        Else
                            dvCek.RowFilter = ""

                            oRow = tbPostGL.NewRow
                            oRow("acctgoid") = objTable.Rows(C1).Item("stockacctgoid")
                            oRow("debet") = ToDouble(objTable.Rows(C1).Item("transvalueidr").ToString) * ToDouble(objTable.Rows(C1).Item("transqty").ToString)
                            oRow("credit") = ToDouble(objTable.Rows(C1).Item("transvalueidr").ToString) * ToDouble(objTable.Rows(C1).Item("transqty").ToString)

                            tbPostGL.Rows.Add(oRow)
                        End If
                        tbPostGL.AcceptChanges()
                    Next

                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(transdtloid.Text)) & " WHERE tablename='QL_trntransitemdtl' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    If transmststatus.Text = "Post" Then
                        If dTotalIDR > 0 Or dTotalUSD > 0 Then
                            ' Insert GL MST
                            sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & CompnyCode & "', " & glmstoid & ", '" & sTgl & "', '" & sPeriod & "', 'Transfer WH|No. " & transno.Text & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 0, 0, 1, 1, ISNULL((" & dTotalUSD & "/NULLIF(" & dTotalIDR & ", 0)), 0), ISNULL((" & dTotalUSD & "/NULLIF(" & dTotalIDR & ", 0)), 0))"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            ' Insert GL DTL
                            Dim iSeq As Integer = 1
                            For C1 As Integer = 0 To tbPostGL.Rows.Count - 1
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & CompnyCode & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & tbPostGL.Rows(C1)("acctgoid") & ", 'D', " & ToDouble(tbPostGL.Rows(C1)("debet").ToString) & ", '" & transno.Text & "', 'Transfer WH|No. " & transno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(tbPostGL.Rows(C1)("debet").ToString) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(tbPostGL.Rows(C1)("debet").ToString) * cRate.GetRateMonthlyUSDValue & ", 'QL_trntransitemmst " & transmstoid.Text & "')"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                gldtloid += 1
                                iSeq += 1

                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & CompnyCode & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & tbPostGL.Rows(C1)("acctgoid") & ", 'C', " & ToDouble(tbPostGL.Rows(C1)("credit").ToString) & ", '" & transno.Text & "', 'Transfer WH|No. " & transno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(tbPostGL.Rows(C1)("credit").ToString) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(tbPostGL.Rows(C1)("credit").ToString) * cRate.GetRateMonthlyUSDValue & ", 'QL_trntransitemmst " & transmstoid.Text & "')"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                gldtloid += 1
                                iSeq += 1
                            Next

                            sSql = "UPDATE QL_mstoid SET lastoid=" & glmstoid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_mstoid SET lastoid=" & gldtloid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        End If
                    End If
                End If
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        transmststatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    transmststatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 2)
                conn.Close()
                transmststatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & transmstoid.Text & ".<BR>"
            End If
            If transmststatus.Text = "Post" Then
                Session("SavedInfo") &= "Data have been posted with Transfer No. = " & transno.Text & "."
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trnTW.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnTW.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If transmstoid.Text = "" Then
            showMessage("Please select Finish Good Transfer data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNTRANSITEMMST", "transitemmstoid", transmstoid.Text, "transitemmststatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                transmststatus.Text = "In Process"
                Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE FROM QL_trntransitemdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND transitemmstoid=" & transmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trntransitemmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND transitemmstoid=" & transmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnTW.aspx?awal=true")
    End Sub

    Protected Sub btnposting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnposting.Click
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        If isPeriodClosed(CompnyCode, CDate(sDate)) Then
            showMessage("Period " & GetDateToPeriodAcctg(CDate(sDate)) & " Have been Already CLOSED, Please CANCEL this transaction and try again!", 2)
            Exit Sub
        End If
        transmststatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLFromWH()
    End Sub

    Protected Sub suppoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not cbCons.Checked Then
            If suppoid.Items.Count > 0 Then
                transitemmstaddr.Text = GetStrData("SELECT suppaddr FROM QL_mstsupp WHERE suppoid=" & suppoid.SelectedValue)
            Else
                transitemmstaddr.Text = ""
            End If
        Else
            If suppoid.Items.Count > 0 Then
                transitemmstaddr.Text = GetStrData("SELECT custaddr FROM QL_mstcust WHERE custoid=" & suppoid.SelectedValue)
            Else
                transitemmstaddr.Text = ""
            End If
        End If
        EnableGridDetail()
    End Sub

    Protected Sub salesprice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Text = ToMaskEdit(ToDouble(salesprice.Text), 2)
    End Sub

    Protected Sub cbCons_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitDDLTransferTo(cbCons.Checked)
        suppoid.Visible = Not cbCons.Checked
        suppoid2.Visible = cbCons.Checked : btnSearchSupp.Visible = cbCons.Checked : btnEraseSupp.Visible = cbCons.Checked
        cons_type.SelectedIndex = -1 : cons_type.Visible = cbCons.Checked
        transtowhoid.Enabled = True : transfromwhoid.Enabled = True : transtowhoid.CssClass = "inpText" : transfromwhoid.CssClass = "inpText"
        If cbCons.Checked Then
            If cons_type.SelectedValue = "TRANSFER" Then
                transtowhoid.Enabled = False : transtowhoid.CssClass = "inpTextDisabled"
            Else
                transfromwhoid.Enabled = False : transfromwhoid.CssClass = "inpTextDisabled"
            End If
        End If
    End Sub

    Protected Sub btnEraseSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        suppoid2.Text = "" : custoid.Text = "" : transitemmstaddr.Text = ""
        gvCust.DataSource = Nothing : gvCust.DataBind() : gvCust.Visible = False
    End Sub

    Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindDataCustomer()
    End Sub

    Protected Sub gvCust_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        suppoid2.Text = gvCust.SelectedDataKey("custname")
        custoid.Text = gvCust.SelectedDataKey("custoid")
        transitemmstaddr.Text = gvCust.SelectedDataKey("custaddr")
        person_id.SelectedValue = gvCust.SelectedDataKey("person_id")
        If cbCons.Checked Then
            If cons_type.SelectedValue = "TRANSFER" Then
                transtowhoid.SelectedValue = gvCust.SelectedDataKey("warehouse_id")
            Else
                transfromwhoid.SelectedValue = gvCust.SelectedDataKey("warehouse_id")
            End If
        End If
        gvCust.DataSource = Nothing : gvCust.DataBind() : gvCust.Visible = False
    End Sub

    Protected Sub gvCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvCust.PageIndex = e.NewPageIndex
        gvCust.DataSource = Session("TblCust")
        gvCust.DataBind()
    End Sub

    Protected Sub gvListMatMix_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMatMix.PageIndexChanging
        gvListMatMix.PageIndex = e.NewPageIndex
        BindListMatMix()
        mpeListMatMix.Show()
    End Sub

    Protected Sub gvListMatMix_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListMatMix.SelectedIndexChanged
        matoid.Text = gvListMatMix.SelectedDataKey.Item("matoid_mix").ToString
		matcode.Text = gvListMatMix.SelectedDataKey.Item("matcode_mix").ToString
        matlongdesc.Text = gvListMatMix.SelectedDataKey.Item("matlongdesc_mix").ToString
        transunitoid.SelectedValue = gvListMatMix.SelectedDataKey.Item("unitoid_mix")
        salesprice.Text = ToMaskEdit(ToDouble(gvListMatMix.SelectedDataKey.Item("salesprice").ToString), 2)
        stockqty.Text = ToMaskEdit(ToDouble(gvListMatMix.SelectedDataKey.Item("stockqty").ToString), 2)
        cProc.SetModalPopUpExtender(btnHideListMatMix, pnlListMatMix, mpeListMatMix, False)
    End Sub
#End Region

    Protected Sub lbkGantiItem_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        FilterDDLListMatMix.SelectedIndex = -1 : FilterTextListMatMix.Text = "" : gvListMatMix.SelectedIndex = -1
        BindListMatMix()
        cProc.SetModalPopUpExtender(btnHideListMatMix, pnlListMatMix, mpeListMatMix, True)
    End Sub

    Protected Sub btnFindListMatMix_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindListMatMix()
        mpeListMatMix.Show()
    End Sub

    Protected Sub btnAllListMatMix_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListMatMix.SelectedIndex = -1 : FilterTextListMatMix.Text = "" : gvListMatMix.SelectedIndex = -1
        BindListMatMix()
        mpeListMatMix.Show()
    End Sub

    Protected Sub gvListMatMix_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 2)
        End If
    End Sub

    Protected Sub cons_type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        transtowhoid.Enabled = True : transfromwhoid.Enabled = True : transtowhoid.CssClass = "inpText" : transfromwhoid.CssClass = "inpText"
        If cbCons.Checked Then
            If cons_type.SelectedValue = "TRANSFER" Then
                transtowhoid.Enabled = False : transtowhoid.CssClass = "inpTextDisabled"
            Else
                transfromwhoid.Enabled = False : transfromwhoid.CssClass = "inpTextDisabled"
            End If
        End If
    End Sub

    Protected Sub ImbUpdSales_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "update QL_trntransitemmst set person_id=" & person_id.SelectedValue & " where transitemmstoid=" & transmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnTW.aspx?awal=true")
    End Sub
End Class
