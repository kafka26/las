Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class Transaction_MaterialInitStock
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Functions"
    Private Function IsFilterValid()
        Dim sError As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If DDLLocation.SelectedValue = "" Then
            sError &= "- Please select INIT LOCATION field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetTextValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitAllDDL()
        'Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If FillDDL(DDLBusUnit, sSql) Then
            InitDDLLocation()
        End If
    End Sub

    Private Sub InitDDLLocation()
        'Fill DDL Init Location
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND genother1='" & DDLBusUnit.SelectedValue & "' AND genoid>0"
        If DDLType.SelectedValue = "Finish Good" Then
            sSql &= " AND gengroup='ITEM LOCATION'"
        Else
            sSql &= " AND gengroup='MATERIAL LOCATION'"
        End If
        sSql &= " ORDER BY gendesc"
        FillDDL(DDLLocation, sSql)
    End Sub

    Private Sub BindMaterial()
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
        Dim sType As String = "", sRef As String = ""
        If DDLType.SelectedValue = "Raw" Then
            sType = "matraw" : sRef = "RAW MATERIAL"
        ElseIf DDLType.SelectedValue = "General" Then
            sType = "matgen" : sRef = "GENERAL MATERIAL"
        ElseIf DDLType.SelectedValue = "Spare Part" Then
            sType = "sparepart" : sRef = "SPARE PART"
        ElseIf DDLType.SelectedValue = "Finish Good" Then
            sType = "item" : sRef = "FINISH GOOD"
        ElseIf DDLType.SelectedValue = "Log" Then
            sRef = "LOG"
        Else
            sRef = "PALLET"
        End If
        If DDLType.SelectedValue = "Log" Then
            sSql = "SELECT logoid AS matoid, logno AS matno, matrawcode + ' - ' + logno AS matcode, matrawlongdesc AS matlongdesc, logvolume AS matqty, gendesc AS matunit, 'False' AS checkvalue, 'False' AS enableqty FROM QL_mstlog m INNER JOIN QL_mstmatraw mx ON matrawoid=m.refoid INNER JOIN QL_mstgen g ON genoid=logunitoid WHERE m.cmpcode='" & DDLBusUnit.SelectedValue & "' AND ISNULL(logres2, '')='' AND logoid NOT IN (SELECT initrefoid FROM QL_initstock init WHERE init.cmpcode='" & DDLBusUnit.SelectedValue & "' AND initreftype='" & sRef & "' AND initwhoid=" & DDLLocation.SelectedValue & ") ORDER BY logno"
            FilterTextListMat.TextMode = TextBoxMode.MultiLine : FilterTextListMat.Rows = 3 : FilterDDLListMat.Items(0).Enabled = True
        ElseIf DDLType.SelectedValue = "Sawn Timber" Then
            sSql = "SELECT palletoid AS matoid, palletno AS matno, matrawcode + ' - ' + palletno AS matcode, palletlongdesc AS matlongdesc, palletvolume AS matqty, gendesc AS matunit, 'False' AS checkvalue, 'False' AS enableqty FROM QL_mstpallet m INNER JOIN QL_mstmatraw mx ON matrawoid=m.refoid INNER JOIN QL_mstgen g ON genoid=palletunitoid WHERE m.cmpcode='" & DDLBusUnit.SelectedValue & "' AND ISNULL(palletres2, '')='' AND palletoid NOT IN (SELECT initrefoid FROM QL_initstock init WHERE init.cmpcode='" & DDLBusUnit.SelectedValue & "' AND initreftype='" & sRef & "' AND initwhoid=" & DDLLocation.SelectedValue & ") ORDER BY palletno"
            FilterTextListMat.TextMode = TextBoxMode.MultiLine : FilterTextListMat.Rows = 3 : FilterDDLListMat.Items(0).Enabled = True
        Else
            sSql = "SELECT " & sType & "oid AS matoid, '' AS matno, " & sType & "code AS matcode, " & sType & "longdesc AS matlongdesc, 0.0 AS matqty, gendesc AS matunit, 'False' AS checkvalue, 'True' AS enableqty FROM QL_mst" & sType & " m INNER JOIN QL_mstgen g ON genoid=" & sType & "unitoid WHERE m.cmpcode='" & CompnyCode & "'" & IIf(sRef = "FINISH GOOD", "", " AND " & sType & "res2='Non Assets'") & " AND " & sType & "oid NOT IN (SELECT initrefoid FROM QL_initstock init WHERE init.cmpcode='" & DDLBusUnit.SelectedValue & "' AND initreftype='" & sRef & "' AND initwhoid=" & DDLLocation.SelectedValue & ") ORDER BY " & sType & "code"
            FilterTextListMat.TextMode = TextBoxMode.SingleLine : FilterTextListMat.Rows = 0 : FilterDDLListMat.Items(0).Enabled = False
        End If
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstmat")
        If dt.Rows.Count > 0 Then
            Session("TblListMat") = dt
            Session("TblListMatView") = Session("TblListMat")
            gvListMat.DataSource = Session("TblListMatView")
            gvListMat.DataBind()
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            showMessage("Material data can't be found!", 2)
        End If
    End Sub

    Private Sub InitDDLCat1()
        'Fill DDL Category 1
        sSql = "SELECT cat1code, cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        If DDLType.SelectedValue = "Raw" Then
            sSql &= " AND cat1res1='Raw' AND cat1res2 IN ('WIP', 'Non WIP')"
        ElseIf DDLType.SelectedValue = "Log" Then
            sSql &= " AND cat1res1='Raw' AND cat1res2 IN ('Log')"
        ElseIf DDLType.SelectedValue = "Sawn Timber" Then
            sSql &= " AND cat1res1='Raw' AND cat1res2 IN ('Sawn Timber')"
        Else
            sSql &= " AND cat1res1='" & DDLType.SelectedValue & "'"
        End If
        If FillDDL(DDLCat01, sSql) Then
            InitDDLCat2()
        Else
            DDLCat02.Items.Clear()
            DDLCat03.Items.Clear()
            DDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat2()
        'Fill DDL Category 2
        sSql = "SELECT cat1code + '.' + cat2code, cat2shortdesc FROM QL_mstcat2 c2 INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c1.cat1oid=c2.cat1oid WHERE c2.cmpcode='" & CompnyCode & "' AND c2.activeflag='ACTIVE' AND cat1code='" & DDLCat01.SelectedValue & "'"
        If DDLType.SelectedValue = "Raw" Then
            sSql &= " AND cat2res1='Raw' AND cat1res1='Raw' AND cat1res2 IN ('WIP', 'Non WIP')"
        ElseIf DDLType.SelectedValue = "Log" Then
            sSql &= " AND cat2res1='Raw' AND cat1res1='Raw' AND cat1res2 IN ('Log')"
        ElseIf DDLType.SelectedValue = "Sawn Timber" Then
            sSql &= " AND cat2res1='Raw' AND cat1res1='Raw' AND cat1res2 IN ('Sawn Timber')"
        Else
            sSql &= " AND cat2res1='" & DDLType.SelectedValue & "' AND cat1res1='" & DDLType.SelectedValue & "'"
        End If
        If FillDDL(DDLCat02, sSql) Then
            InitDDLCat3()
        Else
            DDLCat03.Items.Clear()
            DDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat3()
        'Fill DDL Category 3
        sSql = "SELECT cat1code + '.' + cat2code + '.' + cat3code, cat3shortdesc FROM QL_mstcat3 c3 INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c3.cmpcode AND c1.cat1oid=c3.cat1oid WHERE c3.cmpcode='" & CompnyCode & "' AND c3.activeflag='ACTIVE' AND cat1code + '.' + cat2code='" & DDLCat02.SelectedValue & "'"
        If DDLType.SelectedValue = "Raw" Then
            sSql &= " AND cat3res1='Raw' AND cat2res1='Raw' AND cat1res1='Raw' AND cat1res2 IN ('WIP', 'Non WIP')"
        ElseIf DDLType.SelectedValue = "Log" Then
            sSql &= " AND cat3res1='Raw' AND cat2res1='Raw' AND cat1res1='Raw' AND cat1res2 IN ('Log')"
        ElseIf DDLType.SelectedValue = "Sawn Timber" Then
            sSql &= " AND cat3res1='Raw' AND cat2res1='Raw' AND cat1res1='Raw' AND cat1res2 IN ('Sawn Timber')"
        Else
            sSql &= " AND cat3res1='" & DDLType.SelectedValue & "' AND cat2res1='" & DDLType.SelectedValue & "' AND cat1res1='" & DDLType.SelectedValue & "'"
        End If
        If FillDDL(DDLCat03, sSql) Then
            InitDDLCat4()
        Else
            DDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat4()
        'Fill DDL Category 3
        sSql = "SELECT cat1code + '.' + cat2code + '.' + cat3code + '.' + cat4code, cat4shortdesc FROM QL_mstcat4 c4 INNER JOIN QL_mstcat3 c3 ON c3.cmpcode=c4.cmpcode AND c3.cat3oid=c4.cat3oid INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c4.cmpcode AND c2.cat2oid=c4.cat2oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c4.cmpcode AND c1.cat1oid=c4.cat1oid WHERE c4.cmpcode='" & CompnyCode & "' AND c4.activeflag='ACTIVE' AND cat1code + '.' + cat2code + '.' + cat3code='" & DDLCat03.SelectedValue & "'"
        If DDLType.SelectedValue = "Raw" Then
            sSql &= " AND cat4res1='Raw' AND cat3res1='Raw' AND cat2res1='Raw' AND cat1res1='Raw' AND cat1res2 IN ('WIP', 'Non WIP')"
        ElseIf DDLType.SelectedValue = "Log" Then
            sSql &= " AND cat4res1='Raw' AND cat3res1='Raw' AND cat2res1='Raw' AND cat1res1='Raw' AND cat1res2 IN ('Log')"
        ElseIf DDLType.SelectedValue = "Sawn Timber" Then
            sSql &= " AND cat4res1='Raw' AND cat3res1='Raw' AND cat2res1='Raw' AND cat1res1='Raw' AND cat1res2 IN ('Sawn Timber')"
        Else
            sSql &= " AND cat4res1='" & DDLType.SelectedValue & "' AND cat3res1='" & DDLType.SelectedValue & "' AND cat2res1='" & DDLType.SelectedValue & "' AND cat1res1='" & DDLType.SelectedValue & "'"
        End If
        FillDDL(DDLCat04, sSql)
    End Sub

    Private Sub UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                            dv(0)("matqty") = ToDouble(GetTextValue(row.Cells(3).Controls))
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMat") = dt
        End If
        If Session("TblListMatView") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMatView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                            dv(0)("matqty") = ToDouble(GetTextValue(row.Cells(3).Controls))
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMatView") = dt
        End If
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("TableDetail")
        dtlTable.Columns.Add("seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("mattype", Type.GetType("System.String"))
        dtlTable.Columns.Add("matwhoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matwh", Type.GetType("System.String"))
        dtlTable.Columns.Add("matoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("matlongdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("matqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("matunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("enableqty", Type.GetType("System.String"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal : DDLBusUnit.CssClass = sCss
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnMIS.aspx")
        End If
        If checkPagePermission("~\Transaction\trnMIS.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        btnPost.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POSTING this data?');")
        upduser.Text = Session("UserID") : updtime.Text = GetServerTime().ToString
        Page.Title = CompnyName & " - Material Init Stock"
        If Not Page.IsPostBack Then
            InitAllDDL()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                mpeListMat.Show()
            End If
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLLocation()
    End Sub

    Protected Sub DDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLType.SelectedIndexChanged
        InitDDLLocation()
        If DDLType.SelectedIndex > 3 Then
            FilterDDLListMat.Items(0).Enabled = True
            FilterTextListMat.Rows = 3 : FilterTextListMat.TextMode = TextBoxMode.MultiLine
        Else
            FilterDDLListMat.Items(0).Enabled = False
            FilterTextListMat.Rows = 0 : FilterTextListMat.TextMode = TextBoxMode.SingleLine
        End If
        FilterDDLListMat.SelectedIndex = -1
    End Sub

    Protected Sub btnLookUpMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLookUpMat.Click
        If IsFilterValid() Then
            FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : InitDDLCat1()
            cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
            Session("TblListMat") = Nothing : Session("TblListMatView") = Nothing : gvListMat.DataSource = Session("TblListMat") : gvListMat.DataBind()
            BindMaterial()
        End If
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                Dim dv As DataView = dt.DefaultView
                Dim sFilter As String = ""
                If DDLType.SelectedIndex > 3 Then
                    If FilterDDLListMat.SelectedIndex = 0 Then
                        Dim sText() As String = FilterTextListMat.Text.Split(";")
                        For C1 As Integer = 0 To sText.Length - 1
                            If sText(C1) <> "" Then
                                sFilter &= FilterDDLListMat.SelectedValue & " LIKE '" & Tchar(sText(C1)) & "' OR "
                            End If
                        Next
                        If sFilter <> "" Then
                            sFilter = "(" & Left(sFilter, sFilter.Length - 4) & ")"
                        Else
                            sFilter = "1=1"
                        End If
                    Else
                        sFilter = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
                    End If
                Else
                    sFilter = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
                End If
                If cbCat04.Checked And DDLCat04.SelectedValue <> "" Then
                    sFilter &= " AND matcode LIKE '" & DDLCat04.SelectedValue & "%'"
                Else
                    If cbCat03.Checked And DDLCat03.SelectedValue <> "" Then
                        sFilter &= " AND matcode LIKE '" & DDLCat03.SelectedValue & "%'"
                    Else
                        If cbCat02.Checked And DDLCat02.SelectedValue <> "" Then
                            sFilter &= " AND matcode LIKE '" & DDLCat02.SelectedValue & "%'"
                        Else
                            If cbCat01.Checked And DDLCat01.SelectedValue <> "" Then
                                sFilter &= " AND matcode LIKE '" & DDLCat01.SelectedValue & "%'"
                            End If
                        End If
                    End If
                End If
                dv.RowFilter = sFilter
                Session("TblListMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblListMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : InitDDLCat1()
                cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
                Session("TblListMatView") = Session("TblListMat")
                gvListMat.DataSource = Session("TblListMatView")
                gvListMat.DataBind()
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub DDLCat01_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLCat01.SelectedIndexChanged
        InitDDLCat2()
        mpeListMat.Show()
    End Sub

    Protected Sub DDLCat02_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLCat02.SelectedIndexChanged
        InitDDLCat3()
        mpeListMat.Show()
    End Sub

    Protected Sub DDLCat03_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLCat03.SelectedIndexChanged
        InitDDLCat4()
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        UpdateCheckedMat()
        gvListMat.PageIndex = e.NewPageIndex
        gvListMat.DataSource = Session("TblListMatView")
        gvListMat.DataBind()
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(3).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 4)
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub cbHdrLM_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For C1 As Integer = 0 To gvListMat.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        mpeListMat.Show()
    End Sub

    Protected Sub lbAddToListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListMat.Click
        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "checkvalue='True'"
                If dv.Count > 0 Then
                    Dim sErr As String = ""
                    For C1 As Integer = 0 To dv.Count - 1
                        If ToDouble(dv(C1)("matqty").ToString) <= 0 Then
                            sErr = "Every Qty of selected data must be more than 0!" : Exit For
                        End If
                    Next
                    If sErr <> "" Then
                        Session("WarningListMat") = sErr
                        showMessage(Session("WarningListMat"), 2) : Exit Sub
                    End If
                    If Session("TblDtl") Is Nothing Then
                        CreateTblDetail()
                    End If
                    Dim objTbl As DataTable = Session("TblDtl")
                    Dim iSeq As Integer = objTbl.Rows.Count + 1
                    Dim objView As DataView = objTbl.DefaultView
                    For C1 As Integer = 0 To dv.Count - 1
                        objView.RowFilter = "mattype='" & DDLType.SelectedValue & "' AND matoid=" & dv(C1)("matoid")
                        If objView.Count > 0 Then
                            objView(0)("matqty") = ToDouble(dv(C1)("matqty").ToString)
                        Else
                            Dim rv As DataRowView = objView.AddNew()
                            rv.BeginEdit()
                            rv("seq") = iSeq
                            rv("mattype") = DDLType.SelectedValue
                            rv("matwhoid") = DDLLocation.SelectedValue
                            rv("matwh") = DDLLocation.SelectedItem.Text
                            rv("matoid") = dv(C1)("matoid").ToString
                            rv("matcode") = dv(C1)("matcode").ToString
                            rv("matlongdesc") = dv(C1)("matlongdesc").ToString
                            rv("matqty") = ToDouble(dv(C1)("matqty").ToString)
                            rv("matunit") = dv(C1)("matunit").ToString
                            If DDLType.SelectedValue.ToUpper = "LOG" Or DDLType.SelectedValue.ToUpper = "SAWN TIMBER" Then
                                rv("enableqty") = "False"
                            Else
                                rv("enableqty") = "True"
                            End If
                            rv.EndEdit()
                            iSeq += 1
                        End If
                        objView.RowFilter = ""
                    Next
                    objTbl.AcceptChanges()
                    Session("TblDtl") = objTbl
                    gvDtl.DataSource = Session("TblDtl")
                    gvDtl.DataBind()
                    EnableHeader(Not (gvDtl.Rows.Count > 0))
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                End If
            Else
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub lbSelectAllToList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSelectAllToList.Click
        If Session("TblListMatView") IsNot Nothing Then
            Dim dtTbl As DataTable = Session("TblListMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblListMat")
                Dim objView As DataView = objTbl.DefaultView
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matoid=" & dtTbl.Rows(C1)("matoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblListMat") = objTbl
                Session("TblListMatView") = dtTbl
                gvListMat.DataSource = Session("TblListMatView")
                gvListMat.DataBind()
                lbAddToListMat_Click(Nothing, Nothing)
            Else
                Session("WarningListMat") = "Please show material data first!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Please show material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub gvDtl_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDtl.PageIndexChanging
        e.Cancel = True
        gvDtl.EditIndex = -1
        gvDtl.PageIndex = e.NewPageIndex
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
    End Sub

    Protected Sub gvDtl_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles gvDtl.RowCancelingEdit
        e.Cancel = True
        gvDtl.EditIndex = -1
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
    End Sub

    Protected Sub gvDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtl.RowDeleting
        e.Cancel = True
        gvDtl.EditIndex = -1
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("seq") = C1 + 1
            dr.EndEdit()
        Next
        objTable.AcceptChanges()
        Session("TblDtl") = objTable
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
    End Sub

    Protected Sub gvDtl_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvDtl.RowEditing
        gvDtl.EditIndex = e.NewEditIndex
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
    End Sub

    Protected Sub gvDtl_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gvDtl.RowUpdating
        Dim row As GridViewRow = gvDtl.Rows(e.RowIndex)
        Dim dQty As Double = ToDouble(CType(row.FindControl("tbQty"), System.Web.UI.WebControls.TextBox).Text)
        Dim iOid As Integer = ToInteger(gvDtl.DataKeys(e.RowIndex).Value.ToString())
        Session("TblDtl").DefaultView.RowFilter = "seq=" & iOid
        Session("TblDtl").DefaultView(0)("matqty") = dQty
        Session("TblDtl").DefaultView.RowFilter = ""
        e.Cancel = True
        gvDtl.EditIndex = -1
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPost.Click
        If Session("TblDtl") Is Nothing Then
            showMessage("Please initiate some material data first!", 2)
            Exit Sub
        End If
        Dim objTable As DataTable = Session("TblDtl")
        If objTable.Rows.Count = 0 Then
            showMessage("Please initiate some material data first!", 2)
            Exit Sub
        End If
        Dim iInitStockOid As Integer = GenerateID("QL_INITSTOCK", CompnyCode)
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            For C1 As Int16 = 0 To objTable.Rows.Count - 1
                Dim sRef As String = ""
                If objTable.Rows(C1).Item("mattype").ToString.ToUpper = "RAW" Then
                    sRef = "RAW MATERIAL"
                ElseIf objTable.Rows(C1).Item("mattype").ToString.ToUpper = "GENERAL" Then
                    sRef = "GENERAL MATERIAL"
                ElseIf objTable.Rows(C1).Item("mattype").ToString.ToUpper = "SPARE PART" Then
                    sRef = "SPARE PART"
                ElseIf objTable.Rows(C1).Item("mattype").ToString.ToUpper = "FINISH GOOD" Then
                    sRef = "FINISH GOOD"
                ElseIf objTable.Rows(C1).Item("mattype").ToString.ToUpper = "LOG" Then
                    sRef = "LOG"
                Else
                    sRef = "PALLET"
                End If
                sSql = "INSERT INTO QL_initstock (cmpcode, initoid, initdate, periodacctg, initreftype, initrefoid, initwhoid, initqty, initflag, upduser, updtime) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iInitStockOid & ", '" & sDate & "', '" & sPeriod & "', '" & sRef & "', " & objTable.Rows(C1).Item("matoid") & ", " & objTable.Rows(C1).Item("matwhoid") & ", " & ToDouble(objTable.Rows(C1).Item("matqty").ToString) & ", 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                iInitStockOid += 1
                If sRef = "LOG" Then
                    sSql = "UPDATE QL_mstlog SET logres2='Used' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND logoid=" & objTable.Rows(C1).Item("matoid")
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                ElseIf sRef = "PALLET" Then
                    sSql = "UPDATE QL_mstpallet SET palletres2='Used' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND palletoid=" & objTable.Rows(C1).Item("matoid")
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
            Next
            sSql = "UPDATE QL_mstoid SET lastoid=" & iInitStockOid - 1 & " WHERE tablename='QL_INITSTOCK' AND cmpcode='" & CompnyCode & "' "
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch exSql As SqlException
            objTrans.Rollback() : conn.Close()
            If ToDouble(Session("ErrorCounter")) < 5 Then
                If exSql.Number = 2627 Then
                    Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                    btnPost_Click(sender, e)
                    Exit Sub
                Else
                    showMessage(exSql.Message, 1)
                    Exit Sub
                End If
            Else
                showMessage(exSql.Message, 1)
                Exit Sub
            End If
        Catch ex As Exception
            objTrans.Rollback()
            showMessage(ex.Message, 1)
            conn.Close()
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnMIS.aspx?awal=true")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnMIS.aspx?awal=true")
    End Sub
#End Region

End Class