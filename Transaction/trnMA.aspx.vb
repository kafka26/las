Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_MaterialAdjustment
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Private cRate As New ClassRate
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2, "")
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2, "")
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2, "")
            Return False
        End If
        Return True
    End Function

    Public Function GetCssClass() As String
        Return IIf((ddlType.SelectedValue = "LOG" Or ddlType.SelectedValue = "PALLET"), "inpTextDisabled", "inpText")
    End Function

    Public Function GetEnabled() As Boolean
        Return Not (ddlType.SelectedValue = "LOG" Or ddlType.SelectedValue = "PALLET")
    End Function

    Public Function GetSelected() As String
        Return Eval("selected")
    End Function

    Public Function GetTransID() As String
        Return (Eval("cmpcode") & "," & Eval("resfield1"))
    End Function

    Public Function GetAdjQty() As String
        Return ToMaskEdit(ToDouble(Eval("adjqty")), 2)
    End Function

    Private Function GetCheckBoxValue(ByVal gvTarget As GridView, ByVal nRowNum As Integer, ByVal nCol As Integer) As Boolean
        Dim bReturn As Boolean = False
        Dim row As System.Web.UI.WebControls.GridViewRow = gvTarget.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    bReturn = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                End If
            Next
        End If
        Return bReturn
    End Function

    Private Function GetTextBoxValue(ByVal nRowNum As Integer, ByVal nCol As Integer) As String
        Dim sReturn As String = ""
        Dim row As System.Web.UI.WebControls.GridViewRow = gvFindCrd.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                End If
            Next
        End If
        Return sReturn
    End Function

    Private Function GetDDLItem(ByVal nRowNum As Integer, ByVal nCol As Integer) As String
        Dim sReturn As String = ""
        Dim row As System.Web.UI.WebControls.GridViewRow = gvFindCrd.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                    If CType(myControl, System.Web.UI.WebControls.DropDownList).Items.Count > 0 Then
                        sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
                    End If
                End If
            Next
        End If
        Return sReturn
    End Function

    Private Function UpdateDetailData() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblCrd") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCrd")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvFindCrd.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvFindCrd.Rows(C1)
                    dtView.RowFilter = "seq=" & row.Cells(0).Text
                    If ToDouble(GetTextBoxValue(C1, 5)) < 0 Or ToDouble(GetTextBoxValue(C1, 5)) > 9999999999.99 Then
                        showMessage("Adjustment quantity must between 0 and 9,999,999,999.99!", 2, "")
                        Return bReturn
                    Else
                        dtView(0)("adjqty") = ToDouble(GetTextBoxValue(C1, 5))
                        dtView(0)("note") = GetTextBoxValue(C1, 7)
                    End If
                    dtView(0)("reason") = GetDDLItem(C1, 6)
                    If (ddlType.SelectedValue = "LOG" Or ddlType.SelectedValue = "PALLET") Then
                        dtView(0)("selected") = GetCheckBoxValue(gvFindCrd, C1, 9)
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl = dtView.ToTable
                Session("TblCrd") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function GetDataFromMutation() As DataTable
        Dim sMatType As String = "", sFieldNo As String = ""
        If ddlType.SelectedValue = "GENERAL MATERIAL" Then
            sMatType = "matgen" : sFieldNo = "''"
        ElseIf ddlType.SelectedValue = "RAW MATERIAL" Then
            sMatType = "matraw" : sFieldNo = "''"
        ElseIf ddlType.SelectedValue = "SPARE PART" Then
            sMatType = "sparepart" : sFieldNo = "''"
        ElseIf ddlType.SelectedValue = "FINISH GOOD" Then
            sMatType = "item" : sFieldNo = "''"
        End If
        sSql = "SELECT 0 seq, " & sMatType & "oid refoid, " & sFieldNo & " matno, " & sMatType & "code matcode, " & sMatType & "longdesc matlongdesc, 0.0 lastqty, '" & FilterWarehouse.SelectedItem.Text & "' location, " & FilterWarehouse.SelectedValue & " mtrwhoid, 0.0 adjqty, '' reason, '' Note, ISNULL((SELECT SUM((ISNULL(stockqty, 0.0) * ISNULL(stockvalueidr, 0.0))) / NULLIF((SUM(ISNULL(stockqty, 0.0))), 0) FROM QL_stockvalue st WHERE st.cmpcode='" & busunit.SelectedValue & "' AND refname='" & ddlType.SelectedValue & "' AND refoid=" & sMatType & "oid AND closeflag='' AND periodacctg IN ('" & GetDateToPeriodAcctg(CDate(stockadjdate.Text)) & "', '" & GetLastPeriod(GetDateToPeriodAcctg(CDate(stockadjdate.Text))) & "')), 0.0) stockvalueidr, ISNULL((SELECT SUM((ISNULL(stockqty, 0.0) * ISNULL(stockvalueusd, 0.0))) / NULLIF((SUM(ISNULL(stockqty, 0.0))), 0) FROM QL_stockvalue st WHERE st.cmpcode='" & busunit.SelectedValue & "' AND refname='" & ddlType.SelectedValue & "' AND refoid=" & sMatType & "oid AND closeflag='' AND periodacctg IN ('" & GetDateToPeriodAcctg(CDate(stockadjdate.Text)) & "', '" & GetLastPeriod(GetDateToPeriodAcctg(CDate(stockadjdate.Text))) & "')), 0.0) stockvalueusd, " & sMatType & "unitoid unitoid, 'False' selected FROM QL_mst" & sMatType & " m WHERE " & sMatType & "" & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' AND ISNULL(" & sMatType & "flag, '')<>''"
        If cbCat01.Checked Then
            If FilterDDLCat01.SelectedValue <> "" Then
                sSql &= " AND SUBSTRING(" & sMatType & "code, 1, 2)='" & FilterDDLCat01.SelectedValue & "'"
            End If
        End If
        If cbCat02.Checked Then
            If FilterDDLCat02.SelectedValue <> "" Then
                sSql &= " AND SUBSTRING(" & sMatType & "code, 4, 3)='" & FilterDDLCat02.SelectedValue & "'"
            End If
        End If
        If cbCat03.Checked Then
            If FilterDDLCat03.SelectedValue <> "" Then
                sSql &= " AND SUBSTRING(" & sMatType & "code, 8, 4)='" & FilterDDLCat03.SelectedValue & "'"
            End If
        End If
        If cbCat04.Checked Then
            If FilterDDLCat04.SelectedValue <> "" Then
                sSql &= " AND SUBSTRING(" & sMatType & "code, 13, 4)='" & FilterDDLCat04.SelectedValue & "'"
            End If
        End If
        sSql &= " ORDER BY " & sMatType & "code, " & sMatType & "longdesc"
        Return cKon.ambiltabel(sSql, "QL_crdmtr_new")
    End Function
#End Region

#Region "Procedures"
    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("QL_trnmrrawdtl")
        dtlTable.Columns.Add("seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("refoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("refname", Type.GetType("System.String"))
        dtlTable.Columns.Add("refcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemoldcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("reflongdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("mtrwhoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("location", Type.GetType("System.String"))
        dtlTable.Columns.Add("lastqty", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("adjqty", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("reasonoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("reason", Type.GetType("System.String"))
        dtlTable.Columns.Add("reasonacctgoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("note", Type.GetType("System.String"))
        dtlTable.Columns.Add("stockvalueidr", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("stockvalueusd", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("unitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("createuser", Type.GetType("System.String"))
        dtlTable.Columns.Add("createdate", Type.GetType("System.DateTime"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub ResetFind()
        Session("TblCrd") = Nothing
        gvFindCrd.DataSource = Nothing
        gvFindCrd.DataBind()
        lblCount.Text = "" : imbAddToList.Visible = False
        btnSelectAll.Visible = False 'imbAddToList.Visible
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvList.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvList.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvList.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next

                    Dim sID() As String = sOid.Split(",")
                    dv.RowFilter = "cmpcode='" & sID(0) & "' AND resfield1='" & sID(1) & "'"
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub BindData(ByVal sSQLPlus As String)
        sSql = "SELECT DISTINCT a.cmpcode,d.divname,CAST(a.resfield1 AS INTEGER) resfield1,a.stockadjno,CONVERT(VARCHAR(10),a.stockadjdate,101) stockadjdate,COUNT(-1) AS adjcount,a.stockadjstatus,'False' AS checkvalue FROM QL_trnstockadj a INNER JOIN QL_mstdivision d ON d.divcode=a.cmpcode "
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " WHERE a.cmpcode='" & Session("CompnyCode") & "' AND a.stockadjstatus LIKE '" & FilterDDLStatus.SelectedValue & "' "
        Else
            sSql &= " WHERE a.cmpcode LIKE '%' AND a.stockadjstatus LIKE '" & FilterDDLStatus.SelectedValue & "' "
        End If
        sSql &= sSQLPlus & " GROUP BY a.cmpcode,d.divname,a.resfield1,a.stockadjno,a.stockadjdate,a.stockadjstatus ORDER BY 3 DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trnstockadj")
        gvList.DataSource = Session("TblMst")
        gvList.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub FillTextBox(ByVal sNo As String)
        Dim sID() As String = sNo.Split(",")
        If sID.Length < 2 Then
            showMessage("Invalid Transaction ID selection.", 3, "REDIR")
        Else
            Dim sMsg As String = ""
            sSql = "SELECT TOP 1 a.cmpcode,a.stockadjno,a.stockadjdate,a.stockadjstatus,a.resfield1,a.createuser,a.createdate,a.upduser,a.updtime,  adjtype AS stockadjtype " & _
                "FROM QL_trnstockadj a INNER JOIN QL_mstdivision d ON d.divcode=a.cmpcode WHERE a.cmpcode='" & sID(0) & "' AND a.resfield1='" & sID(1) & "' "
            Dim tbHdr As DataTable = cKon.ambiltabel(sSql, "Mst")
            If tbHdr.Rows.Count < 1 Then
                sMsg &= "- Can't load transaction header data.<BR>"
            Else
                busunit.SelectedValue = tbHdr.Rows(0)("cmpcode").ToString
                InitWH(ddlType.SelectedValue)
                resfield1.Text = tbHdr.Rows(0)("resfield1").ToString
                stockadjno.Text = tbHdr.Rows(0)("stockadjno").ToString
                stockadjdate.Text = Format(CDate(tbHdr.Rows(0)("stockadjdate").ToString), "MM/dd/yyyy")
                createuser.Text = tbHdr.Rows(0)("createuser").ToString
                createtime.Text = tbHdr.Rows(0)("createdate").ToString
                upduser.Text = tbHdr.Rows(0)("upduser").ToString
                updtime.Text = tbHdr.Rows(0)("updtime").ToString
                stockadjstatus.Text = tbHdr.Rows(0)("stockadjstatus").ToString
                If tbHdr.Rows(0)("stockadjtype").ToString.ToUpper = "ADJUSTMENT" Then
                    rbAdjust.Checked = True : rbInit.Checked = False
                Else
                    rbAdjust.Checked = False : rbInit.Checked = True
                End If
            End If

            sSql = "SELECT 0 seq, a.refoid, a.refname, 'refcode'=(SELECT m.itemCode FROM QL_mstitem m WHERE m.itemoid=a.refoid), 'reflongdesc'=(SELECT m.itemShortDescription FROM QL_mstitem m WHERE m.itemoid=a.refoid), a.mtrwhoid, g1.gendesc location, CONVERT (DECIMAL (18,2), a.stockadjqtybefore) AS lastqty, CONVERT (DECIMAL (18,2), a.stockadjqty) AS adjqty, ISNULL(a.reasonoid,0) AS reasonoid, ISNULL(g2.gendesc,'') AS reason, ISNULL(CAST(g2.genother1 AS INT),0) reasonacctgoid, a.stockadjnote note, a.stockadjamtidr stockvalueidr, a.stockadjamtusd stockvalueusd, a.stockadjunit unitoid, a.createuser, a.createdate, 'itemoldcode'=(SELECT m.itemoldcode FROM QL_mstitem m WHERE m.itemoid=a.refoid) FROM QL_trnstockadj a INNER JOIN QL_mstgen g1 ON g1.genoid=a.mtrwhoid LEFT JOIN QL_mstgen g2 ON g2.genoid=a.reasonoid AND g2.gengroup='ADJUSMENT REASON' WHERE a.cmpcode='" & sID(0) & "' AND a.resfield1='" & sID(1) & "' ORDER BY a.stockadjoid"
            Dim tbDtl As DataTable = cKon.ambiltabel(sSql, "QL_trnstockadj")
            If tbDtl.Rows.Count < 1 Then
                sMsg &= "- Can't load transaction detail data.<BR>"
            Else
                For R1 As Integer = 0 To tbDtl.Rows.Count - 1
                    tbDtl.Rows(R1)("seq") = R1 + 1
                Next
                tbDtl.AcceptChanges()
                Session("TblDtl") = tbDtl : gvDtl.DataSource = Session("TblDtl") : gvDtl.DataBind()
            End If

            If sMsg <> "" Then
                showMessage(sMsg, 2, "")
            Else
                busunit.Enabled = False
                busunit.CssClass = "inpTextDisabled"
                If stockadjstatus.Text <> "In Process" And stockadjstatus.Text <> "Revised" Then
                    lblNo.Text = "Adj. No"
                    resfield1.Visible = False : stockadjno.Visible = True
                    imbFind.Visible = False
                    imbSave.Visible = False : imbPosting.Visible = False : imbDelete.Visible = False
                    gvDtl.Columns(10).Visible = False
                Else
                    lblNo.Text = "Draft No"
                    resfield1.Visible = True : stockadjno.Visible = False
                    imbFind.Visible = True
                    imbSave.Visible = True : imbPosting.Visible = True : imbDelete.Visible = True
                    gvDtl.Columns(10).Visible = True
                End If
            End If
        End If
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer, ByVal sState As String)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        Dim sAsliTemp As String = sMessage.Replace("<br />", vbCrLf).Replace("<BR>", vbCrLf)
        Dim sTemp() As String = sAsliTemp.Split(vbCrLf)
        If sTemp.Length > 25 Then
            lblPopUpMsg.Text = "<textarea class='inpText' readonly='true' style='height:250px;width:99%;'>" & sAsliTemp & "</textarea>"
        Else
            lblPopUpMsg.Text = sMessage
        End If
        lblCaption.Text = strCaption : lblState.Text = sState
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub GenerateDraftNo()
        sSql = "SELECT ISNULL(MAX(CAST(ISNULL(resfield1,0) AS INTEGER)) + 1, 1) AS IDNEW FROM ql_trnstockadj"
        resfield1.Text = ToDouble(cKon.ambilscalar(sSql).ToString)
    End Sub

    Private Sub GenerateNo()
        Dim sNo As String = "ADJ-" & Format(GetServerTime, "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(stockadjno, 6) AS INTEGER)) + 1, 1) AS IDNEW FROM ql_trnstockadj WHERE cmpcode='" & busunit.SelectedValue & "' AND stockadjno LIKE '" & sNo & "%'"
        stockadjno.Text = GenNumberString(sNo, "", ToDouble(cKon.ambilscalar(sSql).ToString), DefaultFormatCounter)
    End Sub

    Private Sub InitAllDDL()
        ' Init DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE' "
        Dim x As String = Session("CompnyCode")
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
            busunit.Enabled = False
            busunit.CssClass = "inpTextDisabled"
        Else
            busunit.Enabled = True
            busunit.CssClass = "inpText"
        End If
        If FillDDL(busunit, sSql) Then
            InitWH(ddlType.SelectedValue)
        End If
    End Sub

    Private Sub PrintReport()
        Try
            Dim sWhere As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sWhere &= IIf(sWhere = "", " WHERE ", " OR ") & " (a.cmpcode='" & dv(C1)("cmpcode").ToString & "' AND a.resfield1='" & dv(C1)("resfield1").ToString & "') "
                Next
                dv.RowFilter = ""
            End If

            report.Load(Server.MapPath(folderReport & "crPrintAdj.rpt"))

            report.SetParameterValue("swhere", sWhere)
            cProc.SetDBLogonForReport(report)

            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            Try
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "StockAdjustment_" & Format(GetServerTime(), "yyyyMMddHHmmss"))
            Catch ex As Exception
                report.Close()
                report.Dispose()
            End Try
        Catch ex As Exception
            showMessage(ex.Message, 1, "")
        End Try
    End Sub

    Private Sub InitWH(ByVal Type As String)
        sSql = "SELECT genoid,gendesc FROM QL_mstgen WHERE activeflag='ACTIVE' AND genoid>0 AND gengroup='WAREHOUSE' "
        sSql &= " ORDER BY gendesc"
        FillDDL(FilterWarehouse, sSql)
    End Sub

    Private Sub InitFilterDDLCat1(ByVal sType As String)
        'Fill DDL Category1
        sSql = "SELECT cat1oid, cat1code+' - '+cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' "
        If sType = "Raw" Then
            sSql &= " AND cat1res1='Raw' "
        Else
            sSql &= " AND cat1res1='" & sType & "'"
        End If
        sSql &= " ORDER BY cat1code "
        FillDDL(FilterDDLCat01, sSql)
        If FilterDDLCat01.Items.Count > 0 Then
            InitFilterDDLCat2(sType)
        Else
            FilterDDLCat02.Items.Clear()
            FilterDDLCat03.Items.Clear()
            FilterDDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitFilterDDLCat2(ByVal sType As String)
        'Fill DDL Category2
        sSql = "SELECT cat2oid, cat2code+' - '+cat2shortdesc FROM QL_mstcat2 cat2 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat2.cmpcode AND cat1.cat1oid=cat2.cat1oid AND cat1.cat1oid='" & FilterDDLCat01.SelectedValue & "' WHERE cat2.cmpcode='" & CompnyCode & "' AND cat2.activeflag='ACTIVE' /*AND cat2res1='" & sType & "'*/"
        If sType = "Raw" Then
            sSql &= " AND cat2res1='Raw'"
        Else
            sSql &= " AND cat2res1='" & sType & "' "
        End If

        FillDDL(FilterDDLCat02, sSql)
        If FilterDDLCat02.Items.Count > 0 Then
            InitFilterDDLCat3(sType)
        Else
            FilterDDLCat03.Items.Clear()
            FilterDDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitFilterDDLCat3(ByVal sType As String)
        'Fill DDL Category3
        sSql = "SELECT cat3oid, cat3code+' - '+cat3shortdesc FROM QL_mstcat3 cat3 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat3.cmpcode AND cat1.cat1oid=cat3.cat1oid AND cat1.cat1oid='" & FilterDDLCat01.SelectedValue & "' INNER JOIN QL_mstcat2 cat2 ON cat2.cmpcode=cat3.cmpcode AND cat2.cat2oid=cat3.cat2oid AND cat2.cat2oid='" & FilterDDLCat02.SelectedValue & "' WHERE cat3.cmpcode='" & CompnyCode & "' AND cat3.activeflag='ACTIVE' /*AND cat3res1='" & sType & "'*/"
        If sType = "Raw" Then
            sSql &= " AND cat1res1='Raw' "
        Else
            sSql &= " AND cat3res1='" & sType & "' "
        End If

        FillDDL(FilterDDLCat03, sSql)
        'If FilterDDLCat03.Items.Count > 0 Then
        '    InitFilterDDLCat4(sType)
        'Else
        '    FilterDDLCat04.Items.Clear()
        'End If
    End Sub

    Private Sub InitFilterDDLCat4(ByVal sType As String)
        'Fill DDL Category4
        sSql = "SELECT cat4code, cat4shortdesc FROM QL_mstcat4 cat4 INNER JOIN QL_mstcat1 cat1 ON cat1.cmpcode=cat4.cmpcode AND cat1.cat1oid=cat4.cat1oid AND cat1.cat1code='" & FilterDDLCat01.SelectedValue & "' INNER JOIN QL_mstcat2 cat2 ON cat2.cmpcode=cat4.cmpcode AND cat2.cat2oid=cat4.cat2oid AND cat2.cat2code='" & FilterDDLCat02.SelectedValue & "' INNER JOIN QL_mstcat3 cat3 ON cat3.cmpcode=cat4.cmpcode AND cat3.cat3oid=cat4.cat3oid AND cat3.cat3code='" & FilterDDLCat03.SelectedValue & "' WHERE cat4.cmpcode='" & CompnyCode & "' AND cat4.activeflag='ACTIVE' /*AND cat4res1='" & sType & "'*/"
        If sType = "Raw" Then
            sSql &= " AND cat4res1='Raw' AND cat3res1='Raw' AND cat2res1='Raw' AND cat1res1='Raw' AND cat1res2 IN ('WIP', 'Non WIP')"
        ElseIf sType = "Log" Then
            sSql &= " AND cat4res1='Raw' AND cat3res1='Raw' AND cat2res1='Raw' AND cat1res1='Raw' AND cat1res2 IN ('Log')"
        ElseIf sType = "Sawn Timber" Then
            sSql &= " AND cat4res1='Raw' AND cat3res1='Raw' AND cat2res1='Raw' AND cat1res1='Raw' AND cat1res2 IN ('Sawn Timber')"
        Else
            sSql &= " AND cat4res1='" & sType & "' AND cat3res1='" & sType & "' AND cat2res1='" & sType & "' AND cat1res1='" & sType & "'"
        End If
        FillDDL(FilterDDLCat04, sSql)
    End Sub

    Private Sub InitDDLReason()
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE gengroup='ADJUSMENT REASON' AND activeflag='ACTIVE' AND cmpcode='" & CompnyCode & "'"
        For i As Integer = 1 To gvFindCrd.Rows.Count
            Dim row As System.Web.UI.WebControls.GridViewRow = gvFindCrd.Rows(i - 1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(6).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                        FillDDL(CType(myControl, System.Web.UI.WebControls.DropDownList), sSql)
                        If CType(myControl, System.Web.UI.WebControls.DropDownList).Items.Count = 0 Then
                            showMessage("Please input ADJUSTMENT REASON from MASTER GENERAL first xxx", 2, "")
                            Exit Sub
                        End If
                    End If
                Next
            End If
        Next
    End Sub

    Private Sub AddToJurnal(ByVal dtJurnal As DataTable, ByVal iAcctgOid As Integer, ByVal sDbCr As String, ByVal dAmt As Decimal, ByVal dAmtIDR As Decimal, ByVal dAmtUSD As Decimal, ByVal iRefOid As String)
        Dim nuRow As DataRow = dtJurnal.NewRow
        nuRow("acctgoid") = iAcctgOid
        nuRow("gldbcr") = sDbCr
        nuRow("glamt") = dAmt
        nuRow("glamtidr") = dAmtIDR
        nuRow("glamtusd") = dAmtUSD
        nuRow("refoid") = iRefOid
        dtJurnal.Rows.Add(nuRow)
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then Response.Redirect("~/Other/login.aspx")
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnMA.aspx")
        End If
        If checkPagePermission("~\Transaction\trnMA.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Stock Adjustment"
        Session("oid") = Request.QueryString("oid")

        imbPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to send this data for APPROVAL?');")
        imbDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")

        If Not Page.IsPostBack Then
            InitAllDDL()
            ddlType_SelectedIndexChanged(Nothing, Nothing)
            If (Session("oid") <> Nothing And Session("oid") <> "") Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
                rbAdjust.Enabled = False : rbInit.Enabled = False
            Else
                FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
                FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
                createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
                stockadjdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                GenerateDraftNo()
                TabContainer1.ActiveTabIndex = 0
            End If
            Dim dt As New DataTable : dt = Session("TblDtl") : gvDtl.DataSource = dt : gvDtl.DataBind()
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Close() : report.Dispose()
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        If lblState.Text = "REDIR" Then
            Response.Redirect("~\Transaction\trnMA.aspx?awal=true")
        Else
            cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        End If
    End Sub

    Protected Sub busunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles busunit.SelectedIndexChanged
        InitWH(ddlType.SelectedValue)
        Session("TblDtl") = Nothing
        ResetFind()
    End Sub

    Protected Sub FilterWarehouse_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterWarehouse.SelectedIndexChanged
        ResetFind()
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        If ddlType.SelectedValue = "RAW MATERIAL" Then
            InitFilterDDLCat1("RAW") : InitWH("RAW")
        Else
            InitFilterDDLCat1("FG") : InitWH("FG")
        End If

        If ddlType.SelectedValue = "LOG" Or ddlType.SelectedValue = "PALLET" Then
            lblWarnType.Text = "* Adjust Stock by checking the Checkbox on right most column."
            FilterDDL.Items(0).Enabled = True : FilterText.TextMode = TextBoxMode.MultiLine : FilterText.Rows = 3
        Else
            lblWarnType.Text = "* Adjust Stock by editing New Qty field."
            FilterDDL.Items(0).Enabled = True : FilterText.TextMode = TextBoxMode.SingleLine : FilterText.Rows = 0
        End If
        ResetFind()
    End Sub

    Protected Sub FilterDDLCat01_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat01.SelectedIndexChanged
        If ddlType.SelectedValue = "RAW MATERIAL" Then
            InitFilterDDLCat2("Raw")
        Else
            InitFilterDDLCat2("FG")
        End If
        ResetFind()
    End Sub

    Protected Sub FilterDDLCat02_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat02.SelectedIndexChanged
        If ddlType.SelectedValue = "RAW MATERIAL" Then
            InitFilterDDLCat3("Raw")
        Else
            InitFilterDDLCat3("FG")
        End If
        ResetFind()
    End Sub

    Protected Sub FilterDDLCat03_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat03.SelectedIndexChanged
        If ddlType.SelectedValue = "RAW MATERIAL" Then
            InitFilterDDLCat4("Raw")
        Else
            InitFilterDDLCat4("FG")
        End If
        ResetFind()
    End Sub

    Protected Sub FilterDDLCat04_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLCat04.SelectedIndexChanged
        ResetFind()
    End Sub

    Protected Sub imbFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFind.Click
        Dim sErr As String = ""
        If Not IsValidDate(stockadjdate.Text, "MM/dd/yyyy", sErr) Then
            showMessage("ADJ. DATE is invalid. " & sErr, 2, "")
            Exit Sub
        End If
        Dim sMatType As String = "", sLastQtyQuery As String = "0.0", sQueryInHistStock As String = ""
        If ddlType.SelectedValue = "RAW MATERIAL" Then
            sMatType = "item"
        Else
            sMatType = "item"
        End If
        If rbAdjust.Checked Then
            sLastQtyQuery = "(select dbo.getstockqty(" & sMatType & "oid, " & FilterWarehouse.SelectedValue & "))"
            sQueryInHistStock = " AND " & sMatType & "oid IN (SELECT DISTINCT refoid FROM QL_crdstock con WHERE con.cmpcode='" & busunit.SelectedValue & "' AND mtrlocoid=" & FilterWarehouse.SelectedValue & ")"
        Else
            sQueryInHistStock = " AND " & sMatType & "oid NOT IN (SELECT DISTINCT refoid FROM QL_constock con WHERE con.cmpcode='" & busunit.SelectedValue & "' AND mtrlocoid=" & FilterWarehouse.SelectedValue & ")"
        End If

        'Find Data
        sSql = "SELECT 0 seq, " & sMatType & "oid refoid, '' matno, " & sMatType & "code matcode, " & sMatType & "longdescription matlongdesc, " & sLastQtyQuery & " lastqty, '" & FilterWarehouse.SelectedItem.Text & "' location, " & FilterWarehouse.SelectedValue & " mtrwhoid, " & sLastQtyQuery & " adjqty, '' reason, '' Note, 0.0 stockvalueidr, 0.0 stockvalueusd, " & sMatType & "unit1 unitoid, 'False' selected, itemoldcode FROM QL_mst" & sMatType & " m WHERE m.cmpcode='" & CompnyCode & "' AND m.itemrecordstatus='ACTIVE' AND " & sMatType & "" & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'" & sQueryInHistStock
        If cbCat01.Checked Then
            If FilterDDLCat01.SelectedValue <> "" Then
                sSql &= " AND itemcat1='" & FilterDDLCat01.SelectedValue & "'"
            End If
        End If
        If cbCat02.Checked Then
            If FilterDDLCat02.SelectedValue <> "" Then
                sSql &= " AND itemcat2='" & FilterDDLCat02.SelectedValue & "'"
            End If
        End If
        If cbCat03.Checked Then
            If FilterDDLCat03.SelectedValue <> "" Then
                sSql &= " AND itemcat3='" & FilterDDLCat03.SelectedValue & "'"
            End If
        End If
       
        sSql &= " ORDER BY " & sMatType & "code"

        Try
            Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_crdmtr")
            For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                dtTbl.Rows.Item(C1)("seq") = C1 + 1
            Next
            gvFindCrd.Columns(9).Visible = (ddlType.SelectedValue = "LOG" Or ddlType.SelectedValue = "PALLET")
            Session("TblCrd") = dtTbl
            gvFindCrd.DataSource = Session("TblCrd")
            gvFindCrd.DataBind()
            If FilterText.Text = "" Then
                FilterDDL.SelectedIndex = -1
            End If
            lblCount.Text = dtTbl.Rows.Count & " data found."
            imbAddToList.Visible = (dtTbl.Rows.Count > 0)
            btnSelectAll.Visible = False 'imbAddToList.Visible
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 1, "")
        End Try
    End Sub

    Protected Sub gvFindCrd_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvFindCrd.PageIndexChanging
        UpdateDetailData()
        gvFindCrd.PageIndex = e.NewPageIndex
        Dim dtDtl As DataTable = Session("TblCrd")
        gvFindCrd.DataSource = dtDtl : gvFindCrd.DataBind()
    End Sub

    Protected Sub gvFindCrd_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFindCrd.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 2)
            Dim DDLReason As DropDownList = e.Row.FindControl("ddlReason")
            sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE gengroup='ADJUSMENT REASON' AND activeflag='ACTIVE' "
            FillDDL(DDLReason, sSql)
            If DDLReason.Items.Count < 1 Then
                showMessage("Please input ADJUSTMENT REASON from MASTER GENERAL first fff", 2, "")
                Exit Sub
            End If
            DDLReason.SelectedValue = DDLReason.ToolTip
            ' Define Header Checkbox based on Checkbox in Each Row
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(9).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    If CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = True Then
                        CType(gvFindCrd.HeaderRow.FindControl("chkSelectAll"), CheckBox).Checked = True
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For C1 As Integer = 0 To gvFindCrd.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvFindCrd.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(8).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        UpdateDetailData()
    End Sub

    Protected Sub imbAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbAddToList.Click
        Try
            UpdateDetailData()
            Dim sMsg As String = ""
            If Session("TblDtl") Is Nothing Then CreateTblDetail()
            If Session("TblCrd") Is Nothing Then
                sMsg &= "- No data to be added.<BR>"
            Else
                Dim dtCek As DataTable = Session("TblCrd")
                If dtCek.Rows.Count < 1 Then
                    sMsg &= "- No data to be added.<BR>"
                Else
                    If GetDDLItem(0, 6) = "" Then
                        sMsg &= "- Please input ADJUSTMENT REASON from MASTER GENERAL first.<BR>"
                    End If
                    Dim dvCek As DataView = dtCek.DefaultView
                    dvCek.RowFilter = "(adjqty<0 OR adjqty>9999999999.9999) AND adjqty<>lastqty"
                    If dvCek.Count > 0 Then
                        sMsg &= "- Valid Qty is between 0 and 9,999,999,999.9999 (" & dvCek(0)("adjqty") & ")<BR>"
                    End If
                    dvCek.RowFilter = ""

                    If ddlType.SelectedValue = "LOG" Or ddlType.SelectedValue = "PALLET" Then
                        dvCek.RowFilter = "selected='True'"
                    Else
                        dvCek.RowFilter = "adjqty<>lastqty"
                    End If
                    If dvCek.Count < 1 Then
                        sMsg &= "- Failed to Add to List data: " & lblWarnType.Text
                        dvCek.RowFilter = ""
                    Else
                        Dim dtDtlCek As DataTable = Session("TblDtl")
                        Dim dvDtlCek As DataView = dtDtlCek.DefaultView

                        Dim sItem As String = ""
                        For R1 As Integer = 0 To dvCek.Count - 1
                            dvDtlCek.RowFilter = "refname='" & ddlType.SelectedValue & "' AND refoid=" & dvCek(R1)("refoid").ToString & " AND mtrwhoid=" & dvCek(R1)("mtrwhoid").ToString
                            If dvDtlCek.Count > 0 Then
                                sItem &= "# Code = " & dvCek(R1)("matcode").ToString & " ; Location = " & dvCek(R1)("location").ToString & "<BR>"
                            End If
                            dvDtlCek.RowFilter = ""
                        Next
                        If sItem <> "" Then
                            sMsg &= "- Following " & ddlType.SelectedValue & " has been added before:<BR>" & sItem
                        End If
                    End If
                End If
            End If

            If sMsg <> "" Then
                showMessage(sMsg, 2, "") : Exit Sub
            End If

            Dim dtCrd As DataTable = Session("TblCrd")
            Dim dvCrd As DataView = dtCrd.DefaultView
            If ddlType.SelectedValue = "LOG" Or ddlType.SelectedValue = "PALLET" Then
                dvCrd.RowFilter = "selected='True'"
            Else
                dvCrd.RowFilter = "adjqty<>lastqty"
            End If
            If dvCrd.Count < 1 Then
                showMessage("Failed to Add to List data: " & dvCrd.Count & lblWarnType.Text, 2, "")
                dvCrd.RowFilter = "" : Exit Sub
            End If

            Dim dtDtl As DataTable : dtDtl = Session("TblDtl")
            For R1 As Integer = 0 To dvCrd.Count - 1
                sSql = "SELECT gendesc FROM QL_mstgen WHERE genoid='" & dvCrd(R1)("reason").ToString & "'"
                Dim sReason As String = GetStrData(sSql)
                sSql = "SELECT genother1 FROM QL_mstgen WHERE genoid='" & dvCrd(R1)("reason").ToString & "'"
                Dim iReasonAcctg As String = ToDouble(GetStrData(sSql).ToString)

                Dim nuRow As DataRow = dtDtl.NewRow
                nuRow("seq") = dtDtl.Rows.Count + 1
                nuRow("refoid") = dvCrd(R1)("refoid").ToString
                nuRow("refname") = ddlType.SelectedValue
                nuRow("refcode") = dvCrd(R1)("matcode").ToString
                nuRow("itemoldcode") = dvCrd(R1)("itemoldcode").ToString
                nuRow("reflongdesc") = dvCrd(R1)("matlongdesc").ToString
                nuRow("mtrwhoid") = dvCrd(R1)("mtrwhoid").ToString
                nuRow("location") = dvCrd(R1)("location").ToString
                nuRow("lastqty") = ToDouble(dvCrd(R1)("lastqty").ToString)
                nuRow("adjqty") = ToDouble(dvCrd(R1)("adjqty").ToString)
                nuRow("reasonoid") = 0
                nuRow("reason") = ""
                nuRow("reasonacctgoid") = 0
                nuRow("note") = dvCrd(R1)("note").ToString
                nuRow("stockvalueidr") = ToDouble(dvCrd(R1)("stockvalueidr").ToString)
                nuRow("stockvalueusd") = ToDouble(dvCrd(R1)("stockvalueusd").ToString)
                nuRow("unitoid") = dvCrd(R1)("unitoid").ToString
                nuRow("createuser") = Session("UserID")
                nuRow("createdate") = GetServerTime()
                dtDtl.Rows.Add(nuRow)
            Next
            dvCrd.RowFilter = ""

            ResetFind()
            Session("TblDtl") = dtDtl
            gvDtl.DataSource = dtDtl : gvDtl.DataBind()
        Catch ex As Exception
            showMessage(ex.ToString, 2, "")
        End Try

    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 2)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 2)
        End If
    End Sub

    Protected Sub lkbDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dtDtl As DataTable : dtDtl = Session("TblDtl")
        Dim dvDel As DataView = dtDtl.DefaultView
        dvDel.RowFilter = "seq=" & sender.ToolTip
        If dvDel.Count > 0 Then
            dvDel(0).Delete()
        End If
        dvDel.RowFilter = ""
        dtDtl.AcceptChanges()

        For C1 As Integer = 0 To dtDtl.Rows.Count - 1
            dtDtl.Rows.Item(C1)("seq") = C1 + 1
        Next

        Session("TblDtl") = dtDtl
        gvDtl.DataSource = dtDtl : gvDtl.DataBind()
    End Sub

    Protected Sub imbPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbPosting.Click
        sSql = "SELECT approvaluser, apppersontype FROM QL_approvalperson WHERE tablename='QL_trnstockadj' AND activeflag='ACTIVE' AND cmpcode IN ('" & CompnyCode & "', '" & busunit.SelectedValue & "')"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_approvalperson")
        If dt.Rows.Count = 0 Then
            showMessage("There is no user can't approve this transaction. Please define approval user in <STRONG>APPROVAL USER</STRONG> form.", 2, "")
        Else
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "apppersontype='Final'"
            If dv.Count = 0 Then
                showMessage("Final approval user is not defined for this transaction. Please define final approval user in <STRONG>APPROVAL USER</STRONG> form.", 2, "")
                dv.RowFilter = ""
            Else
                dv.RowFilter = ""
                Session("AppPerson") = dt
                stockadjstatus.Text = "In Approval"
                imbSave_Click(Nothing, Nothing)
            End If
        End If
    End Sub

    Protected Sub imbSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSave.Click
        Dim sMsg As String = "" : Dim sErr As String = ""
        If Not IsValidDate(stockadjdate.Text, "MM/dd/yyyy", sErr) Then
            sMsg &= "- Invalid Stock Adjustment Date: " & sErr & "<BR>"
        End If
        If Session("TblDtl") Is Nothing Then
            sMsg &= "- No detail Stock Adjustment data.<BR>"
        Else
            Dim dtCek As DataTable = Session("TblDtl")
            If dtCek.Rows.Count < 1 Then
                sMsg &= "- No detail Stock Adjustment data.<BR>"
            End If
        End If
        'If stockadjstatus.Text = "Post" Then
        '    cRate.SetRateValue(1, stockadjdate.Text)
        '    If cRate.GetRateDailyLastError <> "" Then sMsg &= "- " & cRate.GetRateDailyLastError & ".<BR>"
        '    If cRate.GetRateMonthlyLastError <> "" Then sMsg &= "- " & cRate.GetRateMonthlyLastError & ".<BR>"
        'End If

        'Dim sVarErr As String = ""
        'If Not IsInterfaceExists("VAR_STOCK_RM", busunit.SelectedValue) Then sVarErr = "VAR_STOCK_RM"
        'If Not IsInterfaceExists("VAR_STOCK_GM", busunit.SelectedValue) Then sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_STOCK_GM"
        'If Not IsInterfaceExists("VAR_STOCK_SP", busunit.SelectedValue) Then sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_STOCK_SP"
        'If Not IsInterfaceExists("VAR_STOCK_LOG", busunit.SelectedValue) Then sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_STOCK_LOG"
        'If Not IsInterfaceExists("VAR_STOCK_ST", busunit.SelectedValue) Then sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_STOCK_ST"
        'If Not IsInterfaceExists("VAR_STOCK_FG", busunit.SelectedValue) Then sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_STOCK_FG"
        'If Not IsInterfaceExists("VAR_STOCK_ADJUSTMENT", busunit.SelectedValue) Then sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_STOCK_ADJUSTMENT"
        'If sVarErr <> "" Then sMsg &= "- " & GetInterfaceWarning(sVarErr, "posting")

        If sMsg <> "" Then
            stockadjstatus.Text = "In Process"
            showMessage(sMsg, 2, "") : Exit Sub
        End If

        Dim sDraftNo As String = resfield1.Text : Dim sPostNo As String = stockadjno.Text
        If Session("oid") = Nothing Or Session("oid") = "" Then
            sSql = "SELECT COUNT(*) FROM QL_trnstockadj WHERE resfield1='" & resfield1.Text & "'"
            If CheckDataExists(sSql) Then
                GenerateDraftNo()
            End If
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_trnstockadj", "resfield1", resfield1.Text, "stockadjstatus", updtime.Text, "Approved")
            If sStatusInfo <> "" Then
                'showMessage(sStatusInfo, 2, "")
                'stockadjstatus.Text = "In Process" : Exit Sub
            End If
        End If

        'If stockadjstatus.Text = "Post" Then GenerateNo()
        If stockadjstatus.Text = "Revised" Then stockadjstatus.Text = "In Process"

        Dim dtJurnal As New DataTable
        dtJurnal.Columns.Add("acctgoid", Type.GetType("System.Int32"))
        dtJurnal.Columns.Add("gldbcr", Type.GetType("System.String"))
        dtJurnal.Columns.Add("glamt", Type.GetType("System.Decimal"))
        dtJurnal.Columns.Add("glamtidr", Type.GetType("System.Decimal"))
        dtJurnal.Columns.Add("glamtusd", Type.GetType("System.Decimal"))
        dtJurnal.Columns.Add("refoid", Type.GetType("System.String"))

        Dim crdmatoid As Integer = GenerateID("QL_CRDSTOCK", CompnyCode)
        Dim conmtroid As Integer = GenerateID("QL_CONSTOCK", CompnyCode)
        Dim iStockValueOid As Integer = GenerateID("QL_STOCKVALUE", CompnyCode)
        Dim iStockAdjOid As Integer = GenerateID("QL_TRNSTOCKADJ", CompnyCode)
        Dim iGLMstOid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
        Dim iGLDtlOid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)

        Dim sType As String = ""
        Dim iStockAcctgOid, iStockAcctgGMOid, iStockAcctgRMOid, iStockAcctgSPOid, iStockAcctgLGOid, iStockAcctgSTOid, iStockAcctgITOid As Integer
        iStockAcctgGMOid = GetAcctgOID(GetVarInterface("VAR_STOCK_GM", busunit.SelectedValue), CompnyCode)
        iStockAcctgRMOid = GetAcctgOID(GetVarInterface("VAR_STOCK_RM", busunit.SelectedValue), CompnyCode)
        iStockAcctgSPOid = GetAcctgOID(GetVarInterface("VAR_STOCK_SP", busunit.SelectedValue), CompnyCode)
        iStockAcctgLGOid = GetAcctgOID(GetVarInterface("VAR_STOCK_LOG", busunit.SelectedValue), CompnyCode)
        iStockAcctgSTOid = GetAcctgOID(GetVarInterface("VAR_STOCK_ST", busunit.SelectedValue), CompnyCode)
        iStockAcctgITOid = GetAcctgOID(GetVarInterface("VAR_STOCK_FG", busunit.SelectedValue), CompnyCode)
        Dim iAppOid As Integer = GenerateID("QL_APPROVAL", CompnyCode)
        Dim periodacctg As String = GetDateToPeriodAcctg(CDate(stockadjdate.Text))

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Dim objTable As DataTable = Session("TblDtl")
        Try
            If Session("oid") = Nothing Or Session("oid") = "" Then
                ' Do Nothing
            Else
                sSql = "DELETE FROM QL_trnstockadj WHERE cmpcode='" & busunit.SelectedValue & "' AND resfield1='" & resfield1.Text & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            For C1 As Integer = 0 To objTable.Rows.Count - 1
                Dim dQtyAdjIn As Decimal = 0 : Dim dQtyAdjOut As Decimal = 0
                If ToDouble(objTable.Rows(C1)("lastqty").ToString) > ToDouble(objTable.Rows(C1)("adjqty").ToString) Then
                    dQtyAdjIn = 0
                    dQtyAdjOut = ToDouble(objTable.Rows(C1)("lastqty").ToString) - ToDouble(objTable.Rows(C1)("adjqty").ToString)
                ElseIf ToDouble(objTable.Rows(C1)("lastqty").ToString) < ToDouble(objTable.Rows(C1)("adjqty").ToString) Then
                    dQtyAdjIn = ToDouble(objTable.Rows(C1)("adjqty").ToString) - ToDouble(objTable.Rows(C1)("lastqty").ToString)
                    dQtyAdjOut = 0
                End If
                If dQtyAdjIn <> 0 Or dQtyAdjOut <> 0 Then
                    Select Case objTable.Rows(C1)("refname").ToString
                        Case "GENERAL MATERIAL"
                            iStockAcctgOid = iStockAcctgGMOid : sType = "GMA"
                        Case "RAW MATERIAL"
                            iStockAcctgOid = iStockAcctgRMOid : sType = "RMA"
                        Case "SPARE PART"
                            iStockAcctgOid = iStockAcctgSPOid : sType = "SPA"
                        Case "LOG"
                            iStockAcctgOid = iStockAcctgLGOid : sType = "LGA"
                        Case "PALLET"
                            iStockAcctgOid = iStockAcctgSTOid : sType = "STA"
                        Case Else
                            iStockAcctgOid = iStockAcctgITOid : sType = "FGA"
                    End Select

                    sSql = "INSERT INTO QL_trnstockadj (cmpcode, stockadjoid, stockadjno, stockadjdate, periodacctg, refname, refoid, mtrwhoid, reasonoid, reasonacctgoid, stockadjqtybefore, stockadjqty, stockadjunit, stockadjamtidr, stockadjamtusd, stockadjnote, stockadjstatus, resfield1, resfield2, createuser, createdate, upduser, updtime, adjtype) VALUES ('" & busunit.SelectedValue & "', " & iStockAdjOid & ", '" & stockadjno.Text & "', '" & CDate(stockadjdate.Text) & "', '" & GetDateToPeriodAcctg(CDate(stockadjdate.Text)) & "', '" & objTable.Rows(C1)("refname").ToString & "', " & objTable.Rows(C1)("refoid").ToString & ", " & objTable.Rows(C1)("mtrwhoid").ToString & ", " & objTable.Rows(C1)("reasonoid").ToString & ", " & objTable.Rows(C1)("reasonacctgoid").ToString & ", " & ToDouble(objTable.Rows(C1)("lastqty").ToString) & ", " & ToDouble(objTable.Rows(C1)("adjqty").ToString) & ", " & objTable.Rows(C1)("unitoid").ToString & ", " & ToDouble(objTable.Rows(C1)("stockvalueidr").ToString) & ", " & ToDouble(objTable.Rows(C1)("stockvalueusd").ToString) & ", '" & Tchar(objTable.Rows(C1)("note").ToString) & "', '" & stockadjstatus.Text & "', '" & resfield1.Text & "', '" & iStockAcctgOid & "', '" & objTable.Rows(C1)("createuser").ToString & "', '" & CDate(objTable.Rows(C1)("createdate").ToString) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & IIf(rbAdjust.Checked, rbAdjust.Text, rbInit.Text) & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    If stockadjstatus.Text = "Post" Then
                        sSql = "INSERT INTO QL_conmat (cmpcode, conmtroid, type, trndate, periodacctg, formaction, formoid, refoid, refname, mtrwhoid, qtyin, qtyout, reason, upduser, updtime, typemin, note) VALUES ('" & busunit.SelectedValue & "', " & conmtroid & ", '" & sType & "', '" & stockadjdate.Text & "', '" & GetDateToPeriodAcctg(CDate(stockadjdate.Text)) & "', 'QL_trnstockadj', " & iStockAdjOid & ", " & objTable.Rows(C1)("refoid").ToString & ", '" & objTable.Rows(C1)("refname").ToString & "', " & objTable.Rows(C1)("mtrwhoid").ToString & ", " & dQtyAdjIn & ", " & dQtyAdjOut & ", 'STOCK ADJUSTMENT', '" & Session("UserID") & "', CURRENT_TIMESTAMP, -1, 'STOCK ADJUSTMENT - " & stockadjno.Text & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        conmtroid += 1

                        sSql = "UPDATE QL_crdmtr SET qtyadjin=qtyadjin + " & dQtyAdjIn & ", qtyadjout=qtyadjout + " & dQtyAdjOut & ", saldoakhir=saldoakhir + " & dQtyAdjIn - dQtyAdjOut & ", lasttranstype='QL_trnstockadj', lasttransdate='" & stockadjdate.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & busunit.SelectedValue & "' AND refoid=" & objTable.Rows(C1)("refoid").ToString & " AND refname='" & objTable.Rows(C1)("refname").ToString & "' AND periodacctg='" & GetDateToPeriodAcctg(CDate(stockadjdate.Text)) & "' AND mtrwhoid=" & objTable.Rows(C1)("mtrwhoid").ToString
                        xCmd.CommandText = sSql
                        If xCmd.ExecuteNonQuery() <= 0 Then
                            sSql = "INSERT INTO QL_crdmtr (cmpcode, crdmatoid, periodacctg, refoid, refname, mtrwhoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir, lasttranstype, lasttransdate, upduser, updtime, createuser, createdate, closeuser, closingdate) VALUES ('" & busunit.SelectedValue & "', " & crdmatoid & ", '" & GetDateToPeriodAcctg(CDate(stockadjdate.Text)) & "', " & objTable.Rows(C1)("refoid").ToString & ", '" & objTable.Rows(C1)("refname").ToString & "', " & objTable.Rows(C1)("mtrwhoid").ToString & ", 0, 0, " & dQtyAdjIn & ", " & dQtyAdjOut & ", 0, " & dQtyAdjIn - dQtyAdjOut & ", 'QL_trnstockadj', '" & CDate(stockadjdate.Text) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', '1/1/1900')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            crdmatoid += 1
                        End If

                        If dQtyAdjIn > 0 Then
                            sSql = "UPDATE QL_stockvalue SET stockqty=stockqty + " & dQtyAdjIn & ", stockvalueidr=((stockqty * stockvalueidr) + (" & dQtyAdjIn * ToDouble(objTable.Rows(C1)("stockvalueidr").ToString) & "))/(stockqty + " & dQtyAdjIn & "), stockvalueusd=((stockqty * stockvalueusd) + (" & dQtyAdjIn * ToDouble(objTable.Rows(C1)("stockvalueusd").ToString) & "))/(stockqty + " & dQtyAdjIn & "), lasttranstype='QL_trnstockadj', lasttransdate='" & stockadjdate.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & busunit.SelectedValue & "' AND refoid=" & objTable.Rows(C1)("refoid").ToString & " AND refname='" & objTable.Rows(C1)("refname").ToString & "' AND periodacctg='" & GetDateToPeriodAcctg(CDate(stockadjdate.Text)) & "'"
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() <= 0 Then
                                sSql = "INSERT INTO QL_stockvalue (cmpcode, stockvalueoid, periodacctg, refoid, refname, stockqty, stockvalueidr, stockvalueusd, lasttranstype, lasttransdate, note, upduser, updtime, backupqty, backupvalueidr, backupvalueusd, closeflag) VALUES ('" & busunit.SelectedValue & "', " & iStockValueOid & ", '" & GetDateToPeriodAcctg(CDate(stockadjdate.Text)) & "', " & objTable.Rows(C1)("refoid").ToString & ", '" & objTable.Rows(C1)("refname").ToString & "', " & dQtyAdjIn & ", " & ToDouble(objTable.Rows(C1)("stockvalueidr").ToString) & ", " & ToDouble(objTable.Rows(C1)("stockvalueusd").ToString) & ", 'QL_trnstockadj', '" & CDate(stockadjdate.Text) & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, 0, '')"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                iStockValueOid += 1
                            End If
                        End If

                        AddToJurnal(dtJurnal, IIf(dQtyAdjIn > 0, iStockAcctgOid, objTable.Rows(C1)("reasonacctgoid").ToString), "D", IIf(dQtyAdjIn > 0, dQtyAdjIn, dQtyAdjOut) * ToDouble(objTable.Rows(C1)("stockvalueidr").ToString), IIf(dQtyAdjIn > 0, dQtyAdjIn, dQtyAdjOut) * ToDouble(objTable.Rows(C1)("stockvalueidr").ToString), IIf(dQtyAdjIn > 0, dQtyAdjIn, dQtyAdjOut) * ToDouble(objTable.Rows(C1)("stockvalueusd").ToString), iStockAdjOid)
                        AddToJurnal(dtJurnal, IIf(dQtyAdjIn > 0, objTable.Rows(C1)("reasonacctgoid").ToString, iStockAcctgOid), "C", IIf(dQtyAdjIn > 0, dQtyAdjIn, dQtyAdjOut) * ToDouble(objTable.Rows(C1)("stockvalueidr").ToString), IIf(dQtyAdjIn > 0, dQtyAdjIn, dQtyAdjOut) * ToDouble(objTable.Rows(C1)("stockvalueidr").ToString), IIf(dQtyAdjIn > 0, dQtyAdjIn, dQtyAdjOut) * ToDouble(objTable.Rows(C1)("stockvalueusd").ToString), iStockAdjOid)
                    End If
                    iStockAdjOid += 1
                End If
            Next
            If stockadjstatus.Text = "In Approval" Then
                Dim dt As DataTable = Session("AppPerson")
                For C1 As Integer = 0 To dt.Rows.Count - 1
                    sSql = "INSERT INTO QL_approval (cmpcode, approvaloid, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvalnote) VALUES ('" & busunit.SelectedValue & "', " & iAppOid + C1 & ", 'MSA" & resfield1.Text & "_" & (iAppOid + C1) & "', '" & Session("UserID") & "', '" & stockadjdate.Text & "', 'New', 'QL_trnstockadj', " & resfield1.Text & ", 'In Approval', '0', '" & dt.Rows(C1)("approvaluser").ToString & "', '1/1/1900', '')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Next
                sSql = "UPDATE QL_mstOid SET lastoid=" & iAppOid + dt.Rows.Count - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_APPROVAL'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If

            If stockadjstatus.Text = "Post" Then
                ' Insert GL MST
                sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & busunit.SelectedValue & "', " & iGLMstOid & ", '" & CDate(stockadjdate.Text) & "', '" & GetDateToPeriodAcctg(CDate(stockadjdate.Text)) & "', 'STOCK ADJUSTMENT|NO. " & stockadjno.Text & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                For R1 As Integer = 0 To dtJurnal.Rows.Count - 1
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glother1, glother2, glother3, glflag, upduser, updtime, glamtidr, glamtusd) VALUES ('" & busunit.SelectedValue & "', " & iGLDtlOid & ", " & R1 + 1 & ", " & iGLMstOid & ", " & dtJurnal.Rows(R1)("acctgoid").ToString & ", '" & dtJurnal.Rows(R1)("gldbcr").ToString & "', " & ToDouble(dtJurnal.Rows(R1)("glamt").ToString) & ", '" & stockadjno.Text & "', 'STOCK ADJUSTMENT|NO. " & stockadjno.Text & "', 'QL_trnstockadj', '" & dtJurnal.Rows(R1)("refoid").ToString & "', '', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dtJurnal.Rows(R1)("glamtidr").ToString) & ", " & ToDouble(dtJurnal.Rows(R1)("glamtusd").ToString) & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    iGLDtlOid += 1
                Next

                sSql = "UPDATE QL_mstoid SET lastoid=" & conmtroid - 1 & " WHERE tablename='QL_CONMAT' AND cmpcode='" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & crdmatoid - 1 & " WHERE tablename='QL_CRDMTR' AND cmpcode='" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & iStockValueOid - 1 & " WHERE tablename='QL_STOCKVALUE' AND cmpcode='" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & iGLMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & iGLDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            sSql = "UPDATE QL_mstoid SET lastoid=" & iStockAdjOid - 1 & " WHERE tablename='QL_TRNSTOCKADJ' AND cmpcode='" & CompnyCode & "' "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch exSql As SqlException
            objTrans.Rollback() : conn.Close()
            If ToDouble(Session("ErrorCounter")) < 5 Then
                If exSql.Number = 2627 Then
                    Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                    imbSave_Click(sender, e)
                    Exit Sub
                Else
                    showMessage(exSql.Message & sSql, 1, "")
                    stockadjstatus.Text = "In Process"
                    Exit Sub
                End If
            Else
                showMessage(exSql.Message & sSql, 1, "")
                stockadjstatus.Text = "In Process"
                Exit Sub
            End If
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message & sSql, 1, "") : Exit Sub
        End Try

        If sDraftNo <> resfield1.Text Then
            showMessage("Draft No. have been regenerated because have been used by another data. Your new Draft No. is " & resfield1.Text & ".<BR>", 3, "REDIR")
        Else
            If sPostNo <> stockadjno.Text Then
                showMessage("Data have been posted with Stock Adj. No. " & stockadjno.Text & ".", 3, "REDIR")
            Else
                Response.Redirect("~\Transaction\trnMA.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub imbDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbDelete.Click
        If ToDouble(resfield1.Text) = 0 Then
            showMessage("Invalid Stock Adjustment data.<BR>", 2, "")
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
           
            sSql = "DELETE FROM QL_trnstockadj WHERE resfield1='" & resfield1.Text & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, 1, "") : Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnMA.aspx?awal=true")
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClear.Click
        Response.Redirect("~\Transaction\trnMA.aspx?awal=true")
    End Sub

    Protected Sub imbPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbPrint.Click
        'If Session("TblDtl") Is Nothing Then
        '    showMessage("Please define detail data first!", 2, "")
        '    Exit Sub
        'End If
        'If UpdateDetailData() Then
        '    ShowReport()
        'End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDLMst.SelectedValue & " LIKE '%" & Tchar(FilterTextMst.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND a.stockadjdate>='" & FilterPeriod1.Text & " 00:00:00' AND a.stockadjdate<='" & FilterPeriod2.Text & "'"
            Else
                Exit Sub
            End If
        End If
        If checkPagePermission("~\Transaction\trnMA.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND a.createuser='" & Session("UserID") & "'"
        End If
        BindData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        FilterDDLStatus.SelectedIndex = 0
        If checkPagePermission("~\Transaction\trnMA.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND a.createuser='" & Session("UserID") & "'"
        End If
        BindData(sSqlPlus)
    End Sub

    Protected Sub btnPrintHdr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrintHdr.Click
        UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dv As DataView = Session("TblMst").DefaultView
            dv.RowFilter = "checkvalue='True'"
            If dv.Count < 1 Then
                dv.RowFilter = ""
                showMessage("Please Select data to Print first.", 2, "")
            Else
                dv.RowFilter = ""
                PrintReport()
            End If
        Else
            showMessage("Please Select data to Print first.", 2, "")
        End If
    End Sub

    Protected Sub gvList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvList.PageIndexChanging
        UpdateCheckedValue()
        gvList.PageIndex = e.NewPageIndex
        gvList.DataSource = Session("TblMst")
        gvList.DataBind()
    End Sub

    Protected Sub gvList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvList.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt As DataTable = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvList.DataSource = Session("TblMst")
            gvList.DataBind()
        End If
    End Sub

    Protected Sub lkbSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("~\Transaction\trnMA.aspx?oid=" & sender.ToolTip)
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        Dim sReason As String = GetStrData("SELECT TOP 1 genoid FROM QL_mstgen WHERE gengroup='ADJUSMENT REASON' AND activeflag='ACTIVE'")
        If Not Session("TblCrd") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCrd")
            For c1 As Integer = 0 To dtTbl.Rows.Count - 1
                dtTbl.Rows(c1)("selected") = "True"
                dtTbl.Rows(c1)("reason") = sReason
            Next
            dtTbl.AcceptChanges()
            Session("TblCrd") = dtTbl
            gvFindCrd.DataSource = Session("TblCrd") : gvFindCrd.DataBind()
        End If
    End Sub
#End Region

End Class