Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Structure VarCostValue
    Public ValDM_IDR As Double
    Public ValDM_USD As Double
    Public ValDL_IDR As Double
    Public ValDL_USD As Double
    Public ValFOH_IDR As Double
    Public ValFOH_USD As Double
    Public ValCost_IDR As Double
    Public ValCost_USD As Double
    Public ValCost_DLA As Double
End Structure

Partial Class Transaction_ProductionResult
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If wodtl2oid.Text = "" Then
            sError &= "- Please select KIK NO. field!<BR>"
        End If
        If prodrestype.Text <> "FG" Then
            If todeptoid.SelectedValue = "" Then
                sError &= "- Please select TO DEPARTMENT field!<BR>"
            End If
        End If
        If prodresqty.Text = "" Then
            sError &= "- Please fill RESULT QTY field!<BR>"
        Else
            If ToDouble(prodresqty.Text) <= 0 Then
                sError &= "- RESULT QTY field must be more than 0!<BR>"
            Else
                Dim sErrReply As String = ""
                If Not isLengthAccepted("prodresqty", "QL_trnprodresdtl", ToDouble(prodresqty.Text), sErrReply) Then
                    sError &= "- RESULT QTY field must be less than MAX RESULT QTY (" & sErrReply & ") allowed stored in database!<BR>"
                Else
                    If ToDouble(prodresqty.Text) > ToDouble(wodtl2qty.Text) Then
                        sError &= "- RESULT QTY must be less than MAX QTY 1!<BR>"
                    End If
                    'If resultmaxqty.Text <> "Null" Then
                    '    If ToDouble(prodresqty.Text) > ToDouble(resultmaxqty.Text) Then
                    '        sError &= "- RESULT QTY must be less than MAX QTY 2!<BR>"
                    '    End If
                    'End If
                    If Not IsQtyRounded(ToDouble(prodresqty.Text), ToDouble(resultlimitqty.Text)) Then
                        sError &= "- RESULT QTY must be rounded by ROUNDING QTY!<BR>"
                    End If
                End If
            End If
        End If
        If prodresunitoid.SelectedValue = "" Then
            sError &= "- Please select UNIT field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If deptoid.SelectedValue = "" Then
            sError &= "- Please select DEPARTMENT field!<BR>"
        End If
        If prodresdate.Text = "" Then
            sError &= "- Please fill RESULT DATE field!<BR>"
            sErr = "None"
        Else
            If Not IsValidDate(prodresdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- RESULT DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If suppoid.SelectedValue = "" Then
            sError &= "- Please select GROUP NAME field!<BR>"
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill DETAIL DATA!<BR>"
        Else
            Dim objTbl As DataTable = Session("TblDtl")
            If objTbl.Rows.Count <= 0 Then
                sError &= "- Please fill DETAIL DATA!<BR>"
            Else
                If sError = "" Then
                    For C1 As Integer = 0 To objTbl.Rows.Count - 1
                        If CDate(objTbl.Rows(C1)("wodate").ToString) > CDate(prodresdate.Text) Then
                            sError &= "- Every KIK DATE must be less than RESULT DATE!<BR>"
                            Exit For
                        End If
                        Dim sAdd As String = ""
                        If Session("oid") IsNot Nothing And Session("oid") <> "" Then
                            sAdd = " AND resd.prodresmstoid<>" & Session("oid")
                        End If
                        sSql = "SELECT (wod2.wodtl2qty - ISNULL((SELECT SUM(resd.prodresqty) FROM QL_trnprodresdtl resd WHERE resd.cmpcode=wod2.cmpcode AND resd.wodtl2oid=wod2.wodtl2oid" & sAdd & "), 0.0)) AS wodtl2qty FROM QL_trnwodtl2 wod2 INNER JOIN QL_trnwomst wom ON wom.cmpcode=wod2.cmpcode AND wom.womstoid=wod2.womstoid INNER JOIN QL_mstdept de ON de.cmpcode=wod2.cmpcode AND de.deptoid=wod2.todeptoid INNER JOIN QL_mstgen g ON g.genoid=wod2.wodtl2unitoid WHERE wod2.wodtl2oid=" & objTbl.Rows(C1)("wodtl2oid")
                        Dim dMax01 As Double = ToDouble(GetStrData(sSql))
                        If ToDouble(objTbl.Rows(C1)("wodtl2qty").ToString) <> dMax01 Then
                            objTbl.Rows(C1)("wodtl2qty") = dMax01
                        End If
                        sSql = "SELECT (CASE WHEN (SELECT COUNT(-1) FROM QL_trnwodtl2 wodx WHERE wodx.cmpcode=wod2.cmpcode AND wodx.womstoid=wod2.womstoid AND wodx.todeptoid=wod2.deptoid AND wodx.wodtl2type<>'FG')=0 THEN -1.0 ELSE (ISNULL((SELECT MIN(tbltmp.prodresqty) FROM (SELECT ISNULL(SUM(resd.prodresqty), 0.0) AS prodresqty FROM QL_trnwodtl2 wod2x LEFT JOIN QL_trnprodresdtl resd ON resd.cmpcode=wod2x.cmpcode AND resd.wodtl2oid=wod2x.wodtl2oid" & sAdd & " AND resd.prodresmstoid IN (SELECT resm.prodresmstoid FROM QL_trnprodresmst resm WHERE resm.cmpcode=wod2x.cmpcode AND resm.prodresmststatus IN ('Post', 'Closed')) WHERE wod2x.cmpcode=wod2.cmpcode AND (CASE wod2x.wodtl2type WHEN 'FG' THEN 0 ELSE wod2x.todeptoid END)=wod2.deptoid AND wod2x.womstoid=wod2.womstoid GROUP BY wod2x.wodtl2oid) AS tbltmp), 0.0)) END) AS resultmaxqty FROM QL_trnwodtl2 wod2 INNER JOIN QL_trnwomst wom ON wom.cmpcode=wod2.cmpcode AND wom.womstoid=wod2.womstoid INNER JOIN QL_mstdept de ON de.cmpcode=wod2.cmpcode AND de.deptoid=wod2.todeptoid INNER JOIN QL_mstgen g ON g.genoid=wod2.wodtl2unitoid WHERE wod2.wodtl2oid=" & objTbl.Rows(C1)("wodtl2oid")
                        Dim dMax02 As Double = ToDouble(GetStrData(sSql))
                        If ToDouble(objTbl.Rows(C1)("resultmaxqty").ToString) <> dMax02 Then
                            objTbl.Rows(C1)("resultmaxqty") = dMax02
                        End If
                    Next
                    For C1 As Integer = 0 To objTbl.Rows.Count - 1
                        If ToDouble(objTbl.Rows(C1)("prodresqty").ToString) > ToDouble(objTbl.Rows(C1)("wodtl2qty").ToString) Then
                            sError &= "- RESULT QTY for detail No. " & ToDouble(objTbl.Rows(C1)("prodresdtlseq").ToString) & " must be less than MAX QTY (updated by other user)!<BR>"
                        End If
                        'If ToDouble(objTbl.Rows(C1)("resultmaxqty").ToString) <> -1 Then
                        '    If ToDouble(objTbl.Rows(C1)("prodresqty").ToString) > ToDouble(objTbl.Rows(C1)("resultmaxqty").ToString) Then
                        '        sError &= "- RESULT QTY for detail No. " & ToDouble(objTbl.Rows(C1)("prodresdtlseq").ToString) & " must be less than MAX QTY 2 (updated by other user)!<BR>"
                        '    End If
                        'End If
                        'If prodresmststatus.Text = "Post" Then
                        '    If ToDouble(objTbl.Rows(C1)("prodresdlcvalue").ToString) <= 0 Then
                        '        sError &= "- Please define DL VALUE for detail No. " & ToDouble(objTbl.Rows(C1)("prodresdtlseq").ToString) & " in DLC FORM before posting this data!<BR>"
                        '    End If
                        '    If ToDouble(objTbl.Rows(C1)("prodresohdvalue").ToString) <= 0 Then
                        '        sError &= "- Please define FOH VALUE for detail No. " & ToDouble(objTbl.Rows(C1)("prodresdtlseq").ToString) & " in DLC FORM before posting this data!<BR>"
                        '    End If
                        'End If
                    Next
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            prodresmststatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function GetTextValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function GetAcctgStock(ByVal sType As String) As String
        GetAcctgStock = ""
        sSql = "SELECT ISNULL(genother1,'') acctgoid FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='GROUPITEM' AND gencode='" & sType & "' AND activeflag='ACTIVE'"
        GetAcctgStock = GetStrData(sSql)
        Return GetAcctgStock
    End Function

    Public Function GetParameterID() As String
        Return Eval("itemoid") & "," & Eval("selectedunitoid")
    End Function

    Private Function GetDropDownListValue(ByVal cc As System.Web.UI.ControlCollection, Optional ByVal sText As String = "") As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                If sText = "" Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
                Else
                    sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedItem.Text
                End If
            End If
        Next
        Return sReturn
    End Function

    Private Function GetOutstandingTrans(ByVal sKIKOid As String, ByVal sSeq As String) As String
        Dim sResult As String = ""
        Dim iDataCount As Integer = 0

        ' Check outstanding Material Usage (Raw, Gen, SP, Kayu, By SO, By SO (Kayu))
        sSql = "SELECT COUNT(*) FROM QL_trnusagemst h1 WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagemststatus IN ('In Process') AND h1.womstoid=" & sKIKOid & ""
        iDataCount = ToInteger(GetStrData(sSql))
        If iDataCount > 0 Then
            sResult &= "- Material Usage: " & iDataCount & " Data<BR>"
        End If

        sSql = "SELECT COUNT(*) FROM QL_trnreturnbamst h1 INNER JOIN QL_trnreturnbadtl d1 ON d1.cmpcode=h1.cmpcode AND d1.retbamstoid=h1.retbamstoid INNER JOIN QL_trnbrtacaramst d2 ON d2.cmpcode=d1.cmpcode AND d2.acaramstoid=d1.acaramstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND retbamststatus IN ('In Process') AND d2.womstoid=" & sKIKOid & ""
        iDataCount = ToInteger(GetStrData(sSql))
        If iDataCount > 0 Then
            sResult &= "- Berita Acara Return: " & iDataCount & " Data<BR>"
        End If

        ' Check outstanding Berita Acara
        sSql = "SELECT COUNT(*) FROM QL_trnbrtacaramst h1 WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND acaramststatus IN ('In Process', 'In Approval', 'Revised') AND h1.womstoid=" & sKIKOid & ""
        iDataCount = ToInteger(GetStrData(sSql))
        If iDataCount > 0 Then
            sResult &= "- Berita Acara: " & iDataCount & " Data<BR>"
        End If

        ' Check outstanding Production Result
        sSql = "SELECT COUNT(*) FROM QL_trnprodresmst h1 INNER JOIN QL_trnprodresdtl d1 ON d1.cmpcode=h1.cmpcode AND d1.prodresmstoid=h1.prodresmstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND prodresmststatus IN ('In Process') AND d1.womstoid=" & sKIKOid & " AND prodresmstoid<>" & prodresmstoid.Text
        iDataCount = ToInteger(GetStrData(sSql))
        If iDataCount > 0 Then
            sResult &= "- Production Result: " & iDataCount & " Data<BR>"
        End If
        If sResult <> "" Then
            sResult = "Every outstanding data transaction for selected SPK must be <B>POSTED</B> or <B>APPROVED</B> before continue on Production Result Confirmation. Please click link below to view detail information!"
        End If
        Return sResult
    End Function

    Private Function GetHppPercentage(ByVal sOid As String) As Double
        Dim dReturn As Double = 0
        Try
            dReturn = ToDouble(GetStrData("SELECT TOP 1 gendesc FROM QL_mstgen WHERE activeflag='ACTIVE' AND genother1='" & sOid & "' AND gengroup='HPP PERCENTAGE (%)' ORDER BY updtime DESC"))
        Catch ex As Exception
            dReturn = 0
        End Try
        Return dReturn
    End Function

    Private Function GetNumericValues(ByVal sData As String) As String
        Dim sNumeric As String = ""
        Dim sCurr As String = ""
        For C1 As Integer = 1 To Len(sData)
            sCurr = Mid(sData, C1, 1)
            If IsNumeric(sCurr) Then
                sNumeric &= sCurr
            End If
        Next
        GetNumericValues = sNumeric
    End Function

    Private Function GenerateAPProdNo2(ByVal iCounter As Integer) As String
        Dim sNo As String = "APRES-" & Format(GetServerTime(), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(approductno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnapproductmst WHERE cmpcode='" & CompnyCode & "' AND approductno LIKE '" & sNo & "%'"
        xCmd.CommandText = sSql
        Return GenNumberString(sNo, "", xCmd.ExecuteScalar(), DefaultFormatCounter)
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer, Optional ByVal bView As Boolean = False)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage : lbViewDetail.Visible = bView
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckResultStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trnprodresmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND prodresmststatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnProdRes.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lkbResultInProcess.Visible = True
            lkbResultInProcess.Text = "You have " & GetStrData(sSql) & " In Process Production Result data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitAllDDL()
        Dim sAdd As String = ""
        If Session("oid") = Nothing And Session("oid") = "" Then
            sAdd = " AND suppoid IN (SELECT DISTINCT wom.suppoid FROM QL_trnwomst wom WHERE womststatus='Post' AND wom.womstoid IN (SELECT usm.womstoid FROM QL_trnusagemst usm))"
        End If
        'Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        FillDDL(DDLBusUnit, sSql)
        ' Fill DDL Group Name
        sSql = "SELECT suppoid, suppname FROM QL_mstsupp WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND suppgroup='Supplier Maklon' " & sAdd & " ORDER BY suppname"
        FillDDL(suppoid, sSql)
        sSql = "SELECT suppoid, suppname FROM QL_mstsupp WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND suppgroup='Supplier Maklon' ORDER BY suppname"
        FillDDLWithAdditionalText(FilterDDLGroupName, sSql, "None", "0")
        ' Fill DDL Unit
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='UNIT' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDL(prodresunitoid, sSql)
        FillDDL(sisaqtyunitoid, sSql)
        'Fill DDL Warehouse
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='WAREHOUSE' AND activeflag='ACTIVE' AND genoid IN (SELECT TOP 1 gx.genother1 FROM QL_mstgen gx WHERE gx.gengroup='GUDANG PRODUKSI' ORDER BY gx.updtime DESC) ORDER BY gendesc"
        FillDDL(warehouseoid, sSql)
    End Sub

    Private Sub InitDDLUnit(ByVal iItemOid As Integer)
        sSql = "select itemUnit1, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit1 where itemoid=" & iItemOid & " union all select itemUnit2, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit2 where itemoid=" & iItemOid & " union all select itemUnit3, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit3 where itemoid=" & iItemOid & ""
        FillDDL(sisaqtyunitoid, sSql)
    End Sub

    Private Sub InitDDLDept2()
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND activeflag='ACTIVE' ORDER BY deptname"
        FillDDL(deptoid, sSql)
        FillDDL(todeptoid, sSql)
    End Sub

    Private Sub InitDDLDept()
        ' Init DDL Department
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND activeflag='ACTIVE' AND deptoid IN (SELECT deptoid FROM QL_trnwodtl2 wod2 INNER JOIN QL_trnwomst wom ON wom.cmpcode=wod2.cmpcode AND wom.womstoid=wod2.womstoid  WHERE wod2.cmpcode='" & DDLBusUnit.SelectedValue & "' AND womststatus='Post' AND wodtl2resflag='' AND wodtl2type='FG' AND wod2.womstoid IN (SELECT usm.womstoid FROM QL_trnusagemst usm)) ORDER BY deptname"
        If FillDDL(deptoid, sSql) Then
            If deptoid.Items.Count <= 0 Then
                InitDDLDept2()
            Else
                InitDDLDeptReplace()
            End If
        End If
    End Sub

    Private Sub InitDDLDeptReplace()
        ' Init DDL Replacement Department
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND activeflag='ACTIVE' AND deptoid<>" & deptoid.SelectedValue & " ORDER BY deptname"
        FillDDLWithAdditionalText(deptreplaceoid, sSql, "None", "0")
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT * FROM (SELECT resm.cmpcode, resm.prodresmstoid, resm.prodresno, CONVERT(VARCHAR(10), resm.prodresdate, 101) prodresdate, resm.prodresdate sortdate, de.deptname, ISNULL(suppname, 'None') AS suppname, resm.prodresmststatus, resm.prodresmstnote, div.divname, 'False' AS checkvalue, (SELECT TOP 1 approductno FROM QL_trnapproductmst apm INNER JOIN QL_trnapproductdtl apd ON apd.cmpcode=apm.cmpcode AND apd.approductmstoid=apm.approductmstoid WHERE apd.cmpcode=resm.cmpcode AND apd.prodresmstoid=resm.prodresmstoid and apd.prodresdtloid>0) AS approductno, resm.createuser, resm.suppoid FROM QL_trnprodresmst resm INNER JOIN QL_mstdept de ON de.cmpcode=resm.cmpcode AND de.deptoid=resm.deptoid LEFT JOIN QL_mstsupp s ON s.suppoid=resm.suppoid INNER JOIN QL_mstdivision div ON div.cmpcode=resm.cmpcode) AS dtData WHERE"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " cmpcode='" & Session("CompnyCode") & "'"
        Else
            sSql &= " cmpcode LIKE '%'"
        End If
        sSql &= sSqlPlus & " ORDER BY prodresmstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trnprodresmst")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "prodresmstoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub PrintReport()
        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("prodresmstoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            report.Load(Server.MapPath(folderReport & "rptProdResult.rpt"))
            Dim sWhere As String = " WHERE"
            If Session("CompnyCode") <> CompnyCode Then
                sWhere &= " resm.cmpcode='" & Session("CompnyCode") & "'"
            Else
                sWhere &= " resm.cmpcode LIKE '%'"
            End If
            If sOid = "" Then
                sWhere = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND resm.prodresdate>='" & FilterPeriod1.Text & " 00:00:00' AND resm.prodresdate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbGroupName.Checked Then
                    sWhere &= " AND resm.suppoid=" & FilterDDLGroupName.SelectedValue
                End If
                If cbStatus.Checked Then
                    sWhere &= " AND resm.prodresmststatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Transaction\trnProdRes.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND resm.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sWhere &= " AND resm.prodresmstoid IN (" & sOid & ")"
            End If
            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "ProductionResultPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
        Response.Redirect("~\Transaction\trnProdRes.aspx?awal=true")
    End Sub

    Private Sub BindListKIK()
        Dim sAdd As String = ""
        If Session("oid") IsNot Nothing And Session("oid") <> "" Then
            sAdd = " AND resd.prodresmstoid<>" & Session("oid")
        End If
        sSql = "SELECT DISTINCT 'False' AS checkvalue, wom.womstoid, wod2.wodtl2oid, wom.wono,i.itemlongdescription AS itemlongdesc, CONVERT(VARCHAR(10), wom.wodate, 101) AS wodate, (CASE wod2.wodtl2type WHEN 'WIP' THEN de.deptname ELSE 'END' END) AS todeptname, wod2.todeptoid, wod2.wodtl2procseq, (wod2.wodtl2qty - ISNULL((SELECT SUM(resd.prodresqty) FROM QL_trnprodresdtl resd WHERE resd.cmpcode=wod2.cmpcode AND prodresdtlstatus<>'Cancel' AND resd.wodtl2oid=wod2.wodtl2oid" & sAdd & "), 0.0)) AS wodtl2qty, (CASE WHEN (SELECT COUNT(-1) FROM QL_trnwodtl2 wodx WHERE wodx.cmpcode=wod2.cmpcode AND wodx.womstoid=wod2.womstoid AND wodx.todeptoid=wod2.deptoid AND wodx.wodtl2type<>'FG')=0 THEN -1.0 ELSE (ISNULL((SELECT MIN(tbltmp.prodresqty) FROM (SELECT ISNULL(SUM(resd.prodresqty), 0.0) AS prodresqty FROM QL_trnwodtl2 wod2x LEFT JOIN QL_trnprodresdtl resd ON resd.cmpcode=wod2x.cmpcode AND resd.prodresdtlstatus<>'Cancel' AND resd.wodtl2oid=wod2x.wodtl2oid" & sAdd & " AND resd.prodresmstoid IN (SELECT resm.prodresmstoid FROM QL_trnprodresmst resm WHERE resm.cmpcode=wod2x.cmpcode AND resm.prodresmststatus IN ('Post', 'Closed')) WHERE wod2x.cmpcode=wod2.cmpcode AND (CASE wod2x.wodtl2type WHEN 'FG' THEN 0 ELSE wod2x.todeptoid END)=wod2.deptoid AND wod2x.womstoid=wod2.womstoid GROUP BY wod2x.wodtl2oid) AS tbltmp), 0.0) - ISNULL((SELECT SUM(resd.prodresqty) FROM QL_trnprodresdtl resd WHERE resd.cmpcode=wod2.cmpcode AND resd.wodtl2oid=wod2.wodtl2oid" & sAdd & "), 0.0)) END) AS resultmaxqty, ISNULL((SELECT TOP 1 i.roundqty AS itemlimitqty FROM QL_trnwodtl1 wod1 INNER JOIN QL_mstitem i ON i.itemoid=wod1.itemoid WHERE wod1.cmpcode=wod2.cmpcode AND wod1.wodtl1oid=wod2.wodtl1oid AND wod1.womstoid=wod2.womstoid ORDER BY wod1.wodtl1seq DESC), 1.0) AS resultlimitqty, wod2.wodtl2unitoid, g.gendesc AS wodtl2unit, wod2.wodtl2type, 0.0 AS prodresqty, ISNULL((SELECT TOP 1 i.itemcode FROM QL_trnwodtl1 wod1 INNER JOIN QL_mstitem i ON i.itemoid=wod1.itemoid WHERE wod1.cmpcode=wod2.cmpcode AND wod1.wodtl1oid=wod2.wodtl1oid AND wod1.womstoid=wod2.womstoid ORDER BY wod1.wodtl1seq DESC), '') AS itemcode, ISNULL((SELECT TOP 1 i.itemlongdescription AS itemlongdesc FROM QL_trnwodtl1 wod1 INNER JOIN QL_mstitem i ON i.itemoid=wod1.itemoid WHERE wod1.cmpcode=wod2.cmpcode AND wod1.wodtl1oid=wod2.wodtl1oid AND wod1.womstoid=wod2.womstoid ORDER BY wod1.wodtl1seq DESC), '') AS itemlongdesc, dlcamount AS dlcdtlamount, ohdamount AS dlcohdamount, /*ISNULL((SELECT curroid_ohd FROM QL_trnwodtl1 wod1 WHERE wod1.cmpcode=wod2.cmpcode AND wod1.wodtl1oid=wod2.wodtl1oid AND wod1.womstoid=wod2.womstoid), 0)*/ 1 AS curroid_ohd, 0.00 AS prodresqty_sisa, wod.itemoid, i.itemunit1 AS selectedunitoid, unit1.gendesc AS selectedunit, (CASE WHEN i.itemgroup='RAW' THEN 'RAW MATERIAL' ELSE 'FINISH GOOD' END) AS itemgroup, wom.lastconfirmdate, balanceqty_cogm, balanceamt_cogm, 0.00 AS confirmvalueidr, 0.00 AS confirmvalueusd, 0.00 AS confirmdmidr, 0.00 AS confirmdmusd, 0.00 AS confirmdlidr, 0.00 AS confirmdlusd, 0.00 AS confirmohdidr, 0.00 AS confirmohdusd, ISNULL((SELECT cat2res2 FROM QL_mstcat2 cat2 WHERE cat2.cat2oid=i.itemcat2),'') AS itemcat2, sup.suppoid, sup.suppname, kancingamt, kainamt, labelamt, wom.othervalue, 0.00 AS confirmkancingidr, 0.00 AS confirmkainidr, 0.00 AS confirmlabelidr, 0.00 AS confirmotheridr, ohdamount_use_ap bordir_use_ap, kancingamt_use_ap kancing_use_ap, kainamt_use_ap kain_use_ap, labelamt_use_ap label_use_ap, othervalue_use_ap other_use_ap, bordiramt bordirvalue, suppoid_bordir FROM QL_trnwodtl2 wod2 INNER JOIN QL_trnwomst wom ON wom.cmpcode=wod2.cmpcode AND wom.womstoid=wod2.womstoid INNER JOIN QL_mstdept de ON de.cmpcode=wod2.cmpcode AND de.deptoid=wod2.todeptoid INNER JOIN QL_mstgen g ON g.genoid=wod2.wodtl2unitoid INNER JOIN QL_trnwodtl1 wod on wod.womstoid=wom.womstoid and wod.wodtl1oid=wod2.wodtl1oid INNER JOIN QL_mstitem i on i.itemoid=wod.itemoid INNER JOIN QL_mstgen unit1 ON unit1.genoid=i.itemUnit1 INNER JOIN QL_mstsupp sup ON sup.suppoid=wom.suppoid WHERE wod2.cmpcode='" & DDLBusUnit.SelectedValue & "' AND wod2.deptoid=" & deptoid.SelectedValue & " AND wom.womststatus='Post' AND wod2.wodtl2resflag='' AND wom.suppoid=" & suppoid.SelectedValue & " ORDER BY wom.wono"
        Dim dtKIK As DataTable = cKon.ambiltabel(sSql, "QL_trnwodtl2")
        If dtKIK.DefaultView.Count > 0 Then
            For C1 As Integer = 0 To dtKIK.DefaultView.Count - 1
                If ToDouble(dtKIK.DefaultView(C1)("resultmaxqty").ToString) > 0 Then
                    If ToDouble(dtKIK.DefaultView(C1)("resultmaxqty").ToString) < ToDouble(dtKIK.DefaultView(C1)("wodtl2qty").ToString) Then
                        dtKIK.DefaultView(C1)("prodresqty") = ToDouble(dtKIK.DefaultView(C1)("resultmaxqty").ToString)
                    Else
                        dtKIK.DefaultView(C1)("prodresqty") = ToDouble(dtKIK.DefaultView(C1)("wodtl2qty").ToString)
                    End If
                Else
                    dtKIK.DefaultView(C1)("prodresqty") = ToDouble(dtKIK.DefaultView(C1)("wodtl2qty").ToString)
                End If
            Next
            Session("TblListKIK") = dtKIK.DefaultView.ToTable
            Session("TblListKIKView") = dtKIK.DefaultView.ToTable
            gvListKIK.DataSource = Session("TblListKIKView")
            gvListKIK.DataBind()
            cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, True)
        Else
            showMessage("There is no posting SPK data available for this time!", 2)
        End If
    End Sub

    Private Sub UpdateCheckedListKIK()
        If Session("TblListKIK") IsNot Nothing Then
            For C1 As Integer = 0 To gvListKIK.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListKIK.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                            Session("TblListKIK").DefaultView.RowFilter = "wodtl2oid=" & sOid
                            If cbcheck = True Then
                                If Session("TblListKIK").DefaultView.Count > 0 Then
                                    Session("TblListKIK").DefaultView(0)("checkvalue") = "True"
                                    Dim cc2 As System.Web.UI.ControlCollection = row.Cells(4).Controls
                                    Session("TblListKIK").DefaultView(0)("prodresqty") = ToDouble(GetTextValue(cc2))
                                End If
                            Else
                                If Session("TblListKIK").DefaultView.Count > 0 Then
                                    Session("TblListKIK").DefaultView(0)("checkvalue") = "False"
                                End If
                            End If
                            Session("TblListKIK").DefaultView.RowFilter = ""
                        End If
                    Next
                End If
            Next
        End If
        If Session("TblListKIKView") IsNot Nothing Then
            For C1 As Integer = 0 To gvListKIK.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListKIK.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                            Session("TblListKIKView").DefaultView.RowFilter = "wodtl2oid=" & sOid
                            If cbcheck = True Then
                                If Session("TblListKIKView").DefaultView.Count > 0 Then
                                    Session("TblListKIKView").DefaultView(0)("checkvalue") = "True"
                                    Dim cc2 As System.Web.UI.ControlCollection = row.Cells(4).Controls
                                    Session("TblListKIKView").DefaultView(0)("prodresqty") = ToDouble(GetTextValue(cc2))
                                End If
                            Else
                                If Session("TblListKIKView").DefaultView.Count > 0 Then
                                    Session("TblListKIKView").DefaultView(0)("checkvalue") = "False"
                                End If
                            End If
                            Session("TblListKIKView").DefaultView.RowFilter = ""
                        End If
                    Next
                End If
            Next
        End If
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("QL_trnprodresdtl")
        dtlTable.Columns.Add("prodresdtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl2oid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("womstoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wono", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodate", Type.GetType("System.String"))
        dtlTable.Columns.Add("prodrestype", Type.GetType("System.String"))
        dtlTable.Columns.Add("prodresprocseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("todeptoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("todeptname", Type.GetType("System.String"))
        dtlTable.Columns.Add("resultmaxqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl2qty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("resultlimitqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("prodresqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("prodresqty_unitkecil", Type.GetType("System.Double"))
        dtlTable.Columns.Add("prodresqty_unitbesar", Type.GetType("System.Double"))
        dtlTable.Columns.Add("sisaqty_unitkecil", Type.GetType("System.Double"))
        dtlTable.Columns.Add("sisaqty_unitbesar", Type.GetType("System.Double"))
        dtlTable.Columns.Add("prodresqty_sisa", Type.GetType("System.Double"))
        dtlTable.Columns.Add("prodresunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("sisaunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("prodresunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("sisaunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("prodresdtlnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("refname", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemcat2", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemlongdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("prodresdlcvalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("prodresdlcamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("prodresohdvalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("prodresohdamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("curroid_ohd", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("prodresohdvalueusd", Type.GetType("System.Double"))
        dtlTable.Columns.Add("prodresohdamtusd", Type.GetType("System.Double"))
        dtlTable.Columns.Add("whoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("warehouse", Type.GetType("System.String"))
        dtlTable.Columns.Add("confirmvalueidr", Type.GetType("System.Double"))
        dtlTable.Columns.Add("lastconfirmdate", Type.GetType("System.DateTime"))
        dtlTable.Columns.Add("balanceqty_cogm", Type.GetType("System.Double"))
        dtlTable.Columns.Add("balanceamt_cogm", Type.GetType("System.Double"))
        dtlTable.Columns.Add("suppoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("suppoid_bordir", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("suppname", Type.GetType("System.String"))
        dtlTable.Columns.Add("kancingamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("kainamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("labelamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("otheramt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("bordiramt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("kancingvalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("kainvalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("labelvalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("othervalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("bordirvalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("confirmdmidr", Type.GetType("System.Double"))
        dtlTable.Columns.Add("confirmdlidr", Type.GetType("System.Double"))
        dtlTable.Columns.Add("confirmohdidr", Type.GetType("System.Double"))
        dtlTable.Columns.Add("confirmkancingidr", Type.GetType("System.Double"))
        dtlTable.Columns.Add("confirmkainidr", Type.GetType("System.Double"))
        dtlTable.Columns.Add("confirmlabelidr", Type.GetType("System.Double"))
        dtlTable.Columns.Add("confirmotheridr", Type.GetType("System.Double"))
        dtlTable.Columns.Add("confirmbordiridr", Type.GetType("System.Double"))

        dtlTable.Columns.Add("bordir_use_ap", Type.GetType("System.String"))
        dtlTable.Columns.Add("kain_use_ap", Type.GetType("System.String"))
        dtlTable.Columns.Add("kancing_use_ap", Type.GetType("System.String"))
        dtlTable.Columns.Add("label_use_ap", Type.GetType("System.String"))
        dtlTable.Columns.Add("other_use_ap", Type.GetType("System.String"))
        dtlTable.Columns.Add("apresno", Type.GetType("System.String"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub ClearDetail()
        i_u2.Text = "New Detail"
        prodresdtlseq.Text = "1"
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            If dt.Rows.Count > 0 Then
                EnableHeader(False)
                prodresdtlseq.Text = dt.Rows.Count + 1
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        womstoid.Text = ""
        wodtl2oid.Text = ""
        prodrestype.Text = ""
        wono.Text = ""
        wodate.Text = ""
        todeptoid.SelectedIndex = -1
        resultmaxqty.Text = ""
        wodtl2qty.Text = ""
        resultlimitqty.Text = ""
        prodresunitoid.SelectedIndex = -1
        prodresprocseq.Text = ""
        If prodrestype.Text = "FG" Then
            lbltodept.Visible = False : septtodept.Visible = False : todeptoid.Visible = False
        Else
            lbltodept.Visible = True : septtodept.Visible = True : todeptoid.Visible = True
        End If
        prodresqty.Text = ""
        prodresqty_sisa.Text = ""
        prodresdtlnote.Text = ""
        prodresdlcvalue.Text = ""
        prodresohdvalue.Text = ""
        suppoid2.Text = ""
        suppname2.Text = ""
        gvDtl.SelectedIndex = -1
        btnSearchKIK.Visible = True
        warehouseoid.SelectedIndex = -1
        prodreskainvalue.Text = ""
        prodreskancingvalue.Text = ""
        prodreslabelvalue.Text = ""
        prodresothervalue.Text = ""
        prodresbordirvalue.Text = ""
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal : DDLBusUnit.CssClass = sCss
        deptoid.Enabled = bVal : deptoid.CssClass = sCss
        suppoid.Enabled = bVal : suppoid.CssClass = sCss
        prodresdate.Enabled = bVal : prodresdate.CssClass = sCss
    End Sub

    Private Sub GenerateResultNo()
        Dim sNo As String = "PRODRES-" & Format(GetServerTime(), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(prodresno, " & DefaultFormatCounter & ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnprodresmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND prodresno LIKE '" & sNo & "%'"
        prodresno.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
    End Sub

    Private Sub GenerateAPProdNo()
        Dim sNo As String = "APRES-" & Format(GetServerTime(), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(approductno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnapproductmst WHERE cmpcode='" & CompnyCode & "' AND approductno LIKE '" & sNo & "%'"
        approductno.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT prm.cmpcode, prm.prodresmstoid, prm.periodacctg, prodresdate, prodresno, deptoid, prm.suppoid, prodresmstnote, prodresmststatus, prm.createuser, prm.createtime, prm.upduser, prm.updtime, ISNULL(deptreplaceoid, 0) AS deptreplaceoid, apm.approductno FROM QL_trnprodresmst prm LEFT JOIN QL_trnapproductdtl apd ON apd.cmpcode=prm.cmpcode AND apd.prodresmstoid=prm.prodresmstoid LEFT JOIN QL_trnapproductmst apm ON apm.cmpcode=apd.cmpcode AND apm.approductmstoid=apd.approductmstoid WHERE prm.prodresmstoid=" & sOid
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = Trim(xreader("cmpcode").ToString)
                DDLBusUnit_SelectedIndexChanged(Nothing, Nothing)
                prodresmstoid.Text = Trim(xreader("prodresmstoid").ToString)
                periodacctg.Text = Trim(xreader("periodacctg").ToString)
                prodresdate.Text = Format(xreader("prodresdate"), "MM/dd/yyyy")
                prodresno.Text = Trim(xreader("prodresno").ToString)
                deptoid.SelectedValue = Trim(xreader("deptoid").ToString)
                deptoid_SelectedIndexChanged(Nothing, Nothing)
                suppoid.SelectedValue = Trim(xreader("suppoid").ToString)
                prodresmstnote.Text = Trim(xreader("prodresmstnote").ToString)
                prodresmststatus.Text = Trim(xreader("prodresmststatus").ToString)
                createuser.Text = Trim(xreader("createuser").ToString)
                createtime.Text = Trim(xreader("createtime").ToString)
                upduser.Text = Trim(xreader("upduser").ToString)
                updtime.Text = Trim(xreader("updtime").ToString)
                deptreplaceoid.SelectedValue = Trim(xreader("deptreplaceoid").ToString)
                approductno.Text = Trim(xreader("approductno").ToString)
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message & sSql, 1)
            btnSave.Visible = False : btnDelete.Visible = False : btnPost.Visible = False
            Exit Sub
        End Try
        If prodresmststatus.Text = "Post" Or prodresmststatus.Text = "Closed" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnPost.Visible = False
            lblTrnNo.Text = "Result No."
            prodresmstoid.Visible = False
            prodresno.Visible = True
            btnAddToList.Visible = False
            gvDtl.Columns(0).Visible = False
            gvDtl.Columns(gvDtl.Columns.Count - 1).Visible = False
        End If
        sSql = "SELECT DISTINCT resd.prodresdtlseq, resd.wodtl2oid, resd.womstoid, wom.wono, CONVERT(VARCHAR(10), wom.wodate, 101) AS wodate, resd.prodrestype, resd.prodresprocseq, resd.todeptoid, (CASE resd.prodrestype WHEN 'WIP' THEN de.deptname ELSE 'END' END) AS todeptname, (CASE WHEN (SELECT COUNT(-1) FROM QL_trnwodtl2 wodx WHERE wodx.cmpcode=wod2.cmpcode AND wodx.womstoid=wod2.womstoid AND wodx.todeptoid=wod2.deptoid AND wodx.wodtl2type<>'FG')=0 THEN -1.0 ELSE (ISNULL((SELECT MIN(tbltmp.prodresqty) FROM (SELECT ISNULL(SUM(resdx.prodresqty), 0.0) AS prodresqty FROM QL_trnwodtl2 wod2x LEFT JOIN QL_trnprodresdtl resdx ON resdx.cmpcode=wod2x.cmpcode AND resdx.prodresdtlstatus<>'Cancel' AND resdx.wodtl2oid=wod2x.wodtl2oid AND resdx.prodresmstoid<>resd.prodresmstoid AND resdx.prodresmstoid IN (SELECT resmx.prodresmstoid FROM QL_trnprodresmst resmx WHERE resmx.cmpcode=wod2x.cmpcode AND resmx.prodresmststatus IN ('Post', 'Closed')) WHERE wod2x.cmpcode=resd.cmpcode AND (CASE wod2x.wodtl2type WHEN 'FG' THEN 0 ELSE wod2x.todeptoid END)=wod2.deptoid AND wod2x.womstoid=resd.womstoid GROUP BY wod2x.wodtl2oid) AS tbltmp), 0.0) - ISNULL((SELECT SUM(resdx.prodresqty) FROM QL_trnprodresdtl resdx WHERE resdx.cmpcode=wod2.cmpcode AND resdx.prodresdtlstatus<>'Cancel' AND resdx.wodtl2oid=wod2.wodtl2oid AND resdx.prodresmstoid<>resd.prodresmstoid), 0.0)) END) AS resultmaxqty, (wod2.wodtl2qty - ISNULL((SELECT SUM(resdx.prodresqty) FROM QL_trnprodresdtl resdx WHERE resdx.cmpcode=resd.cmpcode AND resdx.prodresdtlstatus<>'Cancel' AND resdx.wodtl2oid=resd.wodtl2oid AND resdx.prodresmstoid<>resd.prodresmstoid), 0.0)) AS wodtl2qty, ISNULL((SELECT TOP 1 i.roundqty FROM QL_trnwodtl1 wod1 INNER JOIN QL_mstitem i ON i.itemoid=wod1.itemoid WHERE wod1.cmpcode=resd.cmpcode AND wod1.wodtl1oid=wod2.wodtl1oid AND wod1.womstoid=resd.womstoid ORDER BY wod1.wodtl1seq DESC), 1.0) AS resultlimitqty, resd.prodresqty, resd.prodresunitoid, g.gendesc AS prodresunit, resd.prodresdtlnote, ISNULL((SELECT TOP 1 i.itemcode FROM QL_trnwodtl1 wod1 INNER JOIN QL_mstitem i ON i.itemoid=wod1.itemoid WHERE wod1.cmpcode=resd.cmpcode AND wod1.wodtl1oid=wod2.wodtl1oid AND wod1.womstoid=resd.womstoid ORDER BY wod1.wodtl1seq DESC), '') AS itemcode, ISNULL((SELECT TOP 1 i.itemlongdescription FROM QL_trnwodtl1 wod1 INNER JOIN QL_mstitem i ON i.itemoid=wod1.itemoid WHERE wod1.cmpcode=resd.cmpcode AND wod1.wodtl1oid=wod2.wodtl1oid AND wod1.womstoid=resd.womstoid ORDER BY wod1.wodtl1seq DESC), '') AS itemlongdesc, prodresdlcvalue, prodresdlcamt, prodresohdvalue, prodresohdamt, ISNULL((SELECT 1 AS curroid_ohd), 0) AS curroid_ohd, 0.0 AS prodresohdvalueusd, 0.0 AS prodresohdamtusd, prodresqty_unitkecil, prodresqty_unitbesar, prodresqty_sisa, sisaqty_unitkecil, sisaqty_unitbesar, sisaunitoid, gx.gendesc AS sisaunit, ISNULL((SELECT TOP 1 wod1.itemoid FROM QL_trnwodtl1 wod1 WHERE wod1.womstoid=resd.womstoid ORDER BY wod1.wodtl1seq DESC), '') AS itemoid, ISNULL((SELECT TOP 1 (CASE WHEN m.itemgroup='RAW' THEN 'RAW MATERIAL' ELSE 'FINISH GOOD' END) FROM QL_trnwodtl1 wod1 INNER JOIN QL_mstitem m ON m.itemoid=wod1.itemoid WHERE wod1.womstoid=resd.womstoid ORDER BY wod1.wodtl1seq DESC), '') AS refname, ISNULL((SELECT TOP 1 cat2res2 FROM QL_mstcat2 cat2 INNER JOIN QL_mstitem i ON i.itemcat2=cat2.cat2oid WHERE i.itemoid=i.itemoid),'') AS itemcat2, whoid, gx.gendesc AS warehouse, wom.lastconfirmdate, balanceqty_cogm, balanceamt_cogm, 0.00 AS confirmvalueidr, 0.00 AS confirmvalueusd, 0.00 AS confirmdmidr, 0.00 AS confirmdmusd, 0.00 AS confirmdlidr, 0.00 AS confirmdlusd, 0.00 AS confirmohdidr, 0.00 AS confirmohdusd, sup.suppoid, sup.suppname, prodreskancingamt kancingamt, prodreskainamt kainamt, prodreslabelamt labelamt, prodresotheramt otheramt, 0.00 AS confirmkancingidr, 0.00 AS confirmkainidr, 0.00 AS confirmlabelidr, 0.00 AS confirmotheridr, 0.00 AS confirmbordiridr, prodreskancingvalue AS kancingvalue, prodreskainvalue AS kainvalue, prodreslabelvalue AS labelvalue, prodresothervalue othervalue, ohdamount_use_ap bordir_use_ap, kancingamt_use_ap kancing_use_ap, kainamt_use_ap kain_use_ap, labelamt_use_ap label_use_ap, othervalue_use_ap other_use_ap, resd.bordirvalue, resd.bordiramt, suppoid_bordir, '' apresno FROM QL_trnprodresdtl resd INNER JOIN QL_trnwodtl2 wod2 ON wod2.cmpcode=resd.cmpcode AND wod2.wodtl2oid=resd.wodtl2oid INNER JOIN QL_trnwomst wom ON wom.cmpcode=resd.cmpcode AND wom.womstoid=resd.womstoid INNER JOIN QL_mstdept de ON de.cmpcode=resd.cmpcode AND de.deptoid=resd.todeptoid INNER JOIN QL_mstgen g ON g.genoid=resd.prodresunitoid INNER JOIN QL_mstgen gx ON gx.genoid=resd.whoid INNER JOIN QL_mstsupp sup ON sup.suppoid=wom.suppoid WHERE resd.prodresmstoid=" & sOid & " ORDER BY resd.prodresdtlseq"
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnprodresdtl")
        Session("TblDtl") = dtTbl
        gvDtl.DataSource = dtTbl
        gvDtl.DataBind()
        ClearDetail()
        CountHdrAmount()
    End Sub

    Private Sub GetTotalCost(ByVal sKIKOid As String, ByRef varCost As VarCostValue, ByVal sStartDate As String)
        Dim sEndDate As String = GetServerTime().ToString()
        ' ==> GET DM IDR
        sSql = "SELECT SUM(totalcostidr) AS totalcostidr FROM ("
        sSql &= " SELECT ISNULL(SUM(usageqty * usagevalueidr), 0.0) AS totalcostidr FROM QL_trnusagedtl mud INNER JOIN QL_trnusagemst mum ON mum.cmpcode=mud.cmpcode AND mum.usagemstoid=mud.usagemstoid WHERE mud.cmpcode='" & DDLBusUnit.SelectedValue & "' AND mud.updtime>=CAST('" & sStartDate & "' AS DATETIME) AND mud.updtime<=CAST('" & sEndDate & "' AS DATETIME) AND usagemststatus<>'In Process' AND mum.usagemstoid>0 AND mum.womstoid=" & sKIKOid & " UNION ALL "
        sSql &= " SELECT SUM(totalcostidr) AS totalcostidr FROM (SELECT ISNULL((acaraqty * acaravalueidr), 0.0) AS totalcostidr FROM QL_trnbrtacaradtl bad INNER JOIN QL_trnbrtacaramst bam ON bam.cmpcode=bad.cmpcode AND bad.acaramstoid=bam.acaramstoid WHERE bad.cmpcode='" & DDLBusUnit.SelectedValue & "' AND bam.approvaldatetime>=CAST('" & sStartDate & "' AS DATETIME) AND bam.approvaldatetime<=CAST('" & sEndDate & "' AS DATETIME) AND acaramststatus IN ('Approved', 'Closed') AND bam.acaramstoid>0 AND bam.womstoid=" & sKIKOid & " AND acaratype<>'Material Return') AS QL_trnbrtacaradtl UNION ALL "
        sSql &= " SELECT (SUM(totalcostidr) * -1) AS totalcostidr FROM (SELECT ISNULL((retbaqty * retbavalueidr), 0.0) AS totalcostidr FROM QL_trnreturnbadtl retd INNER JOIN QL_trnreturnbamst retm ON retm.cmpcode=retd.cmpcode AND retm.retbamstoid=retd.retbamstoid INNER JOIN QL_trnbrtacaradtl bad ON bad.cmpcode=retd.cmpcode AND bad.acaradtloid=retd.acaradtloid AND bad.acaramstoid=retd.acaramstoid INNER JOIN QL_trnbrtacaramst bam ON bam.cmpcode=bad.cmpcode AND bad.acaramstoid=bam.acaramstoid WHERE retd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND retm.updtime>=CAST('" & sStartDate & "' AS DATETIME) AND retm.updtime<=CAST('" & sEndDate & "' AS DATETIME) AND retbamststatus<>'In Process' AND retm.retbamstoid>0 AND bam.acaramstoid>0 AND bam.womstoid=" & sKIKOid & ") AS QL_trnreturnbadtl UNION ALL"
        sSql &= " SELECT ISNULL(SUM(acaraqty * acaravalueidr), 0.0) * -1 AS totalcostidr FROM QL_trnbrtacaradtl bad INNER JOIN QL_trnbrtacaramst bam ON bam.cmpcode=bad.cmpcode AND bad.acaramstoid=bam.acaramstoid WHERE bad.cmpcode='" & DDLBusUnit.SelectedValue & "' AND bad.updtime>=CAST('" & sStartDate & "' AS DATETIME) AND bad.updtime<=CAST('" & sEndDate & "' AS DATETIME) AND acaramststatus IN ('Approved', 'Closed') AND bam.acaramstoid>0 AND bam.womstoid=" & sKIKOid & " AND acaratype='Material Return' "
        sSql &= ") AS tblUsageIDR"
        varCost.ValDM_IDR = ToDouble(GetStrData(sSql))

        ' ==> COUNT TOTAL COST
        varCost.ValCost_IDR = varCost.ValDM_IDR '+ varCost.ValDL_IDR + varCost.ValFOH_IDR
        varCost.ValCost_USD = varCost.ValDM_IDR '+ varCost.ValDL_USD + varCost.ValFOH_USD
    End Sub

    Private Sub PrintOutstandingTrans()
        Try
            report.Load(Server.MapPath(folderReport & "rptOutstandingConfirm.rpt"))

            Dim sKIKOid As String = ""
            Dim dtTbl As DataTable = Session("TblDtl")
            For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                If dtTbl.Rows(C1)("checkvalue").ToString.ToUpper = "TRUE" Then
                    sKIKOid &= dtTbl.Rows(C1)("womstoid").ToString & ","
                End If
            Next
            If sKIKOid <> "" Then
                sKIKOid = Left(sKIKOid, sKIKOid.Length - 1)
            End If

            ' Get data outstanding Material Request (Raw, Gen, SP, Kayu, By SO, By SO (Kayu))
            sSql = "SELECT tblData.*, wono [KIK No.], itemcode [Code FG], itemlongdesc [Description FG], (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=tblData.cmpcode) [Business Unit], groupcode [Division Code], groupdesc [Division Name] FROM ("
            sSql &= "SELECT 'Raw Material Request' [Form Name], CAST(h1.reqrawmstoid AS VARCHAR(10)) [Draft No.], h1.reqrawno [Trans. No.], h1.reqrawmststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], h1.womstoid [ID KIK], h1.cmpcode FROM QL_trnreqrawmst h1 FULL OUTER JOIN QL_trnusagerawdtl d1 ON d1.cmpcode=h1.cmpcode AND d1.reqrawmstoid=h1.reqrawmstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqrawmststatus IN ('In Process', 'Post') AND d1.reqrawmstoid IS NULL AND h1.womstoid IN (" & sKIKOid & ") UNION ALL "
            sSql &= "SELECT 'General Material Request' [Form Name], CAST(h1.reqgenmstoid AS VARCHAR(10)) [Draft No.], h1.reqgenno [Trans. No.], h1.reqgenmststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], h1.womstoid [ID KIK], h1.cmpcode FROM QL_trnreqgenmst h1 FULL OUTER JOIN QL_trnusagegendtl d1 ON d1.cmpcode=h1.cmpcode AND d1.reqgenmstoid=h1.reqgenmstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqgenmststatus IN ('In Process', 'Post') AND d1.reqgenmstoid IS NULL AND h1.womstoid IN (" & sKIKOid & ") UNION ALL "
            sSql &= "SELECT 'Spare Part Request' [Form Name], CAST(h1.reqspmstoid AS VARCHAR(10)) [Draft No.], h1.reqspno [Trans. No.], h1.reqspmststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], h1.womstoid [ID KIK], h1.cmpcode FROM QL_trnreqspmst h1 FULL OUTER JOIN QL_trnusagespdtl d1 ON d1.cmpcode=h1.cmpcode AND d1.reqspmstoid=h1.reqspmstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqspmststatus IN ('In Process', 'Post') AND d1.reqspmstoid IS NULL AND h1.womstoid IN (" & sKIKOid & ") UNION ALL "
            sSql &= "SELECT 'Material Request (Kayu)' [Form Name], CAST(h1.reqkayumstoid AS VARCHAR(10)) [Draft No.], h1.reqkayuno [Trans. No.], h1.reqkayumststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], h1.womstoid [ID KIK], h1.cmpcode FROM QL_trnreqkayumst h1 FULL OUTER JOIN QL_trnusagekayudtl d1 ON d1.cmpcode=h1.cmpcode AND d1.reqkayumstoid=h1.reqkayumstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqkayumststatus IN ('In Process', 'Post') AND d1.reqkayumstoid IS NULL AND h1.womstoid IN (" & sKIKOid & ") UNION ALL "
            sSql &= "SELECT 'Material Request By SO' [Form Name], CAST(h1.reqmstoid AS VARCHAR(10)) [Draft No.], h1.reqno [Trans. No.], h1.reqmststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], d1.womstoid [ID KIK], h1.cmpcode FROM QL_trnreqmst h1 INNER JOIN QL_trnreqdtl2 d1 ON d1.cmpcode=h1.cmpcode AND d1.reqmstoid=h1.reqmstoid FULL OUTER JOIN QL_trnusagedtl d2 ON d2.cmpcode=h1.cmpcode AND d2.reqmstoid=h1.reqmstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqmststatus IN ('In Process', 'Post') AND d2.reqmstoid IS NULL AND d1.womstoid IN (" & sKIKOid & ") UNION ALL "
            sSql &= "SELECT 'Material Request By SO (Kayu)' [Form Name], CAST(h1.req2mstoid AS VARCHAR(10)) [Draft No.], h1.req2no [Trans. No.], h1.req2mststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], d1.womstoid [ID KIK], h1.cmpcode FROM QL_trnreq2mst h1 INNER JOIN QL_trnreq2dtl2 d1 ON d1.cmpcode=h1.cmpcode AND d1.req2mstoid=h1.req2mstoid FULL OUTER JOIN QL_trnusage2dtl d2 ON d2.cmpcode=h1.cmpcode AND d2.req2mstoid=h1.req2mstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND req2mststatus IN ('In Process', 'Post') AND d2.req2mstoid IS NULL AND d1.womstoid IN (" & sKIKOid & ") UNION ALL "
            ' Get data outstanding Material Usage (Raw, Gen, SP, Kayu, By SO, By SO (Kayu))
            sSql &= "SELECT 'Raw Material Usage' [Form Name], CAST(h1.usagerawmstoid AS VARCHAR(10)) [Draft No.], h1.usagerawno [Trans. No.], h1.usagerawmststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], h2.womstoid [ID KIK], h1.cmpcode FROM QL_trnusagerawmst h1 INNER JOIN QL_trnusagerawdtl d1 ON d1.cmpcode=h1.cmpcode AND d1.usagerawmstoid=h1.usagerawmstoid INNER JOIN QL_trnreqrawmst h2 ON h2.cmpcode=d1.cmpcode AND h2.reqrawmstoid=d1.reqrawmstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagerawmststatus IN ('In Process') AND h2.womstoid IN (" & sKIKOid & ") UNION ALL "
            sSql &= "SELECT 'General Material Usage' [Form Name], CAST(h1.usagegenmstoid AS VARCHAR(10)) [Draft No.], h1.usagegenno [Trans. No.], h1.usagegenmststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], h2.womstoid [ID KIK], h1.cmpcode FROM QL_trnusagegenmst h1 INNER JOIN QL_trnusagegendtl d1 ON d1.cmpcode=h1.cmpcode AND d1.usagegenmstoid=h1.usagegenmstoid INNER JOIN QL_trnreqgenmst h2 ON h2.cmpcode=d1.cmpcode AND h2.reqgenmstoid=d1.reqgenmstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagegenmststatus IN ('In Process') AND h2.womstoid IN (" & sKIKOid & ") UNION ALL "
            sSql &= "SELECT 'Spare Part Usage' [Form Name], CAST(h1.usagespmstoid AS VARCHAR(10)) [Draft No.], h1.usagespno [Trans. No.], h1.usagespmststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], h2.womstoid [ID KIK], h1.cmpcode FROM QL_trnusagespmst h1 INNER JOIN QL_trnusagespdtl d1 ON d1.cmpcode=h1.cmpcode AND d1.usagespmstoid=h1.usagespmstoid INNER JOIN QL_trnreqspmst h2 ON h2.cmpcode=d1.cmpcode AND h2.reqspmstoid=d1.reqspmstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagespmststatus IN ('In Process') AND h2.womstoid IN (" & sKIKOid & ") UNION ALL "
            sSql &= "SELECT 'Material Usage (Kayu)' [Form Name], CAST(h1.usagekayumstoid AS VARCHAR(10)) [Draft No.], h1.usagekayuno [Trans. No.], h1.usagekayumststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], h2.womstoid [ID KIK], h1.cmpcode FROM QL_trnusagekayumst h1 INNER JOIN QL_trnusagekayudtl d1 ON d1.cmpcode=h1.cmpcode AND d1.usagekayumstoid=h1.usagekayumstoid INNER JOIN QL_trnreqkayumst h2 ON h2.cmpcode=d1.cmpcode AND h2.reqkayumstoid=d1.reqkayumstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagekayumststatus IN ('In Process') AND h2.womstoid IN (" & sKIKOid & ") UNION ALL "
            sSql &= "SELECT 'Material Usage By SO' [Form Name], CAST(h1.usagemstoid AS VARCHAR(10)) [Draft No.], h1.usageno [Trans. No.], h1.usagemststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], d2.womstoid [ID KIK], h1.cmpcode FROM QL_trnusagemst h1 INNER JOIN QL_trnusagedtl2 d1 ON d1.cmpcode=h1.cmpcode AND d1.usagemstoid=h1.usagemstoid INNER JOIN QL_trnreqdtl2 d2 ON d2.cmpcode=d1.cmpcode AND d2.reqdtl2oid=d1.reqdtl2oid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagemststatus IN ('In Process') AND d2.womstoid IN (" & sKIKOid & ") UNION ALL "
            sSql &= "SELECT 'Material Usage By SO (Kayu)' [Form Name], CAST(h1.usage2mstoid AS VARCHAR(10)) [Draft No.], h1.usage2no [Trans. No.], h1.usage2mststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], d2.womstoid [ID KIK], h1.cmpcode FROM QL_trnusage2mst h1 INNER JOIN QL_trnusage2dtl2 d1 ON d1.cmpcode=h1.cmpcode AND d1.usage2mstoid=h1.usage2mstoid INNER JOIN QL_trnreq2dtl2 d2 ON d2.cmpcode=d1.cmpcode AND d2.req2dtl2oid=d1.req2dtl2oid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND usage2mststatus IN ('In Process') AND d2.womstoid IN (" & sKIKOid & ") UNION ALL "
            ' Get data outstanding Material Return (By SO, By SO (Kayu))
            sSql &= "SELECT 'Material Return By SO' [Form Name], CAST(h1.retmstoid AS VARCHAR(10)) [Draft No.], h1.retno [Trans. No.], h1.retmststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], d3.womstoid [ID KIK], h1.cmpcode FROM QL_trnreturnmst h1 INNER JOIN QL_trnreturndtl2 d1 ON d1.cmpcode=h1.cmpcode AND d1.retmstoid=h1.retmstoid INNER JOIN QL_trnusagedtl2 d2 ON d2.cmpcode=d1.cmpcode AND d2.usagedtl2oid=d1.usagedtl2oid INNER JOIN QL_trnreqdtl2 d3 ON d3.cmpcode=d2.cmpcode AND d3.reqdtl2oid=d2.reqdtl2oid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND retmststatus IN ('In Process') AND d3.womstoid IN (" & sKIKOid & ") UNION ALL "
            sSql &= "SELECT 'Material Return By SO (Kayu)' [Form Name], CAST(h1.ret2mstoid AS VARCHAR(10)) [Draft No.], h1.ret2no [Trans. No.], h1.ret2mststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], d3.womstoid [ID KIK], h1.cmpcode FROM QL_trnreturn2mst h1 INNER JOIN QL_trnreturn2dtl2 d1 ON d1.cmpcode=h1.cmpcode AND d1.ret2mstoid=h1.ret2mstoid INNER JOIN QL_trnusage2dtl2 d2 ON d2.cmpcode=d1.cmpcode AND d2.usage2dtl2oid=d1.usage2dtl2oid INNER JOIN QL_trnreq2dtl2 d3 ON d3.cmpcode=d2.cmpcode AND d3.req2dtl2oid=d2.req2dtl2oid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND ret2mststatus IN ('In Process') AND d3.womstoid IN (" & sKIKOid & ") UNION ALL "
            ' Get data outstanding Berita Acara
            sSql &= "SELECT 'Berita Acara' [Form Name], CAST(h1.acaramstoid AS VARCHAR(10)) [Draft No.], h1.acarano [Trans. No.], h1.acaramststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], h1.womstoid [ID KIK], h1.cmpcode FROM QL_trnbrtacaramst h1 WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND acaramststatus IN ('In Process', 'In Approval', 'Revised') AND h1.womstoid IN (" & sKIKOid & ") UNION ALL "
            ' Get data outstanding Berita Acara Product
            sSql &= "SELECT 'Berita Acara Product' [Form Name], CAST(h1.acaraprodmstoid AS VARCHAR(10)) [Draft No.], h1.acaraprodno [Trans. No.], h1.acaraprodmststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], d1.womstoid [ID KIK], h1.cmpcode FROM QL_trnbrtacaraprodmst h1 INNER JOIN QL_trnbrtacaraproddtl d1 ON d1.cmpcode=h1.cmpcode AND d1.acaraprodmstoid=h1.acaraprodmstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND acaraprodmststatus IN ('In Process', 'In Approval', 'Revised') AND d1.womstoid IN (" & sKIKOid & ") UNION ALL "
            ' Get data outstanding Production Result
            sSql &= "SELECT 'Production Result' [Form Name], CAST(h1.prodresmstoid AS VARCHAR(10)) [Draft No.], h1.prodresno [Trans. No.], h1.prodresmststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], d1.womstoid [ID KIK], h1.cmpcode FROM QL_trnprodresmst h1 INNER JOIN QL_trnprodresdtl d1 ON d1.cmpcode=h1.cmpcode AND d1.prodresmstoid=h1.prodresmstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND prodresmststatus IN ('In Process') AND d1.womstoid IN (" & sKIKOid & ")"
            sSql &= ") AS tblData INNER JOIN QL_trnwomst wom ON wom.cmpcode=tblData.cmpcode AND wom.womstoid=tblData.[ID KIK] INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=wom.cmpcode AND wod1.womstoid=wom.womstoid INNER JOIN QL_trnsoitemmst som ON som.cmpcode=wod1.cmpcode AND som.soitemmstoid=wod1.soitemmstoid INNER JOIN QL_mstdeptgroup dg ON dg.cmpcode=som.cmpcode AND dg.groupoid=som.groupoid INNER JOIN QL_mstitem i ON i.itemoid=wod1.itemoid"
            Dim dtOutstanding As DataTable = cKon.ambiltabel(sSql, "tblData")

            report.SetDataSource(dtOutstanding)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            report.PrintOptions.PaperSize = PaperSize.PaperA4
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "OutstandingConfirmPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Private Sub CountHdrAmount()
        Dim dAmt As Double = 0, dTax As Double = 0
        Dim dDlcAmt As Double = 0, dTotalDLC As Double = 0
        Dim dKancingAmt As Double = 0, dTotalKancingAmt As Double = 0, dTotalKainAmt As Double = 0, dTotalLabelAmt As Double = 0, dTotalBordirAmt As Double = 0, dTotalOtherAmt As Double = 0
        If Not Session("TblDtl") Is Nothing Then
            Dim objTable As DataTable = Session("TblDtl")
            Dim dDLC As Object = objTable.Compute("SUM(prodresdlcamt)", "")
            Dim dKancing As Object = objTable.Compute("SUM(kancingamt)", "kancing_use_ap='Y'")
            Dim dKain As Object = objTable.Compute("SUM(kainamt)", "kain_use_ap='Y'")
            Dim dLabel As Object = objTable.Compute("SUM(labelamt)", "label_use_ap='Y'")
            Dim dBordir As Object = objTable.Compute("SUM(prodresohdamt)", "bordir_use_ap='Y'")
            Dim dOther As Object = objTable.Compute("SUM(otheramt)", "other_use_ap='Y'")

            dTotalDLC = IIf(IsDBNull(dDLC), 0, dDLC)
            dTotalKancingAmt = IIf(IsDBNull(dKancing), 0, dKancing)
            dTotalKainAmt = IIf(IsDBNull(dKain), 0, dKain)
            dTotalLabelAmt = IIf(IsDBNull(dLabel), 0, dLabel)
            dTotalBordirAmt = IIf(IsDBNull(dBordir), 0, dBordir)
            dTotalOtherAmt = IIf(IsDBNull(dOther), 0, dOther)
        End If
        approductgrandtotal.Text = ToMaskEdit(dTotalDLC + dTotalKancingAmt + dTotalKainAmt + dTotalLabelAmt + dTotalBordirAmt + dTotalOtherAmt, 2)
    End Sub

    Private Sub ReGenerateAPResNo(ByVal sOid As Integer)
        sSql = "SELECT * FROM QL_trnapproductmst WHERE prodresmstoid=" & sOid & " ORDER BY updtime"
        Dim objTable As DataTable = cKon.ambiltabel(sSql, "QL_trnapproductmst")
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            For C1 As Integer = 0 To objTable.Rows.Count - 1
                Dim sNo As String = GenerateAPProdNo2(C1)
                sSql = "UPDATE QL_trnapproductmst SET approductno='" & sNo & "' WHERE approductmstoid=" & objTable.Rows(C1)("approductmstoid")
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            Next
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            showMessage(ex.ToString, 1)
            conn.Close()
            Exit Sub
        End Try
    End Sub

#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnProdRes.aspx")
        End If
        If checkPagePermission("~\Transaction\trnProdRes.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Production Result"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPost.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            CheckResultStatus()
            InitAllDDL()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitDDLDept()
            suppoid_SelectedIndexChanged(Nothing, Nothing)
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                prodresmstoid.Text = GenerateID("QL_TRNPRODRESMST", CompnyCode)
                prodresmststatus.Text = "In Process"
                prodresdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime)
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvDtl.DataSource = dt
            gvDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListKIK") Is Nothing And Session("WarningListKIK") <> "" Then
            Session("WarningListKIK") = Nothing
            cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, True)
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnProdRes.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lkbResultInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbResultInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = 0
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, resm.updtime, GETDATE()) > " & nDays & " AND resm.prodresmststatus='In Process'"
        If checkPagePermission("~\Transaction\trnProdRes.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND resm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND sortdate>='" & FilterPeriod1.Text & " 00:00:00' AND sortdate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbGroupName.Checked Then
            sSqlPlus &= " AND suppoid=" & FilterDDLGroupName.SelectedValue
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND prodresmststatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Transaction\trnProdRes.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = ""
        FilterDDL.SelectedIndex = 0
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbGroupName.Checked = False
        FilterDDLGroupName.SelectedIndex = -1
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Transaction\trnProdRes.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND resm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        UpdateCheckedValue()
        PrintReport()
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLDept()
        deptoid_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub deptoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles deptoid.SelectedIndexChanged
        If deptoid.Items.Count <= 0 Then
            InitDDLDept2()
        Else
            InitDDLDeptReplace()
        End If
        ClearDetail()
    End Sub

    Protected Sub btnSearchKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchKIK.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        If deptoid.SelectedValue = "" Then
            showMessage("Please select Department first!", 2)
            Exit Sub
        End If
        FilterDDLListKIK.SelectedIndex = -1 : FilterTextListKIK.Text = "" : gvListKIK.DataSource = Nothing : gvListKIK.DataBind() : Session("TblListKIK") = Nothing : Session("TblListKIKView") = Nothing
        BindListKIK()
    End Sub

    Protected Sub btnFindListKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListKIK.Click
        UpdateCheckedListKIK()
        If Session("TblListKIK") IsNot Nothing Then
            Session("TblListKIK").DefaultView.RowFilter = FilterDDLListKIK.SelectedValue & " LIKE '%" & Tchar(FilterTextListKIK.Text) & "%'"
            If Session("TblListKIK").DefaultView.Count > 0 Then
                Session("TblListKIKView") = Session("TblListKIK").DefaultView.ToTable
                gvListKIK.DataSource = Session("TblListKIKView")
                gvListKIK.DataBind()
                mpeListKIK.Show()
            Else
                Session("WarningListKIK") = "KIK data can't be found!"
                showMessage(Session("WarningListKIK"), 2)
            End If
        Else
            Session("WarningListKIK") = "There is no posting KIK data available for this time!"
            showMessage(Session("WarningListKIK"), 2)
        End If
    End Sub

    Protected Sub btnAllListKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListKIK.Click
        FilterDDLListKIK.SelectedIndex = -1 : FilterTextListKIK.Text = "" : gvListKIK.SelectedIndex = -1
        UpdateCheckedListKIK()
        If Session("TblListKIK") IsNot Nothing Then
            Session("TblListKIKView") = Session("TblListKIK")
            gvListKIK.DataSource = Session("TblListKIKView")
            gvListKIK.DataBind()
            mpeListKIK.Show()
        Else
            Session("WarningListKIK") = "There is no posting KIK data available for this time!"
            showMessage(Session("WarningListKIK"), 2)
        End If
    End Sub

    Protected Sub cbHdrListKIK_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim bVal As Boolean = sender.Checked
        For C1 As Integer = 0 To gvListKIK.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListKIK.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = bVal
                    End If
                Next
            End If
        Next
        mpeListKIK.Show()
    End Sub

    Protected Sub gvListKIK_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListKIK.PageIndexChanging
        UpdateCheckedListKIK()
        gvListKIK.PageIndex = e.NewPageIndex
        gvListKIK.DataSource = Session("TblListKIKView")
        gvListKIK.DataBind()
        mpeListKIK.Show()
    End Sub

    Protected Sub gvListKIK_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListKIK.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 2)
        End If
    End Sub

    Protected Sub lkbAddToListListKIK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListKIK.Click
        UpdateCheckedListKIK()
        If Session("TblListKIK") IsNot Nothing Then
            Dim dv As DataView = Session("TblListKIK").DefaultView
            dv.RowFilter = "checkvalue='True'"
            Dim iCount As Integer = dv.Count
            If dv.Count > 0 Then
                dv.RowFilter = "checkvalue='True' AND prodresqty > 0"
                If dv.Count <> iCount Then
                    Session("WarningListKIK") = "Result Qty for every selected KIK data must be more than 0!"
                    showMessage(Session("WarningListKIK"), 2)
                    dv.RowFilter = "" : Exit Sub
                End If
                dv.RowFilter = "checkvalue='True' AND prodresqty <= wodtl2qty"
                If dv.Count <> iCount Then
                    Session("WarningListKIK") = "Result Qty for every selected KIK data must be less or equal than SPK Qty!"
                    showMessage(Session("WarningListKIK"), 2)
                    dv.RowFilter = "" : Exit Sub
                End If
                dv.RowFilter = "checkvalue='True'"
                If Not IsQtyRounded(dv, "prodresqty", "resultlimitqty") Then
                    Session("WarningListKIK") = "Result Qty for every selected KIK data must be rounded by Round Qty!"
                    showMessage(Session("WarningListKIK"), 2)
                    dv.RowFilter = "" : Exit Sub
                End If
                dv.RowFilter = "checkvalue='True'"
                If Session("TblDtl") Is Nothing Then
                    CreateTblDetail()
                End If
                Dim objTable As DataTable = Session("TblDtl")
                Dim objView As DataView = objTable.DefaultView
                Dim iSeq As Integer = objTable.Rows.Count + 1
                For C1 As Integer = 0 To dv.Count - 1
                    objView.RowFilter = "wodtl2oid=" & dv(C1)("wodtl2oid")
                    If objView.Count > 0 Then
                        objView(0)("prodresqty") = ToDouble(dv(C1)("prodresqty").ToString)
                        objView(0)("prodresdlcvalue") = ToDouble(dv(C1)("dlcdtlamount").ToString)
                        objView(0)("prodresdlcamt") = ToDouble(dv(C1)("prodresqty").ToString) * ToDouble(dv(C1)("dlcdtlamount").ToString)
                        objView(0)("prodresohdvalue") = ToDouble(dv(C1)("dlcohdamount").ToString)
                        objView(0)("prodresohdamt") = ToDouble(dv(C1)("prodresqty").ToString) * ToDouble(dv(C1)("dlcohdamount").ToString)
                        objView(0)("kancingvalue") = ToDouble(dv(C1)("kancingamt").ToString)
                        objView(0)("kancingamt") = ToDouble(dv(C1)("prodresqty").ToString) * ToDouble(dv(C1)("kancingamt").ToString)
                        objView(0)("kainvalue") = ToDouble(dv(C1)("kainamt").ToString)
                        objView(0)("kainamt") = ToDouble(dv(C1)("prodresqty").ToString) * ToDouble(dv(C1)("kainamt").ToString)
                        objView(0)("labelvalue") = ToDouble(dv(C1)("labelamt").ToString)
                        objView(0)("labelamt") = ToDouble(dv(C1)("prodresqty").ToString) * ToDouble(dv(C1)("labelamt").ToString)
                        objView(0)("othervalue") = ToDouble(dv(C1)("othervalue").ToString)
                        objView(0)("otheramt") = ToDouble(dv(C1)("prodresqty").ToString) * ToDouble(dv(C1)("othervalue").ToString)
                        objView(0)("curroid_ohd") = ToInteger(dv(C1)("curroid_ohd").ToString)
                        objView(0)("whoid") = warehouseoid.SelectedValue
                        objView(0)("warehouse") = warehouseoid.SelectedItem.Text
                        objView(0)("bordirvalue") = ToDouble(dv(C1)("bordirvalue").ToString)
                        objView(0)("bordiramt") = ToDouble(dv(C1)("prodresqty").ToString) * ToDouble(dv(C1)("bordirvalue").ToString)
                        objView(0)("suppoid_bordir") = dv(C1)("suppoid_bordir").ToString
                    Else
                        Dim rv As DataRowView = objView.AddNew()
                        rv.BeginEdit()
                        rv("prodresdtlseq") = iSeq
                        rv("wodtl2oid") = dv(C1)("wodtl2oid")
                        rv("womstoid") = dv(C1)("womstoid")
                        rv("wono") = dv(C1)("wono").ToString
                        rv("wodate") = dv(C1)("wodate").ToString
                        rv("prodrestype") = dv(C1)("wodtl2type").ToString
                        rv("prodresprocseq") = dv(C1)("wodtl2procseq")
                        rv("todeptoid") = dv(C1)("todeptoid")
                        rv("todeptname") = dv(C1)("todeptname").ToString
                        rv("resultmaxqty") = ToDouble(dv(C1)("resultmaxqty").ToString)
                        rv("wodtl2qty") = ToDouble(dv(C1)("wodtl2qty").ToString)
                        rv("resultlimitqty") = ToDouble(dv(C1)("resultlimitqty").ToString)
                        rv("prodresqty") = ToDouble(dv(C1)("prodresqty").ToString)
                        rv("prodresunitoid") = dv(C1)("wodtl2unitoid")
                        rv("sisaunitoid") = dv(C1)("selectedunitoid")
                        rv("prodresunit") = dv(C1)("wodtl2unit").ToString
                        rv("sisaunit") = dv(C1)("selectedunit").ToString
                        rv("prodresdtlnote") = ""
                        rv("itemoid") = dv(C1)("itemoid").ToString
                        rv("refname") = dv(C1)("itemgroup").ToString
                        rv("itemcode") = dv(C1)("itemcode").ToString
                        rv("itemlongdesc") = dv(C1)("itemlongdesc").ToString
                        rv("prodresdlcvalue") = ToDouble(dv(C1)("dlcdtlamount").ToString)
                        rv("prodresdlcamt") = ToDouble(dv(C1)("prodresqty").ToString) * ToDouble(dv(C1)("dlcdtlamount").ToString)
                        rv("prodresohdvalue") = ToDouble(dv(C1)("dlcohdamount").ToString)
                        rv("prodresohdamt") = ToDouble(dv(C1)("prodresqty").ToString) * ToDouble(dv(C1)("dlcohdamount").ToString)
                        rv("curroid_ohd") = ToInteger(dv(C1)("curroid_ohd").ToString)
                        rv("lastconfirmdate") = dv(C1)("lastconfirmdate").ToString
                        rv("whoid") = warehouseoid.SelectedValue
                        rv("warehouse") = warehouseoid.SelectedItem.Text
                        rv("balanceqty_cogm") = ToDouble(dv(C1)("balanceqty_cogm").ToString)
                        rv("balanceamt_cogm") = ToDouble(dv(C1)("balanceamt_cogm").ToString)
                        rv("itemcat2") = dv(C1)("itemcat2").ToString
                        rv("suppoid") = dv(C1)("suppoid").ToString
                        rv("suppname") = dv(C1)("suppname").ToString
                        rv("kancingvalue") = ToDouble(dv(C1)("kancingamt").ToString)
                        rv("kancingamt") = ToDouble(dv(C1)("prodresqty").ToString) * ToDouble(dv(C1)("kancingamt").ToString)
                        rv("kainvalue") = ToDouble(dv(C1)("kainamt").ToString)
                        rv("kainamt") = ToDouble(dv(C1)("prodresqty").ToString) * ToDouble(dv(C1)("kainamt").ToString)
                        rv("labelvalue") = ToDouble(dv(C1)("labelamt").ToString)
                        rv("labelamt") = ToDouble(dv(C1)("prodresqty").ToString) * ToDouble(dv(C1)("labelamt").ToString)
                        rv("othervalue") = ToDouble(dv(C1)("othervalue").ToString)
                        rv("otheramt") = ToDouble(dv(C1)("prodresqty").ToString) * ToDouble(dv(C1)("othervalue").ToString)
                        rv("bordirvalue") = ToDouble(dv(C1)("bordirvalue").ToString)
                        rv("bordiramt") = ToDouble(dv(C1)("prodresqty").ToString) * ToDouble(dv(C1)("bordirvalue").ToString)
                        rv("suppoid_bordir") = dv(C1)("suppoid_bordir").ToString

                        rv("bordir_use_ap") = dv(C1)("bordir_use_ap").ToString
                        rv("kancing_use_ap") = dv(C1)("kancing_use_ap").ToString
                        rv("kain_use_ap") = dv(C1)("kain_use_ap").ToString
                        rv("label_use_ap") = dv(C1)("label_use_ap").ToString
                        rv("other_use_ap") = dv(C1)("other_use_ap").ToString

                        rv.EndEdit()
                        iSeq += 1
                    End If
                    objView.RowFilter = ""
                Next
                dv.RowFilter = ""
                Session("TblDtl") = objTable
                gvDtl.DataSource = Session("TblDtl")
                gvDtl.DataBind()
                ClearDetail()
                CountHdrAmount()
                cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, False)
            Else
                Session("WarningListKIK") = "Please select KIK data first!"
                showMessage(Session("WarningListKIK"), 2)
            End If
        Else
            Session("WarningListKIK") = "There is no posting KIK data available for this time!"
            showMessage(Session("WarningListKIK"), 2)
        End If
    End Sub

    Protected Sub lkbCloseListKIK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListKIK.Click
        cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, False)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            If Session("TblDtl") IsNot Nothing Then
                Dim objTable As DataTable = Session("TblDtl")
                Dim objRow As DataRow
                objRow = objTable.Rows(prodresdtlseq.Text - 1)
                objRow.BeginEdit()
                objRow("prodresqty") = ToDouble(prodresqty.Text)
                objRow("prodresdtlnote") = prodresdtlnote.Text
                objRow("prodresdlcamt") = ToDouble(prodresqty.Text) * ToDouble(prodresdlcvalue.Text)
                objRow("prodresohdamt") = ToDouble(prodresqty.Text) * ToDouble(prodresohdvalue.Text)
                objRow("kancingamt") = ToDouble(prodresqty.Text) * ToDouble(prodreskancingvalue.Text)
                objRow("kainamt") = ToDouble(prodresqty.Text) * ToDouble(prodreskainvalue.Text)
                objRow("labelamt") = ToDouble(prodresqty.Text) * ToDouble(prodreslabelvalue.Text)
                objRow("otheramt") = ToDouble(prodresqty.Text) * ToDouble(prodresothervalue.Text)
                objRow("bordiramt") = ToDouble(prodresqty.Text) * ToDouble(prodresbordirvalue.Text)
                objRow("whoid") = warehouseoid.SelectedValue
                objRow("warehouse") = warehouseoid.SelectedItem.Text
                objRow("suppoid") = suppoid2.Text
                objRow("suppname") = suppname2.Text

                objRow.EndEdit()
                Session("TblDtl") = objTable
                gvDtl.DataSource = Session("TblDtl")
                gvDtl.DataBind()
                ClearDetail()
                CountHdrAmount()
            End If
            End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 2)
        End If
    End Sub

    Protected Sub gvDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("prodresdtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvDtl.DataSource = objTable
        gvDtl.DataBind()
        ClearDetail()
        CountHdrAmount()
    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDtl.SelectedIndexChanged
        Try
            prodresdtlseq.Text = gvDtl.SelectedDataKey.Item("prodresdtlseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "prodresdtlseq=" & prodresdtlseq.Text
                wodtl2oid.Text = dv.Item(0).Item("wodtl2oid").ToString
                womstoid.Text = dv.Item(0).Item("womstoid").ToString
                wono.Text = dv.Item(0).Item("wono").ToString
                wodate.Text = dv.Item(0).Item("wodate").ToString
                prodrestype.Text = dv.Item(0).Item("prodrestype").ToString
                prodresprocseq.Text = dv.Item(0).Item("prodresprocseq").ToString
                If prodrestype.Text = "FG" Then
                    lbltodept.Visible = False : septtodept.Visible = False : todeptoid.Visible = False
                    todeptoid.Items.Clear()
                Else
                    lbltodept.Visible = True : septtodept.Visible = True : todeptoid.Visible = True
                    todeptoid.SelectedValue = dv.Item(0).Item("todeptoid").ToString
                End If
                resultmaxqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("resultmaxqty").ToString), 2)
                If ToDouble(resultmaxqty.Text) = -1 Then
                    resultmaxqty.Text = "Null"
                End If
                wodtl2qty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("wodtl2qty").ToString), 2)
                resultlimitqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("resultlimitqty").ToString), 2)
                prodresqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("prodresqty").ToString), 2)
                prodresqty_sisa.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("prodresqty_sisa").ToString), 2)
                prodresunitoid.SelectedValue = dv.Item(0).Item("prodresunitoid").ToString
                sisaqtyunitoid.SelectedValue = dv.Item(0).Item("sisaunitoid").ToString
                prodresdtlnote.Text = dv.Item(0).Item("prodresdtlnote").ToString
                itemrefoid.Text = dv.Item(0).Item("itemoid").ToString
                prodresdlcvalue.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("prodresdlcvalue").ToString), 2)
                prodresohdvalue.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("prodresohdvalue").ToString), 2)
                warehouseoid.SelectedValue = dv.Item(0).Item("whoid").ToString
                suppoid2.Text = dv.Item(0).Item("suppoid").ToString
                suppname2.Text = dv.Item(0).Item("suppname").ToString
                prodreskancingvalue.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("kancingvalue").ToString), 2)
                prodreskainvalue.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("kainvalue").ToString), 2)
                prodreslabelvalue.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("labelvalue").ToString), 2)
                prodresothervalue.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("othervalue").ToString), 2)
                prodresbordirvalue.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("bordirvalue").ToString), 2)

                dv.RowFilter = ""
                btnSearchKIK.Visible = False
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim dtTbl As DataTable = Nothing
            Dim objTable As DataTable = Nothing
            If Not Session("TblDtl") Is Nothing Then
                dtTbl = Session("TblDtl")
                objTable = Session("TblDtl")
            End If

            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                If CheckDataExists("SELECT COUNT(*) FROM QL_trnprodresmst WHERE prodresmstoid=" & prodresmstoid.Text) Then
                    prodresmstoid.Text = GenerateID("QL_TRNPRODRESMST", CompnyCode)
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_trnprodresmst", "prodresmstoid", prodresmstoid.Text, "prodresmststatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    prodresmststatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            Dim sErrReply As String = ""
            If prodresmststatus.Text = "Post" Then
                If Not Session("TblDtl") Is Nothing Then
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        If Not isLengthAccepted("prodresqty", "QL_trnprodresdtl", ToDouble(objTable.Rows(C1)("prodresqty").ToString), sErrReply) Then
                            showMessage("Prodres Qty for every selected data must be less than Max. Confirm Qty (" & sErrReply & ") allowed stored in database!", 2)
                            Exit Sub
                        End If
                        'If ToDouble(objTable.Rows(C1)("balanceqty_cogm").ToString) - ToDouble(objTable.Rows(C1)("prodresqty").ToString) <= 0 Then
                        sErrReply = GetOutstandingTrans(objTable.Rows(C1)("womstoid").ToString, objTable.Rows(C1)("prodresdtlseq").ToString)
                        If sErrReply <> "" Then
                            showMessage(sErrReply, 2, True)
                            Exit Sub
                        End If
                        'End If
                    Next
                End If
            End If
            periodacctg.Text = GetDateToPeriodAcctg(prodresdate.Text)
            prodresdtloid.Text = GenerateID("QL_TRNPRODRESDTL", CompnyCode)
            Dim iConAPOid As Int32 = GenerateID("QL_CONAP", CompnyCode)
            Dim dDueDate As Date = DateAdd(DateInterval.Day, ToDouble(GetNumericValues(approductpaytypeoid.Text)), CDate(prodresdate.Text))
            Dim iGLMstOid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
            Dim iGLDtlOid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
            Dim iConfirmOid As Integer = GenerateID("QL_TRNPRODRESCONFIRM", CompnyCode)
            Dim iConMatOid As Integer = GenerateID("QL_CONSTOCK", CompnyCode)
            Dim iCrdMtrOid As Integer = GenerateID("QL_CRDSTOCK", CompnyCode)
            Dim iStockValOid As Integer = GenerateID("QL_STOCKVALUE", CompnyCode)
            Dim iAPProductOid As Integer = GenerateID("QL_TRNAPPRODUCTMST", CompnyCode)
            Dim iAPProductDtlOid As Integer = GenerateID("QL_TRNAPPRODUCTDTL", CompnyCode)
            Dim dToday As Date = CDate(Format(GetServerTime(), "MM/dd/yyyy"))
            Dim sPeriod As String = GetDateToPeriodAcctg(dToday)
            Dim iStockWIPAcctgOid, iStockFGAcctgOid, iAPAcctgOid, iPPNAcctgOid As Integer
            iStockWIPAcctgOid = GetAcctgOID(GetVarInterface("VAR_STOCK_WIP", CompnyCode), CompnyCode) 'GetAcctgStock("WIP")
            iStockFGAcctgOid = GetAcctgOID(GetVarInterface("VAR_STOCK_FG", CompnyCode), CompnyCode) 'GetAcctgStock("FG")
            Dim dTotalAmtIDR As Double = 0, dTotalAmtUSD As Double = 0, dTotalAmtIDR_unitkecil As Double = 0, dTotalAmtUSD_unitkecil As Double = 0, dQty_unitkecil As Double = 0, dQty_unitbesar As Double = 0
            Dim iDLAssignAcctgOid, iBOPBebanAcctgOid, iBOPRealAcctgOid, iSelisihAcctgOid As Integer
            Dim dTotalDL As Double = 0, dTotalDLIDR As Double = 0, dTotalDLUSD As Double = 0, dTotalOHD As Double = 0, dTotalOHDIDR As Double = 0, dTotalOHDUSD As Double = 0, DLAMT As Double = 0, OHDAMT As Double = 0, dKancingAmt As Double = 0, dKainAmt As Double = 0, dLabelAmt As Double = 0, dOtherAmt As Double = 0, dTotalCost As Decimal = 0, dTotalWIP As Double = 0, dBordirAmt As Double = 0, dTotalBordirIDR As Double = 0
            Dim cRate As New ClassRate
            Dim cRateOHD As New ClassRate
            Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
            Dim iGroupOid As Integer = 0
            Dim varCost As VarCostValue
            If prodresmststatus.Text = "Post" Then
                For C1 As Integer = 0 To objTable.Rows.Count - 1
                    DLAMT = ToDouble(objTable.Rows(C1).Item("prodresdlcamt").ToString)
                    OHDAMT = ToDouble(objTable.Rows(C1).Item("prodresohdamt").ToString)
                    dKancingAmt = ToDouble(objTable.Rows(C1).Item("kancingamt").ToString)
                    dKainAmt = ToDouble(objTable.Rows(C1).Item("kainamt").ToString)
                    dLabelAmt = ToDouble(objTable.Rows(C1).Item("labelamt").ToString)
                    dOtherAmt = ToDouble(objTable.Rows(C1).Item("otheramt").ToString)
                    dBordirAmt = ToDouble(objTable.Rows(C1).Item("bordiramt").ToString)
                    GetTotalCost(objTable.Rows(C1)("womstoid").ToString, varCost, objTable.Rows(C1)("lastconfirmdate").ToString)
                    dTotalCost = varCost.ValCost_IDR + ToDouble(objTable.Rows(C1)("balanceamt_cogm").ToString) + DLAMT + OHDAMT + dKancingAmt + dKainAmt + dLabelAmt + dOtherAmt + dBordirAmt
                    objTable.Rows(C1)("confirmvalueidr") = dTotalCost / (ToDouble(objTable.Rows(C1)("prodresqty").ToString))
                    objTable.Rows(C1)("confirmdmidr") = varCost.ValCost_IDR
                    objTable.Rows(C1)("confirmdlidr") = DLAMT
                    objTable.Rows(C1)("confirmohdidr") = OHDAMT
                    objTable.Rows(C1)("confirmkancingidr") = dKancingAmt
                    objTable.Rows(C1)("confirmkainidr") = dKainAmt
                    objTable.Rows(C1)("confirmlabelidr") = dLabelAmt
                    objTable.Rows(C1)("confirmotheridr") = dOtherAmt
                    objTable.Rows(C1)("confirmbordiridr") = dBordirAmt
                Next
            End If
            If prodresmststatus.Text = "Post" Then
                GenerateResultNo()
                GenerateAPProdNo()
            End If
            If prodresmststatus.Text = "Post" Then 'disabled posting
                cRate.SetRateValue(1, sDate)
                If cRate.GetRateDailyLastError <> "" Then
                    showMessage(cRate.GetRateDailyLastError, 2) : prodresmststatus.Text = "In Process" : Exit Sub
                End If
                If cRate.GetRateMonthlyLastError <> "" Then
                    showMessage(cRate.GetRateMonthlyLastError, 2) : prodresmststatus.Text = "In Process" : Exit Sub
                End If
                Dim dtTmp As DataTable = Session("TblDtl")
                Dim sCurrOid_OHD As String = ""
                For C1 As Integer = 0 To dtTmp.Rows.Count - 1
                    If Not sCurrOid_OHD.Contains("[" & dtTmp.Rows(C1)("curroid_ohd").ToString & "]") Then
                        sCurrOid_OHD &= "[" & dtTmp.Rows(C1)("curroid_ohd").ToString & "],"
                    End If
                Next

                Dim sVarErr As String = ""
                If Not IsInterfaceExists("VAR_DL_ASSIGNMENT", CompnyCode) Then
                    sVarErr &= IIf(sVarErr = "", "", ", ") & "VAR_DL_ASSIGNMENT"
                End If
                If Not IsInterfaceExists("VAR_BOP_BEBAN", CompnyCode) Then
                    sVarErr &= IIf(sVarErr = "", "", ", ") & "VAR_BOP_BEBAN"
                End If
                If Not IsInterfaceExists("VAR_BOP_REAL", CompnyCode) Then
                    sVarErr &= IIf(sVarErr = "", "", ", ") & "VAR_BOP_REAL"
                End If
                If sVarErr <> "" Then
                    showMessage(GetInterfaceWarning(sVarErr, "posting"), 2)
                    Exit Sub
                End If
                iSelisihAcctgOid = GetAcctgOID(GetVarInterface("VAR_SELISIH_EFISIENSI", CompnyCode), CompnyCode)
                iAPAcctgOid = GetAcctgOID(GetVarInterface("VAR_AP_DIRECT_LABOR", CompnyCode), CompnyCode)
                iPPNAcctgOid = GetAcctgOID(GetVarInterface("VAR_PPN_IN", CompnyCode), CompnyCode)
                iStockWIPAcctgOid = GetAcctgOID(GetVarInterface("VAR_STOCK_WIP", CompnyCode), CompnyCode)
                iDLAssignAcctgOid = GetAcctgOID(GetVarInterface("VAR_DL_ASSIGNMENT", CompnyCode), CompnyCode)
                iBOPBebanAcctgOid = GetAcctgOID(GetVarInterface("VAR_BOP_BEBAN", CompnyCode), CompnyCode)
                iBOPRealAcctgOid = GetAcctgOID(GetVarInterface("VAR_BOP_REAL", CompnyCode), CompnyCode)
                iGroupOid = ToInteger(GetStrData("SELECT TOP 1 groupoid FROM QL_mstdeptgroupdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND deptoid=" & IIf(deptreplaceoid.SelectedValue = "0", deptoid.SelectedValue, deptreplaceoid.SelectedValue) & " ORDER BY updtime DESC"))
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trnprodresmst (cmpcode, prodresmstoid, periodacctg, prodresdate, prodresno, deptoid, suppoid, prodresmstnote, prodresmststatus, createuser, createtime, upduser, updtime, deptreplaceoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & prodresmstoid.Text & ", '" & periodacctg.Text & "', '" & prodresdate.Text & "', '" & prodresno.Text & "', " & deptoid.SelectedValue & ", " & suppoid.SelectedValue & ", '" & Tchar(prodresmstnote.Text) & "', '" & prodresmststatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & deptreplaceoid.SelectedValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & prodresmstoid.Text & " WHERE tablename='QL_TRNPRODRESMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trnprodresmst SET periodacctg='" & periodacctg.Text & "', prodresdate='" & prodresdate.Text & "', prodresno='" & prodresno.Text & "', deptoid=" & deptoid.SelectedValue & ", suppoid=" & suppoid.SelectedValue & ", prodresmstnote='" & Tchar(prodresmstnote.Text) & "', prodresmststatus='" & prodresmststatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, deptreplaceoid=" & deptreplaceoid.SelectedValue & " WHERE prodresmstoid=" & prodresmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnwodtl2 SET wodtl2resflag='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl2oid IN (SELECT wodtl2oid FROM QL_trnprodresdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND prodresmstoid=" & prodresmstoid.Text & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trnprodresdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND prodresmstoid=" & prodresmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trnprodresdtl (cmpcode, prodresdtloid, prodresmstoid, prodresdtlseq, womstoid, wodtl2oid, prodresprocseq, todeptoid, prodrestype, prodresqty, prodresunitoid, prodresdtlstatus, prodresdtlnote, upduser, updtime, prodresdlcvalue, prodresdlcamt, prodresohdvalue, prodresohdamt, prodresdlcvalueusd, prodresdlcamtusd, prodresohdvalueusd, prodresohdamtusd, prodresqty_sisa, prodresqty_unitkecil, prodresqty_unitbesar, sisaqty_unitkecil, sisaqty_unitbesar, sisaunitoid, whoid, itemoid, prodreskancingvalue, prodreskancingamt, prodreskainvalue, prodreskainamt, prodreslabelvalue, prodreslabelamt, prodresothervalue, prodresotheramt, bordirvalue, bordiramt) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C1 + CInt(prodresdtloid.Text)) & ", " & prodresmstoid.Text & ", " & C1 + 1 & ", " & objTable.Rows(C1).Item("womstoid") & ", " & objTable.Rows(C1).Item("wodtl2oid") & ", " & objTable.Rows(C1).Item("prodresprocseq") & ", " & objTable.Rows(C1).Item("todeptoid") & ", '" & objTable.Rows(C1).Item("prodrestype").ToString & "', " & ToDouble(objTable.Rows(C1).Item("prodresqty").ToString) & ", " & objTable.Rows(C1).Item("prodresunitoid") & ", '', '" & Tchar(objTable.Rows(C1).Item("prodresdtlnote").ToString) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(C1).Item("prodresdlcvalue").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("prodresdlcamt").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("prodresohdvalue").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("prodresohdamt").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("prodresdlcvalue").ToString) * cRate.GetRateMonthlyUSDValue & ", " & ToDouble(objTable.Rows(C1).Item("prodresdlcamt").ToString) * cRate.GetRateMonthlyUSDValue & ", " & ToDouble(objTable.Rows(C1).Item("prodresohdvalue").ToString) * cRate.GetRateMonthlyUSDValue & ", " & ToDouble(objTable.Rows(C1).Item("prodresohdamt").ToString) * cRate.GetRateMonthlyUSDValue & ", " & ToDouble(objTable.Rows(C1).Item("prodresqty_sisa").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("prodresqty_unitkecil").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("prodresqty_unitbesar").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("sisaqty_unitkecil").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("sisaqty_unitbesar").ToString) & ", " & CInt(objTable.Rows(C1).Item("sisaunitoid").ToString) & ", " & objTable.Rows(C1).Item("whoid") & ", " & objTable.Rows(C1).Item("itemoid") & ", " & ToDouble(objTable.Rows(C1).Item("kancingvalue").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("kancingamt").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("kainvalue").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("kainamt").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("labelvalue").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("labelamt").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("othervalue").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("otheramt").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("bordirvalue").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("bordiramt").ToString) & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        If ToDouble(objTable.Rows(C1).Item("prodresqty").ToString) >= ToDouble(objTable.Rows(C1).Item("wodtl2qty").ToString) Then
                            sSql = "UPDATE QL_trnwodtl2 SET wodtl2resflag='Complete' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl2oid=" & objTable.Rows(C1)("wodtl2oid")
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(prodresdtloid.Text)) & " WHERE tablename='QL_TRNPRODRESDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If

                'INSERT INTO PRODRESCONFIRM
                Dim dTotalWIPIDR As Double = 0
                If prodresmststatus.Text = "Post" Then
                    If Not Session("TblDtl") Is Nothing Then
                        For C1 As Int16 = 0 To objTable.Rows.Count - 1
                            dTotalAmtIDR = (ToDouble(objTable.Rows(C1)("prodresqty").ToString)) * ToDouble(objTable.Rows(C1)("confirmvalueidr").ToString)
                            dTotalWIPIDR = (objTable.Rows(C1)("confirmdlidr").ToString) + ToDouble(objTable.Rows(C1)("confirmohdidr").ToString) + ToDouble(objTable.Rows(C1)("confirmkancingidr").ToString) + ToDouble(objTable.Rows(C1)("confirmkainidr").ToString) + ToDouble(objTable.Rows(C1)("confirmlabelidr").ToString) + ToDouble(objTable.Rows(C1)("confirmotheridr").ToString) + ToDouble(objTable.Rows(C1)("confirmbordiridr").ToString)
                            dTotalWIP += Math.Round(dTotalWIPIDR, 2, MidpointRounding.AwayFromZero)
                            dTotalBordirIDR += ToDouble(objTable.Rows(C1)("confirmbordiridr").ToString)

                            sSql = "INSERT INTO QL_trnprodresconfirm (cmpcode, confirmoid, prodresdtloid, deptoid, confirmdate, confirmqty, confirmunitoid, confirmwhoid, confirmstatus, confirmnote, upduser, updtime, confirmvalue, confirmvalueidr, confirmvalueusd, prodresmstoid, confirmdm, confirmdmidr, confirmdmusd, confirmdl, confirmdlidr, confirmdlusd, confirmohd, confirmohdidr, confirmohdusd, womstoid, saldoawal, saldoawalidr, saldoawalusd, saldoakhir, saldoakhiridr, saldoakhirusd, confirmqty_unitkecil, confirmqty_unitbesar, confirmvalueidr_unitkecil, confirmvalueusd_unitkecil, confirmqty_sisa, confirmkancingidr, confirmkainidr, confirmlabelidr, confirmotheridr, confirmbordiridr) VALUES ('" & CompnyCode & "', " & iConfirmOid & ", " & (C1 + CInt(prodresdtloid.Text)) & ", " & deptoid.SelectedValue & ", '" & dToday & "', " & ToDouble(objTable.Rows(C1)("prodresqty").ToString) & ", " & objTable.Rows(C1)("prodresunitoid") & ", " & objTable.Rows(C1)("whoid") & ", '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(C1)("confirmvalueidr").ToString) & ", " & ToDouble(objTable.Rows(C1)("confirmvalueidr").ToString) & ", 0, " & prodresmstoid.Text & ", " & ToDouble(objTable.Rows(C1)("confirmdmidr").ToString) & ", " & ToDouble(objTable.Rows(C1)("confirmdmidr").ToString) & ", 0, " & ToDouble(objTable.Rows(C1)("confirmdlidr").ToString) & ", " & ToDouble(objTable.Rows(C1)("confirmdlidr").ToString) & ", 0, " & ToDouble(objTable.Rows(C1)("confirmohdidr").ToString) & ", " & ToDouble(objTable.Rows(C1)("confirmohdidr").ToString) & ", 0, " & objTable.Rows(C1)("womstoid") & ", " & ToDouble(objTable.Rows(C1)("balanceamt_cogm").ToString) & ", " & ToDouble(objTable.Rows(C1)("balanceamt_cogm").ToString) & ", 0, " & ToDouble(objTable.Rows(C1)("balanceamt_cogm").ToString) + ToDouble(objTable.Rows(C1)("confirmdmidr").ToString) + ToDouble(objTable.Rows(C1)("confirmdlidr").ToString) + ToDouble(objTable.Rows(C1)("confirmohdidr").ToString) + ToDouble(objTable.Rows(C1)("confirmkancingidr").ToString) + ToDouble(objTable.Rows(C1)("confirmkainidr").ToString) + ToDouble(objTable.Rows(C1)("confirmlabelidr").ToString) + ToDouble(objTable.Rows(C1)("confirmotheridr").ToString) + ToDouble(objTable.Rows(C1)("confirmbordiridr").ToString) - dTotalAmtIDR & ", " & ToDouble(objTable.Rows(C1)("balanceamt_cogm").ToString) + ToDouble(objTable.Rows(C1)("confirmdmidr").ToString) + ToDouble(objTable.Rows(C1)("confirmdlidr").ToString) + ToDouble(objTable.Rows(C1)("confirmohdidr").ToString) + ToDouble(objTable.Rows(C1)("confirmkancingidr").ToString) + ToDouble(objTable.Rows(C1)("confirmkainidr").ToString) + ToDouble(objTable.Rows(C1)("confirmlabelidr").ToString) + ToDouble(objTable.Rows(C1)("confirmotheridr").ToString) + ToDouble(objTable.Rows(C1)("confirmbordiridr").ToString) - dTotalAmtIDR & ", 0, " & ToDouble(objTable.Rows(C1)("prodresqty").ToString) & ", 0, " & ToDouble(objTable.Rows(C1)("confirmvalueidr").ToString) & ",0, 0, " & ToDouble(objTable.Rows(C1)("confirmkancingidr").ToString) & ", " & ToDouble(objTable.Rows(C1)("confirmkainidr").ToString) & ", " & ToDouble(objTable.Rows(C1)("confirmlabelidr").ToString) & ", " & ToDouble(objTable.Rows(C1)("confirmotheridr").ToString) & ", " & ToDouble(objTable.Rows(C1)("confirmbordiridr").ToString) & ")"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            If ToDouble(objTable.Rows(C1)("prodresqty").ToString) >= ToDouble(objTable.Rows(C1)("prodresqty").ToString) Then
                                sSql = "UPDATE QL_trnprodresdtl SET prodresdtlres1='Complete' WHERE cmpcode='" & CompnyCode & "' AND prodresdtloid=" & (C1 + CInt(prodresdtloid.Text)) & " AND prodresmstoid=" & prodresmstoid.Text
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                            End If
                            sSql = "UPDATE QL_trnwomst SET balanceamt_cogm=" & ToDouble(objTable.Rows(C1)("balanceamt_cogm").ToString) + ToDouble(objTable.Rows(C1)("confirmdmidr").ToString) + ToDouble(objTable.Rows(C1)("confirmdlidr").ToString) + ToDouble(objTable.Rows(C1)("confirmohdidr").ToString) + ToDouble(objTable.Rows(C1)("confirmkancingidr").ToString) + ToDouble(objTable.Rows(C1)("confirmkainidr").ToString) + ToDouble(objTable.Rows(C1)("confirmlabelidr").ToString) + ToDouble(objTable.Rows(C1)("confirmotheridr").ToString) + ToDouble(objTable.Rows(C1)("confirmbordiridr").ToString) - dTotalAmtIDR & ", balanceamtusd_cogm=0 , balanceqty_cogm=" & ToDouble(objTable.Rows(C1)("balanceqty_cogm").ToString) - (ToDouble(objTable.Rows(C1)("prodresqty").ToString)) & ", lastconfirmdate=CURRENT_TIMESTAMP"
                            If ToDouble(objTable.Rows(C1)("balanceqty_cogm").ToString) - ToDouble(objTable.Rows(C1)("prodresqty").ToString) <= 0 Then
                                sSql &= ", womststatus='Closed', woclosinguser='" & Session("UserID") & "', woclosingdate=CURRENT_TIMESTAMP"
                            End If
                            sSql &= " WHERE cmpcode='" & CompnyCode & "' AND womstoid=" & objTable.Rows(C1)("womstoid")
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()

                            ' Insert Into QL_conmat
                            sSql = "INSERT INTO QL_constock (cmpcode, constockoid, contype, typemin, trndate, periodacctg, formaction, formoid, refoid, refname, mtrlocoid, qtyin, qtyout, reason, note, upduser, updtime, createuser, createtime, valueidr, valueusd) VALUES ('" & CompnyCode & "', " & iConMatOid & ", 'PRCONF', 1, '" & dToday & "', '" & sPeriod & "', 'QL_trnprodresconfirm', " & iConfirmOid & ", " & objTable.Rows(C1)("itemoid") & ", '" & objTable.Rows(C1)("refname").ToString & "', " & objTable.Rows(C1)("whoid") & ", " & ToDouble(objTable.Rows(C1)("prodresqty").ToString) & ", 0, 'Production Result Confirmation', 'Confirm : " & prodresno.Text & " # " & objTable.Rows(C1)("wono").ToString & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(C1)("confirmvalueidr").ToString) & ", 0)"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iConMatOid += 1

                            If dTotalAmtIDR > 0 Or dTotalAmtUSD > 0 Then
                                ' Insert QL_trnglmst
                                sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGLMstOid & ", '" & sDate & "', '" & sPeriod & "', 'Confirm ProdRes|No. " & prodresno.Text & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 0, 0, 1, 1, 0, 0)"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                ' Insert QL_trngldtl
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGLDtlOid & ", 1, " & iGLMstOid & ", " & iStockFGAcctgOid & ", 'D', " & dTotalAmtIDR & ", '" & prodresno.Text & "', 'Confirm ProdRes|No. " & prodresno.Text & " (SPK No. " & objTable.Rows(C1)("wono").ToString & ")', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalAmtIDR & ", " & dTotalAmtUSD & ", 'QL_trnprodresconfirm " & iConfirmOid & "')"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iGLDtlOid += 1
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGLDtlOid & ", 2, " & iGLMstOid & ", " & iStockWIPAcctgOid & ", 'C', " & dTotalAmtIDR & ", '" & prodresno.Text & "', 'Confirm ProdRes|No. " & prodresno.Text & " (SPK No. " & objTable.Rows(C1)("wono").ToString & ")', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalAmtIDR & ", " & dTotalAmtUSD & ", 'QL_trnprodresconfirm " & iConfirmOid & "')"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iGLDtlOid += 1 : iGLMstOid += 1
                            End If
                            iConfirmOid += 1

                            'UPDATE lastprice
                            Dim price As Double = ToDouble(objTable.Rows(C1)("confirmvalueidr").ToString)
                            Dim dPercentage As Double = GetHppPercentage(objTable.Rows(C1)("itemcat2").ToString)
                            Dim hpp As Double = ToMaskEdit(price * (dPercentage / 100), 2)
                            'update minStock di tabel mstitem
                            sSql = "UPDATE QL_mstitem SET itemMinPrice = " & price + hpp & ", itemMinPrice1 = " & price + hpp & ", itemMinPrice2 = " & price + hpp & ", itemMinPrice3 = " & price + hpp & " WHERE itemoid = '" & objTable.Rows(C1).Item("itemoid") & "' and cmpcode = '" & CompnyCode & "' "
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        Next
                    End If
                End If

                'INSERT INTO A/P PRODUCTION RESULT
                If prodresmststatus.Text = "Post" Then
                    Dim sPostSuppOid() As String = Nothing
                    If Not Session("TblDtl") Is Nothing Then
                        Dim dv As DataView = objTable.DefaultView
                        Dim sSuppOid As String = ""
                        For C1 As Integer = 0 To objTable.Rows.Count - 1
                            If Not sSuppOid.Contains("[" & objTable.Rows(C1)("suppoid_bordir").ToString & "]") Then
                                sSuppOid &= "[" & objTable.Rows(C1)("suppoid_bordir").ToString & "],"
                            End If
                        Next
                        If sSuppOid <> "" Then
                            sSuppOid = Left(sSuppOid, sSuppOid.Length - 1).Replace("[", "").Replace("]", "")
                            sPostSuppOid = sSuppOid.Split(",")
                            If sPostSuppOid.Length > 0 Then
                                For C2 As Integer = 0 To sPostSuppOid.Length - 1
                                    sSql = "SELECT gendesc FROM QL_mstgen g INNER JOIN QL_mstsupp s ON s.supppaymentoid=g.genoid WHERE suppoid=" & sPostSuppOid(C2)
                                    Dim dDueDate2 As Date = DateAdd(DateInterval.Day, ToDouble(GetNumericValues(ToInteger(cKon.ambilscalar(sSql)))), CDate(prodresdate.Text))
                                    Dim dAmt As Double = ToDouble(objTable.Compute("SUM(bordiramt)", "suppoid_bordir=" & sPostSuppOid(C2)).ToString)

                                    sSql = "INSERT INTO QL_trnapproductmst (cmpcode, approductmstoid, periodacctg, approductdate, approductno, suppoid, approductpaytypeoid, curroid, rateoid, approductratetoidr, approductratetousd, rate2oid, approductrate2toidr, approductrate2tousd, approductgrandtotal, approductsupptotal, approductdatetakegiro, approductmstnote, approductmststatus, createuser, createtime, upduser, updtime, approvalcode, isSE, approducttotal, approducttaxtype, approducttaxvalue, approducttaxamt, prodresmstoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iAPProductOid & ", '" & periodacctg.Text & "', '" & prodresdate.Text & "', '', " & sPostSuppOid(C2) & ", " & approductpaytypeoid.Text & ", 1, 0, '1', '1', 0, '1', '1', " & dAmt & ", " & dAmt & ", '1/1/1900', '" & Tchar(prodresmstnote.Text) & "', '" & prodresmststatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 'False', " & dAmt & ", '', 0, 0, " & prodresmstoid.Text & ")"
                                    xCmd.CommandText = sSql
                                    xCmd.ExecuteNonQuery()

                                    sSql = "INSERT INTO QL_conap (cmpcode, conapoid, reftype, refoid, payrefoid, suppoid, acctgoid, trnapstatus, trnaptype, trnapdate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans, amtbayar, trnapnote, trnapres1, trnapres2, trnapres3, createuser, createtime, upduser, updtime, amttransidr, amtbayaridr, amttransusd, amtbayarusd) VALUES ('" & CompnyCode & "', " & iConAPOid & ", 'QL_trnapproductmst', " & iAPProductOid & ", 0, " & sPostSuppOid(C2) & ", " & iAPAcctgOid & ", 'Post', 'APPRES', '" & sDate & "', '" & sPeriod & "', 0, '1/1/1900', '', 0, '" & dDueDate2 & "', " & dAmt & ", 0, '', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dAmt & ", 0, " & dAmt & ", 0)"
                                    xCmd.CommandText = sSql
                                    xCmd.ExecuteNonQuery()
                                    iConAPOid += 1
                                    dv.RowFilter = "suppoid_bordir=" & sPostSuppOid(C2)
                                    If dv.Count > 0 Then
                                        For C1 As Integer = 0 To dv.Count - 1
                                            sSql = "INSERT INTO QL_trnapproductdtl (cmpcode, approductdtloid, approductmstoid, approductdtlseq, approducttype, prodresmstoid, prodresdtloid, costacctgoid, approductqty, approductunitoid, approductprice, approductdtlamt, approductdtlnote, approductdtlstatus, upduser, updtime, approductfoh, approductfohamt, womstoid, approductdlcvalue) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iAPProductDtlOid & ", " & iAPProductOid & ", " & C1 + 1 & ", 'PR', " & prodresmstoid.Text & ", 0, 0, " & ToDouble(dv(C1)("prodresqty").ToString) & ", " & dv(C1)("prodresunitoid") & ", " & ToDouble(dv(C1)("bordirvalue").ToString) & ", " & ToDouble(dv(C1)("bordiramt").ToString) & ", '" & Tchar(dv(C1)("prodresdtlnote")) & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, " & dv(C1)("womstoid") & ", 0)"
                                            xCmd.CommandText = sSql
                                            xCmd.ExecuteNonQuery()
                                            iAPProductDtlOid += 1
                                        Next
                                    End If
                                    iAPProductOid += 1
                                    dv.RowFilter = ""
                                Next
                            End If
                        End If
                    End If

                    sSql = "INSERT INTO QL_trnapproductmst (cmpcode, approductmstoid, periodacctg, approductdate, approductno, suppoid, approductpaytypeoid, curroid, rateoid, approductratetoidr, approductratetousd, rate2oid, approductrate2toidr, approductrate2tousd, approductgrandtotal, approductsupptotal, approductdatetakegiro, approductmstnote, approductmststatus, createuser, createtime, upduser, updtime, approvalcode, isSE, approducttotal, approducttaxtype, approducttaxvalue, approducttaxamt, prodresmstoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iAPProductOid & ", '" & periodacctg.Text & "', '" & prodresdate.Text & "', '', " & suppoid.SelectedValue & ", " & approductpaytypeoid.Text & ", 1, 0, '1', '1', 0, '1', '1', " & ToDouble(approductgrandtotal.Text) & ", " & ToDouble(approductgrandtotal.Text) & ", '1/1/1900', '" & Tchar(prodresmstnote.Text) & "', '" & prodresmststatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 'False', " & ToDouble(approductgrandtotal.Text) & ", '', 0, 0, " & prodresmstoid.Text & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    If Not Session("TblDtl") Is Nothing Then
                        For C1 As Int16 = 0 To objTable.Rows.Count - 1
                            sSql = "INSERT INTO QL_trnapproductdtl (cmpcode, approductdtloid, approductmstoid, approductdtlseq, approducttype, prodresmstoid, prodresdtloid, costacctgoid, approductqty, approductunitoid, approductprice, approductdtlamt, approductdtlnote, approductdtlstatus, upduser, updtime, approductfoh, approductfohamt, womstoid, approductdlcvalue) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C1 + CInt(iAPProductDtlOid)) & ", " & iAPProductOid & ", " & C1 + 1 & ", 'PR', " & prodresmstoid.Text & ", " & (C1 + CInt(prodresdtloid.Text)) & ", 0, " & ToDouble(objTable.Rows(C1)("prodresqty").ToString) & ", " & objTable.Rows(C1)("prodresunitoid") & ", " & ToDouble(objTable.Rows(C1)("prodresdlcvalue").ToString) + IIf(objTable.Rows(C1)("bordir_use_ap").ToString = "Y", ToDouble(objTable.Rows(C1)("prodresohdvalue").ToString), 0) + IIf(objTable.Rows(C1)("kancing_use_ap").ToString = "Y", ToDouble(objTable.Rows(C1)("kancingvalue").ToString), 0) + IIf(objTable.Rows(C1)("kain_use_ap").ToString = "Y", ToDouble(objTable.Rows(C1)("kainvalue").ToString), 0) + IIf(objTable.Rows(C1)("label_use_ap").ToString = "Y", ToDouble(objTable.Rows(C1)("labelvalue").ToString), 0) + IIf(objTable.Rows(C1)("other_use_ap").ToString = "Y", ToDouble(objTable.Rows(C1)("othervalue").ToString), 0) & ", " & ToDouble(objTable.Rows(C1)("prodresdlcamt").ToString) + IIf(objTable.Rows(C1)("bordir_use_ap").ToString = "Y", ToDouble(objTable.Rows(C1)("prodresohdamt").ToString), 0) + IIf(objTable.Rows(C1)("kancing_use_ap").ToString = "Y", ToDouble(objTable.Rows(C1)("kancingamt").ToString), 0) + IIf(objTable.Rows(C1)("kain_use_ap").ToString = "Y", ToDouble(objTable.Rows(C1)("kainamt").ToString), 0) + IIf(objTable.Rows(C1)("label_use_ap").ToString = "Y", ToDouble(objTable.Rows(C1)("labelamt").ToString), 0) + IIf(objTable.Rows(C1)("other_use_ap").ToString = "Y", ToDouble(objTable.Rows(C1)("otheramt").ToString), 0) & ", '" & Tchar(objTable.Rows(C1)("prodresdtlnote")) & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, " & objTable.Rows(C1)("womstoid") & ", 0)"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iAPProductDtlOid += 1
                        Next
                    End If

                    'INSERT INTO QL_conap
                    ' Insert CONAP
                    Dim dValueIDR As Double = 0, dValueUSD As Double = 0
                    dValueIDR = ToDouble(approductgrandtotal.Text) + dTotalBordirIDR

                    sSql = "INSERT INTO QL_conap (cmpcode, conapoid, reftype, refoid, payrefoid, suppoid, acctgoid, trnapstatus, trnaptype, trnapdate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans, amtbayar, trnapnote, trnapres1, trnapres2, trnapres3, createuser, createtime, upduser, updtime, amttransidr, amtbayaridr, amttransusd, amtbayarusd) VALUES ('" & CompnyCode & "', " & iConAPOid & ", 'QL_trnapproductmst', " & iAPProductOid & ", 0, " & suppoid.SelectedValue & ", " & iAPAcctgOid & ", 'Post', 'APPRES', '" & sDate & "', '" & sPeriod & "', 0, '1/1/1900', '', 0, '" & dDueDate & "', " & ToDouble(approductgrandtotal.Text) & ", 0, '', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(approductgrandtotal.Text) & ", 0, " & ToDouble(approductgrandtotal.Text) & ", 0)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iConAPOid += 1

                    Dim dSelisihIDR As Double = dValueIDR - dTotalWIP
                    Dim sDbCRSelisihIDR As String = "D"
                    If dSelisihIDR < 0 Then
                        sDbCRSelisihIDR = "C" : dSelisihIDR *= -1
                    End If
                    ' Auto Jurnal Accounting
                    ' Insert GL MST
                    Dim iSeq As Integer = 0
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & CompnyCode & "', " & iGLMstOid & ", '" & sDate & "', '" & sPeriod & "', 'A/P PRes |No. " & prodresno.Text & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iSeq = 1

                    If dTotalWIP > 0 And dValueIDR > 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & CompnyCode & "', " & iGLDtlOid & ", " & iSeq & ", " & iGLMstOid & ", " & iStockWIPAcctgOid & ", 'D', " & dTotalWIP & ", '" & prodresno.Text & "', 'A/P PRes |No. " & prodresno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalWIP & ", " & dTotalWIP & ", 'QL_trnapproductmst " & iAPProductOid & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iGLDtlOid += 1 : iSeq += 1
                        If dSelisihIDR > 0 Then
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & CompnyCode & "', " & iGLDtlOid & ", " & iSeq & ", " & iGLMstOid & ", " & iSelisihAcctgOid & ", '" & sDbCRSelisihIDR & "', " & dSelisihIDR & ", '" & prodresno.Text & "', 'Selisih Efisiensi Produksi A/P PRes |No. " & prodresno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dSelisihIDR & ", " & dSelisihIDR & ", 'QL_trnapproductmst " & iAPProductOid & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iGLDtlOid += 1 : iSeq += 1
                        End If
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & CompnyCode & "', " & iGLDtlOid & ", " & iSeq & ", " & iGLMstOid & ", " & iAPAcctgOid & ", 'C', " & dValueIDR & ", '" & prodresno.Text & "', 'A/P PRes |No. " & prodresno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dValueIDR & ", " & dValueIDR & ", 'QL_trnapproductmst " & iAPProductOid & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iGLDtlOid += 1 : iGLMstOid += 1
                    End If
                    iAPProductOid += 1
                End If
                sSql = "UPDATE QL_mstoid SET lastoid=" & iConAPOid - 1 & " WHERE tablename='QL_CONAP' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & iAPProductDtlOid - 1 & " WHERE tablename='QL_TRNAPPRODUCTDTL' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & iAPProductOid - 1 & " WHERE tablename='QL_TRNAPPRODUCTMST' and cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & iGLMstOid - 1 & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & iGLDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & iConfirmOid - 1 & " WHERE tablename='QL_TRNPRODRESCONFIRM' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & iConMatOid - 1 & " WHERE tablename='QL_CONSTOCK' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & iCrdMtrOid - 1 & " WHERE tablename='QL_CRDSTOCK' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & iStockValOid - 1 & " WHERE tablename='QL_STOCKVALUE' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.ToString, 1)
                        prodresmststatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.ToString, 1)
                    prodresmststatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.ToString & sSql, 1)
                conn.Close()
                prodresmststatus.Text = "In Process"
                Exit Sub
            Finally
                ReGenerateAPResNo(prodresmstoid.Text)
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & prodresmstoid.Text & ".<BR>"
            End If
            If prodresmststatus.Text = "Post" Then
                Session("SavedInfo") &= "Data have been posted with Result No. = " & prodresno.Text & "."
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trnProdRes.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnProdRes.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If prodresmstoid.Text.Trim = "" Then
            showMessage("Please select Production Result data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_trnprodresmst", "prodresmstoid", prodresmstoid.Text, "prodresmststatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                prodresmststatus.Text = "In Process"
                Exit Sub
            End If
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trnwodtl2 SET wodtl2resflag='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl2oid IN (SELECT wodtl2oid FROM QL_trnprodresdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND prodresmstoid=" & prodresmstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnprodresdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND prodresmstoid=" & prodresmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnprodresmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND prodresmstoid=" & prodresmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnProdRes.aspx?awal=true")
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPost.Click
        prodresmststatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub suppoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        approductpaytypeoid.Text = 0
        If suppoid.Items.Count > 0 Then
            Try
                sSql = "SELECT supppaymentoid FROM QL_mstsupp WHERE suppoid=" & suppoid.SelectedValue & ""
            Catch ex As Exception
                approductpaytypeoid.Text = 0
            End Try
            approductpaytypeoid.Text = CInt(GetStrData(sSql))
        End If
    End Sub
#End Region

End Class