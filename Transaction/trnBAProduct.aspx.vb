Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_BeritaAcaraProduct
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetTextValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function GetDDLValue(ByVal cc As System.Web.UI.ControlCollection, ByVal sType As String) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                If sType = "Text" Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedItem.Text
                Else
                    sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
                End If
            End If
        Next
        Return sReturn
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If womstoid.Text = "" Then
            sError &= "- Please select KIK NO. field!<BR>"
        End If
        If deptoid.SelectedValue = "" Then
            sError &= "- Please select DEPARTMENT field!<BR>"
        End If
        If acaraprodqty.Text = "" Then
            sError &= "- Please fill BA QTY field!<BR>"
        Else
            If ToDouble(acaraprodqty.Text) <= 0 Then
                sError &= "- BA QTY field must be more than 0!<BR>"
            Else
                Dim sErrReply As String = ""
                If Not isLengthAccepted("acaraprodqty", "QL_trnbrtacaraproddtl", ToDouble(acaraprodqty.Text), sErrReply) Then
                    sError &= "- BA QTY field must be less than MAX BA QTY (" & sErrReply & ") allowed stored in database!<BR>"
                Else
                    If ToDouble(acaraprodqty.Text) > ToDouble(wodtl1qty.Text) Then
                        sError &= "- BA QTY field must be less than KIK QTY!<BR>"
                    End If
                End If
            End If
        End If
        If acaraprodunitoid.SelectedValue = "" Then
            sError &= "- Please select UNIT field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If groupoid.SelectedValue = "" Then
            sError &= "- Please select DIVISION field!<BR>"
        End If
        If acaraproddate.Text = "" Then
            sError &= "- Please fill BA DATE field!<BR>"
            sErr = "None"
        Else
            If Not IsValidDate(acaraproddate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- BA DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If suppoid.SelectedValue = "" Then
            sError &= "- Please select GROUP NAME field!<BR>"
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill DETAIL DATA!<BR>"
        Else
            Dim objTbl As DataTable = Session("TblDtl")
            If objTbl.Rows.Count <= 0 Then
                sError &= "- Please fill DETAIL DATA!<BR>"
            Else
                If sError = "" Then
                    For C1 As Integer = 0 To objTbl.Rows.Count - 1
                        If CDate(objTbl.Rows(C1)("wodate").ToString) > CDate(acaraproddate.Text) Then
                            sError &= "- Every KIK DATE must be less than BA DATE!<BR>"
                            Exit For
                        End If
                        Dim sAdd As String = ""
                        If Session("oid") IsNot Nothing And Session("oid") <> "" Then
                            sAdd = " AND bad.acaraprodmstoid<>" & Session("oid")
                        End If
                        sSql = "SELECT (wodtl1qty - ISNULL((SELECT SUM(bad.acaraprodqty) FROM QL_trnbrtacaraproddtl bad WHERE bad.cmpcode=wod1.cmpcode AND bad.womstoid=wod1.womstoid AND bad.deptoid=" & objTbl.Rows(C1)("deptoid") & sAdd & "), 0.0)) AS wodtl1qty FROM QL_trnwodtl1 wod1 WHERE wod1.womstoid=" & objTbl.Rows(C1)("womstoid")
                        Dim dMax01 As Double = ToDouble(GetStrData(sSql))
                        If ToDouble(objTbl.Rows(C1)("wodtl1qty").ToString) <> dMax01 Then
                            objTbl.Rows(C1)("wodtl1qty") = dMax01
                        End If
                    Next
                    For C1 As Integer = 0 To objTbl.Rows.Count - 1
                        If ToDouble(objTbl.Rows(C1)("acaraprodqty").ToString) > ToDouble(objTbl.Rows(C1)("wodtl1qty").ToString) Then
                            sError &= "- BA QTY for detail No. " & ToDouble(objTbl.Rows(C1)("acaraproddtlseq").ToString) & " must be less than KIK QTY (updated)!<BR>"
                        End If
                    Next
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            acaraprodmststatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function GetDLValue(ByVal sCode As String, ByVal sDeptOid As String, Optional ByVal sType As String = "dtl") As Double
        GetDLValue = 0
        Try
            GetDLValue = ToDouble(GetStrData("SELECT dlc" & sType & "amount FROM QL_mstbom bom INNER JOIN QL_mstdlc dlcm ON dlcm.cmpcode=bom.cmpcode AND dlcm.bomoid=bom.bomoid INNER JOIN QL_mstdlcdtl dlcd ON dlcd.cmpcode=dlcm.cmpcode AND dlcd.dlcoid=dlcm.dlcoid INNER JOIN QL_mstitem i ON i.itemoid=bom.itemoid WHERE bom.cmpcode='" & DDLBusUnit.SelectedValue & "' AND itemcode='" & sCode & "' AND dlcd.deptoid=" & sDeptOid & ""))
        Catch ex As Exception
            GetDLValue = 0
        End Try
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trnbrtacaraprodmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND acaraprodmststatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnBAProduct.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lkbResultInProcess.Visible = True
            lkbResultInProcess.Text = "You have " & GetStrData(sSql) & " In Process Berita Acara Product data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
        sSql = "SELECT COUNT(*) FROM QL_trnbrtacaraprodmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND acaraprodmststatus='In Approval'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnBAProduct.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lkbResultInProcess.Visible = True
            lkbResultInProcess.Text = "You have " & GetStrData(sSql) & " In Approval Berita Acara Product data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitAllDDL()
        'Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If FillDDL(DDLBusUnit, sSql) Then
            InitDDLDivision()
        End If
        ' Fill DDL Group Name
        sSql = "SELECT suppoid, suppname FROM QL_mstsupp WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND suppres1='Pemborong'"
        FillDDLWithAdditionalText(suppoid, sSql, "None", "0")
        FillDDLWithAdditionalText(FilterDDLGroupName, sSql, "None", "0")
        ' Fill DDL Unit
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND (gengroup='MATERIAL UNIT' OR gengroup='ITEM UNIT') AND activeflag='ACTIVE'"
        FillDDL(acaraprodunitoid, sSql)
    End Sub

    Private Sub InitDDLDivision()
        ' Init DDL Division
        sSql = "SELECT groupoid, groupcode + ' - ' + groupdesc FROM QL_mstdeptgroup WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND activeflag='ACTIVE'"
        If FillDDL(groupoid, sSql) Then
            InitDDLDept()
        End If
    End Sub

    Private Sub InitDDLDept()
        ' Init DDL Department
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND activeflag='ACTIVE' AND deptoid IN (SELECT deptoid FROM QL_mstdeptgroupdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND groupoid=" & groupoid.SelectedValue & ")"
        FillDDL(deptoid, sSql)
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT bam.acaraprodmstoid, bam.acaraprodno, CONVERT(VARCHAR(10), bam.acaraproddate, 101) acaraproddate, ISNULL(suppname, 'None') AS suppname, bam.acaraprodmststatus, bam.acaraprodmstnote, div.divname, 'False' AS checkvalue FROM QL_trnbrtacaraprodmst bam LEFT JOIN QL_mstsupp s ON s.suppoid=bam.suppoid INNER JOIN QL_mstdivision div ON div.cmpcode=bam.cmpcode WHERE"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " bam.cmpcode='" & Session("CompnyCode") & "'"
        Else
            sSql &= " bam.cmpcode LIKE '%'"
        End If
        sSql &= sSqlPlus & " ORDER BY CONVERT(DATETIME, bam.acaraproddate) DESC, bam.acaraprodmstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trnbrtacaraprodmst")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "acaraprodmstoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub PrintReport()
        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("acaraprodmstoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            report.Load(Server.MapPath(folderReport & "rptBAProduct.rpt"))
            Dim sWhere As String = " WHERE"
            If Session("CompnyCode") <> CompnyCode Then
                sWhere &= " bam.cmpcode='" & Session("CompnyCode") & "'"
            Else
                sWhere &= " bam.cmpcode LIKE '%'"
            End If
            If sOid = "" Then
                sWhere = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND bam.acaraproddate>='" & FilterPeriod1.Text & " 00:00:00' AND bam.acaraproddate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbGroupName.Checked Then
                    sWhere &= " AND bam.suppoid=" & FilterDDLGroupName.SelectedValue
                End If
                If cbStatus.Checked Then
                    sWhere &= " AND bam.acaraprodmststatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Transaction\trnBAProduct.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND bam.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sWhere &= " AND bam.acaraprodmstoid IN (" & sOid & ")"
            End If
            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "BeritaAcaraProductPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
        Response.Redirect("~\Transaction\trnBAProduct.aspx?awal=true")
    End Sub

    Private Sub BindListKIK()
        sSql = "SELECT 'False' AS checkvalue, wom.womstoid, wono, CONVERT(VARCHAR(10), wodate, 101) AS wodate, '' AS deptname, 0 AS deptoid, wodtl1unitoid, g.gendesc AS wodtl1unit, wodtl1qty, 0.0 AS acaraprodqty, itemcode, itemlongdesc, '' AS acaraproddtlnote, 0.0 AS dlcdtlamount, 0.0 AS dlcohdamount FROM QL_trnwomst wom INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=wom.cmpcode AND wod1.womstoid=wom.womstoid INNER JOIN QL_trnsoitemmst som ON som.cmpcode=wod1.cmpcode AND som.soitemmstoid=wod1.soitemmstoid INNER JOIN QL_mstitem i ON i.itemoid=wod1.itemoid INNER JOIN QL_mstgen g ON g.genoid=wodtl1unitoid WHERE wom.cmpcode='" & DDLBusUnit.SelectedValue & "' AND womststatus='Post' AND som.groupoid=" & groupoid.SelectedValue & " ORDER BY wom.wono"
        Dim dtKIK As DataTable = cKon.ambiltabel(sSql, "QL_trnwodtl2")
        dtKIK.DefaultView.RowFilter = ""
        If dtKIK.DefaultView.Count > 0 Then
            Session("TblListKIK") = dtKIK.DefaultView.ToTable
            Session("TblListKIKView") = dtKIK.DefaultView.ToTable
            gvListKIK.DataSource = Session("TblListKIKView")
            gvListKIK.DataBind()
            cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, True)
        Else
            showMessage("There is no posting KIK data available for this time!", 2)
        End If
    End Sub

    Private Sub UpdateCheckedListKIK()
        If Session("TblListKIK") IsNot Nothing Then
            For C1 As Integer = 0 To gvListKIK.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListKIK.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                            Session("TblListKIK").DefaultView.RowFilter = "womstoid=" & sOid
                            If cbcheck = True Then
                                If Session("TblListKIK").DefaultView.Count > 0 Then
                                    Session("TblListKIK").DefaultView(0)("checkvalue") = "True"
                                    Dim cc2 As System.Web.UI.ControlCollection = row.Cells(2).Controls
                                    Session("TblListKIK").DefaultView(0)("deptoid") = ToInteger(GetDDLValue(cc2, ""))
                                    Session("TblListKIK").DefaultView(0)("deptname") = GetDDLValue(cc2, "Text")
                                    cc2 = row.Cells(3).Controls
                                    Session("TblListKIK").DefaultView(0)("acaraprodqty") = ToDouble(GetTextValue(cc2))
                                    cc2 = row.Cells(5).Controls
                                    Session("TblListKIK").DefaultView(0)("acaraproddtlnote") = GetTextValue(cc2)
                                End If
                            Else
                                If Session("TblListKIK").DefaultView.Count > 0 Then
                                    Session("TblListKIK").DefaultView(0)("checkvalue") = "False"
                                End If
                            End If
                            Session("TblListKIK").DefaultView.RowFilter = ""
                        End If
                    Next
                End If
            Next
        End If
        If Session("TblListKIKView") IsNot Nothing Then
            For C1 As Integer = 0 To gvListKIK.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListKIK.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                            Session("TblListKIKView").DefaultView.RowFilter = "womstoid=" & sOid
                            If cbcheck = True Then
                                If Session("TblListKIKView").DefaultView.Count > 0 Then
                                    Session("TblListKIKView").DefaultView(0)("checkvalue") = "True"
                                    Dim cc2 As System.Web.UI.ControlCollection = row.Cells(2).Controls
                                    Session("TblListKIKView").DefaultView(0)("deptoid") = ToInteger(GetDDLValue(cc2, ""))
                                    Session("TblListKIKView").DefaultView(0)("deptname") = GetDDLValue(cc2, "Text")
                                    cc2 = row.Cells(3).Controls
                                    Session("TblListKIKView").DefaultView(0)("acaraprodqty") = ToDouble(GetTextValue(cc2))
                                    cc2 = row.Cells(5).Controls
                                    Session("TblListKIKView").DefaultView(0)("acaraproddtlnote") = GetTextValue(cc2)
                                End If
                            Else
                                If Session("TblListKIKView").DefaultView.Count > 0 Then
                                    Session("TblListKIKView").DefaultView(0)("checkvalue") = "False"
                                End If
                            End If
                            Session("TblListKIKView").DefaultView.RowFilter = ""
                        End If
                    Next
                End If
            Next
        End If
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("QL_trnbrtacaraproddtl")
        dtlTable.Columns.Add("acaraproddtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("womstoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wono", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodate", Type.GetType("System.String"))
        dtlTable.Columns.Add("acaraprodtype", Type.GetType("System.String"))
        dtlTable.Columns.Add("deptoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("deptname", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl1qty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("acaraprodqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("acaraprodunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("acaraprodunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("acaraproddtlnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemlongdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("acaraproddlcvalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("acaraproddlcamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("acaraprodohdvalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("acaraprodohdamt", Type.GetType("System.Double"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub ClearDetail()
        i_u2.Text = "New Detail"
        acaraproddtlseq.Text = "1"
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            If dt.Rows.Count > 0 Then
                EnableHeader(False)
                acaraproddtlseq.Text = dt.Rows.Count + 1
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        womstoid.Text = ""
        acaraprodtype.Text = ""
        wono.Text = ""
        wodate.Text = ""
        deptoid.SelectedIndex = -1
        acaraprodqty.Text = ""
        acaraprodunitoid.SelectedIndex = -1
        wodtl1qty.Text = ""
        acaraproddtlnote.Text = ""
        acaraproddlcvalue.Text = ""
        acaraprodohdvalue.Text = ""
        gvDtl.SelectedIndex = -1
        btnSearchKIK.Visible = True
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal : DDLBusUnit.CssClass = sCss
        groupoid.Enabled = bVal : groupoid.CssClass = sCss
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT cmpcode, acaraprodmstoid, periodacctg, acaraproddate, acaraprodno, suppoid, acaraprodmstnote, acaraprodmststatus, createuser, createtime, upduser, updtime, groupoid FROM QL_trnbrtacaraprodmst WHERE acaraprodmstoid=" & sOid
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = Trim(xreader("cmpcode").ToString)
                DDLBusUnit_SelectedIndexChanged(Nothing, Nothing)
                acaraprodmstoid.Text = Trim(xreader("acaraprodmstoid").ToString)
                periodacctg.Text = Trim(xreader("periodacctg").ToString)
                acaraproddate.Text = Format(xreader("acaraproddate"), "MM/dd/yyyy")
                acaraprodno.Text = Trim(xreader("acaraprodno").ToString)
                suppoid.SelectedValue = Trim(xreader("suppoid").ToString)
                acaraprodmstnote.Text = Trim(xreader("acaraprodmstnote").ToString)
                acaraprodmststatus.Text = Trim(xreader("acaraprodmststatus").ToString)
                createuser.Text = Trim(xreader("createuser").ToString)
                createtime.Text = Trim(xreader("createtime").ToString)
                upduser.Text = Trim(xreader("upduser").ToString)
                updtime.Text = Trim(xreader("updtime").ToString)
                groupoid.SelectedValue = Trim(xreader("groupoid").ToString)
                groupoid_SelectedIndexChanged(Nothing, Nothing)
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False : btnSendApproval.Visible = False
            Exit Sub
        End Try
        If acaraprodmststatus.Text <> "In Process" And acaraprodmststatus.Text <> "Revised" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnSendApproval.Visible = False
            btnAddToList.Visible = False
            gvDtl.Columns(0).Visible = False
            gvDtl.Columns(gvDtl.Columns.Count - 1).Visible = False
            If acaraprodmststatus.Text = "Approved" Or acaraprodmststatus.Text = "Closed" Then
                lblTrnNo.Text = "BA No."
                acaraprodmstoid.Visible = False
                acaraprodno.Visible = True
            End If
        End If
        sSql = "SELECT acaraproddtlseq, bad.womstoid, wono, CONVERT(VARCHAR(10), wodate, 101) AS wodate, acaraprodtype, bad.deptoid, deptname, wodtl1qty, acaraprodqty, acaraprodunitoid, gendesc AS acaraprodunit, acaraproddtlnote, itemcode, itemlongdesc, ISNULL((SELECT dlcdtlamount FROM QL_mstbom bom INNER JOIN QL_mstdlc dlcm ON dlcm.cmpcode=bom.cmpcode AND dlcm.bomoid=bom.bomoid INNER JOIN QL_mstdlcdtl dlcd ON dlcd.cmpcode=dlcm.cmpcode AND dlcd.dlcoid=dlcm.dlcoid AND dlcd.deptoid=bad.deptoid WHERE bom.cmpcode=bad.cmpcode AND bom.itemoid=wod1.itemoid), 0.0) AS acaraproddlcvalue, acaraproddlcamt, ISNULL((SELECT dlcohdamount FROM QL_mstbom bom INNER JOIN QL_mstdlc dlcm ON dlcm.cmpcode=bom.cmpcode AND dlcm.bomoid=bom.bomoid INNER JOIN QL_mstdlcdtl dlcd ON dlcd.cmpcode=dlcm.cmpcode AND dlcd.dlcoid=dlcm.dlcoid AND dlcd.deptoid=bad.deptoid WHERE bom.cmpcode=bad.cmpcode AND bom.itemoid=wod1.itemoid), 0.0) AS acaraprodohdvalue, acaraprodohdamt FROM QL_trnbrtacaraproddtl bad INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=bad.cmpcode AND wod1.womstoid=bad.womstoid INNER JOIN QL_trnwomst wom ON wom.cmpcode=bad.cmpcode AND wom.womstoid=bad.womstoid INNER JOIN QL_mstdept de ON de.cmpcode=bad.cmpcode AND de.deptoid=bad.deptoid INNER JOIN QL_mstgen g ON g.genoid=bad.acaraprodunitoid INNER JOIN QL_mstitem i ON i.itemoid=wod1.itemoid WHERE acaraprodmstoid=" & sOid & " ORDER BY acaraproddtlseq"
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnbrtacaraproddtl")
        For C1 As Integer = 0 To dtTbl.Rows.Count - 1
            dtTbl.Rows(C1)("acaraproddlcamt") = ToDouble(dtTbl.Rows(C1)("acaraproddlcvalue").ToString) * ToDouble(dtTbl.Rows(C1)("acaraprodqty").ToString)
            dtTbl.Rows(C1)("acaraprodohdamt") = ToDouble(dtTbl.Rows(C1)("acaraprodohdvalue").ToString) * ToDouble(dtTbl.Rows(C1)("acaraprodqty").ToString)
        Next
        dtTbl.AcceptChanges()
        Session("TblDtl") = dtTbl
        gvDtl.DataSource = dtTbl
        gvDtl.DataBind()
        ClearDetail()
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnBAProduct.aspx")
        End If
        If checkPagePermission("~\Transaction\trnBAProduct.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Berita Acara Product"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnSendApproval.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to send this data for approval?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            CheckStatus()
            InitAllDDL()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                acaraprodmstoid.Text = GenerateID("QL_TRNBRTACARAPRODMST", CompnyCode)
                acaraprodmststatus.Text = "In Process"
                acaraproddate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime)
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvDtl.DataSource = dt
            gvDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListKIK") Is Nothing And Session("WarningListKIK") <> "" Then
            Session("WarningListKIK") = Nothing
            cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, True)
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnBAProduct.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lkbResultInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbResultInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = 0
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, bam.updtime, GETDATE()) > " & nDays & " AND bam.acaraprodmststatus='In Process'"
        If checkPagePermission("~\Transaction\trnBAProduct.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND bam.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub lkbResultInApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbResultInApproval.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = 0
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 1
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, bam.updtime, GETDATE()) > " & nDays & " AND bam.acaraprodmststatus='In Approval'"
        If checkPagePermission("~\Transaction\trnBAProduct.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND bam.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND bam.acaraproddate>='" & FilterPeriod1.Text & " 00:00:00' AND bam.acaraproddate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbGroupName.Checked Then
            sSqlPlus &= " AND bam.suppoid=" & FilterDDLGroupName.SelectedValue
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND bam.acaraprodmststatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Transaction\trnBAProduct.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND bam.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = ""
        FilterDDL.SelectedIndex = 0
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbGroupName.Checked = False
        FilterDDLGroupName.SelectedIndex = -1
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Transaction\trnBAProduct.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND bam.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        UpdateCheckedValue()
        PrintReport()
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLDivision()
    End Sub

    Protected Sub groupoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles groupoid.SelectedIndexChanged
        InitDDLDept()
    End Sub

    Protected Sub btnSearchKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchKIK.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        If groupoid.SelectedValue = "" Then
            showMessage("Please select Division first!", 2)
            Exit Sub
        End If
        FilterDDLListKIK.SelectedIndex = -1 : FilterTextListKIK.Text = "" : gvListKIK.DataSource = Nothing : gvListKIK.DataBind() : Session("TblListKIK") = Nothing : Session("TblListKIKView") = Nothing
        BindListKIK()
    End Sub

    Protected Sub btnFindListKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListKIK.Click
        UpdateCheckedListKIK()
        If Session("TblListKIK") IsNot Nothing Then
            Session("TblListKIK").DefaultView.RowFilter = FilterDDLListKIK.SelectedValue & " LIKE '%" & Tchar(FilterTextListKIK.Text) & "%'"
            If Session("TblListKIK").DefaultView.Count > 0 Then
                Session("TblListKIKView") = Session("TblListKIK").DefaultView.ToTable
                gvListKIK.DataSource = Session("TblListKIKView")
                gvListKIK.DataBind()
                mpeListKIK.Show()
                Session("TblListKIK").DefaultView.RowFilter = ""
            Else
                Session("TblListKIK").DefaultView.RowFilter = ""
                Session("WarningListKIK") = "KIK data can't be found!"
                showMessage(Session("WarningListKIK"), 2)
            End If
        Else
            Session("WarningListKIK") = "There is no posting KIK data available for this time!"
            showMessage(Session("WarningListKIK"), 2)
        End If
    End Sub

    Protected Sub btnAllListKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListKIK.Click
        FilterDDLListKIK.SelectedIndex = -1 : FilterTextListKIK.Text = "" : gvListKIK.SelectedIndex = -1
        UpdateCheckedListKIK()
        If Session("TblListKIK") IsNot Nothing Then
            Session("TblListKIKView") = Session("TblListKIK")
            gvListKIK.DataSource = Session("TblListKIKView")
            gvListKIK.DataBind()
            mpeListKIK.Show()
        Else
            Session("WarningListKIK") = "There is no posting KIK data available for this time!"
            showMessage(Session("WarningListKIK"), 2)
        End If
    End Sub

    Protected Sub cbHdrListKIK_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim bVal As Boolean = sender.Checked
        For C1 As Integer = 0 To gvListKIK.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListKIK.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = bVal
                    End If
                Next
            End If
        Next
        mpeListKIK.Show()
    End Sub

    Protected Sub gvListKIK_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListKIK.PageIndexChanging
        UpdateCheckedListKIK()
        gvListKIK.PageIndex = e.NewPageIndex
        gvListKIK.DataSource = Session("TblListKIKView")
        gvListKIK.DataBind()
        mpeListKIK.Show()
    End Sub

    Protected Sub gvListKIK_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListKIK.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim sOid As String = ""
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(0).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                End If
            Next
            cc = e.Row.Cells(2).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                    sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND activeflag='ACTIVE' AND deptoid NOT IN (SELECT deptoid FROM QL_trnwodtl2 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid=" & sOid & ") AND deptoid IN (SELECT deptoid FROM QL_mstdeptgroupdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND groupoid=" & groupoid.SelectedValue & ") ORDER BY deptname"
                    FillDDL(CType(myControl, System.Web.UI.WebControls.DropDownList), sSql)
                    If CType(myControl, System.Web.UI.WebControls.DropDownList).ToolTip <> "0" Then
                        CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue = CType(myControl, System.Web.UI.WebControls.DropDownList).ToolTip
                    End If
                End If
            Next
            cc = e.Row.Cells(3).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 4)
                    End If
                End If
            Next
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
        End If
    End Sub

    Protected Sub lkbAddToListListKIK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListKIK.Click
        UpdateCheckedListKIK()
        If Session("TblListKIK") IsNot Nothing Then
            Dim dv As DataView = Session("TblListKIK").DefaultView
            dv.RowFilter = "checkvalue='True'"
            Dim iCount As Integer = dv.Count
            If dv.Count > 0 Then
                dv.RowFilter = "checkvalue='True' AND acaraprodqty > 0"
                If dv.Count <> iCount Then
                    Session("WarningListKIK") = "Result Qty for every selected KIK data must be more than 0!"
                    showMessage(Session("WarningListKIK"), 2)
                    dv.RowFilter = "" : Exit Sub
                End If
                dv.RowFilter = "checkvalue='True' AND acaraprodqty <= wodtl1qty"
                If dv.Count <> iCount Then
                    Session("WarningListKIK") = "Result Qty for every selected KIK data must be less than KIK Qty!"
                    showMessage(Session("WarningListKIK"), 2)
                    dv.RowFilter = "" : Exit Sub
                End If
                dv.RowFilter = "checkvalue='True'"
                If Session("TblDtl") Is Nothing Then
                    CreateTblDetail()
                End If
                Dim objTable As DataTable = Session("TblDtl")
                Dim objView As DataView = objTable.DefaultView
                Dim iSeq As Integer = objTable.Rows.Count + 1
                For C1 As Integer = 0 To dv.Count - 1
                    objView.RowFilter = "womstoid=" & dv(C1)("womstoid")
                    Dim dDLVal As Double = GetDLValue(dv(C1)("itemcode").ToString, dv(C1)("deptoid").ToString)
                    Dim dOHDVal As Double = GetDLValue(dv(C1)("itemcode").ToString, dv(C1)("deptoid").ToString, "ohd")
                    If objView.Count > 0 Then
                        objView(0)("acaraprodqty") = ToDouble(dv(C1)("acaraprodqty").ToString)
                        objView(0)("acaraproddlcvalue") = dDLVal
                        objView(0)("acaraproddlcamt") = ToDouble(dv(C1)("acaraprodqty").ToString) * dDLVal
                        objView(0)("acaraprodohdvalue") = dOHDVal
                        objView(0)("acaraprodohdamt") = ToDouble(dv(C1)("acaraprodqty").ToString) * dOHDVal
                    Else
                        Dim rv As DataRowView = objView.AddNew()
                        rv.BeginEdit()
                        rv("acaraproddtlseq") = iSeq
                        rv("womstoid") = dv(C1)("womstoid")
                        rv("wono") = dv(C1)("wono").ToString
                        rv("wodate") = dv(C1)("wodate").ToString
                        rv("acaraprodtype") = "WIP"
                        rv("deptoid") = dv(C1)("deptoid")
                        rv("deptname") = dv(C1)("deptname").ToString
                        rv("wodtl1qty") = ToDouble(dv(C1)("wodtl1qty").ToString)
                        rv("acaraprodqty") = ToDouble(dv(C1)("acaraprodqty").ToString)
                        rv("acaraprodunitoid") = dv(C1)("wodtl1unitoid")
                        rv("acaraprodunit") = dv(C1)("wodtl1unit").ToString
                        rv("acaraproddtlnote") = ""
                        rv("itemcode") = dv(C1)("itemcode").ToString
                        rv("itemlongdesc") = dv(C1)("itemlongdesc").ToString
                        rv("acaraproddlcvalue") = dDLVal
                        rv("acaraproddlcamt") = ToDouble(dv(C1)("acaraprodqty").ToString) * dDLVal
                        rv("acaraprodohdvalue") = dOHDVal
                        rv("acaraprodohdamt") = ToDouble(dv(C1)("acaraprodqty").ToString) * dOHDVal
                        rv.EndEdit()
                        iSeq += 1
                    End If
                    objView.RowFilter = ""
                Next
                dv.RowFilter = ""
                Session("TblDtl") = objTable
                gvDtl.DataSource = Session("TblDtl")
                gvDtl.DataBind()
                ClearDetail()
                cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, False)
            Else
                Session("WarningListKIK") = "Please select KIK data first!"
                showMessage(Session("WarningListKIK"), 2)
            End If
        Else
            Session("WarningListKIK") = "There is no posting KIK data available for this time!"
            showMessage(Session("WarningListKIK"), 2)
        End If
    End Sub

    Protected Sub lkbCloseListKIK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListKIK.Click
        cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, False)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            If Session("TblDtl") IsNot Nothing Then
                Dim objTable As DataTable = Session("TblDtl")
                Dim objRow As DataRow
                objRow = objTable.Rows(acaraproddtlseq.Text - 1)
                objRow.BeginEdit()
                objRow("deptoid") = deptoid.SelectedValue
                objRow("deptname") = deptoid.SelectedItem.Text
                objRow("acaraprodqty") = ToDouble(acaraprodqty.Text)
                objRow("acaraproddtlnote") = acaraproddtlnote.Text
                objRow("acaraproddlcamt") = ToDouble(acaraprodqty.Text) * ToDouble(acaraproddlcvalue.Text)
                objRow("acaraprodohdamt") = ToDouble(acaraprodqty.Text) * ToDouble(acaraprodohdvalue.Text)
                objRow.EndEdit()
                Session("TblDtl") = objTable
                gvDtl.DataSource = Session("TblDtl")
                gvDtl.DataBind()
                ClearDetail()
            End If
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 4)
        End If
    End Sub

    Protected Sub gvDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("acaraproddtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvDtl.DataSource = objTable
        gvDtl.DataBind()
        ClearDetail()
    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDtl.SelectedIndexChanged
        Try
            acaraproddtlseq.Text = gvDtl.SelectedDataKey.Item("acaraproddtlseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "acaraproddtlseq=" & acaraproddtlseq.Text
                womstoid.Text = dv.Item(0).Item("womstoid").ToString
                wono.Text = dv.Item(0).Item("wono").ToString
                wodate.Text = dv.Item(0).Item("wodate").ToString
                sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND activeflag='ACTIVE' AND deptoid NOT IN (SELECT deptoid FROM QL_trnwodtl2 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid=" & dv.Item(0).Item("womstoid").ToString & ") ORDER BY deptname"
                FillDDL(deptoid, sSql)
                deptoid.SelectedValue = dv.Item(0).Item("deptoid").ToString
                acaraprodtype.Text = dv.Item(0).Item("acaraprodtype").ToString
                acaraprodqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("acaraprodqty").ToString), 4)
                wodtl1qty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("wodtl1qty").ToString), 4)
                acaraprodunitoid.SelectedValue = dv.Item(0).Item("acaraprodunitoid").ToString
                acaraproddtlnote.Text = dv.Item(0).Item("acaraproddtlnote").ToString
                acaraproddlcvalue.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("acaraproddlcvalue").ToString), 4)
                acaraprodohdvalue.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("acaraprodohdvalue").ToString), 4)
                dv.RowFilter = ""
                btnSearchKIK.Visible = False
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                If CheckDataExists("SELECT COUNT(*) FROM QL_trnbrtacaraprodmst WHERE acaraprodmstoid=" & acaraprodmstoid.Text) Then
                    acaraprodmstoid.Text = GenerateID("QL_TRNBRTACARAPRODMST", CompnyCode)
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_trnbrtacaraprodmst", "acaraprodmstoid", acaraprodmstoid.Text, "acaraprodmststatus", updtime.Text, "Approved")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    acaraprodmststatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            periodacctg.Text = GetDateToPeriodAcctg(acaraproddate.Text)
            acaraproddtloid.Text = GenerateID("QL_TRNBRTACARAPRODDTL", CompnyCode)
            Dim iAppOid As Integer = GenerateID("QL_APPROVAL", CompnyCode)
            If acaraprodmststatus.Text = "Revised" Then
                acaraprodmststatus.Text = "In Process"
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trnbrtacaraprodmst (cmpcode, acaraprodmstoid, periodacctg, acaraproddate, acaraprodno, suppoid, acaraprodmstnote, acaraprodmststatus, createuser, createtime, upduser, updtime, groupoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & acaraprodmstoid.Text & ", '" & periodacctg.Text & "', '" & acaraproddate.Text & "', '" & acaraprodno.Text & "', " & suppoid.SelectedValue & ", '" & Tchar(acaraprodmstnote.Text) & "', '" & acaraprodmststatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & groupoid.SelectedValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & acaraprodmstoid.Text & " WHERE tablename='QL_TRNBRTACARAPRODMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trnbrtacaraprodmst SET periodacctg='" & periodacctg.Text & "', acaraproddate='" & acaraproddate.Text & "', acaraprodno='" & acaraprodno.Text & "', suppoid=" & suppoid.SelectedValue & ", acaraprodmstnote='" & Tchar(acaraprodmstnote.Text) & "', acaraprodmststatus='" & acaraprodmststatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, groupoid=" & groupoid.SelectedValue & " WHERE acaraprodmstoid=" & acaraprodmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trnbrtacaraproddtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND acaraprodmstoid=" & acaraprodmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trnbrtacaraproddtl (cmpcode, acaraproddtloid, acaraprodmstoid, acaraproddtlseq, womstoid, deptoid, acaraprodtype, acaraprodqty, acaraprodunitoid, acaraproddtlstatus, acaraproddtlnote, upduser, updtime, acaraproddlcvalue, acaraproddlcamt, acaraprodohdvalue, acaraprodohdamt) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C1 + CInt(acaraproddtloid.Text)) & ", " & acaraprodmstoid.Text & ", " & C1 + 1 & ", " & objTable.Rows(C1).Item("womstoid") & ", " & objTable.Rows(C1).Item("deptoid") & ", '" & objTable.Rows(C1).Item("acaraprodtype").ToString & "', " & ToDouble(objTable.Rows(C1).Item("acaraprodqty").ToString) & ", " & objTable.Rows(C1).Item("acaraprodunitoid") & ", '', '" & Tchar(objTable.Rows(C1).Item("acaraproddtlnote").ToString) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(C1).Item("acaraproddlcvalue").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("acaraproddlcamt").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("acaraprodohdvalue").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("acaraprodohdamt").ToString) & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(acaraproddtloid.Text)) & " WHERE tablename='QL_TRNBRTACARAPRODDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If acaraprodmststatus.Text = "In Approval" Then
                    Dim dt As DataTable = Session("AppPerson")
                    For C1 As Integer = 0 To dt.Rows.Count - 1
                        sSql = "INSERT INTO QL_approval (cmpcode, approvaloid, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvalnote) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iAppOid + C1 & ", 'BAP" & acaraprodmstoid.Text & "_" & (iAppOid + C1) & "', '" & Session("UserID") & "', '" & acaraproddate.Text & "', 'New', 'QL_trnbrtacaraprodmst', " & acaraprodmstoid.Text & ", 'In Approval', '0', '" & dt.Rows(C1)("approvaluser").ToString & "', '1/1/1900', '')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                    sSql = "UPDATE QL_mstOid SET lastoid=" & iAppOid + dt.Rows.Count - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_APPROVAL'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        acaraprodmststatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    acaraprodmststatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.ToString, 1)
                conn.Close()
                acaraprodmststatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & acaraprodmstoid.Text & ".<BR>"
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trnBAProduct.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnBAProduct.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If acaraprodmstoid.Text.Trim = "" Then
            showMessage("Please select Berita Acara Product data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_trnbrtacaraprodmst", "acaraprodmstoid", acaraprodmstoid.Text, "acaraprodmststatus", updtime.Text, "Approved")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                acaraprodmststatus.Text = "In Process"
                Exit Sub
            End If
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE FROM QL_trnbrtacaraproddtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND acaraprodmstoid=" & acaraprodmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnbrtacaraprodmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND acaraprodmstoid=" & acaraprodmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnBAProduct.aspx?awal=true")
    End Sub

    Protected Sub btnSendApproval_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSendApproval.Click
        sSql = "SELECT approvaluser, apppersontype FROM QL_approvalperson WHERE tablename='QL_trnbrtacaraprodmst' AND activeflag='ACTIVE' AND cmpcode IN ('" & CompnyCode & "', '" & DDLBusUnit.SelectedValue & "')"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_approvalperson")
        If dt.Rows.Count = 0 Then
            showMessage("There is no user can't approve this transaction. Please define approval user in <STRONG>APPROVAL USER</STRONG> form.", 2)
        Else
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "apppersontype='Final'"
            If dv.Count = 0 Then
                showMessage("Final approval user is not defined for this transaction. Please define final approval user in <STRONG>APPROVAL USER</STRONG> form.", 2)
                dv.RowFilter = ""
            Else
                dv.RowFilter = ""
                Session("AppPerson") = dt
                acaraprodmststatus.Text = "In Approval"
                btnSave_Click(Nothing, Nothing)
            End If
        End If
    End Sub
#End Region

End Class