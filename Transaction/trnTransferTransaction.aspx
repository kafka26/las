<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnTransferTransaction.aspx.vb" Inherits="Transaction_GiroCancel" title="" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Transfer Transaction" CssClass="Title" ForeColor="SaddleBrown"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Transfer Transaction :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                <asp:UpdatePanel id="upPOClosing" runat="server">
                    <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3></TD></TR><TR><TD style="WIDTH: 10%" class="Label" align=left></TD><TD style="WIDTH: 2%" class="Label" align=center></TD><TD id="TD1" class="Label" align=left runat="server" visible="false"><asp:TextBox id="FilterPeriode1" runat="server" CssClass="inpText" Width="60px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="btnCalendar1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> To <asp:TextBox id="FilterPeriode2" runat="server" CssClass="inpText" Width="60px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="btnCalendar2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label46" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR><TR><TD style="WIDTH: 10%" class="Label" align=left><asp:Label id="Label3" runat="server" Text="Periode"></asp:Label></TD><TD style="WIDTH: 2%" class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLPeriod" runat="server" CssClass="inpText" Width="150px">
            </asp:DropDownList></TD></TR><TR><TD style="WIDTH: 10%" class="Label" align=left><asp:Label id="Label9" runat="server" Text="Type"></asp:Label></TD><TD style="WIDTH: 2%" class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="FilterDDLType" runat="server" CssClass="inpText" Width="232px" AutoPostBack="True" OnSelectedIndexChanged="FilterDDLGiroType_SelectedIndexChanged"><asp:ListItem>SALES INVOICE</asp:ListItem>
<asp:ListItem Value="PURCHASE INVOICE">PURCHASE INVOICE</asp:ListItem>
<asp:ListItem Enabled="False">PURCHASE INVOICE GENERAL</asp:ListItem>
<asp:ListItem Enabled="False">PURCHASE INVOICE SPARE PART</asp:ListItem>
<asp:ListItem Enabled="False">PROD. SORTIR</asp:ListItem>
<asp:ListItem Enabled="False">PROD. WASHING</asp:ListItem>
<asp:ListItem Enabled="False">PROD. PELLET</asp:ListItem>
<asp:ListItem Enabled="False">PROD. FINISH GOOD</asp:ListItem>
<asp:ListItem Enabled="False">BANK</asp:ListItem>
<asp:ListItem Enabled="False">CASH</asp:ListItem>
</asp:DropDownList> <asp:CheckBox id="cbNonTax" runat="server" Text="Show COA Non Tax" Visible="False"></asp:CheckBox></TD></TR><TR><TD style="WIDTH: 10%" class="Label" align=left><asp:Label id="Label2" runat="server" Text="Transaction No."></asp:Label></TD><TD style="WIDTH: 2%" class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="224px"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 10%" class="Label" align=left></TD><TD style="WIDTH: 2%" class="Label" align=center></TD><TD class="Label" align=left><ajaxToolkit:CalendarExtender id="Ce1" runat="server" TargetControlID="FilterPeriode1" PopupButtonID="btnCalendar1" Format="MM/dd/yyyy">
            </ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="Ce2" runat="server" TargetControlID="FilterPeriode2" PopupButtonID="btnCalendar2" Format="MM/dd/yyyy">
            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MeeDate1" runat="server" TargetControlID="FilterPeriode1" MaskType="Date" Mask="99/99/9999">
            </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MeeDate2" runat="server" TargetControlID="FilterPeriode2" MaskType="Date" Mask="99/99/9999">
            </ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD style="WIDTH: 10%" class="Label" align=left></TD><TD style="WIDTH: 2%" class="Label" align=center></TD><TD class="Label" align=left><asp:ImageButton id="btnShowData" onclick="btnShowData_Click" runat="server" ImageUrl="~/Images/showdata.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="nomor" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Label id="Label8" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Transaction Detail Data :" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:GridView id="gvPODtl" runat="server" ForeColor="#333333" Width="100%" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle Wrap="True" BackColor="#EFF3FB"></RowStyle>
<Columns>
<asp:TemplateField><HeaderTemplate>
<asp:CheckBox id="cbHdr" runat="server" AutoPostBack="True" OnCheckedChanged="cbHdr_CheckedChanged" __designer:wfdid="w2"></asp:CheckBox> 
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="cbTrans" runat="server" ToolTip='<%# eval("trnjualmstoid") %>' Checked='<%# eval("CheckValue") %>' __designer:wfdid="w1"></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="trndtlseq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualno" HeaderText="Invoice No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualdate" HeaderText="Faktur Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dono" HeaderText="DO No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sono" HeaderText="SO No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="personname" HeaderText="Faktur No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualamtnetto" HeaderText="Total Netto">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle Wrap="True" BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=3><asp:GridView id="gvPODtl2" runat="server" ForeColor="#333333" Width="100%" CellPadding="4" AutoGenerateColumns="False" GridLines="None" OnRowDataBound="gvPODtl2_RowDataBound">
<RowStyle BackColor="#EFF3FB"></RowStyle>
<Columns>
<asp:TemplateField><HeaderTemplate>
                    <asp:CheckBox ID="cbHdr2" runat="server" AutoPostBack="True" OnCheckedChanged="cbHdr2_CheckedChanged" />
                
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="cbTrans" runat="server" ToolTip='<%# eval("nomor") %>' Checked='<%# eval("CheckValue") %>' __designer:wfdid="w1"></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="trndtlseq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnno" HeaderText="Invoice No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trndate" HeaderText="Faktur Date">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelimstres3" HeaderText="Faktur No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsuppcustname" HeaderText="Supplier">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgcode" HeaderText="Account No" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Account Desc" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnamount" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnnote" HeaderText="Note" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Faktur No." Visible="False"><ItemTemplate>
                    <asp:TextBox ID="fakturno" runat="server" CssClass="inpText" MaxLength="100" Text='<%# eval("fakturno") %>'
                        Width="120px"></asp:TextBox>
                    <ajaxToolkit:MaskedEditExtender ID="MeeFakturNo" runat="server" Mask="999.999-99.99999999"
                        TargetControlID="fakturno" ClearMaskOnLostFocus="False">
                    </ajaxToolkit:MaskedEditExtender>
                
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Faktur Date" Visible="False"><ItemTemplate>
<asp:TextBox id="fakturdate" runat="server" CssClass="inpText" Text='<%# eval("fakturdate") %>' Width="64px" __designer:wfdid="w2"></asp:TextBox> <asp:ImageButton id="btnDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w3"></asp:ImageButton> <ajaxToolkit:CalendarExtender id="CeDate" runat="server" Format="MM/dd/yyyy" PopupButtonID="btnDate" TargetControlID="fakturdate" __designer:wfdid="w4"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MeeDate" runat="server" TargetControlID="fakturdate" Mask="99/99/9999" MaskType="Date" __designer:wfdid="w5">
                    </ajaxToolkit:MaskedEditExtender> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left colSpan=3>Processed By <asp:Label id="upduser" runat="server"></asp:Label>&nbsp;On <asp:Label id="updtime" runat="server"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:ImageButton id="imbClose" runat="server" ImageUrl="~/Images/generate.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upPOClosing">
                                <ProgressTemplate>
                                    <span></span><span style="font-weight: bold; font-size: 10pt; color: purple">
                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />
                                        Please Wait .....</span><br />
                                </ProgressTemplate>
                            </asp:UpdateProgress> </DIV>
</contenttemplate>
                </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upListPO" runat="server">
        <contenttemplate>
<asp:Panel id="PanelListPO" runat="server" Width="600px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListPO" runat="server" Font-Size="Medium" Font-Bold="True" Text="List Of Outstanding Giro"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListPR" runat="server" Width="100%" DefaultButton="btnFindListPO">
                <asp:Label id="Label24" runat="server" Text="Filter :"></asp:Label> <asp:DropDownList id="FilterDDLListPO" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="cashbankno">Cash/Bank No.</asp:ListItem>
                    <asp:ListItem Value="name">Supplier/Customer</asp:ListItem>
    <asp:ListItem Value="cashbankrefno">Ref. No.</asp:ListItem>
    <asp:ListItem Value="cashbanknote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListPO" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListPO" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewAllListPO" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; BORDER-BOTTOM-STYLE: none" id="Fieldset1"><DIV id="Div1"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="gvListPO" runat="server" ForeColor="#333333" BorderColor="Black" Width="98%" CellPadding="4" AutoGenerateColumns="False" BorderStyle="None" DataKeyNames="cashbankoid,cashbankno,cashbankdate,cashbanknote,cashbankrefno,name,cashbankstatus,refname,cashbankamt,cashbankgroup,cashbankacctgoid" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="cashbankno" HeaderText="Cash/Bank No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankdate" HeaderText="Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="name" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbanknote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                            <asp:Label ID="lblInfoOnListGV" runat="server" CssClass="Important" Text="There is no data !!"></asp:Label>
                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> &nbsp;</DIV>&nbsp;</FIELDSET> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbListPO" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListPO" runat="server" PopupControlID="PanelListPO" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListPO" TargetControlID="btnHiddenListPO"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListPO" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel><asp:UpdatePanel id="UpdPnlInv" runat="server">
        <contenttemplate>
            <asp:Panel id="PanelInv" runat="server" Width="600px" CssClass="modalBox" Visible="False">
                <TABLE style="WIDTH: 100%">
                    <TBODY>
                        <TR>
                            <TD class="Label" align=center colSpan=3>
                                <asp:Label ID="lblListInv" runat="server" Font-Bold="True" Font-Size="Medium" Text="List Of Internal Invoice"></asp:Label></td>
                        </tr>
                        <TR>
                            <TD style="HEIGHT: 5px" class="Label" align=center colSpan=3>
                            </td>
                        </tr>
                        <TR>
                            <TD class="Label" align=center colSpan=3>
                                <asp:Panel id="pnlFindListInv" runat="server" Width="100%" DefaultButton="btnFindListInv">
                                    <asp:Label ID="lblFilter" runat="server" Text="Filter :"></asp:Label>
                                    <asp:DropDownList id="FilterDDLListInv" runat="server" CssClass="inpText" Width="100px">
                                        <asp:ListItem Value="custname">Customer</asp:ListItem>
                                        <asp:ListItem Value="custaddr">Address</asp:ListItem>
                                        <asp:ListItem Value="trnjualno">Invoice No</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:TextBox ID="FilterTextListInv" runat="server" CssClass="inpText" Width="150px"></asp:TextBox>
                                    <asp:ImageButton ID="btnFindListInv" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/btnfind.bmp"
                                        OnClick="btnFindListInv_Click" />
                                    <asp:ImageButton ID="btnViewAllListInv" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewall.png"
                                        OnClick="btnViewAllListInv_Click" /></asp:Panel>
                            </td>
                        </tr>
                        <TR>
                            <TD style="HEIGHT: 5px" class="Label" align=center colSpan=3>
                            </td>
                        </tr>
                        <TR>
                            <TD class="Label" align=center colSpan=3>
                                <FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; BORDER-BOTTOM-STYLE: none" id="Fieldset2">
                                    <DIV id="Div2">
                                    </div>
                                    <DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%">
                                        <asp:GridView id="gvListInv" runat="server" ForeColor="#333333" BorderColor="Black" Width="98%" CellPadding="4" AutoGenerateColumns="False" BorderStyle="None" DataKeyNames="trnjualno,trnjualmstoid" GridLines="None" OnSelectedIndexChanged="gvListInv_SelectedIndexChanged">
                                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                            <Columns>
                                                <asp:CommandField ShowSelectButton="True">
                                                    <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:CommandField>
                                                <asp:BoundField DataField="trnjualno" HeaderText="Invoice No.">
                                                    <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="trnjualdate" HeaderText="Date">
                                                    <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="custname" HeaderText="Customer">
                                                    <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="custaddr" HeaderText="Address">
                                                    <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                            </Columns>
                                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                            <EmptyDataTemplate>
                                                <asp:Label ID="lblInfoOnListGV" runat="server" CssClass="Important" Text="There is no data !!"></asp:Label>
                                            </EmptyDataTemplate>
                                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                            <AlternatingRowStyle BackColor="White" />
                                        </asp:GridView>
                                        &nbsp;</div>
                                    &nbsp;</fieldset>
                            </td>
                        </tr>
                        <TR>
                            <TD style="HEIGHT: 5px" class="Label" align=center colSpan=3>
                            </td>
                        </tr>
                        <TR>
                            <TD align=center colSpan=3>
                                <asp:LinkButton ID="lkbListInv" runat="server" OnClick="lkbListInv_Click">[ Cancel & Close ]</asp:LinkButton></td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender id="MpeListInv" runat="server" PopupControlID="PanelInv" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListInv" TargetControlID="btnHideListInv">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="btnHideListInv" runat="server" Visible="False" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

