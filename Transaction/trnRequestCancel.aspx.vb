Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class Transaction_MaterialRequestBySOCancellation
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitDDL()
        ' Init DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)
    End Sub

    Private Sub BindListReq()
        sSql = "SELECT * FROM (SELECT reqmstoid, reqno, CONVERT(VARCHAR(10), reqdate, 101) AS reqdate, ISNULL((SELECT soitemno FROM QL_trnsoitemmst som WHERE som.soitemmstoid=req.soitemmstoid), '') AS soitemno, ISNULL((SELECT deptname FROM QL_mstdept de WHERE de.deptoid=req.deptoid), '') AS deptname, reqmstnote FROM QL_trnreqmst req WHERE req.cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqmststatus='Post' AND reqmstoid NOT IN (SELECT reqmstoid FROM QL_trnusagedtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "')) AS tbl_request WHERE " & FilterDDLListReq.SelectedValue & " LIKE '%" & Tchar(FilterTextListReq.Text) & "%' ORDER BY CONVERT(DATETIME, reqdate) DESC, reqmstoid DESC"
        FillGV(gvListReq, sSql, "tbl_request")
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnRequestCancel.aspx")
        End If
        If checkPagePermission("~\Transaction\trnRequestCancel.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Material Request By SO Cancellation"
        btnProcess.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to CANCEL this data?');")
        If Not Page.IsPostBack Then
            closeuser.Text = Session("UserID") : closetime.Text = GetServerTime().ToString
            InitDDL()
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvReqDtl.DataSource = dt
            gvReqDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("Success") Is Nothing And Session("Success") <> "" Then
            Response.Redirect("~\Transaction\trnRequestCancel.aspx?awal=true")
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        btnClearReq_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnSearchReq_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchReq.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2) : Exit Sub
        End If
        FilterDDLListReq.SelectedIndex = -1 : FilterTextListReq.Text = "" : gvListReq.SelectedIndex = -1
        BindListReq()
        cProc.SetModalPopUpExtender(btnHideListReq, pnlListReq, mpeListReq, True)
    End Sub

    Protected Sub btnClearReq_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearReq.Click
        reqmstoid.Text = "" : reqno.Text = "" : Session("TblDtl") = Nothing : gvReqDtl.DataSource = Session("TblDtl") : gvReqDtl.DataBind()
    End Sub

    Protected Sub btnFindListReq_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListReq.Click
        BindListReq()
        mpeListReq.Show()
    End Sub

    Protected Sub btnAllListReq_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListReq.Click
        FilterDDLListReq.SelectedIndex = -1 : FilterTextListReq.Text = "" : gvListReq.SelectedIndex = -1
        BindListReq()
        mpeListReq.Show()
    End Sub

    Protected Sub gvListReq_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListReq.PageIndexChanging
        gvListReq.PageIndex = e.NewPageIndex
        BindListReq()
        mpeListReq.Show()
    End Sub

    Protected Sub gvListReq_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListReq.SelectedIndexChanged
        reqmstoid.Text = gvListReq.SelectedDataKey.Item("reqmstoid").ToString
        reqno.Text = gvListReq.SelectedDataKey.Item("reqno").ToString
        ' Generate Detail Data
        sSql = "SELECT reqdtloid, reqdtlseq, reqreftype, (CASE reqreftype WHEN 'Raw' THEN (SELECT matrawcode FROM QL_mstmatraw m WHERE matrawoid=reqrefoid) WHEN 'General' THEN (SELECT matgencode FROM QL_mstmatgen m WHERE matgenoid=reqrefoid) WHEN 'Spare Part' THEN (SELECT sparepartcode FROM QL_mstsparepart m WHERE sparepartoid=reqrefoid) ELSE '' END) AS matcode, (CASE reqreftype WHEN 'Raw' THEN (SELECT matrawlongdesc FROM QL_mstmatraw m WHERE matrawoid=reqrefoid) WHEN 'General' THEN (SELECT matgenlongdesc FROM QL_mstmatgen m WHERE matgenoid=reqrefoid) WHEN 'Spare Part' THEN (SELECT sparepartlongdesc FROM QL_mstsparepart m WHERE sparepartoid=reqrefoid) ELSE '' END) AS matlongdesc, reqqty, gendesc AS requnit, reqdtlnote FROM QL_trnreqdtl req INNER JOIN QL_mstgen g ON genoid=requnitoid WHERE req.cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqmstoid=" & reqmstoid.Text & " ORDER BY reqdtlseq"
        Session("TblDtl") = cKon.ambiltabel(sSql, "tbl_requestdetail") : gvReqDtl.DataSource = Session("TblDtl") : gvReqDtl.DataBind()
        cProc.SetModalPopUpExtender(btnHideListReq, pnlListReq, mpeListReq, False)
    End Sub

    Protected Sub lbCloseListReq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListReq.Click
        cProc.SetModalPopUpExtender(btnHideListReq, pnlListReq, mpeListReq, False)
    End Sub

    Protected Sub gvReqDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvReqDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnRequestCancel.aspx?awal=true")
    End Sub

    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnProcess.Click
        If reqmstoid.Text = "" Then
            showMessage("Please select Request No. first!", 2) : Exit Sub
        End If
        If closereason.Text = "" Then
            showMessage("Please fill Cancel Reason first!", 2) : Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trnwodtl3 SET wodtl3reqflag='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl3reftype='Raw' AND wodtl3refoid IN (SELECT reqrefoid FROM QL_trnreqdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqmstoid=" & reqmstoid.Text & " AND reqreftype='Raw') AND wodtl2oid IN (SELECT wodtl2oid FROM QL_trnreqmst reqm INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=reqm.cmpcode AND wod1.soitemmstoid=reqm.soitemmstoid INNER JOIN QL_trnwodtl2 wod2 ON wod2.cmpcode=wod1.cmpcode AND wod2.wodtl1oid=wod1.wodtl1oid AND wod2.deptoid=reqm.deptoid WHERE reqm.cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqmstoid=" & reqmstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_trnwodtl3 SET wodtl3reqflag='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl3reftype='General' AND wodtl3refoid IN (SELECT reqrefoid FROM QL_trnreqdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqmstoid=" & reqmstoid.Text & " AND reqreftype='General') AND wodtl2oid IN (SELECT wodtl2oid FROM QL_trnreqmst reqm INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=reqm.cmpcode AND wod1.soitemmstoid=reqm.soitemmstoid INNER JOIN QL_trnwodtl2 wod2 ON wod2.cmpcode=wod1.cmpcode AND wod2.wodtl1oid=wod1.wodtl1oid AND wod2.deptoid=reqm.deptoid WHERE reqm.cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqmstoid=" & reqmstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_trnwodtl3 SET wodtl3reqflag='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl3reftype='Spare Part' AND wodtl3refoid IN (SELECT reqrefoid FROM QL_trnreqdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqmstoid=" & reqmstoid.Text & " AND reqreftype='Spare Part') AND wodtl2oid IN (SELECT wodtl2oid FROM QL_trnreqmst reqm INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=reqm.cmpcode AND wod1.soitemmstoid=reqm.soitemmstoid INNER JOIN QL_trnwodtl2 wod2 ON wod2.cmpcode=wod1.cmpcode AND wod2.wodtl1oid=wod1.wodtl1oid AND wod2.deptoid=reqm.deptoid WHERE reqm.cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqmstoid=" & reqmstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_trnwodtl2 SET wodtl2reqflag='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl2oid IN (SELECT wodtl2oid FROM QL_trnreqmst reqm INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=reqm.cmpcode AND wod1.soitemmstoid=reqm.soitemmstoid INNER JOIN QL_trnwodtl2 wod2 ON wod2.cmpcode=wod1.cmpcode AND wod2.wodtl1oid=wod1.wodtl1oid AND wod2.deptoid=reqm.deptoid WHERE reqm.cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqmstoid=" & reqmstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_trnreqmst SET reqmststatus='Cancel', closereason='" & Tchar(closereason.Text) & "', closeuser='" & closeuser.Text & "', closetime='" & closetime.Text & "' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqmstoid=" & reqmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Session("Success") = "Request No. : " & reqno.Text & " have been cancelled successfully."
        showMessage(Session("Success"), 3)
    End Sub
#End Region

End Class