Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class Transaction_POClosing
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Function"
    Private Function GetOidDetail() As String
        Dim sReturn As String = ""
        For C1 As Integer = 0 To gvPODtl.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvPODtl.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        If cbCheck Then
                            sReturn &= CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip & ","
                        End If
                    End If
                Next
            End If
        Next
        If sReturn <> "" Then
            sReturn = Left(sReturn, sReturn.Length - 1)
        End If
        Return sReturn
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitDDL(ByVal gencode As String)
        ' Init DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(pobusinessunit, sSql)
        sSql = "SELECT gencode,gendesc As Code, gendesc FROM QL_mstgen WHERE cmpcode='" & pobusinessunit.SelectedValue & "' " & gencode & "  AND gengroup='GROUPITEM' AND activeflag='ACTIVE' ORDER BY gencode"
        FillDDL(ddlType, sSql)
    End Sub

    Private Sub BindListPO()
        Dim sType As String = ddlType.SelectedValue

        sSql = "SELECT DISTINCT pm.cmpcode,pm.pomstoid,pm.pogroup,pm.pono,pm.podate,sp.suppname,pm.pomststatus,pm.pomstnote FROM QL_trnpomst pm INNER JOIN QL_mstsupp sp ON sp.suppoid=pm.suppoid INNER JOIN QL_trnmrmst mr ON mr.pomstoid=pm.pomstoid AND mr.cmpcode=pm.cmpcode WHERE pm.cmpcode='" & pobusinessunit.SelectedValue & "' AND pomststatus='Approved' AND pm.pogroup='" & ddlType.SelectedValue & "' "
     
        If checkPagePermission("~\Transaction\trnPOClosing.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND pm.createuser='" & Session("UserID") & "'"
        End If
        sSql &= " ORDER BY pm.podate DESC, pomstoid DESC"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnpomst")
        If dt.Rows.Count = 0 Then
            showMessage("There is no Approved PO can be closed for this time.", 2)
            Exit Sub
        End If
        Session("Tblpomst") = dt
        gvListPO.DataSource = Session("Tblpomst")
        gvListPO.DataBind()
        gvListPO.SelectedIndex = -1
        cProc.SetModalPopUpExtender(btnHiddenListPO, PanelListPO, mpeListPO, True)
    End Sub

    Private Sub ViewDetailPO(ByVal iOid As Integer)
        sSql = " SELECT pom.cmpcode,pod.pomstoid,pom.pono,pod.podtloid,pom.pogroup,sp.suppname,i.itemoid,i.itemCode,i.itemLongDescription, isnull(pod.poqty,0.0) AS poqty,(SELECT ISNULL(SUM(md.mrQty),0.0) FROM QL_trnmrdtl md WHERE md.podtloid=pod.podtloid) AS MrQty,(SELECT ISNULL(SUM(rtd.retqty),0.0) FROM QL_trnreturdtl rtd WHERE rtd.matoid=i.itemoid AND i.cmpcode=rtd.cmpcode) AS RetQty,ISNULL(pod.poqty,0.0)-((SELECT ISNULL(SUM(md.mrQty),0.0) FROM QL_trnmrdtl md WHERE md.podtloid=pod.podtloid AND md.cmpcode=pod.cmpcode)-(SELECT ISNULL(SUM(rtd.retqty),0.0) FROM QL_trnreturdtl rtd WHERE rtd.matoid=i.itemoid AND i.cmpcode=rtd.cmpcode)) AS QtyOs,CASE ISNULL(pod.poqty,0.0)-((SELECT ISNULL(SUM(md.mrQty),0.0) FROM QL_trnmrdtl md WHERE md.podtloid=pod.podtloid)- (SELECT ISNULL(SUM(rtd.retqty),0.0) FROM QL_trnreturdtl rtd WHERE rtd.matoid=i.itemoid AND i.cmpcode=rtd.cmpcode)) WHEN 0 THEN pod.poqty ELSE ISNULL(pod.poqty,0.0)-((SELECT ISNULL(SUM(md.mrQty),0.0) FROM QL_trnmrdtl md WHERE md.podtloid=pod.podtloid)-(SELECT ISNULL(SUM(rtd.retqty),0.0) FROM QL_trnreturdtl rtd WHERE rtd.matoid=i.itemoid AND i.cmpcode=rtd.cmpcode)) END QtyOs1,u.gendesc,pod.podtlseq,pod.podtlstatus,pod.podtlnote " & _
        " FROM QL_trnpomst pom " & _
        " INNER JOIN QL_trnpodtl pod ON pom.pomstoid=pod.pomstoid " & _
        " AND pom.cmpcode=pod.cmpcode AND pod.cmpcode=pom.cmpcode " & _
        " AND pom.pomststatus='Approved' AND pom.pomststatus <> 'Complete'" & _
        " AND pod.podtlstatus <> 'COMPLETE' " & _
        " INNER JOIN QL_mstitem i ON i.itemoid=pod.matoid " & _
        " INNER JOIN QL_mstsupp sp ON sp.suppoid=pom.suppoid " & _
        " INNER JOIN QL_mstgen u ON u.genoid=pod.pounitoid " & _
        " WHERE pom.cmpcode='" & pobusinessunit.SelectedValue & "' " & _
        " AND pom.pomstoid=" & pomstoid.Text & " AND pom.pogroup='" & ddlType.SelectedValue & "' GROUP BY pom.cmpcode,pod.cmpcode,pod.pomstoid,pom.pono,pom.pogroup,sp.suppname,i.itemoid, pod.podtlseq,i.itemCode, i.itemLongDescription, pod.poqty, u.gendesc, pod.podtlstatus,pod.podtloid, pod.podtlnote,i.cmpcode ORDER BY i.itemCode"

        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnpodtl")
        'For C1 As Integer = 0 To dt.Rows.Count - 1
        '    dt.Rows(C1)("podtlseq") = C1 + 1
        '    dt.Rows(C1)("poqty") = ToDouble(dt.Rows(C1)("mrqty").ToString) - ToDouble(dt.Rows(C1)("poqty").ToString)
        'Next
        Session("TblDtlPO") = dt
        gvPODtl.DataSource = dt
        gvPODtl.DataBind()
    End Sub

    Private Sub ClearData()
        pomstoid.Text = ""
        pono.Text = ""
        podate.Text = ""
        suppname.Text = ""
        pomstnote.Text = ""
        pomststatus.Text = ""
        gvPODtl.DataSource = Nothing
        gvPODtl.DataBind()
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnPOClosing.aspx")
        End If
        If checkPagePermission("~\Transaction\trnPOClosing.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - PO Closing"
        btnClosing.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to CLOSE this selected data?');")
        btnClosingAll.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to CLOSE this data?');")
        If Not Page.IsPostBack Then
            upduser.Text = Session("UserID") : updtime.Text = GetServerTime().ToString
            InitDDL("")
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("Success") Is Nothing And Session("Success") <> "" Then
            Response.Redirect("~\Transaction\trnPOClosing.aspx?awal=true")
        End If
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        ClearData()
    End Sub

    Protected Sub pobusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles pobusinessunit.SelectedIndexChanged
        ClearData()
    End Sub

    Protected Sub imbFindPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindPO.Click
        DDLFilterListPO.SelectedIndex = -1
        txtFilterListPO.Text = ""
        Session("Tblpomst") = Nothing
        gvListPO.DataSource = Session("Tblpomst")
        gvListPO.DataBind()
        BindListPO()
    End Sub

    Protected Sub imbErasePO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbErasePO.Click
        ClearData()
        InitDDL("")
        ddlType.Enabled = True
        ddlType.CssClass = "inpText"
    End Sub

    Protected Sub btnFindListPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListPO.Click
        Dim dv As DataView = Session("Tblpomst").DefaultView
        dv.RowFilter = DDLFilterListPO.SelectedValue & " LIKE '%" & Tchar(txtFilterListPO.Text) & "%'"
        gvListPO.DataSource = dv.ToTable
        gvListPO.DataBind()
        dv.RowFilter = ""
        mpeListPO.Show()
    End Sub

    Protected Sub btnViewAllListPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListPO.Click
        txtFilterListPO.Text = ""
        DDLFilterListPO.SelectedIndex = -1
        gvListPO.SelectedIndex = -1
        gvListPO.DataSource = Session("Tblpomst")
        gvListPO.DataBind()
        mpeListPO.Show()
    End Sub

    Protected Sub gvListPO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListPO.PageIndexChanging
        gvListPO.PageIndex = e.NewPageIndex
        Dim dv As DataView = Session("Tblpomst").DefaultView
        dv.RowFilter = DDLFilterListPO.SelectedValue & " LIKE '%" & Tchar(txtFilterListPO.Text) & "%'"
        gvListPO.DataSource = dv.ToTable
        gvListPO.DataBind()
        dv.RowFilter = ""
        mpeListPO.Show()
    End Sub

    Protected Sub gvListPO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListPO.SelectedIndexChanged
        If pomstoid.Text = gvListPO.SelectedDataKey.Item("pomstoid").ToString Then
            ClearData()
        End If
        pono.Text = gvListPO.SelectedDataKey.Item("pono").ToString
        pomstoid.Text = gvListPO.SelectedDataKey.Item("pomstoid").ToString
        podate.Text = Format(CDate(gvListPO.SelectedDataKey.Item("podate").ToString), "MM/dd/yyyy")
        suppname.Text = gvListPO.SelectedDataKey.Item("suppname").ToString
        pomststatus.Text = gvListPO.SelectedDataKey.Item("pomststatus").ToString
        pomstnote.Text = gvListPO.SelectedDataKey.Item("pomstnote").ToString
        InitDDL("AND gencode='" & gvListPO.SelectedDataKey.Item("pogroup").ToString & "'")
        ViewDetailPO(CInt(pomstoid.Text))
        ddlType.Enabled = False
        ddlType.CssClass = "inpTextDisabled"
        cProc.SetModalPopUpExtender(btnHiddenListPO, PanelListPO, mpeListPO, False)
    End Sub

    Protected Sub lkbListPO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListPO.Click
        cProc.SetModalPopUpExtender(btnHiddenListPO, PanelListPO, mpeListPO, False)
    End Sub

    Protected Sub gvpoDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPODtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 4)
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnPOClosing.aspx?awal=true")
    End Sub

    'Protected Sub btnClosing_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClosing.Click
    '    If pomstoid.Text = "" Then
    '        showMessage("Please select PO Data first!", 2)
    '        Exit Sub
    '    End If
    '    Dim sType As String = ddlType.SelectedValue
    '    Dim sOid As String = GetOidDetail()

    '    If sOid = "" Then
    '        showMessage("Please select PO Detail Data first!", 2)
    '        Exit Sub
    '    Else
    '        sSql = "SELECT COUNT(*) FROM QL_trnpodtl WHERE pomstoid=" & pomstoid.Text & " AND podtloid IN (" & sOid & ") AND podtlstatus='COMPLETE'"
    '        If CheckDataExists(sSql) Then
    '            showMessage("This data has been Closed by another user. Please CANCEL this transaction and try again!", 2)
    '            Exit Sub
    '        End If
    '    End If
    '    Dim objTable As DataTable
    '    objTable = Session("TblDtlPO")
    '    Dim dv As DataView = objTable.DefaultView
    '    dv.RowFilter = "podtloid IN (" & sOid & ")"
    '    Dim objTrans As SqlClient.SqlTransaction
    '    If conn.State = ConnectionState.Closed Then
    '        conn.Open()
    '    End If
    '    objTrans = conn.BeginTransaction()
    '    xCmd.Transaction = objTrans
    '    Try
    '        For C1 As Integer = 0 To dv.Count - 1
    '            sSql = "UPDATE QL_trnpodtl SET podtlstatus='COMPLETE',closeqty='" & objTable.Rows(C1).Item("OsQty") & "', podtlres1='" & dv(C1)("poqty") & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & Session("CompnyCode") & "' AND pomstoid=" & pomstoid.Text & " AND podtloid =" & dv(C1)("podtloid")
    '            xCmd.CommandText = sSql
    '            xCmd.ExecuteNonQuery()
    '        Next
    '        sSql = "UPDATE QL_trnpomst SET pomststatus='Closed', pomstres1='Force Closed', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & Session("CompnyCode") & "' AND pomstoid=" & pomstoid.Text & " AND (SELECT COUNT(-1) FROM QL_trnpodtl WHERE cmpcode='" & Session("CompnyCode") & "' AND podtlstatus='' AND pomstoid=" & pomstoid.Text & " AND podtloid NOT IN (" & sOid & "))=0"
    '        xCmd.CommandText = sSql
    '        xCmd.ExecuteNonQuery()
    '        objTrans.Commit()
    '        xCmd.Connection.Close()
    '    Catch ex As Exception
    '        objTrans.Rollback()
    '        xCmd.Connection.Close()
    '        showMessage(ex.Message, 1)
    '        Exit Sub
    '    End Try
    '    Session("Success") = "Some PO Detail for PO No : " & pono.Text & " have been close successfully."
    '    showMessage(Session("Success"), 3)
    'End Sub

    Protected Sub btnClosingAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClosingAll.Click
        Dim sType As String = ddlType.SelectedValue
        Dim sOid As String = ""
        Dim objTable As DataTable
        objTable = Session("TblDtlPO")
        If pomstoid.Text = "" Then
            showMessage("Please select PO Data first!", 2)
            Exit Sub
        Else
            For C1 As Integer = 0 To objTable.Rows.Count - 1
                sOid &= objTable.Rows(C1).Item("podtloid") & ","
            Next
            sOid = Left(sOid, sOid.Length - 1)
            sSql = "SELECT COUNT(*) FROM QL_trnpodtl WHERE pomstoid=" & pomstoid.Text & " AND podtloid IN (" & sOid & ") AND podtlstatus='COMPLETE' AND cmpcode='" & pobusinessunit.SelectedValue & "'"
            If CheckDataExists(sSql) Then
                showMessage("This data has been Closed by another user. Please CANCEL this transaction and try again!", 2)
                Exit Sub
            End If
        End If
        If pomstnote.Text = "" Then
            showMessage("Please fill NOTE field as a reason for closing this data!", 2)
            Exit Sub
        End If

        If pomstnote.Text.Length > 96 Then
            showMessage("Please fill NOTE field as a reason for closing this data!", 2)
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            'If sType <> "service" And sType <> "Ekspedisi" Then
            '    sSql = "UPDATE QL_pr" & sType & "dtl SET pr" & sType & "dtlstatus='', pr" & sType & "dtlres1='' WHERE cmpcode='" & pobusinessunit.SelectedValue & "' AND pr" & sType & "dtloid IN (SELECT pr" & sType & "dtloid FROM QL_trnpo" & sType & "dtl WHERE cmpcode='" & pobusinessunit.SelectedValue & "' AND po" & sType & "mstoid=" & pomstoid.Text & ")"
            '    xCmd.CommandText = sSql
            '    xCmd.ExecuteNonQuery()
            '    sSql = "UPDATE QL_pr" & sType & "mst SET pr" & sType & "mststatus='Approved' WHERE cmpcode='" & pobusinessunit.SelectedValue & "' AND pr" & sType & "mstoid IN (SELECT DISTINCT pr" & sType & "mstoid FROM QL_trnpo" & sType & "dtl WHERE cmpcode='" & pobusinessunit.SelectedValue & "' AND po" & sType & "mstoid=" & pomstoid.Text & ")"
            '    xCmd.CommandText = sSql
            '    xCmd.ExecuteNonQuery()
            'End If
            For C1 As Integer = 0 To objTable.Rows.Count - 1
                'podtlres1='" & objTable.Rows(C1).Item("poqty") & "', 
                sSql = "UPDATE QL_trnpodtl SET closeqty='" & objTable.Rows(C1).Item("QtyOs") & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & pobusinessunit.SelectedValue & "' AND podtloid=" & objTable.Rows(C1).Item("podtloid") & " AND pomstoid=" & pomstoid.Text
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            Next
            sSql = "UPDATE QL_trnpomst SET pomstnote='" & Tchar(pomstnote.Text.Trim) & "',pomststatus='Closed', pomstres1='Force Closed', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & pobusinessunit.SelectedValue & "' AND pomstoid=" & pomstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Session("Success") = "po No : " & pono.Text & " have been close successfully."
        showMessage(Session("Success"), 3)
    End Sub

    Protected Sub gvListPO_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "MM/dd/yyyy")
        End If
    End Sub
#End Region
End Class