<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnAR.aspx.vb" Inherits="Transaction_AR" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Sales Invoice" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Sales Invoice :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 8%" class="Label" align=left>Filter</TD><TD style="WIDTH: 2%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="105px"><asp:ListItem Value="arm.trnjualmstoid">Draft No.</asp:ListItem>
<asp:ListItem Value="arm.trnjualno">Invoice No.</asp:ListItem>
<asp:ListItem Value="dom.dono">DO No.</asp:ListItem>
<asp:ListItem Value="sono">SO No.</asp:ListItem>
<asp:ListItem Value="s.custname">Customer</asp:ListItem>
<asp:ListItem Value="arm.trnjualnote">Note</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="250px"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text="Periode"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="60px" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label5" runat="server" Text="to"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="60px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="lblFormatTgl" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="100px"><asp:ListItem>All</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
<asp:ListItem>Closed</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="CBprint" runat="server" Text="Print Option Tax" AutoPostBack="True" OnCheckedChanged="CBprint_CheckedChanged"></asp:CheckBox></TD><TD class="Label" align=center><asp:Label id="Label42" runat="server" Text=":" Visible="False"></asp:Label></TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="DDLType" runat="server" CssClass="inpText" Width="100px" Visible="False"><asp:ListItem>PRINT FAKTUR</asp:ListItem>
<asp:ListItem>PRINT NOTA</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="FilterPeriod1" MaskType="Date" Mask="99/99/9999" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" PopupButtonID="imbDate1" Format="MM/dd/yyyy" TargetControlID="FilterPeriod1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" TargetControlID="FilterPeriod2" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" PopupButtonID="imbDate2" Format="MM/dd/yyyy" TargetControlID="FilterPeriod2"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:LinkButton id="lkbAPInProcess" runat="server" Visible="False"></asp:LinkButton> <asp:LinkButton id="lkbAPInApproval" runat="server" Visible="False"></asp:LinkButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvTRN" runat="server" ForeColor="#333333" Width="100%" GridLines="None" CellPadding="4" AutoGenerateColumns="False" PageSize="8" DataKeyNames="trnjualmstoid" AllowPaging="True" OnRowDataBound="gvTRN_RowDataBound" AllowSorting="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="trnjualmstoid" DataNavigateUrlFormatString="~\Transaction\trnar.aspx?oid={0}" DataTextField="trnjualmstoid" HeaderText="Draft No." SortExpression="trnjualmstoid">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="trnjualno" HeaderText="Invoice No." SortExpression="trnjualno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualdate" HeaderText="Invoice Date" SortExpression="trnjualdate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dono" HeaderText="DO No.">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="sono" HeaderText="SO No.">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer" SortExpression="custname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sales" HeaderText="Sales">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="netto" HeaderText="Total Netto">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualnote" HeaderText="Note" SortExpression="trnjualnote">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualstatus" HeaderText="Status" SortExpression="trnjualstatus">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="divname" HeaderText="Business Unit" SortExpression="divname" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbGVMst" runat="server" Checked='<%# eval("checkvalue") %>'></asp:CheckBox><asp:Label id="lblOidGVMst" runat="server" Text='<%# eval("trnjualmstoid") %>' Visible="False"></asp:Label> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label40" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Invoice Header" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="periodacctg" runat="server" Visible="False"></asp:Label> <asp:Label id="valueidr" runat="server" Visible="False"></asp:Label> <asp:Label id="valueusd" runat="server" Visible="False"></asp:Label><asp:Label id="i_u" runat="server" CssClass="Important" Text="New Data"></asp:Label> <asp:Label id="Label10" runat="server" Text="Business Unit" Visible="False"></asp:Label><ajaxToolkit:CalendarExtender id="ceRSD" runat="server" TargetControlID="Rdate" Format="MM/dd/yyyy" PopupButtonID="btnRSD"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeRSD" runat="server" TargetControlID="Rdate" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="custoid" runat="server" Visible="False"></asp:Label>&nbsp; <asp:Label id="Rdate2" runat="server" Visible="False"></asp:Label> <asp:Label id="CRFreeIDR" runat="server" Visible="False"></asp:Label> <asp:Label id="CRFreeUSD" runat="server" Visible="False"></asp:Label> <asp:Label id="paymentType" runat="server" Visible="False"></asp:Label> <ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" TargetControlID="dateETD" Format="MM/dd/yyyy" PopupButtonID="imgitg"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" TargetControlID="dateETD" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpText" AutoPostBack="True" Visible="False"></asp:DropDownList></TD><TD class="Label" align=left><ajaxToolkit:CalendarExtender id="ceARDate" runat="server" TargetControlID="arrawdate" Format="MM/dd/yyyy" PopupButtonID="imbPRDate"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="ceGiroDate" runat="server" TargetControlID="arrawdatetakegiro" Format="MM/dd/yyyy" PopupButtonID="imbDTG"></ajaxToolkit:CalendarExtender></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><ajaxToolkit:MaskedEditExtender id="meeARDate" runat="server" TargetControlID="arrawdate" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meeGiroDate" runat="server" TargetControlID="arrawdatetakegiro" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD id="BusinessUnit1" class="Label" align=left runat="server" visible="true"><asp:Label id="lblTrnNo" runat="server" Text="Draft No."></asp:Label></TD><TD id="BusinessUnit2" class="Label" align=center runat="server" visible="true">
    :</TD><TD id="BusinessUnit3" class="Label" align=left runat="server" visible="true"><asp:TextBox id="arrawno" runat="server" CssClass="inpTextDisabled" Width="125px" Visible="False" Enabled="False"></asp:TextBox> <asp:Label id="arrawmstoid" runat="server"></asp:Label></TD><TD class="Label" align=left>&nbsp;</TD><TD class="Label" align=center></TD><TD class="Label" align=left>&nbsp;&nbsp;</TD></TR><TR><TD class="Label" align=left><asp:Label id="Label6" runat="server" Text="Customer"></asp:Label> <asp:Label id="Label44" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custname" runat="server" CssClass="inpTextDisabled" Width="150px" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSearchCust" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnClearCust" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="Label8" runat="server" Text="Invoice Date"></asp:Label> <asp:Label id="Label43" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="arrawdate" runat="server" CssClass="inpText" Width="100px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbPRDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label29" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label12" runat="server" Text="Currency"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="curroid" runat="server" CssClass="inpText" Width="75px" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label16" runat="server" Text="Payment Type"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="arrawpaytypeoid" runat="server" CssClass="inpText" Width="130px"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label13" runat="server" Text="Daily Rate to IDR"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="rateidrvalue" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False">1.00</asp:TextBox> <asp:Label id="rateoid" runat="server" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label34" runat="server" Text="Daily Rate To USD"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="arrawratetousd" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False">0.00</asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label33" runat="server" Text="Monthly Rate to IDR"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="rate2idrvalue" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False">0.00</asp:TextBox>&nbsp;<asp:Label id="rate2oid" runat="server" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label35" runat="server" Text="Monthly Rate To USD"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="arrawrate2tousd" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False">0.00</asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label17" runat="server" Text="Total Amt"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="arrawtotalamt" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False">0</asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label18" runat="server" Text="Header Disc."></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="arrawtotaldisc" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False">0</asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label20" runat="server" Text="Tax"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="arrawtaxtype" runat="server" CssClass="inpTextDisabled" Width="100px" AutoPostBack="True" Enabled="False"><asp:ListItem Value="EXC">EXCLUDE</asp:ListItem>
<asp:ListItem Value="INC">INCLUDE</asp:ListItem>
<asp:ListItem Value="NT">NON  TAX</asp:ListItem>
</asp:DropDownList> <asp:DropDownList id="doType" runat="server" CssClass="inpTextDisabled" Width="150px" AutoPostBack="True" Visible="False" Enabled="False"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label31" runat="server" Text="Tax Amt"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="arrawvat" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False">0</asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label41" runat="server" Text="DPP Amt"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="trnJualDPP" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False">0</asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label19" runat="server" Text="Total Netto"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="arrawtotalnetto" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False">0</asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label45" runat="server" Text="Sales"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLsales" runat="server" CssClass="inpText" Width="200px" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="lblRdate" runat="server" Text="Receive Date" Visible="False"></asp:Label> <asp:Label id="lblPetik" runat="server" CssClass="Important" Text="*" Visible="False"></asp:Label></TD><TD class="Label" align=center><asp:Label id="lblTitik" runat="server" Text=":" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="Rdate" runat="server" CssClass="inpText" Width="100px" ToolTip="MM/DD/YYYY" Visible="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnRSD" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Width="16px" Visible="False" Height="16px"></asp:ImageButton>&nbsp;<asp:Label id="Label49" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" Visible="False" Height="16px"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label23" runat="server" Text="No. Faktur Pajak"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="fakturpajak" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" MaxLength="29"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label39" runat="server" Text="Ref No."></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="refno" runat="server" CssClass="inpText" Width="100px" MaxLength="19"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblDTG" runat="server" Text="Date Take Giro"></asp:Label> <asp:Label id="Label47" runat="server" CssClass="Important" Text="*" Visible="False"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="arrawdatetakegiro" runat="server" CssClass="inpText" Width="80px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbDTG" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label3" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label25" runat="server" Text="ETD"></asp:Label> <asp:Label id="Label48" runat="server" CssClass="Important" Text="*" Visible="False"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="dateETD" runat="server" CssClass="inpText" Width="98px"></asp:TextBox> <asp:ImageButton id="imgitg" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="lblFormatTgl3" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label24" runat="server" Text="Header Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="arrawmstnote" runat="server" CssClass="inpText" Width="200px" MaxLength="99"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label4" runat="server" Text="Status"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="arrawmststatus" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False" MaxLength="20"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><ajaxToolkit:FilteredTextBoxExtender id="ftbDiscHdr" runat="server" TargetControlID="arrawmstdiscvalue" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender></TD><TD class="Label" align=center></TD><TD class="Label" align=left>&nbsp; <ajaxToolkit:FilteredTextBoxExtender id="ftbTax" runat="server" TargetControlID="arrawtaxamt" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender></TD><TD class="Label" align=left><asp:Label id="Label46" runat="server" Text="Komisi Sales" Visible="False"></asp:Label> <asp:Label id="Label26" runat="server" Text="Total Disc. Dtl Amt" Visible="False"></asp:Label> <asp:TextBox id="hpp" runat="server" CssClass="inpText" Width="100px" Visible="False" MaxLength="100"></asp:TextBox><asp:TextBox id="komisiSales" runat="server" CssClass="inpTextDisabled" Width="100px" Visible="False" Enabled="False">0</asp:TextBox></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="arrawtotaldiscdtl" runat="server" CssClass="inpTextDisabled" Width="100px" Visible="False" Enabled="False">0</asp:TextBox> <asp:TextBox id="arrawmstres3" runat="server" CssClass="inpText" Width="100px" Visible="False" MaxLength="100"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblPOHdrDisc" runat="server" Text="Outstanding SO Disc." Visible="False"></asp:Label></TD><TD class="Label" align=center><asp:Label id="SeptPOHdrDisc" runat="server" Text=":" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="sorawmstdiscvalue" runat="server" CssClass="inpTextDisabled" Width="100px" Visible="False" Enabled="False">0</asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label21" runat="server" Text="Delivery Cost" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="arrawdeliverycost" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" Visible="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left rowSpan=1></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="arrawmstdisctype" runat="server" CssClass="inpText" Width="80px" AutoPostBack="True" Visible="False"><asp:ListItem Value="P">Percentage</asp:ListItem>
<asp:ListItem Value="A">Amount</asp:ListItem>
</asp:DropDownList><asp:TextBox id="arrawmstdiscvalue" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" Visible="False" MaxLength="18">0</asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label27" runat="server" Text="Header Disc. Amt" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="arrawmstdiscamt" runat="server" CssClass="inpTextDisabled" Width="100px" Visible="False" Enabled="False">0</asp:TextBox></TD></TR><TR><TD class="Label" align=left rowSpan=1><asp:Label id="Label28" runat="server" Text="Total Disc. Amt" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD><TD class="Label" align=left><asp:TextBox id="arrawtaxamt" runat="server" CssClass="inpTextDisabled" Width="100px" Visible="False" Enabled="False"></asp:TextBox></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="arrawgrandtotal" runat="server" CssClass="inpTextDisabled" Width="100px" Visible="False" Enabled="False">0</asp:TextBox></TD></TR><TR><TD class="Label" align=left rowSpan=1><asp:Label id="Label22" runat="server" Text="Total Cost" Visible="False"></asp:Label> <asp:Label id="Label30" runat="server" Text="Other Cost" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="arrawtotalcost" runat="server" CssClass="inpTextDisabled" Width="100px" Visible="False" Enabled="False">0</asp:TextBox> <asp:TextBox id="arrawothercost" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" Visible="False"></asp:TextBox></TD><TD class="Label" align=left><ajaxToolkit:MaskedEditExtender id="meeDC" runat="server" TargetControlID="arrawdeliverycost" Mask="999,999,999.99" MaskType="Number" InputDirection="RightToLeft"></ajaxToolkit:MaskedEditExtender></TD><TD class="Label" align=center></TD><TD class="Label" align=left><ajaxToolkit:MaskedEditExtender id="meeOC" runat="server" TargetControlID="arrawothercost" Mask="999,999,999.99" MaskType="Number" InputDirection="RightToLeft"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6 rowSpan=1></TD></TR><TR><TD class="Label" align=left><asp:Label id="i_u2" runat="server" Text="New Detail" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="arrawdtlseq" runat="server" Visible="False">1</asp:Label> <asp:Label id="arrawdtloid" runat="server" Visible="False"></asp:Label> <asp:Label id="matrawoid" runat="server" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:Label id="shipmentrawmstoid" runat="server" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="shipmentrawdtloid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label9" runat="server" Text="Shipment No." Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="shipmentrawno" runat="server" CssClass="inpTextDisabled" Width="150px" Visible="False" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchDO" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearDO" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left>&nbsp;</TD></TR><TR><TD id="TD1" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label11" runat="server" Text="Raw Material"></asp:Label></TD><TD id="TD9" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD5" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="matrawlongdesc" runat="server" CssClass="inpTextDisabled" Width="250px" Enabled="False"></asp:TextBox></TD><TD id="TD13" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label14" runat="server" Text="Quantity"></asp:Label></TD><TD id="TD21" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD20" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="arrawqty" runat="server" CssClass="inpTextDisabled" Width="98px" Enabled="False"></asp:TextBox> <asp:Label id="unit" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD id="TD2" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label32" runat="server" Text="Price Per Unit"></asp:Label></TD><TD id="TD10" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD6" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="arrawprice" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" MaxLength="18"></asp:TextBox></TD><TD id="TD14" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label36" runat="server" Text="Detail Amount"></asp:Label></TD><TD id="TD22" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD19" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="arrawdtlamt" runat="server" CssClass="inpTextDisabled" Width="98px" Enabled="False"></asp:TextBox></TD></TR><TR><TD id="TD3" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label37" runat="server" Text="Detail Disc"></asp:Label></TD><TD id="TD11" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD7" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="arrawdtldisctype" runat="server" CssClass="inpText" Width="80px" AutoPostBack="True"><asp:ListItem Value="P">Percentage</asp:ListItem>
<asp:ListItem Value="A">Amount</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="arrawdtldiscvalue" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" MaxLength="18"></asp:TextBox></TD><TD id="TD15" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label38" runat="server" Text="Detail Disc Amount"></asp:Label></TD><TD id="TD23" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD18" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="arrawdtldiscamt" runat="server" CssClass="inpTextDisabled" Width="98px" Enabled="False"></asp:TextBox></TD></TR><TR><TD id="TD4" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label2" runat="server" Text="Detail Netto"></asp:Label></TD><TD id="TD12" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD8" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="arrawdtlnetto" runat="server" CssClass="inpTextDisabled" Width="98px" Enabled="False"></asp:TextBox></TD><TD id="TD16" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label15" runat="server" Text="Detail Note"></asp:Label></TD><TD id="TD24" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD17" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="arrawdtlnote" runat="server" CssClass="inpText" Width="200px" MaxLength="100"></asp:TextBox></TD></TR><TR><TD id="TD25" class="Label" align=left runat="server" Visible="false"><ajaxToolkit:FilteredTextBoxExtender id="ftbPrice" runat="server" TargetControlID="arrawprice" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender></TD><TD id="TD26" class="Label" align=center runat="server" Visible="false"></TD><TD id="TD27" class="Label" align=left runat="server" Visible="false"><asp:CheckBox id="chkUpdPrice" runat="server" Font-Bold="True" Text="Update Price for All same Raw Material" Checked="True"></asp:CheckBox></TD><TD id="TD28" class="Label" align=left runat="server" Visible="false"><ajaxToolkit:FilteredTextBoxExtender id="ftbDiscDtl" runat="server" TargetControlID="arrawdtldiscvalue" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender></TD><TD id="TD29" class="Label" align=center runat="server" Visible="false"></TD><TD id="TD30" class="Label" align=left runat="server" Visible="false"><asp:CheckBox id="chkUpdDisc" runat="server" Font-Bold="True" Text="Update Discount for All same Raw Material" Checked="True"></asp:CheckBox></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=6>&nbsp;<asp:Label id="Label7" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Invoicel Detail" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:GridView id="gvTblDtl" runat="server" Width="100%" DataKeyNames="trnjualdtlseq" PageSize="5" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
<RowStyle BackColor="#E3EAEB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="sono" HeaderText="SO No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dono" HeaderText="DO No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dodate" HeaderText="DO Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="price" HeaderText="Price">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="disc1" HeaderText="Disc.  1">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="disc2" HeaderText="Disc.  2">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="disc3" HeaderText="Disc. 3">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="taxdtlamt" HeaderText="Tax Amt" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="netto" HeaderText="Total  Amt">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#666666" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#7C6F57"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=6></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update&nbsp;By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton> <asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" Visible="False" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton id="btnPostikng" onclick="btnPostikng_Click" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton id="btnSendApproval" runat="server" ImageUrl="~/Images/sendapproval.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnPrint2" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnUpdate" onclick="btnUpdate_Click" runat="server" ImageUrl="~/Images/Update.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint2"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Sales Invoice :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
        <tr>
            <td align="left">
    <asp:UpdatePanel id="upListCustSO" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListCustSO" runat="server" CssClass="modalBox" Width="700px" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListCustSO" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Customer"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListCustSO" runat="server" Width="100%" DefaultButton="btnFindCust">Filter : <asp:DropDownList id="DDLFilterCust" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="s.custcode">Code</asp:ListItem>
<asp:ListItem Value="s.custname">Name</asp:ListItem>
<asp:ListItem Value="s.custaddr">Address</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterCust" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindCust" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewAllCust" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; BORDER-BOTTOM-STYLE: none" id="Fieldset1"><DIV id="Div1"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="gvCust" runat="server" ForeColor="#333333" Width="96%" GridLines="None" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="custoid,custname,custpaymentoid,custtaxable">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="custcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListCustSO" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE><ajaxToolkit:ModalPopupExtender id="mpeListCustSO" runat="server" TargetControlID="btnHidCustSO" Drag="True" PopupControlID="pnlListCustSO" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListCustSO"></ajaxToolkit:ModalPopupExtender></asp:Panel> <asp:Button id="btnHidCustSO" runat="server" Visible="False"></asp:Button>
</ContentTemplate>
    </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td align="left">
    <asp:UpdatePanel id="upListSO" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListSO" runat="server" CssClass="modalBox" Width="800px" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSO" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Sales Order - Raw Material"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListSO" runat="server" Width="100%" DefaultButton="btnFindListSO">Filter : <asp:DropDownList id="FilterDDLListSO" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="som.sorawno">SO No.</asp:ListItem>
<asp:ListItem Value="g.gendesc">Payment Type</asp:ListItem>
<asp:ListItem Value="som.sorawmstnote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListSO" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListSO" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListSO" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; BORDER-BOTTOM-STYLE: none" id="Fieldset3"><DIV id="Div3"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="gvListSO" runat="server" ForeColor="#333333" Width="96%" DataKeyNames="sorawmstoid" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="sorawno" HeaderText="SO No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sorawdate" HeaderText="SO ETD">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sorawpaytype" HeaderText="Payment Type">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sorawmstnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListSO" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE><ajaxToolkit:ModalPopupExtender id="mpeListSO" runat="server" TargetControlID="btnHideListSO" PopupDragHandleControlID="lblListSO" BackgroundCssClass="modalBackground" PopupControlID="pnlListSO" Drag="True"></ajaxToolkit:ModalPopupExtender></asp:Panel> <asp:Button id="btnHideListSO" runat="server" Visible="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:UpdatePanel ID="upListDO" runat="server">
                    <ContentTemplate>
<asp:Panel id="pnlListDO" runat="server" CssClass="modalBox" Width="800px" DefaultButton="btnFindListDO" Visible="False">
                            <table width="100%">
                                <tbody>
                                    <tr>
                                        <td align="center" class="Label" colspan="3">
                                            <asp:Label ID="lblListDO" runat="server" Font-Bold="True" Font-Size="Medium" Text="List of Shipment Finish Good"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td align="center" class="Label" colspan="3">
                                            Filter :
                                            <asp:DropDownList ID="FilterDDLListDO" runat="server" CssClass="inpText" Width="100px">
                                                <asp:ListItem Value="shipmentrawno">Shipment No</asp:ListItem>
                                                <asp:ListItem Value="dorawno">DO No.</asp:ListItem>
                                                <asp:ListItem Value="dorawcustpono">Cust PO No.</asp:ListItem>
                                                <asp:ListItem Value="shipmentrawmstnote">Note</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox ID="FilterTextListDO" runat="server" CssClass="inpText" Width="200px"></asp:TextBox>
                                            <asp:ImageButton ID="btnFindListDO" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/btnfind.bmp" />
                                            <asp:ImageButton ID="btnAllListDO" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewall.png" /></td>
                                    </tr>
                                    <tr>
                                        <td align="center" class="Label" colspan="3">
                                            <asp:GridView ID="gvListDO" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                CellPadding="4" DataKeyNames="shipmentrawmstoid,shipmentrawno,shipmentrawdate"
                                                GridLines="None" PageSize="5" Width="99%">
                                                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                                <Columns>
                                                    <asp:CommandField ShowSelectButton="True">
                                                        <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:CommandField>
                                                    <asp:BoundField DataField="shipmentrawno" HeaderText="Shipment No.">
                                                        <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="shipmentrawdate" HeaderText="Date">
                                                        <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="dorawno" HeaderText="DO No.">
                                                        <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="dorawcustpono" HeaderText="Cust PO No.">
                                                        <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="shipmentrawmstnote" HeaderText="Note">
                                                        <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                </Columns>
                                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle CssClass="gvpopup" Font-Bold="True" ForeColor="White" HorizontalAlign="Right" />
                                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                <AlternatingRowStyle BackColor="White" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="3">
                                            <asp:LinkButton ID="lkbCloseListDO" runat="server">[ Cancel & Close ]</asp:LinkButton></td>
                                    </tr>
                                </tbody>
                            </table>
                        </asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListDO" runat="server" TargetControlID="btnHideListDO" PopupDragHandleControlID="lblListDO" BackgroundCssClass="modalBackground" PopupControlID="pnlListDO" Drag="True">
                        </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListDO" runat="server" Visible="False"></asp:Button>
</ContentTemplate>
                </asp:UpdatePanel>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="left">
    <asp:UpdatePanel id="upApproval" runat="server">
        <contenttemplate>
<asp:Panel id="pnlApproval" runat="server" CssClass="modalBox" Width="250px" Visible="False"><TABLE width="100%"><TR><TD align=center colSpan=2><asp:Label id="lblApproval" runat="server" Font-Size="Medium" Font-Bold="True" Text="Choose Rate Type"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" align=center colSpan=2><asp:Label id="lblRptOid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=2><asp:DropDownList id="DDLRateType" runat="server" CssClass="inpText" Width="150px">
                <asp:ListItem>Rate Default</asp:ListItem>
                <asp:ListItem>Rate To IDR</asp:ListItem>
                <asp:ListItem>Rate To USD</asp:ListItem>
            </asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 5px" align=center colSpan=2></TD></TR><TR><TD align=center colSpan=2><asp:ImageButton id="btnContinue" runat="server" ImageUrl="~/Images/continue.png" ImageAlign="AbsBottom" AlternateText="Continue"></asp:ImageButton> <asp:ImageButton id="btnCancelPrint" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeApproval" runat="server" TargetControlID="btnPnlApproval" PopupDragHandleControlID="lblApproval" BackgroundCssClass="modalBackground" PopupControlID="pnlApproval" Drag="True">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnPnlApproval" runat="server" Visible="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td align="left">
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>

