<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnCalculateHPP.aspx.vb" Inherits="Accounting_CalculateHPP" title="" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Calculate HPP" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                    <HeaderTemplate>
                        <strong><span style="font-size: 9pt">
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            List of Calculate HPP :.</span></strong>
                    </HeaderTemplate>
                    <ContentTemplate>
                        <asp:UpdatePanel id="UpdatePanel2" runat="server">
                            <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 8%" class="Label" align=left><asp:Label id="lblFilter" runat="server" Text="Filter"></asp:Label></TD><TD style="WIDTH: 2%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="105px"><asp:ListItem Value="cm.calculatehppmstoid">Draft No.</asp:ListItem>
<asp:ListItem Value="m.itemcode">Code FG</asp:ListItem>
<asp:ListItem Value="bom.bomnote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="250px"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text="Period"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="60px" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label55" runat="server" Text="to"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="60px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="lblLabelPeriod" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status" Visible="False"></asp:CheckBox></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="105px" Visible="False"><asp:ListItem>ACTIVE</asp:ListItem>
<asp:ListItem>INACTIVE</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" TargetControlID="FilterPeriod1" PopupButtonID="imbDate1" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="FilterPeriod1" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" TargetControlID="FilterPeriod2" PopupButtonID="imbDate2" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" TargetControlID="FilterPeriod2" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" onclick="btnSearch_Click" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnAll" onclick="btnAll_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPrintHdr" onclick="btnPrintHdr_Click" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvTRN" runat="server" ForeColor="#333333" Width="100%" DataKeyNames="calculatehppmstoid" PageSize="8" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="calculatehppmstoid" DataNavigateUrlFormatString="~\Transaction\trncalculatehpp.aspx?oid={0}" DataTextField="calculatehppmstoid" HeaderText="Draft No." SortExpression="calculatehppmstoid">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="matcode" HeaderText="Code FG" SortExpression="itemcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemshortdesc" HeaderText="Finish Good" SortExpression="itemshortdesc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="calculatehppdate" HeaderText="Date" SortExpression="calculatedate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemqty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dlcamount" HeaderText="DLC">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ohdamount" HeaderText="OHD">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="calculatehppmststatus" HeaderText="Status" SortExpression="activeflag" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:CheckBox id="cbGVMst" runat="server" __designer:wfdid="w155" Checked='<%# eval("checkvalue") %>'></asp:CheckBox> <asp:Label id="lblOidGVMst" runat="server" Text='<%# eval("calculatehppmstoid") %>' Visible="False" __designer:wfdid="w156"></asp:Label> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=4><asp:UpdateProgress id="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image21" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                            <triggers>
<asp:PostBackTrigger ControlID="btnPrintHdr"></asp:PostBackTrigger>
</triggers>
                        </asp:UpdatePanel>
                    </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Calculate HPP :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                <asp:UpdatePanel id="upReqCancel" runat="server">
                    <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3></TD></TR><TR><TD class="Label" align=left colSpan=3><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 14%" class="Label"></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label"><asp:Label id="matreqmstoid" runat="server" Visible="False" __designer:wfdid="w67"></asp:Label> <asp:Label id="calcdtlseq" runat="server" Visible="False" __designer:wfdid="w68"></asp:Label> <asp:Label id="calculatehppmstoid" runat="server" Visible="False" __designer:wfdid="w69"></asp:Label></TD><TD class="Label"></TD><TD class="Label"></TD><TD class="Label"></TD></TR><TR><TD class="Label"><asp:Label id="Label8" runat="server" Text="Business Unit" Visible="False" __designer:wfdid="w70"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label"><asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpTextDisabled" Visible="False" __designer:wfdid="w71" Enabled="False"></asp:DropDownList> <asp:Label id="i_u2" runat="server" Visible="False" __designer:wfdid="w72"></asp:Label> <asp:Label id="i_u" runat="server" Visible="False" __designer:wfdid="w73"></asp:Label></TD><TD class="Label"></TD><TD class="Label"></TD><TD class="Label"></TD></TR><TR><TD class="Label"><asp:Label id="Label3" runat="server" Text="Date" __designer:wfdid="w74"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label"><asp:TextBox id="tbDate" runat="server" CssClass="inpTextDisabled" Width="75px" ToolTip="(MM/dd/yyyy)" __designer:wfdid="w75" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w76"></asp:ImageButton> <asp:Label id="Label4" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w77"></asp:Label></TD><TD class="Label">&nbsp;</TD><TD class="Label"></TD><TD class="Label"></TD></TR><TR><TD class="Label"><asp:Label id="Label10" runat="server" Text="Finish Good" __designer:wfdid="w78"></asp:Label>&nbsp;<asp:Label id="Label28" runat="server" CssClass="Important" Text="*" __designer:wfdid="w79"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label"><asp:TextBox id="itemshortdesc" runat="server" CssClass="inpTextDisabled" Width="177px" __designer:wfdid="w80" Enabled="False"></asp:TextBox>&nbsp;- <asp:DropDownList id="itemunitoid" runat="server" CssClass="inpText" Width="70px" AutoPostBack="True" OnSelectedIndexChanged="itemunitoid_SelectedIndexChanged" __designer:wfdid="w81"></asp:DropDownList>&nbsp;<asp:ImageButton id="btnSearchItem" onclick="btnSearchItem_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w82"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearItem" onclick="btnClearItem_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w83"></asp:ImageButton> <asp:Label id="itemoid" runat="server" Visible="False" __designer:wfdid="w84"></asp:Label> <asp:Label id="itemcode" runat="server" Visible="False" __designer:wfdid="w85"></asp:Label></TD><TD class="Label"><asp:Label id="Label33" runat="server" Text="Finish Good Qty" __designer:wfdid="w86"></asp:Label>&nbsp;<asp:Label id="Label34" runat="server" CssClass="Important" Text="*" __designer:wfdid="w87"></asp:Label></TD><TD class="Label">:</TD><TD class="Label"><asp:TextBox id="fgqty" runat="server" CssClass="inpText" Width="75px" AutoPostBack="True" __designer:wfdid="w88" OnTextChanged="bomqty_TextChanged" MaxLength="12"></asp:TextBox></TD></TR><TR><TD class="Label"><asp:Label id="Label22" runat="server" Text="Material" __designer:wfdid="w89"></asp:Label>&nbsp;<asp:Label id="Label29" runat="server" CssClass="Important" Text="*" __designer:wfdid="w90"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label"><asp:TextBox id="matshortdesc" runat="server" CssClass="inpTextDisabled" Width="280px" __designer:wfdid="w91" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchMat" onclick="btnSearchMat_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w92"></asp:ImageButton> <asp:Label id="matoid" runat="server" Visible="False" __designer:wfdid="w93"></asp:Label></TD><TD class="Label"><asp:Label id="Label25" runat="server" Text="Mat. Qty" __designer:wfdid="w94"></asp:Label>&nbsp;<asp:Label id="Label30" runat="server" CssClass="Important" Text="*" __designer:wfdid="w95"></asp:Label></TD><TD class="Label">:</TD><TD class="Label"><asp:TextBox id="matqty" runat="server" CssClass="inpText" Width="75px" AutoPostBack="True" __designer:wfdid="w96" OnTextChanged="matqty_TextChanged" MaxLength="6"></asp:TextBox>&nbsp;<asp:DropDownList id="matunitoid" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w97"></asp:DropDownList></TD></TR><TR><TD class="Label"><asp:Label id="Label2" runat="server" Text="DLC" __designer:wfdid="w98"></asp:Label>&nbsp;<asp:Label id="Label5" runat="server" CssClass="Important" Text="*" __designer:wfdid="w99"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label"><asp:TextBox id="dlcamount" runat="server" CssClass="inpText" Width="75px" AutoPostBack="True" __designer:wfdid="w100" OnTextChanged="dlcamount_TextChanged" MaxLength="16"></asp:TextBox>&nbsp;<asp:DropDownList id="dlcunitoid" runat="server" CssClass="inpTextDisabled" Width="70px" __designer:wfdid="w101" Enabled="False"></asp:DropDownList> <asp:Label id="matqty_unitkecil" runat="server" Visible="False" __designer:wfdid="w102"></asp:Label> <asp:Label id="matqty_unitbesar" runat="server" Visible="False" __designer:wfdid="w103"></asp:Label></TD><TD class="Label"><asp:Label id="Label6" runat="server" Text="OHD" __designer:wfdid="w104"></asp:Label>&nbsp;<asp:Label id="Label11" runat="server" CssClass="Important" Text="*" __designer:wfdid="w105"></asp:Label></TD><TD class="Label">:</TD><TD class="Label"><asp:TextBox id="ohdamount" runat="server" CssClass="inpText" Width="75px" AutoPostBack="True" __designer:wfdid="w106" OnTextChanged="ohdamount_TextChanged" MaxLength="16"></asp:TextBox>&nbsp;<asp:DropDownList id="ohdunitoid" runat="server" CssClass="inpTextDisabled" Width="99px" __designer:wfdid="w107" Enabled="False"></asp:DropDownList></TD></TR><TR><TD class="Label"></TD><TD class="Label" align=center></TD><TD class="Label"><ajaxToolkit:CalendarExtender id="ceDate" runat="server" Format="MM/dd/yyyy" PopupButtonID="btnDate" TargetControlID="tbDate" __designer:wfdid="w108">
            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeDate" runat="server" TargetControlID="tbDate" Mask="99/99/9999" MaskType="Date" __designer:wfdid="w109" UserDateFormat="MonthDayYear">
            </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="FteQtyFG" runat="server" TargetControlID="fgqty" __designer:wfdid="w110" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FteQtyMat" runat="server" TargetControlID="matqty" __designer:wfdid="w111" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FteDLC" runat="server" TargetControlID="dlcamount" __designer:wfdid="w112" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FteOHD" runat="server" TargetControlID="ohdamount" __designer:wfdid="w113" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender> </TD><TD class="Label"></TD><TD class="Label"></TD><TD class="Label"></TD></TR><TR><TD class="Label"></TD><TD class="Label" align=center></TD><TD class="Label"><asp:ImageButton id="btnAddToList" onclick="btnAddToList_Click" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w114"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClear" onclick="btnClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w115"></asp:ImageButton></TD><TD class="Label"></TD><TD class="Label"></TD><TD class="Label"></TD></TR></TBODY></TABLE></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Label id="Label9" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Detail Material :" __designer:wfdid="w116"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Panel id="pnlGVDetail" runat="server" CssClass="inpText" Width="100%" Height="180px" __designer:wfdid="w117" ScrollBars="Vertical"><asp:GridView id="GVDtl" runat="server" ForeColor="#333333" Width="98%" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="5" DataKeyNames="calcdtlseq" OnSelectedIndexChanged="GVDtl_SelectedIndexChanged" __designer:wfdid="w118" OnRowDeleting="GVDtl_RowDeleting">
<RowStyle BackColor="#E3EAEB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="calcdtlseq" HeaderText="No.">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="matcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matshortdesc" HeaderText="Material">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matqty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matunit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stockvalue" HeaderText="Value">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#666666" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#7C6F57"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel> </TD></TR><TR><TD class="Label" align=left colSpan=3><asp:ImageButton id="btnShowData" runat="server" ImageUrl="~/Images/calculate.png" ImageAlign="AbsMiddle" __designer:wfdid="w119"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=3><asp:Label id="Label7" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="HPP Calculation Detail :" __designer:wfdid="w120"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=3><asp:Panel id="pnlCalculate" runat="server" CssClass="inpText" Width="100%" Height="250px" __designer:wfdid="w121" ScrollBars="Vertical"><asp:GridView id="gvCalculate" runat="server" ForeColor="#333333" Width="98%" __designer:wfdid="w122" PageSize="5" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#E3EAEB"></RowStyle>
<Columns>
<asp:BoundField DataField="Code_Mat" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Description" HeaderText="Detail">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Qty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Unit_Mat" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Value IDR" HeaderText="Value">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Amount IDR" HeaderText="Total Bahan">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="DL IDR" HeaderText="DLC">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="FOH IDR" HeaderText="OHD">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Confirm Qty" HeaderText="Confirm Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Confirm IDR" HeaderText="Confirm Value">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="hppsatuan" HeaderText="HPP Satuan">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#666666" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#7C6F57"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left colSpan=3>Processed By <asp:Label id="upduser" runat="server" __designer:wfdid="w123"></asp:Label>&nbsp;On <asp:Label id="updtime" runat="server" __designer:wfdid="w124"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:ImageButton id="btnPosting" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w125"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w126"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upReqCancel" __designer:wfdid="w127"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w128"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upListMat" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListMat2" runat="server" CssClass="modalBox" Width="900px" Visible="False" DefaultButton="btnFindListMat2"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat2" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Material Level 5" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat2" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="matcode">Code</asp:ListItem>
<asp:ListItem Value="matshortdesc">Description</asp:ListItem>
<asp:ListItem Value="matunit">Unit</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat2" runat="server" CssClass="inpText" Width="200px"></asp:TextBox> <asp:ImageButton id="btnFindListMat2" onclick="btnFindListMat2_Click" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat2" onclick="btnAllListMat2_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:CheckBox id="cbCat01ListMat" runat="server" Text="Cat. 1 :"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat01ListMat" runat="server" CssClass="inpText" Width="200px" AutoPostBack="True"></asp:DropDownList> <asp:CheckBox id="cbCat02ListMat" runat="server" Text="Cat. 2 :"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat02ListMat" runat="server" CssClass="inpText" Width="200px" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:CheckBox id="cbCat03ListMat" runat="server" Text="Cat. 3 :"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat03ListMat" runat="server" CssClass="inpText" Width="200px" AutoPostBack="True"></asp:DropDownList> <asp:CheckBox id="cbCat04ListMat" runat="server" Text="Cat. 4 :"></asp:CheckBox> <asp:DropDownList id="FilterDDLCat04ListMat" runat="server" CssClass="inpText" Width="200px"></asp:DropDownList></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:UpdateProgress id="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat"><ProgressTemplate>
<TABLE style="WIDTH: 200px"><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple">Load Data, Please Wait ...</TD></TR><TR><TD><asp:Image id="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif"></asp:Image></TD></TR></TABLE>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat2" runat="server" ForeColor="#333333" Width="99%" PageSize="5" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="gvListMat2_PageIndexChanging" OnRowDataBound="gvListMat2_RowDataBound">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbListMat2" runat="server" ToolTip='<%# eval("matoid") %>' __designer:wfdid="w67" Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="matcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matshortdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Qty"><ItemTemplate>
<asp:TextBox id="tbBOMQty" runat="server" CssClass="inpText" Text='<%# eval("matqty") %>' Width="50px" __designer:wfdid="w68" MaxLength="16"></asp:TextBox>&nbsp;<ajaxToolkit:FilteredTextBoxExtender id="ftbQtyGV" runat="server" __designer:wfdid="w69" TargetControlID="tbBOMQty" ValidChars="1234567890,.">
</ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Unit"><ItemTemplate>
<asp:DropDownList id="DDLUnit" runat="server" CssClass="inpText" Width="80px" ToolTip="<%# GetParameterID() %>" __designer:wfdid="w70"></asp:DropDownList> 
</ItemTemplate>

<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 15px" align=center colSpan=3><asp:LinkButton id="lkbAddToList" onclick="lkbAddToList_Click" runat="server">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lkbCloseListMat2" onclick="lkbCloseListMat2_Click" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat2" runat="server" TargetControlID="btnHideListMat2" Drag="True" PopupDragHandleControlID="lblTitleListMat2" BackgroundCssClass="modalBackground" PopupControlID="pnlListMat2"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat2" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upListItem" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListItem" runat="server" CssClass="modalBox" Width="600px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListItem" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Finish Good" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListItem" runat="server" Width="100%" DefaultButton="btnFindListItem">Filter : <asp:DropDownList id="FilterDDLListItem" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="i.itemcode">Code</asp:ListItem>
<asp:ListItem Value="i.itemlongdescription">Description</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListItem" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListItem" onclick="btnFindListItem_Click" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListItem" onclick="btnAllListItem_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD align=center colSpan=3><asp:GridView id="gvListItem" runat="server" ForeColor="#333333" Width="99%" DataKeyNames="itemoid,itemshortdesc,itemcode,itemunitoid" PageSize="8" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" OnSelectedIndexChanged="gvListItem_SelectedIndexChanged" OnPageIndexChanging="gvListItem_PageIndexChanging">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemshortdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle CssClass="gvpopup" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label114" runat="server" Text="No data found !!" CssClass="Important"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListItem" onclick="lkbCloseListItem_Click" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHideListItem" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="mpeListItem" runat="server" TargetControlID="btnHideListItem" Drag="True" PopupDragHandleControlID="lblTitleListItem" BackgroundCssClass="modalBackground" PopupControlID="pnlListItem"></ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>