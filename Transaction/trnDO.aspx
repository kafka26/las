<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnDO.aspx.vb" Inherits="Transaction_DO" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Delivery Order" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Delivery Order :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="Label4" runat="server" Text="Filter"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="dom.domstoid">Draft No.</asp:ListItem>
<asp:ListItem Value="dono">DO No.</asp:ListItem>
<asp:ListItem Value="sono">SO No.</asp:ListItem>
<asp:ListItem Value="custname">Customer</asp:ListItem>
<asp:ListItem Value="domstnote">Note</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="200px"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriod" runat="server" Text="Period"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="60px" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label5" runat="server" Text="to"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="60px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label9" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="100px"><asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Approved</asp:ListItem>
<asp:ListItem>In Approval</asp:ListItem>
<asp:ListItem>Rejected</asp:ListItem>
<asp:ListItem>Revised</asp:ListItem>
<asp:ListItem>Cancel</asp:ListItem>
<asp:ListItem>Closed</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" TargetControlID="FilterPeriod1" Format="MM/dd/yyyy" PopupButtonID="imbDate1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="FilterPeriod1" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" TargetControlID="FilterPeriod2" Format="MM/dd/yyyy" PopupButtonID="imbDate2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" TargetControlID="FilterPeriod2" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:CheckBox id="cbShowPrice" runat="server" Text="Print Show Price" Visible="False"></asp:CheckBox> <asp:LinkButton id="lbInProcess" runat="server" Visible="False"></asp:LinkButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvTRN" runat="server" ForeColor="#333333" Width="100%" AllowSorting="True" AllowPaging="True" DataKeyNames="domstoid" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="8">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="domstoid" DataNavigateUrlFormatString="~\Transaction\trnDO.aspx?oid={0}" DataTextField="domstoid" HeaderText="Draft No." SortExpression="domstoid">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="dono" HeaderText="DO No." SortExpression="dono">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dodate" HeaderText="DO Date" SortExpression="dodate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sono" HeaderText="SO No.">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer" SortExpression="custname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="domststatus" HeaderText="Status" SortExpression="domststatus">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="domstnote" HeaderText="Note" SortExpression="domstnote">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reason" HeaderText="Reason" SortExpression="reason">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbGVMst" runat="server" Checked='<%# eval("checkvalue") %>'></asp:CheckBox> <asp:Label id="lblOidGVMst" runat="server" Text='<%# eval("domstoid") %>' Visible="False"></asp:Label> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=4><asp:UpdateProgress id="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image21" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</triggers>
                           
                           
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label16" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Delivery Order Header" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="i_u" runat="server" CssClass="Important" Text="New Data"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="periodacctg" runat="server" Visible="False"></asp:Label> <asp:Label id="custoid" runat="server" Visible="False"></asp:Label> <asp:Label id="doDueDate" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="sorawmstoid" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text="Currency" Visible="False"></asp:Label> <asp:DropDownList id="curroid" runat="server" CssClass="inpTextDisabled" Width="80px" Visible="False" Enabled="False"></asp:DropDownList> <asp:Label id="Label12" runat="server" Text="Direct Cust PO No." Visible="False"></asp:Label></TD></TR><TR><TD id="TD18" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label2" runat="server" Text="Business Unit" Visible="False"></asp:Label></TD><TD id="TD17" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD16" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpText" Visible="False" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="dorawcustpono" runat="server" CssClass="inpText" Width="200px" Visible="False" MaxLength="100"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblTrnNo" runat="server" Text="Draft No."></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:Label id="dorawmstoid" runat="server"></asp:Label><asp:TextBox id="dorawno" runat="server" CssClass="inpTextDisabled" Width="160px" Visible="False" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label19" runat="server" Text="SO date"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="sodate" runat="server" CssClass="inpTextDisabled" Width="75px" Enabled="False" ToolTip="MM/dd/yyyy"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label22" runat="server" Text="Customer"></asp:Label> <asp:Label id="Label24" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custname" runat="server" CssClass="inpTextDisabled" Width="160px" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSearchCust" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClearCust" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD id="TD11" class="Label" align=left runat="server" Visible="true"><asp:Label id="Label6" runat="server" Text="Plan Delivery Date"></asp:Label></TD><TD id="TD10" class="Label" align=center runat="server" Visible="true">:</TD><TD id="TD12" class="Label" align=left runat="server" Visible="true"><asp:TextBox id="dorawreqdate" runat="server" CssClass="inpText" Width="75px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="btnRSD" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Width="16px" Height="16px"></asp:ImageButton> <asp:Label id="Label10" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" Height="16px"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label23" runat="server" Text="SO No."></asp:Label> <asp:Label id="Label26" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="sorawno" runat="server" CssClass="inpTextDisabled" Width="160px" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSearchSO" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClearSO" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD class="Label" align=left runat="server" Visible="true"><asp:Label id="Label7" runat="server" Text="Delivery Date"></asp:Label> <asp:Label id="Label32" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center runat="server" Visible="true">:</TD><TD class="Label" align=left runat="server" Visible="true"><asp:TextBox id="dorawdate" runat="server" CssClass="inpText" Width="75px" ToolTip="MM/DD/YYYY"></asp:TextBox>&nbsp;<asp:ImageButton id="btnDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Width="16px" Height="16px" __designer:wfdid="w1"></asp:ImageButton>&nbsp;<asp:Label id="Label33" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" Height="16px" __designer:wfdid="w2"></asp:Label></TD></TR><TR><TD id="TD19" class="Label" align=left runat="server" Visible="true"><asp:Label id="Label21" runat="server" Text="Expedisi"></asp:Label></TD><TD id="TD21" class="Label" align=center runat="server" Visible="true">:</TD><TD id="TD20" class="Label" align=left runat="server" Visible="true"><asp:DropDownList id="expedisioid" runat="server" CssClass="inpText" Width="200px"></asp:DropDownList></TD><TD id="TD22" class="Label" align=left rowSpan=1 runat="server" Visible="true"><asp:Label id="Label13" runat="server" Text="No Polisi"></asp:Label></TD><TD id="TD23" class="Label" align=center rowSpan=1 runat="server" Visible="true">:</TD><TD id="TD24" class="Label" align=left rowSpan=1 runat="server" Visible="true"><asp:TextBox id="nopolisi" runat="server" CssClass="inpText" Width="200px" MaxLength="29"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label20" runat="server" Text="Warehouse"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="dowhoid" runat="server" CssClass="inpText" Width="200px"></asp:DropDownList></TD><TD class="Label" align=left rowSpan=1><asp:Label id="lbTotalAmt" runat="server" Text="Total Amount"></asp:Label></TD><TD class="Label" align=center rowSpan=1>:</TD><TD class="Label" align=left rowSpan=1><asp:TextBox id="dorawtotalamt" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD id="TD13" class="Label" align=left runat="server" Visible="true"><asp:Label id="Label8" runat="server" Text="Header Note"></asp:Label></TD><TD id="TD15" class="Label" align=center runat="server" Visible="true">:</TD><TD id="TD14" class="Label" align=left runat="server" Visible="true"><asp:TextBox id="dorawmstnote" runat="server" CssClass="inpText" Width="200px" MaxLength="99"></asp:TextBox></TD><TD class="Label" align=left rowSpan=1><asp:Label id="Label25" runat="server" Text="Status"></asp:Label></TD><TD class="Label" align=center rowSpan=1>:</TD><TD class="Label" align=left rowSpan=1><asp:TextBox id="dorawmststatus" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD><TD class="Label" align=left rowSpan=1><asp:Label id="Label31" runat="server" Text="SO Type" Visible="False"></asp:Label></TD><TD class="Label" align=center rowSpan=1></TD><TD class="Label" align=left rowSpan=1><asp:DropDownList id="sotype" runat="server" CssClass="inpTextDisabled" Width="200px" Visible="False" Enabled="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:CalendarExtender id="ceRSD" runat="server" PopupButtonID="btnRSD" Format="MM/dd/yyyy" TargetControlID="dorawreqdate">
            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeRSD" runat="server" TargetControlID="dorawreqdate" MaskType="Date" Mask="99/99/9999">
            </ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="CeDate" runat="server" PopupButtonID="btnDate" Format="MM/dd/yyyy" TargetControlID="dorawdate" __designer:wfdid="w3"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="MeeDate" runat="server" TargetControlID="dorawdate" MaskType="Date" Mask="99/99/9999" __designer:wfdid="w4"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><asp:Label id="Label17" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Delivery Order Detail" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="i_u2" runat="server" Text="New Detail" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="dorawdtlseq" runat="server" Visible="False">1</asp:Label> <asp:Label id="dorawdtloid" runat="server" Visible="False"></asp:Label> <asp:Label id="dorawallocoid" runat="server" Visible="False"></asp:Label> <asp:Label id="matrawoid" runat="server" Visible="False"></asp:Label> <asp:Label id="oldcode" runat="server" Visible="False"></asp:Label> <asp:Label id="stock" runat="server" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="matrawcode" runat="server" Visible="False"></asp:Label> <asp:Label id="sorawdtloid" runat="server" Visible="False" ToolTip="soitemdtloid"></asp:Label> <asp:Label id="matrawconvertunit" runat="server" Visible="False"></asp:Label> <asp:Label id="dounit2oid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label11" runat="server" Text="Material"></asp:Label> <asp:Label id="Label29" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="matrawlongdesc" runat="server" CssClass="inpTextDisabled" Width="180px" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSearchMat" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="Label28" runat="server" Text="Price"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="stockqty" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox> <asp:TextBox id="dounit2" runat="server" CssClass="inpTextDisabled" Width="75px" Visible="False" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="MaxQty" runat="server" Text="SO Qty"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="sorawqty" runat="server" CssClass="inpTextDisabled" Width="75px" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label27" runat="server" Text="Delivered Qty"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="delivQty" runat="server" CssClass="inpTextDisabled" Width="75px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label14" runat="server" Text="Quantity"></asp:Label> <asp:Label id="Label30" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="dorawqty" runat="server" CssClass="inpText" Width="75px" AutoPostBack="True" MaxLength="18"></asp:TextBox> <asp:DropDownList id="dorawunitoid" runat="server" CssClass="inpTextDisabled" Width="100px" Visible="False" Enabled="False" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label15" runat="server" Text="Detail Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="dorawdtlnote" runat="server" CssClass="inpText" Width="200px" MaxLength="99"></asp:TextBox></TD></TR><TR><TD id="TD1" class="Label" align=left runat="server" Visible="false"><asp:Label id="lbPrice" runat="server" Text="Price"></asp:Label> <asp:Label id="lbWarnPrice" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD id="TD6" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD5" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="dorawprice" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" MaxLength="16"></asp:TextBox></TD><TD id="TD4" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label34" runat="server" Text="Rounding Qty" Visible="False"></asp:Label> <asp:Label id="lbAmount" runat="server" Text="Amount"></asp:Label></TD><TD id="TD3" class="Label" align=center runat="server" Visible="false"></TD><TD id="TD2" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="matrawlimitqty" runat="server" CssClass="inpTextDisabled" Width="100px" Visible="False" Enabled="False"></asp:TextBox> <asp:TextBox id="dorawdtlamt" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD><TD id="TD9" class="Label" align=left runat="server" Visible="true"></TD><TD id="TD8" class="Label" align=center runat="server" Visible="true"></TD><TD id="TD7" class="Label" align=left runat="server" Visible="true"></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="ftbQty" runat="server" TargetControlID="dorawqty" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbPrice" runat="server" TargetControlID="dorawprice" ValidChars="1234567890,.">
        </ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Panel id="Panel2" runat="server" Width="100%" ScrollBars="Vertical">
                <asp:GridView id="gvDtl" runat="server" ForeColor="#333333" Width="98%" DataKeyNames="dodtlseq" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="5">
<RowStyle BackColor="#E3EAEB" Wrap="True"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="dodtlseq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="oldcode" HeaderText="Old Code">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="soqty" HeaderText="SO Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="doqty" HeaderText="DO Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dounit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="doprice" HeaderText="Price" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dodtlamt" HeaderText="Amount" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dodtlnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dounitoid" HeaderText="unitoid" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stockqty" HeaderText="stockqty" Visible="False"></asp:BoundField>
<asp:CommandField DeleteImageUrl="~/Images/del.jpeg" DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#666666" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" Wrap="True"></HeaderStyle>

<EditRowStyle BackColor="#7C6F57"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> 
            </asp:Panel> </TD></TR><TR><TD class="Label" align=left colSpan=6></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left colSpan=6>Created By <asp:Label id="createuser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left colSpan=6>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton> <asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton id="btnPost" runat="server" ImageUrl="~/Images/sendapproval.png" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=6><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center">&nbsp;</DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Delivery Order :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upListCust" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlListCust" runat="server" CssClass="modalBox" Width="650px" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListCust" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Customer"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListCust" runat="server" Width="100%" DefaultButton="btnFindListCust">Filter : <asp:DropDownList id="FilterDDLListCust" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="custname">Name</asp:ListItem>
<asp:ListItem Value="custcode">Code</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListCust" runat="server" CssClass="inpText" Width="200px"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListCust" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListCust" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListCust" runat="server" ForeColor="#333333" Width="99%" DataKeyNames="custoid,custname,curroid" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="5" AllowPaging="True">
                                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True">
                                        <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:CommandField>
                                    <asp:BoundField DataField="custcode" HeaderText="Code">
                                        <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="custname" HeaderText="Name">
                                        <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="currcode" HeaderText="SO Currency">
                                        <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                </Columns>
                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <PagerStyle CssClass="gvpopup" Font-Bold="True" ForeColor="White" HorizontalAlign="Right" />
                                <EmptyDataTemplate>
                                    <asp:Label ID="lblEmptyListPopUp" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label>
                                </EmptyDataTemplate>
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="White" />
                            </asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbCloseListCust" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListCust" runat="server" TargetControlID="btnHideListCust" PopupDragHandleControlID="lblListCust" PopupControlID="pnlListCust" Drag="True" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListCust" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upListSO" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlListSO" runat="server" CssClass="modalBox" Width="650px" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSO" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of SO"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListSO" runat="server" Width="100%" DefaultButton="btnFindListSO">Filter : <asp:DropDownList id="FilterDDLListSO" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="sono">SO No.</asp:ListItem>
<asp:ListItem Value="somstnote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListSO" runat="server" CssClass="inpText" Width="200px"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListSO" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListSO" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListSO" runat="server" ForeColor="#333333" Width="99%" PageSize="5" GridLines="None" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="somstoid,sono,sodate,somstres1,soetd,warehouse_id" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="sono" HeaderText="SO No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sodate" HeaderText="SO Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="somstnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="somstres1" HeaderText="soType" Visible="False"></asp:BoundField>
<asp:BoundField DataField="somstoid" HeaderText="somstoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="soetd" HeaderText="soetd" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                                    <asp:Label ID="lblEmptyListPopUp" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label>
                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbCloseListSO" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSO" runat="server" TargetControlID="btnHideListSO" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlListSO" PopupDragHandleControlID="lblListSO">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListSO" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upListMat" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlListMat" runat="server" CssClass="modalBox" Width="900px" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListMat" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Item"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListMat" runat="server" Width="100%" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblFilterInListMat" runat="server" Text="Filter :"></asp:Label> <asp:DropDownList id="FilterDDLListMat" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="itemdesc">Description</asp:ListItem>
<asp:ListItem Value="itemcode">Code</asp:ListItem>
<asp:ListItem>oldcode</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" onclick="btnAllListMat_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE><asp:ImageButton id="btnSelectAll" onclick="btnSelectAll_Click" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNone" onclick="btnSelectNone_Click" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewChecked" onclick="btnViewChecked_Click" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton><BR /><asp:UpdateProgress id="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat"><ProgressTemplate>
<TABLE style="WIDTH: 200px"><TBODY><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple" align=center>Load Data, Please Wait ...</TD></TR><TR><TD align=center><asp:Image id="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif"></asp:Image></TD></TR></TBODY></TABLE>
</ProgressTemplate>
</asp:UpdateProgress></asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat" runat="server" ForeColor="#333333" Width="99%" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="5">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><HeaderTemplate>
<asp:CheckBox id="cbHdrMat" runat="server" Visible="False" AutoPostBack="True" OnCheckedChanged="cbHdrMat_CheckedChanged"></asp:CheckBox> 
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="cbSelectMat" runat="server" ToolTip='<%# eval("sodtloid") %>' Checked='<%# eval("CheckValue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemoldcode" HeaderText="Old Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stockqty" HeaderText="Stock Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit1" HeaderText="Unit Qty">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stockqtybesar" HeaderText="Stock Qty 3" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit3" HeaderText="Unit Qty 3" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unitoid" HeaderText="Unitoid" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="soqty" HeaderText="SO Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="DO Qty"><ItemTemplate>
<asp:TextBox id="tbMatQty" runat="server" CssClass="inpText" Text='<%# eval("doqty") %>' Width="50px" MaxLength="18"></asp:TextBox><BR /><ajaxToolkit:FilteredTextBoxExtender id="ftbeQty" runat="server" TargetControlID="tbMatQty" ValidChars="1234567890.">
                                            </ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="unitdesc" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Note"><ItemTemplate>
<asp:TextBox id="tbMatNote" runat="server" CssClass="inpText" Text='<%# eval("note") %>' Width="150px" MaxLength="100"></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="price" HeaderText="price" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemoid" HeaderText="itemoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label18" runat="server" ForeColor="Red" Font-Bold="True" Text="Data Not Found..!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbAddListMat" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlListMat" PopupDragHandleControlID="lblListMat">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
        <triggers>
<asp:AsyncPostBackTrigger ControlID="btnFindListMat"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="btnAllListMat"></asp:AsyncPostBackTrigger>
</triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

