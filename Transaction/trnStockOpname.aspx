<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnStockOpname.aspx.vb" Inherits="Transaction_StockOpname" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text=".: Stock Opname" CssClass="Title" Font-Size="16pt"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            &nbsp;<strong><span style="font-size: 9pt">List of Stock Opname :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" __designer:wfdid="w181" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 8%" class="Label" align=left>Filter</TD><TD style="WIDTH: 2%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="105px" __designer:wfdid="w182"><asp:ListItem Value="m.stockopoid">Draft</asp:ListItem>
<asp:ListItem Value="m.stockopno">Stock Opname No</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="250px" __designer:wfdid="w183"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text="Periode" __designer:wfdid="w184"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="60px" __designer:wfdid="w185" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w186"></asp:ImageButton><ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" __designer:wfdid="w187" Format="MM/dd/yyyy" PopupButtonID="imbDate1" TargetControlID="FilterPeriod1"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" __designer:wfdid="w188" TargetControlID="FilterPeriod1" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <asp:Label id="Label5" runat="server" Text="to" __designer:wfdid="w189"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="60px" __designer:wfdid="w190" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w191"></asp:ImageButton> <asp:Label id="lblinfodate" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w192"></asp:Label> <ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" __designer:wfdid="w193" Format="MM/dd/yyyy" PopupButtonID="imbDate2" TargetControlID="FilterPeriod2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" __designer:wfdid="w194" TargetControlID="FilterPeriod2" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status" __designer:wfdid="w195"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="105px" __designer:wfdid="w196"><asp:ListItem>All</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w197"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w198"></asp:ImageButton>&nbsp;</TD></TR><TR><TD class="Label" align=left colSpan=4><asp:LinkButton id="lkbPRInProcess" runat="server" Visible="False" __designer:wfdid="w199"></asp:LinkButton></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:LinkButton id="lkbPRInApproval" runat="server" Visible="False" __designer:wfdid="w200"></asp:LinkButton></TD></TR><TR><TD style="HEIGHT: 5px" id="TD47" class="Label" align=left colSpan=4 runat="server" Visible="false"></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvTRN" runat="server" ForeColor="Transparent" Width="100%" __designer:wfdid="w201" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True" GridLines="None" DataKeyNames="stockopoid,stockopno" PageSize="8" AllowSorting="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="stockopoid" DataNavigateUrlFormatString="~\Transaction\trnStockOpname.aspx?oid={0}" DataTextField="stockopoid" HeaderText="Draft No." SortExpression="stockopoid">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="stockopno" HeaderText="Stock Opname No." SortExpression="stockopno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stockopdate" HeaderText="Date" SortExpression="stockopdate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stockopstatus" HeaderText="Status" SortExpression="stockopstatus">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:CommandField SelectImageUrl="~/Images/print.gif" ShowSelectButton="True" ButtonType="Image">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True" Font-Italic="False" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="Sienna" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="No Data Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Transparent" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w202"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="gvTrn"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD style="WIDTH: 13%" class="Label" align=left><asp:Label id="i_u" runat="server" CssClass="Important" Text="New Data" __designer:wfdid="w267" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=left><ajaxToolkit:CalendarExtender id="ceDate" runat="server" __designer:wfdid="w268" TargetControlID="stockopdate" PopupButtonID="btnDate" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender></TD><TD style="WIDTH: 13%" class="Label" align=left><asp:Label id="Label21" runat="server" Text="Business Unit" Width="83px" __designer:wfdid="w270" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><ajaxToolkit:MaskedEditExtender id="meeDate" runat="server" __designer:wfdid="w269" TargetControlID="stockopdate" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <asp:DropDownList id="busunit" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w272" Visible="False" Enabled="False" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblTrnNo" runat="server" Text="Draft" __designer:wfdid="w273"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label15" runat="server" Text=":" __designer:wfdid="w274"></asp:Label></TD><TD class="Label" align=left><asp:Label id="stockopoid" runat="server" __designer:wfdid="w275"></asp:Label><asp:TextBox id="stockno" runat="server" CssClass="inpTextDisabled" Width="125px" __designer:wfdid="w276" Visible="False" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label9" runat="server" Text="Status" __designer:wfdid="w277" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="stockstatus" runat="server" CssClass="inpTextDisabled" Width="80px" __designer:wfdid="w278" Visible="False" Enabled="False">In Process</asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label12" runat="server" Text="Date" __designer:wfdid="w279"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label16" runat="server" Text=":" __designer:wfdid="w280"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="stockopdate" runat="server" CssClass="inpTextDisabled" Width="80px" __designer:wfdid="w281" AutoPostBack="True" ToolTip="MM/dd/yyyy" ReadOnly="True" OnTextChanged="stockopdate_TextChanged"></asp:TextBox>&nbsp;<asp:ImageButton id="btnDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w282" Visible="False"></asp:ImageButton>&nbsp;<asp:Label id="Label18" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w283"></asp:Label></TD><TD class="Label" align=left><asp:Label id="periodacctg" runat="server" __designer:wfdid="w284" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left runat="server" visible="true"><asp:Label id="Label14" runat="server" Text="Item Type" __designer:wfdid="w288"></asp:Label></TD><TD class="Label" align=left runat="server" visible="true"><asp:Label id="Label17" runat="server" Text=":" __designer:wfdid="w286"></asp:Label></TD><TD class="Label" align=left runat="server" visible="true"><asp:DropDownList id="ddlType" runat="server" CssClass="inpText" Width="155px" __designer:wfdid="w290" AutoPostBack="True" OnSelectedIndexChanged="ddlType_SelectedIndexChanged"></asp:DropDownList></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD id="Td28" class="Label" align=left runat="server" visible="true"><asp:Label id="Label2" runat="server" Text="Warehouse" __designer:wfdid="w285"></asp:Label></TD><TD class="Label" align=left runat="server" visible="true"><asp:Label id="Label22" runat="server" Text=":" __designer:wfdid="w289"></asp:Label></TD><TD id="Td29" class="Label" align=left runat="server" visible="true"><asp:DropDownList id="stockopwarehouseoid" runat="server" CssClass="inpText" Width="155px" __designer:wfdid="w287" AutoPostBack="True" OnSelectedIndexChanged="stockopwarehouseoid_SelectedIndexChanged"></asp:DropDownList></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD id="Td4" class="Label" align=left runat="server" visible="true"><asp:CheckBox id="Cat1CB" runat="server" Text="Category 1" Width="96px" __designer:wfdid="w2"></asp:CheckBox></TD><TD class="Label" align=left runat="server" visible="true"><asp:Label id="Label23" runat="server" Text=":" __designer:wfdid="w292"></asp:Label></TD><TD id="Td6" class="Label" align=left runat="server" visible="true"><asp:DropDownList id="itemcat" runat="server" CssClass="inpText" Width="155px" __designer:wfdid="w293" AutoPostBack="True" OnSelectedIndexChanged="itemcat_SelectedIndexChanged"></asp:DropDownList></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left runat="server" visible="true"><asp:CheckBox id="Cat2CB" runat="server" Text="Category 2" Width="96px" __designer:wfdid="w3"></asp:CheckBox></TD><TD id="Td32" class="Label" align=left runat="server" visible="true"><asp:Label id="Label24" runat="server" Text=":" __designer:wfdid="w295"></asp:Label></TD><TD class="Label" align=left runat="server" visible="true"><asp:DropDownList id="ddlGenCat" runat="server" CssClass="inpText" Width="155px" __designer:wfdid="w296" AutoPostBack="True" OnSelectedIndexChanged="ddlGenCat_SelectedIndexChanged"></asp:DropDownList></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD id="Td1" class="Label" align=left runat="server" visible="true"><asp:CheckBox id="Cat3CB" runat="server" Text="Category 3" Width="96px" __designer:wfdid="w4"></asp:CheckBox></TD><TD id="Td33" class="Label" align=left runat="server" visible="true"><asp:Label id="Label25" runat="server" Text=":" __designer:wfdid="w298"></asp:Label></TD><TD id="Td3" class="Label" align=left runat="server" visible="true"><asp:DropDownList id="ddlGenSubCat" runat="server" CssClass="inpText" Width="155px" __designer:wfdid="w299"></asp:DropDownList></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD id="Td11" class="Label" align=left runat="server" visible="true"><asp:Label id="Label20" runat="server" Text="Filter" Width="32px" __designer:wfdid="w300"></asp:Label></TD><TD id="Td34" class="Label" align=left runat="server" visible="true"><asp:Label id="Label26" runat="server" Text=":" __designer:wfdid="w301"></asp:Label></TD><TD id="Td14" class="Label" align=left runat="server" visible="true"><asp:DropDownList id="ddlFilter" runat="server" CssClass="inpText" Width="155px" __designer:wfdid="w302"><asp:ListItem Value="i.ItemCode">Code</asp:ListItem>
<asp:ListItem Value="i.itemLongDescription">Description</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilter" runat="server" CssClass="inpText" Width="155px" __designer:wfdid="w303" ToolTip="dd/MM/yyyy" MaxLength="50"></asp:TextBox></TD><TD id="Td17" class="Label" align=left runat="server" Visible="true"></TD><TD id="Td19" class="Label" align=center runat="server" Visible="true"></TD><TD id="Td21" class="Label" align=left runat="server" Visible="true"></TD></TR><TR><TD id="Td7" class="Label" align=left runat="server" visible="false"><asp:Label id="Label6" runat="server" Text="Location" __designer:wfdid="w304"></asp:Label></TD><TD id="Td38" class="Label" align=left runat="server" visible="false"></TD><TD id="Td9" class="Label" align=left runat="server" visible="false"><asp:DropDownList id="stockoplocationoid" runat="server" CssClass="inpText" Width="155px" __designer:wfdid="w305" OnSelectedIndexChanged="stockoplocationoid_SelectedIndexChanged"></asp:DropDownList></TD><TD id="TD8" class="Label" align=left runat="server" Visible="false"></TD><TD id="TD2" class="Label" align=center runat="server" Visible="false"></TD><TD id="TD5" class="Label" align=left runat="server" Visible="false"></TD></TR><TR><TD id="Td10" class="Label" align=left runat="server" visible="false"><asp:Label id="Label7" runat="server" Text="Item Group" __designer:wfdid="w306"></asp:Label></TD><TD id="Td39" class="Label" align=left runat="server" visible="false"></TD><TD id="Td12" class="Label" align=left runat="server" visible="false"><asp:DropDownList id="itemgroup" runat="server" CssClass="inpText" Width="155px" __designer:wfdid="w307"><asp:ListItem>All</asp:ListItem>
<asp:ListItem>MATERIAL</asp:ListItem>
<asp:ListItem>FG</asp:ListItem>
<asp:ListItem>WIP</asp:ListItem>
</asp:DropDownList></TD><TD id="TD22" class="Label" align=left runat="server" Visible="false"></TD><TD id="TD20" class="Label" align=center runat="server" Visible="false"></TD><TD id="TD23" class="Label" align=left runat="server" Visible="false"></TD></TR><TR><TD id="Td13" class="Label" align=left runat="server" visible="false"><asp:Label id="Label8" runat="server" Text="Item Type" __designer:wfdid="w308"></asp:Label></TD><TD id="Td40" class="Label" align=left runat="server" visible="false"></TD><TD id="Td15" class="Label" align=left runat="server" visible="false"><asp:DropDownList id="itemtype" runat="server" CssClass="inpText" Width="155px" __designer:wfdid="w309"></asp:DropDownList></TD><TD id="TD36" class="Label" align=left runat="server" Visible="false"></TD><TD id="TD37" class="Label" align=center runat="server" Visible="false"></TD><TD id="TD27" class="Label" align=left runat="server" Visible="false"></TD></TR><TR><TD id="Td16" class="Label" align=left runat="server" visible="false"><asp:Label id="Label10" runat="server" Text="Item Class" __designer:wfdid="w310"></asp:Label></TD><TD id="Td41" class="Label" align=left runat="server" visible="false"></TD><TD id="Td18" class="Label" align=left runat="server" visible="false"><asp:DropDownList id="itemclass" runat="server" CssClass="inpText" Width="155px" __designer:wfdid="w311"></asp:DropDownList></TD><TD id="TD25" class="Label" align=left runat="server" Visible="false"></TD><TD id="TD26" class="Label" align=center runat="server" Visible="false"></TD><TD id="TD24" class="Label" align=left runat="server" Visible="false"></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><asp:ImageButton id="imbFindForm" onclick="imbFindForm_Click" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w314"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w315"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><asp:GridView id="gvStockOpname" runat="server" Width="100%" __designer:wfdid="w316" GridLines="None" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" OnRowDataBound="gvStockOpname_RowDataBound" OnPageIndexChanging="gvStockOpname_PageIndexChanging">
<PagerSettings Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="boxAI" Visible="False"><EditItemTemplate>
<asp:TextBox id="TextBox1" runat="server" __designer:wfdid="w339"></asp:TextBox> 
</EditItemTemplate>
<HeaderTemplate>
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="isApply" runat="server" __designer:wfdid="w240" ToolTip='<%# eval("dtlseq") %>' Checked='<%# eval("isApply") %>'></asp:CheckBox>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="dtlseq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="warehouse" HeaderText="Location">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlocaldesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Qty 1"><ItemTemplate>
&nbsp;<asp:TextBox style="TEXT-ALIGN: right" id="qty1" runat="server" CssClass="inpText" Text='<%# String.Format("{0:#,##0}", Eval("Qty1")) %>' Width="70px" __designer:wfdid="w229" MaxLength="10"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="fteqty1" runat="server" __designer:wfdid="w230" TargetControlID="qty1" Enabled="True" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="unit1" HeaderText="Unit1">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Qty 3"><ItemTemplate>
<asp:TextBox style="TEXT-ALIGN: right" id="qty2" runat="server" CssClass="inpText" Text='<%# String.Format("{0:#,##0}", Eval("Qty2")) %>' Width="70px" __designer:wfdid="w235" MaxLength="10"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="fteqty2" runat="server" __designer:wfdid="w236" TargetControlID="qty2" Enabled="True" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="unit2" HeaderText="Unit 3">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="lblWarning" runat="server" ForeColor="Red" __designer:wfdid="w317"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=6></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w318"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w319"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w320"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w321"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w322" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w323" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w324" AlternateText="Delete"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPosting" onclick="btnPosting_Click" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w325"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPrintHdr" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w326" Visible="False"></asp:ImageButton> </TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w327" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w328"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress></DIV>
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrintHdr"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            &nbsp;<strong><span style="font-size: 9pt">Form Stock Opname :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upPopUpConf" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlPopUpConf" runat="server" CssClass="modalMsgBox" Visible="False">
                <table>
                    <tr>
                        <td colspan="2" style="background-color: #cc0000; text-align: left">
                            <asp:Label ID="lblCaptionConf" runat="server" Font-Bold="True" Font-Size="Small"
                                ForeColor="White"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 10px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Image ID="imIconConf" runat="server" Height="24px" ImageUrl="~/Images/error.jpg"
                                Width="24px" /></td>
                        <td class="Label" style="text-align: left">
                            <asp:Label ID="lblPopUpConf" runat="server" ForeColor="Red"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 10px; text-align: center">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center">
                            &nbsp;<asp:ImageButton ID="imbYesPopUpConf" runat="server" ImageAlign="AbsMiddle"
                                ImageUrl="~/Images/yes.png" />
                            <asp:ImageButton ID="imbNoPopUpConf" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/no.png" /></td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpePopUpConf" runat="server" BackgroundCssClass="modalBackground"
                Drag="True" DropShadow="True" PopupControlID="pnlPopUpConf" PopupDragHandleControlID="lblCaptionConf"
                TargetControlID="bePopUpConf">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="bePopUpConf" runat="server" CausesValidation="False" Visible="False" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

