Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports System.Data.Odbc
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_GiroCancel
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Dim conn2 As New OdbcConnection(ConfigurationSettings.AppSettings("QL_CONN2"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function GetDDLValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
            End If
        Next
        Return sReturn
    End Function

    Private Function GetDDLText(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedItem.Text
            End If
        Next
        Return sReturn
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function GetToolTipValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).ToolTip
            End If
        Next
        Return sReturn
    End Function

    Private Function UpdateCheckedMat(Optional ByVal sInternalInv As String = "", Optional ByVal sInternalInvOid As Integer = 0, Optional ByVal sRefNo As String = "") As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblAR") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblAR")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvPODtl.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvPODtl.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "trnjualmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl = dtView.ToTable
                Session("TblAR") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblDtlPO2") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDtlPO2")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvPODtl2.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvPODtl2.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "nomor='" & cbOid & "'"
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("fakturno") = GetTextBoxValue(row.Cells(9).Controls)
                                    dtView(0)("fakturdate") = GetTextBoxValue(row.Cells(10).Controls)
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl = dtView.ToTable
                Session("TblDtlPO2") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function GenerateSONo(ByVal sDate As String) As String
        Dim sSONo As String = ""
        Dim sMonthRomawi As String = ""
        Select Case Format(CDate(sDate), "MM")
            Case "01"
                sMonthRomawi = "I"
            Case "02"
                sMonthRomawi = "II"
            Case "03"
                sMonthRomawi = "III"
            Case "04"
                sMonthRomawi = "IV"
            Case "05"
                sMonthRomawi = "V"
            Case "06"
                sMonthRomawi = "VI"
            Case "07"
                sMonthRomawi = "VII"
            Case "08"
                sMonthRomawi = "VIII"
            Case "09"
                sMonthRomawi = "IX"
            Case "10"
                sMonthRomawi = "X"
            Case "11"
                sMonthRomawi = "XI"
            Case "12"
                sMonthRomawi = "XII"
        End Select
        Dim sNo As String = Format(CDate(sDate), "yyMM") & "-1.1." '"PMSCTE/" & sMonthRomawi & "/" & Format(CDate(sDate), "yy") & "/"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(soitemno, 3) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnsoitemmst WHERE cmpcode='" & CompnyCode & "' AND soitemno LIKE '" & sNo & "%'"
        xCmd.CommandText = sSql
        sSONo = GenNumberString(sNo, "", xCmd.ExecuteScalar, 3)
        Return sSONo
    End Function

    Private Function GenerateSINo(ByVal sDate As String, ByVal sType As String) As String
        'Dim sSINo As String = ""
        'Dim sMonthRomawi As String = ""
        'Select Case Format(CDate(sDate), "MM")
        '    Case "01"
        '        sMonthRomawi = "I"
        '    Case "02"
        '        sMonthRomawi = "II"
        '    Case "03"
        '        sMonthRomawi = "III"
        '    Case "04"
        '        sMonthRomawi = "IV"
        '    Case "05"
        '        sMonthRomawi = "V"
        '    Case "06"
        '        sMonthRomawi = "VI"
        '    Case "07"
        '        sMonthRomawi = "VII"
        '    Case "08"
        '        sMonthRomawi = "VIII"
        '    Case "09"
        '        sMonthRomawi = "IX"
        '    Case "10"
        '        sMonthRomawi = "X"
        '    Case "11"
        '        sMonthRomawi = "XI"
        '    Case "12"
        '        sMonthRomawi = "XII"
        'End Select
        Dim iLocOrExp As Integer = 1
        If sType <> "BC 3.0" Then
            iLocOrExp = 2
        End If
        Dim sNo As String = Format(CDate(sDate), "yyMM") & "-" & iLocOrExp & ".5."
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(aritemno, 3) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnaritemmst WHERE cmpcode='" & CompnyCode & "' AND aritemno LIKE '" & sNo & "%'"
        xCmd.CommandText = sSql
        GenerateSINo = GenNumberString(sNo, "", xCmd.ExecuteScalar, 3)
        Return GenerateSINo
    End Function

    Private Function GeneratePONo(ByVal sDate As String, ByVal sType As String) As String
        Dim sPONo As String = ""
        Dim sNo As String = "PO" & sType & "/" & Format(CDate(sDate), "yyMM") & "/"
        sSql = "SELECT ISNULL(MAX(ABS(REPLACE(po" & IIf(sType = "MT", "raw", sType) & "no, '" & sNo & "', ''))),0) + 1 FROM QL_trnpo" & IIf(sType = "MT", "raw", sType) & "mst WHERE (cmpcode = '" & CompnyCode & "') AND (po" & IIf(sType = "MT", "raw", sType) & "no LIKE '" & sNo & "%')"
        xCmd.CommandText = sSql
        sPONo = sNo & Format(xCmd.ExecuteScalar, "0000")
        Return sPONo
    End Function

    Private Function GeneratePINo(ByVal sDate As String, ByVal sType As String) As String
        Dim sPINo As String = ""
        Dim sColName As String = "raw"
        If sType = "SP" Then
            sColName = "sp"
        ElseIf sType = "GM" Then
            sColName = "gen"
        End If
        Dim sNo As String = "PI" & sType & "/" & Format(CDate(sDate), "yyMM") & "/"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(ap" & sColName & "no,4) AS INT)),0) FROM QL_trnap" & sColName & "mst WHERE (cmpcode = '" & CompnyCode & "') AND (ap" & sColName & "no LIKE '" & sNo & "%')"
        xCmd.CommandText = sSql
        sPINo = sNo & Format(xCmd.ExecuteScalar + 1, "0000")
        Return sPINo
    End Function

    Private Function GetWhOid(ByVal sCode As String) As Integer
        GetWhOid = 0
        Try
            sSql = "SELECT TOP 1 g.genoid FROM QL_mstgen g WHERE g.gengroup='WAREHOUSE' AND g.genother1=LEFT(" & sCode.Trim & ", 1) ORDER BY g.updtime DESC"
            GetWhOid = GetStrData(sSql)
        Catch ex As Exception
            GetWhOid = 0
        End Try
        Return GetWhOid
    End Function

    Private Function GetWhOidNonMat(ByVal sCode As String) As Integer
        GetWhOidNonMat = 0
        Try
            sSql = "SELECT TOP 1 g.genoid FROM QL_mstgen g WHERE g.gengroup='WAREHOUSE' AND g.genother1='" & sCode & "' ORDER BY g.updtime DESC"
            GetWhOidNonMat = GetStrData(sSql)
        Catch ex As Exception
            GetWhOidNonMat = 0
        End Try
        Return GetWhOidNonMat
    End Function

    Private Function GetMatOid(ByVal sCode As String) As String
        GetMatOid = 0
        Try
            sSql = "SELECT TOP 1 itemoid FROM QL_mstitem WHERE itemcode='" & sCode.Trim & "'"
            GetMatOid = GetStrData(sSql)
        Catch ex As Exception
            GetMatOid = 0
        End Try
        Return GetMatOid
    End Function

    Private Function GetMatUnitOid(ByVal sCode As String) As String
        GetMatUnitOid = 0
        Try
            sSql = "SELECT TOP 1 itemunitoid FROM QL_mstitem WHERE itemcode='" & sCode.Trim & "'"
            GetMatUnitOid = GetStrData(sSql)
        Catch ex As Exception
            GetMatUnitOid = 0
        End Try
        Return GetMatUnitOid
    End Function

    Private Function GetDetailSO(ByVal sNo As String, ByVal sFieldName As String, Optional ByVal sTypeCol As String = "", Optional ByVal custoid As Integer = 0) As String
        Dim sReturn As String = ""
        Try
            sSql = "SELECT TOP 1 " & sFieldName & " FROM QL_trnsoitemmst WHERE soitemno='" & sNo & "' AND soitemflag='PRINT OUT'"
            sReturn = GetStrData(sSql)
            If String.IsNullOrEmpty(sReturn) Then
                If sTypeCol = "Int64" Then
                    sReturn = 0
                    If sFieldName = "consigneeoid" Then
                        sReturn = ToInteger(GetStrData("SELECT TOP 1 custdtloid FROM QL_mstcustdtl WHERE custoid=" & custoid))
                    ElseIf sFieldName = "custdtloid" Then
                        sReturn = ToInteger(GetStrData("SELECT TOP 1 consigneeoid FROM QL_mstconsignee WHERE custoid=" & custoid))
                    ElseIf sFieldName = "soitempaytypeoid" Then
                        sReturn = ToInteger(GetStrData("SELECT TOP 1 custpaymentoid FROM QL_mstcust WHERE custoid=" & custoid))
                    End If
                ElseIf sTypeCol = "Date" Then
                    sReturn = "1/1/1900"
                End If
            End If
        Catch ex As Exception
            If sTypeCol = "Int64" Then
                sReturn = 0
            ElseIf sTypeCol = "Date" Then
                sReturn = "1/1/1900"
            End If
        End Try
        Return sReturn
    End Function

    Private Function GetDetailSODtl(ByVal sNo As String, ByVal sFieldName As String, ByVal sCode As String) As String
        GetDetailSODtl = ""
        Try
            sSql = "SELECT TOP 1 d." & sFieldName & " FROM QL_trnsoitemdtl d INNER JOIN QL_trnsoitemmst m ON m.soitemmstoid=d.soitemmstoid INNER JOIN QL_mstitem i ON i.itemoid=d.itemoid WHERE soitemno='" & sNo & "' AND itemcode='" & sCode & "' AND soitemflag='PRINT OUT'"
            GetDetailSODtl = GetStrData(sSql)
        Catch ex As Exception
            GetDetailSODtl = 0
        End Try
        Return GetDetailSODtl
    End Function

    Private Function CheckShipmentStatus(ByVal sNo As String) As Integer
        CheckShipmentStatus = 0
        Try
            CheckShipmentStatus = GetStrData("SELECT COUNT(*) FROM QL_trnshipmentitemmst WHERE shipmentitemno='" & sNo & "'")
        Catch ex As Exception
            CheckShipmentStatus = 0
        End Try
        Return CheckShipmentStatus
    End Function

    Private Function CheckSOTemp(ByVal sNo As String) As Integer
        CheckSOTemp = 0
        Try
            CheckSOTemp = GetStrData("SELECT COUNT(*) FROM QL_trnsoitemmst WHERE soitemno='" & sNo & "' AND soitemflag='PRINT OUT'")
        Catch ex As Exception
            CheckSOTemp = 0
        End Try
        Return CheckSOTemp
    End Function

    Private Function CheckARTemp(ByVal sNo As String) As Integer
        CheckARTemp = 0
        Try
            CheckARTemp = GetStrData("SELECT COUNT(*) FROM QL_trnaritemdtl ard INNER JOIN QL_trnshipmentitemmst shm ON shm.shipmentitemmstoid=ard.shipmentitemmstoid WHERE shipmentitemno='" & sNo & "'")
        Catch ex As Exception
            CheckARTemp = 0
        End Try
        Return CheckARTemp
    End Function

    Private Function GetDtlDataFromGeneral(ByVal sTableName As String, ByVal sColumnName As String, ByVal sParam As String) As Integer
        GetDtlDataFromGeneral = 0
        Try
            sSql = "SELECT TOP 1 " & sColumnName & " FROM " & sTableName & " WHERE " & sParam
            GetDtlDataFromGeneral = GetStrData(sSql)
        Catch ex As Exception
            GetDtlDataFromGeneral = 0
        End Try
        Return GetDtlDataFromGeneral
    End Function

    Private Function CheckLBMStatus(ByVal sNo As String) As Integer
        CheckLBMStatus = 0
        Try
            CheckLBMStatus = GetStrData("SELECT COUNT(*) FROM QL_trnmrrawmst WHERE mrrawno='" & sNo & "'")
        Catch ex As Exception
            CheckLBMStatus = 0
        End Try
        Return CheckLBMStatus
    End Function

#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindListPO()
       
    End Sub

    Private Sub GetLastClose()
        ' Get Current Stock Period need to be Closed
        DDLPeriod.Items.Clear() : imbClose.Visible = True
        DDLPeriod.Enabled = True : DDLPeriod.CssClass = "inpText"

        Dim sLastClosePeriod As String = ""
        'sSql = "SELECT '201712' periodacctg"
        'sLastClosePeriod = GetStrData(sSql)
        If sLastClosePeriod = "" Then
            sSql = "SELECT MIN(periodacctg) FROM QL_crdmtr WHERE cmpcode='" & CompnyCode & "' AND closedate='01/01/1900'"
            Dim sFirstPeriod As String = "" : sFirstPeriod = GetStrData(sSql)
            If sFirstPeriod = "" Then
				For C1 As Integer = 11 To 12
                    DDLPeriod.Items.Add(New ListItem(MonthName(C1).ToUpper & " " & GetServerTime.Year-1, GetDateToPeriodAcctg(New Date(GetServerTime.Year-1, C1, 1))))
                Next
                For C1 As Integer = 1 To 12
                    DDLPeriod.Items.Add(New ListItem(MonthName(C1).ToUpper & " " & GetServerTime.Year, GetDateToPeriodAcctg(New Date(GetServerTime.Year, C1, 1))))
                Next
                DDLPeriod.SelectedValue = GetDateToPeriodAcctg(GetServerTime)
                imbClose.Visible = False
                'showMessage("Can't find Stock Data to be closed.", 2) : Exit Sub
            Else
                Dim iFirstMonth As Integer = CInt(Right(sFirstPeriod, 2))
                Dim iFirstYear As Integer = CInt(Left(sFirstPeriod, 4))
                DDLPeriod.Items.Add(New ListItem(MonthName(iFirstMonth).ToUpper & " " & iFirstYear, GetDateToPeriodAcctg(New Date(iFirstYear, iFirstMonth, 1))))
            End If
        Else
            Dim iLastMonth As Integer = CInt(Right(sLastClosePeriod, 2))
            Dim iLastYear As Integer = CInt(Left(sLastClosePeriod, 4))
            If iLastMonth = 12 Then
                DDLPeriod.Items.Add(New ListItem(MonthName(1).ToUpper & " " & iLastYear + 1, GetDateToPeriodAcctg(New Date(iLastYear + 1, 1, 1))))
            Else
                DDLPeriod.Items.Add(New ListItem(MonthName(iLastMonth + 1).ToUpper & " " & iLastYear, GetDateToPeriodAcctg(New Date(iLastYear, iLastMonth + 1, 1))))
            End If
        End If
        imbClose.Visible = True
        Session("LastClosingPeriod") = sLastClosePeriod
    End Sub

    Private Sub ViewDetailPR()
        Try
            Dim iMonth, iYear As Integer
            iMonth = CInt(Right(DDLPeriod.SelectedValue, 2))
            iYear = CInt(Left(DDLPeriod.SelectedValue, 4))

            Session("TblAR") = Nothing : gvPODtl.DataSource = Session("TblAR") : gvPODtl.DataBind()
            Session("TblAP") = Nothing : gvPODtl2.DataSource = Session("TblAP") : gvPODtl2.DataBind()
            If FilterDDLType.SelectedValue = "SALES INVOICE" Then
                sSql = "SELECT 0 trndtlseq, arm.trnjualmstoid, div.divname, arm.trnjualno, CONVERT(CHAR(10), arm.receivedate, 101) AS trnjualdate, s.custname, arm.trnjualstatus, arm.trnjualnote, nofakturpajak personname, arm.trnjualamtnetto, dom.dono, som.sono, 'True' AS checkvalue FROM QL_trnjualmst arm INNER JOIN QL_mstcust s ON arm.custoid=s.custoid INNER JOIN QL_mstdivision div ON div.cmpcode=arm.cmpcode INNER JOIN QL_mstperson p on p.salescode=arm.salescode INNER JOIN QL_trndomst dom ON dom.domstoid=arm.domstoid INNER JOIN QL_trnsomst som ON som.somstoid=dom.somstoid WHERE s.activeflag='ACTIVE' AND arm.trnjualmstoid > 0 AND MONTH(arm.receivedate)='" & iMonth & "' AND YEAR(arm.receivedate)='" & iYear & "' AND trnjualstatus<>'In Process' AND trntaxamt>0 ORDER BY CONVERT(DATETIME, arm.receivedate), arm.trnjualmstoid"
                Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_podtl")
                For C1 As Integer = 0 To dt.Rows.Count - 1
                    dt.Rows(C1)("trndtlseq") = C1 + 1
                Next
                Session("TblAR") = dt
                gvPODtl.DataSource = Session("TblAR")
                gvPODtl.DataBind()
            ElseIf FilterDDLType.SelectedValue = "PURCHASE INVOICE" Then
                sSql = "SELECT 'False' CheckValue, 0 trndtlseq, m.trnbelimstoid nomor, m.cmpcode, m.trnbelino trnno, CONVERT(VARCHAR(10), trnbelimasafakturdate, 101) trndate, c.suppname2 trnsuppcustname, '' acctgcode, '' acctgdesc, trnbeliamtnetto trnamount, trnbelimstnote trnnote, periodacctg, '' fakturno, CONVERT(VARCHAR(10), trnbelimasafakturdate, 101) fakturdate, 0 CheckLBM, ISNULL(trnbelimstres3,'') trnbelimstres3 FROM QL_trnbelimst m INNER JOIN QL_mstsupp c ON c.suppoid=m.suppoid WHERE MONTH(trnbelimasafakturdate)='" & iMonth & "' AND YEAR(trnbelimasafakturdate)='" & iYear & "' AND trnbelimststatus IN ('Approved', 'Closed') AND trnbelimsttaxamt>0 AND ISNULL(trnbelimstres3,'')<>'' ORDER BY trnbelimasafakturdate"
                Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_podtl2")
                For C1 As Integer = 0 To dt.Rows.Count - 1
                    dt.Rows(C1)("trndtlseq") = C1 + 1
                Next
                dt.AcceptChanges()
                Session("TblDtlPO2") = dt
                gvPODtl2.DataSource = Session("TblDtlPO2")
                gvPODtl2.DataBind()
            End If
        Catch ex As Exception
            showMessage(ex.Message, 2)
            Exit Sub
        End Try
    End Sub

    Private Sub ReGenerateARNo(ByVal sData As DataTable)
        Dim sNo As String = ""
        Dim objTable As DataTable = SortingDataTable(sData, "tgl_sj, nomor_fp")
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            For C1 As Integer = 0 To objTable.Rows.Count - 1
                sNo = GenerateSINo(objTable.Rows(C1)("tgl_sj").ToString, objTable.Rows(C1)("jenis_dokpab").ToString)
                sSql = "UPDATE QL_trnaritemmst SET aritemno='" & sNo & "' WHERE aritemmstres3='" & objTable.Rows(C1)("no_sj").ToString.Trim & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_trngldtl SET noref='" & sNo & "' WHERE noref='" & objTable.Rows(C1)("no_sj").ToString.Trim & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            Next
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub ReGenerateAPNo(ByVal sData As DataTable, ByVal sType As String)
        Dim sNo As String = "", sColName As String = "raw"
        If sType = "SP" Then
            sColName = "sp"
        ElseIf sType = "GM" Then
            sColName = "gen"
        End If
        Dim objTable As DataTable = SortingDataTable(sData, "tgl_bpb")
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            For C1 As Integer = 0 To objTable.Rows.Count - 1
                sNo = GeneratePINo(objTable.Rows(C1)("tgl_bpb").ToString, sType)
                sSql = "UPDATE QL_trnap" & sColName & "mst SET ap" & sColName & "no='" & sNo & "' WHERE ap" & sColName & "mstres3='" & objTable.Rows(C1)("no_bpb").ToString.Trim & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            Next
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub SaveRecordAP()
        If UpdateCheckedMat2() Then
            Dim ObjTable As DataTable = Nothing
            Dim dvTableDtl As DataView = Nothing
            Dim sOid As String = ""
            If Not Session("TblDtlPO2") Is Nothing Then
                ObjTable = Session("TblDtlPO2")
                dvTableDtl = ObjTable.DefaultView
                dvTableDtl.RowFilter = "CheckValue='True'"
                For C1 As Integer = 0 To dvTableDtl.Count - 1
                    sOid &= dvTableDtl(C1)("nomor") & ", "
                Next
                If sOid <> "" Then
                    sOid = Left(sOid, sOid.Length - 2)
                End If
            End If
            Dim iMonth, iYear As Integer
            iMonth = CInt(Right(DDLPeriod.SelectedValue, 2))
            iYear = CInt(Left(DDLPeriod.SelectedValue, 4))
            
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                sSql = "DELETE QL_trnbelimst2 WHERE trnbelimstoid IN (" & sOid & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                sSql = "DELETE QL_trnbelidtl2 WHERE trnbelimstoid IN (" & sOid & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "INSERT INTO QL_trnbelimst2 SELECT * FROM QL_trnbelimst x WHERE x.trnbelimstoid IN (" & sOid & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                sSql = "INSERT INTO QL_trnbelidtl2 SELECT * FROM QL_trnbelidtl x WHERE x.trnbelimstoid IN (" & sOid & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                objTrans.Commit() : conn.Close()
            Catch ex As Exception
                objTrans.Rollback() : conn.Close()
                showMessage(ex.ToString & sSql, 1)
                Exit Sub
            End Try
            Session("Success") = "Data " & FilterDDLType.SelectedValue & " have been Generate successfully."
            showMessage(Session("Success"), 3)
        End If
    End Sub

    Private Sub SaveRecordAR()
        If UpdateCheckedMat() Then
            Dim ObjTable As DataTable = Nothing
            Dim dvTableDtl As DataView = Nothing
            Dim sOid As String = ""
            If Not Session("TblAR") Is Nothing Then
                ObjTable = Session("TblAR")
                dvTableDtl = ObjTable.DefaultView
                dvTableDtl.RowFilter = "CheckValue='True'"
                For C1 As Integer = 0 To dvTableDtl.Count - 1
                    sOid &= dvTableDtl(C1)("trnjualmstoid") & ", "
                Next
                If sOid <> "" Then
                    sOid = Left(sOid, sOid.Length - 2)
                End If
            End If
            Dim iMonth, iYear As Integer
            iMonth = CInt(Right(DDLPeriod.SelectedValue, 2))
            iYear = CInt(Left(DDLPeriod.SelectedValue, 4))

            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                sSql = "DELETE QL_trnjualmst2 WHERE trnjualmstoid IN (" & sOid & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                sSql = "DELETE QL_trnjualdtl2 WHERE trnjualmstoid IN (" & sOid & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "INSERT INTO QL_trnjualmst2 SELECT * FROM QL_trnjualmst x WHERE x.trnjualmstoid IN (" & sOid & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                sSql = "INSERT INTO QL_trnjualdtl2 SELECT * FROM QL_trnjualdtl x WHERE x.trnjualmstoid IN (" & sOid & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                objTrans.Commit() : conn.Close()
            Catch ex As Exception
                objTrans.Rollback() : conn.Close()
                showMessage(ex.ToString & sSql, 1)
                Exit Sub
            Finally
                'ReGenerateARNo(dtViewDO.ToTable)
            End Try
            Session("Success") = "Data SALES INVOICE have been Generate successfully."
            showMessage(Session("Success"), 3)
        End If
    End Sub

    Private Sub BindDataInternalInvoice()
        Dim iMonth, iYear As Integer
        iMonth = CInt(Right(DDLPeriod.SelectedValue, 2))
        iYear = CInt(Left(DDLPeriod.SelectedValue, 4))
        sSql = "SELECT jm.cmpcode, c.custname, c.custaddr, jm.trnjualmstoid, jm.trnjualno, CONVERT(VARCHAR(10), jm.trnjualdate, 103) trnjualdate FROM QL_PELITA.dbo.QL_trnjualmst jm INNER JOIN QL_PELITA.dbo.QL_mstcust c ON c.custoid=jm.trncustoid WHERE jm.cmpcode='" & CompnyCode & "' AND MONTH(trnjualdate)='" & iMonth & "' AND YEAR(trnjualdate)='" & iYear & "' AND " & FilterDDLListInv.SelectedValue & " LIKE '%" & FilterTextListInv.Text & "%' AND trnjualstatus IN ('Approved', 'Closed') ORDER BY CAST (trnjualdate AS DATE)"
        Dim objTable As DataTable = cKon.ambiltabel(sSql, "QL_trnjualmst")
        Session("TblInv") = objTable
        gvListInv.DataSource = Session("TblInv")
        gvListInv.DataBind()
    End Sub

    Private Sub SelectOrDeleteInv(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs, ByVal status As String, ByVal nomor As String)
        If status = "Sel" Then
            If UpdateCheckedMat(gvListInv.SelectedDataKey.Item("trnjualno").ToString, nomor) Then
                If Not Session("TblDtlPO") Is Nothing Then
                    gvPODtl.DataSource = Session("TblDtlPO")
                    gvPODtl.DataBind()
                End If
            End If
            'nomor = "" : gvPODtl.SelectedIndex = -1
            cProc.SetModalPopUpExtender(btnHideListInv, PanelInv, MpeListInv, False)
        Else
            If UpdateCheckedMat("", nomor) Then
                If Not Session("TblDtlPO") Is Nothing Then
                    gvPODtl.DataSource = Session("TblDtlPO")
                    gvPODtl.DataBind()
                End If
            End If
            'nomor.Text = "" : gvPODtl.SelectedIndex = -1
        End If
    End Sub

#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("CompnyCode") = cmpcode
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnTransferTransaction.aspx")
        End If
        If checkPagePermission("~\Transaction\trnTransferTransaction.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Transfer Transaction"
        imbClose.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to EXPORT this data?');")
        If Not Page.IsPostBack Then
            upduser.Text = Session("UserID") : updtime.Text = GetServerTime().ToString
            FilterPeriode1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriode2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            GetLastClose()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("Success") Is Nothing And Session("Success") <> "" Then
            Response.Redirect("~\Transaction\trnTransferTransaction.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnFindListPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListPO.Click
        BindListPO()
        mpeListPO.Show()
    End Sub

    Protected Sub btnViewAllListPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListPO.Click
        FilterTextListPO.Text = ""
        FilterDDLListPO.SelectedIndex = -1
        BindListPO()
        mpeListPO.Show()
    End Sub

    Protected Sub gvListPO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListPO.SelectedIndexChanged

    End Sub

    Protected Sub lkbListPO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListPO.Click
        cProc.SetModalPopUpExtender(btnHiddenListPO, PanelListPO, mpeListPO, False)
    End Sub

    Protected Sub gvPODtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPODtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 2)
        End If
    End Sub

    Protected Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbCancel.Click
        Response.Redirect("~\Transaction\trnTransferTransaction.aspx?awal=true")
    End Sub

    Protected Sub imbClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClose.Click
        If FilterDDLType.SelectedValue = "SALES INVOICE" Then
            SaveRecordAR()
        ElseIf FilterDDLType.SelectedValue = "PURCHASE INVOICE" Then
            SaveRecordAP()
        End If
    End Sub

    Protected Sub FilterDDLGiroType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
       
    End Sub

    Protected Sub btnShowData_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ViewDetailPR()
    End Sub

    Protected Sub gvPODtl2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 2)
        End If
    End Sub

    Protected Sub gvMatUsage_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 2)
        End If
    End Sub

    Protected Sub btnFindListInv_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindDataInternalInvoice()
        MpeListInv.Show()
    End Sub

    Protected Sub btnViewAllListInv_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListInv.SelectedIndex = -1
        FilterTextListInv.Text = ""
        BindDataInternalInvoice()
        MpeListInv.Show()
    End Sub

    Protected Sub lkbListInv_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListInv, PanelInv, MpeListInv, False)
    End Sub

    Protected Sub gvListInv_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If UpdateCheckedMat(gvListInv.SelectedDataKey.Item("trnjualno").ToString, gvListInv.SelectedDataKey.Item("trnjualmstoid").ToString, nomor.Text) Then
            If Not Session("TblDtlPO") Is Nothing Then
                gvPODtl.DataSource = Session("TblDtlPO")
                gvPODtl.DataBind()
            End If
        End If
        nomor.Text = "" : gvPODtl.SelectedIndex = -1
        cProc.SetModalPopUpExtender(btnHideListInv, PanelInv, MpeListInv, False)
    End Sub

    Protected Sub cbHdr_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim bVal As Boolean = sender.Checked
        For C1 As Integer = 0 To gvPODtl.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvPODtl.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = bVal
                    End If
                Next
            End If
        Next
    End Sub

    Protected Sub btnSearhIntInv_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        nomor.Text = sender.ToolTip
        BindDataInternalInvoice()
        cProc.SetModalPopUpExtender(btnHideListInv, PanelInv, MpeListInv, True)
    End Sub

    Protected Sub btnClearIntInv_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If UpdateCheckedMat("", 0, sender.ToolTip) Then
            If Not Session("TblDtlPO") Is Nothing Then
                gvPODtl.DataSource = Session("TblDtlPO")
                gvPODtl.DataBind()
            End If
        End If
        nomor.Text = "" : gvPODtl.SelectedIndex = -1
    End Sub

    Protected Sub cbHdr2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim bVal As Boolean = sender.Checked
        For C1 As Integer = 0 To gvPODtl2.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvPODtl2.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = bVal
                    End If
                Next
            End If
        Next
    End Sub

#End Region

End Class