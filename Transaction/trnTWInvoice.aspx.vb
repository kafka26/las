Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Structure VarUsageValue
    Public Val_IDR As Double
    Public Val_USD As Double
End Structure

Partial Class Transaction_TW
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Dim cRate As New ClassRate
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function GetDropDownListValue(ByVal cc As System.Web.UI.ControlCollection, Optional ByVal sText As String = "") As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                If sText = "" Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
                Else
                    sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedItem.Text
                End If
            End If
        Next
        Return sReturn
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If transfromwhoid.SelectedValue = "" Then
            sError &= "- Please select FROM WH field!<BR>"
        End If
        If transtowhoid.SelectedValue = "" Then
            sError &= "- Please select TO WH field!<BR>"
        End If
        If matoid.Text = "" Then
            sError &= "- Please select MATERIAL field!<BR>"
        End If
        If transqty.Text = "" Then
            sError &= "- Please fill QTY field!<BR>"
        Else
            If ToDouble(transqty.Text) <= 0 Then
                sError &= "- QTY field must be more than 0!<BR>"
            Else
                If ToDouble(transqty.Text) > ToDouble(stockqty.Text) Then
                    sError &= "- QTY field must be less than STOCK QTY!<BR>"
                End If
            End If
        End If
        If transunitoid.SelectedValue = "" Then
            sError &= "- Please select UNIT field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If transdate.Text = "" Then
            sError &= "- Please fill TRANSFER DATE field!<BR>"
        Else
            If Not IsValidDate(transdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- TRANSFER DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If transitemmstaddr.Text.Length > 500 Then
            sError &= "- Addres Max 500 Character!"
        End If
        If custoid.Text = "" Then
            sError &= "- Please select CUSTOMER field!!"
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill Detail Data!<BR>"
        Else
			Dim dtTbl As DataTable = Session("TblDtl")
			If dtTbl.Rows.Count <= 0 Then
				sError &= "- Please fill Detail Data!<BR>"
			Else
				If sError = "" Then
					For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                        If Not IsStockAvailable(DDLBusUnit.SelectedValue, Format(GetServerTime(), "yyyyMM"), dtTbl.Rows(C1)("matoid"), dtTbl.Rows(C1)("transfromwhoid"), ToDouble((dtTbl.Compute("SUM(transqty)", "matoid=" & dtTbl.Rows(C1)("matoid") & " AND transfromwhoid=" & dtTbl.Rows(C1)("transfromwhoid") & "")).ToString), "") Then
                            sError &= "- Transfer Qty for Item!<BR><B><STRONG>" & dtTbl.Rows(C1).Item("matlongdesc").ToString & "</STRONG></B> must less or equal than stock QTY !<BR>"
                            Exit For
                        End If
					Next
				End If
			End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            transmststatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Public Function GetParameterID() As String
        Return Eval("matoid") & "," & Eval("selectedunitoid")
    End Function

    Private Function GetStockValue(ByVal sOid As String, Optional ByVal sType As String = "IDR") As Double
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        Try
            GetStockValue = ToDouble(GetStrData("select dbo.getStockValue(" & sOid & ")"))
        Catch ex As Exception
            GetStockValue = 0
        End Try
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trntransitemmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND transitemmststatus='In Process' AND sssflag=1"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnTWInvoice.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lbInProcess.Visible = True
            lbInProcess.Text = "You have " & ToDouble(GetStrData(sSql)) & " In Process Finish Good Transfer data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitAllDDL()
        Dim sError As String = ""
        ' Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE 1=1"
        If Session("oid") Is Nothing Or Session("oid") = "" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If FillDDL(DDLBusUnit, sSql) Then
            InitDDLFromWH()
        End If
        ' Fill DDL Unit
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='UNIT'"
        If Session("oid") Is Nothing Or Session("oid") = "" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
        FillDDL(transunitoid, sSql)
        FillDDL(stockunitoid, sSql)
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='CITY' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDL(transitemmstcityOid, sSql)
        If transitemmstcityOid.Items.Count = 0 Then
            sError = "CITY data is empty. Please input CITY data first or contact your administrator!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            btnSave.Visible = False
            btnposting.Visible = False
        End If
    End Sub

    Private Sub InitDDLTransferTo(ByVal bVal As Boolean)
        If Not bVal Then
            'Fill DDL Supplier
            sSql = "SELECT suppoid, suppname FROM QL_mstsupp WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND suppgroup='Supplier Maklon' ORDER BY suppname"
        Else
            'Fill DDL Customer
            sSql = "SELECT custoid, custname FROM QL_mstcust WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' ORDER BY custname"
        End If
        FillDDLWithAdditionalText(suppoid, sSql, "NONE", 0)
    End Sub

    Private Sub InitDDLFromWH()
        ' Fill DDL Warehouse
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND gengroup='WAREHOUSE' AND genoid > 0"
        If Session("oid") Is Nothing Or Session("oid") = "" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
        sSql &= " order by gendesc"
        If FillDDL(transfromwhoid, sSql) Then
            InitDDLToWH()
        End If
    End Sub

    Private Sub InitDDLToWH()
        ' Fill DDL Warehouse
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND gengroup='WAREHOUSE' AND genoid > 0 AND genoid <> " & transfromwhoid.SelectedValue
        If Session("oid") Is Nothing Or Session("oid") = "" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
        FillDDL(transtowhoid, sSql)
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT * FROM (SELECT tm.cmpcode, 'False' AS checkvalue, divname, transitemmstoid AS transmstoid, transitemno AS transno, CONVERT(VARCHAR(10), transitemdate, 101) AS transdate, transitemdocrefno AS transdocrefno, transitemmststatus AS transmststatus, transitemmstnote AS transmstnote, tm.updtime, tm.createuser, ISNULL(name,'') name FROM QL_trntransitemmst tm INNER JOIN QL_mstdivision div ON div.cmpcode=tm.cmpcode LEFT JOIN (SELECT custoid oid, custname name, 'QL_mstcust' reftype FROM QL_mstcust c UNION ALL SELECT suppoid oid, suppname name, 'QL_mstsupp' reftype FROM QL_mstsupp s) AS c ON c.oid=ISNULL(transitemmstres3,0) AND reftype=(CASE WHEN ISNULL(transitemmstres3, 0)<>0 AND iskonsinyasi='False' THEN 'QL_mstsupp' WHEN ISNULL(transitemmstres3, 0)<>0 AND iskonsinyasi='True' THEN 'QL_mstcust' ELSE '' END) WHERE tm.sssflag=1) AS QL_trntransmst WHERE cmpcode='" & CompnyCode & "'"
        If checkPagePermission("~\Transaction\trnTWInvoice.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND createuser='" & Session("UserID") & "'"
        End If
        sSql &= sSqlPlus & " ORDER BY CAST(transdate AS DATETIME) DESC, transmstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trntransmst")
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvMst.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvMst.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvMst.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "transmstoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub PrintReport()
        Try
            Dim sOid As String = ""
            Dim cekRes As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("transmstoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            If DDLPrintOption.SelectedIndex = 0 Then
                If sOid <> "" Then
                    sOid = Left(sOid, sOid.Length - 1)
                    sSql = "select distinct (case when transitemmstres3='0' then 'None' ELSE 'NotNone' End) res3 from QL_trntransitemmst where cmpcode='" & Session("CompnyCode") & "' and transitemmstoid IN (" & sOid & ")"
                    cekRes = GetStrData(sSql)
                    If cekRes = "None" Then
                        report.Load(Server.MapPath(folderReport & "rptTransferNone.rpt"))
                    Else
                        sSql = "SELECT iskonsinyasi FROM QL_trntransitemmst WHERE transitemmstoid IN (" & sOid & ")"
                        If GetStrData(sSql) = "True" Then
                            report.Load(Server.MapPath(folderReport & "rptDOCons_Trn.rpt"))
                        Else
                            report.Load(Server.MapPath(folderReport & "rptTransfer.rpt"))
                        End If
                    End If
                Else
                    report.Load(Server.MapPath(folderReport & "rptTransfer.rpt"))
                End If
            Else
                report.Load(Server.MapPath(folderReport & "rptARNotaCons.rpt"))
            End If
            If DDLPrintOption.SelectedIndex = 0 Then
                sSql = "SELECT iskonsinyasi FROM QL_trntransitemmst WHERE transitemmstoid IN (" & sOid & ")"
                If GetStrData(sSql) = "False" Then
                    sSql = "SELECT * FROM ( SELECT tm.cmpcode, ( SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=tm.cmpcode) [Business Unit], tm.transitemmstoid [ID Hdr], CAST(tm.transitemmstoid AS VARCHAR(10)) [Draft No.], transitemno [Transfer No.], transitemdate [Transfer Date], transitemdocrefno [Doc. Ref. No.], transitemmstnote [Header Note], transitemmststatus [Status], UPPER(tm.createuser) [Create User], tm.createtime [Create Time], (CASE transitemmststatus WHEN 'In Process' THEN '' ELSE UPPER(tm.upduser) END) [Post User],(CASE transitemmststatus WHEN 'In Process' THEN CAST('01/01/1900' AS DATETIME) ELSE UPPER(tm.updtime) END) [Post Time], transitemdtloid [ID Dtl], transitemdtlseq [No.], g1.gendesc [From WH], g2.gendesc [To WH], m.itemCode [Code], m.itemLongDescription [Description], transitemqty [Qty], g3.gendesc [Unit], transitemdtlnote [Detail Note], suppname [Transfer To], ISNULL(transitemmstaddr,'') [Address], transitemmstcityOid [City Oid], g4.gendesc [City], td.salesprice [value], m.itemoldcode [oldcode] " & _
                                " FROM QL_trntransitemmst tm " & _
                                " INNER JOIN QL_trntransitemdtl td ON td.cmpcode=tm.cmpcode" & _
                                " AND td.transitemmstoid=tm.transitemmstoid" & _
                                " INNER JOIN QL_mstgen g1 ON g1.genoid=transitemfromwhoid " & _
                                " INNER JOIN QL_mstgen g2 ON g2.genoid=transitemtowhoid " & _
                                " INNER JOIN QL_mstitem m ON m.itemoid=td.itemoid " & _
                                " INNER JOIN QL_mstgen g3 ON g3.genoid=transitemunitoid " & _
                                " LEFT JOIN QL_mstsupp sup ON sup.suppoid=ISNULL(transitemmstres3,0) " & _
                                " INNER JOIN QL_mstgen g4 ON g4.gengroup='CITY' " & _
                                " AND g4.genoid=transitemmstcityOid ) AS tblTransfer WHERE"
                    If Session("CompnyCode") <> CompnyCode Then
                        sSql &= " cmpcode='" & Session("CompnyCode") & "'"
                    Else
                        sSql &= " cmpcode LIKE '%'"
                    End If
                    If sOid = "" Then
                        sSql &= " AND " & FilterDDL2.Items(FilterDDL.SelectedIndex).Value & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                        If cbPeriode.Checked Then
                            If IsValidPeriod() Then
                                sSql &= " AND [Transfer Date]>=CAST('" & FilterPeriod1.Text & " 00:00:00' AS DATETIME) AND [Transfer Date]<=CAST('" & FilterPeriod2.Text & " 23:59:59' AS DATETIME)"
                            Else
                                Exit Sub
                            End If
                        End If
                        If cbStatus.Checked Then
                            sSql &= " AND [Status]='" & FilterDDLStatus.SelectedValue & "'"
                        End If
                        If checkPagePermission("~\Transaction\trnTWInvoice.aspx", Session("SpecialAccess")) = False Then
                            sSql &= " AND [Create User]=UPPER('" & Session("UserID") & "')"
                        End If
                    Else
                        sSql &= " AND [ID Hdr] IN (" & sOid & ")"
                    End If
                    Dim dtReport As DataTable = cKon.ambiltabel(sSql, "tblTransfer")
                    report.SetDataSource(dtReport)
                    report.SetParameterValue("MaterialType", "FINISH GOOD")
                    report.SetParameterValue("PrintUserID", Session("UserID"))
                    report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
                Else
                    Dim sWhere As String = " WHERE dom.transitemmstoid IN (" & sOid & ")"
                    report.SetParameterValue("Header", "FINISH GOOD")
                    report.SetParameterValue("sWhere", sWhere)
                    report.SetParameterValue("PrintUserID", Session("UserID"))
                    report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
                End If
            Else
                If sOid <> "" Then
                    sOid = Left(sOid, sOid.Length - 1)
                End If
                Dim sWhere As String = " WHERE arm.transitemmstoid IN (" & sOid & ")"
                report.SetParameterValue("Header", "FINISH GOOD")
                report.SetParameterValue("sWhere", sWhere)
                report.SetParameterValue("PrintUserID", Session("UserID"))
                report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            End If
            cProc.SetDBLogonForReport(report)
            report.PrintOptions.PaperSize = PaperSize.PaperA4
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "MaterialTransferPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
        Response.Redirect("~\Transaction\trnTWInvoice.aspx?awal=true")
    End Sub

    Private Sub SetUsageValue(ByRef objVal As VarUsageValue)
        objVal.Val_IDR = 0
        objVal.Val_USD = 0
        Dim dt As DataTable = Session("TblDtl")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            Dim dValIDR As Double = GetStockValue(dt.Rows(C1)("matoid").ToString)
            Dim dValUSD As Double = GetStockValue(dt.Rows(C1)("matoid").ToString, "USD")
            dt.Rows(C1)("transvalueidr") = dValIDR
            dt.Rows(C1)("transvalueusd") = dValUSD
            objVal.Val_IDR += dValIDR '* ToDouble(dt.Rows(C1)("transqty").ToString)
            objVal.Val_USD += dValUSD '* ToDouble(dt.Rows(C1)("transqty").ToString)
        Next
        Session("TblDtl") = dt
    End Sub

    Private Sub BindMaterialData()
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sSql = "SELECT 'False' AS Checkvalue, 'True' AS enableqty, m.itemoid AS matoid, itemoldcode, CONVERT (DECIMAL (18,2), itempricelist) AS salesprice2, 0.00 AS salesprice, itemcode AS matcode, m.itemLongDescription AS matlongdesc, m.itemUnit1 AS transunitoid, gendesc AS stockunit, (select dbo.getstockqty(m.itemoid, " & transfromwhoid.SelectedValue & ")) AS stockqty, 0.0 AS transqty, '' AS transdtlnote, 0.0 AS ValueIdr, 0.0 AS ValueUsd, m.itemUnit1 AS selectedunitoid, g.gendesc AS selectedunit, (SELECT ISNULL(genother1,'') acctgoid FROM QL_mstgen gx WHERE gx.cmpcode='" & CompnyCode & "' AND gx.gengroup='GROUPITEM' AND gx.gencode=m.itemgroup AND activeflag='ACTIVE') AS stockacctgoid, m.itemunit1 AS stockunitoid, (CASE m.itemGroup When 'RAW' Then 'RAW MATERIAL' When 'FG' Then 'FINISH GOOD' When 'GEN' Then 'GENERAL MATERIAL' When 'WIP' Then 'WIP' End) AS refname FROM QL_mstitem m INNER JOIN QL_mstgen g ON genoid=itemUnit1 WHERE m.cmpcode='" & CompnyCode & "' ORDER BY matcode"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstitem")
        If dt.Rows.Count > 0 Then
            For C1 As Integer = 0 To dt.Rows.Count - 1
                Dim qty As Integer = dt.Rows(C1)("transqty")
                dt.Rows(C1)("transqty") = ToDouble(dt.Rows(C1)("stockqty").ToString)
                dt.Rows(C1)("salesprice") = ToDouble(dt.Rows(C1)("salesprice2").ToString)
                If suppoid.SelectedItem.Text.ToUpper <> "NONE" Then
                    dt.Rows(C1)("salesprice") = "0.00"
                End If
            Next
            dt.AcceptChanges()
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "stockqty>0"
            Session("TblMat") = dv.ToTable
            Session("TblMatView") = dv.ToTable
            gvListMat.DataSource = Session("TblMat")
            gvListMat.DataBind()
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            showMessage("Material data can't be found!", 2)
        End If
    End Sub

    Private Sub UpdateCheckedListMat()
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("transqty") = ToDouble(GetTextBoxValue(row.Cells(6).Controls))
                                    dtView(0)("salesprice") = GetTextBoxValue(row.Cells(8).Controls)
                                    dtView(0)("transdtlnote") = GetTextBoxValue(row.Cells(9).Controls)
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
            End If
        End If
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("transqty") = ToDouble(GetTextBoxValue(row.Cells(6).Controls))
                                    dtView(0)("salesprice") = GetTextBoxValue(row.Cells(8).Controls)
                                    dtView(0)("transdtlnote") = GetTextBoxValue(row.Cells(9).Controls)
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMatView") = dtTbl
            End If
        End If
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("TblTransferDtl")
        dtlTable.Columns.Add("transdtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("transfromwhoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("transfromwh", Type.GetType("System.String"))
        dtlTable.Columns.Add("transtowhoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("transtowh", Type.GetType("System.String"))
        dtlTable.Columns.Add("matoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemoldcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("matlongdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("transqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("stockqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("transunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("transunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("transdtlnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("transvalueidr", Type.GetType("System.Double"))
        dtlTable.Columns.Add("transvalueusd", Type.GetType("System.Double"))
        dtlTable.Columns.Add("refname", Type.GetType("System.String"))
        dtlTable.Columns.Add("lastqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("transitemqty_unitkecil", Type.GetType("System.Double"))
        dtlTable.Columns.Add("transitemqty_unitbesar", Type.GetType("System.Double"))
        dtlTable.Columns.Add("stockunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("stockacctgoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("salesprice", Type.GetType("System.Double"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub ClearDetail()
        transdtlseq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                transdtlseq.Text = objTable.Rows.Count + 1
                EnableHeader(False)
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        i_u2.Text = "New Detail"
        transfromwhoid.SelectedIndex = -1
        transtowhoid.SelectedIndex = -1
        matoid.Text = ""
        matcode.Text = ""
        matlongdesc.Text = ""
        transqty.Text = ""
        stockqty.Text = ""
        transunitoid.SelectedIndex = -1
        transdtlnote.Text = ""
        salesprice.Text = ""
        gvDtl.SelectedIndex = -1
        transfromwhoid.Enabled = True : transfromwhoid.CssClass = "inpText"
        btnSearchMat.Visible = True
        CountHdrAmount()
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal : DDLBusUnit.CssClass = sCss
        cbCons.Enabled = bVal
        suppoid.Enabled = bVal : suppoid.CssClass = sCss
        suppoid2.Enabled = bVal : suppoid2.CssClass = sCss
        btnSearchSupp.Visible = bVal : btnEraseSupp.Visible = bVal
    End Sub

    Private Sub GenerateNo()
        Dim sPrefixCode As String = "SJPROD"
        Dim sNo As String = sPrefixCode & "-" & Format(GetServerTime(), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(transitemno, " & DefaultFormatCounter & ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trntransitemmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND transitemno LIKE '" & sNo & "%'"
        If GetStrData(sSql) = "" Then
            transno.Text = GenNumberString(sNo, "", 1, DefaultFormatCounter)
        Else
            transno.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        End If
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT cmpcode, transitemmstoid, periodacctg, transitemdate, transitemno, transitemdocrefno, transitemmstnote, transitemmststatus, createuser, createtime, upduser, updtime, transitemmstto, transitemmstaddr, transitemmstcityOid, ISNULL(transitemmstres3,0) AS suppoid, iskonsinyasi FROM QL_trntransitemmst WHERE transitemmstoid=" & sOid
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = xreader("cmpcode").ToString
                InitDDLFromWH()
                transmstoid.Text = xreader("transitemmstoid").ToString
                periodacctg.Text = xreader("periodacctg").ToString
                transdate.Text = Format(xreader("transitemdate"), "MM/dd/yyyy")
                transno.Text = xreader("transitemno").ToString
                transdocrefno.Text = xreader("transitemdocrefno").ToString
                transitemmstto.Text = xreader("transitemmstto").ToString
                transitemmstaddr.Text = xreader("transitemmstaddr").ToString
                transitemmstcityOid.SelectedValue = xreader("transitemmstcityOid").ToString
                cbCons.Checked = xreader("iskonsinyasi").ToString
                cbCons_CheckedChanged(Nothing, Nothing)
                suppoid.SelectedValue = xreader("suppoid").ToString
                If cbCons.Checked Then
                    suppoid2.Text = GetStrData("SELECT custname FROM QL_mstcust WHERE custoid=" & xreader("suppoid").ToString)
                    custoid.Text = xreader("suppoid").ToString
                End If
                transmstnote.Text = xreader("transitemmstnote").ToString
                transmststatus.Text = xreader("transitemmststatus").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            Exit Sub
            btnSave.Visible = False
            btnDelete.Visible = False
            btnposting.Visible = False
        End Try
        If transmststatus.Text <> "In Process" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnposting.Visible = False
            lblspuno.Text = "Transfer No."
            transmstoid.Visible = False
            transno.Visible = True
            btnAddToList.Visible = False
            gvDtl.Columns(0).Visible = False
            gvDtl.Columns(gvDtl.Columns.Count - 1).Visible = False
        End If
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sSql = "SELECT 0.0 AS lastqty,td.transitemdtlseq AS transdtlseq, td.transitemfromwhoid AS transfromwhoid, g1.gendesc AS transfromwh, td.transitemtowhoid AS transtowhoid, g2.gendesc AS transtowh, m.itemoid AS matoid, salesprice, itemoldcode, m.itemcode AS matcode, m.itemLongDescription AS matlongdesc, td.transitemqty AS transqty, (select dbo.getstockqty(td.itemoid, td.transitemfromwhoid)) AS stockqty, td.transitemunitoid AS transunitoid, g3.gendesc AS transunit, td.transitemdtlnote AS transdtlnote, 0.0 AS transvalueidr, 0.0 AS transvalueusd, (CASE m.itemGroup When 'RAW' Then 'RAW MATERIAL' When 'FG' Then 'FINISH GOOD' When 'GEN' Then 'GENERAL MATERIAL' When 'WIP' Then 'WIP' End) AS refname,td.transitemqty_unitkecil,td.transitemqty_unitbesar, (SELECT ISNULL(genother1,'') acctgoid FROM QL_mstgen gx WHERE gx.cmpcode='" & CompnyCode & "' AND gx.gengroup='GROUPITEM' AND gx.gencode=m.itemgroup AND activeflag='ACTIVE') AS stockacctgoid, m.itemunit1 AS stockunitoid FROM QL_trntransitemdtl td INNER JOIN QL_mstitem m ON m.itemoid=td.itemoid INNER JOIN QL_mstgen g1 ON g1.genoid=transitemfromwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=transitemtowhoid INNER JOIN QL_mstgen g3 ON g3.genoid=transitemunitoid WHERE transitemmstoid=" & sOid & " ORDER BY transdtlseq"
        Session("TblDtl") = cKon.ambiltabel(sSql, "QL_trntransitemdtl")
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        ClearDetail()
        EnableGridDetail()
    End Sub

    Private Sub EnableGridDetail()
        gvDtl.Columns(gvDtl.Columns.Count - 3).Visible = True
        If Not cbCons.Checked Then
            If suppoid.SelectedItem.Text.ToUpper <> "NONE" Then
                gvDtl.Columns(gvDtl.Columns.Count - 3).Visible = False
            End If
        End If
    End Sub

    Private Sub BindDataCustomer()
        sSql = "SELECT custoid, custcode, custname, custaddr FROM QL_mstcust WHERE activeflag='ACTIVE' AND (custcode LIKE '%" & Tchar(suppoid2.Text) & "%' OR custname LIKE '%" & Tchar(suppoid2.Text) & "%' OR custaddr LIKE '%" & Tchar(suppoid2.Text) & "%') ORDER BY custname"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstcust")
        Session("TblCust") = dt
        gvCust.DataSource = dt
        gvCust.DataBind()
        gvCust.Visible = True
    End Sub

    Private Sub CountHdrAmount()
        Dim dTotalAmt As Double = 0
        If Session("TblDtl") IsNot Nothing Then
            Dim objTbl As DataTable = Session("TblDtl")
            If objTbl.Rows.Count > 0 Then
                For C1 As Integer = 0 To objTbl.Rows.Count - 1
                    dTotalAmt += ToDouble(objTbl.Rows(C1)("salesprice")) * ToDouble(objTbl.Rows(C1)("transqty"))
                Next
            End If
        End If
        transtotalamt.Text = ToMaskEdit(Math.Round(dTotalAmt, 0, MidpointRounding.AwayFromZero), 4)
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnTWInvoice.aspx")
        End If
        If checkPagePermission("~\Transaction\trnTWInvoice.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Finish Good Transfer (Invoice)"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnposting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            lblTitleListMat.Text = "List Of Material"
            CheckStatus()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitAllDDL()
            cbCons_CheckedChanged(Nothing, Nothing)
            suppoid_SelectedIndexChanged(Nothing, Nothing)
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                transmstoid.Text = GenerateID("QL_TRNTRANSITEMMST", CompnyCode)
                transdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                transmststatus.Text = "In Process"
                periodacctg.Text = GetDateToPeriodAcctg(CDate(transdate.Text))
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvDtl.DataSource = dt
            gvDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnTWInvoice.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lbInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = 0
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = 1
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND transmststatus='In Process'"
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND CAST(transdate AS DATETIME)>=CAST('" & FilterPeriod1.Text & " 00:00:00' AS DATETIME) AND CAST(transdate AS DATETIME)<=CAST('" & FilterPeriod2.Text & " 23:59:59' AS DATETIME)"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND transmststatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        cbStatus.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        FilterDDLStatus.SelectedIndex = -1
        BindTrnData("")
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        UpdateCheckedValue()
        gvMst.PageIndex = e.NewPageIndex
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
    End Sub

    Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvMst.DataSource = Session("TblMst")
            gvMst.DataBind()
        End If
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        UpdateCheckedValue()
        PrintReport()
    End Sub

    Protected Sub transfromwhoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles transfromwhoid.SelectedIndexChanged
        InitDDLToWH()
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        If transfromwhoid.SelectedValue = "" Then
            showMessage("Please select From WH first!", 2)
            Exit Sub
        End If
        If custoid.Text = "" Then
            showMessage("Please select CUSTOMER first!", 2)
            Exit Sub
        End If
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        Session("TblMat") = Nothing : Session("TblMatView") = Nothing
        gvListMat.DataSource = Nothing : gvListMat.DataBind()
        BindMaterialData()
        'gvListMat.Columns(gvListMat.Columns.Count - 2).Visible = True
        'If suppoid.SelectedItem.Text.ToUpper <> "NONE" Then
        '    gvListMat.Columns(gvListMat.Columns.Count - 2).Visible = False
        'End If
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        UpdateCheckedListMat()
        If Session("TblMat") IsNot Nothing Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        UpdateCheckedListMat()
        FilterTextListMat.Text = ""
        If Session("TblMat") IsNot Nothing Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = ""
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        UpdateCheckedListMat()
        gvListMat.PageIndex = e.NewPageIndex
        gvListMat.DataSource = Session("TblMatView")
        gvListMat.DataBind()
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(6).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 4)
                    End If
                End If
            Next
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            Dim str As String = e.Row.Cells(1).Text
            e.Row.Cells(1).Text = Mid(str, 1, 7) & vbCrLf & Mid(str, 8, 10) & vbCrLf & Mid(str, 18, 5)

            Dim iItemOid As Integer = 0
            cc = e.Row.Cells(0).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    iItemOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                End If
            Next
        End If
    End Sub

    Protected Sub cbHdrListMat_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For C1 As Integer = 0 To gvListMat.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        mpeListMat.Show()
    End Sub

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        UpdateCheckedListMat()
        If Session("TblMat") IsNot Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            Dim iCheck As Integer = dtView.Count
            If iCheck > 0 Then
                dtView.RowFilter = ""
                dtView.RowFilter = "checkvalue='True' AND transqty>0"
                If dtView.Count <> iCheck Then
                    Session("WarningListMat") = "Transfer Qty for every checked material data must be more than 0!"
                    showMessage(Session("WarningListMat"), 2)
                    dtView.RowFilter = ""
                    Exit Sub
                End If
                dtView.RowFilter = ""
                dtView.RowFilter = "checkvalue='True' AND transqty<=stockqty"
                If dtView.Count <> iCheck Then
                    Session("WarningListMat") = "Transfer Qty for every checked material data must be less than Stock Qty!"
                    showMessage(Session("WarningListMat"), 2)
                    dtView.RowFilter = ""
                    Exit Sub
                End If
                dtView.RowFilter = ""
                dtView.RowFilter = "checkvalue='True'"
                If Session("TblDtl") Is Nothing Then
                    CreateTblDetail()
                End If
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                Dim dQty_unitkecil As Double = 0
                Dim dQty_unitbesar As Double = 0
                Dim objRow As DataRow
                Dim counter As Integer = objTable.Rows.Count
                For C1 As Integer = 0 To dtView.Count - 1
                    dv.RowFilter = "transfromwhoid=" & transfromwhoid.SelectedValue & " AND matoid=" & dtView(C1)("matoid")
                    If dv.Count > 0 Then
                        dv(0)("transqty") = ToDouble(dtView(C1)("transqty").ToString)
                        dv(0)("transdtlnote") = dtView(C1)("transdtlnote").ToString
                        dv(0)("salesprice") = dtView(C1)("salesprice").ToString
                    Else
                        counter += 1
                        objRow = objTable.NewRow()
                        objRow("transdtlseq") = counter
                        objRow("transfromwhoid") = transfromwhoid.SelectedValue
                        objRow("transfromwh") = transfromwhoid.SelectedItem.Text
                        objRow("transtowhoid") = transtowhoid.SelectedValue
                        objRow("transtowh") = transtowhoid.SelectedItem.Text
                        objRow("matoid") = dtView(C1)("matoid")
                        objRow("matcode") = dtView(C1)("matcode").ToString
                        objRow("itemoldcode") = dtView(C1)("itemoldcode").ToString
                        objRow("matlongdesc") = dtView(C1)("matlongdesc").ToString
                        objRow("stockqty") = ToDouble(dtView(C1)("stockqty").ToString)
                        objRow("transqty") = ToDouble(dtView(C1)("transqty").ToString)
                        objRow("lastqty") = ToDouble(dtView(C1)("stockqty").ToString) - ToDouble(dtView(C1)("transqty").ToString)
                        objRow("transunitoid") = dtView(C1)("selectedunitoid")
                        objRow("transunit") = dtView(C1)("selectedunit").ToString
                        objRow("transdtlnote") = dtView(C1)("transdtlnote").ToString
                        objRow("transvalueidr") = ToDouble(dtView(C1)("ValueIdr").ToString)
                        objRow("transvalueusd") = ToDouble(dtView(C1)("ValueUsd").ToString)
                        objRow("refname") = dtView(C1)("refname").ToString
                        objRow("stockunitoid") = dtView(C1)("stockunitoid").ToString
                        objRow("stockacctgoid") = dtView(C1)("stockacctgoid").ToString
                        objRow("salesprice") = dtView(C1)("salesprice").ToString
                        objTable.Rows.Add(objRow)
                    End If
                    dv.RowFilter = ""
                Next
                dtView.RowFilter = ""
                Session("TblDtl") = objTable
                gvDtl.DataSource = objTable
                gvDtl.DataBind()
                If cbOpenListMatUsage.Checked Then
                    btnSearchMat_Click(Nothing, Nothing)
                Else
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                End If
                ClearDetail()
                EnableGridDetail()
            Else
                Session("WarningListMat") = "Please select material to add to list!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub lkbSelectAllToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbSelectAllToListListMat.Click
        If Session("TblMatView") IsNot Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matoid=" & dtTbl.Rows(C1)("matoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                lkbAddToListListMat_Click(Nothing, Nothing)
            Else
                Session("WarningListMat") = "Please show Material data first!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Please show Material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            Dim dQty_unitkecil As Double = 0
            Dim dQty_unitbesar As Double = 0
            Dim objTable As DataTable = Session("TblDtl")
            Dim objRow As DataRow = objTable.Rows(transdtlseq.Text - 1)
            objRow.BeginEdit()
            objRow("transtowhoid") = transtowhoid.SelectedValue
            objRow("transtowh") = transtowhoid.SelectedItem.Text
            objRow("transqty") = ToDouble(transqty.Text)
            objRow("salesprice") = ToDouble(salesprice.Text)
            objRow("transdtlnote") = transdtlnote.Text

            objRow.EndEdit()
            Session("TblDtl") = objTable
            gvDtl.DataSource = Session("TblDtl")
            gvDtl.DataBind()
            ClearDetail()
            EnableGridDetail()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 4)
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 4)
        End If
    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDtl.SelectedIndexChanged
        Try
            transdtlseq.Text = gvDtl.SelectedDataKey.Item("transdtlseq").ToString().Trim
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "transdtlseq=" & transdtlseq.Text
                transfromwhoid.SelectedValue = dv(0)("transfromwhoid").ToString
                transfromwhoid_SelectedIndexChanged(Nothing, Nothing)
                transtowhoid.SelectedValue = dv(0)("transtowhoid").ToString
                matoid.Text = dv(0)("matoid").ToString
                matcode.Text = dv(0)("matcode").ToString
                matlongdesc.Text = dv(0)("matlongdesc").ToString
                stockqty.Text = ToMaskEdit(ToDouble(dv(0)("stockqty").ToString), 4)
                transqty.Text = ToMaskEdit(ToDouble(dv(0)("transqty").ToString), 4)
                transunitoid.SelectedValue = dv(0)("transunitoid").ToString
                transdtlnote.Text = dv(0)("transdtlnote").ToString
                stockunitoid.Text = dv(0)("stockunitoid").ToString
                salesprice.Text = ToMaskEdit(ToDouble(dv(0)("salesprice").ToString), 4)

                dv.RowFilter = ""
                transfromwhoid.Enabled = False : transfromwhoid.CssClass = "inpTextDisabled"
                btnSearchMat.Visible = False
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub gvDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("transdtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        ClearDetail()
        EnableGridDetail()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_trntransitemmst WHERE transitemmstoid=" & transmstoid.Text
                If CheckDataExists(sSql) Then
                    transmstoid.Text = GenerateID("QL_TRNTRANSITEMMST", CompnyCode)
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNTRANSITEMMST", "transitemmstoid", transmstoid.Text, "transitemmststatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    transmststatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            transdtloid.Text = GenerateID("QL_TRNTRANSITEMDTL", CompnyCode)
            Dim iConAROid As Int32 = GenerateID("QL_CONAR", CompnyCode)
            Dim conmtroid As Integer = GenerateID("QL_CONSTOCK", CompnyCode)
            Dim crdmatoid As Integer = GenerateID("QL_CRDSTOCK", CompnyCode)
            Dim glmstoid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
            Dim gldtloid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
            Dim iStockValOid As Integer = GenerateID("QL_STOCKVALUE", CompnyCode)
            Dim iPiutangAcctgOid As Integer = GetAcctgOID(GetVarInterface("VAR_AR_OTHER", DDLBusUnit.SelectedValue), CompnyCode)
            Dim iHppAcctgOid As Integer = GetAcctgOID(GetVarInterface("VAR_HPP_OTHER", DDLBusUnit.SelectedValue), CompnyCode)
            Dim iJualAcctgOid As Integer = GetAcctgOID(GetVarInterface("VAR_PENJUALAN_OTHER", DDLBusUnit.SelectedValue), CompnyCode)
            Dim dTotalIDR As Double, dTotalUSD As Double, dGrandTotalAmtIDR As Double = 0
            Dim cRate As New ClassRate
            Dim sTgl As String = Format(GetServerTime(), "MM/dd/yyyy")
            periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
            Dim objValue As VarUsageValue

            Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sTgl))

            If transmststatus.Text = "Post" Then
                If isPeriodClosed(DDLBusUnit.SelectedValue, sPeriod) Then
                    showMessage("Cannot continue this transaction anymore because the period : " & MonthName(CInt(sPeriod.Substring(4, 2))) & " " & sPeriod.Substring(0, 2) & " have been closed.", 3)
                    transmststatus.Text = "In Process"
                    Exit Sub
                End If
                cRate.SetRateValue("IDR", sTgl)
                If cRate.GetRateDailyLastError <> "" Then
                    showMessage(cRate.GetRateDailyLastError, 2)
                    transmststatus.Text = "In Process"
                    Exit Sub
                End If
                If cRate.GetRateMonthlyLastError <> "" Then
                    showMessage(cRate.GetRateMonthlyLastError, 2)
                    transmststatus.Text = "In Process"
                    Exit Sub
                End If
                Dim sVarErr As String = ""
                If Not IsInterfaceExists("VAR_STOCK", DDLBusUnit.SelectedValue) Then
                    sVarErr = "VAR_STOCK"
                End If
                If sVarErr <> "" Then
                    showMessage(GetInterfaceWarning(sVarErr, "posting"), 2)
                    transmststatus.Text = "In Process"
                    Exit Sub
                End If
                GenerateNo()
                SetUsageValue(objValue)
            End If

            ' Define Tabel Strucure utk Auto Jurnal 
            sSql = "SELECT '' tipe, 0 acctgoid, 0.0 debet, 0.0 credit, '' refno, '' note, '' AS glother1 "
            Dim tbPostGL As DataTable = cKon.ambiltabel(sSql, "PostGL")
            tbPostGL.Rows.Clear()
            Dim oRow As DataRow
            Dim dvCek As DataView = tbPostGL.DefaultView

            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trntransitemmst (cmpcode, transitemmstoid, periodacctg, transitemdate, transitemno, transitemdocrefno, transitemmstnote, transitemmststatus, createuser, createtime, upduser, updtime, transitemmstto, transitemmstaddr, transitemmstcityOid, transitemmstres3, iskonsinyasi, sssflag) VALUES ('" & DDLBusUnit.SelectedValue & "', " & transmstoid.Text & ", '" & periodacctg.Text & "', '" & transdate.Text & "', '" & transno.Text & "', '" & Tchar(transdocrefno.Text) & "', '" & Tchar(transmstnote.Text) & "', '" & transmststatus.Text & "', '" & createuser.Text & "', '" & createtime.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Tchar(transitemmstto.Text) & "', '" & Tchar(transitemmstaddr.Text) & "', " & transitemmstcityOid.SelectedValue & ", " & IIf(cbCons.Checked, custoid.Text, suppoid.SelectedValue) & ", '" & cbCons.Checked & "', 1)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & transmstoid.Text & " WHERE tablename='QL_TRNTRANSITEMMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trntransitemmst SET periodacctg='" & periodacctg.Text & "', transitemdate='" & transdate.Text & "', transitemno='" & transno.Text & "', transitemdocrefno='" & Tchar(transdocrefno.Text) & "', transitemmstnote='" & Tchar(transmstnote.Text) & "', transitemmststatus='" & transmststatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, transitemmstto='" & Tchar(transitemmstto.Text) & "', transitemmstaddr='" & Tchar(transitemmstaddr.Text) & "', transitemmstcityOid=" & transitemmstcityOid.SelectedValue & ", transitemmstres3=" & IIf(cbCons.Checked, custoid.Text, suppoid.SelectedValue) & ", iskonsinyasi='" & cbCons.Checked & "', sssflag=1 WHERE transitemmstoid=" & transmstoid.Text
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trntransitemdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND transitemmstoid=" & transmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trntransitemdtl (cmpcode, transitemdtloid, transitemmstoid, transitemdtlseq, transitemfromwhoid, transitemtowhoid, itemoid, transitemqty, transitemunitoid, transitemdtlnote, transitemdtlstatus, upduser, updtime, transitemvalueidr, transitemvalueusd, salesprice) VALUES ('" & CompnyCode & "', " & (C1 + CInt(transdtloid.Text)) & ", " & transmstoid.Text & ", " & C1 + 1 & ", " & objTable.Rows(C1)("transfromwhoid") & ", " & objTable.Rows(C1)("transtowhoid") & ", " & objTable.Rows(C1)("matoid") & ", " & ToDouble(objTable.Rows(C1)("transqty").ToString) & ", " & objTable.Rows(C1)("transunitoid") & ", '" & Tchar(objTable.Rows(C1)("transdtlnote").ToString) & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(C1)("transvalueidr").ToString) & ", " & ToDouble(objTable.Rows(C1)("transvalueusd").ToString) & ", " & ToDouble(objTable.Rows(C1)("salesprice").ToString) & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        ' Hitung Value Item
                        dTotalIDR += ToDouble(objTable.Rows(C1)("transvalueidr").ToString) * ToDouble(objTable.Rows(C1)("transqty").ToString)
                        dTotalUSD += ToDouble(objTable.Rows(C1)("transvalueusd").ToString) * ToDouble(objTable.Rows(C1)("transqty").ToString)
                        dGrandTotalAmtIDR += Math.Round((ToDouble(objTable.Rows(C1)("salesprice").ToString) * ToDouble(objTable.Rows(C1)("transqty").ToString)), 0, MidpointRounding.AwayFromZero)

                        If transmststatus.Text = "Post" Then
                            ' Insert QL_conmat Out
                            sSql = "INSERT INTO QL_constock (cmpcode ,constockoid ,contype ,trndate ,formaction ,formoid ,periodacctg ,refname ,refoid ,mtrlocoid ,qtyin ,qtyout ,amount ,hpp ,typemin ,note ,reason ,createuser ,createtime ,upduser ,updtime ,refno ,deptoid,valueidr,valueusd) VALUES ('" & CompnyCode & "'," & conmtroid & ",'TWOUT','" & sTgl & "','QL_trntransitemdtl','" & transmstoid.Text & "','" & sPeriod & "','" & objTable.Rows(C1)("refname") & "'," & objTable.Rows(C1)("matoid") & "," & objTable.Rows(C1)("transfromwhoid") & ", 0, " & ToDouble(objTable.Rows(C1)("transqty").ToString) & ", 0, 0, -1,'" & transno.Text & " # KE " & objTable.Rows(C1)("transtowh") & "', 'Transfer Warehouse (Invoice)','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & transno.Text & "',0," & ToDouble(objTable.Rows(C1).Item("transvalueidr").ToString) & "," & ToDouble(objTable.Rows(C1).Item("transvalueusd").ToString) & ")"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            conmtroid += 1

                            ' Insert QL_crdstock Out
                            sSql = "UPDATE QL_crdstock SET qtyout = qtyout + " & ToDouble(objTable.Rows(C1)("transqty").ToString) & ", saldoakhir = saldoakhir - " & ToDouble(objTable.Rows(C1)("transqty").ToString) & ",lasttranstype = 'QL_trntransitemdtl', lasttransdate = '" & sTgl & "', upduser = '" & Session("UserID") & "' WHERE cmpcode='" & CompnyCode & "' AND refoid=" & objTable.Rows(C1)("matoid") & " AND mtrlocoid= " & objTable.Rows(C1)("transfromwhoid") & " AND periodacctg='" & sPeriod & "'"
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() <= 0 Then
                                sSql = "INSERT INTO QL_crdstock (cmpcode,crdstockoid,periodacctg,refoid,refname,mtrlocoid,qtyin,qtyout,qtyadjin,qtyadjout,saldoawal,saldoakhir,lasttranstype ,lasttransdate,createuser,createtime,upduser,updtime,closeuser,closingdate) VALUES ('" & CompnyCode & "', " & crdmatoid & ",'" & sPeriod & "'," & objTable.Rows(C1)("matoid") & ",'" & objTable.Rows(C1)("refname") & "', " & objTable.Rows(C1)("transfromwhoid") & ", 0, " & ToDouble(objTable.Rows(C1)("transqty").ToString) & ", 0, 0, 0," & -ToDouble(objTable.Rows(C1)("transqty").ToString) & ",'QL_trntransitemdtl','" & sTgl & "','" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP,'','1/1/1900')"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                crdmatoid += 1

                                sSql = "UPDATE QL_mstoid SET lastoid=" & crdmatoid - 1 & " WHERE tablename='QL_CRDSTOCK' AND cmpcode='" & CompnyCode & "'"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            End If
                        End If

                        dvCek.RowFilter = "acctgoid=" & objTable.Rows(C1).Item("stockacctgoid").ToString & " "
                        If dvCek.Count > 0 Then
                            dvCek(0)("credit") += ToDouble(objTable.Rows(C1).Item("transvalueidr").ToString) * ToDouble(objTable.Rows(C1).Item("transqty").ToString)
                            dvCek(0)("debet") += ToDouble(objTable.Rows(C1).Item("transvalueidr").ToString) * ToDouble(objTable.Rows(C1).Item("transqty").ToString)
                            dvCek.RowFilter = ""
                        Else
                            dvCek.RowFilter = ""

                            oRow = tbPostGL.NewRow
                            oRow("acctgoid") = objTable.Rows(C1).Item("stockacctgoid")
                            oRow("debet") = ToDouble(objTable.Rows(C1).Item("transvalueidr").ToString) * ToDouble(objTable.Rows(C1).Item("transqty").ToString)
                            oRow("credit") = ToDouble(objTable.Rows(C1).Item("transvalueidr").ToString) * ToDouble(objTable.Rows(C1).Item("transqty").ToString)

                            tbPostGL.Rows.Add(oRow)
                        End If
                        tbPostGL.AcceptChanges()
						
						'Insert STockvalue
                        sSql = GetQueryUpdateStockValue(-ToDouble(objTable.Rows(C1)("transqty").ToString), -ToDouble(objTable.Rows(C1)("transqty").ToString), ToDouble(objTable.Rows(C1).Item("transvalueidr").ToString), ToDouble(objTable.Rows(C1).Item("transvalueidr").ToString), "QL_trntransitemdtl", sTgl, Session("UserID"), DDLBusUnit.SelectedValue, sPeriod, objTable.Rows(C1)("matoid"))
                        xCmd.CommandText = sSql
                        If xCmd.ExecuteNonQuery() = 0 Then
                            sSql = GetQueryInsertStockValue(-ToDouble(objTable.Rows(C1)("transqty").ToString), -ToDouble(objTable.Rows(C1)("transqty").ToString), ToDouble(objTable.Rows(C1).Item("transvalueidr").ToString), ToDouble(objTable.Rows(C1).Item("transvalueidr").ToString), "QL_trntransitemdtl", sTgl, Session("UserID"), DDLBusUnit.SelectedValue, sPeriod, objTable.Rows(C1)("matoid"), iStockValOid)
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            iStockValOid += 1
                        End If
                    Next
					
					sSql = "UPDATE QL_mstoid SET lastoid=" & conmtroid - 1 & " WHERE tablename='QL_CONSTOCK' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
					sSql = "UPDATE QL_mstoid SET lastoid=" & iStockValOid - 1 & " WHERE tablename='QL_stockvalue' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
					sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(transdtloid.Text)) & " WHERE tablename='QL_trntransitemdtl' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    If transmststatus.Text = "Post" Then
                        'Insert conar
						dGrandTotalAmtIDR = Math.Round(dGrandTotalAmtIDR, 0, MidpointRounding.AwayFromZero)
                        Dim sDueDate As String = DateAdd(DateInterval.Day, 30, CDate(sTgl))
                        sSql = "INSERT INTO QL_conar (cmpcode, conaroid, reftype, refoid, payrefoid, custoid, acctgoid, trnarstatus, trnartype, trnardate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans, amtbayar, trnarnote, trnarres1, trnarres2, trnarres3, createuser, createtime, upduser, updtime, amttransidr, amtbayaridr, amttransusd, amtbayarusd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iConAROid & ", 'QL_trntransitemmst', " & transmstoid.Text & ", 0, " & custoid.Text & ", " & iPiutangAcctgOid & ", 'Post', 'AR', '" & sTgl & "', '" & sPeriod & "', 0, '1/1/1900', '', 0, '" & sDueDate & "', " & dGrandTotalAmtIDR & ", 0, '', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dGrandTotalAmtIDR * cRate.GetRateMonthlyIDRValue & ", 0, " & dGrandTotalAmtIDR * cRate.GetRateMonthlyUSDValue & ", 0)"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        
                        If dTotalIDR > 0 Or dTotalUSD > 0 Then
                            ' Insert GL MST
                            sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & CompnyCode & "', " & glmstoid & ", '" & sTgl & "', '" & sPeriod & "', 'Transfer WH|No. " & transno.Text & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 0, 0, 1, 1, ISNULL((" & dTotalUSD & "/NULLIF(" & dTotalIDR & ", 0)), 0), ISNULL((" & dTotalUSD & "/NULLIF(" & dTotalIDR & ", 0)), 0))"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            ' Insert GL DTL
                            Dim iSeq As Integer = 1
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & CompnyCode & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & iHppAcctgOid & ", 'D', " & dTotalIDR & ", '" & transno.Text & "', 'Transfer WH|No. " & transno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalIDR * cRate.GetRateMonthlyIDRValue & ", " & dTotalIDR * cRate.GetRateMonthlyUSDValue & ", 'QL_trntransitemmst " & transmstoid.Text & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            gldtloid += 1
                            iSeq += 1
                            For C1 As Integer = 0 To tbPostGL.Rows.Count - 1
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & CompnyCode & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & tbPostGL.Rows(C1)("acctgoid") & ", 'C', " & ToDouble(tbPostGL.Rows(C1)("credit").ToString) & ", '" & transno.Text & "', 'Transfer WH|No. " & transno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(tbPostGL.Rows(C1)("credit").ToString) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(tbPostGL.Rows(C1)("credit").ToString) * cRate.GetRateMonthlyUSDValue & ", 'QL_trntransitemmst " & transmstoid.Text & "')"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                gldtloid += 1
                                iSeq += 1
                            Next
                            glmstoid += 1
                            ' Insert GL MST 2nd
                            sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & CompnyCode & "', " & glmstoid & ", '" & sTgl & "', '" & sPeriod & "', 'Transfer WH|No. " & transno.Text & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 0, 0, 1, 1, ISNULL((" & dTotalUSD & "/NULLIF(" & dTotalIDR & ", 0)), 0), ISNULL((" & dTotalUSD & "/NULLIF(" & dTotalIDR & ", 0)), 0))"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & CompnyCode & "', " & gldtloid & ", 1, " & glmstoid & ", " & iPiutangAcctgOid & ", 'D', " & dGrandTotalAmtIDR & ", '" & transno.Text & "', 'Transfer WH|No. " & transno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dGrandTotalAmtIDR * cRate.GetRateMonthlyIDRValue & ", " & dGrandTotalAmtIDR * cRate.GetRateMonthlyUSDValue & ", 'QL_trntransitemmst " & transmstoid.Text & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            gldtloid += 1

                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & CompnyCode & "', " & gldtloid & ", 2, " & glmstoid & ", " & iJualAcctgOid & ", 'C', " & dGrandTotalAmtIDR & ", '" & transno.Text & "', 'Transfer WH|No. " & transno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dGrandTotalAmtIDR * cRate.GetRateMonthlyIDRValue & ", " & dGrandTotalAmtIDR * cRate.GetRateMonthlyUSDValue & ", 'QL_trntransitemmst " & transmstoid.Text & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            gldtloid += 1


                            sSql = "UPDATE QL_mstoid SET lastoid=" & iConAROid & " WHERE tablename='QL_CONAR' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_mstoid SET lastoid=" & glmstoid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_mstoid SET lastoid=" & gldtloid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        End If
                    End If
                End If
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        transmststatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    transmststatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 2)
                conn.Close()
                transmststatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & transmstoid.Text & ".<BR>"
            End If
            If transmststatus.Text = "Post" Then
                Session("SavedInfo") &= "Data have been posted with Transfer No. = " & transno.Text & "."
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trnTWInvoice.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnTWInvoice.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If transmstoid.Text = "" Then
            showMessage("Please select Finish Good Transfer data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNTRANSITEMMST", "transitemmstoid", transmstoid.Text, "transitemmststatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                transmststatus.Text = "In Process"
                Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE FROM QL_trntransitemdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND transitemmstoid=" & transmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trntransitemmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND transitemmstoid=" & transmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnTWInvoice.aspx?awal=true")
    End Sub

    Protected Sub btnposting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnposting.Click
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        If isPeriodClosed(CompnyCode, CDate(sDate)) Then
            showMessage("Period " & GetDateToPeriodAcctg(CDate(sDate)) & " Have been Already CLOSED, Please CANCEL this transaction and try again!", 2)
            Exit Sub
        End If
        transmststatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLFromWH()
    End Sub

    Protected Sub suppoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not cbCons.Checked Then
            If suppoid.Items.Count > 0 Then
                transitemmstaddr.Text = GetStrData("SELECT suppaddr FROM QL_mstsupp WHERE suppoid=" & suppoid.SelectedValue)
            Else
                transitemmstaddr.Text = ""
            End If
        Else
            If suppoid.Items.Count > 0 Then
                transitemmstaddr.Text = GetStrData("SELECT custaddr FROM QL_mstcust WHERE custoid=" & suppoid.SelectedValue)
            Else
                transitemmstaddr.Text = ""
            End If
        End If
        EnableGridDetail()
    End Sub

    Protected Sub salesprice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Text = ToMaskEdit(ToDouble(salesprice.Text), 4)
    End Sub

    Protected Sub cbCons_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitDDLTransferTo(cbCons.Checked)
        suppoid.Visible = Not cbCons.Checked
        suppoid2.Visible = cbCons.Checked : btnSearchSupp.Visible = cbCons.Checked : btnEraseSupp.Visible = cbCons.Checked
    End Sub

    Protected Sub btnEraseSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        suppoid2.Enabled = True : suppoid2.CssClass = "inpText"
        suppoid2.Text = "" : custoid.Text = "" : transitemmstaddr.Text = ""
        gvCust.DataSource = Nothing : gvCust.DataBind() : gvCust.Visible = False
    End Sub

    Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindDataCustomer()
    End Sub

    Protected Sub gvCust_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        suppoid2.Text = gvCust.SelectedDataKey("custname")
        custoid.Text = gvCust.SelectedDataKey("custoid")
        transitemmstaddr.Text = gvCust.SelectedDataKey("custaddr")
        suppoid2.Enabled = False : suppoid2.CssClass = "inpTextDisabled"
        gvCust.DataSource = Nothing : gvCust.DataBind() : gvCust.Visible = False
    End Sub

    Protected Sub gvCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvCust.PageIndex = e.NewPageIndex
        gvCust.DataSource = Session("TblCust")
        gvCust.DataBind()
    End Sub
#End Region

End Class
