Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Drawing.Printing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_APRawMaterial
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Dim pkSize = New System.Drawing.Printing.PaperSize
    Dim PrintDoc1 As New System.Drawing.Printing.PrintDocument
    Public folderReport As String = "~/Report/"
    Const iRoundDigit = 2
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        'Dim RowNo As GridViewRow = Nothing
        'Dim RowIndex As Integer = gvListReg.SelectedIndex
        'RowNo = gvListReg.Rows(RowIndex)
        'Dim tb As System.Web.UI.WebControls.TextBox = RowNo.FindControl("FakNo")
        Dim sReturn As String = ""
        'If registermstoid.Text = "" Then
        '    sError &= "- Please select REGISTER NO. field!<BR>"
        'End If

        'If tb.Text = "" Then
        '    sError &= "- Please fill NO. FAKTUR field!<BR>"
        'End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetDetailData(ByVal sOid As String) As DataTable
        Dim dtReturn As DataTable = Nothing
        Dim sAdd As String = ""
        If Session("oid") IsNot Nothing And Session("oid") <> "" Then
            sAdd = " AND apm.trnbelimstoid <>" & Session("oid")
        End If
        sSql = "SELECT 'False' AS CheckValue, '' AS fakturno, '' AS fakturpajakno, pod.podtloid registerdtloid, mrm.mrmstoid mrrawmstoid, mrno AS mrrawno, mrdtloid AS mrrawdtloid, mrd.matoid matrawoid,m.itemCode AS matrawcode,m.itemLongDescription AS matrawlongdesc, (mrqty - ISNULL((SELECT SUM(retqty) FROM QL_trnreturdtl pretd INNER JOIN QL_trnreturmst pretm ON pretm.cmpcode=pretd.cmpcode AND pretm.retmstoid=pretd.retmstoid WHERE pretd.cmpcode=mrd.cmpcode AND pretd.refoiddtl=mrd.mrdtloid AND retdtlres2='QL_trnmrdtl' AND retmststatus<>'Rejected'), 0.0))+mrbonusqty AS mrqty, mrqty_unitkecil, mrqty_unitbesar, pounitoid AS piunitoid, g2.gendesc AS piunit, g.genoid AS piunit1oid, g.gendesc AS piunit1, g1.genoid AS piunit2oid, g1.gendesc AS piunit2, pod.poprice AS porawprice, pod.podtldisctype AS porawdtldisctype, (CASE pod.podtldisctype WHEN 'A' THEN (pod.podtldiscvalue / pod.poqty) ELSE pod.podtldiscvalue END) AS porawdtldiscvalue, potaxtype AS porawtaxtype, potaxamt AS porawtaxamt, batchnumber FROM QL_trnpodtl pod INNER JOIN QL_trnmrdtl mrd ON mrd.cmpcode=pod.cmpcode AND mrd.podtloid=pod.podtloid INNER JOIN QL_trnmrmst mrm ON mrm.cmpcode=mrd.cmpcode AND mrm.mrmstoid=mrd.mrmstoid INNER JOIN QL_mstitem m ON m.itemoid=mrd.matoid INNER JOIN QL_mstgen g ON g.genoid=itemUnit1 INNER JOIN QL_mstgen g1 ON g1.genoid=itemUnit3 INNER JOIN QL_mstgen g2 ON g2.genoid=pounitoid INNER JOIN QL_trnpomst pom ON pom.cmpcode=mrm.cmpcode AND pom.pomstoid=mrm.pomstoid WHERE pom.cmpcode='" & DDLBusUnit.SelectedValue & "' AND pom.pomstoid=" & pomstoid.Text & " AND mrm.mrmstoid IN (" & sOid & ") /*AND ISNULL(mrdtlres1, '')='' AND ISNULL(mrmstres1, '')=''*/ AND mrmststatus IN ('Post','Closed') AND mrd.mrdtloid NOT IN (SELECT mrdtloid FROM QL_trnbelidtl2 apd INNER JOIN QL_trnbelimst2 apm ON apm.cmpcode=apd.cmpcode AND apm.trnbelimstoid=apd.trnbelimstoid WHERE apd.cmpcode=mrm.cmpcode AND trnbelimststatus<>'Rejected'" & sAdd & ") /*AND ISNULL(mrdtlstatus,'')=''*/"
        dtReturn = cKon.ambiltabel(sSql, "QL_trnregisterdtl")
        Return dtReturn
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If aprawdate.Text = "" Then
            sError &= "- Please fill A/P DATE field!<BR>"
            sErr = "Empty"
        Else
            If Not IsValidDate(aprawdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- A/P DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If suppoid.Text = "" Then
            sError &= "- Please select SUPPLIER field!<BR>"
        End If
        'If curroid.SelectedValue = "" Then
        '    sError &= "- Please select CURRENCY field!<BR>"
        'End If
        'If rateoid.Text = "" Then
        '    sError &= "- Please define TAX RATE data!<BR>"
        'End If
        'If rate2oid.Text = "" Then
        '    sError &= "- Please define MONTHLY RATE data!<BR>"
        'End If
        Dim sErr2 As String = ""
        If aprawdatetakegiro.Text <> "" Then
            If Not IsValidDate(aprawdatetakegiro.Text, "MM/dd/yyyy", sErr2) Then
                sError &= "- DATE TAKE GIRO is invalid. " & sErr2 & "<BR>"
            Else
                If sErr = "" Then
                    If CDate(aprawdate.Text) > CDate(aprawdatetakegiro.Text) Then
                        sError &= "- DATE TAKE GIRO must be more or equal than A/P DATE!<BR>"
                    End If
                End If
            End If
        End If
        If aprawmststatus.Text = "In Approval" Then
            If Session("TblDtl") Is Nothing Then
                sError &= "- Please fill DETAIL DATA!<BR>"
            Else
                Dim objTbl As DataTable = Session("TblDtl")
                If objTbl.Rows.Count <= 0 Then
                    sError &= "- Please fill DETAIL DATA!<BR>"
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            aprawmststatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function GetNumericValues(ByVal sData As String) As String
        Dim sNumeric As String = ""
        Dim sCurr As String = ""
        For C1 As Integer = 1 To Len(sData)
            sCurr = Mid(sData, C1, 1)
            If IsNumeric(sCurr) Then
                sNumeric &= sCurr
            End If
        Next
        GetNumericValues = sNumeric
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "mrrawmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("fakturno") = GetTextBoxValue(row.Cells(3).Controls)
                                    dtView(0)("fakturpajakno") = GetTextBoxValue(row.Cells(4).Controls)
                                    dtView(0)("aprawdtlnote") = GetTextBoxValue(row.Cells(5).Controls)
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl = dtView.ToTable
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "mrrawmstoid=" & cbOid
                                dtView2.RowFilter = "mrrawmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("fakturno") = GetTextBoxValue(row.Cells(3).Controls)
                                    dtView(0)("fakturpajakno") = GetTextBoxValue(row.Cells(4).Controls)
                                    dtView(0)("aprawdtlnote") = GetTextBoxValue(row.Cells(5).Controls)
                                    If dtView2.Count > 0 Then
                                        dtView(0)("fakturno") = GetTextBoxValue(row.Cells(3).Controls)
                                        dtView(0)("fakturpajakno") = GetTextBoxValue(row.Cells(4).Controls)
                                        dtView(0)("aprawdtlnote") = GetTextBoxValue(row.Cells(5).Controls)
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckAPStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trnaprawmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND aprawmststatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnAP2.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lkbAPInProcess.Visible = True
            lkbAPInProcess.Text = "You have " & GetStrData(sSql) & " In Process A/P Raw Material data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
        sSql = "SELECT COUNT(*) FROM QL_trnaprawmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND aprawmststatus='In Approval'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnAP2.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lkbAPInApproval.Visible = True
            lkbAPInApproval.Text = "You have " & GetStrData(sSql) & " In Approval A/P Raw Material data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitAllDDL()
        'Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)
        'Fill DDL Payment Type
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='PAYMENT TYPE' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDL(aprawpaytypeoid, sSql)
        ' Fill DDL Currency
        sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        FillDDL(curroid, sSql)

        'Fill DDL Unit
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='UNIT' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDL(unitkeciloid, sSql)
        FillDDL(unitbesaroid, sSql)
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT trnbelimstoid aprawmstoid, trnbelino aprawno, CONVERT(VARCHAR(10), trnbelimasafakturdate, 101) AS aprawdate, suppname2 suppname,  trnbelimststatus aprawmststatus, trnbelimstnote aprawmstnote, divname, 'Print' AS printtext, 'rptPrintOutAP' AS rptname, apm.cmpcode, '' AS formtype, 'False' AS checkvalue, case upper(apm.trnbelimststatus) when 'CLOSED' then apm.closereason when 'REJECTED' then rejectreason when 'REVISED' then revisereason else '' end reason, 'Print' AS printtext, 'rptPrintOutAP' AS rptname, apm.cmpcode, 'raw' AS formtype FROM QL_trnbelimst2 apm INNER JOIN QL_mstsupp s ON s.suppoid=apm.suppoid INNER JOIN QL_mstdivision div ON div.cmpcode=apm.cmpcode WHERE apm.cmpcode='" & CompnyCode & "'"
        sSql &= sSqlPlus & " AND trnbelimstoid>0 ORDER BY CONVERT(DATETIME, trnbelimasafakturdate) DESC, trnbelimstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trnaprawmst")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub BindSupplierData()
        Dim sAdd As String = ""
        If Session("oid") IsNot Nothing And Session("oid") <> "" Then
            sAdd = " AND apm.trnbelimstoid<>" & Session("oid")
        End If
        sSql = "SELECT suppoid, suppcode, suppname, suppaddr, supppaymentoid FROM QL_mstsupp WHERE cmpcode='" & CompnyCode & "' AND suppoid IN (SELECT suppoid FROM QL_trnpomst WHERE cmpcode='" & CompnyCode & "' AND pomststatus IN ('Closed', 'Approved') /*AND pomstoid NOT IN (SELECT pomstoid FROM QL_trnbelidtl2 apd INNER JOIN QL_trnbelimst2 apm ON apm.cmpcode=apd.cmpcode AND apm.trnbelimstoid=apd.trnbelimstoid WHERE apd.cmpcode='" & CompnyCode & "' AND trnbelimststatus<>'Rejected'" & sAdd & ")*/ AND pomstoid IN (SELECT mrm.pomstoid FROM QL_trnmrmst mrm WHERE mrm.cmpcode='" & CompnyCode & "' AND ISNULL(mrmstres1, '')='')) AND " & FilterDDLListSupp.SelectedValue & " LIKE '%" & Tchar(FilterTextListSupp.Text) & "%' ORDER BY suppcode"
        Dim objTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstsupp")
        Session("mstSupp") = objTbl
        gvListSupp.DataSource = objTbl
        gvListSupp.DataBind()
    End Sub

    Private Sub BindRegisterData()
        Dim sAdd As String = ""
        If Session("oid") IsNot Nothing And Session("oid") <> "" Then
            sAdd = " AND apm.trnbelimstoid <>" & Session("oid")
        End If
        sSql = "SELECT DISTINCT pom.pomstoid, pom.pono, CONVERT(VARCHAR(10), pom.podate, 101) AS podate, pom.posuppref, (CASE g.gencode WHEN 'RAW' THEN 'RM' WHEN 'GEN' THEN 'GM' ELSE g.gencode END) AS pogroup, pom.pomstnote, pom.curroid, s.suppoid, s.suppname, pom.potype AS poflag, pom.pogroup AS potype, (CASE WHEN pom.potaxtype_inex='INC' THEN 'INCLUDE' WHEN pom.potaxtype_inex='EXC' THEN 'EXCLUDE' ELSE 'NON TAX' END) AS potaxtype, potaxamt, povat, pomstdisctype, pomstdiscvalue, pomstdiscamt FROM QL_trnpomst pom INNER JOIN QL_trnpodtl pod ON pom.pomstoid=pod.pomstoid INNER JOIN QL_trnmrmst mrm ON mrm.pomstoid=pom.pomstoid INNER JOIN QL_trnmrdtl mrd ON mrd.mrmstoid=mrm.mrmstoid INNER JOIN QL_mstgen g ON g.gencode=pom.pogroup INNER JOIN QL_mstsupp s ON s.suppoid=pom.suppoid WHERE pom.cmpcode='" & CompnyCode & "' AND " & FilterDDLListReg.SelectedValue & " LIKE '%" & Tchar(FilterTextListReg.Text) & "%' AND pom.suppoid=" & suppoid.Text & " AND pom.pomststatus IN ('Closed','Approved') AND mrd.mrdtloid NOT IN (SELECT mrdtloid FROM QL_trnbelidtl2 apd INNER JOIN QL_trnbelimst2 apm ON apm.cmpcode=apd.cmpcode AND apm.trnbelimstoid=apd.trnbelimstoid WHERE apd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnbelimststatus<>'Rejected'" & sAdd & ") AND pom.pomstoid IN (SELECT mrm.pomstoid FROM QL_trnmrmst mrm WHERE mrm.cmpcode='" & DDLBusUnit.SelectedValue & "' AND ISNULL(mrmstres1, '')='') /*AND pom.curroid=" & curroid.SelectedValue & "*/ /*AND (CASE registerflag WHEN 'IMPORT' THEN registermstres1 ELSE 'Closed' END)='Closed'*/  ORDER BY pomstoid"
        Dim objTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnregistermst")
        Session("Register") = objTbl
        gvListReg.DataSource = objTbl
        gvListReg.DataBind()
    End Sub

    Private Sub BindReceiveData()
        Dim sAdd As String = ""
        If Session("oid") IsNot Nothing And Session("oid") <> "" Then
            sAdd = " AND apm.trnbelimstoid <>" & Session("oid")
        End If
        sSql = "SELECT DISTINCT 'False' AS CheckValue, '' AS fakturno, '' AS fakturpajakno, '' AS aprawdtlnote, CONVERT(VARCHAR(10), mrm.mrdate, 101) AS mrrawdate, mrm.mrmstnote AS mrrawmstnote, mrm.mrmstoid AS mrrawmstoid, mrno AS mrrawno FROM QL_trnpodtl pod INNER JOIN QL_trnmrdtl mrd ON mrd.cmpcode=pod.cmpcode AND mrd.podtloid=pod.podtloid INNER JOIN QL_trnmrmst mrm ON mrm.cmpcode=mrd.cmpcode AND mrm.mrmstoid=mrd.mrmstoid WHERE mrm.cmpcode='" & DDLBusUnit.SelectedValue & "' AND " & FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%' AND mrm.pomstoid=" & pomstoid.Text & " /*AND ISNULL(mrdtlres1, '')='' AND ISNULL(mrmstres1, '')=''*/ AND mrmststatus in ('Post','Closed') /*AND ISNULL(mrdtlstatus,'')=''*/ AND mrd.mrdtloid NOT IN (SELECT mrdtloid FROM QL_trnbelidtl2 apd INNER JOIN QL_trnbelimst2 apm ON apm.cmpcode=apd.cmpcode AND apm.trnbelimstoid=apd.trnbelimstoid WHERE apd.cmpcode=mrm.cmpcode AND trnbelimststatus<>'Rejected'" & sAdd & ") ORDER BY mrm.mrno"
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnmrmst")
        If dtTbl.Rows.Count > 0 Then
            Session("TblMat") = dtTbl
            Session("TblMatView") = dtTbl
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            showMessage("LPB data can't be found!", 2)
        End If
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("QL_trnaprawdtl")
        dtlTable.Columns.Add("aprawdtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("registermstoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("registerno", Type.GetType("System.String"))
        dtlTable.Columns.Add("registerdtloid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("mrrawmstoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("mrrawno", Type.GetType("System.String"))
        dtlTable.Columns.Add("mrrawdtloid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matrawoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matrawcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("matrawlongdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("piqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("piqty_unitkecil", Type.GetType("System.Double"))
        dtlTable.Columns.Add("piqty_unitbesar", Type.GetType("System.Double"))
        dtlTable.Columns.Add("piunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("piunit1oid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("piunit2oid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("piunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("piunit1", Type.GetType("System.String"))
        dtlTable.Columns.Add("piunit2", Type.GetType("System.String"))
        dtlTable.Columns.Add("aprawprice", Type.GetType("System.Double"))
        dtlTable.Columns.Add("poprice", Type.GetType("System.Double"))
        dtlTable.Columns.Add("piprice", Type.GetType("System.Double"))
        dtlTable.Columns.Add("aprawdtlamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("aprawdtldisctype", Type.GetType("System.String"))
        dtlTable.Columns.Add("aprawdtldiscvalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("aprawdtldiscamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("aprawdtlnetto", Type.GetType("System.Double"))
        dtlTable.Columns.Add("aprawdtltaxamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("aprawdtlnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("aprawdtlres1", Type.GetType("System.String"))
        dtlTable.Columns.Add("aprawdtlres2", Type.GetType("System.String"))
        dtlTable.Columns.Add("NoFaktur", Type.GetType("System.String"))
        dtlTable.Columns.Add("FkPajak", Type.GetType("System.String"))
        dtlTable.Columns.Add("batchnumber", Type.GetType("System.String"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub CountHdrAmount()
        Dim dTotalNetto As Double = 0
        If Not Session("TblDtl") Is Nothing Then
            Dim dAmt As Double = 0, dTax As Double = 0
            Dim dDiscAmt As Double = 0
            Dim objTable As DataTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                For C1 As Int16 = 0 To objTable.Rows.Count - 1
                    dAmt += ToDouble(objTable.Rows(C1).Item("aprawdtlamt").ToString)
                    dTax += ToDouble(objTable.Rows(C1).Item("aprawdtltaxamt").ToString)
                    dDiscAmt += ToDouble(objTable.Rows(C1).Item("aprawdtldiscamt").ToString)
                Next
            End If
            aprawtotalamt.Text = ToMaskEdit(dAmt, iRoundDigit)
            amtdiscdtl.Text = ToMaskEdit(dDiscAmt, iRoundDigit)
        End If
        If trnbelidisctype.SelectedValue = "P" Then
            amtdischdr.Text = ToMaskEdit((ToDouble(aprawtotalamt.Text) - ToDouble(amtdiscdtl.Text)) * ToDouble(trnbelidisc.Text) / 100, iRoundDigit)
        Else
            amtdischdr.Text = ToMaskEdit(ToDouble(trnbelidisc.Text), iRoundDigit)
        End If
        aptotaldiscamt.Text = ToMaskEdit(ToDouble(amtdiscdtl.Text) + ToDouble(amtdischdr.Text), iRoundDigit)
        dTotalNetto = ToMaskEdit(ToDouble(aprawtotalamt.Text) - ToDouble(aptotaldiscamt.Text), iRoundDigit)
        aprawtotaltax.Text = ToMaskEdit(dTotalNetto * ToDouble(trnbelitaxpct.Text / 100), iRoundDigit)
        aprawgrandtotal.Text = ToMaskEdit(Math.Round(dTotalNetto + ToDouble(aprawtotaltax.Text), MidpointRounding.AwayFromZero), 2)
    End Sub

    Private Sub ClearDetail()
        aprawdtlseq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                EnableHeader(False)
            Else
                EnableHeader(True)
                'aprawsupptotal.Text = ""
            End If
            aprawdtlseq.Text = objTable.Rows.Count + 1
        Else
            EnableHeader(True)
        End If
        i_u2.Text = "New Detail"
        registermstoid.Text = ""
        registerno.Text = ""
        aprawdtlnote.Text = ""
        aprawdtlres1.Text = ""
        aprawdtlres2.Text = ""
        aprawdtlres2a.Text = ""
        aprawdtlres2b.Text = ""
        aprawdtlres2c.Text = ""
        gvTblDtl.SelectedIndex = -1
        matcode.Text = ""
        matlongdesc.Text = ""
        piqty.Text = ""
        unitkecil.Text = ""
        unitkeciloid.SelectedIndex = -1
        unitbesar.Text = ""
        unitbesaroid.SelectedIndex = -1
        poprice.Text = ""
        amtnetto.Text = ""
        DDLDiscType.SelectedIndex = -1
        amtdisc.Text = ""
        amtdisctotal.Text = ""
        aprawdtlnote.Text = ""
        piamount.Text = ""
        btnAddToList.Visible = False
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        DDLBusUnit.Enabled = bVal 'curroid.Enabled = bVal
        btnSearchSupp.Visible = bVal : btnClearSupp.Visible = bVal
        If bVal = True Then
            DDLBusUnit.CssClass = "inpText" 'curroid.CssClass = "inpText"
        Else
            DDLBusUnit.CssClass = "inpTextDisabled" 'curroid.CssClass = "inpTextDisabled"
        End If
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT apm.cmpcode, trnbelimstoid aprawmstoid, apm.periodacctg, trnbelidate aprawdate, trnbelino aprawno, apm.suppoid, suppname2 suppname, trnbelipaytype aprawpaytypeoid, apm.curroid, apm.rateoid, beliratetoidr aprawratetoidr, beliratetousd aprawratetousd, apm.rate2oid, belirate2toidr aprawrate2toidr, belirate2tousd aprawrate2tousd, trnbeliamt aprawtotalamt, trnbelidiscamt aprawtotaldisc, trnbelimsttaxamt aprawtotaltax, trnbeliamtnetto aprawgrandtotal, trnbelimstnote aprawmstnote, trnbelimststatus aprawmststatus, apm.createuser, apm.createtime, apm.upduser, apm.updtime, apsupptotal, apdatetakegiro, pom.pomstoid, pom.pono, pom.pogroup AS potype, (CASE WHEN pom.potaxtype_inex='INC' THEN 'INCLUDE' WHEN pom.potaxtype_inex='EXC' THEN 'EXCLUDE' ELSE 'NON TAX' END) AS potaxtype, potaxamt, povat, (CASE g.gencode WHEN 'RAW' THEN 'RM' WHEN 'GEN' THEN 'GM' ELSE g.gencode END) AS pogroup, trnbelidiscamtdtl, trnbelitotaldiscamt, trnbelidisctype, trnbelidiscvalue, trnbelimsttaxpct, ISNULL(trnbelimstres3, '') aprawfakturno, trnbelimasafakturdate FROM QL_trnbelimst2 apm INNER JOIN QL_mstsupp s ON s.suppoid=apm.suppoid INNER JOIN QL_trnpomst pom ON pom.cmpcode=apm.cmpcode AND pom.pomstoid=apm.pomstoid INNER JOIN QL_mstgen g ON g.gencode=pom.pogroup WHERE trnbelimstoid=" & sOid
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd.CommandText = sSql
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = xreader("cmpcode").ToString
                aprawmstoid.Text = xreader("aprawmstoid").ToString
                periodacctg.Text = Trim(xreader("periodacctg").ToString)
                aprawdate.Text = Format(xreader("aprawdate"), "MM/dd/yyyy")
                aprawno.Text = Trim(xreader("aprawno").ToString)
                suppoid.Text = xreader("suppoid").ToString
                suppname.Text = Trim(xreader("suppname").ToString)
                aprawpaytypeoid.SelectedValue = xreader("aprawpaytypeoid").ToString
                curroid.SelectedValue = xreader("curroid").ToString
                rateoid.Text = xreader("rateoid").ToString
                aprawratetoidr.Text = "1"
                aprawratetousd.Text = "1"
                rate2oid.Text = "1"
                aprawrate2toidr.Text = "1"
                aprawrate2tousd.Text = "1"
                trnbelidisctype.SelectedValue = xreader("trnbelidisctype").ToString
                trnbelidisc.Text = ToMaskEdit(ToDouble(Trim(xreader("trnbelidiscvalue").ToString)), iRoundDigit)
                amtdiscdtl.Text = ToMaskEdit(ToDouble(Trim(xreader("trnbelidiscamtdtl").ToString)), iRoundDigit)
                amtdischdr.Text = ToMaskEdit(ToDouble(Trim(xreader("aprawtotaldisc").ToString)), iRoundDigit)
                aptotaldiscamt.Text = ToMaskEdit(ToDouble(Trim(xreader("trnbelitotaldiscamt").ToString)), iRoundDigit)
                aprawtotalamt.Text = ToMaskEdit(ToDouble(Trim(xreader("aprawtotalamt").ToString)), iRoundDigit)
                aprawtotaltax.Text = ToMaskEdit(ToDouble(Trim(xreader("aprawtotaltax").ToString)), iRoundDigit)
                aprawgrandtotal.Text = ToMaskEdit(ToDouble(Trim(xreader("aprawgrandtotal").ToString)), iRoundDigit)
                aprawmstnote.Text = Trim(xreader("aprawmstnote").ToString)
                aprawmststatus.Text = Trim(xreader("aprawmststatus").ToString)
                createuser.Text = Trim(xreader("createuser").ToString)
                createtime.Text = Trim(xreader("createtime").ToString)
                upduser.Text = Trim(xreader("upduser").ToString)
                updtime.Text = Trim(xreader("updtime").ToString)
                aprawsupptotal.Text = ToMaskEdit(ToDouble(Trim(xreader("apsupptotal").ToString)), iRoundDigit)
                If (Format(xreader("apdatetakegiro"), "MM/dd/yyyy")) = "01/01/1900" Then
                    aprawdatetakegiro.Text = ""
                Else
                    aprawdatetakegiro.Text = Format(xreader("apdatetakegiro"), "MM/dd/yyyy")
                End If
                pomstoid.Text = xreader("pomstoid").ToString
                pono.Text = xreader("pono").ToString
                potype.Text = xreader("pogroup").ToString
                pogroup.Text = xreader("potype").ToString
                trntaxtype.SelectedValue = xreader("potaxtype").ToString
                trnbelitaxpct.Text = xreader("potaxamt").ToString
                trntaxtype.SelectedValue = xreader("potaxtype").ToString
                trntaxpct.Text = ToMaskEdit(ToDouble(Trim(xreader("trnbelimsttaxpct").ToString)), iRoundDigit)
                aprawtotaltax.Text = ToMaskEdit(ToDouble(xreader("potaxamt").ToString), iRoundDigit)
                curroid_SelectedIndexChanged(Nothing, Nothing)
                aprawfakturno.Text = xreader("aprawfakturno").ToString
                aprawmasapajak.Text = Format(xreader("trnbelimasafakturdate"), "MM/dd/yyyy")
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False : btnSendApproval.Visible = False
        End Try
        If aprawmststatus.Text <> "In Process" And aprawmststatus.Text <> "Revised" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnSendApproval.Visible = False
            btnAddToList.Visible = False
            gvTblDtl.Columns(gvTblDtl.Columns.Count - 1).Visible = False
            If aprawmststatus.Text = "Approved" Or aprawmststatus.Text = "Closed" Then
                lblTrnNo.Text = "A/P No."
                aprawmstoid.Visible = False
                aprawno.Visible = True
                btnUpdate.Visible = True
            End If
        End If
        sSql = "SELECT trnbelidtlseq aprawdtlseq, apd.mrmstoid registermstoid, mrno registerno, apd.mrdtloid registerdtloid, apd.mrmstoid mrrawmstoid,mrno mrrawno, apd.mrdtloid mrrawdtloid, apd.itemoid matrawoid, itemCode matrawcode, itemLongDescription matrawlongdesc, trnbelidtlqty AS piqty, trnbelidtlunitoid AS piunitoid, g.gendesc AS piunit, trnbelidtlprice aprawprice, trnbelidtlamt aprawdtlamt, trnbelidtldisctype aprawdtldisctype, trnbelidtldiscvalue aprawdtldiscvalue, trnbelidtldiscamt aprawdtldiscamt, trnbelidtlamtnetto aprawdtlnetto, trnbelidtltaxamt aprawdtltaxamt, trnbelidtlnote aprawdtlnote, trnbeliqty_unitkecil AS piqty_unitkecil, trnbeliqty_unitbesar AS piqty_unitbesar, g1.gendesc AS piunit1, g2.gendesc AS piunit2, poprice, (CASE WHEN trnbelidtlprice=poprice THEN 0 ELSE trnbelidtlprice END) AS piprice, g1.genoid AS piunit1oid, g2.genoid AS piunit2oid,apd.trnbelidtlres2 AS aprawdtlres1,apd.trnbelidtlres3 AS aprawdtlres2, batchnumber FROM QL_trnbelidtl2 apd INNER JOIN QL_trnmrmst mrm ON mrm.cmpcode=apd.cmpcode AND mrm.mrmstoid=apd.mrmstoid INNER JOIN QL_trnmrdtl mrd ON mrd.cmpcode=apd.cmpcode AND mrd.mrdtloid=apd.mrdtloid INNER JOIN QL_mstitem m ON m.itemoid=apd.itemoid INNER JOIN QL_mstgen g ON g.genoid=trnbelidtlunitoid INNER JOIN QL_mstgen g1 ON g1.genoid=m.itemunit1 INNER JOIN QL_mstgen g2 ON g2.genoid=m.itemunit3 WHERE trnbelimstoid=" & sOid & " ORDER BY trnbelidtlseq"
        Dim objTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnaprawdtl")
        Session("TblDtl") = objTbl
        gvTblDtl.DataSource = objTbl
        gvTblDtl.DataBind()
        CountHdrAmount()
        ClearDetail()
    End Sub

    Private Sub PrintReport()
        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("aprawmstoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            report.Load(Server.MapPath(folderReport & "rptFakturM_Xls.rpt"))
            Dim sWhere As String = ""
            If Session("CompnyCode") <> CompnyCode Then
                sWhere = " WHERE bm.cmpcode='" & Session("CompnyCode") & "'"
            Else
                sWhere = " WHERE bm.cmpcode LIKE '%'"
            End If
            sWhere = "WHERE bm.cmpcode='" & Session("CompnyCode") & "' AND bm.trnbelimstoid > 0"
            If sOid = "" Then
                sWhere &= " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND bm.trnbelimasafakturdate > ='" & FilterPeriod1.Text & " 00:00:00' AND bm.trnbelimasafakturdate < ='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sWhere &= " AND bm.trnbelimststatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Transaction\trnAP2.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND bm.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sWhere &= " AND bm.trnbelimstoid IN (" & sOid & ")"
            End If
            sWhere &= " ORDER BY bm.trnbelimasafakturdate"
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "PrintOutPI")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            report.Close()
            report.Dispose()
        End Try
        Response.Redirect("~\Transaction\trnAP2.aspx?awal=true")
    End Sub

    Private Sub GenerateNo(ByVal sType As String)
        Dim sNo As String = "PI" & sType & "-" & Format(GetServerTime(), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trnbelino, " & DefaultFormatCounter & ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnbelimst2 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnbelino LIKE '%" & sNo & "%'"
        If GetStrData(sSql) = "" Then
            aprawno.Text = GenNumberString(sNo, "", 1, DefaultFormatCounter)
        Else
            aprawno.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        End If
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                            sOid = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                        End If
                    Next
                    dv.RowFilter = "aprawmstoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub AddToList(ByVal sOid As String, ByVal sNo As String, ByVal sOidMR As String)
        If IsDetailInputValid() Then
            If Session("TblDtl") Is Nothing Then
                CreateTblDetail()
            End If
            Dim objTable As DataTable = Session("TblDtl")
            Dim objView As DataView = objTable.DefaultView
            objView.RowFilter = "registermstoid=" & sOid & " AND mrrawmstoid='" & sOidMR & "'"
            If objView.Count > 0 Then
                showMessage("This data have been added before. Please check!", 2)
                objView.RowFilter = ""
                Exit Sub
            End If
            objView.RowFilter = ""
            Dim dt As DataTable = GetDetailData(sOid)
            Dim iSeq As Integer = objTable.Rows.Count + 1
            For C1 As Integer = 0 To dt.Rows.Count - 1
                Dim objRow As DataRow = objTable.NewRow
                objRow("aprawdtlseq") = iSeq
                objRow("registermstoid") = sOid
                objRow("registerno") = sNo
                objRow("registerdtloid") = dt.Rows(C1)("registerdtloid")
                objRow("mrrawmstoid") = dt.Rows(C1)("mrrawmstoid")
                objRow("mrrawno") = dt.Rows(C1)("mrrawno").ToString
                objRow("mrrawdtloid") = dt.Rows(C1)("mrrawdtloid")
                objRow("matrawoid") = dt.Rows(C1)("matrawoid")
                objRow("matrawcode") = dt.Rows(C1)("matrawcode").ToString
                objRow("matrawlongdesc") = dt.Rows(C1)("matrawlongdesc").ToString
                objRow("mrrawno") = dt.Rows(C1)("mrrawno").ToString
                Dim dQty As Double = ToDouble(dt.Rows(C1)("mrqty").ToString)
                Dim dQty1 As Double = ToDouble(dt.Rows(C1)("mrqty_unitkecil").ToString)
                Dim dQty2 As Double = ToDouble(dt.Rows(C1)("mrqty_unitbesar").ToString)
                objRow("piqty") = dQty
                objRow("piqty_unitkecil") = dQty1
                objRow("piqty_unitbesar") = dQty2
                objRow("piunitoid") = dt.Rows(C1)("piunitoid")
                objRow("piunit1oid") = dt.Rows(C1)("piunit1oid")
                objRow("piunit2oid") = dt.Rows(C1)("piunit2oid")
                objRow("piunit") = dt.Rows(C1)("piunit").ToString
                objRow("piunit1") = dt.Rows(C1)("piunit1").ToString
                objRow("piunit2") = dt.Rows(C1)("piunit2").ToString
                Dim dPrice As Double
                If ToDouble(piamount.Text) > 0 Then
                    dPrice = ToDouble(piamount.Text)
                Else
                    dPrice = ToDouble(dt.Rows(C1)("porawprice").ToString)
                End If
                objRow("poprice") = ToDouble(dt.Rows(C1)("porawprice").ToString)
                objRow("aprawprice") = dPrice
                objRow("piprice") = "0.00"
                Dim dAmt As Double = dQty * dPrice
                objRow("aprawdtlamt") = dAmt
                Dim sType As String
                If ToDouble(piamount.Text) > 0 Then
                    sType = DDLDiscType.SelectedValue
                Else
                    sType = dt.Rows(C1)("porawdtldisctype").ToString
                End If
                objRow("aprawdtldisctype") = sType
                Dim dDisc As Double
                If ToDouble(amtdisc.Text) > 0 Then
                    dDisc = ToDouble(amtdisc.Text)
                Else
                    dDisc = ToDouble(dt.Rows(C1)("porawdtldiscvalue").ToString)
                End If
                objRow("aprawdtldiscvalue") = dDisc
                Dim dDiscAmt As Double = dDisc * dQty
                If sType = "P" Then
                    dDiscAmt = dAmt * dDisc / 100
                End If
                objRow("aprawdtldiscamt") = dDiscAmt
                objRow("aprawdtlnetto") = dAmt - dDiscAmt
                If dt.Rows(C1)("porawtaxtype").ToString.ToUpper = "TAX" Then
                    objRow("aprawdtltaxamt") = ((dAmt - dDiscAmt) * ToDouble(dt.Rows(C1)("porawtaxamt").ToString)) / 100
                Else
                    objRow("aprawdtltaxamt") = 0
                End If
                objRow("aprawdtlnote") = aprawdtlnote.Text
                objRow("aprawdtlres1") = aprawdtlres1.Text
                objRow("aprawdtlres2") = aprawdtlres2a.Text + "-" + aprawdtlres2b.Text + aprawdtlres2c.Text

                objTable.Rows.Add(objRow)
                iSeq += 1
            Next
            Session("TblDtl") = objTable
            gvTblDtl.DataSource = objTable
            gvTblDtl.DataBind()
            CountHdrAmount()
            aprawsupptotal.Text = aprawgrandtotal.Text
            ClearDetail()
        End If
    End Sub

    Private Sub CountAllDtlAmount()
        Dim dPrice As Double = ToDouble(poprice.Text)
        Dim dDtlAmt As Double, dDiscAmt As Double = 0
        If ToDouble(piamount.Text) > 0 Then
            dPrice = ToDouble(piamount.Text)
        End If
        dDtlAmt = Math.Round(ToDouble(piqty.Text) * dPrice, MidpointRounding.AwayFromZero)
        dDiscAmt = ToDouble(amtdisc.Text) * ToDouble(piqty.Text)
        If DDLDiscType.SelectedValue = "P" Then
            amtdisctotal.Text = ToMaskEdit((ToDouble(amtnetto.Text) * dDiscAmt) / 100, iRoundDigit)
        Else
            amtdisctotal.Text = ToMaskEdit(dDiscAmt, iRoundDigit)
        End If
        amtnetto.Text = ToMaskEdit(dDtlAmt - ToDouble(amtdisctotal.Text), iRoundDigit)
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim scode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = scode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnAP2.aspx")
        End If
        If checkPagePermission("~\Transaction\trnAP2.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Purchase Invoice"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnSendApproval.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to send this data for approval?');")
        btnUpdate.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to UPDATE this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            CheckAPStatus()
            InitAllDDL()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                aprawmstoid.Text = GenerateID("QL_TRNAPRAWMST", CompnyCode)
                aprawdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                aprawmasapajak.Text = Format(GetServerTime(), "MM/dd/yyyy")
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
                GenerateNo(potype.Text)
                aprawmststatus.Text = "In Process"
                curroid_SelectedIndexChanged(Nothing, Nothing)
                btnDelete.Visible = False
                btnUpdate.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvTblDtl.DataSource = dt
            gvTblDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnAP2.aspx?awal=true")
            End If
        End If
        If Not Session("WarningListPO") Is Nothing And Session("WarningListPO") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListPO") Then
                cProc.SetModalPopUpExtender(btnHideListReg, pnlListReg, mpeListReg, True)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
    End Sub

    Protected Sub lkbAPInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAPInProcess.Click
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim nDays As Integer = 7
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, apm.updtime, GETDATE()) > " & nDays & " AND aprawmststatus='In Process'"
        If checkPagePermission("~\Transaction\trnAP2.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND apm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub lkbAPInApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAPInApproval.Click
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 2
        Dim nDays As Integer = 7
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, apm.updtime, GETDATE()) > " & nDays & " AND aprawmststatus='In Approval' "
        If checkPagePermission("~\Transaction\trnAP2.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND apm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND trnbelidate>='" & FilterPeriod1.Text & " 00:00:00' AND trnbelidate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND trnbelimststatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Transaction\trnAP2.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND apm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        Dim sSqlPlus As String = ""
        If checkPagePermission("~\Transaction\trnAP2.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus = " AND apm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData((sSqlPlus))
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        If Session("oid") Is Nothing Or Session("oid") = "" Then
            GenerateNo(potype.Text)
        End If
        btnClearSupp_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupp.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        FilterDDLListSupp.SelectedIndex = -1 : FilterTextListSupp.Text = "" : gvListSupp.SelectedIndex = -1
        BindSupplierData()
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
    End Sub

    Protected Sub btnClearSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupp.Click
        suppoid.Text = "" : suppname.Text = "" : aprawpaytypeoid.SelectedIndex = -1 : aprawdatetakegiro.Text = ""
    End Sub

    Protected Sub btnFindListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSupp.Click
        BindSupplierData()
        mpeListSupp.Show()
    End Sub

    Protected Sub btnAllListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListSupp.Click
        FilterDDLListSupp.SelectedIndex = -1 : FilterTextListSupp.Text = "" : gvListSupp.SelectedIndex = -1
        BindSupplierData()
        mpeListSupp.Show()
    End Sub

    Protected Sub gvListSupp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSupp.PageIndexChanging
        gvListSupp.PageIndex = e.NewPageIndex
        BindSupplierData()
        mpeListSupp.Show()
    End Sub

    Protected Sub gvListSupp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListSupp.SelectedIndexChanged
        If suppoid.Text <> gvListSupp.SelectedDataKey.Item("suppoid").ToString Then
            btnClearSupp_Click(Nothing, Nothing)
        End If
        suppoid.Text = gvListSupp.SelectedDataKey.Item("suppoid").ToString
        suppname.Text = gvListSupp.SelectedDataKey.Item("suppname").ToString
        aprawpaytypeoid.SelectedValue = gvListSupp.SelectedDataKey.Item("supppaymentoid").ToString
        Dim sErr As String = ""
        If aprawdate.Text <> "" Then
            If IsValidDate(aprawdate.Text, "MM/dd/yyyy", sErr) Then
                If aprawpaytypeoid.SelectedValue <> "" Then
                    'Dim sNum As String = GetNumericValues(aprawpaytypeoid.SelectedItem.Text)
                    'Dim sNum As String = "14"
                    'ToDouble(sNum)
                    'aprawdatetakegiro.Text = Format(DateAdd(DateInterval.Day, 14, CDate(aprawdate.Text)), "MM/dd/yyyy")
                End If
            End If
        End If
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub lkbCloseListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListSupp.Click
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub curroid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles curroid.SelectedIndexChanged
        Dim sErr As String = ""
        If aprawdate.Text = "" Then
            showMessage("Please fill A/P Date first!", 2)
            Exit Sub
        Else
            If Not IsValidDate(aprawdate.Text, "MM/dd/yyyy", sErr) Then
                showMessage("A/P Date is invalid. " & sErr, 2)
                Exit Sub
            End If
        End If
        If curroid.SelectedValue <> "" Then
            Dim iCurr As Integer = CInt(curroid.SelectedValue)
            Dim isIDR As Boolean = False
            If curroid.SelectedItem.Text = "IDR" Then
                isIDR = True
                sSql = "SELECT curroid FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND currcode='USD' AND activeflag='ACTIVE'"
                iCurr = CInt(ToDouble(GetStrData(sSql)))
            End If

            ' Insert Daily Rate
            sSql = "SELECT TOP 1 rateoid, rateres1 AS rateidrvalue, rateres2 AS rateusdvalue, ratedate FROM QL_mstrate WHERE cmpcode='" & CompnyCode & "' AND curroid=" & iCurr & " AND '" & aprawdate.Text & " 00:00:00'>=ratedate AND '" & aprawdate.Text & " 00:00:00'<=ratetodate AND activeflag='ACTIVE' ORDER BY ratedate DESC, rateoid DESC"
            Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstrate")
            If dt.Rows.Count > 0 Then
                rateoid.Text = dt.Rows(0)("rateoid").ToString
                aprawratetoidr.Text = ToMaskEdit(ToDouble(dt.Rows(0)("rateidrvalue").ToString), GetRoundValue(dt.Rows(0)("rateidrvalue").ToString))
                aprawratetousd.Text = ToMaskEdit(ToDouble(dt.Rows(0)("rateusdvalue").ToString), GetRoundValue(dt.Rows(0)("rateusdvalue").ToString))
                If isIDR = True Then
                    aprawratetoidr.Text = "1"
                    aprawratetousd.Text = ToMaskEdit(1 / ToDouble(dt.Rows(0)("rateidrvalue").ToString), GetRoundValue((1 / ToDouble(dt.Rows(0)("rateidrvalue").ToString)).ToString))
                End If
            Else
                sErr &= ""
                rateoid.Text = 0 : aprawratetoidr.Text = "" : aprawratetousd.Text = ""
            End If

            ' Insert Monthly Rate
            sSql = "SELECT TOP 1 rate2oid, rate2res1 AS rate2idrvalue, rate2res2 AS rate2usdvalue, rate2date FROM QL_mstrate2 WHERE cmpcode='" & CompnyCode & "' AND curroid=" & iCurr & " AND rate2month=" & Month(CDate(aprawdate.Text)) & " AND rate2year=" & Year(CDate(aprawdate.Text)) & " AND activeflag='ACTIVE' ORDER BY rate2oid DESC"
            Dim dt2 As DataTable = cKon.ambiltabel(sSql, "QL_mstrate2")
            If dt2.Rows.Count > 0 Then
                rate2oid.Text = dt2.Rows(0)("rate2oid").ToString
                aprawrate2toidr.Text = ToMaskEdit(ToDouble(dt2.Rows(0)("rate2idrvalue").ToString), GetRoundValue(dt2.Rows(0)("rate2idrvalue").ToString))
                aprawrate2tousd.Text = ToMaskEdit(ToDouble(dt2.Rows(0)("rate2usdvalue").ToString), GetRoundValue(dt2.Rows(0)("rate2usdvalue").ToString))
                If isIDR = True Then
                    aprawrate2toidr.Text = "1"
                    aprawrate2tousd.Text = ToMaskEdit(1 / ToDouble(dt2.Rows(0)("rate2idrvalue").ToString), GetRoundValue((1 / ToDouble(dt2.Rows(0)("rate2idrvalue").ToString)).ToString))
                End If
            Else
                sErr &= ""
                rate2oid.Text = "" : aprawrate2toidr.Text = "" : aprawrate2tousd.Text = ""
            End If
        End If
        If sErr <> "" Then
            showMessage(sErr, 2)
        End If
    End Sub

    Protected Sub btnSearchReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If suppoid.Text = "" Then
            showMessage("Please select Supplier first!", 2)
            Exit Sub
        End If
        If curroid.SelectedValue = "" Then
            showMessage("Please select Currency first!", 2)
            Exit Sub
        End If
        FilterDDLListReg.SelectedIndex = -1 : FilterTextListReg.Text = "" : gvListReg.SelectedIndex = -1
        BindRegisterData()
        cProc.SetModalPopUpExtender(btnHideListReg, pnlListReg, mpeListReg, True)
    End Sub

    Protected Sub btnClearReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        pomstoid.Text = "" : pono.Text = "" : potype.Text = "" : GenerateNo(potype.Text) : Session("TblDtl") = Nothing : gvTblDtl.DataSource = Session("TblDtl") : gvTblDtl.DataBind() : ClearDetail() : pogroup.Text = "" : trnbelitaxtype.Text = "" : trnbelitaxpct.Text = "" : curroid.SelectedIndex = -1
    End Sub

    Protected Sub btnFindListReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListReg.Click
        BindRegisterData()
        mpeListReg.Show()
    End Sub

    Protected Sub btnAllListReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListReg.Click
        FilterDDLListReg.SelectedIndex = -1 : FilterTextListReg.Text = "" : gvListReg.SelectedIndex = -1
        BindRegisterData()
        mpeListReg.Show()
    End Sub

    Protected Sub gvListReg_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListReg.PageIndexChanging
        gvListReg.PageIndex = e.NewPageIndex
        BindRegisterData()
        mpeListReg.Show()
    End Sub

    Protected Sub gvListReg_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListReg.SelectedIndexChanged
        If pomstoid.Text <> gvListReg.SelectedDataKey.Item("pomstoid").ToString Then
            btnClearReg_Click(Nothing, Nothing)
        End If
        If Session("TblDtl") Is Nothing Then
            pomstoid.Text = gvListReg.SelectedDataKey.Item("pomstoid").ToString
            pono.Text = gvListReg.SelectedDataKey.Item("pono").ToString
            potype.Text = gvListReg.SelectedDataKey.Item("pogroup").ToString
            pogroup.Text = gvListReg.SelectedDataKey.Item("potype").ToString
            trntaxtype.SelectedValue = gvListReg.SelectedDataKey.Item("potaxtype").ToString
            trnbelitaxpct.Text = gvListReg.SelectedDataKey.Item("potaxamt").ToString
            curroid.SelectedValue = gvListReg.SelectedDataKey.Item("curroid").ToString
            trnbelidisctype.SelectedValue = gvListReg.SelectedDataKey.Item("pomstdisctype").ToString
            trnbelidisc.Text = ToMaskEdit(ToDouble(gvListReg.SelectedDataKey.Item("pomstdiscvalue").ToString), iRoundDigit)
            amtdischdr.Text = ToMaskEdit(ToDouble(gvListReg.SelectedDataKey.Item("pomstdiscamt").ToString), iRoundDigit)
            trntaxtype.SelectedValue = gvListReg.SelectedDataKey.Item("potaxtype").ToString
            Dim dTaxPct As Double = 10
            If trntaxtype.SelectedValue = "NON TAX" Or trntaxtype.SelectedValue = "INCLUDE" Then
                dTaxPct = 0
            End If
            trntaxpct.Text = dTaxPct
            aprawtotaltax.Text = ToMaskEdit(ToDouble(gvListReg.SelectedDataKey.Item("potaxamt").ToString), iRoundDigit)
            curroid_SelectedIndexChanged(Nothing, Nothing)
            If Session("oid") Is Nothing Or Session("oid") = "" Then
                GenerateNo(potype.Text)
            End If
        End If
        cProc.SetModalPopUpExtender(btnHideListReg, pnlListReg, mpeListReg, False)
    End Sub

    Protected Sub lkbCloseListReg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListReg.Click
        cProc.SetModalPopUpExtender(btnHideListReg, pnlListReg, mpeListReg, False)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            Dim objTable As DataTable = Session("TblDtl")
            Dim objRow As DataRow = objTable.Rows(aprawdtlseq.Text - 1)
            objRow.BeginEdit()
            Dim dQty As Double = ToDouble(piqty.Text)
            Dim dQty1 As Double = ToDouble(unitkecil.Text)
            Dim dQty2 As Double = ToDouble(unitbesar.Text)
            Dim dPrice As Double
            If ToDouble(piamount.Text) > 0 Then
                dPrice = ToDouble(piamount.Text)
                objRow("aprawprice") = dPrice
            Else
                dPrice = ToDouble(poprice.Text)
            End If
            objRow("poprice") = ToDouble(poprice.Text)
            objRow("piprice") = ToDouble(piamount.Text)
            Dim dAmt As Double = 0
            If trntaxtype.SelectedValue = "INCLUDE" Then
                dAmt = dQty * dPrice
                objRow("aprawdtlamt") = dAmt
            Else
                dAmt = Math.Round(dQty * dPrice, MidpointRounding.AwayFromZero)
                objRow("aprawdtlamt") = dAmt
            End If

            Dim sType As String = DDLDiscType.SelectedValue
            If ToDouble(amtdisc.Text) > 0 Then
                objRow("aprawdtldisctype") = sType
            End If
            Dim dDisc As Double = 0
            Dim dDiscAmt As Double = 0
            If ToDouble(amtdisc.Text) > 0 Then
                dDisc = ToDouble(amtdisc.Text)
                dDiscAmt = dDisc * dQty
                If sType = "P" Then
                    dDiscAmt = dAmt * dDisc / 100
                End If
            End If
            objRow("aprawdtldiscvalue") = dDisc
            objRow("aprawdtldiscamt") = dDiscAmt
            If trntaxtype.SelectedValue = "INCLUDE" Then
                objRow("aprawdtlnetto") = dAmt - dDiscAmt
            Else
                objRow("aprawdtlnetto") = Math.Round(dAmt - dDiscAmt, MidpointRounding.AwayFromZero)
            End If
            If trnbelitaxpct.Text <> "" Then
                objRow("aprawdtltaxamt") = ((dAmt - dDiscAmt) * ToDouble(trnbelitaxpct.Text)) / 100
            Else
                objRow("aprawdtltaxamt") = 0
            End If
            objRow("aprawdtlnote") = aprawdtlnote.Text
            objRow("aprawdtlres1") = aprawdtlres1.Text
            objRow("aprawdtlres2") = aprawdtlres2.Text

            objRow.EndEdit()
            objTable.AcceptChanges()
            Session("TblDtl") = objTable
            gvTblDtl.DataSource = objTable
            gvTblDtl.DataBind()
            CountHdrAmount()
            aprawsupptotal.Text = aprawgrandtotal.Text
            ClearDetail()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvTblDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTblDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), iRoundDigit)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), iRoundDigit)
            e.Row.Cells(10).Text = ToMaskEdit(ToDouble(e.Row.Cells(10).Text), iRoundDigit)
            e.Row.Cells(12).Text = ToMaskEdit(ToDouble(e.Row.Cells(12).Text), iRoundDigit)
            e.Row.Cells(13).Text = ToMaskEdit(ToDouble(e.Row.Cells(13).Text), iRoundDigit)
            e.Row.Cells(14).Text = ToMaskEdit(ToDouble(e.Row.Cells(14).Text), iRoundDigit)
            e.Row.Cells(15).Text = ToMaskEdit(ToDouble(e.Row.Cells(15).Text), iRoundDigit)
            e.Row.Cells(16).Text = ToMaskEdit(ToDouble(e.Row.Cells(16).Text), iRoundDigit)
        End If
    End Sub

    Protected Sub gvTblDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvTblDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        Dim iOid As Integer = objRow(e.RowIndex)("registermstoid")
        Dim dv As DataView = objTable.DefaultView
        dv.RowFilter = "registermstoid=" & iOid
        For C1 As Int16 = 0 To dv.Count - 1
            dv.Delete(0)
        Next
        dv.RowFilter = ""
        Session("TblDtl") = dv.ToTable
        gvTblDtl.DataSource = Session("TblDtl")
        gvTblDtl.DataBind()
        CountHdrAmount() : ClearDetail()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                If CheckDataExists("SELECT COUNT(*) FROM QL_trnbelimst2 WHERE trnbelimstoid=" & aprawmstoid.Text) Then
                    aprawmstoid.Text = GenerateID("QL_trnbelimst2", CompnyCode)
                End If
                If CheckDataExists("SELECT COUNT(*) FROM QL_trnbelimst2 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnbelino='" & aprawno.Text & "'") Then
                    GenerateNo(potype.Text)
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_trnbelimst2", "trnbelimstoid", aprawmstoid.Text, "trnbelimststatus", updtime.Text, "Approved")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    aprawmststatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            aprawdtloid.Text = GenerateID("QL_trnbelidtl2", CompnyCode)
            Dim iAppOid As Integer = GenerateID("QL_APPROVAL", CompnyCode)
            periodacctg.Text = GetDateToPeriodAcctg(aprawdate.Text)
            Dim sDTG As String = "1/1/1900"
            If aprawdatetakegiro.Text <> "" Then
                sDTG = aprawdatetakegiro.Text
            End If
            If aprawmststatus.Text = "Revised" Then
                aprawmststatus.Text = "In Process"
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trnbelimst2 (cmpcode, trnbelimstoid, periodacctg, trnbelino, trnbelidate, trnbelitype, trnbeliref, suppoid, pomstoid, trnbelipaytype, curroid, rateoid, beliratetoidr, beliratetousd, rate2oid, belirate2toidr, belirate2tousd, trnbeliamt, trnbeliamtidr, trnbeliamtusd, trnbelidiscamtdtl, trnbelidisctype, trnbelidiscvalue, trnbelidiscamt, trnbelidiscamtidr, trnbelidiscamtusd, trnbelitotaldiscamt, trnbelimsttaxtype, trnbelimsttaxpct, trnbelimsttaxamt, trnbelimsttaxamtidr, trnbelimsttaxamtusd, trnbeliamtnetto, trnbeliamtnettoidr, trnbeliamtnettousd, trnbelimstnote, trnbelimststatus, createuser, createtime, upduser, updtime, apsupptotal, apdatetakegiro, trnbelimstres3, trnbelimasafakturdate) VALUES ('" & DDLBusUnit.SelectedValue & "', " & aprawmstoid.Text & ", '" & periodacctg.Text & "', '" & aprawno.Text & "', '" & aprawdate.Text & "', '" & potype.Text & "', '', " & suppoid.Text & ", " & pomstoid.Text & ", " & aprawpaytypeoid.SelectedValue & ", 1, 1, '1', '1', 1, '1', '1', " & ToDouble(aprawtotalamt.Text) & ", " & ToDouble(aprawtotalamt.Text) & ", " & ToDouble(aprawtotalamt.Text) & ", " & ToDouble(amtdiscdtl.Text) & ", '" & trnbelidisctype.SelectedValue & "', " & ToDouble(trnbelidisc.Text) & ", " & ToDouble(amtdischdr.Text) & ", " & ToDouble(amtdischdr.Text) & ", 0, " & ToDouble(aptotaldiscamt.Text) & ", '" & trntaxtype.SelectedValue & "', " & ToDouble(trnbelitaxpct.Text) & ", " & ToDouble(aprawtotaltax.Text) & ", " & ToDouble(aprawtotaltax.Text) & ", " & ToDouble(aprawtotaltax.Text) & ", " & ToDouble(aprawgrandtotal.Text) & ", " & ToDouble(aprawgrandtotal.Text) & ", " & ToDouble(aprawgrandtotal.Text) & ", '" & Tchar(aprawmstnote.Text.Trim) & "', '" & aprawmststatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(aprawsupptotal.Text) & ", '" & CDate(sDTG) & "', '" & Tchar(aprawfakturno.Text) & "', '" & aprawmasapajak.Text & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & aprawmstoid.Text & " WHERE tablename='QL_trnbelimst2' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trnbelimst2 SET periodacctg='" & periodacctg.Text & "', trnbelino='" & aprawno.Text & "', trnbelidate='" & CDate(aprawdate.Text) & "', trnbelitype='" & potype.Text & "', trnbeliref='', suppoid=" & suppoid.Text & ", pomstoid=" & pomstoid.Text & ", trnbelipaytype=" & aprawpaytypeoid.SelectedValue & ", trnbeliamt=" & ToDouble(aprawtotalamt.Text) & ", trnbeliamtidr= " & ToDouble(aprawtotalamt.Text) & ", trnbeliamtusd=" & ToDouble(aprawtotalamt.Text) & ", trnbelidisctype='" & trnbelidisctype.SelectedValue & "', trnbelidiscvalue=" & ToDouble(trnbelidisc.Text) & ", trnbelidiscamt=" & ToDouble(amtdischdr.Text) & ", trnbelidiscamtidr=" & ToDouble(amtdischdr.Text) & ", trnbelidiscamtusd=0, trnbelimsttaxtype='" & trntaxtype.SelectedValue & "', trnbelimsttaxpct=" & ToDouble(trnbelitaxpct.Text) & ", trnbelimsttaxamt=" & ToDouble(aprawtotaltax.Text) & ", trnbelimsttaxamtidr=" & ToDouble(aprawtotaltax.Text) * ToDouble(aprawrate2toidr.Text) & ", trnbelimsttaxamtusd=" & ToDouble(aprawtotaltax.Text) & ", trnbeliamtnetto=" & ToDouble(aprawgrandtotal.Text) & ", trnbeliamtnettoidr=" & ToDouble(aprawgrandtotal.Text) & ", trnbeliamtnettousd=" & ToDouble(aprawgrandtotal.Text) & ", trnbelimstnote='" & Tchar(aprawmstnote.Text.Trim) & "', trnbelimststatus='" & aprawmststatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, apsupptotal=" & ToDouble(aprawsupptotal.Text) & ", apdatetakegiro='" & CDate(sDTG) & "', trnbelidiscamtdtl=" & ToDouble(amtdiscdtl.Text) & ", trnbelitotaldiscamt=" & ToDouble(aptotaldiscamt.Text) & ", trnbelimstres3='" & Tchar(aprawfakturno.Text) & "', trnbelimasafakturdate='" & aprawmasapajak.Text & "' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnbelimstoid=" & aprawmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_trnmrdtl SET mrdtlstatus='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrdtloid IN (SELECT mrdtloid FROM QL_trnbelidtl2 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnbelimstoid=" & aprawmstoid.Text & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnmrmst SET mrmststatus='Post' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrmstoid IN (SELECT mrmstoid FROM QL_trnbelidtl2 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnbelimstoid=" & aprawmstoid.Text & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trnbelidtl2 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnbelimstoid=" & aprawmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trnbelidtl2 (cmpcode, trnbelidtloid, trnbelimstoid, trnbelidtlseq, mrdtloid, mrmstoid, itemoid, trnbelidtlqty, trnbelidtlunitoid, trnbelidtlprice, trnbelidtlamt, trnbelidtldisctype, trnbelidtldiscvalue, trnbelidtldiscamt, trnbelidtlamtnetto, trnbelidtltaxamt, trnbelidtlnote, trnbelidtlstatus, upduser, updtime, poprice, trnbeliqty_unitkecil, trnbeliqty_unitbesar,trnbelidtlres3,trnbelidtlres2) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C1 + CInt(aprawdtloid.Text)) & ", " & aprawmstoid.Text & ", " & C1 + 1 & ", " & objTable.Rows(C1)("mrrawdtloid") & ", " & objTable.Rows(C1)("mrrawmstoid") & ", " & objTable.Rows(C1)("matrawoid") & ", " & ToDouble(objTable.Rows(C1)("piqty").ToString) & ", " & objTable.Rows(C1)("piunitoid") & ", " & ToDouble(objTable.Rows(C1)("aprawprice").ToString) & ", " & ToDouble(objTable.Rows(C1)("aprawdtlamt").ToString) & ", '" & objTable.Rows(C1)("aprawdtldisctype").ToString & "', " & ToDouble(objTable.Rows(C1)("aprawdtldiscvalue").ToString) & ", " & ToDouble(objTable.Rows(C1)("aprawdtldiscamt").ToString) & ", " & ToDouble(objTable.Rows(C1)("aprawdtlnetto").ToString) & ", " & ToDouble(objTable.Rows(C1)("aprawdtltaxamt").ToString) & ", '" & Tchar(objTable.Rows(C1)("aprawdtlnote")) & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(C1)("poprice")) & ", " & ToDouble(objTable.Rows(C1)("piqty_unitkecil").ToString) & ", " & ToDouble(objTable.Rows(C1)("piqty_unitbesar").ToString) & ",'" & Tchar(objTable.Rows(C1)("aprawdtlres2")) & "','" & Tchar(objTable.Rows(C1)("aprawdtlres1")) & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        sSql = "UPDATE QL_trnmrdtl SET mrdtlstatus='Complete' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrdtloid=" & objTable.Rows(C1)("mrrawdtloid")
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        sSql = "UPDATE QL_trnmrmst SET mrmststatus='Closed' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrmstoid=" & objTable.Rows(C1)("mrrawmstoid")
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(aprawdtloid.Text)) & " WHERE tablename='QL_trnbelidtl2' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If aprawmststatus.Text = "In Approval" Then
                    Dim sType As String = ""
                    If potype.Text.ToUpper = "RM" Then
                        sType = "raw"
                    ElseIf potype.Text.ToUpper = "GM" Then
                        sType = "gen"
                    ElseIf potype.Text.ToUpper = "FG" Then
                        sType = "fg"
                    Else
                        sType = "wip"
                    End If
                    Dim dt As DataTable = Session("AppPerson")
                    Dim tempAppCode As String = ""
                    For C1 As Integer = 0 To dt.Rows.Count - 1
                        If C1 = 0 Then
                            tempAppCode = dt.Rows(C1)("apppersonlevel").ToString
                        End If
                        sSql = "INSERT INTO QL_approval (cmpcode, approvaloid, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvalnote, posisiLevel) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iAppOid + C1 & ", 'AP-" & sType.ToUpper & "" & aprawmstoid.Text & "_" & (iAppOid + C1) & "', '" & Session("UserID") & "', '" & aprawdate.Text & "', 'New', 'QL_trnap" & sType.ToLower & "mst', " & aprawmstoid.Text & ", 'In Approval', '" & dt.Rows(C1)("apppersonlevel").ToString & "', '" & dt.Rows(C1)("approvaluser").ToString & "', '1/1/1900', '', '" & IIf(tempAppCode = dt.Rows(C1)("apppersonlevel").ToString, "1", "") & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                    sSql = "UPDATE QL_mstOid SET lastoid=" & iAppOid + dt.Rows.Count - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_APPROVAL'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    aprawmststatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.ToString & sSql, 1)
                conn.Close()
                aprawmststatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "A/P No. have been regenerated because being used by another data. Your new A/P No. is " & aprawno.Text & ".<BR>"
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trnAP2.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnAP2.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If aprawmstoid.Text.Trim = "" Then
            showMessage("Please select A/P Raw Material data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_trnbelimst2", "trnbelimstoid", aprawmstoid.Text, "trnbelimststatus", updtime.Text, "Approved")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                aprawmststatus.Text = "In Process"
                Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trnmrdtl SET mrdtlstatus='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrdtloid IN (SELECT mrdtloid FROM QL_trnbelidtl2 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnbelimstoid=" & aprawmstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_trnmrmst SET mrmststatus='Post' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND mrmstoid IN (SELECT mrmstoid FROM QL_trnbelidtl2 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnbelimstoid=" & aprawmstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnbelidtl2 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnbelimstoid=" & aprawmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnbelimst2 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnbelimstoid=" & aprawmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnAP2.aspx?awal=true")
    End Sub

    Protected Sub btnSendApproval_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSendApproval.Click
        sSql = "SELECT approvaluser, apppersontype, case when upper(apppersontype)='FINAL' then 0 else apppersonlevel end apppersonlevel FROM QL_approvalperson WHERE tablename='QL_trnaprawmst' AND activeflag='ACTIVE' AND cmpcode IN ('" & CompnyCode & "', '" & DDLBusUnit.SelectedValue & "') order by apppersontype desc,apppersonlevel asc"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_approvalperson")
        If dt.Rows.Count = 0 Then
            showMessage("There is no user can't approve this transaction. Please define approval user in <STRONG>APPROVAL USER</STRONG> form.", 2)
        Else
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "apppersontype='Final'"
            If dv.Count = 0 Then
                showMessage("Final approval user is not defined for this transaction. Please define final approval user in <STRONG>APPROVAL USER</STRONG> form.", 2)
                dv.RowFilter = ""
            Else
                dv.RowFilter = ""
                Session("AppPerson") = dt
                aprawmststatus.Text = "In Approval"
                btnSave_Click(Nothing, Nothing)
            End If
        End If
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        UpdateCheckedValue()
        PrintReport()
    End Sub

    Protected Sub gvListSupp_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvListSupp.Sorting
        Dim dt = TryCast(Session("mstSupp"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvListSupp.DataSource = Session("mstSupp")
            gvListSupp.DataBind()
        End If
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
    End Sub

    Protected Sub gvListreg_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvListReg.Sorting
        Dim dt = TryCast(Session("Register"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvListReg.DataSource = Session("Register")
            gvListReg.DataBind()
        End If
        mpeListReg.Show()
    End Sub

    Protected Sub gvTRN_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub aprawsupptotal_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles aprawsupptotal.TextChanged
        sender.Text = ToMaskEdit(ToDouble(Trim(aprawsupptotal.Text)), iRoundDigit)
    End Sub

    Protected Sub aprawdtlres2a_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub btncleargiro_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        aprawdatetakegiro.Text = ""
    End Sub

    Protected Sub gvTblDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        aprawdtlseq.Text = gvTblDtl.SelectedDataKey.Item("aprawdtlseq").ToString
        Try
            If Session("TblDtl") IsNot Nothing Then
                i_u2.Text = "Update Detail"
                Dim objTbl As DataTable = Session("TblDtl")
                Dim dv As DataView = objTbl.DefaultView
                dv.RowFilter = "aprawdtlseq=" & aprawdtlseq.Text
                matcode.Text = dv(0).Item("matrawcode").ToString
                matlongdesc.Text = dv(0).Item("matrawlongdesc").ToString
                piqty.Text = dv(0).Item("piqty").ToString
                unitkecil.Text = dv(0).Item("piqty_unitkecil").ToString
                unitkeciloid.SelectedValue = dv(0).Item("piunit1oid").ToString
                unitbesar.Text = dv(0).Item("piqty_unitbesar").ToString
                unitbesaroid.SelectedValue = dv(0).Item("piunit2oid").ToString
                poprice.Text = ToMaskEdit(ToDouble(dv(0).Item("poprice").ToString), iRoundDigit)
                piamount.Text = ToMaskEdit(ToDouble(dv(0).Item("piprice").ToString), iRoundDigit)
                amtnetto.Text = ToMaskEdit(ToDouble(dv(0).Item("aprawdtlnetto").ToString), iRoundDigit)
                DDLDiscType.SelectedValue = dv(0).Item("aprawdtldisctype").ToString
                amtdisc.Text = ToMaskEdit(ToDouble(dv(0).Item("aprawdtldiscvalue").ToString), iRoundDigit)
                amtdisctotal.Text = ToMaskEdit(ToDouble(dv(0).Item("aprawdtldiscamt").ToString), iRoundDigit)
                aprawdtlnote.Text = dv(0).Item("aprawdtlnote").ToString
                registerno.Text = dv(0).Item("mrrawno").ToString
                aprawdtlres1.Text = dv(0).Item("aprawdtlres1").ToString
                aprawdtlres2.Text = dv(0).Item("aprawdtlres2")

                dv.RowFilter = ""
                btnAddToList.Visible = True
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub btnSearchReg_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If suppoid.Text = "" Then
            showMessage("Please select Supplier first!", 2)
            Exit Sub
        End If
        If curroid.SelectedValue = "" Then
            showMessage("Please select Currency first!", 2)
            Exit Sub
        End If
        If pomstoid.Text = "" Then
            showMessage("Please select PO No. first!", 2)
            Exit Sub
        End If
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : gvListMat.SelectedIndex = -1
        BindReceiveData()
    End Sub

    Protected Sub btnClearReg_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDetail()
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "mrrawmstoid=" & dtTbl.Rows(C1)("mrrawmstoid")
                    objView(0)("CheckValue") = "True"
                    dtTbl.Rows(C1)("CheckValue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                dtTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "No material data can't be selected!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "No material data can't be selected!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "mrrawmstoid=" & dtTbl.Rows(C1)("mrrawmstoid")
                    objView(0)("CheckValue") = "False"
                    dtTbl.Rows(C1)("CheckValue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                dtTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "No LPB data can't be selected!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "No LPB data can't be selected!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "CheckValue='True'"
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListMat") = "No material data have been selected before!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub lbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If UpdateCheckedMat() Then
            Dim dtCek As DataTable = Session("TblMat")
            Dim dvCek As DataView = dtCek.DefaultView
            dvCek.RowFilter = "CheckValue='True'"
            Dim iCheck As Integer = dvCek.Count
            If iCheck > 0 Then
                dvCek.RowFilter = ""
                dvCek.RowFilter = "CheckValue='True' AND fakturno<>''"
                If dvCek.Count <> iCheck Then
                    Session("WarningListMat") = "Please Fill Faktur No For every checked LPB!"
                    showMessage(Session("WarningListMat"), 2)
                    dvCek.RowFilter = ""
                    Exit Sub
                End If

                If Session("TblDtl") Is Nothing Then
                    CreateTblDetail()
                End If
                Dim objTable As DataTable
                objTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                Dim objRow As DataRow
                Dim dDisc As Double
                Dim mrmstoid As String = ""
                If Session("TblMat") IsNot Nothing Then
                    For x As Int64 = 0 To dvCek.Count - 1
                        mrmstoid = mrmstoid & "," & dvCek(x).Item("mrrawmstoid").ToString
                    Next
                End If
                If mrmstoid.Trim <> "" Then
                    mrmstoid = mrmstoid.Remove(0, 1)
                End If
                Dim dtTbl As DataTable = GetDetailData(mrmstoid)
                Dim dtView As DataView = dtTbl.DefaultView
                Dim counter As Integer = objTable.Rows.Count
                For C1 As Integer = 0 To dtView.Count - 1
                    dv.RowFilter = "mrrawdtloid=" & dtView(C1)("mrrawdtloid")
                    If dv.Count > 0 Then
                        dv.AllowEdit = True
                        For i As Int64 = 0 To dv.Count - 1
                            If dv(0)("mrrawmstoid").ToString = dvCek(i)("mrrawmstoid") Then
                                dv(0)("aprawdtlnote") = dvCek(i)("aprawdtlnote").ToString
                                dv(0)("aprawdtlres1") = dvCek(i)("fakturno").ToString
                                dv(0)("aprawdtlres2") = dvCek(i)("fakturpajakno").ToString
                            End If
                        Next
                    Else
                        counter += 1
                        objRow = objTable.NewRow()
                        objRow("aprawdtlseq") = counter
                        objRow("registermstoid") = pomstoid.Text
                        objRow("registerno") = pono.Text
                        objRow("registerdtloid") = dtView(C1)("registerdtloid")
                        objRow("mrrawmstoid") = dtView(C1)("mrrawmstoid")
                        objRow("mrrawno") = dtView(C1)("mrrawno").ToString
                        objRow("mrrawdtloid") = dtView(C1)("mrrawdtloid")
                        objRow("matrawoid") = dtView(C1)("matrawoid")
                        objRow("matrawcode") = dtView(C1)("matrawcode").ToString
                        objRow("matrawlongdesc") = dtView(C1)("matrawlongdesc").ToString
                        objRow("mrrawno") = dtView(C1)("mrrawno").ToString
                        Dim dQty As Double = ToDouble(dtView(C1)("mrqty").ToString)
                        Dim dQty1 As Double = ToDouble(dtView(C1)("mrqty_unitkecil").ToString)
                        Dim dQty2 As Double = ToDouble(dtView(C1)("mrqty_unitbesar").ToString)
                        objRow("piqty") = dQty
                        objRow("piqty_unitkecil") = dQty1
                        objRow("piqty_unitbesar") = dQty2
                        objRow("piunitoid") = dtView(C1)("piunitoid")
                        objRow("piunit1oid") = dtView(C1)("piunit1oid")
                        objRow("piunit2oid") = dtView(C1)("piunit2oid")
                        objRow("piunit") = dtView(C1)("piunit").ToString
                        objRow("piunit1") = dtView(C1)("piunit1").ToString
                        objRow("piunit2") = dtView(C1)("piunit2").ToString
                        Dim dPrice As Double
                        If ToDouble(piamount.Text) > 0 Then
                            dPrice = ToDouble(piamount.Text)
                        Else
                            dPrice = ToDouble(dtView(C1)("porawprice").ToString)
                        End If
                        objRow("poprice") = ToDouble(dtView(C1)("porawprice").ToString)
                        objRow("aprawprice") = dPrice
                        objRow("piprice") = "0.00"
                        Dim dAmt As Double = 0
                        If trntaxtype.SelectedValue = "INCLUDE" Then
                            dAmt = dQty * dPrice
                            objRow("aprawdtlamt") = dAmt
                        Else
                            dAmt = dQty * dPrice
                            objRow("aprawdtlamt") = Math.Round(dAmt, MidpointRounding.AwayFromZero)
                        End If
                        Dim sType As String
                        If ToDouble(piamount.Text) > 0 Then
                            sType = DDLDiscType.SelectedValue
                        Else
                            sType = dtView(C1)("porawdtldisctype").ToString
                        End If
                        objRow("aprawdtldisctype") = sType
                        If ToDouble(amtdisc.Text) > 0 Then
                            dDisc = ToDouble(amtdisc.Text)
                        Else
                            dDisc = ToDouble(dtView(C1)("porawdtldiscvalue").ToString)
                        End If
                        objRow("aprawdtldiscvalue") = dDisc
                        Dim dDiscAmt As Double = dDisc * dQty
                        If sType = "P" Then
                            dDiscAmt = dAmt * dDisc / 100
                        End If
                        objRow("aprawdtldiscamt") = dDiscAmt
                        If trntaxtype.SelectedValue = "INCLUDE" Then
                            objRow("aprawdtlnetto") = dAmt - dDiscAmt
                        Else
                            objRow("aprawdtlnetto") = Math.Round(dAmt - dDiscAmt, MidpointRounding.AwayFromZero)
                        End If
                        If dtView(C1)("porawtaxtype").ToString.ToUpper = "TAX" Then
                            objRow("aprawdtltaxamt") = ((dAmt - dDiscAmt) * ToDouble(dtView(C1)("porawtaxamt").ToString)) / 100
                        Else
                            objRow("aprawdtltaxamt") = 0
                        End If
                        For i As Int64 = 0 To dvCek.Count - 1
                            If dtView(C1).Item("mrrawmstoid").ToString = dvCek(i)("mrrawmstoid") Then
                                objRow("aprawdtlnote") = dvCek(i)("aprawdtlnote").ToString
                                objRow("aprawdtlres1") = dvCek(i)("fakturno").ToString
                                objRow("aprawdtlres2") = dvCek(i)("fakturpajakno").ToString
                            End If
                        Next
                        objRow("batchnumber") = dtView(C1)("batchnumber").ToString
                        objTable.Rows.Add(objRow)
                    End If
                    dv.RowFilter = ""
                Next
                Session("TblDtl") = objTable
                gvTblDtl.DataSource = objTable
                gvTblDtl.DataBind()
                CountHdrAmount()
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                aprawsupptotal.Text = aprawgrandtotal.Text
                ClearDetail()
            Else
                Session("WarningListMat") = "Please select LPB to add to list!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        Else
            Session("WarningListMat") = "Please select LPB to add to list!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub


    Protected Sub amtdisc_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Text = ToMaskEdit(ToDouble(amtdisc.Text), iRoundDigit)
        CountAllDtlAmount()
    End Sub

    Protected Sub DDLDiscType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CountAllDtlAmount()
    End Sub
#End Region

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trnbelimst2 SET trnbelimstres3='" & Tchar(aprawfakturno.Text) & "', trnbelimasafakturdate='" & aprawmasapajak.Text & "', apsupptotal=" & ToDouble(aprawsupptotal.Text) & " WHERE trnbelimstoid=" & aprawmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            showMessage(ex.ToString, 1)
            conn.Close()
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnAP2.aspx?awal=true")
    End Sub
End Class