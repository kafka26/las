Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Structure VarUsageValue
    Public Val_IDR As Double
    Public Val_USD As Double
End Structure

Partial Class Transaction_MaterialTransformation
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetTextValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If transformdtl1refoid.Text = "" Then
            sError &= "- Please select MATERIAL field!<BR>"
        End If
        If transformdtl1qty.Text = "" Then
            sError &= "- Please fill QTY field!<BR>"
        Else
            If ToDouble(transformdtl1qty.Text) <= 0 Then
                sError &= "- QTY must be more than 0!<BR>"
            Else
                If ToDouble(transformdtl1qty.Text) > ToDouble(transformdtl1stockqty.Text) Then
                    sError &= "- QTY must be less than STOCK QTY!<BR>"
                Else
                    Dim sErrReply As String = ""
                    If Not isLengthAccepted("transformdtl1qty", "QL_trntransformdtl1", ToDouble(transformdtl1qty.Text), sErrReply) Then
                        sError &= "- QTY must be less than MAX QTY (" & sErrReply & ") allowed stored in database!<BR>"
                    Else
                        If Not IsQtyRounded(ToDouble(transformdtl1qty.Text), ToDouble(transformdtl1limitqty.Text)) Then
                            sError &= "- QTY must be rounded by ROUNDING QTY!<BR>"
                        End If
                    End If
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsDetailOutputValid() As Boolean
        Dim sError As String = ""
        If transformdtl2refoid.Text = "" Then
            sError &= "- Please select MATERIAL field!<BR>"
        End If
        If transformdtl2qty.Text = "" Then
            sError &= "- Please fill QTY field!<BR>"
        Else
            If ToDouble(transformdtl2qty.Text) <= 0 Then
                sError &= "- QTY must be more than 0!<BR>"
            Else
                Dim sErrReply As String = ""
                If Not isLengthAccepted("transformdtl2qty", "QL_trntransformdtl2", ToDouble(transformdtl2qty.Text), sErrReply) Then
                    sError &= "- QTY must be less than MAX QTY (" & sErrReply & ") allowed stored in database!<BR>"
                Else
                    'If Not IsQtyRounded(ToDouble(transformdtl2qty.Text), ToDouble(transformdtl2limitqty.Text)) Then
                    '    sError &= "- QTY must be rounded by ROUNDING QTY!<BR>"
                    'End If
                End If
            End If
        End If
        If Session("TblDtlOutput") IsNot Nothing Then
            Dim objTbl As DataTable = Session("TblDtlOutput")
            If objTbl.Rows.Count > 1 Then
                sError &= "- Detail Output must 1 Finish Good!<BR>"
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If transformdate.Text = "" Then
            sError &= "- Please fill TRANSFORM DATE field!<BR>"
        Else
            If Not IsValidDate(transformdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- TRANSFORM DATE is invalid! " & sErr & "<BR>"
            Else
                If CDate(transformdate.Text) > GetServerTime() Then
                    sError &= "- TRANSFORM DATE must be less or equal than TODAY!<BR>"
                End If
            End If
        End If
        If deptoid.SelectedValue = "" Then
            sError &= "- Please select DEPARTMENT field!<BR>"
        End If
        If transformfromwhoid.SelectedValue = "" Then
            sError &= "- Please select FROM WAREHOUSE field!<BR>"
        End If
        If transformtowhoid.SelectedValue = "" Then
            sError &= "- Please select TO WAREHOUSE field!<BR>"
        End If
        If transformunitoid.SelectedValue = "" Then
            sError &= "- Please select UNIT IN field!<BR>"
        End If
        If transformtounitoid.SelectedValue = "" Then
            sError &= "- Please select UNIT OUT field!<BR>"
        End If
        If suppoid.SelectedValue = "" Then
            sError &= "- Please select GROUP NAME field!<BR>"
        End If
        Dim sErrReply As String = ""
        If suppoid.SelectedValue <> "0" Then
            If transformcostin.Text = "" And transformcostout.Text = "" Then
                sError &= "- Please fill COST IN or COST OUT field!<BR>"
            Else
                If ToDouble(transformcostin.Text) <= 0 And ToDouble(transformcostout.Text) <= 0 Then
                    sError &= "- One of COST IN or COST OUT must be more than 0!<BR>"
                Else
                    If ToDouble(transformcostin.Text) > 0 And ToDouble(transformcostout.Text) > 0 Then
                        sError &= "- Please use only one COST, COST IN or COST OUT!<BR>"
                    Else
                        If ToDouble(transformcostin.Text) > 0 Then
                            If Not isLengthAccepted("transformcostin", "QL_trntransformmst", ToDouble(transformcostin.Text), sErrReply) Then
                                sError &= "- COST IN must be less than MAX COST IN (" & sErrReply & ") allowed stored in database!<BR>"
                            End If
                        Else
                            If Not isLengthAccepted("transformcostout", "QL_trntransformmst", ToDouble(transformcostout.Text), sErrReply) Then
                                sError &= "- COST OUT must be less than MAX COST OUT (" & sErrReply & ") allowed stored in database!<BR>"
                            End If
                        End If
                    End If
                End If
            End If
        End If
      
        If Session("TblDtlInput") Is Nothing Then
            sError &= "- Please fill DETAIL INPUT!<BR>"
        Else
            Dim objTbl As DataTable = Session("TblDtlInput")
            If objTbl.Rows.Count <= 0 Then
                sError &= "- Please fill DETAIL INPUT!<BR>"
            Else
                For C1 As Integer = 0 To objTbl.Rows.Count - 1
                    If Not IsStockAvailable(CompnyCode, GetDateToPeriodAcctg(GetServerTime()), objTbl.Rows(C1).Item("transformdtl1refoid").ToString, objTbl.Rows(C1).Item("transformdtl1whoid").ToString, ToDouble(objTbl.Rows(C1).Item("transformdtl1qty_unitkecil").ToString)) Then
                        sError &= "- Material <B><STRONG>" & objTbl.Rows(C1).Item("transformdtl1reflongdesc").ToString & "</STRONG></B> must less or equal than stock QTY, stock is <B><STRONG>" & ToDouble(objTbl.Rows(C1).Item("transformdtl1stockqty").ToString) & "</STRONG></B> !<BR>"
                    End If
                Next
            End If
        End If
        If Session("TblDtlOutput") Is Nothing Then
            sError &= "- Please fill DETAIL OUTPUT!<BR>"
        Else
            Dim objTbl As DataTable = Session("TblDtlOutput")
            If objTbl.Rows.Count <= 0 Then
                sError &= "- Please fill DETAIL OUTPUT!<BR>"
            Else
                If objTbl.Rows.Count > 1 Then
                    sError &= "- Detail Output must 1 Finish Good!<BR>"
                Else
                    If Not Session("TblDtlInput") Is Nothing Then
                        Dim objTbl2 As DataTable = Session("TblDtlInput")
                        For C1 As Integer = 0 To objTbl2.Rows.Count - 1
                            If objTbl.Rows(0)("transformdtl2refoid") = objTbl2.Rows(C1)("transformdtl1refoid") Then
                                'sError &= "- Detail Output = Detail Input " & objTbl2.Rows(C1)("transformdtl1reflongdesc") & ", please check your transaction !<BR>"
                            End If
                        Next
                    End If
                End If
            End If
        End If
        If transformmstnote.Text = "" Then
            sError &= "- Please fill HEADER NOTE!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            transformmststatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function GetStockValue(ByVal sOid As String, Optional ByVal sType As String = "IDR") As Double
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        Try
            GetStockValue = ToDouble(GetStrData("select dbo.getStockValue(" & sOid & ")"))
        Catch ex As Exception
            GetStockValue = 0
        End Try
    End Function

    Private Function GetAcctgStock(ByVal sType As String) As String
        GetAcctgStock = ""
        sSql = "SELECT ISNULL(genother1,'') acctgoid FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='GROUPITEM' AND gencode='" & sType & "' AND activeflag='ACTIVE'"
        GetAcctgStock = GetStrData(sSql)
        Return GetAcctgStock
    End Function

    Private Function GetHppPercentage(ByVal sOid As String) As Double
        Dim dReturn As Double = 0
        Try
            sSql = "SELECT TOP 1 gendesc FROM QL_mstgen WHERE activeflag='ACTIVE' AND genother1='" & sOid & "' AND gengroup='HPP PERCENTAGE (%)' ORDER BY updtime DESC"
            dReturn = cKon.ambilscalar(sSql)
        Catch ex As Exception
            dReturn = 0
        End Try
        Return dReturn
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trntransformmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND transformmststatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnTransform.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql).ToString) > 0 Then
            lbInProcess.Visible = True
            lbInProcess.Text = "You have " & GetStrData(sSql).ToString & " In Process Material Transformation data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitAllDDL()
        ' Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(FilterDDLBusUnit, sSql)
        If FillDDL(DDLBusUnit, sSql) Then
            InitDDLDept()
            InitDDLWH()
        End If
        ' Fill DDL Group Name
        sSql = "SELECT personoid,personname FROM ql_mstperson WHERE cmpcode='" & CompnyCode & "'"
        'sSql = "SELECT suppoid, suppname FROM QL_mstsupp WHERE cmpcode='" & CompnyCode & "' AND suppres1='PEMBORONG' AND activeflag='ACTIVE' ORDER BY suppname"
        FillDDLWithAdditionalText(suppoid, sSql, "None", "0")

        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='UNIT' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDL(qtyoutunitoid, sSql)
    End Sub

    Private Sub InitDDLWH()
        ' Fill DDL Warehouse From & To
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='WAREHOUSE' AND genoid > 0"
        If i_u.Text = "New Data" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
        sSql &= " ORDER BY gendesc"
        FillDDL(transformfromwhoid, sSql)
        FillDDL(transformtowhoid, sSql)
    End Sub

    Private Sub InitDDLDept()
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND activeflag='ACTIVE' ORDER BY deptname"
        FillDDL(deptoid, sSql)
    End Sub

    Private Sub InitDDLMatType()
        If transformtype.SelectedValue <> "" Then
            Try
                Dim sSplit() As String = transformtype.SelectedValue.Replace("To", "-").Split("-")
                If sSplit.Length > 1 Then
                    'transformdtl1reftype.SelectedValue = RTrim(sSplit(0))
                    transformdtl2reftype.SelectedValue = LTrim(sSplit(1))
                    If transformdtl1reftype.SelectedValue = "WIP" Then
                        FilterDDLListMat1.Items(2).Enabled = False
                        FilterDDLListMat1.Items(3).Enabled = False
                    ElseIf transformdtl1reftype.SelectedValue = "RAW" Then
                        FilterDDLListMat1.Items(2).Enabled = True
                        FilterDDLListMat1.Items(3).Enabled = True
                    ElseIf transformdtl1reftype.SelectedValue = "FG" Then
                        FilterDDLListMat1.Items(2).Enabled = True
                        FilterDDLListMat1.Items(3).Enabled = True
                    Else
                        FilterDDLListMat1.Items(2).Enabled = True
                        FilterDDLListMat1.Items(3).Enabled = False
                    End If
                    If transformdtl2reftype.SelectedValue = "WIP" Then
                        FilterDDLListMat2.Items(2).Enabled = False
                    Else
                        FilterDDLListMat2.Items(2).Enabled = True
                    End If
                End If
            Catch ex As Exception
            End Try
        End If
    End Sub

    Private Sub InitDDLUnitInput()
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='UNIT' AND activeflag='ACTIVE' AND genoid IN (SELECT DISTINCT " & ItemUnit.SelectedValue & " FROM QL_mstitem i WHERE i.itemrecordstatus='ACTIVE') ORDER BY gendesc"
        FillDDL(transformunitoid, sSql)
    End Sub

    Private Sub InitDDLUnitOutput()
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='UNIT' AND activeflag='ACTIVE' AND genoid IN (SELECT DISTINCT " & ItenUnitOut.SelectedValue & " FROM QL_mstitem i WHERE i.itemrecordstatus='ACTIVE') ORDER BY gendesc"
        FillDDL(transformtounitoid, sSql)
    End Sub

	Private Sub BindTrnData(ByVal sSqlPlus As String)
		sSql = "SELECT transformmstoid, transformno, CONVERT(VARCHAR(10), transformdate, 101) AS transformdate, deptname, transformtype, transformmststatus, transformmstnote, 'False' AS checkvalue FROM QL_trntransformmst transm INNER JOIN QL_mstdept de ON de.cmpcode=transm.cmpcode AND de.deptoid=transm.deptoid WHERE"
		If Session("CompnyCode") <> CompnyCode Then
			sSql &= " transm.cmpcode='" & Session("CompnyCode") & "'"
		Else
			sSql &= " transm.cmpcode LIKE '%'"
		End If
		sSql &= sSqlPlus & " ORDER BY CONVERT(DATETIME, transformdate) DESC, transformmstoid DESC"
		Session("TblMst") = cKon.ambiltabel(sSql, "QL_trntransformmst")
		gvTRN.DataSource = Session("TblMst")
		gvTRN.DataBind()
		lblViewInfo.Visible = False
		'sSql = "SELECT transformmstoid, transformno, CONVERT(VARCHAR(10), transformdate, 101) AS transformdate, deptname, transformtype, transformmststatus, transformmstnote, 'False' AS checkvalue FROM QL_trntransformmst transm INNER JOIN QL_mstdept de ON de.cmpcode=transm.cmpcode AND de.deptoid=transm.deptoid WHERE"
		'If Session("CompnyCode") <> CompnyCode Then
		'    sSql &= " transm.cmpcode='" & Session("CompnyCode") & "'"
		'Else
		'    sSql &= " transm.cmpcode LIKE '%'"
		'End If
		'sSql &= sSqlPlus & " ORDER BY CONVERT(DATETIME, transformdate) DESC, transformmstoid DESC"
		'Session("TblMst") = cKon.ambiltabel(sSql, "QL_trntransformmst")
		'gvTRN.DataSource = Session("TblMst")
		'gvTRN.DataBind()
		'lblViewInfo.Visible = False
	End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "transformmstoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub BindMaterialInput(ByVal sFilter As String)
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())

        sSql = "SELECT DISTINCT m.itemoid AS refoid, m.itemcode AS refcode, m.itemLongDescription AS reflongdesc,m.itemgroup AS GroupItem,m.roundQty AS reflimitqty, 0.0 AS refqty, gn1.gendesc AS Unit1, '' AS refnote,'False' AS checkvalue, 'True' AS enableqty, m.createuser, m.itemgroup, m.itemUnit3, (SELECT ISNULL(genother1,'') acctgoid FROM QL_mstgen gx WHERE gx.cmpcode='" & CompnyCode & "' AND gx.gengroup='GROUPITEM' AND gx.gencode=m.itemgroup AND activeflag='ACTIVE') AS stockacctgoid, gn1.genoid AS tranformunitoid, (select dbo.getstockqty(m.itemoid, " & transformfromwhoid.SelectedValue & ")) AS refstockqty, gn1.gendesc AS stockunit FROM QL_mstitem m INNER JOIN QL_mstgen gn1 ON gn1.genoid=itemunit1 WHERE m.cmpcode='" & CompnyCode & "' " & sFilter & " ORDER BY refcode"
        FilterTextListMat1.TextMode = TextBoxMode.SingleLine
        FilterTextListMat1.Rows = 0
        Dim dtMat As DataTable = cKon.ambiltabel(sSql, "QL_crdstock")
        Dim dv As DataView = dtMat.DefaultView
        dv.RowFilter = "refstockqty>0"
        If dv.Count > 0 Then
            Session("TblListMat1") = dv.ToTable
            Session("TblListMat1View") = dv.ToTable
            gvListMat1.DataSource = dv.ToTable
            gvListMat1.DataBind()
            cProc.SetModalPopUpExtender(btnHideListMat1, pnlListMat1, mpeListMat1, True)
        Else
            showMessage("No Material data available for this time!", 2)
        End If
    End Sub

    Private Sub BindMaterialOutput()
        Dim sType As String = "FG"
        If transformtype.SelectedValue = "To WIP" Then
            sType = "WIP"
        ElseIf transformtype.SelectedValue = "To RAW" Then
            sType = "RAW"
        End If
        sSql = "SELECT m.itemoid AS refoid, m.itemCode AS refcode, m.itemLongDescription AS reflongdesc, m.roundQty AS reflimitqty, 0.0 AS refqty, '' AS refnote, 'False' AS checkvalue, 'True' AS enableqty, m.createuser, (SELECT ISNULL(genother1,'') acctgoid FROM QL_mstgen gx WHERE gx.cmpcode='" & CompnyCode & "' AND gx.gengroup='GROUPITEM' AND gx.gencode=m.itemgroup AND activeflag='ACTIVE') AS stockacctgoid, itemunit1 itemunitoid, g.gendesc itemunit FROM QL_mstitem m INNER JOIN QL_mstgen g ON g.genoid=itemunit1 WHERE m.cmpcode='" & CompnyCode & "' AND itemgroup IN ('FG', 'RAW') ORDER BY refcode"
        Dim dtMat As DataTable = cKon.ambiltabel(sSql, "QL_crdmtr")
        If dtMat.Rows.Count > 0 Then
            Session("TblListMat2") = dtMat
            Session("TblListMat2View") = dtMat
            gvListMat2.DataSource = dtMat
            gvListMat2.DataBind()
            cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, True)
        Else
            showMessage("No Material data available for this time!", 2)
        End If
    End Sub

    Private Sub UpdateCheckedMatInput()
        If Session("TblListMat1") IsNot Nothing Then
            For C1 As Integer = 0 To gvListMat1.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat1.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            Session("TblListMat1").DefaultView.RowFilter = "refoid=" & CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                            If Session("TblListMat1").DefaultView.Count > 0 Then
                                If CType(myControl, System.Web.UI.WebControls.CheckBox).Checked Then
                                    Session("TblListMat1").DefaultView(0)("checkvalue") = "True"
                                    Dim cc2 As System.Web.UI.ControlCollection = row.Cells(6).Controls
                                    Session("TblListMat1").DefaultView(0)("refqty") = ToDouble(GetTextValue(cc2))
                                    cc2 = row.Cells(7).Controls
                                    Session("TblListMat1").DefaultView(0)("refnote") = GetTextValue(cc2)
                                Else
                                    Session("TblListMat1").DefaultView(0)("checkvalue") = "False"
                                End If
                            End If
                            Session("TblListMat1").DefaultView.RowFilter = ""
                        End If
                    Next
                End If
            Next
        End If
        If Session("TblListMat1View") IsNot Nothing Then
            For C1 As Integer = 0 To gvListMat1.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat1.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            Session("TblListMat1View").DefaultView.RowFilter = "refoid=" & CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                            If Session("TblListMat1View").DefaultView.Count > 0 Then
                                If CType(myControl, System.Web.UI.WebControls.CheckBox).Checked Then
                                    Session("TblListMat1View").DefaultView(0)("checkvalue") = "True"
                                    Dim cc2 As System.Web.UI.ControlCollection = row.Cells(6).Controls
                                    Session("TblListMat1View").DefaultView(0)("refqty") = ToDouble(GetTextValue(cc2))
                                    cc2 = row.Cells(7).Controls
                                    Session("TblListMat1View").DefaultView(0)("refnote") = GetTextValue(cc2)
                                Else
                                    Session("TblListMat1View").DefaultView(0)("checkvalue") = "False"
                                End If
                            End If
                            Session("TblListMat1View").DefaultView.RowFilter = ""
                        End If
                    Next
                End If
            Next
        End If
    End Sub

    Private Sub CreateTblDetailInput()
        Dim dtlTable As DataTable = New DataTable("QL_trntransformdtl1")
        dtlTable.Columns.Add("transformdtl1seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("transformdtl1reftype", Type.GetType("System.String"))
        dtlTable.Columns.Add("transformdtl1refoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("transformdtl1refcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("transformdtl1reflongdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("transformdtl1qty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("transformdtl1qty_unitkecil", Type.GetType("System.Double"))
        dtlTable.Columns.Add("transformdtl1qty_unitbesar", Type.GetType("System.Double"))
        dtlTable.Columns.Add("transformdtl1stockqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("transformdtl1limitqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("transformdtl1note", Type.GetType("System.String"))
        dtlTable.Columns.Add("transformdtl1valueidr", Type.GetType("System.Double"))
        dtlTable.Columns.Add("transformdtl1valueusd", Type.GetType("System.Double"))
        dtlTable.Columns.Add("transformdtl1whoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("transformdtl1wh", Type.GetType("System.String"))
        dtlTable.Columns.Add("transformdtl1unitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("transformdtl1unit", Type.GetType("System.String"))
        dtlTable.Columns.Add("transformdtl1refunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("stockacctgoid", Type.GetType("System.Int32"))
        Session("TblDtlInput") = dtlTable
    End Sub

    Private Sub ClearDetailInput()
        transformdtl1seq.Text = "1"
        If Session("TblDtlInput") Is Nothing = False Then
            Dim objTable As DataTable = Session("TblDtlInput")
            If objTable.Rows.Count > 0 Then
                transformdtl1seq.Text = objTable.Rows.Count + 1
                EnableHeaderByInput(False)
            Else
                EnableHeaderByInput(True)
            End If
        Else
            EnableHeaderByInput(True)
        End If
        i_u2.Text = "New Detail"
        transformdtl1refoid.Text = ""
        transformdtl1refcode.Text = ""
        transformdtl1reflongdesc.Text = ""
        transformdtl1qty.Text = ""
        transformdtl1stockqty.Text = ""
        transformdtl1limitqty.Text = ""
        transformdtl1note.Text = ""
        transformfromwhoid.SelectedIndex = -1
        ItemUnit.SelectedIndex = -1
        transformunitoid.SelectedIndex = -1
        GVDtl1.SelectedIndex = -1
        btnSearchMat1.Visible = True
        CountTotalMatInput()
        transformdtl1qty.Enabled = True : transformdtl1qty.CssClass = "inpText"
        transformfromwhoid.CssClass = "inpText" : transformfromwhoid.Enabled = True
    End Sub

    Private Sub EnableHeaderByInput(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal : DDLBusUnit.CssClass = sCss
        transformdate.Enabled = bVal : transformdate.CssClass = sCss : imbDate.Visible = bVal
        transformtype.Enabled = bVal : transformtype.CssClass = sCss
    End Sub

    Private Sub CountTotalMatInput()
        Dim dQty As Double = 0
        If Session("TblDtlInput") IsNot Nothing Then
            For C1 As Integer = 0 To Session("TblDtlInput").Rows.Count - 1
                dQty += ToDouble(Session("TblDtlInput").Rows(C1)("transformdtl1qty").ToString)
            Next
        End If
        transformtotalqtyin.Text = ToMaskEdit(dQty, 2)
        transformtotalcostin.Text = ToMaskEdit(ToDouble(transformcostin.Text), 4)
    End Sub

    Private Sub UpdateCheckedMatOutput()
        If Session("TblListMat2") IsNot Nothing Then
            For C1 As Integer = 0 To gvListMat2.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat2.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            Session("TblListMat2").DefaultView.RowFilter = "refoid=" & CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                            If Session("TblListMat2").DefaultView.Count > 0 Then
                                If CType(myControl, System.Web.UI.WebControls.CheckBox).Checked Then
                                    Session("TblListMat2").DefaultView(0)("checkvalue") = "True"
                                    Dim cc2 As System.Web.UI.ControlCollection = row.Cells(4).Controls
                                    Session("TblListMat2").DefaultView(0)("refqty") = ToDouble(GetTextValue(cc2))
                                    cc2 = row.Cells(5).Controls
                                    Session("TblListMat2").DefaultView(0)("refnote") = GetTextValue(cc2)
                                Else
                                    Session("TblListMat2").DefaultView(0)("checkvalue") = "False"
                                End If
                            End If
                            Session("TblListMat2").DefaultView.RowFilter = ""
                        End If
                    Next
                End If
            Next
        End If
        If Session("TblListMat2View") IsNot Nothing Then
            For C1 As Integer = 0 To gvListMat2.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat2.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            Session("TblListMat2View").DefaultView.RowFilter = "refoid=" & CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                            If Session("TblListMat2View").DefaultView.Count > 0 Then
                                If CType(myControl, System.Web.UI.WebControls.CheckBox).Checked Then
                                    Session("TblListMat2View").DefaultView(0)("checkvalue") = "True"
                                    Dim cc2 As System.Web.UI.ControlCollection = row.Cells(4).Controls
                                    Session("TblListMat2View").DefaultView(0)("refqty") = ToDouble(GetTextValue(cc2))
                                    cc2 = row.Cells(5).Controls
                                    Session("TblListMat2View").DefaultView(0)("refnote") = GetTextValue(cc2)
                                Else
                                    Session("TblListMat2View").DefaultView(0)("checkvalue") = "False"
                                End If
                            End If
                            Session("TblListMat2View").DefaultView.RowFilter = ""
                        End If
                    Next
                End If
            Next
        End If
    End Sub

    Private Sub CreateTblDetailOutput()
        Dim dtlTable As DataTable = New DataTable("QL_trntransformdtl2")
        dtlTable.Columns.Add("transformdtl2seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("transformdtl2reftype", Type.GetType("System.String"))
        dtlTable.Columns.Add("transformdtl2refoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("transformdtl2refcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("transformdtl2reflongdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("transformdtl2qty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("transformdtl2qty_unitkecil", Type.GetType("System.Double"))
        dtlTable.Columns.Add("transformdtl2qty_unitbesar", Type.GetType("System.Double"))
        dtlTable.Columns.Add("transformdtl2limitqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("transformdtl2note", Type.GetType("System.String"))
        dtlTable.Columns.Add("transformdtl2unitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("transformdtl2unit", Type.GetType("System.String"))
        dtlTable.Columns.Add("transformdtl2refunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("transformdtl2whoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("transformdtl2wh", Type.GetType("System.String"))
        Session("TblDtlOutput") = dtlTable
    End Sub

    Private Sub ClearDetailOutput()
        transformdtl2seq.Text = "1"
        If Session("TblDtlOutput") Is Nothing = False Then
            Dim objTable As DataTable = Session("TblDtlOutput")
            If objTable.Rows.Count > 0 Then
                transformdtl2seq.Text = objTable.Rows.Count + 1
                EnableHeaderByOutput(False)
            Else
                EnableHeaderByOutput(True)
            End If
        Else
            EnableHeaderByOutput(True)
        End If
        i_u3.Text = "New Detail"
        transformdtl2refoid.Text = ""
        transformdtl2refcode.Text = ""
        transformdtl2reflongdesc.Text = ""
        transformdtl2qty.Text = ""
        transformdtl2limitqty.Text = ""
        transformdtl2note.Text = ""
        GVDtl2.SelectedIndex = -1
        transformtowhoid.SelectedIndex = -1
        ItenUnitOut.SelectedIndex = -1
        transformtounitoid.SelectedIndex = -1
        btnSearchMat2.Visible = True
        CountTotalMatOutput()
        transformdtl2qty.Enabled = True : transformdtl2qty.CssClass = "inpText"
    End Sub

    Private Sub EnableHeaderByOutput(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal : DDLBusUnit.CssClass = sCss
        transformtype.Enabled = bVal : transformtype.CssClass = sCss
        transformtounitoid.Enabled = bVal : transformtounitoid.CssClass = sCss
        ItenUnitOut.Enabled = bVal : ItenUnitOut.CssClass = sCss
        'transformtowhoid.CssClass = sCss : transformtowhoid.Enabled = bVal
    End Sub

    Private Sub CountTotalMatOutput()
        Dim dQty As Double = 0
        If Session("TblDtlOutput") IsNot Nothing Then
            For C1 As Integer = 0 To Session("TblDtlOutput").Rows.Count - 1
                dQty += ToDouble(Session("TblDtlOutput").Rows(C1)("transformdtl2qty_unitkecil").ToString)
            Next
        End If
        transformtotalqtyout.Text = ToMaskEdit(dQty, 4)
        transformtotalcostout.Text = ToMaskEdit(ToDouble(transformcostout.Text), 4)
    End Sub

    Private Sub GenerateNo()
        Dim sNo As String = "MT-" & Format(GetServerTime(), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(transformno, " & DefaultFormatCounter & ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trntransformmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND transformno LIKE '%" & sNo & "%'"
        transformno.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT transm.cmpcode, transm.transformmstoid, transm.periodacctg, transm.transformdate, transm.transformno, transm.deptoid, transm.transformtype, transm.transformfromwhoid, transm.transformtowhoid, transm.transformtotalqtyin, transm.transformtotalqtyout, transm.transformunitoid, transm.transformtounitoid, transm.suppoid, transm.transformcostin, transm.transformcostout, transm.transformtotalcostin, transm.transformtotalcostout, transm.transformmstnote, transm.transformmststatus, transm.createuser, transm.createtime, transm.upduser, transm.updtime FROM QL_trntransformmst transm WHERE transm.transformmstoid=" & sOid
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = xreader("cmpcode").ToString
                DDLBusUnit_SelectedIndexChanged(Nothing, Nothing)
                transformmstoid.Text = xreader("transformmstoid").ToString
                periodacctg.Text = xreader("periodacctg").ToString
                transformdate.Text = Format(xreader("transformdate"), "MM/dd/yyyy")
                transformno.Text = xreader("transformno").ToString
                deptoid.SelectedValue = xreader("deptoid").ToString
                transformtype.SelectedValue = xreader("transformtype").ToString
                transformtype_SelectedIndexChanged(Nothing, Nothing)
                transformtotalqtyin.Text = ToMaskEdit(ToDouble(xreader("transformtotalqtyin").ToString), 4)
                transformtotalqtyout.Text = ToMaskEdit(ToDouble(xreader("transformtotalqtyout").ToString), 4)
                transformunitoid.SelectedValue = xreader("transformunitoid").ToString
                transformtounitoid.SelectedValue = xreader("transformtounitoid").ToString
                suppoid.SelectedValue = xreader("suppoid").ToString
                transformcostin.Text = ToMaskEdit(ToDouble(xreader("transformcostin").ToString), 4)
                transformcostout.Text = ToMaskEdit(ToDouble(xreader("transformcostout").ToString), 4)
                transformtotalcostin.Text = ToMaskEdit(ToDouble(xreader("transformtotalcostin").ToString), 4)
                transformtotalcostout.Text = ToMaskEdit(ToDouble(xreader("transformtotalcostout").ToString), 4)
                transformmstnote.Text = xreader("transformmstnote").ToString.Trim
                transformmststatus.Text = xreader("transformmststatus").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
                qtyoutunitoid.SelectedValue = xreader("updtime").ToString
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False : btnPost.Visible = False
            Exit Sub
        End Try
        If transformmststatus.Text = "Post" Or transformmststatus.Text = "Closed" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnPost.Visible = False
            btnAddToList1.Visible = False
            GVDtl1.Columns(0).Visible = False
            GVDtl1.Columns(GVDtl1.Columns.Count - 1).Visible = False
            lblTrnNo.Text = "Transform No."
            transformmstoid.Visible = False
            transformno.Visible = True
            btnAddToList2.Visible = False
            GVDtl2.Columns(0).Visible = False
            GVDtl2.Columns(GVDtl1.Columns.Count - 1).Visible = False
        End If

        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(transformdate.Text))
        ' Generate Detail Input
        sSql = "SELECT transformdtl1seq, transformdtl1reftype, transformdtl1refoid, itemcode AS transformdtl1refcode, m.itemLongDescription AS transformdtl1reflongdesc, transformdtl1qty, transformdtl1qty_unitkecil, transformdtl1qty_unitbesar,  (select dbo.getstockqty(transformdtl1refoid, transformdtl1res3)) AS transformdtl1stockqty, m.roundqty AS transformdtl1limitqty, transformdtl1note, transformdtl1valueidr, transformdtl1valueusd, (SELECT ISNULL(genother1,'') acctgoid FROM QL_mstgen gx WHERE gx.cmpcode='" & CompnyCode & "' AND gx.gengroup='GROUPITEM' AND gx.gencode=m.itemgroup AND activeflag='ACTIVE') AS stockacctgoid, transformdtl1res3 AS transformdtl1whoid, g1.gendesc transformdtl1wh, transformdtl1res1 AS transformdtl1refunit, transformdtl1res2 AS transformdtl1unitoid, g.gendesc AS transformdtl1unit FROM QL_trntransformdtl1 transd1 INNER JOIN QL_mstitem m ON itemoid=transformdtl1refoid INNER JOIN QL_mstgen g ON g.genoid=transformdtl1res2 INNER JOIN QL_mstgen g1 ON g1.genoid=transformdtl1res3 WHERE transformmstoid=" & sOid & " ORDER BY transformdtl1seq"
        Session("TblDtlInput") = cKon.ambiltabel(sSql, "QL_trntransformdtl1")
        GVDtl1.DataSource = Session("TblDtlInput")
        GVDtl1.DataBind()
        ClearDetailInput()

        ' Generate Detail Output
        sSql = "SELECT transformdtl2seq, transformdtl2reftype, transformdtl2refoid, itemcode AS transformdtl2refcode, m.itemLongDescription AS transformdtl2reflongdesc, transformdtl2qty, transformdtl2qty_unitkecil, transformdtl2qty_unitbesar,m.roundqty AS transformdtl2limitqty, transformdtl2note, (SELECT ISNULL(genother1,'') acctgoid FROM QL_mstgen gx WHERE gx.cmpcode='" & CompnyCode & "' AND gx.gengroup='GROUPITEM' AND gx.gencode=m.itemgroup AND activeflag='ACTIVE') AS stockacctgoid, transformdtl2res1 AS transformdtl2refunit, transformdtl2res2 AS transformdtl2unitoid, g.gendesc AS transformdtl2unit, transformdtl2res3 AS transformdtl2whoid, g1.gendesc transformdtl2wh FROM QL_trntransformdtl2 transd2 INNER JOIN QL_mstitem m ON itemoid=transformdtl2refoid INNER JOIN QL_mstgen g ON g.genoid=transformdtl2res2 INNER JOIN QL_mstgen g1 ON g1.genoid=transformdtl2res3 WHERE transformmstoid=" & sOid & " ORDER BY transformdtl2seq"

        Session("TblDtlOutput") = cKon.ambiltabel(sSql, "QL_trntransformdtl2")
        GVDtl2.DataSource = Session("TblDtlOutput")
        GVDtl2.DataBind()
        ClearDetailOutput()
    End Sub

    Private Sub PrintReport()
        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("transformmstoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            report.Load(Server.MapPath(folderReport & "rptTransform.rpt"))
            Dim sWhere As String = ""
            If sOid = "" Then
                sWhere &= " WHERE " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If Session("CompnyCode") <> CompnyCode Then
                    sWhere &= " AND transm.cmpcode='" & Session("CompnyCode") & "'"
                End If
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND transformdate>='" & FilterPeriod1.Text & " 00:00:00' AND transformdate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbBusUnit.Checked Then
                    If FilterDDLBusUnit.SelectedValue <> "" Then
                        sWhere &= " AND transm.cmpcode='" & FilterDDLBusUnit.SelectedValue & "'"
                    End If
                End If
                If cbType.Checked Then
                    sWhere &= " AND transformtype='" & FilterDDLType.SelectedValue & "'"
                End If
                If cbStatus.Checked Then
                    sWhere &= " AND transformmststatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Transaction\trnTransform.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND transm.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sWhere &= " WHERE transm.transformmstoid IN (" & sOid & ")"
            End If
            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "MatTransformationPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
        Response.Redirect("~\Transaction\trnTransform.aspx?awal=true")
    End Sub

    Private Sub SetUsageValue(ByRef objVal As VarUsageValue)
        objVal.Val_IDR = 0
        objVal.Val_USD = 0
        Dim dt As DataTable = Session("TblDtlInput")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            Dim dValIDR As Double = GetStockValue(dt.Rows(C1)("transformdtl1refoid").ToString)
            Dim dValUSD As Double = GetStockValue(dt.Rows(C1)("transformdtl1refoid").ToString, "USD")
            dt.Rows(C1)("transformdtl1valueidr") = dValIDR
            dt.Rows(C1)("transformdtl1valueusd") = dValUSD
            objVal.Val_IDR += ToDouble(dt.Rows(C1)("transformdtl1qty_unitkecil").ToString) * dValIDR
            objVal.Val_USD += ToDouble(dt.Rows(C1)("transformdtl1qty_unitkecil").ToString) * dValUSD
        Next
    End Sub

    Private Sub InitDDLCat1()
        'Fill DDL Category 1
        sSql = "SELECT cat1oid, cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat1res1='" & transformdtl1reftype.SelectedValue & "'"
        If FillDDL(DDLCat01, sSql) Then
            InitDDLCat2()
        Else
            DDLCat02.Items.Clear()
            DDLCat03.Items.Clear()
            DDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat2()
        'Fill DDL Category 2
        sSql = "SELECT cat2oid, cat2shortdesc FROM QL_mstcat2 c2 INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c1.cat1oid=c2.cat1oid WHERE c2.cmpcode='" & CompnyCode & "' AND c2.activeflag='ACTIVE' AND c1.cat1oid='" & DDLCat01.SelectedValue & "' AND cat2res1='" & transformdtl1reftype.SelectedValue & "'"
        If FillDDL(DDLCat02, sSql) Then
            InitDDLCat3()
        Else
            DDLCat03.Items.Clear()
            DDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat3()
        'Fill DDL Category 3
        sSql = "SELECT cat3oid, cat3shortdesc FROM QL_mstcat3 c3 INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c3.cmpcode AND c1.cat1oid=c3.cat1oid WHERE c3.cmpcode='" & CompnyCode & "' AND c3.activeflag='ACTIVE' AND c2.cat2oid='" & DDLCat02.SelectedValue & "' AND cat3res1='" & transformdtl1reftype.SelectedValue & "'"
        If FillDDL(DDLCat03, sSql) Then
            InitDDLCat4()
        Else
            DDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat4()
        'Fill DDL Category 3
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE gengroup='CAT4' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDL(DDLCat04, sSql)
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnTransform.aspx")
        End If
        If checkPagePermission("~\Transaction\trnTransform.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Material Transformation"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPost.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            CheckStatus()
            InitAllDDL()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            ItemUnit_SelectedIndexChanged(Nothing, Nothing)
            ItenUnitOut_SelectedIndexChanged(Nothing, Nothing)
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                transformmstoid.Text = GenerateID("QL_TRNTRANSFORMMST", CompnyCode)
                transformdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                periodacctg.Text = GetDateToPeriodAcctg(CDate(transformdate.Text))
                transformmststatus.Text = "In Process"
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
                InitDDLMatType()
            End If
        End If
        If Not Session("TblDtlInput") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtlInput")
            GVDtl1.DataSource = dt
            GVDtl1.DataBind()
        End If
        If Not Session("TblDtlOutput") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtlOutput")
            GVDtl2.DataSource = dt
            GVDtl2.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListMat1") Is Nothing And Session("WarningListMat1") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat1") Then
                Session("WarningListMat1") = Nothing
                mpeListMat1.Show()
            End If
        End If
        If Not Session("WarningListMat2") Is Nothing And Session("WarningListMat2") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat2") Then
                Session("WarningListMat2") = Nothing
                mpeListMat2.Show()
            End If
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnTransform.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lbInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbBusUnit.Checked = False
        FilterDDLBusUnit.SelectedIndex = -1
        cbType.Checked = False
        FilterDDLType.SelectedIndex = -1
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, transm.updtime, GETDATE()) > " & nDays & " AND transformmststatus='In Process' "
        If checkPagePermission("~\Transaction\trnTransform.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND transm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
		If cbPeriode.Checked Then
			If IsValidPeriod() Then
				sSqlPlus &= " AND transformdate>='" & FilterPeriod1.Text & " 00:00:00' AND transformdate<='" & FilterPeriod2.Text & " 23:59:59'"
			Else
				Exit Sub
			End If
		End If
		If cbBusUnit.Checked Then
			If FilterDDLBusUnit.SelectedValue <> "" Then
				sSqlPlus &= " AND transm.cmpcode='" & FilterDDLBusUnit.SelectedValue & "'"
			End If
		End If
		If cbType.Checked Then
			sSqlPlus &= " AND transformtype='" & FilterDDLType.SelectedValue & "'"
		End If
		If cbStatus.Checked Then
			sSqlPlus &= " AND transformmststatus='" & FilterDDLStatus.SelectedValue & "'"
		End If
        If checkPagePermission("~\Transaction\trnTransform.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND transm.createuser='" & Session("UserID") & "'"
        End If
		BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbBusUnit.Checked = False
        FilterDDLBusUnit.SelectedIndex = -1
        cbType.Checked = False
        FilterDDLType.SelectedIndex = -1
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Transaction\trnTransform.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND transm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLDept()
        InitDDLWH()
    End Sub

    Protected Sub transformtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles transformtype.SelectedIndexChanged
        InitDDLMatType()
    End Sub

    Protected Sub transformcostin_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles transformcostin.TextChanged
        transformcostin.Text = ToMaskEdit(ToDouble(transformcostin.Text), 4)
        transformtotalcostin.Text = ToMaskEdit(ToDouble(transformcostin.Text), 4)
    End Sub

    Protected Sub transformcostout_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles transformcostout.TextChanged
        transformcostout.Text = ToMaskEdit(ToDouble(transformcostout.Text), 4)
        transformtotalcostout.Text = ToMaskEdit(ToDouble(transformcostout.Text), 4)
    End Sub

    Protected Sub btnSearchMat1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat1.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        Dim sErr As String = ""
        If transformdate.Text = "" Then
            showMessage("Please fill Transform Date first!", 2)
            Exit Sub
        Else
            If Not IsValidDate(transformdate.Text, "MM/dd/yyyy", sErr) Then
                showMessage("Transform Date is invalid. " & sErr, 2)
                Exit Sub
            End If
        End If
        If transformfromwhoid.SelectedValue = "" Then
            showMessage("Please select From Warehouse first!", 2)
            Exit Sub
        End If
        If transformunitoid.SelectedValue = "" Then
            'showMessage("Please select Unit In first!", 2)
            'Exit Sub
        End If
        FilterDDLListMat1.SelectedIndex = -1 : FilterTextListMat1.Text = ""
        Session("TblListMat1") = Nothing : Session("TblListMat1View") = Nothing
        gvListMat1.DataSource = Nothing : gvListMat1.DataBind() : InitDDLCat1()
        cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False
        'cbCat04.Checked = False
		BindMaterialInput("")
    End Sub

    Protected Sub btnFindListMat1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat1.Click
        UpdateCheckedMatInput()
        If Session("TblListMat1") IsNot Nothing Then
			Dim sFilter As String = ""
			Dim Filter As String = ""
            If cbCat03.Checked And DDLCat03.SelectedValue <> "" Then
                Filter &= " AND c3.cat3oid='" & DDLCat03.SelectedValue & "'"
            End If
			If cbCat02.Checked And DDLCat02.SelectedValue <> "" Then
                Filter &= " AND c2.cat2oid='" & DDLCat02.SelectedValue & "'"
			End If
			If cbCat01.Checked And DDLCat01.SelectedValue <> "" Then
                Filter &= " AND c1.cat1oid='" & DDLCat01.SelectedValue & "'"
			End If
			Filter = " AND " & FilterDDLListMat1.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat1.Text) & "%'"
			BindMaterialInput(Filter)

			Session("TblListMat1").DefaultView.RowFilter = sFilter
			If Session("TblListMat1").DefaultView.Count > 0 Then
				Session("TblListMat1View") = Session("TblListMat1").DefaultView.ToTable
				gvListMat1.DataSource = Session("TblListMat1View")
				gvListMat1.DataBind()
				Session("TblListMat1").DefaultView.RowFilter = ""
				mpeListMat1.Show()
			Else
				Session("WarningListMat1") = "Material data can't be found!"
				showMessage(Session("WarningListMat1"), 2)
				Session("TblListMat1").DefaultView.RowFilter = ""
			End If
		Else
			Session("WarningListMat1") = "Please show Material data first!"
			showMessage(Session("WarningListMat1"), 2)
		End If
	End Sub

    Protected Sub btnAllListMat1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat1.Click
        UpdateCheckedMatInput()
        If Session("TblListMat1") IsNot Nothing Then
            FilterDDLListMat1.SelectedIndex = -1 : FilterTextListMat1.Text = "" : gvListMat1.SelectedIndex = -1 : InitDDLCat1()
            cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
            Session("TblListMat1View") = Session("TblListMat1")
            gvListMat1.DataSource = Session("TblListMat1View")
            gvListMat1.DataBind()
            mpeListMat1.Show()
        Else
            Session("WarningListMat1") = "Please show Material data first!"
            showMessage(Session("WarningListMat1"), 2)
        End If
    End Sub

    Protected Sub gvListMat1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat1.PageIndexChanging
        UpdateCheckedMatInput()
        gvListMat1.PageIndex = e.NewPageIndex
        gvListMat1.DataSource = Session("TblListMat1View")
        gvListMat1.DataBind()
        mpeListMat1.Show()
    End Sub

    Protected Sub gvListMat1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(5).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 4)
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub cbListMatHdr_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim bVal As Boolean = sender.Checked
        For C1 As Integer = 0 To gvListMat1.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat1.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = bVal
                    End If
                Next
            End If
        Next
        mpeListMat1.Show()
    End Sub

    Protected Sub lbAddToListMat1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListMat1.Click
        UpdateCheckedMatInput()
        If Session("TblListMat1") IsNot Nothing Then
            Session("TblListMat1").DefaultView.RowFilter = "checkvalue='True'"
            If Session("TblListMat1").DefaultView.Count > 0 Then
                Dim iCount As Integer = Session("TblListMat1").DefaultView.Count
                Session("TblListMat1").DefaultView.RowFilter = "checkvalue='True' AND refqty > 0"
                If Session("TblListMat1").DefaultView.Count <> iCount Then
                    Session("WarningListMat1") = "Every selected Material data must be more than 0!"
                    showMessage(Session("WarningListMat1"), 2)
                    Session("TblListMat1").DefaultView.RowFilter = "" : Exit Sub
                End If
                'Session("TblListMat1").DefaultView.RowFilter = "checkvalue='True' AND refqty <= refstockqty"
                'If Session("TblListMat1").DefaultView.Count <> iCount Then
                    'Session("WarningListMat1") = "Every selected Material data must be less than Stock Qty!"
                    'showMessage(Session("WarningListMat1"), 2)
                    'Session("TblListMat1").DefaultView.RowFilter = "" : Exit Sub
                'End If
                If Not IsQtyRounded(Session("TblListMat1").DefaultView, "refqty", "reflimitqty") Then
                    Session("WarningListMat1") = "Every selected Material data must be rounded by Round Qty!"
                    showMessage(Session("WarningListMat1"), 2)
                    Session("TblListMat1").DefaultView.RowFilter = "" : Exit Sub
                End If
                Dim sErrReply As String = ""
                For C1 As Integer = 0 To Session("TblListMat1").DefaultView.Count - 1
                    If Not isLengthAccepted("transformdtl1qty", "QL_trntransformdtl1", ToDouble(Session("TblListMat1").DefaultView(C1)("refqty").ToString), sErrReply) Then
                        Session("WarningListMat1") = "Every selected Material data must be less than Max. Qty (" & sErrReply & ") allowed stored in database!"
                        showMessage(Session("WarningListMat1"), 2)
                        Session("TblListMat1").DefaultView.RowFilter = "" : Exit Sub
                    End If
                Next
                If Session("TblDtlInput") Is Nothing Then
                    CreateTblDetailInput()
                End If
                Dim dQty_unitkecil, dQty_unitbesar As Double
                Dim objTbl As DataTable = Session("TblDtlInput")
                Dim objView As DataView = objTbl.DefaultView
                Dim iSeq As Integer = objTbl.Rows.Count + 1
                For C1 As Integer = 0 To Session("TblListMat1").DefaultView.Count - 1
                    objView.RowFilter = "transformdtl1refoid=" & Session("TblListMat1").DefaultView(C1)("refoid")
                    If objView.Count > 0 Then
                        objView(0)("transformdtl1qty") = ToDouble(Session("TblListMat1").DefaultView(C1)("refqty").ToString)
                        GetUnitConverter(Session("TblListMat1").DefaultView(C1)("refoid"), transformunitoid.SelectedValue, ToDouble(Session("TblListMat1").DefaultView(C1)("refqty").ToString), dQty_unitkecil, dQty_unitbesar)
                        objView(0)("transformdtl1qty_unitkecil") = dQty_unitkecil
                        objView(0)("transformdtl1qty_unitbesar") = dQty_unitbesar
                        objView(0)("transformdtl1stockqty") = ToDouble(Session("TblListMat1").DefaultView(C1)("refstockqty").ToString)
                        objView(0)("transformdtl1note") = Session("TblListMat1").DefaultView(C1)("refnote").ToString
                        objView(0)("transformdtl1unitoid") = Session("TblListMat1").DefaultView(C1)("tranformunitoid").ToString
                        objView(0)("transformdtl1unit") = Session("TblListMat1").DefaultView(C1)("Unit1").ToString
                        objView(0)("transformdtl1refunit") = Session("TblListMat1").DefaultView(C1)("Unit1").ToString
                        objView(0)("transformdtl1whoid") = transformfromwhoid.SelectedValue
                        objView(0)("transformdtl1wh") = transformfromwhoid.SelectedItem.Text
                    Else
                        Dim rv As DataRowView = objView.AddNew()
                        rv.BeginEdit()
                        rv("transformdtl1seq") = iSeq
                        Dim sRefType As String = "RAW MATERIAL"
                        If transformdtl1reftype.SelectedValue.ToUpper = "FG" Then
                            sRefType = "FINISH GOOD"
                        ElseIf transformdtl1reftype.SelectedValue.ToUpper = "WIP" Then
                            sRefType = "WIP"
                        End If
                        rv("transformdtl1reftype") = sRefType
                        rv("transformdtl1refoid") = Session("TblListMat1").DefaultView(C1)("refoid")
                        rv("transformdtl1refcode") = Session("TblListMat1").DefaultView(C1)("refcode").ToString
                        rv("transformdtl1reflongdesc") = Session("TblListMat1").DefaultView(C1)("reflongdesc").ToString
                        rv("transformdtl1qty") = ToDouble(Session("TblListMat1").DefaultView(C1)("refqty").ToString)
                        GetUnitConverter(Session("TblListMat1").DefaultView(C1)("refoid"), transformunitoid.SelectedValue, ToDouble(Session("TblListMat1").DefaultView(C1)("refqty").ToString), dQty_unitkecil, dQty_unitbesar)
                        rv("transformdtl1qty_unitkecil") = dQty_unitkecil
                        rv("transformdtl1qty_unitbesar") = dQty_unitbesar
                        rv("transformdtl1stockqty") = ToDouble(Session("TblListMat1").DefaultView(C1)("refstockqty").ToString)
                        rv("transformdtl1limitqty") = ToDouble(Session("TblListMat1").DefaultView(C1)("reflimitqty").ToString)
                        rv("transformdtl1note") = Session("TblListMat1").DefaultView(C1)("refnote").ToString
                        rv("stockacctgoid") = Session("TblListMat1").DefaultView(C1)("stockacctgoid").ToString
                        rv("transformdtl1unitoid") = Session("TblListMat1").DefaultView(C1)("tranformunitoid").ToString
                        rv("transformdtl1unit") = Session("TblListMat1").DefaultView(C1)("Unit1").ToString
                        rv("transformdtl1refunit") = Session("TblListMat1").DefaultView(C1)("Unit1").ToString
                        rv("transformdtl1whoid") = transformfromwhoid.SelectedValue
                        rv("transformdtl1wh") = transformfromwhoid.SelectedItem.Text
                        rv.EndEdit()
                        iSeq += 1
                    End If
                    objView.RowFilter = ""
                Next
                Session("TblListMat1").DefaultView.RowFilter = ""
                Session("TblDtlInput") = objTbl
                GVDtl1.DataSource = Session("TblDtlInput")
                GVDtl1.DataBind()
                ClearDetailInput()
                cProc.SetModalPopUpExtender(btnHideListMat1, pnlListMat1, mpeListMat1, False)
            Else
                Session("WarningListMat1") = "Please select some Material data first!"
                showMessage(Session("WarningListMat1"), 2)
                Session("TblListMat1").DefaultView.RowFilter = ""
            End If
        Else
            Session("WarningListMat1") = "Please show Material data first!"
            showMessage(Session("WarningListMat1"), 2)
        End If
    End Sub

    Protected Sub lbSelectAllToListMat1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSelectAllToListMat1.Click
        If Session("TblListMat1View") IsNot Nothing Then
            Dim dtTbl As DataTable = Session("TblListMat1View")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblListMat1")
                Dim objView As DataView = objTbl.DefaultView
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "refoid=" & dtTbl.Rows(C1)("refoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblListMat1") = objTbl
                Session("TblListMat1View") = dtTbl
                gvListMat1.DataSource = Session("TblListMat1View")
                gvListMat1.DataBind()
                lbAddToListMat1_Click(Nothing, Nothing)
            Else
                Session("WarningListMat1") = "Please show Material data first!"
                showMessage(Session("WarningListMat1"), 2)
            End If
        Else
            Session("WarningListMat1") = "Please show Material data first!"
            showMessage(Session("WarningListMat1"), 2)
        End If
    End Sub

    Protected Sub lbCloseListMat1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMat1.Click
        cProc.SetModalPopUpExtender(btnHideListMat1, pnlListMat1, mpeListMat1, False)
    End Sub

    Protected Sub btnAddToList1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList1.Click
        If IsDetailInputValid() Then
            Dim dQty_unitkecil, dQty_unitbesar As Double
            Dim objTable As DataTable = Session("TblDtlInput")
            Dim objRow As DataRow = objTable.Rows(transformdtl1seq.Text - 1)
            objRow.BeginEdit()
            objRow("transformdtl1qty") = ToDouble(transformdtl1qty.Text)
            GetUnitConverter(transformdtl1refoid.Text, transformunitoid.SelectedValue, ToDouble(transformdtl1qty.Text), dQty_unitkecil, dQty_unitbesar)
            objRow("transformdtl1qty_unitkecil") = dQty_unitkecil
            objRow("transformdtl1qty_unitbesar") = dQty_unitbesar
            objRow("transformdtl1stockqty") = ToDouble(transformdtl1stockqty.Text)
            objRow("transformdtl1note") = transformdtl1note.Text
            objRow("transformdtl1whoid") = transformfromwhoid.SelectedValue
            objRow("transformdtl1wh") = transformfromwhoid.SelectedItem.Text
            objRow.EndEdit()
            Session("TblDtlInput") = objTable
            GVDtl1.DataSource = Session("TblDtlInput")
            GVDtl1.DataBind()
            ClearDetailInput()
        End If
    End Sub

    Protected Sub btnClear1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear1.Click
        ClearDetailInput()
    End Sub

    Protected Sub GVDtl1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVDtl1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
        End If
    End Sub

    Protected Sub GVDtl1_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVDtl1.RowDeleting
        Dim objTable As DataTable = Session("TblDtlInput")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("transformdtl1seq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtlInput") = objTable
        GVDtl1.DataSource = Session("TblDtlInput")
        GVDtl1.DataBind()
        ClearDetailInput()
    End Sub

    Protected Sub GVDtl1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDtl1.SelectedIndexChanged
        Try
            transformdtl1seq.Text = GVDtl1.SelectedDataKey.Item("transformdtl1seq").ToString
            If Session("TblDtlInput") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtlInput")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "transformdtl1seq=" & transformdtl1seq.Text
                Dim sRefName As String = "RAW"
                If dv.Item(0).Item("transformdtl1reftype").ToString = "FINISH GOOD" Then
                    sRefName = "FG"
                ElseIf dv.Item(0).Item("transformdtl1reftype").ToString = "WIP" Then
                    sRefName = "WIP"
                End If
                transformdtl1reftype.SelectedValue = sRefName
                transformdtl1refoid.Text = dv.Item(0).Item("transformdtl1refoid").ToString
                transformdtl1refcode.Text = dv.Item(0).Item("transformdtl1refcode").ToString
                transformdtl1reflongdesc.Text = dv.Item(0).Item("transformdtl1reflongdesc").ToString
                transformdtl1qty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("transformdtl1qty").ToString), 4)
                transformdtl1stockqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("transformdtl1stockqty").ToString), 4)
                transformdtl1limitqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("transformdtl1limitqty").ToString), 4)
                transformdtl1note.Text = dv.Item(0).Item("transformdtl1note").ToString
                transformfromwhoid.SelectedValue = dv.Item(0).Item("transformdtl1whoid").ToString
                transformfromwhoid.CssClass = "inpTextDisabled" : transformfromwhoid.Enabled = False
                dv.RowFilter = ""
                btnSearchMat1.Visible = False
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub btnSearchMat2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat2.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        If transformtounitoid.SelectedValue = "" Then
            'showMessage("Please select Unit Out first!", 2)
            'Exit Sub
        End If
        FilterDDLListMat2.SelectedIndex = -1 : FilterTextListMat2.Text = "" : Session("TblListMat2") = Nothing : Session("TblListMat2View") = Nothing : gvListMat2.DataSource = Nothing : gvListMat2.DataBind()
        BindMaterialOutput()
    End Sub

    Protected Sub btnFindListMat2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat2.Click
        UpdateCheckedMatOutput()
        If Session("TblListMat2") IsNot Nothing Then
            Dim sFilter As String = ""
            If transformdtl2reftype.SelectedValue.ToUpper = "LOG" Or transformdtl2reftype.SelectedValue.ToUpper = "SAWN TIMBER" Then
                If FilterDDLListMat2.SelectedIndex = 0 Then
                    Dim sText() As String = FilterTextListMat2.Text.Split(";")
                    For C1 As Integer = 0 To sText.Length - 1
                        If sText(C1) <> "" Then
                            sFilter &= FilterDDLListMat2.SelectedValue & " LIKE '" & Tchar(sText(C1)) & "' OR "
                        End If
                    Next
                    If sFilter <> "" Then
                        sFilter = Left(sFilter, sFilter.Length - 4)
                    End If
                Else
                    sFilter = FilterDDLListMat2.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat2.Text) & "%'"
                End If
            Else
                sFilter = FilterDDLListMat2.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat2.Text) & "%'"
            End If
            Session("TblListMat2").DefaultView.RowFilter = sFilter
            If Session("TblListMat2").DefaultView.Count > 0 Then
                Session("TblListMat2View") = Session("TblListMat2").DefaultView.ToTable
                gvListMat2.DataSource = Session("TblListMat2View")
                gvListMat2.DataBind()
                Session("TblListMat2").DefaultView.RowFilter = ""
                mpeListMat2.Show()
            Else
                Session("WarningListMat2") = "Material data can't be found!"
                showMessage(Session("WarningListMat2"), 2)
                Session("TblListMat2").DefaultView.RowFilter = ""
            End If
        Else
            Session("WarningListMat2") = "Please show Material data first!"
            showMessage(Session("WarningListMat2"), 2)
        End If
    End Sub

    Protected Sub btnAllListMat2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat2.Click
        UpdateCheckedMatOutput()
        If Session("TblListMat2") IsNot Nothing Then
            FilterDDLListMat2.SelectedIndex = -1 : FilterTextListMat2.Text = "" : gvListMat2.SelectedIndex = -1
            Session("TblListMat2View") = Session("TblListMat2")
            gvListMat2.DataSource = Session("TblListMat2View")
            gvListMat2.DataBind()
            mpeListMat2.Show()
        Else
            Session("WarningListMat2") = "Please show Material data first!"
            showMessage(Session("WarningListMat2"), 2)
        End If
    End Sub

    Protected Sub gvListMat2_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat2.PageIndexChanging
        UpdateCheckedMatOutput()
        gvListMat2.PageIndex = e.NewPageIndex
        gvListMat2.DataSource = Session("TblListMat2View")
        gvListMat2.DataBind()
        mpeListMat2.Show()
    End Sub

    Protected Sub gvListMat2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat2.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(4).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 4)
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub cbListMatHdr2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim bVal As Boolean = sender.Checked
        For C1 As Integer = 0 To gvListMat2.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat2.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = bVal
                    End If
                Next
            End If
        Next
        mpeListMat2.Show()
    End Sub

    Protected Sub lbAddToListMat2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListMat2.Click
        UpdateCheckedMatOutput()
        If Session("TblListMat2") IsNot Nothing Then
            Session("TblListMat2").DefaultView.RowFilter = "checkvalue='True'"
            If Session("TblListMat2").DefaultView.Count > 0 Then
                Dim iCount As Integer = Session("TblListMat2").DefaultView.Count
                Session("TblListMat2").DefaultView.RowFilter = "checkvalue='True' AND refqty > 0"
                If Session("TblListMat2").DefaultView.Count <> iCount Then
                    Session("WarningListMat2") = "Every selected Material data must be more than 0!"
                    showMessage(Session("WarningListMat2"), 2)
                    Session("TblListMat2").DefaultView.RowFilter = "" : Exit Sub
                End If
                If iCount > 1 Then
                    Session("WarningListMat2") = "Output Detail must be 1 Finish Good!"
                    showMessage(Session("WarningListMat2"), 2)
                    Session("TblListMat2").DefaultView.RowFilter = "" : Exit Sub
                End If
                If Not IsQtyRounded(Session("TblListMat2").DefaultView, "refqty", "reflimitqty") Then
                    Session("WarningListMat2") = "Every selected Material data must be rounded by Round Qty!"
                    showMessage(Session("WarningListMat2"), 2)
                    Session("TblListMat2").DefaultView.RowFilter = "" : Exit Sub
                End If
                Dim sErrReply As String = ""
                For C1 As Integer = 0 To Session("TblListMat2").DefaultView.Count - 1
                    If Not isLengthAccepted("transformdtl2qty", "QL_trntransformdtl2", ToDouble(Session("TblListMat2").DefaultView(C1)("refqty").ToString), sErrReply) Then
                        Session("WarningListMat2") = "Every selected Material data must be less than Max. Qty (" & sErrReply & ") allowed stored in database!"
                        showMessage(Session("WarningListMat2"), 2)
                        Session("TblListMat2").DefaultView.RowFilter = "" : Exit Sub
                    End If
                Next
                If Session("TblDtlOutput") Is Nothing Then
                    CreateTblDetailOutput()
                End If
                Dim objTbl As DataTable = Session("TblDtlOutput")
                Dim dQty_unitkecil, dQty_unitbesar As Double
                Dim objView As DataView = objTbl.DefaultView
                Dim iSeq As Integer = objTbl.Rows.Count + 1
                For C1 As Integer = 0 To Session("TblListMat2").DefaultView.Count - 1
                    Dim dCodeOut As String = "FINISH GOOD"
                    If transformtype.ToString.ToUpper = "TO WIP" Then
                        dCodeOut = "WIP"
                    ElseIf transformtype.ToString.ToUpper = "TO RAW" Then
                        dCodeOut = "RAW MATERIAL"
                    End If

                    objView.RowFilter = "transformdtl2refoid=" & Session("TblListMat2").DefaultView(C1)("refoid")
                    If objView.Count > 0 Then
                        objView(0)("transformdtl2qty") = ToDouble(Session("TblListMat2").DefaultView(C1)("refqty").ToString)
                        GetUnitConverter(Session("TblListMat2").DefaultView(C1)("refoid"), transformtounitoid.SelectedValue, ToDouble(Session("TblListMat2").DefaultView(C1)("refqty").ToString), dQty_unitkecil, dQty_unitbesar)
                        objView(0)("transformdtl2qty_unitkecil") = dQty_unitkecil
                        objView(0)("transformdtl2qty_unitbesar") = dQty_unitbesar
                        objView(0)("transformdtl2note") = Session("TblListMat2").DefaultView(C1)("refnote").ToString
                        objView(0)("transformdtl2unitoid") = Session("TblListMat2").DefaultView(C1)("itemunitoid").ToString
                        objView(0)("transformdtl2unit") = Session("TblListMat2").DefaultView(C1)("itemunit").ToString
                        objView(0)("transformdtl2refunit") = Session("TblListMat2").DefaultView(C1)("itemunit").ToString
                        objView(0)("transformdtl2whoid") = transformtowhoid.SelectedValue
                        objView(0)("transformdtl2wh") = transformtowhoid.SelectedItem.Text
                    Else
                        Dim rv As DataRowView = objView.AddNew()
                        rv.BeginEdit()
                        rv("transformdtl2seq") = iSeq
                        rv("transformdtl2reftype") = dCodeOut
                        rv("transformdtl2refoid") = Session("TblListMat2").DefaultView(C1)("refoid")
                        rv("transformdtl2refcode") = Session("TblListMat2").DefaultView(C1)("refcode").ToString
                        rv("transformdtl2reflongdesc") = Session("TblListMat2").DefaultView(C1)("reflongdesc").ToString
                        rv("transformdtl2qty") = ToDouble(Session("TblListMat2").DefaultView(C1)("refqty").ToString)
                        GetUnitConverter(Session("TblListMat2").DefaultView(C1)("refoid"), transformtounitoid.SelectedValue, ToDouble(Session("TblListMat2").DefaultView(C1)("refqty").ToString), dQty_unitkecil, dQty_unitbesar)
                        objView(0)("transformdtl2qty_unitkecil") = dQty_unitkecil
                        objView(0)("transformdtl2qty_unitbesar") = dQty_unitbesar
                        rv("transformdtl2limitqty") = ToDouble(Session("TblListMat2").DefaultView(C1)("reflimitqty").ToString)
                        rv("transformdtl2note") = Session("TblListMat2").DefaultView(C1)("refnote").ToString
                        rv("transformdtl2unitoid") = Session("TblListMat2").DefaultView(C1)("itemunitoid").ToString
                        rv("transformdtl2unit") = Session("TblListMat2").DefaultView(C1)("itemunit").ToString
                        rv("transformdtl2refunit") = Session("TblListMat2").DefaultView(C1)("itemunit").ToString
                        rv("transformdtl2whoid") = transformtowhoid.SelectedValue
                        rv("transformdtl2wh") = transformtowhoid.SelectedItem.Text
                        rv.EndEdit()
                        iSeq += 1
                    End If
                    objView.RowFilter = ""
                Next
                Session("TblListMat2").DefaultView.RowFilter = ""
                Session("TblDtlOutput") = objTbl
                GVDtl2.DataSource = Session("TblDtlOutput")
                GVDtl2.DataBind()
                ClearDetailOutput()
                cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, False)
            Else
                Session("WarningListMat2") = "Please select some Material data first!"
                showMessage(Session("WarningListMat2"), 2)
                Session("TblListMat2").DefaultView.RowFilter = ""
            End If
        Else
            Session("WarningListMat2") = "Please show Material data first!"
            showMessage(Session("WarningListMat2"), 2)
        End If
    End Sub

    Protected Sub lbSelectAllToListMat2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSelectAllToListMat2.Click
        If Session("TblListMat2View") IsNot Nothing Then
            Dim dtTbl As DataTable = Session("TblListMat2View")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblListMat2")
                Dim objView As DataView = objTbl.DefaultView
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "refoid=" & dtTbl.Rows(C1)("refoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblListMat2") = objTbl
                Session("TblListMat2View") = dtTbl
                gvListMat2.DataSource = Session("TblListMat2View")
                gvListMat2.DataBind()
                lbAddToListMat2_Click(Nothing, Nothing)
            Else
                Session("WarningListMat2") = "Please show Material data first!"
                showMessage(Session("WarningListMat2"), 2)
            End If
        Else
            Session("WarningListMat2") = "Please show Material data first!"
            showMessage(Session("WarningListMat2"), 2)
        End If
    End Sub

    Protected Sub lbCloseListMat2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMat2.Click
        cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, False)
    End Sub

    Protected Sub btnAddToList2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList2.Click
        If IsDetailOutputValid() Then
            Dim dQty_unitkecil, dQty_unitbesar As Double
            Dim objTable As DataTable = Session("TblDtlOutput")
            Dim objRow As DataRow = objTable.Rows(transformdtl2seq.Text - 1)
            objRow.BeginEdit()
            objRow("transformdtl2qty") = ToDouble(transformdtl2qty.Text)
            GetUnitConverter(transformdtl2refoid.Text, transformtounitoid.SelectedValue, ToDouble(transformdtl2qty.Text), dQty_unitkecil, dQty_unitbesar)
            objRow("transformdtl2qty_unitkecil") = dQty_unitkecil
            objRow("transformdtl2qty_unitbesar") = dQty_unitbesar
            objRow("transformdtl2note") = transformdtl2note.Text
            objRow("transformdtl2whoid") = transformtowhoid.SelectedValue
            objRow("transformdtl2wh") = transformtowhoid.SelectedItem.Text
            objRow.EndEdit()
            Session("TblDtlOutput") = objTable
            GVDtl2.DataSource = Session("TblDtlOutput")
            GVDtl2.DataBind()
            ClearDetailOutput()
        End If
    End Sub

    Protected Sub btnClear2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear2.Click
        ClearDetailOutput()
    End Sub

    Protected Sub GVDtl2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVDtl2.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
        End If
    End Sub

    Protected Sub GVDtl2_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVDtl2.RowDeleting
        Dim objTable As DataTable = Session("TblDtlOutput")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("transformdtl2seq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtlOutput") = objTable
        GVDtl2.DataSource = Session("TblDtlOutput")
        GVDtl2.DataBind()
        ClearDetailOutput()
    End Sub

    Protected Sub GVDtl2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDtl2.SelectedIndexChanged
        Try
            transformdtl2seq.Text = GVDtl2.SelectedDataKey.Item("transformdtl2seq").ToString
            If Session("TblDtlOutput") Is Nothing = False Then
                i_u3.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtlOutput")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "transformdtl2seq=" & transformdtl2seq.Text
                Dim sRefName As String = "WIP"
                If dv.Item(0).Item("transformdtl1reftype").ToString = "FINISH GOOD" Then
                    sRefName = "FG"
                End If
                transformdtl2reftype.SelectedValue = sRefName
                transformdtl2refoid.Text = dv.Item(0).Item("transformdtl2refoid").ToString
                transformdtl2refcode.Text = dv.Item(0).Item("transformdtl2refcode").ToString
                transformdtl2reflongdesc.Text = dv.Item(0).Item("transformdtl2reflongdesc").ToString
                transformdtl2qty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("transformdtl2qty").ToString), 4)
                transformdtl2limitqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("transformdtl2limitqty").ToString), 4)
                transformdtl2note.Text = dv.Item(0).Item("transformdtl2note").ToString
                transformtowhoid.SelectedValue = dv.Item(0).Item("transformdtl2whoid").ToString
                transformtowhoid.CssClass = "inpTextDisabled" : transformtowhoid.Enabled = False
                dv.RowFilter = ""
                btnSearchMat2.Visible = False
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                If CheckDataExists("SELECT COUNT(*) FROM QL_trntransformmst WHERE transformmstoid=" & transformmstoid.Text) Then
                    transformmstoid.Text = GenerateID("QL_TRNTRANSFORMMST", CompnyCode)
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNTRANSFORMMST", "transformmstoid", transformmstoid.Text, "transformmststatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    transformmststatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            transformdtl1oid.Text = GenerateID("QL_TRNTRANSFORMDTL1", CompnyCode)
            transformdtl2oid.Text = GenerateID("QL_TRNTRANSFORMDTL2", CompnyCode)
            Dim iConOid As Integer = 0, iCrdOid As Integer = 0, iGLMstOid As Integer = 0, iGLDtlOid As Integer = 0, iStockOutAcctgOid As Integer = 0, iTransformAcctgOid As Integer = 0, iStockValOid As Integer = 0, iBOPBebanAcctgOid As Integer = 0, iBOPRealAcctgOid As Integer = 0
            Dim cRate As New ClassRate
            periodacctg.Text = GetDateToPeriodAcctg(CDate(transformdate.Text))
            Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
            Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
            Dim objValue As VarUsageValue
            If transformmststatus.Text = "Post" Then
                cRate.SetRateValue("IDR", transformdate.Text)
                If cRate.GetRateDailyLastError <> "" Then
                    showMessage(cRate.GetRateDailyLastError, 2)
                    transformmststatus.Text = "In Process"
                    Exit Sub
                End If
                If cRate.GetRateMonthlyLastError <> "" Then
                    showMessage(cRate.GetRateMonthlyLastError, 2)
                    transformmststatus.Text = "In Process"
                    Exit Sub
                End If
				Dim sVarErr As String = "", sVarIn As String = "VAR_STOCK", sVarOut As String = "VAR_STOCK"
                If transformdtl1reftype.SelectedValue.ToUpper <> "" Then
                    sVarOut = "VAR_STOCK"
                End If
                If Not IsInterfaceExists(sVarIn, DDLBusUnit.SelectedValue) Then
                    sVarErr = sVarIn
                End If
                If Not IsInterfaceExists(sVarOut, DDLBusUnit.SelectedValue) Then
                    sVarErr &= IIf(sVarErr = "", "", ", ") & sVarOut
                End If
                If Not IsInterfaceExists("VAR_COST_TRANSFORM", DDLBusUnit.SelectedValue) Then
                    sVarErr &= IIf(sVarErr = "", "", ", ") & "VAR_COST_TRANSFORM"
                End If
                If sVarErr <> "" Then
                    showMessage(GetInterfaceWarning(sVarErr, "posting"), 2)
                    transformmststatus.Text = "In Process"
                    Exit Sub
                End If
                Dim sRefCodeOut As String = "WIP"
                If transformtype.SelectedValue = "To FG" Then
                    sRefCodeOut = "FG"
                ElseIf transformtype.SelectedValue = "To RAW" Then
                    sRefCodeOut = "RAW"
                End If
                iStockOutAcctgOid = GetAcctgStock(sRefCodeOut)
                iBOPBebanAcctgOid = GetAcctgOID(GetVarInterface("VAR_BOP_BEBAN", CompnyCode), CompnyCode)
                iBOPRealAcctgOid = GetAcctgOID(GetVarInterface("VAR_BOP_REAL", CompnyCode), CompnyCode)
                iTransformAcctgOid = GetAcctgOID(GetVarInterface("VAR_COST_TRANSFORM", DDLBusUnit.SelectedValue), CompnyCode)
                GenerateNo()
                iConOid = GenerateID("QL_CONSTOCK", CompnyCode)
                iCrdOid = GenerateID("QL_CRDSTOCK", CompnyCode)
                iGLMstOid = GenerateID("QL_TRNGLMST", CompnyCode)
                iGLDtlOid = GenerateID("QL_TRNGLDTL", CompnyCode)
                iStockValOid = GenerateID("QL_STOCKVALUE", CompnyCode)
                SetUsageValue(objValue)
                objValue.Val_IDR = objValue.Val_IDR + ToDouble(transformtotalcostin.Text) + ToDouble(transformtotalcostout.Text)
                objValue.Val_USD = objValue.Val_USD + ((ToDouble(transformtotalcostin.Text) + ToDouble(transformtotalcostout.Text)) * cRate.GetRateMonthlyUSDValue)
            End If
            ' Define Tabel Strucure utk Auto Jurnal 
            sSql = "SELECT '' tipe, 0 acctgoid, 0.0 debet, 0.0 credit, '' refno, '' note, '' AS glother1 "
            Dim tbPostGL As DataTable = cKon.ambiltabel(sSql, "PostGL")
            tbPostGL.Rows.Clear()
            Dim oRow As DataRow
            Dim dvCek As DataView = tbPostGL.DefaultView

            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trntransformmst (cmpcode, transformmstoid, periodacctg, transformdate, transformno, deptoid, transformtype, transformfromwhoid, transformtowhoid, transformtotalqtyin, transformtotalqtyout, transformunitoid, transformtounitoid, suppoid, transformcostin, transformcostout, transformtotalcostin, transformtotalcostout, curroid, rateoid, rate2oid, transformmstnote, transformmststatus, createuser, createtime, upduser, updtime) VALUES ('" & DDLBusUnit.SelectedValue & "', " & transformmstoid.Text & ", '" & periodacctg.Text & "', '" & transformdate.Text & "', '" & transformno.Text & "', " & deptoid.SelectedValue & ", '" & transformtype.SelectedValue & "', " & transformfromwhoid.SelectedValue & ", " & transformtowhoid.SelectedValue & ", " & ToDouble(transformtotalqtyin.Text) & ", " & ToDouble(transformtotalqtyout.Text) & ", " & transformunitoid.SelectedValue & ", " & transformtounitoid.SelectedValue & ", " & suppoid.SelectedValue & ", " & ToDouble(transformcostin.Text) & ", " & ToDouble(transformcostout.Text) & ", " & ToDouble(transformtotalcostin.Text) & ", " & ToDouble(transformtotalcostout.Text) & ", " & cRate.GetCurrOid & ", " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", '" & Tchar(transformmstnote.Text) & "', '" & transformmststatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & transformmstoid.Text & " WHERE tablename='QL_TRNTRANSFORMMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trntransformmst SET periodacctg='" & periodacctg.Text & "', transformdate='" & transformdate.Text & "', transformno='" & transformno.Text & "', deptoid=" & deptoid.SelectedValue & ", transformtype='" & transformtype.SelectedValue & "', transformfromwhoid=" & transformfromwhoid.SelectedValue & ", transformtowhoid=" & transformtowhoid.SelectedValue & ", transformtotalqtyin=" & ToDouble(transformtotalqtyin.Text) & ", transformtotalqtyout=" & ToDouble(transformtotalqtyout.Text) & ", transformunitoid=" & transformunitoid.SelectedValue & ", transformtounitoid=" & transformtounitoid.SelectedValue & ", suppoid=" & suppoid.SelectedValue & ", transformcostin=" & ToDouble(transformcostin.Text) & ", transformcostout=" & ToDouble(transformcostout.Text) & ", transformtotalcostin=" & ToDouble(transformtotalcostin.Text) & ", transformtotalcostout=" & ToDouble(transformtotalcostout.Text) & ", curroid=" & cRate.GetCurrOid & ", rateoid=" & cRate.GetRateDailyOid & ", rate2oid=" & cRate.GetRateMonthlyOid & ", transformmstnote='" & Tchar(transformmstnote.Text) & "', transformmststatus='" & transformmststatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND transformmstoid=" & transformmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
          
                    sSql = "DELETE FROM QL_trntransformdtl2 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND transformmstoid=" & transformmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trntransformdtl1 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND transformmstoid=" & transformmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtlInput") Is Nothing Then
                    Dim dtInput As DataTable = Session("TblDtlInput")
                    For C1 As Int16 = 0 To dtInput.Rows.Count - 1
                        Dim InQty As Integer = 0
                        sSql = "INSERT INTO QL_trntransformdtl1 (cmpcode, transformdtl1oid, transformmstoid, transformdtl1seq, transformdtl1reftype, transformdtl1refoid, transformdtl1qty, transformdtl1note, transformdtl1status, upduser, updtime, transformdtl1valueidr, transformdtl1valueusd, transformdtl1qty_unitkecil, transformdtl1qty_unitbesar, transformdtl1res1, transformdtl1res2, transformdtl1res3) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C1 + CInt(transformdtl1oid.Text)) & ", " & transformmstoid.Text & ", " & C1 + 1 & ", '" & dtInput.Rows(C1).Item("transformdtl1reftype").ToString & "', " & dtInput.Rows(C1).Item("transformdtl1refoid") & ", " & ToDouble(dtInput.Rows(C1).Item("transformdtl1qty").ToString) & ", '" & Tchar(dtInput.Rows(C1).Item("transformdtl1note").ToString) & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dtInput.Rows(C1).Item("transformdtl1valueidr").ToString) & ", " & ToDouble(dtInput.Rows(C1).Item("transformdtl1valueusd").ToString) & ", " & ToDouble(dtInput.Rows(C1).Item("transformdtl1qty_unitkecil").ToString) & ", " & ToDouble(dtInput.Rows(C1).Item("transformdtl1qty_unitbesar").ToString) & ", '" & dtInput.Rows(C1).Item("transformdtl1refunit") & "', " & dtInput.Rows(C1).Item("transformdtl1unitoid") & ", " & dtInput.Rows(C1).Item("transformdtl1whoid") & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        If transformmststatus.Text = "Post" Then
                            ' Insert Con Stock Out
                            sSql = "INSERT INTO  QL_constock(cmpcode,constockoid,contype,trndate,formaction,formoid,periodacctg,refname,refoid,mtrlocoid,qtyin,qtyout,amount,hpp,typemin,note,reason,createuser,createtime,upduser,updtime,refno,deptoid,valueidr,valueusd, qtyin_unitbesar, qtyout_unitbesar) VALUES " & _
                           " ('" & DDLBusUnit.SelectedValue & "'," & iConOid & ",'MTI','" & sDate & "','QL_trntransformdtl1'," & transformmstoid.Text & ",'" & sPeriod & "','" & dtInput.Rows(C1).Item("transformdtl1reftype") & "'," & dtInput.Rows(C1).Item("transformdtl1refoid") & "," & dtInput.Rows(C1).Item("transformdtl1whoid") & ", 0," & ToDouble(dtInput.Rows(C1).Item("transformdtl1qty_unitkecil").ToString) & ", 0, 0,-1,'" & transformno.Text & "','Material Transformation Input','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'',0," & ToDouble(dtInput.Rows(C1).Item("transformdtl1valueidr").ToString) & "," & ToDouble(dtInput.Rows(C1).Item("transformdtl1valueusd").ToString) & ", 0," & ToDouble(dtInput.Rows(C1).Item("transformdtl1qty_unitbesar").ToString) & ")"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iConOid += 1
                            ' Update Crd Stock Out
                            sSql = "UPDATE QL_crdstock SET qtyout = qtyout + " & ToDouble(dtInput.Rows(C1).Item("transformdtl1qty_unitkecil").ToString) & ", saldoakhir = saldoakhir - " & ToDouble(dtInput.Rows(C1).Item("transformdtl1qty_unitkecil").ToString) & ", qtyout_unitbesar = qtyout_unitbesar + " & ToDouble(dtInput.Rows(C1).Item("transformdtl1qty_unitbesar").ToString) & ", saldoakhir_unitbesar = saldoakhir_unitbesar - " & ToDouble(dtInput.Rows(C1).Item("transformdtl1qty_unitbesar").ToString) & ", lasttranstype='QL_trntransformdtl1', lasttransdate='" & sDate & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND refoid=" & dtInput.Rows(C1).Item("transformdtl1refoid") & " AND mtrlocoid=" & dtInput.Rows(C1).Item("transformdtl1whoid") & " AND periodacctg='" & sPeriod & "'"
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() <= 0 Then
                                ' Insert Crd Stock Out
                                sSql = "INSERT INTO QL_crdstock(cmpcode,crdstockoid,periodacctg,refoid,refname,mtrlocoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir,lasttranstype,lasttransdate,createuser,createtime,upduser,updtime,closeuser,closingdate,refno, qtyin_unitbesar, qtyout_unitbesar, qtyadjin_unitbesar, qtyadjout_unitbesar, saldoawal_unitbesar, saldoakhir_unitbesar) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iCrdOid & ", '" & sPeriod & "', " & dtInput.Rows(C1).Item("transformdtl1refoid") & ", '" & dtInput.Rows(C1).Item("transformdtl1reftype").ToString & "', " & dtInput.Rows(C1).Item("transformdtl1whoid") & ", 0, " & ToDouble(dtInput.Rows(C1).Item("transformdtl1qty_unitkecil").ToString) & ", 0, 0, 0, " & -ToDouble(dtInput.Rows(C1).Item("transformdtl1qty_unitkecil").ToString) & ", 'QL_trntransformdtl1', '" & sDate & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "',CURRENT_TIMESTAMP, '', '1/1/1900', '', 0, " & ToDouble(dtInput.Rows(C1).Item("transformdtl1qty_unitbesar").ToString) & ", 0, 0, 0, " & -ToDouble(dtInput.Rows(C1).Item("transformdtl1qty_unitbesar").ToString) & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iCrdOid += 1
                            End If

                            dvCek.RowFilter = "acctgoid=" & dtInput.Rows(C1).Item("stockacctgoid").ToString & " "
                            If dvCek.Count > 0 Then
                                dvCek(0)("debet") += ToDouble(dtInput.Rows(C1).Item("transformdtl1valueidr").ToString) * ToDouble(dtInput.Rows(C1).Item("transformdtl1qty_unitkecil").ToString)
                                dvCek.RowFilter = ""
                            Else
                                dvCek.RowFilter = ""

                                oRow = tbPostGL.NewRow
                                oRow("acctgoid") = dtInput.Rows(C1).Item("stockacctgoid")
                                oRow("credit") = 0
                                oRow("debet") = ToDouble(dtInput.Rows(C1).Item("transformdtl1valueidr").ToString) * ToDouble(dtInput.Rows(C1).Item("transformdtl1qty_unitkecil").ToString)

                                tbPostGL.Rows.Add(oRow)
                            End If
                            tbPostGL.AcceptChanges()

                            'Insert STockvalue
                            sSql = GetQueryUpdateStockValue(-ToDouble(dtInput.Rows(C1)("transformdtl1qty_unitkecil").ToString), -ToDouble(dtInput.Rows(C1)("transformdtl1qty_unitbesar").ToString), ToDouble(dtInput.Rows(C1).Item("transformdtl1valueidr").ToString), ToDouble(dtInput.Rows(C1).Item("transformdtl1valueusd").ToString), "QL_trntransformdtl1", sDate, Session("UserID"), DDLBusUnit.SelectedValue, sPeriod, dtInput.Rows(C1)("transformdtl1refoid"))
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() = 0 Then
                                sSql = GetQueryInsertStockValue(-ToDouble(dtInput.Rows(C1)("transformdtl1qty_unitkecil").ToString), -ToDouble(dtInput.Rows(C1)("transformdtl1qty_unitbesar").ToString), ToDouble(dtInput.Rows(C1).Item("transformdtl1valueidr").ToString), ToDouble(dtInput.Rows(C1).Item("transformdtl1valueusd").ToString), "QL_trntransformdtl1", sDate, Session("UserID"), DDLBusUnit.SelectedValue, sPeriod, dtInput.Rows(C1)("transformdtl1refoid"), iStockValOid)
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iStockValOid += 1
                            End If
                        End If
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (dtInput.Rows.Count - 1 + CInt(transformdtl1oid.Text)) & " WHERE tablename='QL_TRNTRANSFORMDTL1' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtlOutput") Is Nothing Then
                    Dim dtOutput As DataTable = Session("TblDtlOutput")
                    Dim dValIDRPerUnit As Double = objValue.Val_IDR / ToDouble(transformtotalqtyout.Text)
                    Dim dValUSDPerUnit As Double = objValue.Val_USD / ToDouble(transformtotalqtyout.Text)
                    For C1 As Int16 = 0 To dtOutput.Rows.Count - 1
                        Dim OutQty As Integer = 0
                        sSql = "INSERT INTO QL_trntransformdtl2 (cmpcode, transformdtl2oid, transformmstoid, transformdtl2seq, transformdtl2reftype, transformdtl2refoid, transformdtl2qty, transformdtl2note, transformdtl2status, upduser, updtime, transformdtl2valueidr, transformdtl2valueusd, transformdtl2qty_unitkecil, transformdtl2qty_unitbesar, transformdtl2res1, transformdtl2res2, transformdtl2res3) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C1 + CInt(transformdtl2oid.Text)) & ", " & transformmstoid.Text & ", " & C1 + 1 & ", '" & dtOutput.Rows(C1).Item("transformdtl2reftype").ToString & "', " & dtOutput.Rows(C1).Item("transformdtl2refoid") & ", " & ToDouble(dtOutput.Rows(C1).Item("transformdtl2qty").ToString) & ", '" & Tchar(dtOutput.Rows(C1).Item("transformdtl2note").ToString) & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dValIDRPerUnit & ", " & dValUSDPerUnit & ", " & ToDouble(dtOutput.Rows(C1).Item("transformdtl2qty_unitkecil").ToString) & ", " & ToDouble(dtOutput.Rows(C1).Item("transformdtl2qty_unitbesar").ToString) & ", '" & dtOutput.Rows(C1).Item("transformdtl2refunit") & "', " & dtOutput.Rows(C1).Item("transformdtl2unitoid") & ", " & dtOutput.Rows(C1).Item("transformdtl2whoid") & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()

                        If transformmststatus.Text = "Post" Then
                            ' Insert Con Stock In
                            sSql = "INSERT INTO  QL_constock (cmpcode,constockoid,contype,trndate,formaction,formoid,periodacctg,refname,refoid,mtrlocoid,qtyin,qtyout,amount,hpp,typemin,note,reason,createuser,createtime,upduser,updtime,refno,deptoid,valueidr,valueusd, qtyin_unitbesar, qtyout_unitbesar)" & _
                            " VALUES ('" & DDLBusUnit.SelectedValue & "', " & iConOid & ",'MTO','" & sDate & "','QL_trntransformdtl2'," & transformmstoid.Text & ",'" & sPeriod & "','" & dtOutput.Rows(C1).Item("transformdtl2reftype").ToString & "', " & dtOutput.Rows(C1).Item("transformdtl2refoid") & "," & dtOutput.Rows(C1).Item("transformdtl2whoid") & "," & ToDouble(dtOutput.Rows(C1).Item("transformdtl2qty_unitkecil").ToString) & ",0,0,0,1, '" & transformno.Text & "-" & Tchar(transformmstnote.Text) & "','Material Transformation Output','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'',0, " & dValIDRPerUnit & ", " & dValUSDPerUnit & ", " & ToDouble(dtOutput.Rows(C1).Item("transformdtl2qty_unitbesar").ToString) & ", 0)"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iConOid += 1
                            ' Update Crd Stock In
                            sSql = "UPDATE QL_crdstock SET qtyin = qtyin + " & ToDouble(dtOutput.Rows(C1).Item("transformdtl2qty_unitkecil").ToString) & ", saldoakhir = saldoakhir + " & ToDouble(dtOutput.Rows(C1).Item("transformdtl2qty_unitkecil").ToString) & ", qtyin_unitbesar = qtyin_unitbesar + " & ToDouble(dtOutput.Rows(C1).Item("transformdtl2qty_unitbesar").ToString) & ", saldoakhir_unitbesar = saldoakhir_unitbesar + " & ToDouble(dtOutput.Rows(C1).Item("transformdtl2qty_unitbesar").ToString) & ", lasttranstype='QL_trntransformdtl2', lasttransdate='" & sDate & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND refoid=" & dtOutput.Rows(C1).Item("transformdtl2refoid") & " AND mtrlocoid=" & dtOutput.Rows(C1).Item("transformdtl2whoid") & " AND periodacctg='" & sPeriod & "'"
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() <= 0 Then
                                ' Insert Crd Stock In
                                sSql = "INSERT INTO QL_crdstock(cmpcode,crdstockoid,periodacctg,refoid,refname,mtrlocoid,qtyin,qtyout,qtyadjin,qtyadjout,saldoawal,saldoakhir,lasttranstype,lasttransdate,createuser,createtime,upduser,updtime,closeuser,closingdate,refno,qtyin_unitbesar,qtyout_unitbesar,qtyadjin_unitbesar,qtyadjout_unitbesar,saldoawal_unitbesar,saldoakhir_unitbesar)" & _
                                " VALUES ('" & DDLBusUnit.SelectedValue & "', " & iCrdOid & ",'" & sPeriod & "'," & dtOutput.Rows(C1).Item("transformdtl2refoid") & ",'" & dtOutput.Rows(C1).Item("transformdtl2reftype").ToString & "'," & dtOutput.Rows(C1).Item("transformdtl2whoid") & "," & ToDouble(dtOutput.Rows(C1).Item("transformdtl2qty_unitkecil").ToString) & ",0,0,0,0," & ToDouble(dtOutput.Rows(C1).Item("transformdtl2qty_unitkecil").ToString) & ",'QL_trntransformdtl2','" & sDate & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'', '1/1/1900',''," & ToDouble(dtOutput.Rows(C1).Item("transformdtl2qty_unitbesar").ToString) & ",0,0,0,0," & ToDouble(dtOutput.Rows(C1).Item("transformdtl2qty_unitbesar").ToString) & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iCrdOid += 1
                            End If
                            'Insert STockvalue
                            sSql = GetQueryUpdateStockValue(ToDouble(dtOutput.Rows(C1)("transformdtl2qty_unitkecil").ToString), ToDouble(dtOutput.Rows(C1)("transformdtl2qty_unitbesar").ToString), dValIDRPerUnit, dValUSDPerUnit, "QL_trntransformdtl2", sDate, Session("UserID"), DDLBusUnit.SelectedValue, sPeriod, dtOutput.Rows(C1)("transformdtl2refoid"))
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() = 0 Then
                                sSql = GetQueryInsertStockValue(ToDouble(dtOutput.Rows(C1)("transformdtl2qty_unitkecil").ToString), ToDouble(dtOutput.Rows(C1)("transformdtl2qty_unitbesar").ToString), dValIDRPerUnit, dValUSDPerUnit, "QL_trntransformdtl2", sDate, Session("UserID"), DDLBusUnit.SelectedValue, sPeriod, dtOutput.Rows(C1)("transformdtl2refoid"), iStockValOid)
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iStockValOid += 1
                            End If
                        End If
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (dtOutput.Rows.Count - 1 + CInt(transformdtl2oid.Text)) & " WHERE tablename='QL_TRNTRANSFORMDTL2' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
					xCmd.ExecuteNonQuery()
                End If
                If transformmststatus.Text = "Post" Then
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iConOid - 1 & " WHERE tablename='QL_CONSTOCK' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iCrdOid - 1 & " WHERE tablename='QL_CRDSTOCK' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iStockValOid - 1 & " WHERE tablename='QL_STOCKVALUE' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    If objValue.Val_IDR > 0 Then
                        ' Insert GL MST
                        sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGLMstOid & ", '" & sDate & "', '" & sPeriod & "', 'Transform|No. " & transformno.Text & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 0, 0, 1, 1, " & objValue.Val_USD / objValue.Val_IDR & ", " & objValue.Val_USD / objValue.Val_IDR & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()

                        Dim iSeq As Integer = 1
                        ' Insert GL DTL
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGLDtlOid & ", " & iSeq & ", " & iGLMstOid & ", " & iStockOutAcctgOid & ", 'D', " & objValue.Val_IDR & ", '" & transformno.Text & "', 'Transform|No. " & transformno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & objValue.Val_IDR & ", " & objValue.Val_USD & ", 'QL_trntransformmst " & transformmstoid.Text & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iGLDtlOid += 1
                        iSeq += 1

                        For C1 As Integer = 0 To tbPostGL.Rows.Count - 1
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGLDtlOid & ", " & iSeq & ", " & iGLMstOid & ", " & tbPostGL.Rows(C1)("acctgoid").ToString & ", 'C', " & ToDouble(tbPostGL.Rows(C1)("debet").ToString) & ", '" & transformno.Text & "', 'Transform|No. " & transformno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(tbPostGL.Rows(C1)("debet").ToString) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(tbPostGL.Rows(C1)("debet").ToString) * cRate.GetRateMonthlyUSDValue & ", 'QL_trntransformmst " & transformmstoid.Text & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iGLDtlOid += 1
                            iSeq += 1
                        Next

                        If ToDouble(transformtotalcostin.Text) + ToDouble(transformtotalcostout.Text) > 0 Then
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGLDtlOid & ", " & iSeq & ", " & iGLMstOid & ", " & iTransformAcctgOid & ", 'C', " & ToDouble(transformtotalcostin.Text) + ToDouble(transformtotalcostout.Text) & ", '" & transformno.Text & "', 'Transform|No. " & transformno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(transformtotalcostin.Text) + ToDouble(transformtotalcostout.Text) & ", " & (ToDouble(transformtotalcostin.Text) + ToDouble(transformtotalcostout.Text)) * cRate.GetRateMonthlyUSDValue & ", 'QL_trntransformmst " & transformmstoid.Text & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iGLDtlOid += 1
                        End If
                        sSql = "UPDATE QL_mstoid SET lastoid=" & iGLMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        sSql = "UPDATE QL_mstoid SET lastoid=" & iGLDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                End If
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        transformmststatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    transformmststatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                conn.Close()
                showMessage(ex.Message, 1)
                transformmststatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & transformmstoid.Text & ".<BR>"
            End If
            If transformmststatus.Text = "Post" Then
                Session("SavedInfo") = "Data have been posted with Transform No. = " & transformno.Text & "."
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trnTransform.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnTransform.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If transformmstoid.Text.Trim = "" Then
            showMessage("Please select Material Transformation data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNTRANSFORMMST", "transformmstoid", transformmstoid.Text, "transformmststatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                transformmststatus.Text = "In Process"
                Exit Sub
            End If
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE FROM QL_trntransformdtl2 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND transformmstoid=" & transformmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trntransformdtl1 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND transformmstoid=" & transformmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trntransformmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND transformmstoid=" & transformmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnTransform.aspx?awal=true")
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPost.Click
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        If isPeriodClosed(CompnyCode, CDate(sDate)) Then
            showMessage("Period " & GetDateToPeriodAcctg(CDate(sDate)) & " Have been Already CLOSED, Please CANCEL this transaction and try again!", 2)
            Exit Sub
        End If
        transformmststatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        UpdateCheckedValue()
        PrintReport()
    End Sub

    Protected Sub DDLCat01_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLCat01.SelectedIndexChanged
        InitDDLCat2()
        mpeListMat1.Show()
    End Sub

    Protected Sub DDLCat02_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLCat02.SelectedIndexChanged
        InitDDLCat3()
        mpeListMat1.Show()
    End Sub

    Protected Sub DDLCat03_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLCat03.SelectedIndexChanged
        InitDDLCat4()
        mpeListMat1.Show()
    End Sub

    Protected Sub ItemUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitDDLUnitInput()
    End Sub

    Protected Sub ItenUnitOut_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitDDLUnitOutput()
    End Sub
#End Region

End Class