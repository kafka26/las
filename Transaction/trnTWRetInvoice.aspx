<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnTWRetInvoice.aspx.vb" Inherits="Transaction_trnTWRetInvoice" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" colspan="2" style="width: 940px; height: 3px" valign="center">
                <asp:Label ID="Label1" runat="server" CssClass="Title" ForeColor="#804000" Text=".: Transfer Return Invoice"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Transfer Warehouse (Invoice) :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
<asp:Panel id="Panel1" runat="server" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="Label15" runat="server" Text="Filter"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="105px"><asp:ListItem Value="retitemmstoid">Draft No.</asp:ListItem>
<asp:ListItem Value="retitemmstno">No. Transaksi</asp:ListItem>
<asp:ListItem Enabled="False" Value="transdocrefno">Doc. Ref. No.</asp:ListItem>
<asp:ListItem Enabled="False" Value="name">Customer/Supplier</asp:ListItem>
<asp:ListItem Enabled="False" Value="transmstnote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="300px"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text="Period"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="80px" ToolTip="(MM/dd/yyyy)"></asp:TextBox> <asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label5" runat="server" Text="to"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="80px" ToolTip="(MM/dd/yyyy)"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="lbldate1" runat="server" Text="(MM/dd/yyyy)" CssClass="Important"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="105px"><asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label22" runat="server" Text="Print Option" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="DDLPrintOption" runat="server" CssClass="inpText" Width="232px" Visible="False"><asp:ListItem>SURAT JALAN</asp:ListItem>
<asp:ListItem>INVOICE</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" TargetControlID="FilterPeriod1" PopupButtonID="imbDate1" Format="MM/dd/yyyy">
                                                        </ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" TargetControlID="FilterPeriod2" PopupButtonID="imbDate2" Format="MM/dd/yyyy">
                                                        </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="FilterPeriod1" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999">
                                                        </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" TargetControlID="FilterPeriod2" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999">
                                                        </ajaxToolkit:MaskedEditExtender> <asp:DropDownList id="FilterDDL2" runat="server" CssClass="inpText" Width="105px" Visible="False">
                                                            <asp:ListItem Value="[Draft No.]">Draft No.</asp:ListItem>
                                                            <asp:ListItem Value="[Transfer No.]">Transfer No.</asp:ListItem>
                                                            <asp:ListItem Value="[Doc. Ref. No.]">Doc. Ref. No.</asp:ListItem>
                                                            <asp:ListItem Value="[Header Note]">Note</asp:ListItem>
                                                        </asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:LinkButton id="lbInProcess" runat="server"></asp:LinkButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvMst" runat="server" ForeColor="#333333" Width="100%" PageSize="8" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True">
                                                            <PagerSettings FirstPageText="First" LastPageText="Last" />
                                                            <RowStyle BackColor="#EFF3FB" Font-Size="X-Small" />
                                                            <EmptyDataRowStyle BackColor="#FFFFC0" />
                                                            
                                                            
                                                            
                                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                            <PagerStyle CssClass="gvhdr" Font-Bold="True" ForeColor="White" HorizontalAlign="Right" />
                                                            <EmptyDataTemplate>
                                                                <asp:Label ID="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label>
                                                            </EmptyDataTemplate>
                                                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White" />
                                                            <EditRowStyle BackColor="#2461BF" />
                                                            <AlternatingRowStyle BackColor="White" />
                                                        </asp:GridView> <asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Transfer Warehouse (Invoice) :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label3" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Finish Good Transfer Header :" Font-Underline="False"></asp:Label> <asp:Label id="periodacctg" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Label id="i_u" runat="server" Font-Bold="True" Text="New Data" CssClass="Important"></asp:Label></TD><TD class="Label" align=left></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD style="WIDTH: 50px" class="Label" align=left><asp:Label id="lblspuno" runat="server" Text="Draft No"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:Label id="retitemmstoid" runat="server"></asp:Label><asp:TextBox id="retitemmstno" runat="server" CssClass="inpTextDisabled" Width="200px" Visible="False" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label6" runat="server" Text="Retur Date" Width="80px"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="retitemmstdate" runat="server" CssClass="inpTextDisabled" Width="80px" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 50px" class="Label" align=left><asp:Label id="Label7" runat="server" Width="83px">No. Transfer</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="transitemno" runat="server" CssClass="inpText" Width="200px"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchTW" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseTW" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> </TD><TD class="Label" align=left><asp:Label id="Label4" runat="server" Text="Doc. Ref. No."></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="retitemdocrefno" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False" MaxLength="50"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="transitemmstoid" runat="server" Visible="False"></asp:Label> <asp:Label id="custoid" runat="server" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=4><asp:GridView id="gvTW" runat="server" ForeColor="#333333" Width="100%" PageSize="8" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True">
                                                    <PagerSettings FirstPageText="First" LastPageText="Last" />
                                                    <RowStyle BackColor="#EFF3FB" Font-Size="X-Small" />
                                                    <EmptyDataRowStyle BackColor="#FFFFC0" />
                                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle CssClass="gvhdr" Font-Bold="True" ForeColor="White" HorizontalAlign="Right" />
                                                    <EmptyDataTemplate>
                                                        <asp:Label ID="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label>
                                                    </EmptyDataTemplate>
                                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White" />
                                                    <EditRowStyle BackColor="#2461BF" />
                                                    <AlternatingRowStyle BackColor="White" />
                                                </asp:GridView> </TD></TR><TR><TD style="WIDTH: 50px" class="Label" vAlign=top align=left><asp:Label id="Label8" runat="server" Text="Header Note" Width="79px"></asp:Label></TD><TD class="Label" vAlign=top align=center>:</TD><TD class="Label" align=left><asp:TextBox id="retitemmstnote" runat="server" CssClass="inpText" Width="300px" MaxLength="100"></asp:TextBox></TD><TD id="TD3" class="Label" vAlign=top align=left runat="server"></TD><TD id="TD2" class="Label" vAlign=top align=center runat="server"></TD><TD id="TD1" class="Label" vAlign=top align=left runat="server"></TD></TR><TR><TD style="WIDTH: 50px" class="Label" align=left><asp:Label id="Label26" runat="server" Width="72px">Grand Total</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="transtotalamt" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label9" runat="server" Text="Status"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="retitemmststatus" runat="server" CssClass="inpTextDisabled" Width="80px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label25" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Finish Good Transfer Detail :" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 50px" class="Label" align=left><asp:Label id="i_u2" runat="server" ForeColor="Red" Font-Bold="True" Text="New Detail" Width="72px" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="retitemdtlseq" runat="server" Visible="False">1</asp:Label> <asp:Label id="retitemdtloid" runat="server"></asp:Label> <asp:Label id="itemoid" runat="server" Visible="False"></asp:Label> <asp:Label id="itemcode" runat="server" Visible="False"></asp:Label> <asp:Label id="transitemqty" runat="server" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:Label id="transitemdtloid" runat="server" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD style="WIDTH: 50px" class="Label" align=left><asp:Label id="Label12" runat="server" Text="Warehouse"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="retitemtowhoid" runat="server" CssClass="inpText" Width="305px">
                                                    </asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label17" runat="server" Text="From WH" Width="68px" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="retitemfromwhoid" runat="server" CssClass="inpText" Width="305px" Visible="False" AutoPostBack="True">
                                                    </asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label11" runat="server" Text="Material"></asp:Label> <asp:Label id="Label23" runat="server" ForeColor="Red" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="matlongdesc" runat="server" CssClass="inpTextDisabled" Width="280px" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSearchMat" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="Label20" runat="server" Text="Stock Qty"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:Label id="stockqty" runat="server">0</asp:Label> -&nbsp;<asp:DropDownList id="transunitoid" runat="server" CssClass="inpTextDisabled" Width="105px" Enabled="False">
                                                    </asp:DropDownList></TD></TR><TR><TD style="WIDTH: 50px" class="Label" align=left><asp:Label id="Label16" runat="server" Text="Qty"></asp:Label> <asp:Label id="Label24" runat="server" ForeColor="Red" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="retitemqty" runat="server" CssClass="inpText" Width="80px"></asp:TextBox> - <asp:DropDownList id="retitemunitoid" runat="server" CssClass="inpTextDisabled" Width="105px" Enabled="False">
                                                    </asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label13" runat="server" Text="Detail Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="retitemdtlnote" runat="server" CssClass="inpText" Width="300px" MaxLength="100"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label18" runat="server" Text="Sales Price" Width="68px"></asp:Label> <asp:Label id="Label19" runat="server" ForeColor="Red" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="retitemprice" runat="server" CssClass="inpTextDisabled" Width="100px" AutoPostBack="True" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="ftbQty" runat="server" ValidChars="0123456789,." TargetControlID="retitemqty">
                                                    </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fteretitemprice" runat="server" ValidChars="1234567890.," TargetControlID="retitemprice">
                                                    </ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:GridView id="gvDtl" runat="server" ForeColor="#333333" Width="100%" GridLines="None" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="retitemdtlseq">
                                                        <RowStyle BackColor="#EFF3FB" />
                                                        <%--<Columns>
                                                          <asp:CommandField ShowSelectButton="True">
                                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:CommandField>
                                                            <asp:BoundField DataField="retitemdtlseq" HeaderText="No.">
                                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="retitemfromwhdesc" HeaderText="From WH">
                                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="retitemtowhdesc" HeaderText="To WH" Visible="False">
                                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="itemoldcode" HeaderText="Old Code">
                                                                <HeaderStyle CssClass="gvhdr" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="itemcode" HeaderText="Code">
                                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="itemLongDescription" HeaderText="Material">
                                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            
                                                             <asp:BoundField DataField="retitemunitname" HeaderText="Unit">
                                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            
                                                            <asp:BoundField DataField="retitemqty" HeaderText="Qty">
                                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Right" />
                                                                <ItemStyle HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                        
                                                            <asp:BoundField DataField="retitemprice" HeaderText="Harga Jual">
                                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Right" />
                                                                <ItemStyle HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="retitemdtlnote" HeaderText="Note">
                                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            
                                                            <asp:CommandField DeleteText="X" ShowDeleteButton="True">
                                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Center" />
                                                                <ItemStyle Font-Bold="True" Font-Size="Small" ForeColor="Red" HorizontalAlign="Center"/>
                                                            </asp:CommandField>
                                                        </Columns>--%>
                                                        
                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <EditRowStyle BackColor="#2461BF" />
                                                        <AlternatingRowStyle BackColor="White" />
                                                    </asp:GridView> </TD></TR><TR><TD class="Label" align=center colSpan=6></TD></TR></TBODY></TABLE><TABLE width="100%"><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton><asp:ImageButton id="btnposting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD align=center><asp:UpdatePanel id="upListMat" runat="server"><ContentTemplate>
<asp:Panel id="pnlListMat" runat="server" CssClass="modalBox" Width="900px" Visible="False" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Medium" Font-Bold="True" Width="152px" Font-Underline="False">List Of Material</asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" CssClass="inpText" Width="100px">
                                                                                    <asp:ListItem Value="itemcode">Code</asp:ListItem>
                                                                                    <asp:ListItem Value="itemoldcode">Old Code</asp:ListItem>
                                                                                    <asp:ListItem Value="itemLongDescription">Description</asp:ListItem>
                                                                                </asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" CssClass="inpText" Width="300px"></asp:TextBox> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat" runat="server" ForeColor="#333333" Width="99%" PageSize="5" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><HeaderTemplate>
                                                                                                &nbsp;<asp:CheckBox ID="cbHdrListMat" runat="server" AutoPostBack="True" OnCheckedChanged="cbHdrListMat_CheckedChanged" />
                                                                                            
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="cbSelectPR" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("itemoid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemoldcode" HeaderText="Old Code" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemLongDescription" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="retitemunitname" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stockqty" HeaderText="Stok Qty">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Transfer Qty"><EditItemTemplate>
                                                                                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("registerqty") %>'></asp:TextBox>
                                                                                            
</EditItemTemplate>
<ItemTemplate>
                                                                                                <asp:TextBox ID="tbTransQty" runat="server" CssClass="inpText" Enabled='<%# eval("enableqty") %>' MaxLength="16" Text='<%# eval("retitemqty") %>' Width="50px"></asp:TextBox>
                                                                                                <ajaxToolkit:FilteredTextBoxExtender ID="ftbQtyOnPP" runat="server" TargetControlID="tbTransQty" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender>
                                                                                            
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="retitemprice" HeaderText="Harga Jual">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Keterangan"><ItemTemplate>
                                                                                                <asp:TextBox ID="tbNote" runat="server" CssClass="inpText" MaxLength="100" Text='<%# eval("retitemdtlnote") %>'
                                                                                                    Width="150px"></asp:TextBox>
                                                                                            
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                                                                                        &nbsp;
                                                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 15px" align=center colSpan=3><asp:CheckBox id="cbOpenListMatUsage" runat="server" Font-Size="8pt" Font-Bold="True" Text="Open List Of Material again after add data to list" CssClass="Important" Visible="False" Checked="True"></asp:CheckBox></TD></TR><TR><TD style="HEIGHT: 15px" align=center colSpan=3><asp:LinkButton id="lkbAddToListListMat" runat="server">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lkbSelectAllToListListMat" runat="server">[ Select All To List ]</asp:LinkButton> <asp:LinkButton id="lkbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" PopupDragHandleControlID="lblTitleListMat" PopupControlID="pnlListMat" Drag="True" BackgroundCssClass="modalBackground">
                                                            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> </TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div3" class="progressBackgroundFilter"></DIV><DIV id="Div4" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> &nbsp; </DIV>
</ContentTemplate>
                            </asp:UpdatePanel>
                            &nbsp;
                            <table width="100%">
                                <tr>
                                    <td align="center" colspan="3" valign="middle">
                            <asp:UpdatePanel ID="upPopUpMsg" runat="server">
                                <ContentTemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2><asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" PopupDragHandleControlID="lblCaption" PopupControlID="pnlPopUpMsg" Drag="True" BackgroundCssClass="modalBackground" DropShadow="True">
                                    </ajaxToolkit:ModalPopupExtender> <asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</ContentTemplate>
                            </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
    </table>

</asp:Content>

