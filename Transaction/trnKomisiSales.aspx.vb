Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_KomisiSales
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function GetSelectedOid() As String
        Dim sRet As String = ""
        For C1 As Integer = 0 To gvDtl.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvDtl.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        If CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = True Then
                            sRet &= CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip & ","
                        End If
                    End If
                Next
            End If
        Next
        If sRet <> "" Then
            sRet = Left(sRet, sRet.Length - 1)
        End If
        Return sRet
    End Function

    Private Function GetSummaryGiro(ByVal sOids As String) As DataTable
        Dim dtRet As DataTable = Nothing
        sSql = "SELECT cashbankoid, cashbankno, cashbankamt, cashbankamtidr, cashbankamtusd, acctgoid, curroid, giroacctgoid, '' AS newcashbankno FROM QL_trncashbankmst WHERE cashbankoid IN (" & sOids & ")"
        dtRet = cKon.ambiltabel(sSql, "QL_girosummary")
        Return dtRet
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitDDL()
        ' BULAN
        For C1 As Integer = 1 To 12
            Dim liMonth2 As New ListItem(MonthName(C1).ToUpper, C1)
            DDLMonth2.Items.Add(liMonth2)
        Next
        DDLMonth2.SelectedValue = GetServerTime.Month
        ' TAHUN
        For C1 As Integer = GetServerTime.Year - 5 To GetServerTime.Year + 5
            Dim liYear2 As New ListItem(C1, C1)
            DDLYear2.Items.Add(liYear2)
        Next
        DDLYear2.SelectedValue = GetServerTime.Year

        ' Init DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)

        sSql = "select distinct salescode,salescode+' - '+personname from ql_mstperson p inner join QL_mstlevel g on g.leveloid =p.leveloid where upper(g.leveldesc)='SALES' and p.activeflag='ACTIVE' AND p.salescode IN (SELECT jm.salescode FROM QL_trnjualmst jm WHERE trnjualstatus IN ('Closed')) ORDER BY salescode"
        FillDDL(DDLsales, sSql)
    End Sub

    Private Sub BindGiroData()
        sSql = "SELECT * FROM ( "
        sSql &= "select 0 AS nmr, trnjualno, c.custname, CONVERT(char(10),trnjualdate, 101) AS tglinv, trnjualdate sortdate, (SELECT SUM(amtbayar) FROM QL_conar con LEFT JOIN (SELECT pay.cmpcode, pay.payaroid, pay.refoid, pay.reftype FROM QL_trnpayar pay INNER JOIN QL_trncashbankmst cb ON cb.cmpcode=pay.cmpcode AND cb.cashbankoid=pay.cashbankoid WHERE ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND cb.cashbankoid NOT IN (SELECT cbx.cashbankoid FROM QL_trncashbankmst cbx WHERE cbx.cashbanktype='BLM' AND giroacctgoid IN (SELECT dp.dparoid FROM QL_trndpar dp WHERE dp.cmpcode=cb.cmpcode AND dp.dparpayrefno NOT LIKE 'SRT%'))) AS Tbl_CB ON Tbl_CB.cmpcode=con.cmpcode AND Tbl_CB.refoid=con.refoid AND Tbl_CB.reftype=con.reftype AND Tbl_CB.payaroid=con.payrefoid WHERE con.payrefoid<>0 AND con.refoid=jm.trnjualmstoid) AS netto, komisi, 0.00 AS komisisales, p.salescode+'-'+p.personname AS nama, jm.trnjualmstoid AS refoid, jm.curroid AS curr, trnjualnote AS keterangan, jm.cmpcode, jm.trnjualres3, p.salescode, trnjualstatus, trnjualamtnetto trnjualnetto, (select MAX(con.paymentdate) from QL_conar con where con.reftype='QL_trnjualmst' and con.refoid=jm.trnjualmstoid) AS paydate FROM QL_trnjualmst jm INNER JOIN QL_mstperson p ON p.salescode=jm.salescode INNER JOIN QL_mstcust c ON c.custoid=jm.custoid "
        sSql &= ") AS dtData "
        sSql &= "WHERE cmpcode='" & CompnyCode & "' AND ISNULL(trnjualres3,'')<>'PAID' AND salescode='" & DDLsales.SelectedValue & "' AND trnjualstatus='Closed' AND MONTH(paydate)='" & ToInteger(DDLMonth2.SelectedValue) & "' AND YEAR(paydate)='" & ToInteger(DDLYear2.SelectedValue) & "'"
        sSql &= " ORDER BY sortdate"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_girodetail")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(C1)("nmr") = C1 + 1
            dt.Rows(C1)("komisisales") = Math.Round(ToDouble(dt.Rows(C1)("netto")) * (dt.Rows(C1)("komisi") / 100), 2, MidpointRounding.AwayFromZero)
        Next
        dt.AcceptChanges()
        Session("TblDataGiro") = dt
        gvDtl.DataSource = Session("TblDataGiro")
        gvDtl.DataBind()
    End Sub

    Private Sub UpdateNote()
        If Session("TblDataGiro") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDataGiro")
            For C1 As Integer = 0 To gvDtl.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvDtl.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim sNote As String = ""
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(10).Controls
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                            sNote = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                        End If
                    Next
                    dt.Rows(C1)("giroflagnote") = sNote
                End If
            Next
            dt.AcceptChanges()
            Session("TblDataGiro") = dt
        End If
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnKomisiSales.aspx")
        End If
        If checkPagePermission("~\Transaction\trnKomisiSales.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Outgoing Giro Flag"
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POSTING this data?');")
        If Not Page.IsPostBack Then
            upduser.Text = Session("UserID") : updtime.Text = GetServerTime().ToString
            tbDate.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitDDL()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("Success") Is Nothing And Session("Success") <> "" Then
            Response.Redirect("~\Transaction\trnKomisiSales.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnShowData_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnShowData.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select BUSINESS UNIT first", 2)
            Exit Sub
        End If
        BindGiroData()
        gvDtl.Visible = True
    End Sub

    Protected Sub cbHeader_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim bCheck As Boolean = False
        If sender.Checked Then
            bCheck = True
        End If
        For C1 As Integer = 0 To gvDtl.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvDtl.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = bCheck
                    End If
                Next
            End If
        Next
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 2)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 2)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 2)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 2)
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnKomisiSales.aspx?awal=true")
    End Sub

    Protected Sub btnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosting.Click
        Dim sOids As String = GetSelectedOid()
        If sOids = "" Then
            showMessage("Please Select Some Detail Data First!", 2)
            Exit Sub
        End If

        'UpdateNote()
        Dim dtGiro As DataTable = Session("TblDataGiro")
        Dim dvGiro As DataView = dtGiro.DefaultView
        dvGiro.RowFilter = ""
        dvGiro.RowFilter = "nmr IN (" & sOids & ") AND komisi<=0"
        If dvGiro.Count > 0 Then
            showMessage("Komisi Sales = 0, isi dulu di Master Employee", 2)
            dvGiro.RowFilter = ""
            Exit Sub
        End If
        dvGiro.RowFilter = "nmr IN (" & sOids & ")"

        Dim cRate As New ClassRate
        For C1 As Integer = 0 To dvGiro.Count - 1
            cRate.SetRateValue(dvGiro(C1)("curr"), Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, 2) : Exit Sub
            End If
        Next

        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(tbDate.Text))
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            For C1 As Integer = 0 To dvGiro.Count - 1
                sSql = "UPDATE QL_trnjualmst SET trnjualres3='PAID', komisisales=" & dvGiro(C1)("komisi") & ", komisiamtidr=" & dvGiro(C1)("komisisales") * cRate.GetRateMonthlyIDRValue & ", komisiamtusd=" & dvGiro(C1)("komisisales") * cRate.GetRateMonthlyUSDValue & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnjualmstoid='" & dvGiro(C1)("refoid") & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            Next
            dvGiro.RowFilter = ""
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch exSql As SqlException
            objTrans.Rollback() : conn.Close()
            If ToDouble(Session("ErrorCounter")) < 5 Then
                If exSql.Number = 2627 Then
                    Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                    btnPosting_Click(sender, e)
                    Exit Sub
                Else
                    showMessage(exSql.Message, 1)
                    Exit Sub
                End If
            Else
                showMessage(exSql.Message, 1)
                Exit Sub
            End If
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Session("Success") = "Selected data have been posted successfully."
        showMessage(Session("Success"), 3)
    End Sub

    Protected Sub DDLsales_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gvDtl.Visible = False
    End Sub
#End Region

End Class