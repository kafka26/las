Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class Transaction_SOClosing
	Inherits System.Web.UI.Page

#Region "Variables"
	Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
	Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
	Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
	Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
	Dim xCmd As New SqlCommand("", conn)
	Dim xreader As SqlDataReader
	Dim sSql As String = ""
	Dim cKon As New Koneksi
	Dim cProc As New ClassProcedure
#End Region

#Region "Function"
	Private Function GetOidDetail() As String
		Dim sReturn As String = ""
		For C1 As Integer = 0 To gvPODtl.Rows.Count - 1
			Dim row As System.Web.UI.WebControls.GridViewRow = gvPODtl.Rows(C1)
			If (row.RowType = DataControlRowType.DataRow) Then
				Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
				For Each myControl As System.Web.UI.Control In cc
					If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
						Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
						Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
						If cbCheck Then
							sReturn &= CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip & ","
						End If
					End If
				Next
			End If
		Next
		If sReturn <> "" Then
			sReturn = Left(sReturn, sReturn.Length - 1)
		End If
		Return sReturn
	End Function
#End Region

#Region "Procedures"
	Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
		Dim strCaption As String = CompnyName
		If iType = 1 Then ' Error
			imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
		ElseIf iType = 2 Then ' Warning
			imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
		ElseIf iType = 3 Then ' Information
			imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
		Else
			imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
		End If
		lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
		cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
	End Sub

	Private Sub InitDDL(ByVal gencode As String)
		' Init DDL Business Unit
		sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
		If Session("CompnyCode") <> CompnyCode Then
			sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
		End If
		FillDDL(pobusinessunit, sSql)
    End Sub

	Private Sub BindListPO()
        Dim sType As String = ddlType.SelectedValue
        sSql = "SELECT * FROM ( SELECT som.cmpcode,som.sotype,som.somstoid AS pomstoid,som.sono AS pono,CONVERT(VARCHAR(10), som.sodate, 101) AS podate,som.somstres1 AS pogroup, somstnote as pomstnote,som.somststatus AS pomststatus,p.personname AS PIC, '' AS suppname FROM QL_trnsomst som INNER JOIN QL_trnwodtl1 wod1 ON som.cmpcode=wod1.cmpcode AND som.somstoid=wod1.soitemmstoid INNER JOIN QL_mstperson p ON p.personoid=som.approvalpic WHERE som.cmpcode='" & CompnyCode & "' AND som.somststatus <> 'Closed' AND ISNULL(som.somstres3, '')='' AND som.sotype='" & ddlType.SelectedValue & "' " & _
        " UNION " & _
        " SELECT som.cmpcode,som.sotype,som.somstoid AS pomstoid,som.sono AS pono,CONVERT(VARCHAR(10), som.sodate, 101) AS podate,som.somstres1 AS pogroup, somstnote as pomstnote,som.somststatus AS pomststatus,'' AS PIC,c.custname AS suppname FROM QL_trnsomst som LEFT JOIN QL_trndomst wod1 ON som.cmpcode=wod1.cmpcode AND som.somstoid=wod1.somstoid LEFT JOIN QL_trnjualmst jm ON jm.cmpcode=wod1.cmpcode AND jm.domstoid=wod1.domstoid INNER JOIN QL_mstcust c ON c.custoid=som.custoid WHERE som.cmpcode='" & CompnyCode & "' AND som.somststatus='Approved' AND ISNULL(som.somstres3, '')='' AND som.sotype='" & ddlType.SelectedValue & "' AND wod1.domststatus='Approved' AND som.somstoid IN (SELECT do.somstoid FROM QL_trndomst do WHERE do.somstoid=som.somstoid AND som.cmpcode=do.cmpcode)) AS dt ORDER BY podate DESC, pomstoid DESC"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnpomst")
        If dt.Rows.Count = 0 Then
            showMessage("There is no SO can be closed for this time.", 2)
            Exit Sub
        End If
        If ddlType.SelectedValue = "Buffer" Then
            gvListPO.Columns(3).Visible = False
            gvListPO.Columns(4).Visible = True
        Else
            gvListPO.Columns(3).Visible = True
            gvListPO.Columns(4).Visible = False
        End If
        Session("Tblpomst") = dt
        gvListPO.DataSource = Session("Tblpomst")
        gvListPO.DataBind()
        gvListPO.SelectedIndex = -1
        cProc.SetModalPopUpExtender(btnHiddenListPO, PanelListPO, mpeListPO, True)
    End Sub

    Private Sub ViewDetailPO(ByVal iOid As Integer)
        If ddlType.SelectedValue = "Buffer" Then
            sSql = "SELECT sod.sodtlseq AS podtlseq,som.cmpcode,som.somstoid AS pomstoid,wod1.wodtl1seq, wod1.soitemmstoid, wod1.soitemdtloid AS podtloid,som.sotype AS pogroup, wod1.itemoid, i.itemLongDescription, i.itemcode, sod.sodtlstatus AS podtlstatus,wod1.wodtl1status, i.roundQty AS itemlimitqty,wod1.wodtl1unitoid, g.gendesc AS gendesc, wod1.wodtl1note AS podtlnote, ISNULL(sod.sodtlqty,0.00) AS PoQty, ISNULL(wod1.wodtl1qty,0.00) AS DoQty, ISNULL(sod.sodtlqty,0.00)-ISNULL(wod1.wodtl1qty,0.00) AS OsQty,0.00 AS RetQty" & _
       " FROM QL_trnwodtl1 wod1" & _
       " INNER JOIN QL_trnsomst som ON som.cmpcode=wod1.cmpcode AND som.somstoid=wod1.soitemmstoid" & _
       " INNER JOIN QL_trnsodtl sod ON sod.cmpcode=wod1.cmpcode AND sod.sodtloid=wod1.soitemdtloid" & _
       " INNER JOIN QL_mstitem i ON i.itemoid=wod1.itemoid" & _
       " INNER JOIN QL_mstgen g ON g.genoid=wod1.wodtl1unitoid" & _
       " WHERE som.cmpcode='" & CompnyCode & "' AND som.somstoid=" & pomstoid.Text & " AND sod.sodtlstatus <> 'Complete' ORDER BY sod.sodtlseq"
            gvPODtl.Columns(6).Visible = False
        Else
            sSql = "SELECT sod.cmpcode,sod.somstoid AS pomstoid,m.itemCode,m.itemLongDescription,sod.sodtlseq AS podtlseq,ISNULL(sod.sodtlqty,0.00) AS PoQty,(SELECT ISNULL(SUM(dot.doqty),0.0) FROM QL_trndodtl dot WHERE dot.sodtloid=sod.sodtloid AND dot.matoid=sod.itemoid AND dot.cmpcode=sod.cmpcode AND sod.somstoid=" & pomstoid.Text & ") AS DoQty,SUM(ISNULL(sr.sretqty,0.0)) AS RetQty,ISNULL(sod.sodtlqty,0.0)-(SELECT ISNULL(SUM(dot.doqty),0.0) FROM QL_trndodtl dot WHERE dot.sodtloid=sod.sodtloid AND dot.matoid=sod.itemoid AND dot.cmpcode=sod.cmpcode AND sod.somstoid=" & pomstoid.Text & ") + SUM(ISNULL(sr.sretqty,0.0)) AS OsQty,gendesc,sod.sodtlstatus AS podtlstatus,sod.sodtlnote AS podtlnote,sod.sodtloid AS podtloid FROM QL_trnsodtl sod " & _
            " INNER JOIN QL_mstitem m ON m.itemoid=sod.itemoid" & _
            " INNER JOIN QL_trndomst do ON do.somstoid=sod.somstoid AND do.cmpcode=sod.cmpcode" & _
            " INNER JOIN QL_trnjualmst jm ON jm.cmpcode=do.cmpcode AND jm.domstoid=do.domstoid" & _
            " INNER JOIN QL_trnjualdtl jd ON jd.trnjualmstoid=jm.trnjualmstoid" & _
            " INNER JOIN QL_mstgen g ON g.genoid=sod.sodtlunitoid AND sod.cmpcode=g.cmpcode" & _
            " LEFT JOIN QL_trnsalesreturdtl sr ON sr.trnjualdtloid=jd.trnjualdtloid AND sr.matoid=m.itemoid" & _
            " WHERE sod.cmpcode='" & CompnyCode & "' AND sod.somstoid=" & pomstoid.Text & " AND sod.sodtlstatus <> 'Complete' GROUP BY sod.cmpcode,sod.somstoid,sod.sodtloid,sod.itemoid,sod.sodtlqty,m.itemCode,m.itemLongDescription,sod.sodtlseq,g.gendesc,sod.sodtlstatus,sod.sodtlnote ORDER BY sodtlseq"
            gvPODtl.Columns(6).Visible = True
        End If

        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnpodtl")
        Session("TblDtlPO") = dt
        gvPODtl.DataSource = dt
        gvPODtl.DataBind()
    End Sub

	Private Sub ClearData()
        pomstoid.Text = ""
        pono.Text = ""
        podate.Text = ""
        suppname.Text = ""
        pomstnote.Text = ""
        pomststatus.Text = ""
        gvPODtl.DataSource = Nothing
        gvPODtl.DataBind()
	End Sub
#End Region

#Region "Events"
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("UserID") = "" Then
			Response.Redirect("~/Other/login.aspx")
		End If
		Session.Timeout = 60
		If Request.QueryString("awal") = "true" Then
			' Simpan session ke variabel temporary supaya tidak hilang
			Dim userId As String = Session("UserID")
			Dim xsetAcc As DataTable = Session("SpecialAccess")
			Dim appLimit As Decimal = Session("ApprovalLimit")
			Dim xsetRole As DataTable = Session("Role")
			Dim cmpcode As String = Session("CompnyCode")
			' Clear all session
			Session.Clear()
			' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
			Session("UserID") = userId
			Session("SpecialAccess") = xsetAcc
			Session("ApprovalLimit") = appLimit
			Session("Role") = xsetRole
			Session("CompnyCode") = cmpcode
			' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnSOClosing.aspx")
		End If
        If checkPagePermission("~\Transaction\trnSOClosing.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
		Page.Title = CompnyName & " - PO Closing"
		btnClosing.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to CLOSE this selected data?');")
		btnClosingAll.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to CLOSE this data?');")
		If Not Page.IsPostBack Then
			upduser.Text = Session("UserID") : updtime.Text = GetServerTime().ToString
			InitDDL("")
		End If
	End Sub

	Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
		cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
		If Not Session("Success") Is Nothing And Session("Success") <> "" Then
            Response.Redirect("~\Transaction\trnSOClosing.aspx?awal=true")
		End If
	End Sub

	Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        'ClearData()
        If ddlType.SelectedValue = "Buffer" Then
            Name.Text = "PIC"
        Else
            Name.Text = "Customer"
        End If
	End Sub

	Protected Sub pobusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles pobusinessunit.SelectedIndexChanged
		ClearData()
	End Sub

	Protected Sub imbFindPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindPO.Click
		DDLFilterListPO.SelectedIndex = -1
		txtFilterListPO.Text = ""
		Session("Tblpomst") = Nothing
		gvListPO.DataSource = Session("Tblpomst")
		gvListPO.DataBind()
		BindListPO()
	End Sub

	Protected Sub imbErasePO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbErasePO.Click
		ClearData()
		InitDDL("")
		ddlType.Enabled = True
		ddlType.CssClass = "inpText"
	End Sub

	Protected Sub btnFindListPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListPO.Click
		Dim dv As DataView = Session("Tblpomst").DefaultView
		dv.RowFilter = DDLFilterListPO.SelectedValue & " LIKE '%" & Tchar(txtFilterListPO.Text) & "%'"
		gvListPO.DataSource = dv.ToTable
		gvListPO.DataBind()
		dv.RowFilter = ""
		mpeListPO.Show()
	End Sub

	Protected Sub btnViewAllListPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListPO.Click
		txtFilterListPO.Text = ""
		DDLFilterListPO.SelectedIndex = -1
		gvListPO.SelectedIndex = -1
		gvListPO.DataSource = Session("Tblpomst")
		gvListPO.DataBind()
		mpeListPO.Show()
	End Sub

	Protected Sub gvListPO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListPO.PageIndexChanging
		gvListPO.PageIndex = e.NewPageIndex
		Dim dv As DataView = Session("Tblpomst").DefaultView
		dv.RowFilter = DDLFilterListPO.SelectedValue & " LIKE '%" & Tchar(txtFilterListPO.Text) & "%'"
		gvListPO.DataSource = dv.ToTable
		gvListPO.DataBind()
		dv.RowFilter = ""
		mpeListPO.Show()
	End Sub

	Protected Sub gvListPO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListPO.SelectedIndexChanged
		If pomstoid.Text = gvListPO.SelectedDataKey.Item("pomstoid").ToString Then
			ClearData()
		End If
		pono.Text = gvListPO.SelectedDataKey.Item("pono").ToString
		pomstoid.Text = gvListPO.SelectedDataKey.Item("pomstoid").ToString
        podate.Text = Format(CDate(gvListPO.SelectedDataKey.Item("podate").ToString), "MM/dd/yyyy")
        If ddlType.SelectedValue = "Buffer" Then
            suppname.Text = gvListPO.SelectedDataKey.Item("PIC").ToString
        Else
            suppname.Text = gvListPO.SelectedDataKey.Item("suppname").ToString
        End If
        pomststatus.Text = gvListPO.SelectedDataKey.Item("pomststatus").ToString
		pomstnote.Text = gvListPO.SelectedDataKey.Item("pomstnote").ToString
        ViewDetailPO(CInt(pomstoid.Text))
		ddlType.Enabled = False
		ddlType.CssClass = "inpTextDisabled"
		cProc.SetModalPopUpExtender(btnHiddenListPO, PanelListPO, mpeListPO, False)
	End Sub

	Protected Sub lkbListPO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListPO.Click
		cProc.SetModalPopUpExtender(btnHiddenListPO, PanelListPO, mpeListPO, False)
	End Sub

	Protected Sub gvpoDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPODtl.RowDataBound
		If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
		End If
	End Sub

	Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnSOClosing.aspx?awal=true")
	End Sub

	Protected Sub btnClosingAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClosingAll.Click
		Dim sType As String = ddlType.SelectedValue
		Dim sOid As String = ""
		Dim objTable As DataTable
		objTable = Session("TblDtlPO")
		If pomstoid.Text = "" Then
            showMessage("Please select SO Data first!", 2)
			Exit Sub
		Else
			For C1 As Integer = 0 To objTable.Rows.Count - 1
				sOid &= objTable.Rows(C1).Item("podtloid") & ","
			Next
			sOid = Left(sOid, sOid.Length - 1)
			sSql = "SELECT COUNT(*) FROM QL_trnsodtl WHERE somstoid=" & pomstoid.Text & " AND sodtloid IN (" & sOid & ") AND sodtlstatus='COMPLETE' AND cmpcode='" & pobusinessunit.SelectedValue & "'"
			If CheckDataExists(sSql) Then
				showMessage("This data has been Closed by another user. Please CANCEL this transaction and try again!", 2)
				Exit Sub
			End If
		End If
		If pomstnote.Text = "" Then
			showMessage("Please fill NOTE field as a reason for closing this data!", 2)
			Exit Sub
        End If

		Dim objTrans As SqlClient.SqlTransaction
		If conn.State = ConnectionState.Closed Then
			conn.Open()
		End If
		objTrans = conn.BeginTransaction()
		xCmd.Transaction = objTrans
		Try
			For C1 As Integer = 0 To objTable.Rows.Count - 1
                sSql = "UPDATE QL_trnsodtl SET closeqty='" & objTable.Rows(C1).Item("OsQty") & "', upduser='" & Session("UserID") & "', sodtlstatus='Complete', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & pobusinessunit.SelectedValue & "' AND sodtloid=" & objTable.Rows(C1).Item("podtloid") & " AND somstoid=" & pomstoid.Text
				xCmd.CommandText = sSql
				xCmd.ExecuteNonQuery()
			Next
            sSql = "UPDATE QL_trnsomst SET somststatus='Closed', somstres3='Force Closed', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, cancelclosedreason='" & Tchar(pomstnote.Text) & "' WHERE cmpcode='" & pobusinessunit.SelectedValue & "' AND somstoid=" & pomstoid.Text
			xCmd.CommandText = sSql
			xCmd.ExecuteNonQuery()
			objTrans.Commit()
			xCmd.Connection.Close()
		Catch ex As Exception
			objTrans.Rollback()
			xCmd.Connection.Close()
			showMessage(ex.Message, 1)
			Exit Sub
		End Try
		Session("Success") = "So. No : " & pono.Text & " have been close successfully."
		showMessage(Session("Success"), 3)
	End Sub

	Protected Sub gvListPO_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
		If e.Row.RowType = DataControlRowType.DataRow Then
			e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "MM/dd/yyyy")
		End If
	End Sub
#End Region
End Class