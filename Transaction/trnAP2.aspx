<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnAP2.aspx.vb" Inherits="Transaction_APRawMaterial" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" CssClass="Title" Font-Bold="False" Text=".: Purchase Invoice"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Purchase Invoice :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 10%" class="Label" align=left>Filter</TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="105px"><asp:ListItem Value="trnbelimstoid">Draft No.</asp:ListItem>
<asp:ListItem Value="trnbelino">Purchase Invoice No.</asp:ListItem>
<asp:ListItem Value="suppname">Supplier</asp:ListItem>
<asp:ListItem Value="trnbelimstnote">Note</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="250px"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text="Period"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="60px" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label5" runat="server" Text="to"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="60px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="lblFormatTgl" runat="server" Text="(MM/dd/yyyy)" CssClass="Important"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="105px"><asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>In Approval</asp:ListItem>
<asp:ListItem>Approved</asp:ListItem>
<asp:ListItem>Rejected</asp:ListItem>
<asp:ListItem>Revised</asp:ListItem>
<asp:ListItem>Closed</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="FilterPeriod1" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" TargetControlID="FilterPeriod1" Format="MM/dd/yyyy" PopupButtonID="imbDate1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" TargetControlID="FilterPeriod2" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" TargetControlID="FilterPeriod2" Format="MM/dd/yyyy" PopupButtonID="imbDate2"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/generate_efaktur.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:LinkButton id="lkbAPInProcess" runat="server" Visible="False"></asp:LinkButton></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:LinkButton id="lkbAPInApproval" runat="server" Visible="False"></asp:LinkButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvTRN" runat="server" ForeColor="#333333" Width="100%" OnSelectedIndexChanged="gvTRN_SelectedIndexChanged" DataKeyNames="aprawmstoid" PageSize="8" AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowPaging="True" AllowSorting="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="aprawmstoid" DataNavigateUrlFormatString="~\Transaction\trnAP2.aspx?oid={0}" DataTextField="aprawmstoid" HeaderText="Draft No." SortExpression="aprawmstoid">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="aprawno" HeaderText="PI No." SortExpression="aprawno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="aprawdate" HeaderText="PI Date" SortExpression="aprawdate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier" SortExpression="suppname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="aprawmststatus" HeaderText="Status" SortExpression="aprawmststatus">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="aprawmstnote" HeaderText="Note" SortExpression="aprawmstnote">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="divname" HeaderText="Business Unit" SortExpression="divname" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reason" HeaderText="Reason" SortExpression="reason">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:HyperLinkField DataNavigateUrlFields="aprawmstoid,rptname,cmpcode" DataNavigateUrlFormatString="~\ReportForm\frmReportPlace.aspx?oid={0}&amp;rpt={1}&amp;code={2}" DataTextField="printtext" Target="_blank" HeaderText="TTF" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:HyperLinkField>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbGVMst" runat="server" ToolTip="Select and Click Button Print" Checked='<%# eval("checkvalue") %>'></asp:CheckBox> <asp:TextBox id="tbOidGVMst" runat="server" Text='<%# eval("aprawmstoid") %>' Width="20px" Visible="False"></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=4><asp:UpdateProgress id="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image11" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label40" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Purchase Invoice Header" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="i_u" runat="server" Text="New Data" CssClass="Important"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="periodacctg" runat="server" Visible="False"></asp:Label> <asp:Label id="suppoid" runat="server" Visible="False"></asp:Label> <asp:Label id="mrmstoid" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="aprawmstoid" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="rateoid" runat="server" Visible="False"></asp:Label> <asp:Label id="rate2oid" runat="server" Visible="False"></asp:Label> <asp:Label id="pomstoid" runat="server" Visible="False"></asp:Label> <asp:Label id="potype" runat="server" Visible="False"></asp:Label> <asp:Label id="pogroup" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label10" runat="server" Text="Division" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpText" Visible="False" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="aprawtotaldisc" runat="server" CssClass="inpTextDisabled" Width="10px" Visible="False" Enabled="False"></asp:TextBox> <asp:Label id="trnbelitaxtype" runat="server" Visible="False"></asp:Label> <asp:Label id="trnbelitaxpct" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblTrnNo" runat="server" Text="PI No."></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="aprawno" runat="server" CssClass="inpTextDisabled" Width="160px" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label8" runat="server" Text="PI Date"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="aprawdate" runat="server" CssClass="inpTextDisabled" Width="80px" Enabled="False" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:Label id="Label49" runat="server" Text="(MM/dd/yyyy)" CssClass="Important" __designer:wfdid="w18"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label6" runat="server" Text="Supplier"></asp:Label> <asp:Label id="Label44" runat="server" Text="*" CssClass="Important"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="suppname" runat="server" CssClass="inpTextDisabled" Width="160px" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSearchSupp" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClearSupp" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="Label19" runat="server" Text="PO No."></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="pono" runat="server" CssClass="inpTextDisabled" Width="160px" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="ImbSearchPO" onclick="btnSearchReg_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="ImbErasePO" onclick="btnClearReg_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label12" runat="server" Text="Currency"></asp:Label> <asp:Label id="Label34" runat="server" Text="*" CssClass="Important"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="curroid" runat="server" CssClass="inpTextDisabled" Width="105px" AutoPostBack="True" Enabled="False"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label16" runat="server" Text="Payment Type"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="aprawpaytypeoid" runat="server" CssClass="inpTextDisabled" Width="155px" Enabled="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Label id="Label37" runat="server" ForeColor="Black" Font-Bold="True" Text="Monthly Rate" Font-Underline="True"></asp:Label></TD><TD class="Label" align=left colSpan=3><asp:Label id="Label38" runat="server" ForeColor="Black" Font-Bold="True" Text="Tax Rate" Font-Underline="True"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label42" runat="server" Text="To IDR"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="aprawrate2toidr" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label13" runat="server" Text="To IDR"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="aprawratetoidr" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label46" runat="server" Text="To USD"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="aprawrate2tousd" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label33" runat="server" Text="To USD"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="aprawratetousd" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label17" runat="server" Text="Total Amt"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="aprawtotalamt" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label39" runat="server" Text="Total Disc. Dtl"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="amtdiscdtl" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label18" runat="server" Text="Header Disc."></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="trnbelidisctype" runat="server" CssClass="inpTextDisabled" Width="80px" Enabled="False">
                <asp:ListItem Value="P">Percentage</asp:ListItem>
                <asp:ListItem Value="A">Amount</asp:ListItem>
            </asp:DropDownList> - <asp:TextBox id="trnbelidisc" runat="server" CssClass="inpTextDisabled" Width="100px" AutoPostBack="True" Enabled="False" MaxLength="10"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label28" runat="server" Text="Header Disc. Amt"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="amtdischdr" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD><TD class="Label" align=left><asp:Label id="Label41" runat="server" Text="Total Disc. Amt"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="aptotaldiscamt" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False">0</asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label36" runat="server" Text="Tax"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="trntaxtype" runat="server" CssClass="inpTextDisabled" Width="80px" Enabled="False">
                <asp:ListItem>INCLUDE</asp:ListItem>
                <asp:ListItem>EXCLUDE</asp:ListItem>
                <asp:ListItem>NON TAX</asp:ListItem>
            </asp:DropDownList> - <asp:TextBox id="trntaxpct" runat="server" CssClass="inpTextDisabled" Width="50px" AutoPostBack="True" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label20" runat="server" Text="Tax Amount"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="aprawtotaltax" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label23" runat="server" Text="Grand Total"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="aprawgrandtotal" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label4" runat="server" Text="Status"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="aprawmststatus" runat="server" CssClass="inpTextDisabled" Width="80px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblGTS" runat="server" Text="Grand Total Supplier"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="aprawsupptotal" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" MaxLength="18" OnTextChanged="aprawsupptotal_TextChanged"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="lblDTG" runat="server" Text="Date Take Giro"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="aprawdatetakegiro" runat="server" CssClass="inpText" Width="80px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbDTG" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btncleargiro" onclick="btncleargiro_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton><asp:Label id="Label48" runat="server" Text="(MM/dd/yyyy)" CssClass="Important" __designer:wfdid="w17"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label43" runat="server" Text="Faktur No." __designer:wfdid="w9"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="aprawfakturno" runat="server" CssClass="inpText" Width="200px" MaxLength="100" __designer:wfdid="w10"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label45" runat="server" Text="Masa Pajak" __designer:wfdid="w11"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="aprawmasapajak" runat="server" CssClass="inpText" Width="80px" ToolTip="MM/dd/yyyy" __designer:wfdid="w12"></asp:TextBox>&nbsp;<asp:ImageButton id="btnDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w13"></asp:ImageButton>&nbsp;<asp:Label id="Label47" runat="server" Text="(MM/dd/yyyy)" CssClass="Important" __designer:wfdid="w11"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label24" runat="server" Text="Header Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="aprawmstnote" runat="server" CssClass="inpText" Width="200px" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="ftbGTS" runat="server" ValidChars="1234567890,." TargetControlID="aprawsupptotal"></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:MaskedEditExtender id="meeDTG" runat="server" TargetControlID="aprawdatetakegiro" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="ceDTG" runat="server" TargetControlID="aprawdatetakegiro" Format="MM/dd/yyyy" PopupButtonID="imbDTG"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="MeeDate2" runat="server" TargetControlID="aprawmasapajak" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear" __designer:wfdid="w14"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="CeDate2" runat="server" TargetControlID="aprawmasapajak" Format="MM/dd/yyyy" PopupButtonID="btnDate2" __designer:wfdid="w15"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6 rowSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><asp:Label id="Label7" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Purchase Invoice Detail" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="i_u2" runat="server" Text="New Detail" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="aprawdtlseq" runat="server" Visible="False">1</asp:Label> <asp:Label id="aprawdtloid" runat="server" Visible="False"></asp:Label> <asp:Label id="registermstoid" runat="server" Visible="False"></asp:Label> <asp:Label id="mrrawmstoid" runat="server" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="piqty" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text="LPB No."></asp:Label>&nbsp;<asp:Label id="Label11" runat="server" Text="*" CssClass="Important"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="registerno" runat="server" CssClass="inpTextDisabled" Width="160px" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchReg" onclick="btnSearchReg_Click1" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearReg" onclick="btnClearReg_Click1" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="Label35" runat="server" Text="Faktur No. "></asp:Label>&nbsp;<asp:Label id="Label14" runat="server" Text="*" CssClass="Important"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="aprawdtlres1" runat="server" CssClass="inpTextDisabled" Width="150px" Enabled="False" MaxLength="30"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label21" runat="server" Text="Code"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="matcode" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label2" runat="server" Text="Faktur Pajak No."></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="aprawdtlres2" runat="server" Text='<%# eval("fakturpajakno") %>' CssClass="inpTextDisabled" Width="150px" Enabled="False" MaxLength="30" OnTextChanged="aprawdtlres2a_TextChanged"></asp:TextBox> <asp:TextBox id="aprawdtlres2a" runat="server" CssClass="inpText" Width="56px" Visible="False" MaxLength="30" OnTextChanged="aprawdtlres2a_TextChanged"></asp:TextBox>&nbsp;<asp:TextBox id="aprawdtlres2b" runat="server" CssClass="inpText" Width="24px" Visible="False" MaxLength="30"></asp:TextBox>&nbsp;<asp:TextBox id="aprawdtlres2c" runat="server" CssClass="inpText" Width="24px" Visible="False" MaxLength="30"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label22" runat="server" Text="Description"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="matlongdesc" runat="server" CssClass="inpTextDisabled" Width="309px" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label26" runat="server" Text="PO Price"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="poprice" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label30" runat="server" Text="Amount Netto"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="amtnetto" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label27" runat="server" Text="Disc. Type"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLDiscType" runat="server" CssClass="inpText" Width="105px" AutoPostBack="True" OnSelectedIndexChanged="DDLDiscType_SelectedIndexChanged">
<asp:ListItem Value="P">Percentage</asp:ListItem>
<asp:ListItem Value="A">Amount</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="amtdisc" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" MaxLength="18" OnTextChanged="amtdisc_TextChanged"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label31" runat="server" Text="Discount"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="amtdisctotal" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False" MaxLength="18" OnTextChanged="aprawsupptotal_TextChanged"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label15" runat="server" Text="Detail Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="aprawdtlnote" runat="server" CssClass="inpText" Width="200px" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label32" runat="server" Text="PI Price"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="piamount" runat="server" CssClass="inpText" Width="100px" MaxLength="12" OnTextChanged="aprawsupptotal_TextChanged">0.00</asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label9" runat="server" Text="Register No." Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label25" runat="server" Text="Qty Unit Kecil" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="unitkecil" runat="server" CssClass="inpTextDisabled" Width="65px" Visible="False" Enabled="False"></asp:TextBox> <asp:DropDownList id="unitkeciloid" runat="server" CssClass="inpTextDisabled" Width="105px" Visible="False"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label29" runat="server" Text="Qty Unit Besar" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="unitbesar" runat="server" CssClass="inpTextDisabled" Width="65px" Visible="False" Enabled="False"></asp:TextBox> <asp:DropDownList id="unitbesaroid" runat="server" CssClass="inpTextDisabled" Width="105px" Visible="False"></asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 22px" class="Label" align=left></TD><TD style="HEIGHT: 22px" class="Label" align=center></TD><TD style="HEIGHT: 22px" class="Label" align=left><ajaxToolkit:FilteredTextBoxExtender id="ftepiprice" runat="server" ValidChars="0123456789.," TargetControlID="piamount"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftedisc" runat="server" ValidChars="0123456789.," TargetControlID="amtdisc"></ajaxToolkit:FilteredTextBoxExtender></TD><TD style="HEIGHT: 22px" class="Label" align=left></TD><TD style="HEIGHT: 22px" class="Label" align=center></TD><TD style="HEIGHT: 22px" class="Label" align=left><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" TargetControlID="aprawdtlres2a" Mask="999.999-99" MaskType="DateTime"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" Enabled="False" TargetControlID="aprawdtlres2" Mask="999-999-99.99999999" MaskType="DateTime"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender3" runat="server" TargetControlID="aprawdtlres2c" Mask="9999" MaskType="Number"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:GridView id="gvTblDtl" runat="server" ForeColor="#333333" Width="100%" OnSelectedIndexChanged="gvTblDtl_SelectedIndexChanged" DataKeyNames="aprawdtlseq" PageSize="5" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
<RowStyle Wrap="True" BackColor="#E3EAEB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="XX-Small"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="aprawdtlres1" HeaderText="Faktur No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="aprawdtlres2" HeaderText="Faktur Pajak No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matrawcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matrawlongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="mrrawno" HeaderText="MR No.">
<HeaderStyle CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="piqty" HeaderText="PI Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="piunit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="piqty_unitkecil" HeaderText="Qty 1" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="piunit1" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="piqty_unitbesar" HeaderText="Qty 3" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="piunit2" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="poprice" HeaderText="PO Price">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="piprice" HeaderText="PI Price">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="aprawdtlamt" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="aprawdtldiscamt" HeaderText="Disc.">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="aprawdtlnetto" HeaderText="Netto">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="aprawdtlnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="XX-Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#666666" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle Wrap="True" BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#7C6F57"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=6></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update&nbsp;By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSendApproval" runat="server" ImageUrl="~/Images/sendapproval.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnUpdate" runat="server" ImageUrl="~/Images/Update.png" ImageAlign="AbsMiddle" __designer:wfdid="w19" OnClick="btnUpdate_Click"></asp:ImageButton></TD></TR></TBODY></TABLE>&nbsp; <DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Purchase Invoice :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upListSupp" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlListSupp" runat="server" CssClass="modalBox" Width="650px" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSupp" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Supplier"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListSupp" runat="server" Width="100%" DefaultButton="btnFindListSupp">Filter : <asp:DropDownList id="FilterDDLListSupp" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="suppname">Name</asp:ListItem>
<asp:ListItem Value="suppcode">Code</asp:ListItem>
<asp:ListItem Value="suppaddr">Address</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListSupp" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListSupp" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListSupp" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListSupp" runat="server" ForeColor="#333333" Width="99%" PageSize="5" AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowSorting="True" AllowPaging="True" DataKeyNames="suppoid,suppname,supppaymentoid">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Code" SortExpression="suppcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Name" SortExpression="suppname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Width="40%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="40%"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListSupp" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSupp" runat="server" TargetControlID="btnHideListSupp" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlListSupp" PopupDragHandleControlID="lblListSupp">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListSupp" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlListReg" runat="server" CssClass="modalBox" Width="800px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListReg" runat="server" Font-Size="Medium" Font-Bold="True" Text="List Of Purchase Order" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListReg" runat="server" Width="100%" DefaultButton="btnFindListReg">Filter : <asp:DropDownList id="FilterDDLListReg" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="pono">PO No.</asp:ListItem>
<asp:ListItem Value="suppname">Supplier</asp:ListItem>
<asp:ListItem Value="posuppref">Supp. Ref No.</asp:ListItem>
<asp:ListItem Value="pomstnote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListReg" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListReg" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListReg" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListReg" runat="server" ForeColor="#333333" Width="99%" GridLines="None" CellPadding="4" AutoGenerateColumns="False" PageSize="5" DataKeyNames="pomstoid,pono,podate,curroid,suppname,suppoid,poflag,pogroup,potype,potaxtype,potaxamt,povat,pomstdisctype,pomstdiscvalue,pomstdiscamt" OnSelectedIndexChanged="gvListReg_SelectedIndexChanged" AllowPaging="True" AllowSorting="True" OnSorting="gvListreg_Sorting" OnPageIndexChanging="gvListReg_PageIndexChanging">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="pono" HeaderText="PO No." SortExpression="pono">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="podate" HeaderText="PO Date" SortExpression="podate">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier" SortExpression="suppname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="potaxtype" HeaderText="Tax Type">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pogroup" HeaderText="Po Type">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="pomstnote" HeaderText="Note">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="mrno" HeaderText="No. LPB" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListReg" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListReg" runat="server" TargetControlID="btnHideListReg" PopupDragHandleControlID="lblListReg" PopupControlID="pnlListReg" Drag="True" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListReg" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upListMat" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListMat" runat="server" CssClass="modalBox" Width="800px" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListMat" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Posted LPB"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListMat" runat="server" Width="100%" DefaultButton="btnFindListMat"><asp:Label id="LabelFilterListMat" runat="server" Text="Filter : "></asp:Label> <asp:DropDownList id="FilterDDLListMat" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="mrno">LPB No.</asp:ListItem>
<asp:ListItem Value="mrdate">LPB Date</asp:ListItem>
<asp:ListItem Value="mrmstnote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" CssClass="inpText" Width="150px"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton><BR /><asp:ImageButton id="btnSelectAll" onclick="btnSelectAll_Click" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnSelectNone" onclick="btnSelectNone_Click" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewChecked" onclick="btnViewChecked_Click" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel></TD></TR><TR><TD style="HEIGHT: 22px" class="Label" align=left colSpan=3>Show <asp:TextBox id="tbData" runat="server" CssClass="inpText" Width="25px"></asp:TextBox> Data Per Page <asp:LinkButton id="lbShowData" runat="server">>> View</asp:LinkButton></TD></TR><TR><TD class="Label" align=center colSpan=3>&nbsp;<asp:GridView id="gvListMat" runat="server" ForeColor="#333333" Width="99%" PageSize="5" AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowPaging="True" AllowSorting="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbSelectPR" runat="server" ToolTip='<%# eval("mrrawmstoid") %>' Checked='<%# eval("CheckValue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="mrrawno" HeaderText="LPB No." SortExpression="mrrawno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="mrrawdate" HeaderText="Date" SortExpression="mrrawdate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Faktur No."><ItemTemplate>
<asp:TextBox id="fakturno" runat="server" Text='<%# Eval("fakturno") %>' CssClass="inpText" Width="150px" MaxLength="30"></asp:TextBox> 
</ItemTemplate>

<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Faktur Pajak No."><ItemTemplate>
<asp:TextBox id="pajak1" runat="server" Text='<%# eval("fakturpajakno") %>' CssClass="inpText" Width="150px" OnTextChanged="aprawdtlres2a_TextChanged" MaxLength="30"></asp:TextBox><ajaxToolkit:MaskedEditExtender id="MEE1" runat="server" Enabled="True" TargetControlID="pajak1" Mask="999-999-99.99999999" MaskType="DateTime"></ajaxToolkit:MaskedEditExtender> 
</ItemTemplate>

<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Detail Note"><ItemTemplate>
<asp:TextBox id="detailnote" runat="server" Text='<%# eval("aprawdtlnote") %>' CssClass="inpText" Width="150px" MaxLength="100"></asp:TextBox>
</ItemTemplate>

<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="mrrawmstnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD colSpan=3><asp:LinkButton id="lbAddToListListMat" onclick="lbAddToListListMat_Click" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lbCloseListMat" onclick="lbCloseListMat_Click" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR><TR><TD colSpan=3><ajaxToolkit:FilteredTextBoxExtender id="ftbData" runat="server" TargetControlID="tbData" FilterType="Numbers">
            </ajaxToolkit:FilteredTextBoxExtender> </TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" PopupDragHandleControlID="lblListMat" PopupControlID="pnlListMat" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

