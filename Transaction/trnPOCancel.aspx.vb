Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class Transaction_POCancelation
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitDDL(ByVal gencode As String)
        '' Init DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(pobusinessunit, sSql)
        sSql = "SELECT gencode,gendesc As Code, gendesc FROM QL_mstgen WHERE cmpcode='" & pobusinessunit.SelectedValue & "' " & gencode & "  AND gengroup='GROUPITEM' AND activeflag='ACTIVE' ORDER BY gencode"
        FillDDL(ddlType, sSql)
    End Sub

    Private Sub BindListPO()
        sSql = "SELECT DISTINCT pm.cmpcode,pm.pomstoid,pm.pogroup,pm.pono,pm.podate,sp.suppname,pm.pomststatus,pm.pomstnote FROM QL_trnpomst pm INNER JOIN QL_mstsupp sp ON sp.suppoid=pm.suppoid INNER JOIN QL_trnpodtl pd ON pd.pomstoid=pm.pomstoid WHERE pm.cmpcode='" & pobusinessunit.SelectedValue & "' AND pomststatus='Approved' AND pm.pogroup='" & ddlType.SelectedValue & "' AND pm.pomstoid NOT IN (SELECT mr.pomstoid FROM QL_trnmrmst mr WHERE mr.cmpcode=pm.cmpcode AND mr.pomstoid=pm.pomstoid)"

        If checkPagePermission("~\Transaction\trnPOCancel.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND pm.createuser='" & Session("UserID") & "'"
        End If
        sSql &= "  ORDER BY pm.podate DESC, pm.pomstoid DESC"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_pomst")
        If dt.Rows.Count = 0 Then
            showMessage("There is no Approved PO can be cancelled for this time.", 2)
            Exit Sub
        End If
        Session("Tblpomst") = dt
        gvListPO.DataSource = Session("Tblpomst")
        gvListPO.DataBind()
        gvListPO.SelectedIndex = -1
        cProc.SetModalPopUpExtender(btnHiddenListPO, PanelListPO, mpeListPO, True)
    End Sub

    Private Sub ViewDetailPO(ByVal iOid As Integer)

        sSql = "SELECT pom.cmpcode,pom.pomstoid,pod.podtloid,pom.pono,pom.pogroup,sp.suppoid,sp.suppname,i.itemoid,i.itemcode,i.itemLongDescription,u.gendesc,pod.poqty,pod.podtlseq,pod.podtlstatus,pod.podtlnote,pom.pomststatus FROM QL_trnpomst pom" & _
        " INNER JOIN QL_trnpodtl pod ON pod.cmpcode=pom.cmpcode " & _
        " AND pod.pomstoid=pom.pomstoid AND pod.podtlstatus <> 'COMPLETE'" & _
        " AND pom.pomststatus='Approved'" & _
        " INNER JOIN QL_mstsupp sp ON sp.suppoid=pom.suppoid" & _
        " INNER JOIN QL_mstitem i ON i.itemoid=pod.matoid" & _
        " INNER JOIN QL_mstgen u ON u.genoid=pod.pounitoid" & _
        " WHERE pom.cmpcode='" & pobusinessunit.SelectedValue & "'" & _
        " AND pom.pomstoid='" & pomstoid.Text & "'" & _
        " AND pom.pogroup = '" & ddlType.SelectedValue & "'" & _
        " AND pom.pomstoid NOT IN (SELECT mr.pomstoid FROM QL_trnmrmst mr" & _
        " WHERE mr.cmpcode=pom.cmpcode AND mr.pomstoid=pom.pomstoid) ORDER BY podtlseq"

        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnpmst")
        Session("TblDtlPO") = dt
        gvPODtl.DataSource = dt
        gvPODtl.DataBind()
    End Sub

    Private Sub ClearData()
        pomstoid.Text = ""
        pono.Text = ""
        podate.Text = ""
        pomstnote.Text = ""
        pomststatus.Text = ""
        suppname.Text = ""
        gvPODtl.DataSource = Nothing
        gvPODtl.DataBind()
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnPOCancel.aspx")
        End If
        If checkPagePermission("~\Transaction\trnPOCancel.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - PO Cancellation"
        btnProcess.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to CANCEL this data?');")
        If Not Page.IsPostBack Then
            upduser.Text = Session("UserID")
            updtime.Text = GetServerTime().ToString
            InitDDL("")
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("Success") Is Nothing And Session("Success") <> "" Then
            Response.Redirect("~\Transaction\trnPOCancel.aspx?awal=true")
        End If
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        ClearData()
    End Sub

    Protected Sub pobusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles pobusinessunit.SelectedIndexChanged
        ClearData()
    End Sub

    Protected Sub imbFindPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindPO.Click
        DDLFilterListPO.SelectedIndex = -1
        txtFilterListPO.Text = ""
        gvListPO.DataSource = Session("Tblpomst")
        gvListPO.DataBind()
        BindListPO()
    End Sub

    Protected Sub imbErasePO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbErasePO.Click
        ClearData()
        InitDDL("")
        ddlType.Enabled = True
        ddlType.CssClass = "inpText"
    End Sub

    Protected Sub btnFindListPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListPO.Click
        Dim dv As DataView = Session("Tblpomst").DefaultView
        dv.RowFilter = DDLFilterListPO.SelectedValue & " LIKE '%" & Tchar(txtFilterListPO.Text) & "%'"
        gvListPO.DataSource = dv.ToTable
        gvListPO.DataBind()
        dv.RowFilter = ""
        mpeListPO.Show()
    End Sub

    Protected Sub btnViewAllListPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListPO.Click
        txtFilterListPO.Text = ""
        DDLFilterListPO.SelectedIndex = -1
        gvListPO.SelectedIndex = -1
        gvListPO.DataSource = Session("Tblpomst")
        gvListPO.DataBind()
        mpeListPO.Show()
    End Sub

    Protected Sub gvListPO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListPO.PageIndexChanging
        gvListPO.PageIndex = e.NewPageIndex
        Dim dv As DataView = Session("Tblpomst").DefaultView
        dv.RowFilter = DDLFilterListPO.SelectedValue & " LIKE '%" & Tchar(txtFilterListPO.Text) & "%'"
        gvListPO.DataSource = dv.ToTable
        gvListPO.DataBind()
        dv.RowFilter = ""
        mpeListPO.Show()
    End Sub

    Protected Sub gvListPO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListPO.SelectedIndexChanged
        If pomstoid.Text = gvListPO.SelectedDataKey.Item("pomstoid").ToString Then
            ClearData()
        End If
        pono.Text = gvListPO.SelectedDataKey.Item("pono").ToString
        pomstoid.Text = gvListPO.SelectedDataKey.Item("pomstoid").ToString
        podate.Text = Format(CDate(gvListPO.SelectedDataKey.Item("podate").ToString), "MM/dd/yyyy")
        suppname.Text = gvListPO.SelectedDataKey.Item("suppname").ToString
        pomststatus.Text = gvListPO.SelectedDataKey.Item("pomststatus").ToString
        pomstnote.Text = gvListPO.SelectedDataKey.Item("pomstnote").ToString
        InitDDL("AND gencode='" & gvListPO.SelectedDataKey.Item("pogroup").ToString & "'")
        ViewDetailPO(CInt(pomstoid.Text))
        ddlType.Enabled = False
        ddlType.CssClass = "inpTextDisabled"
        cProc.SetModalPopUpExtender(btnHiddenListPO, PanelListPO, mpeListPO, False)
    End Sub

    Protected Sub lkbListPO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListPO.Click
        cProc.SetModalPopUpExtender(btnHiddenListPO, PanelListPO, mpeListPO, False)
    End Sub

    Protected Sub gvPODtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPODtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 2)
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnPOCancel.aspx?awal=true")
    End Sub

    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnProcess.Click
        Dim sType As String = ddlType.SelectedValue
        If pomstnote.Text.Length > 96 Then
            showMessage("Please fill NOTE field as a reason for cancel this data!", 2)
            Exit Sub
        End If
        If pomstoid.Text = "" Then
            showMessage("Please select PO Data first!", 2)
            Exit Sub
        Else
            sSql = "SELECT COUNT(*) FROM QL_trnpomst WHERE pomstoid=" & pomstoid.Text & " AND pomststatus='Cancel' AND cmpcode='" & pobusinessunit.SelectedValue & "'"
            If CheckDataExists(sSql) Then
                showMessage("This data has been canceled by another user. Please CANCEL this transaction and try again!", 2)
                Exit Sub
            End If
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trnpomst SET pomststatus='Cancel', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & pobusinessunit.SelectedValue & "' AND pomstoid=" & pomstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If pomststatus.Text = "In Approval" Then
                sSql = "UPDATE QL_approval SET statusrequest='Cancel', event='Cancel' WHERE cmpcode='" & pobusinessunit.SelectedValue & "' AND tablename='QL_trnpomst' AND oid=" & pomstoid.Text
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Session("Success") = "PO No : " & pono.Text & " have been cancel successfully."
        showMessage(Session("Success"), 3)
    End Sub
#End Region

    Protected Sub gvListPO_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "MM/dd/yyyy")
        End If
    End Sub
End Class