Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_DepartmentGroup
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErrBU As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
            sErrBU = "OK"
        End If
        If ToDouble(paycommamt.Text) <= 0 Then
            sError &= "- Amount must more than 0!<BR>"
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill Detail Data!"
        Else
            Dim dtTbl As DataTable = Session("TblDtl")
            If dtTbl.Rows.Count <= 0 Then
                sError &= "- Please fill Detail Data!"
            End If
        End If

        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsDescExists() As String
        sSql = "SELECT COUNT(*) FROM QL_mstdeptgroup WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND groupdesc='" & Tchar(groupdesc.Text) & "'"
        If Not Session("oid") Is Nothing And Session("oid") <> "" Then
            sSql &= " AND groupoid<>" & Session("oid")
        End If
        If ToDouble(GetStrData(sSql).ToString) > 0 Then
            Return "- DESCRIPTION have been used by another data. Please fill another DESCRIPTION!<BR>"
        End If
        Return ""
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If acctgoid2.Text = "" Then
            sError &= "- Please select EXPENSE ACCOUNT field!<BR>"
        End If
        
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsAccountNoExists() As Boolean
        sSql = "SELECT COUNT(*) FROM QL_mstpicgroup WHERE cmpcode='" & CompnyCode & "' AND personoid=" & personoid.SelectedValue
        If Not Session("oid") Is Nothing And Session("oid") <> "" Then
            sSql &= " AND groupoid<>" & Session("oid")
        End If
        If ToDouble(cKon.ambilscalar(sSql).ToString) > 0 Then
            showMessage("PIC have been SET before, please EDIT or DELETE data first before continue using this form!", 2)
            Return True
        End If
        Return False
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitDDL()
        ' Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND divcode='" & Session("CompnyCode") & "'"
        End If
        If FillDDL(DDLBusUnit, sSql) Then
            InitCBL()
        End If
        'Fill DDL PIC
        sSql = "select distinct p.personoid, personname + ' (' + salescode + ')' name from ql_mstperson p inner join QL_mstlevel g on g.leveloid =p.leveloid where upper(g.leveldesc)='SALES' and p.activeflag='ACTIVE' ORDER BY name"
        FillDDL(personoid, sSql)

        'Fill DDL Personal detail
        sSql = "SELECT genoid, gendesc FROM QL_mstgen p WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND activeflag='ACTIVE' AND gengroup='PICGROUP' ORDER BY gendesc"
        FillDDL(personal, sSql)
    End Sub

    Private Sub InitCBL(Optional ByVal sOid As String = "")
      
    End Sub

    Private Sub BindMstData(ByVal sFilter As String)
        sSql = "SELECT 'False' checkvalue, pay.cmpcode, pay.paycommoid, pay.paycommno, CONVERT(VARCHAR(10), pay.paycommdate, 101) paycommdate, personname, paycommnote, paycommstatus, paycommamt, divname FROM QL_trnpaycommmst pay INNER JOIN QL_mstperson p ON p.personoid=pay.personoid INNER JOIN QL_mstdivision div ON div.divcode=pay.cmpcode WHERE " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(sFilter) & "%'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND pay.cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnCommisionPayment.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND pay.createuser='" & Session("UserID") & "'"
        End If
        sSql &= " ORDER BY pay.paycommdate DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trnpaycommmst")
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub FillTextBox()
        Try
            sSql = "SELECT * FROM QL_trnpaycommmst pay WHERE paycommoid=" & Session("oid")
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = xreader("cmpcode").ToString
                groupoid.Text = xreader("paycommoid").ToString
                paycommno.Text = xreader("paycommno").ToString
                paycommdate.Text = Format(CDate(xreader("paycommdate").ToString), "MM/dd/yyyy")
                activeflag.SelectedValue = xreader("paycommstatus").ToString
                personoid.SelectedValue = xreader("personoid").ToString
                groupnote.Text = xreader("paycommnote").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
                paycommamt.Text = ToMaskEdit(ToDouble(xreader("paycommamt").ToString), 2)
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            btnSave.Visible = False : btnDelete.Visible = False
            showMessage(ex.Message, 1) : Exit Sub
        End Try
        personoid.CssClass = "inpTextDisabled" : personoid.Enabled = False
        If activeflag.SelectedValue = "Post" Then
            btnSave.Visible = False
            btnPost.Visible = False
            btnDelete.Visible = False
            btnAddToList.Visible = False
            gvDtl.Columns(0).Visible = False
            gvDtl.Columns(gvDtl.Columns.Count - 1).Visible = False
        End If

        sSql = "SELECT * FROM QL_trnpaycommdtl payd WHERE paycommoid=" & Session("oid") & " ORDER BY paycommdtlseq"
        Dim dtTable As DataTable = cKon.ambiltabel(sSql, "QL_trnpaycommdtl")
        Session("TblDtl") = dtTable
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        ClearDetail()
        If Session("TblDtl") IsNot Nothing Then
            CountHdrAmt()
        End If
    End Sub

    Private Sub BindListCOA()
        Dim sAdd As String = "AND CASE ISNULL(comstatus, '') WHEN '' THEN 'NOT PAID' ELSE comstatus END='NOT PAID'"

        sSql = "SELECT 'False' checkvalue, 0 nmr, Tbl_INV.cmpcode, Tbl_INV.sales, trnjualmstoid, Tbl_INV.trnjualno paycommdtlrefno, Tbl_INV.amttrans, CONVERT(VARCHAR(10), Tbl_INV.trnardate, 101) trnardate, MONTH([Tgl. Bayar]) bulan_trans, YEAR([Tgl. Bayar]) tahun_trans, Tbl_INV.custname, CONVERT(VARCHAR(10), [Tgl. Bayar], 101) paymentdate, ISNULL(amtbayaridr, 0.0) amtbayaridr, amtreturidr amtreturidr, amtretur2idr amtretur2idr, amtbayarotheridr amtbayarotheridr, komisi percentage, ISNULL(DATEDIFF(DAY, trnardate, [Tgl. Bayar]), 0) jml_hari, [No. Cash Bank] paycommdtlrefdesc, Bank, [Tgl. Bayar], (CASE WHEN DAY([Tgl. Bayar]) BETWEEN 1 AND 20 THEN (CASE MONTH([Tgl. Bayar]) WHEN 1 THEN 12 ELSE MONTH([Tgl. Bayar])-1 END) WHEN DAY([Tgl. Bayar]) BETWEEN 21 AND 31 THEN MONTH([Tgl. Bayar]) END) bulan, (CASE WHEN MONTH([Tgl. Bayar])=1 AND DAY([Tgl. Bayar]) BETWEEN 1 AND 20 THEN YEAR([Tgl. Bayar])-1 ELSE YEAR([Tgl. Bayar]) END) tahun, [Kurang Bayar], [Debet Note], [Credit Note], 0.00 percentage2, CASE ISNULL(comstatus, '') WHEN '' THEN 'NOT PAID' ELSE comstatus END statusbyr, 0.0 paycommdtlamt, conaroid, Tbl_INV.personoid, 0.0 ppnamt, 0.0 cashbankamt, dncnamt, Tbl_INV.kode_komisi, 0.0 komisi, 0 range_1, 0 range_2, 0.0 komisi_pct FROM ( "
        sSql &= "SELECT cmpcode, custoid, (tglpay) [Tgl. Bayar], (tglkomisi) [Tgl. Komisi], (amtbayar) amtbayaridr, (returidr) amtreturidr, (retur2idr) amtretur2idr, (amtbayarother) amtbayarotheridr, refoid, cashbankno [No. Cash Bank], acctgdesc [Bank], [Kurang Bayar], [Debet Note], [Credit Note], comstatus, conaroid, dncnamt FROM ( "
        'Payment Cash/Bank
        sSql &= "SELECT con.cmpcode, con.refoid, con.custoid, (CASE cashbanktype WHEN 'BBM' THEN cb.cashbankduedate WHEN 'BGM' THEN cb.cashbankduedate ELSE cb.cashbankdate END) tglpay, (CASE cashbanktype WHEN 'BBM' THEN cb.cashbankduedate WHEN 'BGM' THEN cb.cashbankduedate ELSE cb.cashbankdate END) tglkomisi, con.amtbayar, 0.00 returidr, 0.00 retur2idr, 0.00 amtbayarother, cb.cashbankno, a.acctgdesc, 0.0 [Kurang Bayar], 0.0 [Debet Note], 0.0 [Credit Note], comstatus, con.conaroid, 0.0 dncnamt FROM QL_conar con INNER JOIN QL_trnpayar pay ON pay.cmpcode=con.cmpcode AND pay.payaroid=con.payrefoid AND ISNULL(pay.payarres1,'')='' INNER JOIN QL_trncashbankmst cb ON cb.cashbankoid=pay.cashbankoid INNER JOIN QL_mstacctg a ON a.acctgoid=cb.acctgoid WHERE con.cmpcode='" & DDLBusUnit.SelectedValue & "' AND con.payrefoid<>0 AND con.trnarstatus='Post' AND con.trnartype IN ('PAYAR') AND cashbankstatus='Post' AND cashbanktype<>'BGM' AND cb.giroacctgoid NOT IN (SELECT dp.dparoid FROM QL_trndpar dp WHERE LEFT(dparpayrefno, 3)='SRT') " & sAdd & " UNION ALL "
        'Payment Giro yang sudah cair
        sSql &= "SELECT con.cmpcode, con.refoid, con.custoid, cbx.cashbankdate tglpay, cbx.cashbankdate tglkomisi, con.amtbayar, 0.00 returidr, 0.00 retur2idr, 0.00 amtbayarother, cbx.cashbankno + ' Pencairan Giro ' + cb.cashbankno cashbankno, a.acctgdesc, 0.0 [Kurang Bayar], 0.0 [Debet Note], 0.0 [Credit Note], comstatus, con.conaroid, 0.0 dncnamt FROM QL_conar con INNER JOIN QL_trnpayar pay ON pay.cmpcode=con.cmpcode AND pay.payaroid=con.payrefoid AND ISNULL(pay.payarres1,'')='' INNER JOIN QL_trncashbankmst cb ON cb.cashbankoid=pay.cashbankoid INNER JOIN QL_mstacctg a ON a.acctgoid=cb.acctgoid INNER JOIN QL_trnpayargiro g ON g.refoid=cb.cashbankoid INNER JOIN QL_trncashbankmst cbx ON cbx.cashbankoid=g.cashbankoid WHERE con.cmpcode='" & DDLBusUnit.SelectedValue & "' and con.reftype='QL_trnjualmst' AND con.payrefoid<>0 AND con.trnarstatus='Post' AND con.trnartype IN ('PAYAR') AND cb.cashbankstatus='Post' AND cb.cashbanktype='BGM' " & sAdd & " UNION ALL "
        
        'Payment DP Retur
        sSql &= "SELECT con.cmpcode, con.refoid, con.custoid, paymentdate tglpay, cashbankdate tglkomisi, -con.amtbayar, 0.00 returidr, 0.00 retur2idr, 0.00 amtbayarother, 'DP Return: ' + cb.cashbankno cashbankno, '' acctgdesc, 0.0 [Kurang Bayar], 0.0 [Debet Note], 0.0 [Credit Note], comstatus, con.conaroid, 0.0 dncnamt FROM QL_conar con INNER JOIN QL_trnpayar pay ON pay.cmpcode=con.cmpcode AND pay.payaroid=con.payrefoid AND ISNULL(pay.payarres1,'')='' INNER JOIN QL_trncashbankmst cb ON cb.cmpcode=pay.cmpcode AND cb.cashbankoid=pay.cashbankoid INNER JOIN (SELECT dp.cmpcode, dp.dparoid, dparpayrefno FROM QL_trndpar dp WHERE LEFT(dparpayrefno, 3)='SRT') dp ON dp.cmpcode=cb.cmpcode AND dp.dparoid=cb.giroacctgoid WHERE con.cmpcode='" & DDLBusUnit.SelectedValue & "' and con.reftype='QL_trnjualmst' AND con.payrefoid<>0 AND con.trnarstatus='Post' AND con.trnartype IN ('PAYAR') AND cashbankstatus='Post' AND cashbanktype='BLM' " & sAdd & " UNION ALL "

        'Giro Cancel
        sSql &= "SELECT con.cmpcode, con.refoid, con.custoid, paymentdate tglpay, paymentdate tglkomisi, con.amtbayar, 0.0 returidr, 0.00 retur2idr, 0.0 amtbayarother, trnarnote cashbankno, '' acctgdesc, 0.0 [Kurang Bayar], 0.0 [Debet Note], 0.0 [Credit Note], comstatus, con.conaroid, 0.0 dncnamt FROM QL_conar con WHERE con.cmpcode='" & DDLBusUnit.SelectedValue & "' and con.reftype='QL_trnjualmst' AND con.trnarstatus='Post' AND con.trnartype IN ('GCAR') " & sAdd
        sSql &= ") AS Tb_Pay "
        sSql &= ") AS Tbl_Payment INNER JOIN "
        sSql &= "(SELECT con.cmpcode, personname sales, trnjualmstoid, trnjualno, con.amttrans, con.trnardate, c.custname, c.custoid, con.refoid, con.reftype, 0.00 komisi, '' statusbyr, p.personoid, c.kode_komisi FROM QL_conar con INNER JOIN QL_mstcust c ON c.custoid=con.custoid INNER JOIN QL_trnjualmst jm ON jm.cmpcode=con.cmpcode AND jm.trnjualmstoid=con.refoid INNER JOIN QL_mstperson p ON p.cmpcode=jm.cmpcode AND p.salescode=jm.salescode WHERE con.cmpcode='" & DDLBusUnit.SelectedValue & "' AND con.payrefoid=0 AND c.kode_komisi<>'' and con.reftype='QL_trnjualmst') AS Tbl_INV "
        sSql &= "ON Tbl_INV.cmpcode=Tbl_Payment.cmpcode AND Tbl_INV.custoid=Tbl_Payment.custoid AND Tbl_INV.refoid=Tbl_Payment.refoid "
        sSql &= "WHERE Tbl_INV.cmpcode='" & DDLBusUnit.SelectedValue & "'"
        'Filter Month and Year
        sSql &= " AND personoid='" & personoid.SelectedValue & "' "
        sSql &= " AND YEAR(CAST([Tgl. Bayar] AS DATETIME))>=2023"

        sSql &= " ORDER BY custname, trnjualno, Tbl_Payment.[Tgl. Bayar] DESC"
        Dim dtData As DataTable = cKon.ambiltabel(sSql, "QL_trnkomisisalesmst")

        'Load table sales commission
        sSql = "SELECT rangeone, rangetwo, insentiverate, payrangeone, payrangetwo, commissionrate, personname, kode_komisi FROM QL_tis_mstcommission cm INNER JOIN QL_tis_mstcommissiondtl cd ON cm.commissionoid=cd.commissionoid INNER JOIN QL_mstperson p ON p.personoid=cm.salesteamoid WHERE commissionstatus='ACTIVE'"
        Dim dtView As DataView = cKon.ambiltabel(sSql, "QL_tis_mstcommission").DefaultView

        For C1 As Integer = 0 To dtData.Rows.Count - 1
            Dim dTotalInv As Double = dtData.Compute("SUM(amttrans)", "sales='" & dtData.Rows(C1)("sales") & "'")
            Dim iDays As Integer = dtData.Rows(C1)("jml_hari")

            dtView.RowFilter = "personname='" & dtData.Rows(C1)("sales") & "' AND kode_komisi='" & dtData.Rows(C1)("kode_komisi") & "'"
            If dtView.Count > 0 Then
                For C2 As Integer = 0 To dtView.Count - 1
                    If iDays >= ToDouble(dtView(C2)("payrangeone")) And iDays <= ToDouble(dtView(C2)("payrangetwo")) Then
                        dtData.Rows(C1)("percentage") = ToDouble(dtView(C2)("insentiverate"))
                        dtData.Rows(C1)("komisi_pct") = ToDouble(dtView(C2)("insentiverate"))
                        dtData.Rows(C1)("range_1") = ToDouble(dtView(C2)("payrangeone"))
                        dtData.Rows(C1)("range_2") = ToDouble(dtView(C2)("payrangetwo"))
                        dtData.Rows(C1)("kode_komisi") = dtView(C2)("kode_komisi")
                    End If
                    'If dTotalInv >= ToDouble(dtView(C2)("rangeone")) And dTotalInv <= ToDouble(dtView(C2)("rangetwo")) Then
                    '    dtData.Rows(C1)("percentage2") = ToDouble(dtView(C2)("commissionrate"))
                    'End If
                Next
            End If
            dtView.RowFilter = ""
        Next
        For C1 As Integer = 0 To dtData.Rows.Count - 1
            Dim dNett As Double = 0, dPPH As Double = 0
            dtData.Rows(C1)("nmr") = C1 + 1
            dNett = ToDouble(dtData.Rows(C1)("amtbayaridr").ToString) * (dtData.Rows(C1)("percentage") / 100)
            dPPH = dNett * 5 / 100
            dtData.Rows(C1)("ppnamt") = dPPH
            dtData.Rows(C1)("cashbankamt") = ToDouble(dtData.Rows(C1)("amtbayaridr").ToString)
            dtData.Rows(C1)("paycommdtlamt") = dNett - dPPH
            dtData.Rows(C1)("komisi") = dNett - dPPH
        Next
        dtData.AcceptChanges()
        Session("TblPayment") = dtData
        Session("TblPaymentView") = dtData
        gvListCOA.DataSource = Session("TblPaymentView")
        gvListCOA.DataBind()
        cProc.SetModalPopUpExtender(btnHideListCOA, pnlListCOA, mpeListCOA, True)
    End Sub

    Private Sub ClearDetail()
        i_u2.Text = "New Detail"
        cashbankglseq.Text = "1"
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            If dt.Rows.Count > 0 Then
                cashbankglseq.Text = dt.Rows.Count + 1
            End If
        End If
        acctgoid2.Text = ""
        acctgcode.Text = ""
        acctgdesc.Text = ""
        cashbankglnote.Text = ""
        gvDtl.SelectedIndex = -1
        personal.SelectedIndex = -1
        paycommdtlrefno.Text = ""
        paycommdtlrefdesc.Text = ""
        paycommdtlamt.Text = ""
    End Sub

    Private Sub CountHdrAmt()
        Dim dVal As Double = 0
        If Session("TblDtl") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            If dt.Rows.Count > 0 Then
                For C1 As Integer = 0 To dt.Rows.Count - 1
                    dVal += ToDouble(dt.Rows(C1)("paycommdtlamt").ToString)
                Next
            End If
            paycommamt.Text = ToMaskEdit(dVal, 2)
        End If
    End Sub

    Private Sub GenerateNo()
        Dim sErr As String = ""
        If paycommdate.Text <> "" Then
            If IsValidDate(paycommdate.Text, "MM/dd/yyyy", sErr) Then
                Dim sNo As String = "PAYCOM-" & Format(CDate(paycommdate.Text), "yyMM") & "-"
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(paycommno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnpaycommmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND paycommno LIKE '%" & sNo & "%'"
                If GetStrData(sSql) = "" Then
                    paycommno.Text = GenNumberString(sNo, "", 1, DefaultFormatCounter)
                Else
                    paycommno.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
                End If
            End If
        End If
    End Sub

    Private Sub CreateTblDetail()
        If Session("TblDtl") Is Nothing Then
            Dim dtlTable As DataTable = New DataTable("TabelExpense")
            dtlTable.Columns.Add("paycommdtlseq", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("conaroid", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("paycommdtlrefno", Type.GetType("System.String"))
            dtlTable.Columns.Add("paycommdtlrefdesc", Type.GetType("System.String"))
            dtlTable.Columns.Add("paycommdtlamt", Type.GetType("System.Double"))
            dtlTable.Columns.Add("paycommdtlamtidr", Type.GetType("System.Double"))
            dtlTable.Columns.Add("paycommdtlamtusd", Type.GetType("System.Double"))
            dtlTable.Columns.Add("paycommdtlnote", Type.GetType("System.String"))
            dtlTable.Columns.Add("custname", Type.GetType("System.String"))
            dtlTable.Columns.Add("paymentdate", Type.GetType("System.String"))
            dtlTable.Columns.Add("cashbankamt", Type.GetType("System.Double"))
            dtlTable.Columns.Add("ppnamt", Type.GetType("System.Double"))
            dtlTable.Columns.Add("komisi", Type.GetType("System.Double"))
            dtlTable.Columns.Add("range_1", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("range_2", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("komisi_pct", Type.GetType("System.Double"))
            dtlTable.Columns.Add("kode_komisi", Type.GetType("System.String"))
            Session("TblDtl") = dtlTable
        End If
    End Sub

    Private Sub UpdateCheckedMat()
        If Session("TblPayment") IsNot Nothing Then
            For C1 As Integer = 0 To gvListCOA.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListCOA.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                            Dim sOid2 As String = CType(myControl, System.Web.UI.WebControls.CheckBox).CssClass
                            Session("TblPayment").DefaultView.RowFilter = "paycommdtlrefdesc='" & sOid2 & "' AND conaroid=" & sOid
                            If cbcheck = True Then
                                If Session("TblPayment").DefaultView.Count > 0 Then
                                    Session("TblPayment").DefaultView(0)("checkvalue") = "True"
                                End If
                            Else
                                If Session("TblPayment").DefaultView.Count > 0 Then
                                    Session("TblPayment").DefaultView(0)("checkvalue") = "False"
                                End If
                            End If
                            Session("TblPayment").DefaultView.RowFilter = ""
                        End If
                    Next
                End If
            Next
        End If
        If Session("TblPaymentView") IsNot Nothing Then
            For C1 As Integer = 0 To gvListCOA.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListCOA.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                            Dim sOid2 As String = CType(myControl, System.Web.UI.WebControls.CheckBox).CssClass
                            Session("TblPaymentView").DefaultView.RowFilter = "paycommdtlrefdesc='" & sOid2 & "' AND conaroid=" & sOid
                            If cbcheck = True Then
                                If Session("TblPaymentView").DefaultView.Count > 0 Then
                                    Session("TblPaymentView").DefaultView(0)("checkvalue") = "True"
                                End If
                            Else
                                If Session("TblPaymentView").DefaultView.Count > 0 Then
                                    Session("TblPaymentView").DefaultView(0)("checkvalue") = "False"
                                End If
                            End If
                            Session("TblPaymentView").DefaultView.RowFilter = ""
                        End If
                    Next
                End If
            Next
        End If
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnCommisionPayment.aspx")
        End If
        If checkPagePermission("~\Transaction\trnCommisionPayment.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Commision Payment"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            I_U.Text = "New Data"
        Else
            I_U.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPost.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            InitDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox()
                TabContainer1.ActiveTabIndex = 1
            Else
                paycommdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                groupoid.Text = GenerateID("QL_trnpaycommmst", CompnyCode)
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
                GenerateNo()
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                cProc.SetModalPopUpExtender(btnHideListCOA, pnlListCOA, mpeListCOA, True)
            End If
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
    End Sub

    Protected Sub gvMst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvMst.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 2)
        End If
    End Sub

    Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvMst.DataSource = Session("TblMst")
            gvMst.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitCBL()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_trnpaycommmst WHERE paycommoid=" & groupoid.Text
                If CheckDataExists(sSql) Then
                    groupoid.Text = GenerateID("QL_TRNPAYCOMMMST", CompnyCode)
                End If
                sSql = "SELECT COUNT(*) FROM QL_trnpaycommmst WHERE paycommno='" & paycommno.Text & "'"
                If CheckDataExists(sSql) Then
                    GenerateNo()
                End If
            End If
            groupdtloid.Text = GenerateID("QL_TRNPAYCOMMDTL", CompnyCode)
            Dim cRate As New ClassRate()
            cRate.SetRateValue(1, paycommdate.Text)
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trnpaycommmst (cmpcode, paycommoid, periodacctg, paycommno, paycommdate, personoid, paycommamt, paycommamtidr, paycommamtusd, paycommnote, paycommstatus, createuser, createtime, upduser, updtime) VALUES ('" & CompnyCode & "', " & groupoid.Text & ", '" & GetDateToPeriodAcctg(CDate(paycommdate.Text)) & "', '" & paycommno.Text & "', '" & paycommdate.Text & "', " & personoid.SelectedValue & ", " & ToDouble(paycommamt.Text) & ", " & ToDouble(paycommamt.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(paycommamt.Text) * cRate.GetRateMonthlyUSDValue & ", '" & Tchar(groupnote.Text) & "', '" & activeflag.SelectedValue & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP) "
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & groupoid.Text & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trnpaycommmst'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trnpaycommmst SET personoid=" & personoid.SelectedValue & ", periodacctg='" & GetDateToPeriodAcctg(CDate(paycommdate.Text)) & "', paycommamt=" & ToDouble(paycommamt.Text) & ", paycommamtidr=" & ToDouble(paycommamt.Text) * cRate.GetRateMonthlyIDRValue & ", paycommamtusd=" & ToDouble(paycommamt.Text) * cRate.GetRateMonthlyUSDValue & ", paycommnote='" & Tchar(groupnote.Text) & "', paycommstatus='" & activeflag.SelectedValue & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & CompnyCode & "' AND paycommoid=" & groupoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnjualmst SET trnjualres3='', komisisales=0, komisiamtidr=0, komisiamtusd=0 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnjualmstoid IN (SELECT x.conaroid FROM QL_trnpaycommdtl x WHERE paycommoid=" & groupoid.Text & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE QL_trnpaycommdtl WHERE cmpcode='" & CompnyCode & "' AND paycommoid=" & groupoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trnpaycommdtl (cmpcode, paycommdtloid, paycommdtlseq, paycommoid, conaroid, paycommdtlrefno, paycommdtlrefdesc, paycommdtlamt, paycommdtlamtidr, paycommdtlamtusd, paycommdtlnote, upduser, updtime, custname, paymentdate, cashbankamt, ppnamt, komisi, range_1, range_2, komisi_pct, kode_komisi) VALUES ('" & CompnyCode & "', " & CInt(groupdtloid.Text) + C1 & ", " & C1 + 1 & ", " & groupoid.Text & ", " & objTable.Rows(C1)("conaroid").ToString & ", '" & Tchar(objTable.Rows(C1)("paycommdtlrefno").ToString) & "', '" & Tchar(objTable.Rows(C1)("paycommdtlrefdesc").ToString) & "', " & ToDouble(objTable.Rows(C1)("paycommdtlamt").ToString) & ", " & ToDouble(objTable.Rows(C1)("paycommdtlamt").ToString) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(objTable.Rows(C1)("paycommdtlamt").ToString) * cRate.GetRateMonthlyUSDValue & ", '" & Tchar(objTable.Rows(C1)("paycommdtlnote").ToString) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Tchar(objTable.Rows(C1)("custname").ToString) & "', '" & Tchar(objTable.Rows(C1)("paymentdate").ToString) & "', " & objTable.Rows(C1)("cashbankamt") & ", " & objTable.Rows(C1)("ppnamt") & ", " & objTable.Rows(C1)("komisi") & ", " & objTable.Rows(C1)("range_1") & ", " & objTable.Rows(C1)("range_2") & ", " & objTable.Rows(C1)("komisi_pct") & ", '" & objTable.Rows(C1)("kode_komisi") & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        
                        sSql = "UPDATE QL_conar SET comstatus='PAID' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND conaroid=" & objTable.Rows(C1)("conaroid").ToString
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(groupdtloid.Text)) & " WHERE tablename='QL_trnpaycommdtl' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If

                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                conn.Close()
                showMessage(ex.Message, 1)
                Exit Sub
            End Try
            Response.Redirect("~\Transaction\trnCommisionPayment.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnCommisionPayment.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If groupoid.Text = "" Then
            showMessage("Please select Division data first!", 1)
            Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trnjualmst SET trnjualres3='', komisisales=0, komisiamtidr=0, komisiamtusd=0 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnjualmstoid IN (SELECT x.conaroid FROM QL_trnpaycommdtl x WHERE paycommoid=" & groupoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE QL_trnpaycommdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND paycommoid=" & groupoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE QL_trnpaycommmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND paycommoid=" & groupoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnCommisionPayment.aspx?awal=true")
    End Sub

    Protected Sub btnFindListCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        UpdateCheckedMat()
        If Session("TblPayment") IsNot Nothing Then
            Session("TblPayment").DefaultView.RowFilter = FilterDDLListCOA.SelectedValue & " LIKE '%" & Tchar(FilterTextListCOA.Text) & "%'"
            If Session("TblPayment").DefaultView.Count > 0 Then
                Session("TblPaymentView") = Session("TblPayment").DefaultView.ToTable
                gvListCOA.DataSource = Session("TblPaymentView")
                gvListCOA.DataBind()
                mpeListCOA.Show()
            Else
                Session("WarningListMat") = "data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "There is no posting Commission data available for this time!"
            showMessage(Session("WarningListKIK"), 2)
        End If
    End Sub

    Protected Sub btnAllListCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListCOA.SelectedIndex = -1 : FilterTextListCOA.Text = "" : gvListCOA.SelectedIndex = -1
        UpdateCheckedMat()
        If Session("WarningListMat") IsNot Nothing Then
            Session("WarningListMatView") = Session("WarningListMat")
            gvListCOA.DataSource = Session("WarningListMatView")
            gvListCOA.DataBind()
            mpeListCOA.Show()
        Else
            Session("WarningListMat") = "There is no posting KIK data available for this time!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub lkbCloseListCOA_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListCOA, pnlListCOA, mpeListCOA, False)
    End Sub

    Protected Sub btnSearchCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListCOA.SelectedIndex = -1 : FilterTextListCOA.Text = "" : gvListCOA.DataSource = Nothing : gvListCOA.DataBind() : Session("WarningListMat") = Nothing : Session("WarningListMatView") = Nothing
        BindListCOA()
    End Sub

    Protected Sub btnClearCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        acctgoid2.Text = ""
        acctgdesc.Text = ""
        acctgcode.Text = ""
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsDetailInputValid() Then
            If Session("TblDtl") Is Nothing Then
                CreateTblDetail()
            End If
            Dim objTable As DataTable = Session("TblDtl")
            Dim dv As DataView = objTable.DefaultView
            If i_u2.Text = "New Detail" Then
                dv.RowFilter = "conaroid=" & acctgoid2.Text
            Else
                dv.RowFilter = "conaroid=" & acctgoid2.Text & " AND paycommdtlseq<>" & cashbankglseq.Text & " "
            End If
            If dv.Count > 0 Then
                dv.RowFilter = ""
                showMessage("This Data has been added before, please check!", 2)
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                cashbankglseq.Text = objTable.Rows.Count + 1
                objRow("paycommdtlseq") = cashbankglseq.Text
            Else
                objRow = objTable.Rows(cashbankglseq.Text - 1)
                objRow.BeginEdit()
            End If
            objRow("conaroid") = acctgoid2.Text
            objRow("paycommdtlrefno") = paycommdtlrefno.Text
            objRow("paycommdtlrefdesc") = paycommdtlrefdesc.Text
            objRow("paycommdtlamt") = ToDouble(paycommdtlamt.Text)
            objRow("paycommdtlnote") = cashbankglnote.Text
            If i_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            Session("TblDtl") = objTable
            gvDtl.DataSource = Session("TblDtl")
            gvDtl.DataBind()
            CountHdrAmt()
            ClearDetail()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDetail()
    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            cashbankglseq.Text = gvDtl.SelectedDataKey.Item("paycommdtlseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "paycommdtlseq=" & cashbankglseq.Text
                acctgoid2.Text = dv.Item(0).Item("conaroid").ToString
                paycommdtlrefno.Text = dv.Item(0).Item("paycommdtlrefno").ToString
                paycommdtlrefdesc.Text = dv.Item(0).Item("paycommdtlrefdesc").ToString
                paycommdtlamt.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("paycommdtlamt").ToString), 2)
                cashbankglnote.Text = dv.Item(0).Item("paycommdtlnote").ToString
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub gvListCOA_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        UpdateCheckedMat()
        gvListCOA.PageIndex = e.NewPageIndex
        gvListCOA.DataSource = Session("TblPaymentView")
        gvListCOA.DataBind()
        mpeListCOA.Show()
    End Sub

    Protected Sub gvDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("paycommdtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvDtl.DataSource = objTable
        gvDtl.DataBind()
        CountHdrAmt()
        ClearDetail()
    End Sub
#End Region

    Protected Sub gvListCOA_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 2)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 2)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 2)
        End If
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        activeflag.SelectedValue = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 2)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 2)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 2)
        End If
    End Sub

    Protected Sub lkbAddToList_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        UpdateCheckedMat()
        If Session("TblPayment") IsNot Nothing Then
            Dim dv As DataView = Session("TblPayment").DefaultView
            dv.RowFilter = "checkvalue='True'"
            Dim iCount As Integer = dv.Count
            If dv.Count > 0 Then
                dv.RowFilter = "checkvalue='True'"
                If Session("TblDtl") Is Nothing Then
                    CreateTblDetail()
                End If
                Dim objTable As DataTable = Session("TblDtl")
                Dim objView As DataView = objTable.DefaultView
                Dim iSeq As Integer = objTable.Rows.Count + 1
                For C1 As Integer = 0 To dv.Count - 1
                    objView.RowFilter = "paycommdtlrefdesc='" & dv(C1)("paycommdtlrefdesc") & "' AND conaroid=" & dv(C1)("conaroid")
                    If objView.Count > 0 Then
                        objView(0)("paycommdtlseq") = iSeq
                        objView(0)("conaroid") = dv(C1)("conaroid")
                        objView(0)("paycommdtlrefno") = dv(C1)("paycommdtlrefno")
                        objView(0)("paycommdtlrefdesc") = dv(C1)("paycommdtlrefdesc").ToString
                        objView(0)("paycommdtlamt") = dv(C1)("paycommdtlamt").ToString
                        objView(0)("paycommdtlnote") = cashbankglnote.Text
                        objView(0)("custname") = dv(C1)("custname").ToString
                        objView(0)("paymentdate") = dv(C1)("paymentdate").ToString
                        objView(0)("cashbankamt") = dv(C1)("cashbankamt").ToString
                        objView(0)("ppnamt") = dv(C1)("ppnamt").ToString
                        objView(0)("komisi") = dv(C1)("komisi").ToString
                        objView(0)("range_1") = dv(C1)("range_1").ToString
                        objView(0)("range_2") = dv(C1)("range_2").ToString
                        objView(0)("komisi_pct") = dv(C1)("komisi_pct").ToString
                        objView(0)("kode_komisi") = dv(C1)("kode_komisi").ToString
                    Else
                        Dim rv As DataRowView = objView.AddNew()
                        rv.BeginEdit()

                        rv("paycommdtlseq") = iSeq
                        rv("conaroid") = dv(C1)("conaroid")
                        rv("paycommdtlrefno") = dv(C1)("paycommdtlrefno")
                        rv("paycommdtlrefdesc") = dv(C1)("paycommdtlrefdesc").ToString
                        rv("paycommdtlamt") = dv(C1)("paycommdtlamt").ToString
                        rv("paycommdtlnote") = cashbankglnote.Text
                        rv("custname") = dv(C1)("custname").ToString
                        rv("paymentdate") = dv(C1)("paymentdate").ToString
                        rv("cashbankamt") = dv(C1)("cashbankamt").ToString
                        rv("ppnamt") = dv(C1)("ppnamt").ToString
                        rv("komisi") = dv(C1)("komisi").ToString
                        rv("range_1") = dv(C1)("range_1").ToString
                        rv("range_2") = dv(C1)("range_2").ToString
                        rv("komisi_pct") = dv(C1)("komisi_pct").ToString
                        rv("kode_komisi") = dv(C1)("kode_komisi").ToString

                        rv.EndEdit()
                        iSeq += 1
                    End If
                    objView.RowFilter = ""
                Next
                dv.RowFilter = ""
                Session("TblDtl") = objTable
                gvDtl.DataSource = Session("TblDtl")
                gvDtl.DataBind()
                ClearDetail()
                CountHdrAmt()
                cProc.SetModalPopUpExtender(btnHideListCOA, pnlListCOA, mpeListCOA, False)
            Else
                Session("WarningListMat") = "Please select Commission data first!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "There is no posting Commission data available for this time!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub
End Class
