<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnCommisionCalc.aspx.vb" Inherits="Master_DepartmentGroup" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Commision Calculation" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Commision Calculation :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 10%" class="Label" align=left><asp:Label id="lblFilter" runat="server" Text="Filter"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="personname">Sales</asp:ListItem>
<asp:ListItem Value="t_spv_comm_calc_note">Note</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="200px"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="100px"><asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
</asp:DropDownList> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvMst" runat="server" ForeColor="#333333" Width="100%" CellPadding="4" PageSize="8" AutoGenerateColumns="False" GridLines="None" AllowPaging="True" AllowSorting="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="paycommoid" DataNavigateUrlFormatString="~/Transaction/trnCommisionCalc.aspx?oid={0}" DataTextField="paycommno" HeaderText="Calc No." SortExpression="paycommno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="paycommdate" HeaderText="Date" SortExpression="paycommdate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="personname" HeaderText="Supervisor">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="paycommamt" HeaderText="Amount" SortExpression="paycommamt">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="paycommstatus" HeaderText="Status" SortExpression="activeflag">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="paycommnote" HeaderText="Note" SortExpression="groupnote">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3></TD><TD style="HEIGHT: 5px" class="Label" align=left colSpan=1></TD><TD style="HEIGHT: 5px" class="Label" align=left colSpan=1></TD><TD style="HEIGHT: 5px" class="Label" align=left colSpan=1></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="I_U" runat="server" CssClass="Important" Text="New Data"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="groupoid" runat="server" Visible="False"></asp:Label> <asp:Label id="groupdtloid" runat="server" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label18" runat="server" __designer:wfdid="w1">Pay. No.</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="paycommno" runat="server" CssClass="inpTextDisabled" Width="188px" __designer:wfdid="w7" Enabled="False" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label15" runat="server" __designer:wfdid="w1">Date</asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="paycommdate" runat="server" CssClass="inpTextDisabled" Width="80px" __designer:wfdid="w2" Enabled="False"></asp:TextBox>&nbsp;<asp:Label id="Label16" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w3"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label8" runat="server" __designer:wfdid="w1">Supervisor</asp:Label> <asp:Label id="Label10" runat="server" CssClass="Important" Text="*" __designer:wfdid="w2"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="personoid" runat="server" CssClass="inpText" Width="192px" __designer:wfdid="w3"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label17" runat="server" __designer:wfdid="w4">Amount</asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="paycommamt" runat="server" CssClass="inpTextDisabled" Width="98px" __designer:wfdid="w5" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label6" runat="server">Note</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="groupnote" runat="server" CssClass="inpText" Width="300px" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label5" runat="server">Status</asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="activeflag" runat="server" CssClass="inpTextDisabled" Width="105px" Enabled="False"><asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left colSpan=3></TD><TD class="Label" align=left colSpan=1></TD><TD class="Label" align=left colSpan=1></TD><TD class="Label" align=left colSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Label id="Label4" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" __designer:wfdid="w4">Detail Payment :</asp:Label> <asp:Label id="acctgoid2" runat="server" Visible="False" __designer:wfdid="w30"></asp:Label> <asp:Label id="acctgcode" runat="server" Visible="False" __designer:wfdid="w31"></asp:Label> <asp:Label id="i_u2" runat="server" Text="New Detail" Visible="False" __designer:wfdid="w32"></asp:Label> <asp:Label id="cashbankglseq" runat="server" Visible="False" __designer:wfdid="w33">1</asp:Label></TD><TD class="Label" align=left colSpan=1></TD><TD class="Label" align=left colSpan=1></TD><TD class="Label" align=left colSpan=1></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text="Periode" __designer:wfdid="w1"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="FilterFrom" runat="server" CssClass="inpText" Width="75px" __designer:wfdid="w13"></asp:TextBox>&nbsp;<asp:ImageButton id="btnDateFrom" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w21"></asp:ImageButton>&nbsp;to <asp:TextBox id="FilterTo" runat="server" CssClass="inpText" Width="75px" __designer:wfdid="w14"></asp:TextBox>&nbsp;<asp:ImageButton id="btnDateTo" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w22"></asp:ImageButton>&nbsp;<asp:Label id="Label2" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w15"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:DropDownList id="FilterDDLPay" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w1"><asp:ListItem Value="personname">Sales</asp:ListItem>
<asp:ListItem Value="custname">Customer</asp:ListItem>
<asp:ListItem Value="trnjualno">Invoice No.</asp:ListItem>
<asp:ListItem Value="No. Cash Bank">CashBank. No.</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="FilterTextPay" runat="server" CssClass="inpText" Width="300px" __designer:wfdid="w2" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3><ajaxToolkit:MaskedEditExtender id="MeeDateFrom" runat="server" __designer:wfdid="w16" MaskType="Date" Mask="99/99/9999" UserDateFormat="MonthDayYear" TargetControlID="FilterFrom"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="MeeDateTo" runat="server" __designer:wfdid="w18" MaskType="Date" Mask="99/99/9999" UserDateFormat="MonthDayYear" TargetControlID="FilterTo"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="ceDateFrom" runat="server" __designer:wfdid="w19" TargetControlID="FilterFrom" Format="MM/dd/yyyy" PopupButtonID="btnDateFrom"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="ceDateTo" runat="server" __designer:wfdid="w20" TargetControlID="FilterTo" Format="MM/dd/yyyy" PopupButtonID="btnDateTo"></ajaxToolkit:CalendarExtender></TD><TD style="HEIGHT: 5px" class="Label" align=left colSpan=1></TD><TD style="HEIGHT: 5px" class="Label" align=left colSpan=1></TD><TD style="HEIGHT: 5px" class="Label" align=left colSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=3 rowSpan=1><asp:ImageButton id="btnAddToList" onclick="btnAddToList_Click" runat="server" ImageUrl="~/Images/showdata.png" ImageAlign="AbsMiddle" __designer:wfdid="w13"></asp:ImageButton></TD><TD class="Label" align=left colSpan=1 rowSpan=1></TD><TD class="Label" align=left colSpan=1 rowSpan=1></TD><TD class="Label" align=left colSpan=1 rowSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><asp:Panel id="Panel2" runat="server" Width="100%" __designer:wfdid="w23" ScrollBars="Vertical" Height="300px"><asp:GridView id="gvDtl" runat="server" ForeColor="#333333" Width="98%" __designer:wfdid="w796" OnRowDataBound="gvDtl_RowDataBound" CellPadding="4" PageSize="5" AutoGenerateColumns="False" GridLines="None" DataKeyNames="paycommdtlseq" OnSelectedIndexChanged="gvDtl_SelectedIndexChanged" OnRowDeleting="gvDtl_RowDeleting">
<RowStyle BackColor="#EFF3FB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="personname" HeaderText="Sales">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="paycommdtlrefno" HeaderText="Invoice No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="paycommdtlrefdesc" HeaderText="Pay. No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="paymentdate" HeaderText="Pay. Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankamt" HeaderText="Pay. Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="komisiamtidr" HeaderText="Komisi Sales">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="komisi_nett" HeaderText="Nett">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ppnamt" HeaderText="2.5%">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="paycommdtlamt" HeaderText="Komisi SPV">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="paycommdtlnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label7" runat="server" ForeColor="Red" Text="No detail data !!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=3></TD><TD style="HEIGHT: 10px" class="Label" align=left colSpan=1></TD><TD style="HEIGHT: 10px" class="Label" align=left colSpan=1></TD><TD style="HEIGHT: 10px" class="Label" align=left colSpan=1></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton id="btnPost" onclick="btnPost_Click" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w14"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w25" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w26"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Commision Calculation :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upListCOA" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListCOA" runat="server" CssClass="modalBox" Width="850px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListCOA" runat="server" Font-Size="Medium" Font-Bold="True" Font-Underline="False">List Of Sales Commision</asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListCOA" runat="server" CssClass="Label" Width="100%" DefaultButton="btnFindListCOA">Filter : <asp:DropDownList id="FilterDDLListCOA" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="custname">Customer</asp:ListItem>
<asp:ListItem Value="paycommdtlrefno">Invoice No.</asp:ListItem>
<asp:ListItem Value="paycommdtlrefdesc">Ref. Desc</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListCOA" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListCOA" onclick="btnFindListCOA_Click" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListCOA" onclick="btnAllListCOA_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton><asp:ImageButton id="ImageButton1" onclick="btnAllListCOA_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w1"></asp:ImageButton></asp:Panel></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListCOA" runat="server" ForeColor="#333333" Width="99%" DataKeyNames="conaroid,paycommdtlrefno,paycommdtlrefdesc,paycommdtlamt" GridLines="None" AutoGenerateColumns="False" PageSize="8" CellPadding="4" OnRowDataBound="gvListCOA_RowDataBound" OnPageIndexChanging="gvListCOA_PageIndexChanging" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbOnListMat" runat="server" CssClass='<%# eval("paycommdtlrefdesc") %>' __designer:wfdid="w2" ToolTip='<%# eval("conaroid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="paycommdtlrefno" HeaderText="Invoice No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="paycommdtlrefdesc" HeaderText="Pay. No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="paymentdate" HeaderText="Pay. Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankamt" HeaderText="Pay. Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ppnamt" HeaderText="2.5%">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="paycommdtlamt" HeaderText="Komisi">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label14" runat="server" CssClass="Important" Text="No data found !!" __designer:wfdid="w2"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD vAlign=top align=center colSpan=3><asp:LinkButton id="lkbAddToList" onclick="lkbAddToList_Click" runat="server" __designer:wfdid="w4">[ Add To List ]</asp:LinkButton>&nbsp;-&nbsp;<asp:LinkButton id="lkbCloseListCOA" onclick="lkbCloseListCOA_Click" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHideListCOA" runat="server" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="mpeListCOA" runat="server" PopupControlID="pnlListCOA" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListCOA" Drag="True" TargetControlID="btnHideListCOA"></ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

