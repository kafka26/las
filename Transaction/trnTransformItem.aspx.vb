Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_FinishGoodTransfer
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If transfromwhoid.SelectedValue = "" Then
            sError &= "- Please select FROM WH field!<BR>"
        End If
        If transtowhoid.SelectedValue = "" Then
            sError &= "- Please select TO WH field!<BR>"
        End If
        If matoid.Text = "" Then
            sError &= "- Please select Item From field!<BR>"
        End If
        If matoid2.Text = "" Then
            sError &= "- Please select Item To field!<BR>"
        End If
        If transqty.Text = "" Then
            sError &= "- Please fill QTY field!<BR>"
        Else
            If ToDouble(transqty.Text) <= 0 Then
                sError &= "- QTY field must be more than 0!<BR>"
            Else
                If ToDouble(transqty.Text) > ToDouble(stockqty.Text) Then
                    sError &= "- QTY field must be less than STOCK QTY!<BR>"
                End If
            End If
        End If
        If transunitoid.SelectedValue = "" Then
            sError &= "- Please select UNIT field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If transdate.Text = "" Then
            sError &= "- Please fill TRANSFORM DATE field!<BR>"
        Else
            If Not IsValidDate(transdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- TRANSFORM DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill Detail Data!<BR>"
        Else
            Dim dtTbl As DataTable = Session("TblDtl")
            If dtTbl.Rows.Count <= 0 Then
                sError &= "- Please fill Detail Data!<BR>"
            Else
                If sError = "" Then
                    For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                        If Not IsStockAvailable(DDLBusUnit.SelectedValue, Format(GetServerTime(), "yyyyMM"), dtTbl.Rows(C1)("matoidfrom"), dtTbl.Rows(C1)("transfromwhoid"), ToDouble((dtTbl.Compute("SUM(transqty)", "matoidfrom=" & dtTbl.Rows(C1)("matoidfrom") & " AND transfromwhoid=" & dtTbl.Rows(C1)("transfromwhoid") & "")).ToString)) Then
                            sError &= "- Transform Qty for Code " & dtTbl.Rows(C1)("matcodefrom").ToString & " must be less than Stock Qty!<BR>"
                            Exit For
                        End If
                    Next
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            transmststatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function GetStockValue(ByVal sOid As String, ByVal sRef As String, Optional ByVal sType As String = "IDR") As Double
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        Try
            GetStockValue = ToDouble(GetStrData("select dbo.getStockValue(" & sOid & ")"))
        Catch ex As Exception
            GetStockValue = 0
        End Try
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trntransformitemmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND transformitemmststatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnTransformItem.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lbInProcess.Visible = True
            lbInProcess.Text = "You have " & ToDouble(GetStrData(sSql)) & " In Process Finish Good Transfer data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitAllDDL()
        Dim sError As String = ""
        ' Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        sSql &= " ORDER BY divname"
        If FillDDL(DDLBusUnit, sSql) Then
            InitDDLFromWH()
        End If
        ' Fill DDL Unit
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='UNIT'"
        If Session("oid") Is Nothing Or Session("oid") = "" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
        FillDDL(transunitoid, sSql)
        If sError <> "" Then
            showMessage(sError, 2)
            btnSave.Visible = False
            btnposting.Visible = False
        End If
    End Sub

    Private Sub InitDDLFromWH()
        ' Fill DDL Warehouse
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='WAREHOUSE' AND genoid>0 AND gencode<>'OTW' "
        If Session("oid") Is Nothing Or Session("oid") = "" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
        If FillDDL(transfromwhoid, sSql) Then
            InitDDLToWH()
        End If
    End Sub

    Private Sub InitDDLToWH()
        ' Fill DDL Warehouse
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='WAREHOUSE' AND genoid>0 AND gencode<>'OTW'"
        If Session("oid") Is Nothing Or Session("oid") = "" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
        FillDDL(transtowhoid, sSql)
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT * FROM (SELECT tm.cmpcode, 'False' AS checkvalue, divname, transformitemmstoid AS transmstoid, transformitemno AS transno, CONVERT(VARCHAR(10), transformitemdate, 101) AS transdate, transformitemdocrefno AS transdocrefno, transformitemmststatus AS transmststatus, transformitemmstnote AS transmstnote, tm.updtime, tm.createuser FROM QL_trntransformitemmst tm INNER JOIN QL_mstdivision div ON div.cmpcode=tm.cmpcode) AS QL_trntransmst WHERE"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " cmpcode='" & Session("CompnyCode") & "'"
        Else
            sSql &= " cmpcode LIKE '%'"
        End If
        If checkPagePermission("~\Transaction\trnTransformItem.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND createuser='" & Session("UserID") & "'"
        End If
        sSql &= sSqlPlus & " ORDER BY CAST(transdate AS DATETIME) DESC, transmstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trntransmst")
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvMst.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvMst.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvMst.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "transmstoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub PrintReport()
        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("transmstoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            report.Load(Server.MapPath(folderReport & "rptTransfer.rpt"))
            sSql = "SELECT * FROM (SELECT tm.cmpcode, (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=tm.cmpcode) [Business Unit], tm.transformitemmstoid [ID Hdr], CAST(tm.transformitemmstoid AS VARCHAR(10)) [Draft No.], transformitemno [Transfer No.], transformitemdate [Transfer Date], transformitemdocrefno [Doc. Ref. No.], transformitemmstnote [Header Note], transformitemmststatus [Status], UPPER(tm.createuser) [Create User], tm.createtime [Create Time], (CASE transformitemmststatus WHEN 'In Process' THEN '' ELSE UPPER(tm.upduser) END) [Post User], (CASE transformitemmststatus WHEN 'In Process' THEN CAST('01/01/1900' AS DATETIME) ELSE UPPER(tm.updtime) END) [Post Time], transformitemdtloid [ID Dtl], transformitemdtlseq [No.], g1.gendesc [From WH], g2.gendesc [To WH], itemcode [Code], itemlongdescription [Description], transformitemqty [Qty], g3.gendesc [Unit], transformitemdtlnote [Detail Note], transformitemmstto [Transfer To], ISNULL(transformitemmstaddr,'') [Address] , transformitemmstcityOid, g4.gendesc [City] FROM QL_trntransformitemmst tm INNER JOIN QL_trntransformitemdtl td ON td.cmpcode=tm.cmpcode AND td.transformitemmstoid=tm.transformitemmstoid INNER JOIN QL_mstgen g1 ON g1.genoid=transformitemfromwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=transformitemtowhoid INNER JOIN QL_mstitem m ON m.itemoid=td.itemoid INNER JOIN QL_mstgen g3 ON g3.genoid=transformitemunitoid INNER JOIN QL_mstgen g4 ON g4.gengroup='CITY' AND g4.genoid=transformitemmstcityOid) AS tblTransfer WHERE"
            If Session("CompnyCode") <> CompnyCode Then
                sSql &= " cmpcode='" & Session("CompnyCode") & "'"
            Else
                sSql &= " cmpcode LIKE '%'"
            End If
            If sOid = "" Then
                sSql &= " AND " & FilterDDL2.Items(FilterDDL.SelectedIndex).Value & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sSql &= " AND [Transfer Date]>=CAST('" & FilterPeriod1.Text & " 00:00:00' AS DATETIME) AND [Transfer Date]<=CAST('" & FilterPeriod2.Text & " 23:59:59' AS DATETIME)"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sSql &= " AND [Status]='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Transaction\trnTransformItem.aspx", Session("SpecialAccess")) = False Then
                    sSql &= " AND [Create User]=UPPER('" & Session("UserID") & "')"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sSql &= " AND [ID Hdr] IN (" & sOid & ")"
            End If
            Dim dtReport As DataTable = cKon.ambiltabel(sSql, "tblTransfer")
            report.SetDataSource(dtReport)
            report.SetParameterValue("MaterialType", "FINISH GOOD")
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            cProc.SetDBLogonForReport(report)
            report.PrintOptions.PaperSize = PaperSize.PaperA4
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "MaterialTransferPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
        Response.Redirect("~\Transaction\trnTransformItem.aspx?awal=true")
    End Sub

    Private Sub BindMaterialData()
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sSql = "SELECT 'False' AS Checkvalue, 'True' AS enableqty, m.itemoid AS matoidfrom, itemcode AS matcodefrom, itemshortdescription AS matshortdescfrom, itemlongdescription AS matlongdescfrom, itemunit1 AS transunitoid, gendesc AS transunit, (select dbo.getstockqty(m.itemoid, " & transfromwhoid.SelectedValue & ")) AS stockqty, 0.0 AS transqty, '' AS transdtlnote FROM QL_mstitem m INNER JOIN QL_mstgen g ON genoid=itemunit1 ORDER BY matcodefrom"

        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstitem")
        If dt.Rows.Count > 0 Then
            For C1 As Integer = 0 To dt.Rows.Count - 1
                dt.Rows(C1)("transqty") = ToDouble(dt.Rows(C1)("stockqty").ToString)
            Next
            Session("TblMat") = dt
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMat")
            gvListMat.DataBind()
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            showMessage("Material data can't be found!", 2)
        End If
    End Sub

    Private Sub BindMaterialData2()
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sSql = "SELECT 'False' AS Checkvalue, 'True' AS enableqty, itemoid AS matoidto, itemcode AS matcodeto, itemshortdescription AS matshortdescto, itemlongdescription AS matlongdescto, itemunit1 AS transunitoid, gendesc AS transunit, (select dbo.getstockqty(m.itemoid, " & transtowhoid.SelectedValue & ")) AS stockqty, 0.0 AS transqty, '' AS transdtlnote FROM QL_mstitem m INNER JOIN QL_mstgen g ON genoid=itemunit1 WHERE m.itemoid<>" & ToInteger(matoid.Text) & " ORDER BY matcodeto"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstitem2")
        If dt.Rows.Count > 0 Then
            Session("TblMat2") = dt
            Session("TblMatView2") = dt
            gvListMat2.DataSource = Session("TblMat2")
            gvListMat2.DataBind()
            cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, True)
        Else
            showMessage("Material data can't be found!", 2)
        End If
    End Sub

    Private Sub UpdateCheckedListMat()
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matoidfrom=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("transqty") = ToDouble(GetTextBoxValue(row.Cells(4).Controls))
                                    dtView(0)("transdtlnote") = GetTextBoxValue(row.Cells(6).Controls)
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
            End If
        End If
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matoidfrom=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("transqty") = ToDouble(GetTextBoxValue(row.Cells(4).Controls))
                                    dtView(0)("transdtlnote") = GetTextBoxValue(row.Cells(6).Controls)
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMatView") = dtTbl
            End If
        End If
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("TblTransferDtl")
        dtlTable.Columns.Add("transdtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("transfromwhoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("transfromwh", Type.GetType("System.String"))
        dtlTable.Columns.Add("transtowhoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("transtowh", Type.GetType("System.String"))
        dtlTable.Columns.Add("matoidfrom", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matoidto", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matcodefrom", Type.GetType("System.String"))
        dtlTable.Columns.Add("matlongdescfrom", Type.GetType("System.String"))
        dtlTable.Columns.Add("matcodeto", Type.GetType("System.String"))
        dtlTable.Columns.Add("matlongdescto", Type.GetType("System.String"))
        dtlTable.Columns.Add("transqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("stockqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("transunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("transunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("transdtlnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("transvalueidr", Type.GetType("System.Double"))
        dtlTable.Columns.Add("transvalueusd", Type.GetType("System.Double"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub ClearDetail()
        transdtlseq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                transdtlseq.Text = objTable.Rows.Count + 1
                EnableHeader(False)
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        i_u2.Text = "New Detail"
        'transfromwhoid.SelectedIndex = -1
        'transtowhoid.SelectedIndex = -1
        matoid.Text = ""
        matcode.Text = ""
        matlongdesc.Text = ""
        matoid2.Text = ""
        matcode2.Text = ""
        matlongdesc2.Text = ""
        transqty.Text = ""
        stockqty.Text = ""
        transunitoid.SelectedIndex = -1
        transdtlnote.Text = ""
        gvDtl.SelectedIndex = -1
        transfromwhoid.Enabled = True : transfromwhoid.CssClass = "inpText"
        btnSearchMat.Visible = True
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal : DDLBusUnit.CssClass = sCss
    End Sub

    Private Sub GenerateNo()
        Dim sNo As String = "TRFM-" & Format(CDate(transdate.Text), "yyyy.MM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(transformitemno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trntransformitemmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND transformitemno LIKE '" & sNo & "%'"
        If GetStrData(sSql) = "" Then
            transno.Text = GenNumberString(sNo, "", 1, DefaultFormatCounter)
        Else
            transno.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        End If
    End Sub

    Private Sub FillStockValue()
        Dim dt As DataTable = Session("TblDtl")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(C1)("transvalueidr") = GetStockValue(dt.Rows(C1)("matoidfrom").ToString, "ITEM")
            dt.Rows(C1)("transvalueusd") = GetStockValue(dt.Rows(C1)("matoidfrom").ToString, "ITEM", "USD")
        Next
        dt.AcceptChanges()
        Session("TblDtl") = dt
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT cmpcode, transformitemmstoid, periodacctg, transformitemdate, transformitemno, transformitemdocrefno, transformitemmstnote, transformitemmststatus, createuser, createtime, upduser, updtime FROM QL_trntransformitemmst WHERE transformitemmstoid=" & sOid
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = xreader("cmpcode").ToString
                InitDDLFromWH()
                transmstoid.Text = xreader("transformitemmstoid").ToString
                periodacctg.Text = xreader("periodacctg").ToString
                transdate.Text = Format(xreader("transformitemdate"), "MM/dd/yyyy")
                transno.Text = xreader("transformitemno").ToString
                transdocrefno.Text = xreader("transformitemdocrefno").ToString
                transmstnote.Text = xreader("transformitemmstnote").ToString
                transmststatus.Text = xreader("transformitemmststatus").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            Exit Sub
            btnSave.Visible = False : btnDelete.Visible = False : btnposting.Visible = False
        End Try
        If transmststatus.Text = "Post" Or transmststatus.Text = "Closed" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnposting.Visible = False
            lblspuno.Text = "Transform No."
            transmstoid.Visible = False
            transno.Visible = True
            btnAddToList.Visible = False
            gvDtl.Columns(0).Visible = False
            gvDtl.Columns(gvDtl.Columns.Count - 1).Visible = False
        End If
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sSql = "SELECT transformitemdtlseq AS transdtlseq, transformitemfromwhoid AS transfromwhoid, g1.gendesc AS transfromwh, transformitemtowhoid AS transtowhoid, g2.gendesc AS transtowh, td.itemoidfrom AS matoidfrom, m.itemcode AS matcodefrom, m.itemshortdescription matshortdescfrom, m.itemlongdescription AS matlongdescfrom, td.itemoidto AS matoidto, mx.itemcode AS matcodeto, mx.itemshortdescription matshortdescto, mx.itemlongdescription AS matlongdescto, transformitemqty AS transqty, (select dbo.getstockqty(td.itemoidfrom, transformitemfromwhoid)) AS stockqty, transformitemunitoid AS transunitoid, g3.gendesc AS transunit, transformitemdtlnote AS transdtlnote, transformitemvalueidr AS transvalueidr, transformitemvalueusd AS transvalueusd FROM QL_trntransformitemdtl td INNER JOIN QL_mstitem m ON m.itemoid=td.itemoidfrom INNER JOIN QL_mstitem mx ON mx.itemoid=td.itemoidto INNER JOIN QL_mstgen g1 ON g1.genoid=transformitemfromwhoid INNER JOIN QL_mstgen g2 ON g2.genoid=transformitemtowhoid INNER JOIN QL_mstgen g3 ON g3.genoid=transformitemunitoid WHERE transformitemmstoid=" & sOid & " ORDER BY transdtlseq"
        Session("TblDtl") = cKon.ambiltabel(sSql, "QL_trntransformitemdtl")
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        ClearDetail()
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            Dim branchcode As String = Session("BranchCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            Session("BranchCode") = branchcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnTransformItem.aspx")
        End If
        If checkPagePermission("~\Transaction\trnTransformItem.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Transform Item"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnposting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            lblTitleListMat.Text = "List Of Item"
            lblTitleListMat2.Text = "List Of Item"
            CheckStatus()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitAllDDL()
            transtowhoid2.Text = ToInteger(GetStrData("SELECT genoid FROM QL_mstgen WHERE gengroup='WAREHOUSE' AND gencode='OTW'"))
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                transmstoid.Text = GenerateID("QL_TRNTRANSFORMITEMMST", CompnyCode)
                transdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                transmststatus.Text = "In Process"
                periodacctg.Text = GetDateToPeriodAcctg(CDate(transdate.Text))
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvDtl.DataSource = dt
            gvDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("WarningListMat2") Is Nothing And Session("WarningListMat2") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat2") Then
                Session("WarningListMat2") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, True)
            End If
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnTransformItem.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lbInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = 0
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = 1
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND transmststatus='In Process'"
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND CAST(transdate AS DATETIME)>=CAST('" & FilterPeriod1.Text & " 00:00:00' AS DATETIME) AND CAST(transdate AS DATETIME)<=CAST('" & FilterPeriod2.Text & " 23:59:59' AS DATETIME)"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND transmststatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        cbStatus.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        FilterDDLStatus.SelectedIndex = -1
        BindTrnData("")
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        UpdateCheckedValue()
        gvMst.PageIndex = e.NewPageIndex
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
    End Sub

    Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvMst.DataSource = Session("TblMst")
            gvMst.DataBind()
        End If
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        UpdateCheckedValue()
        PrintReport()
    End Sub

    Protected Sub transfromwhoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles transfromwhoid.SelectedIndexChanged
        InitDDLToWH()
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2) : Exit Sub
        End If
        If transfromwhoid.SelectedValue = "" Then
            showMessage("Please select From WH first!", 2) : Exit Sub
        End If
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        Session("TblMat") = Nothing : Session("TblMatView") = Nothing : gvListMat.DataSource = Nothing : gvListMat.DataBind()
        BindMaterialData()
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblMat") IsNot Nothing Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        If Session("TblMat") IsNot Nothing Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = ""
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        gvListMat.PageIndex = e.NewPageIndex
        gvListMat.DataSource = Session("TblMatView")
        gvListMat.DataBind()
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 2)
        End If
    End Sub

    Protected Sub cbHdrListMat_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For C1 As Integer = 0 To gvListMat.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        mpeListMat.Show()
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            If Session("TblDtl") Is Nothing Then
                CreateTblDetail()
            End If
            Dim objTable As DataTable
            objTable = Session("TblDtl")
            Dim dv As DataView = objTable.DefaultView
            If i_u2.Text = "New Detail" Then
                dv.RowFilter = "matoidfrom=" & matoid.Text & " AND transfromwhoid=" & transfromwhoid.SelectedValue & " AND transtowhoid=" & transtowhoid.SelectedValue & " AND matoidto=" & matoid2.Text
            Else
                dv.RowFilter = "matoidfrom=" & matoid.Text & " AND transfromwhoid=" & transfromwhoid.SelectedValue & " AND transtowhoid=" & transtowhoid.SelectedValue & " AND matoidto=" & matoid2.Text & " AND transdtlseq<>" & transdtlseq.Text
            End If
            If dv.Count > 0 Then
                showMessage("This Data has been added before, please check!", 2)
                dv.RowFilter = ""
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                objRow("transdtlseq") = objTable.Rows.Count + 1
            Else
                objRow = objTable.Rows(transdtlseq.Text - 1)
                objRow.BeginEdit()
            End If
            objRow("transfromwhoid") = transfromwhoid.SelectedValue
            objRow("transfromwh") = transfromwhoid.SelectedItem.Text
            objRow("transtowhoid") = transtowhoid.SelectedValue
            objRow("transtowh") = transtowhoid.SelectedItem.Text
            objRow("matoidfrom") = matoid.Text
            objRow("matcodefrom") = matcode.Text
            objRow("matlongdescfrom") = matlongdesc.Text
            objRow("matoidto") = matoid2.Text
            objRow("matcodeto") = matcode2.Text
            objRow("matlongdescto") = matlongdesc2.Text
            objRow("stockqty") = ToDouble(stockqty.Text)
            objRow("transqty") = ToDouble(transqty.Text)
            objRow("transunitoid") = transunitoid.SelectedValue
            objRow("transunit") = transunitoid.SelectedItem.Text
            objRow("transdtlnote") = Tchar(transdtlnote.Text)
            objRow("transvalueidr") = 0
            objRow("transvalueusd") = 0
            If i_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            Session("TblDtl") = objTable
            gvDtl.DataSource = objTable
            gvDtl.DataBind()
            ClearDetail()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 2)
        End If
    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDtl.SelectedIndexChanged
        Try
            transdtlseq.Text = gvDtl.SelectedDataKey.Item("transdtlseq").ToString().Trim
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "transdtlseq=" & transdtlseq.Text
                transfromwhoid.SelectedValue = dv(0)("transfromwhoid").ToString
                transfromwhoid_SelectedIndexChanged(Nothing, Nothing)
                transtowhoid.SelectedValue = dv(0)("transtowhoid").ToString
                matoid.Text = dv(0)("matoidfrom").ToString
                matcode.Text = dv(0)("matcodefrom").ToString
                matoid2.Text = dv(0)("matoidto").ToString
                matcode2.Text = dv(0)("matcodeto").ToString
                matlongdesc.Text = dv(0)("matlongdescfrom").ToString
                matlongdesc2.Text = dv(0)("matlongdescto").ToString
                stockqty.Text = ToMaskEdit(ToDouble(dv(0)("stockqty").ToString), 2)
                transqty.Text = ToMaskEdit(ToDouble(dv(0)("transqty").ToString), 2)
                transunitoid.SelectedValue = dv(0)("transunitoid").ToString
                transdtlnote.Text = dv(0)("transdtlnote").ToString
                dv.RowFilter = ""
                transfromwhoid.Enabled = False : transfromwhoid.CssClass = "inpTextDisabled"
                btnSearchMat.Visible = False
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub gvDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("transdtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        ClearDetail()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_trntransformitemmst WHERE transformitemmstoid=" & transmstoid.Text
                If CheckDataExists(sSql) Then
                    transmstoid.Text = GenerateID("QL_TRNTRANSFORMITEMMST", CompnyCode)
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNTRANSFORMITEMMST", "transformitemmstoid", transmstoid.Text, "transformitemmststatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    transmststatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            transdtloid.Text = GenerateID("QL_TRNTRANSFORMITEMDTL", CompnyCode)
            Dim conmtroid As Integer = GenerateID("QL_constock", CompnyCode)
            Dim crdmatoid As Integer = GenerateID("QL_crdstock", CompnyCode)
            Dim iStockAcctgOid As Integer = 0
            Dim dTotalIDR As Double = 0D, dTotalUSD As Double = 0D
            periodacctg.Text = GetDateToPeriodAcctg(CDate(transdate.Text))
            Dim sDate As String = Format(CDate(transdate.Text), "MM/dd/yyyy")
            Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
            If isPeriodClosed(DDLBusUnit.SelectedValue, sDate) Then
                showMessage("Cannot Save data to period " & MonthName(Month(CDate(sDate))).ToUpper & " " & Year(CDate(sDate)).ToString & " anymore because the period has been closed. Please select another period!", 3) : transmststatus.Text = "In Process" : Exit Sub
            End If
            If transmststatus.Text = "Post" Then
                GenerateNo()
                FillStockValue()
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trntransformitemmst (cmpcode, transformitemmstoid, periodacctg, transformitemdate, transformitemno, transformitemdocrefno, transformitemmstnote, transformitemmststatus, createuser, createtime, upduser, updtime) VALUES ('" & DDLBusUnit.SelectedValue & "', " & transmstoid.Text & ", '" & periodacctg.Text & "', '" & transdate.Text & "', '" & transno.Text & "', '" & Tchar(transdocrefno.Text) & "', '" & Tchar(transmstnote.Text) & "', '" & transmststatus.Text & "', '" & createuser.Text & "', '" & createtime.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & transmstoid.Text & " WHERE tablename='QL_TRNTRANSFORMITEMMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trntransformitemmst SET periodacctg='" & periodacctg.Text & "', transformitemdate='" & transdate.Text & "', transformitemno='" & transno.Text & "', transformitemdocrefno='" & Tchar(transdocrefno.Text) & "', transformitemmstnote='" & Tchar(transmstnote.Text) & "', transformitemmststatus='" & transmststatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE transformitemmstoid=" & transmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trntransformitemdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND transformitemmstoid=" & transmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trntransformitemdtl (cmpcode, transformitemdtloid, transformitemmstoid, transformitemdtlseq, transformitemfromwhoid, transformitemtowhoid, itemoidfrom, itemoidto, transformitemqty, transformitemunitoid, transformitemdtlnote, transformitemdtlstatus, upduser, updtime, transformitemvalueidr, transformitemvalueusd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C1 + CInt(transdtloid.Text)) & ", " & transmstoid.Text & ", " & C1 + 1 & ", " & objTable.Rows(C1)("transfromwhoid") & ", " & objTable.Rows(C1)("transtowhoid") & ", " & objTable.Rows(C1)("matoidfrom") & ", " & objTable.Rows(C1)("matoidto") & ", " & ToDouble(objTable.Rows(C1)("transqty").ToString) & ", " & objTable.Rows(C1)("transunitoid") & ", '" & Tchar(objTable.Rows(C1)("transdtlnote").ToString) & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(C1)("transvalueidr").ToString) & ", " & ToDouble(objTable.Rows(C1)("transvalueusd").ToString) & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        dTotalIDR += ToDouble(objTable.Rows(C1)("transvalueidr").ToString) * ToDouble(objTable.Rows(C1)("transqty").ToString)
                        dTotalUSD += ToDouble(objTable.Rows(C1)("transvalueusd").ToString) * ToDouble(objTable.Rows(C1)("transqty").ToString)
                        If transmststatus.Text = "Post" Then
                            ' Insert Con Stock Out
                            sSql = "INSERT INTO  QL_constock(cmpcode,constockoid,contype,trndate,formaction,formoid,periodacctg,refname,refoid,mtrlocoid,qtyin,qtyout,amount,hpp,typemin,note,reason,createuser,createtime,upduser,updtime,refno,deptoid,valueidr,valueusd, qtyin_unitbesar, qtyout_unitbesar) VALUES ('" & DDLBusUnit.SelectedValue & "'," & conmtroid & ",'TRFRM','" & sDate & "','QL_trntransformitemdtl'," & transmstoid.Text & ",'" & sPeriod & "','FINISH GOOD'," & objTable.Rows(C1)("matoidfrom") & "," & objTable.Rows(C1)("transfromwhoid") & ", 0," & ToDouble(objTable.Rows(C1)("transqty").ToString) & ", 0, 0,-1,'" & transno.Text & " dari " & objTable.Rows(C1)("matcodefrom") & " ke " & objTable.Rows(C1)("matcodeto") & "','Material Transformation dari " & objTable.Rows(C1)("matcodefrom") & " ke " & objTable.Rows(C1)("matcodeto") & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'',0," & ToDouble(objTable.Rows(C1)("transvalueidr").ToString) & "," & ToDouble(objTable.Rows(C1)("transvalueidr").ToString) & ", 0," & ToDouble(objTable.Rows(C1)("transqty").ToString) & ")"

                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            conmtroid += 1
                            ' Update Crd Stock Out
                            sSql = "UPDATE QL_crdstock SET qtyout = qtyout + " & ToDouble(objTable.Rows(C1)("transqty").ToString) & ", saldoakhir = saldoakhir - " & ToDouble(objTable.Rows(C1)("transqty").ToString) & ", qtyout_unitbesar = qtyout_unitbesar + " & ToDouble(objTable.Rows(C1)("transqty").ToString) & ", saldoakhir_unitbesar = saldoakhir_unitbesar - " & ToDouble(objTable.Rows(C1)("transqty").ToString) & ", lasttranstype='QL_trntransformitemdtl', lasttransdate='" & sDate & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND refoid=" & objTable.Rows(C1)("matoidfrom") & " AND mtrlocoid=" & objTable.Rows(C1)("transfromwhoid") & " AND periodacctg='" & sPeriod & "'"
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() <= 0 Then
                                ' Insert Crd Stock Out
                                sSql = "INSERT INTO QL_crdstock(cmpcode,crdstockoid,periodacctg,refoid,refname,mtrlocoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir,lasttranstype,lasttransdate,createuser,createtime,upduser,updtime,closeuser,closingdate,refno, qtyin_unitbesar, qtyout_unitbesar, qtyadjin_unitbesar, qtyadjout_unitbesar, saldoawal_unitbesar, saldoakhir_unitbesar) VALUES ('" & DDLBusUnit.SelectedValue & "', " & crdmatoid & ", '" & sPeriod & "', " & objTable.Rows(C1)("matoidfrom") & ", 'FINISH GOOD', " & objTable.Rows(C1)("transfromwhoid") & ", 0, " & ToDouble(objTable.Rows(C1)("transqty").ToString) & ", 0, 0, 0, " & -ToDouble(objTable.Rows(C1)("transqty").ToString) & ", 'QL_trntransformitemdtl', '" & sDate & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "',CURRENT_TIMESTAMP, '', '1/1/1900', '', 0, " & ToDouble(objTable.Rows(C1)("transqty").ToString) & ", 0, 0, 0, " & -ToDouble(objTable.Rows(C1)("transqty").ToString) & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                crdmatoid += 1
                            End If

                            ' Insert Con Stock In
                            sSql = "INSERT INTO  QL_constock(cmpcode,constockoid,contype,trndate,formaction,formoid,periodacctg,refname,refoid,mtrlocoid,qtyin,qtyout,amount,hpp,typemin,note,reason,createuser,createtime,upduser,updtime,refno,deptoid,valueidr,valueusd, qtyin_unitbesar, qtyout_unitbesar) VALUES ('" & DDLBusUnit.SelectedValue & "'," & conmtroid & ",'TRFRM','" & sDate & "','QL_trntransformitemdtl'," & transmstoid.Text & ",'" & sPeriod & "','FINISH GOOD'," & objTable.Rows(C1)("matoidto") & "," & objTable.Rows(C1)("transtowhoid") & "," & ToDouble(objTable.Rows(C1)("transqty").ToString) & ", 0, 0, 0,1,'" & transno.Text & " dari " & objTable.Rows(C1)("matcodefrom") & " ke " & objTable.Rows(C1)("matcodeto") & "','Material Transformation dari " & objTable.Rows(C1)("matcodefrom") & " ke " & objTable.Rows(C1)("matcodeto") & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'',0," & ToDouble(objTable.Rows(C1)("transvalueidr").ToString) & "," & ToDouble(objTable.Rows(C1)("transvalueidr").ToString) & "," & ToDouble(objTable.Rows(C1)("transqty").ToString) & ", 0)"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            conmtroid += 1
                            ' Update Crd Stock In
                            sSql = "UPDATE QL_crdstock SET qtyin = qtyin + " & ToDouble(objTable.Rows(C1)("transqty").ToString) & ", saldoakhir = saldoakhir + " & ToDouble(objTable.Rows(C1)("transqty").ToString) & ", qtyout_unitbesar = qtyout_unitbesar + " & ToDouble(objTable.Rows(C1)("transqty").ToString) & ", saldoakhir_unitbesar = saldoakhir_unitbesar + " & ToDouble(objTable.Rows(C1)("transqty").ToString) & ", lasttranstype='QL_trntransformitemdtl', lasttransdate='" & sDate & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND refoid=" & objTable.Rows(C1)("matoidto") & " AND mtrlocoid=" & objTable.Rows(C1)("transtowhoid") & " AND periodacctg='" & sPeriod & "'"
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() <= 0 Then
                                ' Insert Crd Stock Out
                                sSql = "INSERT INTO QL_crdstock(cmpcode,crdstockoid,periodacctg,refoid,refname,mtrlocoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir,lasttranstype,lasttransdate,createuser,createtime,upduser,updtime,closeuser,closingdate,refno, qtyin_unitbesar, qtyout_unitbesar, qtyadjin_unitbesar, qtyadjout_unitbesar, saldoawal_unitbesar, saldoakhir_unitbesar) VALUES ('" & DDLBusUnit.SelectedValue & "', " & crdmatoid & ", '" & sPeriod & "', " & objTable.Rows(C1)("matoidto") & ", 'FINISH GOOD', " & objTable.Rows(C1)("transtowhoid") & ", " & ToDouble(objTable.Rows(C1)("transqty").ToString) & ", 0, 0, 0, 0, " & ToDouble(objTable.Rows(C1)("transqty").ToString) & ", 'QL_trntransformitemdtl', '" & sDate & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "',CURRENT_TIMESTAMP, '', '1/1/1900', '', " & ToDouble(objTable.Rows(C1)("transqty").ToString) & ", 0, 0, 0, 0, " & ToDouble(objTable.Rows(C1)("transqty").ToString) & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                crdmatoid += 1
                            End If
                        End If
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(transdtloid.Text)) & " WHERE tablename='QL_TRNTRANSFORMITEMDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    If transmststatus.Text = "Post" Then
                        sSql = "UPDATE QL_mstoid SET lastoid=" & conmtroid - 1 & " WHERE tablename='QL_constock' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        sSql = "UPDATE QL_mstoid SET lastoid=" & crdmatoid - 1 & " WHERE tablename='QL_crdstock' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                End If
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        transmststatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    transmststatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 2)
                conn.Close()
                transmststatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & transmstoid.Text & ".<BR>"
            End If
            If transmststatus.Text = "Post" Then
                Session("SavedInfo") &= "Data have been posted with Transform No. = " & transno.Text & "."
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trnTransformItem.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnTransformItem.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If transmstoid.Text = "" Then
            showMessage("Please select Transform Item data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNTRANSFORMITEMMST", "transformitemmstoid", transmstoid.Text, "transformitemmststatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                transmststatus.Text = "In Process"
                Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE FROM QL_trntransformitemdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND transformitemmstoid=" & transmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trntransformitemmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND transformitemmstoid=" & transmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnTransformItem.aspx?awal=true")
    End Sub

    Protected Sub btnposting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnposting.Click
        transmststatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLFromWH()
    End Sub

    Protected Sub gvListMat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If matoid.Text <> gvListMat.SelectedDataKey.Item("matoidfrom").ToString Then
            btnClear_Click(Nothing, Nothing)
        End If
        matoid.Text = gvListMat.SelectedDataKey.Item("matoidfrom").ToString
        matcode.Text = gvListMat.SelectedDataKey.Item("matcodefrom").ToString
        matlongdesc.Text = gvListMat.SelectedDataKey.Item("matlongdescfrom").ToString
        stockqty.Text = ToMaskEdit(ToDouble(gvListMat.SelectedDataKey.Item("stockqty").ToString), 2)
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub gvListMat2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        matoid2.Text = gvListMat2.SelectedDataKey.Item("matoidto").ToString
        matcode2.Text = gvListMat2.SelectedDataKey.Item("matcodeto").ToString
        matlongdesc2.Text = gvListMat2.SelectedDataKey.Item("matlongdescto").ToString
        cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, False)
    End Sub

    Protected Sub lkbCloseListMat2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, False)
    End Sub

    Protected Sub btnFindListMat2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("TblMat2") IsNot Nothing Then
            Dim dv As DataView = Session("TblMat2").DefaultView
            dv.RowFilter = FilterDDLListMat2.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat2.Text) & "%'"
            If dv.Count > 0 Then
                Session("TblMatView2") = dv.ToTable
                gvListMat2.DataSource = Session("TblMatView2")
                gvListMat2.DataBind()
                dv.RowFilter = ""
                mpeListMat2.Show()
            Else
                dv.RowFilter = ""
                Session("WarningListMat2") = "Material data can't be found!"
                showMessage(Session("WarningListMat2"), 2)
            End If
        Else
            Session("WarningListMat2") = "Material data can't be found!"
            showMessage(Session("WarningListMat2"), 2)
        End If
    End Sub

    Protected Sub btnAllListMat2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("TblMat2") IsNot Nothing Then
            Dim dv As DataView = Session("TblMat2").DefaultView
            dv.RowFilter = ""
            If dv.Count > 0 Then
                Session("TblMatView2") = dv.ToTable
                gvListMat2.DataSource = Session("TblMatView2")
                gvListMat2.DataBind()
                dv.RowFilter = ""
                mpeListMat2.Show()
            Else
                dv.RowFilter = ""
                Session("WarningListMat2") = "Material data can't be found!"
                showMessage(Session("WarningListMat2"), 2)
            End If
        Else
            Session("WarningListMat2") = "Material data can't be found!"
            showMessage(Session("WarningListMat2"), 2)
        End If
    End Sub

    Protected Sub gvListMat2_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvListMat2.PageIndex = e.NewPageIndex
        gvListMat2.DataSource = Session("TblMatView2")
        gvListMat2.DataBind()
        mpeListMat2.Show()
    End Sub

    Protected Sub gvListMat2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 2)
        End If
    End Sub

    Protected Sub btnEraseListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        matoid.Text = ""
        matcode.Text = ""
        matlongdesc.Text = ""
        transqty.Text = ""
        stockqty.Text = ""
        transunitoid.SelectedIndex = -1
    End Sub

    Protected Sub btnEraseListMat2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        matoid2.Text = ""
        matcode2.Text = ""
        matlongdesc2.Text = ""
    End Sub

    Protected Sub btnSearchMat2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2) : Exit Sub
        End If
        If transtowhoid.SelectedValue = "" Then
            showMessage("Please select To WH first!", 2) : Exit Sub
        End If
        If matoid.Text = "" Then
            showMessage("Please select From Item first!", 2) : Exit Sub
        End If
        FilterDDLListMat2.SelectedIndex = -1 : FilterTextListMat2.Text = ""
        Session("TblMat2") = Nothing : Session("TblMatView2") = Nothing : gvListMat2.DataSource = Nothing : gvListMat2.DataBind()
        BindMaterialData2()
    End Sub
#End Region

End Class
