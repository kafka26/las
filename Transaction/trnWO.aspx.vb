Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_WorkOrder
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
    Const iRoundDigit = 4
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsDetailItemInputValid() As Boolean
        Dim sError As String = ""
        If soitemmstoid.Text = "" Then
            sError &= "- Please select SO NO. field!<BR>"
        End If
        If itemoid.Text = "" Then
            sError &= "- Please select FINISH GOOD field!<BR>"
            'Else
            '    If Not CheckDataExists("SELECT COUNT(*) FROM QL_mstbom WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND itemoid=" & itemoid.Text & "") Then
            '        sError &= "- Please create BOM from your selected FINISH GOOD!<BR>"
            '    Else
            '        If Not CheckDataExists("SELECT COUNT(*) FROM QL_mstdlc WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND bomoid=" & bomoid.Text & "") Then
            '            sError &= "- Please create DLC from your selected FINISH GOOD!<BR>"
            '        Else
            '            Dim iTotalBOMProcess As Integer = ToInteger(GetStrData("SELECT COUNT(*) FROM QL_mstbomdtl1 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND bomoid=" & bomoid.Text & ""))
            '            Dim iTotalDLCProcess As Integer = ToInteger(GetStrData("SELECT COUNT(*) FROM QL_mstdlcdtl dld INNER JOIN QL_mstdlc dlm ON dlm.cmpcode=dld.cmpcode AND dlm.dlcoid=dld.dlcoid WHERE dld.cmpcode='" & DDLBusUnit.SelectedValue & "' AND bomoid=" & bomoid.Text & ""))
            '            'If iTotalBOMProcess <> iTotalDLCProcess Then
            '            '    sError &= "- Total process of DLC must be equal with Total Process of BOM from your selected FINISH GOOD!<BR>"
            '            'End If
            '        End If
            '    End If
        End If
        If wodtl1qty.Text = "" Then
            sError &= "- Please fill QTY field!<BR>"
        Else
            If ToDouble(wodtl1qty.Text) <= 0 Then
                sError &= "- QTY field must be more than 0!<BR>"
            Else
                Dim sErrReply As String = ""
                If Not isLengthAccepted("wodtl1qty", "QL_trnwodtl1", ToDouble(wodtl1qty.Text), sErrReply) Then
                    sError &= "- QTY field must be less than MAX QTY (" & sErrReply & ") allowed stored in database!<BR>"
                Else
                    If Not IsQtyRounded(ToDouble(wodtl1qty.Text), ToDouble(itemlimitqty.Text)) Then
                        sError &= "- QTY field must be rounded by ROUNDING QTY!<BR>"
                    End If
                End If
            End If
        End If
       
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsDetailProcessInputValid() As Boolean
        Dim sError As String = ""
        If wodtl2qty.Text = "" Then
            'sError &= "- Please fill QTY field!<BR>"
        Else
            If ToDouble(wodtl2qty.Text) <= 0 Then
                'sError &= "- QTY field must be more than 0!<BR>"
            Else
                Dim sErrReply As String = ""
                If Not isLengthAccepted("wodtl2qty", "QL_trnwodtl2", ToDouble(wodtl2qty.Text), sErrReply) Then
                    sError &= "- QTY field must be less than MAX QTY (" & sErrReply & ") allowed stored in database!<BR>"
                    'Else
                    '    If Not IsQtyRounded(ToDouble(wodtl2qty.Text), ToDouble(processlimitqty.Text)) Then
                    '        sError &= "- QTY field must be rounded by ROUNDING QTY!<BR>"
                    '    End If
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsDetailMatInputValid() As Boolean
        Dim sError As String = ""
        If DDLProcess.SelectedValue = "" Then
            sError &= "- Please select PROCESS field!<BR>"
        End If
        If wodtl3refoid.Text = "" Then
            sError &= "- Please select MATERIAL field!<BR>"
        End If
        If wodtl3qty.Text = "" Then
            sError &= "- Please fill QUANTITY of MATERIAL field!<BR>"
        Else
            If ToDouble(wodtl3qty.Text) <= 0 Then
                sError &= "- QUANTITY of MATERIAL must be more than 0!<BR>"
            Else
                Dim sErrReply As String = ""
                If Not isLengthAccepted("bomdtl2refqty", "QL_mstbomdtl2", ToDouble(wodtl3qty.Text), sErrReply) Then
                    sError &= "- QUANTITY of MATERIAL must be less than MAX QUANTITY (" & sErrReply & ") allowed stored in database!<BR>"
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
       
        If makloonoid.Text = "" Then
            sError &= "- Please Select Makloon Field!<BR>"
        End If
        If wodate.Text = "" Then
            sError &= "- Please fill DATE field!<BR>"
        Else
            If Not IsValidDate(wodate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If dlcamt.Text = "" Then
            sError &= "- Please fill DLC field!<BR>"
        Else
            If ToDouble(dlcamt.Text) <= 0 Then
                sError &= "- DLC Must more than 0!<BR>"
            End If
        End If
        If startdate.Text = "" Then
            sError &= "- Please fill START DATE field!<BR>"
        Else
            If Not IsValidDate(startdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- START DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If finishdate.Text = "" Then
            sError &= "- Please fill FINISH DATE field!<BR>"
        Else
            If Not IsValidDate(finishdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- FINISH DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If cbBordir_use_ap.Checked Then
            If ohdamt.Text = "" Then
                sError &= "- Please fill BORDIR/SABLON field!<BR>"
            Else
                If ToDouble(ohdamt.Text) <= 0 Then
                    sError &= "- BORDIR/SABLON field must more than 0!<BR>"
                End If
            End If
        End If
        If cbKancing_use_ap.Checked Then
            If kancingamt.Text = "" Then
                sError &= "- Please fill KANCING field!<BR>"
            Else
                If ToDouble(kancingamt.Text) <= 0 Then
                    sError &= "- KANCING field must more than 0!<BR>"
                End If
            End If
        End If
        If cbKain_use_ap.Checked Then
            If kainamt.Text = "" Then
                sError &= "- Please fill KAIN field!<BR>"
            Else
                If ToDouble(kainamt.Text) <= 0 Then
                    sError &= "- KAIN field must more than 0!<BR>"
                End If
            End If
        End If
        If cbLabel_use_ap.Checked Then
            If labelamt.Text = "" Then
                sError &= "- Please fill LABEL field!<BR>"
            Else
                If ToDouble(labelamt.Text) <= 0 Then
                    sError &= "- LABEL field must more than 0!<BR>"
                End If
            End If
        End If
        If cbOther_use_ap.Checked Then
            If othervalue.Text = "" Then
                sError &= "- Please fill OTHER field!<BR>"
            Else
                If ToDouble(othervalue.Text) <= 0 Then
                    sError &= "- OTHER field must more than 0!<BR>"
                End If
            End If
        End If
        If Session("TblDtlMatLev5") Is Nothing Then
            sError &= "- Please fill Detail MATERIAL field!<BR>"
        Else
            Dim objTbl As DataTable = Session("TblDtlMatLev5")
            If objTbl.Rows.Count <= 0 Then
                sError &= "- Please fill DETAIL MATERIAL!<BR>"
            End If
        End If
        Dim sErrDtl As String = ""
        If Session("TblDtlFG") Is Nothing Then
            sErrDtl &= "- Please fill DETAIL FINISH GOOD!<BR>"
        Else
            Dim objTbl As DataTable = Session("TblDtlFG")
            If objTbl.Rows.Count <= 0 Then
                sErrDtl &= "- Please fill DETAIL FINISH GOOD!<BR>"
            Else
                If objTbl.Rows.Count > 1 Then
                    sErrDtl &= "- Can't Set Multiple DETAIL FINISH GOOD!<BR>"
                End If
            End If
        End If
        If Session("TblDtlProcess") Is Nothing Then
            sErrDtl &= "- Please fill DETAIL PROCESS!<BR>"
        Else
            Dim objTbl As DataTable = Session("TblDtlProcess")
            If objTbl.Rows.Count <= 0 Then
                sErrDtl &= "- Please fill DETAIL PROCESS!<BR>"
            End If
        End If
        If sErrDtl <> "" Then
            sError &= sErrDtl
        Else
            Dim objTblSO As DataTable = Session("TblDtlFG")
            Dim sGroupOid As String = objTblSO.Rows(0)("groupoid").ToString
            Dim objTblProc As DataTable = Session("TblDtlProcess")
            For C1 As Integer = 0 To objTblProc.Rows.Count - 1
                If Not CheckDataExists("SELECT COUNT(*) FROM QL_mstdept de left JOIN QL_mstdeptgroupdtl dgd ON dgd.cmpcode=de.cmpcode AND dgd.deptoid=de.deptoid WHERE /*dgd.groupoid=" & sGroupOid & " AND*/ de.deptoid=" & objTblProc.Rows(C1)("deptoid").ToString & "") Then
                    sError &= "- Every selected DEPARTMENT in DETAIL PROCESS must be in one Division!<BR>" : Exit For
                End If
            Next
        End If
        If ToInteger(suppoid_bordir.Text) <> 0 Then
            If ToDouble(bordiramt.Text) <= 0 Then
                sError &= "- BORDIR AMT. field must more than 0!<BR>"
            End If
        End If
        If ToInteger(ddlNoTransfer.SelectedValue) = 0 Then
            sError &= "- Pilih nomor transfer terlebih dahulu 0!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            womststatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function GetTextValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function GetDDLValue(ByVal cc As System.Web.UI.ControlCollection, Optional ByVal sText As String = "") As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                If sText = "" Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
                Else
                    sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedItem.Text
                End If
            End If
        Next
        Return sReturn
    End Function

    Public Function GetParameterID() As String
        Return Eval("matrefoid") & "," & Eval("matrefunitoid")
    End Function

    Private Function GenerateSOFGNo() As String
        Dim sNo As String = "BS-" & Format(GetServerTime(), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(sono, " & DefaultFormatCounter & ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnsomst WHERE cmpcode='" & CompnyCode & "' AND sono LIKE '" & sNo & "%'"
        sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
        Return sNo
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Session("soitemdtloid") = "0"
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListItem.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListItem.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "soitemdtloid=" & cbOid
                                Session("soitemdtloid") = cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("soitemqty") = ToDouble(GetTextBoxValue(row.Cells(3).Controls))
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function GetStockValue(ByVal sOid As String) As Double
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        Try
            sSql = "SELECT SUM(ISNULL(stockqty, 0) * ISNULL(stockvalueidr, 0)) / SUM(ISNULL(stockqty, 0)) FROM QL_stockvalue WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND periodacctg IN ('" & sPeriod & "', '" & GetLastPeriod(sPeriod) & "') AND refoid=" & sOid & " AND closeflag=''"
            GetStockValue = ToDouble(GetStrData(sSql))
        Catch ex As Exception
            GetStockValue = 0
        End Try
    End Function

#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckWOStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trnwomst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND womststatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnWO.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lkbWOInProcess.Visible = True
            lkbWOInProcess.Text = "You have " & GetStrData(sSql) & " In Process Job Costing MO data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitAllDDL()
        'Fill DDL Division
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(FilterDDLBusUnit, sSql)
        If FillDDL(DDLBusUnit, sSql) Then
            InitDDLDept()
        End If
        ' Fill DDL Unit Finish Good
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='UNIT' AND activeflag='ACTIVE'"
        FillDDL(wodtl1unitoid, sSql)
        ' Fill DDL Unit Process
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='UNIT' AND activeflag='ACTIVE'"
        FillDDL(wodtl2unitoid, sSql)
        ' Fill DDL Unit Material
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='UNIT' AND activeflag='ACTIVE'"
        FillDDL(wodtl3unitoid, sSql)
        FillDDL(wodtl4unitoid, sSql)
    End Sub

    Private Sub InitDDLDept()
        'Fill DDL Department
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE activeflag='ACTIVE' AND cmpcode='" & DDLBusUnit.SelectedValue & "'"
        FillDDL(deptoid, sSql)
        FillDDL(todeptoid, sSql)
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT wom.womstoid, CONVERT(VARCHAR(10), wom.wodate, 101) AS wodate, wom.wono, wom.womstres3 soitemno, wom.womstres1 itemcode,wom.womstres2 itemlongdesc, 0 wodtl1qty, (CASE WHEN wom.womststatus='In Process' THEN '' ELSE wom.upduser END) AS postuser, 'False' AS checkvalue, womststatus, wotempstatus FROM QL_trnwomst wom WHERE "
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= "wom.cmpcode='" & Session("CompnyCode") & "'"
        Else
            sSql &= "wom.cmpcode LIKE '%'"
        End If
        sSql &= sSqlPlus & " ORDER BY CONVERT(DATETIME, wodate) DESC, wom.womstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trnwomst")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "womstoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub BindSOData()
        sSql = "SELECT * FROM (SELECT som.somstoid AS soitemmstoid, som.sono AS soitemno, CONVERT(VARCHAR(10), som.sodate, 101) AS soitemdate, som.somstres1 AS soitemtype, c.custname AS custname, som.groupoid,'' groupdesc, somstnote FROM QL_trnsomst som LEFT JOIN QL_mstcust c ON c.custoid=som.custoid WHERE som.sotype='Buffer' AND som.cmpcode='" & DDLBusUnit.SelectedValue & "' AND somststatus='Post' AND " & FilterDDLListSO.SelectedValue & " LIKE '%" & Tchar(FilterTextListSO.Text) & "%' AND ISNULL(som.somstres3, '')='' UNION ALL "
        sSql &= "SELECT som.somstoid AS soitemmstoid, som.sono AS soitemno, CONVERT(VARCHAR(10), som.sodate, 101) AS soitemdate, som.sotype AS soitemtype, c.custname AS custname, som.groupoid,'' groupdesc, somstnote FROM QL_trnsomst som INNER JOIN QL_mstcust c ON c.custoid=som.custoid WHERE som.cmpcode='" & DDLBusUnit.SelectedValue & "' AND somststatus='Approved' AND " & FilterDDLListSO.SelectedValue & " LIKE '%" & Tchar(FilterTextListSO.Text) & "%' AND ISNULL(som.somstres3, '')='') AS dt ORDER BY soitemmstoid DESC"
        FillGV(gvListSO, sSql, "QL_trnsomst")
    End Sub

    Private Sub BindItemData()
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        Dim sAdd As String = ""
        If Session("oid") IsNot Nothing And Session("oid") <> "" Then
            sAdd = " AND wod1.womstoid<>" & Session("oid")
        End If
        sSql = "SELECT 'False' AS CheckValue,sod.sodtloid AS soitemdtloid, sod.sodtlnote,sod.somstoid AS soitemmstoid,som.sono AS soitemno,som.groupoid, bom.itemunitoid AS unitbomoid, gx.gendesc AS unitbom, sod.itemoid, i.itemcode, i.roundqty AS itemlimitqty, i.itemlongdescription AS itemlongdesc, i.unit3unit1conversion, sod.sodtlunitoid soitemunitoid, g.gendesc AS soitemunit, isnull(bom.bomoid,0) bomoid, (sod.sodtlqty - ISNULL((SELECT SUM(wod1.wodtl1qty_soqty) FROM QL_trnwodtl1 wod1 INNER JOIN QL_trnwomst wom ON wod1.cmpcode=wom.cmpcode AND wod1.womstoid=wom.womstoid WHERE wod1.cmpcode=sod.cmpcode AND wod1.soitemdtloid=sod.sodtloid AND wom.womststatus<>'Cancel'" & sAdd & "), 0.0)) AS soitemqty,(sod.sodtlqty - ISNULL((SELECT SUM(wod1.wodtl1qty_soqty) FROM QL_trnwodtl1 wod1 INNER JOIN QL_trnwomst wom ON wod1.cmpcode=wom.cmpcode AND wod1.womstoid=wom.womstoid WHERE wod1.cmpcode=sod.cmpcode AND wod1.soitemdtloid=sod.sodtloid AND wom.womststatus<>'Cancel'" & sAdd & "), 0.0)) AS soitemqty_real,(sod.sodtlqty - ISNULL((SELECT SUM(wod1.wodtl1qty_soqty) FROM QL_trnwodtl1 wod1 INNER JOIN QL_trnwomst wom ON wod1.cmpcode=wom.cmpcode AND wod1.womstoid=wom.womstoid WHERE wod1.cmpcode=sod.cmpcode AND wod1.soitemdtloid=sod.sodtloid AND wom.womststatus<>'Cancel'" & sAdd & "), 0.0)) AS soitemqty_real, '' AS itemres2, precosttotalmatamt, precostdlcamt, precostoverheadamt, precostmarginamt, precostsalesprice, curroid_overhead, curroid_salesprice, precostoverheadamtidr, precostsalespriceidr, pr.upduser AS lastupduser_precost, pr.updtime AS lastupdtime_precost, bom.upduser AS lastupduser_bom, bom.updtime AS lastupdtime_bom, dlm.upduser AS lastupduser_dlc, dlm.updtime AS lastupdtime_dlc,case isnull(bomdesc,'No Formula') when 'No Formula' then 'No BOM' else 'With BOM' end formula, (select dbo.getstockqty2(i.itemoid)) AS stockqty1, (select dbo.getstockqty2(i.itemoid)) AS stockqty3, (SELECT gx.gendesc FROM QL_mstgen gx WHERE gx.genoid=i.itemunit1) AS stockunit1, (SELECT gx.gendesc FROM QL_mstgen gx WHERE gx.genoid=i.itemunit3) AS stockunit3 FROM QL_trnsodtl sod INNER JOIN QL_trnsomst som on som.somstoid=sod.somstoid INNER JOIN QL_mstitem i ON i.itemoid=sod.itemoid INNER JOIN QL_mstgen g ON g.genoid=sod.sodtlunitoid LEFT JOIN QL_mstbom bom ON bom.cmpcode=sod.cmpcode AND bom.itemoid=sod.itemoid LEFT JOIN QL_mstgen gx ON gx.genoid=bom.itemunitoid LEFT JOIN QL_mstprecost pr ON pr.cmpcode=bom.cmpcode AND pr.itemoid=bom.itemoid LEFT JOIN QL_mstdlc dlm ON dlm.cmpcode=bom.cmpcode AND dlm.bomoid=bom.bomoid WHERE sod.cmpcode='" & DDLBusUnit.SelectedValue & "' AND bom.activeflag='ACTIVE' AND sod.somstoid=" & soitemmstoid.Text & " AND " & FilterDDLListItem.SelectedValue & " LIKE '%" & Tchar(FilterTextListItem.Text) & "%' AND ISNULL(sod.sodtlres3, '')='' ORDER BY i.itemcode"
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstmat")
        Dim dv As DataView = dtTbl.DefaultView
        dv.RowFilter = "soitemqty>0"
        Session("TblMat") = dv.ToTable
        gvListItem.DataSource = Session("TblMat")
        gvListItem.DataBind()
    End Sub

    Private Sub BindItemData2()
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sSql = "SELECT 'False' AS CheckValue,i.itemoid AS soitemdtloid, '' AS sodtlnote, 0 AS soitemmstoid,'' AS soitemno,0 AS groupoid, i.itemunit1 AS unitbomoid, g.gendesc AS unitbom, i.itemoid, i.itemcode, i.roundqty AS itemlimitqty, i.itemlongdescription AS itemlongdesc, i.unit3unit1conversion, i.itemunit1 AS soitemunitoid, g.gendesc AS soitemunit, isnull(bom.bomoid,0) bomoid, 0.0 AS soitemqty, 0.0 AS soitemqty_real, 0.0 AS soitemqty_real, '' AS itemres2, 0.0 precosttotalmatamt, 0.0 precostdlcamt, 0.0 precostoverheadamt, 0.0 precostmarginamt, 0.0 precostsalesprice, 1 curroid_overhead, 1 curroid_salesprice, 0.0 precostoverheadamtidr, 0.0 precostsalespriceidr, '' AS lastupduser_precost, GETDATE() AS lastupdtime_precost, bom.upduser AS lastupduser_bom, bom.updtime AS lastupdtime_bom, '' AS lastupduser_dlc, GETDATE() AS lastupdtime_dlc,case isnull(bomdesc,'No Formula') when 'No Formula' then 'No BOM' else 'With BOM' end formula, /*(select dbo.getstockqty2(i.itemoid))*/0.0 AS stockqty1, (SELECT gx.gendesc FROM QL_mstgen gx WHERE gx.genoid=i.itemunit1) AS stockunit1, (SELECT gx.gendesc FROM QL_mstgen gx WHERE gx.genoid=i.itemunit3) AS stockunit3, itemPriceList FROM QL_mstitem i INNER JOIN QL_mstgen g ON g.genoid=i.itemUnit1 LEFT JOIN QL_mstbom bom ON bom.itemoid=i.itemoid WHERE i.cmpcode='" & DDLBusUnit.SelectedValue & "' AND " & FilterDDLListItem.SelectedValue & " LIKE '%" & Tchar(FilterTextListItem.Text) & "%' ORDER BY i.itemcode"
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstmat")
        Session("TblMat") = dtTbl
        gvListItem.DataSource = Session("TblMat")
        gvListItem.DataBind()
    End Sub

    Private Sub CreateTblDetailItem()
        Dim dtlTable As DataTable = New DataTable("QL_trnwodtl1")
        dtlTable.Columns.Add("wodtl1seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("soitemmstoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("soitemno", Type.GetType("System.String"))
        dtlTable.Columns.Add("groupoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("groupdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("soitemdtloid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemshortdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemlimitqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("soitemqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl1qty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl1qty_soqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl1unitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl1unit", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl1note", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl1area", Type.GetType("System.Double"))

        dtlTable.Columns.Add("wodtl1totalmatamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl1dlcamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl1ohdamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("curroid_ohd", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl1ohdamtidr", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl1marginamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl1precostamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("curroid_precost", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl1precostamtidr", Type.GetType("System.Double"))

        dtlTable.Columns.Add("lastupduser_precost", Type.GetType("System.String"))
        dtlTable.Columns.Add("lastupdtime_precost", Type.GetType("System.DateTime"))
        dtlTable.Columns.Add("lastupduser_bom", Type.GetType("System.String"))
        dtlTable.Columns.Add("lastupdtime_bom", Type.GetType("System.DateTime"))
        dtlTable.Columns.Add("lastupduser_dlc", Type.GetType("System.String"))
        dtlTable.Columns.Add("lastupdtime_dlc", Type.GetType("System.DateTime"))

        Session("TblDtlFG") = dtlTable
    End Sub

    Private Sub GenerateDetailFromBOM()
        Session("TblDtlProcess") = Nothing
        Session("TblDtlMatLev5") = Nothing
        Session("TblDtlMatLev3") = Nothing
        If Session("TblDtlProcess") Is Nothing Then
            CreateTblDetailProcess()
        End If
        If Session("TblDtlMatLev5") Is Nothing Then
            CreateTblDetailMatLev5()
        End If
        If Session("TblDtlMatLev3") Is Nothing Then
            CreateTblDetailMatLev3()
        End If
        Dim dtProc As DataTable = Session("TblDtlProcess")
        Dim dtMatLev5 As DataTable = Session("TblDtlMatLev5")
        Dim dtMatLev3 As DataTable = Session("TblDtlMatLev3")


        'Get untuk bomoidnya
        Dim tempBomoid As String = ""
        Dim objTable As DataTable
        objTable = Session("TblDtlFG")
        If Session("TblDtlFG") IsNot Nothing Then
            For x As Int64 = 0 To objTable.Rows.Count - 1
                tempBomoid = tempBomoid & "," & objTable.Rows(x).Item("bomoid").ToString
            Next
        End If
        If tempBomoid.Trim <> "" Then
            tempBomoid = tempBomoid.Remove(0, 1)
        End If
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        ' Get Process Data From BOM
        sSql = "SELECT bom.itemqty AS bomqty, bod1.bomoid,bod1.bomdtl1oid, bod1.bomdtl1seq, bod1.bomdtl1deptoid, de.deptname, bod1.bomdtl1todeptoid, (CASE bod1.bomdtl1reftype WHEN 'WIP' THEN de2.deptname ELSE 'END' END) AS todeptname, bod1.bomdtl1reftype, bod1.bomdtl1refunitoid, g.gendesc AS bomdtl1refunit, bod1.bomdtl1res1, 0 AS dlcdtlamount, 0 AS dlcohdamount, m.itemcode AS bomfgcode, m.itemshortdescription AS bomfgdesc FROM QL_mstbomdtl1 bod1 INNER JOIN QL_mstdept de ON de.cmpcode=bod1.cmpcode AND de.deptoid=bod1.bomdtl1deptoid INNER JOIN QL_mstdept de2 ON de2.cmpcode=bod1.cmpcode AND de2.deptoid=bod1.bomdtl1todeptoid INNER JOIN QL_mstgen g ON g.genoid=bod1.bomdtl1refunitoid LEFT JOIN QL_mstbom bom ON bom.cmpcode=bod1.cmpcode AND bom.bomoid=bod1.bomoid INNER JOIN QL_mstitem m ON m.itemoid=bom.itemoid WHERE bod1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND bod1.bomoid IN (" & tempBomoid & ") ORDER BY bod1.bomdtl1seq"
        Dim dtProcBOM As DataTable = cKon.ambiltabel(sSql, "QL_mstbomdtl1")

        ' Get Material Level 5 Data From BOM
        sSql = "SELECT bomqty, bomoid, bomdtl2seq, bomdtl1oid, bomdtl2oid, bomdtl2reftype, bomdtl2refoid, bomdtl2refshortdesc, bomdtl2refcode, bomdtl2refqty, bomdtl2refunitoid, bomdtl2refunit, bomdtl2refvalue, wodtl3stockqty, cat1, cat2, cat3, wodtl3fgcode FROM ("
        sSql &= "SELECT bom.itemqty AS bomqty, bod2.bomoid, bod2.bomdtl2seq, bod2.bomdtl1oid, bod2.bomdtl2oid, bod2.bomdtl2reftype, bod2.bomdtl2refoid, m.itemLongDescription AS bomdtl2refshortdesc, m.itemCode AS bomdtl2refcode, bod2.bomdtl2refqty, bod2.bomdtl2refunitoid, g.gendesc AS bomdtl2refunit, ISNULL((SELECT TOP 1 lastpurchaseprice * ratetoidr FROM QL_mstitemprice mp WHERE mp.itemoid=bomdtl2refoid ORDER BY mp.updtime DESC), 0.0) AS bomdtl2refvalue, (select dbo.getstockqty2(bod2.bomdtl2refoid)) AS wodtl3stockqty, m.itemcat1 AS cat1, m.itemcat2 AS cat2, m.itemcat3 AS cat3, m.itemcode AS wodtl3fgcode FROM QL_mstbomdtl2 bod2 INNER JOIN QL_mstitem m ON m.itemoid=bod2.bomdtl2refoid INNER JOIN QL_mstgen g ON g.genoid=bod2.bomdtl2refunitoid INNER JOIN QL_mstbom bom ON bom.cmpcode=bod2.cmpcode AND bom.bomoid=bod2.bomoid WHERE bod2.cmpcode='" & DDLBusUnit.SelectedValue & "' AND bod2.bomoid IN (" & tempBomoid & ")"
        sSql &= ") AS tbltemp ORDER BY bomdtl1oid, bomdtl2seq"
        Dim dtMatLev5BOM As DataTable = cKon.ambiltabel(sSql, "QL_mstbomdtl2")
        Dim dvMatLev5BOM As DataView = dtMatLev5BOM.DefaultView

        ' Get Material Level 2 Data From BOM
        sSql = "SELECT bod3.bomoid,bod3.bomdtl1oid, bod3.bomdtl3oid, bod3.bomdtl3reftype, bod3.bomdtl3refoid, (RTRIM((CASE WHEN (LTRIM(c1.cat1shortdesc)='None' OR LTRIM(c1.cat1shortdesc)='') THEN '' ELSE c1.cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(c2.cat2shortdesc)='None' OR LTRIM(c2.cat2shortdesc)='') THEN '' ELSE c2.cat2shortdesc + ' ' END))) AS bomdtl3refshortdesc, (c1.cat1code + '.' + c2.cat2code) AS bomdtl3refcode, bod3.bomdtl3refqty, bod3.bomdtl3refunitoid, g.gendesc AS bomdtl3refunit, ISNULL((SELECT TOP 1 lastpurchaseprice * ratetoidr FROM QL_mstcat3price mp INNER JOIN QL_mstcat3 cx3 ON cx3.cat3oid=mp.cat3oid INNER JOIN QL_mstcat2 cx2 ON cx2.cat2oid=cx3.cat2oid WHERE mp.cmpcode=bod3.cmpcode AND cx2.cat2oid=bomdtl3refoid ORDER BY mp.updtime DESC), 0.0) AS bomdtl3refvalue FROM QL_mstbomdtl3 bod3 INNER JOIN QL_mstcat2 c2 ON c2.cat2oid=bod3.bomdtl3refoid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c1.cat1oid=c2.cat1oid INNER JOIN QL_mstgen g ON g.genoid=bod3.bomdtl3refunitoid WHERE bod3.cmpcode='" & DDLBusUnit.SelectedValue & "' AND bod3.bomoid in (" & tempBomoid & ") ORDER BY bod3.bomdtl3oid"
        Dim dtMatLev3BOM As DataTable = cKon.ambiltabel(sSql, "QL_mstbomdtl3")
        Dim dvMatLev3BOM As DataView = dtMatLev3BOM.DefaultView

        Dim iCountProc As Integer = dtProc.Rows.Count + 1
        Dim iCountMatLev5 As Integer = dtMatLev5.Rows.Count + 1
        Dim iCountMatLev3 As Integer = dtMatLev3.Rows.Count + 1
        For C1 As Integer = 0 To dtProcBOM.Rows.Count - 1
            ' Added Process Data
            Dim drProc As DataRow = dtProc.NewRow
            drProc("wodtl2seq") = iCountProc + C1
            drProc("bomdtl1oid") = dtProcBOM.Rows(C1)("bomdtl1oid")
            drProc("wodtl2procseq") = ToInteger(dtProcBOM.Rows(C1)("bomdtl1res1").ToString)
            drProc("wodtl2type") = dtProcBOM.Rows(C1)("bomdtl1reftype").ToString
            drProc("deptoid") = dtProcBOM.Rows(C1)("bomdtl1deptoid")
            drProc("deptname") = dtProcBOM.Rows(C1)("deptname").ToString
            drProc("todeptoid") = dtProcBOM.Rows(C1)("bomdtl1todeptoid")
            drProc("todeptname") = dtProcBOM.Rows(C1)("todeptname").ToString
            For x As Int64 = 0 To objTable.Rows.Count - 1
                If dtProcBOM.Rows(C1)("bomoid").ToString = objTable.Rows(x).Item("bomoid").ToString Then
                    drProc("wodtl1seq") = objTable.Rows(x).Item("wodtl1seq").ToString
                    drProc("wodtl2fgcode") = objTable.Rows(x).Item("itemcode").ToString
                    drProc("wodtl2qty") = ToMaskEdit(ToDouble(objTable.Compute("SUM(wodtl1qty)", "bomoid=" & dtProcBOM.Rows(C1)("bomoid").ToString)), iRoundDigit)
                    drProc("processlimitqty") = ToDouble(objTable.Rows(x).Item("itemlimitqty").ToString)
                End If
            Next
            drProc("wodtl2unitoid") = dtProcBOM.Rows(C1)("bomdtl1refunitoid")
            drProc("wodtl2unit") = dtProcBOM.Rows(C1)("bomdtl1refunit").ToString
            drProc("wodtl2note") = ""
            Dim str As String = dtProcBOM.Rows(C1)("deptname").ToString & " - " & dtProcBOM.Rows(C1)("todeptname").ToString
            drProc("wodtl2desc") = str
            drProc("wodtl2dlcvalue") = ToDouble(dtProcBOM.Rows(C1)("dlcdtlamount").ToString)
            drProc("wodtl2fohvalue") = ToDouble(dtProcBOM.Rows(C1)("dlcohdamount").ToString)
            dtProc.Rows.Add(drProc)
            DDLProcess.Items.Add(str)
            DDLProcess.Items.Item(DDLProcess.Items.Count - 1).Value = iCountProc + C1
            DDLProcess2.Items.Add(str)
            DDLProcess2.Items.Item(DDLProcess2.Items.Count - 1).Value = iCountProc + C1

            ' Added Material Level 5 Data
            dvMatLev5BOM.RowFilter = "bomdtl1oid=" & dtProcBOM.Rows(C1)("bomdtl1oid")
            For C2 As Integer = 0 To dvMatLev5BOM.Count - 1
                Dim drMat As DataRow = dtMatLev5.NewRow
                drMat("wodtl3seq") = iCountMatLev5 + C2
                drMat("wodtl2seq") = iCountProc + C1
                drMat("wodtl2desc") = str
                drMat("bomdtl2oid") = dvMatLev5BOM(C2)("bomdtl2oid")
                drMat("wodtl3reftype") = dvMatLev5BOM(C2)("bomdtl2reftype").ToString
                drMat("wodtl3refoid") = dvMatLev5BOM(C2)("bomdtl2refoid")
                drMat("wodtl3refshortdesc") = dvMatLev5BOM(C2)("bomdtl2refshortdesc").ToString
                drMat("wodtl3refcode") = dvMatLev5BOM(C2)("bomdtl2refcode").ToString
                For x As Int64 = 0 To objTable.Rows.Count - 1
                    If dtProcBOM.Rows(C1)("bomoid").ToString = objTable.Rows(x).Item("bomoid").ToString Then
                        drMat("wodtl1seq") = objTable.Rows(x).Item("wodtl1seq").ToString
                        drMat("wodtl3fgcode") = objTable.Rows(x).Item("itemcode").ToString
                        drMat("wodtl3qty") = Math.Ceiling(ToDouble(dvMatLev5BOM(C2)("bomdtl2refqty").ToString) * ToDouble(objTable.Rows(x).Item("wodtl1qty").ToString))
                    End If
                Next
                Dim str2 As String = dtProcBOM.Rows(C1)("bomfgcode").ToString & " - " & dtProcBOM.Rows(C1)("bomfgdesc").ToString
                drMat("wodtl3unitoid") = dvMatLev5BOM(C2)("bomdtl2refunitoid")
                drMat("wodtl3unit") = dvMatLev5BOM(C2)("bomdtl2refunit").ToString
                drMat("wodtl3note") = ""
                drMat("wodtl3qtyref") = ToDouble(dvMatLev5BOM(C2)("bomdtl2refqty").ToString)
                drMat("wodtl3value") = ToDouble(dvMatLev5BOM(C2)("bomdtl2refvalue").ToString)
                drMat("wodtl3stockqty") = ToDouble(dvMatLev5BOM(C2)("wodtl3stockqty").ToString)
                drMat("cat1") = ToDouble(dvMatLev5BOM(C2)("cat1").ToString)
                drMat("cat2") = ToDouble(dvMatLev5BOM(C2)("cat2").ToString)
                drMat("cat3") = ToDouble(dvMatLev5BOM(C2)("cat3").ToString)
                dtMatLev5.Rows.Add(drMat)
                For x As Int64 = 0 To objTable.Rows.Count - 1
                    'If dtProcBOM.Rows(C1)("bomfgcode").ToString = objTable.Rows(x).Item("itemcode").ToString Then
                   
                    'End If
                Next
                DDLbom.Items.Add(str2)
                DDLbom.Items.Item(DDLbom.Items.Count - 1).Value = iCountProc + C1
            Next
            iCountMatLev5 += dvMatLev5BOM.Count
            dvMatLev5BOM.RowFilter = ""
           
            ' Added Material Level 2 Data
            dvMatLev3BOM.RowFilter = "bomdtl1oid=" & dtProcBOM.Rows(C1)("bomdtl1oid")
            For C2 As Integer = 0 To dvMatLev3BOM.Count - 1
                Dim drMat As DataRow = dtMatLev3.NewRow
                drMat("wodtl4seq") = iCountMatLev3 + C2
                drMat("wodtl2seq") = iCountProc + C1
                drMat("wodtl2desc") = str
                drMat("bomdtl3oid") = dvMatLev3BOM(C2)("bomdtl3oid")
                drMat("wodtl4reftype") = dvMatLev3BOM(C2)("bomdtl3reftype").ToString
                drMat("wodtl4refoid") = dvMatLev3BOM(C2)("bomdtl3refoid")
                drMat("wodtl4refshortdesc") = dvMatLev3BOM(C2)("bomdtl3refshortdesc").ToString
                drMat("wodtl4refcode") = dvMatLev3BOM(C2)("bomdtl3refcode").ToString
                For x As Int64 = 0 To objTable.Rows.Count - 1
                    If dtProcBOM.Rows(C1)("bomoid").ToString = objTable.Rows(x).Item("bomoid").ToString Then
                        drMat("wodtl1seq") = objTable.Rows(x).Item("wodtl1seq").ToString
                        drMat("wodtl4qty") = ToDouble(dvMatLev3BOM(C2)("bomdtl3refqty").ToString) * ToDouble(objTable.Rows(x).Item("wodtl1qty").ToString)
                    End If
                Next
                drMat("wodtl4unitoid") = dvMatLev3BOM(C2)("bomdtl3refunitoid")
                drMat("wodtl4unit") = dvMatLev3BOM(C2)("bomdtl3refunit").ToString
                drMat("wodtl4note") = ""
                drMat("wodtl4qtyref") = ToDouble(dvMatLev3BOM(C2)("bomdtl3refqty").ToString)
                drMat("wodtl4value") = ToDouble(dvMatLev3BOM(C2)("bomdtl3refvalue").ToString)
                dtMatLev3.Rows.Add(drMat)
            Next
            iCountMatLev3 += dvMatLev3BOM.Count
            dvMatLev3BOM.RowFilter = ""
        Next
        Session("TblDtlProcess") = dtProc
        GVDtl2.DataSource = Session("TblDtlProcess")
        GVDtl2.DataBind()
        ClearDetailProcess()
        Session("TblDtlMatLev5") = dtMatLev5
        GVDtl3.DataSource = Session("TblDtlMatLev5")
        GVDtl3.DataBind()
        ClearDetailMaterialLevel5()
        Session("TblDtlMatLev3") = dtMatLev3
        GVDtl4.DataSource = Session("TblDtlMatLev3")
        GVDtl4.DataBind()
        ClearDetailMaterialLevel3()
    End Sub

    Private Sub CreateTblDetailProcess()
        Dim dtlTable As DataTable = New DataTable("QL_trnwodtl2")
        dtlTable.Columns.Add("wodtl2oid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl2seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl1seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("bomdtl1oid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl2procseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl2type", Type.GetType("System.String"))
        dtlTable.Columns.Add("deptoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("deptname", Type.GetType("System.String"))
        dtlTable.Columns.Add("todeptoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("todeptname", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl2qty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl2unitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl2unit", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl2note", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl2desc", Type.GetType("System.String"))
        dtlTable.Columns.Add("processlimitqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl2dlcvalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl2fohvalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl2fgcode", Type.GetType("System.String"))
        Session("TblDtlProcess") = dtlTable
    End Sub

    Private Sub CreateTblDetailMatLev5()
        Dim dtlTable As DataTable = New DataTable("QL_trnwodtl3")
        dtlTable.Columns.Add("wodtl3seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl3oid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl1seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl2seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl2desc", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl2oid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl3reftype", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl3refoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl3refshortdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl3refcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl3qty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl3stockqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl3unitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl3unit", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl3note", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl3qtyref", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl3value", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl3amt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("cat1", Type.GetType("System.String"))
        dtlTable.Columns.Add("cat2", Type.GetType("System.String"))
        dtlTable.Columns.Add("cat3", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl3fgcode", Type.GetType("System.String"))
        Session("TblDtlMatLev5") = dtlTable
    End Sub

    Private Sub CreateTblDetailMatLev3()
        Dim dtlTable As DataTable = New DataTable("QL_trnwodtl4")
        dtlTable.Columns.Add("wodtl4seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl1seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl2seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl2desc", Type.GetType("System.String"))
        dtlTable.Columns.Add("bomdtl3oid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl4reftype", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl4refoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl4refshortdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl4refcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl4qty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl4unitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl4unit", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl4note", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl4qtyref", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl4value", Type.GetType("System.Double"))
        Session("TblDtlMatLev3") = dtlTable
    End Sub

    Private Sub ClearDetailProcess()
        wodtl2seq.Text = "1"
        If Session("TblDtlProcess") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("TblDtlProcess")
            wodtl2seq.Text = objTable.Rows.Count + 1
        End If
        i_u4.Text = "New Detail"
        wodtl2procseq.Text = "1"
        wodtl2type.SelectedIndex = -1
        deptoid.SelectedIndex = -1
        todeptoid.SelectedIndex = -1
        If wodtl2type.SelectedValue = "FG" Then
            lbltodept.Visible = False : septtodept.Visible = False : todeptoid.Visible = False
        Else
            lbltodept.Visible = True : septtodept.Visible = True : todeptoid.Visible = True
        End If
        'wodtl2qty.Text = ""
        wodtl2unitoid.SelectedIndex = -1
        wodtl2note.Text = ""
        wodtl2desc.Text = ""
        GVDtl2.SelectedIndex = -1
    End Sub

    Private Sub ClearDetailMaterialLevel5()
        wodtl3seq.Text = "1"
        If Session("TblDtlMatLev5") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("TblDtlMatLev5")
            wodtl3seq.Text = objTable.Rows.Count + 1
        End If
        i_u3.Text = "New Detail"
        DDLProcess.SelectedIndex = -1
        wodtl3reftype.SelectedIndex = -1
        wodtl3refoid.Text = ""
        wodtl3refshortdesc.Text = ""
        wodtl3refcode.Text = ""
        wodtl3qty.Text = ""
        wodtl3unitoid.SelectedIndex = -1
        wodtl3note.Text = ""
        wodtl3qtyref.Text = ""
        GVDtl3.SelectedIndex = -1
        itemcat1.Text = ""
        itemcat2.Text = ""
        itemcat3.Text = ""
        wodtl3unitoid.Items.Clear()
        InitAllDDL()
        wodtl3unitoid.CssClass = "inpText" : wodtl3unitoid.Enabled = True
    End Sub

    Private Sub ClearDetailMaterialLevel3()
        wodtl4seq.Text = "1"
        If Session("TblDtlMatLev3") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("TblDtlMatLev3")
            wodtl4seq.Text = objTable.Rows.Count + 1
        End If
        i_u5.Text = "New Detail"
        DDLProcess2.SelectedIndex = -1
        wodtl4reftype.SelectedIndex = -1
        wodtl4refoid.Text = ""
        wodtl4refshortdesc.Text = ""
        wodtl4refcode.Text = ""
        wodtl4qty.Text = ""
        wodtl4unitoid.SelectedIndex = -1
        wodtl4note.Text = ""
        wodtl4qtyref.Text = ""
        GVDtl4.SelectedIndex = -1
    End Sub

    Private Sub UpdateDetailProcess()
        If Not Session("TblDtlProcess") Is Nothing Then
            Dim dt As DataTable = Session("TblDtlProcess")
            Dim dv As DataView = dt.DefaultView
            If dv.Count > 0 Then
                For C1 As Integer = 0 To dv.Count - 1
                    dv(C1)("wodtl2qty") = ToDouble(wodtl2qty.Text)
                Next
            End If
            dv.RowFilter = ""
            Session("TblDtlProcess") = dv.ToTable
            GVDtl2.DataSource = Session("TblDtlProcess")
            GVDtl2.DataBind()
        End If
    End Sub

    Private Sub UpdateDetailMatLevel5(ByVal dQty As Double, Optional ByVal sType As String = "")
        If Not Session("TblDtlMatLev5") Is Nothing Then
            Dim dt As DataTable = Session("TblDtlMatLev5")
            Dim dv As DataView = dt.DefaultView
            If dv.Count > 0 Then
                For C1 As Integer = 0 To dv.Count - 1
                    Dim qty As Double = ToDouble(dv(C1)("wodtl3qtyref").ToString) * dQty
                    dv(C1)("wodtl3qty") = qty
                    dv(C1)("wodtl3amt") = ToDouble(dv(C1)("wodtl3value").ToString) * qty
                Next
            End If
            dv.RowFilter = ""
            Session("TblDtlMatLev5") = dv.ToTable
            GVDtl3.DataSource = Session("TblDtlMatLev5")
            GVDtl3.DataBind()
        End If
    End Sub

    Private Sub UpdateDetailMatLevel3(ByVal dQty As Double, Optional ByVal sType As String = "")
        If Not Session("TblDtlMatLev3") Is Nothing Then
            Dim dt As DataTable = Session("TblDtlMatLev3")
            Dim dv As DataView = dt.DefaultView
            If sType = "" Then
                dv.RowFilter = "wodtl1seq=" & wodtl1seq.Text
            Else
                dv.RowFilter = "wodtl2seq=" & wodtl2seq.Text
            End If
            If dv.Count > 0 Then
                For C1 As Integer = 0 To dv.Count - 1
                    dv(C1)("wodtl4qty") = ToDouble(dv(C1)("wodtl4qtyref").ToString) * dQty
                Next
            End If
            dv.RowFilter = ""
            Session("TblDtlMatLev3") = dv.ToTable
            GVDtl4.DataSource = Session("TblDtlMatLev3")
            GVDtl4.DataBind()
        End If
    End Sub

    Private Sub ClearDetailFinishGood()
        wodtl1seq.Text = "1"
        If Session("TblDtlFG") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("TblDtlFG")
            wodtl1seq.Text = objTable.Rows.Count + 1
            If objTable.Rows.Count > 0 Then
                EnableHeader(False)
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        i_u2.Text = "New Detail"
        soitemmstoid.Text = ""
        soitemno.Text = ""
        groupoid.Text = ""
        groupdesc.Text = ""
        soitemdtloid.Text = ""
        itemoid.Text = ""
        itemshortdesc.Text = ""
        itemcode.Text = ""
        soitemqty.Text = ""
        wodtl1qty.Text = ""
        wodtl1area.Text = ""
        itemlimitqty.Text = ""
        wodtl1unitoid.SelectedIndex = -1
        bomoid.Text = ""
        wodtl1note.Text = ""

        wodtl1totalmatamt.Text = ""
        wodtl1dlcamt.Text = ""
        wodtl1ohdamt.Text = ""
        curroid_ohd.Text = ""
        wodtl1ohdamtidr.Text = ""
        wodtl1marginamt.Text = ""
        wodtl1precostamt.Text = ""
        curroid_precost.Text = ""
        wodtl1precostamtidr.Text = ""

        lastupduser_precost.Text = ""
        lastupdtime_precost.Text = ""
        lastupduser_bom.Text = ""
        lastupdtime_bom.Text = ""
        lastupduser_dlc.Text = ""
        lastupdtime_dlc.Text = ""

        GVDtl1.SelectedIndex = -1
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal
        DDLBusUnit.CssClass = sCss
        cbAutoGenerate.Enabled = bVal
    End Sub

    Private Sub DeleteDetailProcess(ByVal iSeq As Integer)
        If Not Session("TblDtlProcess") Is Nothing Then
            Dim dt As DataTable = Session("TblDtlProcess")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "wodtl1seq=" & iSeq
            dv.AllowDelete = True
            For C1 As Integer = 0 To dv.Count - 1
                DeleteDetailMatLevel5(CInt(dv(0)("wodtl2seq").ToString))
                DeleteDetailMatLevel3(CInt(dv(0)("wodtl2seq").ToString))
                dv.Delete(0)
            Next
            dv.RowFilter = ""
            dv.AllowEdit = True
            For C1 As Integer = 0 To dv.Count - 1
                UpdateSeqDetailMatLevel5(CInt(dv(C1)("wodtl2seq").ToString), C1 + 1)
                UpdateSeqDetailMatLevel3(CInt(dv(C1)("wodtl2seq").ToString), C1 + 1)
                dv(C1)("wodtl2seq") = C1 + 1
            Next
            Session("TblDtlProcess") = dv.ToTable
            GVDtl2.DataSource = Session("TblDtlProcess")
            GVDtl2.DataBind()
        End If
    End Sub

    Private Sub DeleteDetailMatLevel5(ByVal iSeq As Integer)
        If Not Session("TblDtlMatLev5") Is Nothing Then
            Dim dt As DataTable = Session("TblDtlMatLev5")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "wodtl2seq=" & iSeq
            dv.AllowDelete = True
            For C1 As Integer = 0 To dv.Count - 1
                dv.Delete(0)
            Next
            dv.RowFilter = ""
            dv.AllowEdit = True
            For C1 As Integer = 0 To dv.Count - 1
                dv(C1)("wodtl3seq") = C1 + 1
            Next
            Session("TblDtlMatLev5") = dv.ToTable
            GVDtl3.DataSource = Session("TblDtlMatLev5")
            GVDtl3.DataBind()
        End If
    End Sub

    Private Sub DeleteDetailMatLevel3(ByVal iSeq As Integer)
        If Not Session("TblDtlMatLev3") Is Nothing Then
            Dim dt As DataTable = Session("TblDtlMatLev3")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "wodtl2seq=" & iSeq
            dv.AllowDelete = True
            For C1 As Integer = 0 To dv.Count - 1
                dv.Delete(0)
            Next
            dv.RowFilter = ""
            dv.AllowEdit = True
            For C1 As Integer = 0 To dv.Count - 1
                dv(C1)("wodtl4seq") = C1 + 1
            Next
            Session("TblDtlMatLev3") = dv.ToTable
            GVDtl4.DataSource = Session("TblDtlMatLev3")
            GVDtl4.DataBind()
        End If
    End Sub

    Private Sub UpdateSeqDetailMatLevel5(ByVal iLastSeq As Integer, ByVal iNewSeq As Integer)
        If Not Session("TblDtlMatLev5") Is Nothing Then
            Dim dt As DataTable = Session("TblDtlMatLev5")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "wodtl2seq=" & iLastSeq
            For C1 As Integer = 0 To dv.Count - 1
                dv(0)("wodtl2seq") = iNewSeq
            Next
            dv.RowFilter = ""
            Session("TblDtlMatLev5") = dv.ToTable
            GVDtl3.DataSource = Session("TblDtlMatLev5")
            GVDtl3.DataBind()
        End If
    End Sub

    Private Sub UpdateSeqDetailMatLevel3(ByVal iLastSeq As Integer, ByVal iNewSeq As Integer)
        If Not Session("TblDtlMatLev3") Is Nothing Then
            Dim dt As DataTable = Session("TblDtlMatLev3")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "wodtl2seq=" & iLastSeq
            For C1 As Integer = 0 To dv.Count - 1
                dv(0)("wodtl2seq") = iNewSeq
            Next
            dv.RowFilter = ""
            Session("TblDtlMatLev3") = dv.ToTable
            GVDtl4.DataSource = Session("TblDtlMatLev3")
            GVDtl4.DataBind()
        End If
    End Sub

    Private Sub UpdateSeqDetailProcess(ByVal iLastSeq As Integer, ByVal iNewSeq As Integer)
        If Not Session("TblDtlProcess") Is Nothing Then
            Dim dt As DataTable = Session("TblDtlProcess")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "wodtl1seq=" & iLastSeq
            For C1 As Integer = 0 To dv.Count - 1
                dv(0)("wodtl1seq") = iNewSeq
            Next
            dv.RowFilter = ""
            Session("TblDtlProcess") = dv.ToTable
            GVDtl2.DataSource = Session("TblDtlProcess")
            GVDtl2.DataBind()
        End If
    End Sub

    Private Sub GenerateWONo()
        Dim sNo As String = "SPK-" & Format(CDate(wodate.Text), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(wono, " & DefaultFormatCounter & ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnwomst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wono LIKE '" & sNo & "%'"
        wono.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        sSql = "SELECT wom.cmpcode, womstoid, periodacctg, wodate, wono, wodocrefno, womstnote, womststatus, wom.createuser, wom.createtime, wom.upduser, wom.updtime, ISNULL(autogenerate,'') AS autogenerate, dlcamount, ohdamount, s.suppoid, s.suppname, wostartdate, wofinishdate, wotolerance, kancingamt, kainamt, labelamt, othervalue, kancingamt_use_ap, kainamt_use_ap, labelamt_use_ap, othervalue_use_ap, ohdamount_use_ap, bordiramt, suppoid_bordir, s2.suppname suppname_bordir, jumlah_kancing, harga_kancing, harga_jual, lastplanmstoid, wotempstatus FROM QL_trnwomst wom INNER JOIN QL_mstsupp s ON s.suppoid=wom.suppoid LEFT JOIN QL_mstsupp s2 ON s2.suppoid=suppoid_bordir WHERE womstoid=" & sOid
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        While xreader.Read
            DDLBusUnit.SelectedValue = xreader("cmpcode").ToString
            womstoid.Text = Trim(xreader("womstoid").ToString)
            PeriodAcctg.Text = Trim(xreader("periodacctg").ToString)
            wodate.Text = Format(xreader("wodate"), "MM/dd/yyyy")
            wono.Text = Trim(xreader("wono").ToString)
            wodocrefno.Text = Trim(xreader("wodocrefno").ToString)
            womstnote.Text = Trim(xreader("womstnote").ToString)
            womststatus.Text = Trim(xreader("womststatus").ToString)
            dlcamt.Text = ToMaskEdit(ToDouble(xreader("dlcamount").ToString), 2)
            ohdamt.Text = ToMaskEdit(ToDouble(xreader("ohdamount").ToString), 2)
            kancingamt.Text = ToMaskEdit(ToDouble(xreader("kancingamt").ToString), 2)
            kainamt.Text = ToMaskEdit(ToDouble(xreader("kainamt").ToString), 2)
            labelamt.Text = ToMaskEdit(ToDouble(xreader("labelamt").ToString), 2)
            makloonoid.Text = Trim(xreader("suppoid").ToString)
            makloonname.Text = Trim(xreader("suppname").ToString)
            createuser.Text = Trim(xreader("createuser").ToString)
            createtime.Text = Trim(xreader("createtime").ToString)
            upduser.Text = Trim(xreader("upduser").ToString)
            updtime.Text = Trim(xreader("updtime").ToString)
            startdate.Text = Format(xreader("wostartdate"), "MM/dd/yyyy")
            finishdate.Text = Format(xreader("wofinishdate"), "MM/dd/yyyy")
            wotolerance.Text = ToMaskEdit(ToDouble(xreader("wotolerance").ToString), 2)
            othervalue.Text = ToMaskEdit(ToDouble(xreader("othervalue").ToString), 2)
            txtJumlahKancing.Text = ToMaskEdit(ToDouble(xreader("jumlah_kancing").ToString), 2)
            txtRefPriceKancing.Text = ToMaskEdit(ToDouble(xreader("harga_kancing").ToString), 2)
            txtHargaJualFinal.Text = ToMaskEdit(ToDouble(xreader("harga_jual").ToString), 2)
            'If xreader("autogenerate").ToString.ToUpper = "YES" Then
            '    cbAutoGenerate.Checked = True
            'Else
            '    cbAutoGenerate.Checked = False
            'End If
            cbAutoGenerate.Checked = xreader("autogenerate").ToString.ToUpper = "YES"
            'Dim bVal_kancing As Boolean = False, bVal_kain As Boolean = False, bVal_label As Boolean = False, bVal_other As Boolean = False, bVal_ohd As Boolean = False
            'If xreader("kancingamt_use_ap").ToString.ToUpper = "Y" Then
            '    bVal_kancing = True
            'End If
            'If xreader("kainamt_use_ap").ToString.ToUpper = "Y" Then
            '    bVal_kain = True
            'End If
            'If xreader("labelamt_use_ap").ToString.ToUpper = "Y" Then
            '    bVal_label = True
            'End If
            'If xreader("othervalue_use_ap").ToString.ToUpper = "Y" Then
            '    bVal_other = True
            'End If
            'If xreader("ohdamount_use_ap").ToString.ToUpper = "Y" Then
            '    bVal_ohd = True
            'End If
            cbKancing_use_ap.Checked = xreader("kancingamt_use_ap").ToString.ToUpper = "Y"
            cbKain_use_ap.Checked = xreader("kainamt_use_ap").ToString.ToUpper = "Y"
            cbLabel_use_ap.Checked = xreader("labelamt_use_ap").ToString.ToUpper = "Y"
            cbOther_use_ap.Checked = xreader("othervalue_use_ap").ToString.ToUpper = "Y"
            cbBordir_use_ap.Checked = xreader("ohdamount_use_ap").ToString.ToUpper = "Y"
            bordiramt.Text = ToMaskEdit(ToDouble(xreader("bordiramt").ToString), 2)
            suppname_bordir.Text = xreader("suppname_bordir").ToString
            suppoid_bordir.Text = xreader("suppoid_bordir").ToString
            initDDLTransfer()
            ddlNoTransfer.SelectedValue = xreader("lastplanmstoid").ToString
            wotempstatus.Text = xreader("wotempstatus").ToString
        End While
        xreader.Close()
        conn.Close()

        ' Generate Detail Finish Good
        sSql = "SELECT wod1.wodtl1oid, wod1.wodtl1seq, wod1.soitemmstoid, som.sono AS soitemno, som.groupoid,'' groupdesc, wod1.soitemdtloid, wod1.itemoid, i.itemLongDescription AS itemshortdesc, i.itemcode, i.roundQty AS itemlimitqty, (sod.sodtlqty - ISNULL((SELECT SUM(wodx.wodtl1qty_soqty) FROM QL_trnwodtl1 wodx INNER JOIN QL_trnwomst womx ON womx.cmpcode=wodx.cmpcode AND womx.womstoid=wodx.womstoid WHERE wodx.cmpcode=wod1.cmpcode AND wodx.soitemdtloid=wod1.soitemdtloid AND womx.womststatus<>'Cancel' AND wodx.womstoid<>wod1.womstoid), 0)) AS soitemqty, wod1.wodtl1qty, wod1.wodtl1unitoid, g.gendesc AS wodtl1unit, wod1.bomoid, wod1.wodtl1note, wod1.wodtl1area, wodtl1totalmatamt, wodtl1dlcamt, wodtl1ohdamt, curroid_ohd, wodtl1ohdamtidr, wodtl1marginamt, wodtl1precostamt, curroid_precost, wodtl1precostamtidr, lastupduser_precost, lastupdtime_precost, lastupduser_bom, lastupdtime_bom, lastupduser_dlc, lastupdtime_dlc, wodtl1qty_soqty FROM QL_trnwodtl1 wod1 LEFT JOIN QL_trnsomst som ON som.cmpcode=wod1.cmpcode AND som.somstoid=wod1.soitemmstoid /*INNER JOIN QL_mstdeptgroup dg ON dg.cmpcode=som.cmpcode AND dg.groupoid=som.groupoid*/ LEFT JOIN QL_trnsodtl sod ON sod.cmpcode=wod1.cmpcode AND sod.sodtloid=wod1.soitemdtloid /*INNER JOIN QL_mstbom bom ON bom.cmpcode=wod1.cmpcode AND bom.bomoid=wod1.bomoid*/ INNER JOIN QL_mstitem i ON i.itemoid=wod1.itemoid INNER JOIN QL_mstgen g ON g.genoid=wod1.wodtl1unitoid WHERE wod1.womstoid=" & sOid & " ORDER BY wod1.wodtl1seq"
        Dim dtFG As DataTable = cKon.ambiltabel(sSql, "QL_trnwodtl1")
        Session("TblDtlFG") = dtFG
        GVDtl1.DataSource = Session("TblDtlFG")
        GVDtl1.DataBind()

        ' Generate Detail Process
        sSql = "SELECT wod2.wodtl2oid, wod2.wodtl2seq, wod1.wodtl1seq, i.itemcode AS wodtl2fgcode, i.itemshortdescription AS wodtl2fgdesc, wod2.bomdtl1oid, wod2.wodtl2procseq, wod2.wodtl2type, wod2.deptoid, de.deptname, wod2.todeptoid, (CASE wod2.wodtl2type WHEN 'WIP' THEN de2.deptname ELSE 'END' END) AS todeptname, wod2.wodtl2qty, wod2.wodtl2unitoid, g.gendesc AS wodtl2unit, wod2.wodtl2note, wod2.wodtl2desc, i.roundQty AS processlimitqty, wodtl2dlcvalue, wodtl2fohvalue FROM QL_trnwodtl2 wod2 INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=wod2.cmpcode AND wod1.wodtl1oid=wod2.wodtl1oid INNER JOIN QL_mstgen g ON g.genoid=wod2.wodtl2unitoid INNER JOIN QL_mstdept de ON de.cmpcode=wod2.cmpcode AND de.deptoid=wod2.deptoid INNER JOIN QL_mstdept de2 ON de2.cmpcode=wod2.cmpcode AND de2.deptoid=wod2.todeptoid INNER JOIN QL_mstitem i ON i.itemoid=wod1.itemoid WHERE wod2.womstoid=" & sOid & " ORDER BY wod1.wodtl1seq, wod2.wodtl2seq"
        Dim dtProc As DataTable = cKon.ambiltabel(sSql, "QL_trnwodtl2")
        For C1 As Integer = 0 To dtProc.Rows.Count - 1
            dtProc.Rows(C1)("wodtl2seq") = C1 + 1
            DDLProcess.Items.Add(dtProc.Rows(C1)("deptname").ToString & " - " & dtProc.Rows(C1)("todeptname").ToString)
            DDLProcess.Items.Item(DDLProcess.Items.Count - 1).Value = C1 + 1
            DDLbom.Items.Add(dtProc.Rows(C1)("wodtl2fgcode").ToString & " - " & dtProc.Rows(C1)("wodtl2fgdesc").ToString)
            DDLbom.Items.Item(DDLbom.Items.Count - 1).Value = C1 + 1
        Next
        Session("TblDtlProcess") = dtProc
        GVDtl2.DataSource = Session("TblDtlProcess")
        GVDtl2.DataBind()
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        ' Generate Detail Material Level 5
        sSql = "SELECT wodtl3seq, wodtl1seq, wodtl2seq, wodtl2desc, bomdtl2oid, wodtl3reftype, wodtl3refoid, wodtl3refshortdesc, wodtl3refcode, wodtl3qty, wodtl3amt, wodtl3unitoid, wodtl3unit, wodtl3note, wodtl3qtyref, wodtl3value, wodtl3stockqty, wodtl3fgcode, cat1, cat2, cat3, wodtl3oid FROM ("
        sSql &= "SELECT DISTINCT wodtl3oid, mx.itemcat1 AS cat1, mx.itemcat2 AS cat2, mx.itemcat3 AS cat3, wod3.wodtl3seq, wod1.wodtl1seq, (wod2.wodtl2seq + (SELECT ISNULL(MAX(wod2x.wodtl2seq), 0) FROM QL_trnwodtl2 wod2x WHERE wod2x.cmpcode=wod2.cmpcode AND wod2x.wodtl1oid<wod2.wodtl1oid AND wod2x.womstoid=wod2.womstoid)) AS wodtl2seq, wod2.wodtl2desc, wod3.bomdtl2oid, wod3.wodtl3reftype, wod3.wodtl3refoid, m.itemLongDescription AS wodtl3refshortdesc, m.itemCode AS wodtl3refcode, wod3.wodtl3qty, wod3.wodtl3unitoid, g.gendesc AS wodtl3unit, wod3.wodtl3note, (wod3.wodtl3qty / wod1.wodtl1qty) AS wodtl3qtyref, case wodtl3value when 0 then isnull((select isnull(sum(valueidr * (qtyin - qtyout)) / nullif(sum(qtyin - qtyout), 0.0), 0.0) from QL_constock where refoid=wod3.wodtl3refoid), 0.0) else wodtl3value end wodtl3value, case wodtl3amt when 0 then isnull((select isnull(sum(valueidr * (qtyin - qtyout)) / nullif(sum(qtyin - qtyout), 0.0), 0.0) from QL_constock where refoid=wod3.wodtl3refoid), ISNULL((SELECT TOP 1 lastpurchaseprice FROM QL_mstitemprice mp WHERE mp.itemoid=wod3.wodtl3refoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0)) * wod3.wodtl3qty else wodtl3amt end wodtl3amt, (select dbo.getstockqty2(wod3.wodtl3refoid)) AS wodtl3stockqty, mx.itemcode AS wodtl3fgcode FROM QL_trnwodtl3 wod3 LEFT JOIN QL_trnwodtl2 wod2 ON wod2.cmpcode=wod3.cmpcode AND wod2.wodtl2oid=wod3.wodtl2oid LEFT JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=wod3.cmpcode AND wod1.wodtl1oid=wod3.wodtl1oid INNER JOIN QL_mstitem m ON m.itemoid=wod3.wodtl3refoid INNER JOIN QL_mstgen g ON g.genoid=wod3.wodtl3unitoid LEFT JOIN QL_mstitem mx ON mx.itemoid=wod1.itemoid WHERE wod3.womstoid=" & sOid
        sSql &= ") AS tbltmp ORDER BY wodtl1seq, wodtl2seq, wodtl3seq"
        Dim dtMatLev5 As DataTable = cKon.ambiltabel(sSql, "QL_trnwodtl3")
        For C1 As Integer = 0 To dtMatLev5.Rows.Count - 1
            dtMatLev5.Rows(C1)("wodtl3seq") = C1 + 1
        Next
        Session("TblDtlMatLev5") = dtMatLev5
        GVDtl3.DataSource = Session("TblDtlMatLev5")
        GVDtl3.DataBind()

        ' Generate Detail Material Level 2
        sSql = "SELECT DISTINCT wod4.wodtl4seq, wod1.wodtl1seq, (wod2.wodtl2seq + (SELECT ISNULL(MAX(wod2x.wodtl2seq), 0) FROM QL_trnwodtl2 wod2x WHERE wod2x.cmpcode=wod2.cmpcode AND wod2x.wodtl1oid<wod2.wodtl1oid AND wod2x.womstoid=wod2.womstoid)) AS wodtl2seq, wod2.wodtl2desc, wod4.bomdtl3oid, wod4.wodtl4reftype, wod4.wodtl4refoid, (RTRIM((CASE WHEN (LTRIM(c1.cat1shortdesc)='None' OR LTRIM(c1.cat1shortdesc)='') THEN '' ELSE c1.cat1shortdesc + ' ' END) + (CASE WHEN (LTRIM(c2.cat2shortdesc)='None' OR LTRIM(c2.cat2shortdesc)='') THEN '' ELSE c2.cat2shortdesc + ' ' END))) AS wodtl4refshortdesc, (c1.cat1code + '.' + c2.cat2code) AS wodtl4refcode, wod4.wodtl4qty, wod4.wodtl4unitoid, g.gendesc AS wodtl4unit, wod4.wodtl4note, (wod4.wodtl4qty / wod1.wodtl1qty) AS wodtl4qtyref, wodtl4value FROM QL_trnwodtl4 wod4 INNER JOIN QL_trnwodtl2 wod2 ON wod2.cmpcode=wod4.cmpcode AND wod2.wodtl2oid=wod4.wodtl2oid INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=wod4.cmpcode AND wod1.wodtl1oid=wod4.wodtl1oid INNER JOIN QL_mstcat2 c2 ON c2.cat2oid=wod4.wodtl4refoid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c1.cat1oid=c2.cat1oid INNER JOIN QL_mstgen g ON g.genoid=wod4.wodtl4unitoid WHERE wod4.womstoid=" & sOid
        Dim dtMatLev3 As DataTable = cKon.ambiltabel(sSql, "QL_trnwodtl4")
        For C1 As Integer = 0 To dtMatLev3.Rows.Count - 1
            dtMatLev3.Rows(C1)("wodtl4seq") = C1 + 1
        Next
        Session("TblDtlMatLev3") = dtMatLev3
        GVDtl4.DataSource = Session("TblDtlMatLev3")
        GVDtl4.DataBind()

        If womststatus.Text = "Post" Or womststatus.Text = "Closed" Or womststatus.Text = "Cancel" Or womststatus.Text = "In Approval" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnPost.Visible = False
            'btnAddToList1.Visible = False
            btnAddToList2.Visible = False
            'GVDtl1.Columns(0).Visible = False
            GVDtl1.Columns(GVDtl1.Columns.Count - 1).Visible = False
            GVDtl2.Columns(0).Visible = False
            GVDtl2.Columns(GVDtl2.Columns.Count - 1).Visible = False
            'GVDtl3.Columns(0).Visible = False
            lblTrnNo.Text = "SPK No."
            womstoid.Visible = False
            wono.Visible = True
            If Session("UserID").ToString().ToLower() = "admin" or Session("UserID").ToString().ToLower() = "isti" Then
                btnUpdate.Visible = True
            Else
                btnUpdate.Visible = False : btnSendApp.Visible = True
            End If
            If womststatus.Text = "In Approval" Then
                btnSendApp.Visible = False
            End If
            btnAddToList.Visible = False
            GVDtl3.Columns(GVDtl3.Columns.Count - 1).Visible = False
            GVDtl3.Columns(0).Visible = False
            btnSearchMat.Visible = False
            btnEraseMat.Visible = False

            wodtl3reftype.Enabled = False
            wodtl3reftype.CssClass = "inpTextDisabled"
        End If
        EnableHeader(False)
        ClearDetailFinishGood()
        ClearDetailProcess()
        ClearDetailMaterialLevel5()
        ClearDetailMaterialLevel3()
        GetTotalQty() : getTotalCalculaion()
    End Sub

    Private Sub PrintReport()
        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("womstoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            report.Load(Server.MapPath(folderReport & "rptJobCosting.rpt"))
            Dim sWhere As String = ""
            If sOid = "" Then
                sWhere &= " WHERE " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If FilterDDL.SelectedIndex = FilterDDL.Items.Count - 1 Then
                    sWhere &= " AND wom.womststatus<>'In Process'"
                End If
                If Session("CompnyCode") <> CompnyCode Then
                    sWhere &= " AND wom.cmpcode='" & Session("CompnyCode") & "'"
                End If
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND wom.bomdate>='" & FilterPeriod1.Text & " 00:00:00' AND wom.bomdate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbBusUnit.Checked Then
                    If FilterDDLBusUnit.SelectedValue <> "" Then
                        sWhere &= " AND wom.cmpcode='" & FilterDDLBusUnit.SelectedValue & "'"
                    End If
                End If
                If cbQtyKIK.Checked Then
                    sWhere &= " AND wod1.wodtl1qty " & FilterDDLQtyKIK.SelectedValue & " " & ToDouble(FilterTextQtyKIK.Text)
                End If
                If cbStatus.Checked Then
                    sWhere &= " AND wom.womststatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Transaction\trnWO.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND wom.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sWhere &= " AND wom.womstoid IN (" & sOid & ")"
            End If
            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "JobCostingMOPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
        Response.Redirect("~\Transaction\trnWO.aspx?awal=true")
    End Sub

    Private Sub InitDDLCat01ListMat()
        'Fill DDL Category 1
        sSql = "SELECT c1.cat1oid, c1.cat1code +' - '+ c1.cat1shortdesc FROM QL_mstcat1 c1 WHERE c1.cmpcode='" & CompnyCode & "' AND c1.activeflag='ACTIVE'"
        If wodtl3reftype.SelectedValue = "Raw" Then
            sSql &= " AND cat1res1='Raw' /*AND cat1res2 IN ('WIP', 'Non WIP')*/"
        Else
            sSql &= " AND cat1res1='" & wodtl3reftype.SelectedValue & "'"
        End If
        sSql &= " ORDER BY cat1code"
        If FillDDL(FilterDDLCat01ListMat, sSql) Then
            InitDDLCat02ListMat()
        Else
            FilterDDLCat02ListMat.Items.Clear()
            FilterDDLCat03ListMat.Items.Clear()
            FilterDDLCat04ListMat.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat02ListMat()
        'Fill DDL Category 2
        sSql = "SELECT cat2oid, cat2code +' - '+ cat2shortdesc FROM QL_mstcat2 c2 INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c1.cat1oid=c2.cat1oid WHERE c2.cmpcode='" & CompnyCode & "' AND c2.activeflag='ACTIVE' AND c1.cat1oid='" & FilterDDLCat01ListMat.SelectedValue & "'"
        If wodtl3reftype.SelectedValue = "Raw" Then
            sSql &= " AND cat2res1='Raw' AND cat1res1='Raw' /*AND cat1res2 IN ('WIP', 'Non WIP')*/"
        Else
            sSql &= " AND cat2res1='" & wodtl3reftype.SelectedValue & "' AND cat1res1='" & wodtl3reftype.SelectedValue & "'"
        End If
        sSql &= " ORDER BY cat2code"
        If FillDDL(FilterDDLCat02ListMat, sSql) Then
            InitDDLCat03ListMat()
        Else
            FilterDDLCat03ListMat.Items.Clear()
            'FilterDDLCat04ListMat.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat03ListMat()
        'Fill DDL Category 3
        sSql = "SELECT cat3oid, cat3code +' - '+ cat3shortdesc FROM QL_mstcat3 c3 INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c3.cmpcode AND c1.cat1oid=c3.cat1oid WHERE c3.cmpcode='" & CompnyCode & "' AND c3.activeflag='ACTIVE' AND c2.cat2oid='" & FilterDDLCat02ListMat.SelectedValue & "'"
        If wodtl3reftype.SelectedValue = "Raw" Then
            sSql &= " AND cat3res1='Raw' AND cat2res1='Raw' AND cat1res1='Raw' /*AND cat1res2 IN ('WIP', 'Non WIP')*/"
        Else
            sSql &= " AND cat3res1='" & wodtl3reftype.SelectedValue & "' AND cat2res1='" & wodtl3reftype.SelectedValue & "' AND cat1res1='" & wodtl3reftype.SelectedValue & "'"
        End If
        sSql &= " ORDER BY cat3code"
        If FillDDL(FilterDDLCat03ListMat, sSql) Then
            InitDDLCat04ListMat()
        Else
            FilterDDLCat04ListMat.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat04ListMat()
        'Fill DDL Category 4
        sSql = "SELECT genoid, gencode +' - '+ gendesc FROM QL_mstgen WHERE gengroup='CAT4' AND activeflag='ACTIVE' ORDER BY gencode"
        FillDDL(FilterDDLCat04ListMat, sSql)
    End Sub

    Private Sub BindMatData()
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sSql = "SELECT 'False' AS checkvalue, itemcat1, itemcat2, 0 AS bomdtl2oid, itemcat3, itemcat4, m.itemoid AS matrefoid, m.itemCode AS matrefcode, m.itemLongDescription AS matrefshortdesc, m.itemUnit1 AS matrefunitoid, g.gendesc AS matrefunit, 0.0 AS bomdtl2qty, '' AS bomdtl2note, roundQty AS roundqtydtl2, 0 AS precostdtlrefoid, isnull((select isnull(sum(valueidr * (qtyin - qtyout)) / nullif(sum(qtyin - qtyout), 0.0), 0.0) from QL_constock where refoid=m.itemoid), ISNULL((SELECT TOP 1 lastpurchaseprice FROM QL_mstitemprice mp WHERE mp.itemoid=m.itemoid AND refname='SUPPLIER' ORDER BY lasttransdate DESC), 0.0)) AS bomdtl2refvalue, /*(select dbo.getstockqty2(m.itemoid))*/0.0 AS wodtl3stockqty FROM QL_mstcat1 c1 INNER JOIN QL_mstitem m ON m.itemCat1=c1.cat1oid AND cat1res1='" & wodtl3reftype.SelectedValue & "' INNER JOIN QL_mstgen g ON genoid=itemUnit1 WHERE m.cmpcode='" & DDLBusUnit.SelectedValue & "' ORDER BY m.itemCode"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstmat")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = ""
        If dv.Count > 0 Then
            Session("TblListMat") = dv.ToTable
            Session("TblListMatView") = dv.ToTable
            gvListMat2.DataSource = Session("TblListMatView")
            gvListMat2.DataBind()
        Else
            Session("ErrMat") = "Material data can't be found!"
            showMessage(Session("ErrMat"), 2)
        End If
    End Sub

    Private Sub UpdateCheckedMatKIK()
        Dim dv As DataView = Session("TblListMat").DefaultView
        dv.AllowEdit = True
        For C1 As Integer = 0 To gvListMat2.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat2.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        dv.RowFilter = "matrefoid=" & sOid
                        If cbcheck = True Then
                            dv(0)("checkvalue") = "True"
                            Dim cc2 As System.Web.UI.ControlCollection = row.Cells(3).Controls
                            dv(0)("bomdtl2qty") = ToDouble(GetTextValue(cc2))
                            cc2 = row.Cells(5).Controls
                            dv(0)("bomdtl2note") = GetTextValue(cc2)
                            cc2 = row.Cells(4).Controls
                            dv(0)("matrefunit") = GetDDLValue(cc2, "Text")
                            dv(0)("matrefunitoid") = GetDDLValue(cc2)
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                        dv.RowFilter = ""
                    End If
                Next
            End If
        Next
    End Sub

    Private Sub UpdateCheckedMatViewKIK()
        Dim dv As DataView = Session("TblListMatView").DefaultView
        dv.AllowEdit = True
        For C1 As Integer = 0 To gvListMat2.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat2.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        dv.RowFilter = "matrefoid=" & sOid
                        If cbcheck = True Then
                            dv(0)("checkvalue") = "True"
                            Dim cc2 As System.Web.UI.ControlCollection = row.Cells(3).Controls
                            dv(0)("bomdtl2qty") = ToDouble(GetTextValue(cc2))
                            cc2 = row.Cells(5).Controls
                            dv(0)("bomdtl2note") = GetTextValue(cc2)
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                        dv.RowFilter = ""
                    End If
                Next
            End If
        Next
    End Sub

    Private Sub BindSupplierData()
        sSql = "SELECT suppoid, suppcode, supptype, suppname, supppaymentoid, suppaddr, supptaxable FROM QL_mstsupp WHERE cmpcode='" & CompnyCode & "' AND " & FilterDDLListSupp.SelectedValue & " LIKE '%" & Tchar(FilterTextListSupp.Text) & "%' AND suppgroup='Supplier Maklon' AND activeflag='ACTIVE'"
        sSql &= " ORDER BY suppcode"
        Dim objTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstsupp")
        Session("mstSupp") = objTbl
        gvListSupp.DataSource = objTbl
        gvListSupp.DataBind()
    End Sub

    Private Sub BindSupplierData2()
        sSql = "SELECT suppoid, suppcode, supptype, suppname, supppaymentoid, suppaddr, supptaxable FROM QL_mstsupp WHERE cmpcode='" & CompnyCode & "' AND " & FilterDDLListSupp2.SelectedValue & " LIKE '%" & Tchar(FilterTextListSupp2.Text) & "%' AND suppgroup='Supplier Maklon' AND activeflag='ACTIVE'"
        sSql &= " ORDER BY suppcode"
        Dim objTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstsupp2")
        Session("mstSupp2") = objTbl
        gvListSupp2.DataSource = objTbl
        gvListSupp2.DataBind()
    End Sub

    Private Sub InitDDLToDept(Optional ByVal sDeptEdit As String = "")
        ' Fill DDL Department
        If wodtl2type.SelectedValue = "FG" Then
            sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE activeflag='ACTIVE' AND cmpcode='" & DDLBusUnit.SelectedValue & "' AND deptoid=" & deptoid.SelectedValue
        Else
            sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE activeflag='ACTIVE' AND cmpcode='" & DDLBusUnit.SelectedValue & "' AND deptoid<>" & deptoid.SelectedValue
            Dim sOid As String = ""
            If Session("TblDtl") IsNot Nothing Then
                Dim dt As DataTable = Session("TblDtl")
                For C1 As Integer = 0 To dt.Rows.Count - 1
                    If sDeptEdit <> dt.Rows(C1)("bomdtl1deptoid").ToString Then
                        If ToInteger(wodtl2procseq.Text) > ToInteger(dt.Rows(C1)("bomdtl1res1").ToString) Then
                            sOid &= dt.Rows(C1)("bomdtl1deptoid").ToString & ","
                        End If
                    End If
                Next
            End If
            If sOid <> "" Then
                sOid = Left(sOid, sOid.Length - 1)
                sSql &= " AND deptoid NOT IN (" & sOid & ")"
            End If
        End If
        FillDDL(todeptoid, sSql)
    End Sub

    Private Sub getTotalCalculaion()
        Dim dVal As Double = 0 : dlcamtPlus.Text = ToMaskEdit(ToDouble(dlcamt.Text) + (ToDouble(dlcamt.Text) * 11 / 100), 2)
        If Session("TblDtlMatLev5") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtlMatLev5")
            For C1 As Integer = 0 To dt.Rows.Count - 1
                dVal += ToDouble(dt.Rows(C1)("wodtl3amt").ToString)
            Next
        End If
        Dim ongkosJahit As Double = ToDouble(txtQtyFG.Text) * ToDouble(dlcamtPlus.Text)
        Dim totalKancing As Double = ToDouble(txtQtyFG.Text) * ToDouble(kancingamt.Text)
        Dim totalLabel As Double = ToDouble(txtQtyFG.Text) * ToDouble(labelamt.Text)
        Dim totalKain As Double = ToDouble(txtQtyFG.Text) * ToDouble(kainamt.Text)
        Dim totalBordir As Double = ToDouble(txtQtyFG.Text) * ToDouble(ohdamt.Text)
        Dim totalBordir2 As Double = ToDouble(txtQtyFG.Text) * ToDouble(bordiramt.Text)
        Dim totalLain As Double = ToDouble(txtQtyFG.Text) * ToDouble(othervalue.Text)
        txtTotalBahan.Text = ToMaskEdit(dVal + ongkosJahit + totalKancing + totalLabel + totalKain + totalBordir + totalBordir2 + totalLain, 2)
        txtHPP.Text = ToMaskEdit(ToDouble(txtTotalBahan.Text) / ToDouble(txtQtyFG.Text), 2)
        txtMargin.Text = ToMaskEdit(ToDouble(txtHPP.Text) * 50 / 100, 2)
        txtHargaJual.Text = ToMaskEdit(ToDouble(txtHPP.Text) + ToDouble(txtMargin.Text), 2)
        If ToDouble(txtHargaJualFinal.Text) = 0 Then
            Dim harga_jual As Double = ToDouble(txtHPP.Text) + ToDouble(txtMargin.Text)
            Dim ppn As Double = ToDouble(harga_jual) * 11 / 100
            txtHargaJualFinal.Text = ToMaskEdit(harga_jual + ppn, 2)
        End If
    End Sub

    Private Sub GetTotalQty()
        Dim dVal As Double = 0, dValOther As Double = 0
        If Session("TblDtlFG") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtlFG")
            For C1 As Integer = 0 To dt.Rows.Count - 1
                dVal += ToDouble(dt.Rows(C1)("wodtl1qty").ToString)
            Next
        End If
        wodtl2qty.Text = ToMaskEdit(dVal, 2)
        txtQtyFG.Text = ToMaskEdit(dVal, 2)
    End Sub

    Private Sub ClearDetail2()
        wodtl2seq.Text = "1"
        If Session("TblDtl2") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("TblDtl2")
            wodtl2seq.Text = objTable.Rows.Count + 1
        End If
        i_u3.Text = "New Detail"
        wodtl3reftype.SelectedIndex = -1
        wodtl3refshortdesc.Text = ""
        wodtl3refoid.Text = ""
        wodtl3qty.Text = ""
        wodtl3unitoid.SelectedIndex = -1
        wodtl3note.Text = ""
        roundqtydtl3.Text = ""
        GVDtl2.SelectedIndex = -1
        wodtl3reftype.CssClass = "inpText" : wodtl3reftype.Enabled = True : btnSearchMat.Visible = True
    End Sub

    Private Sub initDDLTransfer()
        sSql = "select transitemmstoid, transitemno + ' - ' + transitemmstnote transitemno from QL_trntransitemmst where transitemno like 'sjp%' and iskonsinyasi='False' and transitemdate>='" & CDate(wodate.Text).AddMonths(-5) & "' and transitemmststatus='Post' and transitemmstres3='" & makloonoid.Text & "'"
        sSql &= " ORDER BY transitemdate desc"
        FillDDLWithAdditionalText(ddlNoTransfer, sSql, "Pilih No. Transfer", "0")
    End Sub

#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnWO.aspx")
        End If
        'If checkPagePermission("~\Transaction\trnWO.aspx", Session("Role")) = False Then
        '    Response.Redirect("~\Other\NotAuthorize.aspx")
        'End If
        Page.Title = CompnyName & " - Job Costing MO"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPost.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        btnSendApp.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to Send Approval this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            CheckWOStatus()
            InitAllDDL()
            cbAutoGenerate_CheckedChanged(Nothing, Nothing)
            DDLProcess.Items.Clear() : DDLProcess2.Items.Clear()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            startdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
            finishdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
            MultiView1.SetActiveView(View1) : pnlMatDtl3.Visible = False
            wodtl2procseq_TextChanged(Nothing, Nothing)
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                womstoid.Text = GenerateID("QL_TRNWOMST", CompnyCode)
                wodate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                womststatus.Text = "In Process"
                PeriodAcctg.Text = GetDateToPeriodAcctg(GetServerTime())
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        If Not Session("TblDtlFG") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtlFG")
            GVDtl1.DataSource = dt
            GVDtl1.DataBind()
        End If
        If Not Session("TblDtlProcess") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtlProcess")
            GVDtl2.DataSource = dt
            GVDtl2.DataBind()
        End If
        If Not Session("TblDtlMatLev5") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtlMatLev5")
            GVDtl3.DataSource = dt
            GVDtl3.DataBind()
        End If
        If Not Session("TblDtlMatLev3") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtlMatLev3")
            GVDtl4.DataSource = dt
            GVDtl4.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnWO.aspx?awal=true")
            ElseIf Session("SavedInfo") = "Warning BOM" Then
                cProc.SetModalPopUpExtender(btnHideListItem, pnlListItem, mpeListItem, True)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                cProc.SetModalPopUpExtender(btnHideListItem, pnlListItem, mpeListItem, True)
            End If
        End If
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub lkbWOInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbWOInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbBusUnit.Checked = False
        FilterDDLBusUnit.SelectedIndex = -1
        cbQtyKIK.Checked = False
        FilterDDLQtyKIK.SelectedIndex = -1
        FilterTextQtyKIK.Text = ""
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, wom.updtime, GETDATE()) > " & nDays & " AND wom.womststatus='In Process' "
        If checkPagePermission("~\Transaction\trnWO.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND wom.createuser='" & Session("UserID") & "' "
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If FilterDDL.SelectedIndex = FilterDDL.Items.Count - 1 Then
            sSqlPlus &= " AND wom.womststatus<>'In Process'"
        End If
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND wom.wodate>='" & FilterPeriod1.Text & " 00:00:00' AND wom.wodate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbBusUnit.Checked Then
            If FilterDDLBusUnit.SelectedValue <> "" Then
                sSqlPlus &= " AND wom.cmpcode='" & FilterDDLBusUnit.SelectedValue & "'"
            End If
        End If
        If cbQtyKIK.Checked Then
            sSqlPlus &= " AND wod1.wodtl1qty " & FilterDDLQtyKIK.SelectedValue & " " & ToDouble(FilterTextQtyKIK.Text)
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND wom.womststatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If cbsts.Checked Then
            sSqlPlus &= " AND wom.wotempstatus='" & ddlstatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Transaction\trnWO.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND wom.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbBusUnit.Checked = False
        FilterDDLBusUnit.SelectedIndex = -1
        cbQtyKIK.Checked = False
        FilterDDLQtyKIK.SelectedIndex = -1
        FilterTextQtyKIK.Text = ""
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        cbsts.Checked = False
        ddlstatus.SelectedIndex = -1
        If checkPagePermission("~\Transaction\trnWO.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND wom.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTRN.RowDataBound
        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
        'End If
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLDept()
        btnClearSO_Click(Nothing, Nothing)
    End Sub

    Protected Sub lkbDetProc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbDetProc1.Click
        MultiView1.SetActiveView(View2)
    End Sub

    Protected Sub lkbDetMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbDetMat1.Click
        MultiView1.SetActiveView(View3)
    End Sub

    Protected Sub lkbDetItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbDetItem1.Click
        MultiView1.SetActiveView(View1)
    End Sub

    Protected Sub lbLevel05_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbLevel05.Click
        pnlMatDtl5.Visible = True : pnlMatDtl3.Visible = False
    End Sub

    Protected Sub lbLevel03_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbLevel03.Click
        pnlMatDtl5.Visible = False : pnlMatDtl3.Visible = True
    End Sub

    Protected Sub btnSearchSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSO.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        FilterDDLListSO.SelectedIndex = -1 : FilterTextListSO.Text = "" : gvListSO.SelectedIndex = -1
        BindSOData()
        cProc.SetModalPopUpExtender(btnHideListSO, pnlListSO, mpeListSO, True)
    End Sub

    Protected Sub btnClearSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSO.Click
        soitemmstoid.Text = "" : soitemno.Text = "" : groupoid.Text = "" : groupdesc.Text = ""
        btnClearItem_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnFindListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSO.Click
        BindSOData()
        mpeListSO.Show()
    End Sub

    Protected Sub btnAllListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListSO.Click
        FilterDDLListSO.SelectedIndex = -1 : FilterTextListSO.Text = "" : gvListSO.SelectedIndex = -1
        BindSOData()
        mpeListSO.Show()
    End Sub

    Protected Sub gvListSO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSO.PageIndexChanging
        gvListSO.PageIndex = e.NewPageIndex
        BindSOData()
        mpeListSO.Show()
    End Sub

    Protected Sub gvListSO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListSO.SelectedIndexChanged
        If soitemmstoid.Text <> gvListSO.SelectedDataKey.Item("soitemmstoid").ToString Then
            btnClearSO_Click(Nothing, Nothing)
        End If
        soitemmstoid.Text = gvListSO.SelectedDataKey.Item("soitemmstoid").ToString
        soitemno.Text = gvListSO.SelectedDataKey.Item("soitemno").ToString
        groupoid.Text = gvListSO.SelectedDataKey.Item("groupoid").ToString
        groupdesc.Text = gvListSO.SelectedDataKey.Item("groupdesc").ToString
        cProc.SetModalPopUpExtender(btnHideListSO, pnlListSO, mpeListSO, False)
        btnSearchItem_Click(Nothing, Nothing)
    End Sub

    Protected Sub lbCloseListSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListSO.Click
        cProc.SetModalPopUpExtender(btnHideListSO, pnlListSO, mpeListSO, False)
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchItem.Click
        If cbAutoGenerate.Checked = False Then
            If soitemmstoid.Text = "" Then
                showMessage("Please select SO No. first!", 2)
                Exit Sub
            End If
            BindItemData()
        Else
            BindItemData2()
        End If
        FilterDDLListItem.SelectedIndex = -1 : FilterTextListItem.Text = "" : gvListItem.SelectedIndex = -1
        cProc.SetModalPopUpExtender(btnHideListItem, pnlListItem, mpeListItem, True)
    End Sub

    Protected Sub btnClearItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearItem.Click
        soitemdtloid.Text = "" : itemshortdesc.Text = "" : itemoid.Text = "" : itemcode.Text = "" : itemlimitqty.Text = "" : wodtl1unitoid.SelectedIndex = -1 : soitemqty.Text = "" : wodtl1area.Text = ""

        wodtl1totalmatamt.Text = ""
        wodtl1dlcamt.Text = ""
        wodtl1ohdamt.Text = ""
        curroid_ohd.Text = ""
        wodtl1ohdamtidr.Text = ""
        wodtl1marginamt.Text = ""
        wodtl1precostamt.Text = ""
        curroid_precost.Text = ""
        wodtl1precostamtidr.Text = ""

        lastupduser_precost.Text = ""
        lastupdtime_precost.Text = ""
        lastupduser_bom.Text = ""
        lastupdtime_bom.Text = ""
        lastupduser_dlc.Text = ""
        lastupdtime_dlc.Text = ""
    End Sub

    Protected Sub btnFindListItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListItem.Click
        If cbAutoGenerate.Checked Then
            BindItemData2()
        Else
            BindItemData()
        End If
        mpeListItem.Show()
    End Sub

    Protected Sub btnAllListItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListItem.Click
        FilterDDLListItem.SelectedIndex = -1 : FilterTextListItem.Text = "" : gvListItem.SelectedIndex = -1
        If cbAutoGenerate.Checked Then
            BindItemData2()
        Else
            BindItemData()
        End If
        mpeListItem.Show()
    End Sub

    Protected Sub gvListItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListItem.PageIndexChanging
        If UpdateCheckedMat() Then
            gvListItem.PageIndex = e.NewPageIndex
            gvListItem.DataSource = Session("TblMat")
            gvListItem.DataBind()
        End If
        mpeListItem.Show()
    End Sub

    Protected Sub gvListItem_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListItem.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 2)
        End If
    End Sub

    Protected Sub gvListItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListItem.SelectedIndexChanged
        If itemoid.Text <> gvListItem.SelectedDataKey.Item("itemoid").ToString Then
            btnClearItem_Click(Nothing, Nothing)
        End If
        If gvListItem.SelectedDataKey("formula").ToString().Trim = "No BOM" Then
            Session("SavedInfo") = "Warning BOM"
            showMessage("Please create formula (BOM) for this item first !", 2)
            Exit Sub
        End If
        soitemdtloid.Text = gvListItem.SelectedDataKey("soitemdtloid").ToString().Trim
        itemoid.Text = gvListItem.SelectedDataKey("itemoid").ToString().Trim
        itemcode.Text = gvListItem.SelectedDataKey("itemcode").ToString().Trim
        itemshortdesc.Text = gvListItem.SelectedDataKey("itemlongdesc").ToString().Trim
        wodtl1unitoid.SelectedValue = gvListItem.SelectedDataKey("soitemunitoid").ToString
        itemlimitqty.Text = ToMaskEdit(ToDouble(gvListItem.SelectedDataKey("itemlimitqty").ToString), 4)
        soitemqty.Text = ToMaskEdit(ToDouble(gvListItem.SelectedDataKey("soitemqty").ToString), 4)
        wodtl1qty.Text = ToMaskEdit(ToDouble(gvListItem.SelectedDataKey("soitemqty").ToString), 4)
        bomoid.Text = gvListItem.SelectedDataKey.Item("bomoid").ToString
        wodtl1area.Text = ToMaskEdit(ToDouble(gvListItem.SelectedDataKey("itemres2").ToString), 4)

        wodtl1totalmatamt.Text = ToDouble(gvListItem.SelectedDataKey("precosttotalmatamt").ToString)
        wodtl1dlcamt.Text = ToDouble(gvListItem.SelectedDataKey("precostdlcamt").ToString)
        wodtl1ohdamt.Text = ToDouble(gvListItem.SelectedDataKey("precostoverheadamt").ToString)
        curroid_ohd.Text = ToInteger(gvListItem.SelectedDataKey("curroid_overhead").ToString)
        wodtl1ohdamtidr.Text = ToDouble(gvListItem.SelectedDataKey("precostoverheadamtidr").ToString)
        wodtl1marginamt.Text = ToDouble(gvListItem.SelectedDataKey("precostmarginamt").ToString)
        wodtl1precostamt.Text = ToDouble(gvListItem.SelectedDataKey("precostsalesprice").ToString)
        curroid_precost.Text = ToInteger(gvListItem.SelectedDataKey("curroid_salesprice").ToString)
        wodtl1precostamtidr.Text = ToDouble(gvListItem.SelectedDataKey("precostsalespriceidr").ToString)

        lastupduser_precost.Text = gvListItem.SelectedDataKey.Item("lastupduser_precost").ToString
        lastupdtime_precost.Text = gvListItem.SelectedDataKey.Item("lastupdtime_precost").ToString
        lastupduser_bom.Text = gvListItem.SelectedDataKey.Item("lastupduser_bom").ToString
        lastupdtime_bom.Text = gvListItem.SelectedDataKey.Item("lastupdtime_bom").ToString
        lastupduser_dlc.Text = gvListItem.SelectedDataKey.Item("lastupduser_dlc").ToString
        lastupdtime_dlc.Text = gvListItem.SelectedDataKey.Item("lastupdtime_dlc").ToString

        cProc.SetModalPopUpExtender(btnHideListItem, pnlListItem, mpeListItem, False)
    End Sub

    Protected Sub lkbCloseListItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListItem.Click
        cProc.SetModalPopUpExtender(btnHideListItem, pnlListItem, mpeListItem, False)
    End Sub

    Protected Sub btnAddToList1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList1.Click
        If IsDetailItemInputValid() Then
            If Session("TblDtlFG") Is Nothing Then
                CreateTblDetailItem()
            Else
                'If i_u2.Text = "New Detail" Then
                '    showMessage("Only 1 detail finish good available for this transaction!", 2)
                '    Exit Sub
                'End If
            End If
            Dim objTable As DataTable
            objTable = Session("TblDtlFG")
            Dim dv As DataView = objTable.DefaultView
            If i_u2.Text = "New Detail" Then
                dv.RowFilter = "soitemmstoid=" & soitemmstoid.Text & " AND itemoid=" & itemoid.Text & " AND bomoid=" & bomoid.Text
            Else
                dv.RowFilter = "soitemmstoid=" & soitemmstoid.Text & " AND itemoid=" & itemoid.Text & " AND bomoid=" & bomoid.Text & " AND wodtl1seq<>" & wodtl1seq.Text
            End If
            If dv.Count > 0 Then
                showMessage("This Data has been added before!", 2)
                dv.RowFilter = ""
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                wodtl1seq.Text = objTable.Rows.Count + 1
                objRow("wodtl1seq") = objTable.Rows.Count + 1
            Else
                objRow = objTable.Rows(wodtl1seq.Text - 1)
                objRow.BeginEdit()
            End If
            objRow("soitemmstoid") = soitemmstoid.Text
            objRow("soitemno") = soitemno.Text
            objRow("groupoid") = 0
            objRow("groupdesc") = groupdesc.Text
            objRow("soitemdtloid") = soitemdtloid.Text
            objRow("itemoid") = itemoid.Text
            objRow("itemshortdesc") = itemshortdesc.Text
            objRow("itemcode") = itemcode.Text
            objRow("itemlimitqty") = ToDouble(itemlimitqty.Text)
            objRow("soitemqty") = ToDouble(soitemqty.Text)
            objRow("wodtl1qty") = ToDouble(wodtl1qty.Text)
            objRow("wodtl1qty_soqty") = ToDouble(soitemqty.Text)
            objRow("wodtl1unitoid") = wodtl1unitoid.SelectedValue
            objRow("wodtl1unit") = wodtl1unitoid.SelectedItem.Text
            objRow("bomoid") = bomoid.Text
            objRow("wodtl1note") = wodtl1note.Text
            objRow("wodtl1area") = 0 'ToDouble(wodtl1area.Text)
            objRow("wodtl1totalmatamt") = ToDouble(wodtl1totalmatamt.Text)
            objRow("wodtl1dlcamt") = ToDouble(wodtl1dlcamt.Text)
            objRow("wodtl1ohdamt") = ToDouble(wodtl1ohdamt.Text)
            objRow("curroid_ohd") = ToInteger(curroid_ohd.Text)
            objRow("wodtl1ohdamtidr") = ToDouble(wodtl1ohdamtidr.Text)
            objRow("wodtl1marginamt") = ToDouble(wodtl1marginamt.Text)
            objRow("wodtl1precostamt") = ToDouble(wodtl1precostamt.Text)
            objRow("curroid_precost") = ToInteger(curroid_precost.Text)
            objRow("wodtl1precostamtidr") = ToDouble(wodtl1precostamtidr.Text)

            If i_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
                ' GenerateDetailFromBOM()
            Else
                objRow.EndEdit()
                'UpdateDetailProcess()
                'UpdateDetailMatLevel5(ToDouble(wodtl1qty.Text))
                'UpdateDetailMatLevel3(ToDouble(wodtl1qty.Text))
            End If
            Session("TblDtlFG") = objTable
            GVDtl1.DataSource = objTable
            GVDtl1.DataBind()
            'GenerateDetailFromBOM()
            ClearDetailFinishGood()
            GetTotalQty() : getTotalCalculaion()
            'If Not btnUpdate.Visible Then
            '    UpdateDetailProcess()
            '    UpdateDetailMatLevel5(ToDouble(wodtl2qty.Text))
            'End If
            UpdateDetailProcess()
            UpdateDetailMatLevel5(ToDouble(wodtl2qty.Text))
        End If
    End Sub

    Protected Sub btnClear1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear1.Click
        ClearDetailFinishGood()
    End Sub

    Protected Sub GVDtl1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVDtl1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
        End If
    End Sub

    Protected Sub GVDtl1_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVDtl1.RowDeleting
        Dim objTable As DataTable = Session("TblDtlFG")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        'DeleteDetailProcess(CInt(objRow(e.RowIndex)("wodtl1seq").ToString))
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            UpdateSeqDetailProcess(CInt(objTable.Rows(C1)("wodtl1seq").ToString), C1 + 1)
            dr.BeginEdit()
            dr("wodtl1seq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtlFG") = objTable
        GVDtl1.DataSource = Session("TblDtlFG")
        GVDtl1.DataBind()
        ClearDetailFinishGood()
        GetTotalQty()
    End Sub

    Protected Sub GVDtl1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDtl1.SelectedIndexChanged
        Try
            wodtl1seq.Text = GVDtl1.SelectedDataKey.Item("wodtl1seq").ToString
            If Session("TblDtlFG") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtlFG")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "wodtl1seq=" & wodtl1seq.Text
                soitemmstoid.Text = dv.Item(0).Item("soitemmstoid").ToString
                soitemno.Text = dv.Item(0).Item("soitemno").ToString
                groupoid.Text = dv.Item(0).Item("groupoid").ToString
                groupdesc.Text = dv.Item(0).Item("groupdesc").ToString
                soitemdtloid.Text = dv.Item(0).Item("soitemdtloid").ToString
                itemoid.Text = dv.Item(0).Item("itemoid").ToString
                itemlimitqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("itemlimitqty").ToString), 4)
                itemshortdesc.Text = dv.Item(0).Item("itemshortdesc").ToString
                itemcode.Text = dv.Item(0).Item("itemcode").ToString
                soitemqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("soitemqty").ToString), 4)
                wodtl1qty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("wodtl1qty").ToString), 4)
                wodtl1area.Text = 0
                wodtl1unitoid.SelectedValue = dv.Item(0).Item("wodtl1unitoid").ToString
                bomoid.Text = dv.Item(0).Item("bomoid").ToString
                wodtl1note.Text = dv.Item(0).Item("wodtl1note").ToString

                wodtl1totalmatamt.Text = ToDouble(dv.Item(0).Item("wodtl1totalmatamt").ToString)
                wodtl1dlcamt.Text = ToDouble(dv.Item(0).Item("wodtl1dlcamt").ToString)
                wodtl1ohdamt.Text = ToDouble(dv.Item(0).Item("wodtl1ohdamt").ToString)
                curroid_ohd.Text = ToInteger(dv.Item(0).Item("curroid_ohd").ToString)
                wodtl1ohdamtidr.Text = ToDouble(dv.Item(0).Item("wodtl1ohdamtidr").ToString)
                wodtl1marginamt.Text = ToDouble(dv.Item(0).Item("wodtl1marginamt").ToString)
                wodtl1precostamt.Text = ToDouble(dv.Item(0).Item("wodtl1precostamt").ToString)
                curroid_precost.Text = ToInteger(dv.Item(0).Item("curroid_precost").ToString)
                wodtl1precostamtidr.Text = ToDouble(dv.Item(0).Item("wodtl1precostamtidr").ToString)

                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub btnAddToList2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList2.Click
        If IsDetailProcessInputValid() Then
            If Session("TblDtlProcess") Is Nothing Then
                CreateTblDetailProcess()
            End If
            Dim objTable As DataTable
            objTable = Session("TblDtlProcess")
            Dim dv As DataView = objTable.DefaultView
            If i_u2.Text = "New Detail" Then
                dv.RowFilter = "deptoid=" & deptoid.SelectedValue
            Else
                dv.RowFilter = "deptoid=" & deptoid.SelectedValue & " AND wodtl2procseq<>" & wodtl2seq.Text
            End If
            If dv.Count > 0 Then
                showMessage("This Data has been added before!", 2)
                dv.RowFilter = ""
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                wodtl2seq.Text = objTable.Rows.Count + 1
                objRow("wodtl2procseq") = objTable.Rows.Count + 1
            Else
                objRow = objTable.Rows(wodtl2seq.Text - 1)
                objRow.BeginEdit()
            End If
            Dim str As String = deptoid.SelectedItem.Text & " - " & IIf(wodtl2type.SelectedValue = "FG", "END", todeptoid.SelectedItem.Text)
            objRow("wodtl2desc") = str
            objRow("deptoid") = deptoid.SelectedValue
            objRow("deptname") = deptoid.SelectedItem.Text
            objRow("todeptoid") = todeptoid.SelectedValue
            objRow("wodtl2fgcode") = wodtl2code.Text
            objRow("wodtl2qty") = ToDouble(wodtl2qty.Text)
            objRow("wodtl2unitoid") = wodtl2unitoid.SelectedValue
            objRow("wodtl2unit") = wodtl2unitoid.SelectedItem.Text
            objRow("wodtl2note") = ""
            If wodtl2type.SelectedValue = "FG" Then
                objRow("todeptname") = "END"
            Else
                objRow("todeptname") = todeptoid.SelectedItem.Text
            End If
            objRow("wodtl2type") = wodtl2type.SelectedValue
            objRow("wodtl2note") = wodtl2note.Text
            If i_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
                DDLProcess.Items.Add(str)
                DDLProcess.Items.Item(DDLProcess.Items.Count - 1).Value = wodtl2seq.Text
            Else
                objRow.EndEdit()
                DDLProcess.Items.Item(CInt(wodtl2seq.Text) - 1).Text = str
                'UpdateDetailMat(str)
            End If
            ClearDetailProcess()
            UpdateDetailMatLevel5(ToDouble(wodtl2qty.Text), "2")
            Session("TblDtlProcess") = objTable
            GVDtl2.DataSource = Session("TblDtlProcess")
            GVDtl2.DataBind()
            DDLProcess.Items.Add(str)
            DDLProcess.Items.Item(DDLProcess.Items.Count - 1).Value = 1
        End If
    End Sub

    Protected Sub btnClear2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear2.Click
        ClearDetailProcess()
    End Sub

    Protected Sub GVDtl2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVDtl2.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
        End If
    End Sub

    Protected Sub GVDtl2_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVDtl2.RowDeleting
        Dim objTable As DataTable = Session("TblDtlProcess")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        DeleteDetailMatLevel5(CInt(objRow(e.RowIndex)("wodtl2seq").ToString))
        DeleteDetailMatLevel3(CInt(objRow(e.RowIndex)("wodtl2seq").ToString))
        objTable.Rows.Remove(objRow(e.RowIndex))
        DDLProcess.Items.RemoveAt(e.RowIndex)
        DDLProcess2.Items.RemoveAt(e.RowIndex)
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            DDLProcess.Items(C1).Value = C1 + 1
            DDLProcess2.Items(C1).Value = C1 + 1
            UpdateSeqDetailMatLevel5(CInt(objTable.Rows(C1)("wodtl2seq").ToString), C1 + 1)
            UpdateSeqDetailMatLevel3(CInt(objTable.Rows(C1)("wodtl2seq").ToString), C1 + 1)
            dr.BeginEdit()
            dr("wodtl2seq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtlProcess") = objTable
        GVDtl2.DataSource = Session("TblDtlProcess")
        GVDtl2.DataBind()
        ClearDetailProcess()
    End Sub

    Protected Sub GVDtl2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDtl2.SelectedIndexChanged
        Try
            wodtl2seq.Text = GVDtl2.SelectedDataKey.Item("wodtl2seq").ToString
            If Session("TblDtlProcess") Is Nothing = False Then
                i_u4.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtlProcess")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "wodtl2seq=" & wodtl2seq.Text
                wodtl2procseq.Text = dv.Item(0).Item("wodtl2procseq").ToString
                wodtl2type.SelectedValue = dv.Item(0).Item("wodtl2type").ToString
                deptoid.SelectedValue = dv.Item(0).Item("deptoid").ToString
                todeptoid.SelectedValue = dv.Item(0).Item("todeptoid").ToString
                If wodtl2type.SelectedValue = "FG" Then
                    lbltodept.Visible = False : septtodept.Visible = False : todeptoid.Visible = False
                Else
                    lbltodept.Visible = True : septtodept.Visible = True : todeptoid.Visible = True
                End If
                wodtl2qty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("wodtl2qty").ToString), 4)
                processlimitqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("processlimitqty").ToString), 4)
                wodtl2unitoid.SelectedValue = dv.Item(0).Item("wodtl2unitoid").ToString
                wodtl2note.Text = dv.Item(0).Item("wodtl2note").ToString
                wodtl2desc.Text = dv.Item(0).Item("wodtl2desc").ToString
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub btnClear3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear3.Click
        ClearDetailMaterialLevel5()
    End Sub

    Protected Sub GVDtl3_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVDtl3.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 4)
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 4)
            e.Row.Cells(11).Text = ToMaskEdit(ToDouble(e.Row.Cells(11).Text), 4)
            e.Row.Cells(12).Text = ToMaskEdit(ToDouble(e.Row.Cells(12).Text), 4)
        End If
    End Sub

    Protected Sub GVDtl3_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVDtl3.RowDeleting
        Dim objTable As DataTable = Session("TblDtlMatLev5")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("wodtl3seq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtlMatLev5") = objTable
        GVDtl3.DataSource = Session("TblDtlMatLev5")
        GVDtl3.DataBind()
        ClearDetailMaterialLevel5()
        getTotalCalculaion()
    End Sub

    Protected Sub GVDtl3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDtl3.SelectedIndexChanged
        Try
            wodtl3seq.Text = GVDtl3.SelectedDataKey.Item("wodtl3seq").ToString
            If Session("TblDtlMatLev5") Is Nothing = False Then
                i_u3.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtlMatLev5")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "wodtl3seq=" & wodtl3seq.Text
                'DDLProcess.SelectedValue = dv.Item(0).Item("wodtl2seq").ToString
                'DDLbom.SelectedValue = dv.Item(0).Item("wodtl1seq").ToString
                wodtl3reftype.SelectedValue = dv.Item(0).Item("wodtl3reftype").ToString
                wodtl3refoid.Text = dv.Item(0).Item("wodtl3refoid").ToString
                wodtl3refshortdesc.Text = dv.Item(0).Item("wodtl3refshortdesc").ToString
                wodtl3refcode.Text = dv.Item(0).Item("wodtl3refcode").ToString
                wodtl3qty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("wodtl3qty").ToString) / ToDouble(wodtl2qty.Text), 2)
                wodtl3unitoid.SelectedValue = dv.Item(0).Item("wodtl3unitoid").ToString
                wodtl3note.Text = dv.Item(0).Item("wodtl3note").ToString
                wodtl3qtyref.Text = ToDouble(dv.Item(0).Item("wodtl3qtyref").ToString)
                itemcat1.Text = dv.Item(0).Item("cat1").ToString
                itemcat2.Text = dv.Item(0).Item("cat2").ToString
                itemcat3.Text = dv.Item(0).Item("cat3").ToString
                bomdtl2oid.Text = dv.Item(0).Item("bomdtl2oid").ToString
                wodtl3unitoid.CssClass = "inpTextDisabled" : wodtl3unitoid.Enabled = False
                txtRefValue.Text = ToDouble(dv.Item(0).Item("wodtl3value").ToString)
                txtRefAmt.Text = ToDouble(dv.Item(0).Item("wodtl3amt").ToString)
                wodtl3oid.Text = ToDouble(dv.Item(0).Item("wodtl3oid").ToString)
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub btnClear4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear4.Click
        ClearDetailMaterialLevel3()
    End Sub

    Protected Sub GVDtl4_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVDtl4.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
        End If
    End Sub

    Protected Sub GVDtl4_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDtl4.SelectedIndexChanged
        Try
            wodtl4seq.Text = GVDtl4.SelectedDataKey.Item("wodtl4seq").ToString
            If Session("TblDtlMatLev3") Is Nothing = False Then
                i_u5.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtlMatLev3")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "wodtl4seq=" & wodtl4seq.Text
                DDLProcess2.SelectedValue = dv.Item(0).Item("wodtl2seq").ToString
                wodtl4reftype.SelectedValue = dv.Item(0).Item("wodtl4reftype").ToString
                wodtl4refoid.Text = dv.Item(0).Item("wodtl4refoid").ToString
                wodtl4refshortdesc.Text = dv.Item(0).Item("wodtl4refshortdesc").ToString
                wodtl4refcode.Text = dv.Item(0).Item("wodtl4refcode").ToString
                wodtl4qty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("wodtl4qty").ToString), 4)
                wodtl4unitoid.SelectedValue = dv.Item(0).Item("wodtl4unitoid").ToString
                wodtl4note.Text = dv.Item(0).Item("wodtl4note").ToString
                wodtl4qtyref.Text = ToDouble(dv.Item(0).Item("wodtl4qtyref").ToString)
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            wodtl1oid.Text = GenerateID("QL_TRNWODTL1", CompnyCode)
            wodtl2oid.Text = GenerateID("QL_TRNWODTL2", CompnyCode)
            wodtl3oid.Text = GenerateID("QL_TRNWODTL3", CompnyCode)
            wodtl4oid.Text = GenerateID("QL_TRNWODTL4", CompnyCode)
            Dim dTotalDLC As Double = 0, dTotalFOH As Double = 0
            If Session("TblDtlProcess") IsNot Nothing Then
                Dim dtTmp As DataTable = Session("TblDtlProcess")
                For C1 As Integer = 0 To dtTmp.Rows.Count - 1
                    dTotalDLC += ToDouble(dtTmp.Rows(C1)("wodtl2dlcvalue").ToString) * ToDouble(dtTmp.Rows(C1)("wodtl2qty").ToString)
                    dTotalFOH += ToDouble(dtTmp.Rows(C1)("wodtl2fohvalue").ToString) * ToDouble(dtTmp.Rows(C1)("wodtl2qty").ToString)
                Next
            End If
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_trnwomst WHERE womstoid=" & womstoid.Text
                If CheckDataExists(sSql) Then
                    womstoid.Text = GenerateID("QL_TRNWOMST", CompnyCode)
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_trnwomst", "womstoid", womstoid.Text, "womststatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    womststatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            PeriodAcctg.Text = GetDateToPeriodAcctg(CDate(wodate.Text))
            Dim dBalanceQty As Double = 0
            Dim dtlTbl As DataTable = Session("TblDtlFG")
            Dim sNoSO As String = ""
            Dim iSoMstOid, iSoDtlOid As Integer
            If womststatus.Text = "Post" Then
                GenerateWONo()
                dBalanceQty = ToDouble(dtlTbl.Compute("SUM(wodtl1qty)", ""))
                If cbAutoGenerate.Checked Then
                    sNoSO = GenerateSOFGNo()
                    iSoDtlOid = GenerateID("QL_TRNSODTL", CompnyCode)
                    iSoMstOid = GenerateID("QL_TRNSOMST", CompnyCode)
                End If
            End If
            '=========Get code dan deskripsi
            Dim womstres1 As String = ""
            Dim womstres2 As String = ""
            Dim womstres3 As String = ""
            If (Session("TblDtlFG") IsNot Nothing) Then
                Dim dtlTable As DataTable = Session("TblDtlFG")
                For x As Int64 = 0 To dtlTable.Rows.Count - 1
                    If womstres1.Contains(dtlTable.Rows(x).Item("itemcode").ToString) = False Then
                        womstres1 = womstres1 & "," & dtlTable.Rows(x).Item("itemcode").ToString
                    End If
                    If womstres2.Contains(dtlTable.Rows(x).Item("itemshortdesc").ToString) = False Then
                        womstres2 = womstres2 & ", " & dtlTable.Rows(x).Item("itemshortdesc").ToString
                    End If
                    If womstres3.Contains(dtlTable.Rows(x).Item("soitemno").ToString) = False Then
                        womstres3 = womstres3 & "," & dtlTable.Rows(x).Item("soitemno").ToString
                    End If
                Next
            End If
            If womstres1.Trim <> "" Then
                womstres1 = womstres1.Remove(0, 1)
            End If
            If womstres2.Trim <> "" Then
                womstres2 = womstres2.Remove(0, 1)
            End If
            If womstres3.Trim <> "" Then
                womstres3 = womstres3.Remove(0, 1)
            End If
            '=======================================

            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trnwomst (cmpcode, womstoid, periodacctg, wodate, wono, wodocrefno, womstnote, womststatus, womstres1, womstres2, womstres3, createuser, createtime, upduser, updtime, wototaldlc, wototalfoh, balanceqty_cogm, autogenerate, dlcamount, ohdamount, suppoid, wotempstatus_tmp, wostartdate, wofinishdate, wotolerance, kancingamt, kainamt, labelamt, othervalue, kancingamt_use_ap, kainamt_use_ap, labelamt_use_ap, othervalue_use_ap, ohdamount_use_ap, bordiramt, suppoid_bordir, jumlah_kancing, harga_kancing, harga_jual, lastplanmstoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & womstoid.Text & ", '" & PeriodAcctg.Text & "', '" & wodate.Text & "', '" & wono.Text & "', '" & Tchar(wodocrefno.Text) & "', '" & Tchar(womstnote.Text.Trim) & "', '" & womststatus.Text & "', '" & Tchar(womstres1) & "', '" & Tchar(womstres2) & "', '" & Tchar(womstres3) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalDLC & ", " & dTotalFOH & ", " & dBalanceQty & ", '" & IIf(cbAutoGenerate.Checked, "YES", "NO") & "', " & ToDouble(dlcamt.Text) & ", " & ToDouble(ohdamt.Text) & ", " & makloonoid.Text & ", '" & womststatus.Text & "', '" & startdate.Text & "', '" & finishdate.Text & "', " & ToDouble(wotolerance.Text) & ", " & ToDouble(kancingamt.Text) & ", " & ToDouble(kainamt.Text) & ", " & ToDouble(labelamt.Text) & ", " & ToDouble(othervalue.Text) & ", '" & IIf(cbKancing_use_ap.Checked, "Y", "N") & "', '" & IIf(cbKain_use_ap.Checked, "Y", "N") & "', '" & IIf(cbLabel_use_ap.Checked, "Y", "N") & "', '" & IIf(cbOther_use_ap.Checked, "Y", "N") & "', '" & IIf(cbBordir_use_ap.Checked, "Y", "N") & "', " & ToDouble(bordiramt.Text) & ", " & ToInteger(suppoid_bordir.Text) & ", " & ToDouble(txtJumlahKancing.Text) & ", " & ToDouble(txtRefPriceKancing.Text) & ", " & ToDouble(txtHargaJualFinal.Text) & ", " & ToInteger(ddlNoTransfer.SelectedValue) & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & womstoid.Text & " WHERE tablename='QL_TRNWOMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trnwomst SET periodacctg='" & PeriodAcctg.Text & "', wodate='" & wodate.Text & "', wono='" & wono.Text & "', wodocrefno='" & Tchar(wodocrefno.Text) & "', womstnote='" & Tchar(womstnote.Text.Trim) & "', womststatus='" & womststatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP,womstres1='" & Tchar(womstres1) & "',womstres2='" & Tchar(womstres2) & "',womstres3='" & Tchar(womstres3) & "', balanceqty_cogm=" & dBalanceQty & ", autogenerate='" & IIf(cbAutoGenerate.Checked, "YES", "NO") & "', dlcamount=" & ToDouble(dlcamt.Text) & ", ohdamount=" & ToDouble(ohdamt.Text) & ", suppoid=" & makloonoid.Text & ", wotempstatus_tmp='" & womststatus.Text & "', wostartdate='" & startdate.Text & "', wofinishdate='" & finishdate.Text & "', wotolerance=" & ToDouble(wotolerance.Text) & ", kancingamt=" & ToDouble(kancingamt.Text) & ", kainamt=" & ToDouble(kainamt.Text) & ", labelamt=" & ToDouble(labelamt.Text) & ", othervalue=" & ToDouble(othervalue.Text) & ", kancingamt_use_ap='" & IIf(cbKancing_use_ap.Checked, "Y", "N") & "', kainamt_use_ap='" & IIf(cbKain_use_ap.Checked, "Y", "N") & "', labelamt_use_ap='" & IIf(cbLabel_use_ap.Checked, "Y", "N") & "', othervalue_use_ap='" & IIf(cbOther_use_ap.Checked, "Y", "N") & "', ohdamount_use_ap='" & IIf(cbBordir_use_ap.Checked, "Y", "N") & "', bordiramt=" & ToDouble(bordiramt.Text) & ", suppoid_bordir=" & ToInteger(suppoid_bordir.Text) & ", jumlah_kancing=" & ToDouble(txtJumlahKancing.Text) & ", harga_kancing=" & ToDouble(txtRefPriceKancing.Text) & ", harga_jual=" & ToDouble(txtHargaJualFinal.Text) & ", lastplanmstoid=" & ToInteger(ddlNoTransfer.SelectedValue) & " WHERE womstoid=" & womstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnsodtl SET sodtlres3='' WHERE sodtloid IN (SELECT DISTINCT sodtloid FROM QL_trnwodtl1 WHERE womstoid=" & womstoid.Text & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnsomst SET somstres3='' WHERE somstoid IN (SELECT DISTINCT somstoid FROM QL_trnwodtl1 WHERE womstoid=" & womstoid.Text & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trnwodtl4 WHERE womstoid=" & womstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trnwodtl3 WHERE womstoid=" & womstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trnwodtl2 WHERE womstoid=" & womstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trnwodtl1 WHERE womstoid=" & womstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                'If Autogenerate Post Insert Into SO Type Buffer Stock
                If womststatus.Text = "Post" Then
                    If cbAutoGenerate.Checked Then
                        sSql = "INSERT INTO QL_trnsomst (cmpcode, somstoid, periodacctg, sotype, sono, sodate,somstnote, somststatus, createuser, createtime, upduser, updtime, custoid, approvalpic) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iSoMstOid & ", '" & PeriodAcctg.Text & "', 'Buffer', '" & sNoSO & "', '" & wodate.Text & "', '" & Tchar(womstnote.Text.Trim) & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP,0, 0)"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()

                        sSql = "UPDATE QL_trnwomst SET womstres3='" & sNoSO & "' WHERE womstoid=" & womstoid.Text
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()

                        sSql = "UPDATE QL_mstoid SET lastoid=" & iSoMstOid & " WHERE tablename='QL_TRNSOMST' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                End If
                If (Session("TblDtlFG") IsNot Nothing) Or (Session("TblDtlProcess") IsNot Nothing) Or (Session("TblDtlMatLev5") IsNot Nothing) Or (Session("TblDtlMatLev3") IsNot Nothing) Then
                    Dim dtItem As DataTable = Session("TblDtlFG")
                    Dim dtProc As DataTable = Session("TblDtlProcess")
                    Dim dvProc As DataView = dtProc.DefaultView
                    Dim dtMatLev5 As DataTable = Session("TblDtlMatLev5")
                    Dim dvMatLev5 As DataView = dtMatLev5.DefaultView
                    Dim dtMatLev3 As DataTable = Session("TblDtlMatLev3")
                    Dim dvMatLev3 As DataView = dtMatLev3.DefaultView
                    For C1 As Int16 = 0 To dtItem.Rows.Count - 1
                        sSql = "INSERT INTO QL_trnwodtl1 (cmpcode, wodtl1oid, womstoid, wodtl1seq, soitemmstoid, soitemdtloid, itemoid, wodtl1qty, wodtl1unitoid, bomoid, wodtl1area, wodtl1status, wodtl1note, wodtl1res1, wodtl1res2, wodtl1res3, upduser, updtime, wodtl1totalmatamt, wodtl1dlcamt, wodtl1ohdamt, curroid_ohd, wodtl1ohdamtidr, wodtl1marginamt, wodtl1precostamt, curroid_precost, wodtl1precostamtidr, lastupduser_precost, lastupdtime_precost, lastupduser_bom, lastupdtime_bom, lastupduser_dlc, lastupdtime_dlc, wodtl1qty_soqty) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C1 + CInt(wodtl1oid.Text)) & ", " & womstoid.Text & ", " & C1 + 1 & ", " & dtItem.Rows(C1).Item("soitemmstoid") & ", " & dtItem.Rows(C1).Item("soitemdtloid") & ", " & dtItem.Rows(C1).Item("itemoid") & ", " & ToDouble(dtItem.Rows(C1).Item("wodtl1qty").ToString) & ", " & dtItem.Rows(C1).Item("wodtl1unitoid") & ", " & dtItem.Rows(C1).Item("bomoid") & ", " & ToDouble(dtItem.Rows(C1).Item("wodtl1area").ToString) & ", '', '" & Tchar(dtItem.Rows(C1).Item("wodtl1note").ToString) & "', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dtItem.Rows(C1).Item("wodtl1totalmatamt").ToString) & ", " & ToDouble(dtItem.Rows(C1).Item("wodtl1dlcamt").ToString) & ", " & ToDouble(dtItem.Rows(C1).Item("wodtl1ohdamt").ToString) & ", " & ToInteger(dtItem.Rows(C1).Item("curroid_ohd").ToString) & ", " & ToDouble(dtItem.Rows(C1).Item("wodtl1ohdamtidr").ToString) & ", " & ToDouble(dtItem.Rows(C1).Item("wodtl1marginamt").ToString) & ", " & ToDouble(dtItem.Rows(C1).Item("wodtl1precostamt").ToString) & ", " & ToInteger(dtItem.Rows(C1).Item("curroid_precost").ToString) & ", " & ToDouble(dtItem.Rows(C1).Item("wodtl1precostamtidr").ToString) & ", '" & dtItem.Rows(C1).Item("lastupduser_precost").ToString & "', '" & dtItem.Rows(C1).Item("lastupdtime_precost").ToString & "', '" & dtItem.Rows(C1).Item("lastupduser_bom").ToString & "', '" & dtItem.Rows(C1).Item("lastupdtime_bom").ToString & "', '" & dtItem.Rows(C1).Item("lastupduser_dlc").ToString & "', '" & dtItem.Rows(C1).Item("lastupdtime_dlc").ToString & "', " & ToDouble(dtItem.Rows(C1).Item("wodtl1qty_soqty").ToString) & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        If ToDouble(dtItem.Rows(C1).Item("wodtl1qty_soqty").ToString) >= ToDouble(dtItem.Rows(C1).Item("soitemqty").ToString) Then
                            sSql = "UPDATE QL_trnsodtl SET sodtlres3='Complete' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND sodtloid=" & dtItem.Rows(C1).Item("soitemdtloid").ToString
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_trnsomst SET somstres3='Closed' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND somstoid=" & dtItem.Rows(C1).Item("soitemmstoid") & " AND (SELECT COUNT(sodtloid) FROM QL_trnsodtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND ISNULL(sodtlres3, '')='' AND somstoid=" & dtItem.Rows(C1).Item("soitemmstoid") & " AND sodtloid<>" & dtItem.Rows(C1).Item("soitemdtloid").ToString & ")=0"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                        'Insert Detail SO Buffer
                        If womststatus.Text = "Post" Then
                            If cbAutoGenerate.Checked Then
                                sSql = "INSERT INTO QL_trnsodtl (cmpcode, sodtloid, somstoid, sodtlseq, itemoid, sodtlqty, sodtlunitoid, sodtlnote, sodtlstatus, upduser, updtime, soqty_unitkecil) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iSoDtlOid & ", " & iSoMstOid & ",  " & C1 + 1 & ", " & dtItem.Rows(C1).Item("itemoid") & ", " & ToDouble(dtItem.Rows(C1).Item("wodtl1qty").ToString) & ", " & dtItem.Rows(C1).Item("wodtl1unitoid") & ", '" & Tchar(dtItem.Rows(C1).Item("wodtl1note")) & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0)"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()

                                sSql = "UPDATE QL_trnwodtl1 SET soitemmstoid=" & iSoMstOid & ", soitemdtloid=" & iSoDtlOid & " WHERE wodtl1oid=" & (C1 + CInt(wodtl1oid.Text))
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iSoDtlOid += 1
                            End If
                            'Update Default Penjahit di master Item/Material
                            sSql = "UPDATE QL_mstitem SET suppoid=" & makloonoid.Text & " WHERE itemoid=" & dtItem.Rows(C1).Item("itemoid") & ""
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            If ToDouble(txtHargaJualFinal.Text) > 0 Then
                                sSql = "UPDATE QL_mstitem SET itemPriceList=" & ToDouble(txtHargaJualFinal.Text) & ", backup_itempricelist=itemPriceList WHERE itemoid=" & dtItem.Rows(C1).Item("itemoid") & ""
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                            End If
                        End If

                        'dvProc.RowFilter = "wodtl1seq=" & dtItem.Rows(C1).Item("wodtl1seq").ToString
                        For C2 As Integer = 0 To dvProc.Count - 1
                            sSql = "INSERT INTO QL_trnwodtl2 (cmpcode, wodtl2oid, wodtl1oid, womstoid, wodtl2seq, bomdtl1oid, wodtl2procseq, deptoid, todeptoid, wodtl2desc, wodtl2type, wodtl2qty, wodtl2unitoid, wodtl2status, wodtl2note, wodtl2res1, wodtl2res2, wodtl2res3, upduser, updtime, wodtl2dlcvalue, wodtl2fohvalue) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C2 + CInt(wodtl2oid.Text)) & ", " & (C1 + CInt(wodtl1oid.Text)) & ", " & womstoid.Text & ", " & C2 + 1 & ", 0, " & dvProc(C2).Item("wodtl2procseq").ToString & ", " & dvProc(C2).Item("deptoid").ToString & ", " & dvProc(C2).Item("todeptoid").ToString & ", '" & dvProc(C2).Item("wodtl2desc").ToString & "', '" & dvProc(C2).Item("wodtl2type").ToString & "', " & ToDouble(dvProc(C2).Item("wodtl2qty").ToString) & ", " & dvProc(C2).Item("wodtl2unitoid").ToString & ", '', '" & Tchar(dvProc(C2).Item("wodtl2note").ToString) & "', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dvProc(C2).Item("wodtl2dlcvalue").ToString) & ", " & ToDouble(dvProc(C2).Item("wodtl2fohvalue").ToString) & ")"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            If dvMatLev5.Count > 0 Then
                                'dvMatLev5.RowFilter = "wodtl2seq=" & dvProc(C2).Item("wodtl2seq").ToString
                                For C3 As Integer = 0 To dvMatLev5.Count - 1
                                    sSql = "INSERT INTO QL_trnwodtl3 (cmpcode, wodtl3oid, wodtl2oid, wodtl1oid, womstoid, wodtl3seq, bomdtl2oid, wodtl3reftype, wodtl3refoid, wodtl3qty, wodtl3unitoid, wodtl3status, wodtl3note, wodtl3res1, wodtl3res2, wodtl3res3, upduser, updtime, wodtl3value, wodtl3amt) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C3 + CInt(wodtl3oid.Text)) & ", " & (C2 + CInt(wodtl2oid.Text)) & ", " & (C1 + CInt(wodtl1oid.Text)) & ", " & womstoid.Text & ", " & C3 + 1 & ", 0, '" & dvMatLev5(C3).Item("wodtl3reftype").ToString & "', " & dvMatLev5(C3).Item("wodtl3refoid").ToString & ", " & ToDouble(dvMatLev5(C3).Item("wodtl3qty").ToString) & ", " & dvMatLev5(C3).Item("wodtl3unitoid").ToString & ", '', '" & Tchar(dvMatLev5(C3).Item("wodtl3note").ToString) & "', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dvMatLev5(C3).Item("wodtl3value").ToString) & ", " & ToDouble(dvMatLev5(C3).Item("wodtl3amt").ToString) & ")"
                                    xCmd.CommandText = sSql
                                    xCmd.ExecuteNonQuery()
                                Next
                                wodtl3oid.Text = CInt(wodtl3oid.Text) + dvMatLev5.Count
                                dvMatLev5.RowFilter = ""
                            End If
                            If dvMatLev3.Count > 0 Then
                                dvMatLev3.RowFilter = "wodtl2seq=" & dvProc(C2).Item("wodtl2seq").ToString
                                For C3 As Integer = 0 To dvMatLev3.Count - 1
                                    sSql = "INSERT INTO QL_trnwodtl4 (cmpcode, wodtl4oid, wodtl2oid, wodtl1oid, womstoid, wodtl4seq, bomdtl3oid, wodtl4reftype, wodtl4refoid, wodtl4qty, wodtl4unitoid, wodtl4status, wodtl4note, wodtl4res1, wodtl4res2, wodtl4res3, upduser, updtime, wodtl4value) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C3 + CInt(wodtl4oid.Text)) & ", " & (C2 + CInt(wodtl2oid.Text)) & ", " & (C1 + CInt(wodtl1oid.Text)) & ", " & womstoid.Text & ", " & C3 + 1 & ", " & dvMatLev3(C3).Item("bomdtl3oid").ToString & ", '" & dvMatLev3(C3).Item("wodtl4reftype").ToString & "', " & dvMatLev3(C3).Item("wodtl4refoid").ToString & ", " & ToDouble(dvMatLev3(C3).Item("wodtl4qty").ToString) & ", " & dvMatLev3(C3).Item("wodtl4unitoid").ToString & ", '', '" & Tchar(dvMatLev3(C3).Item("wodtl4note").ToString) & "', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dvMatLev3(C3).Item("wodtl4value").ToString) & ")"
                                    xCmd.CommandText = sSql
                                    xCmd.ExecuteNonQuery()
                                Next
                                wodtl4oid.Text = CInt(wodtl4oid.Text) + dvMatLev3.Count
                                dvMatLev3.RowFilter = ""
                            End If
                        Next
                        wodtl2oid.Text = CInt(wodtl2oid.Text) + dvProc.Count
                        dvProc.RowFilter = ""
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (dtItem.Rows.Count - 1 + CInt(wodtl1oid.Text)) & " WHERE tablename='QL_TRNWODTL1' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & CInt(wodtl2oid.Text) - 1 & " WHERE tablename='QL_TRNWODTL2' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & CInt(wodtl3oid.Text) - 1 & " WHERE tablename='QL_TRNWODTL3' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & CInt(wodtl4oid.Text) - 1 & " WHERE tablename='QL_TRNWODTL4' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    If womststatus.Text = "Post" Then
                        If cbAutoGenerate.Checked Then
                            sSql = "UPDATE QL_mstoid SET lastoid=" & iSoMstOid & " WHERE tablename='QL_TRNSOMST' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_mstoid SET lastoid=" & iSoDtlOid - 1 & " WHERE tablename='QL_TRNSODTL' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                    End If
                End If

                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        womststatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    womststatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.ToString & sSql, 1)
                conn.Close()
                womststatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & womstoid.Text & ".<BR>"
            End If
            If womststatus.Text = "Post" Then
                Session("SavedInfo") &= "Data have been posted with SPK No. = " & wono.Text & "."
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trnWO.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnWO.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If womstoid.Text.Trim = "" Then
            showMessage("Please select Job Costing MO data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_trnwomst", "womstoid", womstoid.Text, "womststatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                womststatus.Text = "In Process"
                Exit Sub
            End If
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trnsodtl SET sodtlres3='' WHERE sodtloid IN (SELECT DISTINCT sodtloid FROM QL_trnwodtl1 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid=" & womstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_trnsomst SET somstres3='' WHERE somstoid IN (SELECT DISTINCT somstoid FROM QL_trnwodtl1 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid=" & womstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnwodtl4 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid=" & womstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnwodtl3 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid=" & womstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnwodtl2 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid=" & womstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnwodtl1 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid=" & womstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnwomst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid=" & womstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnWO.aspx?awal=true")
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPost.Click
        womststatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        UpdateCheckedValue()
        PrintReport()
    End Sub

    Protected Sub lbAddListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddListMat.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "CheckValue='True'"
            Dim iCheck As Integer = dtView.Count
            If iCheck > 0 Then
                If cbAutoGenerate.Checked = False Then
                    dtView.RowFilter = ""
                    dtView.RowFilter = "CheckValue='True' AND soitemqty>0 AND soitemqty<=soitemqty_real"
                    If dtView.Count <> iCheck Then
                        Session("WarningListMat") = "SO qty for every checked material data must be more than 0 and less or equal than SO qty!"
                        showMessage(Session("WarningListMat"), 2)
                        dtView.RowFilter = ""
                        Exit Sub
                    End If
                Else
                    dtView.RowFilter = ""
                    dtView.RowFilter = "CheckValue='True' AND soitemqty>0 "
                    If dtView.Count <> iCheck Then
                        Session("WarningListMat") = "SO qty for every checked material data must be more than 0!"
                        showMessage(Session("WarningListMat"), 2)
                        dtView.RowFilter = ""
                        Exit Sub
                    End If
                End If
                If Session("TblDtlFG") Is Nothing Then
                    CreateTblDetailItem()
                End If
                Dim objTable As DataTable
                objTable = Session("TblDtlFG")
                Dim dv As DataView = objTable.DefaultView
                Dim objRow As DataRow
                Dim counter As Integer = objTable.Rows.Count
                Dim dQty_unitkecil, dQty_unitbesar As Double
                For C1 As Integer = 0 To dtView.Count - 1
                    dv.RowFilter = "soitemdtloid=" & dtView(C1)("soitemdtloid")
                    If dv.Count > 0 Then
                        dv.AllowEdit = True
                        dv(0)("soitemqty") = dtView(C1)("soitemqty")
                    Else
                        GetUnitConverter(dtView(C1)("itemoid"), dtView(C1)("soitemunitoid"), dtView(C1)("soitemqty"), dQty_unitkecil, dQty_unitbesar)
                        counter += 1
                        objRow = objTable.NewRow()
                        objRow("wodtl1seq") = counter
                        objRow("soitemmstoid") = dtView(C1)("soitemmstoid")
                        objRow("soitemno") = dtView(C1)("soitemno")
                        objRow("groupoid") = dtView(C1)("groupoid")
                        objRow("groupdesc") = ""
                        objRow("soitemdtloid") = dtView(C1)("soitemdtloid")
                        objRow("itemoid") = dtView(C1)("itemoid")
                        objRow("itemshortdesc") = dtView(C1)("itemlongdesc")
                        objRow("itemcode") = dtView(C1)("itemcode")
                        objRow("itemlimitqty") = dtView(C1)("itemlimitqty")
                        objRow("soitemqty") = dtView(C1)("soitemqty_real")
                        objRow("wodtl1qty") = dtView(C1)("soitemqty")
                        objRow("wodtl1qty_soqty") = dtView(C1)("soitemqty")
                        objRow("wodtl1unitoid") = dtView(C1)("unitbomoid")
                        objRow("wodtl1unit") = dtView(C1)("unitbom")
                        objRow("bomoid") = dtView(C1)("bomoid")
                        objRow("wodtl1note") = dtView(C1)("sodtlnote")
                        objRow("wodtl1area") = 0
                        objRow("wodtl1totalmatamt") = dtView(C1)("precosttotalmatamt")
                        objRow("wodtl1dlcamt") = dtView(C1)("precostdlcamt")
                        objRow("wodtl1ohdamt") = dtView(C1)("precostoverheadamt")
                        objRow("curroid_ohd") = dtView(C1)("curroid_overhead")
                        objRow("wodtl1ohdamtidr") = dtView(C1)("precostoverheadamtidr")
                        objRow("wodtl1marginamt") = dtView(C1)("precostmarginamt")
                        objRow("wodtl1precostamt") = dtView(C1)("precostsalesprice")
                        objRow("curroid_precost") = dtView(C1)("curroid_salesprice")
                        objRow("wodtl1precostamtidr") = dtView(C1)("precostsalespriceidr")

                        wodtl2unitoid.SelectedValue = dtView(C1)("soitemunitoid")
                        wodtl2code.Text = dtView(C1)("itemcode")
                        objTable.Rows.Add(objRow)
                        DDLbom.Items.Add(dtView(C1)("itemcode") & " - " & dtView(C1)("itemlongdesc"))
                        DDLbom.Items.Item(DDLbom.Items.Count - 1).Value = counter + C1
                    End If
                    dv.RowFilter = ""
                Next
                dtView.RowFilter = ""
                Session("TblDtlFG") = objTable
                GVDtl1.DataSource = objTable
                GVDtl1.DataBind()
                GenerateDetailFromBOM()
                ClearDetailFinishGood()
                GetTotalQty() : getTotalCalculaion()
                txtHargaJualFinal.Text = ToMaskEdit(ToDouble(dtView(0)("itemPriceList")), 2)
                btnAddToList2_Click(Nothing, Nothing)
                cProc.SetModalPopUpExtender(btnHideListItem, pnlListItem, mpeListItem, False)
            Else
                Session("WarningListMat") = "Please select material to add to list!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        End If
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'If DDLbom.SelectedValue = "" Then
        '    showMessage("Please select Finish Good first!", 2)
        '    Exit Sub
        'End If
        If DDLProcess.SelectedValue = "" Then
            showMessage("Please select Process first!", 2)
            Exit Sub
        End If
        If ddlNoTransfer.SelectedValue = "" Then
            showMessage("Pilih nomor transfer terlebih dahulu!", 2)
            Exit Sub
        End If
        'If wodtl3refshortdesc.Text = "" Then
        '    showMessage("Please select Material Data For Subtitution Item !", 2)
        '    Exit Sub
        'End If
        InitDDLCat01ListMat() : cbCat01ListMat.Checked = False : cbCat02ListMat.Checked = False : cbCat03ListMat.Checked = False : cbCat04ListMat.Checked = False
        FilterDDLListMat2.SelectedIndex = -1 : FilterTextListMat2.Text = "" : Session("TblListMat") = Nothing : Session("TblListMatView") = Nothing : gvListMat2.DataSource = Session("TblListMatView") : gvListMat2.DataBind()
        cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, True)
    End Sub

    Protected Sub btnEraseMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDetailMaterialLevel5()
    End Sub

    Protected Sub gvListMat2_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat2.PageIndexChanging
        If Not Session("TblListMat") Is Nothing Then
            UpdateCheckedMatKIK()
            UpdateCheckedMatViewKIK()
            gvListMat2.PageIndex = e.NewPageIndex
            gvListMat2.DataSource = Session("TblListMatView")
            gvListMat2.DataBind()
        End If
        mpeListMat2.Show()
    End Sub

    Protected Sub gvListMat2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat2.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(3).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    End If
                End If
            Next
            Dim str As String = e.Row.Cells(1).Text
            e.Row.Cells(1).Text = Mid(str, 1, 7) & vbCrLf & Mid(str, 8, 10) & vbCrLf & Mid(str, 18, 5)

            Dim iItemOid As Integer = 0
            cc = e.Row.Cells(0).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    iItemOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                End If
            Next

            cc = e.Row.Cells(4).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                    FillDDL(CType(myControl, System.Web.UI.WebControls.DropDownList), "select itemUnit1, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit1 where itemoid=" & iItemOid & " union all select itemUnit2, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit2 where itemoid=" & iItemOid & " union all select itemUnit3, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit3 where itemoid=" & iItemOid & "")
                    CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue = CType(myControl, System.Web.UI.WebControls.DropDownList).ToolTip
                End If
            Next
        End If
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsDetailMatInputValid() Then
            If Session("TblDtlMatLev5") Is Nothing Then
                CreateTblDetailMatLev5()
            End If
            Dim dtMat As DataTable = Session("TblDtlMatLev5")
            Dim dv As DataView = dtMat.DefaultView
            If i_u3.Text = "New Detail" Then
                dv.RowFilter = "wodtl1seq=" & DDLbom.SelectedValue & " AND wodtl2seq=" & DDLProcess.SelectedValue & " AND wodtl3reftype='" & wodtl3reftype.SelectedValue & "' AND wodtl3refoid=" & wodtl3refoid.Text
            Else
                dv.RowFilter = "wodtl1seq=" & DDLbom.SelectedValue & " AND wodtl2seq=" & DDLProcess.SelectedValue & " AND wodtl3reftype='" & wodtl3reftype.SelectedValue & "' AND wodtl3refoid=" & wodtl3refoid.Text & " AND wodtl3seq<>" & wodtl3seq.Text
            End If
            If dv.Count > 0 Then
                showMessage("This Data has been added before!", 2)
                dv.RowFilter = ""
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u3.Text = "New Detail" Then
                objRow = dtMat.NewRow()
                objRow("wodtl3seq") = dtMat.Rows.Count + 1
            Else
                objRow = dtMat.Rows(wodtl3seq.Text - 1)
                objRow.BeginEdit()
            End If
            objRow("wodtl2seq") = DDLProcess.SelectedValue
            objRow("wodtl1seq") = DDLbom.SelectedValue
            objRow("wodtl2desc") = DDLProcess.SelectedItem.Text
            objRow("wodtl3reftype") = wodtl3reftype.SelectedValue
            objRow("wodtl3refoid") = wodtl3refoid.Text
            objRow("bomdtl2oid") = 0
            objRow("wodtl3qty") = Math.Round(ToDouble(wodtl3qty.Text) * ToDouble(wodtl2qty.Text), 2, MidpointRounding.AwayFromZero)
            objRow("wodtl3unitoid") = wodtl3unitoid.SelectedValue
            objRow("wodtl3qtyref") = ToDouble(wodtl3qty.Text)
            objRow("wodtl3note") = wodtl3note.Text
            objRow("wodtl3refcode") = wodtl3refcode.Text
            objRow("wodtl3fgcode") = Left(DDLbom.SelectedItem.Text, 7)
            objRow("wodtl3refshortdesc") = wodtl3refshortdesc.Text
            objRow("wodtl3unit") = wodtl3unitoid.SelectedItem.Text
            objRow("wodtl3value") = ToDouble(txtRefValue.Text)
            objRow("wodtl3amt") = ToDouble(txtRefValue.Text) * ToDouble(objRow("wodtl3qty"))
            objRow("wodtl3oid") = ToInteger(wodtl3oid.Text)
            If i_u3.Text = "New Detail" Then
                dtMat.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            Session("TblDtlMatLev5") = dtMat
            GVDtl3.DataSource = dtMat
            GVDtl3.DataBind()
            ClearDetailMaterialLevel5()
            getTotalCalculaion()
        End If
    End Sub

    Protected Sub gvListMat2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        wodtl3refshortdesc.Text = gvListMat2.SelectedDataKey.Item("matrefshortdesc").ToString
        wodtl3refoid.Text = gvListMat2.SelectedDataKey.Item("matrefoid").ToString
        wodtl3refcode.Text = gvListMat2.SelectedDataKey.Item("matrefcode").ToString
        bomdtl2oid.Text = gvListMat2.SelectedDataKey.Item("bomdtl2oid").ToString
        FillDDL(wodtl3unitoid, "select itemUnit1, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit1 where itemoid=" & wodtl3refoid.Text & " union all select itemUnit2, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit2 where itemoid=" & wodtl3refoid.Text & " union all select itemUnit3, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit3 where itemoid=" & wodtl3refoid.Text & "")
        cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, False)
    End Sub

    Protected Sub lkbAddToList_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Session("TblListMat") Is Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                UpdateCheckedMatKIK()
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "checkvalue='True'"
                If dv.Count = 0 Then
                    Session("ErrMat") = "Please select Material data!"
                    showMessage(Session("ErrMat"), 2)
                    dv.RowFilter = ""
                    Exit Sub
                End If
                dv.RowFilter = ""
                dv.RowFilter = "checkvalue='True' AND bomdtl2qty=0"
                If dv.Count > 0 Then
                    Session("ErrMat") = "BOM Qty for every selected Material must be more than 0!"
                    showMessage(Session("ErrMat"), 2)
                    dv.RowFilter = ""
                    Exit Sub
                End If
                dv.RowFilter = ""
                If Session("TblDtlMatLev5") Is Nothing Then
                    CreateTblDetailMatLev5()
                End If
                Dim dtMat As DataTable = Session("TblDtlMatLev5")
                Dim dvMat As DataView = dtMat.DefaultView
                Dim iSeq As Integer = dvMat.Count + 1
                dvMat.AllowEdit = True
                dvMat.AllowNew = True
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    dvMat.RowFilter = "wodtl2seq=" & DDLProcess.SelectedValue & " AND wodtl3reftype='" & wodtl3reftype.SelectedValue & "' AND wodtl3refoid=" & dv(C1)("matrefoid").ToString
                    If dvMat.Count > 0 Then
                        dvMat(0)("wodtl3qty") = ToDouble(dv(C1)("wodtl3qty").ToString)
                        dvMat(0)("wodtl3note") = dv(C1)("wodtl3note").ToString
                    Else
                        Dim rv As DataRowView = dvMat.AddNew()
                        rv.BeginEdit()
                        rv("wodtl3seq") = iSeq
                        rv("wodtl3reftype") = wodtl3reftype.SelectedValue
                        rv("wodtl3refoid") = dv(C1)("matrefoid").ToString
                        rv("wodtl3qty") = Math.Round(ToDouble(dv(C1)("bomdtl2qty").ToString) * ToDouble(wodtl2qty.Text), 2, MidpointRounding.AwayFromZero)
                        rv("wodtl3unitoid") = dv(C1)("matrefunitoid").ToString
                        rv("wodtl3note") = dv(C1)("bomdtl2note").ToString
                        rv("wodtl3refcode") = dv(C1)("matrefcode").ToString
                        rv("wodtl3refshortdesc") = dv(C1)("matrefshortdesc").ToString
                        rv("wodtl3unit") = dv(C1)("matrefunit").ToString
                        rv("wodtl3stockqty") = dv(C1)("wodtl3stockqty").ToString
                        rv("wodtl3qtyref") = ToDouble(dv(C1)("bomdtl2qty").ToString)
                        rv("wodtl3value") = ToDouble(dv(C1)("bomdtl2refvalue").ToString)
                        rv("wodtl3amt") = ToDouble(dv(C1)("bomdtl2refvalue").ToString) * ToDouble(rv("wodtl3qty"))
						rv("wodtl3oid") = 0

                        rv.EndEdit()
                        iSeq += 1
                    End If
                    dvMat.RowFilter = ""
                Next
                dv.RowFilter = ""
                Session("TblDtlMatLev5") = dvMat.ToTable
                GVDtl3.DataSource = Session("TblDtlMatLev5")
                GVDtl3.DataBind()
                ClearDetail2()
                getTotalCalculaion()
            End If
            cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, False)
        Else

        End If
    End Sub

    Protected Sub btnFindListMat2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("TblListMat") Is Nothing Then
            BindMatData()
        End If
        If Not Session("TblListMat") Is Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                UpdateCheckedMat()
                Dim dv As DataView = dt.DefaultView
                Dim sFilter As String = FilterDDLListMat2.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat2.Text) & "%'"
                If cbCat04ListMat.Checked And FilterDDLCat04ListMat.SelectedValue <> "" Then
                    sFilter &= " AND itemcat4=" & FilterDDLCat04ListMat.SelectedValue & ""
                Else
                    If cbCat03ListMat.Checked And FilterDDLCat03ListMat.SelectedValue <> "" Then
                        sFilter &= " AND itemcat3=" & FilterDDLCat03ListMat.SelectedValue & ""
                    Else
                        If cbCat02ListMat.Checked And FilterDDLCat02ListMat.SelectedValue <> "" Then
                            sFilter &= " AND itemcat2=" & FilterDDLCat02ListMat.SelectedValue & ""
                        Else
                            If cbCat01ListMat.Checked And FilterDDLCat01ListMat.SelectedValue <> "" Then
                                sFilter &= " AND itemcat1=" & FilterDDLCat01ListMat.SelectedValue & ""
                            End If
                        End If
                    End If
                End If
                dv.RowFilter = sFilter
                If dv.Count > 0 Then
                    Session("TblListMatView") = dv.ToTable
                    gvListMat2.DataSource = Session("TblListMatView")
                    gvListMat2.DataBind()
                    dv.RowFilter = ""
                    mpeListMat2.Show()
                Else
                    dv.RowFilter = ""
                    Session("ErrMat") = "Material data can't be found!"
                    showMessage(Session("ErrMat"), 2)
                End If
            Else
                Session("ErrMat") = "Material data can't be found!"
                showMessage(Session("ErrMat"), 2)
            End If
        End If
    End Sub

    Protected Sub btnAllListMat2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("TblListMat") Is Nothing Then
            BindMatData()
        End If
        If Not Session("TblListMat") Is Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                UpdateCheckedMat()
                FilterDDLListMat2.SelectedIndex = -1 : FilterTextListMat2.Text = ""
                InitDDLCat01ListMat() : cbCat01ListMat.Checked = False : cbCat02ListMat.Checked = False : cbCat03ListMat.Checked = False : cbCat04ListMat.Checked = False
                Session("TblListMatView") = dt
                gvListMat2.DataSource = Session("TblListMatView")
                gvListMat2.DataBind()
                mpeListMat2.Show()
            Else
                Session("ErrMat") = "Material data can't be found!"
                showMessage(Session("ErrMat"), 2)
            End If
        End If
    End Sub

    Protected Sub lkbCloseListMat2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, False)
    End Sub

    Protected Sub cbAutoGenerate_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim bVal As Boolean = True
        If cbAutoGenerate.Checked Then
            bVal = False
        End If
        btnSearchSO.Visible = bVal : btnClearSO.Visible = bVal
    End Sub

    Protected Sub dlcamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Text = ToMaskEdit(ToDouble(dlcamt.Text), 2) : getTotalCalculaion()
    End Sub

    Protected Sub ohdamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Text = ToMaskEdit(ToDouble(ohdamt.Text), 2) : getTotalCalculaion()
    End Sub

    Protected Sub ImbSearchMakloon_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListSupp.SelectedIndex = -1 : FilterTextListSupp.Text = "" : gvListSupp.SelectedIndex = -1
        BindSupplierData()
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
    End Sub

    Protected Sub ImbEraseMakloon_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        makloonoid.Text = ""
        makloonname.Text = ""
        ddlNoTransfer.Items.Clear()
    End Sub

    Protected Sub btnFindListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindSupplierData()
        mpeListSupp.Show()
    End Sub

    Protected Sub btnAllListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListSupp.SelectedIndex = -1 : FilterTextListSupp.Text = "" : gvListSupp.SelectedIndex = -1
        BindSupplierData()
        mpeListSupp.Show()
    End Sub

    Protected Sub lkbCloseListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub gvListSupp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        makloonoid.Text = gvListSupp.SelectedDataKey.Item("suppoid").ToString
        makloonname.Text = gvListSupp.SelectedDataKey.Item("suppname").ToString
        initDDLTransfer()
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub gvListSupp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvListSupp.PageIndex = e.NewPageIndex
        gvListSupp.DataSource = Session("mstSupp")
        gvListSupp.DataBind()
        mpeListSupp.Show()
    End Sub

    Protected Sub wodtl2procseq_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim bVal As Boolean = False
        Dim sCss As String = "inpTextDisabled"
        If wodtl2procseq.Text <> "" Then
            bVal = True : sCss = "inpText"
        End If
        deptoid.Enabled = bVal : deptoid.CssClass = sCss
        todeptoid.Enabled = bVal : todeptoid.CssClass = sCss
    End Sub

    Protected Sub wodtl2type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If wodtl2type.SelectedValue = "FG" Then
            lbltodept.Visible = False : septtodept.Visible = False : todeptoid.Visible = False
        Else
            lbltodept.Visible = True : septtodept.Visible = True : todeptoid.Visible = True
        End If
        InitDDLToDept()
    End Sub

    Protected Sub kancingamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Text = ToMaskEdit(ToDouble(kancingamt.Text), 2) : getTotalCalculaion()
    End Sub

    Protected Sub kainamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Text = ToMaskEdit(ToDouble(kainamt.Text), 2) : getTotalCalculaion()
    End Sub

    Protected Sub labelamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Text = ToMaskEdit(ToDouble(labelamt.Text), 2) : getTotalCalculaion()
    End Sub

    Protected Sub othervalue_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Text = ToMaskEdit(ToDouble(othervalue.Text), 2) : getTotalCalculaion()
    End Sub

#End Region

    Protected Sub bordiramt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Text = ToMaskEdit(ToDouble(sender.Text), 2) : getTotalCalculaion()
    End Sub

    Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListSupp2.SelectedIndex = -1 : FilterTextListSupp2.Text = "" : gvListSupp2.SelectedIndex = -1
        BindSupplierData2()
        cProc.SetModalPopUpExtender(btnHideListSupp2, pnlListSupp2, mpeListSupp2, True)
    End Sub

    Protected Sub btnEraseSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        suppoid_bordir.Text = "" : suppname_bordir.Text = "" : bordiramt.Text = ""
    End Sub

    Protected Sub btnFindListSupp2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindSupplierData2()
        mpeListSupp2.Show()
    End Sub

    Protected Sub btnAllListSupp2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListSupp2.SelectedIndex = -1 : FilterTextListSupp2.Text = "" : gvListSupp2.SelectedIndex = -1
        BindSupplierData2()
        mpeListSupp2.Show()
    End Sub

    Protected Sub gvListSupp2_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvListSupp2.PageIndex = e.NewPageIndex
        gvListSupp2.DataSource = Session("mstSupp2")
        gvListSupp2.DataBind()
        mpeListSupp2.Show()
    End Sub

    Protected Sub gvListSupp2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        suppoid_bordir.Text = gvListSupp2.SelectedDataKey.Item("suppoid").ToString
        suppname_bordir.Text = gvListSupp2.SelectedDataKey.Item("suppname").ToString
        cProc.SetModalPopUpExtender(btnHideListSupp2, pnlListSupp2, mpeListSupp2, False)
    End Sub

    Protected Sub lkbCloseListSupp2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListSupp2, pnlListSupp2, mpeListSupp2, False)
    End Sub

    Protected Sub txtHargaJualFinal_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Text = ToMaskEdit(ToDouble(txtHargaJualFinal.Text), 2)
    End Sub

    Protected Sub txtRefPriceKancing_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Text = ToMaskEdit(ToDouble(txtRefPriceKancing.Text), 2)
        kancingamt.Text = ToMaskEdit(ToDouble(txtRefPriceKancing.Text) * ToDouble(txtJumlahKancing.Text), 2)
    End Sub

    Protected Sub txtJumlahKancing_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        kancingamt.Text = ToMaskEdit(ToDouble(txtRefPriceKancing.Text) * ToDouble(txtJumlahKancing.Text), 2)
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("UserID").ToString().ToLower() = "admin" or Session("UserID").ToString().ToLower() = "isti" Then
            wotempstatus.Text = "Post"
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trnwomst SET periodacctg='" & PeriodAcctg.Text & "', wodate='" & wodate.Text & "', wodocrefno='" & Tchar(wodocrefno.Text) & "', womstnote='" & Tchar(womstnote.Text.Trim) & "', womststatus='" & wotempstatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, autogenerate='" & IIf(cbAutoGenerate.Checked, "YES", "NO") & "', dlcamount=" & ToDouble(dlcamt.Text) & ", ohdamount=" & ToDouble(ohdamt.Text) & ", suppoid=" & makloonoid.Text & ", wotempstatus_tmp='" & womststatus.Text & "', wostartdate='" & startdate.Text & "', wofinishdate='" & finishdate.Text & "', wotolerance=" & ToDouble(wotolerance.Text) & ", kancingamt=" & ToDouble(kancingamt.Text) & ", kainamt=" & ToDouble(kainamt.Text) & ", labelamt=" & ToDouble(labelamt.Text) & ", othervalue=" & ToDouble(othervalue.Text) & ", kancingamt_use_ap='" & IIf(cbKancing_use_ap.Checked, "Y", "N") & "', kainamt_use_ap='" & IIf(cbKain_use_ap.Checked, "Y", "N") & "', labelamt_use_ap='" & IIf(cbLabel_use_ap.Checked, "Y", "N") & "', othervalue_use_ap='" & IIf(cbOther_use_ap.Checked, "Y", "N") & "', ohdamount_use_ap='" & IIf(cbBordir_use_ap.Checked, "Y", "N") & "', bordiramt=" & ToDouble(bordiramt.Text) & ", suppoid_bordir=" & ToInteger(suppoid_bordir.Text) & ", jumlah_kancing=" & ToDouble(txtJumlahKancing.Text) & ", harga_kancing=" & ToDouble(txtRefPriceKancing.Text) & ", harga_jual=" & ToDouble(txtHargaJualFinal.Text) & ", lastplanmstoid=" & ToInteger(ddlNoTransfer.SelectedValue) & ", wotempstatus='" & wotempstatus.Text & "' WHERE womstoid=" & womstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If (Session("TblDtlFG") IsNot Nothing) Then
                Dim dtItem As DataTable = Session("TblDtlFG")
                For C1 As Int16 = 0 To dtItem.Rows.Count - 1
                    sSql = "update QL_trnwodtl1 set wodtl1qty=" & ToDouble(dtItem.Rows(C1).Item("wodtl1qty").ToString) & " where womstoid=" & womstoid.Text & " and wodtl1oid=" & dtItem.Rows(C1).Item("wodtl1oid") & " and itemoid=" & dtItem.Rows(C1).Item("itemoid")
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    If wotempstatus.Text = "Post" Then
                        If ToDouble(txtHargaJualFinal.Text) > 0 Then
                            sSql = "UPDATE QL_mstitem SET itemPriceList=" & ToDouble(txtHargaJualFinal.Text) & ", backup_itempricelist=itemPriceList WHERE itemoid=" & dtItem.Rows(C1).Item("itemoid") & ""
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                    End If
                Next
            End If
            If (Session("TblDtlProcess") IsNot Nothing) Then
                Dim dtItem As DataTable = Session("TblDtlProcess")
                For C1 As Int16 = 0 To dtItem.Rows.Count - 1
                    sSql = "update QL_trnwodtl2 set wodtl2qty=" & ToDouble(dtItem.Rows(C1).Item("wodtl2qty").ToString) & " where wodtl2oid=" & dtItem.Rows(C1).Item("wodtl2oid")
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Next
            End If
            If (Session("TblDtlMatLev5") IsNot Nothing) Then
                Dim dtItem As DataTable = Session("TblDtlMatLev5")
                Dim wodtl3oid = GenerateID("QL_TRNWODTL3", CompnyCode)
                'Dim list_wodtloid As String = ""
                For C1 As Int16 = 0 To dtItem.Rows.Count - 1
                    sSql = "update QL_trnwodtl3 set wodtl3qty=" & ToDouble(dtItem.Rows(C1).Item("wodtl3qty").ToString) & ", wodtl3value=" & ToDouble(dtItem.Rows(C1).Item("wodtl3value").ToString) & ", wodtl3amt=" & ToDouble(dtItem.Rows(C1).Item("wodtl3amt").ToString) & " where womstoid=" & womstoid.Text & " and wodtl3oid=" & dtItem.Rows(C1).Item("wodtl3oid")
                    'list_wodtloid += dtItem.Rows(C1).Item("wodtl3oid") & ","
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    'If xCmd.ExecuteNonQuery() <= 0 Then
                    '    sSql = "INSERT INTO QL_trnwodtl3 (cmpcode, wodtl3oid, wodtl2oid, wodtl1oid, womstoid, wodtl3seq, bomdtl2oid, wodtl3reftype, wodtl3refoid, wodtl3qty, wodtl3unitoid, wodtl3status, wodtl3note, wodtl3res1, wodtl3res2, wodtl3res3, upduser, updtime, wodtl3value, wodtl3amt) VALUES ('" & DDLBusUnit.SelectedValue & "', " & CInt(wodtl3oid) & ", 0, 0, " & womstoid.Text & ", " & C1 + 1 & ", 0, '" & dtItem.Rows(C1).Item("wodtl3reftype").ToString & "', " & dtItem.Rows(C1).Item("wodtl3refoid").ToString & ", " & ToDouble(dtItem.Rows(C1).Item("wodtl3qty").ToString) & ", " & dtItem.Rows(C1).Item("wodtl3unitoid").ToString & ", '', '" & Tchar(dtItem.Rows(C1).Item("wodtl3note").ToString) & "', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dtItem.Rows(C1).Item("wodtl3value").ToString) & ", " & ToDouble(dtItem.Rows(C1).Item("wodtl3amt").ToString) & ")"
                    '    xCmd.CommandText = sSql
                    '    xCmd.ExecuteNonQuery()
                    '    wodtl3oid += 1
                    'End If
                Next
                'list_wodtloid += "0"

                'sSql = "UPDATE QL_mstoid SET lastoid=" & CInt(wodtl3oid) - 1 & " WHERE tablename='QL_TRNWODTL3' AND cmpcode='" & CompnyCode & "'"
                'xCmd.CommandText = sSql
                'xCmd.ExecuteNonQuery()
                'sSql = "delete from QL_trnwodtl3 where wodtl3oid not in (" & list_wodtloid & ") and wodtl3oid not in (select reqdtloid from QL_trnusagedtl where reqmstoid=" & womstoid.Text & ") and womstoid=" & womstoid.Text
                'xCmd.CommandText = sSql
                'xCmd.ExecuteNonQuery()
            End If

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            showMessage(ex.ToString & sSql, 1)
            conn.Close()
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnWO.aspx?awal=true")
    End Sub

    Protected Sub btnSendApp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        wotempstatus.Text = "In Approval"
        btnUpdate_Click(Nothing, Nothing)
    End Sub
End Class