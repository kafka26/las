Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Structure VarCostValue
    Public ValDM_IDR As Double
    Public ValDM_USD As Double
    Public ValDL_IDR As Double
    Public ValDL_USD As Double
    Public ValFOH_IDR As Double
    Public ValFOH_USD As Double
    Public ValCost_IDR As Double
    Public ValCost_USD As Double
End Structure

Partial Class Transaction_ProductionResultConfirmation
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If DDLDept.SelectedValue = "" Then
            sError &= "- Please select DEPARTMENT field!<BR>"
        End If
        If tbDate.Text = "" Then
            sError &= "- Please fill MAX. DATE field!<BR>"
        Else
            If Not IsValidDate(tbDate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- MAX. DATE is invalid! " & sErr & "<BR>"
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetOutstandingTrans(ByVal sKIKOid As String, ByVal sSeq As String) As String
        Dim sResult As String = ""
        Dim iDataCount As Integer = 0
        ' Check outstanding Material Request (Raw, Gen, SP, Kayu, By SO, By SO (Kayu))
        sSql = "SELECT COUNT(*) FROM QL_trnreqmst h1 FULL OUTER JOIN QL_trnusagedtl d1 ON d1.cmpcode=h1.cmpcode AND d1.reqmstoid=h1.reqmstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqmststatus IN ('In Process') AND d1.reqmstoid IS NULL AND h1.womstoid=" & sKIKOid & ""
        iDataCount = ToInteger(GetStrData(sSql))
        If iDataCount > 0 Then
            sResult &= "- Material Request: " & iDataCount & " Data<BR>"
        End If

        ' Check outstanding Material Usage (Raw, Gen, SP, Kayu, By SO, By SO (Kayu))
        sSql = "SELECT COUNT(*) FROM QL_trnusagemst h1 INNER JOIN QL_trnusagedtl d1 ON d1.cmpcode=h1.cmpcode AND d1.usagemstoid=h1.usagemstoid INNER JOIN QL_trnreqmst h2 ON h2.cmpcode=d1.cmpcode AND h2.reqmstoid=d1.reqmstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagemststatus IN ('In Process') AND h2.womstoid=" & sKIKOid & ""
        iDataCount = ToInteger(GetStrData(sSql))
        If iDataCount > 0 Then
            sResult &= "- Material Usage: " & iDataCount & " Data<BR>"
        End If

        ' Check outstanding Berita Acara
        sSql = "SELECT COUNT(*) FROM QL_trnbrtacaramst h1 WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND acaramststatus IN ('In Process', 'In Approval', 'Revised') AND h1.womstoid=" & sKIKOid & ""
        iDataCount = ToInteger(GetStrData(sSql))
        If iDataCount > 0 Then
            sResult &= "- Berita Acara: " & iDataCount & " Data<BR>"
        End If

        ' Check outstanding Production Result
        sSql = "SELECT COUNT(*) FROM QL_trnprodresmst h1 INNER JOIN QL_trnprodresdtl d1 ON d1.cmpcode=h1.cmpcode AND d1.prodresmstoid=h1.prodresmstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND prodresmststatus IN ('In Process') AND d1.womstoid=" & sKIKOid & ""
        iDataCount = ToInteger(GetStrData(sSql))
        If iDataCount > 0 Then
            sResult &= "- Production Result: " & iDataCount & " Data<BR>"
        End If
        If sResult <> "" Then
            sResult = "Every outstanding data transaction for selected SPK must be <B>POSTED</B> or <B>APPROVED</B> before continue on Production Result Confirmation. Please click link below to view detail information!"
        End If
        Return sResult
    End Function

    Private Function GetAcctgStock(ByVal sType As String) As String
        GetAcctgStock = ""
        sSql = "SELECT ISNULL(genother1,'') acctgoid FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='GROUPITEM' AND gencode='" & sType & "' AND activeflag='ACTIVE'"
        GetAcctgStock = GetStrData(sSql)
        Return GetAcctgStock
    End Function

    Private Function GetQtyConverter(ByVal iOid As Integer, ByVal iUnitOid As Integer, ByVal dQty As Double, Optional ByVal sType As String = "KECIL") As Double
        Dim dQty_unitkecil As Double, dQty_unitbesar As Double
        GetUnitConverter(iOid, iUnitOid, dQty, dQty_unitkecil, dQty_unitbesar)
        If sType = "KECIL" Then
            Return dQty_unitkecil
        Else
            Return dQty_unitbesar
        End If
    End Function

    Private Function CheckRequestKIK(ByVal sKIKOid As String) As Boolean
        sSql = "SELECT COUNT(*) FROM QL_trnusagemst h1 WHERE h1.cmpcode='" & CompnyCode & "' AND h1.womstoid=" & sKIKOid
        If ToDouble(GetStrData(sSql)) <= 0 Then
            Return False
        End If
        Return True
    End Function

#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer, Optional ByVal bView As Boolean = False)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage : lbViewDetail.Visible = bView
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitAllDDL()
        'Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If FillDDL(DDLBusUnit, sSql) Then
            InitDDLDept()
            InitDDLWH()
        End If
    End Sub

    Private Sub InitDDLDept()
        'Fill DDL Department
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND deptoid IN (SELECT wod2.deptoid FROM QL_trnwodtl2 wod2 INNER JOIN QL_trnwomst wom ON wom.cmpcode=wod2.cmpcode AND wom.womstoid=wod2.womstoid WHERE womststatus='Post' AND wodtl2type='FG') ORDER BY deptname"
        FillDDL(DDLDept, sSql)
    End Sub

    Private Sub InitDDLWH()
        'Fill DDL Warehouse
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='WAREHOUSE' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDL(DDLSetWH, sSql)
    End Sub

    Private Sub InitDDLWarehouse()
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='WAREHOUSE' AND activeflag='ACTIVE' ORDER BY gendesc"
        For C1 As Integer = 0 To gvDtl.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvDtl.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(9).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                        FillDDL(CType(myControl, System.Web.UI.WebControls.DropDownList), sSql)
                    End If
                Next
            End If
        Next
    End Sub

    Private Sub UpdateTblDtl(ByRef dtRef As DataTable)
        Dim bVal As Boolean, sOid As String, dQty As Double, sWHOid As String
        For C1 As Integer = 0 To gvDtl.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvDtl.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                bVal = False : sOid = "" : dQty = 0 : sWHOid = "0"
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        bVal = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                    End If
                Next
                cc = row.Cells(6).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                        dQty = ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text)
                    End If
                Next
                cc = row.Cells(9).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                        sWHOid = ToInteger(CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue)
                    End If
                Next
                dtRef.DefaultView.RowFilter = "trnoid='" & sOid & "'"
                If dtRef.DefaultView.Count > 0 Then
                    If bVal = True Then
                        dtRef.DefaultView(0)("checkvalue") = "True"
                        dtRef.DefaultView(0)("confirmqty") = dQty
                        dtRef.DefaultView(0)("confirmwhoid") = sWHOid
                    Else
                        dtRef.DefaultView(0)("checkvalue") = "False"
                    End If
                End If
                dtRef.DefaultView.RowFilter = ""
            End If
        Next
        dtRef.AcceptChanges()
        Session("TblDtl") = dtRef
    End Sub

    Private Sub GetTotalCost(ByVal sKIKOid As String, ByRef varCost As VarCostValue, ByVal sStartDate As String)
        Dim sEndDate As String = GetServerTime().ToString()
        ' ==> GET DM IDR
        sSql = "SELECT SUM(totalcostidr) AS totalcostidr FROM (" & _
               "SELECT ISNULL(SUM(usageqty * usagevalueidr), 0.0) AS totalcostidr FROM QL_trnusagedtl mud INNER JOIN QL_trnusagemst mum ON mum.cmpcode=mud.cmpcode AND mum.usagemstoid=mud.usagemstoid WHERE mud.cmpcode='" & DDLBusUnit.SelectedValue & "' AND mud.updtime>=CAST('" & sStartDate & "' AS DATETIME) AND mud.updtime<=CAST('" & sEndDate & "' AS DATETIME) AND usagemststatus<>'In Process' AND mum.usagemstoid>0 AND mum.womstoid=" & sKIKOid & " UNION ALL " & _
                 "SELECT SUM(totalcostidr) AS totalcostidr FROM (SELECT ISNULL(((acaraqty /*- ISNULL((SELECT SUM(retbaqty) FROM QL_trnreturnbadtl retd INNER JOIN QL_trnreturnbamst retm ON retm.cmpcode=retd.cmpcode AND retm.retbamstoid=retd.retbamstoid WHERE retd.cmpcode=bad.cmpcode AND retbamststatus<>'In Process' AND retd.acaradtloid=bad.acaradtloid AND retd.acaramstoid=bad.acaramstoid), 0.0)*/) * acaravalueidr), 0.0) AS totalcostidr FROM QL_trnbrtacaradtl bad INNER JOIN QL_trnbrtacaramst bam ON bam.cmpcode=bad.cmpcode AND bad.acaramstoid=bam.acaramstoid WHERE bad.cmpcode='" & DDLBusUnit.SelectedValue & "' AND bad.updtime>=CAST('" & sStartDate & "' AS DATETIME) AND bad.updtime<=CAST('" & sEndDate & "' AS DATETIME) AND acaramststatus IN ('Approved', 'Closed') AND bam.acaramstoid>0 AND bam.womstoid=" & sKIKOid & " AND acaratype<>'Material Return') AS QL_trnbrtacaradtl UNION ALL " & _
                  "SELECT ISNULL(SUM(acaraqty * acaravalueidr), 0.0) * -1 AS totalcostidr FROM QL_trnbrtacaradtl bad INNER JOIN QL_trnbrtacaramst bam ON bam.cmpcode=bad.cmpcode AND bad.acaramstoid=bam.acaramstoid WHERE bad.cmpcode='" & DDLBusUnit.SelectedValue & "' AND bad.updtime>=CAST('" & sStartDate & "' AS DATETIME) AND bad.updtime<=CAST('" & sEndDate & "' AS DATETIME) AND acaramststatus IN ('Approved', 'Closed') AND bam.acaramstoid>0 AND bam.womstoid=" & sKIKOid & " AND acaratype='Material Return' " & _
                ") AS tblUsageIDR"
        varCost.ValDM_IDR = ToDouble(GetStrData(sSql))
        ' ==> GET DL IDR
        sSql = "SELECT SUM(totalcostidr) AS totalcostidr FROM (" & _
               "SELECT ISNULL(SUM(prodresdlcamt), 0.0) AS totalcostidr FROM QL_trnprodresdtl resd INNER JOIN QL_trnprodresmst resm ON resm.cmpcode=resd.cmpcode AND resm.prodresmstoid=resd.prodresmstoid WHERE resd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND resd.updtime>=CAST('" & sStartDate & "' AS DATETIME) AND resd.updtime<=CAST('" & sEndDate & "' AS DATETIME) AND prodresmststatus<>'In Process' AND resm.prodresmstoid>0 AND resd.womstoid=" & sKIKOid & " /*UNION ALL " & _
               "SELECT ISNULL(SUM(acaraproddlcamt), 0.0) AS totalcostidr FROM QL_trnbrtacaraproddtl resd INNER JOIN QL_trnbrtacaraprodmst resm ON resm.cmpcode=resd.cmpcode AND resm.acaraprodmstoid=resd.acaraprodmstoid WHERE resd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND resd.updtime>=CAST('" & sStartDate & "' AS DATETIME) AND resd.updtime<=CAST('" & sEndDate & "' AS DATETIME) AND acaraprodmststatus IN ('Approved', 'Closed') AND resm.acaraprodmstoid>0 AND resd.womstoid=" & sKIKOid & "" & _
               "*/) AS tblUsageIDR"
        varCost.ValDL_IDR = ToDouble(GetStrData(sSql))
        ' ==> GET FOH IDR
        sSql = "SELECT SUM(totalcostidr) AS totalcostidr FROM (" & _
               "SELECT ISNULL(SUM(prodresohdamt), 0.0) AS totalcostidr FROM QL_trnprodresdtl resd INNER JOIN QL_trnprodresmst resm ON resm.cmpcode=resd.cmpcode AND resm.prodresmstoid=resd.prodresmstoid WHERE resd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND resd.updtime>=CAST('" & sStartDate & "' AS DATETIME) AND resd.updtime<=CAST('" & sEndDate & "' AS DATETIME) AND prodresmststatus<>'In Process' AND resm.prodresmstoid>0 AND resd.womstoid=" & sKIKOid & " /*UNION ALL " & _
               "SELECT ISNULL(SUM(acaraprodohdamt), 0.0) AS totalcostidr FROM QL_trnbrtacaraproddtl resd INNER JOIN QL_trnbrtacaraprodmst resm ON resm.cmpcode=resd.cmpcode AND resm.acaraprodmstoid=resd.acaraprodmstoid WHERE resd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND resd.updtime>=CAST('" & sStartDate & "' AS DATETIME) AND resd.updtime<=CAST('" & sEndDate & "' AS DATETIME) AND acaraprodmststatus IN ('Approved', 'Closed') AND resm.acaraprodmstoid>0 AND resd.womstoid=" & sKIKOid & "" & _
               "*/) AS tblUsageIDR"
        varCost.ValFOH_IDR = ToDouble(GetStrData(sSql))
        ' ==> GET DM USD
        sSql = "SELECT SUM(totalcostusd) AS totalcostusd FROM (" & _
               "SELECT ISNULL(SUM(usageqty * usagevalueusd), 0.0) AS totalcostusd FROM QL_trnusagedtl mud INNER JOIN QL_trnusagemst mum ON mum.cmpcode=mud.cmpcode AND mum.usagemstoid=mud.usagemstoid WHERE mud.cmpcode='" & DDLBusUnit.SelectedValue & "' AND mud.updtime>=CAST('" & sStartDate & "' AS DATETIME) AND mud.updtime<=CAST('" & sEndDate & "' AS DATETIME) AND usagemststatus<>'In Process' AND mum.usagemstoid>0 AND mum.womstoid=" & sKIKOid & " /*UNION ALL " & _
               "SELECT ISNULL(SUM(usagegenqty * usagegenvalueusd), 0.0) AS totalcostusd FROM QL_trnusagegendtl mud INNER JOIN QL_trnusagegenmst mum ON mum.cmpcode=mud.cmpcode AND mum.usagegenmstoid=mud.usagegenmstoid INNER JOIN QL_trnreqgenmst reqm ON reqm.cmpcode=mud.cmpcode AND reqm.reqgenmstoid=mud.reqgenmstoid WHERE mud.cmpcode='" & DDLBusUnit.SelectedValue & "' AND mud.updtime>=CAST('" & sStartDate & "' AS DATETIME) AND mud.updtime<=CAST('" & sEndDate & "' AS DATETIME) AND usagegenmststatus<>'In Process' AND mum.usagegenmstoid>0 AND reqm.womstoid=" & sKIKOid & " UNION ALL " & _
               "SELECT ISNULL(SUM(usagespqty * usagespvalueusd), 0.0) AS totalcostusd FROM QL_trnusagespdtl mud INNER JOIN QL_trnusagespmst mum ON mum.cmpcode=mud.cmpcode AND mum.usagespmstoid=mud.usagespmstoid INNER JOIN QL_trnreqspmst reqm ON reqm.cmpcode=mud.cmpcode AND reqm.reqspmstoid=mud.reqspmstoid WHERE mud.cmpcode='" & DDLBusUnit.SelectedValue & "' AND mud.updtime>=CAST('" & sStartDate & "' AS DATETIME) AND mud.updtime<=CAST('" & sEndDate & "' AS DATETIME) AND usagespmststatus<>'In Process' AND mum.usagespmstoid>0 AND reqm.womstoid=" & sKIKOid & " UNION ALL " & _
               "SELECT ISNULL(SUM(usagekayuqty * usagekayuvalueusd), 0.0) AS totalcostusd FROM QL_trnusagekayudtl mud INNER JOIN QL_trnusagekayumst mum ON mum.cmpcode=mud.cmpcode AND mum.usagekayumstoid=mud.usagekayumstoid INNER JOIN QL_trnreqkayumst reqm ON reqm.cmpcode=mud.cmpcode AND reqm.reqkayumstoid=mud.reqkayumstoid WHERE mud.cmpcode='" & DDLBusUnit.SelectedValue & "' AND mud.updtime>=CAST('" & sStartDate & "' AS DATETIME) AND mud.updtime<=CAST('" & sEndDate & "' AS DATETIME) AND usagekayumststatus<>'In Process' AND mum.usagekayumstoid>0 AND reqm.womstoid=" & sKIKOid & " UNION ALL " & _
               "SELECT SUM(totalcostusd) AS totalcostusd FROM (SELECT ISNULL(((usagedtl2qty - ISNULL((SELECT SUM(retdtl2qty) FROM QL_trnreturndtl2 retd2 INNER JOIN QL_trnreturnmst retm ON retm.cmpcode=retd2.cmpcode AND retm.retmstoid=retd2.retmstoid WHERE retd2.cmpcode=mud2.cmpcode AND retmststatus<>'In Process' AND retd2.usagedtl2oid=mud2.usagedtl2oid), 0.0)) * usagevalueusd), 0.0) AS totalcostusd FROM QL_trnusagedtl2 mud2 INNER JOIN QL_trnusagedtl mud ON mud.cmpcode=mud2.cmpcode AND mud.usagedtloid=mud2.usagedtloid INNER JOIN QL_trnusagemst mum ON mum.cmpcode=mud.cmpcode AND mum.usagemstoid=mud.usagemstoid INNER JOIN QL_trnreqdtl2 reqd2 ON reqd2.cmpcode=mud2.cmpcode AND reqd2.reqdtl2oid=mud2.reqdtl2oid WHERE mud2.cmpcode='" & DDLBusUnit.SelectedValue & "' AND mud2.updtime>=CAST('" & sStartDate & "' AS DATETIME) AND mud2.updtime<=CAST('" & sEndDate & "' AS DATETIME) AND usagemststatus<>'In Process' AND mum.usagemstoid>0 AND reqd2.womstoid=" & sKIKOid & ") AS QL_trnreturndtl2 UNION ALL " & _
               "SELECT SUM(totalcostusd) AS totalcostusd FROM (SELECT ISNULL(((usage2qty - ISNULL((SELECT SUM(ret2qty) FROM QL_trnreturn2dtl retd INNER JOIN QL_trnreturn2mst retm ON retm.cmpcode=retd.cmpcode AND retm.ret2mstoid=retd.ret2mstoid WHERE retd.cmpcode=mud.cmpcode AND ret2mststatus<>'In Process' AND retd.usage2dtloid=mud.usage2dtloid), 0.0)) * usage2valueidr), 0.0) AS totalcostusd FROM QL_trnusage2dtl2 mud2 INNER JOIN QL_trnusage2dtl mud ON mud.cmpcode=mud2.cmpcode AND mud.req2dtloid=mud2.req2dtloid INNER JOIN QL_trnusage2mst mum ON mum.cmpcode=mud.cmpcode AND mum.usage2mstoid=mud.usage2mstoid INNER JOIN QL_trnreq2dtl2 reqd2 ON reqd2.cmpcode=mud2.cmpcode AND reqd2.req2dtl2oid=mud2.req2dtl2oid WHERE mud2.cmpcode='" & DDLBusUnit.SelectedValue & "' AND mud2.updtime>=CAST('" & sStartDate & "' AS DATETIME) AND mud2.updtime<=CAST('" & sEndDate & "' AS DATETIME) AND usage2mststatus<>'In Process' AND mum.usage2mstoid>0 AND reqd2.womstoid=" & sKIKOid & ") AS QL_trnreturn2dtl2*/ UNION ALL " & _
               "SELECT ISNULL(SUM(acaraqty * acaravalueusd), 0.0) AS totalcostusd FROM QL_trnbrtacaradtl bad INNER JOIN QL_trnbrtacaramst bam ON bam.cmpcode=bad.cmpcode AND bad.acaramstoid=bam.acaramstoid WHERE bad.cmpcode='" & DDLBusUnit.SelectedValue & "' AND bad.updtime>=CAST('" & sStartDate & "' AS DATETIME) AND bad.updtime<=CAST('" & sEndDate & "' AS DATETIME) AND acaramststatus IN ('Approved', 'Closed') AND bam.acaramstoid>0 AND bam.womstoid=" & sKIKOid & " AND acaratype<>'Material Return' UNION ALL " & _
               "SELECT ISNULL(SUM(acaraqty * acaravalueusd), 0.0) * -1 AS totalcostusd FROM QL_trnbrtacaradtl bad INNER JOIN QL_trnbrtacaramst bam ON bam.cmpcode=bad.cmpcode AND bad.acaramstoid=bam.acaramstoid WHERE bad.cmpcode='" & DDLBusUnit.SelectedValue & "' AND bad.updtime>=CAST('" & sStartDate & "' AS DATETIME) AND bad.updtime<=CAST('" & sEndDate & "' AS DATETIME) AND acaramststatus IN ('Approved', 'Closed') AND bam.acaramstoid>0 AND bam.womstoid=" & sKIKOid & " AND acaratype='Material Return'" & _
               ") AS tblUsageUSD"
        varCost.ValDM_USD = ToDouble(GetStrData(sSql))
        ' ==> GET DL USD
        sSql = "SELECT SUM(totalcostusd) AS totalcostusd FROM (" & _
               "SELECT ISNULL(SUM(prodresdlcamtusd), 0.0) AS totalcostusd FROM QL_trnprodresdtl resd INNER JOIN QL_trnprodresmst resm ON resm.cmpcode=resd.cmpcode AND resm.prodresmstoid=resd.prodresmstoid WHERE resd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND resd.updtime>=CAST('" & sStartDate & "' AS DATETIME) AND resd.updtime<=CAST('" & sEndDate & "' AS DATETIME) AND prodresmststatus<>'In Process' AND resm.prodresmstoid>0 AND resd.womstoid=" & sKIKOid & " /*UNION ALL " & _
               "SELECT ISNULL(SUM(acaraproddlcamtusd), 0.0) AS totalcostusd FROM QL_trnbrtacaraproddtl resd INNER JOIN QL_trnbrtacaraprodmst resm ON resm.cmpcode=resd.cmpcode AND resm.acaraprodmstoid=resd.acaraprodmstoid WHERE resd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND resd.updtime>=CAST('" & sStartDate & "' AS DATETIME) AND resd.updtime<=CAST('" & sEndDate & "' AS DATETIME) AND acaraprodmststatus IN ('Approved', 'Closed') AND resm.acaraprodmstoid>0 AND resd.womstoid=" & sKIKOid & "" & _
               "*/) AS tblUsageUSD"
        varCost.ValDL_USD = ToDouble(GetStrData(sSql))
        ' ==> GET FOH USD
        sSql = "SELECT SUM(totalcostusd) AS totalcostusd FROM (" & _
               "SELECT ISNULL(SUM(prodresohdamtusd), 0.0) AS totalcostusd FROM QL_trnprodresdtl resd INNER JOIN QL_trnprodresmst resm ON resm.cmpcode=resd.cmpcode AND resm.prodresmstoid=resd.prodresmstoid WHERE resd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND resd.updtime>=CAST('" & sStartDate & "' AS DATETIME) AND resd.updtime<=CAST('" & sEndDate & "' AS DATETIME) AND prodresmststatus<>'In Process' AND resm.prodresmstoid>0 AND resd.womstoid=" & sKIKOid & " /*UNION ALL " & _
               "SELECT ISNULL(SUM(acaraprodohdamtusd), 0.0) AS totalcostusd FROM QL_trnbrtacaraproddtl resd INNER JOIN QL_trnbrtacaraprodmst resm ON resm.cmpcode=resd.cmpcode AND resm.acaraprodmstoid=resd.acaraprodmstoid WHERE resd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND resd.updtime>=CAST('" & sStartDate & "' AS DATETIME) AND resd.updtime<=CAST('" & sEndDate & "' AS DATETIME) AND acaraprodmststatus IN ('Approved', 'Closed') AND resm.acaraprodmstoid>0 AND resd.womstoid=" & sKIKOid & "" & _
               "*/) AS tblUsageUSD"
        varCost.ValFOH_USD = ToDouble(GetStrData(sSql))
        ' ==> COUNT TOTAL COST
        varCost.ValCost_IDR = varCost.ValDM_IDR + varCost.ValDL_IDR + varCost.ValFOH_IDR
        varCost.ValCost_USD = varCost.ValDM_USD + varCost.ValDL_USD + varCost.ValFOH_USD
    End Sub

    Private Sub PrintOutstandingTrans()
        Try
            report.Load(Server.MapPath(folderReport & "rptOutstandingConfirm.rpt"))

            Dim sKIKOid As String = ""
            Dim dtTbl As DataTable = Session("TblDtl")
            For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                If dtTbl.Rows(C1)("checkvalue").ToString.ToUpper = "TRUE" Then
                    sKIKOid &= dtTbl.Rows(C1)("womstoid").ToString & ","
                End If
            Next
            If sKIKOid <> "" Then
                sKIKOid = Left(sKIKOid, sKIKOid.Length - 1)
            End If

            ' Get data outstanding Material Request (Raw, Gen, SP, Kayu, By SO, By SO (Kayu))
            sSql = "SELECT tblData.*, wono [KIK No.], itemcode [Code FG], itemlongdesc [Description FG], (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=tblData.cmpcode) [Business Unit], groupcode [Division Code], groupdesc [Division Name] FROM ("
            sSql &= "SELECT 'Raw Material Request' [Form Name], CAST(h1.reqrawmstoid AS VARCHAR(10)) [Draft No.], h1.reqrawno [Trans. No.], h1.reqrawmststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], h1.womstoid [ID KIK], h1.cmpcode FROM QL_trnreqrawmst h1 FULL OUTER JOIN QL_trnusagerawdtl d1 ON d1.cmpcode=h1.cmpcode AND d1.reqrawmstoid=h1.reqrawmstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqrawmststatus IN ('In Process', 'Post') AND d1.reqrawmstoid IS NULL AND h1.womstoid IN (" & sKIKOid & ") UNION ALL "
            sSql &= "SELECT 'General Material Request' [Form Name], CAST(h1.reqgenmstoid AS VARCHAR(10)) [Draft No.], h1.reqgenno [Trans. No.], h1.reqgenmststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], h1.womstoid [ID KIK], h1.cmpcode FROM QL_trnreqgenmst h1 FULL OUTER JOIN QL_trnusagegendtl d1 ON d1.cmpcode=h1.cmpcode AND d1.reqgenmstoid=h1.reqgenmstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqgenmststatus IN ('In Process', 'Post') AND d1.reqgenmstoid IS NULL AND h1.womstoid IN (" & sKIKOid & ") UNION ALL "
            sSql &= "SELECT 'Spare Part Request' [Form Name], CAST(h1.reqspmstoid AS VARCHAR(10)) [Draft No.], h1.reqspno [Trans. No.], h1.reqspmststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], h1.womstoid [ID KIK], h1.cmpcode FROM QL_trnreqspmst h1 FULL OUTER JOIN QL_trnusagespdtl d1 ON d1.cmpcode=h1.cmpcode AND d1.reqspmstoid=h1.reqspmstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqspmststatus IN ('In Process', 'Post') AND d1.reqspmstoid IS NULL AND h1.womstoid IN (" & sKIKOid & ") UNION ALL "
            sSql &= "SELECT 'Material Request (Kayu)' [Form Name], CAST(h1.reqkayumstoid AS VARCHAR(10)) [Draft No.], h1.reqkayuno [Trans. No.], h1.reqkayumststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], h1.womstoid [ID KIK], h1.cmpcode FROM QL_trnreqkayumst h1 FULL OUTER JOIN QL_trnusagekayudtl d1 ON d1.cmpcode=h1.cmpcode AND d1.reqkayumstoid=h1.reqkayumstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqkayumststatus IN ('In Process', 'Post') AND d1.reqkayumstoid IS NULL AND h1.womstoid IN (" & sKIKOid & ") UNION ALL "
            sSql &= "SELECT 'Material Request By SO' [Form Name], CAST(h1.reqmstoid AS VARCHAR(10)) [Draft No.], h1.reqno [Trans. No.], h1.reqmststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], d1.womstoid [ID KIK], h1.cmpcode FROM QL_trnreqmst h1 INNER JOIN QL_trnreqdtl2 d1 ON d1.cmpcode=h1.cmpcode AND d1.reqmstoid=h1.reqmstoid FULL OUTER JOIN QL_trnusagedtl d2 ON d2.cmpcode=h1.cmpcode AND d2.reqmstoid=h1.reqmstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqmststatus IN ('In Process', 'Post') AND d2.reqmstoid IS NULL AND d1.womstoid IN (" & sKIKOid & ") UNION ALL "
            sSql &= "SELECT 'Material Request By SO (Kayu)' [Form Name], CAST(h1.req2mstoid AS VARCHAR(10)) [Draft No.], h1.req2no [Trans. No.], h1.req2mststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], d1.womstoid [ID KIK], h1.cmpcode FROM QL_trnreq2mst h1 INNER JOIN QL_trnreq2dtl2 d1 ON d1.cmpcode=h1.cmpcode AND d1.req2mstoid=h1.req2mstoid FULL OUTER JOIN QL_trnusage2dtl d2 ON d2.cmpcode=h1.cmpcode AND d2.req2mstoid=h1.req2mstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND req2mststatus IN ('In Process', 'Post') AND d2.req2mstoid IS NULL AND d1.womstoid IN (" & sKIKOid & ") UNION ALL "
            ' Get data outstanding Material Usage (Raw, Gen, SP, Kayu, By SO, By SO (Kayu))
            sSql &= "SELECT 'Raw Material Usage' [Form Name], CAST(h1.usagerawmstoid AS VARCHAR(10)) [Draft No.], h1.usagerawno [Trans. No.], h1.usagerawmststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], h2.womstoid [ID KIK], h1.cmpcode FROM QL_trnusagerawmst h1 INNER JOIN QL_trnusagerawdtl d1 ON d1.cmpcode=h1.cmpcode AND d1.usagerawmstoid=h1.usagerawmstoid INNER JOIN QL_trnreqrawmst h2 ON h2.cmpcode=d1.cmpcode AND h2.reqrawmstoid=d1.reqrawmstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagerawmststatus IN ('In Process') AND h2.womstoid IN (" & sKIKOid & ") UNION ALL "
            sSql &= "SELECT 'General Material Usage' [Form Name], CAST(h1.usagegenmstoid AS VARCHAR(10)) [Draft No.], h1.usagegenno [Trans. No.], h1.usagegenmststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], h2.womstoid [ID KIK], h1.cmpcode FROM QL_trnusagegenmst h1 INNER JOIN QL_trnusagegendtl d1 ON d1.cmpcode=h1.cmpcode AND d1.usagegenmstoid=h1.usagegenmstoid INNER JOIN QL_trnreqgenmst h2 ON h2.cmpcode=d1.cmpcode AND h2.reqgenmstoid=d1.reqgenmstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagegenmststatus IN ('In Process') AND h2.womstoid IN (" & sKIKOid & ") UNION ALL "
            sSql &= "SELECT 'Spare Part Usage' [Form Name], CAST(h1.usagespmstoid AS VARCHAR(10)) [Draft No.], h1.usagespno [Trans. No.], h1.usagespmststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], h2.womstoid [ID KIK], h1.cmpcode FROM QL_trnusagespmst h1 INNER JOIN QL_trnusagespdtl d1 ON d1.cmpcode=h1.cmpcode AND d1.usagespmstoid=h1.usagespmstoid INNER JOIN QL_trnreqspmst h2 ON h2.cmpcode=d1.cmpcode AND h2.reqspmstoid=d1.reqspmstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagespmststatus IN ('In Process') AND h2.womstoid IN (" & sKIKOid & ") UNION ALL "
            sSql &= "SELECT 'Material Usage (Kayu)' [Form Name], CAST(h1.usagekayumstoid AS VARCHAR(10)) [Draft No.], h1.usagekayuno [Trans. No.], h1.usagekayumststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], h2.womstoid [ID KIK], h1.cmpcode FROM QL_trnusagekayumst h1 INNER JOIN QL_trnusagekayudtl d1 ON d1.cmpcode=h1.cmpcode AND d1.usagekayumstoid=h1.usagekayumstoid INNER JOIN QL_trnreqkayumst h2 ON h2.cmpcode=d1.cmpcode AND h2.reqkayumstoid=d1.reqkayumstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagekayumststatus IN ('In Process') AND h2.womstoid IN (" & sKIKOid & ") UNION ALL "
            sSql &= "SELECT 'Material Usage By SO' [Form Name], CAST(h1.usagemstoid AS VARCHAR(10)) [Draft No.], h1.usageno [Trans. No.], h1.usagemststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], d2.womstoid [ID KIK], h1.cmpcode FROM QL_trnusagemst h1 INNER JOIN QL_trnusagedtl2 d1 ON d1.cmpcode=h1.cmpcode AND d1.usagemstoid=h1.usagemstoid INNER JOIN QL_trnreqdtl2 d2 ON d2.cmpcode=d1.cmpcode AND d2.reqdtl2oid=d1.reqdtl2oid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagemststatus IN ('In Process') AND d2.womstoid IN (" & sKIKOid & ") UNION ALL "
            sSql &= "SELECT 'Material Usage By SO (Kayu)' [Form Name], CAST(h1.usage2mstoid AS VARCHAR(10)) [Draft No.], h1.usage2no [Trans. No.], h1.usage2mststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], d2.womstoid [ID KIK], h1.cmpcode FROM QL_trnusage2mst h1 INNER JOIN QL_trnusage2dtl2 d1 ON d1.cmpcode=h1.cmpcode AND d1.usage2mstoid=h1.usage2mstoid INNER JOIN QL_trnreq2dtl2 d2 ON d2.cmpcode=d1.cmpcode AND d2.req2dtl2oid=d1.req2dtl2oid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND usage2mststatus IN ('In Process') AND d2.womstoid IN (" & sKIKOid & ") UNION ALL "
            ' Get data outstanding Material Return (By SO, By SO (Kayu))
            sSql &= "SELECT 'Material Return By SO' [Form Name], CAST(h1.retmstoid AS VARCHAR(10)) [Draft No.], h1.retno [Trans. No.], h1.retmststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], d3.womstoid [ID KIK], h1.cmpcode FROM QL_trnreturnmst h1 INNER JOIN QL_trnreturndtl2 d1 ON d1.cmpcode=h1.cmpcode AND d1.retmstoid=h1.retmstoid INNER JOIN QL_trnusagedtl2 d2 ON d2.cmpcode=d1.cmpcode AND d2.usagedtl2oid=d1.usagedtl2oid INNER JOIN QL_trnreqdtl2 d3 ON d3.cmpcode=d2.cmpcode AND d3.reqdtl2oid=d2.reqdtl2oid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND retmststatus IN ('In Process') AND d3.womstoid IN (" & sKIKOid & ") UNION ALL "
            sSql &= "SELECT 'Material Return By SO (Kayu)' [Form Name], CAST(h1.ret2mstoid AS VARCHAR(10)) [Draft No.], h1.ret2no [Trans. No.], h1.ret2mststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], d3.womstoid [ID KIK], h1.cmpcode FROM QL_trnreturn2mst h1 INNER JOIN QL_trnreturn2dtl2 d1 ON d1.cmpcode=h1.cmpcode AND d1.ret2mstoid=h1.ret2mstoid INNER JOIN QL_trnusage2dtl2 d2 ON d2.cmpcode=d1.cmpcode AND d2.usage2dtl2oid=d1.usage2dtl2oid INNER JOIN QL_trnreq2dtl2 d3 ON d3.cmpcode=d2.cmpcode AND d3.req2dtl2oid=d2.req2dtl2oid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND ret2mststatus IN ('In Process') AND d3.womstoid IN (" & sKIKOid & ") UNION ALL "
            ' Get data outstanding Berita Acara
            sSql &= "SELECT 'Berita Acara' [Form Name], CAST(h1.acaramstoid AS VARCHAR(10)) [Draft No.], h1.acarano [Trans. No.], h1.acaramststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], h1.womstoid [ID KIK], h1.cmpcode FROM QL_trnbrtacaramst h1 WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND acaramststatus IN ('In Process', 'In Approval', 'Revised') AND h1.womstoid IN (" & sKIKOid & ") UNION ALL "
            ' Get data outstanding Berita Acara Product
            sSql &= "SELECT 'Berita Acara Product' [Form Name], CAST(h1.acaraprodmstoid AS VARCHAR(10)) [Draft No.], h1.acaraprodno [Trans. No.], h1.acaraprodmststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], d1.womstoid [ID KIK], h1.cmpcode FROM QL_trnbrtacaraprodmst h1 INNER JOIN QL_trnbrtacaraproddtl d1 ON d1.cmpcode=h1.cmpcode AND d1.acaraprodmstoid=h1.acaraprodmstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND acaraprodmststatus IN ('In Process', 'In Approval', 'Revised') AND d1.womstoid IN (" & sKIKOid & ") UNION ALL "
            ' Get data outstanding Production Result
            sSql &= "SELECT 'Production Result' [Form Name], CAST(h1.prodresmstoid AS VARCHAR(10)) [Draft No.], h1.prodresno [Trans. No.], h1.prodresmststatus [Status], h1.createuser [Create User], h1.createtime [Create Datetime], h1.upduser [Last Update User], h1.updtime [Last Update Datetime], d1.womstoid [ID KIK], h1.cmpcode FROM QL_trnprodresmst h1 INNER JOIN QL_trnprodresdtl d1 ON d1.cmpcode=h1.cmpcode AND d1.prodresmstoid=h1.prodresmstoid WHERE h1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND prodresmststatus IN ('In Process') AND d1.womstoid IN (" & sKIKOid & ")"
            sSql &= ") AS tblData INNER JOIN QL_trnwomst wom ON wom.cmpcode=tblData.cmpcode AND wom.womstoid=tblData.[ID KIK] INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=wom.cmpcode AND wod1.womstoid=wom.womstoid INNER JOIN QL_trnsoitemmst som ON som.cmpcode=wod1.cmpcode AND som.soitemmstoid=wod1.soitemmstoid INNER JOIN QL_mstdeptgroup dg ON dg.cmpcode=som.cmpcode AND dg.groupoid=som.groupoid INNER JOIN QL_mstitem i ON i.itemoid=wod1.itemoid"
            Dim dtOutstanding As DataTable = cKon.ambiltabel(sSql, "tblData")

            report.SetDataSource(dtOutstanding)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            report.PrintOptions.PaperSize = PaperSize.PaperA4
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "OutstandingConfirmPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Private Sub GetConvertQty(ByVal objTable As DataTable)
     
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnProdConf.aspx")
        End If
        If checkPagePermission("~\Transaction\trnProdConf.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Production Result Confirmation"
        btnConfirm.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to CONFIRM this data?');")
        If Not Page.IsPostBack Then
            upduser.Text = Session("UserID") : updtime.Text = GetServerTime().ToString
            InitAllDDL()
            tbDate.Text = Format(GetServerTime(), "MM/dd/yyyy")
        End If
        'If Session("TblDtl") IsNot Nothing Then
        '    gvDtl.DataSource = Session("TblDtl")
        '    gvDtl.DataBind()
        'End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLDept()
        InitDDLWH()
    End Sub

    Protected Sub btnShowData_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnShowData.Click
        If IsInputValid() Then
            sSql = "SELECT DISTINCT 0 AS seq, 'False' AS checkvalue, resm.cmpcode, resm.deptoid, resm.prodresmstoid, resd.prodresdtloid, resm.prodresno, CONVERT(VARCHAR(10), resm.prodresdate, 101) AS prodresdate, wom.wono, 0.0 AS confirmqty, 0.0 AS confirmqty_unitkecil, 0.0 AS confirmqty_unitbesar, 0.0 AS confirm_perqty_unitkecil, 0.0 AS confirm_perqty_unitbesar, (resd.prodresqty - ISNULL((SELECT SUM(conf.confirmqty) FROM QL_trnprodresconfirm conf WHERE conf.cmpcode=resd.cmpcode AND conf.prodresdtloid=resd.prodresdtloid AND conf.prodresmstoid=resd.prodresmstoid), 0.0)) AS prodresqty, 0 AS confirmwhoid, '" & tbDate.Text & "' AS confirmdate, ISNULL(i.roundqty, 1.0) AS confirmlimitqty, resd.prodresunitoid AS confirmunitoid, wod1.itemoid, 0.0 AS confirmvalueidr, 0.0 AS confirmvalueusd, 0.0 AS confirmvalueidr_unitkecil, 0.0 AS confirmvalueusd_unitkecil, (CAST(resm.prodresmstoid AS VARCHAR(10)) + '-' + CAST(resd.prodresdtloid AS VARCHAR(10))) AS trnoid, resd.womstoid, 0.0 AS confirmdmidr, 0.0 AS confirmdmusd, 0.0 AS confirmdlidr, 0.0 AS confirmdlusd, 0.0 AS confirmohdidr, 0.0 AS confirmohdusd, balanceamt_cogm, balanceamtusd_cogm, balanceqty_cogm, lastconfirmdate, ISNULL((SELECT gx.gendesc FROM QL_mstgen gx WHERE gx.genoid=i.itemgroupoid),'') AS refname, gx.gendesc AS confunit, gx.genoid AS confunitoid, g1.gendesc AS sisaunitkecil, g1.genoid AS sisaunitkeciloid, (resd.prodresqty_sisa - ISNULL((SELECT SUM(conf.confirmqty_sisa) FROM QL_trnprodresconfirm conf WHERE conf.cmpcode=resd.cmpcode AND conf.prodresdtloid=resd.prodresdtloid AND conf.prodresmstoid=resd.prodresmstoid), 0.0)) AS sisaqty, sisaqty_unitkecil, sisaqty_unitbesar, 0.0 AS sisaqty_real, wod1.itemoid AS wodtl1itemoid, i.itemshortdescription AS itemdesc, i.itemcode, prodresqty_unitkecil " & _
            "FROM QL_trnprodresmst resm INNER JOIN QL_trnprodresdtl resd ON resd.cmpcode=resm.cmpcode AND resd.prodresmstoid=resm.prodresmstoid INNER JOIN QL_trnwomst wom ON wom.cmpcode=resd.cmpcode AND wom.womstoid=resd.womstoid INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=wom.cmpcode AND wod1.womstoid=wom.womstoid INNER JOIN QL_mstitem i ON i.itemoid=wod1.itemoid INNER JOIN QL_mstgen gx ON gx.genoid=resd.prodresunitoid INNER JOIN QL_mstgen g1 ON g1.genoid=resd.sisaunitoid WHERE resm.cmpcode='" & DDLBusUnit.SelectedValue & "' AND resm.deptoid=" & DDLDept.SelectedValue & " AND resm.prodresmststatus IN ('Post', 'Closed') AND CONVERT(DATETIME, resm.prodresdate)<=CONVERT(DATETIME, '" & tbDate.Text & " 23:59:59') AND ISNULL(resd.prodresdtlres1, '')='' AND resd.prodrestype='FG' AND resm.prodresmstoid>0 ORDER BY resm.prodresno"
            Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnprodresmst")
            For C1 As Integer = 0 To dt.Rows.Count - 1
                dt.Rows(C1)("seq") = C1 + 1
                dt.Rows(C1)("confirmqty") = ToDouble(dt.Rows(C1)("prodresqty").ToString)
            Next
            dt.AcceptChanges()
            Session("TblDtl") = dt
            gvDtl.DataSource = Session("TblDtl")
            gvDtl.DataBind()
            InitDDLWarehouse()
            pnlSelect.Visible = (gvDtl.Rows.Count > 0)
        End If
    End Sub

    Protected Sub cbSelectHdr_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim bVal As Boolean = sender.Checked
        For C1 As Integer = 0 To gvDtl.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvDtl.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = bVal
                    End If
                Next
            End If
        Next
    End Sub

    Protected Sub gvDtl_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDtl.PageIndexChanging
        Dim dtTbl As DataTable = Session("TblDtl")
        UpdateTblDtl(dtTbl)
        Session("TblDtl") = dtTbl
        gvDtl.PageIndex = e.NewPageIndex
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 2)

            ' DDL Warehouse
            Dim DDLWH As DropDownList = e.Row.FindControl("DDLWarehouse")
            sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='WAREHOUSE' AND activeflag='ACTIVE' ORDER BY gendesc"
            FillDDL(DDLWH, sSql)
            DDLWH.SelectedValue = DDLWH.ToolTip

            ' Define Header Checkbox based on Checkbox in Each Row
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(0).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    If CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = True Then
                        CType(gvDtl.HeaderRow.FindControl("cbSelectHdr"), CheckBox).Checked = True
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub btnConfirm_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnConfirm.Click
        If Session("TblDtl") IsNot Nothing Then
            Dim dtTbl As DataTable = Session("TblDtl")
            If dtTbl.Rows.Count > 0 Then
                UpdateTblDtl(dtTbl)
                dtTbl.DefaultView.RowFilter = "checkvalue='True'"
                If dtTbl.DefaultView.Count <= 0 Then
                    showMessage("Please select some Production Result data first!", 2)
                    dtTbl.DefaultView.RowFilter = "" : Exit Sub
                End If
                Dim iCount As Integer = dtTbl.DefaultView.Count
                dtTbl.DefaultView.RowFilter = ""
                dtTbl.DefaultView.RowFilter = "checkvalue='True' AND confirmqty > 0"
                If dtTbl.DefaultView.Count <> iCount Then
                    showMessage("Confirm Qty for every selected data must be more than 0!", 2)
                    dtTbl.DefaultView.RowFilter = "" : Exit Sub
                End If
                dtTbl.DefaultView.RowFilter = ""
                dtTbl.DefaultView.RowFilter = "checkvalue='True' AND confirmqty <= prodresqty"
                If dtTbl.DefaultView.Count <> iCount Then
                    showMessage("Confirm Qty for every selected data must be less or equal than Result Qty!", 2)
                    dtTbl.DefaultView.RowFilter = "" : Exit Sub
                End If
                dtTbl.DefaultView.RowFilter = ""
                If cbSetWH.Checked Then
                    If DDLSetWH.SelectedValue = "" Then
                        showMessage("Please define selected Warehouse for all selected data!", 2)
                        dtTbl.DefaultView.RowFilter = "" : Exit Sub
                    Else
                        For C1 As Integer = 0 To dtTbl.DefaultView.Count - 1
                            dtTbl.DefaultView(C1)("confirmwhoid") = DDLSetWH.SelectedValue
                        Next
                    End If
                Else
                    dtTbl.DefaultView.RowFilter = "checkvalue='True' AND confirmwhoid <> 0"
                    If dtTbl.DefaultView.Count <> iCount Then
                        showMessage("Please select Warehouse for every selected data!", 2)
                        dtTbl.DefaultView.RowFilter = "" : Exit Sub
                    End If
                End If
                dtTbl.DefaultView.RowFilter = ""
                dtTbl.DefaultView.RowFilter = "checkvalue='True'"
                If Not IsQtyRounded(dtTbl.DefaultView, "confirmqty", "confirmlimitqty") Then
                    showMessage("Confirm Qty for every selected data must be rounded by Round Qty!", 2)
                    dtTbl.DefaultView.RowFilter = "" : Exit Sub
                End If
                dtTbl.DefaultView.RowFilter = ""
                dtTbl.DefaultView.RowFilter = "checkvalue='True'"
                Dim sErrReply As String = ""
                For C1 As Integer = 0 To dtTbl.DefaultView.Count - 1
                    If Not isLengthAccepted("confirmqty", "QL_trnprodresconfirm", ToDouble(dtTbl.DefaultView(C1)("confirmqty").ToString), sErrReply) Then
                        showMessage("Confirm Qty for every selected data must be less than Max. Confirm Qty (" & sErrReply & ") allowed stored in database!", 2)
                        dtTbl.DefaultView.RowFilter = "" : Exit Sub
                    End If
                    If ToDouble(dtTbl.DefaultView(C1)("balanceqty_cogm").ToString) - ToDouble(dtTbl.DefaultView(C1)("confirmqty").ToString) <= 0 Then
                        sErrReply = GetOutstandingTrans(dtTbl.DefaultView(C1)("womstoid").ToString, dtTbl.DefaultView(C1)("seq").ToString)
                        If sErrReply <> "" Then
                            showMessage(sErrReply, 2, True)
                            dtTbl.DefaultView.RowFilter = "" : Exit Sub
                        End If
                    End If

                    If Not CheckRequestKIK(dtTbl.DefaultView(C1)("womstoid").ToString) Then
                        showMessage("- SPK No <STRONG>" & dtTbl.DefaultView(C1)("wono").ToString & "</STRONG> Belum Pernah dibuatkan Material Request dan Usage!!", 2)
                        Exit Sub
                    End If
                Next
                GetConvertQty(dtTbl)
                Dim iWHSisaProd As String = GetStrData("SELECT TOP 1 genother1 FROM QL_mstgen WHERE gengroup='WH SISA PROD' AND activeflag='ACTIVE' ORDER BY updtime DESC")
                If iWHSisaProd = "" Then
                    showMessage("- Please SETUP WAREHOUSE for SISA PRODUKSI in Master General Group (WH SISA PROD) First!", 2)
                    Exit Sub
                End If
                dtTbl.DefaultView.RowFilter = ""
                dtTbl.DefaultView.RowFilter = "checkvalue='True'"
                Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
                Dim varCost As VarCostValue
                For C1 As Integer = 0 To dtTbl.DefaultView.Count - 1
                    GetTotalCost(dtTbl.DefaultView(C1)("womstoid").ToString, varCost, dtTbl.DefaultView(C1)("lastconfirmdate").ToString)
                    dtTbl.DefaultView(C1)("confirmvalueidr") = (varCost.ValCost_IDR + ToDouble(dtTbl.DefaultView(C1)("balanceamt_cogm").ToString)) / (ToDouble(dtTbl.DefaultView(C1)("confirmqty").ToString))
                    dtTbl.DefaultView(C1)("confirmvalueusd") = (varCost.ValCost_USD + ToDouble(dtTbl.DefaultView(C1)("balanceamtusd_cogm").ToString)) / (ToDouble(dtTbl.DefaultView(C1)("confirmqty").ToString))

                    dtTbl.DefaultView(C1)("confirmvalueidr_unitkecil") = dtTbl.DefaultView(C1)("confirmvalueidr")
                    dtTbl.DefaultView(C1)("confirmvalueusd_unitkecil") = dtTbl.DefaultView(C1)("confirmvalueusd") 
                    dtTbl.DefaultView(C1)("confirmdmidr") = varCost.ValDM_IDR
                    dtTbl.DefaultView(C1)("confirmdmusd") = varCost.ValDM_USD
                    dtTbl.DefaultView(C1)("confirmdlidr") = varCost.ValDL_IDR
                    dtTbl.DefaultView(C1)("confirmdlusd") = varCost.ValDL_USD
                    dtTbl.DefaultView(C1)("confirmohdidr") = varCost.ValFOH_IDR
                    dtTbl.DefaultView(C1)("confirmohdusd") = varCost.ValFOH_USD
                Next
               
                Dim iConfirmOid As Integer = GenerateID("QL_TRNPRODRESCONFIRM", CompnyCode)
                Dim iConMatOid As Integer = GenerateID("QL_CONSTOCK", CompnyCode)
                Dim iCrdMtrOid As Integer = GenerateID("QL_CRDSTOCK", CompnyCode)
                Dim iGLMstOid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
                Dim iGLDtlOid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
                Dim iStockValOid As Integer = GenerateID("QL_STOCKVALUE", CompnyCode)
                Dim dToday As Date = CDate(Format(GetServerTime(), "MM/dd/yyyy"))
                Dim sPeriod As String = GetDateToPeriodAcctg(dToday)
                Dim iStockWIPAcctgOid, iStockFGAcctgOid As Integer
                iStockWIPAcctgOid = GetAcctgOID(GetVarInterface("VAR_STOCK_WIP", CompnyCode), CompnyCode) 'GetAcctgStock("WIP")
                iStockFGAcctgOid = GetAcctgOID(GetVarInterface("VAR_STOCK_FG", CompnyCode), CompnyCode) 'GetAcctgStock("FG")
                Dim dTotalAmtIDR As Double = 0, dTotalAmtUSD As Double = 0, dTotalAmtIDR_unitkecil As Double = 0, dTotalAmtUSD_unitkecil As Double = 0, dQty_unitkecil As Double = 0, dQty_unitbesar As Double = 0
                Dim objTrans As SqlClient.SqlTransaction
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If
                objTrans = conn.BeginTransaction()
                xCmd.Transaction = objTrans
                Try
                    For C1 As Integer = 0 To dtTbl.DefaultView.Count - 1
                        dTotalAmtIDR = (ToDouble(dtTbl.DefaultView(C1)("confirmqty").ToString)) * ToDouble(dtTbl.DefaultView(C1)("confirmvalueidr").ToString)
                        dTotalAmtUSD = (ToDouble(dtTbl.DefaultView(C1)("confirmqty").ToString)) * ToDouble(dtTbl.DefaultView(C1)("confirmvalueusd").ToString)

                        sSql = "INSERT INTO QL_trnprodresconfirm (cmpcode, confirmoid, prodresdtloid, deptoid, confirmdate, confirmqty, confirmunitoid, confirmwhoid, confirmstatus, confirmnote, upduser, updtime, confirmvalue, confirmvalueidr, confirmvalueusd, prodresmstoid, confirmdm, confirmdmidr, confirmdmusd, confirmdl, confirmdlidr, confirmdlusd, confirmohd, confirmohdidr, confirmohdusd, womstoid, saldoawal, saldoawalidr, saldoawalusd, saldoakhir, saldoakhiridr, saldoakhirusd, confirmqty_unitkecil, confirmqty_unitbesar, confirmvalueidr_unitkecil, confirmvalueusd_unitkecil, confirmqty_sisa) VALUES ('" & dtTbl.DefaultView(C1)("cmpcode").ToString & "', " & iConfirmOid & ", " & dtTbl.DefaultView(C1)("prodresdtloid") & ", " & dtTbl.DefaultView(C1)("deptoid") & ", '" & dToday & "', " & ToDouble(dtTbl.DefaultView(C1)("confirmqty").ToString) & ", " & dtTbl.DefaultView(C1)("confirmunitoid") & ", " & dtTbl.DefaultView(C1)("confirmwhoid") & ", '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dtTbl.DefaultView(C1)("confirmvalueidr").ToString) & ", " & ToDouble(dtTbl.DefaultView(C1)("confirmvalueidr").ToString) & ", " & ToDouble(dtTbl.DefaultView(C1)("confirmvalueusd").ToString) & ", " & dtTbl.DefaultView(C1)("prodresmstoid") & ", " & ToDouble(dtTbl.DefaultView(C1)("confirmdmidr").ToString) & ", " & ToDouble(dtTbl.DefaultView(C1)("confirmdmidr").ToString) & ", " & ToDouble(dtTbl.DefaultView(C1)("confirmdmusd").ToString) & ", " & ToDouble(dtTbl.DefaultView(C1)("confirmdlidr").ToString) & ", " & ToDouble(dtTbl.DefaultView(C1)("confirmdlidr").ToString) & ", " & ToDouble(dtTbl.DefaultView(C1)("confirmdlusd").ToString) & ", " & ToDouble(dtTbl.DefaultView(C1)("confirmohdidr").ToString) & ", " & ToDouble(dtTbl.DefaultView(C1)("confirmohdidr").ToString) & ", " & ToDouble(dtTbl.DefaultView(C1)("confirmohdusd").ToString) & ", " & dtTbl.DefaultView(C1)("womstoid") & ", " & ToDouble(dtTbl.DefaultView(C1)("balanceamt_cogm").ToString) & ", " & ToDouble(dtTbl.DefaultView(C1)("balanceamt_cogm").ToString) & ", " & ToDouble(dtTbl.DefaultView(C1)("balanceamtusd_cogm").ToString) & ", " & ToDouble(dtTbl.DefaultView(C1)("balanceamt_cogm").ToString) + ToDouble(dtTbl.DefaultView(C1)("confirmdmidr").ToString) + ToDouble(dtTbl.DefaultView(C1)("confirmdlidr").ToString) + ToDouble(dtTbl.DefaultView(C1)("confirmohdidr").ToString) - dTotalAmtIDR & ", " & ToDouble(dtTbl.DefaultView(C1)("balanceamt_cogm").ToString) + ToDouble(dtTbl.DefaultView(C1)("confirmdmidr").ToString) + ToDouble(dtTbl.DefaultView(C1)("confirmdlidr").ToString) + ToDouble(dtTbl.DefaultView(C1)("confirmohdidr").ToString) - dTotalAmtIDR & ", " & ToDouble(dtTbl.DefaultView(C1)("balanceamtusd_cogm").ToString) + ToDouble(dtTbl.DefaultView(C1)("confirmdmusd").ToString) + ToDouble(dtTbl.DefaultView(C1)("confirmdlusd").ToString) + ToDouble(dtTbl.DefaultView(C1)("confirmohdusd").ToString) - dTotalAmtUSD & ", " & ToDouble(dtTbl.DefaultView(C1)("confirmqty").ToString) & ", " & ToDouble(dtTbl.DefaultView(C1)("confirmqty_unitbesar").ToString) & ", " & ToDouble(dtTbl.DefaultView(C1)("confirmvalueidr").ToString) & ", " & ToDouble(dtTbl.DefaultView(C1)("confirmvalueusd").ToString) & ", " & ToDouble(dtTbl.DefaultView(C1)("sisaqty").ToString) & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        If ToDouble(dtTbl.DefaultView(C1)("confirmqty").ToString) >= ToDouble(dtTbl.DefaultView(C1)("prodresqty").ToString) Then
                            sSql = "UPDATE QL_trnprodresdtl SET prodresdtlres1='Complete' WHERE cmpcode='" & dtTbl.DefaultView(C1)("cmpcode").ToString & "' AND prodresdtloid=" & dtTbl.DefaultView(C1)("prodresdtloid") & " AND prodresmstoid=" & dtTbl.DefaultView(C1)("prodresmstoid")
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                        sSql = "UPDATE QL_trnwomst SET balanceamt_cogm=" & ToDouble(dtTbl.DefaultView(C1)("balanceamt_cogm").ToString) + ToDouble(dtTbl.DefaultView(C1)("confirmdmidr").ToString) + ToDouble(dtTbl.DefaultView(C1)("confirmdlidr").ToString) + ToDouble(dtTbl.DefaultView(C1)("confirmohdidr").ToString) - dTotalAmtIDR & ", balanceamtusd_cogm=" & ToDouble(dtTbl.DefaultView(C1)("balanceamtusd_cogm").ToString) + ToDouble(dtTbl.DefaultView(C1)("confirmdmusd").ToString) + ToDouble(dtTbl.DefaultView(C1)("confirmdlusd").ToString) + ToDouble(dtTbl.DefaultView(C1)("confirmohdusd").ToString) - dTotalAmtUSD & ", balanceqty_cogm=" & ToDouble(dtTbl.DefaultView(C1)("balanceqty_cogm").ToString) - (ToDouble(dtTbl.DefaultView(C1)("confirmqty").ToString)) & ", lastconfirmdate=CURRENT_TIMESTAMP"
                        If ToDouble(dtTbl.DefaultView(C1)("balanceqty_cogm").ToString) - ToDouble(dtTbl.DefaultView(C1)("confirmqty").ToString) <= 0 Then
                            sSql &= ", womststatus='Closed', woclosinguser='" & Session("UserID") & "', woclosingdate=CURRENT_TIMESTAMP"
                        End If
                        sSql &= " WHERE cmpcode='" & dtTbl.DefaultView(C1)("cmpcode").ToString & "' AND womstoid=" & dtTbl.DefaultView(C1)("womstoid")
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        ' Insert Into QL_conmat
                        sSql = "INSERT INTO QL_constock (cmpcode, constockoid, contype, typemin, trndate, periodacctg, formaction, formoid, refoid, refname, mtrlocoid, qtyin, qtyout, reason, note, upduser, updtime, createuser, createtime, valueidr, valueusd, qtyin_unitbesar, qtyout_unitbesar) VALUES ('" & dtTbl.DefaultView(C1)("cmpcode").ToString & "', " & iConMatOid & ", 'PRCONF', 1, '" & dToday & "', '" & sPeriod & "', 'QL_trnprodresconfirm', " & iConfirmOid & ", " & dtTbl.DefaultView(C1)("itemoid") & ", '" & dtTbl.DefaultView(C1)("refname").ToString & "', " & dtTbl.DefaultView(C1)("confirmwhoid") & ", " & ToDouble(dtTbl.DefaultView(C1)("confirmqty").ToString) & ", 0, 'Production Result Confirmation', 'Confirm : " & dtTbl.DefaultView(C1)("prodresno").ToString & " # " & dtTbl.DefaultView(C1)("wono").ToString & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dtTbl.DefaultView(C1)("confirmvalueidr").ToString) & ", " & ToDouble(dtTbl.DefaultView(C1)("confirmvalueusd").ToString) & ", " & ToDouble(dtTbl.DefaultView(C1)("confirmqty_unitbesar").ToString) & ", 0)"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iConMatOid += 1
                        ' Insert Into QL_crdmtr
                        sSql = "UPDATE QL_crdstock SET qtyin = qtyin + " & ToDouble(dtTbl.DefaultView(C1)("confirmqty").ToString) & ", saldoakhir = saldoakhir + " & ToDouble(dtTbl.DefaultView(C1)("confirmqty").ToString) & ", qtyin_unitbesar = qtyin_unitbesar + " & ToDouble(dtTbl.DefaultView(C1)("confirmqty_unitbesar").ToString) & ", saldoakhir_unitbesar = saldoakhir_unitbesar + " & ToDouble(dtTbl.DefaultView(C1)("confirmqty_unitbesar").ToString) & ", lasttranstype='QL_trnprodresconfirm', lasttransdate='" & dToday & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & dtTbl.DefaultView(C1)("cmpcode").ToString & "' AND refoid=" & dtTbl.DefaultView(C1)("itemoid") & " AND mtrlocoid=" & dtTbl.DefaultView(C1)("confirmwhoid") & " AND periodacctg='" & sPeriod & "'"
                        xCmd.CommandText = sSql
                        If xCmd.ExecuteNonQuery() = 0 Then
                            sSql = "INSERT INTO QL_crdstock (cmpcode, crdstockoid, periodacctg, refoid, refname, mtrlocoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir, lasttranstype, lasttransdate, upduser, updtime, createuser, createtime, closeuser, closingdate, qtyin_unitbesar, qtyout_unitbesar, qtyadjin_unitbesar, qtyadjout_unitbesar, saldoawal_unitbesar, saldoakhir_unitbesar) VALUES ('" & dtTbl.DefaultView(C1)("cmpcode").ToString & "', " & iCrdMtrOid & ", '" & sPeriod & "', " & dtTbl.DefaultView(C1)("itemoid") & ", '" & dtTbl.DefaultView(C1)("refname").ToString & "', " & dtTbl.DefaultView(C1)("confirmwhoid") & ", " & ToDouble(dtTbl.DefaultView(C1)("confirmqty").ToString) & ", 0, 0, 0, 0, " & ToDouble(dtTbl.DefaultView(C1)("confirmqty").ToString) & ", 'QL_trnprodresconfirm', '" & dToday & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', '1/1/1900', " & ToDouble(dtTbl.DefaultView(C1)("confirmqty_unitbesar").ToString) & ", 0, 0, 0, 0, " & ToDouble(dtTbl.DefaultView(C1)("confirmqty_unitbesar").ToString) & ")"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iCrdMtrOid += 1
                        End If

                        'Insert Stockvalue
                        sSql = GetQueryUpdateStockValue(ToDouble(dtTbl.DefaultView(C1)("confirmqty").ToString), ToDouble(dtTbl.DefaultView(C1)("confirmqty_unitbesar").ToString), ToDouble(dtTbl.DefaultView(C1)("confirmvalueidr").ToString), ToDouble(dtTbl.DefaultView(C1)("confirmvalueusd").ToString), "QL_trnprodresconfirm", dToday, Session("UserID"), dtTbl.DefaultView(C1)("cmpcode").ToString, sPeriod, dtTbl.DefaultView(C1)("itemoid"))
                        xCmd.CommandText = sSql
                        If xCmd.ExecuteNonQuery() = 0 Then
                            sSql = GetQueryInsertStockValue(ToDouble(dtTbl.DefaultView(C1)("confirmqty").ToString), ToDouble(dtTbl.DefaultView(C1)("confirmqty_unitbesar").ToString), ToDouble(dtTbl.DefaultView(C1)("confirmvalueidr").ToString), ToDouble(dtTbl.DefaultView(C1)("confirmvalueusd").ToString), "QL_trnprodresconfirm", dToday, Session("UserID"), dtTbl.DefaultView(C1)("cmpcode").ToString, sPeriod, dtTbl.DefaultView(C1)("itemoid"), iStockValOid)
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iStockValOid += 1
                        End If

                        If ToDouble(dtTbl.DefaultView(C1)("sisaqty").ToString) > 0 Then
                            ' Insert Into QL_conmat
                            sSql = "INSERT INTO QL_constock (cmpcode, constockoid, contype, typemin, trndate, periodacctg, formaction, formoid, refoid, refname, mtrlocoid, qtyin, qtyout, reason, note, upduser, updtime, createuser, createtime, valueidr, valueusd, qtyin_unitbesar, qtyout_unitbesar) VALUES ('" & dtTbl.DefaultView(C1)("cmpcode").ToString & "', " & iConMatOid & ", 'PRCONF', 1, '" & dToday & "', '" & sPeriod & "', 'QL_trnprodresconfirm', " & iConfirmOid & ", " & dtTbl.DefaultView(C1)("itemoid") & ", '" & dtTbl.DefaultView(C1)("refname").ToString & "', " & iWHSisaProd & ", " & ToDouble(dtTbl.DefaultView(C1)("sisaqty").ToString) & ", 0, 'Production Result Confirmation', 'Confirm : " & dtTbl.DefaultView(C1)("prodresno").ToString & " # " & dtTbl.DefaultView(C1)("wono").ToString & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dtTbl.DefaultView(C1)("confirmvalueidr").ToString) & ", " & ToDouble(dtTbl.DefaultView(C1)("confirmvalueusd").ToString) & ", " & ToDouble(dtTbl.DefaultView(C1)("sisaqty_unitbesar").ToString) & ", 0)"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iConMatOid += 1
                            ' Insert Into QL_crdmtr
                            sSql = "UPDATE QL_crdstock SET qtyin = qtyin + " & ToDouble(dtTbl.DefaultView(C1)("sisaqty").ToString) & ", saldoakhir = saldoakhir + " & ToDouble(dtTbl.DefaultView(C1)("sisaqty").ToString) & ", qtyin_unitbesar = qtyin_unitbesar + " & ToDouble(dtTbl.DefaultView(C1)("sisaqty_unitbesar").ToString) & ", saldoakhir_unitbesar = saldoakhir_unitbesar + " & ToDouble(dtTbl.DefaultView(C1)("sisaqty_unitbesar").ToString) & ", lasttranstype='QL_trnprodresconfirm', lasttransdate='" & dToday & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & dtTbl.DefaultView(C1)("cmpcode").ToString & "' AND refoid=" & dtTbl.DefaultView(C1)("itemoid") & " AND mtrlocoid=" & iWHSisaProd & " AND periodacctg='" & sPeriod & "'"
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() = 0 Then
                                sSql = "INSERT INTO QL_crdstock (cmpcode, crdstockoid, periodacctg, refoid, refname, mtrlocoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir, lasttranstype, lasttransdate, upduser, updtime, createuser, createtime, closeuser, closingdate, qtyin_unitbesar, qtyout_unitbesar, qtyadjin_unitbesar, qtyadjout_unitbesar, saldoawal_unitbesar, saldoakhir_unitbesar) VALUES ('" & dtTbl.DefaultView(C1)("cmpcode").ToString & "', " & iCrdMtrOid & ", '" & sPeriod & "', " & dtTbl.DefaultView(C1)("itemoid") & ", '" & dtTbl.DefaultView(C1)("refname").ToString & "', " & iWHSisaProd & ", " & ToDouble(dtTbl.DefaultView(C1)("sisaqty").ToString) & ", 0, 0, 0, 0, " & ToDouble(dtTbl.DefaultView(C1)("sisaqty").ToString) & ", 'QL_trnprodresconfirm', '" & dToday & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', '1/1/1900', " & ToDouble(dtTbl.DefaultView(C1)("sisaqty_unitbesar").ToString) & ", 0, 0, 0, 0, " & ToDouble(dtTbl.DefaultView(C1)("sisaqty_unitbesar").ToString) & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iCrdMtrOid += 1
                            End If

                            'Insert Stockvalue
                            sSql = GetQueryUpdateStockValue(ToDouble(dtTbl.DefaultView(C1)("sisaqty").ToString), ToDouble(dtTbl.DefaultView(C1)("sisaqty_unitbesar").ToString), ToDouble(dtTbl.DefaultView(C1)("confirmvalueidr").ToString), ToDouble(dtTbl.DefaultView(C1)("confirmvalueusd").ToString), "QL_trnprodresconfirm", dToday, Session("UserID"), dtTbl.DefaultView(C1)("cmpcode").ToString, sPeriod, dtTbl.DefaultView(C1)("itemoid"))
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() = 0 Then
                                sSql = GetQueryInsertStockValue(ToDouble(dtTbl.DefaultView(C1)("sisaqty").ToString), ToDouble(dtTbl.DefaultView(C1)("sisaqty_unitbesar").ToString), ToDouble(dtTbl.DefaultView(C1)("confirmvalueidr").ToString), ToDouble(dtTbl.DefaultView(C1)("confirmvalueusd").ToString), "QL_trnprodresconfirm", dToday, Session("UserID"), dtTbl.DefaultView(C1)("cmpcode").ToString, sPeriod, dtTbl.DefaultView(C1)("itemoid"), iStockValOid)
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iStockValOid += 1
                            End If
                        End If

                        If dTotalAmtIDR > 0 Or dTotalAmtUSD > 0 Then
                            ' Insert QL_trnglmst
                            sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGLMstOid & ", '" & sDate & "', '" & sPeriod & "', 'Confirm ProdRes|No. " & dtTbl.DefaultView(C1)("prodresno").ToString & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 0, 0, 1, 1, 0, 0)"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            ' Insert QL_trngldtl
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGLDtlOid & ", 1, " & iGLMstOid & ", " & iStockFGAcctgOid & ", 'D', " & dTotalAmtIDR & ", '" & dtTbl.DefaultView(C1)("prodresno").ToString & "', 'Confirm ProdRes|No. " & dtTbl.DefaultView(C1)("prodresno").ToString & " (SPK No. " & dtTbl.DefaultView(C1)("wono").ToString & ")', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalAmtIDR & ", " & dTotalAmtUSD & ", 'QL_trnprodresconfirm " & iConfirmOid & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iGLDtlOid += 1
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGLDtlOid & ", 2, " & iGLMstOid & ", " & iStockWIPAcctgOid & ", 'C', " & dTotalAmtIDR & ", '" & dtTbl.DefaultView(C1)("prodresno").ToString & "', 'Confirm ProdRes|No. " & dtTbl.DefaultView(C1)("prodresno").ToString & " (SPK No. " & dtTbl.DefaultView(C1)("wono").ToString & ")', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalAmtIDR & ", " & dTotalAmtUSD & ", 'QL_trnprodresconfirm " & iConfirmOid & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iGLDtlOid += 1
                            iGLMstOid += 1
                        End If
                        iConfirmOid += 1
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iConfirmOid - 1 & " WHERE tablename='QL_TRNPRODRESCONFIRM' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iConMatOid - 1 & " WHERE tablename='QL_CONSTOCK' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iCrdMtrOid - 1 & " WHERE tablename='QL_CRDSTOCK' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGLMstOid - 1 & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGLDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iStockValOid - 1 & " WHERE tablename='QL_STOCKVALUE' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    objTrans.Commit()
                    conn.Close()
                Catch ex As Exception
                    objTrans.Rollback()
                    showMessage(ex.ToString, 1)
                    conn.Close()
                    Exit Sub
                End Try
                dtTbl.DefaultView.RowFilter = ""
                Response.Redirect("~\Transaction\trnProdConf.aspx?awal=true")
            Else
                showMessage("Please show Production Result data first!", 2)
            End If
        Else
            showMessage("Please show Production Result data first!", 2)
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnProdConf.aspx?awal=true")
    End Sub

    Protected Sub lbViewDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbViewDetail.Click
        Dim dtTbl As DataTable = Session("TblDtl")
        UpdateTblDtl(dtTbl)
        Session("TblDtl") = dtTbl
        PrintOutstandingTrans()
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub tbQty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim dtTbl As DataTable = Session("TblDtl")
        'UpdateTblDtl(dtTbl)
        'Dim dv As DataView = dtTbl.DefaultView
        'dv.RowFilter = "checkvalue='True'"
        'If dv.Count > 0 Then
        '    Dim dQty_unitkecil As Double, dQty_unitbesar As Double, dQty As Double
        '    For C1 As Integer = 0 To dv.Count - 1
        '        dQty = ToDouble(dv(C1)("prodresqty")) - ToDouble(dv(C1)("confirmqty"))
        '        GetUnitConverter(dv(C1)("wodtl1itemoid"), dv(C1)("confunitoid"), dQty, dQty_unitkecil, dQty_unitbesar)
        '        If dQty > 0 Then
        '            dv(C1)("sisaqty") = dQty_unitkecil
        '            dv(C1)("sisaqty_unitbesar") = dQty_unitbesar
        '            dv(C1)("sisaqty_real") = dQty
        '        End If
        '    Next
        '    Session("TblDtl") = dv.ToTable
        '    gvDtl.DataSource = Session("TblDtl")
        '    gvDtl.DataBind()
        'End If
    End Sub

    Protected Sub cbSetWH_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If cbSetWH.Checked Then
            gvDtl.Columns(9).Visible = False
        Else
            gvDtl.Columns(9).Visible = True
        End If
    End Sub

    Protected Sub cbSelectItem_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        tbQty_TextChanged(Nothing, Nothing)
    End Sub
#End Region

End Class