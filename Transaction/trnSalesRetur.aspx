<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnSalesRetur.aspx.vb" Inherits="Transaction_SalesRetur" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Sales Return" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Sales Return :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 10%" class="Label" align=left><asp:Label id="Label4" runat="server" Text="Filter"></asp:Label></TD><TD style="WIDTH: 2%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="sretm.sretmstoid">Draft No.</asp:ListItem>
<asp:ListItem Value="sretno">Return No.</asp:ListItem>
<asp:ListItem Value="c.custname">Customer</asp:ListItem>
<asp:ListItem Value="trnjualno">Jual No.</asp:ListItem>
<asp:ListItem Value="sretmstnote">Note</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="200px"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text="Periode"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="60px" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label5" runat="server" Text="to"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="60px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label9" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR>
    <tr>
        <td align="left" class="Label">
            <asp:CheckBox ID="cbType" runat="server" Text="Type" /></td>
        <td align="center" class="Label">
            :</td>
        <td align="left" class="Label" colspan="2">
            <asp:DropDownList id="DDLFilterType" runat="server" CssClass="inpText" Width="100px">
                <asp:ListItem Value="ITEM">PAYMENT</asp:ListItem>
                <asp:ListItem Value="DP">DOWN PAYMENT</asp:ListItem>
                <asp:ListItem Value="CASH">CASH BACK</asp:ListItem>
            </asp:DropDownList></td>
    </tr>
    <TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="100px">
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>In Approval</asp:ListItem>
    <asp:ListItem>Approved</asp:ListItem>
    <asp:ListItem>Revised</asp:ListItem>
    <asp:ListItem>Rejected</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" Format="MM/dd/yyyy" PopupButtonID="imbDate1" TargetControlID="FilterPeriod1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="FilterPeriod1" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" Format="MM/dd/yyyy" PopupButtonID="imbDate2" TargetControlID="FilterPeriod2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" TargetControlID="FilterPeriod2" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPrintHdr" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:LinkButton id="lbInProcess" runat="server" Visible="False"></asp:LinkButton> <asp:LinkButton id="lbInApproval" runat="server" Visible="False"></asp:LinkButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvList" runat="server" ForeColor="#333333" Width="100%" PageSize="8" AutoGenerateColumns="False" DataKeyNames="sretmstoid" CellPadding="4" GridLines="None" AllowPaging="True" AllowSorting="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="sretmstoid" DataNavigateUrlFormatString="~\Transaction\trnSalesRetur.aspx?oid={0}" DataTextField="sretmstoid" HeaderText="Draft No." SortExpression="sretmstoid">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="sretno" HeaderText="Return No." SortExpression="sretno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sretdate" HeaderText="Return Date" SortExpression="sretdate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer" SortExpression="custname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualno" HeaderText="Jual No." SortExpression="trnjualno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="srettype" HeaderText="Return Type" SortExpression="srettype">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sretmststatus" HeaderText="Status" SortExpression="sretmststatus">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sretmstnote" HeaderText="Note" SortExpression="sretmstnote">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="divname" HeaderText="Business Unit" SortExpression="divname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reason" HeaderText="Reason">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:CommandField SelectImageUrl="~/Images/print.gif" ShowSelectButton="True" ButtonType="Image">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True" Font-Italic="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="No Data Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="gvList"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnPrintHdr"></asp:PostBackTrigger>
</triggers>
                           
                           
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label16" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Sales Return Header" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 13%" class="Label" align=left><asp:Label id="i_u" runat="server" CssClass="Important" Text="New Data"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 30%" class="Label" align=left><asp:Label id="periodacctg" runat="server" Visible="False"></asp:Label>&nbsp;<asp:Label id="custoid" runat="server" Visible="False"></asp:Label> <asp:Label id="curroid" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 13%" class="Label" align=left></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="shipmentrawmstoid" runat="server" Visible="False"></asp:Label> <asp:Label id="tipe" runat="server" Visible="False"></asp:Label> <asp:Label id="lblDisc" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD id="TD3" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label2" runat="server" Text="Business Unit" Visible="False"></asp:Label></TD><TD id="TD1" class="Label" align=center runat="server" Visible="false"></TD><TD id="TD6" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpText" Visible="False" AutoPostBack="True"></asp:DropDownList></TD><TD id="TD4" class="Label" align=left rowSpan=1 runat="server" Visible="false"><asp:Label id="Label31" runat="server" Text="Inv. Type" Visible="False"></asp:Label></TD><TD id="TD2" class="Label" align=center rowSpan=1 runat="server" Visible="false"></TD><TD id="TD5" class="Label" align=left rowSpan=1 runat="server" Visible="false"><asp:DropDownList id="invType" runat="server" CssClass="inpTextDisabled" Width="165px" Visible="False" AutoPostBack="True" Enabled="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblTrnNo" runat="server" Text="Draft No."></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:Label id="sretrawmstoid" runat="server"></asp:Label><asp:TextBox id="sretrawno" runat="server" CssClass="inpTextDisabled" Width="160px" Visible="False" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left rowSpan=1><asp:Label id="Label6" runat="server" Text="Return Date"></asp:Label></TD><TD class="Label" align=center rowSpan=1>:</TD><TD class="Label" align=left rowSpan=1><asp:TextBox id="sretrawdate" runat="server" CssClass="inpText" Width="70px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="ibsodate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Width="16px" Height="16px"></asp:ImageButton> <asp:Label id="Label55" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" Height="16px"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label22" runat="server" Text="Customer"></asp:Label> <asp:Label id="Label18" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custname" runat="server" CssClass="inpTextDisabled" Width="160px" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchCust" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearCust" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD class="Label" align=left rowSpan=1><asp:Label id="Label29" runat="server" Text="Nota Type"></asp:Label></TD><TD class="Label" align=center rowSpan=1>:</TD><TD class="Label" align=left rowSpan=1><asp:DropDownList id="DDLTypeNota" runat="server" CssClass="inpText" Width="165px" AutoPostBack="True" OnSelectedIndexChanged="DDLTypeNota_SelectedIndexChanged"><asp:ListItem>NOTA BARU</asp:ListItem>
<asp:ListItem>NOTA LAMA</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label24" runat="server" Text="No. Invoice"></asp:Label> <asp:Label id="Label27" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="shipmentrawno" runat="server" CssClass="inpTextDisabled" Width="160px" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSearchShip" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClearShip" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD class="Label" align=left rowSpan=1><asp:Label id="Label3" runat="server" Text="Invoice Date"></asp:Label></TD><TD class="Label" align=center rowSpan=1>:</TD><TD class="Label" align=left rowSpan=1><asp:TextBox id="invDate" runat="server" CssClass="inpTextDisabled" Width="70px" Enabled="False"></asp:TextBox> <asp:TextBox id="curr" runat="server" CssClass="inpTextDisabled" Width="90px" Visible="False" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label19" runat="server" Text="Total Retur"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="totalRetur" runat="server" CssClass="inpTextDisabled" Width="90px" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left rowSpan=1><asp:Label id="Label14" runat="server" Text="No. Ref"></asp:Label> <asp:Label id="Label30" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center rowSpan=1>:</TD><TD class="Label" align=left rowSpan=1><asp:TextBox id="sretRefNo" runat="server" CssClass="inpText" Width="160px" MaxLength="19"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label7" runat="server" Text="Disc Hdr." __designer:wfdid="w3"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="sretmstdisctype" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w4"><asp:ListItem Value="P">Percentage</asp:ListItem>
<asp:ListItem Value="A">Amount</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="sretmstdiscvalue" runat="server" CssClass="inpTextDisabled" Width="72px" Enabled="False" __designer:wfdid="w5"></asp:TextBox></TD><TD class="Label" align=left rowSpan=1><asp:Label id="Label33" runat="server" Text="Disc Hdr. Amt" __designer:wfdid="w3"></asp:Label></TD><TD class="Label" align=center rowSpan=1>:</TD><TD class="Label" align=left rowSpan=1><asp:TextBox id="sretmstdiscamt" runat="server" CssClass="inpTextDisabled" Width="90px" Enabled="False" __designer:wfdid="w7"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label32" runat="server" Text="Total Disc"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="discHdr" runat="server" CssClass="inpTextDisabled" Width="90px" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left rowSpan=1><asp:Label id="Label23" runat="server" Text="Tax Amount"></asp:Label></TD><TD class="Label" align=center rowSpan=1>:</TD><TD class="Label" align=left rowSpan=1><asp:TextBox id="taxAmt" runat="server" CssClass="inpTextDisabled" Width="90px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblReason" runat="server" Text="Reason Type" Visible="False"></asp:Label></TD><TD class="Label" align=center><asp:Label id="ttkReason" runat="server" Text=":" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="sretrawmstres2" runat="server" CssClass="inpText" Width="165px" Visible="False" AutoPostBack="True"></asp:DropDownList> <asp:Label id="lunas" runat="server" CssClass="Important" Visible="False" Height="16px"></asp:Label></TD><TD class="Label" align=left rowSpan=1><asp:Label id="Label20" runat="server" Text="Total Retur Netto"></asp:Label></TD><TD class="Label" align=center rowSpan=1>:</TD><TD class="Label" align=left rowSpan=1><asp:TextBox id="totalReturNetto" runat="server" CssClass="inpTextDisabled" Width="90px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label8" runat="server" Text="Header Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="sretrawmstnote" runat="server" CssClass="inpText" Width="200px" MaxLength="99"></asp:TextBox></TD><TD class="Label" align=left rowSpan=1><asp:Label id="Label25" runat="server" Text="Status"></asp:Label></TD><TD class="Label" align=center rowSpan=1>:</TD><TD class="Label" align=left rowSpan=1><asp:TextBox id="sretrawmststatus" runat="server" CssClass="inpTextDisabled" Width="90px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:CalendarExtender id="ceDate3" runat="server" Format="MM/dd/yyyy" PopupButtonID="ibsodate" TargetControlID="sretrawdate"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeDate3" runat="server" TargetControlID="sretrawdate" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><asp:Label id="Label17" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Sales Return Detail" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="i_u2" runat="server" Text="New Detail" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="sretrawdtlseq" runat="server" Visible="False">1</asp:Label> <asp:Label id="sretrawdtloid" runat="server" Visible="False"></asp:Label> <asp:Label id="shipmentrawdtloid" runat="server" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:Label id="matrawoid" runat="server" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="matrawcode" runat="server" Visible="False"></asp:Label>&nbsp; <asp:Label id="sretrawwhoid" runat="server" Visible="False"></asp:Label> <asp:Label id="price" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label11" runat="server" Text="Material"></asp:Label><asp:Label id="Label10" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="matrawlongdesc" runat="server" CssClass="inpTextDisabled" Width="185px" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchMat" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="disc1" runat="server" Visible="False"></asp:Label> <asp:Label id="disc2" runat="server" Visible="False"></asp:Label> <asp:Label id="disc3" runat="server" Visible="False"></asp:Label> <asp:Label id="oldcode" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label21" runat="server" Text="Qty"></asp:Label> <asp:Label id="Label12" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="sretrawqty" runat="server" CssClass="inpText" Width="60px" AutoPostBack="True" MaxLength="12" OnTextChanged="sretrawqty_TextChanged"></asp:TextBox> <asp:DropDownList id="sretrawunitoid" runat="server" CssClass="inpTextDisabled" Width="95px" AutoPostBack="True" Enabled="False"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="MaxQty" runat="server" Text="Invoice Qty"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="shipmentrawqty" runat="server" CssClass="inpTextDisabled" Width="90px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label26" runat="server" Text="Promo Disc"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="promoDisc" runat="server" CssClass="inpTextDisabled" Width="90px" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label28" runat="server" Text="Detail Amount"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="dtlAmt" runat="server" CssClass="inpTextDisabled" Width="90px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label15" runat="server" Text="Detail Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="sretrawdtlnote" runat="server" CssClass="inpText" Width="200px" MaxLength="99"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label13" runat="server" Text="Round Qty" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="matrawlimitqty" runat="server" CssClass="inpTextDisabled" Width="60px" Visible="False" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="ftbQty" runat="server" TargetControlID="sretrawqty" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Panel id="Panel2" runat="server" CssClass="inpText" Width="100%" Height="200px" ScrollBars="Vertical"><asp:GridView id="gvListDtl" runat="server" ForeColor="#333333" Width="100%" PageSize="5" AutoGenerateColumns="False" DataKeyNames="sretdtlseq" CellPadding="4" GridLines="None">
<RowStyle BackColor="#E3EAEB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="oldcode" HeaderText="Old Code">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="jualqty" HeaderText="Jual Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sretqty" HeaderText="Retur Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sretunit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sretvalueidr" HeaderText="Price">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="sretamt" HeaderText="Detail Amt">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="sretdtlnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sretunitoid" HeaderText="unitoid" Visible="False"></asp:BoundField>
<asp:CommandField DeleteImageUrl="~/Images/del.jpeg" DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="disc1" HeaderText="disc1" Visible="False"></asp:BoundField>
<asp:BoundField DataField="disc2" HeaderText="disc2" Visible="False"></asp:BoundField>
<asp:BoundField DataField="disc3" HeaderText="disc3" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#666666" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#7C6F57"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel></TD></TR><TR><TD class="Label" align=left colSpan=6></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left colSpan=6>Created By <asp:Label id="createuser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left colSpan=6>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton> <asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton id="btnSendApproval" runat="server" ImageUrl="~/Images/sendapproval.png" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnShowCOA" onclick="btnShowCOA_Click" runat="server" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsMiddle" Visible="False" AlternateText="Show COA"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=6><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center">&nbsp;</DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Sales Return :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upListCust" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlListCust" runat="server" CssClass="modalBox" Visible="False" Width="650px">
                <table width="100%">
                    <tbody>
                        <tr>
                            <td align="center" class="Label" colspan="3">
                                <asp:Label ID="lblListCust" runat="server" Font-Bold="True" Font-Size="Medium" Text="List of Customer"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="center" class="Label" colspan="3" style="height: 5px">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="Label" colspan="3" valign="top">
                                <asp:Panel ID="pnlFilterListCust" runat="server" DefaultButton="btnFindListCust"
                                    Width="100%">
                                    Filter :
                                    <asp:DropDownList ID="FilterDDLListCust" runat="server" CssClass="inpText" Width="100px">
                                        <asp:ListItem Value="custcode">Code</asp:ListItem>
                                        <asp:ListItem Value="custname">Name</asp:ListItem>
                                        <asp:ListItem Value="custaddr">Address</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:TextBox ID="FilterTextListCust" runat="server" CssClass="inpText" Width="150px"></asp:TextBox>
                                    <asp:ImageButton ID="btnFindListCust" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/btnfind.bmp" />
                                    <asp:ImageButton ID="btnAllListCust" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewall.png" /></asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="Label" colspan="3" style="height: 5px" valign="top">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="Label" colspan="3" valign="top">
                                <asp:GridView ID="gvListCust" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                    CellPadding="4" DataKeyNames="custoid,custname" ForeColor="#333333" GridLines="None"
                                    PageSize="5" Width="98%">
                                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="True">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:CommandField>
                                        <asp:BoundField DataField="custcode" HeaderText="Code">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="custname" HeaderText="Name">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="custaddr" HeaderText="Address">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" Width="40%" />
                                            <ItemStyle HorizontalAlign="Left" Width="40%" />
                                        </asp:BoundField>
                                    </Columns>
                                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle CssClass="gvpopup" Font-Bold="True" ForeColor="White" HorizontalAlign="Right" />
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="Label" colspan="3" style="height: 5px" valign="top">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3">
                                <asp:LinkButton ID="lkbCloseListCust" runat="server">[ Cancel & Close ]</asp:LinkButton></td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpeListCust" runat="server" BackgroundCssClass="modalBackground"
                Drag="True" PopupControlID="pnlListCust" PopupDragHandleControlID="lblListCust"
                TargetControlID="btnHideListCust">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="btnHideListCust" runat="server" Visible="False" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upListShip" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListShip" runat="server" CssClass="modalBox" Width="800px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListShip" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of lnvoice" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListShip" runat="server" Width="100%" DefaultButton="btnFindListShip">Filter : <asp:DropDownList id="FilterDDLListShip" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="trnjualno">Invoice No.</asp:ListItem>
<asp:ListItem Value="trnjualnote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListShip" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListShip" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListShip" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListShip" runat="server" ForeColor="#333333" Width="98%" GridLines="None" CellPadding="4" DataKeyNames="trnjualmstoid,trnjualno,trnjualdate,domstoid,trnjualamt,trntaxamt,trnjualamtnetto,trnjualres1,trndiscamt,tipe,trndiscvalue" AutoGenerateColumns="False" PageSize="5" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnjualno" HeaderText="Invoice No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualdate" HeaderText="Invoice Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="domstoid" HeaderText="domstoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="trnjualamt" HeaderText="trnjualamt" Visible="False"></asp:BoundField>
<asp:BoundField DataField="trntaxamt" HeaderText="trnjualtaxamt" Visible="False"></asp:BoundField>
<asp:BoundField DataField="trnjualamtnetto" HeaderText="trnjualamtnetto" Visible="False"></asp:BoundField>
<asp:BoundField DataField="trnjualres1" HeaderText="invType" Visible="False"></asp:BoundField>
<asp:BoundField DataField="trndiscamt" HeaderText="disc" Visible="False"></asp:BoundField>
<asp:BoundField DataField="tipe" HeaderText="tipe" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                                                <asp:Label ID="Label114" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListShip" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListShip" runat="server" TargetControlID="btnHideListShip" PopupDragHandleControlID="lblListShip" PopupControlID="pnlListShip" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListShip" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upListMat" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListMat" runat="server" CssClass="modalBox" Width="1000px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Item" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMat" runat="server" Width="100%" DefaultButton="btnFindListMat">Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="itemCode">Code</asp:ListItem>
<asp:ListItem Value="oldcode">Old Code</asp:ListItem>
<asp:ListItem Value="itemdesc">Description</asp:ListItem>
<asp:ListItem Value="unit">Unit</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" CssClass="inpText" Width="200px"></asp:TextBox> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton><BR /><asp:ImageButton id="btnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnSelectNone" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewChecked" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat" runat="server" ForeColor="#333333" Width="100%" OnSelectedIndexChanged="gvListMat_SelectedIndexChanged" GridLines="None" CellPadding="4" DataKeyNames="trnjualmstoid,trnjualno,trnjualdate,domstoid,trnjualamt,trntaxamt,trnjualamtnetto,trnjualres1,trndiscamt,tipe" AutoGenerateColumns="False" PageSize="5" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbOnListMat" runat="server" ToolTip='<%# eval("itemoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="oldcode" HeaderText="Old Code">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="jualqty" HeaderText="SI Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Qty"><ItemTemplate>
<asp:TextBox id="tbQtyOnListMat" runat="server" CssClass="inpText" Text='<%# eval("retqty") %>' Width="50px" MaxLength="12"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftbQtyOnListMat" runat="server" TargetControlID="tbQtyOnListMat" ValidChars="1234567890,.">
            </ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Price"><ItemTemplate>
<asp:TextBox id="tbAmtOnListMat" runat="server" CssClass="inpText" Text='<%# eval("jualprice") %>' Width="80px" MaxLength="12"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftbAmtOnListMat" runat="server" TargetControlID="tbAmtOnListMat" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender>
</ItemTemplate>

<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Note"><ItemTemplate>
<asp:TextBox id="tbNoteOnListMat" runat="server" CssClass="inpText" Text='<%# eval("retdtlnote") %>' Width="150px" MaxLength="100"></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="tax" HeaderText="tax" Visible="False"></asp:BoundField>
<asp:TemplateField HeaderText="HPP"><ItemTemplate>
<asp:TextBox id="tbAmtValue" runat="server" CssClass="inpText" Text='<%# eval("valueidr") %>' Width="80px" MaxLength="12"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftbAmtValue" runat="server" TargetControlID="tbAmtValue" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender>
</ItemTemplate>

<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="trnjualno" HeaderText="Invoice No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField SelectText="Select Invoice No." ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label114" runat="server" Text="No data found !!" CssClass="Important"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbAddToListListMat" runat="server">[ Add To List ]</asp:LinkButton>&nbsp;<asp:LinkButton id="lkbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE><ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" PopupDragHandleControlID="lblTitleListMat" PopupControlID="pnlListMat" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender></asp:Panel> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upCOAPosting" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlCOAPosting" runat="server" CssClass="modalBox" Visible="False"
                Width="600px">
                <table style="width: 100%">
                    <tbody>
                        <tr>
                            <td align="center" class="Label" colspan="3">
                                <asp:Label ID="lblCOAPosting" runat="server" Font-Bold="True" Font-Size="Medium"
                                    Text="COA - Posting"></asp:Label>
                                <asp:DropDownList ID="DDLRateType" runat="server" AutoPostBack="True" CssClass="inpText"
                                    Font-Bold="True" Font-Size="Medium" Visible="False" Width="125px">
                                    <asp:ListItem Value="Default">Rate Default</asp:ListItem>
                                    <asp:ListItem Value="IDR">Rate To IDR</asp:ListItem>
                                    <asp:ListItem Value="USD">Rate To USD</asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td align="center" class="Label" colspan="3" style="height: 5px">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="Label" colspan="3">
                                <asp:GridView ID="gvCOAPosting" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                    ForeColor="#333333" GridLines="None" OnRowDataBound="gvCOAPosting_RowDataBound"
                                    Width="99%">
                                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                    <Columns>
                                        <asp:BoundField DataField="acctgcode" HeaderText="Account No.">
                                            <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="acctgdesc" HeaderText="Account Desc.">
                                            <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="acctgdebet" HeaderText="Debet">
                                            <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Right" Width="15%" />
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="acctgcredit" HeaderText="Credit">
                                            <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Right" Width="15%" />
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" />
                                        </asp:BoundField>
                                    </Columns>
                                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        <asp:Label ID="Label13" runat="server" ForeColor="Red" Text="No Data Found"></asp:Label>
                                    </EmptyDataTemplate>
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="Label" colspan="3" style="height: 5px">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3">
                                <asp:LinkButton ID="lkbCloseCOAPosting" runat="server" OnClick="lkbCloseCOAPosting_Click">[ CLOSE ]</asp:LinkButton></td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <asp:Button ID="btnHideCOAPosting" runat="server" Visible="False" /><ajaxToolkit:ModalPopupExtender
                ID="mpeCOAPosting" runat="server" BackgroundCssClass="modalBackground" Drag="True"
                PopupControlID="pnlCOAPosting" PopupDragHandleControlID="lblCOAPosting" TargetControlID="btnHideCOAPosting">
            </ajaxToolkit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

