<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnKomisiSales.aspx.vb" Inherits="Transaction_KomisiSales" title="" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".:  Komisi Sales" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Komisi Sales :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                <asp:UpdatePanel id="upReqCancel" runat="server">
                    <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3></TD></TR><TR><TD class="Label" align=left colSpan=3><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 14%" class="Label"></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label"><asp:Label id="matreqmstoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label"><asp:Label id="Label8" runat="server" Text="Business Unit" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label"><asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:DropDownList> <asp:TextBox id="tbDate" runat="server" CssClass="inpText" Width="75px" Visible="False" ToolTip="(MM/dd/yyyy)"></asp:TextBox> <asp:ImageButton id="btnDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:Label id="Label4" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" Visible="False"></asp:Label></TD></TR><TR><TD class="Label"><asp:Label id="Label2" runat="server" Text="Period"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label"><asp:DropDownList id="DDLMonth2" runat="server" CssClass="inpText" Width="100px">
            </asp:DropDownList>&nbsp;<asp:DropDownList id="DDLYear2" runat="server" CssClass="inpText" Width="75px">
            </asp:DropDownList></TD></TR><TR><TD class="Label"><asp:Label id="Label3" runat="server" Text="Sales"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label"><asp:DropDownList id="DDLsales" runat="server" CssClass="inpText" Width="200px" OnSelectedIndexChanged="DDLsales_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label"></TD><TD class="Label" align=center></TD><TD class="Label"><ajaxToolkit:CalendarExtender id="ceDate" runat="server" Format="MM/dd/yyyy" PopupButtonID="btnDate" TargetControlID="tbDate">
            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeDate" runat="server" TargetControlID="tbDate" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear">
            </ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD class="Label"></TD><TD class="Label" align=center></TD><TD class="Label"><asp:ImageButton id="btnShowData" runat="server" ImageUrl="~/Images/showdata.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Label id="Label9" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Transaksi Data Sales :"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Panel id="pnlGVDetail" runat="server" CssClass="inpText" Width="100%" Height="180px" ScrollBars="Vertical"><asp:GridView id="gvDtl" runat="server" ForeColor="#333333" Width="98%" PageSize="5" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#EFF3FB"></RowStyle>
<Columns>
<asp:TemplateField><HeaderTemplate>
                                <asp:CheckBox ID="cbHeader" runat="server" AutoPostBack="True" OnCheckedChanged="cbHeader_CheckedChanged" />
                            
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="cbField" runat="server" ToolTip='<%# eval("nmr") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="nmr" HeaderText="No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualno" HeaderText="Invoice No.">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
    <asp:BoundField DataField="custname" HeaderText="Customer">
        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
        <ItemStyle HorizontalAlign="Left" />
    </asp:BoundField>
<asp:BoundField DataField="tglinv" HeaderText="Invoice Date.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
    <asp:BoundField DataField="trnjualnetto" HeaderText="Invoice">
        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Right" />
        <ItemStyle HorizontalAlign="Right" />
    </asp:BoundField>
<asp:BoundField DataField="netto" HeaderText="Total Bayar">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="komisi" HeaderText="Komisi (%)">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="komisisales" HeaderText="Komisi Sales">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="nama" HeaderText="Sales" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                        <asp:Label ID="Label7" runat="server" ForeColor="Red" Text="No detail data !!"></asp:Label>
                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </asp:Panel> </TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=3></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left colSpan=3>Processed By <asp:Label id="upduser" runat="server"></asp:Label>&nbsp;On <asp:Label id="updtime" runat="server"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:ImageButton id="btnPosting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upReqCancel">
                                <ProgressTemplate>
                                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                    </div>
                                    <div id="processMessage" class="processMessage">
                                        <span style="font-weight: bold; font-size: 10pt; color: purple">
                                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />
                                            Please Wait .....</span><br />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress> </DIV>
</contenttemplate>
                </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>