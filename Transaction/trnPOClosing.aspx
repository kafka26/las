<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnPOClosing.aspx.vb" Inherits="Transaction_POClosing" title="" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: PO Closing" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form PO Closing :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                <asp:UpdatePanel id="upPOClosing" runat="server">
                    <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left colSpan=3><TABLE style="WIDTH: 100%"><TBODY><TR><TD id="BusinessUnit1" class="Label" visible="false"><asp:Label id="Label8" runat="server" Text="Business Unit" Visible="False"></asp:Label></TD><TD id="BusinessUnit2" class="Label" align=center visible="false"></TD><TD id="BusinessUnit3" class="Label" visible="false"><asp:DropDownList id="pobusinessunit" runat="server" CssClass="inpTextDisabled" Enabled="False" AutoPostBack="True" Visible="False"></asp:DropDownList></TD><TD id="TD3" class="Label" runat="server" Visible="false"></TD><TD id="TD1" class="Label" align=center runat="server" Visible="false"></TD><TD id="TD2" class="Label" runat="server" Visible="false"></TD></TR><TR><TD class="Label"><asp:Label id="Label4" runat="server" Text="Type" __designer:wfdid="w2"></asp:Label> </TD><TD class="Label" align=center>:</TD><TD class="Label"><asp:DropDownList id="ddlType" runat="server" CssClass="inpText" __designer:wfdid="w1"></asp:DropDownList>&nbsp;&nbsp; </TD><TD class="Label"><asp:Label id="Label2" runat="server" Text="PO No" __designer:wfdid="w3"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label"><asp:TextBox id="pono" runat="server" CssClass="inpTextDisabled" Width="120px" Enabled="False" __designer:wfdid="w4"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindPO" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w5"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbErasePO" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w6"></asp:ImageButton> <asp:Label id="pomstoid" runat="server" Visible="False" __designer:wfdid="w1"></asp:Label></TD></TR><TR><TD class="Label"><asp:Label id="Label3" runat="server" Text="Supplier"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label"><asp:TextBox id="suppname" runat="server" CssClass="inpTextDisabled" Width="160px" Enabled="False"></asp:TextBox></TD><TD class="Label"><asp:Label id="Label5" runat="server" Text="PO Date" Width="48px"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label"><asp:TextBox id="podate" runat="server" CssClass="inpTextDisabled" Width="75px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label"><asp:Label id="Label6" runat="server" Text="Note"></asp:Label>&nbsp;<asp:Label id="Label9" runat="server" CssClass="Important" Text="*" __designer:wfdid="w1"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label"><asp:TextBox id="pomstnote" runat="server" CssClass="inpText" Width="200px" MaxLength="90"></asp:TextBox></TD><TD class="Label"><asp:Label id="Label7" runat="server" Text="Status"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label"><asp:TextBox id="pomststatus" runat="server" CssClass="inpTextDisabled" Width="75px" Enabled="False"></asp:TextBox></TD></TR></TBODY></TABLE></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:GridView id="gvPODtl" runat="server" ForeColor="#333333" Width="100%" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>
<Columns>
<asp:TemplateField Visible="False"><ItemTemplate>
            <asp:CheckBox ID="cbCheckClosed" runat="server" ForeColor="Transparent" ToolTip='<%# eval("podtloid") %>' />
        
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="podtlseq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Code Item">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemLongDescription" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="poqty" HeaderText="PO Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="mrqty" HeaderText="IR Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="RetQty" HeaderText="Qty Retur">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="QtyOs" HeaderText="Qty OS">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="podtlstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="podtlnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD id="TD4" class="Label" align=left colSpan=3 runat="server" Visible="false"></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left colSpan=3>Processed By <asp:Label id="upduser" runat="server"></asp:Label>&nbsp;On <asp:Label id="updtime" runat="server"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:ImageButton id="btnClosingAll" runat="server" ImageUrl="~/Images/Close.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClosing" runat="server" ImageUrl="~/Images/closeselected.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upPOClosing"><ProgressTemplate>
<DIV id="Div1" class="progressBackgroundFilter"></DIV><DIV id="Div2" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="ImagePOCancel" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upListPO" runat="server">
        <ContentTemplate>
<asp:Panel id="PanelListPO" runat="server" CssClass="modalBox" Width="800px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListPO" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Approved Purchase Order"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListPO" runat="server" Width="100%" DefaultButton="btnFindListPO"><asp:Label id="Label24" runat="server" Text="Filter :"></asp:Label> <asp:DropDownList id="DDLFilterListPO" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="pono">PO No</asp:ListItem>
<asp:ListItem Value="pomststatus">Status</asp:ListItem>
<asp:ListItem Value="pomstnote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListPO" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListPO" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewAllListPO" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListPO" runat="server" BorderColor="Black" ForeColor="#333333" Width="98%" AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowPaging="True" PageSize="8" BorderStyle="None" DataKeyNames="pomstoid,pono,podate,pomststatus,pomstnote,suppname,pogroup" OnRowDataBound="gvListPO_RowDataBound">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="pono" HeaderText="PO No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="podate" HeaderText="PO Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pomststatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pomstnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                                        <asp:Label ID="lblInfoOnListGV" runat="server" CssClass="Important" Text="There is no data !!"></asp:Label>
                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbListPO" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListPO" runat="server" PopupControlID="PanelListPO" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListPO" TargetControlID="btnHiddenListPO">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListPO" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

