<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnTransform.aspx.vb" Inherits="Transaction_MaterialTransformation" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Material Transformation" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Material Transformation :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" __designer:wfdid="w35" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="lblFilter" runat="server" Text="Filter" __designer:wfdid="w36"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="105px" __designer:wfdid="w37"><asp:ListItem Value="transm.transformmstoid">Draft No.</asp:ListItem>
<asp:ListItem Value="transformno">Transform No.</asp:ListItem>
<asp:ListItem Value="deptname">Department</asp:ListItem>
<asp:ListItem Value="transformmstnote">Note</asp:ListItem>
    <asp:ListItem>Pallet In</asp:ListItem>
    <asp:ListItem>Pallet Out</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="250px" __designer:wfdid="w38"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text="Period" __designer:wfdid="w39"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="80px" __designer:wfdid="w40" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w41"></asp:ImageButton>&nbsp;<asp:Label id="Label555" runat="server" Text="to" __designer:wfdid="w42"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="80px" __designer:wfdid="w43" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w44"></asp:ImageButton> <asp:Label id="lblLabelPeriod" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w45"></asp:Label></TD></TR><TR><TD id="TD8" class="Label" align=left runat="server" Visible="false"><asp:CheckBox id="cbBusUnit" runat="server" Text="Business Unit" __designer:wfdid="w46"></asp:CheckBox></TD><TD id="TD7" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD6" class="Label" align=left colSpan=2 runat="server" Visible="false"><asp:DropDownList id="FilterDDLBusUnit" runat="server" CssClass="inpText" Width="250px" __designer:wfdid="w47"></asp:DropDownList></TD></TR><TR><TD id="TD5" class="Label" align=left runat="server" Visible="false"><asp:CheckBox id="cbType" runat="server" Text="Type" __designer:wfdid="w48"></asp:CheckBox></TD><TD id="TD4" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD3" class="Label" align=left colSpan=2 runat="server" Visible="false"><asp:DropDownList id="FilterDDLType" runat="server" CssClass="inpText" Width="250px" __designer:wfdid="w49"><asp:ListItem>RAW To FG</asp:ListItem>
<asp:ListItem>RAW To WIP</asp:ListItem>
<asp:ListItem>WIP To FG</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status" __designer:wfdid="w50"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="105px" __designer:wfdid="w51">
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" __designer:wfdid="w52" Format="MM/dd/yyyy" PopupButtonID="imbDate1" TargetControlID="FilterPeriod1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" __designer:wfdid="w53" TargetControlID="FilterPeriod1" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" __designer:wfdid="w54" Format="MM/dd/yyyy" PopupButtonID="imbDate2" TargetControlID="FilterPeriod2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" __designer:wfdid="w55" TargetControlID="FilterPeriod2" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w56"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w57"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w58"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:LinkButton id="lbInProcess" runat="server" __designer:wfdid="w59" Visible="False"></asp:LinkButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvTRN" runat="server" ForeColor="#333333" Width="100%" __designer:wfdid="w60" GridLines="None" CellPadding="4" AutoGenerateColumns="False" PageSize="8" AllowPaging="True" AllowSorting="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="transformmstoid" DataNavigateUrlFormatString="~\Transaction\trnTransform.aspx?oid={0}" DataTextField="transformmstoid" HeaderText="Draft No." SortExpression="transformmstoid">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="transformno" HeaderText="Transform No." SortExpression="transformno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transformdate" HeaderText="Transform Date" SortExpression="transformdate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="deptname" HeaderText="Department" SortExpression="deptname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
    <asp:BoundField DataField="transformtype" HeaderText="Type" SortExpression="transformtype">
        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
        <ItemStyle HorizontalAlign="Left" />
    </asp:BoundField>
<asp:BoundField DataField="transformmststatus" HeaderText="Status" SortExpression="transformmststatus">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transformmstnote" HeaderText="Note" SortExpression="transformmstnote">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
    <asp:TemplateField>
        <ItemTemplate>
            <asp:CheckBox ID="cbGVMst" runat="server" Checked='<%# eval("checkvalue") %>' />
            <asp:Label ID="lblOidGVMst" runat="server" Text='<%# eval("transformmstoid") %>'
                Visible="False"></asp:Label>
        </ItemTemplate>
        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Center" />
        <ItemStyle HorizontalAlign="Center" />
    </asp:TemplateField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" ForeColor="White" CssClass="gvhdr" Font-Bold="True"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w61"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=4><asp:UpdateProgress id="UpdateProgress2" runat="server" __designer:wfdid="w62" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image12" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w63"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label16" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Material Transformation Header" Font-Underline="False" __designer:wfdid="w115"></asp:Label></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="i_u" runat="server" CssClass="Important" Text="New Data" Visible="False" __designer:wfdid="w116"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="periodacctg" runat="server" Visible="False" __designer:wfdid="w117"></asp:Label></TD><TD style="WIDTH: 14%" class="Label" align=left></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblTrnNo" runat="server" Text="Draft No." __designer:wfdid="w120"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:Label id="transformmstoid" runat="server" __designer:wfdid="w121"></asp:Label><asp:TextBox id="transformno" runat="server" CssClass="inpTextDisabled" Width="260px" Visible="False" Enabled="False" __designer:wfdid="w122"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label6" runat="server" Text="Date" __designer:wfdid="w123"></asp:Label> <asp:Label id="Label7" runat="server" CssClass="Important" Text="*" __designer:wfdid="w124"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="transformdate" runat="server" CssClass="inpText" Width="80px" ToolTip="MM/dd/yyyy" __designer:wfdid="w125"></asp:TextBox> <asp:ImageButton id="imbDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w126"></asp:ImageButton> <asp:Label id="Label5" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w127"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label31" runat="server" Text="Total Qty In" __designer:wfdid="w136"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="transformtotalqtyin" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False" __designer:wfdid="w137"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label33" runat="server" Text="Total Qty Out" __designer:wfdid="w138"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="transformtotalqtyout" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False" __designer:wfdid="w139"></asp:TextBox>&nbsp;<asp:DropDownList id="qtyoutunitoid" runat="server" CssClass="inpTextDisabled" Width="105px" Visible="False" Enabled="False" __designer:wfdid="w59"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label36" runat="server" Text="Cost In" __designer:wfdid="w146"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="transformcostin" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" MaxLength="16" __designer:wfdid="w147"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label37" runat="server" Text="Cost Out" __designer:wfdid="w148"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="transformcostout" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" MaxLength="16" __designer:wfdid="w149"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label38" runat="server" Text="Total Cost In" __designer:wfdid="w150"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="transformtotalcostin" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False" __designer:wfdid="w151"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label39" runat="server" Text="Total Cost Out" __designer:wfdid="w152"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="transformtotalcostout" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False" __designer:wfdid="w153"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label8" runat="server" Text="Header Note" __designer:wfdid="w154"></asp:Label>&nbsp;<asp:Label id="Label13" runat="server" CssClass="Important" Text="*" __designer:wfdid="w1"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="transformmstnote" runat="server" CssClass="inpText" Width="300px" MaxLength="100" __designer:wfdid="w155"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label9" runat="server" Text="Status" __designer:wfdid="w156"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="transformmststatus" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False" __designer:wfdid="w157"></asp:TextBox></TD></TR><TR><TD id="TD9" class="Label" align=left colSpan=6 runat="server" Visible="false"><asp:Label id="Label3" runat="server" Text="Department" __designer:wfdid="w128"></asp:Label> <asp:DropDownList id="deptoid" runat="server" CssClass="inpText" Width="305px" __designer:wfdid="w129">
            </asp:DropDownList> <asp:Label id="Label4" runat="server" Text="Type" __designer:wfdid="w130"></asp:Label> <asp:DropDownList id="transformtype" runat="server" CssClass="inpTextDisabled" Width="102px" AutoPostBack="True" Enabled="False" __designer:wfdid="w131"><asp:ListItem>To FG</asp:ListItem>
<asp:ListItem>To WIP</asp:ListItem>
<asp:ListItem Value="To RAW">To RAW</asp:ListItem>
</asp:DropDownList> <asp:Label id="Label35" runat="server" Text="Group Name" __designer:wfdid="w140"></asp:Label> <asp:DropDownList id="suppoid" runat="server" CssClass="inpTextDisabled" Width="305px" Enabled="False" __designer:wfdid="w141" EnableTheming="True"></asp:DropDownList> <asp:Label id="Label15" runat="server" Text="Mat. Type" __designer:wfdid="w169"></asp:Label> <asp:DropDownList id="transformdtl1reftype" runat="server" CssClass="inpTextDisabled" Width="105px" Enabled="False" __designer:wfdid="w170"><asp:ListItem Value="FG">FG</asp:ListItem>
<asp:ListItem Value="RAW">RAW </asp:ListItem>
<asp:ListItem Value="WIP">WIP</asp:ListItem>
</asp:DropDownList> <asp:Label id="Label43" runat="server" Text="Mat. Type" __designer:wfdid="w196"></asp:Label> <asp:DropDownList id="transformdtl2reftype" runat="server" CssClass="inpTextDisabled" Width="105px" Enabled="False" __designer:wfdid="w197"><asp:ListItem Value="FG">FG</asp:ListItem>
<asp:ListItem Value="WIP"></asp:ListItem>
<asp:ListItem>RAW</asp:ListItem>
</asp:DropDownList>&nbsp;&nbsp; <asp:DropDownList id="transformunitoid" runat="server" CssClass="inpText" Width="105px" __designer:wfdid="w143">
            </asp:DropDownList> <asp:DropDownList id="transformtounitoid" runat="server" CssClass="inpText" Width="105px" __designer:wfdid="w145">
            </asp:DropDownList> <asp:Label id="Label21" runat="server" Text="Business Unit" Visible="False" __designer:wfdid="w118"></asp:Label> <asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpText" Width="305px" Visible="False" AutoPostBack="True" __designer:wfdid="w119"></asp:DropDownList> <asp:DropDownList id="ItemUnit" runat="server" CssClass="inpTextDisabled" Width="105px" Enabled="False" __designer:wfdid="w142" OnSelectedIndexChanged="ItemUnit_SelectedIndexChanged"><asp:ListItem Value="itemunit1">Unit 1</asp:ListItem>
<asp:ListItem Value="itemunit3">Unit 3</asp:ListItem>
</asp:DropDownList> <asp:DropDownList id="ItenUnitOut" runat="server" CssClass="inpTextDisabled" Width="105px" Enabled="False" __designer:wfdid="w144" OnSelectedIndexChanged="ItenUnitOut_SelectedIndexChanged"><asp:ListItem Value="itemunit1">Unit 1</asp:ListItem>
<asp:ListItem Value="itemunit3">Unit 3</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><ajaxToolkit:CalendarExtender id="ceDate" runat="server" Format="MM/dd/yyyy" PopupButtonID="imbDate" TargetControlID="transformdate" __designer:wfdid="w158">
            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeDate" runat="server" TargetControlID="transformdate" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear" __designer:wfdid="w159">
            </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbCostIn" runat="server" TargetControlID="transformcostin" ValidChars="1234567890,." __designer:wfdid="w160">
            </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbCostOut" runat="server" TargetControlID="transformcostout" ValidChars="1234567890,." __designer:wfdid="w161">
            </ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6 rowSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><asp:Label id="Label17" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Material Transformation Detail" Font-Underline="False" __designer:wfdid="w162"></asp:Label></TD></TR><TR><TD style="WIDTH: 50%" class="Label" vAlign=top align=left colSpan=3 rowSpan=1><asp:Panel id="pnlDtlInput" runat="server" BorderColor="Silver" Width="98%" BorderWidth="1px" BorderStyle="Outset" __designer:wfdid="w163"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left colSpan=3><asp:Label id="Label2" runat="server" ForeColor="DarkGreen" Font-Size="8pt" Font-Bold="True" Text="Detail Input" Font-Underline="True" __designer:wfdid="w164"></asp:Label></TD></TR><TR><TD style="WIDTH: 28%" class="Label" align=left><asp:Label id="i_u2" runat="server" Text="New Detail" Visible="False" __designer:wfdid="w165"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="transformdtl1seq" runat="server" Visible="False" __designer:wfdid="w166">1</asp:Label> <asp:Label id="transformdtl1oid" runat="server" Visible="False" __designer:wfdid="w167"></asp:Label> <asp:Label id="transformdtl1refoid" runat="server" Visible="False" __designer:wfdid="w168"></asp:Label> <asp:Label id="transformdtl1reflongdesc" runat="server" Visible="False" __designer:wfdid="w171"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label32" runat="server" Text="From Warehouse" __designer:wfdid="w132"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="transformfromwhoid" runat="server" CssClass="inpText" Width="305px" __designer:wfdid="w133">
            </asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label24" runat="server" Text="Material" __designer:wfdid="w172"></asp:Label> <asp:Label id="Label23" runat="server" CssClass="Important" Text="*" __designer:wfdid="w173"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="transformdtl1refcode" runat="server" CssClass="inpTextDisabled" Width="280px" Enabled="False" __designer:wfdid="w174"></asp:TextBox> <asp:ImageButton id="btnSearchMat1" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w175"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label14" runat="server" Text="Qty" __designer:wfdid="w176"></asp:Label> <asp:Label id="Label47" runat="server" CssClass="Important" Text="*" __designer:wfdid="w177"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="transformdtl1qty" runat="server" CssClass="inpText" Width="100px" MaxLength="16" __designer:wfdid="w178"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label12" runat="server" Text="Stock Qty" __designer:wfdid="w179"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="transformdtl1stockqty" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False" __designer:wfdid="w180"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label10" runat="server" Text="Rounding Qty" Visible="False" __designer:wfdid="w181"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="transformdtl1limitqty" runat="server" CssClass="inpTextDisabled" Width="100px" Visible="False" Enabled="False" __designer:wfdid="w182"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label26" runat="server" Text="Detail Note" __designer:wfdid="w183"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="transformdtl1note" runat="server" CssClass="inpText" Width="300px" MaxLength="100" __designer:wfdid="w184"></asp:TextBox></TD></TR><TR><TD class="Label" align=left colSpan=3><ajaxToolkit:FilteredTextBoxExtender id="ftbQtyIn" runat="server" TargetControlID="transformdtl1qty" ValidChars="1234567890,." __designer:wfdid="w185">
                            </ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:ImageButton id="btnAddToList1" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w186"></asp:ImageButton> <asp:ImageButton id="btnClear1" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w187"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Panel id="pnlGVDtl1" runat="server" CssClass="inpText" Width="100%" ScrollBars="Vertical" Height="200px" __designer:wfdid="w188"><asp:GridView id="GVDtl1" runat="server" ForeColor="#333333" Width="96%" GridLines="None" CellPadding="4" DataKeyNames="transformdtl1seq" AutoGenerateColumns="False" PageSize="5" __designer:wfdid="w189">
<RowStyle BackColor="#E3EAEB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="transformdtl1refcode" HeaderText="Code/No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transformdtl1reflongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transformdtl1qty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transformdtl1unit" HeaderText="Unit">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="transformdtl1wh" HeaderText="Warehouse">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transformdtl1note" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#666666" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#7C6F57"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel> </TD></TR></TBODY></TABLE></asp:Panel> </TD><TD class="Label" vAlign=top align=left colSpan=3 rowSpan=1><asp:Panel id="pnlDtlOutput" runat="server" BorderColor="Silver" Width="100%" BorderWidth="1px" BorderStyle="Outset" __designer:wfdid="w190"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left colSpan=3><asp:Label id="Label42" runat="server" ForeColor="DarkGreen" Font-Size="8pt" Font-Bold="True" Text="Detail Output" Font-Underline="True" __designer:wfdid="w191"></asp:Label></TD></TR><TR><TD style="WIDTH: 28%" class="Label" align=left><asp:Label id="i_u3" runat="server" Text="New Detail" Visible="False" __designer:wfdid="w192"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="transformdtl2seq" runat="server" Visible="False" __designer:wfdid="w193">1</asp:Label> <asp:Label id="transformdtl2oid" runat="server" Visible="False" __designer:wfdid="w194"></asp:Label> <asp:Label id="transformdtl2refoid" runat="server" Visible="False" __designer:wfdid="w195"></asp:Label> <asp:Label id="transformdtl2reflongdesc" runat="server" Visible="False" __designer:wfdid="w198"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label27" runat="server" Text="To Warehouse" __designer:wfdid="w134"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="transformtowhoid" runat="server" CssClass="inpText" Width="300px" __designer:wfdid="w135"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label44" runat="server" Text="Material" __designer:wfdid="w199"></asp:Label> <asp:Label id="Label48" runat="server" CssClass="Important" Text="*" __designer:wfdid="w200"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="transformdtl2refcode" runat="server" CssClass="inpTextDisabled" Width="280px" Enabled="False" __designer:wfdid="w201"></asp:TextBox> <asp:ImageButton id="btnSearchMat2" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w202"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label45" runat="server" Text="Qty" __designer:wfdid="w203"></asp:Label> <asp:Label id="Label49" runat="server" CssClass="Important" Text="*" __designer:wfdid="w204"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="transformdtl2qty" runat="server" CssClass="inpText" Width="100px" MaxLength="16" __designer:wfdid="w205"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label11" runat="server" Text="Rounding Qty" Visible="False" __designer:wfdid="w206"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="transformdtl2limitqty" runat="server" CssClass="inpTextDisabled" Width="100px" Visible="False" Enabled="False" __designer:wfdid="w207"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label46" runat="server" Text="Detail Note" __designer:wfdid="w208"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="transformdtl2note" runat="server" CssClass="inpText" Width="300px" MaxLength="100" __designer:wfdid="w209"></asp:TextBox></TD></TR><TR><TD class="Label" align=left colSpan=3><ajaxToolkit:FilteredTextBoxExtender id="ftbQtyOut" runat="server" TargetControlID="transformdtl2qty" ValidChars="1234567890,." __designer:wfdid="w210">
                            </ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD style="HEIGHT: 30px" class="Label" align=left colSpan=3></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:ImageButton id="btnAddToList2" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w211"></asp:ImageButton> <asp:ImageButton id="btnClear2" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w212"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Panel id="pnlGVDtl2" runat="server" CssClass="inpText" Width="100%" ScrollBars="Vertical" Height="200px" __designer:wfdid="w213"><asp:GridView id="GVDtl2" runat="server" ForeColor="#333333" Width="100%" GridLines="None" CellPadding="4" DataKeyNames="transformdtl2seq" AutoGenerateColumns="False" PageSize="5" __designer:wfdid="w214">
<RowStyle BackColor="#E3EAEB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="transformdtl2refcode" HeaderText="Code/No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transformdtl2reflongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transformdtl2qty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transformdtl2unit" HeaderText="Unit">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="transformdtl2wh" HeaderText="Warehouse">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transformdtl2note" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#666666" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#7C6F57"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel> </TD></TR></TBODY></TABLE></asp:Panel> </TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=6></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w215"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w216"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w217"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w218"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" AlternateText="Save" __designer:wfdid="w219"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" AlternateText="Cancel" __designer:wfdid="w220"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" AlternateText="Delete" __designer:wfdid="w221"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPost" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" AlternateText="Post" __designer:wfdid="w222"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1" __designer:wfdid="w223"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w224"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Material Transformation :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upListMat1" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListMat1" runat="server" CssClass="modalBox" Width="800px" Visible="False" DefaultButton="btnFindListMat1"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListMat1" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Material Input" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 30%" class="Label" align=right colSpan=1>Filter : <asp:DropDownList id="FilterDDLListMat1" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="m.itemcode">Code/No</asp:ListItem>
<asp:ListItem Value="m.itemLongDescription">Description</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left colSpan=2><asp:TextBox id="FilterTextListMat1" runat="server" CssClass="inpText" Width="300px"></asp:TextBox> <asp:ImageButton id="btnFindListMat1" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat1" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD id="TD1" class="Label" align=center colSpan=3 runat="server"><asp:CheckBox id="cbCat01" runat="server" Text="Cat 1 :"></asp:CheckBox> <asp:DropDownList id="DDLCat01" runat="server" CssClass="inpText" Width="200px" AutoPostBack="True">
            </asp:DropDownList> <asp:CheckBox id="cbCat02" runat="server" Text="Cat 2 :"></asp:CheckBox> <asp:DropDownList id="DDLCat02" runat="server" CssClass="inpText" Width="200px" AutoPostBack="True">
            </asp:DropDownList></TD></TR><TR><TD id="TD2" class="Label" align=center colSpan=3 runat="server"><asp:CheckBox id="cbCat03" runat="server" Text="Cat 3 :"></asp:CheckBox> <asp:DropDownList id="DDLCat03" runat="server" CssClass="inpText" Width="200px" AutoPostBack="True">
            </asp:DropDownList> <asp:CheckBox id="cbCat04" runat="server" Text="Cat 4 :"></asp:CheckBox> <asp:DropDownList id="DDLCat04" runat="server" CssClass="inpText" Width="200px"></asp:DropDownList></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat1" runat="server" ForeColor="#333333" Width="99%" PageSize="5" AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><HeaderTemplate>
<asp:CheckBox id="cbListMatHdr" runat="server" AutoPostBack="True" OnCheckedChanged="cbListMatHdr_CheckedChanged"></asp:CheckBox> 
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="cbListMat" runat="server" ToolTip='<%# eval("refoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="refcode" HeaderText="Code/No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reflongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reflimitqty" HeaderText="Round Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="refstockqty" HeaderText="Stock Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stockunit" HeaderText="Unit">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Qty"><ItemTemplate>
<asp:TextBox id="tbMatQty" runat="server" CssClass="inpText" Text='<%# eval("refqty") %>' Width="60px" MaxLength="16" Enabled='<%# eval("enableqty") %>'></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftbMatQty" runat="server" ValidChars="1234567890,." TargetControlID="tbMatQty"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="Unit1" HeaderText="Unit">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Note"><ItemTemplate>
<asp:TextBox id="tbMatNote" runat="server" CssClass="inpText" Text='<%# eval("refnote") %>' Width="150px" MaxLength="100"></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label114" runat="server" Text="No data found !!" CssClass="Important"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbAddToListMat1" runat="server">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lbSelectAllToListMat1" runat="server">[ Select All To List ]</asp:LinkButton>&nbsp;<asp:LinkButton id="lbCloseListMat1" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat1" runat="server" TargetControlID="btnHideListMat1" PopupDragHandleControlID="lblListMat1" BackgroundCssClass="modalBackground" PopupControlID="pnlListMat1" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat1" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel><asp:UpdatePanel id="upListMat2" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlListMat2" runat="server" CssClass="modalBox" Width="800px" Visible="False" DefaultButton="btnFindListMat2"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListMat2" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Material Output" Font-Underline="False"></asp:Label></TD></TR>
    <tr>
        <td align="right" class="Label" colspan="1" style="width: 30%">
            Filter : <asp:DropDownList id="FilterDDLListMat2" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="refcode">Code/No</asp:ListItem>
<asp:ListItem Value="reflongdesc">Description</asp:ListItem>
    <asp:ListItem Value="createuser">Upload User</asp:ListItem>
</asp:DropDownList></td>
        <td align="left" class="Label" colspan="2">
            <asp:TextBox id="FilterTextListMat2" runat="server" CssClass="inpText" Width="300px"></asp:TextBox> <asp:ImageButton id="btnFindListMat2" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat2" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></td>
    </tr>
    <TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat2" runat="server" ForeColor="#333333" Width="99%" PageSize="5" AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><HeaderTemplate>
<asp:CheckBox id="cbListMatHdr2" runat="server" AutoPostBack="True" OnCheckedChanged="cbListMatHdr2_CheckedChanged"></asp:CheckBox> 
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="cbListMat" runat="server" ToolTip='<%# eval("refoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="refcode" HeaderText="Code/No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reflongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reflimitqty" HeaderText="Round Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Qty"><ItemTemplate>
<asp:TextBox id="tbMatQty" runat="server" CssClass="inpText" Text='<%# eval("refqty") %>' Width="60px" MaxLength="16" Enabled='<%# eval("enableqty") %>'></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftbMatQty" runat="server" TargetControlID="tbMatQty" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Note"><ItemTemplate>
<asp:TextBox id="tbMatNote" runat="server" CssClass="inpText" Text='<%# eval("refnote") %>' Width="150px" MaxLength="100"></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label114" runat="server" Text="No data found !!" CssClass="Important"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbAddToListMat2" runat="server">[ Add To List ]</asp:LinkButton>
    <asp:LinkButton ID="lbSelectAllToListMat2" runat="server">[ Select All To List ]</asp:LinkButton>&nbsp;<asp:LinkButton id="lbCloseListMat2" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat2" runat="server" TargetControlID="btnHideListMat2" PopupDragHandleControlID="lblListMat2" BackgroundCssClass="modalBackground" PopupControlID="pnlListMat2" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat2" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

