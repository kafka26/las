Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_DepartmentGroup
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        If ToDouble(paycommamt.Text) <= 0 Then
            sError &= "- Amount must more than 0!<BR>"
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill Detail Data!"
        Else
            Dim dtTbl As DataTable = Session("TblDtl")
            If dtTbl.Rows.Count <= 0 Then
                sError &= "- Please fill Detail Data!"
            End If
        End If

        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitDDL()
        sSql = "SELECT personoid, personname FROM QL_mstperson WHERE leveloid=1 AND activeflag='ACTIVE' AND personoid IN (SELECT m_person_id FROM QL_m_spv_comm WHERE m_active_flag='ACTIVE') ORDER BY personname"
        FillDDL(personoid, sSql)
    End Sub

    Private Sub BindMstData(ByVal sFilter As String)
        sSql = "SELECT 'False' checkvalue, pay.t_spv_comm_calc_id paycommoid, pay.t_spv_comm_calc_no paycommno, CONVERT(VARCHAR(10), pay.t_spv_comm_calc_date, 101) paycommdate, personname, t_spv_comm_calc_note paycommnote, t_spv_comm_calc_status paycommstatus, t_spv_comm_calc_amt paycommamt FROM QL_t_spv_comm_calc pay INNER JOIN QL_mstperson p ON p.personoid=pay.m_person_id WHERE " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(sFilter) & "%'"
        If checkPagePermission("~\Transaction\trnCommisionCalc.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND pay.create_by='" & Session("UserID") & "'"
        End If
        sSql &= " ORDER BY t_spv_comm_calc_date"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_t_spv_comm_calc")
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub FillTextBox()
        Try
            sSql = "SELECT * FROM QL_t_spv_comm_calc pay WHERE t_spv_comm_calc_id=" & Session("oid")
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                groupoid.Text = xreader("t_spv_comm_calc_id").ToString
                paycommno.Text = xreader("t_spv_comm_calc_no").ToString
                paycommdate.Text = Format(CDate(xreader("t_spv_comm_calc_date").ToString), "MM/dd/yyyy")
                activeflag.SelectedValue = xreader("t_spv_comm_calc_status").ToString
                personoid.SelectedValue = xreader("m_person_id").ToString
                groupnote.Text = xreader("t_spv_comm_calc_note").ToString
                createuser.Text = xreader("create_by").ToString
                createtime.Text = xreader("create_at").ToString
                upduser.Text = xreader("last_edited_by").ToString
                updtime.Text = xreader("last_edited_at").ToString
                paycommamt.Text = ToMaskEdit(ToDouble(xreader("t_spv_comm_calc_amt").ToString), 2)
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            btnSave.Visible = False : btnDelete.Visible = False
            showMessage(ex.Message, 1) : Exit Sub
        End Try
        personoid.CssClass = "inpTextDisabled" : personoid.Enabled = False
        If activeflag.SelectedValue = "Post" Then
            btnSave.Visible = False
            btnPost.Visible = False
            btnDelete.Visible = False
            btnAddToList.Visible = False
            gvDtl.Columns(0).Visible = False
            gvDtl.Columns(gvDtl.Columns.Count - 1).Visible = False
        End If

        sSql = "SELECT t_spv_comm_calc_seq paycommdtlseq, conaroid, personname, t_spv_comm_calc_ref_no paycommdtlrefno, t_spv_comm_calc_payrefno paycommdtlrefdesc, t_spv_comm_calc_d_amt_netto paycommdtlamt, t_spv_comm_calc_d_note paycommdtlnote, CONVERT(VARCHAR(10), t_spv_comm_calc_payrefdate, 101) paymentdate, c.custname, t_spv_comm_calc_d_amt cashbankamt, t_spv_comm_calc_d_amt_pph ppnamt, t_spv_comm_calc_range_from, t_spv_comm_calc_range_to, t_spv_comm_calc_days, t_spv_comm_calc_pct, t_spv_comm_calc_ref_id, m_person_id FROM QL_t_spv_comm_calc_d d INNER JOIN QL_mstperson p ON p.personoid=d.m_person_id INNER JOIN QL_trnjualmst j ON j.trnjualmstoid=d.t_spv_comm_calc_ref_id INNER JOIN QL_mstcust c ON c.custoid=j.custoid WHERE t_spv_comm_calc_id=" & Session("oid") & " ORDER BY paycommdtlseq"
        Dim dtTable As DataTable = cKon.ambiltabel(sSql, "QL_t_spv_comm_calc_d")
        Session("TblDtl") = dtTable
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        ClearDetail()
        If Session("TblDtl") IsNot Nothing Then
            CountHdrAmt()
        End If
    End Sub

    Private Sub BindListCOA()
        Dim sAdd As String = "AND con_status_spv_calc=''"

        sSql = "SELECT 'True' checkvalue, 0 nmr, Tbl_INV.cmpcode, Tbl_INV.sales, Tbl_INV.personname, trnjualmstoid, Tbl_INV.trnjualno paycommdtlrefno, Tbl_INV.amttrans, CONVERT(VARCHAR(10), Tbl_INV.trnardate, 101) trnardate, MONTH([Tgl. Bayar]) bulan_trans, YEAR([Tgl. Bayar]) tahun_trans, Tbl_INV.custname, CONVERT(VARCHAR(10), [Tgl. Bayar], 101) paymentdate, ISNULL(amtbayaridr, 0.0) amtbayaridr, amtreturidr amtreturidr, amtretur2idr amtretur2idr, amtbayarotheridr amtbayarotheridr, komisi percentage, ISNULL(DATEDIFF(DAY, trnardate, [Tgl. Bayar]), 0) jml_hari, [No. Cash Bank] paycommdtlrefdesc, Bank, [Tgl. Bayar], (CASE WHEN DAY([Tgl. Bayar]) BETWEEN 1 AND 20 THEN (CASE MONTH([Tgl. Bayar]) WHEN 1 THEN 12 ELSE MONTH([Tgl. Bayar])-1 END) WHEN DAY([Tgl. Bayar]) BETWEEN 21 AND 31 THEN MONTH([Tgl. Bayar]) END) bulan, (CASE WHEN MONTH([Tgl. Bayar])=1 AND DAY([Tgl. Bayar]) BETWEEN 1 AND 20 THEN YEAR([Tgl. Bayar])-1 ELSE YEAR([Tgl. Bayar]) END) tahun, [Kurang Bayar], [Debet Note], [Credit Note], 0.00 percentage2, CASE ISNULL(comstatus, '') WHEN '' THEN 'NOT PAID' ELSE comstatus END statusbyr, 0.0 paycommdtlamt, conaroid, Tbl_INV.personoid, 0.0 ppnamt, 0.0 cashbankamt, dncnamt, 0 rangeone, 0 rangetwo, komisiamtidr, 0.0 komisi_nett FROM ( "
        sSql &= "SELECT cmpcode, custoid, (tglpay) [Tgl. Bayar], (tglkomisi) [Tgl. Komisi], (amtbayar) amtbayaridr, (returidr) amtreturidr, (retur2idr) amtretur2idr, (amtbayarother) amtbayarotheridr, refoid, cashbankno [No. Cash Bank], acctgdesc [Bank], [Kurang Bayar], [Debet Note], [Credit Note], comstatus, conaroid, dncnamt FROM ( "
        'Payment Cash/Bank
        sSql &= "SELECT con.cmpcode, con.refoid, con.custoid, (CASE cashbanktype WHEN 'BBM' THEN cb.cashbankduedate WHEN 'BGM' THEN cb.cashbankduedate ELSE cb.cashbankdate END) tglpay, (CASE cashbanktype WHEN 'BBM' THEN cb.cashbankduedate WHEN 'BGM' THEN cb.cashbankduedate ELSE cb.cashbankdate END) tglkomisi, con.amtbayar, 0.00 returidr, 0.00 retur2idr, 0.00 amtbayarother, cb.cashbankno, a.acctgdesc, 0.0 [Kurang Bayar], 0.0 [Debet Note], 0.0 [Credit Note], comstatus, con.conaroid, 0.0 dncnamt FROM QL_conar con INNER JOIN QL_trnpayar pay ON pay.cmpcode=con.cmpcode AND pay.payaroid=con.payrefoid AND ISNULL(pay.payarres1,'')='' INNER JOIN QL_trncashbankmst cb ON cb.cashbankoid=pay.cashbankoid INNER JOIN QL_mstacctg a ON a.acctgoid=cb.acctgoid WHERE con.cmpcode='" & CompnyCode & "' AND con.payrefoid<>0 AND con.trnarstatus='Post' AND con.trnartype IN ('PAYAR') AND cashbankstatus='Post' AND cashbanktype<>'BGM' AND cb.giroacctgoid NOT IN (SELECT dp.dparoid FROM QL_trndpar dp WHERE LEFT(dparpayrefno, 3)='SRT') " & sAdd & " UNION ALL "
        'Payment Giro yang sudah cair
        sSql &= "SELECT con.cmpcode, con.refoid, con.custoid, cbx.cashbankdate tglpay, cbx.cashbankdate tglkomisi, con.amtbayar, 0.00 returidr, 0.00 retur2idr, 0.00 amtbayarother, cbx.cashbankno + ' Pencairan Giro ' + cb.cashbankno cashbankno, a.acctgdesc, 0.0 [Kurang Bayar], 0.0 [Debet Note], 0.0 [Credit Note], comstatus, con.conaroid, 0.0 dncnamt FROM QL_conar con INNER JOIN QL_trnpayar pay ON pay.cmpcode=con.cmpcode AND pay.payaroid=con.payrefoid AND ISNULL(pay.payarres1,'')='' INNER JOIN QL_trncashbankmst cb ON cb.cashbankoid=pay.cashbankoid INNER JOIN QL_mstacctg a ON a.acctgoid=cb.acctgoid INNER JOIN QL_trnpayargiro g ON g.refoid=cb.cashbankoid INNER JOIN QL_trncashbankmst cbx ON cbx.cashbankoid=g.cashbankoid WHERE con.cmpcode='" & CompnyCode & "' AND con.payrefoid<>0 AND con.trnarstatus='Post' AND con.trnartype IN ('PAYAR') AND cb.cashbankstatus='Post' AND cb.cashbanktype='BGM' " & sAdd & " UNION ALL "
        'Payment DP Retur
        sSql &= "SELECT con.cmpcode, con.refoid, con.custoid, paymentdate tglpay, cashbankdate tglkomisi, -con.amtbayar, 0.00 returidr, 0.00 retur2idr, 0.00 amtbayarother, 'DP Return: ' + cb.cashbankno cashbankno, '' acctgdesc, 0.0 [Kurang Bayar], 0.0 [Debet Note], 0.0 [Credit Note], comstatus, con.conaroid, 0.0 dncnamt FROM QL_conar con INNER JOIN QL_trnpayar pay ON pay.cmpcode=con.cmpcode AND pay.payaroid=con.payrefoid AND ISNULL(pay.payarres1,'')='' INNER JOIN QL_trncashbankmst cb ON cb.cmpcode=pay.cmpcode AND cb.cashbankoid=pay.cashbankoid INNER JOIN (SELECT dp.cmpcode, dp.dparoid, dparpayrefno FROM QL_trndpar dp WHERE LEFT(dparpayrefno, 3)='SRT') dp ON dp.cmpcode=cb.cmpcode AND dp.dparoid=cb.giroacctgoid WHERE con.cmpcode='" & CompnyCode & "' AND con.payrefoid<>0 AND con.trnarstatus='Post' AND con.trnartype IN ('PAYAR') AND cashbankstatus='Post' AND cashbanktype='BLM' " & sAdd & " UNION ALL "
        'Giro Cancel
        sSql &= "SELECT con.cmpcode, con.refoid, con.custoid, paymentdate tglpay, paymentdate tglkomisi, con.amtbayar, 0.0 returidr, 0.00 retur2idr, 0.0 amtbayarother, trnarnote cashbankno, '' acctgdesc, 0.0 [Kurang Bayar], 0.0 [Debet Note], 0.0 [Credit Note], comstatus, con.conaroid, 0.0 dncnamt FROM QL_conar con WHERE con.cmpcode='" & CompnyCode & "' AND con.trnarstatus='Post' AND con.trnartype IN ('GCAR') " & sAdd
        sSql &= ") AS Tb_Pay "
        sSql &= ") AS Tbl_Payment INNER JOIN "
        sSql &= "(SELECT con.cmpcode, p.salescode sales, trnjualmstoid, trnjualno, con.amttrans, con.trnardate, c.custname, c.custoid, con.refoid, con.reftype, 0.00 komisi, '' statusbyr, p.personoid, p.personname, jm.komisiamtidr FROM QL_conar con INNER JOIN QL_mstcust c ON c.custoid=con.custoid INNER JOIN QL_trnjualmst jm ON jm.cmpcode=con.cmpcode AND jm.trnjualmstoid=con.refoid INNER JOIN QL_mstperson p ON p.cmpcode=jm.cmpcode AND p.salescode=jm.salescode WHERE con.cmpcode='" & CompnyCode & "' AND con.payrefoid=0 AND jm.trnjualres3='PAID' AND con.reftype='QL_trnjualmst' AND trnjualmstoid NOT IN (SELECT t_spv_comm_calc_ref_id FROM QL_t_spv_comm_calc_d)) AS Tbl_INV "
        sSql &= "ON Tbl_INV.cmpcode=Tbl_Payment.cmpcode AND Tbl_INV.custoid=Tbl_Payment.custoid AND Tbl_INV.refoid=Tbl_Payment.refoid "
        sSql &= "WHERE Tbl_INV.cmpcode='" & CompnyCode & "'"
        'Filter Month and Year
        sSql &= " AND personoid IN (SELECT d.m_person_id FROM QL_m_spv_comm_d d INNER JOIN QL_m_spv_comm m ON m.m_spv_comm_id=d.m_spv_comm_id WHERE m.m_person_id=" & personoid.SelectedValue & ") "
        sSql &= " AND CAST(trnardate AS DATETIME)>='" & FilterFrom.Text & " 00:00:00' AND CAST(trnardate AS DATETIME)<='" & FilterTo.Text & " 23:59:59' AND " & FilterDDLPay.SelectedValue & " LIKE '%" & Tchar(FilterTextPay.Text) & "%'"

        sSql &= " ORDER BY custname, trnjualno, Tbl_Payment.[Tgl. Bayar]"
        Dim dtData As DataTable = cKon.ambiltabel(sSql, "QL_trnkomisisalesmst")

        'Load table sales commission
        sSql = "SELECT m_spv_comm_pay_from rangeone, m_spv_comm_pay_to rangetwo, m_spv_comm_pct insentiverate, salescode personname FROM QL_m_spv_comm m INNER JOIN QL_m_spv_comm_d d ON m.m_spv_comm_id=d.m_spv_comm_id INNER JOIN QL_mstperson p ON p.personoid=d.m_person_id WHERE m.m_active_flag='ACTIVE' AND m.m_person_id=" & personoid.SelectedValue
        Dim dtView As DataView = cKon.ambiltabel(sSql, "QL_tis_mstcommission").DefaultView

        For C1 As Integer = 0 To dtData.Rows.Count - 1
            Dim iDays As Integer = dtData.Rows(C1)("jml_hari")

            dtView.RowFilter = "personname='" & dtData.Rows(C1)("sales") & "'"
            If dtView.Count > 0 Then
                For C2 As Integer = 0 To dtView.Count - 1
                    If iDays >= ToDouble(dtView(C2)("rangeone")) And iDays <= ToDouble(dtView(C2)("rangetwo")) Then
                        dtData.Rows(C1)("percentage") = ToDouble(dtView(C2)("insentiverate"))
                        dtData.Rows(C1)("rangeone") = ToInteger(dtView(C2)("rangeone"))
                        dtData.Rows(C1)("rangetwo") = ToInteger(dtView(C2)("rangetwo"))
                    End If
                Next
            End If
            dtView.RowFilter = ""
        Next
        For C1 As Integer = 0 To dtData.Rows.Count - 1
            Dim dNett As Double = 0, dPPH As Double = 0
            dtData.Rows(C1)("nmr") = C1 + 1
            dNett = (ToDouble(dtData.Rows(C1)("amtbayaridr").ToString) - ToDouble(dtData.Rows(C1)("komisiamtidr").ToString)) * (dtData.Rows(C1)("percentage") / 100)
            dtData.Rows(C1)("komisi_nett") = (ToDouble(dtData.Rows(C1)("amtbayaridr").ToString) - ToDouble(dtData.Rows(C1)("komisiamtidr").ToString))
            dPPH = dNett * 2.5 / 100
            dtData.Rows(C1)("ppnamt") = dPPH
            dtData.Rows(C1)("cashbankamt") = ToDouble(dtData.Rows(C1)("amtbayaridr").ToString)
            dtData.Rows(C1)("paycommdtlamt") = dNett - dPPH
        Next
        dtData.AcceptChanges()
        Session("TblPayment") = dtData
    End Sub

    Private Sub ClearDetail()
        i_u2.Text = "New Detail"
        cashbankglseq.Text = "1"
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            If dt.Rows.Count > 0 Then
                cashbankglseq.Text = dt.Rows.Count + 1
            End If
        End If

    End Sub

    Private Sub CountHdrAmt()
        Dim dVal As Double = 0
        If Session("TblDtl") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            If dt.Rows.Count > 0 Then
                For C1 As Integer = 0 To dt.Rows.Count - 1
                    dVal += ToDouble(dt.Rows(C1)("paycommdtlamt").ToString)
                Next
            End If
            paycommamt.Text = ToMaskEdit(dVal, 2)
        End If
    End Sub

    Private Sub GenerateNo()
        Dim sErr As String = ""
        If paycommdate.Text <> "" Then
            If IsValidDate(paycommdate.Text, "MM/dd/yyyy", sErr) Then
                Dim sNo As String = "SCC-" & Format(CDate(paycommdate.Text), "yyMM") & "-"
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(t_spv_comm_calc_no, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_t_spv_comm_calc WHERE cmpcode='" & CompnyCode & "' AND t_spv_comm_calc_no LIKE '%" & sNo & "%'"
                If GetStrData(sSql) = "" Then
                    paycommno.Text = GenNumberString(sNo, "", 1, DefaultFormatCounter)
                Else
                    paycommno.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
                End If
            End If
        End If
    End Sub

    Private Sub CreateTblDetail()
        If Session("TblDtl") Is Nothing Then
            Dim dtlTable As DataTable = New DataTable("TabelExpense")
            dtlTable.Columns.Add("paycommdtlseq", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("conaroid", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("personname", Type.GetType("System.String"))
            dtlTable.Columns.Add("paycommdtlrefno", Type.GetType("System.String"))
            dtlTable.Columns.Add("paycommdtlrefdesc", Type.GetType("System.String"))
            dtlTable.Columns.Add("paycommdtlamt", Type.GetType("System.Double"))
            dtlTable.Columns.Add("paycommdtlamtidr", Type.GetType("System.Double"))
            dtlTable.Columns.Add("paycommdtlamtusd", Type.GetType("System.Double"))
            dtlTable.Columns.Add("paycommdtlnote", Type.GetType("System.String"))
            dtlTable.Columns.Add("custname", Type.GetType("System.String"))
            dtlTable.Columns.Add("paymentdate", Type.GetType("System.String"))
            dtlTable.Columns.Add("cashbankamt", Type.GetType("System.Double"))
            dtlTable.Columns.Add("ppnamt", Type.GetType("System.Double"))

            dtlTable.Columns.Add("t_spv_comm_calc_range_from", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("t_spv_comm_calc_range_to", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("t_spv_comm_calc_days", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("t_spv_comm_calc_pct", Type.GetType("System.Double"))
            dtlTable.Columns.Add("t_spv_comm_calc_ref_id", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("m_person_id", Type.GetType("System.Int32"))
            Session("TblDtl") = dtlTable
        End If
    End Sub

    Private Sub UpdateCheckedMat()
        If Session("TblPayment") IsNot Nothing Then
            For C1 As Integer = 0 To gvListCOA.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListCOA.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                            Dim sOid2 As String = CType(myControl, System.Web.UI.WebControls.CheckBox).CssClass
                            Session("TblPayment").DefaultView.RowFilter = "paycommdtlrefdesc='" & sOid2 & "' AND conaroid=" & sOid
                            If cbcheck = True Then
                                If Session("TblPayment").DefaultView.Count > 0 Then
                                    Session("TblPayment").DefaultView(0)("checkvalue") = "True"
                                End If
                            Else
                                If Session("TblPayment").DefaultView.Count > 0 Then
                                    Session("TblPayment").DefaultView(0)("checkvalue") = "False"
                                End If
                            End If
                            Session("TblPayment").DefaultView.RowFilter = ""
                        End If
                    Next
                End If
            Next
        End If
        If Session("TblPaymentView") IsNot Nothing Then
            For C1 As Integer = 0 To gvListCOA.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListCOA.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                            Dim sOid2 As String = CType(myControl, System.Web.UI.WebControls.CheckBox).CssClass
                            Session("TblPaymentView").DefaultView.RowFilter = "paycommdtlrefdesc='" & sOid2 & "' AND conaroid=" & sOid
                            If cbcheck = True Then
                                If Session("TblPaymentView").DefaultView.Count > 0 Then
                                    Session("TblPaymentView").DefaultView(0)("checkvalue") = "True"
                                End If
                            Else
                                If Session("TblPaymentView").DefaultView.Count > 0 Then
                                    Session("TblPaymentView").DefaultView(0)("checkvalue") = "False"
                                End If
                            End If
                            Session("TblPaymentView").DefaultView.RowFilter = ""
                        End If
                    Next
                End If
            Next
        End If
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnCommisionCalc.aspx")
        End If
        If checkPagePermission("~\Transaction\trnCommisionCalc.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Commision Calculation"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            I_U.Text = "New Data"
        Else
            I_U.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPost.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            InitDDL()
            FilterFrom.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterTo.Text = Format(GetServerTime(), "MM/dd/yyyy")
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox()
                TabContainer1.ActiveTabIndex = 1
            Else
                paycommdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
                GenerateNo()
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                cProc.SetModalPopUpExtender(btnHideListCOA, pnlListCOA, mpeListCOA, True)
            End If
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
    End Sub

    Protected Sub gvMst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvMst.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 2)
        End If
    End Sub

    Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvMst.DataSource = Session("TblMst")
            gvMst.DataBind()
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_t_spv_comm_calc WHERE t_spv_comm_calc_no='" & paycommno.Text & "'"
                If CheckDataExists(sSql) Then
                    GenerateNo()
                End If
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_t_spv_comm_calc SELECT '" & paycommdate.Text & "', '" & paycommno.Text & "', " & personoid.SelectedValue & ", " & ToDouble(paycommamt.Text) & ", '" & Tchar(groupnote.Text) & "', '" & activeflag.SelectedValue & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP; SELECT SCOPE_IDENTITY()"
                    xCmd.CommandText = sSql
                    groupoid.Text = xCmd.ExecuteScalar()
                Else
                    sSql = "UPDATE QL_t_spv_comm_calc SET m_person_id=" & personoid.SelectedValue & ", t_spv_comm_calc_amt='" & ToDouble(paycommamt.Text) & "', t_spv_comm_calc_note='" & Tchar(groupnote.Text) & "', t_spv_comm_calc_status='" & activeflag.SelectedValue & "', last_edited_by='" & Session("UserID") & "', last_edited_at=CURRENT_TIMESTAMP WHERE t_spv_comm_calc_id=" & groupoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE QL_t_spv_comm_calc_d WHERE t_spv_comm_calc_id=" & groupoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_t_spv_comm_calc_d SELECT " & groupoid.Text & ", " & C1 + 1 & ", " & objTable.Rows(C1)("m_person_id").ToString & ", " & objTable.Rows(C1)("t_spv_comm_calc_range_from").ToString & ", " & objTable.Rows(C1)("t_spv_comm_calc_range_to").ToString & ", " & objTable.Rows(C1)("t_spv_comm_calc_days").ToString & ", " & objTable.Rows(C1)("t_spv_comm_calc_pct").ToString & ", " & objTable.Rows(C1)("t_spv_comm_calc_ref_id").ToString & ", '" & Tchar(objTable.Rows(C1)("paycommdtlrefno").ToString) & "', " & objTable.Rows(C1)("conaroid").ToString & ", '" & objTable.Rows(C1)("paymentdate").ToString & "', '" & Tchar(objTable.Rows(C1)("paycommdtlrefdesc").ToString) & "', " & ToDouble(objTable.Rows(C1)("cashbankamt").ToString) & ", " & ToDouble(objTable.Rows(C1)("ppnamt").ToString) & ", " & ToDouble(objTable.Rows(C1)("paycommdtlamt").ToString) & ", '" & Tchar(objTable.Rows(C1)("paycommdtlnote").ToString) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        If activeflag.SelectedValue = "Post" Then
                            sSql = "UPDATE QL_conar SET con_status_spv_calc='Complete' WHERE conaroid=" & objTable.Rows(C1)("conaroid").ToString
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                    Next
                End If       
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                conn.Close()
                showMessage(ex.Message, 1)
                Exit Sub
            End Try
            Response.Redirect("~\Transaction\trnCommisionCalc.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnCommisionCalc.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If groupoid.Text = "" Then
            showMessage("Please select Division data first!", 1)
            Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_conar SET con_status_spv_calc='' WHERE conaroid IN (SELECT x.conaroid FROM QL_t_spv_comm_calc_d x WHERE t_spv_comm_calc_id=" & groupoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE QL_t_spv_comm_calc_d WHERE t_spv_comm_calc_id=" & groupoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE QL_t_spv_comm_calc WHERE t_spv_comm_calc_id=" & groupoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnCommisionCalc.aspx?awal=true")
    End Sub

    Protected Sub btnFindListCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        UpdateCheckedMat()
        If Session("TblPayment") IsNot Nothing Then
            Session("TblPayment").DefaultView.RowFilter = FilterDDLListCOA.SelectedValue & " LIKE '%" & Tchar(FilterTextListCOA.Text) & "%'"
            If Session("TblPayment").DefaultView.Count > 0 Then
                Session("TblPaymentView") = Session("TblPayment").DefaultView.ToTable
                gvListCOA.DataSource = Session("TblPaymentView")
                gvListCOA.DataBind()
                mpeListCOA.Show()
            Else
                Session("WarningListMat") = "data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "There is no posting Commission data available for this time!"
            showMessage(Session("WarningListKIK"), 2)
        End If
    End Sub

    Protected Sub btnAllListCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListCOA.SelectedIndex = -1 : FilterTextListCOA.Text = "" : gvListCOA.SelectedIndex = -1
        UpdateCheckedMat()
        If Session("WarningListMat") IsNot Nothing Then
            Session("WarningListMatView") = Session("WarningListMat")
            gvListCOA.DataSource = Session("WarningListMatView")
            gvListCOA.DataBind()
            mpeListCOA.Show()
        Else
            Session("WarningListMat") = "There is no posting KIK data available for this time!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub lkbCloseListCOA_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListCOA, pnlListCOA, mpeListCOA, False)
    End Sub

    Protected Sub btnSearchCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListCOA.SelectedIndex = -1 : FilterTextListCOA.Text = "" : gvListCOA.DataSource = Nothing : gvListCOA.DataBind() : Session("WarningListMat") = Nothing : Session("WarningListMatView") = Nothing
        BindListCOA()
    End Sub

    Protected Sub btnClearCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        acctgoid2.Text = ""
        acctgcode.Text = ""
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindListCOA()
        If Session("TblPayment") IsNot Nothing Then
            Dim dv As DataView = Session("TblPayment").DefaultView
            dv.RowFilter = "checkvalue='True'"
            Dim iCount As Integer = dv.Count
            If dv.Count > 0 Then
                dv.RowFilter = "checkvalue='True'"
                If Session("TblDtl") Is Nothing Then
                    CreateTblDetail()
                End If
                Dim objTable As DataTable = Session("TblDtl")
                Dim objView As DataView = objTable.DefaultView
                Dim iSeq As Integer = objTable.Rows.Count + 1
                dv.Sort = "personname, paycommdtlrefno"
                For C1 As Integer = 0 To dv.Count - 1
                    objView.RowFilter = "conaroid=" & dv(C1)("conaroid")
                    If objView.Count > 0 Then

                    Else
                        Dim rv As DataRowView = objView.AddNew()
                        rv.BeginEdit()

                        rv("paycommdtlseq") = iSeq
                        rv("conaroid") = dv(C1)("conaroid")
                        rv("personname") = dv(C1)("sales")
                        rv("paycommdtlrefno") = dv(C1)("paycommdtlrefno")
                        rv("paycommdtlrefdesc") = dv(C1)("paycommdtlrefdesc").ToString
                        rv("paycommdtlamt") = dv(C1)("paycommdtlamt").ToString
                        rv("paycommdtlnote") = ""
                        rv("custname") = dv(C1)("custname").ToString
                        rv("paymentdate") = dv(C1)("paymentdate").ToString
                        rv("cashbankamt") = dv(C1)("cashbankamt").ToString
                        rv("ppnamt") = dv(C1)("ppnamt").ToString

                        rv("t_spv_comm_calc_range_from") = dv(C1)("rangeone").ToString
                        rv("t_spv_comm_calc_range_to") = dv(C1)("rangetwo").ToString
                        rv("t_spv_comm_calc_days") = dv(C1)("jml_hari").ToString
                        rv("t_spv_comm_calc_pct") = dv(C1)("percentage").ToString
                        rv("t_spv_comm_calc_ref_id") = dv(C1)("trnjualmstoid").ToString
                        rv("m_person_id") = dv(C1)("personoid").ToString

                        rv.EndEdit()
                        iSeq += 1
                    End If
                    objView.RowFilter = ""
                Next
                dv.RowFilter = ""
                Session("TblDtl") = objTable
                gvDtl.DataSource = Session("TblDtl")
                gvDtl.DataBind()
                ClearDetail()
                CountHdrAmt()
                cProc.SetModalPopUpExtender(btnHideListCOA, pnlListCOA, mpeListCOA, False)
            Else
                Session("WarningListDtl") = "Please select Commission data first!"
                showMessage(Session("WarningListDtl"), 2)
            End If
        Else
            Session("WarningListDtl") = "There is no posting Commission data available for this time!"
            showMessage(Session("WarningListDtl"), 2)
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDetail()
    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            cashbankglseq.Text = gvDtl.SelectedDataKey.Item("paycommdtlseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "paycommdtlseq=" & cashbankglseq.Text
                acctgoid2.Text = dv.Item(0).Item("conaroid").ToString
                'paycommdtlrefno.Text = dv.Item(0).Item("paycommdtlrefno").ToString
                'paycommdtlrefdesc.Text = dv.Item(0).Item("paycommdtlrefdesc").ToString
                'paycommdtlamt.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("paycommdtlamt").ToString), 2)
                'cashbankglnote.Text = dv.Item(0).Item("paycommdtlnote").ToString
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub gvListCOA_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        UpdateCheckedMat()
        gvListCOA.PageIndex = e.NewPageIndex
        gvListCOA.DataSource = Session("TblPaymentView")
        gvListCOA.DataBind()
        mpeListCOA.Show()
    End Sub

    Protected Sub gvDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("paycommdtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvDtl.DataSource = objTable
        gvDtl.DataBind()
        CountHdrAmt()
        ClearDetail()
    End Sub
#End Region

    Protected Sub gvListCOA_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 2)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 2)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 2)
        End If
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        activeflag.SelectedValue = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 2)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 2)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 2)
        End If
    End Sub

    Protected Sub lkbAddToList_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        UpdateCheckedMat()
        If Session("TblPayment") IsNot Nothing Then
            Dim dv As DataView = Session("TblPayment").DefaultView
            dv.RowFilter = "checkvalue='True'"
            Dim iCount As Integer = dv.Count
            If dv.Count > 0 Then
                dv.RowFilter = "checkvalue='True'"
                If Session("TblDtl") Is Nothing Then
                    CreateTblDetail()
                End If
                Dim objTable As DataTable = Session("TblDtl")
                Dim objView As DataView = objTable.DefaultView
                Dim iSeq As Integer = objTable.Rows.Count + 1
                For C1 As Integer = 0 To dv.Count - 1
                    objView.RowFilter = "paycommdtlrefdesc='" & dv(C1)("paycommdtlrefdesc") & "' AND conaroid=" & dv(C1)("conaroid")
                    If objView.Count > 0 Then
                        objView(0)("paycommdtlseq") = iSeq
                        objView(0)("conaroid") = dv(C1)("conaroid")
                        objView(0)("paycommdtlrefno") = dv(C1)("paycommdtlrefno")
                        objView(0)("paycommdtlrefdesc") = dv(C1)("paycommdtlrefdesc").ToString
                        objView(0)("paycommdtlamt") = dv(C1)("paycommdtlamt").ToString
                        objView(0)("paycommdtlnote") = ""
                        objView(0)("custname") = dv(C1)("custname").ToString
                        objView(0)("paymentdate") = dv(C1)("paymentdate").ToString
                        objView(0)("cashbankamt") = dv(C1)("cashbankamt").ToString
                        objView(0)("ppnamt") = dv(C1)("ppnamt").ToString
                    Else
                        Dim rv As DataRowView = objView.AddNew()
                        rv.BeginEdit()

                        rv("paycommdtlseq") = iSeq
                        rv("conaroid") = dv(C1)("conaroid")
                        rv("paycommdtlrefno") = dv(C1)("paycommdtlrefno")
                        rv("paycommdtlrefdesc") = dv(C1)("paycommdtlrefdesc").ToString
                        rv("paycommdtlamt") = dv(C1)("paycommdtlamt").ToString
                        rv("paycommdtlnote") = ""
                        rv("custname") = dv(C1)("custname").ToString
                        rv("paymentdate") = dv(C1)("paymentdate").ToString
                        rv("cashbankamt") = dv(C1)("cashbankamt").ToString
                        rv("ppnamt") = dv(C1)("ppnamt").ToString

                        rv.EndEdit()
                        iSeq += 1
                    End If
                    objView.RowFilter = ""
                Next
                dv.RowFilter = ""
                Session("TblDtl") = objTable
                gvDtl.DataSource = Session("TblDtl")
                gvDtl.DataBind()
                ClearDetail()
                CountHdrAmt()
                cProc.SetModalPopUpExtender(btnHideListCOA, pnlListCOA, mpeListCOA, False)
            Else
                Session("WarningListMat") = "Please select Commission data first!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "There is no posting Commission data available for this time!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub
End Class
