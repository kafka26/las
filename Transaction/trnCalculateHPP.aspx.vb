Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Accounting_CalculateHPP
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
    Dim iMask As Integer = 2
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetSelectedOid() As String
        Dim sRet As String = ""
        For C1 As Integer = 0 To GVDtl.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = GVDtl.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        If CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = True Then
                            sRet &= CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip & ","
                        End If
                    End If
                Next
            End If
        Next
        If sRet <> "" Then
            sRet = Left(sRet, sRet.Length - 1)
        End If
        Return sRet
    End Function

    Private Function GetSummaryGiro(ByVal sOids As String) As DataTable
        Dim dtRet As DataTable = Nothing
        sSql = "SELECT cashbankoid, cashbankno, cashbankamt, cashbankamtidr, cashbankamtusd, acctgoid, curroid, giroacctgoid, '' AS newcashbankno FROM QL_trncashbankmst WHERE cashbankoid IN (" & sOids & ")"
        dtRet = cKon.ambiltabel(sSql, "QL_girosummary")
        Return dtRet
    End Function

    Private Function GetTextValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function GetDDLValue(ByVal cc As System.Web.UI.ControlCollection, Optional ByVal sText As String = "") As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                If sText = "" Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
                Else
                    sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedItem.Text
                End If
            End If
        Next
        Return sReturn
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If matoid.Text = "" Then
            sError &= "- Please select MATERIAL field!<BR>"
        End If
        If matqty.Text = "" Then
            sError &= "- Please Fill MATERIAL Qty field!<BR>"
        Else
            If ToDouble(matqty.Text) = 0 Then
                sError &= "- Material Qty Must more than 0!<BR>"
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        If itemoid.Text = "" Then
            sError &= "- Please select Finish Good field!<BR>"
        End If
        If fgqty.Text = "" Then
            sError &= "- Please Fill Finish Good Qty field!<BR>"
        Else
            If ToDouble(fgqty.Text) = 0 Then
                sError &= "- Finish Good Qty Must more than 0!<BR>"
            End If
        End If
        Dim TblDtl As DataTable = Session("TblDtl2")
        If TblDtl Is Nothing Then
            sError &= "- Please Fill Detail Data!<BR>"
        Else
            If TblDtl.Rows.Count <= 0 Then
                sError &= "- Please Fill Detail Data!<BR>"
            End If
        End If

        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Public Function GetParameterID() As String
        Return Eval("matoid") & "," & Eval("matunitoid")
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitAllDDL()
        ' Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
      FillDDL(DDLBusUnit, sSql)
        ' Fill DDL Unit Material
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='UNIT' AND activeflag='ACTIVE'"
        FillDDL(dlcunitoid, sSql)
        FillDDL(ohdunitoid, sSql)
    End Sub

    Private Sub BindGiroData(ByVal dtTblUsage As DataTable)
        Dim dDMAmt As Double = 0
        Dim dDLCAmt As Double = ToMaskEdit(ToDouble(dlcamount.Text) * ToDouble(fgqty.Text), 2)
        Dim dFOHAmt As Double = ToMaskEdit(ToDouble(ohdamount.Text) * ToDouble(fgqty.Text), 2)

        For C1 As Integer = 0 To dtTblUsage.Rows.Count - 1
            sSql &= " SELECT " & dtTblUsage.Rows(C1)("stockvalue") * dtTblUsage.Rows(C1)("matqty") & " [DM IDR] , 0.0 AS  [DL IDR], 0.0 AS [FOH IDR], 0.0 [Confirm Qty] ,0.0 [Confirm IDR], '" & dtTblUsage.Rows(C1)("matcode").ToString & "' AS [Code_Mat], '" & dtTblUsage.Rows(C1)("matshortdesc").ToString & "' AS [Description], " & dtTblUsage.Rows(C1)("matqty") & " AS [Qty], " & dtTblUsage.Rows(C1)("stockvalue") & " AS [Value IDR], " & dtTblUsage.Rows(C1)("stockvalue") * dtTblUsage.Rows(C1)("matqty") & " AS [Amount IDR], '" & dtTblUsage.Rows(C1)("matunit").ToString & "' AS [Unit_Mat], 0.00 AS hppsatuan UNION ALL "
            dDMAmt += dtTblUsage.Rows(C1)("stockvalue") * dtTblUsage.Rows(C1)("matqty")
        Next
        'PRODUCTION RESULT DLC
        sSql &= " SELECT 0.0 AS  [DM IDR], " & dDLCAmt & " [DL IDR], 0.0 AS [FOH IDR], 0.0 [Confirm Qty], 0.0 [Confirm IDR], '' AS [Code_Mat], 'DLC' AS [Description], 0.0 AS [Qty], 0.0 AS [Value IDR], 0.0 AS [Amount IDR], '' AS [Unit_Mat], 0.00 AS hppsatuan UNION ALL"
        'PRODUCTION RESULT FOH
        sSql &= "  SELECT 0.0 AS  [DM IDR], 0.0 AS [DL IDR], " & dFOHAmt & " [FOH IDR], 0.0 [Confirm Qty],0.0 [Confirm IDR], '' AS [Code], 'OHD' AS [Description], 0.0 AS [Qty], 0.0 AS [Value IDR], 0.0 AS [Amount IDR], '' AS [Unit_Mat], 0.00 AS hppsatuan UNION ALL"
        'PRODUCTION RESULT CONF
        sSql &= " SELECT 0.0 AS  [DM IDR], 0.0 AS [DL IDR], 0.0 AS [FOH IDR], " & ToDouble(fgqty.Text) & " [Confirm Qty], 0.0 [Confirm IDR], '' AS [Code_Mat], 'Production Result' AS [Description], 0.0 AS [Qty], 0.0 AS [Value IDR], 0.0 AS [Amount IDR], '' AS [Unit_Mat], 0.00 AS hppsatuan UNION ALL "
        'PRODUCTION RESULT CONF
        sSql &= " SELECT 0.0 AS  [DM IDR], " & dDLCAmt & " AS [DL IDR], " & dFOHAmt & " AS [FOH IDR], 0.0 [Confirm Qty], " & (dDMAmt + dDLCAmt + dFOHAmt) & " AS [Confirm IDR], '' AS [Code_Mat], 'Cost Of Good Manufacture' AS [Description], 0.0 AS [Qty], 0.0 AS [Value IDR], " & dDMAmt & " AS [Amount IDR], '' AS [Unit_Mat], " & (dDMAmt + dDLCAmt + dFOHAmt) / ToDouble(fgqty.Text) & " AS hppsatuan "

        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstKIK")
        dtTbl.AcceptChanges()
        Session("TblCalculateHPP") = dtTbl
        gvCalculate.DataSource = Session("TblCalculateHPP")
        gvCalculate.DataBind()
    End Sub

    Private Sub UpdateNote()
        If Session("TblDataGiro") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDataGiro")
            For C1 As Integer = 0 To gvDtl.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvDtl.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim sNote As String = ""
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(10).Controls
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                            sNote = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                        End If
                    Next
                    dt.Rows(C1)("giroflagnote") = sNote
                End If
            Next
            dt.AcceptChanges()
            Session("TblDataGiro") = dt
        End If
    End Sub

    Private Sub BindListItem()
        sSql = "SELECT i.itemoid, i.itemcode, i.itemlongdescription AS itemshortdesc, 0.0 AS cbf, i.itemunit1 AS itemunitoid FROM QL_mstitem i WHERE i.cmpcode='" & CompnyCode & "' AND i.itemrecordstatus='ACTIVE' AND " & FilterDDLListItem.SelectedValue & " LIKE '%" & Tchar(FilterTextListItem.Text) & "%' ORDER BY i.itemcode"
        FillGV(gvListItem, sSql, "QL_mstitem")
    End Sub

    Private Sub InitDDLUnit2()
        'InitDDLUnit()
        ' Fill DDL Unit Finish Good
        sSql = "select itemUnit1, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit1 where itemoid=" & itemoid.Text & " union all select itemUnit2, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit2 where itemoid=" & itemoid.Text & " union all select itemUnit3, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit3 where itemoid=" & itemoid.Text & ""
        FillDDL(itemunitoid, sSql)
    End Sub

    Private Sub InitDDLUnit3()
        'InitDDLUnit()
        ' Fill DDL Unit Finish Good
        sSql = "select itemUnit1, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit1 where itemoid=" & matoid.Text & " union all select itemUnit2, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit2 where itemoid=" & matoid.Text & " union all select itemUnit3, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit3 where itemoid=" & matoid.Text & ""
        FillDDL(matunitoid, sSql)
    End Sub

    Private Sub InitDDLCat01ListMat()
        'Fill DDL Category 1
        sSql = "SELECT c1.cat1oid, c1.cat1code +' - '+ c1.cat1shortdesc FROM QL_mstcat1 c1 WHERE c1.cmpcode='" & CompnyCode & "' AND c1.activeflag='ACTIVE'"
        sSql &= " ORDER BY cat1code"
        If FillDDL(FilterDDLCat01ListMat, sSql) Then
            InitDDLCat02ListMat()
        Else
            FilterDDLCat02ListMat.Items.Clear()
            FilterDDLCat03ListMat.Items.Clear()
            FilterDDLCat04ListMat.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat02ListMat()
        'Fill DDL Category 2
        sSql = "SELECT cat2oid, cat2code +' - '+ cat2shortdesc FROM QL_mstcat2 c2 INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c1.cat1oid=c2.cat1oid WHERE c2.cmpcode='" & CompnyCode & "' AND c2.activeflag='ACTIVE' AND c1.cat1oid='" & FilterDDLCat01ListMat.SelectedValue & "'"
        sSql &= " ORDER BY cat2code"
        If FillDDL(FilterDDLCat02ListMat, sSql) Then
            InitDDLCat03ListMat()
        Else
            FilterDDLCat03ListMat.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat03ListMat()
        'Fill DDL Category 3
        sSql = "SELECT cat3oid, cat3code +' - '+ cat3shortdesc FROM QL_mstcat3 c3 INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c3.cmpcode AND c1.cat1oid=c3.cat1oid WHERE c3.cmpcode='" & CompnyCode & "' AND c3.activeflag='ACTIVE' AND c2.cat2oid='" & FilterDDLCat02ListMat.SelectedValue & "'"
        sSql &= " ORDER BY cat3code"
        If FillDDL(FilterDDLCat03ListMat, sSql) Then
            InitDDLCat04ListMat()
        Else
            FilterDDLCat04ListMat.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat04ListMat()
        'Fill DDL Category 4
        sSql = "SELECT genoid, gencode +' - '+ gendesc FROM QL_mstgen WHERE gengroup='CAT4' AND activeflag='ACTIVE' ORDER BY gencode"
        FillDDL(FilterDDLCat04ListMat, sSql)
    End Sub

    Private Sub BindMatData()
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sSql = "SELECT 'False' AS checkvalue, itemcat1, itemcat2, itemcat3, itemcat4, m.itemoid AS matoid, m.itemCode AS matcode, m.itemLongDescription AS matshortdesc, m.itemUnit1 AS matunitoid, g.gendesc AS matunit, 0.0 AS matqty, (ISNULL((SELECT SUM(ISNULL(st.stockqty, 0) * ISNULL(st.stockvalueidr, 0)) / NULLIF(SUM(ISNULL(st.stockqty, 0)), 0) FROM QL_stockvalue st WHERE st.cmpcode=m.cmpcode AND st.periodacctg IN ('" & sPeriod & "', '" & GetLastPeriod(sPeriod) & "') AND st.refoid=m.itemoid AND closeflag=''), 0.0)) AS stockvalue, ISNULL((SELECT SUM(crd.saldoakhir) FROM QL_crdstock crd WHERE crd.cmpcode=m.cmpcode AND crd.refoid=m.itemoid AND crd.periodacctg IN ('" & sPeriod & "', '" & GetLastPeriod(sPeriod) & "') AND crd.closingdate='01/01/1900'), 0.0) AS stockqty,unit3unit1conversion, unit2unit1conversion, itemunit1, itemunit2, itemunit3 FROM QL_mstcat1 c1 INNER JOIN QL_mstitem m ON m.itemCat1=c1.cat1oid INNER JOIN QL_mstgen g ON genoid=itemUnit1 WHERE m.cmpcode='" & DDLBusUnit.SelectedValue & "' ORDER BY m.itemCode"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstmat")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = "stockqty>0"
        If dv.Count > 0 Then
            Session("TblListMat") = dv.ToTable
            Session("TblListMatView") = dv.ToTable
            gvListMat2.DataSource = Session("TblListMatView")
            gvListMat2.DataBind()
        Else
            Session("ErrMat") = "Material data can't be found!"
            showMessage(Session("ErrMat"), 2)
        End If
    End Sub

    Private Sub UpdateCheckedMat()
        Dim dv As DataView = Session("TblListMat").DefaultView
        dv.AllowEdit = True
        For C1 As Integer = 0 To gvListMat2.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat2.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        dv.RowFilter = "matoid=" & sOid
                        If cbcheck = True Then
                            dv(0)("checkvalue") = "True"
                            Dim cc2 As System.Web.UI.ControlCollection = row.Cells(3).Controls
                            dv(0)("matqty") = ToDouble(GetTextValue(cc2))
                            cc2 = row.Cells(4).Controls
                            dv(0)("matunit") = GetDDLValue(cc2, "Text")
                            dv(0)("matunitoid") = GetDDLValue(cc2)
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                        dv.RowFilter = ""
                    End If
                Next
            End If
        Next
    End Sub

    Private Sub UpdateCheckedMatView()
        Dim dv As DataView = Session("TblListMatView").DefaultView
        dv.AllowEdit = True
        For C1 As Integer = 0 To gvListMat2.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat2.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        dv.RowFilter = "matoid=" & sOid
                        If cbcheck = True Then
                            dv(0)("checkvalue") = "True"
                            Dim cc2 As System.Web.UI.ControlCollection = row.Cells(3).Controls
                            dv(0)("matqty") = ToDouble(GetTextValue(cc2))
                            cc2 = row.Cells(4).Controls
                            dv(0)("matunit") = GetDDLValue(cc2, "Text")
                            dv(0)("matunitoid") = GetDDLValue(cc2)
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                        dv.RowFilter = ""
                    End If
                Next
            End If
        Next
    End Sub

    Private Sub CreateTblDetailMat()
        Dim dtlTable As DataTable = New DataTable("QL_mstbomdtl2")
        dtlTable.Columns.Add("calcdtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matreftype", Type.GetType("System.String"))
        dtlTable.Columns.Add("matoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("matqty_unitkecil", Type.GetType("System.Double"))
        dtlTable.Columns.Add("matqty_unitbesar", Type.GetType("System.Double"))
        dtlTable.Columns.Add("matunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("matshortdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("matunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("stockvalue", Type.GetType("System.Double"))
        Session("TblDtl2") = dtlTable
    End Sub

    Private Sub ClearDetail2()
        matshortdesc.Text = ""
        matoid.Text = ""
        matqty.Text = ""
        matunitoid.Items.Clear()
        matqty_unitkecil.Text = ""
        matqty_unitbesar.Text = ""
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        sSql = "SELECT cm.cmpcode, cm.calculatehppmstoid, cm.itemoid, cm.itemunitoid, m.itemShortDescription AS itemshortdesc, m.itemCode AS matcode, cm.calculatehppdate, cm.dlcamount, cm.ohdamount, cm.upduser, cm.updtime, itemqty FROM QL_trncalculatehppmst cm INNER JOIN QL_mstitem m ON m.itemoid=cm.itemoid INNER JOIN QL_mstgen g ON g.genoid=cm.itemunitoid WHERE cm.cmpcode='" & CompnyCode & "' AND cm.calculatehppmstoid=" & sOid
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        While xreader.Read
            DDLBusUnit.SelectedValue = xreader("cmpcode").ToString
            calculatehppmstoid.Text = Trim(xreader("calculatehppmstoid").ToString)
            itemoid.Text = Trim(xreader("itemoid").ToString)
            tbDate.Text = Format(xreader("calculatehppdate"), "MM/dd/yyyy")
            itemshortdesc.Text = Trim(xreader("itemshortdesc").ToString)
            fgqty.Text = ToMaskEdit(ToDouble(Trim(xreader("itemqty").ToString)), 4)
            dlcamount.Text = ToMaskEdit(ToDouble(Trim(xreader("dlcamount").ToString)), 2)
            ohdamount.Text = ToMaskEdit(ToDouble(Trim(xreader("ohdamount").ToString)), 2)
            InitDDLUnit2()
            itemunitoid.SelectedValue = (xreader("itemunitoid").ToString)
            dlcunitoid.SelectedValue = (xreader("itemunitoid").ToString)
            ohdunitoid.SelectedValue = (xreader("itemunitoid").ToString)
            itemcode.Text = Trim(xreader("matcode").ToString)
            itemshortdesc.Text = Trim(xreader("itemshortdesc").ToString)
            upduser.Text = Trim(xreader("upduser").ToString)
            updtime.Text = Trim(xreader("updtime").ToString)
        End While
        xreader.Close()
        conn.Close()
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sSql = "SELECT dtl.cmpcode, dtl.calculatehppdtloid, dtl.calculatehppmstoid, dtl.calculatehppdtlseq AS calcdtlseq, dtl.matoid, dtl.matunitoid, matqty, matqty_unitbesar, matqty_unitkecil, m.itemcode AS matcode, m.itemshortdescription AS matshortdesc, g.gendesc AS matunit, valueidr AS stockvalue, ISNULL((SELECT SUM(crd.saldoakhir) FROM QL_crdstock crd WHERE crd.cmpcode=m.cmpcode AND crd.refoid=m.itemoid AND crd.periodacctg IN ('" & sPeriod & "', '" & GetLastPeriod(sPeriod) & "') AND crd.closingdate='01/01/1900'), 0.0) AS stockqty,unit3unit1conversion, unit2unit1conversion, itemunit1, itemunit2, itemunit3 FROM QL_trncalculatehppdtl dtl INNER JOIN QL_mstitem m ON m.itemoid=dtl.matoid INNER JOIN QL_mstgen g ON g.genoid=dtl.matunitoid WHERE dtl.calculatehppmstoid=" & sOid

        Dim dtMat2 As DataTable = cKon.ambiltabel(sSql, "QL_trncalculatehppdtl")
        Dim dvx2 As DataView = dtMat2.DefaultView
        dvx2.Sort = "calcdtlseq"
        dtMat2 = dvx2.ToTable
        For C1 As Integer = 0 To dtMat2.Rows.Count - 1
            dtMat2.Rows(C1)("calcdtlseq") = C1 + 1
        Next
        dtMat2.AcceptChanges()
        Session("TblDtl2") = dtMat2
        GVDtl.DataSource = Session("TblDtl2")
        GVDtl.DataBind()

        'calculate COGM
        If Not Session("TblDtl2") Is Nothing Then
            Dim dtTblUsage As DataTable = Session("TblDtl2")
            Dim dDMAmt As Double = 0
            Dim dDLCAmt As Double = ToDouble(dlcamount.Text) * ToDouble(fgqty.Text)
            Dim dFOHAmt As Double = ToDouble(ohdamount.Text) * ToDouble(fgqty.Text)
            sSql = ""
            For C1 As Integer = 0 To dtTblUsage.Rows.Count - 1
                sSql &= " SELECT " & dtTblUsage.Rows(C1)("stockvalue") * dtTblUsage.Rows(C1)("matqty") & " [DM IDR] , 0.0 AS  [DL IDR], 0.0 AS [FOH IDR], 0.0 [Confirm Qty] ,0.0 [Confirm IDR], '" & dtTblUsage.Rows(C1)("matcode").ToString & "' AS [Code_Mat], '" & dtTblUsage.Rows(C1)("matshortdesc").ToString & "' AS [Description], " & dtTblUsage.Rows(C1)("matqty") & " AS [Qty], " & dtTblUsage.Rows(C1)("stockvalue") & " AS [Value IDR], " & dtTblUsage.Rows(C1)("stockvalue") * dtTblUsage.Rows(C1)("matqty") & " AS [Amount IDR], '" & dtTblUsage.Rows(C1)("matunit").ToString & "' AS [Unit_Mat], 0.0 AS hppsatuan UNION ALL "
                dDMAmt += dtTblUsage.Rows(C1)("stockvalue") * dtTblUsage.Rows(C1)("matqty")
            Next
            'PRODUCTION RESULT DLC
            sSql &= " SELECT 0.0 AS  [DM IDR], " & dDLCAmt & " [DL IDR], 0.0 AS [FOH IDR], 0.0 [Confirm Qty], 0.0 [Confirm IDR], '' AS [Code_Mat], 'DLC' AS [Description], 0.0 AS [Qty], 0.0 AS [Value IDR], 0.0 AS [Amount IDR], '' AS [Unit_Mat], 0.0 AS hppsatuan UNION ALL"
            'PRODUCTION RESULT FOH
            sSql &= "  SELECT 0.0 AS  [DM IDR], 0.0 AS [DL IDR], " & dFOHAmt & " [FOH IDR], 0.0 [Confirm Qty],0.0 [Confirm IDR], '' AS [Code], 'OHD' AS [Description], 0.0 AS [Qty], 0.0 AS [Value IDR], 0.0 AS [Amount IDR], '' AS [Unit_Mat], 0.0 AS hppsatuan UNION ALL"
            'PRODUCTION RESULT CONF
            sSql &= " SELECT 0.0 AS  [DM IDR], 0.0 AS [DL IDR], 0.0 AS [FOH IDR], " & ToDouble(fgqty.Text) & " [Confirm Qty], 0.0 [Confirm IDR], '' AS [Code_Mat], 'Production Result' AS [Description], 0.0 AS [Qty], 0.0 AS [Value IDR], 0.0 AS [Amount IDR], '' AS [Unit_Mat], 0.0 AS hppsatuan UNION ALL "
            'PRODUCTION RESULT CONF
            sSql &= " SELECT 0.0 AS  [DM IDR], " & dDLCAmt & " AS [DL IDR], " & dFOHAmt & " AS [FOH IDR], 0.0 [Confirm Qty], " & (dDMAmt + dDLCAmt + dFOHAmt) & " AS [Confirm IDR], '' AS [Code_Mat], 'Cost Of Good Manufacture' AS [Description], 0.0 AS [Qty], 0.0 AS [Value IDR], " & dDMAmt & " AS [Amount IDR], '' AS [Unit_Mat], " & (dDMAmt + dDLCAmt + dFOHAmt) / ToDouble(fgqty.Text) & " AS hppsatuan "

            Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstKIK")
            dtTbl.AcceptChanges()
            Session("TblCalculateHPP") = dtTbl
            gvCalculate.DataSource = Session("TblCalculateHPP")
            gvCalculate.DataBind()
        End If
        ClearDetail2()
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT 'False' AS checkvalue, cm.cmpcode, cm.calculatehppmstoid, cm.itemunitoid, m.itemShortDescription AS itemshortdesc, m.itemCode AS matcode, CONVERT(CHAR(10), cm.calculatehppdate, 101) AS calculatehppdate, cm.dlcamount, cm.ohdamount, g.gendesc AS unit, itemqty, calculatehppmststatus FROM QL_trncalculatehppmst cm INNER JOIN QL_mstitem m ON m.itemoid=cm.itemoid INNER JOIN QL_mstgen g ON g.genoid=cm.itemunitoid WHERE m.cmpcode='" & DDLBusUnit.SelectedValue & "' "

        sSql &= sSqlPlus & " ORDER BY CAST(cm.calculatehppdate AS DATETIME) DESC, cm.calculatehppmstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_mstbom")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnCalculateHPP.aspx")
        End If
        If checkPagePermission("~\Transaction\trnCalculateHPP.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Calculate HPP"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        If Not Page.IsPostBack Then
            upduser.Text = Session("UserID") : updtime.Text = GetServerTime().ToString
            InitAllDDL()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                calculatehppmstoid.Text = GenerateID("QL_TRNCALCULATEHPPMST", CompnyCode)
                tbDate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("Success") Is Nothing And Session("Success") <> "" Then
            Response.Redirect("~\Transaction\trnCalculateHPP.aspx?awal=true")
        End If

        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("ErrMat") Is Nothing And Session("ErrMat") <> "" Then
            Session("ErrMat") = Nothing
            cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, True)
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Master\mstBOM.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnShowData_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnShowData.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select BUSINESS UNIT first", 2)
            Exit Sub
        End If
        Dim sErr As String = ""
        If tbDate.Text = "" Then
            sErr = "Please fill DATE first!"
        Else
            If Not IsValidDate(tbDate.Text, "MM/dd/yyyy", sErr) Then
                sErr = "DATE is invalid! " & sErr
            Else
                If CDate(tbDate.Text & " 23:59:59") > CDate(Format(GetServerTime(), "MM/dd/yyyy 23:59:59")) Then
                    sErr = "DATE must be less or equal than TODAY!"
                End If
            End If
        End If
        If fgqty.Text = "" Then
            sErr = "Please fill Finish Good Qty!"
        Else
            If ToDouble(fgqty.Text) = 0 Then
                sErr = "Finish Good Qty Must more than 0!"
            End If
        End If
        If dlcamount.Text = "" Then
            sErr = "Please fill DLC Amount!"
        Else
            If ToDouble(dlcamount.Text) = 0 Then
                sErr = "DLC Amount Must more than 0!"
            End If
        End If
        If ohdamount.Text = "" Then
            sErr = "Please fill OHD Amount!"
        Else
            If ToDouble(ohdamount.Text) = 0 Then
                sErr = "OHD Amount Must more than 0!"
            End If
        End If
        If sErr <> "" Then
            showMessage(sErr, 2)
            Exit Sub
        End If
        BindGiroData(Session("TblDtl2"))
    End Sub

    Protected Sub cbHeader_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim bCheck As Boolean = False
        If sender.Checked Then
            bCheck = True
        End If
        For C1 As Integer = 0 To GVDtl.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = GVDtl.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = bCheck
                    End If
                Next
            End If
        Next
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 2)
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnCalculateHPP.aspx?awal=true")
    End Sub

    Protected Sub btnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosting.Click
        If IsInputValid() Then
            Dim dQty_unitkecil, dQty_unitbesar As Double
            GetUnitConverter(itemoid.Text, itemunitoid.SelectedValue, ToDouble(fgqty.Text), dQty_unitkecil, dQty_unitbesar)
            Dim iCalculatehppdtlOid As Integer = GenerateID("QL_TRNCALCULATEHPPDTL", CompnyCode)
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trncalculatehppmst (cmpcode,calculatehppmstoid,calculatehppdate,itemoid,itemqty,itemunitoid,dlcamount,ohdamount,calculatehppmststatus, createuser,createtime,upduser,updtime,itemqty_unitkecil,itemqty_unitbesar) VALUES ('" & CompnyCode & "', " & calculatehppmstoid.Text & ", '" & CDate(tbDate.Text) & "', " & itemoid.Text & ", " & ToDouble(fgqty.Text) & ", " & itemunitoid.SelectedValue & ", " & ToDouble(dlcamount.Text) & ", " & ToDouble(ohdamount.Text) & ", '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dQty_unitkecil & ", " & dQty_unitbesar & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_mstoid SET lastoid=" & calculatehppmstoid.Text & " WHERE tablename='QL_trncalculatehppmst' "
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trncalculatehppmst SET itemoid=" & itemoid.Text & ", itemqty=" & ToDouble(fgqty.Text) & ", itemunitoid=" & itemunitoid.SelectedValue & ", dlcamount=" & ToDouble(dlcamount.Text) & ", ohdamount=" & ToDouble(ohdamount.Text) & ", calculatehppmststatus='', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, itemqty_unitkecil=" & dQty_unitkecil & ", itemqty_unitbesar=" & dQty_unitbesar & " WHERE cmpcode='" & CompnyCode & "' AND calculatehppmstoid=" & calculatehppmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    sSql = "DELETE QL_trncalculatehppdtl WHERE cmpcode='" & CompnyCode & "' AND calculatehppmstoid=" & calculatehppmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If

                If Not Session("TblDtl2") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl2")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trncalculatehppdtl (cmpcode, calculatehppdtloid, calculatehppmstoid, calculatehppdtlseq, calculatehppreftype, matoid, matqty, matunitoid, calculatehppdtlnote, upduser, updtime, matqty_unitkecil, matqty_unitbesar, valueidr) VALUES ('" & CompnyCode & "', " & iCalculatehppdtlOid & ", " & calculatehppmstoid.Text & ", " & C1 + 1 & ", '', " & objTable.Rows(C1)("matoid").ToString & ", " & ToDouble(objTable.Rows(C1)("matqty").ToString) & ", " & objTable.Rows(C1)("matunitoid").ToString & ", '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(C1)("matqty_unitkecil").ToString) & ", " & ToDouble(objTable.Rows(C1)("matqty_unitbesar").ToString) & ", " & ToDouble(objTable.Rows(C1)("stockvalue").ToString) & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iCalculatehppdtlOid += 1
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iCalculatehppdtlOid - 1 & " WHERE tablename='QL_trncalculatehppdtl' "
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If

                objTrans.Commit()
                xCmd.Connection.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnPosting_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.ToString, 1)
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.ToString, 1)
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                xCmd.Connection.Close()
                showMessage(ex.ToString, 1)
                Exit Sub
            End Try
            Response.Redirect("~\Transaction\trnCalculateHPP.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnFindListMat2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("TblListMat") Is Nothing Then
            BindMatData()
        End If
        If Not Session("TblListMat") Is Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                UpdateCheckedMat()
                Dim dv As DataView = dt.DefaultView
                Dim sFilter As String = FilterDDLListMat2.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat2.Text) & "%'"
                If cbCat04ListMat.Checked And FilterDDLCat04ListMat.SelectedValue <> "" Then
                    sFilter &= " AND itemcat4=" & FilterDDLCat04ListMat.SelectedValue & ""
                Else
                    If cbCat03ListMat.Checked And FilterDDLCat03ListMat.SelectedValue <> "" Then
                        sFilter &= " AND itemcat3=" & FilterDDLCat03ListMat.SelectedValue & ""
                    Else
                        If cbCat02ListMat.Checked And FilterDDLCat02ListMat.SelectedValue <> "" Then
                            sFilter &= " AND itemcat2=" & FilterDDLCat02ListMat.SelectedValue & ""
                        Else
                            If cbCat01ListMat.Checked And FilterDDLCat01ListMat.SelectedValue <> "" Then
                                sFilter &= " AND itemcat1=" & FilterDDLCat01ListMat.SelectedValue & ""
                            End If
                        End If
                    End If
                End If
                dv.RowFilter = sFilter
                If dv.Count > 0 Then
                    Session("TblListMatView") = dv.ToTable
                    gvListMat2.DataSource = Session("TblListMatView")
                    gvListMat2.DataBind()
                    dv.RowFilter = ""
                    mpeListMat2.Show()
                Else
                    dv.RowFilter = ""
                    Session("ErrMat") = "Material data can't be found!"
                    showMessage(Session("ErrMat"), 2)
                End If
            Else
                Session("ErrMat") = "Material data can't be found!"
                showMessage(Session("ErrMat"), 2)
            End If
        End If
    End Sub

    Protected Sub btnAllListMat2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("TblListMat") Is Nothing Then
            BindMatData()
        End If
        If Not Session("TblListMat") Is Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                UpdateCheckedMat()
                FilterDDLListMat2.SelectedIndex = -1 : FilterTextListMat2.Text = ""
                InitDDLCat01ListMat() : cbCat01ListMat.Checked = False : cbCat02ListMat.Checked = False : cbCat03ListMat.Checked = False : cbCat04ListMat.Checked = False
                Session("TblListMatView") = dt
                gvListMat2.DataSource = Session("TblListMatView")
                gvListMat2.DataBind()
                mpeListMat2.Show()
            Else
                Session("ErrMat") = "Material data can't be found!"
                showMessage(Session("ErrMat"), 2)
            End If
        End If
    End Sub

    Protected Sub lkbAddToList_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Session("TblListMat") Is Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                UpdateCheckedMat()
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "checkvalue='True'"
                If dv.Count = 0 Then
                    Session("ErrMat") = "Please select Material data!"
                    showMessage(Session("ErrMat"), 2)
                    dv.RowFilter = ""
                    Exit Sub
                End If
                dv.RowFilter = ""
                dv.RowFilter = "checkvalue='True' AND matqty=0"
                If dv.Count > 0 Then
                    Session("ErrMat") = "Qty for every selected Material must be more than 0!"
                    showMessage(Session("ErrMat"), 2)
                    dv.RowFilter = ""
                    Exit Sub
                End If
                dv.RowFilter = ""
                If Session("TblDtl2") Is Nothing Then
                    CreateTblDetailMat()
                End If
                Dim dtMat As DataTable = Session("TblDtl2")
                Dim dvMat As DataView = dtMat.DefaultView
                Dim dQty_unitkecil, dQty_unitbesar As Double
                Dim iSeq As Integer = dvMat.Count + 1
                dvMat.AllowEdit = True
                dvMat.AllowNew = True
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    dvMat.RowFilter = "matoid=" & dv(C1)("matoid").ToString
                    If dvMat.Count > 0 Then
                        dvMat(0)("matqty") = ToDouble(dv(C1)("matqty").ToString)
                        dvMat(0)("matunitoid") = dv(C1)("matunitoid").ToString
                        GetUnitConverter(dv(C1)("matoid").ToString, dv(C1)("matunitoid").ToString, ToDouble(dv(C1)("matqty").ToString), dQty_unitkecil, dQty_unitbesar)
                        dvMat(0)("matqty_unitkecil") = dQty_unitkecil
                        dvMat(0)("matqty_unitbesar") = dQty_unitkecil
                        Dim dValue As Double = ToDouble(dv(C1)("stockvalue").ToString)
                        If dv(C1)("matunitoid").ToString = dv(C1)("itemunit2").ToString Then
                            dValue = ToDouble(dv(C1)("stockvalue").ToString) * ToDouble(dv(C1)("unit2unit1conversion").ToString)
                        ElseIf dv(C1)("matunitoid").ToString = dv(C1)("itemunit3").ToString Then
                            dValue = ToDouble(dv(C1)("stockvalue").ToString) * ToDouble(dv(C1)("unit3unit1conversion").ToString)
                        End If
                        dvMat(0)("stockvalue") = dValue
                    Else
                        Dim rv As DataRowView = dvMat.AddNew()
                        rv.BeginEdit()
                        rv("calcdtlseq") = iSeq
                        rv("matoid") = dv(C1)("matoid").ToString
                        rv("matqty") = ToDouble(dv(C1)("matqty").ToString)
                        GetUnitConverter(dv(C1)("matoid").ToString, dv(C1)("matunitoid").ToString, ToDouble(dv(C1)("matqty").ToString), dQty_unitkecil, dQty_unitbesar)
                        rv("matqty_unitkecil") = dQty_unitkecil
                        rv("matqty_unitbesar") = dQty_unitbesar
                        rv("matunitoid") = dv(C1)("matunitoid").ToString
                        rv("matcode") = dv(C1)("matcode").ToString
                        rv("matshortdesc") = dv(C1)("matshortdesc").ToString
                        rv("matunit") = dv(C1)("matunit").ToString
                        Dim dValue As Double = ToDouble(dv(C1)("stockvalue").ToString)
                        If dv(C1)("matunitoid").ToString = dv(C1)("itemunit2").ToString Then
                            dValue = ToDouble(dv(C1)("stockvalue").ToString) * ToDouble(dv(C1)("unit2unit1conversion").ToString)
                        ElseIf dv(C1)("matunitoid").ToString = dv(C1)("itemunit3").ToString Then
                            dValue = ToDouble(dv(C1)("stockvalue").ToString) * ToDouble(dv(C1)("unit3unit1conversion").ToString)
                        End If
                        rv("stockvalue") = dValue
                        rv.EndEdit()
                        iSeq += 1
                    End If
                    dvMat.RowFilter = ""
                Next
                dv.RowFilter = ""
                Session("TblDtl2") = dvMat.ToTable
                GVDtl.DataSource = Session("TblDtl2")
                GVDtl.DataBind()
                ClearDetail2()
            End If
            cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, False)
        Else

        End If
    End Sub

    Protected Sub lkbCloseListMat2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, False)
    End Sub

    Protected Sub btnFindListItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindListItem()
        mpeListItem.Show()
    End Sub

    Protected Sub btnAllListItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListItem.SelectedIndex = -1 : FilterTextListItem.Text = "" : gvListItem.SelectedIndex = -1
        BindListItem()
        mpeListItem.Show()
    End Sub

    Protected Sub lkbCloseListItem_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListItem, pnlListItem, mpeListItem, False)
    End Sub

    Protected Sub gvListItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        itemoid.Text = gvListItem.SelectedDataKey.Item("itemoid").ToString
        InitDDLUnit2()
        itemcode.Text = gvListItem.SelectedDataKey.Item("itemcode").ToString
        itemshortdesc.Text = gvListItem.SelectedDataKey.Item("itemshortdesc").ToString
        itemunitoid.SelectedValue = gvListItem.SelectedDataKey("itemunitoid").ToString
        itemunitoid_SelectedIndexChanged(Nothing, Nothing)
        btnSearchMat_Click(Nothing, Nothing)
        cProc.SetModalPopUpExtender(btnHideListItem, pnlListItem, mpeListItem, False)
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2) : Exit Sub
        End If
        FilterDDLListItem.SelectedIndex = -1 : FilterTextListItem.Text = "" : gvListItem.SelectedIndex = -1
        BindListItem()
        cProc.SetModalPopUpExtender(btnHideListItem, pnlListItem, mpeListItem, True)
    End Sub

    Protected Sub btnClearItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        itemoid.Text = ""
        itemshortdesc.Text = ""
        itemcode.Text = ""
        itemunitoid.Items.Clear()
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If itemoid.Text = "" Then
            showMessage("Please select Finish Good first!", 2)
            Exit Sub
        End If
        InitDDLCat01ListMat() : cbCat01ListMat.Checked = False : cbCat02ListMat.Checked = False : cbCat03ListMat.Checked = False : cbCat04ListMat.Checked = False
        FilterDDLListMat2.SelectedIndex = -1 : FilterTextListMat2.Text = "" : Session("TblListMat") = Nothing : Session("TblListMatView") = Nothing : gvListMat2.DataSource = Session("TblListMatView") : gvListMat2.DataBind()
        cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, True)
    End Sub

    Protected Sub bomqty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Text = ToMaskEdit(ToDouble(fgqty.Text), 4)
    End Sub

    Protected Sub matqty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Text = ToMaskEdit(ToDouble(matqty.Text), iMask)
    End Sub

    Protected Sub dlcamount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Text = ToMaskEdit(ToDouble(dlcamount.Text), iMask)
    End Sub

    Protected Sub ohdamount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Text = ToMaskEdit(ToDouble(ohdamount.Text), iMask)
    End Sub

    Protected Sub gvListItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvListItem.PageIndex = e.NewPageIndex
        BindListItem()
        mpeListItem.Show()
    End Sub

    Protected Sub gvListMat2_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        If Not Session("TblListMat") Is Nothing Then
            UpdateCheckedMat()
            UpdateCheckedMatView()
            gvListMat2.PageIndex = e.NewPageIndex
            gvListMat2.DataSource = Session("TblListMatView")
            gvListMat2.DataBind()
        End If
        mpeListMat2.Show()
    End Sub

    Protected Sub gvListMat2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(3).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    End If
                End If
            Next
            Dim str As String = e.Row.Cells(1).Text
            e.Row.Cells(1).Text = Mid(str, 1, 7) & vbCrLf & Mid(str, 8, 10) & vbCrLf & Mid(str, 18, 5)

            Dim iItemOid As Integer = 0
            cc = e.Row.Cells(0).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    iItemOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                End If
            Next

            cc = e.Row.Cells(4).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                    FillDDL(CType(myControl, System.Web.UI.WebControls.DropDownList), "select itemUnit1, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit1 where itemoid=" & iItemOid & " union all select itemUnit2, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit2 where itemoid=" & iItemOid & " union all select itemUnit3, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit3 where itemoid=" & iItemOid & "")
                    CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue = CType(myControl, System.Web.UI.WebControls.DropDownList).ToolTip
                End If
            Next
        End If
    End Sub

    Protected Sub gvCalculate_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCalculate.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 4)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 2)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 2)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 2)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 2)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 2)
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 2)
            e.Row.Cells(10).Text = ToMaskEdit(ToDouble(e.Row.Cells(10).Text), 2)
        End If
    End Sub

    Protected Sub itemunitoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        dlcunitoid.SelectedValue = itemunitoid.SelectedValue
        ohdunitoid.SelectedValue = itemunitoid.SelectedValue
    End Sub

    Protected Sub GVDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            calcdtlseq.Text = GVDtl.SelectedDataKey.Item("calcdtlseq").ToString
            If Session("TblDtl2") Is Nothing = False Then
                'i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl2")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "calcdtlseq=" & calcdtlseq.Text
                matoid.Text = dv.Item(0).Item("matoid").ToString
                matshortdesc.Text = dv.Item(0).Item("matshortdesc").ToString
                matqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("matqty").ToString), 2)
                matqty_unitkecil.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("matqty_unitkecil").ToString), 2)
                matqty_unitbesar.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("matqty_unitbesar").ToString), 2)
                InitDDLUnit3()
                matunitoid.SelectedValue = dv.Item(0).Item("matunitoid").ToString

                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsDetailInputValid() Then
            If Session("TblDtl2") Is Nothing Then
                CreateTblDetailMat()
            End If
            Dim objTable As DataTable = Session("TblDtl2")
            Dim dv As DataView = objTable.DefaultView
            If i_u2.Text = "New Detail" Then
                dv.RowFilter = "matoid=" & matoid.Text
            Else
                dv.RowFilter = "matoid=" & matoid.Text & " AND calcdtlseq<>" & calcdtlseq.Text
            End If
            If dv.Count > 0 Then
                dv.RowFilter = ""
                showMessage("This Data has been added before, please check!", 2)
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            Dim dQty_unitkecil, dQty_unitbesar As Double
            If i_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                calcdtlseq.Text = objTable.Rows.Count + 1
            Else
                objRow = objTable.Rows(calcdtlseq.Text - 1)
                objRow.BeginEdit()
            End If
            objRow("calcdtlseq") = calcdtlseq.Text
            objRow("matoid") = matoid.Text
            objRow("matshortdesc") = matshortdesc.Text
            objRow("matqty") = ToDouble(matqty.Text)
            objRow("matunitoid") = matunitoid.SelectedValue
            objRow("matunit") = matunitoid.SelectedItem.Text
            GetUnitConverter(matoid.Text, matunitoid.SelectedValue, ToDouble(matqty.Text), dQty_unitkecil, dQty_unitbesar)
            objRow("matqty_unitkecil") = dQty_unitkecil
            objRow("matqty_unitbesar") = dQty_unitbesar
            If i_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            Session("TblDtl2") = objTable
            GVDtl.DataSource = Session("TblDtl2")
            GVDtl.DataBind()
            ClearDetail2()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDetail2()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND cm.calculatehppdate>='" & FilterPeriod1.Text & " 00:00:00' AND cm.calculatehppdate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND cm.calculatehppmststatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Transaction\trncalculatehpp.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND cm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sSqlPlus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Transaction\trncalculatehpp.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND cm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnPrintHdr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub gvTRN_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTRN.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 2)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 2)
        End If
    End Sub
#End Region

    Protected Sub GVDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        Dim objTable As DataTable = Session("TblDtl2")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("calcdtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl2") = objTable
        GVDtl.DataSource = objTable
        GVDtl.DataBind()
        ClearDetail2()
    End Sub
End Class