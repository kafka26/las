Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_MaterialRequestNonKIK
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("matreqqty") = ToDouble(GetTextBoxValue(row.Cells(3).Controls))
									dtView(0)("matreqdtlnote") = GetTextBoxValue(row.Cells(5).Controls)
                                    'dtView(0)("selectedunitoid") = GetDropDownListValue(row.Cells(4).Controls)
                                    'dtView(0)("selectedunit") = GetDropDownListValue(row.Cells(4).Controls, "Text")
								End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
	End Function

	Private Function GetDropDownListValue(ByVal cc As System.Web.UI.ControlCollection, Optional ByVal sText As String = "") As String
		Dim sReturn As String = ""
		For Each myControl As System.Web.UI.Control In cc
			If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
				If sText = "" Then
					sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
				Else
					sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedItem.Text
				End If
			End If
		Next
		Return sReturn
	End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                dtView2.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("matreqqty") = ToDouble(GetTextBoxValue(row.Cells(3).Controls))
                                    dtView(0)("matreqdtlnote") = GetTextBoxValue(row.Cells(5).Controls)
                                    If dtView2.Count > 0 Then
                                        dtView2(0)("matreqqty") = ToDouble(GetTextBoxValue(row.Cells(3).Controls))
                                        dtView2(0)("matreqdtlnote") = GetTextBoxValue(row.Cells(5).Controls)
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If matreqrefoid.Text = "" Then
            sError &= "- Please select MATERIAL field!<BR>"
        End If
        If matreqqty.Text = "" Then
            sError &= "- Please fill QTY field!<BR>"
        Else
            If ToDouble(matreqqty.Text) <= 0 Then
                sError &= "- QTY must be more than 0 !<BR>"
            End If
        End If
        If matrequnitoid.SelectedValue = "" Then
            sError &= "- Please select UNIT field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If matreqdate.Text = "" Then
            sError &= "- Please fill REQUEST DATE field!<BR>"
            sErr = "None"
        Else
            If Not IsValidDate(matreqdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- REQUEST DATE is invalid! " & sErr & "<BR>"
            End If
        End If
        If deptoid.SelectedValue = "" Then
            sError &= "- Please select DEPARTMENT field!<BR>"
        End If
        If matreqwhoid.SelectedValue = "" Then
            sError &= "- Please select WAREHOUSE field!<BR>"
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill Detail Data!"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            matreqmststatus.Text = "In Process"
            Return False
        End If
        Return True
	End Function

	Public Function GetParameterID() As String
		Return Eval("itemoid") & "," & Eval("selectedunitoid")
	End Function
#End Region

#Region "Procedures"
	Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
		Dim strCaption As String = CompnyName
		If iType = 1 Then ' Error
			imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
		ElseIf iType = 2 Then ' Warning
			imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
		ElseIf iType = 3 Then ' Information
			imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
		Else
			imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
		End If
		lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
		cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
	End Sub

	Private Sub CheckRequestStatus()
		Dim nDays As Integer = 7
		sSql = "SELECT COUNT(*) FROM QL_trnmatreqmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND matreqmststatus='In Process'"
		If Session("CompnyCode") <> CompnyCode Then
			sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
		End If
		If checkPagePermission("~\Transaction\trnMatRequest.aspx", Session("SpecialAccess")) = False Then
			sSql &= " AND createuser='" & Session("UserID") & "'"
		End If
		If ToDouble(GetStrData(sSql)) > 0 Then
			lkbReqInProcess.Visible = True
			lkbReqInProcess.Text = "You have " & GetStrData(sSql) & " In Process Material Request Non KIK data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
		End If
	End Sub

	Private Sub InitALLDDL()
		' Fill DDL Business Unit
		sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
		If Session("CompnyCode") <> CompnyCode Then
			sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
		End If
		FillDDL(DDLBusUnit, sSql)
		If DDLBusUnit.Items.Count > 0 Then
			InitDDLDept()
			InitDDLWH()
		End If
		' Fill DDL Unit
		sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='UNIT'"
		If i_u.Text = "New Data" Then
			sSql &= " AND activeflag='ACTIVE'"
		End If
		FillDDL(matrequnitoid, sSql)
	End Sub

	Private Sub InitDDLGroupItem()
		sSql = "SELECT genoid,gencode FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='GROUPITEM' AND genoid > 0"
		FillDDL(matreqreftype, sSql)
	End Sub

	Private Sub InitDDLDept()
		' Fill DDL Department
		sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE cmpcode='" & DDLBusUnit.SelectedValue & "'"
		If i_u.Text = "New Data" Then
			sSql &= " AND activeflag='ACTIVE'"
		End If
		FillDDL(deptoid, sSql)
	End Sub

	Private Sub InitDDLWH()
		' Fill DDL Warehouse
		sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='WAREHOUSE' AND genoid>0"
		If i_u.Text = "New Data" Then
			sSql &= " AND activeflag='ACTIVE'"
		End If
		FillDDL(matreqwhoid, sSql)
	End Sub

	Private Sub BindTrnData(ByVal sSqlPlus As String)
		sSql = "SELECT div.divname, reqm.matreqmstoid, reqm.matreqno, CONVERT(VARCHAR(10), reqm.matreqdate, 101) AS matreqdate, de.deptname, g1.gendesc AS matreqwh, reqm.matreqmststatus AS  matreqmststatus, reqm.matreqmstnote AS matreqmstnote, reqm.createuser FROM QL_trnmatreqmst reqm INNER JOIN QL_mstdept de ON de.cmpcode=reqm.cmpcode AND de.deptoid=reqm.deptoid INNER JOIN QL_mstgen g1 ON g1.genoid=reqm.matreqwhoid INNER JOIN QL_mstdivision div ON div.cmpcode=reqm.cmpcode WHERE reqm.cmpcode='" & CompnyCode & "'"
		sSql &= sSqlPlus & " ORDER BY CONVERT(DATETIME, reqm.matreqdate) DESC, reqm.matreqmstoid DESC"
		Session("TblMst") = cKon.ambiltabel(sSql, "QL_trnmatreqmst")
		gvTRN.DataSource = Session("TblMst")
		gvTRN.DataBind()
		lblViewInfo.Visible = False
	End Sub

	Private Sub BindMatData()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, 'True' AS enableqty,itemcode AS matreqrefcode ,m.itemoid,m.itemGroup AS matreqreftype,m.itemgroupoid, '' AS matreqrefno,m.itemCode, m.itemLongDescription , 0.0 AS matreqqty , m.itemUnit1 ,m.itemUnit1 AS selectedunitoid, g.gendesc AS gendesc , '' AS matreqdtlnote, m.createuser, m.itemUnit1 AS UnitKecil ,m.itemUnit3 AS unitBesar,gendesc AS selectedunit,'' AS itemunit FROM QL_mstitem m INNER JOIN QL_mstgen g ON m.cmpcode=g.cmpcode AND m.itemUnit1=g.genoid WHERE m.cmpcode='" & CompnyCode & "' AND " & FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%' ORDER BY m.itemCode"
		Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstitem")
		If dt.Rows.Count > 0 Then
			Session("TblMat") = dt
			Session("TblMatView") = dt
			gvListMat.DataSource = Session("TblMat")
			gvListMat.DataBind()
			cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
		Else
			cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
			showMessage("Material data can't be found!", 2)
		End If
	End Sub

	Private Sub InitDDLCat1()
    End Sub

	Private Sub InitDDLCat2()
    End Sub

	Private Sub InitDDLCat3()
    End Sub

	Private Sub InitDDLCat4()
    End Sub

	Private Sub CreateTblDetail()
		Dim dtlTable As DataTable = New DataTable("QL_trnmatreqdtl")
		dtlTable.Columns.Add("matreqdtlseq", Type.GetType("System.Int32"))
		dtlTable.Columns.Add("matreqreftype", Type.GetType("System.String"))
		dtlTable.Columns.Add("matreqrefoid", Type.GetType("System.Int32"))
		dtlTable.Columns.Add("matreqrefcode", Type.GetType("System.String"))
		dtlTable.Columns.Add("matreqreflongdesc", Type.GetType("System.String"))
		dtlTable.Columns.Add("matreqqty", Type.GetType("System.Double"))
		dtlTable.Columns.Add("matrequnitoid", Type.GetType("System.Int32"))
		dtlTable.Columns.Add("matrequnit", Type.GetType("System.String"))
		dtlTable.Columns.Add("matreqdtlnote", Type.GetType("System.String"))
		dtlTable.Columns.Add("matreqqty_unitkecil", Type.GetType("System.Double"))
		dtlTable.Columns.Add("matreqqty_unitbesar", Type.GetType("System.Double"))
		Session("TblDtl") = dtlTable
	End Sub

	Private Sub ClearDetail()
		i_u2.Text = "New Detail"
		matreqdtlseq.Text = "1"
		If Not Session("TblDtl") Is Nothing Then
			Dim dt As DataTable = Session("TblDtl")
			If dt.Rows.Count > 0 Then
				matreqdtlseq.Text = dt.Rows.Count + 1
			End If
		End If
		matreqreftype.SelectedIndex = -1
		matreqrefoid.Text = ""
		matreqrefcode.Text = ""
		matreqreflongdesc.Text = ""
		btnSearchMat.Visible = True : matreqreftype.Enabled = True : matreqreftype.CssClass = "inpText"
		matreqqty.Text = ""
		matrequnitoid.SelectedIndex = -1
		matreqdtlnote.Text = ""
		gvReqDtl.SelectedIndex = -1
	End Sub

	Private Sub GenerateNoRequest()
        Dim sNo As String = "REQNK-" & Format(CDate(matreqdate.Text), "yyMM") & "-"
		sSql = "SELECT ISNULL(MAX(CAST(RIGHT(matreqno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnmatreqmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matreqno LIKE '%" & sNo & "%'"
		matreqno.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
	End Sub

	Private Sub FillTextBox(ByVal sOid As String)
		Try
			sSql = "SELECT reqm.cmpcode, reqm.matreqmstoid, reqm.periodacctg, reqm.matreqdate, reqm.matreqno, reqm.deptoid, reqm.matreqwhoid, reqm.matreqmstnote, reqm.matreqmststatus, reqm.createuser, reqm.createtime, reqm.upduser, reqm.updtime FROM QL_trnmatreqmst reqm WHERE reqm.cmpcode='" & CompnyCode & "' AND reqm.matreqmstoid=" & sOid & ""

			If conn.State = ConnectionState.Closed Then
				conn.Open()
			End If
			xCmd = New SqlCommand(sSql, conn)
			xreader = xCmd.ExecuteReader
			While xreader.Read
				DDLBusUnit.SelectedValue = Trim(xreader("cmpcode").ToString)
				DDLBusUnit_SelectedIndexChanged(Nothing, Nothing)
				deptoid.SelectedValue = Trim(xreader("deptoid").ToString)
				matreqmstoid.Text = Trim(xreader("matreqmstoid").ToString)
				periodacctg.Text = Trim(xreader("periodacctg").ToString)
				matreqdate.Text = Format(xreader("matreqdate"), "MM/dd/yyyy")
				matreqno.Text = Trim(xreader("matreqno").ToString)
				matreqwhoid.SelectedValue = Trim(xreader("matreqwhoid").ToString)
				matreqmstnote.Text = Trim(xreader("matreqmstnote").ToString)
				matreqmststatus.Text = Trim(xreader("matreqmststatus").ToString)
				createuser.Text = Trim(xreader("createuser").ToString)
				createtime.Text = Trim(xreader("createtime").ToString)
				upduser.Text = Trim(xreader("upduser").ToString)
				updtime.Text = Trim(xreader("updtime").ToString)
			End While
			xreader.Close()
			conn.Close()
		Catch ex As Exception
			showMessage(ex.Message, 1)
			btnSave.Visible = False : btnposting.Visible = False : btnDelete.Visible = False
		End Try
		DDLBusUnit.CssClass = "inpTextDisabled"
		DDLBusUnit.Enabled = False
		If matreqmststatus.Text <> "In Process" Then
			btnSave.Visible = False
			btnDelete.Visible = False
			btnposting.Visible = False
			btnAddToList.Visible = False
			gvReqDtl.Columns(0).Visible = False
			gvReqDtl.Columns(gvReqDtl.Columns.Count - 1).Visible = False
			lblsprno.Text = "Request No."
			matreqmstoid.Visible = False
			matreqno.Visible = True
		End If

        sSql = "SELECT reqd.matreqdtlseq, '' AS matreqrefcode ,reqd.matreqreftype, reqd.matreqrefoid,(SELECT m.itemLongDescription FROM QL_mstitem m WHERE m.itemoid=reqd.matreqrefoid AND reqd.cmpcode=m.cmpcode) AS matreqreflongdesc,reqd.matreqqty, reqd.matrequnitoid, g.gendesc AS matrequnit, reqd.matreqdtlnote AS matreqdtlnote,reqd.matreqqty_unitkecil,reqd.matreqqty_unitbesar FROM QL_trnmatreqdtl reqd INNER JOIN QL_mstgen g ON g.genoid=reqd.matrequnitoid WHERE reqd.cmpcode='" & CompnyCode & "' AND reqd.matreqmstoid=" & sOid & " ORDER BY reqd.matreqdtlseq"
		Dim dtTable As DataTable = cKon.ambiltabel(sSql, "QL_trnmatreqdtl")
		Session("TblDtl") = dtTable
		gvReqDtl.DataSource = Session("TblDtl")
		gvReqDtl.DataBind()
		ClearDetail()
	End Sub

	Private Sub PrintReport(ByVal sOid As String)
		Try
			report.Load(Server.MapPath(folderReport & "rptMatRequest.rpt"))
			Dim sWhere As String = "WHERE"
			If Session("CompnyCode") <> CompnyCode Then
				sWhere &= " reqm.cmpcode='" & Session("CompnyCode") & "'"
			Else
				sWhere &= " reqm.cmpcode LIKE '%'"
			End If
			If sOid = "" Then
				sWhere = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
				If cbPeriode.Checked Then
					If IsValidPeriod() Then
						sWhere &= " AND reqm.matreqdate>='" & FilterPeriod1.Text & " 00:00:00' AND reqm.matreqdate<='" & FilterPeriod2.Text & " 23:59:59'"
					Else
						Exit Sub
					End If
				End If
				If cbStatus.Checked Then
					sWhere &= " And reqm.matreqmststatus='" & FilterDDLStatus.SelectedValue & "'"
				End If
				If checkPagePermission("~\Transaction\trnMatRequest.aspx", Session("SpecialAccess")) = False Then
					sWhere &= " AND reqm.createuser='" & Session("UserID") & "'"
				End If
			Else
				sWhere &= " AND reqm.matreqmstoid=" & sOid
			End If
			report.SetParameterValue("sWhere", sWhere)
			cProc.SetDBLogonForReport(report)
			Response.Buffer = False
			Response.ClearContent()
			Response.ClearHeaders()
			report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "MaterialRequestPrintOut")
			report.Close()
			report.Dispose()
		Catch ex As Exception
			report.Close()
			report.Dispose()
		End Try
		Response.Redirect("~\Transaction\trnMatRequest.aspx?awal=true")
	End Sub
#End Region

#Region "Events"
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("UserID") = "" Then
			Response.Redirect("~/Other/login.aspx")
		End If
		If Request.QueryString("awal") = "true" Then
			' Simpan session ke variabel temporary supaya tidak hilang
			Dim userId As String = Session("UserID")
			Dim xsetAcc As DataTable = Session("SpecialAccess")
			Dim appLimit As Decimal = Session("ApprovalLimit")
			Dim xsetRole As DataTable = Session("Role")
			Dim cmpcode As String = Session("CompnyCode")
			' Clear all session
			Session.Clear()
			' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
			Session("UserID") = userId
			Session("SpecialAccess") = xsetAcc
			Session("ApprovalLimit") = appLimit
			Session("Role") = xsetRole
			Session("CompnyCode") = cmpcode
			' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
			Response.Redirect("~\Transaction\trnMatRequest.aspx")
		End If
		If checkPagePermission("~\Transaction\trnMatRequest.aspx", Session("Role")) = False Then
			Response.Redirect("~\Other\NotAuthorize.aspx")
		End If
		Page.Title = CompnyName & " - Material Request Non KIK"
		Session("oid") = Request.QueryString("oid")
		If Session("oid") = "" Or Session("oid") = Nothing Then
			i_u.Text = "New Data"
		Else
			i_u.Text = "Update Data"
		End If
		btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
		btnposting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
		If Not Page.IsPostBack Then
			createuser.Text = Session("UserID")
			createtime.Text = GetServerTime().ToString
			upduser.Text = "-"
			updtime.Text = "-"
			CheckRequestStatus()
			FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
			FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
			InitALLDDL()
			InitDDLGroupItem()
			If Session("oid") <> Nothing And Session("oid") <> "" Then
				FillTextBox(Session("oid"))
				TabContainer1.ActiveTabIndex = 1
			Else
                matreqmstoid.Text = GenerateID("QL_TRNMATREQMST", CompnyCode)
				matreqdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
				matreqmststatus.Text = "In Process"
				periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
				btnDelete.Visible = False
				TabContainer1.ActiveTabIndex = 0
			End If
		End If
		If Not Session("TblDtl") Is Nothing Then
			Dim dt As New DataTable
			dt = Session("TblDtl")
			gvReqDtl.DataSource = dt
			gvReqDtl.DataBind()
		End If
	End Sub

	Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
		cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
		If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
			If lblPopUpMsg.Text = Session("EmptyListMat") Then
				Session("EmptyListMat") = Nothing
				cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
			End If
		End If
		If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
			If lblPopUpMsg.Text = Session("WarningListMat") Then
				Session("WarningListMat") = Nothing
				cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
			End If
		End If
		If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
			If lblPopUpMsg.Text = Session("SavedInfo") Then
				Response.Redirect("~\Transaction\trnMatRequest.aspx?awal=true")
			End If
		End If
	End Sub

	Protected Sub lkbReqInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbReqInProcess.Click
		Dim nDays As Integer = 7
		FilterText.Text = ""
		FilterDDL.SelectedIndex = -1
		cbPeriode.Checked = False
		FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
		FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
		cbStatus.Checked = True
		FilterDDLStatus.SelectedIndex = 0
		Dim sSqlPlus As String = " AND DATEDIFF(DAY, reqm.updtime, GETDATE()) > " & nDays & " AND matreqmststatus='In Process' "
		If checkPagePermission("~\Transaction\trnMatRequest.aspx", Session("SpecialAccess")) = False Then
			sSqlPlus &= " AND reqm.createuser='" & Session("UserID") & "' "
		End If
		BindTrnData(sSqlPlus)
	End Sub

	Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
		Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
		If cbPeriode.Checked Then
			If IsValidPeriod() Then
				sSqlPlus &= " AND reqm.reqdate>='" & FilterPeriod1.Text & " 00:00:00' AND reqm.reqdate<='" & CDate(FilterPeriod2.Text) & " 23:59:59'"
			Else
				Exit Sub
			End If
		End If
		If cbStatus.Checked Then
			If FilterDDLStatus.SelectedValue <> "All" Then
				sSqlPlus &= " AND reqm.matreqmststatus='" & FilterDDLStatus.SelectedValue & "'"
			End If
		End If
		If checkPagePermission("~\Transaction\trnMatRequest.aspx", Session("SpecialAccess")) = False Then
			sSqlPlus &= " AND reqm.createuser='" & Session("UserID") & "'"
		End If
		BindTrnData(sSqlPlus)
	End Sub

	Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
		Dim sSqlplus As String = ""
		FilterDDL.SelectedIndex = -1
		FilterText.Text = ""
		cbPeriode.Checked = False
		FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
		FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
		cbStatus.Checked = False
		FilterDDLStatus.SelectedIndex = -1
		If checkPagePermission("~\Transaction\trnMatRequest.aspx", Session("SpecialAccess")) = False Then
			sSqlplus &= " AND reqm.createuser='" & Session("UserID") & "'"
		End If
		BindTrnData(sSqlplus)
	End Sub

	Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
		gvTRN.PageIndex = e.NewPageIndex
		gvTRN.DataSource = Session("TblMst")
		gvTRN.DataBind()
	End Sub

	Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
		'Retrieve the table from the session object.
		Dim dt = TryCast(Session("TblMst"), DataTable)
		If dt IsNot Nothing Then
			'Sort the data.
			dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
			gvTRN.DataSource = Session("TblMst")
			gvTRN.DataBind()
		End If
	End Sub

	Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
		InitDDLDept()
		InitDDLWH()
	End Sub

	Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
		FilterDDLListMat.SelectedIndex = -1
		FilterTextListMat.Text = ""
		InitDDLCat1()
		Session("TblMat") = Nothing : Session("TblMatView") = Nothing : gvListMat.DataSource = Nothing
		gvListMat.DataBind() : tbData.Text = "5" : gvListMat.PageSize = CInt(tbData.Text)
		BindMatData()
		If matreqreftype.SelectedIndex > 2 Then
			'FilterTextListMat.TextMode = TextBoxMode.MultiLine
			FilterTextListMat.Rows = 3
			'FilterDDLListMat.Items(0).Enabled = True
		Else
			'FilterTextListMat.TextMode = TextBoxMode.SingleLine
			FilterTextListMat.Rows = 0
			'FilterDDLListMat.Items(0).Enabled = False
		End If
		cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
	End Sub

	Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
		'BindMatData()
		Dim sPlus As String = ""
		If UpdateCheckedMat() Then
			If matreqreftype.SelectedIndex > 2 Then
				If FilterDDLListMat.SelectedIndex = 0 Then
					Dim sText() As String = FilterTextListMat.Text.Split(";")
					For C1 As Integer = 0 To sText.Length - 1
						If sText(C1) <> "" Then
							sPlus &= FilterDDLListMat.SelectedValue & " LIKE '" & Tchar(sText(C1)) & "' OR "
						End If
					Next
					If sPlus <> "" Then
						sPlus = Left(sPlus, sPlus.Length - 4)
					Else
						sPlus = "1=1"
					End If
				Else
					sPlus = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
				End If
			Else
				sPlus = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
			End If
			Dim dv As DataView = Session("TblMat").DefaultView
			dv.RowFilter = sPlus
			If dv.Count > 0 Then
				Session("TblMatView") = dv.ToTable
				gvListMat.DataSource = Session("TblMatView")
				gvListMat.DataBind()
				dv.RowFilter = ""
				mpeListMat.Show()
			Else
				dv.RowFilter = ""
				Session("TblMatView") = Nothing
				gvListMat.DataSource = Session("TblMatView")
				gvListMat.DataBind()
				Session("WarningListMat") = "Material data can't be found!"
				showMessage(Session("WarningListMat"), 2)
			End If
		Else
			mpeListMat.Show()
		End If
	End Sub

	Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
		FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
		'InitDDLCat1()
		'cbCat01.Checked = False : cbCat02.Checked = False
		'cbCat03.Checked = False : cbCat04.Checked = False
		'If Session("TblMat") Is Nothing Then
		'BindMatData()
		'	If Session("TblMat").Rows.Count <= 0 Then
		'		Session("EmptyListMat") = "Material data can't be found!"
		'		showMessage(Session("EmptyListMat"), 2)
		'		Exit Sub
		'	End If
		'End If
		If UpdateCheckedMat() Then
			Dim dt As DataTable = Session("TblMat")
			Session("TblMatView") = dt
			gvListMat.DataSource = Session("TblMatView")
			gvListMat.DataBind()
		End If
		mpeListMat.Show()
	End Sub

	Protected Sub DDLCat01_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLCat01.SelectedIndexChanged
		InitDDLCat2()
		mpeListMat.Show()
	End Sub

	Protected Sub DDLCat02_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLCat02.SelectedIndexChanged
		InitDDLCat3()
		mpeListMat.Show()
	End Sub

	Protected Sub DDLCat03_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLCat03.SelectedIndexChanged
		InitDDLCat4()
		mpeListMat.Show()
	End Sub

	Protected Sub DDLCat04_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLCat04.SelectedIndexChanged
		mpeListMat.Show()
	End Sub

	Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
		If Not Session("TblMatView") Is Nothing Then
			Dim dtTbl As DataTable = Session("TblMatView")
			If dtTbl.Rows.Count > 0 Then
				Dim objTbl As DataTable = Session("TblMat")
				Dim objView As DataView = objTbl.DefaultView
				objView.AllowEdit = True
				objView.RowFilter = ""
				For C1 As Integer = 0 To dtTbl.Rows.Count - 1
					objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
					objView(0)("checkvalue") = "True"
					dtTbl.Rows(C1)("checkvalue") = "True"
					objView.RowFilter = ""
				Next
				objTbl.AcceptChanges()
				Session("TblMat") = objTbl
				Session("TblMatView") = dtTbl
				gvListMat.DataSource = Session("TblMatView")
				gvListMat.DataBind()
			End If
			mpeListMat.Show()
		Else
			Session("WarningListMat") = "Please show some material data first!"
			showMessage(Session("WarningListMat"), 2)
		End If
	End Sub

	Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
		If Not Session("TblMatView") Is Nothing Then
			Dim dtTbl As DataTable = Session("TblMatView")
			If dtTbl.Rows.Count > 0 Then
				Dim objTbl As DataTable = Session("TblMat")
				Dim objView As DataView = objTbl.DefaultView
				objView.AllowEdit = True
				objView.RowFilter = ""
				For C1 As Integer = 0 To dtTbl.Rows.Count - 1
					objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
					objView(0)("checkvalue") = "False"
					objView(0)("matreqqty") = 0
					objView(0)("matreqdtlnote") = ""
					dtTbl.Rows(C1)("checkvalue") = "False"
					dtTbl.Rows(C1)("matreqqty") = 0
					dtTbl.Rows(C1)("matreqdtlnote") = ""
					objView.RowFilter = ""
				Next
				objTbl.AcceptChanges()
				Session("TblMat") = objTbl
				Session("TblMatView") = dtTbl
				gvListMat.DataSource = Session("TblMatView")
				gvListMat.DataBind()
			End If
			mpeListMat.Show()
		Else
			Session("WarningListMat") = "Please show some material data first!"
			showMessage(Session("WarningListMat"), 2)
		End If
	End Sub

	Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
		If Session("TblMat") Is Nothing Then
			Session("WarningListMat") = "Selected material data can't be found!"
			showMessage(Session("WarningListMat"), 2)
			Exit Sub
		End If
		If UpdateCheckedMat() Then
			Dim dtTbl As DataTable = Session("TblMat")
			Dim dtView As DataView = dtTbl.DefaultView
			dtView.RowFilter = "checkvalue='True'"
			If dtView.Count > 0 Then
				FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : InitDDLCat1()
				cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
				Session("TblMatView") = dtView.ToTable
				gvListMat.DataSource = Session("TblMatView")
				gvListMat.DataBind()
				dtView.RowFilter = ""
				mpeListMat.Show()
			Else
				dtView.RowFilter = ""
				Session("TblMatView") = Nothing
				gvListMat.DataSource = Session("TblMatView")
				gvListMat.DataBind()
				Session("WarningListMat") = "Selected material data can't be found!"
				showMessage(Session("WarningListMat"), 2)
			End If
		Else
			mpeListMat.Show()
		End If
	End Sub

	Protected Sub lbShowData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbShowData.Click
		If Session("TblMat") Is Nothing Then
			Session("WarningListMat") = "Please find material data first!"
			showMessage(Session("WarningListMat"), 2)
			Exit Sub
		End If
		If tbData.Text = "" Then
			Session("WarningListMat") = "Please fill total data to be shown!"
			showMessage(Session("WarningListMat"), 2)
			Exit Sub
		Else
			If ToDouble(tbData.Text) <= 0 Then
				Session("WarningListMat") = "Total data to be shown must be more than 0!"
				showMessage(Session("WarningListMat"), 2)
				Exit Sub
			End If
		End If
		If UpdateCheckedMat2() Then
			gvListMat.PageSize = Int(tbData.Text)
			gvListMat.DataSource = Session("TblMatView")
			gvListMat.DataBind()
		End If
		mpeListMat.Show()
	End Sub

	Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
		If UpdateCheckedMat2() Then
			gvListMat.PageIndex = e.NewPageIndex
			gvListMat.DataSource = Session("TblMatView")
			gvListMat.DataBind()
		End If
		mpeListMat.Show()
	End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            'e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(3).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 4)
                    End If
                End If
            Next
            Dim iItemOid As Integer = 0
            cc = e.Row.Cells(0).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    iItemOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                End If
            Next
            cc = e.Row.Cells(4).Controls
            'For Each myControl As System.Web.UI.Control In cc
            '    If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
            '        FillDDL(CType(myControl, System.Web.UI.WebControls.DropDownList), "select itemUnit1, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit1 where itemoid=" & iItemOid & " union all select itemUnit2, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit2 where itemoid=" & iItemOid & " union all select itemUnit3, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit3 where itemoid=" & iItemOid & "")
            '        CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue = CType(myControl, System.Web.UI.WebControls.DropDownList).ToolTip
            '    End If
            'Next
        End If
    End Sub

	Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
		If Not Session("TblMat") Is Nothing Then
			If UpdateCheckedMat() Then
				Dim dtTbl As DataTable = Session("TblMat")
				Dim dtView As DataView = dtTbl.DefaultView
				dtView.RowFilter = "checkvalue='True'"
				Dim iCheck As Integer = dtView.Count
				If iCheck > 0 Then
					dtView.RowFilter = ""
					dtView.RowFilter = "checkvalue='True' AND matreqqty>0"
					If dtView.Count <> iCheck Then
						Session("WarningListMat") = "Quantity for every checked material data must be more than 0!"
						showMessage(Session("WarningListMat"), 2)
						dtView.RowFilter = ""
						Exit Sub
					End If
					If Session("TblDtl") Is Nothing Then
						CreateTblDetail()
					End If
					Dim objTable As DataTable
					objTable = Session("TblDtl")
					Dim dv As DataView = objTable.DefaultView
					Dim objRow As DataRow
					Dim counter As Integer = objTable.Rows.Count
					For C1 As Integer = 0 To dtView.Count - 1
						dv.RowFilter = "matreqrefoid=" & dtView(C1)("itemoid") & " AND matreqreftype='" & dtView(C1)("matreqreftype") & "'"
						If dv.Count > 0 Then
							dv.AllowEdit = True
							dv(0)("matreqqty") = dtView(C1)("matreqqty")
							dv(0)("matreqdtlnote") = dtView(C1)("matreqdtlnote")
							Dim OidUnit As Integer = dtView(C1)("selectedunitoid")
							Dim dUnit As String = dtView(C1)("selectedunit").ToString
						Else
                            counter += 1
                            Dim OidUnit As Integer = dtView(C1)("selectedunitoid")
                            Dim dUnit As String = dtView(C1)("selectedunit").ToString
                            objRow = objTable.NewRow()
                            objRow("matreqdtlseq") = counter
                            objRow("matreqreftype") = dtView(C1)("matreqreftype")
                            objRow("matreqrefoid") = dtView(C1)("itemoid")
                            objRow("matreqrefcode") = dtView(C1)("itemcode").ToString
                            objRow("matreqreflongdesc") = dtView(C1)("itemLongDescription").ToString
                            objRow("matreqqty") = dtView(C1)("matreqqty")
                            objRow("matreqqty_unitkecil") = dtView(C1)("matreqqty")
                            objRow("matreqqty_unitbesar") = dtView(C1)("matreqqty")
                            objRow("matrequnitoid") = dtView(C1)("selectedunitoid")
                            objRow("matrequnit") = dtView(C1)("selectedunit").ToString
                            objRow("matreqdtlnote") = dtView(C1)("matreqdtlnote")
                            objTable.Rows.Add(objRow)
						End If
						dv.RowFilter = ""
					Next
					dtView.RowFilter = ""
					Session("TblDtl") = objTable
					gvReqDtl.DataSource = objTable
					gvReqDtl.DataBind()
					cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
					ClearDetail()
				Else
					Session("WarningListMat") = "Please select material to add to list!"
					showMessage(Session("WarningListMat"), 2)
					Exit Sub
				End If
			End If
		Else
			Session("WarningListMat") = "Please show some material data first!"
			showMessage(Session("WarningListMat"), 2)
			Exit Sub
		End If
	End Sub

	Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
		cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
	End Sub

	Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
		If IsDetailInputValid() Then
			If Session("TblDtl") Is Nothing Then
				CreateTblDetail()
			End If
			Dim objTable As DataTable = Session("TblDtl")
			Dim dv As DataView = objTable.DefaultView
			If i_u2.Text = "New Detail" Then
                dv.RowFilter = "matreqrefoid=" & matreqrefoid.Text & " AND matreqreftype='" & matreqreftype.SelectedItem.Text & "'"
			Else
                dv.RowFilter = "matreqrefoid=" & matreqrefoid.Text & " AND matreqreftype='" & matreqreftype.SelectedItem.Text & "' AND matreqdtlseq<>" & matreqdtlseq.Text
			End If
			If dv.Count > 0 Then
				dv.RowFilter = ""
				showMessage("This Data has been added before, please check!", 2)
				Exit Sub
			End If
			dv.RowFilter = ""
			Dim objRow As DataRow
			If i_u2.Text = "New Detail" Then
				objRow = objTable.NewRow()
				matreqdtlseq.Text = objTable.Rows.Count + 1
				objRow("matreqdtlseq") = matreqdtlseq.Text
			Else
				objRow = objTable.Rows(matreqdtlseq.Text - 1)
				objRow.BeginEdit()
			End If
            objRow("matreqreftype") = matreqreftype.SelectedItem.Text
			objRow("matreqrefoid") = matreqrefoid.Text
			objRow("matreqrefcode") = matreqrefcode.Text
			objRow("matreqreflongdesc") = matreqreflongdesc.Text
			objRow("matreqqty") = ToDouble(matreqqty.Text)
			objRow("matrequnitoid") = matrequnitoid.SelectedValue
			objRow("matrequnit") = matrequnitoid.SelectedItem.Text
			objRow("matreqdtlnote") = matreqdtlnote.Text.Trim
			If i_u2.Text = "New Detail" Then
				objTable.Rows.Add(objRow)
			Else
				objRow.EndEdit()
			End If
			Session("TblDtl") = objTable
			gvReqDtl.DataSource = Session("TblDtl")
			gvReqDtl.DataBind()
			ClearDetail()
		End If
	End Sub

	Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
		ClearDetail()
	End Sub

	Protected Sub gvReqDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvReqDtl.RowDataBound
		If e.Row.RowType = DataControlRowType.DataRow Then
			e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
		End If
	End Sub

	Protected Sub gvReqDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvReqDtl.SelectedIndexChanged
		Try
			matreqdtlseq.Text = gvReqDtl.SelectedDataKey.Item("matreqdtlseq").ToString
			If Session("TblDtl") Is Nothing = False Then
				i_u2.Text = "Update Detail"
				Dim objTable As DataTable = Session("TblDtl")
				Dim dv As DataView = objTable.DefaultView
				dv.RowFilter = "matreqdtlseq=" & matreqdtlseq.Text
				matreqreftype.SelectedItem.Text = dv.Item(0).Item("matreqreftype").ToString
				btnSearchMat.Visible = False : matreqreftype.Enabled = False : matreqreftype.CssClass = "inpTextDisabled"
				'TypeRef.Text = ""
				matreqrefoid.Text = dv.Item(0).Item("matreqrefoid").ToString
				matreqrefcode.Text = dv.Item(0).Item("matreqrefcode").ToString
				matreqreflongdesc.Text = dv.Item(0).Item("matreqreflongdesc").ToString
				matreqqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("matreqqty").ToString), 4)
				matrequnitoid.SelectedValue = dv.Item(0).Item("matrequnitoid").ToString
				matreqdtlnote.Text = dv.Item(0).Item("matreqdtlnote").ToString
				If matreqreftype.SelectedValue = "Log" Or matreqreftype.SelectedValue = "Sawn Timber" Then
					matreqqty.Enabled = "False"
					matreqqty.CssClass = "inpTextDisabled"
				Else
					matreqqty.Enabled = "True"
					matreqqty.CssClass = "inpText"
				End If
				dv.RowFilter = ""
			End If
		Catch ex As Exception
			showMessage(ex.Message, 1)
		End Try
	End Sub

	Protected Sub gvReqDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvReqDtl.RowDeleting
		Dim objTable As DataTable = Session("TblDtl")
		Dim objRow() As DataRow
		objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
		objTable.Rows.Remove(objRow(e.RowIndex))
		For C1 As Int16 = 0 To objTable.Rows.Count - 1
			Dim dr As DataRow = objTable.Rows(C1)
			dr.BeginEdit()
			dr("matreqdtlseq") = C1 + 1
			dr.EndEdit()
		Next
		Session("TblDtl") = objTable
		gvReqDtl.DataSource = objTable
		gvReqDtl.DataBind()
		ClearDetail()
	End Sub

	Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
		If IsInputValid() Then
			Dim isRegenOid As Boolean = False
			If Session("oid") = Nothing Or Session("oid") = "" Then
				sSql = "SELECT COUNT(*) FROM QL_trnmatreqmst WHERE matreqmstoid=" & matreqmstoid.Text
				If CheckDataExists(sSql) Then
                    matreqmstoid.Text = GenerateID("QL_TRNMATREQMST", CompnyCode)
					isRegenOid = True
				End If
			Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNMATREQMST", "matreqmstoid", matreqmstoid.Text, "matreqmststatus", updtime.Text, "Post")
				If sStatusInfo <> "" Then
					showMessage(sStatusInfo, 2)
					matreqmststatus.Text = "In Process"
					Exit Sub
				End If
            End If
            matreqdtloid.Text = GenerateID("QL_TRNMATREQDTL", CompnyCode)
			periodacctg.Text = GetDateToPeriodAcctg(CDate(matreqdate.Text))
			If matreqmststatus.Text = "Post" Then
				GenerateNoRequest()
			End If
			Dim objTrans As SqlClient.SqlTransaction
			If conn.State = ConnectionState.Closed Then
				conn.Open()
			End If
			objTrans = conn.BeginTransaction()
			xCmd.Transaction = objTrans
			Try
				If Session("oid") = Nothing Or Session("oid") = "" Then
					sSql = "INSERT INTO QL_trnmatreqmst(cmpcode,matreqmstoid,periodacctg,matreqdate,matreqno,deptoid,matreqwhoid,matreqmstnote,matreqmststatus,createuser,createtime,upduser,updtime) VALUES " & _
				 " ('" & DDLBusUnit.SelectedValue & "', " & matreqmstoid.Text & ",'" & periodacctg.Text & "','" & matreqdate.Text & "','" & matreqno.Text & "'," & deptoid.SelectedValue & "," & matreqwhoid.SelectedValue & ",'" & Tchar(matreqmstnote.Text.ToString) & "','" & matreqmststatus.Text & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP)"
					xCmd.CommandText = sSql
					xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & matreqmstoid.Text & " WHERE tablename='QL_TRNMATREQMST' and cmpcode='" & CompnyCode & "'"
					xCmd.CommandText = sSql
					xCmd.ExecuteNonQuery()
				Else
					sSql = "UPDATE QL_trnmatreqmst SET periodacctg='" & periodacctg.Text & "', matreqdate='" & matreqdate.Text & "', matreqno='" & matreqno.Text & "', deptoid=" & deptoid.SelectedValue & ", matreqwhoid=" & matreqwhoid.SelectedValue & ", matreqmstnote='" & Tchar(matreqmstnote.Text.Trim) & "', matreqmststatus='" & matreqmststatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE matreqmstoid=" & matreqmstoid.Text
					xCmd.CommandText = sSql
					xCmd.ExecuteNonQuery()
					sSql = "DELETE FROM QL_trnmatreqdtl WHERE matreqmstoid=" & matreqmstoid.Text
					xCmd.CommandText = sSql
					xCmd.ExecuteNonQuery()
				End If
				If Not Session("TblDtl") Is Nothing Then
					Dim objTable As DataTable = Session("TblDtl")
					For c1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trnmatreqdtl (cmpcode,matreqdtloid,matreqmstoid,matreqdtlseq,matreqreftype,matreqrefoid,matreqqty,matrequnitoid,matreqdtlnote,matreqdtlstatus,upduser,updtime) VALUES" & _
                     " ('" & DDLBusUnit.SelectedValue & "'," & CInt(matreqdtloid.Text) & "," & matreqmstoid.Text & ", " & c1 + 1 & ",'" & objTable.Rows(c1).Item("matreqreftype").ToString & "'," & objTable.Rows(c1).Item("matreqrefoid") & "," & ToDouble(objTable.Rows(c1).Item("matreqqty").ToString) & ", " & objTable.Rows(c1).Item("matrequnitoid") & ",'" & Tchar(objTable.Rows(c1).Item("matreqdtlnote")) & "','','" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        matreqdtloid.Text += 1
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (CInt(matreqdtloid.Text) - 1) & " WHERE tablename='QL_TRNMATREQDTL' AND cmpcode='" & CompnyCode & "'"
					xCmd.CommandText = sSql
					xCmd.ExecuteNonQuery()
				End If
				objTrans.Commit()
				conn.Close()
			Catch exSql As SqlException
				objTrans.Rollback() : conn.Close()
				If ToDouble(Session("ErrorCounter")) < 5 Then
					If exSql.Number = 2627 Then
						Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
						btnSave_Click(sender, e)
						Exit Sub
					Else
						showMessage(exSql.Message, 1)
						matreqmststatus.Text = "In Process"
						Exit Sub
					End If
				Else
					showMessage(exSql.Message, 1)
					matreqmststatus.Text = "In Process"
					Exit Sub
				End If
			Catch ex As Exception
				objTrans.Rollback()
				showMessage(ex.Message, 1)
				conn.Close()
				matreqmststatus.Text = "In Process"
				Exit Sub
			End Try
			If isRegenOid Then
				Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & matreqmstoid.Text & ".<BR>"
			End If
			If matreqmststatus.Text = "Post" Then
				Session("SavedInfo") &= "Data have been posted with Request No. = " & matreqno.Text & "."
			End If
			If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
				showMessage(Session("SavedInfo"), 3)
			Else
				Response.Redirect("~\Transaction\trnMatRequest.aspx?awal=true")
			End If
		End If
	End Sub

	Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
		Response.Redirect("~\Transaction\trnMatRequest.aspx?awal=true")
	End Sub

	Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
		If matreqmstoid.Text = "" Then
			showMessage("Please select Material Request data first!", 2)
			Exit Sub
		Else
			Dim sStatusInfo As String = GetMultiUserStatus("QL_trnmatreqmst", "matreqmstoid", matreqmstoid.Text, "matreqmststatus", updtime.Text, "Post")
			If sStatusInfo <> "" Then
				showMessage(sStatusInfo, 2)
				matreqmststatus.Text = "In Process"
				Exit Sub
			End If
		End If
		If conn.State = ConnectionState.Closed Then
			conn.Open()
		End If
		Dim objTrans As SqlClient.SqlTransaction
		objTrans = conn.BeginTransaction()
		xCmd.Transaction = objTrans
		Try
			sSql = "DELETE FROM QL_trnmatreqdtl WHERE matreqmstoid=" & matreqmstoid.Text & ""
			xCmd.CommandText = sSql
			xCmd.ExecuteNonQuery()
			sSql = "DELETE FROM QL_trnmatreqmst WHERE matreqmstoid=" & matreqmstoid.Text & ""
			xCmd.CommandText = sSql
			xCmd.ExecuteNonQuery()
			objTrans.Commit()
			conn.Close()
		Catch ex As Exception
			objTrans.Rollback()
			conn.Close()
			showMessage(ex.Message, 1)
			Exit Sub
		End Try
		Response.Redirect("~\Transaction\trnMatRequest.aspx?awal=true")
	End Sub

    Protected Sub btnposting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnposting.Click
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        If isPeriodClosed(CompnyCode, CDate(sDate)) Then
            showMessage("Period " & GetDateToPeriodAcctg(CDate(sDate)) & " Have been Already CLOSED, Please CANCEL this transaction and try again!", 2)
            Exit Sub
        End If
        matreqmststatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

	Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
		PrintReport("")
	End Sub

	Protected Sub gvTRN_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvTRN.SelectedIndexChanged
		PrintReport(gvTRN.SelectedDataKey.Item("matreqmstoid").ToString)
	End Sub
#End Region

End Class