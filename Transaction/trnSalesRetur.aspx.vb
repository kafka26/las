Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_SalesRetur
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

    Structure VarUsageValue
        Public Val_IDR As Double
        Public Val_USD As Double
    End Structure

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    If shipmentrawmstoid.Text > 0 Then
                                        dtView(0)("retqty") = ToDouble(GetTextBoxValue(row.Cells(6).Controls))
                                        dtView(0)("retdtlnote") = GetTextBoxValue(row.Cells(7).Controls)
                                    Else
                                        dtView(0)("retqty") = ToDouble(GetTextBoxValue(row.Cells(6).Controls))
                                        dtView(0)("jualprice") = ToDouble(GetTextBoxValue(row.Cells(7).Controls))
                                        dtView(0)("retdtlnote") = GetTextBoxValue(row.Cells(8).Controls)
                                        dtView(0)("valueidr") = ToDouble(GetTextBoxValue(row.Cells(10).Controls))
                                        dtView(0)("valueusd") = ToDouble(GetTextBoxValue(row.Cells(10).Controls))
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl = dtView.ToTable
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                dtView2.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    If shipmentrawmstoid.Text > 0 Then
                                        dtView(0)("retqty") = ToDouble(GetTextBoxValue(row.Cells(6).Controls))
                                        dtView(0)("retdtlnote") = GetTextBoxValue(row.Cells(7).Controls)
                                    Else
                                        dtView(0)("retqty") = ToDouble(GetTextBoxValue(row.Cells(6).Controls))
                                        dtView(0)("jualprice") = ToDouble(GetTextBoxValue(row.Cells(7).Controls))
                                        dtView(0)("retdtlnote") = GetTextBoxValue(row.Cells(8).Controls)
                                        dtView(0)("valueidr") = ToDouble(GetTextBoxValue(row.Cells(10).Controls))
                                        dtView(0)("valueusd") = ToDouble(GetTextBoxValue(row.Cells(10).Controls))
                                    End If
                                    If dtView2.Count > 0 Then
                                        If shipmentrawmstoid.Text > 0 Then
                                            dtView2(0)("retqty") = ToDouble(GetTextBoxValue(row.Cells(6).Controls))
                                            dtView2(0)("retdtlnote") = GetTextBoxValue(row.Cells(7).Controls)
                                        Else
                                            dtView2(0)("retqty") = ToDouble(GetTextBoxValue(row.Cells(6).Controls))
                                            dtView2(0)("jualprice") = ToDouble(GetTextBoxValue(row.Cells(7).Controls))
                                            dtView2(0)("retdtlnote") = GetTextBoxValue(row.Cells(8).Controls)
                                            dtView2(0)("valueidr") = ToDouble(GetTextBoxValue(row.Cells(10).Controls))
                                            dtView2(0)("valueusd") = ToDouble(GetTextBoxValue(row.Cells(10).Controls))
                                        End If
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If matrawoid.Text = "" Then
            sError &= "- Please select MATERIAL field!<BR>"
        End If
        If sretrawqty.Text = "" Then
            sError &= "- Please fill QTY field!<BR>"
        Else
            If shipmentrawmstoid.Text > 0 Then
                If ToDouble(sretrawqty.Text) <= 0 Then
                    sError &= "- QTY must be more than 0!<BR>"
                Else
                    If ToDouble(sretrawqty.Text) >= 100000 Then
                        sError &= "- QTY must be less than 100,000!<BR>"
                    Else
                        If ToDouble(sretrawqty.Text) > ToDouble(shipmentrawqty.Text) Then
                            sError &= "- QTY must be less than DO QTY!<BR>"
                        End If
                    End If
                End If
            End If
        End If
        If sretrawunitoid.SelectedValue = "" Then
            sError &= "- Please select UNIT field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
            Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If custoid.Text = "" Then
            sError &= "- Please select CUSTOMER field!<BR>"
        End If
        If shipmentrawmstoid.Text = "" AND DDLTypeNota.SelectedValue = "NOTA BARU" Then
            sError &= "- Please select AR NO. field!<BR>"
        End If
        If shipmentrawmstoid.Text < 0 Then
            sSql = "select trnjualamtnetto from QL_trnjualmst where cmpcode='" & CompnyCode & "' AND trnjualmstoid=" & shipmentrawmstoid.Text & ""
            Dim cekNett As Double = ToDouble(GetStrData(sSql))
            If ToDouble(totalReturNetto.Text) > cekNett Then
                sError &= "- Total Amount Retur > Total Amount Init AR!<BR>"
            End If
        End If
        If Tchar(sretRefNo.Text) = "" Then
            sError &= "- Please select Ref NO. field!<BR>"
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill DETAIL DATA!<BR>"
        Else
            Dim objTbl As DataTable = Session("TblDtl")
            If objTbl.Rows.Count <= 0 Then
                sError &= "- Please fill DETAIL DATA!<BR>"
            Else
                If DDLBusUnit.SelectedValue <> "" Then
                    If DDLTypeNota.SelectedValue = "NOTA BARU" Then
                        For C1 As Int16 = 0 To objTbl.Rows.Count - 1
                            sSql = "SELECT (dod.doqty_unitkecil - ISNULL((SELECT SUM(qtyunitkecil) FROM QL_trnsalesreturdtl sretd WHERE sretd.cmpcode=shd.cmpcode AND sretd.trnjualdtloid=shd.trnjualdtloid AND sretd.sretmstoid IN(select sretmstoid from QL_trnsalesreturmst where sretmststatus<>'Rejected') AND sretmstoid<>" & sretrawmstoid.Text & "), 0)) AS qty FROM QL_trnjualdtl shd INNER JOIN QL_trndodtl dod ON shd.cmpcode=dod.cmpcode AND shd.dodtloid=dod.dodtloid WHERE shd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND shd.trnjualdtloid=" & objTbl.Rows(C1)("trnjualdtloid").ToString
                            Dim qtyJual As Double = ToDouble(GetStrData(sSql))
                            If ToDouble(qtyJual) < ToDouble(objTbl.Rows(C1)("qtyunitkecil")) Then
                                sError &= "- AR QTY for Code= '" & objTbl.Rows(C1)("itemcode").ToString & "', Please check that every detail Shipment QTY must be more or equal than RETURN QTY!<BR>"
                            End If
                        Next
                    End If
                    Session("TblDtl") = objTbl
                    gvListDtl.DataSource = Session("TblDtl")
                    gvListDtl.DataBind()
                End If
            End If
        End If
        'If DDLTypeNota.SelectedValue.ToUpper <> "NOTA LAMA" Then
        '    If Session("sisa") > 0 Then
        '        If ToDouble(totalReturNetto.Text) > Session("sisa") Then
        '            sError &= "- Total Amount Retur > Sisa Invoice Sebesar " & ToMaskEdit(ToDouble(Session("sisa")), 4) & "!<BR>"
        '        End If
        '    End If
        'End If
        If sError <> "" Then
            showMessage(sError, 2)
            sretrawmststatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function GetStockValue(ByVal sOid As String, Optional ByVal sType As String = "IDR") As Double
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        Try
            sSql = "SELECT SUM(ISNULL(stockqty, 0) * ISNULL(stockvalue" & sType.ToLower & ", 0)) / SUM(ISNULL(stockqty, 0)) FROM QL_stockvalue WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND periodacctg IN ('" & sPeriod & "', '" & GetLastPeriod(sPeriod) & "') AND refoid=" & sOid & " AND closeflag=''"
            GetStockValue = ToDouble(GetStrData(sSql))
        Catch ex As Exception
            GetStockValue = 0
        End Try
    End Function

#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trnsretmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND sretmststatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnSalesRetur.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql).ToString) > 0 Then
            lbInProcess.Visible = True
            lbInProcess.Text = "You have " & cKon.ambilscalar(sSql).ToString & " In Process Sales Return Raw Material data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
        sSql = "SELECT COUNT(*) FROM QL_trnsretmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND sretmststatus='In Approval'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnSalesRetur.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql).ToString) > 0 Then
            lbInApproval.Visible = True
            lbInApproval.Text = "You have " & cKon.ambilscalar(sSql).ToString & " In Approval Sales Return Raw Material data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitAllDDL()
        'Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)
        'Fill DDL Unit
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='UNIT' AND activeflag='ACTIVE'"
        FillDDL(sretrawunitoid, sSql)
        'Fill DDL Group Type
        sSql = "SELECT gencode, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='GROUPITEM' AND activeflag='ACTIVE'"
        FillDDL(invType, sSql)
    End Sub

    Private Sub DDLreason(Optional ByVal sGroup As String = "")
        'Fill DDL Reason
        sretrawmstres2.Items.Clear()
        If sGroup.ToUpper = "LUNAS" Then
            sretrawmstres2.Items.Add("DP")
            sretrawmstres2.Items.Item(sretrawmstres2.Items.Count - 1).Value = "DP"
            sretrawmstres2.Items.Add("CASH")
            sretrawmstres2.Items.Item(sretrawmstres2.Items.Count - 1).Value = "CASH"
        ElseIf sGroup.ToUpper = "NOTALAMA" Then
            sretrawmstres2.Items.Add("DP")
            sretrawmstres2.Items.Item(sretrawmstres2.Items.Count - 1).Value = "DP"
        Else
            sretrawmstres2.Items.Add("KEMBALI BARANG")
            sretrawmstres2.Items.Item(sretrawmstres2.Items.Count - 1).Value = "ITEM"
        End If
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT sretmstoid, sretno, CONVERT(VARCHAR(10), sretdate, 101) AS sretdate, c.custname, CASE WHEN ISNULL(sretmstres3,'')='NOTA LAMA' THEN sretmstres4 ELSE trnjualno END AS trnjualno, sretmststatus, sretmstnote, divname, (CASE sretmstres2 WHEN 'DP' THEN 'DOWN PAYMENT' WHEN 'ITEM' THEN 'PAYMENT' WHEN 'CASH' THEN 'CASH BACK' ELSE '' END) srettype, (case upper(sretmststatus) when 'Closed' then '' when 'Rejected' then rejectreason when 'Revised' then revisereason else '' end) reason FROM QL_trnsalesreturmst sretm LEFT JOIN QL_trnjualmst shm ON shm.cmpcode=sretm.cmpcode AND shm.trnjualmstoid=sretm.trnjualmstoid INNER JOIN QL_mstcust c ON c.custoid=sretm.custoid INNER JOIN QL_mstdivision div ON div.cmpcode=sretm.cmpcode WHERE"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " sretm.cmpcode='" & Session("CompnyCode") & "'"
        Else
            sSql &= " sretm.cmpcode LIKE '%'"
        End If
        sSql &= sSqlPlus & " ORDER BY CONVERT(DATETIME, sretm.sretdate) DESC, sretm.sretmstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trnsalesreturmst")
        gvList.DataSource = Session("TblMst")
        gvList.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub BindCustomerData()
        sSql = "SELECT DISTINCT custoid, custcode, custname, custaddr FROM QL_mstcust WHERE cmpcode='" & CompnyCode & "' AND " & FilterDDLListCust.SelectedValue & " LIKE '%" & Tchar(FilterTextListCust.Text) & "%' AND custoid IN (SELECT custoid FROM QL_trnjualmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND ISNULL(trnjualres2, '')<>'Closed') ORDER BY custcode, custname"
        FillGV(gvListCust, sSql, "QL_mstcust")
    End Sub

    Private Sub BindShipmentData()
        sSql = "select trnjualmstoid, trnjualno, CONVERT(VARCHAR(10), trnjualdate, 101) AS trnjualdate, trnjualnote, domstoid, trnjualamt, trndiscamt, trntaxamt, trnjualamtnetto, trnjualres1, trntaxtype AS tipe, trndiscvalue FROM QL_trnjualmst jm WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND custoid=" & custoid.Text & " AND " & FilterDDLListShip.SelectedValue & " LIKE '%" & Tchar(FilterTextListShip.Text) & "%' AND ISNULL(trnjualres2, '')<>'Closed' AND trnjualstatus<>'In Process' ORDER BY trnjualmstoid"
        FillGV(gvListShip, sSql, "QL_trnjualmst")
    End Sub

    Private Sub BindMaterialData()
        Dim sAdd As String = "", sAdd2 As String = ""
        If Not Session("oid") Is Nothing And Session("oid") <> "" Then
            sAdd = "AND sretmstoid<>" & Session("oid")
        End If
        If ToInteger(shipmentrawmstoid.Text) <> 0 Then
            sAdd2 = " AND shd.trnjualmstoid=" & shipmentrawmstoid.Text
        End If
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        If DDLTypeNota.SelectedValue = "NOTA BARU" Then
            sSql = "SELECT DISTINCT 'False' AS checkvalue, CONVERT(VARCHAR(10), trnjualdate, 101) AS trnjualdate, trnjualnote, dod.domstoid, trnjualamt, trndiscamt, trntaxamt, trnjualamtnetto, trnjualres1, trntaxtype AS tipe, shd.trnjualmstoid, trnjualno, trnjualdtloid, shd.itemoid, shd.old_code AS oldcode, itemCode AS itemcode, itemLongDescription AS itemdesc, trnjualdtlunitoid AS unitoid, g.gendesc AS unit, (SELECT genoid FROM QL_mstgen WHERE gengroup='WAREHOUSE' AND gencode='GBR') AS whoid, (SELECT gendesc FROM QL_mstgen WHERE gengroup='WAREHOUSE' AND gencode='GBR') AS wh, (trnjualdtlqty - ISNULL((SELECT SUM(sretqty) FROM QL_trnsalesreturdtl sretd WHERE sretd.cmpcode=shd.cmpcode AND sretd.trnjualdtloid=shd.trnjualdtloid AND sretd.sretmstoid IN(select sretmstoid from QL_trnsalesreturmst where sretmststatus<>'Rejected') " & sAdd & "), 0)) AS jualqty, 0.00 AS retqty, '' AS retdtlnote, trnjualdtlseq, trnjualdtlprice AS jualprice, trnjualdtlnetto AS jualnetto, ISNULL(shd.trnjualdtldiscamt, 0) AS disc1, ISNULL(shd.trnjualdtldiscamt2, 0) AS disc2, ISNULL(shd.trnjualdtldiscamt3, 0) AS disc3, (shd.trnjualdtltaxamt/shd.trnjualdtlqty) AS tax, dod.valueidr AS valueidr, dod.valueusd AS valueusd, trndiscvalue FROM QL_trnjualdtl shd INNER JOIN QL_trnjualmst jm ON jm.trnjualmstoid=shd.trnjualmstoid INNER JOIN QL_mstitem m ON m.itemoid=shd.itemoid INNER JOIN ql_trndodtl dod on dod.cmpcode=shd.cmpcode AND dod.dodtloid=shd.dodtloid INNER JOIN QL_trndomst dom ON dom.domstoid=dod.domstoid INNER JOIN QL_mstgen g ON genoid=trnjualdtlunitoid INNER JOIN QL_mstgen g2 ON g2.genoid=dom.dowhoid WHERE shd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND dom.custoid=" & custoid.Text & " " & sAdd2 & " AND ISNULL(trnjualdtlres1, '')<>'Complete' ORDER BY shd.trnjualmstoid, trnjualdtlseq"
        Else
            sSql = "SELECT DISTINCT 'False' AS checkvalue, '1/1/1900' AS trnjualdate, '' trnjualnote, 0 domstoid, 0.0 trnjualamt, 0.0 trndiscamt, 0.0 trntaxamt, 0.0 trnjualamtnetto, '' trnjualres1, '' AS tipe, 0 trnjualmstoid, '' trnjualno, 0 AS trnjualdtloid, itemoldcode AS oldcode, m.itemoid, itemCode AS itemcode, itemLongDescription AS itemdesc, m.itemUnit1 AS unitoid, g.gendesc AS unit, (select genoid from ql_mstgen where gengroup='WAREHOUSE' AND gencode='GBR') AS whoid, (select gendesc from ql_mstgen where gengroup='WAREHOUSE' AND gencode='GBR') AS wh, ISNULL((SELECT SUM(saldoakhir) FROM QL_crdstock crd WHERE crd.cmpcode='LA' AND refoid=itemoid AND closingdate='01/01/1900' AND periodacctg IN ('" & sPeriod & "', '" & GetLastPeriod(sPeriod) & "')), 0.0) AS jualqty, 0.00 AS retqty, '' AS retdtlnote, 1 trnjualdtlseq, 0.00 AS jualprice, 0.00 AS jualnetto, 0.00 AS disc1, 0.00 AS disc2, 0.00 AS disc3, 0.00 AS tax, 0.00 AS valueidr, 0.00 AS valueusd, 0.0 trndiscvalue FROM QL_mstitem m INNER JOIN QL_mstgen g ON g.genoid=m.itemUnit1"
        End If
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnjualdtl")

        If dtTbl.Rows.Count > 0 Then
            If DDLTypeNota.SelectedValue = "NOTA BARU" Then
                gvListMat.Columns(gvListMat.Columns.Count - 1).Visible = True
                gvListMat.Columns(gvListMat.Columns.Count - 2).Visible = True
                gvListMat.Columns(gvListMat.Columns.Count - 3).Visible = False
                gvListMat.Columns(gvListMat.Columns.Count - 6).Visible = False
                gvListMat.Columns(gvListMat.Columns.Count - 9).HeaderText = "SI Qty"
            Else
                gvListMat.Columns(gvListMat.Columns.Count - 1).Visible = False
                gvListMat.Columns(gvListMat.Columns.Count - 2).Visible = False
                gvListMat.Columns(gvListMat.Columns.Count - 3).Visible = True
                gvListMat.Columns(gvListMat.Columns.Count - 6).Visible = True
                gvListMat.Columns(gvListMat.Columns.Count - 9).HeaderText = "Stock Qty"
            End If
            Session("TblMat") = dtTbl
            Session("TblMatView") = dtTbl
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            showMessage("Material data can't be found!", 2)
        End If
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("QL_trnsalesreturdtl")
        dtlTable.Columns.Add("sretdtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("sretwhoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("sretwh", Type.GetType("System.String"))
        dtlTable.Columns.Add("trnjualdtloid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("oldcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("jualqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("sretqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("sretunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("sretunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("sretdtlnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("disc1", Type.GetType("System.Double"))
        dtlTable.Columns.Add("disc2", Type.GetType("System.Double"))
        dtlTable.Columns.Add("disc3", Type.GetType("System.Double"))
        dtlTable.Columns.Add("taxamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("sretamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("sretamtnetto", Type.GetType("System.Double"))
        dtlTable.Columns.Add("sretvalueidr", Type.GetType("System.Double"))
        dtlTable.Columns.Add("qtyunitkecil", Type.GetType("System.Double"))
        dtlTable.Columns.Add("qtyunitbesar", Type.GetType("System.Double"))
        dtlTable.Columns.Add("valueidr", Type.GetType("System.Double"))
        dtlTable.Columns.Add("valueusd", Type.GetType("System.Double"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub ClearDetail()
        sretrawdtlseq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                EnableHeader(False)
                sretrawdtlseq.Text = objTable.Rows.Count + 1
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        i_u2.Text = "New Detail"
        sretrawwhoid.Text = ""
        shipmentrawdtloid.Text = ""
        matrawoid.Text = ""
        matrawcode.Text = ""
        matrawlongdesc.Text = ""
        'matrawlimitqty.Text = ""
        shipmentrawqty.Text = ""
        price.Text = ""
        disc1.Text = ""
        disc2.Text = ""
        disc3.Text = ""
        dtlAmt.Text = ""
        promoDisc.Text = ""
        sretrawqty.Text = ""
        sretrawunitoid.SelectedIndex = -1
        sretrawdtlnote.Text = ""
        gvListDtl.SelectedIndex = -1
        btnSearchMat.Visible = True
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal : DDLBusUnit.CssClass = sCss
        btnSearchCust.Visible = bVal : btnClearCust.Visible = bVal
        btnSearchShip.Visible = bVal : btnClearShip.Visible = bVal
        DDLTypeNota.Enabled = bVal : DDLTypeNota.CssClass = sCss
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT sretm.cmpcode, sretmstoid, sretm.periodacctg, sretm.sretdate, sretm.sretno, sretm.custoid, c.custname, sretm.curroid, CASE WHEN ISNULL(sretmstres3,'')='NOTA LAMA' THEN 0 ELSE shm.trnjualmstoid END AS trnjualmstoid, CASE WHEN ISNULL(sretmstres3,'')='NOTA LAMA' THEN sretmstres4 ELSE trnjualno END AS trnjualno, CASE WHEN ISNULL(sretmstres3,'')='NOTA LAMA' THEN sretm.sretdate ELSE trnjualdate END AS trnjualdate, sretmststatus, sretmstnote, sretm.createuser, sretm.createtime, sretm.upduser, sretm.updtime, sretm.reasonoid AS reasonoid2, cr.currcode,sretm.sretamt,sretm.srettaxamt,sretm.sretamtnetto,sretm.sretmstrefno, sretdiscamt AS disc, sretmstres2 AS reasonoid, sretmstres3 AS typenota, shm.trntaxtype, trndiscvalue FROM QL_trnsalesreturmst sretm LEFT JOIN QL_trnjualmst shm ON shm.cmpcode=sretm.cmpcode AND shm.trnjualmstoid=sretm.trnjualmstoid INNER JOIN QL_mstcust c ON c.custoid=sretm.custoid inner join QL_mstcurr cr ON cr.curroid=sretm.curroid WHERE sretm.sretmstoid=" & sOid
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = xreader("cmpcode").ToString
                sretrawmstoid.Text = xreader("sretmstoid").ToString
                periodacctg.Text = xreader("periodacctg").ToString
                sretrawdate.Text = Format(xreader("sretdate"), "MM/dd/yyyy")
                invDate.Text = Format(xreader("trnjualdate"), "MM/dd/yyyy")
                sretrawno.Text = xreader("sretno").ToString
                custoid.Text = xreader("custoid").ToString
                DDLTypeNota.SelectedValue = xreader("typenota").ToString
                custname.Text = xreader("custname").ToString
                curroid.Text = xreader("curroid").ToString
                curr.Text = xreader("currcode").ToString
                shipmentrawmstoid.Text = xreader("trnjualmstoid").ToString
                shipmentrawno.Text = xreader("trnjualno").ToString
                sretrawmstnote.Text = xreader("sretmstnote").ToString
                sretrawmststatus.Text = xreader("sretmststatus").ToString
                sretRefNo.Text = xreader("sretmstrefno").ToString
                discHdr.Text = ToMaskEdit(ToDouble(xreader("disc").ToString), 2)
                taxAmt.Text = ToMaskEdit(ToDouble(xreader("srettaxamt").ToString), 2)
                totalRetur.Text = ToMaskEdit(ToDouble(xreader("sretamt").ToString), 2)
                totalReturNetto.Text = ToMaskEdit(ToDouble(xreader("sretamtnetto").ToString), 2)
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
                'Cek Reason Pembayaran
                sretrawmstres2.Items.Clear()
                If xreader("reasonoid").ToString = "ITEM" Then
                    lunas.Text = "Blm Lunas"
                Else
                    lunas.Text = "Lunas"
                End If
                lunas.Visible = True
                sretrawmstres2.Visible = True
                ttkReason.Visible = True
                lblReason.Visible = True
                sretrawmstres2.Items.Add(xreader("reasonoid").ToString)
                sretrawmstres2.Items.Item(sretrawmstres2.Items.Count - 1).Value = xreader("reasonoid").ToString
                tipe.Text = xreader("trntaxtype").ToString
                sretmstdisctype.SelectedValue = "P"
                sretmstdiscvalue.Text = ToMaskEdit(ToDouble(xreader("trndiscvalue").ToString), 2)
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False : btnSendApproval.Visible = False
            Exit Sub
        End Try
        If sretrawmststatus.Text <> "In Process" And sretrawmststatus.Text <> "Revised" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnSendApproval.Visible = False
            btnAddToList.Visible = False
            gvListDtl.Columns(0).Visible = False
            gvListDtl.Columns(gvListDtl.Columns.Count - 1).Visible = False
            btnShowCOA.Visible = True
            If sretrawmststatus.Text <> "Rejected" Then
                lblTrnNo.Text = "Return No."
                sretrawmstoid.Visible = False
                sretrawno.Visible = True
                btnShowCOA.Visible = True
            End If
        End If
        sSql = "SELECT sretdtlseq, sretwhoid, sretd.trnjualdtloid, sretd.matoid, itemcode, sretd.old_code AS oldcode, itemLongDescription AS itemdesc, CASE WHEN " & shipmentrawmstoid.Text & " > 0 THEN (trnjualdtlqty - ISNULL((SELECT SUM(x.sretqty) FROM QL_trnsalesreturdtl x WHERE x.cmpcode=sretd.cmpcode AND x.trnjualdtloid=sretd.trnjualdtloid AND x.sretmstoid<>sretd.sretmstoid), 0)) ELSE 0 END AS jualqty, sretqty, sretunitoid, gendesc AS sretunit, sretdtlnote, sretvalueidr,sretdtlamt AS sretamt, (sretdtldisc1/sretqty) AS disc1, (sretdtldisc2/sretqty) AS disc2,(sretdtldisc3/sretqty) AS disc3, qtyunitkecil AS qtyunitkecil, qtyunitbesar AS qtyunitbesar, valueidr, valueusd, (sretd.sretdtltaxamt/sretd.sretqty) AS taxamt, sretd.sretdtlnetto sretamtnetto FROM QL_trnsalesreturdtl sretd LEFT JOIN QL_trnjualdtl shd ON shd.cmpcode=sretd.cmpcode AND shd.trnjualdtloid=sretd.trnjualdtloid LEFT JOIN QL_trnjualmst shm ON shm.cmpcode=sretd.cmpcode AND shm.trnjualmstoid=shd.trnjualmstoid INNER JOIN QL_mstitem m ON m.itemoid=sretd.matoid INNER JOIN QL_mstgen g ON genoid=sretunitoid WHERE sretmstoid=" & sOid & " ORDER BY sretdtlseq"
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnsretrawdtl")
        Session("TblDtl") = dtTbl
        gvListDtl.DataSource = dtTbl
        gvListDtl.DataBind()
        CountHdrAmount()
        ClearDetail()
        CekPembayaran(shipmentrawmstoid.Text)
    End Sub

    Private Sub PrintReport(ByVal sOid As String)
        Try
            report.Load(Server.MapPath(folderReport & "rptSRet.rpt"))
            Dim sWhere As String
            If Session("CompnyCode") <> CompnyCode Then
                sWhere = " WHERE sretm.cmpcode='" & Session("CompnyCode") & "'"
            Else
                sWhere = " WHERE sretm.cmpcode LIKE '%'"
            End If
            If sOid = "" Then
                sWhere &= " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND sretdate>='" & FilterPeriod1.Text & " 00:00:00' AND sretdate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    If FilterDDLStatus.SelectedValue <> "All" Then
                        sWhere &= " AND sretmststatus='" & FilterDDLStatus.SelectedValue & "'"
                    End If
                End If
                If checkPagePermission("~\Transaction\trnSalesRetur.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND sretm.createuser='" & Session("UserID") & "'"
                End If
            Else
                sWhere &= " AND sretm.sretmstoid=" & sOid
            End If
            report.SetParameterValue("userID", Session("UserID"))
            report.SetParameterValue("userName", GetStrData("SELECT profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)
            report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "SalesReturnPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            report.Close()
            report.Dispose()
        End Try
        Response.Redirect("~\Transaction\trnSalesRetur.aspx?awal=true")
    End Sub

    Private Sub CountHdrAmount()
        CountDtlAmount()
    End Sub

    Private Sub CountDtlAmount()
        Dim dAmt As Double = 0
        Dim dDisc As Double = 0
        Dim dTax As Double = 0
        Dim dQtySI As Double = 0
        Dim dQtySR As Double = 0
        Dim dDisc1 As Double = 0
        Dim dDisc2 As Double = 0
        Dim dDisc3 As Double = 0
        If Not Session("TblDtl") Is Nothing Then
            Dim objTable As DataTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                For C1 As Int16 = 0 To objTable.Rows.Count - 1
                    dAmt += ToDouble(objTable.Rows(C1).Item("sretamt"))
                    dDisc1 += ToDouble(objTable.Rows(C1).Item("disc1")) * ToDouble(objTable.Rows(C1).Item("sretqty"))
                    dDisc2 += ToDouble(objTable.Rows(C1).Item("disc2")) * ToDouble(objTable.Rows(C1).Item("sretqty"))
                    dDisc3 += ToDouble(objTable.Rows(C1).Item("disc3")) * ToDouble(objTable.Rows(C1).Item("sretqty"))
                    dTax += ToDouble(objTable.Rows(C1).Item("taxamt")) * ToDouble(objTable.Rows(C1).Item("sretqty"))
                    dQtySI += ToDouble(objTable.Rows(C1).Item("jualqty"))
                    dQtySR += ToDouble(objTable.Rows(C1).Item("sretqty"))
                Next
            End If
            dDisc = dDisc1 + dDisc2 + dDisc3
        End If
        Dim dAmtNetto As Double = 0, dAmtDPP As Double = 0
        If tipe.Text = "EXC" Then
            discHdr.Text = ToMaskEdit(dDisc, 2)
            taxAmt.Text = ToMaskEdit(dTax, 2)
            totalRetur.Text = ToMaskEdit(dAmt - dDisc, 2)
            dAmtDPP = ToMaskEdit(Math.Round(ToDouble(totalRetur.Text) + dTax, 0, MidpointRounding.AwayFromZero), 2)
        ElseIf tipe.Text = "INC" Then
            discHdr.Text = ToMaskEdit(dDisc, 2)
            taxAmt.Text = ToMaskEdit(dTax, 2)
            totalRetur.Text = ToMaskEdit(dAmt - dDisc, 2)
            dAmtDPP = ToMaskEdit(Math.Round(ToDouble(totalRetur.Text) + dTax, 0, MidpointRounding.AwayFromZero), 2)
        Else
            discHdr.Text = ToMaskEdit(dDisc, 2)
            taxAmt.Text = 0
            totalRetur.Text = ToMaskEdit(dAmt - dDisc, 2)
            dAmtDPP = ToMaskEdit(Math.Round(ToDouble(totalRetur.Text) + taxAmt.Text, 0, MidpointRounding.AwayFromZero), 2)
        End If
        If ToDouble(sretmstdiscvalue.Text) <> 0 Then
            sretmstdiscamt.Text = ToMaskEdit(dAmtDPP * ToDouble(sretmstdiscvalue.Text) / 100, 2)
        End If
        totalReturNetto.Text = ToMaskEdit(Math.Round(dAmtDPP - ToDouble(sretmstdiscamt.Text), 0, MidpointRounding.AwayFromZero), 2)
        If discHdr.Text = "NaN" Then
            discHdr.Text = 0
        End If
        If taxAmt.Text = "NaN" Then
            taxAmt.Text = 0
        End If
        If totalReturNetto.Text = "NaN" Then
            totalReturNetto.Text = 0
        End If
    End Sub

    Private Sub SetUsageValue(ByRef objVal As VarUsageValue)
        objVal.Val_IDR = 0
        objVal.Val_USD = 0
        Dim dt As DataTable = Session("TblDtl")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            Dim dValIDR As Double = GetStockValue(dt.Rows(C1)("matoid").ToString)
            Dim dValUSD As Double = GetStockValue(dt.Rows(C1)("matoid").ToString, "USD")
            dt.Rows(C1)("valueidr") = dValIDR
            dt.Rows(C1)("valueusd") = dValUSD
            objVal.Val_IDR += dValIDR
            objVal.Val_USD += dValUSD
        Next
        Session("TblDtl") = dt
    End Sub

    Private Sub CekPembayaran(ByVal jualoid As Integer)
        sSql = "select (select ISNULL(SUM(con.amttrans),0) from QL_conar con  where con.reftype='QL_trnjualmst' and con.refoid=jm.trnjualmstoid and con.custoid=jm.custoid and payrefoid=0) - (select ISNULL(SUM(con.amtbayar),0) from QL_conar con LEFT JOIN QL_trnpayar pay ON pay.payaroid=con.payrefoid AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' where con.reftype='QL_trnjualmst' and con.refoid=jm.trnjualmstoid and con.custoid=jm.custoid) from QL_trnjualmst jm WHERE jm.cmpcode='" & DDLBusUnit.SelectedValue & "' AND jm.trnjualmstoid=" & jualoid & ""
        Dim hasilAmt As Double = ToDouble(GetStrData(sSql))
        Session("sisa") = hasilAmt
        If hasilAmt <= 0 Then
            lunas.Text = "Lunas"
            lunas.Visible = True
            sretrawmstres2.Visible = True
            ttkReason.Visible = True
            lblReason.Visible = True
            DDLreason("LUNAS")
        Else
            lunas.Text = "Blm Lunas"
            lunas.Visible = True
            sretrawmstres2.Visible = True
            ttkReason.Visible = True
            lblReason.Visible = True
            DDLreason()
        End If
    End Sub

#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnSalesRetur.aspx")
        End If
        If checkPagePermission("~\Transaction\trnSalesRetur.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Sales Return"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnSendApproval.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to send this data for APPROVAL?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            CheckStatus()
            InitAllDDL()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            DDLTypeNota_SelectedIndexChanged(Nothing, Nothing)
            'Check Rate Pajak & Standart
            Dim cRate As New ClassRate()
            cRate.SetRateValue(1, Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, 2) : Exit Sub
            End If
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                DDLTypeNota.Enabled = False
                DDLTypeNota.CssClass = "inpTextDisabled"
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                sretrawmstoid.Text = GenerateID("QL_TRNSALESRETURMST", CompnyCode)
                sretrawmststatus.Text = "In Process"
                sretrawdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime)
                TabContainer1.ActiveTabIndex = 0
                btnDelete.Visible = False
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvListDtl.DataSource = dt
            gvListDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnSalesRetur.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lbInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, sretm.updtime, GETDATE()) > " & nDays & " AND sretrawmststatus='In Process' "
        If checkPagePermission("~\Transaction\trnSalesRetur.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND sretm.createuser='" & Session("UserID") & "' "
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub lbInApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbInApproval.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 1
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, sretm.updtime, GETDATE()) > " & nDays & " AND sretrawmststatus='In Approval' "
        If checkPagePermission("~\Transaction\trnSalesRetur.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND sretm.createuser='" & Session("UserID") & "' "
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND sretdate>='" & FilterPeriod1.Text & " 00:00:00' AND sretdate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            If FilterDDLStatus.SelectedValue <> "All" Then
                sSqlPlus &= " AND sretmststatus='" & FilterDDLStatus.SelectedValue & "'"
            End If
        End If
        If cbType.Checked Then
            sSqlPlus &= " AND sretmstres2='" & DDLFilterType.SelectedValue & "'"
        End If
        If checkPagePermission("~\Transaction\trnSalesRetur.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND sretm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        cbType.Checked = False
        DDLFilterType.SelectedIndex = -1
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Transaction\trnSalesRetur.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND sretm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub gvList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvList.PageIndexChanging
        gvList.PageIndex = e.NewPageIndex
        gvList.DataSource = Session("TblMst")
        gvList.DataBind()
    End Sub

    Protected Sub gvList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvList.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvList.DataSource = Session("TblMst")
            gvList.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        btnClearCust_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchCust.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        FilterDDLListCust.SelectedIndex = -1 : FilterTextListCust.Text = "" : gvListCust.SelectedIndex = -1
        BindCustomerData()
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
    End Sub

    Protected Sub btnClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearCust.Click
        custoid.Text = ""
        custname.Text = ""
        btnClearShip_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnFindListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListCust.Click
        BindCustomerData()
        mpeListCust.Show()
    End Sub

    Protected Sub btnAllListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListCust.Click
        FilterDDLListCust.SelectedIndex = -1 : FilterTextListCust.Text = "" : gvListCust.SelectedIndex = -1
        BindCustomerData()
        mpeListCust.Show()
    End Sub

    Protected Sub gvListCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListCust.PageIndexChanging
        gvListCust.PageIndex = e.NewPageIndex
        BindCustomerData()
        mpeListCust.Show()
    End Sub

    Protected Sub gvListCust_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListCust.SelectedIndexChanged
        If custoid.Text <> gvListCust.SelectedDataKey.Item("custoid").ToString Then
            btnClearCust_Click(Nothing, Nothing)
            DDLTypeNota_SelectedIndexChanged(Nothing, Nothing)
        End If
        custoid.Text = gvListCust.SelectedDataKey.Item("custoid").ToString
        custname.Text = gvListCust.SelectedDataKey.Item("custname").ToString
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub

    Protected Sub lkbCloseListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListCust.Click
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub

    Protected Sub btnSearchShip_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchShip.Click
        If custoid.Text = "" Then
            showMessage("Please select Customer first!", 2)
            Exit Sub
        End If
        FilterDDLListShip.SelectedIndex = -1 : FilterTextListShip.Text = "" : gvListShip.SelectedIndex = -1
        BindShipmentData()
        cProc.SetModalPopUpExtender(btnHideListShip, pnlListShip, mpeListShip, True)
    End Sub

    Protected Sub btnClearShip_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearShip.Click
        shipmentrawmstoid.Text = 0
        shipmentrawno.Text = ""
        invDate.Text = ""
        totalRetur.Text = ""
        taxAmt.Text = ""
        totalReturNetto.Text = ""
        curroid.Text = ""
        curr.Text = ""
        discHdr.Text = ""
        tipe.Text = ""
        lblDisc.Text = ""
        sretmstdisctype.SelectedIndex = -1
        sretmstdiscvalue.Text = ""
        sretmstdiscamt.Text = ""
        lunas.Visible = False
        sretrawmstres2.Visible = False
        ttkReason.Visible = False
        lblReason.Visible = False
    End Sub

    Protected Sub btnFindListShip_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListShip.Click
        BindShipmentData()
        mpeListShip.Show()
    End Sub

    Protected Sub btnAllListShip_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListShip.Click
        FilterDDLListShip.SelectedIndex = -1 : FilterTextListShip.Text = "" : gvListShip.SelectedIndex = -1
        BindShipmentData()
        mpeListShip.Show()
    End Sub

    Protected Sub gvListShip_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListShip.PageIndexChanging
        gvListShip.PageIndex = e.NewPageIndex
        BindShipmentData()
        mpeListShip.Show()
    End Sub

    Protected Sub gvListShip_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListShip.SelectedIndexChanged
        If shipmentrawmstoid.Text <> gvListShip.SelectedDataKey.Item("trnjualmstoid").ToString Then
            btnClearShip_Click(Nothing, Nothing)
        End If
        shipmentrawmstoid.Text = gvListShip.SelectedDataKey.Item("trnjualmstoid").ToString
        CekPembayaran(shipmentrawmstoid.Text)
        shipmentrawno.Text = gvListShip.SelectedDataKey.Item("trnjualno").ToString
        invDate.Text = gvListShip.SelectedDataKey.Item("trnjualdate").ToString
        taxAmt.Text = ToMaskEdit(ToDouble(gvListShip.SelectedDataKey.Item("trntaxamt").ToString), 2)
        sretmstdisctype.SelectedValue = "P"
        sretmstdiscvalue.Text = ToMaskEdit(ToDouble(gvListShip.SelectedDataKey.Item("trndiscvalue").ToString), 2)
        If shipmentrawmstoid.Text > 0 Then
            totalRetur.Text = ToMaskEdit(ToDouble(gvListShip.SelectedDataKey.Item("trnjualamt").ToString), 2)
            totalReturNetto.Text = ToMaskEdit(ToDouble(gvListShip.SelectedDataKey.Item("trnjualamtnetto").ToString), 2)
        Else
            totalRetur.Text = ToMaskEdit(Session("sisa"), 2)
            totalReturNetto.Text = ToMaskEdit(Session("sisa"), 2)
        End If
        discHdr.Text = ToMaskEdit(ToDouble(gvListShip.SelectedDataKey.Item("trndiscamt").ToString), 2)
        lblDisc.Text = ToMaskEdit(ToDouble(gvListShip.SelectedDataKey.Item("trndiscamt").ToString), 2)
        tipe.Text = gvListShip.SelectedDataKey.Item("tipe").ToString

        If shipmentrawmstoid.Text > 0 Then
            sSql = "SELECT som.curroid FROM QL_trndomst dom INNER JOIN QL_trnsomst som ON som.cmpcode=dom.cmpcode AND som.somstoid=dom.somstoid WHERE dom.domstoid=" & gvListShip.SelectedDataKey.Item("domstoid").ToString & ""
            curroid.Text = cKon.ambilscalar(sSql)
            sSql = "SELECT currcode FROM QL_mstcurr WHERE curroid=" & curroid.Text & ""
            curr.Text = GetStrData(sSql)
        Else
            sSql = "SELECT dom.curroid FROM QL_trnjualmst dom WHERE dom.trnjualmstoid=" & gvListShip.SelectedDataKey.Item("trnjualmstoid").ToString & ""
            curroid.Text = cKon.ambilscalar(sSql)
            sSql = "SELECT currcode FROM QL_mstcurr WHERE curroid=" & curroid.Text & ""
            curr.Text = GetStrData(sSql)
        End If
        cProc.SetModalPopUpExtender(btnHideListShip, pnlListShip, mpeListShip, False)
    End Sub

    Protected Sub lkbCloseListShip_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListShip.Click
        cProc.SetModalPopUpExtender(btnHideListShip, pnlListShip, mpeListShip, False)
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        If custoid.Text = "" Then
            showMessage("Please select Customer first!", 2)
            Exit Sub
        End If
        If Tchar(shipmentrawno.Text.Trim) = "" Then
            'showMessage("Please select Shipment No. first!", 2)
            'Exit Sub
        End If
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : Session("TblMat") = Nothing : Session("TblMatView") = Nothing : gvListMat.DataSource = Nothing : gvListMat.DataBind()
        BindMaterialData()
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If UpdateCheckedMat() Then
            Session("TblMatView") = Session("TblMat")
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "trnjualdtloid=" & dtTbl.Rows(C1)("trnjualdtloid")
                    objView(0)("CheckValue") = "True"
                    dtTbl.Rows(C1)("CheckValue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                dtTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "No material data can't be selected!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "No material data can't be selected!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "trnjualdtloid=" & dtTbl.Rows(C1)("trnjualdtloid")
                    objView(0)("CheckValue") = "False"
                    objView(0)("retqty") = 0
                    objView(0)("retdtlnote") = ""
                    dtTbl.Rows(C1)("CheckValue") = "False"
                    dtTbl.Rows(C1)("retqty") = 0
                    dtTbl.Rows(C1)("retdtlnote") = ""
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                dtTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "No material data can't be selected!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "No material data can't be selected!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "CheckValue='True'"
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListMat") = "No material data have been selected before!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 2)
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(6).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 2)
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub lbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListListMat.Click
        If ToInteger(shipmentrawmstoid.Text) = 0 AND DDLTypeNota.SelectedValue = "NOTA BARU" Then
            Session("WarningListMat") = "Please select NO. INVOICE field!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "CheckValue='True'"
            Dim iCheck As Integer = dtView.Count
            If iCheck > 0 Then
                dtView.RowFilter = ""
                dtView.RowFilter = "CheckValue='True' AND retqty>0"
                If dtView.Count <> iCheck Then
                    Session("WarningListMat") = "Qty for every checked material data must be more than 0!"
                    showMessage(Session("WarningListMat"), 2)
                    dtView.RowFilter = ""
                    Exit Sub
                End If
                dtView.RowFilter = ""
                dtView.RowFilter = "CheckValue='True' AND jualprice>0"
                If dtView.Count <> iCheck Then
                    Session("WarningListMat") = "Price for every checked material data must be more than 0!"
                    showMessage(Session("WarningListMat"), 2)
                    dtView.RowFilter = ""
                    Exit Sub
                End If
                If shipmentrawmstoid.Text > 0 Then
                    dtView.RowFilter = ""
                    dtView.RowFilter = "CheckValue='True' AND retqty<=jualqty"
                    If dtView.Count <> iCheck Then
                        Session("WarningListMat") = "Qty for every checked material data must be less than DO Qty!"
                        showMessage(Session("WarningListMat"), 2)
                        dtView.RowFilter = ""
                        Exit Sub
                    End If
                Else
                    dtView.RowFilter = ""
                    dtView.RowFilter = "CheckValue='True' AND valueidr>0"
                    If dtView.Count <> iCheck Then
                        Session("WarningListMat") = "HPP for every checked material data must be more than 0!"
                        showMessage(Session("WarningListMat"), 2)
                        dtView.RowFilter = ""
                        Exit Sub
                    End If
                End If

                If Session("TblDtl") Is Nothing Then
                    CreateTblDetail()
                End If
                Dim objTable As DataTable
                objTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                Dim objRow As DataRow
                Dim counter As Integer = objTable.Rows.Count
                Dim dQty_unitkecil, dQty_unitbesar As Double
                For C1 As Integer = 0 To dtView.Count - 1
                    dv.RowFilter = "matoid=" & dtView(C1)("itemoid")
                    If dv.Count > 0 Then
                        dv.AllowEdit = True
                        dv(0)("sretqty") = ToDouble(dtView(C1)("retqty"))
                        dv(0)("taxamt") = ToDouble(dtView(C1)("tax"))
                        dv(0)("sretdtlnote") = dtView(C1)("retdtlnote")
                        GetUnitConverter(dtView(C1)("itemoid"), dtView(C1)("unitoid"), ToDouble(dtView(C1)("retqty")), dQty_unitkecil, dQty_unitbesar)
                        dv(0)("qtyunitkecil") = dQty_unitkecil
                        dv(0)("qtyunitbesar") = dQty_unitbesar
                    Else
                        counter += 1
                        objRow = objTable.NewRow()
                        objRow("sretdtlseq") = counter
                        objRow("sretwhoid") = dtView(C1)("whoid")
                        objRow("trnjualdtloid") = dtView(C1)("trnjualdtloid")
                        objRow("matoid") = dtView(C1)("itemoid")
                        objRow("itemcode") = dtView(C1)("itemcode").ToString
                        objRow("oldcode") = dtView(C1)("oldcode").ToString
                        objRow("itemdesc") = dtView(C1)("itemdesc").ToString
                        If shipmentrawmstoid.Text > 0 Then
                            objRow("jualqty") = ToDouble(dtView(C1)("jualqty"))
                        Else
                            objRow("jualqty") = 0
                        End If
                        objRow("sretqty") = ToDouble(dtView(C1)("retqty"))
                        objRow("sretunitoid") = dtView(C1)("unitoid")
                        objRow("sretunit") = dtView(C1)("unit").ToString
                        objRow("sretdtlnote") = dtView(C1)("retdtlnote")
                        objRow("sretvalueidr") = ToDouble(dtView(C1)("jualprice"))
                        If ToDouble(dtView(C1)("disc1")) > 0 Then
                            objRow("disc1") = ToDouble(dtView(C1)("disc1")) / ToDouble(dtView(C1)("jualqty"))
                        Else
                            objRow("disc1") = 0
                        End If
                        If ToDouble(dtView(C1)("disc2")) > 0 Then
                            objRow("disc2") = ToDouble(dtView(C1)("disc2")) / ToDouble(dtView(C1)("jualqty"))
                        Else
                            objRow("disc2") = 0
                        End If
                        If ToDouble(dtView(C1)("disc3")) > 0 Then
                            objRow("disc3") = ToDouble(dtView(C1)("disc3")) / ToDouble(dtView(C1)("jualqty"))
                        Else
                            objRow("disc3") = 0
                        End If
                        objRow("taxamt") = ToDouble(dtView(C1)("tax"))
                        Dim dDisc1 As Double = ToDouble(objRow("disc1")) * ToDouble(dtView(C1)("retqty"))
                        Dim dDisc2 As Double = ToDouble(objRow("disc2")) * ToDouble(dtView(C1)("retqty"))
                        Dim dDisc3 As Double = ToDouble(objRow("disc3")) * ToDouble(dtView(C1)("retqty"))
                        Dim discDtl As Double = dDisc1 + dDisc2 + dDisc3
                        objRow("sretamt") = ToDouble(dtView(C1)("jualprice")) * ToDouble(dtView(C1)("retqty"))
                        objRow("sretamtnetto") = objRow("sretamt") - discDtl
                        GetUnitConverter(dtView(C1)("itemoid"), dtView(C1)("unitoid"), ToDouble(dtView(C1)("retqty")), dQty_unitkecil, dQty_unitbesar)
                        objRow("qtyunitkecil") = dQty_unitkecil
                        objRow("qtyunitbesar") = dQty_unitbesar
                        objRow("valueidr") = ToDouble(dtView(C1)("valueidr"))
                        objRow("valueusd") = ToDouble(dtView(C1)("valueusd"))
                        objTable.Rows.Add(objRow)
                    End If
                    dv.RowFilter = ""
                Next
                dtView.RowFilter = ""
                objTable.AcceptChanges()
                Session("TblDtl") = objTable
                gvListDtl.DataSource = objTable
                gvListDtl.DataBind()
                CountHdrAmount()
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                ClearDetail()
            Else
                Session("WarningListMat") = "Please select material to add to list!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        Else
            Session("WarningListMat") = "Please select material to add to list!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            Dim objTable As DataTable = Session("TblDtl")
            Dim objRow As DataRow = objTable.Rows(sretrawdtlseq.Text - 1)
            Dim dQty_unitkecil, dQty_unitbesar As Double
            objRow.BeginEdit()
            objRow("matoid") = matrawoid.Text
            objRow("itemcode") = matrawcode.Text
            objRow("oldcode") = oldcode.Text
            objRow("itemdesc") = matrawlongdesc.Text
            objRow("jualqty") = ToDouble(shipmentrawqty.Text)
            objRow("sretqty") = ToDouble(sretrawqty.Text)
            objRow("sretunitoid") = sretrawunitoid.SelectedValue
            objRow("sretunit") = sretrawunitoid.SelectedItem.Text
            objRow("sretdtlnote") = sretrawdtlnote.Text
            objRow("sretvalueidr") = ToDouble(price.Text)
            objRow("disc1") = ToDouble(disc1.Text)
            objRow("disc2") = ToDouble(disc2.Text)
            objRow("disc3") = ToDouble(disc3.Text)
            objRow("sretamt") = ToDouble(dtlAmt.Text)
            Dim discDtl As Double = (objRow("disc1") * objRow("sretqty")) + (objRow("disc2") * objRow("sretqty")) + (objRow("disc3") * objRow("sretqty") * objRow("sretqty"))
            objRow("sretamtnetto") = (ToDouble(objRow("sretvalueidr")) * ToDouble(objRow("sretqty"))) - discDtl
            GetUnitConverter(matrawoid.Text, sretrawunitoid.SelectedValue, ToDouble(sretrawqty.Text), dQty_unitkecil, dQty_unitbesar)
            objRow("qtyunitkecil") = dQty_unitkecil
            objRow("qtyunitbesar") = dQty_unitbesar
            objRow.EndEdit()
            objTable.AcceptChanges()
            Session("TblDtl") = objTable
            gvListDtl.DataSource = objTable
            gvListDtl.DataBind()
            CountHdrAmount()
            ClearDetail()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvListDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 2)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 2)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 2)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 2)
        End If
    End Sub

    Protected Sub gvListDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvListDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        'Dim objRows As DataRow = objRow(e.RowIndex)
        'objRows.BeginEdit()
        'totalRetur.Text = ToMaskEdit(ToDouble(totalRetur.Text) + (ToDouble(objRows("sretamt"))), 2)
        'totalReturNetto.Text = ToMaskEdit(ToDouble(totalReturNetto.Text) + (ToDouble(objRows("sretamt"))), 2)
        'objRows.EndEdit()
        'objTable.AcceptChanges()
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("sretdtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvListDtl.DataSource = objTable
        gvListDtl.DataBind()
        CountHdrAmount()
        ClearDetail()
        'btnClearShip_Click(Nothing, Nothing)
    End Sub

    Protected Sub gvListDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListDtl.SelectedIndexChanged
        Try
            sretrawdtlseq.Text = gvListDtl.SelectedDataKey.Item("sretdtlseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "sretdtlseq=" & sretrawdtlseq.Text
                matrawoid.Text = dv.Item(0).Item("matoid").ToString
                matrawcode.Text = dv.Item(0).Item("itemcode").ToString
                oldcode.Text = dv.Item(0).Item("oldcode").ToString
                matrawlongdesc.Text = dv.Item(0).Item("itemdesc").ToString
                promoDisc.Text = 0
                shipmentrawqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("jualqty").ToString), 2)
                sretrawqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("sretqty").ToString), 2)
                sretrawunitoid.SelectedValue = dv.Item(0).Item("sretunitoid").ToString
                sretrawdtlnote.Text = dv.Item(0).Item("sretdtlnote").ToString
                price.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("sretvalueidr").ToString), 2)
                disc1.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("disc1").ToString), 2)
                disc2.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("disc2").ToString), 2)
                disc3.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("disc3").ToString), 2)
                dtlAmt.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("sretamt").ToString), 2)
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        Finally
            ' btnSearchDO.Visible = False : btnClearDO.Visible = False
            btnSearchMat.Visible = False
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            curroid.Text = 1
            If Session("oid") = Nothing Or Session("oid") = "" Then
                If CheckDataExists("SELECT COUNT(*) FROM QL_trnsalesreturmst WHERE sretmstoid=" & sretrawmstoid.Text) Then
                    sretrawmstoid.Text = GenerateID("QL_TRNSALESRETURMST", CompnyCode)
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNSALESRETURMST", "sretmstoid", sretrawmstoid.Text, "sretmststatus", updtime.Text, "Approved")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    sretrawmststatus.Text = "In Process"
                    Exit Sub
                End If
            End If

            Dim cRate As New ClassRate
            cRate.SetRateValue(CInt(curroid.Text), Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, 2) : Exit Sub
            End If
            'Dim objValue As VarUsageValue
            'SetUsageValue(objValue)

            sretrawdtloid.Text = GenerateID("QL_TRNSALESRETURDTL", CompnyCode)
            Dim iAppOid As Integer = GenerateID("QL_APPROVAL", CompnyCode)
            If sretrawmststatus.Text = "Revised" Then
                sretrawmststatus.Text = "In Process"
            End If
            'Dim taxMst As Double = 0
            'If Not Session("TblDtl") Is Nothing Then
            '    Dim objTable1 As DataTable = Session("TblDtl")
            '    For C1 As Int16 = 0 To objTable1.Rows.Count - 1
            '        taxMst += ToDouble(objTable1.Rows(C1)("taxamt")) * ToDouble(objTable1.Rows(C1)("sretqty"))
            '    Next
            'End If

            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trnsalesreturmst (cmpcode, sretmstoid, periodacctg, sretdate, sretno, srettype, custoid, trnjualmstoid, reasonoid, curroid, sretamt, srettaxamt, sretamtnetto, sretmstrefno, sretmstnote, sretmststatus, sretmstres1, sretmstres2, sretmstres3, createuser, createtime, upduser, updtime, ratetoidr, ratetousd, sretamtidr, sretamtusd, sretamtnettoidr, sretamtnettousd, sretmsttaxamt, sretdiscamt, sretmstres4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & sretrawmstoid.Text & ", '" & periodacctg.Text & "', '" & sretrawdate.Text & "', '" & sretrawno.Text & "', 'SRET', " & custoid.Text & ", " & shipmentrawmstoid.Text & ", 0, " & curroid.Text & ", " & ToDouble(totalRetur.Text) & ", " & ToDouble(taxAmt.Text) & ", " & ToDouble(totalReturNetto.Text) & ", '" & Tchar(sretRefNo.Text) & "', '" & Tchar(sretrawmstnote.Text) & "', '" & sretrawmststatus.Text & "', '" & invType.SelectedValue & "', '" & sretrawmstres2.SelectedValue & "', '" & DDLTypeNota.SelectedValue & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & totalRetur.Text * cRate.GetRateDailyIDRValue & ", " & totalRetur.Text * cRate.GetRateDailyUSDValue & ", " & totalReturNetto.Text * cRate.GetRateDailyIDRValue & ", " & totalReturNetto.Text * cRate.GetRateDailyUSDValue & ", " & ToDouble(taxAmt.Text) & ", " & ToDouble(sretmstdiscamt.Text) & ", '" & Tchar(shipmentrawno.Text) & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & sretrawmstoid.Text & " WHERE tablename='QL_TRNSALESRETURMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trnsalesreturmst SET periodacctg='" & periodacctg.Text & "', sretdate='" & sretrawdate.Text & "', sretno='" & sretrawno.Text & "', srettype='SRET', custoid=" & custoid.Text & ", trnjualmstoid=" & shipmentrawmstoid.Text & ", reasonoid=0, curroid=" & curroid.Text & ", sretamt=" & ToDouble(totalRetur.Text) & ", srettaxamt=" & ToDouble(taxAmt.Text) & ", sretamtnetto=" & ToDouble(totalReturNetto.Text) & ", sretmstrefno='" & Tchar(sretRefNo.Text) & "', sretmstnote='" & Tchar(sretrawmstnote.Text) & "', sretmststatus='" & sretrawmststatus.Text & "', sretmstres1='" & invType.SelectedValue & "', sretmstres2='" & sretrawmstres2.SelectedValue & "', sretmstres3='" & DDLTypeNota.SelectedValue & "', sretmstres4='" & Tchar(shipmentrawno.Text) & "', upduser= '" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, ratetoidr=" & cRate.GetRateDailyIDRValue & ", ratetousd=" & cRate.GetRateDailyUSDValue & ", sretamtidr=" & totalRetur.Text * cRate.GetRateDailyIDRValue & ", sretamtusd=" & totalRetur.Text * cRate.GetRateDailyUSDValue & ", sretamtnettoidr=" & totalReturNetto.Text * cRate.GetRateDailyIDRValue & ", sretamtnettousd=" & totalReturNetto.Text * cRate.GetRateDailyUSDValue & ", sretmsttaxamt=" & ToDouble(taxAmt.Text) & ", sretdiscamt=" & ToDouble(sretmstdiscamt.Text) & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND sretmstoid=" & sretrawmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnjualdtl SET trnjualdtlres1='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnjualdtloid IN (SELECT trnjualdtloid FROM QL_trnsalesreturdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND sretmstoid=" & sretrawmstoid.Text & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnjualmst SET trnjualres2='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnjualmstoid IN (SELECT trnjualmstoid FROM QL_trnsalesreturmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND sretmstoid=" & sretrawmstoid.Text & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trnsalesreturdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND sretmstoid=" & sretrawmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trnsalesreturdtl (cmpcode, sretdtloid, sretmstoid, sretdtlseq, trnjualdtloid, sretwhoid, matoid, sretqty, sretunitoid, sretdtlamt, sretdtlstatus, sretdtlnote, upduser, updtime, sretvalueidr, sretvalueusd, sretdtlamtidr, sretdtlamtusd, qtyunitkecil, qtyunitbesar, valueidr, valueusd, sretdtltaxamt, sretdtldisc1, sretdtldisc2, sretdtldisc3, sretdtlnetto) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C1 + CInt(sretrawdtloid.Text)) & ", " & sretrawmstoid.Text & ", " & C1 + 1 & ", " & objTable.Rows(C1)("trnjualdtloid").ToString & ", " & objTable.Rows(C1)("sretwhoid").ToString & ", " & objTable.Rows(C1)("matoid").ToString & ", " & ToDouble(objTable.Rows(C1)("sretqty").ToString) & ", " & objTable.Rows(C1)("sretunitoid").ToString & ", " & ToDouble(objTable.Rows(C1)("sretamt").ToString) & ", 'Post', '" & Tchar(objTable.Rows(C1)("sretdtlnote").ToString) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(C1)("sretvalueidr").ToString) * cRate.GetRateDailyIDRValue & ", " & ToDouble(objTable.Rows(C1)("sretvalueidr").ToString) * cRate.GetRateDailyUSDValue & ", " & ToDouble(objTable.Rows(C1)("sretamt").ToString) * cRate.GetRateDailyIDRValue & ", " & ToDouble(objTable.Rows(C1)("sretamt").ToString) * cRate.GetRateDailyUSDValue & ", " & ToDouble(objTable.Rows(C1)("qtyunitkecil").ToString) & ", " & ToDouble(objTable.Rows(C1)("qtyunitbesar").ToString) & ", " & ToDouble(objTable.Rows(C1)("valueidr").ToString) & ", " & ToDouble(objTable.Rows(C1)("valueusd").ToString) & ", " & ToDouble(objTable.Rows(C1)("taxamt")) * ToDouble(objTable.Rows(C1)("sretqty")) & ", " & ToDouble(objTable.Rows(C1)("disc1")) * ToDouble(objTable.Rows(C1)("sretqty")) & ", " & ToDouble(objTable.Rows(C1)("disc2")) * ToDouble(objTable.Rows(C1)("sretqty")) & ", " & ToDouble(objTable.Rows(C1)("disc3")) * ToDouble(objTable.Rows(C1)("sretqty")) & ", " & ToDouble(objTable.Rows(C1)("sretamtnetto").ToString) & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        If shipmentrawmstoid.Text > 0 Then
                            If ToDouble(objTable.Rows(C1)("sretqty").ToString) >= ToDouble(objTable.Rows(C1)("jualqty").ToString) Then
                                sSql = "UPDATE QL_trnjualdtl SET trnjualdtlres1='Complete' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnjualdtloid=" & objTable.Rows(C1)("trnjualdtloid")
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                sSql = "UPDATE QL_trnjualmst SET trnjualres2='Closed' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnjualmstoid=" & shipmentrawmstoid.Text & " AND (SELECT COUNT(*) FROM QL_trnjualdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnjualmstoid=" & shipmentrawmstoid.Text & " AND trnjualdtloid<>" & objTable.Rows(C1)("trnjualdtloid") & " AND ISNULL(trnjualdtlres1, '')<>'Complete')=0"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                            End If
                        End If                      
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(sretrawdtloid.Text)) & " WHERE tablename='QL_TRNSALESRETURDTL' AND cmpcode='" & CompnyCode & "' "
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If sretrawmststatus.Text = "In Approval" Then
                    Dim dt As DataTable = Session("AppPerson")
                    For C1 As Integer = 0 To dt.Rows.Count - 1
                        sSql = "INSERT INTO QL_approval (cmpcode, approvaloid, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvalnote) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iAppOid + C1 & ", 'SRET-" & invType.SelectedValue & "" & sretrawmstoid.Text & "_" & (iAppOid + C1) & "', '" & Session("UserID") & "', '" & sretrawdate.Text & "', 'New', 'QL_trnsalesreturmst', " & sretrawmstoid.Text & ", 'In Approval', '0', '" & dt.Rows(C1)("approvaluser").ToString & "', '1/1/1900', '')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                    sSql = "UPDATE QL_mstOid SET lastoid=" & iAppOid + dt.Rows.Count - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_APPROVAL'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If

                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        sretrawmststatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    sretrawmststatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 1)
                conn.Close()
                sretrawmststatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & sretrawmstoid.Text & ".<BR>"
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trnSalesRetur.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnSalesRetur.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If sretrawmstoid.Text.Trim = "" Then
            showMessage("Please select Sales Return data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNsalesreturMST", "sretmstoid", sretrawmstoid.Text, "sretmststatus", updtime.Text, "Approved")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                sretrawmststatus.Text = "In Process"
                Exit Sub
            End If
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trnjualdtl SET trnjualdtlres1='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnjualdtloid IN (SELECT trnjualdtloid FROM QL_trnsalesreturdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND sretmstoid=" & sretrawmstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_trnjualmst SET trnjualres2='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnjualmstoid IN (SELECT trnjualmstoid FROM QL_trnsalesreturmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND sretmstoid=" & sretrawmstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnsalesreturdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND sretmstoid=" & sretrawmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnsalesreturmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND sretmstoid=" & sretrawmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnSalesRetur.aspx?awal=true")
    End Sub

    Protected Sub btnSendApproval_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSendApproval.Click
        sSql = "SELECT approvaluser, apppersontype FROM QL_approvalperson WHERE tablename='QL_trnsalesreturmst' AND activeflag='ACTIVE' AND cmpcode IN ('" & CompnyCode & "', '" & DDLBusUnit.SelectedValue & "')"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_approvalperson")
        If dt.Rows.Count = 0 Then
            showMessage("There is no user can't approve this transaction. Please define approval user in <STRONG>APPROVAL USER</STRONG> form.", 2)
        Else
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "apppersontype='Final'"
            If dv.Count = 0 Then
                showMessage("Final approval user is not defined for this transaction. Please define final approval user in <STRONG>APPROVAL USER</STRONG> form.", 2)
                dv.RowFilter = ""
            Else
                dv.RowFilter = ""
                Session("AppPerson") = dt
                sretrawmststatus.Text = "In Approval"
                btnSave_Click(Nothing, Nothing)
            End If
        End If
    End Sub

    Protected Sub btnPrintHdr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrintHdr.Click
        PrintReport("")
    End Sub

    Protected Sub gvList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvList.SelectedIndexChanged
        PrintReport(gvList.SelectedDataKey.Item("sretmstoid").ToString)
    End Sub

    Protected Sub sretrawqty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        dtlAmt.Text = ToMaskEdit((ToDouble(sretrawqty.Text) * ToDouble(price.Text)), 2)
    End Sub

    Protected Sub DDLTypeNota_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If DDLTypeNota.SelectedValue.ToUpper = "NOTA LAMA" Then
            btnSearchShip.Visible = False
            btnClearShip.Visible = False
            shipmentrawno.Enabled = True
            shipmentrawno.CssClass = "inpText"
            shipmentrawmstoid.Text = "0"
            curr.Text = "IDR"
            curroid.Text = "1"
            invDate.Text = Format(GetServerTime, "MM/dd/yyyy")
            lunas.Text = "Lunas"
            lunas.Visible = True
            sretrawmstres2.Visible = True
            ttkReason.Visible = True
            lblReason.Visible = True
            DDLreason("NOTALAMA")
        Else
            btnSearchShip.Visible = True
            btnClearShip.Visible = True
            shipmentrawno.Enabled = False
            shipmentrawno.CssClass = "inpTextDisabled"
            invDate.Text = ""
            lunas.Text = ""
            lunas.Visible = False
            sretrawmstres2.Visible = False
            ttkReason.Visible = False
            lblReason.Visible = False
        End If
        If i_u.Text = "New Data" Then
            btnClearShip_Click(Nothing, Nothing)
        End If
    End Sub

    Protected Sub gvCOAPosting_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 2)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 2)
        End If
    End Sub

    Protected Sub lkbCloseCOAPosting_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, False)
    End Sub

    Protected Sub btnShowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        DDLRateType.SelectedIndex = -1
        ShowCOAPosting(sretrawno.Text, DDLBusUnit.SelectedValue, gvCOAPosting, DDLRateType.SelectedValue, "QL_trnsalesreturmst " & sretrawmstoid.Text)
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, True)
    End Sub

    Protected Sub gvListMat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If shipmentrawmstoid.Text <> gvListMat.SelectedDataKey.Item("trnjualmstoid").ToString Then
                btnClearShip_Click(Nothing, Nothing)
            End If
            shipmentrawmstoid.Text = gvListMat.SelectedDataKey.Item("trnjualmstoid").ToString
            CekPembayaran(shipmentrawmstoid.Text)
            shipmentrawno.Text = gvListMat.SelectedDataKey.Item("trnjualno").ToString
            invDate.Text = gvListMat.SelectedDataKey.Item("trnjualdate").ToString
            taxAmt.Text = ToMaskEdit(ToDouble(gvListMat.SelectedDataKey.Item("trntaxamt").ToString), 2)
            If shipmentrawmstoid.Text > 0 Then
                totalRetur.Text = ToMaskEdit(ToDouble(gvListMat.SelectedDataKey.Item("trnjualamt").ToString), 2)
                totalReturNetto.Text = ToMaskEdit(ToDouble(gvListMat.SelectedDataKey.Item("trnjualamtnetto").ToString), 2)
            Else
                totalRetur.Text = ToMaskEdit(Session("sisa"), 2)
                totalReturNetto.Text = ToMaskEdit(Session("sisa"), 2)
            End If
            discHdr.Text = ToMaskEdit(ToDouble(gvListMat.SelectedDataKey.Item("trndiscamt").ToString), 2)
            lblDisc.Text = ToMaskEdit(ToDouble(gvListMat.SelectedDataKey.Item("trndiscamt").ToString), 2)
            tipe.Text = gvListMat.SelectedDataKey.Item("tipe").ToString

            If shipmentrawmstoid.Text > 0 Then
                sSql = "SELECT som.curroid FROM QL_trndomst dom INNER JOIN QL_trnsomst som ON som.cmpcode=dom.cmpcode AND som.somstoid=dom.somstoid WHERE dom.domstoid=" & gvListMat.SelectedDataKey.Item("domstoid").ToString & ""
                curroid.Text = cKon.ambilscalar(sSql)
                sSql = "SELECT currcode FROM QL_mstcurr WHERE curroid=" & curroid.Text & ""
                curr.Text = GetStrData(sSql)
            Else
                sSql = "SELECT dom.curroid FROM QL_trnjualmst dom WHERE dom.trnjualmstoid=" & gvListMat.SelectedDataKey.Item("trnjualmstoid").ToString & ""
                curroid.Text = cKon.ambilscalar(sSql)
                sSql = "SELECT currcode FROM QL_mstcurr WHERE curroid=" & curroid.Text & ""
                curr.Text = GetStrData(sSql)
            End If
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

#End Region

End Class