Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_BeritaAcara
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetTextValue(ByVal cc As System.Web.UI.ControlCollection, Optional ByVal sType As String = "") As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                If sType = "" Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                Else
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).ToolTip
                End If
            End If
        Next
        Return sReturn
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If acararefoid.Text = "" Then
            sError &= "- Please select MATERIAL field!<BR>"
        End If
        If acaraqty.Text = "" Then
            sError &= "- Please fill QUANTITY field!<BR>"
        Else
            If ToDouble(acaraqty.Text) <= 0 Then
                sError &= "- QUANTITY field must be more than 0!<BR>"
            Else
                'If ToDouble(acaraqty.Text) > ToDouble(maxqty.Text) Then
                '    sError &= "- QUANTITY field must be less than Max Qty!<BR>"
                'End If
            End If
        End If
        If acaratype.SelectedValue = "Material Transform" Then
            If matoid_kik.Text = "" Then
                sError &= "- Please select MATERIAL KIK field!<BR>"
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If acaradate.Text = "" Then
            sError &= "- Please fill BA DATE field!<BR>"
        Else
            If Not IsValidDate(acaradate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- BA DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If womstoid.Text = "" Then
            sError &= "- Please select KIK NO. field!<BR>"
        End If
        If deptoid.SelectedValue = "" Then
            sError &= "- Please select DEPARTMENT field!<BR>"
        End If
        If acarawhoid.SelectedValue = "" Then
            sError &= "- Please select WAREHOUSE field!<BR>"
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill Detail Data!<BR>"
        Else
            Dim dtTbl As DataTable = Session("TblDtl")
            If dtTbl.Rows.Count <= 0 Then
                sError &= "- Please fill Detail Data!<BR>"
            Else
                If acaratype.SelectedValue.ToUpper <> "MATERIAL RETURN" Then
                    For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                        If Not IsStockAvailable(DDLBusUnit.SelectedValue, Format(GetServerTime(), "yyyyMM"), dtTbl.Rows(C1)("acararefoid"), acarawhoid.SelectedValue, ToDouble((dtTbl.Compute("SUM(acaraqty_unitkecil)", "acararefoid='" & dtTbl.Rows(C1)("acararefoid") & "'")).ToString)) Then
                            sError &= "- Material Code <B><STRONG>" & dtTbl.Rows(C1)("acararefcode") & "</STRONG></B> must be equal or less than Stock Qty !<BR> "
                            Exit For
                        End If
                    Next
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            acaramststatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function GetLabelValue(ByVal cc As System.Web.UI.ControlCollection, ByVal sLabelID As String) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                If CType(myControl, System.Web.UI.WebControls.Label).ID = sLabelID Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.Label).Text
                End If
            End If
        Next
        Return sReturn
    End Function

    Public Function GetParameterID() As String
        Return Eval("matoid") & "," & Eval("selectedunitoid")
    End Function

    Private Function GetDropDownListValue(ByVal cc As System.Web.UI.ControlCollection, Optional ByVal sText As String = "") As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                If sText = "" Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
                Else
                    sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedItem.Text
                End If
            End If
        Next
        Return sReturn
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trnbrtacaramst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND acaramststatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnBeritaAcara.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lkbACRInProcess.Visible = True
            lkbACRInProcess.Text = "You have " & GetStrData(sSql) & " In Process Berita Acara data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
        sSql = "SELECT COUNT(*) FROM QL_trnbrtacaramst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND acaramststatus='In Approval'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnBeritaAcara.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lkbACRInApproval.Visible = True
            lkbACRInApproval.Text = "You have " & GetStrData(sSql) & " In Approval Berita Acara data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitAllDDL()
        'Fill DDL Division
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND divcode='" & Session("CompnyCode") & "'"
        End If
        If FillDDL(DDLBusUnit, sSql) Then
            InitDDLWH()
        End If
        'Fill DDL Unit
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='UNIT'"
        If i_u.Text = "New Data" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
        FillDDL(acaraunitoid, sSql)
        FillDDL(matunitoid_kik, sSql)
        FillDDL(stockunitoid, sSql)
    End Sub

    Private Sub InitDDLDept()
        'Fill DDL Department
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND deptoid IN (SELECT deptoid FROM QL_trnwodtl2 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid=" & womstoid.Text & " AND wodtl2type='FG') ORDER BY deptname"
        FillDDL(deptoid, sSql)
    End Sub

    Private Sub InitDDLWH()
        'fill DDL Warehouse
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='WAREHOUSE' AND genoid>0"
        If i_u.Text = "New Data" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
        FillDDL(acarawhoid, sSql)
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT div.divname, acaramstoid, acarano, CONVERT(CHAR(10), acaradate, 101) AS acaradate, deptname, acaramststatus, acaramstnote, 'False' AS checkvalue, wono, womstres2 FROM QL_trnbrtacaramst acr INNER JOIN QL_trnwomst wom ON wom.cmpcode=acr.cmpcode AND wom.womstoid=acr.womstoid INNER JOIN QL_mstdept d ON d.cmpcode=acr.cmpcode AND d.deptoid=acr.deptoid INNER JOIN QL_mstdivision div ON div.cmpcode=acr.cmpcode WHERE"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " acr.cmpcode='" & Session("CompnyCode") & "'"
        Else
            sSql &= " acr.cmpcode LIKE '%'"
        End If
        sSql &= sSqlPlus & " ORDER BY acr.acaramstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trnbrtacaramst")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "acaramstoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub PrintReport()
        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("acaramstoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            report.Load(Server.MapPath(folderReport & "rptBA.rpt"))
            Dim sWhere As String
            If Session("CompnyCode") <> CompnyCode Then
                sWhere = " WHERE acr.cmpcode='" & Session("CompnyCode") & "'"
            Else
                sWhere = " WHERE acr.cmpcode LIKE '%'"
            End If
            If sOid = "" Then
                sWhere &= " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND acaradate>='" & FilterPeriod1.Text & " 00:00:00' AND acaradate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sWhere &= " AND acaramststatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Transaction\trnBeritaAcara.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND acr.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sWhere &= " AND acr.acaramstoid IN (" & sOid & ")"
            End If
            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "BeritaAcaraPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message & ex.ToString, 1)
        End Try
        Response.Redirect("~\Transaction\trnBeritaAcara.aspx?awal=true")
    End Sub

    Private Sub BindListKIK()
        sSql = "SELECT womstoid, wono, CONVERT(VARCHAR(10), wodate, 101) AS wodate, womstnote, womstres2 AS wodesc FROM QL_trnwomst wom WHERE wom.cmpcode='" & DDLBusUnit.SelectedValue & "' AND womststatus='Post' AND " & FilterDDLListKIK.SelectedValue & " LIKE '%" & Tchar(FilterTextListKIK.Text) & "%' ORDER BY wono"
        FillGV(gvListKIK, sSql, "QL_trnwomst")
    End Sub

    Private Sub InitDDLCat1()
        'Fill DDL Category 1
        Dim sType As String = "", sRef As String = "", sItemType As String = ""
        If acarareftype.SelectedValue = "Raw" Then
            sType = "item" : sRef = "RAW MATERIAL" : sItemType = "RAW"
        ElseIf acarareftype.SelectedValue = "General" Then
            sType = "item" : sRef = "GENERAL MATERIAL" : sItemType = "GEN"
        ElseIf acarareftype.SelectedValue = "WIP" Then
            sType = "item" : sRef = "WIP" : sItemType = "WIP"
        Else
            sType = "item" : sRef = "FINISH GOOD" : sItemType = "FG"
        End If

        sSql = "SELECT cat1oid, (cat1code + ' - ' + cat1shortdesc) cat1shortdesc FROM QL_mstcat1 WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND cat1res1='" & sItemType & "'"
        sSql &= " ORDER BY cat1code"
        If FillDDL(DDLCat01, sSql) Then
            InitDDLCat2()
        Else
            DDLCat02.Items.Clear()
            DDLCat03.Items.Clear()
            DDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat2()
        'Fill DDL Category 2
        Dim sType As String = "", sRef As String = "", sItemType As String = ""
        If acarareftype.SelectedValue = "Raw" Then
            sType = "item" : sRef = "RAW MATERIAL" : sItemType = "RAW"
        ElseIf acarareftype.SelectedValue = "General" Then
            sType = "item" : sRef = "GENERAL MATERIAL" : sItemType = "GEN"
        ElseIf acarareftype.SelectedValue = "WIP" Then
            sType = "item" : sRef = "WIP" : sItemType = "WIP"
        Else
            sType = "item" : sRef = "FINISH GOOD" : sItemType = "FG"
        End If
        sSql = "SELECT cat2oid, (cat2code + ' - ' + cat2shortdesc) cat2shortdesc FROM QL_mstcat2 c2 INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c2.cmpcode AND c1.cat1oid=c2.cat1oid WHERE c2.cmpcode='" & CompnyCode & "' AND c2.activeflag='ACTIVE' AND c1.cat1oid='" & DDLCat01.SelectedValue & "' AND cat2res1='" & sItemType & "' AND cat1res1='" & sItemType & "' "
      
        sSql &= " ORDER BY cat2code"
        If FillDDL(DDLCat02, sSql) Then
            InitDDLCat3()
        Else
            DDLCat03.Items.Clear()
            DDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat3()
        'Fill DDL Category 3
        Dim sType As String = "", sRef As String = "", sItemType As String = ""
        If acarareftype.SelectedValue = "Raw" Then
            sType = "item" : sRef = "RAW MATERIAL" : sItemType = "RAW"
        ElseIf acarareftype.SelectedValue = "General" Then
            sType = "item" : sRef = "GENERAL MATERIAL" : sItemType = "GEN"
        ElseIf acarareftype.SelectedValue = "WIP" Then
            sType = "item" : sRef = "WIP" : sItemType = "WIP"
        Else
            sType = "item" : sRef = "FINISH GOOD" : sItemType = "FG"
        End If
        sSql = "SELECT cat3oid, (cat3code + ' - ' + cat3shortdesc) cat3shortdesc FROM QL_mstcat3 c3 INNER JOIN QL_mstcat2 c2 ON c2.cmpcode=c3.cmpcode AND c2.cat2oid=c3.cat2oid INNER JOIN QL_mstcat1 c1 ON c1.cmpcode=c3.cmpcode AND c1.cat1oid=c3.cat1oid WHERE c3.cmpcode='" & CompnyCode & "' AND c3.activeflag='ACTIVE' AND c2.cat2oid='" & DDLCat02.SelectedValue & "' AND cat3res1='" & sItemType & "' AND cat2res1='" & sItemType & "' AND cat1res1='" & sItemType & "'"
      
        sSql &= " ORDER BY cat3code"
        If FillDDL(DDLCat03, sSql) Then
            InitDDLCat4()
        Else
            DDLCat04.Items.Clear()
        End If
    End Sub

    Private Sub InitDDLCat4()
        'Fill DDL Category 3
        sSql = "SELECT genoid, gencode +' - '+ gendesc FROM QL_mstgen WHERE gengroup='CAT4' AND activeflag='ACTIVE' ORDER BY gencode"
        FillDDL(DDLCat04, sSql)
    End Sub

    Private Sub BindListMatUsage()
        Session("TblListMat") = Nothing : Session("TblListMatView") = Nothing : gvListMatUsage.DataSource = Session("TblListMat") : gvListMatUsage.DataBind()
        Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
        Dim sType As String = "", sRef As String = "", sItemType As String = ""
        If acarareftype.SelectedValue = "Raw" Then
            sType = "item" : sRef = "RAW MATERIAL" : sItemType = "RAW"
        ElseIf acarareftype.SelectedValue = "General" Then
            sType = "item" : sRef = "GENERAL MATERIAL" : sItemType = "GEN"
        ElseIf acarareftype.SelectedValue = "WIP" Then
            sType = "item" : sRef = "WIP" : sItemType = "WIP"
        Else
            sType = "item" : sRef = "FINISH GOOD" : sItemType = "FG"
        End If
       
        sSql = "SELECT * FROM ("
        sSql &= "SELECT " & sType & "oid AS matoid, itemoldcode AS oldcode, itemcat1, itemcat2, itemcat3, '' AS matno, " & sType & "code AS matcode, " & sType & "longdescription AS matlongdesc, (select dbo.getstockqty(" & sType & "oid, " & acarawhoid.SelectedValue & ")) AS matqty, 0.0 AS maxqty, " & sType & "unit1 AS matunitoid, g.gendesc AS matunit, 'False' AS checkvalue, 'True' AS enableqty, (select dbo.getStockValue(" & sType & "oid)) AS matvalueidr, 0 AS matvalueusd, m.createuser, 0 AS wodtl2oid, 0 AS wodtl3oid, 0 AS matoid_kik, '' AS matcode_kik, '' AS matlongdesc_kik, 0.0 AS matqty_kik, 0 AS matunitoid_kik, '' AS matoldcode_kik, 0 AS kik_cat1oid, 0 AS kik_cat2oid, 0 AS kik_cat3oid, m.itemUnit1 selectedunitoid, gx.gendesc selectedunit, m.itemunit1 AS stockunitoid FROM QL_mst" & sType & " m INNER JOIN QL_mstgen g ON genoid=" & sType & "unit1 INNER JOIN QL_mstgen gx ON gx.genoid=m.itemUnit1 WHERE m.cmpcode='" & CompnyCode & "' AND m.itemrecordstatus='ACTIVE' AND m.itemgroup='" & sItemType & "' /*AND " & sType & "oid NOT IN (SELECT wodtl3refoid FROM QL_trnwodtl3 wod3 INNER JOIN QL_trnwodtl2 wod2 ON wod2.cmpcode=wod3.cmpcode AND wod2.wodtl2oid=wod3.wodtl2oid WHERE wod3.cmpcode='" & DDLBusUnit.SelectedValue & "' AND wod3.womstoid=" & womstoid.Text & " AND wod2.deptoid=" & deptoid.SelectedValue & " AND wodtl3reftype='" & acarareftype.SelectedValue & "' AND wodtl3reqflag='')*/"
        sSql &= ") AS QL_mstmat WHERE "
        Dim sFilter As String = ""
        If acarareftype.SelectedIndex > 2 Then
            If FilterDDLListMatUsage.SelectedIndex = 0 Then
                Dim sText() As String = FilterTextListMatUsage.Text.Split(";")
                For C1 As Integer = 0 To sText.Length - 1
                    If sText(C1) <> "" Then
                        sFilter &= FilterDDLListMatUsage.SelectedValue & " LIKE '" & Tchar(sText(C1)) & "' OR "
                    End If
                Next
                If sFilter <> "" Then
                    sFilter = "(" & Left(sFilter, sFilter.Length - 4) & ")"
                Else
                    sFilter = "1=1"
                End If
            Else
                sFilter = FilterDDLListMatUsage.SelectedValue & " LIKE '%" & Tchar(FilterTextListMatUsage.Text) & "%'"
            End If
        Else
            sFilter = FilterDDLListMatUsage.SelectedValue & " LIKE '%" & Tchar(FilterTextListMatUsage.Text) & "%'"
        End If
        If cbCat04.Checked And DDLCat04.SelectedValue <> "" Then
            sFilter &= " AND itemcat4='" & DDLCat04.SelectedValue & "'"
        Else
            If cbCat03.Checked And DDLCat03.SelectedValue <> "" Then
                sFilter &= " AND itemcat3='" & DDLCat03.SelectedValue & "'"
            Else
                If cbCat02.Checked And DDLCat02.SelectedValue <> "" Then
                    sFilter &= " AND itemcat2='" & DDLCat02.SelectedValue & "'"
                Else
                    If cbCat01.Checked And DDLCat01.SelectedValue <> "" Then
                        sFilter &= " AND itemcat1='" & DDLCat01.SelectedValue & "'"
                    End If
                End If
            End If
        End If
        sSql &= sFilter
        sSql &= " ORDER BY matno, matcode"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstmat")
        Dim dv As DataView = dt.DefaultView
        'dv.RowFilter = "matqty>0"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                dv(C1)("maxqty") = ToDouble(dv(C1)("matqty").ToString)
            Next
        End If

        Session("TblListMat") = dv.ToTable
        Session("TblListMatView") = dv.ToTable
        gvListMatUsage.DataSource = Session("TblListMatView")
        gvListMatUsage.DataBind()
        Session("IsTransform") = False
    End Sub

    Private Sub BindListMatReturn()
        Try
            Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
            sSql = "SELECT itemoid AS matoid, itemoldcode AS oldcode, itemcat1, itemcat2, itemcat3, '' AS matno, itemcode AS matcode, itemlongdescription AS matlongdesc "
            sSql &= ", (ISNULL((SELECT SUM(usageqty) AS usgqty FROM QL_trnusagedtl mud INNER JOIN QL_trnusagemst mum ON mum.cmpcode=mud.cmpcode AND mum.usagemstoid=mud.usagemstoid WHERE mud.cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagemststatus='Post' AND mum.womstoid=" & womstoid.Text & " AND mud.matoid=m.itemoid),0) - (SELECT ISNULL(SUM(acaraqty), 0.0) FROM QL_trnbrtacaradtl bad INNER JOIN QL_trnbrtacaramst bam ON bam.cmpcode=bad.cmpcode AND bam.acaramstoid=bad.acaramstoid WHERE bad.cmpcode='" & DDLBusUnit.SelectedValue & "' AND acaratype='Material Return' AND acaramststatus='Approved' AND womstoid=" & womstoid.Text & " AND acararefoid=m.itemoid)) AS matqty "
            sSql &= ", (ISNULL((SELECT SUM(usageqty) AS usgqty FROM QL_trnusagedtl mud INNER JOIN QL_trnusagemst mum ON mum.cmpcode=mud.cmpcode AND mum.usagemstoid=mud.usagemstoid WHERE mud.cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagemststatus='Post' AND mum.womstoid=" & womstoid.Text & " AND mud.matoid=m.itemoid),0) - (SELECT ISNULL(SUM(acaraqty), 0.0) FROM QL_trnbrtacaradtl bad INNER JOIN QL_trnbrtacaramst bam ON bam.cmpcode=bad.cmpcode AND bam.acaramstoid=bad.acaramstoid WHERE bad.cmpcode='" & DDLBusUnit.SelectedValue & "' AND acaratype='Material Return' AND acaramststatus='Approved' AND womstoid=" & womstoid.Text & " AND acararefoid=m.itemoid)) AS maxqty "
            sSql &= ", itemunit1 AS matunitoid, g.gendesc AS matunit, 'False' AS checkvalue, 'True' AS enableqty, ISNULL((SELECT SUM(ISNULL(stockqty, 0) * ISNULL(stockvalueidr, 0)) / NULLIF(SUM(stockqty), 0) FROM QL_stockvalue st WHERE st.cmpcode='" & DDLBusUnit.SelectedValue & "' AND st.periodacctg IN ('" & sPeriod & "', '" & GetLastPeriod(sPeriod) & "') AND st.refoid=itemoid AND closeflag=''), 0.0) AS matvalueidr, ISNULL((SELECT SUM(ISNULL(stockqty, 0) * ISNULL(stockvalueusd, 0)) / NULLIF(SUM(stockqty), 0) FROM QL_stockvalue st WHERE st.cmpcode='" & DDLBusUnit.SelectedValue & "' AND st.periodacctg IN ('" & sPeriod & "', '" & GetLastPeriod(sPeriod) & "') AND st.refoid=itemoid AND closeflag=''), 0.0) AS matvalueusd, m.createuser, 0 AS wodtl2oid, 0 AS wodtl3oid, 0 AS matoid_kik, '' AS matcode_kik, '' AS matlongdesc_kik, 0.0 AS matqty_kik, 0 AS matunitoid_kik, '' AS matoldcode_kik, 0 AS kik_cat1oid, 0 AS kik_cat2oid, 0 AS kik_cat3oid, m.itemUnit1 selectedunitoid, gx.gendesc selectedunit, m.itemunit1 AS stockunitoid FROM QL_mstitem m INNER JOIN QL_mstgen g ON genoid=itemunit1 INNER JOIN QL_mstgen gx ON gx.genoid=m.itemUnit1 WHERE m.cmpcode='" & DDLBusUnit.SelectedValue & "' AND m.itemrecordstatus='ACTIVE' AND itemoid IN (SELECT wodtl3refoid FROM QL_trnwodtl3 wod3 INNER JOIN QL_trnwodtl2 wod2 ON wod2.cmpcode=wod3.cmpcode AND wod2.wodtl2oid=wod3.wodtl2oid WHERE wod3.cmpcode='" & DDLBusUnit.SelectedValue & "' AND wod3.womstoid=" & womstoid.Text & ")"
            Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstmat")
            If dt.Rows.Count > 0 Then
                For C1 As Integer = 0 To dt.Rows.Count - 1
                    dt.Rows(C1)("matqty") = ToDouble(dt.Rows(C1)("maxqty").ToString)
                Next
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "matqty > 0"
                Session("TblListMat") = dv.ToTable
                Session("TblListMatView") = Session("TblListMat")
                gvListMatReturn.DataSource = Session("TblListMatView")
                gvListMatReturn.DataBind()
                cProc.SetModalPopUpExtender(btnHideListMatReturn, pnlListMatReturn, mpeListMatReturn, True)
            Else
                showMessage("Material data can't be found!", 2)
            End If
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 2)
        End Try
    End Sub

    Private Sub UpdateCheckedMatUsage()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListMatUsage.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMatUsage.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                            dv(0)("matqty") = ToDouble(GetTextValue(row.Cells(4).Controls))
                            dv(0)("selectedunitoid") = GetDropDownListValue(row.Cells(5).Controls)
                            dv(0)("selectedunit") = GetDropDownListValue(row.Cells(5).Controls, "Text")
                            If acaratype.SelectedValue = "Material Transform" Then
                                dv(0)("wodtl3oid") = GetLabelValue(row.Cells(7).Controls, "lbl01")
                                dv(0)("matoid_kik") = GetLabelValue(row.Cells(7).Controls, "lbl02")
                                dv(0)("matcode_kik") = GetTextValue(row.Cells(7).Controls)
                                dv(0)("matlongdesc_kik") = GetTextValue(row.Cells(7).Controls, "Tooltip")
                                dv(0)("matqty_kik") = ToDouble(GetLabelValue(row.Cells(7).Controls, "lbl03"))
                                dv(0)("matunitoid_kik") = GetLabelValue(row.Cells(7).Controls, "lbl04")
                                dv(0)("wodtl2oid") = GetLabelValue(row.Cells(7).Controls, "lbl05")
                                dv(0)("kik_cat1oid") = GetLabelValue(row.Cells(7).Controls, "kik_cat1oid")
                                dv(0)("kik_cat2oid") = GetLabelValue(row.Cells(7).Controls, "kik_cat2oid")
                                dv(0)("kik_cat3oid") = GetLabelValue(row.Cells(7).Controls, "kik_cat3oid")
                            End If
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMat") = dt
        End If
        If Session("TblListMatView") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMatView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListMatUsage.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMatUsage.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                            dv(0)("matqty") = ToDouble(GetTextValue(row.Cells(4).Controls))
                            dv(0)("selectedunitoid") = GetDropDownListValue(row.Cells(5).Controls)
                            dv(0)("selectedunit") = GetDropDownListValue(row.Cells(5).Controls, "Text")
                            If acaratype.SelectedValue = "Material Transform" Then
                                dv(0)("wodtl3oid") = GetLabelValue(row.Cells(7).Controls, "lbl01")
                                dv(0)("matoid_kik") = GetLabelValue(row.Cells(7).Controls, "lbl02")
                                dv(0)("matcode_kik") = GetTextValue(row.Cells(7).Controls)
                                dv(0)("matlongdesc_kik") = GetTextValue(row.Cells(7).Controls, "Tooltip")
                                dv(0)("matqty_kik") = ToDouble(GetLabelValue(row.Cells(7).Controls, "lbl03"))
                                dv(0)("matunitoid_kik") = GetLabelValue(row.Cells(7).Controls, "lbl04")
                                dv(0)("wodtl2oid") = GetLabelValue(row.Cells(7).Controls, "lbl05")
                                dv(0)("kik_cat1oid") = GetLabelValue(row.Cells(7).Controls, "kik_cat1oid")
                                dv(0)("kik_cat2oid") = GetLabelValue(row.Cells(7).Controls, "kik_cat2oid")
                                dv(0)("kik_cat3oid") = GetLabelValue(row.Cells(7).Controls, "kik_cat3oid")
                            End If
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMatView") = dt
        End If
    End Sub

    Private Sub UpdateCheckedMatReturn()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListMatReturn.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMatReturn.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                            dv(0)("matqty") = ToDouble(GetTextValue(row.Cells(3).Controls))
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMat") = dt
        End If
        If Session("TblListMatView") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMatView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListMatReturn.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListMatReturn.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "matoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                            dv(0)("matqty") = ToDouble(GetTextValue(row.Cells(3).Controls))
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMatView") = dt
        End If
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("TableDetail")
        dtlTable.Columns.Add("acaradtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("acarareftype", Type.GetType("System.String"))
        dtlTable.Columns.Add("acararefoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("acararefcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("acararefoldcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("acarareflongdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("acaraqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("acaraunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("stockunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("acaraunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("maxqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("acaradtlnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("acaravalueidr", Type.GetType("System.Double"))
        dtlTable.Columns.Add("acaravalueusd", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl2oid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl3oid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matoid_kik", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matcode_kik", Type.GetType("System.String"))
        dtlTable.Columns.Add("matoldcode_kik", Type.GetType("System.String"))
        dtlTable.Columns.Add("matlongdesc_kik", Type.GetType("System.String"))
        dtlTable.Columns.Add("matqty_kik", Type.GetType("System.Double"))
        dtlTable.Columns.Add("matunitoid_kik", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("kik_cat1oid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("kik_cat2oid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("kik_cat3oid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("acaraqty_unitkecil", Type.GetType("System.Double"))
        dtlTable.Columns.Add("acaraqty_unitbesar", Type.GetType("System.Double"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub ClearDetail()
        acaradtlseq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                EnableHeader(False)
                acaradtlseq.Text = objTable.Rows.Count + 1
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        i_u2.Text = "New Detail"
        acarareftype.SelectedIndex = -1
        acararefoid.Text = ""
        acararefcode.Text = ""
        acarareflongdesc.Text = ""
        acaraqty.Text = ""
        acaraunitoid.SelectedIndex = -1
        stockunitoid.SelectedIndex = -1
        maxqty.Text = ""
        acaradtlnote.Text = ""
        acaravalueidr.Text = ""
        acaravalueusd.Text = ""
        wodtl2oid.Text = ""
        wodtl3oid.Text = ""
        matoid_kik.Text = ""
        matcode_kik.Text = ""
        oldcode_kik.Text = ""
        matoldcode.Text = ""
        matlongdesc_kik.Text = ""
        matqty_kik.Text = ""
        matunitoid_kik.SelectedIndex = -1
        gvACRDtl.SelectedIndex = -1
        acarareftype.Enabled = True : acarareftype.CssClass = "inpText" : btnSearchMat.Visible = True : acaraqty.Enabled = True : acaraqty.CssClass = "inpText"
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal : DDLBusUnit.CssClass = sCss
        btnSearchKIK.Visible = bVal : btnClearKIK.Visible = bVal
        deptoid.Enabled = bVal : deptoid.CssClass = sCss
        acarawhoid.Enabled = bVal : acarawhoid.CssClass = sCss
        acaratype.Enabled = bVal : acaratype.CssClass = sCss
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        sSql = "SELECT acr.cmpcode, acaramstoid, acr.periodacctg, acarano, acaradate, deptoid, acr.womstoid, acaratype, acarawhoid, acaramstnote, acaramststatus, acr.createuser, acr.createtime, acr.upduser, acr.updtime, wono, womstres2 AS itemdesc FROM QL_trnbrtacaramst acr INNER JOIN QL_trnwomst wom ON wom.cmpcode=acr.cmpcode AND wom.womstoid=acr.womstoid WHERE acaramstoid=" & sOid
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        While xreader.Read
            DDLBusUnit.SelectedValue = xreader("cmpcode").ToString
            acaramstoid.Text = xreader("acaramstoid").ToString
            PeriodAcctg.Text = xreader("periodacctg").ToString
            acarano.Text = xreader("acarano").ToString
            acaradate.Text = Format(xreader("acaradate"), "MM/dd/yyyy")
            womstoid.Text = xreader("womstoid").ToString
            wono.Text = xreader("wono").ToString
            InitDDLDept()
            deptoid.SelectedValue = xreader("deptoid").ToString
            acaratype.SelectedValue = xreader("acaratype").ToString
            acaratype_SelectedIndexChanged(Nothing, Nothing)
            InitDDLWH()
            acarawhoid.SelectedValue = xreader("acarawhoid").ToString
            acaramstnote.Text = xreader("acaramstnote").ToString
            acaramststatus.Text = xreader("acaramststatus").ToString
            createuser.Text = xreader("createuser").ToString
            createtime.Text = xreader("createtime").ToString
            upduser.Text = xreader("upduser").ToString
            updtime.Text = xreader("updtime").ToString
            itemdesc.Text = xreader("itemdesc").ToString
        End While
        xreader.Close()
        conn.Close()
        If acaramststatus.Text <> "In Process" And acaramststatus.Text <> "Revised" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnSendApproval.Visible = False
            btnAddToList.Visible = False
            gvACRDtl.Columns(0).Visible = False
            gvACRDtl.Columns(gvACRDtl.Columns.Count - 1).Visible = False
            If acaramststatus.Text = "Approved" Or acaramststatus.Text = "Closed" Then
                lblTrnNo.Text = "BA No."
                acaramstoid.Visible = False
                acarano.Visible = True
            End If
        End If

        Dim sQueryMax As String = ""
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sQueryMax = "ISNULL((SELECT SUM(saldoakhir) FROM QL_crdstock WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND periodacctg IN ('" & sPeriod & "', '" & GetLastPeriod(sPeriod) & "') AND closingdate='01/01/1900' AND refoid=acararefoid AND mtrlocoid=" & acarawhoid.SelectedValue & "), 0.0)"

        sSql = "SELECT acaradtlseq, acarareftype, acararefoid, (SELECT itemcode FROM QL_mstitem m WHERE itemoid=acararefoid) AS acararefcode, (SELECT itemoldcode FROM QL_mstitem m WHERE itemoid=acararefoid) AS acararefoldcode, (SELECT itemLongDescription FROM QL_mstitem m WHERE itemoid=acararefoid) AS acarareflongdesc, acaraqty, acaraunitoid, gendesc AS acaraunit, (" & sQueryMax & ") AS maxqty, acaradtlnote, acaravalueidr, acaravalueusd, wodtl2oid, wodtl3oid, matoid_kik, (CASE matoid_kik WHEN 0 THEN '' ELSE (SELECT itemCode FROM QL_mstitem m WHERE itemoid=matoid_kik) END) AS matcode_kik, (CASE matoid_kik WHEN 0 THEN '' ELSE (SELECT itemoldcode FROM QL_mstitem m WHERE itemoid=matoid_kik) END) AS matoldcode_kik, (CASE matoid_kik WHEN 0 THEN '' ELSE (SELECT itemLongDescription FROM QL_mstitem m WHERE itemoid=matoid_kik) END) AS matlongdesc_kik, matqty_kik, matunitoid_kik, acaraqty_unitkecil, acaraqty_unitbesar, (SELECT itemunit1 FROM QL_mstitem m WHERE itemoid=acararefoid) AS stockunitoid FROM QL_trnbrtacaradtl acr INNER JOIN QL_mstgen g ON genoid=acaraunitoid WHERE acaramstoid=" & sOid & " ORDER BY acaradtlseq"
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnbrtacaradtl")
        Session("TblDtl") = dtTbl
        gvACRDtl.DataSource = Session("TblDtl")
        gvACRDtl.DataBind()
        ClearDetail()
    End Sub

    Private Sub BindListMatKIK()
        Dim sType As String = "", sType2 As String = "", sTypeBA = ""
        If acarareftype.SelectedValue.ToUpper = "RAW" Then
            sType = "item" : sType2 = ""
        ElseIf acarareftype.SelectedValue.ToUpper = "GENERAL" Then
            sType = "item" : sType2 = ""
        ElseIf acarareftype.SelectedValue.ToUpper = "SPARE PART" Then
            sType = "sparepart" : sType2 = "sp"
        End If
       
        sSql = "SELECT wodtl2oid, wodtl3oid, matoid_kik, matcode_kik, matlongdesc_kik, matqty_kik, matunitoid_kik, matunit_kik, kik_cat1oid, kik_cat2oid, kik_cat3oid, matoldcode_kik FROM (SELECT wod3.wodtl2oid, itemoldcode AS matoldcode_kik, itemcat1 AS kik_cat1oid, itemcat2 AS kik_cat2oid, itemcat3 AS kik_cat3oid, wodtl3oid, wodtl3refoid AS matoid_kik, " & sType & "code AS matcode_kik, " & sType & "longdescription AS matlongdesc_kik, (wodtl3qty - ISNULL((SELECT SUM(usage" & sType2 & "qty) FROM QL_trnusage" & sType2 & "dtl mud INNER JOIN QL_trnreq" & sType2 & "dtl reqd ON reqd.cmpcode=mud.cmpcode AND reqd.req" & sType2 & "dtloid=mud.req" & sType2 & "dtloid WHERE mud.cmpcode=wod3.cmpcode AND reqd.refoid=wod3.wodtl3oid), 0.0) /*- ISNULL((SELECT SUM(usagedtl2qty) FROM QL_trnusagedtl2 mud INNER JOIN QL_trnreqdtl2 reqd ON reqd.cmpcode=mud.cmpcode AND reqd.reqdtl2oid=mud.reqdtl2oid WHERE mud.cmpcode=wod3.cmpcode AND reqd.wodtl3oid=wod3.wodtl3oid), 0.0)*/) AS matqty_kik, wodtl3unitoid AS matunitoid_kik, gendesc AS matunit_kik FROM QL_trnwodtl3 wod3 INNER JOIN QL_trnwodtl2 wod2 ON wod2.cmpcode=wod3.cmpcode AND wod2.wodtl2oid=wod3.wodtl2oid INNER JOIN QL_mst" & sType & " m ON " & sType & "oid=wodtl3refoid INNER JOIN QL_mstgen g ON genoid=wodtl3unitoid WHERE wod3.cmpcode='" & DDLBusUnit.SelectedValue & "' AND wod3.womstoid=" & womstoid.Text & " AND wod2.deptoid=" & deptoid.SelectedValue & " AND wodtl3reftype='" & acarareftype.SelectedValue & "' AND wodtl3reqflag='') AS tbl_MaterialKIK WHERE 1=1 AND " & FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%' ORDER BY matcode_kik"
        FillGV(gvListMat, sSql, "QL_trnwodtl3")
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnBeritaAcara.aspx")
        End If
        If checkPagePermission("~\Transaction\trnBeritaAcara.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Berita Acara"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnSendApproval.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to send this data for approval?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            CheckStatus()
            InitAllDDL()
            Session("IsTransform") = False
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                acaramstoid.Text = GenerateID("QL_TRNBRTACARAMST", CompnyCode)
                acaradate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                acaramststatus.Text = "In Process"
                PeriodAcctg.Text = GetDateToPeriodAcctg(GetServerTime())
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvACRDtl.DataSource = dt
            gvACRDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListMatUsage") Is Nothing And Session("WarningListMatUsage") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMatUsage") Then
                Session("WarningListMatUsage") = Nothing
                mpeListMatUsage.Show()
            End If
        End If
        If Not Session("WarningListMatReturn") Is Nothing And Session("WarningListMatReturn") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMatReturn") Then
                Session("WarningListMatReturn") = Nothing
                mpeListMatReturn.Show()
            End If
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnBeritaAcara.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lkbACRInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbACRInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, acr.updtime, GETDATE()) > " & nDays & " AND acaramststatus='In Process'"
        If checkPagePermission("~\Transaction\trnBeritaAcara.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND acr.createuser='" & Session("UserID") & "' "
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub lkbACRInApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbACRInApproval.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = 0
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 1
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, acr.updtime, GETDATE()) > " & nDays & " AND acaramststatus='In Approval'"
        If checkPagePermission("~\Transaction\trnBeritaAcara.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND acr.createuser='" & Session("UserID") & "' "
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND acaradate>='" & FilterPeriod1.Text & " 00:00:00' AND acaradate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND acaramststatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Transaction\trnBeritaAcara.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND acr.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Transaction\trnBeritaAcara.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND acr.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnPrintHdr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrintHdr.Click
        UpdateCheckedValue()
        PrintReport()
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        btnClearKIK_Click(Nothing, Nothing)
        InitDDLWH()
    End Sub

    Protected Sub btnSearchKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchKIK.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        FilterDDLListKIK.SelectedIndex = -1 : FilterTextListKIK.Text = "" : gvListKIK.SelectedIndex = -1
        BindListKIK()
        cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, True)
    End Sub

    Protected Sub btnClearKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearKIK.Click
        womstoid.Text = "" : wono.Text = "" : deptoid.Items.Clear() : itemdesc.Text = ""
    End Sub

    Protected Sub btnFindListKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListKIK.Click
        BindListKIK()
        mpeListKIK.Show()
    End Sub

    Protected Sub btnAllListKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListKIK.Click
        FilterDDLListKIK.SelectedIndex = -1 : FilterTextListKIK.Text = "" : gvListKIK.SelectedIndex = -1
        BindListKIK()
        mpeListKIK.Show()
    End Sub

    Protected Sub gvListKIK_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListKIK.PageIndexChanging
        gvListKIK.PageIndex = e.NewPageIndex
        BindListKIK()
        mpeListKIK.Show()
    End Sub

    Protected Sub gvListKIK_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListKIK.SelectedIndexChanged
        If womstoid.Text <> gvListKIK.SelectedDataKey.Item("womstoid").ToString Then
            btnClearKIK_Click(Nothing, Nothing)
        End If
        womstoid.Text = gvListKIK.SelectedDataKey.Item("womstoid").ToString
        wono.Text = gvListKIK.SelectedDataKey.Item("wono").ToString
        itemdesc.Text = gvListKIK.SelectedDataKey.Item("wodesc").ToString
        InitDDLDept()
        cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, False)
    End Sub

    Protected Sub lkbCloseListKIK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListKIK.Click
        cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, False)
    End Sub

    Protected Sub acaratype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles acaratype.SelectedIndexChanged
        Dim bVal As Boolean = False, bVal2 As Boolean = False
        If acaratype.SelectedValue = "Material Usage" Then
            bVal = True
        ElseIf acaratype.SelectedValue = "Material Transform" Then
            bVal2 = True
        End If
        FilterDDLListMatUsage.Items(FilterDDLListMatUsage.Items.Count - 1).Enabled = Not bVal : FilterDDLListMatReturn.Items(FilterDDLListMatReturn.Items.Count - 1).Enabled = Not bVal
        pnlBATransform.Visible = bVal2 : gvACRDtl.Columns(gvACRDtl.Columns.Count - 2).Visible = bVal2
        gvACRDtl.Columns(gvACRDtl.Columns.Count - 3).Visible = bVal2 : gvACRDtl.Columns(gvACRDtl.Columns.Count - 4).Visible = bVal2
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        If womstoid.Text = "" Then
            showMessage("Please select KIK No. first!", 2)
            Exit Sub
        End If
        If acaratype.SelectedValue.ToUpper = "MATERIAL USAGE" Or acaratype.SelectedValue.ToUpper = "MATERIAL TRANSFORM" Then
            If acarawhoid.SelectedValue = "" Then
                showMessage("Please select Warehouse first!", 2)
                Exit Sub
            End If
            FilterDDLListMatUsage.SelectedIndex = -1 : FilterTextListMatUsage.Text = "" : InitDDLCat1()
            cbCat03.Checked = False : cbCat04.Checked = False
            Session("TblListMat") = Nothing : Session("TblListMatView") = Nothing : gvListMatUsage.DataSource = Session("TblListMat") : gvListMatUsage.DataBind()
            If acarareftype.SelectedValue = "Log" Or acarareftype.SelectedValue = "Sawn Timber" Then
                FilterTextListMatUsage.TextMode = TextBoxMode.MultiLine : FilterTextListMatUsage.Rows = 3 : FilterDDLListMatUsage.Items(0).Enabled = True
            Else
                FilterTextListMatUsage.TextMode = TextBoxMode.SingleLine : FilterTextListMatUsage.Rows = 0 : FilterDDLListMatUsage.Items(0).Enabled = False
            End If
         
            cProc.SetModalPopUpExtender(btnHideListMatUsage, pnlListMatUsage, mpeListMatUsage, True)
        Else
            If deptoid.SelectedValue = "" Then
                showMessage("Please select Department first!", 2)
                Exit Sub
            End If
            FilterDDLListMatReturn.SelectedIndex = -1 : FilterTextListMatReturn.Text = ""
            Session("TblListMat") = Nothing : Session("TblListMatView") = Nothing : gvListMatReturn.DataSource = Session("TblListMat") : gvListMatReturn.DataBind()
            BindListMatReturn()
        End If
    End Sub

    Protected Sub btnFindListMatUsage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMatUsage.Click
        BindListMatUsage()
        If Session("TblListMat").Rows.Count <= 0 Then
            Session("WarningListMatUsage") = "Material data can't be found!"
            showMessage(Session("WarningListMatUsage"), 2)
            Exit Sub
        End If
        mpeListMatUsage.Show()
        'UpdateCheckedMatUsage()
        'If Session("TblListMat") IsNot Nothing Then
        '    Dim dt As DataTable = Session("TblListMat")
        '    If dt.Rows.Count > 0 Then
        '        Dim dv As DataView = dt.DefaultView
        '        Dim sFilter As String = ""
        '        If acarareftype.SelectedIndex > 2 Then
        '            If FilterDDLListMatUsage.SelectedIndex = 0 Then
        '                Dim sText() As String = FilterTextListMatUsage.Text.Split(";")
        '                For C1 As Integer = 0 To sText.Length - 1
        '                    If sText(C1) <> "" Then
        '                        sFilter &= FilterDDLListMatUsage.SelectedValue & " LIKE '" & Tchar(sText(C1)) & "' OR "
        '                    End If
        '                Next
        '                If sFilter <> "" Then
        '                    sFilter = "(" & Left(sFilter, sFilter.Length - 4) & ")"
        '                Else
        '                    sFilter = "1=1"
        '                End If
        '            Else
        '                sFilter = FilterDDLListMatUsage.SelectedValue & " LIKE '%" & Tchar(FilterTextListMatUsage.Text) & "%'"
        '            End If
        '        Else
        '            sFilter = FilterDDLListMatUsage.SelectedValue & " LIKE '%" & Tchar(FilterTextListMatUsage.Text) & "%'"
        '        End If
        '        If cbCat04.Checked And DDLCat04.SelectedValue <> "" Then
        '            sFilter &= " AND matcode LIKE '" & DDLCat04.SelectedValue & "%'"
        '        Else
        '            If cbCat03.Checked And DDLCat03.SelectedValue <> "" Then
        '                sFilter &= " AND matcode LIKE '" & DDLCat03.SelectedValue & "%'"
        '            Else
        '                If cbCat02.Checked And DDLCat02.SelectedValue <> "" Then
        '                    sFilter &= " AND matcode LIKE '" & DDLCat02.SelectedValue & "%'"
        '                Else
        '                    If cbCat01.Checked And DDLCat01.SelectedValue <> "" Then
        '                        sFilter &= " AND matcode LIKE '" & DDLCat01.SelectedValue & "%'"
        '                    End If
        '                End If
        '            End If
        '        End If
        '        dv.RowFilter = sFilter
        '        Session("TblListMatView") = dv.ToTable
        '        gvListMatUsage.DataSource = Session("TblListMatView")
        '        gvListMatUsage.DataBind()
        '        dv.RowFilter = ""
        '        mpeListMatUsage.Show()
        '    Else
        '        Session("WarningListMatUsage") = "Material data can't be found!"
        '        showMessage(Session("WarningListMatUsage"), 2)
        '    End If
        'Else
        '    Session("WarningListMatUsage") = "Material data can't be found!"
        '    showMessage(Session("WarningListMatUsage"), 2)
        'End If
    End Sub

    Protected Sub btnAllListMatUsage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMatUsage.Click
        If Session("TblListMat") Is Nothing Then
            BindListMatUsage()
            If Session("TblListMat").Rows.Count <= 0 Then
                Session("WarningListMatUsage") = "Material data can't be found!"
                showMessage(Session("WarningListMatUsage"), 2)
                Exit Sub
            End If
        End If
        UpdateCheckedMatUsage()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                FilterDDLListMatUsage.SelectedIndex = -1 : FilterTextListMatUsage.Text = "" : InitDDLCat1()
                cbCat01.Checked = False : cbCat02.Checked = False : cbCat03.Checked = False : cbCat04.Checked = False
                Session("TblListMatView") = Session("TblListMat")
                gvListMatUsage.DataSource = Session("TblListMatView")
                gvListMatUsage.DataBind()
                mpeListMatUsage.Show()
            Else
                Session("WarningListMatUsage") = "Material data can't be found!"
                showMessage(Session("WarningListMatUsage"), 2)
            End If
        Else
            Session("WarningListMatUsage") = "Material data can't be found!"
            showMessage(Session("WarningListMatUsage"), 2)
        End If
    End Sub

    Protected Sub DDLCat01_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLCat01.SelectedIndexChanged
        InitDDLCat2()
        mpeListMatUsage.Show()
    End Sub

    Protected Sub DDLCat02_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLCat02.SelectedIndexChanged
        InitDDLCat3()
        mpeListMatUsage.Show()
    End Sub

    Protected Sub DDLCat03_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLCat03.SelectedIndexChanged
        InitDDLCat4()
        mpeListMatUsage.Show()
    End Sub

    Protected Sub gvListMatUsage_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMatUsage.PageIndexChanging
        UpdateCheckedMatUsage()
        gvListMatUsage.PageIndex = e.NewPageIndex
        gvListMatUsage.DataSource = Session("TblListMatView")
        gvListMatUsage.DataBind()
        mpeListMatUsage.Show()
    End Sub

    Protected Sub gvListMatUsage_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMatUsage.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(4).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 4)
                    End If
                End If
            Next

            Dim iItemOid As Integer = 0
            cc = e.Row.Cells(0).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    iItemOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                End If
            Next
            cc = e.Row.Cells(5).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                    FillDDL(CType(myControl, System.Web.UI.WebControls.DropDownList), "select itemUnit1, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit1 where itemoid=" & iItemOid & " union all select itemUnit2, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit2 where itemoid=" & iItemOid & " union all select itemUnit3, gendesc from QL_mstitem inner join QL_mstgen g on genoid=itemUnit3 where itemoid=" & iItemOid & "")
                    CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue = CType(myControl, System.Web.UI.WebControls.DropDownList).ToolTip
                End If
            Next
        End If
    End Sub

    Protected Sub cbHdrLM_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For C1 As Integer = 0 To gvListMatUsage.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMatUsage.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        mpeListMatUsage.Show()
    End Sub

    Protected Sub lbAddToListMatUsage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListMatUsage.Click
        UpdateCheckedMatUsage()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "checkvalue='True'"
                If dv.Count > 0 Then
                    Dim sErr As String = ""
                    For C1 As Integer = 0 To dv.Count - 1
                        If ToDouble(dv(C1)("matqty").ToString) <= 0 Then
                            sErr = "Every Qty of selected data must be more than 0!" : Exit For
                        End If
                        'If ToDouble(dv(C1)("matqty").ToString) > ToDouble(dv(C1)("maxqty").ToString) Then
                        '    sErr = "Every Qty of selected data must be less than Max Qty!" : Exit For
                        'End If
                    Next
                    If sErr <> "" Then
                        Session("WarningListMatUsage") = sErr
                        showMessage(Session("WarningListMatUsage"), 2) : Exit Sub
                    End If
                    If Session("TblDtl") Is Nothing Then
                        CreateTblDetail()
                    End If
                    Dim objTbl As DataTable = Session("TblDtl")
                    Dim iSeq As Integer = objTbl.Rows.Count + 1
                    Dim objView As DataView = objTbl.DefaultView
                    Dim dQty_unitkecil, dQty_unitbesar As Double
                    For C1 As Integer = 0 To dv.Count - 1
                        objView.RowFilter = "acarareftype='" & acarareftype.SelectedValue & "' AND acararefoid=" & dv(C1)("matoid")
                        If objView.Count > 0 Then
                            GetUnitConverter(dv(C1)("matoid"), dv(C1)("selectedunitoid"), ToDouble(dv(C1)("matqty")), dQty_unitkecil, dQty_unitbesar)
                            objView(0)("acaraqty") = ToDouble(dv(C1)("matqty").ToString)
                            objView(0)("acaravalueidr") = ToDouble(dv(C1)("matvalueidr").ToString)
                            objView(0)("acaravalueusd") = ToDouble(dv(C1)("matvalueusd").ToString)
                            objView(0)("wodtl2oid") = dv(C1)("wodtl2oid")
                            objView(0)("wodtl3oid") = dv(C1)("wodtl3oid")
                            objView(0)("matoid_kik") = dv(C1)("matoid_kik")
                            objView(0)("matcode_kik") = dv(C1)("matcode_kik").ToString
                            objView(0)("matlongdesc_kik") = dv(C1)("matlongdesc_kik").ToString
                            objView(0)("matqty_kik") = ToDouble(dv(C1)("matqty_kik").ToString)
                            objView(0)("matunitoid_kik") = dv(C1)("matunitoid_kik")
                            objView(0)("acaraqty_unitkecil") = dQty_unitkecil
                            objView(0)("acaraqty_unitbesar") = dQty_unitbesar
                        Else
                            Dim rv As DataRowView = objView.AddNew()
                            rv.BeginEdit()
                            rv("acaradtlseq") = iSeq
                            rv("acarareftype") = acarareftype.SelectedValue
                            rv("acararefoid") = dv(C1)("matoid").ToString
                            rv("acararefcode") = dv(C1)("matcode").ToString
                            rv("acararefoldcode") = dv(C1)("oldcode").ToString
                            rv("acarareflongdesc") = dv(C1)("matlongdesc").ToString
                            rv("acaraqty") = ToDouble(dv(C1)("matqty").ToString)
                            rv("acaraunitoid") = dv(C1)("selectedunitoid").ToString
                            rv("stockunitoid") = dv(C1)("stockunitoid").ToString
                            rv("acaraunit") = dv(C1)("selectedunit").ToString
                            rv("maxqty") = ToDouble(dv(C1)("maxqty").ToString)
                            rv("acaradtlnote") = ""
                            rv("acaravalueidr") = ToDouble(dv(C1)("matvalueidr").ToString)
                            rv("acaravalueusd") = ToDouble(dv(C1)("matvalueusd").ToString)
                            rv("wodtl2oid") = dv(C1)("wodtl2oid")
                            rv("wodtl3oid") = dv(C1)("wodtl3oid")
                            rv("matoid_kik") = dv(C1)("matoid_kik")
                            rv("matcode_kik") = dv(C1)("matcode_kik").ToString
                            rv("matoldcode_kik") = dv(C1)("matoldcode_kik").ToString
                            rv("matlongdesc_kik") = dv(C1)("matlongdesc_kik").ToString
                            rv("matqty_kik") = ToDouble(dv(C1)("matqty_kik").ToString)
                            rv("matunitoid_kik") = dv(C1)("matunitoid_kik")
                            GetUnitConverter(dv(C1)("matoid"), dv(C1)("selectedunitoid"), ToDouble(dv(C1)("matqty")), dQty_unitkecil, dQty_unitbesar)
                            rv("acaraqty_unitkecil") = dQty_unitkecil
                            rv("acaraqty_unitbesar") = dQty_unitbesar
                            rv.EndEdit()
                            iSeq += 1
                        End If
                        objView.RowFilter = ""
                    Next
                    objTbl.AcceptChanges()
                    Session("TblDtl") = objTbl
                    gvACRDtl.DataSource = Session("TblDtl")
                    gvACRDtl.DataBind()
                    If cbOpenListMatUsage.Checked Then
                        btnSearchMat_Click(Nothing, Nothing)
                    Else
                        ClearDetail()
                        cProc.SetModalPopUpExtender(btnHideListMatUsage, pnlListMatUsage, mpeListMatUsage, False)
                    End If
                Else
                    Session("WarningListMatUsage") = "Please select material to add to list!"
                    showMessage(Session("WarningListMatUsage"), 2)
                End If
            Else
                Session("WarningListMatUsage") = "Material data can't be found!"
                showMessage(Session("WarningListMatUsage"), 2)
            End If
        Else
            Session("WarningListMatUsage") = "Material data can't be found!"
            showMessage(Session("WarningListMatUsage"), 2)
        End If
    End Sub

    Protected Sub lbSelectAllToList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSelectAllToList.Click
        If Session("TblListMatView") IsNot Nothing Then
            Dim dtTbl As DataTable = Session("TblListMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblListMat")
                Dim objView As DataView = objTbl.DefaultView
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matoid=" & dtTbl.Rows(C1)("matoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblListMat") = objTbl
                Session("TblListMatView") = dtTbl
                gvListMatUsage.DataSource = Session("TblListMatView")
                gvListMatUsage.DataBind()
                lbAddToListMatUsage_Click(Nothing, Nothing)
            Else
                Session("WarningListMatUsage") = "Please show material data first!"
                showMessage(Session("WarningListMatUsage"), 2)
            End If
        Else
            Session("WarningListMatUsage") = "Please show material data first!"
            showMessage(Session("WarningListMatUsage"), 2)
        End If
    End Sub

    Protected Sub lbCloseListMatUsage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMatUsage.Click
        cProc.SetModalPopUpExtender(btnHideListMatUsage, pnlListMatUsage, mpeListMatUsage, False)
    End Sub

    Protected Sub btnFindListMatReturn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMatReturn.Click
        UpdateCheckedMatReturn()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                Dim dv As DataView = dt.DefaultView
                Dim sFilter As String = FilterDDLListMatReturn.SelectedValue & " LIKE '%" & Tchar(FilterTextListMatReturn.Text) & "%'"
                dv.RowFilter = sFilter
                Session("TblListMatView") = dv.ToTable
                gvListMatReturn.DataSource = Session("TblListMatView")
                gvListMatReturn.DataBind()
                dv.RowFilter = ""
                mpeListMatReturn.Show()
            Else
                Session("WarningListMatReturn") = "Material data can't be found!"
                showMessage(Session("WarningListMatReturn"), 2)
            End If
        Else
            Session("WarningListMatReturn") = "Material data can't be found!"
            showMessage(Session("WarningListMatReturn"), 2)
        End If
    End Sub

    Protected Sub btnAllListMatReturn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMatReturn.Click
        UpdateCheckedMatReturn()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                FilterDDLListMatReturn.SelectedIndex = -1 : FilterTextListMatReturn.Text = ""
                Session("TblListMatView") = Session("TblListMat")
                gvListMatReturn.DataSource = Session("TblListMatView")
                gvListMatReturn.DataBind()
                mpeListMatReturn.Show()
            Else
                Session("WarningListMatReturn") = "Material data can't be found!"
                showMessage(Session("WarningListMatReturn"), 2)
            End If
        Else
            Session("WarningListMatReturn") = "Material data can't be found!"
            showMessage(Session("WarningListMatReturn"), 2)
        End If
    End Sub

    Protected Sub gvListMatReturn_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMatReturn.PageIndexChanging
        UpdateCheckedMatReturn()
        gvListMatReturn.PageIndex = e.NewPageIndex
        gvListMatReturn.DataSource = Session("TblListMatView")
        gvListMatReturn.DataBind()
        mpeListMatReturn.Show()
    End Sub

    Protected Sub gvListMatReturn_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMatReturn.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(3).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 4)
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub cbHdrListMat_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For C1 As Integer = 0 To gvListMatReturn.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMatReturn.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        mpeListMatReturn.Show()
    End Sub

    Protected Sub lbAddToListMatReturn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListMatReturn.Click
        UpdateCheckedMatReturn()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "checkvalue='True'"
                If dv.Count > 0 Then
                    Dim sErr As String = ""
                    For C1 As Integer = 0 To dv.Count - 1
                        If ToDouble(dv(C1)("matqty").ToString) <= 0 Then
                            sErr = "Every Qty of selected data must be more than 0!" : Exit For
                        End If
                        If ToDouble(dv(C1)("matqty").ToString) > ToDouble(dv(C1)("maxqty").ToString) Then
                            sErr = "Every Qty of selected data must be less than Max Qty!" : Exit For
                        End If
                    Next
                    If sErr <> "" Then
                        Session("WarningListMatReturn") = sErr
                        showMessage(Session("WarningListMatReturn"), 2) : Exit Sub
                    End If
                    If Session("TblDtl") Is Nothing Then
                        CreateTblDetail()
                    End If
                    Dim objTbl As DataTable = Session("TblDtl")
                    Dim iSeq As Integer = objTbl.Rows.Count + 1
                    Dim dQty_unitkecil, dQty_unitbesar As Double
                    Dim objView As DataView = objTbl.DefaultView
                    For C1 As Integer = 0 To dv.Count - 1
                        objView.RowFilter = "acarareftype='" & acarareftype.SelectedValue & "' AND acararefoid=" & dv(C1)("matoid")
                        If objView.Count > 0 Then
                            objView(0)("acaraqty") = ToDouble(dv(C1)("matqty").ToString)
                            objView(0)("acaraunitoid") = ToDouble(dv(C1)("matunitoid").ToString)
                            objView(0)("acaraunit") = ToDouble(dv(C1)("matunit").ToString)
                            objView(0)("acaravalueidr") = ToDouble(dv(C1)("matvalueidr").ToString)
                            objView(0)("acaravalueusd") = ToDouble(dv(C1)("matvalueusd").ToString)
                            GetUnitConverter(dv(C1)("matoid"), dv(C1)("matunitoid"), ToDouble(dv(C1)("matqty")), dQty_unitkecil, dQty_unitbesar)
                            objView(0)("acaraqty_unitkecil") = dQty_unitkecil
                            objView(0)("acaraqty_unitbesar") = dQty_unitbesar
                        Else
                            Dim rv As DataRowView = objView.AddNew()
                            rv.BeginEdit()
                            rv("acaradtlseq") = iSeq
                            rv("acarareftype") = acarareftype.SelectedValue
                            rv("acararefoid") = dv(C1)("matoid").ToString
                            rv("acararefcode") = dv(C1)("matcode").ToString
                            rv("acarareflongdesc") = dv(C1)("matlongdesc").ToString
                            rv("acaraqty") = ToDouble(dv(C1)("matqty").ToString)
                            rv("acaraunitoid") = dv(C1)("matunitoid").ToString
                            rv("acaraunit") = dv(C1)("matunit").ToString
                            rv("maxqty") = ToDouble(dv(C1)("maxqty").ToString)
                            rv("acaradtlnote") = ""
                            rv("acaravalueidr") = ToDouble(dv(C1)("matvalueidr").ToString)
                            rv("acaravalueusd") = ToDouble(dv(C1)("matvalueusd").ToString)
                            GetUnitConverter(dv(C1)("matoid"), dv(C1)("matunitoid"), ToDouble(dv(C1)("matqty")), dQty_unitkecil, dQty_unitbesar)
                            rv("acaraqty_unitkecil") = dQty_unitkecil
                            rv("acaraqty_unitbesar") = dQty_unitbesar
                            rv.EndEdit()
                            iSeq += 1
                        End If
                        objView.RowFilter = ""
                    Next
                    objTbl.AcceptChanges()
                    Session("TblDtl") = objTbl
                    gvACRDtl.DataSource = Session("TblDtl")
                    gvACRDtl.DataBind()
                    ClearDetail()
                    cProc.SetModalPopUpExtender(btnHideListMatReturn, pnlListMatReturn, mpeListMatReturn, False)
                Else
                    Session("WarningListMatReturn") = "Please select material to add to list!"
                    showMessage(Session("WarningListMatReturn"), 2)
                End If
            Else
                Session("WarningListMatReturn") = "Material data can't be found!"
                showMessage(Session("WarningListMatReturn"), 2)
            End If
        Else
            Session("WarningListMatReturn") = "Material data can't be found!"
            showMessage(Session("WarningListMatReturn"), 2)
        End If
    End Sub

    Protected Sub lbCloseListMatReturn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMatReturn.Click
        cProc.SetModalPopUpExtender(btnHideListMatReturn, pnlListMatReturn, mpeListMatReturn, False)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            Dim dQty_unitkecil, dQty_unitbesar As Double
            Dim objTable As DataTable = Session("TblDtl")
            Dim objRow As DataRow
            objRow = objTable.Rows(acaradtlseq.Text - 1)
            objRow.BeginEdit()
            objRow("acaraqty") = ToDouble(acaraqty.Text)
            objRow("acaradtlnote") = acaradtlnote.Text
            objRow("acaravalueidr") = ToDouble(acaravalueidr.Text)
            objRow("acaravalueusd") = ToDouble(acaravalueusd.Text)
            objRow("wodtl2oid") = wodtl2oid.Text
            objRow("wodtl3oid") = wodtl3oid.Text
            GetUnitConverter(acararefoid.Text, acaraunitoid.SelectedValue, ToDouble(acaraqty.Text), dQty_unitkecil, dQty_unitbesar)
            objRow("acaraqty_unitkecil") = dQty_unitkecil
            objRow("acaraqty_unitbesar") = dQty_unitbesar
        
            objRow.EndEdit()
            Session("TblDtl") = objTable
            gvACRDtl.DataSource = objTable
            gvACRDtl.DataBind()
            ClearDetail()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvACRDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvACRDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
        End If
    End Sub

    Protected Sub gvACRDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvACRDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("acaradtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvACRDtl.DataSource = objTable
        gvACRDtl.DataBind()
        ClearDetail()
    End Sub

    Protected Sub gvACRDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvACRDtl.SelectedIndexChanged
        Try
            acaradtlseq.Text = gvACRDtl.SelectedDataKey.Item("acaradtlseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "acaradtlseq=" & acaradtlseq.Text
                acarareftype.SelectedValue = dv.Item(0).Item("acarareftype").ToString
                acararefoid.Text = dv.Item(0).Item("acararefoid").ToString
                acararefcode.Text = dv.Item(0).Item("acararefcode").ToString
                matoldcode.Text = dv.Item(0).Item("acararefoldcode").ToString
                acarareflongdesc.Text = dv.Item(0).Item("acarareflongdesc").ToString
                acaraqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("acaraqty").ToString), 4)
                acaraunitoid.SelectedValue = dv.Item(0).Item("acaraunitoid").ToString
                stockunitoid.SelectedValue = dv.Item(0).Item("stockunitoid").ToString
                maxqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("maxqty").ToString), 4)
                acaradtlnote.Text = dv.Item(0).Item("acaradtlnote").ToString
                acaravalueidr.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("acaravalueidr").ToString), 4)
                acaravalueusd.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("acaravalueusd").ToString), 4)
                acarareftype.Enabled = False : acarareftype.CssClass = "inpTextDisabled" : btnSearchMat.Visible = False
                wodtl2oid.Text = dv.Item(0).Item("wodtl2oid").ToString
                wodtl3oid.Text = dv.Item(0).Item("wodtl3oid").ToString
                'matoid_kik.Text = dv.Item(0).Item("matoid_kik").ToString
                'matcode_kik.Text = dv.Item(0).Item("matcode_kik").ToString
                'oldcode_kik.Text = dv.Item(0).Item("matoldcode_kik").ToString
                'matlongdesc_kik.Text = dv.Item(0).Item("matlongdesc_kik").ToString
                'matqty_kik.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("matqty_kik").ToString), 4)
                'matunitoid_kik.SelectedValue = dv.Item(0).Item("matunitoid_kik").ToString
                If acarareftype.SelectedValue = "Log" Or acarareftype.SelectedValue = "Sawn Timber" Then
                    acaraqty.Enabled = False : acaraqty.CssClass = "inpTextDisabled"
                End If
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_trnbrtacaramst WHERE acaramstoid=" & acaramstoid.Text
                If CheckDataExists(sSql) Then
                    acaramstoid.Text = GenerateID("QL_TRNBRTACARAMST", CompnyCode)
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_trnbrtacaramst", "acaramstoid", acaramstoid.Text, "acaramststatus", updtime.Text, "Approved")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    acaramststatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            acaradtloid.Text = GenerateID("QL_TRNBRTACARADTL", CompnyCode)
            Dim iAppOid As Integer = GenerateID("QL_APPROVAL", CompnyCode)
            PeriodAcctg.Text = GetDateToPeriodAcctg(CDate(acaradate.Text))
            If acaramststatus.Text = "Revised" Then
                acaramststatus.Text = "In Process"
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trnbrtacaramst (cmpcode, acaramstoid, periodacctg, acarano, acaradate, deptoid, womstoid, acaratype, acarawhoid, acaramstnote, acaramststatus, createuser, createtime, upduser, updtime) VALUES ('" & DDLBusUnit.SelectedValue & "', " & acaramstoid.Text & ", '" & PeriodAcctg.Text & "', '" & acarano.Text & "', '" & acaradate.Text & "', " & deptoid.SelectedValue & ", " & womstoid.Text & ", '" & acaratype.SelectedValue & "', " & acarawhoid.SelectedValue & ", '" & Tchar(acaramstnote.Text) & "', '" & acaramststatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & acaramstoid.Text & " WHERE tablename='QL_TRNBRTACARAMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trnbrtacaramst SET periodacctg='" & PeriodAcctg.Text & "', acaradate='" & acaradate.Text & "', acarano='" & acarano.Text & "', deptoid=" & deptoid.SelectedValue & ", womstoid=" & womstoid.Text & ", acaratype='" & acaratype.SelectedValue & "', acarawhoid=" & acarawhoid.SelectedValue & ", acaramstnote='" & Tchar(acaramstnote.Text) & "', acaramststatus='" & acaramststatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE acaramstoid=" & acaramstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnwodtl3 SET wodtl3reqflag='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl3oid IN (SELECT wodtl3oid FROM QL_trnbrtacaradtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND acaramstoid=" & acaramstoid.Text & " AND wodtl3oid<>0)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnwodtl2 SET wodtl2reqflag='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl2oid IN (SELECT wodtl2oid FROM QL_trnbrtacaradtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND acaramstoid=" & acaramstoid.Text & " AND wodtl2oid<>0)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trnbrtacaradtl WHERE acaramstoid=" & acaramstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trnbrtacaradtl (cmpcode, acaradtloid, acaramstoid, acaradtlseq, acarareftype, acararefoid, acaraqty, acaraunitoid, acaravalueidr, acaravalueusd, acaradtlnote, acaradtlstatus, upduser, updtime, wodtl2oid, wodtl3oid, matoid_kik, matqty_kik, matunitoid_kik, acaraqty_unitkecil, acaraqty_unitbesar) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C1 + CInt(acaradtloid.Text)) & ", " & acaramstoid.Text & ", " & C1 + 1 & ", '" & objTable.Rows(C1).Item("acarareftype") & "', " & objTable.Rows(C1).Item("acararefoid") & ", " & ToDouble(objTable.Rows(C1).Item("acaraqty").ToString) & ", " & objTable.Rows(C1).Item("acaraunitoid") & ", " & ToDouble(objTable.Rows(C1).Item("acaravalueidr").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("acaravalueusd").ToString) & ", '" & Tchar(objTable.Rows(C1).Item("acaradtlnote").ToString) & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToInteger(objTable.Rows(C1).Item("wodtl2oid").ToString) & ", " & ToInteger(objTable.Rows(C1).Item("wodtl3oid").ToString) & ", " & ToInteger(objTable.Rows(C1).Item("matoid_kik").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("matqty_kik").ToString) & ", " & ToInteger(objTable.Rows(C1).Item("matunitoid_kik").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("acaraqty_unitkecil").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("acaraqty_unitbesar").ToString) & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        If ToInteger(objTable.Rows(C1).Item("wodtl3oid").ToString) <> 0 Then
                            sSql = "UPDATE QL_trnwodtl3 SET wodtl3reqflag='Complete' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl3oid=" & objTable.Rows(C1).Item("wodtl3oid")
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_trnwodtl2 SET wodtl2reqflag='Complete' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl2oid=" & objTable.Rows(C1).Item("wodtl2oid") & " AND (SELECT COUNT(*) FROM QL_trnwodtl3 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl2oid=" & objTable.Rows(C1).Item("wodtl2oid") & " AND wodtl3oid<>" & objTable.Rows(C1).Item("wodtl3oid") & " AND wodtl3reqflag='') + (SELECT COUNT(*) FROM QL_trnwodtl4 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl2oid=" & objTable.Rows(C1).Item("wodtl2oid") & " AND wodtl4reqflag='')=0"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(acaradtloid.Text)) & " WHERE tablename='QL_TRNBRTACARADTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If acaramststatus.Text = "In Approval" Then
                    Dim dt As DataTable = Session("AppPerson")
                    For C1 As Integer = 0 To dt.Rows.Count - 1
                        sSql = "INSERT INTO QL_approval (cmpcode, approvaloid, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvalnote) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iAppOid + C1 & ", 'BA" & acaramstoid.Text & "_" & (iAppOid + C1) & "', '" & Session("UserID") & "', '" & acaradate.Text & "', 'New', 'QL_trnbrtacaramst', " & acaramstoid.Text & ", 'In Approval', '0', '" & dt.Rows(C1)("approvaluser").ToString & "', '1/1/1900', '')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                    sSql = "UPDATE QL_mstOid SET lastoid=" & iAppOid + dt.Rows.Count - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_APPROVAL'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        acaramststatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    acaramststatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 2)
                conn.Close()
                acaramststatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & acaramstoid.Text & ".<BR>"
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trnBeritaAcara.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnBeritaAcara.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If acaramstoid.Text.Trim = "" Then
            showMessage("Please select Berita Acara data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_trnbrtacaramst", "acaramstoid", acaramstoid.Text, "acaramststatus", updtime.Text, "Approved")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                acaramststatus.Text = "In Process"
                Exit Sub
            End If
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trnwodtl3 SET wodtl3reqflag='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl3oid IN (SELECT wodtl3oid FROM QL_trnbrtacaradtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND acaramstoid=" & acaramstoid.Text & " AND wodtl3oid<>0)"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_trnwodtl2 SET wodtl2reqflag='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl2oid IN (SELECT wodtl2oid FROM QL_trnbrtacaradtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND acaramstoid=" & acaramstoid.Text & " AND wodtl2oid<>0)"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnbrtacaradtl WHERE acaramstoid=" & acaramstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnbrtacaramst WHERE acaramstoid=" & acaramstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnBeritaAcara.aspx?awal=true")
    End Sub

    Protected Sub btnSendApproval_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSendApproval.Click
        sSql = "SELECT approvaluser, apppersontype FROM QL_approvalperson WHERE tablename='QL_trnbrtacaramst' AND activeflag='ACTIVE' AND cmpcode IN ('" & CompnyCode & "', '" & DDLBusUnit.SelectedValue & "')"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_approvalperson")
        If dt.Rows.Count = 0 Then
            showMessage("There is no user can't approve this transaction. Please define approval user in <STRONG>APPROVAL USER</STRONG> form.", 2)
        Else
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "apppersontype='Final'"
            If dv.Count = 0 Then
                showMessage("Final approval user is not defined for this transaction. Please define final approval user in <STRONG>APPROVAL USER</STRONG> form.", 2)
                dv.RowFilter = ""
            Else
                dv.RowFilter = ""
                Session("AppPerson") = dt
                acaramststatus.Text = "In Approval"
                btnSave_Click(Nothing, Nothing)
            End If
        End If
    End Sub

    Protected Sub btnSearchMatKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        UpdateCheckedMatUsage()
        mpeListMatUsage.Hide()
        hfMatID.Value = sender.CommandArgument
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : gvListMat.SelectedIndex = -1
        BindListMatKIK()
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Protected Sub btnClearMatKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        hfMatID.Value = sender.CommandArgument
        Dim dt As DataTable = Session("TblListMatView")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = "matoid=" & hfMatID.Value
        If dv.Count = 1 Then
            dv(0)("wodtl3oid") = 0
            dv(0)("matoid_kik") = 0
            dv(0)("matcode_kik") = ""
            dv(0)("matlongdesc_kik") = ""
            dv(0)("matqty_kik") = 0
            dv(0)("matunitoid_kik") = 0
        End If
        dv.RowFilter = ""
        hfMatID.Value = ""
        gvListMatUsage.DataSource = Session("TblListMatView")
        gvListMatUsage.DataBind()
        mpeListMatUsage.Show()
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        BindListMatKIK()
        mpeListMat.Show()
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : gvListMat.SelectedIndex = -1
        BindListMatKIK()
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        gvListMat.PageIndex = e.NewPageIndex
        BindListMatKIK()
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
        End If
    End Sub

    Protected Sub gvListMat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListMat.SelectedIndexChanged
        If hfMatID.Value = "" Then
            wodtl2oid.Text = gvListMat.SelectedDataKey.Item("wodtl2oid").ToString
            wodtl3oid.Text = gvListMat.SelectedDataKey.Item("wodtl3oid").ToString
            matoid_kik.Text = gvListMat.SelectedDataKey.Item("matoid_kik").ToString
            matcode_kik.Text = gvListMat.SelectedDataKey.Item("matcode_kik").ToString
            matlongdesc_kik.Text = gvListMat.SelectedDataKey.Item("matlongdesc_kik").ToString
            matqty_kik.Text = ToMaskEdit(ToDouble(gvListMat.SelectedDataKey.Item("matqty_kik").ToString), 4)
            matunitoid_kik.SelectedValue = gvListMat.SelectedDataKey.Item("matunitoid_kik").ToString
            itemcat1.Text = gvListMat.SelectedDataKey.Item("kik_cat1oid").ToString
            itemcat2.Text = gvListMat.SelectedDataKey.Item("kik_cat2oid").ToString
            itemcat3.Text = gvListMat.SelectedDataKey.Item("kik_cat3oid").ToString
            btnSearchMat.Visible = True
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
        Else
            Dim dt As DataTable = Session("TblListMatView")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "matoid=" & hfMatID.Value
            If dv.Count = 1 Then
                dv(0)("wodtl2oid") = ToInteger(gvListMat.SelectedDataKey.Item("wodtl2oid").ToString)
                dv(0)("wodtl3oid") = ToInteger(gvListMat.SelectedDataKey.Item("wodtl3oid").ToString)
                dv(0)("matoid_kik") = ToInteger(gvListMat.SelectedDataKey.Item("matoid_kik").ToString)
                dv(0)("matcode_kik") = gvListMat.SelectedDataKey.Item("matcode_kik").ToString
                dv(0)("matlongdesc_kik") = gvListMat.SelectedDataKey.Item("matlongdesc_kik").ToString
                dv(0)("matqty_kik") = ToDouble(gvListMat.SelectedDataKey.Item("matqty_kik").ToString)
                dv(0)("matunitoid_kik") = ToInteger(gvListMat.SelectedDataKey.Item("matunitoid_kik").ToString)
                dv(0)("kik_cat1oid") = ToInteger(gvListMat.SelectedDataKey.Item("kik_cat1oid").ToString)
                dv(0)("kik_cat2oid") = ToInteger(gvListMat.SelectedDataKey.Item("kik_cat2oid").ToString)
                dv(0)("kik_cat3oid") = ToInteger(gvListMat.SelectedDataKey.Item("kik_cat3oid").ToString)
                dv(0)("matoldcode_kik") = gvListMat.SelectedDataKey.Item("matoldcode_kik").ToString
            End If
            dv.RowFilter = ""
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            hfMatID.Value = ""
            gvListMatUsage.DataSource = Session("TblListMatView")
            gvListMatUsage.DataBind()
            mpeListMatUsage.Show()
        End If
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
        If hfMatID.Value <> "" Then
            hfMatID.Value = ""
            mpeListMatUsage.Show()
        End If
    End Sub

    Protected Sub btnSearchMatKIK_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMatKIK.Click
        If womstoid.Text = "" Then
            showMessage("Please select KIK No. first!", 2)
            Exit Sub
        End If
        hfMatID.Value = ""
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = "" : gvListMat.SelectedIndex = -1
        BindListMatKIK()
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Protected Sub btnCleatMatKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCleatMatKIK.Click
        hfMatID.Value = "" : wodtl2oid.Text = "" : wodtl3oid.Text = "" : matoid_kik.Text = "" : matcode_kik.Text = "" : matlongdesc_kik.Text = "" : matqty_kik.Text = "" : matunitoid_kik.SelectedIndex = -1
        itemcat1.Text = "" : itemcat2.Text = "" : itemcat3.Text = ""
    End Sub
#End Region

End Class
