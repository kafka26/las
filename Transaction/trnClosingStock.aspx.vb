Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_ClosingStock
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public StartPeriod As String = ConfigurationSettings.AppSettings("StartPeriod")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsPeriodClosed(ByVal sPeriod As String) As Boolean
        sSql = "SELECT COUNT(*) FROM QL_crdstock WHERE cmpcode='" & FilterDDLBusUnit.SelectedValue & "' AND periodacctg='" & sPeriod & "' AND closingdate<>'01/01/1900'"
        If ToDouble(GetStrData(sSql).ToString) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function GetTrnCount(ByVal sTbl As String, ByVal sCol As String, ByVal sStatus As String, ByVal sOther As String) As String
        Dim sReturn As String = "0"
        Try
            sReturn = GetStrData("SELECT COUNT(*) FROM " & sTbl & " WHERE cmpcode='" & FilterDDLBusUnit.SelectedValue & "' AND " & sCol & "='" & sStatus & "'" & sOther & "").ToString
        Catch ex As Exception
            sReturn = "0"
        End Try
        Return sReturn
    End Function

    Private Function GetHppPercentage(ByVal sOid As String) As Double
        Dim dReturn As Double = 0
        Try
            dReturn = ToDouble(GetStrData("SELECT TOP 1 gendesc FROM QL_mstgen WHERE activeflag='ACTIVE' AND genother1='" & sOid & "' AND gengroup='HPP PERCENTAGE (%)' ORDER BY updtime DESC"))
        Catch ex As Exception
            dReturn = 0
        End Try
        Return dReturn
    End Function

    Private Function GetBackupValueIDR(ByVal sCmpCode As String, ByVal iRefOid As Integer, ByVal sPeriodAcctg As String) As Double
        Return ToDouble(GetStrData("SELECT TOP 1 valueidr FROM QL_constock WHERE cmpcode='" & sCmpCode & "' AND refoid=" & iRefOid & " AND valueidr>0 AND periodacctg<='" & sPeriodAcctg & "' ORDER BY updtime DESC"))
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub showConfirmation(ByVal sMessage As String)
        Dim strCaption As String = CompnyName
        lblCaptionConfirm.Text = strCaption & " - CONFIRMATION" : lblPopUpConfirm.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpConfirm, pnlPopUpConfirm, mpePopUpConfirm, True)
    End Sub

    Private Sub InitDDL()
        ' Init DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE' "
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(FilterDDLBusUnit, sSql)
    End Sub

    Private Sub GetLastClose()
        ' Get Current Stock Period need to be Closed
        DDLPeriod.Items.Clear() : imbClose.Visible = True
        DDLPeriod.Enabled = True : DDLPeriod.CssClass = "inpText"

        Dim sLastClosePeriod As String = ""
        sSql = "SELECT MAX(periodacctg) FROM QL_crdstock WHERE cmpcode='" & FilterDDLBusUnit.SelectedValue & "' AND closingdate<>'01/01/1900'"
        sLastClosePeriod = GetStrData(sSql)
        If sLastClosePeriod = "" Then
            sSql = "SELECT MIN(periodacctg) FROM QL_crdstock WHERE cmpcode='" & FilterDDLBusUnit.SelectedValue & "' AND closingdate='01/01/1900'"
            Dim sFirstPeriod As String = "" : sFirstPeriod = GetStrData(sSql)
            If sFirstPeriod = "" Then
                For C1 As Integer = 1 To 12
                    DDLPeriod.Items.Add(New ListItem(MonthName(C1).ToUpper & " " & GetServerTime.Year, GetDateToPeriodAcctg(New Date(GetServerTime.Year, C1, 1))))
                Next
                DDLPeriod.SelectedValue = GetDateToPeriodAcctg(GetServerTime)
                imbClose.Visible = False
                showMessage("Can't find Stock Data to be closed.", 2) : Exit Sub
            Else
                Dim iFirstMonth As Integer = CInt(Right(sFirstPeriod, 2))
                Dim iFirstYear As Integer = CInt(Left(sFirstPeriod, 4))
                DDLPeriod.Items.Add(New ListItem(MonthName(iFirstMonth).ToUpper & " " & iFirstYear, GetDateToPeriodAcctg(New Date(iFirstYear, iFirstMonth, 1))))
            End If
        Else
            Dim iLastMonth As Integer = CInt(Right(sLastClosePeriod, 2))
            Dim iLastYear As Integer = CInt(Left(sLastClosePeriod, 4))
            If iLastMonth = 12 Then
                DDLPeriod.Items.Add(New ListItem(MonthName(1).ToUpper & " " & iLastYear + 1, GetDateToPeriodAcctg(New Date(iLastYear + 1, 1, 1))))
            Else
                DDLPeriod.Items.Add(New ListItem(MonthName(iLastMonth + 1).ToUpper & " " & iLastYear, GetDateToPeriodAcctg(New Date(iLastYear, iLastMonth + 1, 1))))
            End If
        End If
        imbClose.Visible = True : DDLPeriod.Enabled = False : DDLPeriod.CssClass = "inpTextDisabled"
        Session("LastClosingPeriod") = sLastClosePeriod
    End Sub

    Private Sub GetOutstandingTrnData()
        Dim xTable As New DataTable("tblOutTrn")
        xTable.Columns.Add("seq", Type.GetType("System.Int32"))
        xTable.Columns.Add("trnname", Type.GetType("System.String"))
        xTable.Columns.Add("trnqty", Type.GetType("System.Int32"))
        xTable.Columns.Add("trnstatus", Type.GetType("System.String"))
        Dim arrTrnName() As String = {"Material Usage Non KIK", "Raw Material Usage", "Gen Material Usage", "Spare Part Usage", "Raw Material Transfer", "Gen Material Transfer", "Spare Part Transfer", "Finish Good Transfer", "Proses Costing MO Usage", "Production Result Confirmation", "Process Costing MO Res Conf.", "Delivery Order Raw Material", "Delivery Order General Material", "Delivery Order Spare Part", "Delivery Order Finish Good", "Berita Acara", "Raw Material Received", "General Material Received", "Spare Part Received", "Raw Material Return", "General Material Return", "Spare Part Return", "Proses Costing MO Return"}
        Dim arrTrnTable() As String = {"QL_trnmatusagemst", "QL_trnusagerawmst", "QL_trnusagegenmst", "QL_trnusagespmst", "QL_trntransrawmst", "QL_trntransgenmst", "QL_trntransspmst", "QL_trntransitemmst", "QL_trnjousagemst", "QL_trnprodresmst", "QL_trnjoresultmst", "QL_trndorawmst", "QL_trndogenmst", "QL_trndospmst", "QL_trndoitemmst", "QL_trnbrtacaramst", "QL_trnmrrawmst", "QL_trnmrgenmst", "QL_trnmrspmst", "QL_trnretrawmst", "QL_trnretgenmst", "QL_trnretspmst", "QL_trnjoretmst"}
        Dim arrTrnField() As String = {"matusagemststatus", "usagerawmststatus", "usagegenmststatus", "usagespmststatus", "transrawmststatus", "transgenmststatus", "transspmststatus", "transitemmststatus", "jousagemststatus", "prodresmststatus", "joresultmststatus", "dorawmststatus", "dogenmststatus", "dospmststatus", "doitemmststatus", "acaramststatus", "mrrawmststatus", "mrgenmststatus", "mrspmststatus", "retrawmststatus", "retgenmststatus", "retspmststatus", "joretmststatus"}
        Dim arrTrnStatus() As String = {"In Process", "In Process", "In Process", "In Process", "In Process", "In Process", "In Process", "In Process", "In Process", "Post", "Post", "In Approval", "In Approval", "In Approval", "In Approval", "In Approval", "In Process", "In Process", "In Process", "In Process", "In Process", "In Process", "In Process"}
        Dim arrOther() As String = {"", "", "", "", "", "", "", "", "", " AND (prodresmstres3 IS NULL OR prodresmstres3='') ", " AND (joresultmstres3 IS NULL OR joresultmstres3='') ", "", "", "", "", "", "", "", "", "", "", "", ""}

        Dim iSeq As Int16 = 1
        For C1 As Integer = 0 To arrTrnName.Length - 1
            Dim iCount As Integer = CInt(GetTrnCount(arrTrnTable(C1), arrTrnField(C1), arrTrnStatus(C1), arrOther(C1)))
            If iCount > 0 Then
                Dim xRow As DataRow = xTable.NewRow
                xRow("seq") = iSeq
                xRow("trnname") = arrTrnName(C1)
                xRow("trnqty") = iCount
                xRow("trnstatus") = arrTrnStatus(C1)
                xTable.Rows.Add(xRow)
                iSeq += 1
            End If
        Next
        Session("TblData") = xTable
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnClosingStock.aspx")
        End If
        If checkPagePermission("~\Transaction\trnClosingStock.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Closing Stock"
        imbClose.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to CLOSING this period?');")
        If Not Page.IsPostBack Then
            upduser.Text = Session("UserID") : updtime.Text = GetServerTime().ToString
            InitDDL()
            GetLastClose()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        Response.Redirect("~\Other\menu.aspx?awal=true")
    End Sub

    Protected Sub FilterDDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLBusUnit.SelectedIndexChanged
        GetLastClose()
    End Sub

    Protected Sub imbClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClose.Click
        If DDLPeriod.SelectedIndex > 0 Then
            showMessage("Please close period " & DDLPeriod.Items(0).Text & " first before close period " & DDLPeriod.SelectedItem.Text & "!", 2)
            Exit Sub
        End If
        'showConfirmation("Are you sure to closing this period?")
        imbYesPopUpConfirm_Click(Nothing, Nothing)

        '' OLD Outstanding Transaction Confirmation before Closing Stock
        'If Session("TblData") Is Nothing Then
        '    GetOutstandingTrnData()
        'End If
        'Dim dt As DataTable = Session("TblData")
        'If dt.Rows.Count > 0 Then
        '    gvOutTrn.DataSource = dt
        '    gvOutTrn.DataBind()
        '    cProc.SetModalPopUpExtender(btnHideOutTrn, pnlOutTrn, mpeOutTrn, True)
        'Else
        '    showConfirmation("Are you sure to closing this period?")
        'End If
    End Sub

    Protected Sub imbOKOutTrn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKOutTrn.Click
        cProc.SetModalPopUpExtender(btnHideOutTrn, pnlOutTrn, mpeOutTrn, False)
    End Sub

    Protected Sub imbYesPopUpConfirm_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbYesPopUpConfirm.Click
        Dim dStart As New DateTime(Now.Year, Now.Month, Now.Day, Now.Hour, Now.Minute, Now.Second)
        cProc.SetModalPopUpExtender(bePopUpConfirm, pnlPopUpConfirm, mpePopUpConfirm, False)
        If CheckDataExists("SELECT COUNT(-1) FROM QL_crdstock WHERE cmpcode='" & FilterDDLBusUnit.SelectedValue & "' AND periodacctg='" & Session("LastClosingPeriod") & "' AND closingdate='01/01/1900'") And Session("LastClosingPeriod") <> DDLPeriod.SelectedValue Then
            showMessage("Period " & Session("LastClosingPeriod") & " wasn't closing", 2)
            Exit Sub
        Else
            If CheckDataExists("SELECT COUNT(-1) FROM QL_crdstock WHERE cmpcode='" & FilterDDLBusUnit.SelectedValue & "' AND periodacctg='" & DDLPeriod.SelectedValue & "' AND closingdate<>'01/01/1900'") Then
                showMessage("Data has been closing for this period", 2)
                Exit Sub
            End If
        End If

        If GetServerTime() < New Date(CInt(Left(DDLPeriod.SelectedValue, 4)), CInt(Right(DDLPeriod.SelectedValue, 2)), Date.DaysInMonth(CInt(Left(DDLPeriod.SelectedValue, 4)), CInt(Right(DDLPeriod.SelectedValue, 2)))) Then
            showMessage("Minimum closing is at the end of the month that will be closed.", 2)
            Exit Sub
        End If

        Dim iMonth, iYear, iNextMonth, iNextYear As Integer
        Dim sNextPeriod As String
        iMonth = CInt(Right(DDLPeriod.SelectedValue, 2))
        iYear = CInt(Left(DDLPeriod.SelectedValue, 4))
        If iMonth = 12 Then
            iNextMonth = 1 : iNextYear = iYear + 1
        Else
            iNextMonth = iMonth + 1 : iNextYear = iYear
        End If
        sNextPeriod = GetDateToPeriodAcctg(New Date(iNextYear, iNextMonth, 1))

        Dim crdmatoid As Int64 = GenerateID("QL_CRDSTOCK", CompnyCode)
        sSql = "SELECT crd.*, ISNULL(stockvalueidr, 0.0) amtawal_idr_fix, ISNULL(stockvalueusd, 0.0) amtawal_usd_fix FROM QL_crdstock crd LEFT JOIN (SELECT stx.* FROM QL_stockvalue stx WHERE stx.cmpcode='" & FilterDDLBusUnit.SelectedValue & "' AND stx.periodacctg='" & DDLPeriod.SelectedValue & "' AND stx.stockqty<>0) st ON st.refoid=crd.refoid WHERE crd.cmpcode='" & FilterDDLBusUnit.SelectedValue & "' AND crd.periodacctg='" & DDLPeriod.SelectedValue & "' AND saldoakhir>0 AND ISNULL(closeuser, '')=''"
        Dim oTable As DataTable = cKon.ambiltabel(sSql, "QL_crdmtr")

        Dim iStockValueoid As Int64 = GenerateID("QL_STOCKVALUE", CompnyCode)
        sSql = "SELECT * FROM QL_stockvalue WHERE cmpcode='" & FilterDDLBusUnit.SelectedValue & "' AND periodacctg='" & DDLPeriod.SelectedValue & "' AND ISNULL(closeflag, '')='' AND stockqty<>0"
        Dim dtPrice As DataTable = cKon.ambiltabel(sSql, "QL_stockvalue")

        sSql = "SELECT i.*, ISNULL(stockvalueidr, 0.0) price_idr, 0 price_usd FROM QL_mstitem i INNER JOIN (SELECT stx.* FROM QL_stockvalue stx WHERE stx.cmpcode='" & FilterDDLBusUnit.SelectedValue & "' AND stx.periodacctg='" & DDLPeriod.SelectedValue & "' ) st ON st.refoid=i.itemoid WHERE i.cmpcode='" & FilterDDLBusUnit.SelectedValue & "' AND itemrecordstatus='ACTIVE'"
        Dim dtItem As DataTable = cKon.ambiltabel(sSql, "QL_mstitem")
        For C1 As Integer = 0 To dtItem.Rows.Count - 1
            If ToDouble(dtItem.Rows(C1)("price_idr")) <= 0 Then
                dtItem.Rows(C1)("price_idr") = GetBackupValueIDR(FilterDDLBusUnit.SelectedValue, dtItem.Rows(C1)("itemoid"), DDLPeriod.SelectedValue)
            End If
        Next
        dtItem.AcceptChanges()

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction
        xCmd.Transaction = objTrans
        xCmd.CommandTimeout = 0
        Try
            sSql = "UPDATE QL_crdstock SET closeuser='" & Session("UserID") & "', closingdate=CURRENT_TIMESTAMP WHERE periodacctg='" & DDLPeriod.SelectedValue & "' AND cmpcode='" & FilterDDLBusUnit.SelectedValue & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            For C1 As Integer = 0 To oTable.Rows.Count - 1
                Dim dSaldoAkhir As Decimal = ToDouble(oTable.Rows(C1).Item("saldoawal").ToString) + ToDouble(oTable.Rows(C1).Item("qtyin").ToString) + ToDouble(oTable.Rows(C1).Item("qtyadjin").ToString) - ToDouble(oTable.Rows(C1).Item("qtyout").ToString) - ToDouble(oTable.Rows(C1).Item("qtyadjout").ToString)
                sSql = "UPDATE QL_crdstock SET lasttransdate=CURRENT_TIMESTAMP, saldoawal=" & dSaldoAkhir & ", saldoakhir=saldoakhir + " & dSaldoAkhir & ", amtawal_idr=" & ToDouble(oTable.Rows(C1).Item("amtawal_idr_fix").ToString) & ", amtawal_usd=" & ToDouble(oTable.Rows(C1).Item("amtawal_usd_fix").ToString) & " WHERE periodacctg='" & sNextPeriod & "' AND refoid=" & oTable.Rows(C1).Item("refoid") & " AND mtrlocoid=" & oTable.Rows(C1).Item("mtrlocoid") & " AND cmpcode='" & FilterDDLBusUnit.SelectedValue & "'"
                xCmd.CommandText = sSql
                If xCmd.ExecuteNonQuery() <= 0 Then
                    sSql = "INSERT INTO QL_crdstock (cmpcode, crdstockoid, periodacctg, refoid, refname, mtrlocoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir, lasttranstype, lasttransdate, upduser, updtime, createuser, createtime, closeuser, closingdate, amtawal_idr, amtawal_usd) VALUES ('" & FilterDDLBusUnit.SelectedValue & "', " & crdmatoid & ", '" & sNextPeriod & "', " & oTable.Rows(C1).Item("refoid") & ", '" & oTable.Rows(C1).Item("refname") & "', " & oTable.Rows(C1).Item("mtrlocoid") & ", 0, 0, 0, 0, " & dSaldoAkhir & ", " & dSaldoAkhir & ", 'CLOSING STOCK', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', '1/1/1900', " & ToDouble(oTable.Rows(C1).Item("amtawal_idr_fix").ToString) & ", " & ToDouble(oTable.Rows(C1).Item("amtawal_usd_fix").ToString) & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    crdmatoid += 1
                End If
            Next

            sSql = "UPDATE QL_stockvalue SET lasttranstype='CLOSING STOCK', closeflag='CLOSED', lasttransdate=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE periodacctg='" & DDLPeriod.SelectedValue & "' AND cmpcode='" & FilterDDLBusUnit.SelectedValue & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            For C1 As Integer = 0 To dtPrice.Rows.Count - 1
                sSql = "UPDATE QL_stockvalue SET stockqty=stockqty + " & ToDouble(dtPrice.Rows(C1)("stockqty").ToString) & ", stockvalueidr=ISNULL((((stockqty * stockvalueidr) + (" & ToDouble(dtPrice.Rows(C1)("stockqty").ToString) * ToDouble(dtPrice.Rows(C1)("stockvalueidr").ToString) & ")) / NULLIF((stockqty + " & ToDouble(dtPrice.Rows(C1)("stockqty").ToString) & "), 0)), 0.0), stockvalueusd=ISNULL((((stockqty * stockvalueusd) + (" & ToDouble(dtPrice.Rows(C1)("stockqty").ToString) * ToDouble(dtPrice.Rows(C1)("stockvalueusd").ToString) & ")) / NULLIF((stockqty + " & ToDouble(dtPrice.Rows(C1)("stockqty").ToString) & "), 0)), 0.0), backupqty=stockqty, backupvalueidr=stockvalueidr, backupvalueusd=stockvalueusd, lasttranstype='CLOSING STOCK', lasttransdate=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE periodacctg='" & sNextPeriod & "' AND refoid=" & dtPrice.Rows(C1).Item("refoid") & " AND cmpcode='" & FilterDDLBusUnit.SelectedValue & "'"
                xCmd.CommandText = sSql
                If xCmd.ExecuteNonQuery() <= 0 Then
                    sSql = "INSERT INTO QL_stockvalue (cmpcode, stockvalueoid, periodacctg, refoid, refname, stockqty, stockvalueidr, stockvalueusd, lasttranstype, lasttransdate, note, upduser, updtime, backupqty, backupvalueidr, backupvalueusd) VALUES ('" & FilterDDLBusUnit.SelectedValue & "', " & iStockValueoid & ", '" & sNextPeriod & "', " & dtPrice.Rows(C1)("refoid") & ", '" & dtPrice.Rows(C1)("refname") & "', " & ToDouble(dtPrice.Rows(C1)("stockqty").ToString) & ", " & ToDouble(dtPrice.Rows(C1)("stockvalueidr").ToString) & ", " & ToDouble(dtPrice.Rows(C1)("stockvalueusd").ToString) & ", 'CLOSING STOCK', CURRENT_TIMESTAMP, '" & Tchar(dtPrice.Rows(C1)("note").ToString) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dtPrice.Rows(C1)("stockqty").ToString) & ", " & ToDouble(dtPrice.Rows(C1)("stockvalueidr").ToString) & ", " & ToDouble(dtPrice.Rows(C1)("stockvalueusd").ToString) & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    iStockValueoid += 1
                End If
            Next

            For C1 As Integer = 0 To dtItem.Rows.Count - 1
                Dim price As Double = ToDouble(dtItem.Rows(C1)("price_idr").ToString)
                Dim dPercentage As Double = GetHppPercentage(dtItem.Rows(C1)("itemcat2").ToString)
                Dim hpp As Double = ToMaskEdit(price * (dPercentage / 100), 2)
                'update minStock di tabel mstitem
                sSql = "UPDATE QL_mstitem SET itemMinPrice = " & price + hpp & ", itemMinPrice1 = " & price + hpp & ", itemMinPrice2 = " & price + hpp & ", itemMinPrice3 = " & price + hpp & " WHERE itemoid = '" & dtItem.Rows(C1).Item("itemoid") & "' and cmpcode = '" & FilterDDLBusUnit.SelectedValue & "' "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            Next

            sSql = "UPDATE QL_mstoid SET lastoid=" & crdmatoid - 1 & " WHERE tablename='QL_CRDSTOCK' AND cmpcode='" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_mstoid SET lastoid=" & iStockValueoid - 1 & " WHERE tablename='QL_STOCKVALUE' AND cmpcode='" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch exSql As SqlException
            objTrans.Rollback() : conn.Close()
            If ToDouble(Session("ErrorCounter")) < 5 Then
                If exSql.Number = 2627 Then
                    Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                    imbYesPopUpConfirm_Click(sender, e)
                    Exit Sub
                Else
                    showMessage(exSql.Message, 1)
                    Exit Sub
                End If
            Else
                showMessage(exSql.Message, 1)
                Exit Sub
            End If
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, 1) : Exit Sub
        End Try
        Dim dEnd As New DateTime(Now.Year, Now.Month, Now.Day, Now.Hour, Now.Minute, Now.Second)
        Dim tsDuration As TimeSpan = dEnd - dStart
        showMessage("Closing Success in " & tsDuration.TotalMinutes & " Minutes", 3)
    End Sub

    Protected Sub imbNoPopUpConfirm_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbNoPopUpConfirm.Click
        cProc.SetModalPopUpExtender(bePopUpConfirm, pnlPopUpConfirm, mpePopUpConfirm, False)
    End Sub

    Protected Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbCancel.Click
        Response.Redirect("~\Transaction\trnClosingStock.aspx?awal=true")
    End Sub
#End Region

End Class