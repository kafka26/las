Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_MaterialRequestNonKIKClosing
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function GetSelectedID() As String
        Dim sRet As String = ""
        For C1 As Integer = 0 To gvDtl.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvDtl.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        If CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = True Then
                            sRet &= CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip & ","
                        End If
                    End If
                Next
            End If
        Next
        If sRet <> "" Then
            sRet = Left(sRet, sRet.Length - 1)
        End If
        Return sRet
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitDDL()
        ' Init DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)
    End Sub

	Private Sub BindListMatReq()
		sSql = "SELECT req.matreqmstoid,req.matreqno, CONVERT(VARCHAR(10), matreqdate, 101) AS matreqdate, deptname, gendesc AS matreqwh,req.matreqmstnote FROM QL_trnmatreqmst req INNER JOIN QL_mstdept de ON de.cmpcode=req.cmpcode AND de.deptoid=req.deptoid INNER JOIN QL_mstgen g ON genoid=req.matreqwhoid WHERE req.cmpcode='" & DDLBusUnit.SelectedValue & "' AND req.matreqmststatus='Post' AND " & FilterDDLListMatReq.SelectedValue & " LIKE '%" & Tchar(FilterTextListMatReq.Text) & "%' AND req.matreqmstoid IN (SELECT mud.matreqmstoid FROM QL_trnmatusagedtl mud INNER JOIN QL_trnmatusagemst mum ON mum.cmpcode=mud.cmpcode AND mum.matusagemstoid=mud.matusagemstoid WHERE mud.cmpcode='" & DDLBusUnit.SelectedValue & "' AND matusagemststatus='Post') ORDER BY matreqmstoid"

		FillGV(gvListMatReq, sSql, "QL_trnmatreqmst")
	End Sub

	Private Sub GenerateDetailData()
		sSql = "SELECT req.matreqdtloid,req.matreqdtlseq,req.matreqreftype,req.matreqrefoid, (SELECT m.itemCode FROM QL_mstitem m WHERE m.itemoid=req.matreqrefoid AND m.cmpcode=req.cmpcode) AS matreqrefcode,(SELECT m.itemLongDescription FROM QL_mstitem m WHERE m.itemoid=req.matreqrefoid AND m.cmpcode=req.cmpcode) AS matreqreflongdesc,req.matreqqty, ISNULL((SELECT SUM(matusageqty) FROM QL_trnmatusagedtl mud INNER JOIN QL_trnmatusagemst mum ON mum.cmpcode=mud.cmpcode AND mum.matusagemstoid=mud.matusagemstoid WHERE mud.cmpcode=req.cmpcode AND mud.matreqdtloid=req.matreqdtloid AND mum.matusagemststatus='Post'), 0.0) AS matusageqty, 0.0 AS outstandingqty,req.matrequnitoid AS matrequnitoid, gendesc AS matrequnit,req.matreqdtlnote AS matreqdtlnote FROM QL_trnmatreqdtl req INNER JOIN QL_mstgen ON genoid=matrequnitoid WHERE req.cmpcode='" & DDLBusUnit.SelectedValue & "' AND matreqmstoid=" & matreqmstoid.Text & " AND matreqdtlstatus='' ORDER BY matreqdtlseq"

		Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnmatreqdtl")
		For C1 As Integer = 0 To dt.Rows.Count - 1
			dt.Rows(C1)("matreqdtlseq") = C1 + 1
			dt.Rows(C1)("outstandingqty") = ToDouble(dt.Rows(C1)("matreqqty").ToString) - ToDouble(dt.Rows(C1)("matusageqty").ToString)
		Next
		dt.AcceptChanges()
		gvDtl.DataSource = dt
		gvDtl.DataBind()
	End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnMatRequestClosing.aspx")
        End If
        If checkPagePermission("~\Transaction\trnMatRequestClosing.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Material Request Non KIK Closing"
        btnCloseSelected.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to CLOSE selected data?');")
        btnCloseAll.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to CLOSE all data?');")
        If Not Page.IsPostBack Then
            upduser.Text = Session("UserID") : updtime.Text = GetServerTime().ToString
            InitDDL()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("Success") Is Nothing And Session("Success") <> "" Then
            Response.Redirect("~\Transaction\trnMatRequestClosing.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnSearchMatReq_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMatReq.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        FilterDDLListMatReq.SelectedIndex = -1 : FilterTextListMatReq.Text = "" : gvListMatReq.SelectedIndex = -1
        BindListMatReq()
        cProc.SetModalPopUpExtender(btnHideListMatReq, pnlListMatReq, mpeListMatReq, True)
    End Sub

    Protected Sub btnFindListMatReq_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMatReq.Click
        BindListMatReq()
        mpeListMatReq.Show()
    End Sub

    Protected Sub btnAllListMatReq_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMatReq.Click
        FilterDDLListMatReq.SelectedIndex = -1 : FilterTextListMatReq.Text = "" : gvListMatReq.SelectedIndex = -1
        BindListMatReq()
        mpeListMatReq.Show()
    End Sub

    Protected Sub gvListMatReq_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMatReq.PageIndexChanging
        gvListMatReq.PageIndex = e.NewPageIndex
        BindListMatReq()
        mpeListMatReq.Show()
    End Sub

    Protected Sub gvListMatReq_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListMatReq.SelectedIndexChanged
        matreqmstoid.Text = gvListMatReq.SelectedDataKey.Item("matreqmstoid").ToString
        matreqno.Text = gvListMatReq.SelectedDataKey.Item("matreqno").ToString
        matreqmstnote.Text = gvListMatReq.SelectedDataKey.Item("matreqmstnote").ToString
        cProc.SetModalPopUpExtender(btnHideListMatReq, pnlListMatReq, mpeListMatReq, False)
        DDLBusUnit.Enabled = False : DDLBusUnit.CssClass = "inpTextDisabled"
        GenerateDetailData()
    End Sub

    Protected Sub lbCloseListMatReq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMatReq.Click
        cProc.SetModalPopUpExtender(btnHideListMatReq, pnlListMatReq, mpeListMatReq, False)
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 4)
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnMatRequestClosing.aspx?awal=true")
    End Sub

    Protected Sub btnCloseSelected_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCloseSelected.Click
        If matreqmstoid.Text = "" Then
            showMessage("Please select Request No. first!", 2)
            Exit Sub
        End If
        Dim sOid As String = GetSelectedID()
        If sOid = "" Then
            showMessage("Please select some detail data to be closed!", 2)
            Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
			sSql = "UPDATE QL_trnmatreqdtl SET matreqdtlstatus='Complete', matreqdtlres1='Force Complete', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matreqdtloid IN (" & sOid & ") AND matreqmstoid=" & matreqmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
			sSql = "UPDATE QL_trnmatreqmst SET matreqmststatus='Closed', matreqmstres1='Force Closed', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND (SELECT COUNT(*) FROM QL_trnmatreqdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matreqmstoid=" & matreqmstoid.Text & " AND matreqdtloid NOT IN (" & sOid & ") AND matreqdtlstatus='')=0 AND matreqmstoid=" & matreqmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Session("Success") = "Selected data for Request No. : " & matreqno.Text & " have been closed successfully."
        showMessage(Session("Success"), 3)
    End Sub

    Protected Sub btnCloseAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCloseAll.Click
        If matreqmstoid.Text = "" Then
            showMessage("Please select Request No. first!", 2)
            Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
			sSql = "UPDATE QL_trnmatreqdtl SET matreqdtlstatus='Complete', matreqdtlres1='Force Complete', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matreqdtlstatus='' AND matreqmstoid=" & matreqmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
			sSql = "UPDATE QL_trnmatreqmst SET matreqmststatus='Closed', matreqmstres1='Force Closed', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND matreqmstoid=" & matreqmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Session("Success") = "Request No. : " & matreqno.Text & " have been closed successfully."
        showMessage(Session("Success"), 3)
    End Sub
#End Region

End Class