<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnTransformItem.aspx.vb" Inherits="Transaction_FinishGoodTransfer" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2" style="height: 3px">
                <asp:Label ID="Label1" runat="server" ForeColor="#804000" Text=".: Transform Item" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Transform Item :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="Label15" runat="server" Text="Filter"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="105px"><asp:ListItem Value="transmstoid">Draft No.</asp:ListItem>
<asp:ListItem Value="transno">Transform No.</asp:ListItem>
<asp:ListItem Value="transmstnote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="300px"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text="Period"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="80px" ToolTip="(MM/dd/yyyy)"></asp:TextBox> <asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label5" runat="server" Text="to"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="80px" ToolTip="(MM/dd/yyyy)"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="lbldate1" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="105px">
    <asp:ListItem>In Process</asp:ListItem>
    <asp:ListItem>Post</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" TargetControlID="FilterPeriod1" PopupButtonID="imbDate1" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" TargetControlID="FilterPeriod2" PopupButtonID="imbDate2" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="FilterPeriod1" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" TargetControlID="FilterPeriod2" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><asp:DropDownList id="FilterDDL2" runat="server" CssClass="inpText" Width="105px" Visible="False">
    <asp:ListItem Value="[Draft No.]">Draft No.</asp:ListItem>
    <asp:ListItem Value="[Transfer No.]">Transfer No.</asp:ListItem>
    <asp:ListItem Value="[Doc. Ref. No.]">Doc. Ref. No.</asp:ListItem>
    <asp:ListItem Value="[Header Note]">Note</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:LinkButton id="lbInProcess" runat="server"></asp:LinkButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvMst" runat="server" ForeColor="#333333" Width="100%" AllowSorting="True" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" PageSize="8">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="transmstoid" DataNavigateUrlFormatString="~\Transaction\trnTransformItem.aspx?oid={0}" DataTextField="transmstoid" HeaderText="Draft No." SortExpression="transmstoid">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="transno" HeaderText="Transform No." SortExpression="transno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transdate" HeaderText="Transform Date" SortExpression="transdate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transmstnote" HeaderText="Note" SortExpression="transmstnote">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transmststatus" HeaderText="Status" SortExpression="transmststatus">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
    <asp:TemplateField Visible="False">
        <ItemTemplate>
            <asp:CheckBox ID="cbGVMst" runat="server" Checked='<%# eval("checkvalue") %>' /><asp:Label
                ID="lblOidGVMst" runat="server" Text='<%# eval("transmstoid") %>' Visible="False"></asp:Label>
        </ItemTemplate>
        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Center" />
        <ItemStyle HorizontalAlign="Center" />
    </asp:TemplateField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" ForeColor="White" CssClass="gvhdr" Font-Bold="True"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label3" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Transfer Transform Item :" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="i_u" runat="server" CssClass="Important" Text="New Data"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="periodacctg" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 14%" class="Label" align=left></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label10" runat="server" Text="Business Unit"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpText" Width="305px" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblspuno" runat="server" Text="Draft No"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:Label id="transmstoid" runat="server"></asp:Label><asp:TextBox id="transno" runat="server" CssClass="inpTextDisabled" Width="200px" Visible="False" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label6" runat="server" Text="Transform Date"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="transdate" runat="server" CssClass="inpText" Width="80px"></asp:TextBox>&nbsp;<asp:ImageButton
        ID="btnTransdate" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/oCalendar.gif" />
    <asp:Label ID="Label2" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label4" runat="server" Text="Doc. Ref. No."></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="transdocrefno" runat="server" CssClass="inpText" Width="300px" MaxLength="50"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label8" runat="server" Text="Header Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="transmstnote" runat="server" CssClass="inpText" Width="300px" MaxLength="100"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label9" runat="server" Text="Status"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="transmststatus" runat="server" CssClass="inpTextDisabled" Width="80px" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label25" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Transform Item Detail :" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="i_u2" runat="server" Text="New Detail" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="transdtlseq" runat="server" Visible="False">1</asp:Label> <asp:Label id="transdtloid" runat="server"></asp:Label> <asp:Label id="matoid" runat="server" Visible="False"></asp:Label> <asp:Label id="matcode" runat="server" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left>
    <asp:Label ID="transtowhoid2" runat="server" Visible="False"></asp:Label>
    <asp:Label ID="matoid2" runat="server" Visible="False"></asp:Label>
    <asp:Label ID="matcode2" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label17" runat="server" Text="From WH"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="transfromwhoid" runat="server" CssClass="inpText" Width="305px" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label12" runat="server" Text="To WH"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="transtowhoid" runat="server" CssClass="inpText" Width="305px"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label11" runat="server" Text="From Item"></asp:Label> <asp:Label id="Label23" runat="server" ForeColor="Red" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="matlongdesc" runat="server" CssClass="inpTextDisabled" Width="264px" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSearchMat" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton ID="btnEraseListMat" runat="server" ImageAlign="AbsMiddle"
        ImageUrl="~/Images/erase.bmp" OnClick="btnEraseListMat_Click" /></TD><TD class="Label" align=left>
        <asp:Label ID="Label14" runat="server" Text="To Item"></asp:Label>
        <asp:Label ID="Label18" runat="server" ForeColor="Red" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left>
        <asp:TextBox ID="matlongdesc2" runat="server" CssClass="inpTextDisabled" Enabled="False"
            Width="264px"></asp:TextBox>&nbsp;<asp:ImageButton ID="btnSearchMat2" runat="server"
                ImageAlign="AbsMiddle" ImageUrl="~/Images/search.gif" OnClick="btnSearchMat2_Click" />
            <asp:ImageButton ID="btnEraseListMat2" runat="server"
                ImageAlign="AbsMiddle" ImageUrl="~/Images/erase.bmp" OnClick="btnEraseListMat2_Click" /></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label16" runat="server" Text="Qty"></asp:Label> <asp:Label id="Label24" runat="server" ForeColor="Red" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="transqty" runat="server" CssClass="inpText" Width="80px"></asp:TextBox> <asp:DropDownList id="transunitoid" runat="server" CssClass="inpTextDisabled" Width="105px" Enabled="False"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label20" runat="server" Text="Stock Qty"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:Label id="stockqty" runat="server">0</asp:Label></TD></TR>
    <tr>
        <td align="left" class="Label">
            <asp:Label id="Label13" runat="server" Text="Detail Note"></asp:Label></td>
        <td align="center" class="Label">
            :</td>
        <td align="left" class="Label">
            <asp:TextBox id="transdtlnote" runat="server" CssClass="inpText" Width="300px" MaxLength="100"></asp:TextBox></td>
        <td align="left" class="Label">
        </td>
        <td align="center" class="Label">
        </td>
        <td align="left" class="Label">
        </td>
    </tr>
    <TR><TD class="Label" align=left colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="ftbQty" runat="server" ValidChars="0123456789,." TargetControlID="transqty"></ajaxToolkit:FilteredTextBoxExtender> 
        <ajaxToolkit:CalendarExtender ID="CeTransdate" runat="server" Format="MM/dd/yyyy"
            PopupButtonID="btnTransdate" TargetControlID="transdate">
        </ajaxToolkit:CalendarExtender>
        <ajaxToolkit:MaskedEditExtender ID="MeeTransdate" runat="server" Mask="99/99/9999"
            MaskType="Date" TargetControlID="transdate">
        </ajaxToolkit:MaskedEditExtender>
    </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:GridView id="gvDtl" runat="server" ForeColor="#333333" Width="100%" GridLines="None" CellPadding="4" DataKeyNames="transdtlseq" AutoGenerateColumns="False">
<RowStyle BackColor="#EFF3FB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
    <asp:BoundField DataField="matcodefrom" HeaderText="From Code">
        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
        <ItemStyle HorizontalAlign="Left" />
    </asp:BoundField>
<asp:BoundField DataField="matlongdescfrom" HeaderText="From Material">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transfromwh" HeaderText="From WH">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
    <asp:BoundField DataField="matcodeto" HeaderText="To Code">
        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
    </asp:BoundField>
    <asp:BoundField DataField="matlongdescto" HeaderText="To Description">
        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
    </asp:BoundField>
<asp:BoundField DataField="transtowh" HeaderText="To WH">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transqty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transunit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transdtlnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=6></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True"></asp:Label>&nbsp;On <asp:Label id="createtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnposting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div1" class="progressBackgroundFilter"></DIV><DIV id="Div2" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Transform Item :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel id="upListMat" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListMat" runat="server" CssClass="modalBox" Width="900px" DefaultButton="btnFindListMat" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Medium" Font-Bold="True" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="matcodefrom">Code</asp:ListItem>
<asp:ListItem Value="matlongdescfrom">Description</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" CssClass="inpText" Width="300px"></asp:TextBox> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat" runat="server" ForeColor="#333333" Width="99%" PageSize="5" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" DataKeyNames="matoidfrom,matcodefrom,matlongdescfrom,stockqty" OnSelectedIndexChanged="gvListMat_SelectedIndexChanged">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
    <asp:CommandField ShowSelectButton="True">
        <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center" />
        <ItemStyle HorizontalAlign="Center" />
    </asp:CommandField>
<asp:BoundField DataField="matcodefrom" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matlongdescfrom" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stockqty" HeaderText="Stock Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transunit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" ForeColor="White" Font-Bold="True"></PagerStyle>
<EmptyDataTemplate>
                                        &nbsp;
                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3> <asp:LinkButton id="lkbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" Drag="True" PopupControlID="pnlListMat" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblTitleListMat">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel><asp:UpdatePanel id="UpListMat2" runat="server">
        <contenttemplate>
            <asp:Panel id="pnlListMat2" runat="server" CssClass="modalBox" Width="900px" DefaultButton="btnFindListMat2" Visible="False">
                <TABLE style="WIDTH: 100%">
                    <TBODY>
                        <TR>
                            <TD class="Label" align=center colSpan=3>
                                <asp:Label ID="lblTitleListMat2" runat="server" Font-Bold="True" Font-Size="Medium"
                                    Font-Underline="False"></asp:Label></td>
                        </tr>
                        <TR>
                            <TD class="Label" align=center colSpan=3>
                                Filter :
                                <asp:DropDownList id="FilterDDLListMat2" runat="server" CssClass="inpText" Width="100px">
                                    <asp:ListItem Value="matcodeto">Code</asp:ListItem>
                                    <asp:ListItem Value="matlongdescto">Description</asp:ListItem>
                                </asp:DropDownList>
                                <asp:TextBox ID="FilterTextListMat2" runat="server" CssClass="inpText" Width="300px"></asp:TextBox>
                                <asp:ImageButton ID="btnFindListMat2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/btnfind.bmp" OnClick="btnFindListMat2_Click" />
                                <asp:ImageButton ID="btnAllListMat2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewall.png" OnClick="btnAllListMat2_Click" /></td>
                        </tr>
                        <TR>
                            <TD class="Label" align=center colSpan=3>
                                <asp:GridView id="gvListMat2" runat="server" ForeColor="#333333" Width="99%" PageSize="5" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" DataKeyNames="matoidto,matcodeto,matlongdescto" OnPageIndexChanging="gvListMat2_PageIndexChanging" OnRowDataBound="gvListMat2_RowDataBound" OnSelectedIndexChanged="gvListMat2_SelectedIndexChanged">
                                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="True">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:CommandField>
                                        <asp:BoundField DataField="matcodeto" HeaderText="Code">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="matlongdescto" HeaderText="Description">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="stockqty" HeaderText="Stock Qty">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="transunit" HeaderText="Unit">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                    </Columns>
                                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle CssClass="gvpopup" Font-Bold="True" ForeColor="White" HorizontalAlign="Right" />
                                    <EmptyDataTemplate>
                                        &nbsp;
                                    </EmptyDataTemplate>
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <TR>
                            <TD align=center colSpan=3>
                                <asp:LinkButton ID="lkbCloseListMat2" runat="server" OnClick="lkbCloseListMat2_Click">[ Cancel & Close ]</asp:LinkButton></td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender id="mpeListMat2" runat="server" TargetControlID="btnHideListMat2" Drag="True" PopupControlID="pnlListMat2" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblTitleListMat2">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="btnHideListMat2" runat="server" ForeColor="Transparent" Visible="False" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2><asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>