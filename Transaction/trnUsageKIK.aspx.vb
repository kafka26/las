Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Structure VarUsageValue
    Public Val_IDR As Double
    Public Val_USD As Double
End Structure

Partial Class Transaction_RawMaterialUsage
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "reqdtloid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("usageqty") = ToDouble(GetTextBoxValue(row.Cells(5).Controls))
                                    dtView(0)("usagedtlnote") = GetTextBoxValue(row.Cells(7).Controls)
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "reqdtloid=" & cbOid
                                dtView2.RowFilter = "reqdtloid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("usageqty") = ToDouble(GetTextBoxValue(row.Cells(5).Controls))
                                    dtView(0)("usagedtlnote") = GetTextBoxValue(row.Cells(7).Controls)
                                    If dtView2.Count > 0 Then
                                        dtView2(0)("usageqty") = ToDouble(GetTextBoxValue(row.Cells(5).Controls))
                                        dtView2(0)("usagedtlnote") = GetTextBoxValue(row.Cells(7).Controls)
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If reqmstoid.Text = "" Then
            sError &= "- Please select REQUEST NO. field!<BR>"
        End If
        If usagewhoid.SelectedValue = "" Then
            sError &= "- Please select WAREHOUSE field!<BR>"
        End If
        If matoid.Text = "" Then
            sError &= "- Please select MATERIAL field!<BR>"
        End If
        If usageqty.Text = "" Then
            sError &= "- Please fill QTY field!<BR>"
        Else
            If ToDouble(usageqty.Text) <= 0 Then
                sError &= "- QTY field must be more than 0!<BR>"
            Else
                Dim sErrReply As String = ""
                If Not isLengthAccepted("usageqty", "QL_trnusagerawdtl", ToDouble(usageqty.Text), sErrReply) Then
                    sError &= "- QTY field must be less than MAX QTY (" & sErrReply & ") allowed stored in database!<BR>"
                Else
                    If ToDouble(usageqty.Text) > (ToDouble(wodtl3qty.Text) + (ToDouble(wodtl3qty.Text) * (ToDouble(wotolerance.Text) / 100))) Then
                        ' sError &= "- QTY field must be less or equal than SPK. QTY!<BR>"
                    Else
                        If ToDouble(usageqty.Text) > ToDouble(stockqty.Text) Then
                            sError &= "- QTY field must be less than STOCK QTY!<BR>"
                        End If
                    End If
                End If
            End If
        End If
        If usageunitoid.SelectedValue = "" Then
            sError &= "- Please select UNIT field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If usagedate.Text = "" Then
            sErr = "None"
            sError &= "- Please fill DATE field!<BR>"
        Else
            If Not IsValidDate(usagedate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If deptoid.SelectedValue = "" Then
            sError &= "- Please select DEPARTMENT field!<BR>"
        End If
        If ToInteger(matoid_mix.Text) <> 0 Then
            If qty_mix.Text = "" Then
                sError &= "- Please fill QTY MIXING field!<BR>"
            Else
                If ToDouble(qty_mix.Text) <= 0 Then
                    sError &= "- QTY MIXING must be more than 0!<BR>"
                End If
            End If
            If unitoid_mix.SelectedValue = "" Then
                sError &= "- Please select MATERIAL MIXING UNIT field!<BR>"
            End If
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill Detail Data!<BR>"
        Else
            Dim dtTbl As DataTable = Session("TblDtl")
            If dtTbl.Rows.Count <= 0 Then
                sError &= "- Please fill Detail Data!<BR>"
            Else
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    If Not IsStockAvailable(DDLBusUnit.SelectedValue, Format(GetServerTime(), "yyyyMM"), dtTbl.Rows(C1)("matoid"), dtTbl.Rows(C1)("usagewhoid"), ToDouble((dtTbl.Compute("SUM(usageqty)", "matoid='" & dtTbl.Rows(C1)("matoid") & "'")).ToString)) Then
                        sError &= "- Material Code <B><STRONG>" & dtTbl.Rows(C1)("matcode") & "</STRONG></B> must be less than Stock Qty !<BR> "
                        Exit For
                    End If
                Next
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            usagemststatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function GetStockValue(ByVal sOid As String, Optional ByVal sType As String = "IDR") As Double
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        Try
            GetStockValue = ToDouble(GetStrData("select dbo.getStockValue(" & sOid & ")"))
        Catch ex As Exception
            GetStockValue = 0
        End Try
    End Function

    Private Function GetAcctgStock(ByVal sType As String) As String
        GetAcctgStock = ""
        sSql = "SELECT ISNULL(genother1,'') acctgoid FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='GROUPITEM' AND gencode='" & sType & "' AND activeflag='ACTIVE'"
        GetAcctgStock = GetStrData(sSql)
        Return GetAcctgStock
    End Function

    Private Function GetAmtByGroup(ByVal dtRef As DataTable) As DataTable
        Dim dtReturn As New DataTable()
        dtReturn.Columns.Add("acctgoid", Type.GetType("System.Int32"))
        dtReturn.Columns.Add("amount", Type.GetType("System.Double"))
        sSql = "SELECT DISTINCT genother1 AS acctgoid FROM QL_mstgen WHERE cmpcode='" & Session("CompnyCode") & "' AND gengroup='GROUPITEM' AND activeflag='ACTIVE'"
        Dim dtAcctg As DataTable = cKon.ambiltabel(sSql, "QL_mstacctg")
        For C1 As Integer = 0 To dtAcctg.Rows.Count - 1
            Dim obj As Object
            obj = dtRef.Compute("SUM(usagevalueidr)", "stockacctgoid=" & dtAcctg.Rows(C1)("acctgoid").ToString)
            If Not IsDBNull(obj) Then
                Dim dr As DataRow = dtReturn.NewRow()
                dr("acctgoid") = CInt(dtAcctg.Rows(C1)("acctgoid").ToString.Trim)
                dr("amount") = ToDouble(obj.ToString)
                dtReturn.Rows.Add(dr)
            End If
        Next
        Return dtReturn
    End Function

    Private Function GetFGDescription(ByVal sNo As String) As String
        Return GetStrData("SELECT TOP 1 i.itemshortdescription FROM QL_trnwomst wom INNER JOIN QL_trnwodtl1 wod1 ON wom.cmpcode=wod1.cmpcode AND wom.womstoid=wod1.womstoid INNER JOIN QL_mstitem i ON i.itemoid=wod1.itemoid WHERE wom.wono='" & sNo & "'")
    End Function

#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckUsageStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trnusagerawmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND usagerawmststatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnUsageKIK.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lkbUsageInProcess.Visible = True
            lkbUsageInProcess.Text = "You have " & GetStrData(sSql) & " In Process Raw Material Usage data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitAllDDL()
        ' Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)
        ' Fill DDL Warehouse
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='WAREHOUSE' AND activeflag='ACTIVE' AND genoid>0"
        FillDDL(usagewhoid, sSql)
        ' Fill DDL Unit
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='UNIT' AND activeflag='ACTIVE'"
        FillDDL(usageunitoid, sSql)
        FillDDL(stockunitoid, sSql)
        ' Fill DDL Material Mixing Unit
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='UNIT'"
        If i_u.Text = "New Data" Then
            sSql &= " AND activeflag='ACTIVE'"
        End If
        FillDDL(unitoid_mix, sSql)
        ' Fill DDL Department
        sSql = "SELECT top 1 deptoid, deptname FROM QL_mstdept WHERE deptname='PRODUCTION'"
        FillDDL(deptoid, sSql)
    End Sub

    Private Sub InitDDLDept()
        ' Fill DDL Department
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE deptname='PRODUCTION'"
        FillDDL(deptoid, sSql)
    End Sub

    Private Sub InitDDLDept2()
        ' Fill DDL Department
        Dim sAdd As String = ""
        Dim sAdd2 As String = ""
        If i_u.Text = "Update Data" Then
            sAdd = " OR deptoid IN (SELECT deptoid FROM QL_trnusagemst WHERE usagemstoid=" & usagemstoid.Text & " AND womstoid=" & womstoid.Text & ")"
        Else
            sAdd2 = " AND wodtl3reqflag=''"
        End If
        sSql = "SELECT deptoid, deptname FROM QL_mstdept WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND (deptoid IN (SELECT deptoid FROM QL_trnwodtl2 wod2 INNER JOIN QL_trnwodtl3 wod3 ON wod3.cmpcode=wod2.cmpcode AND wod3.wodtl1oid=wod2.wodtl1oid WHERE wod2.cmpcode='" & DDLBusUnit.SelectedValue & "' AND wod2.womstoid=" & womstoid.Text & " " & sAdd2 & " AND wodtl2type='FG')" & sAdd & ") ORDER BY deptname"
        FillDDL(deptoid, sSql)
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT div.divname, mum.usagemstoid AS usagemstoid, mum.usageno AS usageno, CONVERT(VARCHAR(10), mum.usagedate, 101) AS usagedate, d.deptname, mum.usagemststatus AS usagemststatus, mum.usagemstnote AS usagemstnote, 'False' AS checkvalue FROM QL_trnusagemst mum INNER JOIN QL_mstdept d ON d.deptoid=mum.deptoid AND d.cmpcode=mum.cmpcode INNER JOIN QL_mstdivision div ON mum.cmpcode=div.cmpcode WHERE "
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " mum.cmpcode='" & Session("CompnyCode") & "'"
        Else
            sSql &= " mum.cmpcode LIKE '%'"
        End If
        sSql &= sSqlPlus & " ORDER BY mum.usagemstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trnusagerawmst")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "usagemstoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub PrintReport()
        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("usagemstoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            report.Load(Server.MapPath(folderReport & "rptUsageRaw.rpt"))
            Dim sWhere As String = " WHERE"
            If Session("CompnyCode") <> CompnyCode Then
                sWhere &= " mum.cmpcode='" & Session("CompnyCode") & "'"
            Else
                sWhere &= " mum.cmpcode LIKE '%'"
            End If
            If sOid = "" Then
                sWhere &= " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND mum.usagerawdate>='" & FilterPeriod1.Text & " 00:00:00' AND mum.usagerawdate<='" & FilterPeriod2.Text & "'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sWhere &= " AND mum.usagerawmststatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Transaction\trnUsageKIK.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND mum.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sWhere &= " AND mum.usagemstoid IN (" & sOid & ")"
            End If
            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "RawMatUsagePrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
        Response.Redirect("~\Transaction\trnUsageKIK.aspx?awal=true")
    End Sub

    Private Sub BindRequestData()
        sSql = "SELECT reqm.reqmstoid AS reqmstoid, reqm.reqno AS reqno, CONVERT(VARCHAR(10), reqm.reqdate, 101) AS reqdate, reqm.reqwhoid AS reqwhoid, g.gendesc AS reqwh, reqm.reqmstnote AS reqmstnote, ISNULL((SELECT wono FROM QL_trnwomst wom WHERE wom.cmpcode=reqm.cmpcode AND wom.womstoid=reqm.womstoid), '') AS wono, reqm.reqmstres3 AS itemlongdesc FROM QL_trnreqmst reqm INNER JOIN QL_mstgen g ON g.genoid=reqm.reqwhoid WHERE reqm.cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqm.reqmststatus='Post' AND " & FilterDDLListMatReq.SelectedValue & " LIKE '%" & Tchar(FilterTextListMatReq.Text) & "%' AND reqm.deptoid=" & deptoid.SelectedValue & " ORDER BY reqm.reqmstoid"
        FillGV(gvListMatReq, sSql, "QL_trnreqrawmst")
    End Sub

    Private Sub BindMaterialData()
        Dim sStr As String = ""
        If Session("oid") IsNot Nothing And Session("oid") <> "" Then
            sStr = " AND usagemstoid<>" & Session("oid")
        End If
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sSql = "SELECT 'False' AS checkvalue, reqd.wodtl3seq AS reqrawdtlseq, reqd.wodtl3oid AS reqdtloid, reqd.wodtl3refoid AS matoid, m.itemcode AS matcode, m.itemlongdescription AS matlongdesc, m.roundqty AS matlimitqty, (reqd.wodtl3qty - ISNULL((SELECT SUM(usageqty) FROM QL_trnusagedtl mud WHERE mud.cmpcode=reqd.cmpcode AND mud.reqdtloid=reqd.wodtl3oid" & sStr & "), 0.0)) AS reqqty, (select dbo.getstockqty(reqd.wodtl3refoid, " & usagewhoid.SelectedValue & ")) AS stockqty, 0.0 AS usageqty, reqd.wodtl3unitoid AS requnitoid, g.gendesc AS requnit, gx.gendesc AS stockunit, gx.genoid AS stockunitoid, '' AS usagedtlnote, (CASE m.itemgroup WHEN 'Raw' THEN 'RAW MATERIAL' WHEN 'Gen' THEN 'GENERAL MATERIAL' WHEN 'Wip' THEN 'WIP' ELSE 'FINISH GOOD' END) AS itemgroup, m.itemgroup AS itemgroupcode, 0.0 AS usagevalueidr, (SELECT ISNULL(genother1,'') acctgoid FROM QL_mstgen gx WHERE gx.cmpcode='" & DDLBusUnit.SelectedValue & "' AND gx.gengroup='GROUPITEM' AND gx.gencode=m.itemgroup AND activeflag='ACTIVE') AS stockacctgoid, '' AS toleransi, 5 wotolerance FROM QL_trnwodtl3 reqd INNER JOIN QL_trnwomst wom ON wom.cmpcode=reqd.cmpcode AND wom.womstoid=reqd.womstoid INNER JOIN QL_mstitem m ON m.itemoid=reqd.wodtl3refoid INNER JOIN QL_mstgen g ON g.genoid=reqd.wodtl3unitoid INNER JOIN QL_mstgen gx ON gx.genoid=m.itemunit1 WHERE reqd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqd.womstoid=" & womstoid.Text & " AND reqd.wodtl3reqflag='' ORDER BY reqd.wodtl3seq"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstmat")
        If dt.Rows.Count > 0 Then
            For C1 As Integer = 0 To dt.Rows.Count - 1
                dt.Rows(C1)("usageqty") = ToDouble(dt.Rows(C1)("reqqty").ToString)
                dt.Rows(C1)("toleransi") = ToDouble(dt.Rows(C1)("wotolerance").ToString) & " %"
            Next
            Session("TblMat") = dt
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        Else
            showMessage("Material data can't be found!", 2)
        End If
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("TblUsageDtl")
        dtlTable.Columns.Add("usagedtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("reqmstoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("reqno", Type.GetType("System.String"))
        dtlTable.Columns.Add("reqdate", Type.GetType("System.String"))
        dtlTable.Columns.Add("usagewhoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("usagewh", Type.GetType("System.String"))
        dtlTable.Columns.Add("reqdtloid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("matlongdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("usageqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("usageqty_unitkecil", Type.GetType("System.Double"))
        dtlTable.Columns.Add("usageqty_unitbesar", Type.GetType("System.Double"))
        dtlTable.Columns.Add("reqqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("usagelimitqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("stockqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("usageunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("stockunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("usageunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("usagedtlnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("usagevalueidr", Type.GetType("System.Double"))
        dtlTable.Columns.Add("usagevalueusd", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wono", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemgroup", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemgroupcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("stockacctgoid", Type.GetType("System.String"))
        dtlTable.Columns.Add("wotolerance", Type.GetType("System.Double"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub ClearDetail()
        usagedtlseq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                usagedtlseq.Text = objTable.Rows.Count + 1
                EnableHeader(False)
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        i_u2.Text = "New Detail"
        reqmstoid.Text = ""
        reqno.Text = ""
        reqdate.Text = ""
        usagewhoid.SelectedIndex = -1
        reqdtloid.Text = ""
        matoid.Text = ""
        matcode.Text = ""
        matlongdesc.Text = ""
        usagelimitqty.Text = ""
        usageqty.Text = ""
        reqqty.Text = ""
        stockqty.Text = ""
        usageunitoid.SelectedIndex = -1
        usagedtlnote.Text = ""
        gvDtl.SelectedIndex = -1
        btnSearchMat.Visible = True
        itemgroup.Text = ""
        itemgroupdesc.Text = ""
        wotolerance.Text = ""
        wodtl3qty.Text = ""
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal : DDLBusUnit.CssClass = sCss
        deptoid.Enabled = bVal : deptoid.CssClass = sCss
        usagedate.Enabled = bVal : usagedate.CssClass = sCss : imbPRDate.Visible = bVal
    End Sub

    Private Sub generateUsageNo()
        Dim sNo As String = "USG-" & Format(GetServerTime(), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(usageno, " & DefaultFormatCounter & ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnusagemst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND usageno LIKE '%" & sNo & "%'"
        usageno.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
    End Sub

    Private Sub SetUsageValue(ByRef objVal As VarUsageValue)
        objVal.Val_IDR = 0
        objVal.Val_USD = 0
        Dim dt As DataTable = Session("TblDtl")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            Dim dValIDR As Double = GetStockValue(dt.Rows(C1)("matoid").ToString)
            Dim dValUSD As Double = GetStockValue(dt.Rows(C1)("matoid").ToString, "USD")
            dt.Rows(C1)("usagevalueidr") = dValIDR
            dt.Rows(C1)("usagevalueusd") = dValUSD
            objVal.Val_IDR += dValIDR
            objVal.Val_USD += dValUSD
        Next
        Session("TblDtl") = dt
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT usm.cmpcode, usagemstoid AS usagerawmstoid, usm.periodacctg, usagedate AS usagerawdate, usageno AS usagerawno, deptoid, usagemstnote AS usagerawmstnote, usagemststatus AS usagerawmststatus, usm.createuser, usm.createtime, usm.upduser, usm.updtime, matoid_mix, qty_mix, unitoid_mix, wom.womstoid, wom.wodate, wom.womstres2 AS wodesc, wono, usm.deptoid FROM QL_trnusagemst usm INNER JOIN QL_trnwomst wom ON usm.cmpcode=wom.cmpcode AND usm.womstoid=wom.womstoid WHERE usagemstoid=" & sOid
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = xreader("cmpcode").ToString
                DDLBusUnit_SelectedIndexChanged(Nothing, Nothing)
                usagemstoid.Text = xreader("usagerawmstoid").ToString
                periodacctg.Text = xreader("periodacctg").ToString
                usagedate.Text = Format(xreader("usagerawdate"), "MM/dd/yyyy")
                usageno.Text = xreader("usagerawno").ToString
                deptoid.SelectedValue = xreader("deptoid").ToString
                usagemstnote.Text = xreader("usagerawmstnote").ToString
                usagemststatus.Text = xreader("usagerawmststatus").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
                matoid_mix.Text = xreader("matoid_mix").ToString
                womstoid.Text = xreader("womstoid").ToString
                wono.Text = xreader("wono").ToString
                itemdesc.Text = xreader("wodesc").ToString
                wodate.Text = Format(xreader("wodate"), "MM/dd/yyyy")
                InitDDLDept()
                deptoid.SelectedValue = xreader("deptoid").ToString
                If ToInteger(matoid_mix.Text) <> 0 Then
                    EnableQtyMix(True)
                Else
                    EnableQtyMix(False)
                End If
                qty_mix.Text = ToMaskEdit(ToDouble(xreader("qty_mix").ToString), 2)
                If qty_mix.Text = "0" Then
                    qty_mix.Text = ""
                End If
                unitoid_mix.SelectedValue = xreader("unitoid_mix").ToString
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            xreader.Close()
            conn.Close()
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False : btnPost.Visible = False
            Exit Sub
        End Try
        If usagemststatus.Text <> "In Process" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnPost.Visible = False
            lblspuno.Text = "Usage No."
            usagemstoid.Visible = False
            usageno.Visible = True
            btnAddToList.Visible = False
            gvDtl.Columns(0).Visible = False
            gvDtl.Columns(gvDtl.Columns.Count - 1).Visible = False
        End If
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        sSql = "SELECT mud.usagedtlseq AS usagedtlseq, mud.reqmstoid AS reqmstoid, '' AS reqno, CONVERT(VARCHAR(10), reqm.wodate, 101) AS reqdate, mud.usagewhoid AS usagewhoid, g1.gendesc AS usagewh, mud.reqdtloid AS reqdtloid, mud.matoid AS matoid, m.itemcode AS matcode, m.itemLongDescription AS matlongdesc, m.roundQty AS usagelimitqty, mud.usageqty AS usageqty, (reqd.wodtl3qty - (ISNULL((SELECT SUM(x.usageqty) FROM QL_trnusagedtl x WHERE x.cmpcode=mud.cmpcode AND x.reqdtloid=mud.reqdtloid AND x.usagemstoid<>mud.usagemstoid), 0))) AS reqqty, (select dbo.getstockqty(reqd.wodtl3refoid, " & usagewhoid.SelectedValue & ")) AS stockqty, mud.usageunitoid AS usageunitoid, g2.gendesc AS usageunit, mud.usagedtlnote AS usagedtlnote, ISNULL((SELECT wono FROM QL_trnwomst wom WHERE wom.cmpcode=reqm.cmpcode AND wom.womstoid=reqm.womstoid), '') AS wono, (SELECT ISNULL(genother1,'') acctgoid FROM QL_mstgen gx WHERE gx.cmpcode='" & CompnyCode & "' AND gx.gengroup='GROUPITEM' AND gx.gencode=m.itemgroup AND activeflag='ACTIVE') AS stockacctgoid, (CASE m.itemgroup WHEN 'Raw' THEN 'RAW MATERIAL' WHEN 'Gen' THEN 'GENERAL MATERIAL' WHEN 'Wip' THEN 'WIP' ELSE 'FINISH GOOD' END) AS itemgroup, m.itemgroup AS itemgroupcode, 0.0 AS usagevalueidr, 0.0000 AS usagevalueusd, usageqty_unitkecil, usageqty_unitbesar, gx.genoid AS stockunitoid, wotolerance FROM QL_trnusagedtl mud INNER JOIN QL_trnwodtl3 reqd ON reqd.cmpcode=mud.cmpcode AND reqd.wodtl3oid=mud.reqdtloid INNER JOIN QL_trnwomst reqm ON reqm.cmpcode=reqd.cmpcode AND reqm.womstoid=reqd.womstoid INNER JOIN QL_mstitem m ON m.itemoid=mud.matoid INNER JOIN QL_mstgen g1 ON g1.genoid=mud.usagewhoid INNER JOIN QL_mstgen g2 ON g2.genoid=mud.usageunitoid INNER JOIN QL_mstgen gx ON gx.genoid=m.itemunit1 WHERE mud.usagemstoid=" & sOid & " ORDER BY usagedtlseq"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnusagerawdtl")
        Session("TblDtl") = dt
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        ClearDetail()
    End Sub

    Private Sub EnableQtyMix(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        qty_mix.Enabled = bVal : qty_mix.CssClass = sCss
        unitoid_mix.Enabled = bVal : unitoid_mix.CssClass = sCss
    End Sub

    Private Sub BindListMatMix()
        sSql = "SELECT * FROM (SELECT matrawoid AS matoid_mix, matrawcode AS matcode_mix, matrawlongdesc AS matlongdesc_mix, gendesc AS matunit_mix, matrawunitoid AS unitoid_mix FROM QL_mstmatraw m INNER JOIN QL_mstgen g ON g.cmpcode=m.cmpcode AND genoid=matrawunitoid WHERE m.cmpcode='" & CompnyCode & "' AND LEFT(matrawcode, 2) IN (SELECT cat1code FROM QL_mstcat1 c1 WHERE c1.cmpcode='" & CompnyCode & "' AND cat1oid IN (SELECT c0d.cat1oid FROM QL_mstcat0dtl c0d INNER JOIN QL_mstoid oi ON oi.cmpcode=c0d.cmpcode AND lastoid=cat0oid WHERE c0d.cmpcode='" & CompnyCode & "' AND tablename='SET MATERIAL MIXING'))) AS tbl_MatMix WHERE " & FilterDDLListMatMix.SelectedValue & " LIKE '%" & Tchar(FilterTextListMatMix.Text) & "%' ORDER by matcode_mix"
        FillGV(gvListMatMix, sSql, "tbl_MatMix")
    End Sub

    Private Sub BindListKIK()
        sSql = "SELECT DISTINCT wom.womstoid, wono, CONVERT(VARCHAR(10), wodate, 101) AS wodate,wom.womstres2 itemlongdesc FROM QL_trnwomst wom INNER JOIN QL_trnwodtl3 wod3 ON wod3.cmpcode=wom.cmpcode AND wod3.womstoid=wom.womstoid WHERE wom.cmpcode='" & DDLBusUnit.SelectedValue & "' AND womststatus='Post' AND " & FilterDDLListKIK.SelectedValue & " LIKE '%" & Tchar(FilterTextListKIK.Text) & "%' AND wod3.wodtl3reqflag='' ORDER BY wono"
        FillGV(gvListKIK, sSql, "QL_trnwomst")
    End Sub

#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnUsageKIK.aspx")
        End If
        If checkPagePermission("~\Transaction\trnUsageKIK.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Material Usage KIK"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPost.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            CheckUsageStatus()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitAllDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                usagemstoid.Text = GenerateID("QL_TRNUSAGEMST", CompnyCode)
                usagedate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                usagemststatus.Text = "In Process"
                periodacctg.Text = GetDateToPeriodAcctg(CDate(usagedate.Text))
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvDtl.DataSource = dt
            gvDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnUsageKIK.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lkbUsageInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbUsageInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, mum.updtime, GETDATE()) > " & nDays & " AND mum.usagerawmststatus='In Process'"
        If checkPagePermission("~\Transaction\trnUsageKIK.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND mum.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND mum.usagedate>='" & FilterPeriod1.Text & " 00:00:00' AND mum.usagedate<='" & FilterPeriod2.Text & "'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND mum.usagemststatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Transaction\trnUsageKIK.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND mum.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        cbStatus.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Transaction\trnUsageKIK.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND mum.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        UpdateCheckedValue()
        PrintReport()
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLDept()
        deptoid_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub deptoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles deptoid.SelectedIndexChanged
        btnClearReq_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnSearchReq_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchReq.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        If deptoid.SelectedValue = "" Then
            showMessage("Please select Department first!", 2)
            Exit Sub
        End If
        FilterDDLListMatReq.SelectedIndex = -1 : FilterTextListMatReq.Text = "" : gvListMatReq.SelectedIndex = -1
        BindRequestData()
        cProc.SetModalPopUpExtender(btnHideListMatReq, pnlListMatReq, mpeListMatReq, True)
    End Sub

    Protected Sub btnClearReq_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearReq.Click
        reqmstoid.Text = "" : reqno.Text = "" : reqdate.Text = "" : usagewhoid.SelectedIndex = -1
    End Sub

    Protected Sub btnFindListMatReq_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMatReq.Click
        BindRequestData()
        mpeListMatReq.Show()
    End Sub

    Protected Sub btnAllListMatReq_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMatReq.Click
        FilterDDLListMatReq.SelectedIndex = -1 : FilterTextListMatReq.Text = "" : gvListMatReq.SelectedIndex = -1
        BindRequestData()
        mpeListMatReq.Show()
    End Sub

    Protected Sub gvListMatReq_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMatReq.PageIndexChanging
        gvListMatReq.PageIndex = e.NewPageIndex
        BindRequestData()
        mpeListMatReq.Show()
    End Sub

    Protected Sub gvListMatReq_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListMatReq.SelectedIndexChanged
        If reqmstoid.Text <> gvListMatReq.SelectedDataKey.Item("reqmstoid").ToString Then
            btnClearReq_Click(Nothing, Nothing)
        End If
        reqmstoid.Text = gvListMatReq.SelectedDataKey.Item("reqmstoid").ToString
        reqno.Text = gvListMatReq.SelectedDataKey.Item("reqno").ToString
        usagewhoid.SelectedValue = gvListMatReq.SelectedDataKey.Item("reqwhoid").ToString
        reqdate.Text = gvListMatReq.SelectedDataKey.Item("reqdate").ToString
        wono.Text = gvListMatReq.SelectedDataKey.Item("wono").ToString
        cProc.SetModalPopUpExtender(btnHideListMatReq, pnlListMatReq, mpeListMatReq, False)
        btnSearchMat_Click(Nothing, Nothing)
    End Sub

    Protected Sub lbCloseListMatReq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMatReq.Click
        cProc.SetModalPopUpExtender(btnHideListMatReq, pnlListMatReq, mpeListMatReq, False)
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        Dim sErr As String = ""
        If usagedate.Text = "" Then
            showMessage("Please fill Usage Date first!", 2)
            Exit Sub
        Else
            If Not IsValidDate(usagedate.Text, "MM/dd/yyyy", sErr) Then
                showMessage("Usage Date is invalid. " & sErr, 2)
                usagedate.Text = ""
                Exit Sub
            End If
        End If
        If womstoid.Text = "" Then
            showMessage("Please select SPK first!", 2)
            Exit Sub
        End If
        If usagewhoid.SelectedValue = "" Then
            showMessage("Please select Warehouse first!", 2)
            Exit Sub
        End If
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        Session("TblMat") = Nothing : Session("TblMatView") = Nothing : gvListMat.DataSource = Nothing : gvListMat.DataBind()
        BindMaterialData()
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        Dim sPlus As String = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub cbHdrListMat_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim bVal As Boolean = sender.Checked
        For C1 As Integer = 0 To gvListMat.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = bVal
                    End If
                Next
            End If
        Next
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If Not Session("TblMat") Is Nothing Then
            UpdateCheckedMat()
            UpdateCheckedMat2()
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(5).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 4)
                    End If
                End If
            Next
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            Dim str As String = e.Row.Cells(1).Text
            e.Row.Cells(1).Text = Mid(str, 1, 7) & vbCrLf & Mid(str, 8, 10) & vbCrLf & Mid(str, 18, 5)
        End If
    End Sub

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    dtView.RowFilter = ""
                    dtView.RowFilter = "checkvalue='True' AND usageqty>0"
                    If dtView.Count <> iCheck Then
                        Session("WarningListMat") = "Usage Qty for every checked material data must be more than 0!"
                        showMessage(Session("WarningListMat"), 2)
                        dtView.RowFilter = ""
                        Exit Sub
                    End If
                    dtView.RowFilter = ""
                    dtView.RowFilter = "checkvalue='True' AND usageqty<=reqqty"
                    If dtView.Count <> iCheck Then
                        ' Session("WarningListMat") = "Usage Qty for every checked material data must be less or equal than SPK Qty + TOLERANCE!"
                        ' showMessage(Session("WarningListMat"), 2)
                        ' dtView.RowFilter = ""
                        ' Exit Sub
                    End If
                    dtView.RowFilter = ""
                    dtView.RowFilter = "checkvalue='True'"
                    If Not IsQtyRounded(dtView, "usageqty", "matlimitqty") Then
                        Session("WarningListMat") = "Usage Qty for every selected Material must be rounded by Round Qty!"
                        showMessage(Session("WarningListMat"), 2)
                        dtView.RowFilter = ""
                        Exit Sub
                    End If
                    dtView.RowFilter = ""
                    dtView.RowFilter = "checkvalue='True'"
                    If Session("TblDtl") Is Nothing Then
                        CreateTblDetail()
                    End If
                    Dim objTable As DataTable
                    objTable = Session("TblDtl")
                    Dim dv As DataView = objTable.DefaultView
                    Dim objRow As DataRow
                    Dim counter As Integer = objTable.Rows.Count
                    For C1 As Integer = 0 To dtView.Count - 1
                        dv.RowFilter = "reqdtloid=" & dtView(C1)("reqdtloid")
                        If dv.Count > 0 Then
                            dv.AllowEdit = True
                            dv(0)("usageqty") = dtView(C1)("usageqty")
                            dv(0)("usagedtlnote") = dtView(C1)("usagedtlnote")
                            dv(0)("wotolerance") = dtView(C1)("wotolerance")
                        Else
                            counter += 1
                            objRow = objTable.NewRow()
                            objRow("usagedtlseq") = counter
                            objRow("reqmstoid") = womstoid.Text
                            objRow("reqno") = ""
                            objRow("reqdate") = "1/1/1900"
                            objRow("usagewhoid") = usagewhoid.SelectedValue
                            objRow("usagewh") = usagewhoid.SelectedItem.Text
                            objRow("reqdtloid") = dtView(C1)("reqdtloid")
                            objRow("matoid") = dtView(C1)("matoid")
                            objRow("matcode") = dtView(C1)("matcode")
                            objRow("matlongdesc") = dtView(C1)("matlongdesc")
                            objRow("usagelimitqty") = ToDouble(dtView(C1)("matlimitqty"))
                            objRow("reqqty") = ToDouble(dtView(C1)("reqqty"))
                            objRow("stockqty") = ToDouble(dtView(C1)("stockqty"))
                            objRow("usageqty") = ToDouble(dtView(C1)("usageqty"))
                            objRow("usageunitoid") = dtView(C1)("requnitoid")
                            objRow("usageunit") = dtView(C1)("requnit")
                            objRow("usagedtlnote") = dtView(C1)("usagedtlnote")
                            objRow("itemgroup") = dtView(C1)("itemgroup")
                            objRow("itemgroupcode") = dtView(C1)("itemgroupcode")
                            objRow("stockacctgoid") = dtView(C1)("stockacctgoid")
                            objRow("usagevalueidr") = dtView(C1)("usagevalueidr")
                            objRow("wono") = wono.Text
                            objRow("wotolerance") = dtView(C1)("wotolerance")
                            objTable.Rows.Add(objRow)
                        End If
                        dv.RowFilter = ""
                    Next
                    dtView.RowFilter = ""
                    Session("TblDtl") = objTable
                    gvDtl.DataSource = objTable
                    gvDtl.DataBind()
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                    ClearDetail()
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            If Session("TblDtl") Is Nothing Then
                CreateTblDetail()
            End If
            Dim dQty_unitkecil, dQty_unitbesar As Double
            Dim objTable As DataTable = Session("TblDtl")
            Dim dv As DataView = objTable.DefaultView
            If i_u2.Text = "New Detail" Then
                dv.RowFilter = "reqdtloid=" & reqdtloid.Text
            Else
                dv.RowFilter = "usagedtlseq<>" & usagedtlseq.Text & " AND reqdtloid=" & reqdtloid.Text
            End If
            If dv.Count > 0 Then
                dv.RowFilter = ""
                showMessage("This Data has been added before, please check!", 2)
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                objRow("usagedtlseq") = objTable.Rows.Count + 1
            Else
                objRow = objTable.Rows(usagedtlseq.Text - 1)
                objRow.BeginEdit()
            End If
            objRow("reqmstoid") = reqmstoid.Text
            objRow("reqno") = reqno.Text
            objRow("reqdate") = reqdate.Text
            objRow("usagewhoid") = usagewhoid.SelectedValue
            objRow("usagewh") = usagewhoid.SelectedItem.Text
            objRow("reqdtloid") = reqdtloid.Text
            objRow("matoid") = matoid.Text
            objRow("matcode") = matcode.Text
            objRow("matlongdesc") = matlongdesc.Text
            objRow("usageqty") = ToDouble(usageqty.Text)
            objRow("usagelimitqty") = ToDouble(usagelimitqty.Text)
            objRow("reqqty") = ToDouble(wodtl3qty.Text)
            objRow("stockqty") = ToDouble(stockqty.Text)
            objRow("usageunitoid") = usageunitoid.SelectedValue
            objRow("usageunit") = usageunitoid.SelectedItem.Text
            objRow("usagedtlnote") = usagedtlnote.Text
            objRow("wono") = wono.Text
            objRow("itemgroup") = itemgroup.Text
            objRow("itemgroupcode") = itemgroupdesc.Text
            If i_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            Session("TblDtl") = objTable
            gvDtl.DataSource = Session("TblDtl")
            gvDtl.DataBind()
            ClearDetail()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
        End If
    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDtl.SelectedIndexChanged
        Try
            usagedtlseq.Text = gvDtl.SelectedDataKey.Item("usagedtlseq").ToString().Trim
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "usagedtlseq=" & usagedtlseq.Text
                reqmstoid.Text = dv.Item(0).Item("reqmstoid").ToString
                'reqno.Text = dv.Item(0).Item("reqno").ToString
                'reqdate.Text = dv.Item(0).Item("reqdate").ToString
                usagewhoid.SelectedValue = dv.Item(0).Item("usagewhoid").ToString
                reqdtloid.Text = dv.Item(0).Item("reqdtloid").ToString
                matoid.Text = dv.Item(0).Item("matoid").ToString
                matcode.Text = dv.Item(0).Item("matcode").ToString
                matlongdesc.Text = dv.Item(0).Item("matlongdesc").ToString
                usageqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("usageqty").ToString), 2)
                wodtl3qty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("reqqty").ToString), 2)
                stockqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("stockqty").ToString), 2)
                usageunitoid.SelectedValue = dv.Item(0).Item("usageunitoid")
                usagedtlnote.Text = dv.Item(0).Item("usagedtlnote").ToString
                itemgroup.Text = dv.Item(0).Item("itemgroupcode").ToString
                itemgroupdesc.Text = dv.Item(0).Item("itemgroup").ToString
                wotolerance.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("wotolerance").ToString), 2)
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        Finally
            btnSearchReq.Visible = False : btnClearReq.Visible = False : btnSearchMat.Visible = False
        End Try
    End Sub

    Protected Sub gvDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("usagedtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        ClearDetail()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_trnusagemst WHERE usagemstoid=" & usagemstoid.Text
                If CheckDataExists(sSql) Then
                    usagemstoid.Text = GenerateID("QL_TRNUSAGEMST", CompnyCode)
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_trnusagemst", "usagemstoid", usagemstoid.Text, "usagemststatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    usagemststatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            usagedtloid.Text = GenerateID("QL_TRNUSAGEDTL", CompnyCode)
            Dim conmtroid As Integer = 0, crdmatoid As Integer = 0, glmstoid As Integer = 0, gldtloid As Integer = 0, conmtroid_mix As Integer = 0, crdmatoid_mix As Integer = 0, iStockValOid As Integer = 0
            Dim iPostAcctgOid As Integer = 0, iStockAcctgOid As Integer = 0
            Dim objValue As VarUsageValue
            Dim sNo As String = ""
            periodacctg.Text = GetDateToPeriodAcctg(CDate(usagedate.Text))
            Dim cRate As New ClassRate
            Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
            Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
            Dim iGroupOid As Integer = 0
            If usagemststatus.Text = "Post" Then
                If isPeriodClosed(DDLBusUnit.SelectedValue, sPeriod) Then
                    showMessage("Cannot continue this transaction anymore because the period : " & MonthName(CInt(sPeriod.Substring(4, 2))) & " " & sPeriod.Substring(0, 2) & " have been closed.", 3)
                    Exit Sub
                End If
                cRate.SetRateValue("IDR", sDate)
                If cRate.GetRateDailyLastError <> "" Then
                    showMessage(cRate.GetRateDailyLastError, 2)
                    usagemststatus.Text = "In Process"
                    Exit Sub
                End If
                If cRate.GetRateMonthlyLastError <> "" Then
                    showMessage(cRate.GetRateMonthlyLastError, 2)
                    usagemststatus.Text = "In Process"
                    Exit Sub
                End If
                conmtroid = GenerateID("QL_CONSTOCK", CompnyCode) : crdmatoid = GenerateID("QL_CRDSTOCK", CompnyCode)
                glmstoid = GenerateID("QL_TRNGLMST", CompnyCode) : gldtloid = GenerateID("QL_TRNGLDTL", CompnyCode)
                conmtroid_mix = GenerateID("QL_CONMAT_MIX", CompnyCode) : crdmatoid_mix = GenerateID("QL_CRDMTR_MIX", CompnyCode)
                iStockValOid = GenerateID("QL_STOCKVALUE", CompnyCode)
                Dim sVarErr As String = ""
                'sNo = GetFGDescription(wono.Text)
                iPostAcctgOid = GetAcctgOID(GetVarInterface("VAR_USAGE_KIK", DDLBusUnit.SelectedValue), CompnyCode)
                'dtSumStock = GetAmtByGroup(Session("TblDtl"))
                SetUsageValue(objValue)
                generateUsageNo()
                iGroupOid = ToInteger(GetStrData("SELECT TOP 1 groupoid FROM QL_mstdeptgroupdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND deptoid=" & deptoid.SelectedValue & " ORDER BY updtime DESC"))
            End If
            ' Define Tabel Strucure utk Auto Jurnal 
            sSql = "SELECT '' tipe, 0 acctgoid, 0.0 debet, 0.0 credit, '' refno, '' note, '' AS glother1 "
            Dim tbPostGL As DataTable = cKon.ambiltabel(sSql, "PostGL")
            tbPostGL.Rows.Clear()
            Dim oRow As DataRow

            Dim dvCek As DataView = tbPostGL.DefaultView
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trnusagemst (cmpcode, usagemstoid, periodacctg, usagedate, usageno, deptoid, usagemstnote, usagemststatus, createuser, createtime, upduser, updtime, matoid_mix, qty_mix, unitoid_mix, womstoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & usagemstoid.Text & ", '" & periodacctg.Text & "', '" & usagedate.Text & "', '" & usageno.Text & "', " & deptoid.SelectedValue & ", '" & Tchar(usagemstnote.Text) & "', '" & usagemststatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToInteger(matoid_mix.Text) & ", " & ToDouble(qty_mix.Text) & ", " & unitoid_mix.SelectedValue & ", " & womstoid.Text & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & usagemstoid.Text & " WHERE tablename='QL_TRNUSAGEMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trnusagemst SET periodacctg='" & periodacctg.Text & "', usagedate='" & usagedate.Text & "', usageno='" & usageno.Text & "', deptoid=" & deptoid.SelectedValue & ", usagemstnote='" & Tchar(usagemstnote.Text) & "', usagemststatus='" & usagemststatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, matoid_mix=" & ToInteger(matoid_mix.Text) & ", qty_mix=" & ToDouble(qty_mix.Text) & ", unitoid_mix=" & unitoid_mix.SelectedValue & ", womstoid=" & womstoid.Text & " WHERE usagemstoid=" & usagemstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnwodtl3 SET wodtl3reqflag='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl3oid IN (SELECT reqdtloid FROM QL_trnusagedtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagemstoid=" & usagemstoid.Text & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnwomst SET wotempstatus_tmp='Post' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid IN (SELECT reqmstoid FROM QL_trnusagedtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagemstoid=" & usagemstoid.Text & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trnusagedtl WHERE usagemstoid=" & usagemstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trnusagedtl (cmpcode, usagedtloid, usagemstoid, usagedtlseq, reqmstoid, reqdtloid, matoid, usageqty, usageunitoid, usagewhoid, usagedtlnote, usagedtlstatus, upduser, updtime, usagevalueidr, usagevalueusd, usageqty_unitkecil, usageqty_unitbesar) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (C1 + CInt(usagedtloid.Text)) & ", " & usagemstoid.Text & ", " & C1 + 1 & ", " & objTable.Rows(C1).Item("reqmstoid") & ", " & objTable.Rows(C1).Item("reqdtloid") & ", " & objTable.Rows(C1).Item("matoid") & ", " & ToDouble(objTable.Rows(C1).Item("usageqty").ToString) & ", " & objTable.Rows(C1).Item("usageunitoid") & ", " & objTable.Rows(C1).Item("usagewhoid") & ", '" & Tchar(objTable.Rows(C1).Item("usagedtlnote")) & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(C1).Item("usagevalueidr").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("usagevalueusd").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("usageqty").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("usageqty_unitbesar").ToString) & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        If objTable.Rows(C1).Item("usageqty") >= objTable.Rows(C1).Item("reqqty") Then
                            sSql = "UPDATE QL_trnwodtl3 SET wodtl3reqflag='COMPLETE' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl3oid=" & objTable.Rows(C1).Item("reqdtloid")
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_trnwomst SET wotempstatus_tmp='Closed' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid=" & objTable.Rows(C1).Item("reqmstoid") & " AND (SELECT COUNT(*) FROM QL_trnwodtl3 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid=" & objTable.Rows(C1).Item("reqmstoid") & " AND wodtl3oid<>" & objTable.Rows(C1).Item("reqdtloid") & " AND wodtl3reqflag='')=0"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                        If usagemststatus.Text = "Post" Then
                            ' Insert QL_conmat
                            sSql = "INSERT INTO QL_constock (cmpcode, constockoid, contype, typemin, trndate, periodacctg, formaction, formoid, refoid, refname, mtrlocoid, qtyin, qtyout, reason, note, upduser, updtime, refno, deptoid, valueidr, valueusd, qtyin_unitbesar, qtyout_unitbesar, createuser, createtime) VALUES ('" & DDLBusUnit.SelectedValue & "', " & conmtroid & ", 'MU', -1, '" & sDate & "', '" & sPeriod & "', 'QL_trnusagedtl', " & usagemstoid.Text & ", " & objTable.Rows(C1).Item("matoid") & ", '" & objTable.Rows(C1).Item("itemgroup").ToString & "', " & objTable.Rows(C1).Item("usagewhoid") & ", 0, " & ToDouble(objTable.Rows(C1).Item("usageqty")) & ", 'Material Usage', '" & usageno.Text & " # " & wono.Text & " (" & Tchar(itemdesc.Text) & ")" & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & objTable.Rows(C1)("wono").ToString & "', " & deptoid.SelectedValue & ", " & ToDouble(objTable.Rows(C1).Item("usagevalueidr").ToString) & ", " & ToDouble(objTable.Rows(C1).Item("usagevalueusd").ToString) & ", 0, " & ToDouble(objTable.Rows(C1).Item("usageqty_unitbesar").ToString) & ", '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            conmtroid += 1
                            ' Insert QL_crdmtr
                            sSql = "UPDATE QL_crdstock SET qtyout=qtyout + " & ToDouble(objTable.Rows(C1).Item("usageqty")) & ", saldoakhir=saldoakhir - " & ToDouble(objTable.Rows(C1).Item("usageqty")) & ", lasttranstype='QL_trnusagedtl', lasttransdate='" & sDate & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND refoid=" & objTable.Rows(C1).Item("matoid") & " AND mtrlocoid=" & objTable.Rows(C1).Item("usagewhoid") & " AND periodacctg='" & sPeriod & "'"
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() = 0 Then
                                sSql = "INSERT INTO QL_crdstock (cmpcode, crdstockoid, periodacctg, refoid, refname, mtrlocoid, qtyin, qtyout, qtyadjin, qtyadjout, saldoawal, saldoakhir, lasttranstype, lasttransdate, upduser, updtime, createuser, createtime, closeuser, closingdate) VALUES ('" & DDLBusUnit.SelectedValue & "', " & crdmatoid & ", '" & sPeriod & "', " & objTable.Rows(C1).Item("matoid") & ", '" & objTable.Rows(C1).Item("itemgroup").ToString & "', " & objTable.Rows(C1).Item("usagewhoid") & ", 0, " & ToDouble(objTable.Rows(C1).Item("usageqty")) & ", 0, 0, 0, " & -ToDouble(objTable.Rows(C1).Item("usageqty")) & ", 'QL_trnusagedtl', '" & sDate & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', '1/1/1900')"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                crdmatoid += 1
                            End If

                            dvCek.RowFilter = "acctgoid=" & objTable.Rows(C1).Item("stockacctgoid").ToString & " "
                            If dvCek.Count > 0 Then
                                dvCek(0)("credit") += ToDouble(objTable.Rows(C1).Item("usagevalueidr").ToString) * ToDouble(objTable.Rows(C1).Item("usageqty").ToString)
                                dvCek.RowFilter = ""
                            Else
                                dvCek.RowFilter = ""

                                oRow = tbPostGL.NewRow
                                oRow("acctgoid") = objTable.Rows(C1).Item("stockacctgoid")
                                oRow("debet") = 0
                                oRow("credit") = ToDouble(objTable.Rows(C1).Item("usagevalueidr").ToString) * ToDouble(objTable.Rows(C1).Item("usageqty").ToString)

                                tbPostGL.Rows.Add(oRow)
                            End If
                            tbPostGL.AcceptChanges()

                            'Insert STockvalue
                            sSql = GetQueryUpdateStockValue(-ToDouble(objTable.Rows(C1)("usageqty").ToString), 0, ToDouble(objTable.Rows(C1).Item("usagevalueidr").ToString), ToDouble(objTable.Rows(C1).Item("usagevalueusd").ToString), "QL_trnusagedtl", sDate, Session("UserID"), DDLBusUnit.SelectedValue, sPeriod, objTable.Rows(C1)("matoid"))
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() = 0 Then
                                sSql = GetQueryInsertStockValue(-ToDouble(objTable.Rows(C1)("usageqty").ToString), 0, ToDouble(objTable.Rows(C1).Item("usagevalueidr").ToString), ToDouble(objTable.Rows(C1).Item("usagevalueusd").ToString), "QL_trnusagedtl", sDate, Session("UserID"), DDLBusUnit.SelectedValue, sPeriod, objTable.Rows(C1)("matoid"), iStockValOid)
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iStockValOid += 1
                            End If
                        End If
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(usagedtloid.Text)) & " WHERE tablename='QL_TRNUSAGEDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    If usagemststatus.Text = "Post" Then
                        sSql = "UPDATE QL_mstoid SET lastoid=" & conmtroid - 1 & " WHERE tablename='QL_CONSTOCK' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        sSql = "UPDATE QL_mstoid SET lastoid=" & crdmatoid - 1 & " WHERE tablename='QL_CRDSTOCK' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        sSql = "UPDATE QL_mstoid SET lastoid=" & iStockValOid - 1 & " WHERE tablename='QL_STOCKVALUE' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                    If usagemststatus.Text = "Post" Then
                        Dim sValue As Double = tbPostGL.Compute("SUM(credit)", "").ToString
                        If sValue > 0 Then
                            ' Insert GL MST
                            sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & glmstoid & ", '" & sDate & "', '" & sPeriod & "', 'Usage |No. " & usageno.Text & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            ' Insert GL DTL
                            Dim iSeq As Integer = 1
                            ' PEMAKAIAN
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, groupoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & iPostAcctgOid & ", 'D', " & sValue & ", '" & usageno.Text & "', 'Usage |No. " & usageno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & sValue * cRate.GetRateMonthlyIDRValue & ", " & sValue * cRate.GetRateMonthlyUSDValue & ", 'QL_trnusagemst " & usagemstoid.Text & "', " & iGroupOid & ")"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iSeq += 1
                            gldtloid += 1

                            ' PERSEDIAAN
                            For C1 As Integer = 0 To tbPostGL.Rows.Count - 1
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, groupoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & tbPostGL.Rows(C1)("acctgoid") & ", 'C', " & ToDouble(tbPostGL.Rows(C1)("credit")) & ", '" & usageno.Text & "', 'Usage |No. " & usageno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & tbPostGL.Rows(C1)("credit") * cRate.GetRateMonthlyIDRValue & ", " & tbPostGL.Rows(C1)("credit") * cRate.GetRateMonthlyUSDValue & ", 'QL_trnusagemst " & usagemstoid.Text & "', " & iGroupOid & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iSeq += 1
                                gldtloid += 1
                            Next
                            sSql = "UPDATE QL_mstoid SET lastoid=" & glmstoid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_mstoid SET lastoid=" & gldtloid & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                    End If
                End If
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.ToString, 1)
                        usagemststatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.ToString, 1)
                    usagemststatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.ToString, 2)
                conn.Close()
                usagemststatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & usagemstoid.Text & ".<BR>"
            End If
            If usagemststatus.Text = "Post" Then
                Session("SavedInfo") &= "Data have been posted with Usage No. = " & usageno.Text & "."
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trnUsageKIK.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnUsageKIK.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If usagemstoid.Text = "" Then
            showMessage("Please select Raw Material Usage data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_trnusagemst", "usagemstoid", usagemstoid.Text, "usagemststatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                usagemststatus.Text = "In Process"
                Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trnwodtl3 SET wodtl3reqflag='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl3oid IN (SELECT reqdtloid FROM QL_trnusagedtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagemstoid=" & usagemstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_trnwomst SET wotempstatus_tmp='Post' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid IN (SELECT reqmstoid FROM QL_trnusagedtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagemstoid=" & usagemstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnusagedtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagemstoid=" & usagemstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnusagemst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND usagemstoid=" & usagemstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnUsageKIK.aspx?awal=true")
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPost.Click
        usagemststatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnSearchMatMix_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMatMix.Click
        FilterDDLListMatMix.SelectedIndex = -1 : FilterTextListMatMix.Text = "" : gvListMatMix.SelectedIndex = -1
        BindListMatMix()
        cProc.SetModalPopUpExtender(btnHideListMatMix, pnlListMatMix, mpeListMatMix, True)
    End Sub

    Protected Sub btnClearMatMix_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearMatMix.Click
        matoid_mix.Text = "" : matlongdesc_mix.Text = "" : qty_mix.Text = "" : unitoid_mix.SelectedIndex = -1 : EnableQtyMix(False)
    End Sub

    Protected Sub btnFindListMatMix_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMatMix.Click
        BindListMatMix()
        mpeListMatMix.Show()
    End Sub

    Protected Sub btnAllListMatMix_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMatMix.Click
        FilterDDLListMatMix.SelectedIndex = -1 : FilterTextListMatMix.Text = "" : gvListMatMix.SelectedIndex = -1
        BindListMatMix()
        mpeListMatMix.Show()
    End Sub

    Protected Sub gvListMatMix_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMatMix.PageIndexChanging
        gvListMatMix.PageIndex = e.NewPageIndex
        BindListMatMix()
        mpeListMatMix.Show()
    End Sub

    Protected Sub gvListMatMix_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListMatMix.SelectedIndexChanged
        matoid_mix.Text = gvListMatMix.SelectedDataKey.Item("matoid_mix").ToString
        matlongdesc_mix.Text = gvListMatMix.SelectedDataKey.Item("matlongdesc_mix").ToString
        unitoid_mix.SelectedValue = gvListMatMix.SelectedDataKey.Item("unitoid_mix").ToString
        EnableQtyMix(True)
        cProc.SetModalPopUpExtender(btnHideListMatMix, pnlListMatMix, mpeListMatMix, False)
    End Sub

    Protected Sub lbCloseListMatMix_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMatMix.Click
        cProc.SetModalPopUpExtender(btnHideListMatMix, pnlListMatMix, mpeListMatMix, False)
    End Sub

    Protected Sub usageqty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dQty_unitkecil, dQty_unitbesar As Double
        GetUnitConverter(matoid.Text, usageunitoid.SelectedValue, ToDouble(usageqty.Text), dQty_unitkecil, dQty_unitbesar)
        usageqty_unitkecil.Text = dQty_unitkecil
    End Sub

    Protected Sub btnSearchKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        FilterDDLListKIK.SelectedIndex = -1 : FilterTextListKIK.Text = "" : gvListKIK.SelectedIndex = -1
        BindListKIK()
        cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, True)
    End Sub

    Protected Sub btnClearKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        womstoid.Text = "" : wono.Text = "" : wodate.Text = ""
        itemdesc.Text = ""
    End Sub

    Protected Sub btnFindListKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindListKIK()
        mpeListKIK.Show()
    End Sub

    Protected Sub btnAllListKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListKIK.SelectedIndex = -1 : FilterTextListKIK.Text = "" : gvListKIK.SelectedIndex = -1
        BindListKIK()
        mpeListKIK.Show()
    End Sub

    Protected Sub gvListKIK_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If womstoid.Text <> gvListKIK.SelectedDataKey.Item("womstoid").ToString Then
            btnClearKIK_Click(Nothing, Nothing)
        End If
        womstoid.Text = gvListKIK.SelectedDataKey.Item("womstoid").ToString
        wono.Text = gvListKIK.SelectedDataKey.Item("wono").ToString
        wodate.Text = gvListKIK.SelectedDataKey.Item("wodate").ToString
        itemdesc.Text = gvListKIK.SelectedDataKey.Item("itemlongdesc").ToString
        'InitDDLDept2() : deptoid_SelectedIndexChanged(Nothing, Nothing)
        cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, False)
    End Sub

    Protected Sub lkbCloseListKIK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, False)
    End Sub
#End Region

End Class
