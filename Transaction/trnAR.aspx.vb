Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports ClassFunction
Imports System.Drawing.Printing
Imports System.Management
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_AR
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function 'OK

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If i_u2.Text = "New Detail" Then
            If shipmentrawmstoid.Text = "" Then
                sError &= "- Please select Shipment first.<BR>"
            End If
        Else
            If ToDouble(shipmentrawdtloid.Text) = 0 Then sError &= "- No A/R Detail selected.<BR>"
            If ToDouble(arrawprice.Text) < 0 Then sError &= "- Price Per Unit must be greater than 0.<BR>"

            If ToDouble(arrawdtlnetto.Text) < 0 Then sError &= "- Detail Netto must be greater than 0.<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetDetailShipment(ByVal sOid As String) As DataTable
        Dim dtReturn As DataTable = Nothing
        sSql = "SELECT sd.shipmentrawmstoid, shipmentrawdtloid, shipmentrawdtlseq, sd.matrawoid, matrawcode, matrawlongdesc, (shipmentrawqty - ISNULL((SELECT SUM(sretrawqty) FROM QL_trnsretrawdtl sretd INNER JOIN QL_trnsretrawmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.sretrawmstoid=sretd.sretrawmstoid WHERE sretd.cmpcode=sd.cmpcode AND sretd.shipmentrawdtloid=sd.shipmentrawdtloid AND sretrawmststatus<>'Rejected'), 0.0)) AS shipmentrawqty, shipmentrawunitoid, gendesc AS unit, dorawprice AS sorawprice, '' AS sorawdtldisctype, 0.0 AS sorawdtldiscvalue FROM QL_trnshipmentrawdtl sd INNER JOIN QL_mstmatraw m ON m.matrawoid=sd.matrawoid INNER JOIN QL_mstgen g ON genoid=shipmentrawunitoid INNER JOIN QL_trndorawdtl dod ON dod.cmpcode=sd.cmpcode AND dod.dorawdtloid=sd.dorawdtloid WHERE sd.cmpcode='" & DDLBusUnit.SelectedValue & "' AND sd.shipmentrawmstoid=" & sOid & " AND shipmentrawdtlstatus='' AND (shipmentrawqty - ISNULL((SELECT SUM(sretrawqty) FROM QL_trnsretrawdtl sretd INNER JOIN QL_trnsretrawmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.sretrawmstoid=sretd.sretrawmstoid WHERE sretd.cmpcode=sd.cmpcode AND sretd.shipmentrawdtloid=sd.shipmentrawdtloid AND sretrawmststatus<>'Rejected'), 0.0)) > 0 ORDER BY shipmentrawdtlseq"
        dtReturn = cKon.ambiltabel(sSql, "QL_trnshipmentrawdtl")
        Return dtReturn
    End Function 'OK

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select Bussiness Unit first.<BR>"
        End If
        If arrawdate.Text = "" Then
            sError &= "- Please fill PEB Date field.<BR>"
            sErr = "Error"
        Else
            If Not IsValidDate(arrawdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- PEB Date is invalid. " & sErr & "<BR>"
            End If
        End If
        If custoid.Text = "" Then
            sError &= "- Please select Customer first.<BR>"
        End If
       
        Dim sErr2 As String = ""
        If arrawdatetakegiro.Text <> "" Then
            If Not IsValidDate(arrawdatetakegiro.Text, "MM/dd/yyyy", sErr2) Then
                sError &= "- Date Take Giro is invalid. " & sErr2 & "<BR>"
            Else
                If sErr = "" Then
                    If CDate(arrawdate.Text) > CDate(arrawdatetakegiro.Text) Then
                        sError &= "- Date Take Giro must be more or equal than PEB Date.<BR>"
                    End If
                End If
            End If
        End If
        If dateETD.Text <> "" Then
            If Not IsValidDate(dateETD.Text, "MM/dd/yyyy", sErr2) Then
                sError &= "- Date date ETD is invalid. " & sErr2 & "<BR>"
            Else
                If sErr = "" Then
                    If CDate(arrawdate.Text) > CDate(dateETD.Text) Then
                        sError &= "- date ETD must be more or equal than PEB Date.<BR>"
                    End If
                End If
            End If
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill A/R Detail data.<BR>"
        Else
            Dim objTbl As DataTable = Session("TblDtl")
            If objTbl.Rows.Count <= 0 Then
                sError &= "- Please fill A/R Detail data.<BR>"
            Else
                If sErr = "" Then
                    For C1 As Integer = 0 To objTbl.Rows.Count - 1
                        'If CDate(arrawdate.Text) < CDate(objTbl.Rows(C1)("shipmentrawdate").ToString) Then
                        '    sError &= "- PEB Date must be more or equal than every Shipment ETD.<BR>"
                        '    Exit For
                        'End If
                    Next
                End If
            End If
        End If
        If paymentType.Text.ToUpper <> "CASH" Then
            If curroid.SelectedItem.ToString <> "IDR" Then
                If ToDouble(arrawtotalnetto.Text) > ToDouble(CRFreeUSD.Text) Then
                    sError &= "- Total Netto > Limit Credit Customer!<BR>"
                End If
            Else
                If ToDouble(arrawtotalnetto.Text) > ToDouble(CRFreeIDR.Text) Then
                    'sError &= "- Total Netto > Limit Credit Customer!<BR>"
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            arrawmststatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function 'OK

    Private Function GetNumericValues(ByVal sData As String) As String
        Dim sNumeric As String = ""
        Dim sCurr As String = ""
        For C1 As Integer = 1 To Len(sData)
            sCurr = Mid(sData, C1, 1)
            If IsNumeric(sCurr) Then
                sNumeric &= sCurr
            End If
        Next
        GetNumericValues = sNumeric
    End Function

    Private Function GetTaxValue() As Double
        GetTaxValue = 0
        Try
            sSql = "SELECT TOP 1 gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND gengroup='DEFAULT TAX' ORDER BY updtime DESC"
            GetTaxValue = ToDouble(GetStrData(sSql))
        Catch ex As Exception
            GetTaxValue = 0
        End Try
    End Function
#End Region

#Region "Procedures"
    Private Sub ReamountDetail()
        arrawprice.Text = ToMaskEdit(ToDouble(arrawprice.Text), 2)
        arrawdtlamt.Text = ToMaskEdit(ToDouble(arrawprice.Text) * ToDouble(arrawqty.Text), 2)
        arrawdtldiscvalue.Text = ToMaskEdit(ToDouble(arrawdtldiscvalue.Text), 2)
        If arrawdtldisctype.SelectedValue = "P" Then
            arrawdtldiscamt.Text = ToMaskEdit((ToDouble(arrawdtlamt.Text) * ToDouble(arrawdtldiscvalue.Text)) / 100, 2)
        Else
            arrawdtldiscamt.Text = ToMaskEdit(ToDouble(arrawdtldiscvalue.Text) * ToDouble(arrawqty.Text), 2)
        End If
        arrawdtlnetto.Text = ToMaskEdit(ToDouble(arrawdtlamt.Text) - ToDouble(arrawdtldiscamt.Text), 2)
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub 'OK

    Private Sub CheckSOStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_TRNARRAWMST WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND arrawmststatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnAR.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lkbAPInProcess.Visible = True
            lkbAPInProcess.Text = "You have " & GetStrData(sSql) & " In Process A/R Raw Material data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
        sSql = "SELECT COUNT(*) FROM QL_TRNARRAWMST WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND arrawmststatus='In Approval'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnAR.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lkbAPInApproval.Visible = True
            lkbAPInApproval.Text = "You have " & GetStrData(sSql) & " In Approval A/R Raw Material data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub 'OK

    Private Sub InitAllDDL()
        'Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE' "
        'AND divcode<>'" & CompnyCode & "'"
        'If Session("CompnyCode") <> CompnyCode Then
        '    sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        'End If
        FillDDL(DDLBusUnit, sSql)
        'Fill DDL Payment Type
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='PAYMENT TYPE' AND activeflag='ACTIVE'"
        FillDDL(arrawpaytypeoid, sSql)
        ' Fill DDL Currency
        sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        FillDDL(curroid, sSql)
        'fill DDL do type
        sSql = "SELECT gencode, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='GROUPITEM' AND activeflag='ACTIVE' ORDER BY gendesc"
        FillDDL(doType, sSql)
        'Isi Kode sales
        sSql = "select distinct salescode,salescode+'-'+personname from ql_mstperson p inner join QL_mstlevel g on g.leveloid =p.leveloid where upper(g.leveldesc)='SALES' and p.activeflag='ACTIVE'"
        FillDDL(DDLsales, sSql)
        'Fill DDL Printer
        'Dim pkInstalledPrinters As String = ""
        '' Find all printers installed
        'For Each pkInstalledPrinters In _
        '    PrinterSettings.InstalledPrinters
        '    DDLListPrinter.Items.Add(pkInstalledPrinters)
        'Next pkInstalledPrinters
    End Sub 'OK

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT arm.trnjualmstoid, div.divname, arm.trnjualno, CONVERT(CHAR(10), arm.trnjualdate, 101) AS trnjualdate, s.custname, arm.trnjualstatus, arm.trnjualnote, p.personname AS sales, arm.trnjualamtnetto AS netto, dom.dono, som.sono, 'False' AS checkvalue FROM QL_trnjualmst arm INNER JOIN QL_mstcust s ON arm.custoid=s.custoid INNER JOIN QL_mstdivision div ON div.cmpcode=arm.cmpcode inner join QL_mstperson p on p.salescode=arm.salescode inner join QL_trndomst dom ON dom.domstoid=arm.domstoid inner join QL_trnsomst som ON som.somstoid=dom.somstoid WHERE s.activeflag='ACTIVE' AND arm.trnjualmstoid > 0 " & sSqlPlus & " ORDER BY CONVERT(DATETIME, arm.trnjualdate) DESC, arm.trnjualmstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_TRNJUALMST")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub 'OK

    Private Sub BindCustomer()
        sSql = "SELECT s.custoid,s.custcode,s.custname,s.custaddr,s.custpaymentoid,custtaxable FROM QL_mstcust s WHERE s.cmpcode='" & CompnyCode & "' AND " & DDLFilterCust.SelectedValue & " LIKE '%" & Tchar(txtFilterCust.Text) & "%' AND s.activeflag='ACTIVE' AND s.custoid IN (SELECT custoid FROM QL_trnshipmentrawmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND shipmentrawmststatus='Approved') ORDER BY s.custname"
        FillGV(gvCust, sSql, "QL_mstcust")
    End Sub 'OK

    Private Sub BindListSO()
        sSql = "SELECT som.sorawmstoid, som.sorawno, CONVERT(VARCHAR(10), som.sorawdate, 101) AS sorawdate, g.gendesc AS sorawpaytype, som.sorawmstnote FROM QL_trnsorawmst som INNER JOIN QL_mstgen g ON g.genoid=som.sorawpaytypeoid WHERE som.cmpcode='" & DDLBusUnit.SelectedValue & "' AND som.custoid=" & custoid.Text & " AND som.sorawmstoid IN (SELECT sorawmstoid FROM QL_trndorawmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dorawmststatus='Approved') AND " & FilterDDLListSO.SelectedValue & " LIKE '%" & Tchar(FilterTextListSO.Text) & "%' ORDER BY som.sorawdate DESC, som.sorawmstoid DESC"
        FillGV(gvListSO, sSql, "QL_trnsorawmst")
    End Sub 'OK

    Private Sub SetSOData(ByVal sOid As String)
        sSql = "SELECT som.sorawno, som.sorawpaytypeoid, som.curroid, som.rateoid, r.rateidrvalue, som.sorawmstdisctype, (CASE som.sorawmstdisctype WHEN 'A' THEN (som.sorawmstdiscvalue - ISNULL((SELECT SUM(arrawmstdiscvalue) FROM QL_TRNARRAWMST WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND sorawmstoid=" & sOid & " AND arrawmststatus<>'Rejected'), 0)) ELSE som.sorawmstdiscvalue END) AS sorawmstdiscvalue, som.sorawtaxtype, som.sorawtaxamt, (som.sorawdeliverycost - ISNULL((SELECT SUM(arrawdeliverycost) FROM QL_TRNARRAWMST WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND sorawmstoid=" & sOid & " AND arrawmststatus<>'Rejected'), 0)) AS sorawdeliverycost, (som.sorawothercost - ISNULL((SELECT SUM(arrawothercost) FROM QL_TRNARRAWMST WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND sorawmstoid=" & sOid & " AND arrawmststatus<>'Rejected'), 0)) AS sorawothercost FROM QL_trnsorawmst som INNER JOIN QL_mstrate r ON som.rateoid=r.rateoid WHERE som.cmpcode='" & DDLBusUnit.SelectedValue & "' AND som.sorawmstoid=" & sOid
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        While xreader.Read
            'sorawno.Text = Trim(xreader("sorawno").ToString)
            arrawpaytypeoid.SelectedValue = xreader("sorawpaytypeoid")
            curroid.SelectedValue = xreader("curroid").ToString
            curroid_SelectedIndexChanged(Nothing, Nothing)
            rateoid.Text = xreader("rateoid").ToString
            rateidrvalue.Text = ToMaskEdit(ToDouble(Trim(xreader("rateidrvalue").ToString)), 2)
            arrawmstdisctype.SelectedValue = Trim(xreader("sorawmstdisctype").ToString)
            If arrawmstdisctype.SelectedValue = "P" Then
                lblPOHdrDisc.Visible = False
                SeptPOHdrDisc.Visible = False
                sorawmstdiscvalue.Visible = False
                sorawmstdiscvalue.Text = "0"
                arrawmstdiscvalue.Text = ToMaskEdit(ToDouble(Trim(xreader("sorawmstdiscvalue").ToString)), 2)
            Else
                lblPOHdrDisc.Visible = True
                SeptPOHdrDisc.Visible = True
                sorawmstdiscvalue.Visible = True
                sorawmstdiscvalue.Text = ToMaskEdit(ToDouble(Trim(xreader("sorawmstdiscvalue").ToString)), 2)
                arrawmstdiscvalue.Text = "0"
            End If
            arrawtaxtype.SelectedValue = Trim(xreader("sorawtaxtype").ToString)
            arrawtaxamt.Text = ToMaskEdit(ToDouble(Trim(xreader("sorawtaxamt").ToString)), 2)
        End While
        xreader.Close()
        conn.Close()
        CountHdrAmount()
    End Sub 'OK

    Private Sub CountHdrAmount()
        CountDtlAmount()
        If arrawmstdiscvalue.Text = "" Then
            arrawmstdiscvalue.Text = "0"
        End If
        If arrawmstdisctype.SelectedValue = "P" Then
            arrawmstdiscamt.Text = ToMaskEdit(((ToDouble(arrawtotalamt.Text) - ToDouble(arrawtotaldiscdtl.Text)) * ToDouble(arrawmstdiscvalue.Text)) / 100, 2)
        Else
            arrawmstdiscamt.Text = ToMaskEdit(ToDouble(arrawmstdiscvalue.Text), 2)
        End If
        arrawtotaldisc.Text = ToMaskEdit(ToDouble(arrawtotaldiscdtl.Text) + ToDouble(arrawmstdiscamt.Text), 2)
        arrawtotalnetto.Text = ToMaskEdit(ToDouble(arrawtotalamt.Text) - ToDouble(arrawtotaldisc.Text), 2)
        If arrawtaxtype.SelectedValue = "NON TAX" Then
            arrawvat.Text = "0"
            arrawtaxamt.Text = "0"
        Else
            If arrawtaxamt.Text = "" Then
                arrawtaxamt.Text = "0"
            End If
            arrawvat.Text = ToMaskEdit((ToDouble(arrawtotalnetto.Text) * ToDouble(arrawtaxamt.Text)) / 100, 2)
        End If
        If arrawdeliverycost.Text = "" Then
            arrawdeliverycost.Text = "0.00"
        End If
        If arrawothercost.Text = "" Then
            arrawothercost.Text = "0.00"
        End If
        arrawtotalcost.Text = ToMaskEdit(ToDouble(arrawdeliverycost.Text) + ToDouble(arrawothercost.Text), 2)
        arrawgrandtotal.Text = ToMaskEdit(ToDouble(arrawtotalnetto.Text) + ToDouble(arrawvat.Text) + ToDouble(arrawtotalcost.Text), 2)
    End Sub 'OK

    Private Sub CountDtlAmount()
        If Not Session("TblDtl") Is Nothing Then
            Dim TotalAmount As Double = 0
            Dim TotalDisc As Double = 0
            Dim objTable As DataTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                For C1 As Int16 = 0 To objTable.Rows.Count - 1
                    TotalAmount += ToDouble(objTable.Rows(C1).Item("arrawdtlamt").ToString)
                    TotalDisc += ToDouble(objTable.Rows(C1).Item("arrawdtldiscamt").ToString)
                Next
                arrawtotalamt.Text = ToMaskEdit(TotalAmount, 2)
                arrawtotaldiscdtl.Text = ToMaskEdit(TotalDisc, 2)
            End If
        End If
    End Sub 'OK

    Private Sub BindListShipment()
        sSql = "SELECT DISTINCT sm.shipmentrawmstoid, shipmentrawno, CONVERT(VARCHAR(10), shipmentrawetd, 101) AS shipmentrawdate, shipmentrawmstnote, dorawno, dorawcustpono FROM QL_trnshipmentrawmst sm INNER JOIN QL_trnshipmentrawdtl sd ON sd.cmpcode=sm.cmpcode AND sd.shipmentrawmstoid=sm.shipmentrawmstoid INNER JOIN QL_trndorawmst dom ON dom.cmpcode=sd.cmpcode AND dom.dorawmstoid=sd.dorawmstoid WHERE sm.cmpcode='" & DDLBusUnit.SelectedValue & "' AND sm.custoid=" & custoid.Text & " AND shipmentrawmststatus='Approved' AND " & FilterDDLListDO.SelectedValue & " LIKE '%" & Tchar(FilterTextListDO.Text) & "%' AND sm.curroid=" & curroid.SelectedValue & " ORDER BY shipmentrawdate DESC, sm.shipmentrawmstoid DESC"
        FillGV(gvListDO, sSql, "QL_trnshipmentrawmst")
    End Sub 'OK

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("QL_TRNJUALDTL")
        dtlTable.Columns.Add("trnjualdtlseq", Type.GetType("System.Int32"))
        'dtlTable.Columns.Add("shipmentrawmstoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("dono", Type.GetType("System.String"))
        dtlTable.Columns.Add("dodate", Type.GetType("System.String"))
        'dtlTable.Columns.Add("shipmentrawdtloid", Type.GetType("System.Int32"))
        'dtlTable.Columns.Add("matrawoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("qty", Type.GetType("System.Double"))
        'dtlTable.Columns.Add("arrawunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("unit", Type.GetType("System.String"))
        dtlTable.Columns.Add("price", Type.GetType("System.Double"))
        dtlTable.Columns.Add("taxdtlamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("disc1", Type.GetType("System.String"))
        dtlTable.Columns.Add("disc2", Type.GetType("System.Double"))
        dtlTable.Columns.Add("disc3", Type.GetType("System.Double"))
        dtlTable.Columns.Add("netto", Type.GetType("System.Double"))
        dtlTable.Columns.Add("note", Type.GetType("System.String"))
        Session("TblDtl") = dtlTable
    End Sub 'OK

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "trnjualmstoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub GenerateJualMstNo()
        Dim type As String = ""
        If arrawtaxtype.SelectedValue = "INC" Then
            type = "T"
        Else
            type = "N"
        End If
        Dim sJM As String = "LA" & type & "-" & Format(GetServerTime(), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trnjualno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnjualmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnjualno LIKE '%" & sJM & "%'"
        Session("sJM") = GenNumberString(sJM, "", GetStrData(sSql), DefaultFormatCounter)
    End Sub

    Private Sub PrintReport()
        Try
            Dim UserName As String
            sSql = "SELECT profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"
            UserName = GetStrData(sSql)
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("trnjualmstoid").ToString & ","
                    'Untuk menyimpan Print ke 
                    sSql = "SELECT trnjualstatus FROM QL_trnjualmst WHERE cmpcode='" & Session("CompnyCode") & "' AND trnjualmstoid=" & dv(C1)("trnjualmstoid")
                    Dim sStatus As String = GetStrData(sSql)
                    If sStatus <> "In Process" Then
                        sSql = "SELECT printseq FROM QL_trnjualmst WHERE cmpcode='" & Session("CompnyCode") & "' AND trnjualmstoid=" & dv(C1)("trnjualmstoid")
                        Dim Print As Double = cKon.ambilscalar(sSql)
                        If Print >= 0 Then
                            Print += 1
                        End If
                        If conn.State = ConnectionState.Closed Then
                            conn.Open()
                        End If
                        sSql = "UPDATE QL_trnjualmst SET printseq=" & Print & ", lastprintdate='" & GetServerTime() & "', lastprintuser='" & Session("UserID") & "' WHERE trnjualmstoid=" & dv(C1)("trnjualmstoid") & ""
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        conn.Close()
                    End If
                Next
                dv.RowFilter = ""
            End If
            If CBprint.Checked Then
                If DDLType.SelectedIndex = 0 Then
                    report.Load(Server.MapPath(folderReport & "rptARTrn.rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptARNota.rpt"))
                End If
            Else
                report.Load(Server.MapPath(folderReport & "rptARTrn.rpt"))
            End If

            Dim sWhere As String = ""
            sWhere &= " WHERE arm.cmpcode='" & Session("CompnyCode") & "'"
            If sOid = "" Then
                sWhere &= " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND arm.trnjualdate>='" & FilterPeriod1.Text & " 00:00:00' AND arm.trnjualdate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sWhere &= " AND arm.trnjualstatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sWhere &= " AND arm.trnjualmstoid IN (" & sOid & ")"
            End If
            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            cProc.SetDBLogonForReport(report)
            'If Not cbPdf.Checked Then
            '    'Direct Print
            '    report.PrintOptions.PrinterName = "@\\SERVER\EPSON LX-300+ II" 'DDLListPrinter.SelectedValue
            '    report.PrintToPrinter(1, False, 0, 0)
            'End If
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            'If cbPdf.Checked Then
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "SalesInvoicePrintOut")
            'End If
            report.Close()
            report.Dispose()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            report.Close()
            report.Dispose()
        End Try
        ' Response.Redirect("~\Transaction\trnAR.aspx?awal=true")
    End Sub

    'Private Sub PrintReport(ByVal sOid As String)
    '    Try
    '        If DDLRateType.SelectedIndex = 0 Then
    '            report.Load(Server.MapPath(folderReport & "rptarraw.rpt"))
    '        ElseIf DDLRateType.SelectedIndex = 1 Then
    '            report.Load(Server.MapPath(folderReport & "rptarrawIDR.rpt"))
    '        Else
    '            report.Load(Server.MapPath(folderReport & "rptarrawUSD.rpt"))
    '        End If
    '        Dim sWhere As String
    '        If Session("CompnyCode") <> CompnyCode Then
    '            sWhere = " WHERE arm.cmpcode='" & Session("CompnyCode") & "'"
    '        Else
    '            sWhere = " WHERE arm.cmpcode LIKE '%'"
    '        End If
    '        If sOid = "" Then
    '            sWhere &= " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
    '            If cbPeriode.Checked Then
    '                If IsValidPeriod() Then
    '                    sWhere &= " AND arm.arrawdate>='" & FilterPeriod1.Text & " 00:00:00' AND arm.arrawdate<='" & FilterPeriod2.Text & " 23:59:59'"
    '                Else
    '                    Exit Sub
    '                End If
    '            End If
    '            If cbStatus.Checked Then
    '                If FilterDDLStatus.SelectedValue <> "All" Then
    '                    sWhere &= " AND arm.arrawmststatus='" & FilterDDLStatus.SelectedValue & "'"
    '                End If
    '            End If
    '            If checkPagePermission("~\Transaction\trnAR.aspx", Session("SpecialAccess")) = False Then
    '                sWhere &= " AND arm.createuser='" & Session("UserID") & "'"
    '            End If
    '        Else
    '            sWhere &= " AND arm.arrawmstoid=" & sOid
    '        End If
    '        report.SetParameterValue("sWhere", sWhere)
    '        cProc.SetDBLogonForReport(report)
    '        Dim sRptName As String = "ARRawPrintOut_" & Format(GetServerTime(), "yyyyMMddHHmmss") & ".pdf"
    '        Dim sRptPath As String = Server.MapPath("~/Files/" & sRptName)
    '        report.ExportToDisk(ExportFormatType.PortableDocFormat, sRptPath)
    '        btnContinue.OnClientClick = "javascript:window.open('../Files/" & sRptName & "');"
    '        btnContinue.AlternateText = "Open File"
    '        btnContinue.ImageUrl = "~/Images/openfile.png"
    '        report.Close()
    '        report.Dispose()
    '    Catch ex As Exception
    '        report.Close()
    '        report.Dispose()
    '        showMessage(ex.Message, 1)
    '    End Try
    'End Sub

    Private Sub ClearDetail()
        arrawdtlseq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                EnableHeader(False)
            Else
                EnableHeader(True)
            End If
            arrawdtlseq.Text = objTable.Rows.Count + 1
        Else
            EnableHeader(True)
        End If
        i_u2.Text = "New Detail"
        shipmentrawmstoid.Text = ""
        shipmentrawno.Text = ""
        dateETD.Text = ""
        arrawdtlnote.Text = ""
        gvTblDtl.SelectedIndex = -1
        btnSearchDO.Visible = True : btnClearDO.Visible = True
        TD1.Visible = False : TD2.Visible = False : TD3.Visible = False : TD4.Visible = False
        TD5.Visible = False : TD6.Visible = False : TD7.Visible = False : TD8.Visible = False
        TD9.Visible = False : TD10.Visible = False : TD11.Visible = False : TD12.Visible = False
        TD13.Visible = False : TD14.Visible = False : TD15.Visible = False : TD16.Visible = False
        TD17.Visible = False : TD18.Visible = False : TD19.Visible = False : TD20.Visible = False
        TD21.Visible = False : TD22.Visible = False : TD23.Visible = False : TD24.Visible = False
        TD25.Visible = False : TD26.Visible = False : TD23.Visible = False : TD27.Visible = False
        TD28.Visible = False : TD22.Visible = False : TD29.Visible = False : TD30.Visible = False
        chkUpdDisc.Checked = True : chkUpdPrice.Checked = True
        gvTblDtl.Columns(0).Visible = False
        gvTblDtl.Columns(gvTblDtl.Columns.Count - 1).Visible = True
    End Sub 'OK

    Private Sub EnableHeader(ByVal bVal As Boolean)
        DDLBusUnit.Enabled = bVal
        btnSearchCust.Visible = bVal
        btnClearCust.Visible = bVal
        If bVal = True Then
            DDLBusUnit.CssClass = "inpText"
        Else
            DDLBusUnit.CssClass = "inpTextDisabled"
        End If
    End Sub 'OK

    Private Sub FillTextBox(ByVal sOid As String)
        sSql = "SELECT arm.*,s.custname,dom.curroid,(SELECT ISNULL(SUM(trnjualdtldiscamt),0) from QL_trnjualdtl jd WHERE jd.trnjualmstoid=arm.trnjualmstoid) AS disc1, (SELECT ISNULL(SUM(trnjualdtldiscamt2),0) from QL_trnjualdtl jd WHERE jd.trnjualmstoid=arm.trnjualmstoid) AS disc2, (SELECT ISNULL(SUM(trnjualdtldiscamt3),0) from QL_trnjualdtl jd WHERE jd.trnjualmstoid=arm.trnjualmstoid) AS disc3, (select ISNULL(SUM(dod.valueidr*dod.doqty),0) from QL_trnjualdtl jd inner join ql_trndodtl dod ON dod.dodtloid=jd.dodtloid WHERE jd.trnjualmstoid=arm.trnjualmstoid) AS valueidr, 0.0 AS valueusd, p.salescode, (p.salescode+'-'+p.personname) AS sales, komisisales, ISNULL(g1.gendesc,'') AS paymenttype, (cg.custcreditlimitrupiah-cg.custcreditlimitusagerupiah) CRFreeIDR, (cg.custcreditlimitusd-cg.custcreditlimitusageusd) CRFreeUSD FROM QL_trnjualmst arm INNER JOIN QL_mstcust s ON arm.custoid=s.custoid INNER JOIN QL_trndomst dom ON dom.domstoid=arm.domstoid inner join ql_trnsomst som ON som.somstoid=dom.somstoid inner join QL_mstperson p ON p.salescode=som.sosalescode INNER JOIN QL_mstcustgroup cg ON s.custgroupoid=cg.custgroupoid LEFT JOIN QL_mstgen g1 ON g1.genoid=som.sopaytypeoid WHERE arm.trnjualmstoid=" & sOid
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        While xreader.Read
            DDLBusUnit.SelectedValue = xreader("cmpcode").ToString
            arrawmstoid.Text = Trim(xreader("trnjualmstoid").ToString)
            periodacctg.Text = Trim(xreader("periodacctg").ToString)
            arrawdate.Text = Format(xreader("trnjualdate"), "MM/dd/yyyy")
            Rdate2.Text = Format(xreader("receivedate"), "MM/dd/yyyy")
            Rdate.Text = Format(xreader("receivedate"), "MM/dd/yyyy")
            If Rdate.Text = "01/01/1900" Then
                Rdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
            Else
                Rdate.Text = Format(xreader("receivedate"), "MM/dd/yyyy")
            End If
            arrawno.Text = Trim(xreader("trnjualno").ToString)
            custoid.Text = Trim(xreader("custoid").ToString)
            CRFreeIDR.Text = ToMaskEdit(ToDouble(Trim(xreader("CRFreeIDR").ToString)), 2)
            CRFreeUSD.Text = ToMaskEdit(ToDouble(Trim(xreader("CRFreeUSD").ToString)), 2)
            paymentType.Text = Trim(xreader("paymenttype").ToString)
            custname.Text = Trim(xreader("custname").ToString)
            arrawpaytypeoid.SelectedValue = xreader("trnpaytype")
            curroid.SelectedValue = xreader("curroid").ToString
            curroid_SelectedIndexChanged(Nothing, Nothing)
            arrawtotalamt.Text = ToMaskEdit(ToDouble(Trim(xreader("trnjualamt").ToString)), 2)
            trnJualDPP.Text = ToMaskEdit(ToDouble(Trim(xreader("trnjualhpp").ToString)), 2)
            hpp.Text = ToMaskEdit(ToDouble(Trim(xreader("trnjualhpp").ToString)), 2)
            arrawmstdisctype.SelectedValue = Trim(xreader("trndisctype").ToString)
            arrawmstdiscvalue.Text = ToMaskEdit(ToDouble(Trim(xreader("trndiscvalue").ToString)), 2)
            arrawmstdiscamt.Text = ToMaskEdit(ToDouble(Trim(xreader("trndiscamt").ToString)), 2)
            arrawtotaldisc.Text = ToMaskEdit(ToDouble(Trim(xreader("trndiscamt").ToString)), 2)
            arrawtotalnetto.Text = ToMaskEdit(ToDouble(Trim(xreader("trnjualamtnetto").ToString)), 2)
            arrawtaxtype.SelectedValue = Trim(xreader("trntaxtype").ToString)
            arrawvat.Text = ToMaskEdit(ToDouble(Trim(xreader("trntaxamt").ToString)), 2)
            arrawmstnote.Text = Trim(xreader("trnjualnote").ToString)
            fakturpajak.Text = Trim(xreader("nofakturpajak").ToString)
            refno.Text = Trim(xreader("trnjualrefno").ToString)
            arrawmststatus.Text = Trim(xreader("trnjualstatus").ToString)
            valueidr.Text = Trim(xreader("valueidr").ToString)
            valueusd.Text = Trim(xreader("valueusd").ToString)
            DDLsales.SelectedValue = Trim(xreader("salescode").ToString)
            komisiSales.Text = ToMaskEdit(ToDouble(Trim(xreader("komisisales"))), 2)
            createuser.Text = Trim(xreader("crtuser").ToString)
            createtime.Text = Trim(xreader("crttime").ToString)
            upduser.Text = Trim(xreader("upduser").ToString)
            updtime.Text = Trim(xreader("updtime").ToString)
        End While
        xreader.Close()
        conn.Close()
        DDLBusUnit.CssClass = "inpTextDisabled"
        DDLBusUnit.Enabled = False
        arrawdate.CssClass = "inpText"
        arrawdatetakegiro.CssClass = "inpText"
        dateETD.CssClass = "inpText"
        arrawdate.Enabled = True
        arrawdatetakegiro.Enabled = True
        dateETD.Enabled = True
        lblRdate.Visible = False
        lblTitik.Visible = False
        lblPetik.Visible = False
        Rdate.Visible = False
        btnRSD.Visible = False
        Label49.Visible = False
        lblTrnNo.Text = "Invoice No."
        arrawno.Visible = True
        arrawmstoid.Visible = False
        BusinessUnit2.Visible = True
        If arrawmststatus.Text = "Approved" Or arrawmststatus.Text = "Closed" Or arrawmststatus.Text = "Post" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnSendApproval.Visible = False
            btnAddToList.Visible = False
            arrawdate.CssClass = "inpTextDisabled"
            arrawdatetakegiro.CssClass = "inpTextDisabled"
            dateETD.CssClass = "inpTextDisabled"
            arrawdate.Enabled = False
            arrawdatetakegiro.Enabled = False
            dateETD.Enabled = False
            lblRdate.Visible = True
            lblTitik.Visible = True
            lblPetik.Visible = True
            Rdate.Visible = True
            btnRSD.Visible = True
            Label49.Visible = True
            btnPostikng.Visible = False
            'If Rdate2.Text = "01/01/1900" Then
            '    btnUpdate.Visible = True
            'Else
            '    btnUpdate.Visible = False
            'End If
            btnUpdate.Visible = True
        ElseIf arrawmststatus.Text = "In Approval" Or arrawmststatus.Text = "Rejected" Then
            btnPrint2.Visible = False
            btnSave.Visible = False
            btnDelete.Visible = False
            btnSendApproval.Visible = False
            btnAddToList.Visible = False
            btnPostikng.Visible = False
        ElseIf arrawmststatus.Text = "In Process" Or arrawmststatus.Text = "Revised" Then
            btnPrint2.Visible = False
            btnPostikng.Visible = True
        End If

        sSql = "SELECT jd.dodtloid, jd.trnjualdtlseq, som.sono, dom.dono,CONVERT(CHAR(10),dom.dodate,101) AS dodate,i.itemCode AS itemcode,i.itemLongDescription AS itemdesc, jd.trnjualdtlqty AS qty, g.gendesc AS unit,jd.trnjualdtlprice AS price, jd.trnjualdtltaxamt AS taxdtlamt, jd.trnjualdtldiscamt AS disc1, jd.trnjualdtldiscamt2 AS disc2, jd.trnjualdtldiscamt3 AS disc3, case when som.sotaxtype='INC' then jd.trnjualdtldpp-(jd.trnjualdtldiscamt+ jd.trnjualdtldiscamt2+ jd.trnjualdtldiscamt3) else trnjualdtlnetto end AS netto, jd.trnjualdtlnote AS note from QL_trnjualdtl jd inner join QL_mstitem i on i.itemoid=jd.itemoid inner join QL_trndodtl dod on dod.dodtloid=jd.dodtloid inner join QL_trndomst dom on dom.domstoid=dod.domstoid inner join QL_mstgen g on g.genoid=jd.trnjualdtlunitoid inner join QL_trnsomst som ON som.somstoid=dom.somstoid WHERE jd.trnjualmstoid=" & sOid & " ORDER BY jd.trnjualdtlseq"
        Dim objTbl As DataTable = cKon.ambiltabel(sSql, "QL_TRNJUALDTL")
        Session("TblDtl") = objTbl
        gvTblDtl.DataSource = objTbl
        gvTblDtl.DataBind()
        'CountHdrAmount()
        'EnableHeader(False)
        'ClearDetail()
    End Sub 'OK

    'Private Sub PrintDoc(ByVal sOid As String)
    '    Try
    '        report.Load(Server.MapPath(folderReport & "rptarrawDoc.rpt"))
    '        Dim sWhere As String = " WHERE arm.arrawmstoid=" & sOid
    '        report.SetParameterValue("sWhere", sWhere)
    '        cProc.SetDBLogonForReport(report)
    '        Response.Buffer = False
    '        Response.ClearContent()
    '        Response.ClearHeaders()
    '        report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "AccountReceivableRMDoc_" & Format(GetServerTime(), "yyyyMMddHHmmss"))
    '        report.Close()
    '        report.Dispose()
    '    Catch ex As Exception
    '        report.Close()
    '        report.Dispose()
    '        showMessage(ex.Message, 1)
    '    End Try
    '    Response.Redirect("~\Transaction\trnAR.aspx?awal=true")
    'End Sub 'OK

    Private Sub ClearTempFile()
        Dim xDir As New IO.DirectoryInfo(Server.MapPath("~/Files/"))
        Dim xFileDir() As IO.FileInfo = xDir.GetFiles()
        For Each xFile As IO.FileInfo In xFileDir
            If File.Exists(xFile.FullName) Then
                If xFile.FullName.Contains("Raw") Then
                    Try
                        File.Delete(xFile.FullName)
                    Catch ex As Exception
                    End Try
                End If
            End If
        Next
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim scode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = scode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnAR.aspx")
        End If
        If checkPagePermission("~\Transaction\trnAR.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Sales Invoice"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnSendApproval.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to send this data for approval?');")
        btnPostikng.Attributes.Add("OnClick", "javascript:return confirm('Are you sure this data for posting?');")
        btnUpdate.Attributes.Add("OnClick", "javascript:return confirm('Are you sure this data for Update Receive Date?');")
        If Not Page.IsPostBack Then
            'ClearTempFile()
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            'CheckSOStatus()
            InitAllDDL()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            arrawdatetakegiro.Text = Format(GetServerTime(), "MM/dd/yyyy")
            dateETD.Text = Format(GetServerTime(), "MM/dd/yyyy")
            'Check Rate Pajak & Standart
            Dim cRate As New ClassRate()
            cRate.SetRateValue(1, Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, 2) : Exit Sub
            End If
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                arrawmstoid.Text = GenerateID("QL_TRNJUALMST", CompnyCode)
                arrawdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                curroid_SelectedIndexChanged(Nothing, Nothing)
                arrawmststatus.Text = "In Process"
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
                btnSave.Visible = False
                btnPostikng.Visible = False
                btnDelete.Visible = False
                btnPrint2.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvTblDtl.DataSource = dt
            gvTblDtl.DataBind()
        End If
    End Sub 'OK

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnAR.aspx?awal=true")
            End If
        End If
    End Sub 'OK

    Protected Sub lkbAPInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAPInProcess.Click
        FilterText.Text = ""
        FilterDDL.SelectedIndex = 0
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 1
        Dim nDays As Integer = 7
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, arm.updtime, GETDATE()) > " & nDays & " AND arm.arrawmststatus='In Process' "
        If Session("CompnyCode") <> CompnyCode Then
            sSqlPlus &= "AND arm.cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnAR.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND arm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub 'OK

    Protected Sub lkbAPInApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAPInApproval.Click
        FilterText.Text = ""
        FilterDDL.SelectedIndex = 0
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 3
        Dim nDays As Integer = 7
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, arm.updtime, GETDATE()) > " & nDays & " AND arm.arrawmststatus='In Approval' "
        If Session("CompnyCode") <> CompnyCode Then
            sSqlPlus &= "AND arm.cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnAR.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND arm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub 'OK

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If Session("CompnyCode") <> CompnyCode Then
            sSqlPlus &= " AND arm.cmpcode='" & Session("CompnyCode") & "' "
        End If
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND arm.trnjualdate>='" & FilterPeriod1.Text & " 00:00:00' AND arm.trnjualdate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            If FilterDDLStatus.SelectedValue <> "All" Then
                sSqlPlus &= " AND arm.trnjualstatus='" & FilterDDLStatus.SelectedValue & "'"
            End If
        End If
        'If checkPagePermission("~\Transaction\trnAR.aspx", Session("SpecialAccess")) = False Then
        '    sSqlPlus &= " AND arm.createuser='" & Session("UserID") & "'"
        'End If
        BindTrnData(sSqlPlus)
    End Sub 'OK

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        FilterDDL.SelectedIndex = 0
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = ""
        If Session("CompnyCode") <> CompnyCode Then
            sSqlPlus &= " AND arm.cmpcode='" & Session("CompnyCode") & "' "
        End If
        If checkPagePermission("~\Transaction\trnAR.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND arm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData((sSqlPlus))
    End Sub 'OK

    'Protected Sub gvTRN_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvTRN.SelectedIndexChanged
    '    btnContinue.OnClientClick = ""
    '    btnContinue.AlternateText = "Continue"
    '    btnContinue.ImageUrl = "~/Images/continue.png"
    '    lblRptOid.Text = gvTRN.SelectedDataKey.Item("arrawmstoid").ToString
    '    cProc.SetModalPopUpExtender(btnPnlApproval, pnlApproval, mpeApproval, True)
    'End Sub 'OK

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub 'OK

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        btnClearCust_Click(Nothing, Nothing)
    End Sub 'OK

    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchCust.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        gvCust.SelectedIndex = -1
        DDLFilterCust.SelectedIndex = -1
        txtFilterCust.Text = ""
        BindCustomer()
        cProc.SetModalPopUpExtender(btnHidCustSO, pnlListCustSO, mpeListCustSO, True)
    End Sub 'OK

    Protected Sub btnClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearCust.Click
        custname.Text = ""
        custoid.Text = ""
        arrawpaytypeoid.SelectedIndex = -1
        curroid.SelectedIndex = -1
        rateidrvalue.Text = "1.00"
        rateoid.Text = ""
        lblPOHdrDisc.Visible = False
        SeptPOHdrDisc.Visible = False
        sorawmstdiscvalue.Text = "0"
        sorawmstdiscvalue.Visible = False
        arrawmstdisctype.SelectedIndex = -1
        arrawmstdiscvalue.Text = "0"
        arrawtaxtype.SelectedIndex = 1
        arrawtaxamt.Text = ""
        btnClear_Click(Nothing, Nothing)
    End Sub 'OK

    Protected Sub btnFindCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindCust.Click
        BindCustomer()
        mpeListCustSO.Show()
    End Sub 'OK

    Protected Sub btnViewAllCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllCust.Click
        DDLFilterCust.SelectedIndex = -1
        txtFilterCust.Text = ""
        gvCust.SelectedIndex = -1
        BindCustomer()
        mpeListCustSO.Show()
    End Sub 'OK

    Protected Sub gvCust_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvCust.SelectedIndexChanged
        If gvCust.SelectedDataKey.Item("custoid").ToString().Trim <> custoid.Text Then
            btnClearCust_Click(Nothing, Nothing)
        End If
        custoid.Text = gvCust.SelectedDataKey.Item("custoid").ToString().Trim
        custname.Text = gvCust.SelectedDataKey.Item("custname").ToString().Trim
        arrawpaytypeoid.SelectedValue = gvCust.SelectedDataKey.Item("custpaymentoid").ToString
        Dim sErr As String = ""
        If arrawdate.Text <> "" Then
            If IsValidDate(arrawdate.Text, "MM/dd/yyyy", sErr) Then
                If arrawpaytypeoid.SelectedValue <> "" Then
                    Dim sNum As String = GetNumericValues(arrawpaytypeoid.SelectedItem.Text)
                    arrawdatetakegiro.Text = Format(DateAdd(DateInterval.Day, ToDouble(sNum), CDate(arrawdate.Text)), "MM/dd/yyyy")
                End If
            End If
        End If
        If gvCust.SelectedDataKey.Item("custtaxable").ToString = "1" Then
            arrawtaxtype.SelectedIndex = 0
            arrawtaxamt.Text = GetNewTaxValue(arrawdate.Text) 'ToMaskEdit(GetTaxValue(), 2)
        Else
            arrawtaxtype.SelectedIndex = 1
            arrawtaxamt.Text = ""
        End If
        cProc.SetModalPopUpExtender(btnHidCustSO, pnlListCustSO, mpeListCustSO, False)
    End Sub 'OK

    Protected Sub lkbCloseListCustSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListCustSO.Click
        cProc.SetModalPopUpExtender(btnHidCustSO, pnlListCustSO, mpeListCustSO, False)
    End Sub 'OK

    Protected Sub btnFindListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSO.Click
        BindListSO()
        mpeListSO.Show()
    End Sub 'OK

    Protected Sub btnAllListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListSO.Click
        gvListSO.SelectedIndex = -1
        FilterDDLListSO.SelectedIndex = -1
        FilterTextListSO.Text = ""
        BindListSO()
        mpeListSO.Show()
    End Sub 'OK

    Protected Sub gvListSO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListSO.SelectedIndexChanged
        ' sorawmstoid.Text = gvListSO.SelectedDataKey.Item("sorawmstoid").ToString
        ' SetSOData(sorawmstoid.Text)
        cProc.SetModalPopUpExtender(btnHideListSO, pnlListSO, mpeListSO, False)
    End Sub 'OK

    Protected Sub lkbCloseListSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListSO.Click
        cProc.SetModalPopUpExtender(btnHideListSO, pnlListSO, mpeListSO, False)
    End Sub 'OK

    Protected Sub apitemmstdisctype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles arrawmstdisctype.SelectedIndexChanged, arrawmstdiscvalue.TextChanged, arrawtaxtype.SelectedIndexChanged, arrawtaxamt.TextChanged, arrawdeliverycost.TextChanged, arrawothercost.TextChanged
        CountHdrAmount()
    End Sub 'OK

    Protected Sub curroid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles curroid.SelectedIndexChanged
        Dim sErr As String = ""
        If arrawdate.Text = "" Then
            showMessage("Please fill PEB Date first!", 2)
            Exit Sub
        Else
            If Not IsValidDate(arrawdate.Text, "MM/dd/yyyy", sErr) Then
                showMessage("PEB Date is invalid. " & sErr, 2)
                Exit Sub
            End If
        End If
        Dim cRate As New ClassRate()
        If curroid.SelectedValue <> "" Then
            cRate.SetRateValue(CInt(curroid.SelectedValue), arrawdate.Text)
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, 2)
                Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, 2)
                Exit Sub
            End If
            rateoid.Text = cRate.GetRateDailyOid
            rateidrvalue.Text = ToMaskEdit(cRate.GetRateDailyIDRValue, GetRoundValue(cRate.GetRateDailyIDRValue.ToString))
            arrawratetousd.Text = ToMaskEdit(cRate.GetRateDailyUSDValue, GetRoundValue(cRate.GetRateDailyUSDValue.ToString))
            rate2oid.Text = cRate.GetRateMonthlyOid
            rate2idrvalue.Text = ToMaskEdit(cRate.GetRateMonthlyIDRValue, GetRoundValue(cRate.GetRateMonthlyIDRValue.ToString))
            arrawrate2tousd.Text = ToMaskEdit(cRate.GetRateMonthlyUSDValue, GetRoundValue(cRate.GetRateMonthlyUSDValue.ToString))
        End If
    End Sub

    Protected Sub btnSearchDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchDO.Click
        If custoid.Text = "" Then
            showMessage("Please select Customer first!", 2)
            Exit Sub
        End If
        If curroid.SelectedValue = "" Then
            showMessage("Please select Currency first!", 2)
            Exit Sub
        End If
        gvListDO.SelectedIndex = -1
        FilterDDLListDO.SelectedIndex = -1
        FilterTextListDO.Text = ""
        BindListShipment()
        cProc.SetModalPopUpExtender(btnHideListDO, pnlListDO, mpeListDO, True)
    End Sub 'OK

    Protected Sub btnClearDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearDO.Click
        shipmentrawmstoid.Text = ""
        shipmentrawno.Text = ""
        dateETD.Text = ""
    End Sub 'OK

    Protected Sub btnFindListDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListDO.Click
        BindListShipment()
        mpeListDO.Show()
    End Sub 'OK

    Protected Sub btnAllListDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListDO.Click
        gvListDO.SelectedIndex = -1
        FilterDDLListDO.SelectedIndex = -1
        FilterTextListDO.Text = ""
        BindListShipment()
        mpeListDO.Show()
    End Sub 'OK

    Protected Sub gvListDO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListDO.PageIndexChanging
        gvListDO.PageIndex = e.NewPageIndex
        BindListShipment()
        mpeListDO.Show()
    End Sub 'OK

    Protected Sub gvListDO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListDO.SelectedIndexChanged
        ' shipmentrawmstoid,shipmentrawno,shipmentrawdate
        shipmentrawmstoid.Text = gvListDO.SelectedDataKey.Item("shipmentrawmstoid").ToString
        shipmentrawno.Text = gvListDO.SelectedDataKey.Item("shipmentrawno").ToString
        dateETD.Text = gvListDO.SelectedDataKey.Item("shipmentrawdate").ToString
        cProc.SetModalPopUpExtender(btnHideListDO, pnlListDO, mpeListDO, False)
    End Sub 'OK

    Protected Sub lkbCloseListDO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListDO.Click
        cProc.SetModalPopUpExtender(btnHideListDO, pnlListDO, mpeListDO, False)
    End Sub 'OK

    Protected Sub detailText_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles arrawprice.TextChanged, arrawdtldiscvalue.TextChanged
        ReamountDetail()
    End Sub

    Protected Sub arrawdtldisctype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles arrawdtldisctype.SelectedIndexChanged
        ReamountDetail()
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            If Session("TblDtl") Is Nothing Then CreateTblDetail()
            Dim objTable As DataTable = Session("TblDtl")
            Dim objView As DataView = objTable.DefaultView

            If i_u2.Text = "New Detail" Then

                Dim dt As DataTable = GetDetailShipment(shipmentrawmstoid.Text)
                Dim iSeq As Integer = objTable.Rows.Count + 1

                For C1 As Integer = 0 To dt.Rows.Count - 1
                    objView.RowFilter = "shipmentrawmstoid=" & shipmentrawmstoid.Text & " AND shipmentrawdtloid=" & dt.Rows(C1)("shipmentrawdtloid").ToString
                    If objView.Count = 0 Then
                        Dim objRow As DataRow = objTable.NewRow
                        objRow("arrawdtlseq") = iSeq
                        objRow("shipmentrawmstoid") = shipmentrawmstoid.Text
                        objRow("shipmentrawno") = shipmentrawno.Text
                        objRow("shipmentrawdate") = dateETD.Text
                        objRow("shipmentrawdtloid") = dt.Rows(C1)("shipmentrawdtloid").ToString
                        objRow("matrawoid") = dt.Rows(C1)("matrawoid").ToString
                        objRow("matrawcode") = dt.Rows(C1)("matrawcode").ToString
                        objRow("matrawlongdesc") = dt.Rows(C1)("matrawlongdesc").ToString
                        objRow("arrawqty") = ToDouble(dt.Rows(C1)("shipmentrawqty").ToString)
                        objRow("arrawunitoid") = dt.Rows(C1)("shipmentrawunitoid").ToString
                        objRow("unit") = dt.Rows(C1)("unit").ToString
                        objRow("arrawprice") = ToDouble(dt.Rows(C1)("sorawprice").ToString)
                        objRow("arrawdtlamt") = ToDouble(dt.Rows(C1)("shipmentrawqty").ToString) * ToDouble(dt.Rows(C1)("sorawprice").ToString)
                        objRow("arrawdtldisctype") = dt.Rows(C1)("sorawdtldisctype").ToString
                        objRow("arrawdtldiscvalue") = ToDouble(dt.Rows(C1)("sorawdtldiscvalue").ToString)
                        If dt.Rows(C1)("sorawdtldisctype").ToString = "P" Then
                            objRow("arrawdtldiscamt") = (ToDouble(dt.Rows(C1)("sorawdtldiscvalue").ToString) / 100) * ToDouble(dt.Rows(C1)("shipmentrawqty").ToString) * ToDouble(dt.Rows(C1)("sorawprice").ToString)
                            objRow("arrawdtlnetto") = (ToDouble(dt.Rows(C1)("shipmentrawqty").ToString) * ToDouble(dt.Rows(C1)("sorawprice").ToString)) - ((ToDouble(dt.Rows(C1)("sorawdtldiscvalue").ToString) / 100) * ToDouble(dt.Rows(C1)("shipmentrawqty").ToString) * ToDouble(dt.Rows(C1)("sorawprice").ToString))
                        Else
                            objRow("arrawdtldiscamt") = ToDouble(dt.Rows(C1)("sorawdtldiscvalue").ToString) * ToDouble(dt.Rows(C1)("shipmentrawqty").ToString)
                            objRow("arrawdtlnetto") = (ToDouble(dt.Rows(C1)("shipmentrawqty").ToString) * ToDouble(dt.Rows(C1)("sorawprice").ToString)) - (ToDouble(dt.Rows(C1)("sorawdtldiscvalue").ToString) * ToDouble(dt.Rows(C1)("shipmentrawqty").ToString))
                        End If
                        objRow("arrawdtlnote") = arrawdtlnote.Text
                        objTable.Rows.Add(objRow)
                        iSeq += 1
                    Else
                        showMessage("This Shipment No has been added before.", 2)
                    End If
                    objView.RowFilter = ""
                Next
            Else
                objView.RowFilter = "arrawdtlseq=" & arrawdtlseq.Text
                If objView.Count > 0 Then
                    objView(0)("arrawprice") = ToDouble(arrawprice.Text)
                    objView(0)("arrawdtlamt") = ToDouble(arrawqty.Text) * ToDouble(arrawprice.Text)
                    objView(0)("arrawdtldisctype") = arrawdtldisctype.SelectedValue
                    objView(0)("arrawdtldiscvalue") = ToDouble(arrawdtldiscvalue.Text)
                    If arrawdtldisctype.SelectedValue = "P" Then
                        objView(0)("arrawdtldiscamt") = (ToDouble(arrawdtldiscvalue.Text) / 100) * ToDouble(arrawqty.Text) * ToDouble(arrawprice.Text)
                        objView(0)("arrawdtlnetto") = (ToDouble(arrawqty.Text) * ToDouble(arrawprice.Text)) - ((ToDouble(arrawdtldiscvalue.Text) / 100) * ToDouble(arrawqty.Text) * ToDouble(arrawprice.Text))
                    Else
                        objView(0)("arrawdtldiscamt") = ToDouble(arrawdtldiscvalue.Text) * ToDouble(arrawqty.Text)
                        objView(0)("arrawdtlnetto") = (ToDouble(arrawqty.Text) * ToDouble(arrawprice.Text)) - (ToDouble(arrawdtldiscvalue.Text) * ToDouble(arrawqty.Text))
                    End If
                    objView(0)("arrawdtlnote") = arrawdtlnote.Text
                End If
                objView.RowFilter = ""
                objTable.AcceptChanges()

                If chkUpdPrice.Checked Or chkUpdDisc.Checked Then
                    objView.RowFilter = "matrawoid=" & matrawoid.Text & " AND arrawdtlseq<>" & arrawdtlseq.Text
                    For R1 As Integer = 0 To objView.Count - 1
                        If chkUpdPrice.Checked Then objView(R1)("arrawprice") = ToDouble(arrawprice.Text)
                        objView(R1)("arrawdtlamt") = ToDouble(objView(R1)("arrawqty").ToString) * ToDouble(objView(R1)("arrawprice").ToString)
                        If chkUpdDisc.Checked Then
                            objView(R1)("arrawdtldisctype") = arrawdtldisctype.SelectedValue
                            objView(R1)("arrawdtldiscvalue") = ToDouble(arrawdtldiscvalue.Text)
                        End If
                        If arrawdtldisctype.SelectedValue = "P" Then
                            objView(R1)("arrawdtldiscamt") = (ToDouble(objView(R1)("arrawdtldiscvalue").ToString) / 100) * ToDouble(objView(R1)("arrawqty").ToString) * ToDouble(objView(R1)("arrawprice").ToString)
                            objView(R1)("arrawdtlnetto") = (ToDouble(objView(R1)("arrawqty").ToString) * ToDouble(objView(R1)("arrawprice").ToString)) - ((ToDouble(objView(R1)("arrawdtldiscvalue").ToString) / 100) * ToDouble(objView(R1)("arrawqty").ToString) * ToDouble(objView(R1)("arrawprice").ToString))
                        Else
                            objView(R1)("arrawdtldiscamt") = ToDouble(objView(R1)("arrawdtldiscvalue").ToString) * ToDouble(objView(R1)("arrawqty").ToString)
                            objView(R1)("arrawdtlnetto") = (ToDouble(objView(R1)("arrawqty").ToString) * ToDouble(objView(R1)("arrawprice").ToString)) - (ToDouble(objView(R1)("arrawdtldiscvalue").ToString) * ToDouble(objView(R1)("arrawqty").ToString))
                        End If
                    Next
                    objView.RowFilter = ""
                    objTable.AcceptChanges()
                End If
            End If

            Session("TblDtl") = objTable
            gvTblDtl.DataSource = objTable
            gvTblDtl.DataBind()
            CountHdrAmount()
            ClearDetail()
        End If
    End Sub 'OK

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub 'OK

    Protected Sub gvTblDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvTblDtl.SelectedIndexChanged
        gvTblDtl.Columns(0).Visible = False
        gvTblDtl.Columns(gvTblDtl.Columns.Count - 1).Visible = False
        'TD1.Visible = True : TD2.Visible = True : TD3.Visible = True : TD4.Visible = True
        'TD5.Visible = True : TD6.Visible = True : TD7.Visible = True : TD8.Visible = True
        'TD9.Visible = True : TD10.Visible = True : TD11.Visible = True : TD12.Visible = True
        'TD13.Visible = True : TD14.Visible = True : TD15.Visible = True : TD16.Visible = True
        'TD17.Visible = True : TD18.Visible = True : TD19.Visible = True : TD20.Visible = True
        'TD21.Visible = True : TD22.Visible = True : TD23.Visible = True : TD24.Visible = True
        'TD25.Visible = True : TD26.Visible = True : TD23.Visible = True : TD27.Visible = True
        'TD28.Visible = True : TD22.Visible = True : TD29.Visible = True : TD30.Visible = True
        btnSearchDO.Visible = False : btnClearDO.Visible = False
        Try
            arrawdtlseq.Text = gvTblDtl.SelectedDataKey.Item("arrawdtlseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable : objTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "arrawdtlseq=" & arrawdtlseq.Text

                shipmentrawmstoid.Text = dv.Item(0).Item("shipmentrawmstoid").ToString.Trim
                shipmentrawdtloid.Text = dv.Item(0).Item("shipmentrawdtloid").ToString.Trim
                shipmentrawno.Text = dv.Item(0).Item("shipmentrawno").ToString.Trim
                dateETD.Text = Format(CDate(dv.Item(0).Item("shipmentrawdate").ToString.Trim), "MM/dd/yyyy")

                matrawlongdesc.Text = dv.Item(0).Item("matrawlongdesc").ToString.Trim
                matrawoid.Text = dv.Item(0).Item("matrawoid").ToString
                arrawqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("arrawqty").ToString), 2)
                Unit.Text = dv.Item(0).Item("unit").ToString
                arrawprice.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("arrawprice").ToString), 2)
                arrawdtlamt.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("arrawdtlamt").ToString), 2)
                arrawdtldisctype.SelectedValue = dv.Item(0).Item("arrawdtldisctype").ToString
                arrawdtldiscvalue.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("arrawdtldiscvalue").ToString), 2)
                arrawdtldiscamt.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("arrawdtldiscamt").ToString), 2)
                arrawdtlnetto.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("arrawdtlnetto").ToString), 2)
                ReamountDetail()
                arrawdtlnote.Text = dv.Item(0).Item("arrawdtlnote").ToString
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub gvTblDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTblDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 2)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 2)
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 2)
            e.Row.Cells(10).Text = ToMaskEdit(ToDouble(e.Row.Cells(10).Text), 2)
            e.Row.Cells(11).Text = ToMaskEdit(ToDouble(e.Row.Cells(11).Text), 2)
            e.Row.Cells(12).Text = ToMaskEdit(ToDouble(e.Row.Cells(12).Text), 2)
            e.Row.Cells(13).Text = ToMaskEdit(ToDouble(e.Row.Cells(13).Text), 2)
        End If
    End Sub 'OK

    Protected Sub gvTblDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvTblDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        Dim iOid As Integer = objRow(e.RowIndex)("shipmentrawmstoid")
        Dim dv As DataView = objTable.DefaultView
        dv.RowFilter = "shipmentrawmstoid=" & iOid
        For C1 As Int16 = 0 To dv.Count - 1
            dv.Delete(0)
        Next
        dv.RowFilter = ""
        For R1 As Int16 = 0 To dv.Count - 1
            dv(R1)("arrawdtlseq") = R1 + 1
        Next
        Session("TblDtl") = dv.ToTable
        gvTblDtl.DataSource = Session("TblDtl")
        gvTblDtl.DataBind()
        CountHdrAmount()
        ClearDetail()
    End Sub 'OK

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        curroid_SelectedIndexChanged(Nothing, Nothing)
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            Dim sPeriod As String = GetDateToPeriodAcctg(arrawdate.Text)
            Dim iConAROid As Int32 = GenerateID("QL_CONAR", CompnyCode)
            Dim iGlMstOid As Int32 = GenerateID("QL_TRNGLMST", CompnyCode)
            Dim iGlDtlOid As Int32 = GenerateID("QL_TRNGLDTL", CompnyCode)
            Dim iPiutangAcctgOid, iDiscAcctgOid, iJualAcctgOid, iPpnAcctgOid, iHppAcctgOid, iPersediaanAcctgOid As Integer
            periodacctg.Text = GetDateToPeriodAcctg(arrawdate.Text)
            iPiutangAcctgOid = GetAcctgOID(GetVarInterface("VAR_AR", DDLBusUnit.SelectedValue), CompnyCode)
            iDiscAcctgOid = GetAcctgOID(GetVarInterface("VAR_DISC_JUAL", DDLBusUnit.SelectedValue), CompnyCode)
            iJualAcctgOid = GetAcctgOID(GetVarInterface("VAR_PENJUALAN", DDLBusUnit.SelectedValue), CompnyCode)
            iPpnAcctgOid = GetAcctgOID(GetVarInterface("VAR_PPN_OUT", DDLBusUnit.SelectedValue), CompnyCode)
            iHppAcctgOid = GetAcctgOID(GetVarInterface("VAR_HPP", DDLBusUnit.SelectedValue), CompnyCode)
            iPersediaanAcctgOid = GetAcctgOID(GetVarInterface("VAR_STOCK_TRANSIT", DDLBusUnit.SelectedValue), CompnyCode)

            Dim iSeq As Int16 = 1 : Dim iSeq2 As Int16 = 1
            Dim cRate As New ClassRate
            cRate.SetRateValue(curroid.SelectedItem.ToString, Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, 2) : Exit Sub
            End If

            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            If arrawmststatus.Text = "Revised" Then
                arrawmststatus.Text = "In Process"
            End If
            If arrawmststatus.Text = "Post" Then
                If isPeriodClosed(DDLBusUnit.SelectedValue, sPeriod) Then
                    showMessage("Cannot continue this transaction anymore because the period : " & MonthName(CInt(sPeriod.Substring(4, 2))) & " " & sPeriod.Substring(0, 4) & " have been closed.", 2)
                    Exit Sub
                End If
                If isPeriodAcctgClosed(DDLBusUnit.SelectedValue, sPeriod) Then
                    showMessage("Cannot continue this transaction anymore because the period : " & MonthName(CInt(sPeriod.Substring(4, 2))) & " " & sPeriod.Substring(0, 4) & " have been closed.", 2)
                    Exit Sub
                End If
                'GenerateJualMstNo()
            Else
                Session("sJM") = ""
            End If
            Try
                sSql = "UPDATE QL_trnjualmst SET periodacctg='" & periodacctg.Text & "', trnjualdate='" & arrawdate.Text & "', curroid=" & curroid.SelectedValue & ", trnpaytype=" & arrawpaytypeoid.SelectedValue & ", jualratetoidr=" & rateidrvalue.Text & ", jualratetousd=" & arrawratetousd.Text & ", trnjualrefno='" & refno.Text & "', nofakturpajak='" & fakturpajak.Text & "', datetakegiro='" & arrawdatetakegiro.Text & "', trnjualdateetd='" & dateETD.Text & "', trnjualstatus='" & arrawmststatus.Text & "', trnjualnote='" & arrawmstnote.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnjualmstoid=" & arrawmstoid.Text & ""
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                If arrawmststatus.Text = "Post" Then
                    'Insert conar
                    sSql = "INSERT INTO QL_conar (cmpcode, conaroid, reftype, refoid, payrefoid, custoid, acctgoid, trnarstatus, trnartype, trnardate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans, amtbayar, trnarnote, trnarres1, trnarres2, trnarres3, createuser, createtime, upduser, updtime, amttransidr, amtbayaridr, amttransusd, amtbayarusd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iConAROid & ", 'QL_trnjualmst', " & arrawmstoid.Text & ", 0, " & custoid.Text & ", " & iPiutangAcctgOid & ", 'Post', 'AR', '" & arrawdate.Text & "', '" & periodacctg.Text & "', 0, '1/1/1900', '', 0, '" & dateETD.Text & "', " & ToDouble(arrawtotalnetto.Text) & ", 0, '', '', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(arrawtotalnetto.Text) * cRate.GetRateMonthlyIDRValue & ", 0, " & ToDouble(arrawtotalnetto.Text) * cRate.GetRateMonthlyUSDValue & ", 0)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iConAROid & " WHERE tablename='QL_CONAR' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    ' Posting Auto Journal hpp
                    ' Insert QL_trnglmst
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlMstOid & ", '" & arrawdate.Text & "', '" & periodacctg.Text & "', 'Sales Invoice|No. " & arrawno.Text & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Insert QL_trngldtl hpp
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iHppAcctgOid & ", 'D', " & ToDouble(valueidr.Text) & ", '" & arrawno.Text & "', 'Sales Invoice|No. " & arrawno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(valueidr.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(valueusd.Text) * cRate.GetRateMonthlyUSDValue & ", 'QL_trnjualmst " & arrawmstoid.Text & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1 : iSeq += 1
                    ' Insert QL_trngldtl stock dlm perjalanan
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iPersediaanAcctgOid & ", 'C', " & ToDouble(valueidr.Text) & ", '" & arrawno.Text & "', 'Sales Invoice|No. " & arrawno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(valueidr.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(valueusd.Text) * cRate.GetRateMonthlyUSDValue & ", 'QL_trnjualmst " & arrawmstoid.Text & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1 : iSeq += 1

                    ' Posting Auto Journal penjualan
                    ' Insert QL_trnglmst
                    iGlMstOid += 1
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlMstOid & ", '" & arrawdate.Text & "', '" & periodacctg.Text & "', 'Sales Invoice|No. " & arrawno.Text & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Insert QL_trngldtl piutang
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq2 & ", " & iGlMstOid & ", " & iPiutangAcctgOid & ", 'D', " & ToDouble(arrawtotalnetto.Text) & ", '" & arrawno.Text & "', 'Sales Invoice|No. " & arrawno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(arrawtotalnetto.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(arrawtotalnetto.Text) * cRate.GetRateMonthlyUSDValue & ", 'QL_trnjualmst " & arrawmstoid.Text & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1 : iSeq2 += 1
                    ' Insert QL_trngldtl disc
                    If ToDouble(arrawtotaldisc.Text) > 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq2 & ", " & iGlMstOid & ", " & iDiscAcctgOid & ", 'D', " & ToDouble(arrawtotaldisc.Text) & ", '" & arrawno.Text & "', 'Sales Invoice|No. " & arrawno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(arrawtotaldisc.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(arrawtotaldisc.Text) * cRate.GetRateMonthlyUSDValue & ", 'QL_trnjualmst " & arrawmstoid.Text & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1 : iSeq2 += 1
                    End If
                    ' Insert QL_trngldtl penjualan
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq2 & ", " & iGlMstOid & ", " & iJualAcctgOid & ", 'C', " & ToDouble(arrawtotalamt.Text) & ", '" & arrawno.Text & "', 'Sales Invoice|No. " & arrawno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(arrawtotalamt.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(arrawtotalamt.Text) * cRate.GetRateMonthlyUSDValue & ", 'QL_trnjualmst " & arrawmstoid.Text & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1 : iSeq2 += 1
                    ' Insert QL_trngldtl PPN
                    If ToDouble(arrawvat.Text) > 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq2 & ", " & iGlMstOid & ", " & iPpnAcctgOid & ", 'C', " & ToDouble(arrawvat.Text) & ", '" & arrawno.Text & "', 'Sales Invoice|No. " & arrawno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(arrawvat.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(arrawvat.Text) * cRate.GetRateMonthlyUSDValue & ", 'QL_trnjualmst " & arrawmstoid.Text & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1 : iSeq2 += 1
                    End If
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    If paymentType.Text.ToUpper <> "CASH" Then
                        ' Update Credit limit Customer
                        sSql = "SELECT custgroupoid from QL_mstcust WHERE cmpcode='" & CompnyCode & "' AND custoid=" & custoid.Text & ""
                        Dim group As Integer = cKon.ambilscalar(sSql)
                        sSql = "SELECT currcode from QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND curroid=" & curroid.Text & ""
                        Dim curr As String = GetStrData(sSql)
                        If curr = "IDR" Then
                            sSql = "UPDATE QL_mstcustgroup SET custcreditlimitusagerupiah = custcreditlimitusagerupiah + (" & arrawtotalnetto.Text * cRate.GetRateDailyIDRValue & ") WHERE cmpcode='" & CompnyCode & "' AND custgroupoid=" & group & ""
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        Else
                            sSql = "UPDATE QL_mstcustgroup SET custcreditlimitusageusd = custcreditlimitusageusd + (" & arrawtotalnetto.Text * cRate.GetRateDailyUSDValue & ") WHERE cmpcode='" & CompnyCode & "' AND custgroupoid=" & group & ""
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                    End If
                End If

                    objTrans.Commit()
                    conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        arrawmststatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    arrawmststatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 1)
                conn.Close()
                arrawmststatus.Text = "In Process"
                Exit Sub
            End Try
            Session("sJM") = Nothing
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & arrawmstoid.Text & ".<BR>"
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trnAR.aspx?awal=true")
            End If
        End If
    End Sub 'OK

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnAR.aspx?awal=true")
    End Sub 'OK

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If arrawmstoid.Text.Trim = "" Then
            showMessage("Please select A/R Raw Material data first!", 2)
            Exit Sub
            'Else
            'Dim sStatusInfo As String = GetMultiUserStatus("QL_TRNJUALMST", "trnjualmstoid", arrawmstoid.Text, "trnjualstatus", updtime.Text, "Post")
            'If sStatusInfo <> "" Then
            '    showMessage(sStatusInfo, 2)
            '    arrawmststatus.Text = "In Process"
            '    Exit Sub
            'End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trndodtl SET dodtlstatus='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dodtloid IN(SELECT dodtloid FROM QL_TRNJUALDTL WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnjualmstoid=" & arrawmstoid.Text & ")"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "UPDATE QL_trndomst SET domststatus='Approved' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND domstoid IN(SELECT domstoid FROM QL_TRNJUALMST WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnjualmstoid=" & arrawmstoid.Text & ")"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM QL_TRNJUALDTL WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnjualmstoid=" & arrawmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM QL_TRNJUALMST WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnjualmstoid=" & arrawmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnAR.aspx?awal=true")
    End Sub 'OK

    'Protected Sub btnPrint2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint2.Click
    '    'PrintDoc(arrawmstoid.Text)
    'End Sub 'OK

    Protected Sub btnSendApproval_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSendApproval.Click
        sSql = "SELECT approvaluser, apppersontype FROM QL_approvalperson WHERE tablename='QL_TRNARRAWMST' AND activeflag='ACTIVE' AND cmpcode IN ('" & CompnyCode & "', '" & DDLBusUnit.SelectedValue & "')"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_approvalperson")
        If dt.Rows.Count = 0 Then
            showMessage("There is no user can't approve this transaction. Please define approval user in <STRONG>APPROVAL USER</STRONG> form.", 2)
        Else
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "apppersontype='Final'"
            If dv.Count = 0 Then
                showMessage("Final approval user is not defined for this transaction. Please define final approval user in <STRONG>APPROVAL USER</STRONG> form.", 2)
                dv.RowFilter = ""
            Else
                dv.RowFilter = ""
                Session("AppPerson") = dt
                arrawmststatus.Text = "In Approval"
                btnSave_Click(Nothing, Nothing)
            End If
        End If
    End Sub 'OK

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        UpdateCheckedValue()
        PrintReport()
    End Sub

    Protected Sub btnPostikng_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        arrawmststatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub gvTRN_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 2)
        End If
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sErr As String = ""
        If Not IsValidDate(Rdate.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Receive Date is invalid. " & sErr & "", 2)
            Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trnjualmst SET receivedate='" & CDate(Rdate.Text) & "', salescode='" & DDLsales.SelectedValue & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, nofakturpajak='" & fakturpajak.Text & "', trnjualnote='" & arrawmstnote.Text & "' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnjualmstoid=" & arrawmstoid.Text & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            showMessage(ex.Message, 1)
            conn.Close()
        End Try
        Response.Redirect("~\Transaction\trnAR.aspx?awal=true")
    End Sub

    'Protected Sub btnContinue_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnContinue.Click
    '    If btnContinue.ImageUrl = "~/Images/continue.png" Then
    '        PrintReport(lblRptOid.Text)
    '        mpeApproval.Show()
    '    Else
    '        cProc.SetModalPopUpExtender(btnPnlApproval, pnlApproval, mpeApproval, False)
    '    End If
    'End Sub

    'Protected Sub btnCancelPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelPrint.Click
    '    cProc.SetModalPopUpExtender(btnPnlApproval, pnlApproval, mpeApproval, False)
    'End Sub
#End Region

    Protected Sub CBprint_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If CBprint.Checked Then
            DDLType.Visible = True
            Label42.Visible = True
        Else
            DDLType.Visible = False
            Label42.Visible = False
        End If
    End Sub
End Class