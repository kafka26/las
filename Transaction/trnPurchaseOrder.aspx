<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnPurchaseOrder.aspx.vb" Inherits="Transaction_PORawMaterial" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Purchase Order" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Purchase Order :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 10%" class="Label" align=left><asp:Label id="Label22" runat="server" Text="Filter"></asp:Label></TD><TD style="WIDTH: 2%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="105px"><asp:ListItem Value="pom.pomstoid">Draft No.</asp:ListItem>
<asp:ListItem Value="pom.pono">PO No.</asp:ListItem>
<asp:ListItem Value="s.suppname">Supplier</asp:ListItem>
<asp:ListItem Value="pom.pomstnote">Note</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="200px"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text="Periode"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="60px" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label5" runat="server" Text="to"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="60px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="lblFormatTgl" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="105px"><asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>In Approval</asp:ListItem>
<asp:ListItem>Approved</asp:ListItem>
<asp:ListItem>Rejected</asp:ListItem>
<asp:ListItem>Revised</asp:ListItem>
<asp:ListItem>Closed</asp:ListItem>
<asp:ListItem>Force Closed</asp:ListItem>
<asp:ListItem>Cancel</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="FilterPeriod1" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" TargetControlID="FilterPeriod1" PopupButtonID="imbDate1" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" TargetControlID="FilterPeriod2" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" TargetControlID="FilterPeriod2" PopupButtonID="imbDate2" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:LinkButton id="lkbPOInProcess" runat="server" Visible="False"></asp:LinkButton></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:LinkButton id="lkbPOInApproval" runat="server" Visible="False"></asp:LinkButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvTRN" runat="server" ForeColor="#333333" Width="100%" GridLines="None" CellPadding="4" DataKeyNames="pomstoid,supptype" AutoGenerateColumns="False" PageSize="8" AllowPaging="True" AllowSorting="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="pomstoid" DataNavigateUrlFormatString="~\Transaction\trnPurchaseOrder.aspx?oid={0}" DataTextField="pomstoid" HeaderText="Draft No." SortExpression="pomstoid">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="pono" HeaderText="PO No." SortExpression="pono">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="podate" HeaderText="Date" SortExpression="podate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier" SortExpression="suppname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pomstnote" HeaderText="Note" SortExpression="pomstnote">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pomststatus" HeaderText="Status" SortExpression="pomststatus">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reason" HeaderText="Reason" SortExpression="reason">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbGVMst" runat="server" Checked='<%# eval("checkvalue") %>'></asp:CheckBox> <asp:Label id="lblOidGVMst" runat="server" Text='<%# eval("pomstoid") %>' Visible="False"></asp:Label> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="gvTRN"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label40" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Purchase Order Header" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 15%" class="Label" align=left><asp:Label id="i_u" runat="server" CssClass="Important" Text="New Data"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="periodacctg" runat="server" Visible="False"></asp:Label> <asp:Label id="suppoid" runat="server" Visible="False"></asp:Label> <asp:Label id="porawmstres1" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 15%" class="Label" align=left></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="rateoid" runat="server" Visible="False"></asp:Label> <asp:Label id="rate2oid" runat="server" Visible="False"></asp:Label> <asp:Label id="suppres3" runat="server" Visible="False"></asp:Label> <asp:Label id="partnerflag" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblTrnNo" runat="server" Text="Draft No."></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:Label id="porawmstoid" runat="server"></asp:Label><asp:TextBox id="porawno" runat="server" CssClass="inpTextDisabled" Width="160px" Visible="False" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text="PO Type"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="porawtype" runat="server" CssClass="inpTextDisabled" Width="85px" Enabled="False"><asp:ListItem>LOCAL</asp:ListItem>
<asp:ListItem>IMPORT</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label6" runat="server" Text="Supplier"></asp:Label> <asp:Label id="Label44" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="suppname" runat="server" CssClass="inpTextDisabled" Width="160px" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchSupp" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearSupp" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="Label8" runat="server" Text="PO Date"></asp:Label> <asp:Label id="Label43" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="porawdate" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="imbpodate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label48" runat="server" Text="Type"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLTypePO" runat="server" CssClass="inpText" Width="164px" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label9" runat="server" Text="Up"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="porawsuppref" runat="server" CssClass="inpText" Width="160px" MaxLength="50"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label12" runat="server" Text="Currency"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="curroid" runat="server" CssClass="inpText" Width="105px" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label16" runat="server" Text="Payment Type"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="porawpaytypeoid" runat="server" CssClass="inpTextDisabled" Width="105px" Enabled="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left rowSpan=1><asp:Label id="Label13" runat="server" Text="Daily Rate to IDR"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="porawratetoidr" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False">0</asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label32" runat="server" Text="Daily Rate To USD"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="porawratetousd" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False">0</asp:TextBox></TD></TR><TR><TD class="Label" align=left rowSpan=1><asp:Label id="Label42" runat="server" Text="Monthly Rate to IDR"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="porawrate2toidr" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False">0</asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label46" runat="server" Text="Monthly Rate To USD"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="porawrate2tousd" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False">0</asp:TextBox></TD></TR><TR><TD class="Label" align=left rowSpan=1><asp:Label id="Label17" runat="server" Text="Total Amt"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="porawtotalamt" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False">0</asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label26" runat="server" Text="Total Disc. Dtl Amt"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="porawtotaldiscdtl" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False">0</asp:TextBox></TD></TR>
    <tr>
        <td align="left" class="Label" rowspan="1">
            <asp:Label ID="Label18" runat="server" Text="Header Disc"></asp:Label></td>
        <td align="center" class="Label">
            :</td>
        <td align="left" class="Label">
            <asp:DropDownList ID="ddlHD" runat="server" AutoPostBack="True" CssClass="inpText"
                OnSelectedIndexChanged="ddlHD_SelectedIndexChanged" Width="80px">
                <asp:ListItem Value="P">Percentage</asp:ListItem>
                <asp:ListItem Value="A">Amount</asp:ListItem>
            </asp:DropDownList>
            -
            <asp:TextBox ID="tbHD" runat="server" AutoPostBack="True" CssClass="inpText" MaxLength="18"
                OnTextChanged="tbHD_TextChanged" Width="100px">0</asp:TextBox></td>
        <td align="left" class="Label">
            <asp:Label ID="Label27" runat="server" Text="Header Disc. Amt"></asp:Label></td>
        <td align="center" class="Label">
            :</td>
        <td align="left" class="Label">
            <asp:TextBox ID="HDA" runat="server" CssClass="inpTextDisabled" Enabled="False" Width="100px">0</asp:TextBox></td>
    </tr>
    <tr>
        <td align="left" class="Label" rowspan="1">
        </td>
        <td align="center" class="Label">
        </td>
        <td align="left" class="Label">
        </td>
        <td align="left" class="Label">
            <asp:Label ID="Label28" runat="server" Text="Total Disc. Amt"></asp:Label></td>
        <td align="center" class="Label">
            :</td>
        <td align="left" class="Label">
            <asp:TextBox ID="pototaldiscamt" runat="server" CssClass="inpTextDisabled" Enabled="False"
                Width="100px">0</asp:TextBox></td>
    </tr>
    <TR><TD class="Label" align=left rowSpan=1><asp:Label id="Label19" runat="server" Text="Total Netto"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="porawtotalnetto" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False">0</asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label45" runat="server" Text="Tolerance"></asp:Label> <asp:Label id="Label47" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="tolerance" runat="server" CssClass="inpText" Width="24px" MaxLength="3"></asp:TextBox>&nbsp;% - Max Tolerance 10%</TD></TR><TR><TD class="Label" align=left rowSpan=1><asp:Label id="Label20" runat="server" Text="Tax"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="porawtaxtype" runat="server" CssClass="inpText" Width="85px" AutoPostBack="True"><asp:ListItem>TAX</asp:ListItem>
<asp:ListItem Value="NON TAX">Non TAX</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:Label id="lbltax" runat="server" Text="-"></asp:Label> <asp:TextBox id="porawtaxamt" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox> <asp:DropDownList id="DDLinex" runat="server" CssClass="inpText" Visible="False" AutoPostBack="True" OnSelectedIndexChanged="DDLinex_SelectedIndexChanged"><asp:ListItem>EXC</asp:ListItem>
<asp:ListItem>INC</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label31" runat="server" Text="Tax Amount"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="porawvat" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False">0</asp:TextBox></TD></TR><TR><TD id="TDLabelTotalCost" class="Label" align=left rowSpan=1 runat="server"><asp:Label id="Label23" runat="server" Text="Grand Total Amt"></asp:Label></TD><TD id="TDSeptTotalCost" class="Label" align=center runat="server">:</TD><TD id="TDTotalCost" class="Label" align=left runat="server"><asp:TextBox id="porawgrandtotalamt" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False">0</asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label4" runat="server" Text="Status"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="porawmststatus" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False" MaxLength="20"></asp:TextBox></TD></TR><TR><TD class="Label" align=left rowSpan=1><asp:Label id="Label24" runat="server" Text="Header Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="porawmstnote" runat="server" CssClass="inpText" Width="200px" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><ajaxToolkit:FilteredTextBoxExtender id="ftbTax" runat="server" ValidChars="1234567890,." TargetControlID="porawtaxamt"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbTolerance" runat="server" ValidChars="0123456789." TargetControlID="tolerance"></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:CalendarExtender id="ceprdate" runat="server" TargetControlID="porawdate" PopupButtonID="imbpodate" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><asp:ImageButton id="btnGenerate" runat="server" ImageUrl="~/Images/gendetail.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6 rowSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><asp:Panel id="pnlDtlMat" runat="server" Width="100%"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left colSpan=6 rowSpan=1><asp:Label id="Label7" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Purchase Order Detail" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 15%" class="Label" align=left><asp:Label id="i_u2" runat="server" Text="New Detail" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="porawdtloid" runat="server" Visible="False"></asp:Label> <asp:Label id="porawdtlseq" runat="server" Visible="False">1</asp:Label></TD><TD style="WIDTH: 15%" class="Label" align=left></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="matrawoid" runat="server" Visible="False"></asp:Label> <asp:Label id="matrawcode" runat="server" Visible="False"></asp:Label> <asp:Label id="prrawdate" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label11" runat="server" Text="Material"></asp:Label> <asp:Label id="Label34" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="matrawlongdesc" runat="server" CssClass="inpTextDisabled" Width="180px" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchMat" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="Label14" runat="server" Text="Quantity"></asp:Label> <asp:Label id="Label25" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="porawqty" runat="server" CssClass="inpText" Width="75px" AutoPostBack="True" MaxLength="6"></asp:TextBox>&nbsp;- <asp:DropDownList id="porawunitoid" runat="server" CssClass="inpText" Width="105px"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblvarprice" runat="server" Text="Var Price"></asp:Label></TD><TD class="Label" align=center><asp:Label id="lblvarprice2" runat="server" Text=":"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="varprice" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" MaxLength="18" OnTextChanged="varprice_TextChanged"></asp:TextBox></TD><TD class="Label" align=left>
</td>
    <TD class="Label" align=center>
    </td>
    <TD class="Label" align=left>
    </td>
</TR><TR><TD class="Label" align=left><asp:Label id="Label35" runat="server" Text="Price Per Unit"></asp:Label> <asp:Label id="Label33" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="porawprice" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" MaxLength="10"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label15" runat="server" Text="Detail Amount"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="porawdtlamt" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False">0</asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label36" runat="server" Text="Detail Disc"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="porawdtldisctype" runat="server" CssClass="inpText" Width="85px" AutoPostBack="True"><asp:ListItem Value="P">Percentage</asp:ListItem>
<asp:ListItem Value="A">Amount</asp:ListItem>
</asp:DropDownList>&nbsp;- <asp:TextBox id="porawdtldiscvalue" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" MaxLength="18"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label37" runat="server" Text="Detail Disc. Amount"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="porawdtldiscamt" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False">0</asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label38" runat="server" Text="Detail Netto "></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="porawdtlnetto" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False">0</asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label39" runat="server" Text="Detail Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="porawdtlnote" runat="server" CssClass="inpText" Width="200px" MaxLength="100"></asp:TextBox></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:CheckBox id="cbPrice" runat="server" Text="Update Price for All Same Material" Checked="True"></asp:CheckBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:CheckBox id="cbDisc" runat="server" Text="Update Disc. for All Same Material" Checked="True"></asp:CheckBox></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="ftbDD" runat="server" ValidChars="1234567890,." TargetControlID="porawdtldiscvalue"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbQty" runat="server" ValidChars="1234567890,." TargetControlID="porawqty"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbPrice" runat="server" ValidChars="1234567890,." TargetControlID="porawprice"></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Panel id="Panel2" runat="server" CssClass="inpText" Width="100%" Height="200px" ScrollBars="Vertical"><asp:GridView id="gvTblDtl" runat="server" ForeColor="#333333" Width="98%" PageSize="5" AutoGenerateColumns="False" DataKeyNames="podtlseq" CellPadding="4" GridLines="None">
<RowStyle BackColor="#E3EAEB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="XX-Small"></ItemStyle>
</asp:CommandField>
    <asp:BoundField DataField="podtlseq" HeaderText="No">
        <HeaderStyle CssClass="gvhdr" Font-Size="XX-Small" HorizontalAlign="Center" />
        <ItemStyle Font-Size="XX-Small" HorizontalAlign="Center" />
    </asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdesc" HeaderText="Material">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="poqty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="lastpoprice" HeaderText="Last Price">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="poprice" HeaderText="Price">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="podtlamt" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="podtldiscamt" HeaderText="Disc">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="podtlnetto" HeaderText="Netto">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="podtlnote" HeaderText="Detail Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="XX-Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#666666" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#7C6F57"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><BR /></asp:Panel> </TD></TR></TBODY></TABLE></asp:Panel> </TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=6></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update&nbsp;By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 25px" align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSaveAs" runat="server" ImageUrl="~/Images/saveas.png" ImageAlign="AbsMiddle" Visible="False" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton id="btnPrint2" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnSendApproval" runat="server" ImageUrl="~/Images/sendapproval.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint2"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Purchase Order :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel id="upListSupp" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListSupp" runat="server" CssClass="modalBox" Width="650px" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSupp" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Supplier"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListSupp" runat="server" Width="100%" DefaultButton="btnFindListSupp">Filter : <asp:DropDownList id="FilterDDLListSupp" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="suppname">Name</asp:ListItem>
<asp:ListItem Value="suppcode">Code</asp:ListItem>
<asp:ListItem Value="suppaddr">Address</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListSupp" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> Type : <asp:DropDownList id="FilterDDLType" runat="server" CssClass="inpText" Width="100px">
        <asp:ListItem>LOCAL</asp:ListItem>
        <asp:ListItem>IMPORT</asp:ListItem>
    </asp:DropDownList><BR /><asp:CheckBox id="cbTag" runat="server" Text="Tag :"></asp:CheckBox> <asp:DropDownList id="FilterDDLTag" runat="server" CssClass="inpText" Width="200px">
    </asp:DropDownList>&nbsp;<asp:ImageButton id="btnFindListSupp" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListSupp" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListSupp" runat="server" ForeColor="#333333" Width="98%" GridLines="None" CellPadding="4" DataKeyNames="suppoid,suppcode,suppname,supppaymentoid,supptaxable,suppres3,supptype,partnerflag" AutoGenerateColumns="False" PageSize="5" AllowPaging="True" AllowSorting="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Code" SortExpression="suppcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Name" SortExpression="suppname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="partnerflag" HeaderText="Partner ID">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListSupp" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSupp" runat="server" TargetControlID="btnHideListSupp" PopupDragHandleControlID="lblListSupp" BackgroundCssClass="modalBackground" PopupControlID="pnlListSupp" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListSupp" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upListPR" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlListPR" runat="server" CssClass="modalBox" Width="850px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListPR" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of PR Raw Material" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFilterListPR" runat="server" Width="100%" DefaultButton="btnFindListPR">Filter : <asp:DropDownList id="FilterDDLListPR" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="prrawno">PR No.</asp:ListItem>
<asp:ListItem Value="deptname">Department</asp:ListItem>
<asp:ListItem Value="prrawmstnote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListPR" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListPR" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListPR" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListPR" runat="server" ForeColor="#333333" Width="98%" GridLines="None" CellPadding="4" DataKeyNames="prrawmstoid,prrawno,prrawdate" AutoGenerateColumns="False" PageSize="5" AllowSorting="True" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="prrawno" HeaderText="PR No." SortExpression="prrawno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prrawdate" HeaderText="PR Date" SortExpression="prrawdate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prrawexpdate" HeaderText="Expired Date" SortExpression="prrawexpdate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="deptname" HeaderText="Department" SortExpression="deptname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="prrawmstnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label114" runat="server" CssClass="Important" Text="No data found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbCloseListPR" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListPR" runat="server" TargetControlID="btnHideListPR" PopupDragHandleControlID="lblListPR" BackgroundCssClass="modalBackground" PopupControlID="pnlListPR" Drag="True">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListPR" runat="server" ForeColor="Transparent" Visible="False"></asp:Button>
</ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upListMat" runat="server"><contenttemplate>
<asp:Panel id="pnlListMat" runat="server" CssClass="modalBox" Width="900px" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListMat" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Material"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListMat" runat="server" Width="100%" DefaultButton="btnFindListMat"><asp:Label id="LabelFilterListMat" runat="server" Text="Filter : "></asp:Label> <asp:DropDownList id="FilterDDLListMat" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="itemlongdesc">Description</asp:ListItem>
<asp:ListItem Value="itemcode">Code</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" CssClass="inpText" Width="150px"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton><BR /><asp:ImageButton id="btnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnSelectNone" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewChecked" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel></TD></TR><TR><TD style="HEIGHT: 22px" class="Label" align=left colSpan=3>Show <asp:TextBox id="tbData" runat="server" CssClass="inpText" Width="25px"></asp:TextBox> Data Per Page <asp:LinkButton id="lbShowData" runat="server">>> View</asp:LinkButton></TD></TR><TR><TD class="Label" align=center colSpan=3>&nbsp;<asp:GridView id="gvListMat" runat="server" ForeColor="#333333" Width="99%" GridLines="None" CellPadding="4" AutoGenerateColumns="False" PageSize="5" AllowSorting="True" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbSelectPR" runat="server" ToolTip='<%# eval("itemoid") %>' Checked='<%# eval("CheckValue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Code" SortExpression="itemcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdesc" HeaderText="Description" SortExpression="itemlongdesc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Unit"><ItemTemplate>
<asp:DropDownList id="DDLUnit" runat="server" CssClass="inpText" Width="80px" ToolTip="<%# GetParameterID() %>"></asp:DropDownList>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Qty"><ItemTemplate>
<asp:TextBox id="tbPRPOQty" runat="server" CssClass="inpText" Text='<%# eval("poqty") %>' Width="50px" MaxLength="16" __designer:wfdid="w5"></asp:TextBox><BR /><ajaxToolkit:FilteredTextBoxExtender id="ftbeQty" runat="server" TargetControlID="tbPRPOQty" ValidChars="1234567890.," __designer:wfdid="w6"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Price"><ItemTemplate>
<asp:TextBox id="tbPRPOPrice" runat="server" CssClass="inpText" Text='<%# eval("poprice") %>' Width="75px" MaxLength="16" __designer:wfdid="w3"></asp:TextBox><BR /><ajaxToolkit:FilteredTextBoxExtender id="ftbePrice" runat="server" TargetControlID="tbPRPOPrice" ValidChars="1234567890.," __designer:wfdid="w4"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="stockqty" HeaderText="Stock Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Note"><ItemTemplate>
<asp:TextBox id="TextBox1" runat="server" CssClass="inpText" Text='<%# eval("podtlnote") %>' Width="150px" MaxLength="100"></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD colSpan=3><asp:CheckBox id="cbOpenListMatUsage" runat="server" Font-Size="8pt" Font-Bold="True" CssClass="Important" Text="Open List Of Material again after add data to list" Checked="True"></asp:CheckBox></TD></TR><TR><TD colSpan=3><asp:LinkButton id="lbAddToListListMat" runat="server">[ Add To List ]</asp:LinkButton>&nbsp;- <asp:LinkButton id="lbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR><TR><TD colSpan=3><ajaxToolkit:FilteredTextBoxExtender id="ftbData" runat="server" TargetControlID="tbData" FilterType="Numbers">
            </ajaxToolkit:FilteredTextBoxExtender> </TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" Drag="True" PopupControlID="pnlListMat" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListMat"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upApproval" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlApproval" runat="server" CssClass="modalBox" Width="250px" Visible="False"><TABLE width="100%"><TBODY><TR><TD align=center colSpan=2><asp:Label id="lblApproval" runat="server" Font-Size="Medium" Font-Bold="True" Text="Choose Language"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" align=center colSpan=2><asp:Label id="lblRptOid" runat="server" Visible="False"></asp:Label> </TD></TR><TR><TD class="Label" align=center colSpan=2><asp:DropDownList id="DDLTypeSupp" runat="server" CssClass="inpText" Width="150px"><asp:ListItem Value="LOCAL">Indonesia</asp:ListItem>
<asp:ListItem Value="IMPORT">English</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 5px" align=center colSpan=2></TD></TR><TR><TD align=center colSpan=2><asp:ImageButton id="btnContinue" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsBottom" AlternateText="Continue"></asp:ImageButton> <asp:ImageButton id="btnCancelPrint" runat="server" ImageUrl="~/Images/Close.png" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeApproval" runat="server" TargetControlID="btnPnlApproval" PopupDragHandleControlID="lblApproval" BackgroundCssClass="modalBackground" PopupControlID="pnlApproval" Drag="True">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnPnlApproval" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
        <triggers>
<asp:PostBackTrigger ControlID="btnContinue"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel><asp:UpdatePanel id="upPopUpConfirm" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpConfirm" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaptionConfirm" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIconConfirm" runat="server" ImageUrl="~/Images/question-mark.png" Width="24px" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpConfirm" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2><asp:ImageButton id="imbYesConfirm" runat="server" ImageUrl="~/Images/yes.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbNoConfirm" runat="server" ImageUrl="~/Images/no.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpConfirm" runat="server" TargetControlID="bePopUpConfirm" PopupDragHandleControlID="lblCaptionConfirm" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpConfirm" Drag="True" DropShadow="True">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="bePopUpConfirm" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

