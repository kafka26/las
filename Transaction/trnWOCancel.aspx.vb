Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_WorkOrderCancellation
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub 'OK

    Private Sub InitDDL()
        ' Init DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE' --AND divcode<>'" & CompnyCode & "'"
        FillDDL(DDLBusUnit, sSql)
    End Sub 'OK

    Private Sub ClearData()
        womstoid.Text = ""
        wono.Text = ""
        wodate.Text = ""
        wodocrefno.Text = ""
        womstnote.Text = ""
        womststatus.Text = ""
        Session("TblDtlFG") = Nothing
        GVDtl1.DataSource = Nothing
        GVDtl1.DataBind()
        Session("TblDtlProc") = Nothing
        GVDtl2.DataSource = Nothing
        GVDtl2.DataBind()
        Session("TblDtlMat") = Nothing
        GVDtl3.DataSource = Nothing
        GVDtl3.DataBind()
    End Sub 'OK

    Private Sub BindListWO()
        sSql = "SELECT wom.womstoid, wom.wono, CONVERT(VARCHAR(10), wom.wodate, 101) AS wodate, wom.wodocrefno, wom.womststatus, wom.womstnote FROM QL_trnwomst wom WHERE wom.cmpcode='" & DDLBusUnit.SelectedValue & "' AND wom.womststatus='Post' AND wom.womstoid NOT IN (SELECT DISTINCT b.womstoid FROM QL_trnprodresmst a INNER JOIN QL_trnprodresdtl b ON b.prodresmstoid = a.prodresmstoid AND b.cmpcode = a.cmpcode WHERE a.cmpcode='" & DDLBusUnit.SelectedValue & "' AND a.prodresmststatus<>'Cancel' UNION ALL SELECT DISTINCT req.womstoid FROM QL_trnusagemst req WHERE req.cmpcode='" & DDLBusUnit.SelectedValue & "' AND req.usagemststatus<>'Cancel') AND " & FilterDDLListWO.SelectedValue & " LIKE '%" & Tchar(FilterTextListWO.Text) & "%' ORDER BY wom.wodate DESC, wom.womstoid DESC"
        FillGV(gvListWO, sSql, "QL_trnwomst")
    End Sub 'OK

    Private Sub ViewDetailWO(ByVal iOid As Integer)
        ' Generate Detail Finish Good
        sSql = "SELECT DISTINCT wod1.wodtl1seq, wod1.soitemmstoid, som.sono AS soitemno, wod1.soitemdtloid, wod1.itemoid, i.itemlongdescription AS itemlongdesc, i.itemcode, (sod.sodtlqty - ISNULL((SELECT SUM(wodx.wodtl1qty) FROM QL_trnwodtl1 wodx WHERE wodx.cmpcode=wod1.cmpcode AND wodx.soitemmstoid=wod1.soitemmstoid AND wodx.soitemdtloid=wod1.soitemdtloid AND wodx.itemoid=wod1.itemoid AND wodx.womstoid<>wod1.womstoid), 0)) AS soitemqty, wod1.wodtl1qty, wod1.wodtl1unitoid, g.gendesc AS wodtl1unit, wod1.bomoid, bom.bomdesc, bom.itemqty AS itemqtybom, wod1.wodtl1note FROM QL_trnwodtl1 wod1 INNER JOIN QL_trnsomst som ON som.cmpcode=wod1.cmpcode AND som.somstoid=wod1.soitemmstoid INNER JOIN QL_trnsodtl sod ON sod.cmpcode=wod1.cmpcode AND sod.somstoid=wod1.soitemmstoid and sod.sodtloid=wod1.soitemdtloid AND sod.itemoid=wod1.itemoid INNER JOIN QL_mstbom bom ON bom.cmpcode=wod1.cmpcode AND bom.bomoid=wod1.bomoid INNER JOIN QL_mstitem i ON i.itemoid=wod1.itemoid INNER JOIN QL_mstgen g ON g.genoid=wod1.wodtl1unitoid WHERE wod1.cmpcode='" & DDLBusUnit.SelectedValue & "' AND wod1.womstoid=" & iOid
        Dim dtFG As DataTable = cKon.ambiltabel(sSql, "QL_trnwodtl1")
        Session("TblDtlFG") = dtFG
        GVDtl1.DataSource = Session("TblDtlFG")
        GVDtl1.DataBind()

        ' Generate Detail Process
        sSql = "SELECT DISTINCT wod2.wodtl2seq, wod1.wodtl1seq, 'Detail Finish Good No. ' + CONVERT(VARCHAR(10), wod1.wodtl1seq) AS wodtl1desc, wod2.deptoid, de.deptname, wod2.wodtl2type, wod2.wodtl2type, wod2.todeptoid, de2.deptname AS wodtl2reflongdesc, de2.deptcode AS wodtl2refcode, wod2.wodtl2qty, wod2.wodtl2unitoid, g.gendesc AS wodtl2unit, wod2.wodtl2note, wod2.wodtl2desc, wod2.wodtl2res3, (wod2.wodtl2qty / wod1.wodtl1qty) AS wodtl2qtyperunit FROM QL_trnwodtl2 wod2 INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=wod2.cmpcode AND wod1.wodtl1oid=wod2.wodtl1oid INNER JOIN QL_mstgen g ON g.genoid=wod2.wodtl2unitoid INNER JOIN QL_mstdept de ON de.cmpcode=wod2.cmpcode AND de.deptoid=wod2.deptoid INNER JOIN QL_mstdept de2 ON de2.cmpcode=wod2.cmpcode AND de2.deptoid=wod2.todeptoid WHERE wod2.cmpcode='" & DDLBusUnit.SelectedValue & "' AND wod2.womstoid=" & iOid & " ORDER BY wod1.wodtl1seq, wod2.wodtl2seq"
        Dim dtProc As DataTable = cKon.ambiltabel(sSql, "QL_trnwodtl2")
        For C1 As Integer = 0 To dtProc.Rows.Count - 1
            dtProc.Rows(C1)("wodtl2seq") = C1 + 1
        Next
        Session("TblDtlProc") = dtProc
        GVDtl2.DataSource = Session("TblDtlProc")
        GVDtl2.DataBind()

        ' Generate Detail Material
        sSql = "SELECT wodtl3seq, wodtl1seq, wodtl2seq, wodtl2desc, wodtl3reftype, wodtl3refoid, wodtl3reflongdesc, wodtl3refcode, wodtl3qty, wodtl3unitoid, wodtl3unit, wodtl3note, wodtl3qtyperunititem, wodtl3qtyperunitprocess FROM ("
        sSql &= "SELECT DISTINCT wod3.wodtl3seq, wod1.wodtl1seq, (wod2.wodtl2seq + (SELECT ISNULL(MAX(wod2x.wodtl2seq),0) FROM QL_trnwodtl2 wod2x WHERE wod2x.cmpcode=wod2.cmpcode AND wod2x.wodtl1oid<wod2.wodtl1oid AND wod2x.womstoid=wod2.womstoid)) AS wodtl2seq, '' AS wodtl2desc, wod3.wodtl3reftype, wod3.wodtl3refoid, m.itemlongdescription AS wodtl3reflongdesc, m.itemcode AS wodtl3refcode, wod3.wodtl3qty, wod3.wodtl3unitoid, g.gendesc AS wodtl3unit, wod3.wodtl3note, (wod3.wodtl3qty / wod1.wodtl1qty) AS wodtl3qtyperunititem, (wod3.wodtl3qty / wod2.wodtl2qty) AS wodtl3qtyperunitprocess FROM QL_trnwodtl3 wod3 INNER JOIN QL_trnwodtl2 wod2 ON wod2.cmpcode=wod3.cmpcode AND wod2.wodtl2oid=wod3.wodtl2oid INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=wod3.cmpcode AND wod1.wodtl1oid=wod3.wodtl1oid INNER JOIN QL_mstitem m ON m.itemoid=wod3.wodtl3refoid INNER JOIN QL_mstgen g ON g.genoid=wod3.wodtl3unitoid WHERE wod3.cmpcode='" & DDLBusUnit.SelectedValue & "' AND wod3.womstoid=" & iOid
        sSql &= ") AS tbltmp ORDER BY wodtl1seq, wodtl2seq, wodtl3seq"
        Dim dtMat As DataTable = cKon.ambiltabel(sSql, "QL_trnwodtl3")
        For C1 As Integer = 0 To dtMat.Rows.Count - 1
            dtMat.Rows(C1)("wodtl3seq") = C1 + 1
            dtMat.Rows(C1)("wodtl2desc") = "Detail Process No. " & dtMat.Rows(C1)("wodtl2seq").ToString
        Next
        Session("TblDtlMat") = dtMat
        GVDtl3.DataSource = Session("TblDtlMat")
        GVDtl3.DataBind()
    End Sub 'OK
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        'Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Transaction\trnWOCancel.aspx")
        End If
        If checkPagePermission("~\Transaction\trnWOCancel.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Job Costing MO Cancellation"
        btnProcess.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to CANCEL this data?');")
        If Not Page.IsPostBack Then
            upduser.Text = Session("UserID") : updtime.Text = GetServerTime().ToString
            InitDDL()
        End If
    End Sub 'OK

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("Success") Is Nothing And Session("Success") <> "" Then
            Response.Redirect("~\Transaction\trnWOCancel.aspx?awal=true")
        End If
    End Sub 'OK

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        ClearData()
    End Sub 'OK

    Protected Sub btnFindWO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindWO.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        BindListWO()
        cProc.SetModalPopUpExtender(btnHiddenListWO, pnlListWO, mpeListWO, True)
    End Sub 'OK

    Protected Sub btnClearWO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearWO.Click
        ClearData()
    End Sub 'OK

    Protected Sub btnFindListWO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListWO.Click
        BindListWO()
        mpeListWO.Show()
    End Sub 'OK

    Protected Sub btnAllListWO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListWO.Click
        FilterTextListWO.Text = ""
        FilterDDLListWO.SelectedIndex = -1
        BindListWO()
        mpeListWO.Show()
    End Sub 'OK

    Protected Sub gvListWO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListWO.SelectedIndexChanged
        womstoid.Text = gvListWO.SelectedDataKey.Item("womstoid").ToString
        wono.Text = gvListWO.SelectedDataKey.Item("wono").ToString
        wodate.Text = gvListWO.SelectedDataKey.Item("wodate").ToString
        wodocrefno.Text = gvListWO.SelectedDataKey.Item("wodocrefno").ToString
        womststatus.Text = gvListWO.SelectedDataKey.Item("womststatus").ToString
        womstnote.Text = gvListWO.SelectedDataKey.Item("womstnote").ToString
        cProc.SetModalPopUpExtender(btnHiddenListWO, pnlListWO, mpeListWO, False)
        ViewDetailWO(CInt(womstoid.Text))
    End Sub 'OK

    Protected Sub lkbCloseListWO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListWO.Click
        cProc.SetModalPopUpExtender(btnHiddenListWO, pnlListWO, mpeListWO, False)
    End Sub 'OK

    Protected Sub GVDtl1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVDtl1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
        End If
    End Sub 'OK

    Protected Sub GVDtl2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVDtl2.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
        End If
    End Sub 'OK

    Protected Sub GVDtl3_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVDtl3.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
        End If
    End Sub 'OK

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnWOCancel.aspx?awal=true")
    End Sub 'OK

    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnProcess.Click
        If womstoid.Text = "" Then
            showMessage("Please select Job Costing MO Data first!", 2)
            Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trnsodtl SET sodtlres3='' WHERE sodtloid IN (SELECT DISTINCT soitemdtloid FROM QL_trnwodtl1 WHERE womstoid=" & womstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_trnsomst SET somstres3='' WHERE somstoid IN (SELECT DISTINCT soitemmstoid FROM QL_trnwodtl1 WHERE womstoid=" & womstoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_trnwomst SET womststatus='Cancel', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid=" & womstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Session("Success") = "WO No. : " & wono.Text & " have been cancel successfully."
        showMessage(Session("Success"), 3)
    End Sub 'OK
#End Region

End Class