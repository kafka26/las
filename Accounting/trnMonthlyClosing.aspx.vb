Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class Accounting_trnMonthlyClosing
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public DefCounter As String = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Dim objCmd As New SqlCommand("", conn)
    Dim objReader As SqlDataReader
    Dim sSql As String = ""
    Dim cKoneksi As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Functions"
    Private Function BindClosingData(ByVal sBussinessUnit As String, ByVal iMonth As Integer, ByVal iYear As Integer) As String
        Dim tempDate As New Date(iYear, iMonth, 1)

        sSql = "SELECT TOP 1 ISNULL(crdglflag, '') FROM QL_crdgl WHERE cmpcode='" & sBussinessUnit & "' AND periodacctg='" & GetDateToPeriodAcctg(tempDate) & "'"
        BindClosingData = CStr(GetStrData(sSql))
        BindClosingData = IIf(BindClosingData = "", "OPEN", BindClosingData)
        If BindClosingData.ToUpper = "OPEN" Then
            sSql = "DECLARE @cmpcode VARCHAR(10);" & _
                "DECLARE @periode VARCHAR(10);" & _
                "DECLARE @month INTEGER;" & _
                "DECLARE @year INTEGER;" & _
                "SET @cmpcode='" & sBussinessUnit & "';" & _
                "SET @periode='" & GetDateToPeriodAcctg(tempDate) & "';" & _
                "SET @month=" & iMonth & ";" & _
                "SET @year=" & iYear & ";" & _
                "SELECT @cmpcode cmpcode, acctgoid, acctgcode, acctgdesc, amtopenidr, SUM(debetidr) AS amtdebetidr, SUM(creditidr) AS amtcreditidr, (amtopenidr + SUM(debetidr) - SUM(creditidr)) AS amtbalanceidr, amtopenusd, SUM(debetusd) AS amtdebetusd, SUM(creditusd) AS amtcreditusd, (amtopenusd + SUM(debetusd) - SUM(creditusd)) AS amtbalanceusd, seq, acctgres3, acctgdbcr FROM (" & _
                "SELECT a1.acctgoid, a1.acctgcode, a1.acctgdesc, ISNULL(crd.amtopen, 0) AS amtopenidr, ISNULL(crd.amtopenusd, 0) AS amtopenusd, 'debetidr'=(CASE WHEN gd.gldbcr='D' AND MONTH(gm.gldate)=@month AND YEAR(gm.gldate)=@year THEN gd.glamtidr ELSE 0 END), 'creditidr'=(CASE WHEN gd.gldbcr='C' AND MONTH(gm.gldate)=@month AND YEAR(gm.gldate)=@year THEN gd.glamtidr ELSE 0 END), 'debetusd'=(CASE WHEN gd.gldbcr='D' AND MONTH(gm.gldate)=@month AND YEAR(gm.gldate)=@year THEN gd.glamtusd ELSE 0 END), 'creditusd'=(CASE WHEN gd.gldbcr='C' AND MONTH(gm.gldate)=@month AND YEAR(gm.gldate)=@year THEN gd.glamtusd ELSE 0 END), 0.00 AS amtbalanceidr, 0.00 AS amtbalanceusd, 0 AS seq, acctgres3, acctgdbcr " & _
                "FROM QL_mstacctg a1 LEFT JOIN QL_crdgl crd ON crd.cmpcode=@cmpcode AND crd.acctgoid=a1.acctgoid AND crd.periodacctg=@periode LEFT JOIN QL_trngldtl gd ON gd.cmpcode=@cmpcode AND a1.acctgoid=gd.acctgoid LEFT JOIN QL_trnglmst gm ON gm.cmpcode=gd.cmpcode AND gm.glmstoid=gd.glmstoid AND gm.glflag='POST' AND gm.glmstoid>0 " & _
                "WHERE a1.acctgoid NOT IN (SELECT DISTINCT a.acctggrp3 FROM QL_mstacctg a WHERE a.acctggrp3 IS NOT NULL AND a.cmpcode=a1.cmpcode)" & _
                ") AS crd GROUP BY acctgoid, acctgcode, acctgdesc, amtopenidr, amtopenusd, seq, acctgres3, acctgdbcr ORDER BY acctgcode"
        ElseIf BindClosingData.ToUpper = "CLOSED" Then
            sSql = "SELECT '" & sBussinessUnit & "' cmpcode, a1.acctgoid, a1.acctgcode, a1.acctgdesc, ISNULL(crd.amtopen, 0.0) amtopenidr, ISNULL(crd.amtdebit, 0.0) amtdebetidr, ISNULL(crd.amtcredit, 0.0) amtcreditidr, ISNULL(crd.amtbalance, 0.0) amtbalanceidr, ISNULL(crd.amtopenusd, 0.0) amtopenusd, ISNULL(crd.amtdebitusd, 0.0) amtdebetusd, ISNULL(crd.amtcreditusd, 0.0) amtcreditusd, ISNULL(crd.amtbalanceusd, 0.0) amtbalanceusd, 0 seq, acctgres3, acctgdbcr " & _
                "FROM QL_mstacctg a1 LEFT JOIN QL_crdgl crd ON a1.acctgoid=crd.acctgoid AND crd.cmpcode='" & sBussinessUnit & "' AND crd.periodacctg='" & GetDateToPeriodAcctg(tempDate) & "' " & _
                "WHERE a1.acctgoid NOT IN (SELECT DISTINCT a.acctggrp3 FROM QL_mstacctg a WHERE a.acctggrp3 IS NOT NULL AND a.cmpcode=a1.cmpcode) ORDER BY a1.acctgcode"
        End If
        Dim dtClosingResult As DataTable : dtClosingResult = cKoneksi.ambiltabel(sSql, "QL_crdgl")
        ' Re-sequence
        For C1 As Integer = 0 To dtClosingResult.Rows.Count - 1
            Dim ed As DataRow = dtClosingResult.Rows(C1)
            ed.BeginEdit()
            ed("seq") = C1 + 1
            ed.EndEdit()
        Next
        Session("TblDtlDep") = dtClosingResult
    End Function

    Public Function GetOpen() As String
        If DDLCurrency.SelectedValue = "IDR" Then
            Return ToMaskEdit(ToDouble(Eval("amtopenidr")), 4)
        Else
            Return ToMaskEdit(ToDouble(Eval("amtopenusd")), 4)
        End If
    End Function

    Public Function GetDebet() As String
        If DDLCurrency.SelectedValue = "IDR" Then
            Return ToMaskEdit(ToDouble(Eval("amtdebetidr")), 4)
        Else
            Return ToMaskEdit(ToDouble(Eval("amtdebetusd")), 4)
        End If
    End Function

    Public Function GetCredit() As String
        If DDLCurrency.SelectedValue = "IDR" Then
            Return ToMaskEdit(ToDouble(Eval("amtcreditidr")), 4)
        Else
            Return ToMaskEdit(ToDouble(Eval("amtcreditusd")), 4)
        End If
    End Function

    Public Function GetClose() As String
        If DDLCurrency.SelectedValue = "IDR" Then
            Return ToMaskEdit(ToDouble(Eval("amtbalanceidr")), 4)
        Else
            Return ToMaskEdit(ToDouble(Eval("amtbalanceusd")), 4)
        End If
    End Function

    Public Function GetStatus(ByVal sBussinessUnit As String, ByVal iMonth As Integer, ByVal iYear As Integer) As String
        sSql = "SELECT ISNULL(closingstatus, '') FROM QL_acctgclosinghistory WHERE cmpcode='" & sBussinessUnit & "' AND closingperiod='" & GetDateToPeriodAcctg(New Date(iYear, iMonth, 1)) & "'"
        Return CStr(GetStrData(sSql))
    End Function

    Private Function PostAssetDepreciation() As String
        Dim sRet As String = ""
        ' Posting Penyusutan Fixed Assets
        sSql = "SELECT fad.assetdtloid, fad.assetmstoid, fam.assetno, fad.assetperiodvalue, fad.periodacctgoid, fad.perioddepacctgoid, (fad.assetperiodvalue * CAST(rate2res2 AS REAL)) AS assetperiodvalueusd, fam.rateoid, fam.rate2oid, CAST(rateres1 AS REAL) AS ratetoidr, CAST(rateres2 AS REAL) AS ratetousd, CAST(rate2res1 AS REAL) AS rate2toidr, CAST(rate2res2 AS REAL) AS rate2tousd FROM QL_assetdtl fad INNER JOIN QL_assetmst fam ON fam.cmpcode=fad.cmpcode AND fam.assetmstoid=fad.assetmstoid INNER JOIN QL_mstrate2 r2 ON r2.rate2oid=fam.rate2oid INNER JOIN QL_mstrate r ON r.rateoid=fam.rateoid WHERE fad.cmpcode='" & bussinessunit.SelectedValue & "' AND fam.assetmststatus='Post' AND fad.assetdtlstatus='' AND assetperiod='" & GetDateToPeriodAcctg(New Date(DDLYear.SelectedValue, DDLMonth.SelectedValue, 1)) & "' ORDER BY fam.assetno"
        Dim dtPostAsset As DataTable = cKoneksi.ambiltabel(sSql, "QL_assetdtl")
        If dtPostAsset.Rows.Count > 0 Then
            Dim iGLMstOid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
            Dim iGLDtlOid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            objCmd.Transaction = objTrans
            Try
                For C1 As Int16 = 0 To dtPostAsset.Rows.Count - 1
                    sSql = "UPDATE QL_assetdtl SET assetdtlstatus='Post', postuser='" & Session("UserID") & "', postdate=CURRENT_TIMESTAMP WHERE cmpcode='" & bussinessunit.SelectedValue & "' AND assetdtloid=" & dtPostAsset.Rows(C1)("assetdtloid").ToString
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_assetmst SET assetaccumdep=assetaccumdep + " & ToDouble(dtPostAsset.Rows(C1)("assetperiodvalue").ToString) & ", assetmststatus='Closed' WHERE cmpcode='" & bussinessunit.SelectedValue & "' AND assetmstoid=" & dtPostAsset.Rows(C1)("assetmstoid").ToString & " AND (SELECT COUNT(*) FROM QL_assetdtl WHERE cmpcode='" & bussinessunit.SelectedValue & "' AND assetmstoid=" & dtPostAsset.Rows(C1)("assetmstoid").ToString & " AND assetdtlstatus='' AND assetdtloid<>" & dtPostAsset.Rows(C1)("assetdtloid").ToString & ")=0"
                    objCmd.CommandText = sSql
                    If objCmd.ExecuteNonQuery() = 0 Then
                        sSql = "UPDATE QL_assetmst SET assetaccumdep=assetaccumdep + " & ToDouble(dtPostAsset.Rows(C1)("assetperiodvalue").ToString) & " WHERE cmpcode='" & bussinessunit.SelectedValue & "' AND assetmstoid=" & dtPostAsset.Rows(C1)("assetmstoid").ToString
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    End If
                    ' Auto Jurnal Accounting
                    ' Biaya Penyusutan
                    '       Akumulasi Penyusutan
                    ' GL MST
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLMstOid & ", '" & New Date(ToInteger(DDLYear.SelectedValue), ToInteger(DDLMonth.SelectedValue), Date.DaysInMonth(ToInteger(DDLYear.SelectedValue), ToInteger(DDLMonth.SelectedValue))) & "', '" & DDLYear.SelectedValue & DDLMonth.SelectedValue & "', 'Penyusutan Asset|No=" & dtPostAsset.Rows(C1)("assetno").ToString & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & dtPostAsset.Rows(C1)("rateoid") & ", " & dtPostAsset.Rows(C1)("rate2oid") & ", " & ToDouble(dtPostAsset.Rows(C1)("ratetoidr").ToString) & ", " & ToDouble(dtPostAsset.Rows(C1)("rate2toidr").ToString) & ", " & ToDouble(dtPostAsset.Rows(C1)("ratetousd").ToString) & ", " & ToDouble(dtPostAsset.Rows(C1)("rate2tousd").ToString) & ")"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    ' GL DTL
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLDtlOid & ", 1, " & iGLMstOid & ", " & dtPostAsset.Rows(C1)("periodacctgoid").ToString & ", 'D', " & ToDouble(dtPostAsset.Rows(C1)("assetperiodvalue").ToString) & ", '" & dtPostAsset.Rows(C1)("assetno").ToString & "', 'Penyusutan Asset|No=" & dtPostAsset.Rows(C1)("assetno").ToString & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dtPostAsset.Rows(C1)("assetperiodvalue").ToString) & ", " & ToDouble(dtPostAsset.Rows(C1)("assetperiodvalueusd").ToString) & ", 'QL_assetdtl " & dtPostAsset.Rows(C1)("assetdtloid").ToString & "')"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    iGLDtlOid += 1
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLDtlOid & ", 2, " & iGLMstOid & ", " & dtPostAsset.Rows(C1)("perioddepacctgoid").ToString & ", 'C', " & ToDouble(dtPostAsset.Rows(C1)("assetperiodvalue").ToString) & ", '" & dtPostAsset.Rows(C1)("assetno").ToString & "', 'Penyusutan Asset|No=" & dtPostAsset.Rows(C1)("assetno").ToString & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dtPostAsset.Rows(C1)("assetperiodvalue").ToString) & ", " & ToDouble(dtPostAsset.Rows(C1)("assetperiodvalueusd").ToString) & ", 'QL_assetdtl " & dtPostAsset.Rows(C1)("assetdtloid").ToString & "')"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    iGLMstOid += 1
                    iGLDtlOid += 1
                Next
                sSql = "UPDATE QL_mstoid SET lastoid=" & iGLMstOid - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_TRNGLMST'"
                objCmd.CommandText = sSql
                objCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & iGLDtlOid - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_TRNGLDTL'"
                objCmd.CommandText = sSql
                objCmd.ExecuteNonQuery()
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        Call PostAssetDepreciation()
                    Else
                        sRet = exSql.Message
                    End If
                Else
                    sRet = exSql.Message
                End If
            Catch ex As Exception
                objTrans.Rollback()
                conn.Close()
                sRet = ex.Message
            End Try
        End If
        Return sRet
    End Function

    Private Function PostBOPReal() As String
        Dim sRet As String = ""
        Dim sVarErr As String = ""
        Dim dAmtBOPRealIDR As Double = 0 : Dim dAmtBOPRealUSD As Double = 0
        Dim iBOPRealAcctgID As Integer = 0, iSelisihEffAcctgID As Integer = 0
        Dim dTotalBOP_IDR_Fix As Double = 0, dTotalBOP_USD_Fix As Double = 0
        If Not IsInterfaceExists("VAR_CLOSING_BOP", bussinessunit.SelectedValue) Then
            sVarErr = "VAR_CLOSING_BOP"
        End If
        If Not IsInterfaceExists("VAR_BOP_REAL", bussinessunit.SelectedValue) Then
            sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_BOP_REAL"
        Else
            iBOPRealAcctgID = GetAcctgOID(GetVarInterface("VAR_BOP_REAL", bussinessunit.SelectedValue), CompnyCode)
        End If
        If Not IsInterfaceExists("VAR_SELISIH_EFISIENSI", bussinessunit.SelectedValue) Then
            sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_SELISIH_EFISIENSI"
        Else
            iSelisihEffAcctgID = GetAcctgOID(GetVarInterface("VAR_SELISIH_EFISIENSI", bussinessunit.SelectedValue), CompnyCode)
        End If
        If sVarErr <> "" Then
            sRet = GetInterfaceWarning(sVarErr, "closing")
        Else
            Dim oDDL As New DropDownList
            FillDDLAcctg(oDDL, "VAR_CLOSING_BOP", bussinessunit.SelectedValue)
            Dim sAcctgOid As String = ""
            For C1 As Integer = 0 To oDDL.Items.Count - 1
                sAcctgOid &= oDDL.Items(C1).Value & ","
            Next
            sSql = "SELECT SUM(glamtidr * (CASE gldbcr WHEN 'C' THEN -1 ELSE 1 END)) glamtidr, SUM(glamtusd * (CASE gldbcr WHEN 'C' THEN -1 ELSE 1 END)) glamtusd, acctgoid FROM QL_trngldtl gld INNER JOIN QL_trnglmst glm ON glm.cmpcode=gld.cmpcode AND glm.glmstoid=gld.glmstoid WHERE gld.cmpcode='" & bussinessunit.SelectedValue & "' AND acctgoid IN (" & iBOPRealAcctgID & ") AND MONTH(gldate)=" & ToInteger(DDLMonth.SelectedValue) & " AND YEAR(gldate)=" & ToInteger(DDLYear.SelectedValue) & " GROUP BY acctgoid"
            Dim dtGL2 As DataTable = cKoneksi.ambiltabel(sSql, "QL_trngldtl2")
            dAmtBOPRealIDR = ToDouble(dtGL2.Compute("SUM(glamtidr)", "").ToString)
            dAmtBOPRealUSD = ToDouble(dtGL2.Compute("SUM(glamtusd)", "").ToString)

            If sAcctgOid <> "" Then
                sAcctgOid = Left(sAcctgOid, sAcctgOid.Length - 1)
                sSql = "SELECT SUM(glamtidr * (CASE gldbcr WHEN 'C' THEN -1 ELSE 1 END)) glamtidr, SUM(glamtusd * (CASE gldbcr WHEN 'C' THEN -1 ELSE 1 END)) glamtusd, acctgoid FROM QL_trngldtl gld INNER JOIN QL_trnglmst glm ON glm.cmpcode=gld.cmpcode AND glm.glmstoid=gld.glmstoid WHERE gld.cmpcode='" & bussinessunit.SelectedValue & "' AND acctgoid IN (" & sAcctgOid & ") AND MONTH(gldate)=" & ToInteger(DDLMonth.SelectedValue) & " AND YEAR(gldate)=" & ToInteger(DDLYear.SelectedValue) & " GROUP BY acctgoid"
                Dim dtGL As DataTable = cKoneksi.ambiltabel(sSql, "QL_trngldtl")
                Dim iGLMstOid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
                Dim iGLDtlOid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
                Dim dTotalBOP_IDR As Double = 0, dTotalBOP_USD As Double = 0
                Dim iAcctgID_db As Integer = 0, iAcctgID_cr As Integer = 0
                Dim iPengali_IDR As Integer = 1, iPengali_USD As Integer = 1, iPengali_IDR2 As Integer = 0, iPengali_USD2 As Integer = 0
                Dim dAmtIDR As Double = 0, dAmtUSD As Double = 0
                Dim objTrans As SqlClient.SqlTransaction
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If
                objTrans = conn.BeginTransaction()
                objCmd.Transaction = objTrans
                Try
                    For C1 As Integer = 0 To dtGL.Rows.Count - 1
                        If ToDouble(dtGL.Rows(C1)("glamtidr").ToString) <> 0 Or ToDouble(dtGL.Rows(C1)("glamtusd").ToString) <> 0 Then
                            dTotalBOP_IDR += ToDouble(dtGL.Rows(C1)("glamtidr").ToString)
                            dTotalBOP_USD += ToDouble(dtGL.Rows(C1)("glamtusd").ToString)
                            If ToDouble(dtGL.Rows(C1)("glamtidr").ToString) >= 0 And ToDouble(dtGL.Rows(C1)("glamtusd").ToString) >= 0 Then
                                iAcctgID_db = iBOPRealAcctgID : iAcctgID_cr = ToInteger(dtGL.Rows(C1)("acctgoid").ToString)
                                iPengali_IDR = 1 : iPengali_USD = 1 : iPengali_IDR2 = 0 : iPengali_USD2 = 0
                                If ToDouble(dtGL.Rows(C1)("glamtidr").ToString) = 0 Then
                                    iPengali_IDR = 0
                                End If
                                If ToDouble(dtGL.Rows(C1)("glamtusd").ToString) = 0 Then
                                    iPengali_USD = 0
                                End If
                            ElseIf ToDouble(dtGL.Rows(C1)("glamtidr").ToString) < 0 And ToDouble(dtGL.Rows(C1)("glamtusd").ToString) < 0 Then
                                iAcctgID_db = ToInteger(dtGL.Rows(C1)("acctgoid").ToString) : iAcctgID_cr = iBOPRealAcctgID
                                iPengali_IDR = -1 : iPengali_USD = -1 : iPengali_IDR2 = 0 : iPengali_USD2 = 0
                            ElseIf ToDouble(dtGL.Rows(C1)("glamtidr").ToString) >= 0 And ToDouble(dtGL.Rows(C1)("glamtusd").ToString) < 0 Then
                                iAcctgID_db = iBOPRealAcctgID : iAcctgID_cr = ToInteger(dtGL.Rows(C1)("acctgoid").ToString)
                                iPengali_IDR = 1 : iPengali_USD = 0 : iPengali_IDR2 = 0 : iPengali_USD2 = -1
                                If ToDouble(dtGL.Rows(C1)("glamtidr").ToString) = 0 Then
                                    iPengali_IDR = 0
                                End If
                            ElseIf ToDouble(dtGL.Rows(C1)("glamtidr").ToString) < 0 And ToDouble(dtGL.Rows(C1)("glamtusd").ToString) >= 0 Then
                                iAcctgID_db = iBOPRealAcctgID : iAcctgID_cr = ToInteger(dtGL.Rows(C1)("acctgoid").ToString)
                                iPengali_IDR = 0 : iPengali_USD = 1 : iPengali_IDR2 = -1 : iPengali_USD2 = 0
                                If ToDouble(dtGL.Rows(C1)("glamtusd").ToString) = 0 Then
                                    iPengali_USD = 0
                                End If
                            End If
                            If iPengali_IDR <> 0 Or iPengali_USD <> 0 Then
                                ' GL MST
                                sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLMstOid & ", '" & New Date(ToInteger(DDLYear.SelectedValue), ToInteger(DDLMonth.SelectedValue), Date.DaysInMonth(ToInteger(DDLYear.SelectedValue), ToInteger(DDLMonth.SelectedValue))) & "', '" & DDLYear.SelectedValue & DDLMonth.SelectedValue & "', 'Monthly Closing Auto Posting', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 0, 0, 1, 1, 0, 0)"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                ' GL DTL
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLDtlOid & ", 1, " & iGLMstOid & ", " & iAcctgID_db & ", 'D', " & ToDouble(dtGL.Rows(C1)("glamtidr").ToString) * iPengali_IDR & ", '', 'Monthly Closing Auto Posting', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dtGL.Rows(C1)("glamtidr").ToString) * iPengali_IDR & ", " & ToDouble(dtGL.Rows(C1)("glamtusd").ToString) * iPengali_USD & ", '')"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                iGLDtlOid += 1
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLDtlOid & ", 2, " & iGLMstOid & ", " & iAcctgID_cr & ", 'C', " & ToDouble(dtGL.Rows(C1)("glamtidr").ToString) * iPengali_IDR & ", '', 'Monthly Closing Auto Posting', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dtGL.Rows(C1)("glamtidr").ToString) * iPengali_IDR & ", " & ToDouble(dtGL.Rows(C1)("glamtusd").ToString) * iPengali_USD & ", '')"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                iGLMstOid += 1 : iGLDtlOid += 1
                            End If
                            If iPengali_IDR2 <> 0 Or iPengali_USD2 <> 0 Then
                                ' GL MST
                                sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLMstOid & ", '" & New Date(ToInteger(DDLYear.SelectedValue), ToInteger(DDLMonth.SelectedValue), Date.DaysInMonth(ToInteger(DDLYear.SelectedValue), ToInteger(DDLMonth.SelectedValue))) & "', '" & DDLYear.SelectedValue & DDLMonth.SelectedValue & "', 'Monthly Closing Auto Posting', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 0, 0, 1, 1, 0, 0)"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                ' GL DTL
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLDtlOid & ", 1, " & iGLMstOid & ", " & iAcctgID_cr & ", 'D', " & ToDouble(dtGL.Rows(C1)("glamtidr").ToString) * iPengali_IDR2 & ", '', 'Monthly Closing Auto Posting', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dtGL.Rows(C1)("glamtidr").ToString) * iPengali_IDR2 & ", " & ToDouble(dtGL.Rows(C1)("glamtusd").ToString) * iPengali_USD2 & ", '')"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                iGLDtlOid += 1
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLDtlOid & ", 2, " & iGLMstOid & ", " & iAcctgID_db & ", 'C', " & ToDouble(dtGL.Rows(C1)("glamtidr").ToString) * iPengali_IDR2 & ", '', 'Monthly Closing Auto Posting', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dtGL.Rows(C1)("glamtidr").ToString) * iPengali_IDR2 & ", " & ToDouble(dtGL.Rows(C1)("glamtusd").ToString) * iPengali_USD2 & ", '')"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                iGLMstOid += 1 : iGLDtlOid += 1
                            End If
                        End If
                    Next
                    If dTotalBOP_IDR <> 0 Or dTotalBOP_USD <> 0 Then
                        Dim iCOA_db As Integer = 0, iCOA_cr As Integer = 0, iCOA_db2 As Integer = 0, iCOA_cr2 As Integer = 0
                        Dim dTotalBOP_IDR2 As Double = 0, dTotalBOP_USD2 As Double = 0
                        If dTotalBOP_IDR >= 0 And dTotalBOP_USD >= 0 Then
                            iCOA_db = iSelisihEffAcctgID : iCOA_cr = iBOPRealAcctgID
                        ElseIf dTotalBOP_IDR < 0 And dTotalBOP_USD < 0 Then
                            dTotalBOP_IDR *= -1 : dTotalBOP_USD *= -1
                            iCOA_db = iBOPRealAcctgID : iCOA_cr = iSelisihEffAcctgID
                        ElseIf dTotalBOP_IDR >= 0 And dTotalBOP_USD < 0 Then
                            dTotalBOP_USD2 = -1 * dTotalBOP_USD : dTotalBOP_USD = 0
                            iCOA_db = iSelisihEffAcctgID : iCOA_cr = iBOPRealAcctgID
                        Else 'dTotalBOP_IDR < 0 And dTotalBOP_USD >= 0
                            dTotalBOP_IDR2 = -1 * dTotalBOP_IDR : dTotalBOP_IDR = 0
                            iCOA_db = iBOPRealAcctgID : iCOA_cr = iSelisihEffAcctgID
                        End If

                        dTotalBOP_IDR_Fix = dAmtBOPRealIDR - dTotalBOP_IDR
                        dTotalBOP_USD_Fix = dAmtBOPRealUSD - dTotalBOP_USD

                        If dTotalBOP_IDR_Fix >= 0 And dTotalBOP_USD_Fix >= 0 Then
                            iCOA_db = iSelisihEffAcctgID : iCOA_cr = iBOPRealAcctgID
                        ElseIf dTotalBOP_IDR_Fix < 0 And dTotalBOP_USD_Fix < 0 Then
                            dTotalBOP_IDR_Fix *= -1 : dTotalBOP_USD_Fix *= -1
                            iCOA_db = iBOPRealAcctgID : iCOA_cr = iSelisihEffAcctgID
                        ElseIf dTotalBOP_IDR_Fix >= 0 And dTotalBOP_USD_Fix < 0 Then
                            dTotalBOP_USD2 = -1 * dTotalBOP_USD_Fix : dTotalBOP_USD_Fix = 0
                            iCOA_db = iSelisihEffAcctgID : iCOA_cr = iBOPRealAcctgID
                        Else 'dTotalBOP_IDR_Fix < 0 And dTotalBOP_USD_Fix >= 0
                            dTotalBOP_IDR2 = -1 * dTotalBOP_IDR_Fix : dTotalBOP_IDR_Fix = 0
                            iCOA_db = iBOPRealAcctgID : iCOA_cr = iSelisihEffAcctgID
                        End If

                        ' GL MST
                        sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLMstOid & ", '" & New Date(ToInteger(DDLYear.SelectedValue), ToInteger(DDLMonth.SelectedValue), Date.DaysInMonth(ToInteger(DDLYear.SelectedValue), ToInteger(DDLMonth.SelectedValue))) & "', '" & DDLYear.SelectedValue & DDLMonth.SelectedValue & "', 'Monthly Closing Auto Posting', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 0, 0, 1, 1, 0, 0)"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        ' GL DTL
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLDtlOid & ", 1, " & iGLMstOid & ", " & iCOA_db & ", 'D', " & dTotalBOP_IDR_Fix & ", '', 'Monthly Closing Auto Posting', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalBOP_IDR_Fix & ", " & dTotalBOP_USD_Fix & ", '')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        iGLDtlOid += 1
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLDtlOid & ", 2, " & iGLMstOid & ", " & iCOA_cr & ", 'C', " & dTotalBOP_IDR_Fix & ", '', 'Monthly Closing Auto Posting', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalBOP_IDR_Fix & ", " & dTotalBOP_USD_Fix & ", '')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        iGLMstOid += 1 : iGLDtlOid += 1
                        If dTotalBOP_IDR2 > 0 Or dTotalBOP_USD2 > 0 Then
                            ' GL MST
                            sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLMstOid & ", '" & New Date(ToInteger(DDLYear.SelectedValue), ToInteger(DDLMonth.SelectedValue), Date.DaysInMonth(ToInteger(DDLYear.SelectedValue), ToInteger(DDLMonth.SelectedValue))) & "', '" & DDLYear.SelectedValue & DDLMonth.SelectedValue & "', 'Monthly Closing Auto Posting', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 0, 0, 1, 1, 0, 0)"
                            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                            ' GL DTL
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLDtlOid & ", 1, " & iGLMstOid & ", " & iCOA_cr & ", 'D', " & dTotalBOP_IDR2 & ", '', 'Monthly Closing Auto Posting', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalBOP_IDR2 & ", " & dTotalBOP_USD2 & ", '')"
                            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                            iGLDtlOid += 1
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLDtlOid & ", 2, " & iGLMstOid & ", " & iCOA_db & ", 'C', " & dTotalBOP_IDR2 & ", '', 'Monthly Closing Auto Posting', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalBOP_IDR2 & ", " & dTotalBOP_USD2 & ", '')"
                            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                            iGLMstOid += 1 : iGLDtlOid += 1
                        End If
                    End If
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGLMstOid - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_TRNGLMST'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGLDtlOid - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_TRNGLDTL'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    objTrans.Commit() : conn.Close()
                Catch exSql As SqlException
                    objTrans.Rollback() : conn.Close()
                    If ToDouble(Session("ErrorCounter")) < 5 Then
                        If exSql.Number = 2627 Then
                            Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                            Call PostBOPReal()
                        Else
                            sRet = exSql.Message
                        End If
                    Else
                        sRet = exSql.Message
                    End If
                Catch ex As Exception
                    objTrans.Rollback() : conn.Close()
                    sRet = ex.Message
                End Try
            End If
        End If
        Return sRet
    End Function

    Private Function PostDLC() As String
        Dim sRet As String = ""
        Dim sVarErr As String = ""
        Dim dAmtBOPRealIDR As Double = 0 : Dim dAmtBOPRealUSD As Double = 0
        Dim iBOPRealAcctgID As Integer = 0, iSelisihEffAcctgID As Integer = 0
        If Not IsInterfaceExists("VAR_CLOSING_DLC", bussinessunit.SelectedValue) Then
            sVarErr = "VAR_CLOSING_DLC"
        End If
        If Not IsInterfaceExists("VAR_DL_ASSIGNMENT", bussinessunit.SelectedValue) Then
            sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_DL_ASSIGNMENT"
        Else
            iBOPRealAcctgID = GetAcctgOID(GetVarInterface("VAR_DL_ASSIGNMENT", bussinessunit.SelectedValue), CompnyCode)
        End If
        If Not IsInterfaceExists("VAR_SELISIH_EFISIENSI", bussinessunit.SelectedValue) Then
            sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_SELISIH_EFISIENSI"
        Else
            iSelisihEffAcctgID = GetAcctgOID(GetVarInterface("VAR_SELISIH_EFISIENSI", bussinessunit.SelectedValue), CompnyCode)
        End If
        If sVarErr <> "" Then
            sRet = GetInterfaceWarning(sVarErr, "closing")
        Else
            Dim oDDL As New DropDownList
            FillDDLAcctg(oDDL, "VAR_CLOSING_DLC", bussinessunit.SelectedValue)
            Dim sAcctgOid As String = ""
            For C1 As Integer = 0 To oDDL.Items.Count - 1
                sAcctgOid &= oDDL.Items(C1).Value & ","
            Next
            sSql = "SELECT SUM(glamtidr * (CASE gldbcr WHEN 'C' THEN -1 ELSE 1 END)) glamtidr, SUM(glamtusd * (CASE gldbcr WHEN 'C' THEN -1 ELSE 1 END)) glamtusd, acctgoid FROM QL_trngldtl gld INNER JOIN QL_trnglmst glm ON glm.cmpcode=gld.cmpcode AND glm.glmstoid=gld.glmstoid WHERE gld.cmpcode='" & bussinessunit.SelectedValue & "' AND acctgoid IN (" & iBOPRealAcctgID & ") AND gldate<='" & Format(New Date(DDLYear.SelectedValue, DDLMonth.SelectedValue, Date.DaysInMonth(DDLYear.SelectedValue, DDLMonth.SelectedValue)), "MM/dd/yyyy") & " 23:59:59' GROUP BY acctgoid"
            Dim dtGL2 As DataTable = cKoneksi.ambiltabel(sSql, "QL_trngldtl2")
            dAmtBOPRealIDR = ToDouble(dtGL2.Compute("SUM(glamtidr)", "").ToString)
            dAmtBOPRealUSD = ToDouble(dtGL2.Compute("SUM(glamtusd)", "").ToString)

            If sAcctgOid <> "" Then
                sAcctgOid = Left(sAcctgOid, sAcctgOid.Length - 1)
                sSql = "SELECT SUM(glamtidr * (CASE gldbcr WHEN 'C' THEN -1 ELSE 1 END)) glamtidr, SUM(glamtusd * (CASE gldbcr WHEN 'C' THEN -1 ELSE 1 END)) glamtusd, acctgoid FROM QL_trngldtl gld INNER JOIN QL_trnglmst glm ON glm.cmpcode=gld.cmpcode AND glm.glmstoid=gld.glmstoid WHERE gld.cmpcode='" & bussinessunit.SelectedValue & "' AND acctgoid IN (" & sAcctgOid & ") AND MONTH(gldate)=" & ToInteger(DDLMonth.SelectedValue) & " AND YEAR(gldate)=" & ToInteger(DDLYear.SelectedValue) & " GROUP BY acctgoid"
                Dim dtGL As DataTable = cKoneksi.ambiltabel(sSql, "QL_trngldtl")
                Dim iGLMstOid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
                Dim iGLDtlOid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
                Dim dTotalBOP_IDR As Double = 0, dTotalBOP_USD As Double = 0
                Dim iAcctgID_db As Integer = 0, iAcctgID_cr As Integer = 0
                Dim iPengali_IDR As Integer = 1, iPengali_USD As Integer = 1, iPengali_IDR2 As Integer = 0, iPengali_USD2 As Integer = 0
                Dim dAmtIDR As Double = 0, dAmtUSD As Double = 0
                Dim objTrans As SqlClient.SqlTransaction
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If
                objTrans = conn.BeginTransaction()
                objCmd.Transaction = objTrans
                Try
                    For C1 As Integer = 0 To dtGL.Rows.Count - 1
                        If ToDouble(dtGL.Rows(C1)("glamtidr").ToString) <> 0 Or ToDouble(dtGL.Rows(C1)("glamtusd").ToString) <> 0 Then
                            dTotalBOP_IDR += ToDouble(dtGL.Rows(C1)("glamtidr").ToString)
                            dTotalBOP_USD += ToDouble(dtGL.Rows(C1)("glamtusd").ToString)
                            If ToDouble(dtGL.Rows(C1)("glamtidr").ToString) >= 0 And ToDouble(dtGL.Rows(C1)("glamtusd").ToString) >= 0 Then
                                iAcctgID_db = iBOPRealAcctgID : iAcctgID_cr = ToInteger(dtGL.Rows(C1)("acctgoid").ToString)
                                iPengali_IDR = 1 : iPengali_USD = 1 : iPengali_IDR2 = 0 : iPengali_USD2 = 0
                                If ToDouble(dtGL.Rows(C1)("glamtidr").ToString) = 0 Then
                                    iPengali_IDR = 0
                                End If
                                If ToDouble(dtGL.Rows(C1)("glamtusd").ToString) = 0 Then
                                    iPengali_USD = 0
                                End If
                            ElseIf ToDouble(dtGL.Rows(C1)("glamtidr").ToString) < 0 And ToDouble(dtGL.Rows(C1)("glamtusd").ToString) < 0 Then
                                iAcctgID_db = ToInteger(dtGL.Rows(C1)("acctgoid").ToString) : iAcctgID_cr = iBOPRealAcctgID
                                iPengali_IDR = -1 : iPengali_USD = -1 : iPengali_IDR2 = 0 : iPengali_USD2 = 0
                            ElseIf ToDouble(dtGL.Rows(C1)("glamtidr").ToString) >= 0 And ToDouble(dtGL.Rows(C1)("glamtusd").ToString) < 0 Then
                                iAcctgID_db = iBOPRealAcctgID : iAcctgID_cr = ToInteger(dtGL.Rows(C1)("acctgoid").ToString)
                                iPengali_IDR = 1 : iPengali_USD = 0 : iPengali_IDR2 = 0 : iPengali_USD2 = -1
                                If ToDouble(dtGL.Rows(C1)("glamtidr").ToString) = 0 Then
                                    iPengali_IDR = 0
                                End If
                            ElseIf ToDouble(dtGL.Rows(C1)("glamtidr").ToString) < 0 And ToDouble(dtGL.Rows(C1)("glamtusd").ToString) >= 0 Then
                                iAcctgID_db = iBOPRealAcctgID : iAcctgID_cr = ToInteger(dtGL.Rows(C1)("acctgoid").ToString)
                                iPengali_IDR = 0 : iPengali_USD = 1 : iPengali_IDR2 = -1 : iPengali_USD2 = 0
                                If ToDouble(dtGL.Rows(C1)("glamtusd").ToString) = 0 Then
                                    iPengali_USD = 0
                                End If
                            End If
                            If iPengali_IDR <> 0 Or iPengali_USD <> 0 Then
                                ' GL MST
                                sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLMstOid & ", '" & New Date(ToInteger(DDLYear.SelectedValue), ToInteger(DDLMonth.SelectedValue), Date.DaysInMonth(ToInteger(DDLYear.SelectedValue), ToInteger(DDLMonth.SelectedValue))) & " 00:00:00', '" & DDLYear.SelectedValue & DDLMonth.SelectedValue & "', 'Monthly Closing Auto Posting', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 0, 0, 1, 1, 0, 0)"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                ' GL DTL
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLDtlOid & ", 1, " & iGLMstOid & ", " & iAcctgID_db & ", 'D', " & ToDouble(dtGL.Rows(C1)("glamtidr").ToString) * iPengali_IDR & ", '', 'Monthly Closing Auto Posting', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dtGL.Rows(C1)("glamtidr").ToString) * iPengali_IDR & ", " & ToDouble(dtGL.Rows(C1)("glamtusd").ToString) * iPengali_USD & ", '')"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                iGLDtlOid += 1
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLDtlOid & ", 2, " & iGLMstOid & ", " & iAcctgID_cr & ", 'C', " & ToDouble(dtGL.Rows(C1)("glamtidr").ToString) * iPengali_IDR & ", '', 'Monthly Closing Auto Posting', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dtGL.Rows(C1)("glamtidr").ToString) * iPengali_IDR & ", " & ToDouble(dtGL.Rows(C1)("glamtusd").ToString) * iPengali_USD & ", '')"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                iGLMstOid += 1 : iGLDtlOid += 1
                            End If
                            If iPengali_IDR2 <> 0 Or iPengali_USD2 <> 0 Then
                                ' GL MST
                                sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLMstOid & ", '" & New Date(ToInteger(DDLYear.SelectedValue), ToInteger(DDLMonth.SelectedValue), Date.DaysInMonth(ToInteger(DDLYear.SelectedValue), ToInteger(DDLMonth.SelectedValue))) & " 00:00:00', '" & DDLYear.SelectedValue & DDLMonth.SelectedValue & "', 'Monthly Closing Auto Posting', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 0, 0, 1, 1, 0, 0)"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                ' GL DTL
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLDtlOid & ", 1, " & iGLMstOid & ", " & iAcctgID_cr & ", 'D', " & ToDouble(dtGL.Rows(C1)("glamtidr").ToString) * iPengali_IDR2 & ", '', 'Monthly Closing Auto Posting', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dtGL.Rows(C1)("glamtidr").ToString) * iPengali_IDR2 & ", " & ToDouble(dtGL.Rows(C1)("glamtusd").ToString) * iPengali_USD2 & ", '')"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                iGLDtlOid += 1
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLDtlOid & ", 2, " & iGLMstOid & ", " & iAcctgID_db & ", 'C', " & ToDouble(dtGL.Rows(C1)("glamtidr").ToString) * iPengali_IDR2 & ", '', 'Monthly Closing Auto Posting', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dtGL.Rows(C1)("glamtidr").ToString) * iPengali_IDR2 & ", " & ToDouble(dtGL.Rows(C1)("glamtusd").ToString) * iPengali_USD2 & ", '')"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                iGLMstOid += 1 : iGLDtlOid += 1
                            End If
                        End If
                    Next
                    If dTotalBOP_IDR <> 0 Or dTotalBOP_USD <> 0 Then
                        Dim iCOA_db As Integer = 0, iCOA_cr As Integer = 0, iCOA_db2 As Integer = 0, iCOA_cr2 As Integer = 0
                        Dim dTotalBOP_IDR2 As Double = 0, dTotalBOP_USD2 As Double = 0

                        If dTotalBOP_IDR >= 0 And dTotalBOP_USD >= 0 Then
                            iCOA_db = iSelisihEffAcctgID : iCOA_cr = iBOPRealAcctgID
                        ElseIf dTotalBOP_IDR < 0 And dTotalBOP_USD < 0 Then
                            dTotalBOP_IDR *= -1 : dTotalBOP_USD *= -1
                            iCOA_db = iBOPRealAcctgID : iCOA_cr = iSelisihEffAcctgID
                        ElseIf dTotalBOP_IDR >= 0 And dTotalBOP_USD < 0 Then
                            dTotalBOP_USD2 = -1 * dTotalBOP_USD : dTotalBOP_USD = 0
                            iCOA_db = iSelisihEffAcctgID : iCOA_cr = iBOPRealAcctgID
                        Else 'dTotalBOP_IDR < 0 And dTotalBOP_USD >= 0
                            dTotalBOP_IDR2 = -1 * dTotalBOP_IDR : dTotalBOP_IDR = 0
                            iCOA_db = iBOPRealAcctgID : iCOA_cr = iSelisihEffAcctgID
                        End If

                        dTotalBOP_IDR = dAmtBOPRealIDR + dTotalBOP_IDR
                        dTotalBOP_USD = dAmtBOPRealUSD + dTotalBOP_USD

                        If dTotalBOP_IDR >= 0 And dTotalBOP_USD >= 0 Then
                            iCOA_db = iSelisihEffAcctgID : iCOA_cr = iBOPRealAcctgID
                        ElseIf dTotalBOP_IDR < 0 And dTotalBOP_USD < 0 Then
                            dTotalBOP_IDR *= -1 : dTotalBOP_USD *= -1
                            iCOA_db = iBOPRealAcctgID : iCOA_cr = iSelisihEffAcctgID
                        ElseIf dTotalBOP_IDR >= 0 And dTotalBOP_USD < 0 Then
                            dTotalBOP_USD2 = -1 * dTotalBOP_USD : dTotalBOP_USD = 0
                            iCOA_db = iSelisihEffAcctgID : iCOA_cr = iBOPRealAcctgID
                        Else 'dTotalBOP_IDR < 0 And dTotalBOP_USD >= 0
                            dTotalBOP_IDR2 = -1 * dTotalBOP_IDR : dTotalBOP_IDR = 0
                            iCOA_db = iBOPRealAcctgID : iCOA_cr = iSelisihEffAcctgID
                        End If

                        ' GL MST
                        sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLMstOid & ", '" & New Date(ToInteger(DDLYear.SelectedValue), ToInteger(DDLMonth.SelectedValue), Date.DaysInMonth(ToInteger(DDLYear.SelectedValue), ToInteger(DDLMonth.SelectedValue))) & " 23:59:59', '" & DDLYear.SelectedValue & DDLMonth.SelectedValue & "', 'Monthly Closing Auto Posting', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 0, 0, 1, 1, 0, 0)"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        ' GL DTL
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLDtlOid & ", 1, " & iGLMstOid & ", " & iCOA_db & ", 'D', " & dTotalBOP_IDR & ", '', 'Monthly Closing Auto Posting', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalBOP_IDR & ", " & dTotalBOP_USD & ", '')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        iGLDtlOid += 1
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLDtlOid & ", 2, " & iGLMstOid & ", " & iCOA_cr & ", 'C', " & dTotalBOP_IDR & ", '', 'Monthly Closing Auto Posting', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalBOP_IDR & ", " & dTotalBOP_USD & ", '')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        iGLMstOid += 1 : iGLDtlOid += 1
                        If dTotalBOP_IDR2 > 0 Or dTotalBOP_USD2 > 0 Then
                            ' GL MST
                            sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLMstOid & ", '" & New Date(ToInteger(DDLYear.SelectedValue), ToInteger(DDLMonth.SelectedValue), Date.DaysInMonth(ToInteger(DDLYear.SelectedValue), ToInteger(DDLMonth.SelectedValue))) & "  23:59:59', '" & DDLYear.SelectedValue & DDLMonth.SelectedValue & "', 'Monthly Closing Auto Posting', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 0, 0, 1, 1, 0, 0)"
                            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                            ' GL DTL
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLDtlOid & ", 1, " & iGLMstOid & ", " & iCOA_cr & ", 'D', " & dTotalBOP_IDR2 & ", '', 'Monthly Closing Auto Posting', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalBOP_IDR2 & ", " & dTotalBOP_USD2 & ", '')"
                            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                            iGLDtlOid += 1
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLDtlOid & ", 2, " & iGLMstOid & ", " & iCOA_db & ", 'C', " & dTotalBOP_IDR2 & ", '', 'Monthly Closing Auto Posting', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalBOP_IDR2 & ", " & dTotalBOP_USD2 & ", '')"
                            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                            iGLMstOid += 1 : iGLDtlOid += 1
                        End If
                    End If
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGLMstOid - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_TRNGLMST'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGLDtlOid - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_TRNGLDTL'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    objTrans.Commit() : conn.Close()
                Catch exSql As SqlException
                    objTrans.Rollback() : conn.Close()
                    If ToDouble(Session("ErrorCounter")) < 5 Then
                        If exSql.Number = 2627 Then
                            Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                            Call PostBOPReal()
                        Else
                            sRet = exSql.Message
                        End If
                    Else
                        sRet = exSql.Message
                    End If
                Catch ex As Exception
                    objTrans.Rollback() : conn.Close()
                    sRet = ex.Message
                End Try
            End If
        End If
        Return sRet
    End Function

    Private Function PostSelisihKurs() As String
        Dim sRet As String = ""
        Dim sVarErr As String = ""
        Dim iDiffIDRAcctgID As Integer = 0, iDiffUSDAcctgID As Integer = 0
        If Not IsInterfaceExists("VAR_DIFF_CURR_IDR", bussinessunit.SelectedValue) Then
            sVarErr &= "VAR_DIFF_CURR_IDR"
        Else
            iDiffIDRAcctgID = GetAcctgOID(GetVarInterface("VAR_DIFF_CURR_IDR", bussinessunit.SelectedValue), CompnyCode)
        End If
        If Not IsInterfaceExists("VAR_DIFF_CURR_USD", bussinessunit.SelectedValue) Then
            sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_DIFF_CURR_USD"
        Else
            iDiffUSDAcctgID = GetAcctgOID(GetVarInterface("VAR_DIFF_CURR_USD", bussinessunit.SelectedValue), CompnyCode)
        End If
        Dim iNextMonth As Integer = ToInteger(DDLMonth.SelectedValue) + 1, iYear As Integer = ToInteger(DDLYear.SelectedValue)
        If ToInteger(DDLMonth.SelectedValue) = 12 Then iNextMonth = 1 : iYear += 1
        If sVarErr <> "" Then
            sRet = GetInterfaceWarning(sVarErr, "closing")
        Else
            Dim oDDL_Cash As New DropDownList, oDDL_Bank As New DropDownList
            FillDDLAcctg(oDDL_Cash, "VAR_CASH", bussinessunit.SelectedValue)
            FillDDLAcctg(oDDL_Bank, "VAR_BANK", bussinessunit.SelectedValue)
            Dim sAcctgOid_Cash As String = "", sAcctgOid_Bank As String = ""
            For C1 As Integer = 0 To oDDL_Cash.Items.Count - 1
                sAcctgOid_Cash &= oDDL_Cash.Items(C1).Value & ","
            Next
            For C1 As Integer = 0 To oDDL_Bank.Items.Count - 1
                sAcctgOid_Bank &= oDDL_Bank.Items(C1).Value & ","
            Next
            If sAcctgOid_Cash <> "" Then
                sAcctgOid_Cash = Left(sAcctgOid_Cash, sAcctgOid_Cash.Length - 1)
                sSql = "SELECT SUM(glamt * (CASE gldbcr WHEN 'C' THEN -1 ELSE 1 END)) glamt, SUM(glamtidr * (CASE gldbcr WHEN 'C' THEN -1 ELSE 1 END)) glamtidr, SUM(glamtusd * (CASE gldbcr WHEN 'C' THEN -1 ELSE 1 END)) glamtusd, gld.acctgoid, a.curroid, ISNULL((SELECT CAST(rate2res1 AS REAL) FROM QL_mstrate2 r2 WHERE r2.curroid=a.curroid AND rate2month=" & iNextMonth & " AND rate2year=" & iYear & "), 0.0) ratetoidr, ISNULL((SELECT CAST(rate2res2 AS REAL) FROM QL_mstrate2 r2 WHERE r2.curroid=a.curroid AND rate2month=" & iNextMonth & " AND rate2year=" & iYear & "), 0.0) ratetousd FROM QL_trngldtl gld INNER JOIN QL_trnglmst glm ON glm.cmpcode=gld.cmpcode AND glm.glmstoid=gld.glmstoid INNER JOIN QL_mstacctg a ON a.acctgoid=gld.acctgoid WHERE gld.cmpcode='" & bussinessunit.SelectedValue & "' /*AND gld.glnote<>'Monthly Closing Auto Posting'*/ AND gld.acctgoid IN (" & sAcctgOid_Cash & ") AND gldate<=CAST('" & Format(New Date(DDLYear.SelectedValue, DDLMonth.SelectedValue, Date.DaysInMonth(DDLYear.SelectedValue, DDLMonth.SelectedValue)), "MM/dd/yyyy") & " 23:59:59' AS DATETIME) GROUP BY gld.acctgoid, a.curroid"
                Dim dtGL As DataTable = cKoneksi.ambiltabel(sSql, "QL_trngldtl")
                Dim iGLMstOid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
                Dim iGLDtlOid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
                Dim iAcctgID_db As Integer = 0, iAcctgID_cr As Integer = 0
                Dim objTrans As SqlClient.SqlTransaction
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If
                objTrans = conn.BeginTransaction()
                objCmd.Transaction = objTrans
                Try
                    For C1 As Integer = 0 To dtGL.Rows.Count - 1
                        If ToDouble(dtGL.Rows(C1)("glamt").ToString) <> 0 Then
                            If (Math.Round(ToDouble(dtGL.Rows(C1)("glamtidr").ToString), 4) <> Math.Round(ToDouble(dtGL.Rows(C1)("glamt").ToString) * ToDouble(dtGL.Rows(C1)("ratetoidr").ToString), 4)) And ToInteger(dtGL.Rows(C1)("curroid").ToString) <> 1 Then
                                Dim dSelisih As Double = Math.Round(ToDouble(dtGL.Rows(C1)("glamtidr").ToString), 4) - Math.Round(ToDouble(dtGL.Rows(C1)("glamt").ToString) * ToDouble(dtGL.Rows(C1)("ratetoidr").ToString), 4)
                                iAcctgID_db = ToInteger(dtGL.Rows(C1)("acctgoid").ToString) : iAcctgID_cr = iDiffIDRAcctgID
                                If dSelisih < 0 Then iAcctgID_db = iDiffIDRAcctgID : iAcctgID_cr = ToInteger(dtGL.Rows(C1)("acctgoid").ToString) : dSelisih *= -1
                                ' GL MST
                                sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLMstOid & ", '" & New Date(ToInteger(DDLYear.SelectedValue), ToInteger(DDLMonth.SelectedValue), Date.DaysInMonth(ToInteger(DDLYear.SelectedValue), ToInteger(DDLMonth.SelectedValue))) & "', '" & DDLYear.SelectedValue & DDLMonth.SelectedValue & "', 'Monthly Closing Auto Posting', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 0, 0, 1, 1, 0, 0)"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                ' GL DTL
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLDtlOid & ", 1, " & iGLMstOid & ", " & iAcctgID_db & ", 'D', 0, '', 'Monthly Closing Auto Posting', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dSelisih & ", 0, '')"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                iGLDtlOid += 1
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLDtlOid & ", 2, " & iGLMstOid & ", " & iAcctgID_cr & ", 'C', 0, '', 'Monthly Closing Auto Posting', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dSelisih & ", 0, '')"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                iGLMstOid += 1 : iGLDtlOid += 1
                            End If
                            If (Math.Round(ToDouble(dtGL.Rows(C1)("glamtusd").ToString), 6) <> Math.Round(ToDouble(dtGL.Rows(C1)("glamt").ToString) * ToDouble(dtGL.Rows(C1)("ratetousd").ToString), 6)) And ToInteger(dtGL.Rows(C1)("curroid").ToString) <> 4 Then
                                Dim dSelisih As Double = Math.Round(ToDouble(dtGL.Rows(C1)("glamtusd").ToString), 6) - Math.Round(ToDouble(dtGL.Rows(C1)("glamt").ToString) * ToDouble(dtGL.Rows(C1)("ratetousd").ToString), 6)
                                iAcctgID_db = ToInteger(dtGL.Rows(C1)("acctgoid").ToString) : iAcctgID_cr = iDiffUSDAcctgID
                                If dSelisih < 0 Then iAcctgID_db = iDiffUSDAcctgID : iAcctgID_cr = ToInteger(dtGL.Rows(C1)("acctgoid").ToString) : dSelisih *= -1
                                ' GL MST
                                sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLMstOid & ", '" & New Date(ToInteger(DDLYear.SelectedValue), ToInteger(DDLMonth.SelectedValue), Date.DaysInMonth(ToInteger(DDLYear.SelectedValue), ToInteger(DDLMonth.SelectedValue))) & "', '" & DDLYear.SelectedValue & DDLMonth.SelectedValue & "', 'Monthly Closing Auto Posting', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 0, 0, 1, 1, 0, 0)"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                ' GL DTL
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLDtlOid & ", 1, " & iGLMstOid & ", " & iAcctgID_db & ", 'D', 0, '', 'Monthly Closing Auto Posting', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, " & dSelisih & ", '')"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                iGLDtlOid += 1
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLDtlOid & ", 2, " & iGLMstOid & ", " & iAcctgID_cr & ", 'C', 0, '', 'Monthly Closing Auto Posting', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, " & dSelisih & ", '')"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                iGLMstOid += 1 : iGLDtlOid += 1
                            End If
                        End If
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGLMstOid - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_TRNGLMST'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGLDtlOid - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_TRNGLDTL'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    objTrans.Commit() : conn.Close()
                Catch exSql As SqlException
                    objTrans.Rollback() : conn.Close()
                    If ToDouble(Session("ErrorCounter")) < 5 Then
                        If exSql.Number = 2627 Then
                            Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                            Call PostSelisihKurs()
                        Else
                            sRet = exSql.ToString
                        End If
                    Else
                        sRet = exSql.ToString
                    End If
                Catch ex As Exception
                    objTrans.Rollback() : conn.Close()
                    sRet = ex.ToString
                End Try
            End If

            If sAcctgOid_Bank <> "" Then
                sAcctgOid_Bank = Left(sAcctgOid_Bank, sAcctgOid_Bank.Length - 1)
                sSql = "SELECT SUM(glamt * (CASE gldbcr WHEN 'C' THEN -1 ELSE 1 END)) glamt, SUM(glamtidr * (CASE gldbcr WHEN 'C' THEN -1 ELSE 1 END)) glamtidr, SUM(glamtusd * (CASE gldbcr WHEN 'C' THEN -1 ELSE 1 END)) glamtusd, gld.acctgoid, a.curroid, ISNULL((SELECT CAST(rate2res1 AS REAL) FROM QL_mstrate2 r2 WHERE r2.curroid=a.curroid AND rate2month=" & iNextMonth & " AND rate2year=" & iYear & "), 0.0) ratetoidr, ISNULL((SELECT CAST(rate2res2 AS REAL) FROM QL_mstrate2 r2 WHERE r2.curroid=a.curroid AND rate2month=" & iNextMonth & " AND rate2year=" & iYear & "), 0.0) ratetousd FROM QL_trngldtl gld INNER JOIN QL_trnglmst glm ON glm.cmpcode=gld.cmpcode AND glm.glmstoid=gld.glmstoid INNER JOIN QL_mstacctg a ON a.acctgoid=gld.acctgoid WHERE gld.cmpcode='" & bussinessunit.SelectedValue & "' /*AND gld.glnote<>'Monthly Closing Auto Posting'*/ AND gld.acctgoid IN (" & sAcctgOid_Bank & ") AND gldate<=CAST('" & Format(New Date(DDLYear.SelectedValue, DDLMonth.SelectedValue, Date.DaysInMonth(DDLYear.SelectedValue, DDLMonth.SelectedValue)), "MM/dd/yyyy") & " 23:59:59' AS DATETIME) GROUP BY gld.acctgoid, a.curroid"
                Dim dtGL As DataTable = cKoneksi.ambiltabel(sSql, "QL_trngldtl")
                Dim iGLMstOid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
                Dim iGLDtlOid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
                Dim iAcctgID_db As Integer = 0, iAcctgID_cr As Integer = 0
                Dim objTrans As SqlClient.SqlTransaction
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If
                objTrans = conn.BeginTransaction()
                objCmd.Transaction = objTrans
                Try
                    For C1 As Integer = 0 To dtGL.Rows.Count - 1
                        If ToDouble(dtGL.Rows(C1)("glamt").ToString) <> 0 Then
                            If (Math.Round(ToDouble(dtGL.Rows(C1)("glamtidr").ToString), 4) <> Math.Round(ToDouble(dtGL.Rows(C1)("glamt").ToString) * ToDouble(dtGL.Rows(C1)("ratetoidr").ToString), 4)) And ToInteger(dtGL.Rows(C1)("curroid").ToString) <> 1 Then
                                Dim dSelisih As Double = Math.Round(ToDouble(dtGL.Rows(C1)("glamtidr").ToString), 4) - Math.Round(ToDouble(dtGL.Rows(C1)("glamt").ToString) * ToDouble(dtGL.Rows(C1)("ratetoidr").ToString), 4)
                                iAcctgID_db = ToInteger(dtGL.Rows(C1)("acctgoid").ToString) : iAcctgID_cr = iDiffIDRAcctgID
                                If dSelisih < 0 Then iAcctgID_db = iDiffIDRAcctgID : iAcctgID_cr = ToInteger(dtGL.Rows(C1)("acctgoid").ToString) : dSelisih *= -1
                                ' GL MST
                                sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLMstOid & ", '" & New Date(ToInteger(DDLYear.SelectedValue), ToInteger(DDLMonth.SelectedValue), Date.DaysInMonth(ToInteger(DDLYear.SelectedValue), ToInteger(DDLMonth.SelectedValue))) & "', '" & DDLYear.SelectedValue & DDLMonth.SelectedValue & "', 'Monthly Closing Auto Posting', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 0, 0, 1, 1, 0, 0)"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                ' GL DTL
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLDtlOid & ", 1, " & iGLMstOid & ", " & iAcctgID_db & ", 'D', 0, '', 'Monthly Closing Auto Posting', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dSelisih & ", 0, '')"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                iGLDtlOid += 1
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLDtlOid & ", 2, " & iGLMstOid & ", " & iAcctgID_cr & ", 'C', 0, '', 'Monthly Closing Auto Posting', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dSelisih & ", 0, '')"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                iGLMstOid += 1 : iGLDtlOid += 1
                            End If
                            If (Math.Round(ToDouble(dtGL.Rows(C1)("glamtusd").ToString), 6) <> Math.Round(ToDouble(dtGL.Rows(C1)("glamt").ToString) * ToDouble(dtGL.Rows(C1)("ratetousd").ToString), 6)) And ToInteger(dtGL.Rows(C1)("curroid").ToString) <> 4 Then
                                Dim dSelisih As Double = Math.Round(ToDouble(dtGL.Rows(C1)("glamtusd").ToString), 6) - Math.Round(ToDouble(dtGL.Rows(C1)("glamt").ToString) * ToDouble(dtGL.Rows(C1)("ratetousd").ToString), 6)
                                iAcctgID_db = ToInteger(dtGL.Rows(C1)("acctgoid").ToString) : iAcctgID_cr = iDiffUSDAcctgID
                                If dSelisih < 0 Then iAcctgID_db = iDiffUSDAcctgID : iAcctgID_cr = ToInteger(dtGL.Rows(C1)("acctgoid").ToString) : dSelisih *= -1
                                ' GL MST
                                sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLMstOid & ", '" & New Date(ToInteger(DDLYear.SelectedValue), ToInteger(DDLMonth.SelectedValue), Date.DaysInMonth(ToInteger(DDLYear.SelectedValue), ToInteger(DDLMonth.SelectedValue))) & "', '" & DDLYear.SelectedValue & DDLMonth.SelectedValue & "', 'Monthly Closing Auto Posting', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 0, 0, 1, 1, 0, 0)"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                ' GL DTL
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLDtlOid & ", 1, " & iGLMstOid & ", " & iAcctgID_db & ", 'D', 0, '', 'Monthly Closing Auto Posting', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, " & dSelisih & ", '')"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                iGLDtlOid += 1
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & bussinessunit.SelectedValue & "', " & iGLDtlOid & ", 2, " & iGLMstOid & ", " & iAcctgID_cr & ", 'C', 0, '', 'Monthly Closing Auto Posting', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, " & dSelisih & ", '')"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                iGLMstOid += 1 : iGLDtlOid += 1
                            End If
                        End If
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGLMstOid - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_TRNGLMST'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGLDtlOid - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_TRNGLDTL'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    objTrans.Commit() : conn.Close()
                Catch exSql As SqlException
                    objTrans.Rollback() : conn.Close()
                    If ToDouble(Session("ErrorCounter")) < 5 Then
                        If exSql.Number = 2627 Then
                            Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                            Call PostSelisihKurs()
                        Else
                            sRet = exSql.Message
                        End If
                    Else
                        sRet = exSql.Message
                    End If
                Catch ex As Exception
                    objTrans.Rollback() : conn.Close()
                    sRet = ex.Message
                End Try
            End If
        End If
        Return sRet
    End Function
#End Region

#Region "Procedures"
    Private Sub InitDDL()
        ' BULAN
        For C1 As Integer = 1 To 12
            Dim liMonth As New ListItem(MonthName(C1), C1)
            DDLMonth.Items.Add(liMonth)
        Next

        ' TAHUN
        For C1 As Integer = GetServerTime.Year - 5 To GetServerTime.Year + 1
            Dim liYear As New ListItem(C1, C1)
            DDLYear.Items.Add(liYear)
        Next

        ' Init DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE' "
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(bussinessunit, sSql)
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Sub GetLastAndNextMonthlyClosing()
        Try
            sSql = "SELECT ISNULL(closingperiod, '') FROM QL_acctgclosinghistory WHERE cmpcode='" & bussinessunit.SelectedValue & "' AND historyclosingoid=(SELECT MAX(historyclosingoid) FROM QL_acctgclosinghistory WHERE cmpcode='" & bussinessunit.SelectedValue & "' AND closinggroup='MONTHLY' AND closingstatus='CLOSED') "
            Dim history As String = CStr(GetStrData(sSql))

            If history <> "" Then
                lastcloseyear.Text = Left(history, 4) : lastclosemonth.Text = Right(history, 2)
                lblLastClosing.Text = MonthName(lastclosemonth.Text) & " " & lastcloseyear.Text

                If lastclosemonth.Text = 12 Then
                    DDLMonth.SelectedValue = 1 : DDLYear.SelectedValue = ToDouble(lastcloseyear.Text) + 1
                    nextclosemonth.Text = 1 : nextcloseyear.Text = ToDouble(lastcloseyear.Text) + 1
                Else
                    DDLMonth.SelectedValue = ToDouble(lastclosemonth.Text) + 1 : DDLYear.SelectedValue = ToDouble(lastcloseyear.Text)
                    nextclosemonth.Text = ToDouble(lastclosemonth.Text) + 1 : nextcloseyear.Text = ToDouble(lastcloseyear.Text)
                End If
                lblNextClosing.Text = MonthName(nextclosemonth.Text) & " " & nextcloseyear.Text
            Else
                Dim sLastClose As String = ""
                ' Determine first month by QL_crdgl
                sSql = "SELECT ISNULL(MIN(periodacctg), '') FROM QL_crdgl WHERE cmpcode='" & bussinessunit.SelectedValue & "' AND crdglflag='OPEN'"
                sLastClose = CStr(GetStrData(sSql))
                lastcloseyear.Text = "" : lastclosemonth.Text = "" : lblLastClosing.Text = "-"
                If sLastClose <> "" Then
                    nextclosemonth.Text = ToDouble(Right(sLastClose, 2)) : nextcloseyear.Text = ToDouble(Left(sLastClose, 4))
                    lblNextClosing.Text = Format(New Date(ToDouble(Left(sLastClose, 4)), ToDouble(Right(sLastClose, 2)), 1), "MMMM yyyy")
                    DDLMonth.SelectedValue = ToDouble(Right(sLastClose, 2))
                    DDLYear.SelectedValue = ToDouble(Left(sLastClose, 4))
                Else
                    ' Determine first month by QL_trnglmst
                    sSql = "SELECT ISNULL(MIN(periodacctg),'') FROM QL_trnglmst WHERE cmpcode='" & bussinessunit.SelectedValue & "' AND glflag='POST'"
                    sLastClose = CStr(GetStrData(sSql))
                    If sLastClose <> "" Then
                        nextclosemonth.Text = ToDouble(Right(sLastClose, 2)) : nextcloseyear.Text = ToDouble(Left(sLastClose, 4))
                        lblNextClosing.Text = Format(New Date(ToDouble(Left(sLastClose, 4)), ToDouble(Right(sLastClose, 2)), 1), "MMMM yyyy")
                        DDLMonth.SelectedValue = ToDouble(Right(sLastClose, 2))
                        DDLYear.SelectedValue = ToDouble(Left(sLastClose, 4))
                    Else
                        lastcloseyear.Text = "" : lastclosemonth.Text = "" : lblLastClosing.Text = "-"
                        nextclosemonth.Text = "" : nextcloseyear.Text = "" : lblNextClosing.Text = "-"
                        DDLMonth.SelectedValue = Month(GetServerTime) : DDLYear.SelectedValue = Year(GetServerTime)
                    End If
                End If
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        End Try
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            Response.Redirect("~\Accounting\trnMonthlyClosing.aspx")
        End If
        If checkPagePermission("~\Accounting\trnMonthlyClosing.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Monthly Closing"
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        btnOpen.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to REVISE this data?');")
        TabContainer1.Tabs(0).Enabled = False
        TabContainer1.ActiveTabIndex = 1
        If Not Page.IsPostBack Then
            InitDDL()
            GetLastAndNextMonthlyClosing()
            closingstatus.Text = GetStatus(bussinessunit.SelectedValue, DDLMonth.SelectedValue, DDLYear.SelectedValue)
            upduser.Text = Session("UserID")
            updtime.Text = GetServerTime()
        End If
        If Not Session("TblDtlDep") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtlDep")
            gvDtl.DataSource = dt
            gvDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKValidasi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnCalculate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCalculate.Click
        Dim sError As String = "" 'PostAssetDepreciation()
        If sError <> "" Then
            showMessage(sError, 1) : Exit Sub
        End If
        sError = ""'PostBOPReal()
        If sError <> "" Then
            showMessage(sError, 1) : Exit Sub
        End If
        sError = "" 'PostSelisihKurs()
        If sError <> "" Then
            showMessage(sError, 1) : Exit Sub
        End If
        sError = ""'PostDLC()
        If sError <> "" Then
            showMessage(sError, 1) : Exit Sub
        End If
        closingstatus.Text = BindClosingData(bussinessunit.SelectedValue, DDLMonth.SelectedValue, DDLYear.SelectedValue)
        Dim objTable As DataTable : objTable = Session("TblDtlDep")
        gvDtl.DataSource = objTable : gvDtl.DataBind()

        If closingstatus.Text.ToUpper <> "OPEN" Then
            gvPostList.DataSource = Nothing : gvPostList.DataBind()
            gvPostList.Visible = False : btnSave.Visible = False
            btnPosting.Visible = False : Exit Sub
        End If

        Dim sMsg As String = ""

        ' VAR Utk LABA/RUGI Bulanan, Tahunan, Ditahan
        Dim varLabaBerjalan As String = GetVarInterface("VAR_LR_MONTH", bussinessunit.SelectedValue)
        Dim iLabaBerjalanOid As Integer = 0
        If varLabaBerjalan <> "" Then
            iLabaBerjalanOid = GetAcctgOID(varLabaBerjalan, CompnyCode)
        End If

        Dim varLabaTahunBerjalan As String = GetVarInterface("VAR_LR_YEAR", bussinessunit.SelectedValue)
        Dim iLabaTahunBerjalanOid As Integer = 0
        If varLabaTahunBerjalan <> "" Then
            iLabaTahunBerjalanOid = GetAcctgOID(varLabaTahunBerjalan, CompnyCode)
        End If

        Dim varLabaDitahan As String = GetVarInterface("VAR_LR_ONHAND", bussinessunit.SelectedValue)
        Dim iLabaDitahan As Integer = 0
        If varLabaDitahan <> "" Then
            iLabaDitahan = GetAcctgOID(varLabaDitahan, CompnyCode)
        End If

        If iLabaBerjalanOid = 0 Then sMsg &= "- Please setup Inteface for Variable: VAR_LR_MONTH in Business Unit: " & bussinessunit.SelectedItem.Text.ToUpper & "<BR>"
        If iLabaTahunBerjalanOid = 0 Then sMsg &= "- Please setup Inteface for Variable: VAR_LR_YEAR in Business Unit: " & bussinessunit.SelectedItem.Text.ToUpper & "<BR>"
        If iLabaDitahan = 0 Then sMsg &= "- Please setup Inteface for Variable: VAR_LR_ONHAND in Business Unit: " & bussinessunit.SelectedItem.Text.ToUpper & "<BR>"
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        End If

        ' Untuk laba (rugi) berjalan
        Dim totPendapatanIDR As Double = 0 : Dim totPendapatanUSD As Double = 0
        Dim totBiayaIDR As Double = 0 : Dim totBiayaUSD As Double = 0
        Dim totPendapatanIDR2 As Double = 0 : Dim totPendapatanUSD2 As Double = 0
        Dim totBiayaIDR2 As Double = 0 : Dim totBiayaUSD2 As Double = 0

        objTable = Session("TblDtlDep")
        Dim dvTable As DataView = objTable.DefaultView

        ' Hitung laba rugi berjalan
        dvTable.RowFilter = "acctgres3='LABARUGI' AND acctgdbcr='C'"
        For C2 As Integer = 0 To dvTable.Count - 1
            ' Summarize pendapatan (Revenue)
            totPendapatanIDR += (ToDouble(dvTable(C2)("amtdebetidr").ToString) - ToDouble(dvTable(C2)("amtcreditidr").ToString))
            totPendapatanUSD += (ToDouble(dvTable(C2)("amtdebetusd").ToString) - ToDouble(dvTable(C2)("amtcreditusd").ToString))
            totPendapatanIDR2 += ToDouble(dvTable(C2)("amtopenidr").ToString)
            totPendapatanUSD2 += ToDouble(dvTable(C2)("amtopenusd").ToString)
        Next
        dvTable.RowFilter = ""

        dvTable.RowFilter = "acctgres3='LABARUGI' AND acctgdbcr='D'"
        For C3 As Integer = 0 To dvTable.Count - 1
            ' Summarize biaya (Expense)
            totBiayaIDR += (ToDouble(dvTable(C3)("amtdebetidr").ToString) - ToDouble(dvTable(C3)("amtcreditidr").ToString))
            totBiayaUSD += (ToDouble(dvTable(C3)("amtdebetusd").ToString) - ToDouble(dvTable(C3)("amtcreditusd").ToString))
            totBiayaIDR2 += ToDouble(dvTable(C3)("amtopenidr").ToString)
            totBiayaUSD2 += ToDouble(dvTable(C3)("amtopenusd").ToString)
        Next
        dvTable.RowFilter = ""

        ' Membalik Pendapatan 
        totPendapatanIDR *= -1 : totPendapatanUSD *= -1
        totPendapatanIDR2 *= -1 : totPendapatanUSD2 *= -1

        ' Update Laba Rugi Bulan Berjalan ke QL_crdgl (pd Session)
        If iLabaBerjalanOid <> 0 Then
            Dim dv As DataView = objTable.DefaultView
            dv.RowFilter = "acctgoid=" & iLabaBerjalanOid
            If dv.Count > 0 Then
                Dim ed As DataRowView = dv(0)
                ed.BeginEdit()
                If totPendapatanIDR > totBiayaIDR Then
                    ed("amtcreditidr") = totPendapatanIDR - totBiayaIDR
                Else
                    ed("amtdebetidr") = totBiayaIDR - totPendapatanIDR
                End If
                ed("amtbalanceidr") = ToDouble(ed("amtopenidr").ToString) + (ToDouble(ed("amtdebetidr").ToString) - ToDouble(ed("amtcreditidr").ToString))

                If totPendapatanUSD > totBiayaUSD Then
                    ed("amtcreditusd") = totPendapatanUSD - totBiayaUSD
                Else
                    ed("amtdebetusd") = totBiayaUSD - totPendapatanUSD
                End If
                ed("amtbalanceusd") = ToDouble(ed("amtopenusd").ToString) + (ToDouble(ed("amtdebetusd").ToString) - ToDouble(ed("amtcreditusd").ToString))
                ed.EndEdit()
            End If
            dv.RowFilter = ""
        End If

        ' Update Laba Rugi Tahun Berjalan ke QL_crdgl (pd Session)
        If iLabaTahunBerjalanOid <> 0 Then
            Dim dv As DataView = objTable.DefaultView
            dv.RowFilter = "acctgoid=" & iLabaTahunBerjalanOid
            If dv.Count > 0 Then
                Dim ed As DataRowView = dv(0)
                ed.BeginEdit()
                If totPendapatanIDR2 > totBiayaIDR2 Then
                    ed("amtcreditidr") = totPendapatanIDR2 - totBiayaIDR2
                Else
                    ed("amtdebetidr") = totBiayaIDR2 - totPendapatanIDR2
                End If
                ed("amtbalanceidr") = ToDouble(ed("amtopenidr").ToString) + (ToDouble(ed("amtdebetidr").ToString) - ToDouble(ed("amtcreditidr").ToString))

                If totPendapatanUSD2 > totBiayaUSD2 Then
                    ed("amtcreditusd") = totPendapatanUSD2 - totBiayaUSD2
                Else
                    ed("amtdebetusd") = totBiayaUSD2 - totPendapatanUSD2
                End If
                ed("amtbalanceusd") = ToDouble(ed("amtopenusd").ToString) + (ToDouble(ed("amtdebetusd").ToString) - ToDouble(ed("amtcreditusd").ToString))
                ed.EndEdit()
            End If
            dv.RowFilter = ""
        End If

        gvDtl.DataSource = objTable : gvDtl.DataBind()
        Session("TblDtlDep") = objTable

        ' CHECK Not POST Transaction
        sSql = "DECLARE @cmpcode VARCHAR(10);" & _
            "DECLARE @month INT; " & _
            "DECLARE @year INT; " & _
            "SET @cmpcode='" & bussinessunit.SelectedValue & "'; " & _
            "SET @month=" & DDLMonth.SelectedValue & "; " & _
            "SET @year=" & DDLYear.SelectedValue & "; " & _
            "SELECT COUNT(-1) AS jml, 'DOWN PAYMENT A/P' AS tipe, 0 AS seq " & _
            "FROM QL_trncashbankmst cb INNER JOIN QL_trndpap dp ON cb.cmpcode=dp.cmpcode AND cb.cashbankoid=dp.cashbankoid " & _
            "WHERE cb.cmpcode=@cmpcode AND cashbankstatus='IN PROCESS' " & _
            "AND MONTH(CASE SUBSTRING(cashbanktype, 2, 1) WHEN 'B' THEN cashbankduedate ELSE cashbankdate END)=@month " & _
            "AND YEAR(CASE SUBSTRING(cashbanktype, 2, 1) WHEN 'B' THEN cashbankduedate ELSE cashbankdate END)=@year " & _
            "AND cashbankgroup='DPAP' HAVING COUNT(-1)>0 " & _
            "UNION ALL " & _
            "SELECT COUNT(-1) AS jml, 'DOWN PAYMENT A/P BALANCE' AS tipe, 0 AS seq " & _
            "FROM QL_trndpap dp " & _
            "WHERE dp.cmpcode=@cmpcode AND dpapstatus='IN PROCESS' AND dpapoid < 0 " & _
            "AND MONTH(dpapdate)=@month " & _
            "AND YEAR(dpapdate)=@year HAVING COUNT(-1)>0 " & _
            "UNION ALL " & _
            "SELECT COUNT(-1) AS jml, 'DOWN PAYMENT A/R' AS tipe, 0 AS seq " & _
            "FROM QL_trncashbankmst cb INNER JOIN QL_trndpar dr ON cb.cmpcode=dr.cmpcode AND cb.cashbankoid=dr.cashbankoid " & _
            "WHERE cb.cmpcode=@cmpcode AND cashbankstatus='IN PROCESS' " & _
            "AND MONTH(CASE SUBSTRING(cashbanktype, 2, 1) WHEN 'B' THEN cashbankduedate ELSE cashbankdate END)=@month " & _
            "AND YEAR(CASE SUBSTRING(cashbanktype, 2, 1) WHEN 'B' THEN cashbankduedate ELSE cashbankdate END)=@year " & _
            "AND cashbankgroup='DPAR' HAVING COUNT(-1)>0 " & _
            "UNION ALL " & _
            "SELECT COUNT(-1) AS jml, 'DOWN PAYMENT A/R BALANCE' AS tipe, 0 AS seq " & _
            "FROM QL_trndpar dp " & _
            "WHERE dp.cmpcode=@cmpcode AND dparstatus='IN PROCESS' AND dparoid < 0 " & _
            "AND MONTH(dpardate)=@month " & _
            "AND YEAR(dpardate)=@year HAVING COUNT(-1)>0 " & _
            "UNION ALL " & _
            "SELECT COUNT(-1) AS jml, 'A/P INITIAL BALANCE' AS tipe, 0 AS seq " & _
            "FROM QL_trnbelimst dp " & _
            "WHERE dp.cmpcode=@cmpcode AND trnbelimststatus='IN PROCESS' AND trnbelimstoid < 0 " & _
            "AND MONTH(trnbelidate)=@month " & _
            "AND YEAR(trnbelidate)=@year HAVING COUNT(-1)>0 " & _
            "UNION ALL " & _
            "SELECT COUNT(-1) AS jml, 'A/R INITIAL BALANCE' AS tipe, 0 AS seq " & _
            "FROM QL_trnjualmst dp " & _
            "WHERE dp.cmpcode=@cmpcode AND trnjualstatus='IN PROCESS' AND trnjualmstoid < 0 " & _
            "AND MONTH(trnjualdate)=@month " & _
            "AND YEAR(trnjualdate)=@year HAVING COUNT(-1)>0 " & _
            "UNION ALL " & _
            "SELECT COUNT(-1) AS jml, 'A/P PAYMENT' AS tipe, 0 AS seq " & _
            "FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pay ON cb.cmpcode=pay.cmpcode AND cb.cashbankoid=pay.cashbankoid " & _
            "WHERE cb.cmpcode=@cmpcode AND cashbankstatus='IN PROCESS' " & _
            "AND MONTH(CASE SUBSTRING(cashbanktype, 2, 1) WHEN 'B' THEN cashbankduedate ELSE cashbankdate END)=@month " & _
            "AND YEAR(CASE SUBSTRING(cashbanktype, 2, 1) WHEN 'B' THEN cashbankduedate ELSE cashbankdate END)=@year " & _
            "AND cashbankgroup LIKE 'AP%' HAVING COUNT(-1)>0 " & _
            "UNION ALL " & _
            "SELECT COUNT(-1) AS jml, 'A/R PAYMENT' AS tipe, 0 AS seq " & _
            "FROM QL_trncashbankmst cb INNER JOIN QL_trnpayar pay ON cb.cmpcode=pay.cmpcode AND cb.cashbankoid=pay.cashbankoid " & _
            "WHERE cb.cmpcode=@cmpcode AND cashbankstatus='IN PROCESS' " & _
            "AND MONTH(CASE SUBSTRING(cashbanktype, 2, 1) WHEN 'B' THEN cashbankduedate ELSE cashbankdate END)=@month " & _
            "AND YEAR(CASE SUBSTRING(cashbanktype, 2, 1) WHEN 'B' THEN cashbankduedate ELSE cashbankdate END)=@year " & _
            "AND cashbankgroup LIKE 'AR%' HAVING COUNT(-1)>0 " & _
            "UNION ALL " & _
            "SELECT COUNT(-1) AS jml, 'INCOMING GIRO REALIZATION' AS tipe, 0 AS seq " & _
            "FROM QL_trncashbankmst cb INNER JOIN QL_trnpayargiro pay ON cb.cmpcode=pay.cmpcode AND cb.cashbankoid=pay.cashbankoid " & _
            "WHERE cb.cmpcode=@cmpcode AND cashbankstatus='IN PROCESS' " & _
            "AND MONTH(cashbankdate)=@month " & _
            "AND YEAR(cashbankdate)=@year " & _
            "AND cashbankgroup='GIRO IN' HAVING COUNT(-1)>0 " & _
            "UNION ALL " & _
            "SELECT COUNT(-1) AS jml, 'CASH/BANK EXPENSE' AS tipe, 0 AS seq " & _
            "FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl cg ON cg.cmpcode=cb.cmpcode AND cg.cashbankoid=cb.cashbankoid " & _
            "WHERE cb.cmpcode=@cmpcode AND cashbankstatus='IN PROCESS' " & _
            "AND MONTH(CASE SUBSTRING(cashbanktype, 2, 1) WHEN 'B' THEN cashbankduedate ELSE cashbankdate END)=@month " & _
            "AND YEAR(CASE SUBSTRING(cashbanktype, 2, 1) WHEN 'B' THEN cashbankduedate ELSE cashbankdate END)=@year " & _
            "AND cashbankgroup='EXPENSE' HAVING COUNT(-1)>0 " & _
            "UNION ALL " & _
            "SELECT COUNT(-1) AS jml, 'CASH/BANK RECEIPT' AS tipe, 0 AS seq " & _
            "FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl cg ON cg.cmpcode=cb.cmpcode AND cg.cashbankoid=cb.cashbankoid " & _
            "WHERE cb.cmpcode=@cmpcode AND cashbankstatus='IN PROCESS' " & _
            "AND MONTH(CASE SUBSTRING(cashbanktype, 2, 1) WHEN 'B' THEN cashbankduedate ELSE cashbankdate END)=@month " & _
            "AND YEAR(CASE SUBSTRING(cashbanktype, 2, 1) WHEN 'B' THEN cashbankduedate ELSE cashbankdate END)=@year " & _
            "AND cashbankgroup='RECEIPT' HAVING COUNT(-1)>0 " & _
            "UNION ALL " & _
            "SELECT COUNT(-1) AS jml, 'CASH/BANK OVERBOOKING' AS tipe, 0 AS seq " & _
            "FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl cg ON cg.cmpcode=cb.cmpcode AND cg.cashbankoid=cb.cashbankoid " & _
            "WHERE cb.cmpcode=@cmpcode AND cashbankstatus='IN PROCESS' " & _
            "AND MONTH(CASE SUBSTRING(cashbanktype, 2, 1) WHEN 'B' THEN cashbankduedate ELSE cashbankdate END)=@month " & _
            "AND YEAR(CASE SUBSTRING(cashbanktype, 2, 1) WHEN 'B' THEN cashbankduedate ELSE cashbankdate END)=@year " & _
            "AND cashbankgroup='MUTATION' HAVING COUNT(-1)>0 " & _
            "UNION ALL " & _
            "SELECT COUNT(-1) AS jml, 'MEMORIAL JOURNAL' AS tipe, 0 AS seq FROM QL_trnglmst WHERE cmpcode=@cmpcode " & _
            "AND glflag='IN PROCESS' AND MONTH(gldate)=@month AND YEAR(gldate)=@year HAVING COUNT(-1)>0 " & _
            "UNION ALL " & _
            "SELECT COUNT(-1) AS jml, 'FIXED ASSETS BALANCE' AS tipe, 0 AS seq FROM QL_trnfixmst WHERE cmpcode=@cmpcode " & _
            "AND fixflag='IN PROCESS' AND MONTH(fixdate)=@month AND YEAR(fixdate)=@year HAVING COUNT(-1)>0 " & _
            "UNION ALL " & _
            "SELECT COUNT(-1) AS jml, 'FIXED ASSETS PERIOD POSTING' AS tipe, 0 AS seq FROM QL_trnfixmst am INNER JOIN QL_trnfixdtl ad ON ad.cmpcode=am.cmpcode AND ad.fixoid=am.fixoid WHERE am.cmpcode=@cmpcode " & _
            "AND am.fixflag='POST' AND ad.fixflag='IN PROCESS' AND LEFT(fixperiod,4)=@year AND RIGHT(fixperiod,2)=@month HAVING COUNT(-1)>0 "
        Dim dtCekPost As DataTable = cKoneksi.ambiltabel(sSql, "CekPost")

        For C1 As Integer = 0 To dtCekPost.Rows.Count - 1
            Dim ed As DataRow = dtCekPost.Rows(C1)
            ed.BeginEdit() : ed("seq") = C1 + 1 : ed.EndEdit()
        Next
        gvPostList.DataSource = dtCekPost : gvPostList.DataBind()

        If (DDLMonth.SelectedValue <> CInt(nextclosemonth.Text)) Or (DDLYear.SelectedValue <> CInt(nextcloseyear.Text)) Then
            btnSave.Visible = False
            btnPosting.Visible = False
        Else
            ' OK To Monthly Closing
            Dim iCounter As Integer = ToDouble(dtCekPost.Compute("SUM(jml)", "").ToString)
            lblCounter.Text = iCounter : pnlNeedPost.Visible = (iCounter > 0)
            btnSave.Visible = Not (iCounter > 0)
            btnPosting.Visible = Not (iCounter > 0)
        End If
        'btnOpen.Visible = IIf(closingstatus.Text = "CLOSED", True, False)
    End Sub

    Protected Sub DDLCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLCurrency.SelectedIndexChanged
        Dim objTable As DataTable = Session("TblDtlDep")
        gvDtl.DataSource = objTable : gvDtl.DataBind()
    End Sub

    Protected Sub gvDtl_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDtl.PageIndexChanging
        gvDtl.PageIndex = e.NewPageIndex
        Dim objTable As DataTable = Session("TblDtlDep")
        gvDtl.DataSource = objTable : gvDtl.DataBind()
    End Sub

    Protected Sub bussinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles bussinessunit.SelectedIndexChanged
        GetLastAndNextMonthlyClosing()
        closingstatus.Text = GetStatus(bussinessunit.SelectedValue, DDLMonth.SelectedValue, DDLYear.SelectedValue)
        gvDtl.DataSource = Nothing : gvDtl.DataBind() : Session("TblDtlDep") = Nothing
    End Sub

    Protected Sub DDLMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLMonth.SelectedIndexChanged
        closingstatus.Text = GetStatus(bussinessunit.SelectedValue, DDLMonth.SelectedValue, DDLYear.SelectedValue)
        gvDtl.DataSource = Nothing : gvDtl.DataBind() : Session("TblDtlDep") = Nothing
    End Sub

    Protected Sub DDLYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLYear.SelectedIndexChanged
        closingstatus.Text = GetStatus(bussinessunit.SelectedValue, DDLMonth.SelectedValue, DDLYear.SelectedValue)
        gvDtl.DataSource = Nothing : gvDtl.DataBind() : Session("TblDtlDep") = Nothing
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Accounting\trnMonthlyClosing.aspx?awal=true")
    End Sub

    Protected Sub btnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosting.Click
        closingstatus.Text = "CLOSED"
        btnSave_Click(sender, e)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim sMsg As String = ""
        If lastcloseyear.Text <> "" And lastclosemonth.Text <> "" Then
            If (DDLMonth.SelectedValue <> CInt(nextclosemonth.Text)) Or (DDLYear.SelectedValue <> CInt(nextcloseyear.Text)) Then
                sMsg &= "- Next Monthly Closing period is : " & MonthName(nextclosemonth.Text) & " " & nextcloseyear.Text & " !<BR>"
            End If
            If GetServerTime() < New Date(DDLYear.SelectedValue, DDLMonth.SelectedValue, Date.DaysInMonth(DDLYear.SelectedValue, DDLMonth.SelectedValue)) Then
                sMsg &= "- Monthly Closing can be executed minimum at the end of the month period !<BR>"
            End If
        Else
            If GetServerTime() < New Date(DDLYear.SelectedValue, DDLMonth.SelectedValue, Date.DaysInMonth(DDLYear.SelectedValue, DDLMonth.SelectedValue)) Then
                sMsg &= "- Monthly Closing can be executed minimum at the end of the month period !<BR>"
            End If
        End If

        If Session("TblDtlDep") Is Nothing Then
            sMsg &= "- No Monthly Closing detail !<BR>"
        Else
            Dim dtlTbl As DataTable : dtlTbl = Session("TblDtlDep")
            If dtlTbl.Rows.Count < 1 Then
                sMsg &= "- No Monthly Closing detail !<BR>"
            End If
        End If

        ' VAR Utk LABA/RUGI Bulanan, Tahunan, Ditahan
        Dim varLabaBerjalan As String = GetVarInterface("VAR_LR_MONTH", bussinessunit.SelectedValue)
        Dim iLabaBerjalanOid As Integer = 0
        If varLabaBerjalan <> "" Then
            iLabaBerjalanOid = GetAcctgOID(varLabaBerjalan, CompnyCode)
        End If

        Dim varLabaTahunBerjalan As String = GetVarInterface("VAR_LR_YEAR", bussinessunit.SelectedValue)
        Dim iLabaTahunBerjalanOid As Integer = 0
        If varLabaTahunBerjalan <> "" Then
            iLabaTahunBerjalanOid = GetAcctgOID(varLabaTahunBerjalan, CompnyCode)
        End If

        Dim varLabaDitahan As String = GetVarInterface("VAR_LR_ONHAND", bussinessunit.SelectedValue)
        Dim iLabaDitahan As Integer = 0
        If varLabaDitahan <> "" Then
            iLabaDitahan = GetAcctgOID(varLabaDitahan, CompnyCode)
        End If

        If iLabaBerjalanOid = 0 Then sMsg &= "- Please setup Interface for Variable: VAR_LR_MONTH in Business Unit: " & bussinessunit.SelectedItem.Text.ToUpper & "<BR>"
        If iLabaTahunBerjalanOid = 0 Then sMsg &= "- Please setup Interface for Variable: VAR_LR_YEAR in Business Unit: " & bussinessunit.SelectedItem.Text.ToUpper & "<BR>"
        If iLabaDitahan = 0 Then sMsg &= "- Please setup Interface for Variable: VAR_LR_ONHAND in Business Unit: " & bussinessunit.SelectedItem.Text.ToUpper & "<BR>"

        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        End If
        Session("vIDCH") = GenerateID("QL_acctgclosinghistory", bussinessunit.SelectedValue)
        Session("vIDCRD") = GenerateID("QL_crdgl", bussinessunit.SelectedValue)
        Session("vIDGLMst") = GenerateID("QL_trnglmst", bussinessunit.SelectedValue)
        Session("vIDGLDtl") = GenerateID("QL_trngldtl", bussinessunit.SelectedValue)

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        objCmd.Transaction = objTrans
        Try
            Dim first, last, nextfirst As Date
            first = New Date(DDLYear.SelectedValue, DDLMonth.SelectedValue, 1)
            last = New Date(DDLYear.SelectedValue, DDLMonth.SelectedValue, Date.DaysInMonth(DDLYear.SelectedValue, DDLMonth.SelectedValue))
            Dim nextmonth, nextyear As Integer
            If DDLMonth.SelectedValue = 12 Then
                nextmonth = 1 : nextyear = DDLYear.SelectedValue + 1
            Else
                nextmonth = DDLMonth.SelectedValue + 1 : nextyear = DDLYear.SelectedValue
            End If
            nextfirst = New Date(nextyear, nextmonth, 1)

            ' Untuk laba (rugi) berjalan
            Dim totPendapatan As Double = 0
            Dim totBiaya As Double = 0

            Dim objTable As DataTable : objTable = Session("TblDtlDep")
            Dim dLRBulanBerjalanIDR As Double = 0
            Dim dLRBulanBerjalanUSD As Double = 0
            Dim dLRTahunBerjalanIDR As Double = 0
            Dim dLRTahunBerjalanUSD As Double = 0

            ' Get amount L/R Bulan Berjalan this month, utk nanti ditambahkan ke saldo next month
            ' Get amount L/R Tahun Berjalan this month, utk nanti ditambahkan ke saldo next year
            For C1 As Integer = 0 To objTable.Rows.Count - 1
                If (Left(objTable.Rows(C1)("acctgcode").ToString, varLabaBerjalan.Length) = varLabaBerjalan) Then
                    dLRBulanBerjalanIDR += ToDouble(objTable.Rows(C1)("amtbalanceidr").ToString)
                    dLRBulanBerjalanUSD += ToDouble(objTable.Rows(C1)("amtbalanceusd").ToString)
                ElseIf (Left(objTable.Rows(C1)("acctgcode").ToString, varLabaTahunBerjalan.Length) = varLabaTahunBerjalan) Then
                    dLRTahunBerjalanIDR += ToDouble(objTable.Rows(C1)("amtbalanceidr").ToString)
                    dLRTahunBerjalanUSD += ToDouble(objTable.Rows(C1)("amtbalanceusd").ToString)
                End If
            Next

            ' QL_crdgl
            For C1 As Integer = 0 To objTable.Rows.Count - 1
                ' SUMMARIZE CLOSING MONTH
                sSql = "UPDATE QL_crdgl SET amtdebit=" & ToDouble(objTable.Rows(C1)("amtdebetidr").ToString) & ", amtcredit=" & ToDouble(objTable.Rows(C1)("amtcreditidr").ToString) & ", amtbalance=" & ToDouble(objTable.Rows(C1)("amtbalanceidr").ToString) & ", amtdebitusd=" & ToDouble(objTable.Rows(C1)("amtdebetusd").ToString) & ", amtcreditusd=" & ToDouble(objTable.Rows(C1)("amtcreditusd").ToString) & ", amtbalanceusd=" & ToDouble(objTable.Rows(C1)("amtbalanceusd").ToString) & ", postdate=" & IIf(closingstatus.Text = "CLOSED", "CURRENT_TIMESTAMP", "'1/1/1900'") & ", upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, crdglflag='" & closingstatus.Text & "', crdglnote='MONTHLY CLOSING BALANCE - " & GetDateToPeriodAcctg(last) & "' WHERE cmpcode='" & bussinessunit.SelectedValue & "' AND acctgoid=" & objTable.Rows(C1)("acctgoid").ToString & " AND periodacctg='" & GetDateToPeriodAcctg(last) & "'"
                objCmd.CommandText = sSql
                If objCmd.ExecuteNonQuery() <= 0 Then
                    If Not (ToDouble(objTable.Rows(C1)("amtopenidr").ToString) = 0 And ToDouble(objTable.Rows(C1)("amtdebetidr").ToString) = 0 And ToDouble(objTable.Rows(C1)("amtcreditidr").ToString) = 0 And ToDouble(objTable.Rows(C1)("amtbalanceidr").ToString) = 0 And ToDouble(objTable.Rows(C1)("amtopenusd").ToString) = 0 And ToDouble(objTable.Rows(C1)("amtdebetusd").ToString) = 0 And ToDouble(objTable.Rows(C1)("amtcreditusd").ToString) = 0 And ToDouble(objTable.Rows(C1)("amtbalanceusd").ToString) = 0) Then
                        ' INSERT SALDO BULAN CLOSING
                        sSql = "INSERT INTO QL_crdgl (cmpcode, crdgloid, periodacctg, crdgldate, acctgoid, amtopen, amtdebit, amtcredit, amtbalance, amtopenusd, amtdebitusd, amtcreditusd, amtbalanceusd, postdate, crdglflag, crdglnote, createuser, createtime, upduser, updtime) VALUES ('" & bussinessunit.SelectedValue & "', " & Session("vIDCRD") & ", '" & GetDateToPeriodAcctg(last) & "', '" & first & "', " & objTable.Rows(C1)("acctgoid").ToString & ", " & ToDouble(objTable.Rows(C1)("amtopenidr").ToString) & ", " & ToDouble(objTable.Rows(C1)("amtdebetidr").ToString) & ", " & ToDouble(objTable.Rows(C1)("amtcreditidr").ToString) & ", " & ToDouble(objTable.Rows(C1)("amtbalanceidr").ToString) & ", " & ToDouble(objTable.Rows(C1)("amtopenusd").ToString) & ", " & ToDouble(objTable.Rows(C1)("amtdebetusd").ToString) & ", " & ToDouble(objTable.Rows(C1)("amtcreditusd").ToString) & ", " & ToDouble(objTable.Rows(C1)("amtbalanceusd").ToString) & ", " & IIf(closingstatus.Text = "CLOSED", "CURRENT_TIMESTAMP", "'1/1/1900'") & ", '" & closingstatus.Text & "', 'MONTHLY CLOSING BALANCE - " & GetDateToPeriodAcctg(last) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        Session("vIDCRD") += 1
                    End If
                Else
                    If (ToDouble(objTable.Rows(C1)("amtopenidr").ToString) = 0 And ToDouble(objTable.Rows(C1)("amtdebetidr").ToString) = 0 And ToDouble(objTable.Rows(C1)("amtcreditidr").ToString) = 0 And ToDouble(objTable.Rows(C1)("amtbalanceidr").ToString) = 0 And ToDouble(objTable.Rows(C1)("amtopenusd").ToString) = 0 And ToDouble(objTable.Rows(C1)("amtdebetusd").ToString) = 0 And ToDouble(objTable.Rows(C1)("amtcreditusd").ToString) = 0 And ToDouble(objTable.Rows(C1)("amtbalanceusd").ToString) = 0) Then
                        sSql = "DELETE QL_crdgl WHERE cmpcode='" & bussinessunit.SelectedValue & "' AND acctgoid=" & objTable.Rows(C1)("acctgoid").ToString & " AND periodacctg='" & GetDateToPeriodAcctg(last) & "'"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    End If
                End If

                If closingstatus.Text = "CLOSED" Then
                    ' Calculate Value utk Next Month
                    Dim amtBalanceIDR As Double = ToDouble(objTable.Rows(C1)("amtbalanceidr").ToString)
                    Dim amtBalanceUSD As Double = ToDouble(objTable.Rows(C1)("amtbalanceusd").ToString)

                    ' Set AmtOpen NextMonth=0 bila periode closing adalah akhir bulan dan merupakan akun Pendapatan / Biaya
                    If objTable.Rows(C1)("acctgres3").ToString = "LABARUGI" Then
                        amtBalanceIDR = 0 : amtBalanceUSD = 0
                    End If

                    ' Pada Closing Desember L/R Tahun Berjalan dan L/R Bulan Berjalan ditambahkan ke L/R Ditahan next year 
                    If DDLMonth.SelectedValue = 12 Then
                        If Left(objTable.Rows(C1)("acctgcode").ToString, varLabaBerjalan.Length) = varLabaBerjalan Or Left(objTable.Rows(C1)("acctgcode").ToString, varLabaTahunBerjalan.Length) = varLabaTahunBerjalan Then
                            amtBalanceIDR = 0 : amtBalanceUSD = 0
                        ElseIf Left(objTable.Rows(C1)("acctgcode").ToString, varLabaDitahan.Length) = varLabaDitahan Then
                            amtBalanceIDR += (dLRBulanBerjalanIDR + dLRTahunBerjalanIDR)
                            amtBalanceUSD += (dLRBulanBerjalanUSD + dLRTahunBerjalanUSD)
                        End If
                    Else
                        ' Selain Desember, L/R Bulan ini diakumulasikan ke L/R Tahun Berjalan next month
                        If Left(objTable.Rows(C1)("acctgcode").ToString, varLabaBerjalan.Length) = varLabaBerjalan Then
                            amtBalanceIDR = 0 : amtBalanceUSD = 0
                        ElseIf Left(objTable.Rows(C1)("acctgcode").ToString, varLabaTahunBerjalan.Length) = varLabaTahunBerjalan Then
                            amtBalanceIDR += (dLRBulanBerjalanIDR) : amtBalanceUSD += (dLRBulanBerjalanUSD)
                        End If
                    End If

                    If Not (amtBalanceIDR = 0 And amtBalanceUSD = 0) Then
                        ' INSERT SALDO AWAL BULAN SELANJUTNYA
                        sSql = "INSERT INTO QL_crdgl (cmpcode, crdgloid, periodacctg, crdgldate, acctgoid, amtopen, amtdebit, amtcredit, amtbalance, amtopenusd, amtdebitusd, amtcreditusd, amtbalanceusd, postdate, crdglflag, crdglnote, createuser, createtime, upduser, updtime) VALUES ('" & bussinessunit.SelectedValue & "', " & Session("vIDCRD") & ", '" & GetDateToPeriodAcctg(nextfirst) & "', '" & nextfirst & "', " & objTable.Rows(C1)("acctgoid").ToString & ", " & amtBalanceIDR & ", 0, 0, " & amtBalanceIDR & ", " & amtBalanceUSD & ", 0, 0, " & amtBalanceUSD & ", CURRENT_TIMESTAMP, 'OPEN', 'OPENING BALANCE - " & GetDateToPeriodAcctg(nextfirst) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        Session("vIDCRD") += 1
                    End If
                End If
            Next
            sSql = "UPDATE QL_mstoid SET lastoid=" & Session("vIDCRD") - 1 & " WHERE cmpcode='" & bussinessunit.SelectedValue & "' AND tablename='QL_crdgl'"
            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

            ' Simpan Data History Closing Bulanan
            sSql = "UPDATE QL_acctgclosinghistory SET historyclosingdate='" & last & "', historyclosinguserid='" & Session("UserID") & "', historyclosingtime=CURRENT_TIMESTAMP, closingstatus='" & closingstatus.Text & "', closingnote='MONTHLY CLOSING - " & GetDateToPeriodAcctg(last) & "' WHERE cmpcode='" & bussinessunit.SelectedValue & "' AND closingperiod='" & GetDateToPeriodAcctg(last) & "'"
            objCmd.CommandText = sSql
            If objCmd.ExecuteNonQuery <= 0 Then
                sSql = "INSERT INTO QL_acctgclosinghistory (cmpcode, historyclosingoid, historyopendate, historyopenuserid, historyopentime, historyclosingdate, historyclosinguserid, historyclosingtime, closingperiod, closingstatus, closingnote, closinggroup, createuser, createtime, upduser, updtime, closingmonth, closingmonthname, closingyear) VALUES ('" & bussinessunit.SelectedValue & "', " & Session("vIDCH") & ", '" & first & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & last & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & GetDateToPeriodAcctg(last) & "', '" & closingstatus.Text & "', 'MONTHLY CLOSING - " & GetDateToPeriodAcctg(last) & "', 'MONTHLY', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & DDLMonth.SelectedValue & ", '" & DDLMonth.SelectedItem.Text & "', " & DDLYear.SelectedValue & ")"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & Session("vIDCH") & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_acctgclosinghistory'"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
            End If

            objTrans.Commit() : conn.Close()
        Catch exSql As SqlException
            objTrans.Rollback() : conn.Close()
            If ToDouble(Session("ErrorCounter")) < 5 Then
                If exSql.Number = 2627 Then
                    Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                    btnSave_Click(sender, e)
                    Exit Sub
                Else
                    showMessage(exSql.ToString & "<BR>" & sSql, 1) : Exit Sub
                End If
            Else
                showMessage(exSql.ToString & "<BR>" & sSql, 1) : Exit Sub
            End If
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & "<BR>" & sSql, 1) : Exit Sub
        End Try
        Response.Redirect("~\Accounting\trnMonthlyClosing.aspx?awal=true")
    End Sub
#End Region

    Protected Sub btnOpen_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        objCmd.Transaction = objTrans
        Try
            Dim last As Date = New Date(DDLYear.SelectedValue, DDLMonth.SelectedValue, Date.DaysInMonth(DDLYear.SelectedValue, DDLMonth.SelectedValue))
            sSql = "exec dbo.sp_open_data_closing '" & GetDateToPeriodAcctg(last) & "'"
            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & "<BR>" & sSql, 1) : Exit Sub
        End Try
        Response.Redirect("~\Accounting\trnMonthlyClosing.aspx?awal=true")
    End Sub
End Class
