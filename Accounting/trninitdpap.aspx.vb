'Created By Widi On 11 August 2014
'Modified By Vriska On 7 October 2014
'Modified By Chafid on 2 December 2014
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Partial Class Accounting_trninitdpap
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim xCmd As New SqlCommAND("", conn)
    Dim xreader As SqlDataReader
    Private connExcel As New OleDbConnection
    Private adapt As New OleDbDataAdapter
    Dim dsData As New DataSet
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim cRate As New ClassRate
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Public Function GetIDCB() As String
        Return Eval("cmpcode") & "," & Eval("dpapoid")
    End Function

    Public Function getCheckBoxValue(ByVal nRowNum As Integer, ByVal nCol As Integer) As Boolean
        Dim sReturn As Boolean
        Dim row As System.Web.UI.WebControls.GridViewRow = gvMst.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                    If cbcheck Then
                        sReturn = True
                    ElseIf Not cbcheck Then
                        sReturn = False
                    End If
                End If
            Next
        End If
        getCheckBoxValue = sReturn
    End Function

#End Region

#Region "Procedures"

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption
        lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitAllDDL()
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(Outlet, sSql)
        'trndpapdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
        sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & Outlet.SelectedValue & "' AND activeflag='ACTIVE'"
		FillDDL(CurroidDDL, sSql)
        cRate.SetRateValue(CInt(CurroidDDL.SelectedValue), Format(GetServerTime, "MM/dd/yyyy"))
        FillDDLAcctg(trndpapacctgoid, "VAR_DPAP", CompnyCode)
        If trndpapacctgoid.Items.Count < 1 Then
            showMessage("Isi/Buat account VAR_DPAP di master accounting!!", 2)
        End If
    End Sub

    Private Sub BindSupplier()
        sSql = "select suppoid,suppcode,supptype,suppname,suppaddr,apacctgoid AS coa_hutang, acctgcode AS coa FROM QL_mstsupp s inner join ql_mstacctg a ON a.acctgoid=s.apacctgoid WHERE s.cmpcode = '" & Outlet.SelectedValue & "' AND " & FilterDDLListSupp.SelectedValue & " LIKE '%" & Tchar(FilterTextListSupp.Text) & "%' ORDER BY suppname "

        Dim objTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstsupp")
        Session("TblSupp") = objTbl
        gvSupplier.DataSource = objTbl
        gvSupplier.DataBind()
        'sSql = "SELECT c.* FROM QL_mstsupp c " & _
        '    "WHERE c.cmpcode='" & CompnyCode & "' " & sPlus & " ORDER BY c.suppcode"
        'FillGV(gvSupp, sSql, "QL_mstcreditcard")
    End Sub

    Private Sub BindData(ByVal sPLus As String)
        Try
            Dim tgle As Date = CDate(dateAwal.Text)
            tgle = CDate(dateAkhir.Text)
        Catch ex As Exception
            showMessage("Invalid format date !!", 2)
            Exit Sub
        End Try
        If CDate(dateAwal.Text) > CDate(dateAkhir.Text) Then
            showMessage("Second/End Period must be greater than First Period !!", 2)
            Exit Sub
        End If

        sSql = "SELECT ar.dpapoid,ar.dpapno,ar.dpapdate,ar.dpapamt, ar.dpapstatus,ar.dpapnote,0 dpapflag,dpappaytype giroNo,ar.cmpcode,ar.cmpcode as outlet,suppname,dpappaytype, 0 AS SELECTed FROM QL_trndpap ar INNER JOIN QL_mstsupp s ON ar.suppoid=s.suppoid WHERE dpapoid<=0 AND dpapstatus <> 'DELETE'" & sPLus & " AND ar.dpapdate between '" & CDate(dateAwal.Text) & "' AND '" & CDate(dateAkhir.Text) & "' " & IIf(statuse.SelectedValue = "ALL", "", " AND ar.dpapstatus='" & statuse.SelectedValue & "'") & " "
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND ar.cmpcode LIKE '" & Session("CompnyCode") & "%'"
        Else
            sSql &= " AND ar.cmpcode LIKE '%'"
        End If
        sSql += " ORDER BY dpapno"

        Dim objTable As DataTable = cKon.ambiltabel(sSql, "QL_trndpap")
        Session("TblDPAPInit") = objTable
        gvMst.DataSource = Session("TblDPAPInit")
        gvMst.DataBind()
        UnabledCheckBox()
    End Sub

    Private Sub DDLPayreftypeChange()
        If payreftype.SelectedValue = "CASH" Then
            tr1.Visible = False
            tr2.Visible = False
        Else
            tr1.Visible = True
            tr2.Visible = True
            payduedate.Text = Format(Now, "MM/dd/yyyy")
        End If
    End Sub

    Private Sub FillTextBox(ByVal iOid As Integer, ByVal vOutlet As String)
        sSql = "SELECT ar.dpapoid,ar.cashbankoid,ar.dpapno,ar.dpapdate,ar.suppoid,suppname, " & _
               "ar.dpappaytype,0,ar.dpapduedate,ar.dpappayrefno,ar.dpappayacctgoid," & _
               "ar.dpapamt,ar.dpapnote,ar.dpapstatus,ar.upduser, " & _
               "ar.updtime,ar.createuser,ar.createtime,ar.cmpcode " & _
               "FROM QL_trndpap ar INNER JOIN ql_mstsupp s ON ar.suppoid=s.suppoid " & _
               "WHERE ar.cmpcode='" & vOutlet & "' AND ar.dpapoid=" & iOid
        Dim dtMst As DataTable = cKon.ambiltabel(sSql, "QL_trndpap")
        If dtMst.Rows.Count < 1 Then
            showMessage("Can't load data DP AP Balance !!", 2)
            Exit Sub
        Else
            trndpapoid.Text = dtMst.Rows(0)("dpapoid").ToString
            CompnyCode = dtMst.Rows(0)("cmpcode").ToString
            trndpapno.Text = dtMst.Rows(0)("dpapno").ToString
            trndpapdate.Text = Format(CDate(dtMst.Rows(0)("dpapdate").ToString), "MM/dd/yyyy")
            suppname.Text = dtMst.Rows(0)("suppname").ToString
            suppoid.Text = dtMst.Rows(0)("suppoid").ToString
            payreftype.Text = dtMst.Rows(0)("dpappaytype").ToString
            payduedate.Text = Format(CDate(dtMst.Rows(0)("dpapduedate").ToString), "MM/dd/yyyy")
            payrefno.Text = dtMst.Rows(0)("dpappayrefno").ToString
            trndpapacctgoid.SelectedValue = dtMst.Rows(0)("dpappayacctgoid").ToString
            trndpapamt.Text = ToMaskEdit(ToDouble(dtMst.Rows(0)("dpapamt").ToString), 2)
            'trndpapamt.Text = ToDouble(dtMst.Rows(0)("dpapamt").ToString) * 100
            trndpapnote.Text = dtMst.Rows(0)("dpapnote").ToString
            trndpapstatus.Text = dtMst.Rows(0)("dpapstatus").ToString

            If payreftype.SelectedValue = "CASH" Then
                tr1.Visible = False
                tr2.Visible = False
            Else
                tr1.Visible = True
                tr2.Visible = True
            End If

            create.Text = "Created By <B>" & dtMst.Rows(0)("createuser").ToString & "</B> On <B>" & dtMst.Rows(0)("createtime").ToString & "</B> "
            update.Text = "; Last Updated By <B>" & dtMst.Rows(0)("upduser").ToString & "</B> On <B>" & dtMst.Rows(0)("updtime").ToString & "</B> "

            If trndpapstatus.Text <> "POST" Then
                btnSave1.Visible = True
                btnDelete1.Visible = True
                btnposting1.Visible = True
            Else
                btnSave1.Visible = False
                btnDelete1.Visible = False
                btnposting1.Visible = False
            End If
        End If
        outlet.Enabled = False
        outlet.CssClass = "inpTextDisabled"

        If trndpapstatus.Text <> "In Process" Then
            'btnSave.Visible = False
            'btnDelete.Visible = False
            'btnPosting2.Visible = False
            trndpapdate.CssClass = "inpTextDisabled"
            trndpapdate.ReadOnly = True
            imbDPAPDate.Visible = False
            trndpapno.Enabled = False
            outlet.Enabled = False
            suppname.Enabled = False
            imbSearchCust.Visible = False
            imbClearCust.Visible = False
            trndpapacctgoid.Enabled = False
            payreftype.Enabled = False
            payduedate.CssClass = "inpTextDisabled"
            payduedate.ReadOnly = True
            imbDueDate.Visible = False
            payrefno.Enabled = False
            trndpapamt.Enabled = False
            trndpapnote.Enabled = False
        End If
    End Sub

    Public Sub UnabledCheckBox()
        Try
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session("TblDPAPInit")
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            Dim irowne As Int16 = 0
            If objRow.Length() > 0 Then
                For C1 As Integer = 0 + (gvMst.PageIndex * 10) To objRow.Length() - 1
                    If Trim(objRow(C1)("dpapstatus".ToString)) = "POST" Or Trim(objRow(C1)("dpapstatus".ToString) = "DELETE") Then
                        Try
                            Dim row As System.Web.UI.WebControls.GridViewRow = gvMst.Rows(irowne)
                            If (row.RowType = DataControlRowType.DataRow) Then
                                Dim cc As System.Web.UI.ControlCollection = row.Cells(1).Controls
                                For Each myControl As System.Web.UI.Control In cc
                                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                        CType(myControl, System.Web.UI.WebControls.CheckBox).Enabled = False
                                    End If
                                Next
                            End If
                        Catch ex As Exception
                        End Try
                    End If
                    irowne += 1
                Next
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Sub CheckAll()

        Try
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session("TblDPAPInit")
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRow.Length() > 0 Then
                For C1 As Integer = 0 To objRow.Length() - 1
                    If Trim(objRow(C1)("trndpapstatus").ToString) <> "POST" And Trim(objRow(C1)("trndpapstatus").ToString) <> "DELETE" Then
                        Dim row As System.Web.UI.WebControls.GridViewRow = gvMst.Rows(C1)
                        If (row.RowType = DataControlRowType.DataRow) Then
                            Dim cc As System.Web.UI.ControlCollection = row.Cells(1).Controls
                            For Each myControl As System.Web.UI.Control In cc
                                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                    CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = True
                                End If
                            Next
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Sub UncheckAll()
        Try
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session("TblDPAPInit")
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRow.Length() > 0 Then
                For C1 As Integer = 0 To objRow.Length() - 1
                    If Trim(objRow(C1)("trndpapstatus").ToString) <> "POST" And Trim(objRow(C1)("trndpapstatus").ToString) <> "DELETE" Then
                        Dim row As System.Web.UI.WebControls.GridViewRow = gvMst.Rows(C1)
                        If (row.RowType = DataControlRowType.DataRow) Then
                            Dim cc As System.Web.UI.ControlCollection = row.Cells(1).Controls
                            For Each myControl As System.Web.UI.Control In cc
                                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                    CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = False
                                End If
                            Next
                        End If
                    End If
                Next
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub UpdateCheckedPost()
        'Untuk mendapat cek box value multi SELECT
        Dim dv As DataView = Session("TblDPAPInit").DefaultView
        dv.AllowEdit = True
        For C1 As Integer = 0 To gvMst.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvMst.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(1).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        Dim a As String() = sOid.Split(",")
                        dv.RowFilter = "dpapoid=" & a(1) & " AND cmpcode='" & a(0) & "'"
                        If cbcheck = True Then
                            dv(0)("SELECTed") = 1
                        Else
                            dv(0)("SELECTed") = 0
                        End If
                        dv.RowFilter = ""
                    End If
                Next
            End If
        Next
    End Sub

    Private Sub PrintReport(Optional ByVal sType As String = "")
        Try
            If sType = "" Then
                report.Load(Server.MapPath(folderReport & "rptInitDPAP.rpt"))
            Else
                report.Load(Server.MapPath(folderReport & "rptInitDPAPXls.rpt"))
            End If
            'Dim sWhere As String = " WHERE cmpcode='CORP' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
            'If cbCategory.Checked Then
            '    sWhere &= " AND acctggrp1='" & FilterDDLCategory.SelectedValue & "'"
            'End If
            'If cbStatus.Checked Then
            '    If FilterDDLStatus.SelectedValue <> "ALL" Then
            '        sWhere &= " AND activeflag='" & FilterDDLStatus.SelectedValue & "'"
            '    End If
            'End If
            'If checkPagePermission("~\Master\mstAcctg.aspx", Session("SpecialAccess")) = False Then
            '    sWhere &= " AND createuser='" & Session("UserID") & "'"
            'End If
            Dim sWhere As String = ""
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            If sType = "" Then
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "DPAPInitBalanceReport")
            Else
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "DPAPInitBalanceReport")
            End If
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            'showMessage(ex.Message, 1)
        End Try
    End Sub 'OK

#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            Session("sCmpcode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Accounting\trninitdpap.aspx")
        End If
        If checkPagePermission("~\Accounting\trninitdpap.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - DP AP Inittial Balance"
        Session("oid") = Request.QueryString("oid")
   
        btnDelete1.Attributes.Add("OnClick", "javascript:return confirm('Apakah ANDa yakin akan menghapus data ini ?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Apakah ANDa yakin akan mem-posting data ini ?');")
        btnposting1.Attributes.Add("OnClick", "javascript:return confirm('Apakah ANDa yakin akan mem-posting data ini ?');")

        If Not Page.IsPostBack Then
            dateAwal.Text = Format(GetServerTime, "MM/01/yyyy")
            dateAkhir.Text = Format(GetServerTime, "MM/dd/yyyy")
            BindData("")
            InitAllDDL()
            cRate.SetRateValue(CInt(CurroidDDL.SelectedValue), Format(GetServerTime, "MM/01/yyyy"))
            DDLPayreftypeChange( )
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"), Session("sCmpcode"))
                TabContainer1.ActiveTabIndex = 1
            Else
                trndpapdate.Text = Format(GetServerTime, "MM/dd/yyyy")
                payduedate.Text = Format(GetServerTime, "MM/dd/yyyy")
                TabContainer1.ActiveTabIndex = 0
                create.Text = "Created By <B>" & Session("UserID") & "</B> On <B>" & GetServerTime() & "</B>"
                update.Text = ""
                trndpapstatus.Text = "In Process"
                btnDelete1.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
    End Sub

    Protected Sub imbSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListSupp.SelectedIndex = -1
        FilterTextListSupp.Text = ""
        gvSupplier.SelectedIndex = -1
        BindSupplier()
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
        'BindSupplier(" AND (suppname like '%" & Tchar(suppname.Text) & "%' or suppcode like '%" & Tchar(suppname.Text) & "%')")
        'gvSupp.Visible = True
    End Sub

    Protected Sub imbClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        suppoid.Text = ""
        suppname.Text = ""
        'gvSupp.Visible = False
    End Sub

    Protected Sub btnCheckAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckAll.Click
        CheckAll()
    End Sub

    Protected Sub btnUncheckAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUncheckAll.Click
        UncheckAll()
    End Sub

    Protected Sub btnPosting_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosting.Click
        Dim sErrorku As String = ""
        If Not Session("TblDPAPInit") Is Nothing Then
            Dim dt As DataTable = Session("TblDPAPInit")
            If dt.Rows.Count > 0 Then
                UpdateCheckedPost()
                Dim dv As DataView = dt.DefaultView

                dv.RowFilter = "SELECTed=1"
                If dv.Count = 0 Then
                    sErrorku &= "Please SELECT DP AP first!"
                End If
                dv.RowFilter = ""
                dv.RowFilter = "SELECTed=1 AND dpapstatus='POST'"
                If dv.Count > 0 Then
                    sErrorku &= "DP AP have been POSTED before!"
                End If
                dv.RowFilter = ""

                If sErrorku <> "" Then
                    showMessage(sErrorku, 2)
                    dv.RowFilter = ""
                    Exit Sub
                End If

                Dim parsingOID As Integer = 0
                Dim sCmpcode As String = ""
                Dim dtAwal As DataTable = Session("TblDPAPInit")
                Dim dvAwal As DataView = dtAwal.DefaultView
                Dim iSeq As Integer = dvAwal.Count + 1
                dvAwal.AllowEdit = True
                dvAwal.AllowNew = True
                dv.RowFilter = "SELECTed=1 AND dpapstatus='In Process'"
                'poststatx.Text = "SPECIAL"

                Dim seq As Integer = 0
                For C1 As Integer = 0 To dv.Count - 1
                    parsingOID = dv(C1)("dpapoid").ToString
                    sCmpcode = dv(C1)("cmpcode").ToString
                    filltextboxSELECTED(sCmpcode, parsingOID)
                    If Not (Session("errmsg") Is Nothing Or Session("errmsg") = "") Then
                        sErrorku &= "Can't POST DP AP No " & dv(C1)("dpapno").ToString & " with reason:<BR>" & Session("errmsg")
                    End If
                    seq = C1
                Next

                If seq = dv.Count - 1 Then
                    showMessage("Data Telah diPosting <BR>", 2)
                    Response.Redirect("trninitdpap.aspx?awal=true")
                End If

            End If
        End If
    End Sub

    Private Sub filltextboxSELECTED(ByVal sCmpcode As String, ByVal SELECTedoid As Integer)
        'If Not SELECTedoid = Nothing Or SELECTedoid <> 0 Then
        '    Dim sMsg As String = ""
        '    Dim vpayid As Integer = selectedoid
        '    Session("oid") = vpayid.ToString
        '    Try
        '        sSql = "SELECT ar.trndpapoid,ar.cashbankoid,ar.trndpapno,ar.trndpapdate,ar.suppoid,suppname, ar.payreftype,ar.cashbankacctgoid,ar.payduedate,ar.payrefno,ar.trndpapacctgoid, ar.trndpapamt,ar.trndpapnote,ar.trndpapstatus,ar.upduser,  ar.updtime,ar.createuser,ar.createtime,ar.cmpcode FROM QL_trndpap ar INNER JOIN ql_mstsupp s ON ar.suppoid=s.suppoid WHERE ar.cmpcode='" & sCmpcode & "' AND ar.trndpapoid=" & selectedoid
        '        Dim dtMst As DataTable = cKon.ambiltabel(sSql, "QL_trndpap")
        '        If dtMst.Rows.Count < 1 Then
        '            showMessage("can't load data DP AP Balance !!<BR>Info :<BR>" & sSql, 2)
        '            'QLMsgBox1.ShowMessage("can't load data DP AP Balance !!<BR>Info :<BR>" & sSql, 2, "")
        '            Exit Sub
        '        Else
        '            trndpapoid.Text = dtMst.Rows(0)("trndpapoid").ToString
        '            CompnyCode = dtMst.Rows(0)("cmpcode").ToString
        '            trndpapno.Text = dtMst.Rows(0)("trndpapno").ToString
        '            trndpapdate.Text = Format(CDate(dtMst.Rows(0)("trndpapdate").ToString), "dd/MM/yyyy")
        '            suppname.Text = dtMst.Rows(0)("suppname").ToString
        '            suppoid.Text = dtMst.Rows(0)("suppoid").ToString
        '            payreftype.Text = dtMst.Rows(0)("payreftype").ToString
        '            payduedate.Text = Format(CDate(dtMst.Rows(0)("payduedate").ToString), "dd/MM/yyyy")
        '            payrefno.Text = dtMst.Rows(0)("payrefno").ToString
        '            DDLPayreftypeChange()
        '            trndpapacctgoid.SelectedValue = dtMst.Rows(0)("trndpapacctgoid").ToString
        '            trndpapamt.Text = ToMaskEdit(ToDouble(dtMst.Rows(0)("trndpapamt").ToString), 2)
        '            trndpapnote.Text = dtMst.Rows(0)("trndpapnote").ToString
        '            trndpapstatus.Text = dtMst.Rows(0)("trndpapstatus").ToString
        '            create.Text = "Created By <B>" & dtMst.Rows(0)("createuser").ToString & "</B> On <B>" & dtMst.Rows(0)("createtime").ToString & "</B> "
        '            update.Text = "; Last Updated By <B>" & dtMst.Rows(0)("upduser").ToString & "</B> On <B>" & dtMst.Rows(0)("updtime").ToString & "</B> "

        '            Session("TblDPAPInitdtl") = dtMst

        '            If trndpapstatus.Text <> "POST" Then
        '                btnSave.Visible = True : btnDelete.Visible = True : btnPosting.Visible = True
        '            Else
        '                btnSave.Visible = False : btnDelete.Visible = False : btnPosting.Visible = False
        '            End If
        '        End If
        '        outlet.Enabled = False
        '        outlet.CssClass = "inpTextDisabled"

        '    Catch ex As Exception
        '        xreader.Close() : conn.Close()
        '        sMsg &= "Gagal menampilkan data DP AP Balance.<BR>Info:" & ex.Message & "<BR>" & sSql & "<BR><BR>"
        '    End Try

        'Else
        '    showMessage("DP AP Balance salah !", 2)
        '    'QLMsgBox1.ShowMessage("DP AP Balance salah !", 2, "")
        '    Exit Sub
        'End If
        'TabContainer1.ActiveTabIndex = 0
        'btnPosting2_Click(Nothing, Nothing)
        'TabContainer1.ActiveTabIndex = 0
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        BindData(" AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' ")
    End Sub

    Protected Sub gvMst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvMst.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = Format(CDate(e.Row.Cells(4).Text), "MM/dd/yyyy")
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 2)
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindData(" AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' ")
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAll.Click
        dateAwal.Text = Format(Now, "MM/01/yyyy")
        dateAkhir.Text = Format(Now, "MM/dd/yyyy")
        FilterDDL.SelectedIndex = 0
        FilterText.Text = ""
        statuse.SelectedIndex = 0
        BindData(" AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' ")
    End Sub

    Protected Sub payreftype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles payreftype.SelectedIndexChanged
        DDLPayreftypeChange()
    End Sub

    Protected Sub btnCancel1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("~\accounting\trninitdpap.aspx?awal=true")
    End Sub

    Protected Sub btnDelete1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE FROM QL_trndpap WHERE cmpcode='" & CompnyCode & "' AND dpapoid=" & trndpapoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 2)
            Exit Sub
        End Try
        Response.Redirect("trninitdpap.aspx?awal=true")
    End Sub

    Protected Sub btnposting1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        trndpapstatus.Text = "POST"
        btnSave1_Click(sender, e)
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindData(" AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' ")
    End Sub

    Protected Sub btnViewAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        dateAwal.Text = Format(GetServerTime, "MM/01/yyyy")
        dateAkhir.Text = Format(GetServerTime, "MM/dd/yyyy")
        FilterDDL.SelectedIndex = 0 : FilterText.Text = "" : statuse.SelectedIndex = 0
        BindData(" AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' ")
    End Sub

    Protected Sub FilterText_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\accounting\trninitdpap.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PrintReport("Excel")
    End Sub

    Protected Sub trndpapamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        trndpapamt.Text = ToMaskEdit(ToDouble(trndpapamt.Text), 2)
    End Sub

    Protected Sub btnSave1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sMsg As String = ""
        If Not IsValidDate(trndpapdate.Text, "MM/dd/yyyy", "") Then
            showMessage("- Format tanggal DP/AP salah !!<BR>", 2)
            Exit Sub
        End If
        If Not IsValidDate(payduedate.Text, "MM/dd/yyyy", "") Then
            showMessage("- Format tanggal Jatuh Tempo salah !!<BR>", 2)
            Exit Sub
        End If
        If trndpapno.Text = "" Then
            sMsg &= "-  DP No. harus diisi !!<BR>"
        End If

        If suppoid.Text = "" Then
            sMsg &= "- Supplier harus diisi !!<BR>"
        End If

        If Not IsDate(CDate(trndpapdate.Text)) Then
            sMsg &= "- Tanggal harus diisi !!<BR>"
        End If
        If IsDate(CDate(trndpapdate.Text)) = False Then
            sMsg &= "- Tanggal salah !!<BR>"
        End If

        'If payduedate.Visible Then
        '    If Not IsDate(CDate(payduedate.Text)) Then
        '        sMsg &= "- Tanggal Jatuh Tempo harus diisi !!<BR>"
        '    End If
        '    If IsDate(CDate(payduedate.Text)) = False Then
        '        sMsg &= "- Tanggal Jatuh Tempo salah !!<BR>"
        '    End If
        'End If
        If payduedate.Visible And (IsDate(CDate(payduedate.Text))) And (IsDate(CDate(trndpapdate.Text))) Then
            If CDate(payduedate.Text) < CDate(trndpapdate.Text) Then
                sMsg &= "- Tanggal jatuh tempo harus >= tanggal transaksi !!<BR>"
            End If
        End If

        If payrefno.Text = "" And payreftype.SelectedValue <> "CASH" Then
            sMsg &= "- Ref No. harus diisi !!<BR>"
        End If

        If ToDouble(trndpapamt.Text) <= 0 Then
            sMsg &= "- Total DP harus >  0 !!<BR>"
        End If
        If trndpapnote.Text.Trim.Length > 100 Then
            sMsg &= "- Note, maksimal 100 karakter !!<BR>"
        End If

        If CheckDataExists("SELECT COUNT(*) FROM QL_trndpap WHERE cmpcode='" & Session("CompnyCode") & "' AND dpapno='" & Tchar(trndpapno.Text) & "' AND dpapoid<>'" & Session("oid") & "'") = True Then
            sMsg &= "-  DP No sudah ada !!<BR>"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, 2)
            trndpapstatus.Text = "In Process"
            Exit Sub
        End If

        Session("DPAPInit") = GetStrData("SELECT isnull(MIN(dpapoid)-1,-1)" & _
        " FROM QL_trndpap WHERE cmpcode='" & Outlet.SelectedValue & "'")

        sSql = "SELECT count(-1) FROM QL_trndpap WHERE dpapno='" & Tchar(trndpapno.Text) & "'" & _
        "AND dpapstatus='POST' AND  cmpcode='" & Outlet.SelectedValue & "'"
        If GetStrData(sSql) > 0 Then
            showMessage("Data ini sudah di Posting !, Tekan batal untuk lihat Data di List Information", 2)
            trndpapstatus.Text = "In Process"
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        cRate.SetRateValue(CInt(CurroidDDL.SelectedValue), Format(GetServerTime, "MM/dd/yyyy"))
        Try
            If payreftype.SelectedValue = "CASH" Then
                payduedate.Text = "01/01/1900"
            End If

            If Session("oid") = Nothing Or Session("oid") = "" Then
                Dim lastrate As Integer
                Dim RateIDR As Double = ToDouble(trndpapamt.Text) * cRate.GetRateDailyIDRValue
                Dim RateUSD As Double = ToDouble(trndpapamt.Text) * cRate.GetRateDailyUSDValue
                lastrate = GetStrData("SELECT top 1 rateoid FROM ql_mstrate WHERE curroid='" & CurroidDDL.SelectedValue & "' order by rateoid desc")
                sSql = "INSERT INTO QL_trndpap" & _
                "(cmpcode,dpapoid,periodacctg,dpapno,dpapdate,suppoid,acctgoid,cashbankoid,dpappaytype,dpappayacctgoid,dpappayrefno,dpapduedate,curroid,rateoid,rate2oid,dpapamt,dpapaccumamt,dpapnote,dpapstatus,createuser,createtime,upduser,updtime,dpaptakegiro,giroacctgoid,dpapamtidr,dpapamtusd)" & _
                " VALUES" & _
                "('" & Outlet.SelectedValue & "','" & Session("DPAPInit") & "','" & GetDateToPeriodAcctg(CDate(trndpapdate.Text)) & "','" & Tchar(trndpapno.Text) & "','" & CDate(trndpapdate.Text) & "','" & suppoid.Text & "','" & trndpapacctgoid.SelectedValue & "','0','" & payreftype.SelectedValue & "',0,'" & Tchar(payrefno.Text) & "','" & CDate(payduedate.Text) & "','" & CurroidDDL.SelectedValue & "','" & cRate.GetRateDailyOid & "','" & cRate.GetRateMonthlyOid & "','" & ToDouble(trndpapamt.Text) & "',0,'" & Tchar(trndpapnote.Text) & "','" & trndpapstatus.Text & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,1900-01-01,0," & ToDouble(trndpapamt.Text) * cRate.GetRateMonthlyIDRValue & "," & ToDouble(trndpapamt.Text) * cRate.GetRateMonthlyUSDValue & ")"
                '"dpappayacctgoid,dpappaytype,cashbankacctgoid,payduedate,payrefno," & _
                '"dpapamt,taxtype,taxoid,taxpct,taxamt,dpapnote,dpapflag,dpapacumamt,dpapstatus, " & _
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            Else
                sSql = " UPDATE QL_trndpap SET dpapno='" & Tchar(trndpapno.Text) & "'," & _
                 " dpapdate='" & CDate(trndpapdate.Text) & "', " & _
                 " acctgoid=" & trndpapacctgoid.SelectedValue & "," & _
                 " dpapamt=" & ToDouble(trndpapamt.Text) & "," & _
                 " dpapamtidr='" & ToDouble(trndpapamt.Text) * cRate.GetRateMonthlyIDRValue & "'," & _
                 " dpapamtusd='" & ToDouble(trndpapamt.Text) * cRate.GetRateMonthlyUSDValue & "'," & _
                 " dpappaytype='" & payreftype.SelectedValue & "'," & _
                 " dpapduedate='" & CDate(payduedate.Text) & "', " & _
                 " dpappayrefno='" & Tchar(payrefno.Text) & "', " & _
                 " curroid='" & CurroidDDL.SelectedValue & "'," & _
                 " rateoid='" & cRate.GetRateDailyOid & "'," & _
                 " rate2oid='" & cRate.GetRateMonthlyOid & "'," & _
                 " dpapnote='" & Tchar(trndpapnote.Text) & "'," & _
                 " suppoid='" & suppoid.Text & "'," & _
                 " dpapstatus='" & trndpapstatus.Text & "'," & _
                 " upduser='" & Session("UserID") & "',updtime=CURRENT_TIMESTAMP " & _
                 " WHERE cmpcode='" & Outlet.SelectedValue & "' AND dpapoid=" & trndpapoid.Text
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            trndpapstatus.Text = "In Process"
            showMessage(ex.ToString & sSql, 2)
            Exit Sub
        End Try
        'Response.Redirect("~\accounting\trninitdpap.aspx?awal=true")
        If Session("TblDPAPInitdtl") Is Nothing Then
            If trndpapstatus.Text = "POST" Then
                Session("SavedInfo") &= "Data have been posted with DP AP No. = " & trndpapno.Text & " !"
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\accounting\trninitdpap.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub gvSupplier_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupplier.PageIndexChanging
        gvSupplier.PageIndex = e.NewPageIndex
        BindSupplier()
        mpeListSupp.Show()
    End Sub

    Protected Sub gvSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        suppoid.Text = gvSupplier.SelectedDataKey("suppoid").ToString
        suppname.Text = gvSupplier.SelectedDataKey("suppname").ToString
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub btnFindListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSupp.Click
        BindSupplier()
        mpeListSupp.Show()
    End Sub

    Protected Sub btnAllListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListSupp.Click
        FilterDDLListSupp.SelectedIndex = -1
        FilterTextListSupp.Text = ""
        gvSupplier.SelectedIndex = -1
        BindSupplier()
        mpeListSupp.Show()
    End Sub

    Protected Sub lkbCloseListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub trndpapdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim tgle As Date = CDate(trndpapdate.Text)
            tgle = CDate(trndpapdate.Text)
        Catch ex As Exception
            showMessage("Invalid format date !!", 2)
            Exit Sub
        End Try
    End Sub

    Protected Sub payduedate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim tgle As Date = CDate(payduedate.Text)
            tgle = CDate(payduedate.Text)
        Catch ex As Exception
            showMessage("Invalid format date !!", 2)
            Exit Sub
        End Try
    End Sub
#End Region
End Class
