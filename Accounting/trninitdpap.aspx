<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trninitdpap.aspx.vb" Inherits="Accounting_trninitdpap" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                    <asp:Label ID="Label6" runat="server" Font-Bold="True" Text=".: DP AP Balance" CssClass="Title"></asp:Label>
            </th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            &nbsp;<img align="absMiddle" alt="" src="../Images/List.gif" />
                            <strong><span style="font-size: 9pt">:: List of DP AP Balance</span></strong>&nbsp;
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" __designer:wfdid="w443" DefaultButton="btnSearch"><TABLE width=750><TBODY><TR><TD align=left>Periode&nbsp; </TD><TD align=left>:</TD><TD align=left colSpan=4><asp:TextBox id="dateAwal" runat="server" CssClass="inpText" Width="75px" __designer:wfdid="w464"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w465"></asp:ImageButton>&nbsp; -&nbsp; <asp:TextBox id="dateAkhir" runat="server" CssClass="inpText" Width="75px" __designer:wfdid="w466"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w467"></asp:ImageButton></TD></TR><TR><TD align=left>Periode </TD><TD align=left>:</TD><TD align=left colSpan=4><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="139px" __designer:wfdid="w444"><asp:ListItem Value="dpapno">DP AP No.</asp:ListItem>
<asp:ListItem Value="suppname">Supplier</asp:ListItem>
<asp:ListItem Value="dpappaytype">Tipe Pembayaran</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" CssClass="inpText" __designer:wfdid="w445" OnTextChanged="FilterText_TextChanged"></asp:TextBox></TD></TR><TR><TD align=left>Status</TD><TD align=left>:</TD><TD align=left colSpan=4><asp:DropDownList id="statuse" runat="server" CssClass="inpText" Width="119px" __designer:wfdid="w454"><asp:ListItem Value="ALL">ALL</asp:ListItem>
<asp:ListItem Value="IN PROCESS">IN PROCESS</asp:ListItem>
<asp:ListItem>POST</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 25px" align=left colSpan=6><asp:ImageButton id="btnFind" onclick="btnFind_Click" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w455" AlternateText="Find"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewAll" onclick="btnViewAll_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w456" AlternateText="View All"></asp:ImageButton> <asp:ImageButton id="btnExcel" onclick="btnExcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w457"></asp:ImageButton></TD></TR></TBODY></TABLE><asp:GridView id="gvMst" runat="server" Width="100%" __designer:wfdid="w458" AllowSorting="True" GridLines="None" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True" DataKeyNames="dpapoid,cmpcode" PageSize="8" OnPageIndexChanging="gvMst_PageIndexChanging" OnRowDataBound="gvMst_RowDataBound">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True" Visible="False"></asp:CommandField>
<asp:TemplateField HeaderText="posting" Visible="False"></asp:TemplateField>
<asp:HyperLinkField DataNavigateUrlFields="dpapoid,cmpcode" DataNavigateUrlFormatString="trninitdpap.aspx?oid={0}&amp;cmpcode={1}" DataTextField="dpapno" HeaderText="DP AP No.">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="outlet" HeaderText="outlet" Visible="False"></asp:BoundField>
<asp:BoundField DataField="dpapdate" HeaderText="Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dpappaytype" HeaderText="Pay Type">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dpapamt" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dpapstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="No Data Found !!" Width="104px" __designer:wfdid="w27"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w472" TargetControlID="dateAwal" PopupButtonID="imageButton1" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w473" TargetControlID="dateAkhir" PopupButtonID="imageButton2" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" __designer:wfdid="w474" TargetControlID="dateAwal" MaskType="Date" Mask="99/99/9999" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" __designer:wfdid="w475" TargetControlID="dateAkhir" MaskType="Date" Mask="99/99/9999" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> </asp:Panel> <asp:Button id="btnPosting" runat="server" Font-Bold="True" CssClass="orange" Text="POST SELECTED" Visible="False" __designer:wfdid="w459"></asp:Button> <asp:Button id="btnUncheckAll" runat="server" Font-Bold="True" CssClass="red" Text="SELECT NONE" Visible="False" __designer:wfdid="w460"></asp:Button> <asp:Button id="btnCheckAll" runat="server" Font-Bold="True" CssClass="green" Text="SELECT ALL" Visible="False" __designer:wfdid="w461"></asp:Button> <asp:Button id="btnAll" runat="server" Font-Bold="True" CssClass="gray" Text="VIEW ALL" Width="75px" Visible="False" __designer:wfdid="w462"></asp:Button> <asp:Button id="btnSearch" runat="server" Font-Bold="True" CssClass="orange" Text="FIND" Width="75px" Visible="False" __designer:wfdid="w463"></asp:Button> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnExcel"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                            &nbsp;
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 527px"><TBODY><TR><TD style="HEIGHT: 8px" id="TD4" align=left Visible="true"><asp:Label id="trndpapoid" runat="server" Visible="False" __designer:wfdid="w523"></asp:Label></TD><TD style="HEIGHT: 8px" align=left Visible="true"></TD><TD style="HEIGHT: 8px" id="TD7" align=left Visible="true"><asp:Label id="RateOid" runat="server" Visible="False" __designer:wfdid="w524"></asp:Label> <asp:Label id="Rate2Oid" runat="server" Visible="False" __designer:wfdid="w525"></asp:Label> <asp:DropDownList id="Outlet" runat="server" CssClass="inpTextDisabled" Visible="False" __designer:wfdid="w530" Enabled="False"></asp:DropDownList></TD><TD style="HEIGHT: 8px" id="TD6" align=left colSpan=2 Visible="true"><ajaxToolkit:CalendarExtender id="ceDPAPDate" runat="server" __designer:wfdid="w526" TargetControlID="trndpapdate" PopupButtonID="imbDPAPDate" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeDPAPDate" runat="server" __designer:wfdid="w527" TargetControlID="trndpapdate" MaskType="Date" Mask="99/99/9999" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cepayduedate" runat="server" __designer:wfdid="w528" TargetControlID="payduedate" PopupButtonID="imbDueDate" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meepayduedate" runat="server" __designer:wfdid="w529" TargetControlID="payduedate" MaskType="Date" Mask="99/99/9999" CultureName="en-US" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD align=left>Date</TD><TD align=left>:</TD><TD align=left><asp:TextBox id="trndpapdate" runat="server" CssClass="inpText" Width="75px" __designer:wfdid="w531" OnTextChanged="trndpapdate_TextChanged" AutoPostBack="True" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDPAPDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w532" Height="16px"></asp:ImageButton>&nbsp;<asp:Label id="Label1" runat="server" CssClass="Important" Text="MM/dd/yyyy" __designer:wfdid="w533"></asp:Label></TD><TD align=left><asp:Label id="RateToIDR" runat="server" __designer:wfdid="w534"></asp:Label></TD><TD align=left><asp:Label id="RateToUSD" runat="server" __designer:wfdid="w535"></asp:Label></TD></TR><TR><TD align=left>DP No<asp:Label id="Label3" runat="server" CssClass="Important" Text="*" __designer:wfdid="w536"></asp:Label></TD><TD align=left>:</TD><TD align=left><asp:TextBox id="trndpapno" runat="server" CssClass="inpText" Width="150px" __designer:wfdid="w537" MaxLength="20"></asp:TextBox></TD><TD align=left><asp:Label id="Rate2ToIDR" runat="server" __designer:wfdid="w538"></asp:Label></TD><TD align=left><asp:Label id="Rate2ToUSD" runat="server" __designer:wfdid="w539"></asp:Label></TD></TR><TR><TD align=left>Supplier<asp:Label id="Label8" runat="server" CssClass="Important" Text="*" __designer:wfdid="w540"></asp:Label></TD><TD align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="suppname" runat="server" CssClass="inpTextDisabled" Width="150px" __designer:wfdid="w541" Enabled="False" MaxLength="50"></asp:TextBox> <asp:Label id="suppoid" runat="server" Visible="False" __designer:wfdid="w542"></asp:Label> <asp:ImageButton id="imbSearchCust" onclick="imbSearchCust_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w543" Height="16px"></asp:ImageButton> <asp:ImageButton id="imbClearCust" onclick="imbClearCust_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w544"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 4px" align=left><asp:Label id="Label9" runat="server" Text="Account DP" __designer:wfdid="w545" ToolTip="VAR_DPAP"></asp:Label></TD><TD style="HEIGHT: 4px" align=left>:</TD><TD style="HEIGHT: 4px" align=left colSpan=3><asp:DropDownList id="trndpapacctgoid" runat="server" CssClass="inpText" __designer:wfdid="w546"></asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 14px" align=left>Tipe Pembayaran</TD><TD style="HEIGHT: 14px" align=left>:</TD><TD style="HEIGHT: 14px" align=left colSpan=3><asp:DropDownList id="payreftype" runat="server" CssClass="inpText" Width="132px" __designer:wfdid="w547" AutoPostBack="True"><asp:ListItem>CASH</asp:ListItem>
<asp:ListItem>NONCASH</asp:ListItem>
<asp:ListItem>GIRO</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 14px" align=left>Currency</TD><TD style="HEIGHT: 14px" align=left>:</TD><TD style="HEIGHT: 14px" align=left colSpan=3><asp:DropDownList id="CurroidDDL" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w548" Enabled="False"></asp:DropDownList></TD></TR><TR id="tr1" runat="server"><TD style="HEIGHT: 16px" align=left>Jatuh Tempo&nbsp;<asp:Label id="Label4" runat="server" CssClass="Important" Text="*" __designer:wfdid="w549"></asp:Label></TD><TD style="HEIGHT: 16px" align=left>:</TD><TD style="HEIGHT: 16px" align=left colSpan=3><asp:TextBox id="payduedate" runat="server" CssClass="inpText" Width="75px" __designer:wfdid="w550" OnTextChanged="payduedate_TextChanged" AutoPostBack="True" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbDueDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w551"></asp:ImageButton> <asp:Label id="Label5" runat="server" CssClass="Important" Text="MM/dd/yyyy" __designer:wfdid="w552"></asp:Label></TD></TR><TR id="tr2" runat="server"><TD style="HEIGHT: 16px" align=left>Ref No.&nbsp;<asp:Label id="Label2" runat="server" CssClass="Important" Text="*" __designer:wfdid="w553"></asp:Label></TD><TD style="HEIGHT: 16px" align=left>:</TD><TD style="HEIGHT: 16px" align=left colSpan=3><asp:TextBox id="payrefno" runat="server" CssClass="inpText" Width="125px" __designer:wfdid="w554" MaxLength="20"></asp:TextBox></TD></TR><TR><TD style="HEIGHT: 23px" align=left>Amount<asp:Label id="Label7" runat="server" CssClass="Important" Text="*" __designer:wfdid="w555"></asp:Label></TD><TD style="HEIGHT: 23px" align=left>:</TD><TD style="HEIGHT: 23px" align=left><asp:TextBox id="trndpapamt" runat="server" CssClass="inpText" Width="125px" __designer:wfdid="w556" OnTextChanged="trndpapamt_TextChanged" AutoPostBack="True" MaxLength="10">0</asp:TextBox></TD><TD style="HEIGHT: 23px" align=left colSpan=2><ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender4" runat="server" __designer:wfdid="w557" TargetControlID="trndpapamt" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD align=left>Note</TD><TD align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="trndpapnote" runat="server" CssClass="inpText" Width="300px" __designer:wfdid="w558" Height="33px" MaxLength="100" TextMode="MultiLine"></asp:TextBox></TD></TR><TR><TD align=left>Status</TD><TD align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="trndpapstatus" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w559" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" id="TD8" align=left runat="server" Visible="false"></TD><TD style="FONT-SIZE: x-small" align=left runat="server" Visible="false"></TD><TD id="TD1" align=left runat="server" Visible="false"></TD><TD id="TD10" align=left runat="server" Visible="false"></TD><TD id="TD9" align=left runat="server" Visible="false"></TD></TR><TR><TD align=left colSpan=5><asp:Label id="create" runat="server" Font-Bold="False" __designer:wfdid="w560"></asp:Label>&nbsp;<asp:Label id="update" runat="server" Font-Bold="False" __designer:wfdid="w561"></asp:Label><BR /><TABLE><TBODY><TR><TD align=left colSpan=4>&nbsp; <BR /><asp:ImageButton id="btnSave1" onclick="btnSave1_Click" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w562" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel1" onclick="btnCancel1_Click" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w563" AlternateText="Cancel"></asp:ImageButton> <asp:ImageButton id="btnDelete1" onclick="btnDelete1_Click" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w564" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton id="btnposting1" onclick="btnposting1_Click" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w565"></asp:ImageButton> </TD><TD style="FONT-WEIGHT: bold" align=left><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w566"><ProgressTemplate>
<asp:Image id="Image1" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w567"></asp:Image>&nbsp;Please Wait ... 
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                            &nbsp;
                        </ContentTemplate>
                        <HeaderTemplate>
                            &nbsp;<img align="absMiddle" alt="" src="../Images/Form.gif" />
                            <strong><span style="font-size: 9pt">:: Form DP AP Balance</span></strong>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
                <asp:UpdatePanel id="upListSupp" runat="server">
                    <contenttemplate>
<asp:Panel id="pnlListSupp" runat="server" CssClass="modalBox" Width="650px" Visible="False" __designer:wfdid="w10"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSupp" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Supplier" __designer:wfdid="w11"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListSupp" runat="server" Width="100%" __designer:wfdid="w12" DefaultButton="btnFindListSupp">Filter : <asp:DropDownList id="FilterDDLListSupp" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w13"><asp:ListItem Value="suppname">Supp Name</asp:ListItem>
<asp:ListItem Value="suppcode">Supp Code</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListSupp" runat="server" CssClass="inpText" Width="150px" __designer:wfdid="w14"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListSupp" onclick="btnFindListSupp_Click" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w15"></asp:ImageButton> <asp:ImageButton id="btnAllListSupp" onclick="btnAllListSupp_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w16"></asp:ImageButton></asp:Panel></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvSupplier" runat="server" ForeColor="#333333" Width="98%" __designer:wfdid="w17" OnSelectedIndexChanged="gvSupplier_SelectedIndexChanged" AllowSorting="True" GridLines="None" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True" DataKeyNames="suppoid,suppcode,suppname,coa_hutang,supptype,coa" PageSize="5" OnPageIndexChanging="gvSupplier_PageIndexChanging">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Code" SortExpression="suppcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Name" SortExpression="suppname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Width="40%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="40%"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListSupp" onclick="lkbCloseListSupp_Click" runat="server" __designer:wfdid="w18">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSupp" runat="server" __designer:wfdid="w19" TargetControlID="btnHideListSupp" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlListSupp" PopupDragHandleControlID="lblListSupp"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListSupp" runat="server" Visible="False" __designer:wfdid="w20"></asp:Button>
</contenttemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="upPopUpMsg" runat="server">
                                <ContentTemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False" __designer:wfdid="w2"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True" __designer:wfdid="w3"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" __designer:wfdid="w4" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red" __designer:wfdid="w5"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" onclick="imbOKPopUpMsg_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle" __designer:wfdid="w6"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" __designer:wfdid="w7" TargetControlID="bePopUpMsg" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False" __designer:wfdid="w8"></asp:Button>
</contenttemplate></asp:UpdatePanel>
  </asp:Content>