<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnConAP.aspx.vb" Inherits="Accounting_trnConAP" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Maroon" Text=".: A/P Initial Balance"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 15px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" 
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                                        <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                            <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" __designer:wfdid="w20" DefaultButton="btnSearch"><TABLE><TBODY><TR><TD align=left><asp:Label id="Label1" runat="server" Text="Filter :" __designer:wfdid="w21"></asp:Label></TD><TD align=left colSpan=2><asp:DropDownList id="ddlFilter" runat="server" __designer:wfdid="w22" CssClass="inpText" OnSelectedIndexChanged="ddlFilter_SelectedIndexChanged" AutoPostBack="True"><asp:ListItem Value="s.suppname">Supplier</asp:ListItem>
<asp:ListItem Value="bm.trnbelino">Invoice No</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterConap" runat="server" Width="123px" __designer:wfdid="w23" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="imgFind" onclick="imgFind_Click" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w24"></asp:ImageButton> <asp:ImageButton id="imgViewAll" onclick="imgViewAll_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w25"></asp:ImageButton></TD><TD align=left></TD></TR><TR><TD align=left><asp:Label id="Label15" runat="server" Text="Status :" __designer:wfdid="w26"></asp:Label>&nbsp;</TD><TD style="WIDTH: 78px" align=left><asp:DropDownList id="FilterDDLStatus" runat="server" Width="100px" __designer:wfdid="w27" CssClass="inpText"><asp:ListItem>All</asp:ListItem>
<asp:ListItem Value="IN PROCESS">In Process</asp:ListItem>
<asp:ListItem Value="POST">Post</asp:ListItem>
</asp:DropDownList></TD><TD style="WIDTH: 265px" align=left>&nbsp; </TD><TD align=left></TD></TR><TR><TD style="HEIGHT: 10px" align=left colSpan=4><asp:ImageButton id="ibselectall" onclick="ibselectall_Click" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" __designer:wfdid="w28" Visible="False" AlternateText="Delete"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibuncheckall" onclick="ibuncheckall_Click" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" __designer:wfdid="w29" Visible="False" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton id="btnPost" onclick="btnPost_Click" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w30" Visible="False" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton id="btnExcel" onclick="btnExcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w31" Visible="False"></asp:ImageButton> <asp:Label id="multiplepost" runat="server" Text="false" Width="48px" __designer:wfdid="w32" Visible="False"></asp:Label></TD></TR></TBODY></TABLE><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD align=left><asp:GridView id="gvTrnConap" runat="server" Width="100%" __designer:wfdid="w33" OnSelectedIndexChanged="gvTrnConap_SelectedIndexChanged" GridLines="None" PageSize="8" AllowPaging="True" DataKeyNames="trnbelimstoid,cmpcode" CellPadding="4" AutoGenerateColumns="False" AllowSorting="True" OnPageIndexChanging="gvTrnConap_PageIndexChanging" OnRowDataBound="gvTrnConap_RowDataBound">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="trnbelino" HeaderText="Invoice No">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="outlet" HeaderText="outlet" Visible="False"></asp:BoundField>
<asp:BoundField DataField="typebeli" HeaderText="Pay Type" Visible="False"></asp:BoundField>
<asp:BoundField DataField="suppName" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelidate" HeaderText="Invoice Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amttrans" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelimststatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:CheckBox id="cbGVMst" runat="server" __designer:wfdid="w378" Checked='<%# eval("checkvalue") %>'></asp:CheckBox> <asp:Label id="lblOidGVMst" runat="server" Text='<%# eval("trnbelimstoid") %>' __designer:wfdid="w379" Visible="False"></asp:Label>
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="No Data Found !!" Width="104px" __designer:wfdid="w27"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w34"></asp:Label> <asp:Button id="btnSearch" onclick="btnSearch_Click" runat="server" Font-Bold="True" Text="FIND" Width="75px" __designer:wfdid="w35" Visible="False" CssClass="orange"></asp:Button> <asp:Button id="btnAll" onclick="btnAll_Click" runat="server" Font-Bold="True" Text="VIEW ALL" Width="75px" __designer:wfdid="w36" Visible="False" CssClass="gray"></asp:Button></TD></TR></TBODY></TABLE>&nbsp; </asp:Panel> 
</contenttemplate>
                                            <triggers>
<asp:PostBackTrigger ControlID="btnExcel"></asp:PostBackTrigger>
</triggers>
                                        </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/List.gif" />&nbsp; <strong><span style="font-size: 9pt">
                                List Of A/P Initial Balance :.</span></strong>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<DIV style="WIDTH: 100%"><TABLE width="100%"><TBODY><TR><TD style="WIDTH: 43px" align=left><asp:Label id="RateOid" runat="server" __designer:wfdid="w140"></asp:Label><asp:Label id="Rate2Oid" runat="server" __designer:wfdid="w141"></asp:Label><asp:Label id="poratetoidr" runat="server" __designer:wfdid="w142" Visible="False"></asp:Label><asp:Label id="poratetousd" runat="server" __designer:wfdid="w143"></asp:Label><asp:Label id="porate2toidr" runat="server" __designer:wfdid="w144"></asp:Label><asp:Label id="porate2tousd" runat="server" __designer:wfdid="w145"></asp:Label></TD><TD align=left><asp:TextBox id="conapoid" runat="server" Width="10px" __designer:wfdid="w146" CssClass="inpText" Visible="False" MaxLength="10" Enabled="False"></asp:TextBox> <asp:TextBox id="trnbelimstoid" runat="server" Width="10px" __designer:wfdid="w147" CssClass="inpText" Visible="False" MaxLength="10" Enabled="False"></asp:TextBox> <asp:TextBox id="paymentoid" runat="server" Width="10px" __designer:wfdid="w148" CssClass="inpText" Visible="False" MaxLength="10" Enabled="False"></asp:TextBox> <asp:TextBox id="cashbankoid" runat="server" Width="10px" __designer:wfdid="w149" CssClass="inpText" Visible="False" MaxLength="10" Enabled="False"></asp:TextBox> <asp:TextBox id="trnglmstoid" runat="server" Width="10px" __designer:wfdid="w150" CssClass="inpText" Visible="False" MaxLength="10" Enabled="False"></asp:TextBox> <asp:TextBox id="trngldtloid" runat="server" Width="10px" __designer:wfdid="w151" CssClass="inpText" Visible="False" MaxLength="10" Enabled="False"></asp:TextBox>&nbsp;&nbsp;&nbsp; </TD><TD style="WIDTH: 107px" align=left>&nbsp;</TD><TD style="WIDTH: 310px" align=left><asp:DropDownList id="DDLType" runat="server" Width="112px" __designer:wfdid="w152" OnSelectedIndexChanged="DDLType_SelectedIndexChanged" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:DropDownList> <asp:TextBox id="acctgCode" runat="server" __designer:wfdid="w153" CssClass="inpTextDisabled" Visible="False" ReadOnly="True"></asp:TextBox> <asp:ImageButton id="btnSearcAcctg" onclick="btnSearcAcctg_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Width="16px" __designer:wfdid="w154" Visible="False" Height="16px"></asp:ImageButton> <asp:TextBox id="amtbayar" runat="server" Width="103px" __designer:wfdid="w155" AutoPostBack="True" CssClass="inpText" Visible="False" MaxLength="12" Enabled="true" OnTextChanged="amtbeli_TextChanged">0</asp:TextBox></TD><TD style="WIDTH: 369px" align=left><ajaxToolkit:CalendarExtender id="ceduedate" runat="server" __designer:wfdid="w156" TargetControlID="payduedate" Format="MM/dd/yyyy" PopupButtonID="imbduedate"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meepayduedate" runat="server" __designer:wfdid="w157" TargetControlID="payduedate" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD><TD style="WIDTH: 427px" align=left></TD></TR><TR><TD style="WIDTH: 43px" align=left></TD><TD align=left><asp:DropDownList id="DDLOutlet" runat="server" __designer:wfdid="w158" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:DropDownList></TD><TD style="WIDTH: 107px" align=left></TD><TD align=left colSpan=3></TD></TR><TR><TD style="WIDTH: 43px" align=left>Supplier&nbsp;<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="*" __designer:wfdid="w159"></asp:Label></TD><TD style="FONT-SIZE: 8pt; COLOR: #000099" align=left><asp:TextBox id="suppcode" runat="server" Width="150px" __designer:wfdid="w160" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnCariSupp" onclick="btnCariSupp_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Width="16px" __designer:wfdid="w161"></asp:ImageButton>&nbsp;<asp:ImageButton id="ImbEraseSupp" onclick="ImbEraseSupp_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Width="16px" __designer:wfdid="w162"></asp:ImageButton> <asp:Label id="suppoid" runat="server" Font-Size="X-Small" __designer:wfdid="w163" Visible="False"></asp:Label></TD><TD style="WIDTH: 107px" align=left><asp:Label id="Label20" runat="server" Text="A/P Account" Width="104px" __designer:wfdid="w164" ToolTip="VAR_AP"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 310px" align=left><asp:DropDownList id="DDLacctgCode" runat="server" __designer:wfdid="w165" CssClass="inpTextDisabled" Enabled="False"></asp:DropDownList></TD><TD style="WIDTH: 369px" align=left></TD><TD style="FONT-SIZE: 8pt; WIDTH: 427px" align=left></TD></TR><TR><TD style="WIDTH: 43px" align=left>Mata Uang</TD><TD style="FONT-SIZE: 8pt; COLOR: #000099" align=left><asp:DropDownList id="CurroidDDL" runat="server" __designer:wfdid="w166" CssClass="inpTextDisabled" Enabled="False" EnableTheming="True"></asp:DropDownList></TD><TD style="WIDTH: 107px" align=left>Invoice No <asp:Label id="Label18" runat="server" ForeColor="Red" Font-Size="X-Small" Text="*" __designer:wfdid="w167"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 310px" align=left><asp:TextBox id="trnbelino" runat="server" Width="150px" __designer:wfdid="w168" AutoPostBack="True" CssClass="inpText" MaxLength="20" Enabled="true"></asp:TextBox></TD><TD style="WIDTH: 369px" align=left><asp:Label id="Label3" runat="server" Text="Total Invoice (DPP)" Width="117px" __designer:wfdid="w169"></asp:Label> </TD><TD style="FONT-SIZE: 8pt; WIDTH: 427px" align=left><asp:TextBox id="dpp" runat="server" Width="110px" __designer:wfdid="w170" AutoPostBack="True" CssClass="inpText" MaxLength="10" Enabled="true" OnTextChanged="amtbeli_TextChanged">0</asp:TextBox> <asp:Label id="Label12" runat="server" ForeColor="Red" Font-Size="X-Small" Text="*" __designer:wfdid="w171"></asp:Label></TD></TR><TR><TD style="WIDTH: 43px" align=left><asp:Label id="Label22" runat="server" Text="Payment Term" Width="85px" __designer:wfdid="w172"></asp:Label></TD><TD style="FONT-SIZE: 8pt; COLOR: #000099" align=left><asp:DropDownList id="trnpaytype" runat="server" __designer:wfdid="w173" AutoPostBack="True" OnSelectedIndexChanged="trnpaytype_SelectedIndexChanged" CssClass="inpText"></asp:DropDownList></TD><TD style="WIDTH: 107px" align=left><asp:Label id="Label4" runat="server" Text="Invoice Date" Width="83px" __designer:wfdid="w174"></asp:Label>&nbsp;<asp:Label id="Label2" runat="server" ForeColor="Red" Font-Size="X-Small" Text="*" __designer:wfdid="w175"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 310px" align=left><asp:TextBox id="invoicedate" runat="server" Width="70px" __designer:wfdid="w176" CssClass="inpTextDisabled"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w177" Visible="False"></asp:ImageButton>&nbsp;<asp:Label id="Label10" runat="server" Text="(MM/dd/yyyy)" __designer:wfdid="w178" CssClass="Important"></asp:Label></TD><TD style="WIDTH: 369px" align=left>&nbsp;<asp:Label id="Label24" runat="server" Text="Tax (%)" __designer:wfdid="w179"></asp:Label> <asp:TextBox id="trntaxpct" runat="server" Width="50px" __designer:wfdid="w180" AutoPostBack="True" CssClass="inpText" MaxLength="4" Enabled="true" OnTextChanged="trntaxpct_TextChanged">0</asp:TextBox></TD><TD style="FONT-SIZE: 8pt; WIDTH: 427px" align=left><asp:TextBox id="amttax" runat="server" Width="110px" __designer:wfdid="w181" CssClass="inpTextDisabled" MaxLength="12" Enabled="true" ReadOnly="True">0</asp:TextBox></TD></TR><TR><TD style="WIDTH: 43px; HEIGHT: 22px" align=left><asp:Label id="Label7" runat="server" Text="Note" Width="32px" __designer:wfdid="w182"></asp:Label></TD><TD style="FONT-SIZE: 8pt; COLOR: #000099; HEIGHT: 22px" align=left><asp:TextBox id="trnapnote" runat="server" Width="214px" __designer:wfdid="w183" CssClass="inpText" MaxLength="100" Enabled="true"></asp:TextBox></TD><TD style="WIDTH: 107px; HEIGHT: 22px" align=left><asp:Label id="Label5" runat="server" Text="Due Date" Width="68px" __designer:wfdid="w184"></asp:Label>&nbsp;<asp:Label id="Label6" runat="server" ForeColor="Red" Font-Size="X-Small" Text="*" __designer:wfdid="w185"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 310px; HEIGHT: 22px" align=left><asp:TextBox id="payduedate" runat="server" Width="70px" __designer:wfdid="w186" AutoPostBack="True" CssClass="inpTextDisabled" MaxLength="10" Enabled="False" OnTextChanged="payduedate_TextChanged" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="imbduedate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w187" Visible="False"></asp:ImageButton>&nbsp;<asp:Label id="Label11" runat="server" Text="(MM/dd/yyyy)" __designer:wfdid="w188" CssClass="Important"></asp:Label></TD><TD style="WIDTH: 369px; HEIGHT: 22px" align=left>&nbsp;<asp:Label id="Label23" runat="server" Text="Total Invoice(Nett)" Width="114px" __designer:wfdid="w189"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 427px; HEIGHT: 22px" align=left><asp:TextBox id="amtbeli" runat="server" Width="110px" __designer:wfdid="w190" CssClass="inpTextDisabled" MaxLength="12" Enabled="true" OnTextChanged="amtbeli_TextChanged">0</asp:TextBox></TD></TR><TR><TD style="WIDTH: 43px" align=left><asp:Label id="Label9" runat="server" Text="Status" Width="40px" __designer:wfdid="w191"></asp:Label></TD><TD style="FONT-SIZE: 8pt; COLOR: #000099" align=left><asp:Label id="lblPosting" runat="server" __designer:wfdid="w192" CssClass="Important"></asp:Label></TD><TD style="WIDTH: 107px" align=left><asp:Label id="Label27" runat="server" Text="Total Payment" Width="88px" __designer:wfdid="w193" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 310px" align=left><asp:TextBox id="totalAP" runat="server" Width="123px" __designer:wfdid="w194" CssClass="inpTextDisabled" Visible="False" MaxLength="12" Enabled="true" ReadOnly="True" OnTextChanged="amtbeli_TextChanged">0</asp:TextBox></TD><TD style="WIDTH: 369px" align=left><asp:Label id="Label19" runat="server" Text="Amt Rate" Width="68px" __designer:wfdid="w195" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 427px" align=left><asp:TextBox id="rate" runat="server" Width="103px" __designer:wfdid="w196" AutoPostBack="True" CssClass="inpText" Visible="False" MaxLength="12" Enabled="true" OnTextChanged="amtbeli_TextChanged">0</asp:TextBox> <asp:Label id="Label14" runat="server" ForeColor="Red" Text="(*)" __designer:wfdid="w197" Visible="False"></asp:Label></TD></TR><TR><TD id="TD3" align=left colSpan=6 runat="server"><TABLE><TBODY><TR><TD style="FONT-SIZE: x-small; COLOR: #585858" align=left>Create Time On <asp:Label id="CrtTime" runat="server" ForeColor="#585858" Font-Size="X-Small" Font-Bold="True" Text='<%# Eval("UPDTIME", "{0:G}") %>' __designer:wfdid="w198"></asp:Label>&nbsp;by <asp:Label id="CrtUser" runat="server" ForeColor="#585858" Font-Size="X-Small" Font-Bold="True" Text='<%# Eval("UPDUSER", "{0}") %>' __designer:wfdid="w199"></asp:Label><BR />Last Update On <asp:Label id="UpdTime" runat="server" ForeColor="#585858" Font-Size="X-Small" Font-Bold="True" Text='<%# Eval("UPDTIME", "{0:G}") %>' __designer:wfdid="w200"></asp:Label> by <asp:Label id="UpdUser" runat="server" ForeColor="#585858" Font-Size="X-Small" Font-Bold="True" Text='<%# Eval("UPDUSER", "{0}") %>' __designer:wfdid="w201"></asp:Label></TD><TD style="FONT-SIZE: x-small; COLOR: #585858" align=left></TD><TD style="FONT-SIZE: x-small; COLOR: #585858" align=left rowSpan=2></TD></TR><TR><TD align=left>&nbsp;&nbsp;&nbsp;&nbsp;<BR /><asp:ImageButton id="btnSave1" onclick="btnSave1_Click" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w202" AlternateText="Save"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancel1" onclick="btnCancel1_Click1" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w203" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete1" onclick="btnDelete1_Click1" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w204" AlternateText="Delete"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPost1" onclick="btnPost1_Click" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w205"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibRePost" runat="server" ImageUrl="~/Images/repost.png" ImageAlign="AbsMiddle" __designer:wfdid="w206" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnShowCOA" onclick="btnshowCOA_Click" runat="server" ImageUrl="~/Images/showCOA.png" __designer:wfdid="w207" Visible="False"></asp:ImageButton></TD><TD align=left><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w208" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<asp:Image id="Image1" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w209"></asp:Image> Please Wait ..... 
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE>&nbsp;&nbsp;&nbsp;&nbsp; </TD></TR><TR><TD style="WIDTH: 43px" id="TD13" class="Label" align=left runat="server" visible="false"></TD><TD style="FONT-SIZE: 8pt; COLOR: #000099" id="TD11" align=left runat="server" visible="false"><asp:DropDownList id="curroid" runat="server" Width="180px" __designer:wfdid="w210" AutoPostBack="True" OnSelectedIndexChanged="curroid_SelectedIndexChanged" CssClass="inpText" Visible="False" Enabled="False"></asp:DropDownList></TD><TD style="WIDTH: 107px" id="TD9" class="Label" align=left runat="server" visible="false"></TD><TD style="FONT-SIZE: 8pt; WIDTH: 310px" id="TD8" align=left runat="server" visible="false"><asp:TextBox id="currate" runat="server" Width="100px" __designer:wfdid="w211" CssClass="inpTextDisabled" Visible="False" MaxLength="30" Enabled="False">1</asp:TextBox></TD><TD style="WIDTH: 369px" id="TD10" align=left runat="server" visible="false"><asp:Label id="statusRePost" runat="server" Text="POST" __designer:wfdid="w212" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 427px" id="TD12" align=left runat="server" visible="false"></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 43px" align=left><asp:Label id="Label26" runat="server" Text="A/P Payment Account" Width="143px" __designer:wfdid="w213" Visible="False"></asp:Label> </TD><TD align=left><asp:TextBox id="payacctg" runat="server" Width="250px" __designer:wfdid="w214" CssClass="inpText" Visible="False"></asp:TextBox> <asp:ImageButton id="imbPayAcctg" onclick="imbPayAcctg_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Width="16px" __designer:wfdid="w215" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="ImbErasePayAcc" onclick="ImbErasePayAcc_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Width="16px" __designer:wfdid="w216" Visible="False"></asp:ImageButton></TD><TD style="WIDTH: 107px" align=left><asp:Label id="Label16" runat="server" Text="Payment Date" Width="94px" __designer:wfdid="w217" Visible="False"></asp:Label></TD><TD style="WIDTH: 310px" align=left><asp:TextBox id="paymentdate" runat="server" Width="100px" __designer:wfdid="w218" CssClass="inpText" Visible="False" MaxLength="10" Enabled="true"></asp:TextBox> <asp:ImageButton id="imbPaymentDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w219" Visible="False"></asp:ImageButton> <ajaxToolkit:MaskedEditExtender id="mee6" runat="server" __designer:wfdid="w220" TargetControlID="paymentdate" MaskType="Date" Mask="99/99/9999" UserDateFormat="DayMonthYear" CultureName="id-ID" ClearMaskOnLostFocus="true"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="ce3" runat="server" __designer:wfdid="w221" TargetControlID="paymentdate" Format="MM/dd/yyyy" PopupButtonID="imbPaymentDate"></ajaxToolkit:CalendarExtender></TD><TD style="WIDTH: 369px" align=left><asp:Label id="Label17" runat="server" Text="Payment No" Width="94px" __designer:wfdid="w222" Visible="False"></asp:Label></TD><TD style="WIDTH: 427px" align=left><asp:TextBox id="payrefno" runat="server" Width="150px" __designer:wfdid="w223" CssClass="inpText" Visible="False" MaxLength="15" Enabled="true"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 43px" align=left></TD><TD align=left colSpan=5><asp:GridView id="gvAcctg" runat="server" Width="100%" __designer:wfdid="w224" OnSelectedIndexChanged="gvAcctg_SelectedIndexChanged1" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="acctgoid,acctgcode,acctgdesc" AllowPaging="True" PageSize="8" GridLines="None">
<RowStyle CssClass="gvrow"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" HorizontalAlign="Center" Width="40px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="40px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="acctgoid" HeaderText="SuppOid" Visible="False">
<HeaderStyle CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgcode" HeaderText="Code">
<HeaderStyle CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Akun">
<HeaderStyle CssClass="gvhdr" Width="300px"></HeaderStyle>

<ItemStyle Width="300px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle CssClass="gvfooter" Font-Bold="True" HorizontalAlign="Right"></PagerStyle>
<EmptyDataTemplate>
<asp:Label ID="Label13" runat="server" ForeColor="Red" Text="No data found!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#124E81" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 43px; HEIGHT: 15px" align=left><asp:Label id="paymentacctgoid" runat="server" Font-Size="X-Small" __designer:wfdid="w225" Visible="False"></asp:Label></TD><TD style="HEIGHT: 15px" align=left><asp:Label id="acctgoid" runat="server" Font-Size="X-Small" __designer:wfdid="w226" Visible="False"></asp:Label></TD><TD style="WIDTH: 107px; HEIGHT: 15px" align=left><asp:Label id="postdate" runat="server" Font-Size="X-Small" Text="1/1/1900" __designer:wfdid="w227" Visible="False"></asp:Label></TD><TD style="WIDTH: 310px; HEIGHT: 15px" align=left><asp:TextBox id="amtbelinetto" runat="server" Width="50px" __designer:wfdid="w228" CssClass="inpTextDisabled" Visible="False" MaxLength="30" Enabled="true" ReadOnly="True">0.00</asp:TextBox></TD><TD style="WIDTH: 369px; HEIGHT: 15px" align=left><asp:Label id="purchAcctgoid" runat="server" Font-Size="X-Small" Width="99px" __designer:wfdid="w229" Visible="False"></asp:Label></TD><TD style="WIDTH: 427px; HEIGHT: 15px" align=left><asp:TextBox id="purchAcctg" runat="server" Width="50px" __designer:wfdid="w230" CssClass="inpTextDisabled" Visible="False" ReadOnly="True"></asp:TextBox> <asp:ImageButton id="imbPurch" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Width="16px" __designer:wfdid="w231" Visible="False" Height="16px"></asp:ImageButton></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="HEIGHT: 15px" align=left colSpan=3><asp:Button id="btnPosting" onclick="btnPosting_Click" runat="server" Font-Bold="True" __designer:dtid="844424930132000" Text="POST" Width="75px" __designer:wfdid="w232" CssClass="orange" Visible="False"></asp:Button> <asp:Button id="btnDelete" onclick="btnDelete_Click" runat="server" Font-Bold="True" __designer:dtid="844424930132000" Text="DELETE" Width="75px" __designer:wfdid="w233" CssClass="red" Visible="False"></asp:Button> <asp:Button id="btnCancel" onclick="btnCancel_Click" runat="server" Font-Bold="True" __designer:dtid="844424930131999" Text="CANCEL" Width="75px" __designer:wfdid="w234" CssClass="gray" Visible="False"></asp:Button> <asp:Button id="btnSave" runat="server" Font-Bold="True" __designer:dtid="844424930131998" Text="SAVE" Width="75px" __designer:wfdid="w235" CssClass="green" Visible="False"></asp:Button></TD><TD style="WIDTH: 310px; HEIGHT: 15px" align=left></TD><TD style="WIDTH: 369px; HEIGHT: 15px" align=left></TD><TD style="WIDTH: 427px; HEIGHT: 15px" align=left></TD></TR></TBODY></TABLE><ajaxToolkit:MaskedEditExtender id="mee3" runat="server" __designer:wfdid="w236" TargetControlID="invoicedate" MaskType="Date" Mask="99/99/9999" CultureName="en-US" ClearMaskOnLostFocus="true"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender1" runat="server" __designer:wfdid="w237" TargetControlID="dpp" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender2" runat="server" __designer:wfdid="w238" TargetControlID="trntaxpct" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender3" runat="server" __designer:wfdid="w239" TargetControlID="amtbayar" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender4" runat="server" __designer:wfdid="w240" TargetControlID="rate" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender>&nbsp;&nbsp; </DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/Form.gif" />&nbsp; <strong><span style="font-size: 9pt">
                                Form A/P Initial Balance :.</span></strong>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <table style="width: 100%">
        <tr>
            <td style="width: 100px">
            </td>
        </tr>
    </table>
    <asp:UpdatePanel id="upListSupp" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListSupp" runat="server" Width="650px" Visible="False" CssClass="modalBox"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSupp" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Supplier"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListSupp" runat="server" Width="100%" DefaultButton="btnFindListSupp">Filter : <asp:DropDownList id="FilterDDLListSupp" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="suppname">Supp Name</asp:ListItem>
<asp:ListItem Value="suppcode">Supp Code</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListSupp" runat="server" Width="150px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListSupp" onclick="btnFindListSupp_Click" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListSupp" onclick="btnAllListSupp_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvSupplier" runat="server" ForeColor="#333333" Width="98%" OnSelectedIndexChanged="gvSupplier_SelectedIndexChanged" GridLines="None" PageSize="5" AllowPaging="True" DataKeyNames="suppoid,suppcode,suppname,coa_hutang,supptype,coa" CellPadding="4" AutoGenerateColumns="False" OnPageIndexChanging="gvSupplier_PageIndexChanging" AllowSorting="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Code" SortExpression="suppcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Name" SortExpression="suppname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Width="40%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="40%"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListSupp" runat="server" OnClick="lkbCloseListSupp_Click">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSupp" runat="server" TargetControlID="btnHideListSupp" PopupControlID="pnlListSupp" BackgroundCssClass="modalBackground" Drag="True" PopupDragHandleControlID="lblListSupp"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListSupp" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="UpdatePanel6" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPosting2" runat="server" Width="760px" Visible="False" CssClass="modalBox" BorderWidth="2px" BorderStyle="Solid"><TABLE style="WIDTH: 560px"><TBODY><TR><TD style="HEIGHT: 25px" align=center colSpan=3><asp:Label id="lblPosting2" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting"></asp:Label></TD></TR><TR><TD colSpan=3><asp:GridView id="gvakun" runat="server" Width="750px" AutoGenerateColumns="False"><Columns>
<asp:BoundField DataField="acctgcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Account">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kredit" HeaderText="Credit">
<HeaderStyle HorizontalAlign="Right"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCancel2" onclick="lkbCancel2_Click" runat="server" Font-Size="Small" Font-Bold="True">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePosting2" runat="server" TargetControlID="btnHidePosting2" PopupControlID="pnlPosting2" BackgroundCssClass="modalBackground" Drag="True" PopupDragHandleControlID="lblPosting2"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting2" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" Visible="False" CssClass="modalMsgBox"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" onclick="imbOKPopUpMsg_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" PopupDragHandleControlID="lblCaption" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

