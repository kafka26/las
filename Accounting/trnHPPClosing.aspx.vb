'Prgrmr: 4n7JuK | LstUpdt: 130128
Imports System.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports ClassFunction

Partial Class Accounting_trnHPPClosing
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_CONN")
    Public DefCounter As String = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim conn As New SqlConnection(ConnStr)
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKoneksi As New Koneksi
    Dim cfunction As New ClassFunction
    Dim cProc As New ClassProcedure
    Public sql_temp As String
#End Region

#Region "Procedures"
    Private Sub InitDDL()
        ' BULAN
        For C1 As Integer = 1 To 12
            Dim liMonth As New ListItem(MonthName(C1), C1)
            DDLMonth.Items.Add(liMonth)
        Next

        ' TAHUN
        For C1 As Integer = 2009 To GetServerTime.Year + 1
            Dim liYear As New ListItem(C1, C1)
            DDLYear.Items.Add(liYear)
        Next
    End Sub

    Private Sub SetControlPosting(ByVal iState As Integer)
        ' 0 = No Posting krn tidak ada data
        ' 1 = No Posting krn sudah pernah Posting
        ' 2 = OK To Posting
        Select Case iState
            Case 0
                btnSave.Visible = False
                ' imbUnPost.Visible = False
                lblPostingState.Visible = False
            Case 1
                btnSave.Visible = False
                ' imbUnPost.Visible = True
                lblPostingState.Visible = True
            Case 2
                btnSave.Visible = True
                ' imbUnPost.Visible = False
                lblPostingState.Visible = False
        End Select
    End Sub

    Private Sub AddNewDetailDailyPost(ByVal tbDailyPost As DataTable, ByVal iSeqRecord As Integer, ByVal dPostDate As Date, ByVal iAcctgOid As Integer, _
    ByVal sJenis As String, ByVal sNote As String, ByVal dDebetValue As Double, ByVal dCreditValue As Double)
        Dim NuRow As DataRow = tbDailyPost.NewRow
        NuRow("no") = iSeqRecord
        NuRow("postdate") = dPostDate
        NuRow("acctgoid") = iAcctgOid
        NuRow("acctgcode") = cKoneksi.ambilscalar("SELECT acctgcode FROM QL_mstacctg WHERE cmpcode='" & Session("CompnyCode") & "' AND acctgoid=" & iAcctgOid & " ")
        NuRow("acctgdesc") = cKoneksi.ambilscalar("SELECT acctgdesc FROM QL_mstacctg WHERE cmpcode='" & Session("CompnyCode") & "' AND acctgoid=" & iAcctgOid & " ")
        NuRow("jenis") = sJenis
        NuRow("keterangan") = "HPP POSTING " & Format(dPostDate, "MM/dd/yyyy") & " (" & sNote & ")"
        NuRow("debet") = dDebetValue
        NuRow("credit") = dCreditValue
        tbDailyPost.Rows.Add(NuRow)
    End Sub

    Private Sub binddata(ByVal tgl As String)
        lblError.Text = ""
        Try
            sSql = "/*" & _
                "1=JUAL ; 2=USAGE ; 3=DIRECT LABOR ; 4=OVERHEAD ; 5=SALES AND ADM" & _
                "*/" & _
                "DECLARE @cmpcode VARCHAR(10);" & _
                "DECLARE @date DATETIME;" & _
                "DECLARE @periodacctgb4 VARCHAR(10);" & _
                "SET @cmpcode = '" & Session("CompnyCode") & "';" & _
                "SET @date = '" & CDate(tgl) & "';" & _
                "SET @periodacctgb4 = '" & GetDateToPeriodAcctg(DateAdd(DateInterval.Month, -1, CDate(tgl))) & "';" & _
                "SELECT jm.cmpcode,'1' AS refno,'SALES' AS reftype,jd.itemoid AS refoid,i.itemcode AS refcode,i.itemlongdesc AS refdesc," & _
                "jd.trnjualdtlqty AS qty,jd.trnjualdtlprice AS price,(jd.dtldiscamt*jd.trnjualdtlqty) + " & _
                "((jd.trnjualdtlnetto/(SELECT SUM(d.trnjualdtlnetto) FROM QL_trnjualdtl d WHERE d.cmpcode=jd.cmpcode " & _
                "AND d.trnjualmstoid=jd.trnjualmstoid))*jm.trnjualdiscamt) AS disc," & _
                "'subtotal'=(jd.trnjualdtlqty*jd.trnjualdtlprice)-(jd.dtldiscamt*jd.trnjualdtlqty)-" & _
                "((jd.trnjualdtlnetto/(SELECT SUM(d.trnjualdtlnetto) FROM QL_trnjualdtl d WHERE d.cmpcode=jd.cmpcode " & _
                "AND d.trnjualmstoid=jd.trnjualmstoid))*jm.trnjualdiscamt) " & _
                "FROM QL_trnjualmst jm INNER JOIN QL_trnjualdtl jd ON jd.cmpcode=jm.cmpcode AND jd.trnjualmstoid=jm.trnjualmstoid " & _
                "INNER JOIN QL_mstitem i ON i.cmpcode=jd.cmpcode AND i.itemoid=jd.itemoid " & _
                "WHERE jm.cmpcode=@cmpcode AND jm.trnjualmstoid>0 AND jm.trnjualdate=@date " & _
                "AND ((jm.trnjualstatus='POSTING' AND jm.trnjualreftype='NORMAL') OR (jm.trnjualstatus='POSTING' AND jm.trnjualreftype='CORRECTION')) " & _
                "UNION ALL " & _
                "SELECT pm.cmpcode,'2' AS refno,'USAGE' AS reftype,pd.matoid AS refoid,m.matcode AS refcode,m.matlongdesc AS refdesc," & _
                "pd.matqty AS qty,ISNULL(c.avgprice,0.0) AS price,0.0 AS disc,pd.matqty*ISNULL(c.avgprice,0.0) AS subtotal " & _
                "FROM QL_trnprodresultmst pm INNER JOIN QL_trnprodresultdtlmat pd ON pm.cmpcode=pd.cmpcode AND pm.prodresultmstoid=pd.prodresultmstoid " & _
                "INNER JOIN QL_mstmat m ON m.cmpcode=pd.cmpcode AND m.matoid=pd.matoid " & _
                "LEFT JOIN QL_crdstock c ON c.cmpcode=m.cmpcode AND c.refname='MATERIAL' AND c.refoid=m.matoid AND c.periodacctg=@periodacctgb4 " & _
                "WHERE pm.cmpcode=@cmpcode AND pm.prodresultdate=@date " & _
                "AND ((pm.prodresultstatus='POSTING' AND pm.prodresultreftype='NORMAL') OR (pm.prodresultstatus='POSTING' AND pm.prodresultreftype='CORRECTION')) " & _
                "UNION ALL " & _
                "SELECT a.cmpcode,'3' AS refno,'DIRECT LABOR COST' AS reftype,a.acctgoid AS refoid,a.acctgcode AS refcode,a.acctgdesc AS refdesc," & _
                "0.0 AS qty,0.0 AS price,0.0 AS disc,CASE WHEN ISNULL(c.effectivedays,0.0)=0 THEN 0.0 " & _
                "ELSE (ISNULL(c.amtbalance,0.0)/ISNULL(c.effectivedays,0.0)) END AS subtotal " & _
                "FROM QL_mstacctg a LEFT JOIN QL_crdgl c ON c.cmpcode=a.cmpcode AND c.acctgoid=a.acctgoid AND c.periodacctg=@periodacctgb4 AND c.crdglflag='CLOSE' " & _
                "WHERE a.cmpcode=@cmpcode AND a.acctgflag='A' AND a.tipeflag='Y' AND a.acctgoid NOT IN (SELECT DISTINCT a1.acctggrp3 FROM QL_mstacctg a1 " & _
                "WHERE a1.cmpcode=a.cmpcode AND a1.acctggrp3<>'') AND a.acctgcode LIKE (SELECT it.interfacevalue+'%' FROM QL_mstinterface it " & _
                "WHERE it.cmpcode=a.cmpcode AND it.interfacevar='VAR_DL_COST') " & _
                "UNION ALL " & _
                "SELECT a.cmpcode,'4' AS refno,'OVERHEAD COST' AS reftype,a.acctgoid AS refoid,a.acctgcode AS refcode,a.acctgdesc AS refdesc," & _
                "0.0 AS qty,0.0 AS price,0.0 AS disc,CASE WHEN ISNULL(c.effectivedays,0.0)=0 THEN 0.0 " & _
                "ELSE (ISNULL(c.amtbalance,0.0)/ISNULL(c.effectivedays,0.0)) END AS subtotal " & _
                "FROM QL_mstacctg a LEFT JOIN QL_crdgl c ON c.cmpcode=a.cmpcode AND c.acctgoid=a.acctgoid AND c.periodacctg=@periodacctgb4 AND c.crdglflag='CLOSE' " & _
                "WHERE a.cmpcode=@cmpcode AND a.acctgflag='A' AND a.tipeflag='Y' AND a.acctgoid NOT IN (SELECT DISTINCT a1.acctggrp3 FROM QL_mstacctg a1 " & _
                "WHERE a1.cmpcode=a.cmpcode AND a1.acctggrp3<>'') AND a.acctgcode LIKE (SELECT it.interfacevalue+'%' FROM QL_mstinterface it " & _
                "WHERE it.cmpcode=a.cmpcode AND it.interfacevar='VAR_OHD_COST') " & _
                "UNION ALL " & _
                "SELECT a.cmpcode,'5' AS refno,'SALES AND ADM COST' AS reftype,a.acctgoid AS refoid,a.acctgcode AS refcode,a.acctgdesc AS refdesc," & _
                "0.0 AS qty,0.0 AS price,0.0 AS disc,CASE WHEN ISNULL(c.effectivedays,0.0)=0 THEN 0.0 " & _
                "ELSE (ISNULL(c.amtbalance,0.0)/ISNULL(c.effectivedays,0.0)) END AS subtotal " & _
                "FROM QL_mstacctg a LEFT JOIN QL_crdgl c ON c.cmpcode=a.cmpcode AND c.acctgoid=a.acctgoid AND c.periodacctg=@periodacctgb4 AND c.crdglflag='CLOSE' " & _
                "WHERE a.cmpcode=@cmpcode AND a.acctgflag='A' AND a.acctgoid NOT IN (SELECT DISTINCT a1.acctggrp3 FROM QL_mstacctg a1 " & _
                "WHERE a1.cmpcode=a.cmpcode AND a1.acctggrp3<>'') AND a.acctgcode LIKE (SELECT it.interfacevalue+'%' FROM QL_mstinterface it " & _
                "WHERE it.cmpcode=a.cmpcode AND it.interfacevar='VAR_SALES_COST') " & _
                "UNION ALL " & _
                "SELECT a.cmpcode,'5' AS refno,'SALES AND ADM COST' AS reftype,a.acctgoid AS refoid,a.acctgcode AS refcode,a.acctgdesc AS refdesc," & _
                "0.0 AS qty,0.0 AS price,0.0 AS disc,CASE WHEN ISNULL(c.effectivedays,0.0)=0 THEN 0.0 " & _
                "ELSE (ISNULL(c.amtbalance,0.0)/ISNULL(c.effectivedays,0.0)) END AS subtotal " & _
                "FROM QL_mstacctg a LEFT JOIN QL_crdgl c ON c.cmpcode=a.cmpcode AND c.acctgoid=a.acctgoid AND c.periodacctg=@periodacctgb4 AND c.crdglflag='CLOSE' " & _
                "WHERE a.cmpcode=@cmpcode AND a.acctgflag='A' AND a.acctgoid NOT IN (SELECT DISTINCT a1.acctggrp3 FROM QL_mstacctg a1 " & _
                "WHERE a1.cmpcode=a.cmpcode AND a1.acctggrp3<>'') AND a.acctgcode LIKE (SELECT it.interfacevalue+'%' FROM QL_mstinterface it " & _
                "WHERE it.cmpcode=a.cmpcode AND it.interfacevar='VAR_ADM_COST') ORDER BY refno,refcode"
            Dim tbDetailPost As DataTable : tbDetailPost = cKoneksi.ambiltabel(sSql, "QL_crddailyclosing")

            gvMst.DataSource = tbDetailPost
            gvMst.DataBind()

            Session("TbDtlPost") = tbDetailPost
        Catch ex As Exception
            lblError.Text = ex.ToString
        End Try
    End Sub

#End Region

#Region "Functions"
    Private Function SetTableDailyPost() As DataTable
        Dim NuTable As New DataTable("Tb_DailyPost")
        NuTable.Columns.Add("no", Type.GetType("System.Int32"))
        NuTable.Columns.Add("postdate", Type.GetType("System.DateTime"))
        NuTable.Columns.Add("acctgoid", Type.GetType("System.Int32"))
        NuTable.Columns.Add("acctgcode", Type.GetType("System.String"))
        NuTable.Columns.Add("acctgdesc", Type.GetType("System.String"))
        NuTable.Columns.Add("jenis", Type.GetType("System.String"))
        NuTable.Columns.Add("keterangan", Type.GetType("System.String"))
        NuTable.Columns.Add("debet", Type.GetType("System.Decimal"))
        NuTable.Columns.Add("credit", Type.GetType("System.Decimal"))
        Return NuTable
    End Function
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Accounting\trnhppclosing.aspx")
        End If
        If checkPagePermission("~\Accounting\trnhppclosing.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - HPP Closing"
        Me.btnSave.Attributes.Add("OnClick", "javascript:return confirm('Are you sure you want to SAVE this HPP Closing data ?');")
        imbOKUnPOST.Attributes.Add("OnClick", "javascript:return confirm('Are you sure you want to UNPOST this HPP Closing data ?');")

        If Not IsPostBack Then
            FillDDL(DDLUser, "SELECT p.profoid,p.profname " & _
                "FROM QL_mstprof p WHERE p.cmpcode='" & Session("CompnyCode") & "' AND p.activeflag='ACTIVE' ORDER BY p.profname")
            InitDDL()

            ' Get Last Closing
            Dim sLastClose As String = ""
            Dim sErr As String = ""
            sSql = "SELECT MAX(historyclosingdate) FROM QL_acctgclosinghistory WHERE cmpcode='" & Session("CompnyCode") & "' AND closinggroup='HPP' "
            sLastClose = cKoneksi.ambilscalar(sSql).ToString

            If Not IsDate(sLastClose) Then
                lastclosing.Text = "-"

                sSql = "SELECT genother1 FROM QL_mstgen WHERE cmpcode='" & Session("CompnyCode") & "' AND gengroup='CUTOFDATE' "
                sLastClose = cKoneksi.ambilscalar(sSql).ToString
                If Not IsValidDate(sLastClose, "MM/dd/yyyy", sErr) Then
                    nextclosing.Text = "-"
                    lblError.Text = "- Invalid Cut Off Date format: " & sErr & "!<BR>"
                    Exit Sub
                Else
                    nextclosing.Text = Format(CDate(sLastClose), "MMMM yyyy")
                    DDLMonth.SelectedValue = CDate(sLastClose).Month
                    DDLYear.SelectedValue = CDate(sLastClose).Year
                    txtDate.Text = Format(New Date(DDLYear.SelectedValue, DDLMonth.SelectedValue, Date.DaysInMonth(DDLYear.SelectedValue, DDLMonth.SelectedValue)), "MM/dd/yyyy")
                End If
            Else
                lastclosing.Text = Format(CDate(sLastClose), "MMMM yyyy")
                nextclosing.Text = Format(DateAdd(DateInterval.Month, 1, CDate(sLastClose)), "MMMM yyyy")
                Dim tempDate As Date = Format(DateAdd(DateInterval.Month, 1, CDate(sLastClose)), "MM/dd/yyyy")
                DDLMonth.SelectedValue = tempDate.Month
                DDLYear.SelectedValue = tempDate.Year
                txtDate.Text = Format(New Date(DDLYear.SelectedValue, DDLMonth.SelectedValue, Date.DaysInMonth(DDLYear.SelectedValue, DDLMonth.SelectedValue)), "MM/dd/yyyy")
            End If
            SetControlPosting(0)
        End If
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click
        lblError.Text = "" : lbPurch.Visible = False : lbProd.Visible = False : lbhpp.Visible = False
        lbNewAvg.Visible = False : lblCOGS.Visible = False : lblJournal.Visible = False

        Try
            ' CALCULATE PURCHASING
            sSql = "DECLARE @periodacctg VARCHAR(10);" & _
                "DECLARE @cmpcode VARCHAR(10);" & _
                "DECLARE @bulan INT;" & _
                "DECLARE @tahun INT;" & _
                "SET @periodacctg='" & GetDateToPeriodAcctg(CDate(txtDate.Text)) & "';" & _
                "SET @cmpcode='" & Session("CompnyCode") & "';" & _
                "SET @bulan=" & DDLMonth.SelectedValue & ";" & _
                "SET @tahun=" & DDLYear.SelectedValue & "; "
            sSql &= "SELECT bl.cmpcode,bl.matoid,bl.matcode,bl.matlongdesc,bl.matunit,SUM(trnbelidtlqty) AS totalqty,SUM(nett) AS totalprice, SUM(returqty) AS returqty,SUM(returamount) AS totalretur, ISNULL(s.saldoawal,0.0) AS awalqty,ISNULL(s.amtawal_idr,0.0) AS awalprice, ISNULL(s.amtawal_idr*saldoawal,0.0) AS awaltotal,ISNULL(s.saldoakhir,0.0) AS akhirqty, akhirprice=(CASE WHEN (ISNULL(s.saldoawal,0.0)+SUM(trnbelidtlqty)- SUM(returqty))=0 THEN 0.0 ELSE (ISNULL(s.amtawal_idr*saldoawal,0.0)+SUM(nett)-SUM(returamount))/(ISNULL(s.saldoawal,0.0)+SUM(trnbelidtlqty)-SUM(returqty)) END) FROM ( "
            sSql &= "SELECT m.cmpcode,m.itemoid AS matoid,m.itemCode AS matcode,m.itemLongDescription AS matlongdesc,m.itemUnit1 AS matunit,0.0 trnbelidtlqty,0.0 trnbelidtlprice,0.0 nett, 0.0 AS returqty,0.0 AS returprice,0.0 AS returamount FROM QL_mstitem m WHERE m.cmpcode=@cmpcode AND m.itemRecordStatus='ACTIVE' UNION ALL  "
            sSql &= "SELECT bm.cmpcode,m.itemoid AS matoid,m.itemcode AS matcode,m.itemLongDescription AS matlongdesc,m.itemUnit1 AS matunit, mrqty_unitkecil AS trnbelidtlqty,bd.mrvalue AS trnbelidtlprice, mrdtlamt AS nett,0.0 AS returqty,0.0 AS returprice,0.0 AS returamount FROM QL_trnmrmst bm INNER JOIN QL_trnmrdtl bd ON bd.cmpcode=bm.cmpcode AND bd.mrmstoid=bm.mrmstoid INNER JOIN QL_mstitem m ON m.cmpcode=bd.cmpcode AND m.itemoid=bd.matoid WHERE bm.cmpcode=@cmpcode AND MONTH(bm.mrdate)=@bulan AND YEAR(bm.mrdate)=@tahun AND bm.mrmststatus IN ('POST', 'CLOSED') UNION ALL "
            sSql &= "SELECT rm.cmpcode,m.itemoid AS matoid,m.itemCode AS matcode,m.itemLongDescription AS matlongdesc,m.itemUnit1 AS matunit,0.0 trnbelidtlqty,0.0 trnbelidtlprice,0.0 AS nett, rd.retqty_unitkecil AS returqty,rd.retvalue AS returprice, retdtlamt AS returamount FROM QL_trnreturmst rm INNER JOIN QL_trnreturdtl rd ON rd.cmpcode=rm.cmpcode AND rd.retmstoid=rm.retmstoid INNER JOIN QL_mstitem m ON m.cmpcode=rd.cmpcode AND m.itemoid=rd.matoid WHERE rm.cmpcode=@cmpcode AND MONTH(rm.retdate)=@bulan AND YEAR(rm.retdate)=@tahun "
            sSql &= ") AS bl "
            sSql &= "LEFT JOIN QL_crdstock s ON s.cmpcode=bl.cmpcode AND s.refoid=bl.matoid AND s.periodacctg=@periodacctg GROUP BY bl.cmpcode,bl.matoid,bl.matcode,bl.matlongdesc,bl.matunit,s.saldoawal,s.amtawal_idr,s.saldoakhir "
            Dim dtPurch As DataTable = cKoneksi.ambiltabel(sSql, "PURCHASE")
            gvPurchase.DataSource = dtPurch
            gvPurchase.DataBind()
            lbPurch.Visible = True
            lbPurch.Text = "<U>Total Purchasing :</U> " & ToMaskEdit(ToDouble(dtPurch.Compute("SUM(totalprice)", "").ToString), 2)

            Dim dvPurch As DataView : dvPurch = dtPurch.DefaultView
            ' Versi 2.0, Memperhitungkan Barang Jadi yg tidak ada transaksi Production Result
            sSql = "DECLARE @periodacctg VARCHAR(10);" & _
                "DECLARE @cmpcode VARCHAR(10);" & _
                "DECLARE @bulan INT;" & _
                "DECLARE @tahun INT;" & _
                "SET @periodacctg='" & GetDateToPeriodAcctg(CDate(txtDate.Text)) & "';" & _
                "SET @cmpcode='" & Session("CompnyCode") & "';" & _
                "SET @bulan=" & DDLMonth.SelectedValue & ";" & _
                "SET @tahun=" & DDLYear.SelectedValue & ";" & _
                "SELECT u.cmpcode,u.itemoid,u.itemcode,u.itemlongdesc,u.itemunit,u.refqty,u.refhpp,u.reftotal,u.saldoakhir, " & _
                "ISNULL((SELECT SUM(r.prodresqty) FROM QL_trnprodresdtl r  " & _
                "INNER JOIN QL_trnprodresmst rm ON r.cmpcode=rm.cmpcode AND r.prodresmstoid=rm.prodresmstoid  " & _
                "AND MONTH(rm.prodresdate)=@bulan AND YEAR(rm.prodresdate)=@tahun  " & _
                "AND rm.prodresmststatus IN ('POST', 'CLOSED')  " & _
                "WHERE u.cmpcode=r.cmpcode AND u.itemoid=r.itemoid),0.0) AS qtyitem, " & _
                "u.matoid,u.matcode,u.matlongdesc,SUM(u.matqty) AS qtyusage,0.0 AS totalusage  " & _
                "FROM ( " & _
                "SELECT i.cmpcode,i.itemoid,i.itemcode,i.itemLongDescription AS itemlongdesc,i.itemUnit1 AS itemunit,ISNULL(h.refqty,0.0) refqty,ISNULL(h.refhpp,0.0) refhpp, " & _
                "ISNULL(h.reftotal,0.0) reftotal,ISNULL(s.saldoakhir,0.0) saldoakhir,0 matoid,'' matcode,'' matlongdesc,0.0 matqty  " & _
                "FROM QL_mstitem i LEFT JOIN QL_crdhpp h ON h.cmpcode=i.cmpcode AND h.refoid=i.itemoid AND i.itemGroup IN ('RAW', 'FG') AND h.periodacctg=@periodacctg  " & _
                "LEFT JOIN QL_crdstock s ON s.cmpcode=i.cmpcode AND s.refoid=i.itemoid AND s.periodacctg=@periodacctg  " & _
                "WHERE i.cmpcode=@cmpcode AND i.itemoid NOT IN (SELECT ri.itemoid FROM QL_trnprodresmst rm INNER JOIN QL_trnprodresdtl ri  " & _
                "ON ri.cmpcode=rm.cmpcode AND ri.prodresmstoid=rm.prodresmstoid AND MONTH(rm.prodresdate)=@bulan AND YEAR(rm.prodresdate)=@tahun  " & _
                "AND rm.prodresmststatus IN ('POST', 'CLOSED')) " & _
                "UNION ALL " & _
                "SELECT rm.cmpcode,i.itemoid,i.itemcode,i.itemLongDescription AS itemlongdesc,i.itemUnit1 AS itemunit,ISNULL(h.refqty,0.0) refqty,ISNULL(h.refhpp,0.0) refhpp, " & _
                "ISNULL(h.reftotal,0.0) reftotal,ISNULL(s.saldoakhir,0.0) saldoakhir,m.itemoid AS matoid,m.itemCode AS matcode,m.itemLongDescription AS matlongdesc,ri.prodresqty AS matqty  " & _
                "FROM QL_trnprodresmst rm  " & _
                "INNER JOIN QL_trnprodresdtl ri ON ri.cmpcode=rm.cmpcode AND ri.prodresmstoid=rm.prodresmstoid  " & _
                "INNER JOIN QL_trnwodtl1 wod1 ON wod1.cmpcode=ri.cmpcode AND wod1.womstoid=ri.womstoid " & _
                "INNER JOIN QL_mstitem i ON i.cmpcode=ri.cmpcode AND i.itemoid=wod1.itemoid AND ri.prodrestype='FG' " & _
                "INNER JOIN QL_trnwodtl3 wod3 ON wod3.cmpcode=ri.cmpcode AND wod3.womstoid=ri.womstoid " & _
                "INNER JOIN QL_mstitem m ON m.cmpcode=ri.cmpcode AND m.itemoid=wod3.wodtl3refoid AND m.itemgroup<>'FG' " & _
                "LEFT JOIN QL_crdhpp h ON h.cmpcode=i.cmpcode AND h.refoid=i.itemoid AND h.periodacctg=@periodacctg  " & _
                "LEFT JOIN QL_crdstock s ON s.cmpcode=i.cmpcode AND s.refoid=i.itemoid AND s.periodacctg=@periodacctg  " & _
                "WHERE rm.cmpcode=@cmpcode AND MONTH(rm.prodresdate)=@bulan AND YEAR(rm.prodresdate)=@tahun  " & _
                "AND rm.prodresmststatus IN ('POST', 'CLOSED') " & _
                ") AS u  " & _
                "GROUP BY u.cmpcode,u.itemoid,u.itemcode,u.itemlongdesc,u.itemunit,u.refqty,u.refhpp,u.reftotal,u.saldoakhir,u.matoid,u.matcode,u.matlongdesc ORDER BY u.itemoid "
            Dim dtUsage As DataTable = cKoneksi.ambiltabel(sSql, "USAGE")

            ' APPLY AVERAGE TO USAGE (Rounding 2 digit from Avg Price Material)
            For R1 As Integer = 0 To dtUsage.Rows.Count - 1
                If ToDouble(dtUsage.Rows(R1)("matoid").ToString) <> 0 Then
                    dvPurch.RowFilter = "matoid=" & dtUsage.Rows(R1)("matoid").ToString
                    If dvPurch.Count > 0 Then
                        dtUsage.Rows(R1)("totalusage") = Math.Round(ToDouble(dvPurch(0)("akhirprice").ToString), 2, MidpointRounding.AwayFromZero) * ToDouble(dtUsage.Rows(R1)("qtyusage").ToString) '
                        dtUsage.AcceptChanges()
                    End If
                    dvPurch.RowFilter = ""
                End If
            Next

            gvUsage.DataSource = dtUsage
            gvUsage.DataBind()
            lbProd.Visible = True
            lbProd.Text = "<U>Total Production :</U> " & ToMaskEdit(ToDouble(dtUsage.Compute("SUM(totalusage)", "").ToString), 2)

            ' APPLY LABOR & OHD COST
            Dim sMsg As String = ""
            Dim sVarDL As String = "Y" 'GetVarInterface("VAR_DL_COST", Session("CompnyCode"))
            Dim dDLCost As Decimal = 0
            If sVarDL <> "" Then
                sSql = "DECLARE @cmpcode VARCHAR(10);" & _
                    "DECLARE @bulan INT;" & _
                    "DECLARE @tahun INT;" & _
                    "SET @cmpcode='" & Session("CompnyCode") & "';" & _
                    "SET @bulan=" & DDLMonth.SelectedValue & ";" & _
                    "SET @tahun=" & DDLYear.SelectedValue & ";" & _
                    "SELECT SUM(CASE WHEN gd.gldbcr='D' THEN gd.glamt ELSE 0 END)-" & _
                    "SUM(CASE WHEN gd.gldbcr='C' THEN gd.glamt ELSE 0 END) AS totaldl " & _
                    "FROM QL_trnglmst gm INNER JOIN QL_trngldtl gd ON gm.cmpcode=gd.cmpcode AND gm.glmstoid=gd.glmstoid " & _
                    "INNER JOIN QL_mstacctg a ON a.cmpcode=gd.cmpcode AND a.acctgoid=gd.acctgoid " & _
                    "WHERE gm.cmpcode=@cmpcode AND MONTH(gm.gldate)=@bulan AND YEAR(gm.gldate)=@tahun " & _
                    "AND /*a.acctgcode LIKE '" & sVarDL & "%'*/ a.acctgoid=225 AND gm.glnote NOT LIKE 'CLOSING HPP - PERIODE " & GetDateToPeriodAcctg(CDate(txtDate.Text)) & "'"
                dDLCost = ToDouble(cKoneksi.ambilscalar(sSql).ToString)
            Else
                sMsg &= "- Invalid Interface setup for Direct Labor Cost (VAR_DL_COST) !<BR>"
            End If

            Dim sVarOHD As String = "Y" 'GetVarInterface("VAR_OHD_COST", Session("CompnyCode"))
            Dim dOHDCost As Decimal = 0
            If sVarOHD <> "" Then
                sSql = "DECLARE @cmpcode VARCHAR(10);" & _
                    "DECLARE @bulan INT;" & _
                    "DECLARE @tahun INT;" & _
                    "SET @cmpcode='" & Session("CompnyCode") & "';" & _
                    "SET @bulan=" & DDLMonth.SelectedValue & ";" & _
                    "SET @tahun=" & DDLYear.SelectedValue & ";" & _
                    "SELECT SUM(debet)-SUM(credit) FROM (" & _
                    "SELECT debet=CASE WHEN gd.gldbcr='D' THEN gd.glamt ELSE 0 END," & _
                    "credit=CASE WHEN gd.gldbcr='C' THEN gd.glamt ELSE 0 END " & _
                    "FROM QL_trnglmst gm INNER JOIN QL_trngldtl gd ON gm.cmpcode=gd.cmpcode AND gm.glmstoid=gd.glmstoid " & _
                    "INNER JOIN QL_mstacctg a ON a.cmpcode=gd.cmpcode AND a.acctgoid=gd.acctgoid " & _
                    "WHERE gm.cmpcode=@cmpcode AND MONTH(gm.gldate)=@bulan AND YEAR(gm.gldate)=@tahun " & _
                    "AND /*a.acctgcode LIKE '" & sVarOHD & "%'*/ a.acctgoid=262 AND gm.glnote NOT LIKE 'CLOSING HPP - PERIODE " & GetDateToPeriodAcctg(CDate(txtDate.Text)) & "') AS x"
                dOHDCost = ToDouble(cKoneksi.ambilscalar(sSql).ToString)
            Else
                sMsg &= "- Invalid Interface setup for Overhead Cost (VAR_OHD_COST) !<BR>"
            End If

            If sMsg <> "" Then
                lblError.Text = sMsg
                SetControlPosting(0) : Exit Sub
            End If

            ' ======== POSTING AUTO JURNAL STOCK ADJUSTMENT MATERIAL & ITEM dgn COA TARGET NON DL atau NON OHD
            ' Define Tabel Strucure utk Auto Jurnal 

            sSql = "SELECT '' tipe,0 acctgoid,'' acctgcode,'' acctgdesc,0.0 debet,0.0 credit,'' refno,'' note,'' tipejurnal "
            Dim tbPostGL As DataTable = cKoneksi.ambiltabel(sSql, "PostGL")
            tbPostGL.Rows.Clear()

            sSql = "DECLARE @cmpcode VARCHAR(10);" & _
                "DECLARE @bulan INT;" & _
                "DECLARE @tahun INT;" & _
                "SET @cmpcode='" & Session("CompnyCode") & "';" & _
                "SET @bulan=" & DDLMonth.SelectedValue & ";" & _
                "SET @tahun=" & DDLYear.SelectedValue & ";" & _
                "SELECT UPPER(sa.refname) refname,sa.stockadjno,sa.stockadjdate,sa.stockadjqty,sa.stockadjamtidr AS stockadjamount,m.itemoid AS oid,m.itemlongdescription AS deskripsi," & _
                "gx.genother1 AS stockacctgoid,g.gendesc,g.genother1,a.acctgcode,a.acctgdesc,a2.acctgcode acctgcode2,a2.acctgdesc acctgdesc2 " & _
                "FROM QL_trnstockadj sa INNER JOIN QL_mstitem m ON m.cmpcode=sa.cmpcode AND m.itemoid=sa.refoid AND m.itemgroup='WIP' " & _
                "INNER JOIN QL_mstgen g ON g.cmpcode=sa.cmpcode AND g.genoid=sa.reasonoid " & _
                "INNER JOIN QL_mstacctg a ON a.cmpcode=g.cmpcode AND a.acctgoid=g.genother1 " & _
                "INNER JOIN QL_mstgen gx ON gx.genoid=m.itemgroupoid " & _
                "LEFT JOIN QL_mstacctg a2 ON a2.cmpcode=g.cmpcode AND a2.acctgoid=gx.genother1 " & _
                "WHERE sa.cmpcode=@cmpcode AND MONTH(sa.stockadjdate)=@bulan AND YEAR(sa.stockadjdate)=@tahun " & _
                "UNION ALL " & _
                "SELECT UPPER(sa.refname) refname,sa.stockadjno,sa.stockadjdate,sa.stockadjqty,sa.stockadjamtidr AS stockadjamount,i.itemoid oid,i.itemlongdescription AS deskripsi," & _
                "gx.genother1 AS stockacctgoid,g.gendesc,g.genother1,a.acctgcode,a.acctgdesc,a2.acctgcode acctgcode2,a2.acctgdesc acctgdesc2 " & _
                "FROM QL_trnstockadj sa INNER JOIN QL_mstitem i ON i.cmpcode=sa.cmpcode AND i.itemoid=sa.refoid AND i.itemgroup='FG' " & _
                "INNER JOIN QL_mstgen g ON g.cmpcode=sa.cmpcode AND g.genoid=sa.reasonoid " & _
                "INNER JOIN QL_mstacctg a ON a.cmpcode=g.cmpcode AND a.acctgoid=g.genother1 " & _
                "AND a.acctgoid IN (225, 262) /*(a.acctgcode LIKE '" & sVarDL & "%' OR a.acctgcode LIKE '" & sVarOHD & "%')*/ " & _
                "INNER JOIN QL_mstgen gx ON gx.genoid=i.itemgroupoid " & _
                "LEFT JOIN QL_mstacctg a2 ON a2.cmpcode=g.cmpcode AND a2.acctgoid=gx.genother1 " & _
                "WHERE sa.cmpcode=@cmpcode AND MONTH(sa.stockadjdate)=@bulan AND YEAR(sa.stockadjdate)=@tahun "
            Dim dtAdjMatItem As DataTable = cKoneksi.ambiltabel(sSql, "AdjPost")

            For R1 As Integer = 0 To dtAdjMatItem.Rows.Count - 1
                If ToDecimal(dtAdjMatItem.Rows(R1)("stockacctgoid").ToString) = 0 Then
                    sMsg &= "- Invalid Setting Stock Account for " & dtAdjMatItem.Rows(R1)("refname").ToString & " " & dtAdjMatItem.Rows(R1)("deskripsi").ToString & ". <BR>"
                End If
                If ToDecimal(dtAdjMatItem.Rows(R1)("genother1").ToString) = 0 Then
                    sMsg &= "- Invalid Setting Account for ADJUSTMENT REASON " & dtAdjMatItem.Rows(R1)("gendesc").ToString & ". <BR>"
                End If

                Dim oRow As DataRow
                If ToDouble(dtAdjMatItem.Rows(R1)("stockadjqty").ToString) > 0 Then
                    ' STOCK vs BIAYA
                    oRow = tbPostGL.NewRow
                    oRow("tipe") = "STOCK ADJUSTMENT"
                    oRow("acctgoid") = dtAdjMatItem.Rows(R1)("stockacctgoid")
                    oRow("acctgcode") = dtAdjMatItem.Rows(R1)("acctgcode2")
                    oRow("acctgdesc") = dtAdjMatItem.Rows(R1)("acctgdesc2")
                    If dtAdjMatItem.Rows(R1)("refname").ToString.ToUpper = "ITEM" Then
                        If dtUsage.Select("itemoid=" & dtAdjMatItem.Rows(R1)("oid")).Length > 0 Then
                            Dim oRowPrice As DataRow = dtUsage.Select("itemoid=" & dtAdjMatItem.Rows(R1)("oid"))(0)
                            oRow("debet") = ToDouble(Math.Abs(dtAdjMatItem.Rows(R1)("stockadjamount"))) ' ToDouble(Math.Abs(dtAdjMatItem.Rows(R1)("stockadjqty"))) * ToDouble(oRowPrice("refhpp").ToString)
                        Else
                            oRow("debet") = 0
                        End If
                    Else
                        If dtPurch.Select("matoid=" & dtAdjMatItem.Rows(R1)("oid")).Length > 0 Then
                            Dim oRowPrice As DataRow = dtPurch.Select("matoid=" & dtAdjMatItem.Rows(R1)("oid"))(0)
                            oRow("debet") = ToDouble(Math.Abs(dtAdjMatItem.Rows(R1)("stockadjamount"))) ' ToDouble(Math.Abs(dtAdjMatItem.Rows(R1)("stockadjqty"))) * ToDouble(oRowPrice("akhirprice").ToString)
                        Else
                            oRow("debet") = 0
                        End If
                    End If
                    oRow("credit") = 0
                    oRow("refno") = dtAdjMatItem.Rows(R1)("stockadjno").ToString
                    oRow("note") = "STOCK ADJUSTMENT NO. : " & dtAdjMatItem.Rows(R1)("stockadjno").ToString
                    oRow("tipejurnal") = "*"
                    tbPostGL.Rows.Add(oRow)

                    oRow = tbPostGL.NewRow
                    oRow("tipe") = "STOCK ADJUSTMENT"
                    oRow("acctgoid") = dtAdjMatItem.Rows(R1)("genother1")
                    oRow("acctgcode") = dtAdjMatItem.Rows(R1)("acctgcode")
                    oRow("acctgdesc") = dtAdjMatItem.Rows(R1)("acctgdesc")
                    oRow("debet") = 0
                    If dtAdjMatItem.Rows(R1)("refname").ToString.ToUpper = "ITEM" Then
                        If dtUsage.Select("itemoid=" & dtAdjMatItem.Rows(R1)("oid")).Length > 0 Then
                            Dim oRowPrice As DataRow = dtUsage.Select("itemoid=" & dtAdjMatItem.Rows(R1)("oid"))(0)
                            oRow("credit") = ToDouble(Math.Abs(dtAdjMatItem.Rows(R1)("stockadjamount"))) ' ToDouble(Math.Abs(dtAdjMatItem.Rows(R1)("stockadjqty"))) * ToDouble(oRowPrice("refhpp").ToString)
                        Else
                            oRow("credit") = 0
                        End If
                    Else
                        If dtPurch.Select("matoid=" & dtAdjMatItem.Rows(R1)("oid")).Length > 0 Then
                            Dim oRowPrice As DataRow = dtPurch.Select("matoid=" & dtAdjMatItem.Rows(R1)("oid"))(0)
                            oRow("credit") = ToDouble(Math.Abs(dtAdjMatItem.Rows(R1)("stockadjamount"))) ' ToDouble(Math.Abs(dtAdjMatItem.Rows(R1)("stockadjqty"))) * ToDouble(oRowPrice("akhirprice").ToString)
                        Else
                            oRow("credit") = 0
                        End If
                    End If
                    oRow("refno") = dtAdjMatItem.Rows(R1)("stockadjno").ToString
                    oRow("note") = "STOCK ADJUSTMENT NO. : " & dtAdjMatItem.Rows(R1)("stockadjno").ToString
                    oRow("tipejurnal") = "*"
                    tbPostGL.Rows.Add(oRow)

                ElseIf ToDouble(dtAdjMatItem.Rows(R1)("stockadjqty").ToString) < 0 Then
                    ' BIAYA vs STOCK
                    oRow = tbPostGL.NewRow
                    oRow("tipe") = "STOCK ADJUSTMENT"
                    oRow("acctgoid") = dtAdjMatItem.Rows(R1)("genother1")
                    oRow("acctgcode") = dtAdjMatItem.Rows(R1)("acctgcode")
                    oRow("acctgdesc") = dtAdjMatItem.Rows(R1)("acctgdesc")
                    If dtAdjMatItem.Rows(R1)("refname").ToString.ToUpper = "ITEM" Then
                        If dtUsage.Select("itemoid=" & dtAdjMatItem.Rows(R1)("oid")).Length > 0 Then
                            Dim oRowPrice As DataRow = dtUsage.Select("itemoid=" & dtAdjMatItem.Rows(R1)("oid"))(0)
                            oRow("debet") = Math.Abs(ToDouble(dtAdjMatItem.Rows(R1)("stockadjqty"))) * ToDouble(oRowPrice("refhpp").ToString)
                        Else
                            oRow("debet") = 0
                        End If
                    Else
                        If dtPurch.Select("matoid=" & dtAdjMatItem.Rows(R1)("oid")).Length > 0 Then
                            Dim oRowPrice As DataRow = dtPurch.Select("matoid=" & dtAdjMatItem.Rows(R1)("oid"))(0)
                            oRow("debet") = Math.Abs(ToDouble(dtAdjMatItem.Rows(R1)("stockadjqty"))) * ToDouble(oRowPrice("akhirprice").ToString)
                        Else
                            oRow("debet") = 0
                        End If
                    End If
                    oRow("credit") = 0
                    oRow("refno") = dtAdjMatItem.Rows(R1)("stockadjno").ToString
                    oRow("note") = "STOCK ADJUSTMENT NO. : " & dtAdjMatItem.Rows(R1)("stockadjno").ToString
                    oRow("tipejurnal") = "*"
                    tbPostGL.Rows.Add(oRow)

                    oRow = tbPostGL.NewRow
                    oRow("tipe") = "STOCK ADJUSTMENT"
                    oRow("acctgoid") = dtAdjMatItem.Rows(R1)("stockacctgoid")
                    oRow("acctgcode") = dtAdjMatItem.Rows(R1)("acctgcode2")
                    oRow("acctgdesc") = dtAdjMatItem.Rows(R1)("acctgdesc2")
                    oRow("debet") = 0
                    If dtAdjMatItem.Rows(R1)("refname").ToString.ToUpper = "ITEM" Then
                        If dtUsage.Select("itemoid=" & dtAdjMatItem.Rows(R1)("oid")).Length > 0 Then
                            Dim oRowPrice As DataRow = dtUsage.Select("itemoid=" & dtAdjMatItem.Rows(R1)("oid"))(0)
                            oRow("credit") = Math.Abs(ToDouble(dtAdjMatItem.Rows(R1)("stockadjqty"))) * ToDouble(oRowPrice("refhpp").ToString)
                        Else
                            oRow("credit") = 0
                        End If
                    Else
                        If dtPurch.Select("matoid=" & dtAdjMatItem.Rows(R1)("oid")).Length > 0 Then
                            Dim oRowPrice As DataRow = dtPurch.Select("matoid=" & dtAdjMatItem.Rows(R1)("oid"))(0)
                            oRow("credit") = Math.Abs(ToDouble(dtAdjMatItem.Rows(R1)("stockadjqty"))) * ToDouble(oRowPrice("akhirprice").ToString)
                        Else
                            oRow("credit") = 0
                        End If
                    End If

                    oRow("refno") = dtAdjMatItem.Rows(R1)("stockadjno").ToString
                    oRow("note") = "STOCK ADJUSTMENT NO. : " & dtAdjMatItem.Rows(R1)("stockadjno").ToString
                    oRow("tipejurnal") = "*"
                    tbPostGL.Rows.Add(oRow)
                End If
            Next

            ' CHECK FOR ADJUSTMENT WITH COA DL/OHD COST
            Dim dTotalDLCostAdj As Decimal = 0
            ' ADD STOCK ADJUSTMENT with DL COST Account
            dTotalDLCostAdj = Math.Round(ToDecimal(tbPostGL.Compute("SUM(debet)-SUM(credit)", "tipe='STOCK ADJUSTMENT' AND tipejurnal='*' AND acctgcode LIKE '" & sVarDL & "%' ").ToString), 2, MidpointRounding.AwayFromZero)

            Dim dTotalOHDCostAdj As Decimal = 0
            ' ADD STOCK ADJUSTMENT with OHD COST Account
            dTotalOHDCostAdj = Math.Round(ToDecimal(tbPostGL.Compute("SUM(debet)-SUM(credit)", "tipe='STOCK ADJUSTMENT' AND tipejurnal='*' AND acctgcode LIKE '" & sVarOHD & "%' ").ToString), 2, MidpointRounding.AwayFromZero)

            dDLCost += dTotalDLCostAdj
            dOHDCost += dTotalOHDCostAdj

            ' APPLY Usage Material Cost 
            sSql = "SELECT cmpcode,hppoid,periodacctg,reftype,refoid,refqty,refunitoid AS refunit,usagecost,laborcost,ohdcost,refhpp,reftotal," & _
                "hppstatus,0.0 refqtyprod,0.0 refhppprod,0.0 reftotalprod,0.0 refqtyadjin,0.0 refhppadjin,0.0 reftotaladjin," & _
                "refqtyakhir,refhppakhir,reftotalakhir,createuser AS crtuser,createtime AS crttime,upduser,updtime,'' itemcode,'' itemdesc FROM QL_crdhpp WHERE cmpcode='DUMMY' "
            Dim dtItemHPP As DataTable = cKoneksi.ambiltabel(sSql, "HPPItem")

            Dim dvItemCek As DataView = dtItemHPP.DefaultView
            For R2 As Integer = 0 To dtUsage.Rows.Count - 1
                dvItemCek.RowFilter = "refoid=" & dtUsage.Rows(R2)("itemoid").ToString
                Dim bStat As Boolean = False
                If dvItemCek.Count <= 0 Then
                    bStat = True ' INSERT
                End If
                dvItemCek.RowFilter = ""

                If bStat Then
                    Dim oRow As DataRow = dtItemHPP.NewRow
                    oRow("cmpcode") = CompnyCode
                    oRow("hppoid") = 0
                    oRow("periodacctg") = GetDateToPeriodAcctg(CDate(txtDate.Text))
                    oRow("reftype") = "ITEM"
                    oRow("refoid") = dtUsage.Rows(R2)("itemoid").ToString
                    oRow("itemcode") = dtUsage.Rows(R2)("itemcode").ToString
                    oRow("itemdesc") = dtUsage.Rows(R2)("itemlongdesc").ToString
                    oRow("refqty") = ToDouble(dtUsage.Rows(R2)("refqty").ToString)
                    oRow("refhpp") = ToDouble(dtUsage.Rows(R2)("refhpp").ToString)
                    oRow("reftotal") = ToDouble(dtUsage.Rows(R2)("reftotal").ToString)
                    oRow("refunit") = ToDouble(dtUsage.Rows(R2)("itemunit").ToString)
                    oRow("usagecost") = ToDouble(dtUsage.Rows(R2)("totalusage").ToString)
                    oRow("refqtyprod") = ToDouble(dtUsage.Rows(R2)("qtyitem").ToString)
                    oRow("refhppprod") = 0
                    oRow("reftotalprod") = 0
                    oRow("refqtyadjin") = 0
                    oRow("refhppadjin") = 0
                    oRow("reftotaladjin") = 0
                    oRow("refqtyakhir") = ToDouble(dtUsage.Rows(R2)("saldoakhir").ToString)
                    oRow("refhppakhir") = 0
                    oRow("reftotalakhir") = 0
                    oRow("hppstatus") = "CLOSE"
                    oRow("crtuser") = Session("UserID")
                    oRow("crttime") = GetServerTime()
                    oRow("upduser") = Session("UserID")
                    oRow("updtime") = GetServerTime()
                    dtItemHPP.Rows.Add(oRow)
                Else
                    Dim oRow As DataRow = dtItemHPP.Select("refoid=" & dtUsage.Rows(R2)("itemoid").ToString)(0)
                    oRow.BeginEdit()
                    oRow("usagecost") = ToDouble(oRow("usagecost").ToString) + ToDouble(dtUsage.Rows(R2)("totalusage").ToString)
                    oRow("upduser") = Session("UserID")
                    oRow("updtime") = GetServerTime()
                    oRow.EndEdit()
                    dtItemHPP.Select(Nothing, Nothing)
                End If
                dtItemHPP.AcceptChanges()
            Next

            ' Calculate DL and OHD Cost Production
            Dim dTotQty As Decimal = ToDouble(dtItemHPP.Compute("SUM(refqtyprod)", Nothing).ToString)
            For R3 As Integer = 0 To dtItemHPP.Rows.Count - 1
                If dTotQty > 0 Then
                    dtItemHPP.Rows(R3)("laborcost") = (ToDouble(dtItemHPP.Rows(R3)("refqtyprod").ToString) / dTotQty) * dDLCost
                    dtItemHPP.Rows(R3)("ohdcost") = (ToDouble(dtItemHPP.Rows(R3)("refqtyprod").ToString) / dTotQty) * dOHDCost
                Else
                    dtItemHPP.Rows(R3)("laborcost") = 0
                    dtItemHPP.Rows(R3)("ohdcost") = 0
                End If

                dtItemHPP.Rows(R3)("reftotalprod") = ToDouble(dtItemHPP.Rows(R3)("usagecost").ToString) + ToDouble(dtItemHPP.Rows(R3)("laborcost").ToString) + ToDouble(dtItemHPP.Rows(R3)("ohdcost").ToString)
                If ToDouble(dtItemHPP.Rows(R3)("refqtyprod").ToString) > 0 Then
                    dtItemHPP.Rows(R3)("refhppprod") = ToDouble(dtItemHPP.Rows(R3)("reftotalprod").ToString) / ToDouble(dtItemHPP.Rows(R3)("refqtyprod").ToString)
                Else
                    dtItemHPP.Rows(R3)("refhppprod") = 0
                End If
            Next
            dtItemHPP.AcceptChanges()

            gvItemHPP.DataSource = dtItemHPP
            gvItemHPP.DataBind()
            lbhpp.Visible = True

            ' Check for Adjustment Item In
            sSql = "DECLARE @cmpcode VARCHAR(10);" & _
                "DECLARE @bulan INT;" & _
                "DECLARE @tahun INT;" & _
                "SET @cmpcode='" & Session("CompnyCode") & "';" & _
                "SET @bulan=" & DDLMonth.SelectedValue & ";" & _
                "SET @tahun=" & DDLYear.SelectedValue & ";" & _
                "SELECT refoid,stockadjqty,stockadjamtidr/stockadjqty price,stockadjamtidr AS stockadjamount " & _
                "FROM QL_trnstockadj ad INNER JOIN QL_mstitem i ON i.cmpcode=ad.cmpcode AND i.itemoid=ad.refoid " & _
                "WHERE ad.cmpcode=@cmpcode AND MONTH(ad.stockadjdate)=@bulan " & _
                "AND YEAR(ad.stockadjdate)=@tahun AND ad.stockadjqty>0"
            Dim dtAdj As DataTable = cKoneksi.ambiltabel(sSql, "QL_trnstockadjustment")

            If dtAdj.Rows.Count > 0 Then
                For R1 As Integer = 0 To dtAdj.Rows.Count - 1
                    If dtItemHPP.Select("refoid=" & dtAdj.Rows(R1)("refoid")).Length > 0 Then
                        Dim oRow As DataRow = dtItemHPP.Select("refoid=" & dtAdj.Rows(R1)("refoid"))(0)
                        oRow.BeginEdit()
                        oRow("refqtyadjin") += ToDouble(dtAdj.Rows(R1)("stockadjqty").ToString)
                        oRow("reftotaladjin") += ToDouble(dtAdj.Rows(R1)("stockadjamount").ToString)
                        oRow("refhppadjin") = oRow("reftotaladjin") / oRow("refqtyadjin") ' ToDouble(dtAdj.Rows(R1)("price").ToString)
                        oRow("upduser") = Session("UserID")
                        oRow("updtime") = GetServerTime()
                        oRow.EndEdit()
                        dtItemHPP.Select(Nothing, Nothing)
                    End If
                Next
                dtItemHPP.AcceptChanges()
            End If

            ' Check for Adjustment Item Out
            sSql = "DECLARE @cmpcode VARCHAR(10);" & _
                "DECLARE @bulan INT;" & _
                "DECLARE @tahun INT;" & _
                "SET @cmpcode='" & Session("CompnyCode") & "';" & _
                "SET @bulan=" & DDLMonth.SelectedValue & ";" & _
                "SET @tahun=" & DDLYear.SelectedValue & ";" & _
                "SELECT refoid,stockadjqty,stockadjamtidr/stockadjqty price,stockadjamtidr AS stockadjamount " & _
                "FROM QL_trnstockadj ad INNER JOIN QL_mstitem i ON i.cmpcode=ad.cmpcode AND i.itemoid=ad.refoid " & _
                "WHERE ad.cmpcode=@cmpcode AND MONTH(ad.stockadjdate)=@bulan " & _
                "AND YEAR(ad.stockadjdate)=@tahun AND ad.stockadjqty<0"
            Dim dtAdjOut As DataTable = cKoneksi.ambiltabel(sSql, "QL_trnstockadjustment")

            For R4 As Integer = 0 To dtItemHPP.Rows.Count - 1
                If ToDouble(dtItemHPP.Rows(R4)("refqtyprod").ToString) + ToDouble(dtItemHPP.Rows(R4)("refqty").ToString) + ToDouble(dtItemHPP.Rows(R4)("refqtyadjin").ToString) > 0 Then
                    dtItemHPP.Rows(R4)("refhppakhir") = Math.Round((ToDouble(dtItemHPP.Rows(R4)("reftotalprod").ToString) + ToDouble(dtItemHPP.Rows(R4)("reftotal").ToString) + ToDouble(dtItemHPP.Rows(R4)("reftotaladjin").ToString)) / (ToDouble(dtItemHPP.Rows(R4)("refqtyprod").ToString) + ToDouble(dtItemHPP.Rows(R4)("refqty").ToString) + ToDouble(dtItemHPP.Rows(R4)("refqtyadjin").ToString)), 2, MidpointRounding.AwayFromZero)
                Else
                    dtItemHPP.Rows(R4)("refhppakhir") = 0
                End If
                dtItemHPP.Rows(R4)("reftotalakhir") = ToDouble(dtItemHPP.Rows(R4)("refqtyakhir").ToString) * ToDouble(dtItemHPP.Rows(R4)("refhppakhir").ToString)
            Next

            gvNewAvg.DataSource = dtItemHPP
            gvNewAvg.DataBind()
            lbNewAvg.Visible = True

            ' COGS Calculation
            sSql = "SELECT '' item,'' value FROM QL_mstoid WHERE cmpcode='DUMMMY'"
            Dim dtCOGS As DataTable = cKoneksi.ambiltabel(sSql, "COGS")

            ' Persediaan Awal (Beginning finished goods)
            Dim dTotAwal As Decimal = 0
            Dim dTotProd As Decimal = 0
            Dim dTotAkhir As Decimal = 0
            Dim dTotAdjIn As Decimal = 0
            Dim dTotAdjOut As Decimal = 0

            For R1 As Integer = 0 To dtItemHPP.Rows.Count - 1
                dTotAwal += ToDouble(dtItemHPP.Rows(R1)("refqty").ToString) * ToDouble(dtItemHPP.Rows(R1)("refhpp"))
                dTotProd += ToDouble(dtItemHPP.Rows(R1)("reftotalprod").ToString)
                dTotAkhir += ToDouble(dtItemHPP.Rows(R1)("reftotalakhir").ToString)
            Next

            For R2 As Integer = 0 To dtAdj.Rows.Count - 1
                Dim dvItemHPP As DataView = dtItemHPP.DefaultView
                dvItemHPP.RowFilter = "refoid=" & dtAdj.Rows(R2)("refoid").ToString
                If dvItemHPP.Count > 0 Then
                    dTotAdjIn += ToDouble(dtAdj.Rows(R2)("stockadjqty").ToString) * ToDouble(dvItemHPP(0)("refhppakhir").ToString)
                End If
            Next

            For R3 As Integer = 0 To dtAdjOut.Rows.Count - 1
                Dim dvItemHPP As DataView = dtItemHPP.DefaultView
                dvItemHPP.RowFilter = "refoid=" & dtAdjOut.Rows(R3)("refoid").ToString
                If dvItemHPP.Count > 0 Then
                    dTotAdjOut += Math.Abs(ToDouble(dtAdjOut.Rows(R3)("stockadjqty").ToString)) * dvItemHPP(0)("refhppakhir").ToString
                End If
            Next

            Dim oRowCOGS As DataRow = dtCOGS.NewRow
            oRowCOGS("item") = "Persediaan Barang Jadi (Awal)"
            oRowCOGS("value") = ToMaskEdit(dTotAwal, 2)
            dtCOGS.Rows.Add(oRowCOGS)

            ' Harga Pokok Produksi
            oRowCOGS = dtCOGS.NewRow
            oRowCOGS("item") = "Harga Pokok Produksi"
            oRowCOGS("value") = ToMaskEdit(dTotProd, 2)
            dtCOGS.Rows.Add(oRowCOGS)

            ' Separator +
            oRowCOGS = dtCOGS.NewRow
            oRowCOGS("item") = ""
            oRowCOGS("value") = "+ -----------------------"
            dtCOGS.Rows.Add(oRowCOGS)

            ' Persediaan Barang Jadi siap jual
            oRowCOGS = dtCOGS.NewRow
            oRowCOGS("item") = "Persediaan Barang Jadi siap jual"
            oRowCOGS("value") = ToMaskEdit(dTotAwal + dTotProd, 2)
            dtCOGS.Rows.Add(oRowCOGS)

            ' Adjustment In
            oRowCOGS = dtCOGS.NewRow
            oRowCOGS("item") = "Adjustment In"
            oRowCOGS("value") = ToMaskEdit(dTotAdjIn, 2)
            dtCOGS.Rows.Add(oRowCOGS)

            ' Separator +
            oRowCOGS = dtCOGS.NewRow
            oRowCOGS("item") = ""
            oRowCOGS("value") = "+ -----------------------"
            dtCOGS.Rows.Add(oRowCOGS)

            ' Adjustment In
            oRowCOGS = dtCOGS.NewRow
            oRowCOGS("item") = ""
            oRowCOGS("value") = ToMaskEdit(dTotAwal + dTotProd + dTotAdjIn, 2)
            dtCOGS.Rows.Add(oRowCOGS)

            ' Adjustment Out
            oRowCOGS = dtCOGS.NewRow
            oRowCOGS("item") = "Adjustment Out"
            oRowCOGS("value") = ToMaskEdit(dTotAdjOut, 2)
            dtCOGS.Rows.Add(oRowCOGS)

            ' Separator -
            oRowCOGS = dtCOGS.NewRow
            oRowCOGS("item") = ""
            oRowCOGS("value") = "- -----------------------"
            dtCOGS.Rows.Add(oRowCOGS)

            ' Adjustment In
            oRowCOGS = dtCOGS.NewRow
            oRowCOGS("item") = ""
            oRowCOGS("value") = ToMaskEdit(dTotAwal + dTotProd + dTotAdjIn - dTotAdjOut, 2)
            dtCOGS.Rows.Add(oRowCOGS)

            ' Persediaan Barang Jadi akhir
            oRowCOGS = dtCOGS.NewRow
            oRowCOGS("item") = "Persediaan Barang Jadi (Akhir)"
            oRowCOGS("value") = ToMaskEdit(dTotAkhir, 2)
            dtCOGS.Rows.Add(oRowCOGS)

            ' Separator -
            oRowCOGS = dtCOGS.NewRow
            oRowCOGS("item") = ""
            oRowCOGS("value") = "- -----------------------"
            dtCOGS.Rows.Add(oRowCOGS)

            ' Harga Pokok Penjualan
            oRowCOGS = dtCOGS.NewRow
            oRowCOGS("item") = "Harga Pokok Penjualan"
            oRowCOGS("value") = ToMaskEdit(dTotAwal + dTotProd + dTotAdjIn - dTotAdjOut - dTotAkhir, 2)
            dtCOGS.Rows.Add(oRowCOGS)

            gvCOGS.DataSource = dtCOGS
            gvCOGS.DataBind()
            lblCOGS.Visible = True

            Session("tbPurch") = dtPurch
            Session("tbUsage") = dtUsage
            Session("tbItemHPP") = dtItemHPP
            Session("tbCOGS") = dtCOGS

            ' POSTING PER TRANSACTION FOR HPP & STOCK
            ' Usage Material vs Persediaan Material + DL Cost + OHD Cost (Production Result - Material)
            sSql = "DECLARE @periodacctg VARCHAR(10);" & _
                "DECLARE @cmpcode VARCHAR(10);" & _
                "DECLARE @bulan INT;" & _
                "DECLARE @tahun INT;" & _
                "SET @periodacctg='" & GetDateToPeriodAcctg(CDate(txtDate.Text)) & "';" & _
                "SET @cmpcode='" & Session("CompnyCode") & "';" & _
                "SET @bulan=" & DDLMonth.SelectedValue & ";" & _
                "SET @tahun=" & DDLYear.SelectedValue & ";" & _
                "SELECT rm.prodresno AS prodresultno,m.itemoid AS matoid,m.itemlongdescription AS matlongdesc,0 AS purchaseacctgoid,gx.genother1 AS stockacctgoid,a.acctgcode,a.acctgdesc,dm.prodresqty AS matqty,0.0 AS totalprice " & _
                "FROM QL_trnprodresdtl dm INNER JOIN QL_mstitem m ON m.cmpcode=dm.cmpcode AND m.itemoid=dm.itemoid AND dm.prodrestype='WIP' " & _
                "INNER JOIN QL_trnprodresmst rm ON dm.cmpcode=rm.cmpcode AND dm.prodresmstoid=rm.prodresmstoid " & _
                "INNER JOIN QL_mstgen gx ON gx.genoid=m.itemgroupoid " & _
                "INNER JOIN QL_mstacctg a ON a.cmpcode=m.cmpcode AND a.acctgoid=gx.genother1 " & _
                "WHERE m.cmpcode=@cmpcode AND MONTH(rm.prodresdate)=@bulan AND YEAR(rm.prodresdate)=@tahun " & _
                "AND rm.prodresmststatus='POST'"
            Dim dtUsageGL As DataTable = cKoneksi.ambiltabel(sSql, "UsagePostGL ")
            ' Apply Average Price for Material
            For R1 As Integer = 0 To dtUsageGL.Rows.Count - 1
                dvPurch.RowFilter = "matoid=" & dtUsageGL.Rows(R1)("matoid")
                If dvPurch.Count > 0 Then
                    dtUsageGL.Rows(R1)("totalprice") = Math.Round(ToDouble(dvPurch(0)("akhirprice").ToString) * ToDouble(dtUsageGL.Rows(R1)("matqty").ToString), 2, MidpointRounding.AwayFromZero)
                End If
                dvPurch.RowFilter = ""
                dtUsageGL.AcceptChanges()
            Next

            Dim sVarCOGM As String = GetVarInterface("VAR_HPP_COGM", Session("CompnyCode"))
            Dim iCoGM As Integer = cKoneksi.ambilscalar("SELECT acctgoid FROM QL_mstacctg WHERE cmpcode='" & Session("CompnyCode") & "' AND acctgcode='" & sVarCOGM & "'")
            Dim dTotHPP As Decimal = 0

            sSql = "SELECT rm.prodresno AS prodresultno,di.prodresqty AS resultqty,gx.genother1 AS stockacctgoid,i.itemlongdescription FROM QL_trnprodresmst rm " & _
                "INNER JOIN QL_trnprodresdtl di ON di.cmpcode=rm.cmpcode AND di.prodresmstoid=rm.prodresmstoid AND di.prodrestype='FG' " & _
                "INNER JOIN QL_mstitem i ON i.cmpcode=di.cmpcode AND i.itemoid=di.itemoid AND di.prodrestype='FG' " & _
                "INNER JOIN QL_mstgen gx ON gx.genoid=i.itemgroupoid " & _
                "WHERE rm.cmpcode='" & Session("CompnyCode") & "' AND MONTH(rm.prodresdate)=" & DDLMonth.SelectedValue & " AND YEAR(rm.prodresdate)=" & DDLYear.SelectedValue & " " & _
                "AND rm.prodresmststatus='POST' "
            Dim tbPRes As DataTable = cKoneksi.ambiltabel(sSql, "QL_trnprodresultmst")

            Dim dTotResult As Decimal = ToDouble(tbPRes.Compute("SUM(resultqty)", "").ToString)

            Dim tbDtlDLCost As New DataTable
            If sVarDL <> "" Then
                sSql = "DECLARE @cmpcode VARCHAR(10);" & _
                    "DECLARE @bulan INT;" & _
                    "DECLARE @tahun INT;" & _
                    "SET @cmpcode='" & Session("CompnyCode") & "';" & _
                    "SET @bulan=" & DDLMonth.SelectedValue & ";" & _
                    "SET @tahun=" & DDLYear.SelectedValue & ";" & _
                    "SELECT a.acctgoid,a.acctgcode,a.acctgdesc,SUM(CASE WHEN gd.gldbcr='D' THEN gd.glamt ELSE 0 END)-" & _
                    "SUM(CASE WHEN gd.gldbcr='C' THEN gd.glamt ELSE 0 END) AS totaldl " & _
                    "FROM QL_trnglmst gm INNER JOIN QL_trngldtl gd ON gm.cmpcode=gd.cmpcode AND gm.glmstoid=gd.glmstoid " & _
                    "INNER JOIN QL_mstacctg a ON a.cmpcode=gd.cmpcode AND a.acctgoid=gd.acctgoid " & _
                    "WHERE gm.cmpcode=@cmpcode AND MONTH(gm.gldate)=@bulan AND YEAR(gm.gldate)=@tahun " & _
                    "AND /*a.acctgcode LIKE '" & sVarDL & "%'*/ a.acctgoid=225 AND gm.glnote NOT LIKE 'CLOSING HPP - PERIODE " & GetDateToPeriodAcctg(CDate(txtDate.Text)) & "' " & _
                    "GROUP BY a.acctgoid,a.acctgcode,a.acctgdesc"
                tbDtlDLCost = cKoneksi.ambiltabel(sSql, "DLCost")
            Else
                sMsg &= "- Invalid Interface setup for Direct Labor Cost (VAR_DL_COST) !<BR>"
            End If
            Dim dTotDLCost As Decimal = ToDouble(tbDtlDLCost.Compute("SUM(totaldl)", "").ToString)

            Dim tbDtlOHDCost As New DataTable
            If sVarOHD <> "" Then
                sSql = "DECLARE @cmpcode VARCHAR(10);" & _
                    "DECLARE @bulan INT;" & _
                    "DECLARE @tahun INT;" & _
                    "SET @cmpcode='" & Session("CompnyCode") & "';" & _
                    "SET @bulan=" & DDLMonth.SelectedValue & ";" & _
                    "SET @tahun=" & DDLYear.SelectedValue & ";" & _
                    "SELECT a.acctgoid,a.acctgcode,a.acctgdesc,SUM(CASE WHEN gd.gldbcr='D' THEN gd.glamt ELSE 0 END)-" & _
                    "SUM(CASE WHEN gd.gldbcr='C' THEN gd.glamt ELSE 0 END) AS totalohd " & _
                    "FROM QL_trnglmst gm INNER JOIN QL_trngldtl gd ON gm.cmpcode=gd.cmpcode AND gm.glmstoid=gd.glmstoid " & _
                    "INNER JOIN QL_mstacctg a ON a.cmpcode=gd.cmpcode AND a.acctgoid=gd.acctgoid " & _
                    "WHERE gm.cmpcode=@cmpcode AND MONTH(gm.gldate)=@bulan AND YEAR(gm.gldate)=@tahun " & _
                    "AND /*a.acctgcode LIKE '" & sVarOHD & "%'*/ a.acctgoid=262 AND gm.glnote NOT LIKE 'CLOSING HPP - PERIODE " & GetDateToPeriodAcctg(CDate(txtDate.Text)) & "' " & _
                    "GROUP BY a.acctgoid,a.acctgcode,a.acctgdesc"
                tbDtlOHDCost = cKoneksi.ambiltabel(sSql, "OHDCost")
            Else
                sMsg &= "- Invalid Interface setup for Direct Labor Cost (VAR_OHD_COST) !<BR>"
            End If
            Dim dTotOHDCost As Decimal = ToDouble(tbDtlOHDCost.Compute("SUM(totalohd)", "").ToString)

            Dim dvUsageGL As DataView = dtUsageGL.DefaultView
            For R2 As Integer = 0 To tbPRes.Rows.Count - 1
                dvUsageGL.RowFilter = "prodresultno='" & tbPRes.Rows(R2)("prodresultno").ToString & "'"
                dTotHPP = 0
                ' Total Cost Material
                dTotHPP = ToDouble(dtUsageGL.Compute("SUM(totalprice)", "prodresultno='" & tbPRes.Rows(R2)("prodresultno").ToString & "'").ToString)

                If dvUsageGL.Count > 0 Then
                    Dim oRow As DataRow = tbPostGL.NewRow

                    For R3 As Integer = 0 To dvUsageGL.Count - 1
                        Dim dvCek As DataView = tbPostGL.DefaultView

                        If ToDecimal(dvUsageGL(R3)("purchaseacctgoid").ToString) = 0 Then
                            sMsg &= "- Invalid Setting Usage Account for Material " & dvUsageGL(R3)("matlongdesc").ToString & ". <BR>"
                        End If

                        dvCek.RowFilter = "acctgoid=" & dvUsageGL(R3)("purchaseacctgoid") & " AND refno='" & tbPRes.Rows(R2)("prodresultno").ToString & "' "
                        If dvCek.Count > 0 Then
                            dvCek(0)("debet") += ToDouble(dvUsageGL(R3)("totalprice").ToString)
                            dvCek.RowFilter = ""
                        Else
                            dvCek.RowFilter = ""

                            oRow = tbPostGL.NewRow
                            oRow("tipe") = "PRODUCTION RESULT - MATERIAL"
                            oRow("acctgoid") = dvUsageGL(R3)("purchaseacctgoid")
                            oRow("acctgcode") = GetStrData("SELECT acctgcode FROM QL_mstacctg WHERE cmpcode='" & Session("CompnyCode") & "' AND acctgoid=" & dvUsageGL(R3)("purchaseacctgoid"))
                            oRow("acctgdesc") = GetStrData("SELECT acctgdesc FROM QL_mstacctg WHERE cmpcode='" & Session("CompnyCode") & "' AND acctgoid=" & dvUsageGL(R3)("purchaseacctgoid"))
                            oRow("debet") = ToDouble(dvUsageGL(R3)("totalprice").ToString)
                            oRow("credit") = 0
                            oRow("refno") = tbPRes.Rows(R2)("prodresultno").ToString
                            oRow("note") = "HPP - PRODUCTION RESULT NO. : " & tbPRes.Rows(R2)("prodresultno").ToString
                            tbPostGL.Rows.Add(oRow)
                        End If
                        tbPostGL.AcceptChanges()
                    Next

                    tbPostGL.AcceptChanges()

                    For R4 As Integer = 0 To dvUsageGL.Count - 1
                        Dim dvCek As DataView = tbPostGL.DefaultView

                        If ToDecimal(dvUsageGL(R4)("stockacctgoid").ToString) = 0 Then
                            sMsg &= "- Invalid Setting Stock Account for Material " & dvUsageGL(R4)("matlongdesc").ToString & ". <BR>"
                        End If

                        dvCek.RowFilter = "acctgoid=" & dvUsageGL(R4)("stockacctgoid") & " AND refno='" & tbPRes.Rows(R2)("prodresultno").ToString & "' "
                        If dvCek.Count > 0 Then
                            dvCek(0)("credit") += ToDouble(dvUsageGL(R4)("totalprice").ToString)
                            dvCek.RowFilter = ""
                        Else
                            dvCek.RowFilter = ""

                            oRow = tbPostGL.NewRow
                            oRow("tipe") = "PRODUCTION RESULT - MATERIAL"
                            oRow("acctgoid") = dvUsageGL(R4)("stockacctgoid")
                            oRow("acctgcode") = GetStrData("SELECT acctgcode FROM QL_mstacctg WHERE cmpcode='" & Session("CompnyCode") & "' AND acctgoid=" & dvUsageGL(R4)("stockacctgoid"))
                            oRow("acctgdesc") = GetStrData("SELECT acctgdesc FROM QL_mstacctg WHERE cmpcode='" & Session("CompnyCode") & "' AND acctgoid=" & dvUsageGL(R4)("stockacctgoid"))
                            oRow("debet") = 0
                            oRow("credit") = ToDouble(dvUsageGL(R4)("totalprice").ToString)
                            oRow("refno") = tbPRes.Rows(R2)("prodresultno").ToString
                            oRow("note") = "HPP - PRODUCTION RESULT NO. : " & tbPRes.Rows(R2)("prodresultno").ToString
                            tbPostGL.Rows.Add(oRow)
                        End If
                        tbPostGL.AcceptChanges()
                    Next
                End If
                dvUsageGL.RowFilter = ""
            Next

            ' Persediaan Barang Jadi vs HPP Produksi (Production Result - Item)
            ' USE TABLE BEFORE
            For R2 As Integer = 0 To tbPRes.Rows.Count - 1
                dvUsageGL.RowFilter = "prodresultno='" & tbPRes.Rows(R2)("prodresultno").ToString & "'"
                dTotHPP = 0
                ' Total Cost Material
                dTotHPP = ToDouble(dtUsageGL.Compute("SUM(totalprice)", "prodresultno='" & tbPRes.Rows(R2)("prodresultno").ToString & "'").ToString)
                ' Add DL & OHD Cost
                dTotHPP += ((ToDouble(tbPRes.Rows(R2)("resultqty")) / dTotResult) * (dTotDLCost + dTotalDLCostAdj))
                dTotHPP += ((ToDouble(tbPRes.Rows(R2)("resultqty")) / dTotResult) * (dTotOHDCost + dTotalOHDCostAdj))

                If ToDecimal(tbPRes.Rows(R2)("stockacctgoid").ToString) = 0 Then
                    sMsg &= "- Invalid Setting Stock Account for Item " & tbPRes.Rows(R2)("itemlongdesc").ToString & ". <BR>"
                End If

                Dim oRow As DataRow = tbPostGL.NewRow
                oRow("tipe") = "PRODUCTION RESULT - ITEM"
                oRow("acctgoid") = tbPRes.Rows(R2)("stockacctgoid")
                oRow("acctgcode") = GetStrData("SELECT acctgcode FROM QL_mstacctg WHERE cmpcode='" & Session("CompnyCode") & "' AND acctgoid=" & tbPRes.Rows(R2)("stockacctgoid"))
                oRow("acctgdesc") = GetStrData("SELECT acctgdesc FROM QL_mstacctg WHERE cmpcode='" & Session("CompnyCode") & "' AND acctgoid=" & tbPRes.Rows(R2)("stockacctgoid"))
                oRow("debet") = dTotHPP
                oRow("credit") = 0
                oRow("refno") = tbPRes.Rows(R2)("prodresultno").ToString
                oRow("note") = "HPP - PRODUCTION RESULT NO. : " & tbPRes.Rows(R2)("prodresultno").ToString
                tbPostGL.Rows.Add(oRow)

                oRow = tbPostGL.NewRow
                oRow("tipe") = "PRODUCTION RESULT - ITEM"
                oRow("acctgoid") = iCoGM
                oRow("acctgcode") = GetStrData("SELECT acctgcode FROM QL_mstacctg WHERE cmpcode='" & Session("CompnyCode") & "' AND acctgoid=" & iCoGM)
                oRow("acctgdesc") = GetStrData("SELECT acctgdesc FROM QL_mstacctg WHERE cmpcode='" & Session("CompnyCode") & "' AND acctgoid=" & iCoGM)
                oRow("debet") = 0
                oRow("credit") = dTotHPP
                oRow("refno") = tbPRes.Rows(R2)("prodresultno").ToString
                oRow("note") = "HPP - PRODUCTION RESULT NO. : " & tbPRes.Rows(R2)("prodresultno").ToString
                tbPostGL.Rows.Add(oRow)
            Next

            If ToDecimal(iCoGM) = 0 Then
                sMsg &= "- Invalid Setting Account for Cost Of Good Manufactured.<BR>"
            End If

            ' HPP Penjualan vs Persediaan Barang Jadi (Sales Invoice)
            sSql = "DECLARE @periodacctg VARCHAR(10);" & _
                "DECLARE @cmpcode VARCHAR(10);" & _
                "DECLARE @bulan INT;" & _
                "DECLARE @tahun INT;" & _
                "SET @periodacctg='" & GetDateToPeriodAcctg(CDate(txtDate.Text)) & "';" & _
                "SET @cmpcode='" & Session("CompnyCode") & "';" & _
                "SET @bulan=" & DDLMonth.SelectedValue & ";" & _
                "SET @tahun=" & DDLYear.SelectedValue & ";" & _
                "SELECT jm.trnjualno,i.itemoid,i.itemlongdescription AS itemlongdesc,gx.genother1 AS stockacctgoid,a.acctgcode,a.acctgdesc,jd.trnjualdtlqty,0.0 AS totalprice " & _
                "FROM QL_trnjualdtl jd INNER JOIN QL_mstitem i ON i.cmpcode=jd.cmpcode AND i.itemoid=jd.itemoid " & _
                "INNER JOIN QL_mstgen gx ON gx.genoid=i.itemgroupoid " & _
                "INNER JOIN QL_trnjualmst jm ON jd.cmpcode=jm.cmpcode AND jd.trnjualmstoid=jm.trnjualmstoid " & _
                "INNER JOIN QL_mstacctg a ON a.cmpcode=i.cmpcode AND a.acctgoid=gx.genother1 " & _
                "WHERE jm.cmpcode=@cmpcode AND MONTH(jm.trnjualdate)=@bulan AND YEAR(jm.trnjualdate)=@tahun " & _
                "AND jm.trnjualstatus='POST' "
            Dim dtSIGL As DataTable = cKoneksi.ambiltabel(sSql, "SIPostGL")
            ' Apply New HPP Price for Item 
            For R1 As Integer = 0 To dtSIGL.Rows.Count - 1
                dvItemCek.RowFilter = "refoid=" & dtSIGL.Rows(R1)("itemoid")
                If dvItemCek.Count > 0 Then
                    dtSIGL.Rows(R1)("totalprice") = ToDouble(dvItemCek(0)("refhppakhir").ToString) * ToDouble(dtSIGL.Rows(R1)("trnjualdtlqty").ToString)
                End If
                dvItemCek.RowFilter = ""
                dtSIGL.AcceptChanges()
            Next

            Dim sVarCOGS As String = GetVarInterface("VAR_HPP_COGS", Session("CompnyCode"))
            Dim iCoGS As Integer = cKoneksi.ambilscalar("SELECT acctgoid FROM QL_mstacctg WHERE cmpcode='" & Session("CompnyCode") & "' AND acctgcode='" & sVarCOGS & "'")
            Dim dTotHPPItem As Decimal = 0

            If ToDecimal(iCoGS) = 0 Then
                sMsg &= "- Invalid Setting Account for Cost Of Good Sold.<BR>"
            End If

            sSql = "SELECT rm.trnjualno FROM QL_trnjualmst rm WHERE rm.cmpcode='" & Session("CompnyCode") & "' AND MONTH(rm.trnjualdate)=" & DDLMonth.SelectedValue & " AND YEAR(rm.trnjualdate)=" & DDLYear.SelectedValue & " "
            Dim tbSI As DataTable = cKoneksi.ambiltabel(sSql, "QL_trnjualmst")
            Dim dvSIGL As DataView = dtSIGL.DefaultView

            For R2 As Integer = 0 To tbSI.Rows.Count - 1
                dvSIGL.RowFilter = "trnjualno='" & tbSI.Rows(R2)("trnjualno").ToString & "'"
                dTotHPP = ToDouble(dtSIGL.Compute("SUM(totalprice)", "trnjualno='" & tbSI.Rows(R2)("trnjualno").ToString & "'").ToString)

                If dvSIGL.Count > 0 Then
                    Dim oRow As DataRow

                    oRow = tbPostGL.NewRow
                    oRow("tipe") = "SALES INVOICE"
                    oRow("acctgoid") = iCoGS
                    oRow("acctgcode") = GetStrData("SELECT acctgcode FROM QL_mstacctg WHERE cmpcode='" & Session("CompnyCode") & "' AND acctgoid=" & iCoGS)
                    oRow("acctgdesc") = GetStrData("SELECT acctgdesc FROM QL_mstacctg WHERE cmpcode='" & Session("CompnyCode") & "' AND acctgoid=" & iCoGS)
                    oRow("debet") = dTotHPP
                    oRow("credit") = 0
                    oRow("refno") = tbSI.Rows(R2)("trnjualno").ToString
                    oRow("note") = "HPP - SALES INVOICE NO. : " & tbSI.Rows(R2)("trnjualno").ToString
                    tbPostGL.Rows.Add(oRow)
                    tbPostGL.AcceptChanges()

                    For R3 As Integer = 0 To dvSIGL.Count - 1
                        Dim dvCek As DataView = tbPostGL.DefaultView

                        If ToDecimal(dvSIGL(R3)("stockacctgoid").ToString) = 0 Then
                            sMsg &= "- Invalid Setting Stock Account for Item " & dvSIGL(R3)("itemlongdesc").ToString & ". <BR>"
                        End If

                        dvCek.RowFilter = "acctgoid=" & dvSIGL(R3)("stockacctgoid").ToString & " AND refno='" & tbSI.Rows(R2)("trnjualno").ToString & "' "

                        If dvCek.Count > 0 Then
                            dvCek(0)("credit") += ToDouble(dvSIGL(R3)("totalprice").ToString)
                            dvCek.RowFilter = ""
                        Else
                            dvCek.RowFilter = ""

                            oRow = tbPostGL.NewRow
                            oRow("tipe") = "SALES INVOICE"
                            oRow("acctgoid") = dvSIGL(R3)("stockacctgoid")
                            oRow("acctgcode") = GetStrData("SELECT acctgcode FROM QL_mstacctg WHERE cmpcode='" & Session("CompnyCode") & "' AND acctgoid=" & dvSIGL(R3)("stockacctgoid"))
                            oRow("acctgdesc") = GetStrData("SELECT acctgdesc FROM QL_mstacctg WHERE cmpcode='" & Session("CompnyCode") & "' AND acctgoid=" & dvSIGL(R3)("stockacctgoid"))
                            oRow("debet") = 0
                            oRow("credit") = ToDouble(dvSIGL(R3)("totalprice").ToString)
                            oRow("refno") = tbSI.Rows(R2)("trnjualno").ToString
                            oRow("note") = "HPP - SALES INVOICE NO. : " & tbSI.Rows(R2)("trnjualno").ToString
                            tbPostGL.Rows.Add(oRow)
                        End If
                        tbPostGL.AcceptChanges()
                    Next
                End If
                dvSIGL.RowFilter = ""
            Next

            '' AUTO JOURNAL UTK STOCK ADJUSTMENT
            'sSql = "DECLARE @cmpcode VARCHAR(10);" & _
            '    "DECLARE @bulan INT;" & _
            '    "DECLARE @tahun INT;" & _
            '    "SET @cmpcode='" & Session("CompnyCode") & "';" & _
            '    "SET @bulan=" & DDLMonth.SelectedValue & ";" & _
            '    "SET @tahun=" & DDLYear.SelectedValue & ";" & _
            '    "SELECT UPPER(sa.refname) refname,sa.stockadjno,sa.stockadjdate,sa.stockadjqty,m.matoid oid,m.matlongdesc deskripsi," & _
            '    "m.stockacctgoid,g.gendesc,g.genother1,a.acctgcode,a.acctgdesc,a2.acctgcode acctgcode2,a2.acctgdesc acctgdesc2 " & _
            '    "FROM QL_trnstockadjustment sa INNER JOIN QL_mstmat m ON m.cmpcode=sa.cmpcode AND m.matoid=sa.refoid AND sa.refname='MATERIAL' " & _
            '    "INNER JOIN QL_mstgen g ON g.cmpcode=sa.cmpcode AND g.genoid=sa.reasonoid " & _
            '    "LEFT JOIN QL_mstacctg a ON a.cmpcode=g.cmpcode AND a.acctgoid=g.genother1 " & _
            '    "LEFT JOIN QL_mstacctg a2 ON a2.cmpcode=g.cmpcode AND a2.acctgoid=m.stockacctgoid " & _
            '    "WHERE sa.cmpcode=@cmpcode AND MONTH(sa.stockadjdate)=@bulan AND YEAR(sa.stockadjdate)=@tahun " & _
            '    "UNION ALL " & _
            '    "SELECT UPPER(sa.refname) refname,sa.stockadjno,sa.stockadjdate,sa.stockadjqty,i.itemoid oid,i.itemlongdesc deskripsi," & _
            '    "i.stockacctgoid,g.gendesc,g.genother1,a.acctgcode,a.acctgdesc,a2.acctgcode acctgcode2,a2.acctgdesc acctgdesc2 " & _
            '    "FROM QL_trnstockadjustment sa INNER JOIN QL_mstitem i ON i.cmpcode=sa.cmpcode AND i.itemoid=sa.refoid AND sa.refname='ITEM' " & _
            '    "INNER JOIN QL_mstgen g ON g.cmpcode=sa.cmpcode AND g.genoid=sa.reasonoid " & _
            '    "LEFT JOIN QL_mstacctg a ON a.cmpcode=g.cmpcode AND a.acctgoid=g.genother1 " & _
            '    "LEFT JOIN QL_mstacctg a2 ON a2.cmpcode=g.cmpcode AND a2.acctgoid=i.stockacctgoid " & _
            '    "WHERE sa.cmpcode=@cmpcode AND MONTH(sa.stockadjdate)=@bulan AND YEAR(sa.stockadjdate)=@tahun "
            sSql = "DECLARE @cmpcode VARCHAR(10);" & _
                "DECLARE @bulan INT;" & _
                "DECLARE @tahun INT;" & _
                "SET @cmpcode='" & Session("CompnyCode") & "';" & _
                "SET @bulan=" & DDLMonth.SelectedValue & ";" & _
                "SET @tahun=" & DDLYear.SelectedValue & ";" & _
                "SELECT UPPER(sa.refname) refname,sa.stockadjno,sa.stockadjdate,sa.stockadjqty,sa.stockadjamtidr AS stockadjamount,i.itemoid oid,i.itemlongdescription deskripsi," & _
                "gx.genother1 AS stockacctgoid,g.gendesc,g.genother1,a.acctgcode,a.acctgdesc,a2.acctgcode acctgcode2,a2.acctgdesc acctgdesc2 " & _
                "FROM QL_trnstockadj sa INNER JOIN QL_mstitem i ON i.cmpcode=sa.cmpcode AND i.itemoid=sa.refoid " & _
                "INNER JOIN QL_mstgen g ON g.cmpcode=sa.cmpcode AND g.genoid=sa.reasonoid " & _
                "INNER JOIN QL_mstgen gx ON gx.genoid=i.itemgroupoid " & _
                "INNER JOIN QL_mstacctg a ON a.cmpcode=g.cmpcode AND a.acctgoid=g.genother1 " & _
                "AND NOT (a.acctgcode LIKE '" & sVarDL & "%' OR a.acctgcode LIKE '" & sVarOHD & "%') " & _
                "LEFT JOIN QL_mstacctg a2 ON a2.cmpcode=g.cmpcode AND a2.acctgoid=gx.genother1 " & _
                "WHERE sa.cmpcode=@cmpcode AND MONTH(sa.stockadjdate)=@bulan AND YEAR(sa.stockadjdate)=@tahun "
            Dim dtAdjPost As DataTable = cKoneksi.ambiltabel(sSql, "AdjPost")

            For R1 As Integer = 0 To dtAdjPost.Rows.Count - 1
                If ToDecimal(dtAdjPost.Rows(R1)("stockacctgoid").ToString) = 0 Then
                    sMsg &= "- Invalid Setting Stock Account for " & dtAdjPost.Rows(R1)("refname").ToString & " " & dtAdjPost.Rows(R1)("deskripsi").ToString & ". <BR>"
                End If
                If ToDecimal(dtAdjPost.Rows(R1)("genother1").ToString) = 0 Then
                    sMsg &= "- Invalid Setting Account for ADJUSTMENT REASON " & dtAdjPost.Rows(R1)("gendesc").ToString & ". <BR>"
                End If

                Dim oRow As DataRow
                If ToDouble(dtAdjPost.Rows(R1)("stockadjqty").ToString) > 0 Then
                    ' STOCK vs BIAYA
                    oRow = tbPostGL.NewRow
                    oRow("tipe") = "STOCK ADJUSTMENT"
                    oRow("acctgoid") = dtAdjPost.Rows(R1)("stockacctgoid")
                    oRow("acctgcode") = dtAdjPost.Rows(R1)("acctgcode2")
                    oRow("acctgdesc") = dtAdjPost.Rows(R1)("acctgdesc2")
                    If dtAdjPost.Rows(R1)("refname").ToString.ToUpper = "ITEM" Then
                        If dtItemHPP.Select("refoid=" & dtAdjPost.Rows(R1)("oid")).Length > 0 Then
                            Dim oRowPrice As DataRow = dtItemHPP.Select("refoid=" & dtAdjPost.Rows(R1)("oid"))(0)
                            oRow("debet") = ToDouble(dtAdjPost.Rows(R1)("stockadjamount").ToString)
                        Else
                            oRow("debet") = 0
                        End If
                    Else
                        If dtPurch.Select("matoid=" & dtAdjPost.Rows(R1)("oid")).Length > 0 Then
                            Dim oRowPrice As DataRow = dtPurch.Select("matoid=" & dtAdjPost.Rows(R1)("oid"))(0)
                            oRow("debet") = ToDouble(dtAdjPost.Rows(R1)("stockadjamount").ToString)
                        Else
                            oRow("debet") = 0
                        End If
                    End If
                    oRow("credit") = 0
                    oRow("refno") = dtAdjPost.Rows(R1)("stockadjno").ToString
                    oRow("note") = "STOCK ADJUSTMENT NO. : " & dtAdjPost.Rows(R1)("stockadjno").ToString
                    tbPostGL.Rows.Add(oRow)

                    oRow = tbPostGL.NewRow
                    oRow("tipe") = "STOCK ADJUSTMENT"
                    oRow("acctgoid") = dtAdjPost.Rows(R1)("genother1")
                    oRow("acctgcode") = dtAdjPost.Rows(R1)("acctgcode")
                    oRow("acctgdesc") = dtAdjPost.Rows(R1)("acctgdesc")
                    oRow("debet") = 0
                    If dtAdjPost.Rows(R1)("refname").ToString.ToUpper = "ITEM" Then
                        If dtItemHPP.Select("refoid=" & dtAdjPost.Rows(R1)("oid")).Length > 0 Then
                            Dim oRowPrice As DataRow = dtItemHPP.Select("refoid=" & dtAdjPost.Rows(R1)("oid"))(0)
                            oRow("credit") = ToDouble(dtAdjPost.Rows(R1)("stockadjamount").ToString)
                        Else
                            oRow("credit") = 0
                        End If
                    Else
                        If dtPurch.Select("matoid=" & dtAdjPost.Rows(R1)("oid")).Length > 0 Then
                            Dim oRowPrice As DataRow = dtPurch.Select("matoid=" & dtAdjPost.Rows(R1)("oid"))(0)
                            oRow("credit") = ToDouble(dtAdjPost.Rows(R1)("stockadjamount").ToString)
                        Else
                            oRow("credit") = 0
                        End If
                    End If
                    oRow("refno") = dtAdjPost.Rows(R1)("stockadjno").ToString
                    oRow("note") = "STOCK ADJUSTMENT NO. : " & dtAdjPost.Rows(R1)("stockadjno").ToString
                    tbPostGL.Rows.Add(oRow)

                ElseIf ToDouble(dtAdjPost.Rows(R1)("stockadjqty").ToString) < 0 Then
                    ' BIAYA vs STOCK
                    oRow = tbPostGL.NewRow
                    oRow("tipe") = "STOCK ADJUSTMENT"
                    oRow("acctgoid") = dtAdjPost.Rows(R1)("genother1")
                    oRow("acctgcode") = dtAdjPost.Rows(R1)("acctgcode")
                    oRow("acctgdesc") = dtAdjPost.Rows(R1)("acctgdesc")
                    If dtAdjPost.Rows(R1)("refname").ToString.ToUpper = "ITEM" Then
                        If dtItemHPP.Select("refoid=" & dtAdjPost.Rows(R1)("oid")).Length > 0 Then
                            Dim oRowPrice As DataRow = dtItemHPP.Select("refoid=" & dtAdjPost.Rows(R1)("oid"))(0)
                            oRow("debet") = Math.Abs(ToDouble(dtAdjPost.Rows(R1)("stockadjqty"))) * ToDouble(oRowPrice("refhppakhir").ToString)
                        Else
                            oRow("debet") = 0
                        End If
                    Else
                        If dtPurch.Select("matoid=" & dtAdjPost.Rows(R1)("oid")).Length > 0 Then
                            Dim oRowPrice As DataRow = dtPurch.Select("matoid=" & dtAdjPost.Rows(R1)("oid"))(0)
                            oRow("debet") = Math.Abs(ToDouble(dtAdjPost.Rows(R1)("stockadjqty"))) * ToDouble(oRowPrice("akhirprice").ToString)
                        Else
                            oRow("debet") = 0
                        End If
                    End If
                    oRow("credit") = 0
                    oRow("refno") = dtAdjPost.Rows(R1)("stockadjno").ToString
                    oRow("note") = "STOCK ADJUSTMENT NO. : " & dtAdjPost.Rows(R1)("stockadjno").ToString
                    tbPostGL.Rows.Add(oRow)

                    oRow = tbPostGL.NewRow
                    oRow("tipe") = "STOCK ADJUSTMENT"
                    oRow("acctgoid") = dtAdjPost.Rows(R1)("stockacctgoid")
                    oRow("acctgcode") = dtAdjPost.Rows(R1)("acctgcode2")
                    oRow("acctgdesc") = dtAdjPost.Rows(R1)("acctgdesc2")
                    oRow("debet") = 0
                    If dtAdjPost.Rows(R1)("refname").ToString.ToUpper = "ITEM" Then
                        If dtItemHPP.Select("refoid=" & dtAdjPost.Rows(R1)("oid")).Length > 0 Then
                            Dim oRowPrice As DataRow = dtItemHPP.Select("refoid=" & dtAdjPost.Rows(R1)("oid"))(0)
                            oRow("credit") = Math.Abs(ToDouble(dtAdjPost.Rows(R1)("stockadjqty"))) * ToDouble(oRowPrice("refhppakhir").ToString)
                        Else
                            oRow("credit") = 0
                        End If
                    Else
                        If dtPurch.Select("matoid=" & dtAdjPost.Rows(R1)("oid")).Length > 0 Then
                            Dim oRowPrice As DataRow = dtPurch.Select("matoid=" & dtAdjPost.Rows(R1)("oid"))(0)
                            oRow("credit") = Math.Abs(ToDouble(dtAdjPost.Rows(R1)("stockadjqty"))) * ToDouble(oRowPrice("akhirprice").ToString)
                        Else
                            oRow("credit") = 0
                        End If
                    End If

                    oRow("refno") = dtAdjPost.Rows(R1)("stockadjno").ToString
                    oRow("note") = "STOCK ADJUSTMENT NO. : " & dtAdjPost.Rows(R1)("stockadjno").ToString
                    tbPostGL.Rows.Add(oRow)
                End If
            Next

            ' SALES RETURN
            'sSql = "DECLARE @cmpcode VARCHAR(10);" & _
            '    "DECLARE @bulan INT;" & _
            '    "DECLARE @tahun INT;" & _
            '    "SET @cmpcode='" & Session("CompnyCode") & "';" & _
            '    "SET @bulan=" & DDLMonth.SelectedValue & ";" & _
            '    "SET @tahun=" & DDLYear.SelectedValue & ";" & _
            '    "SELECT rm.trnretjualno,rm.trnretjualdate,rd.retjualdtlqty,m.itemoid,m.itemlongdesc,m.stockacctgoid,a.acctgcode,a.acctgdesc,0.0 totalretur " & _
            '    "FROM QL_trnretjualmst rm " & _
            '    "INNER JOIN QL_trnretjualdtl rd ON rd.cmpcode=rm.cmpcode AND rd.trnretjualmstoid=rm.trnretjualmstoid " & _
            '    "INNER JOIN QL_mstitem m ON m.cmpcode=rd.cmpcode AND m.itemoid=rd.itemoid " & _
            '    "LEFT JOIN QL_mstacctg a ON a.cmpcode=m.cmpcode AND a.acctgoid=m.stockacctgoid " & _
            '    "WHERE rm.cmpcode=@cmpcode AND MONTH(rm.trnretjualdate)=@bulan AND YEAR(rm.trnretjualdate)=@tahun"
            Dim dtRetPost As DataTable = Nothing 'cKoneksi.ambiltabel(sSql, "RetPost")
            ' Apply New HPP Price for Item 
            'For R1 As Integer = 0 To dtRetPost.Rows.Count - 1
            '    dvItemCek.RowFilter = "refoid=" & dtRetPost.Rows(R1)("itemoid")
            '    If dvItemCek.Count > 0 Then
            '        dtRetPost.Rows(R1)("totalretur") = Math.Round(ToDouble(dvItemCek(0)("refhppakhir").ToString) * ToDouble(dtRetPost.Rows(R1)("retjualdtlqty").ToString), 2, MidpointRounding.AwayFromZero)
            '    End If
            '    dvItemCek.RowFilter = ""
            '    dtRetPost.AcceptChanges()
            'Next

            'sSql = "SELECT rm.trnretjualno FROM QL_trnretjualmst rm WHERE rm.cmpcode='" & Session("CompnyCode") & "' AND MONTH(rm.trnretjualdate)=" & DDLMonth.SelectedValue & " AND YEAR(rm.trnretjualdate)=" & DDLYear.SelectedValue & " "
            'Dim tbRet As DataTable = cKoneksi.ambiltabel(sSql, "QL_trnjualmst")

            'Dim dvRetPost As DataView = dtRetPost.DefaultView
            'dTotHPPItem = 0

            'For R2 As Integer = 0 To tbRet.Rows.Count - 1
            '    dvRetPost.RowFilter = "trnretjualno='" & tbRet.Rows(R2)("trnretjualno").ToString & "'"
            '    dTotHPP = ToDouble(dtRetPost.Compute("SUM(totalretur)", "trnretjualno='" & tbRet.Rows(R2)("trnretjualno").ToString & "'").ToString)

            '    If dvRetPost.Count > 0 Then
            '        Dim oRow As DataRow

            '        For R3 As Integer = 0 To dvRetPost.Count - 1
            '            Dim dvCek As DataView = tbPostGL.DefaultView

            '            If ToDecimal(dvRetPost(R3)("stockacctgoid").ToString) = 0 Then
            '                sMsg &= "- Invalid Setting Stock Account for Item " & dvRetPost(R3)("itemlongdesc").ToString & ". <BR>"
            '            End If

            '            dvCek.RowFilter = "acctgoid=" & dvRetPost(R3)("stockacctgoid").ToString & " AND refno='" & tbRet.Rows(R2)("trnretjualno").ToString & "' "

            '            If dvCek.Count > 0 Then
            '                dvCek(0)("debet") += ToDouble(dvRetPost(R3)("totalretur").ToString)
            '                dvCek.RowFilter = ""
            '            Else
            '                dvCek.RowFilter = ""

            '                oRow = tbPostGL.NewRow
            '                oRow("tipe") = "SALES RETURN"
            '                oRow("acctgoid") = dvRetPost(R3)("stockacctgoid")
            '                oRow("acctgcode") = GetStrData("SELECT acctgcode FROM QL_mstacctg WHERE cmpcode='" & Session("CompnyCode") & "' AND acctgoid=" & dvRetPost(R3)("stockacctgoid"))
            '                oRow("acctgdesc") = GetStrData("SELECT acctgdesc FROM QL_mstacctg WHERE cmpcode='" & Session("CompnyCode") & "' AND acctgoid=" & dvRetPost(R3)("stockacctgoid"))
            '                oRow("debet") = ToDouble(dvRetPost(R3)("totalretur").ToString)
            '                oRow("credit") = 0
            '                oRow("refno") = tbRet.Rows(R2)("trnretjualno").ToString
            '                oRow("note") = "HPP - SALES RETURN NO. : " & tbRet.Rows(R2)("trnretjualno").ToString
            '                tbPostGL.Rows.Add(oRow)
            '            End If
            '            tbPostGL.AcceptChanges()
            '        Next

            '        If ToDecimal(iCoGS) = 0 Then
            '            sMsg &= "- Invalid Setting Account for Cost of Goods Sold. <BR>"
            '        End If

            '        oRow = tbPostGL.NewRow
            '        oRow("tipe") = "SALES RETURN"
            '        oRow("acctgoid") = iCoGS
            '        oRow("acctgcode") = GetStrData("SELECT acctgcode FROM QL_mstacctg WHERE cmpcode='" & Session("CompnyCode") & "' AND acctgoid=" & iCoGS)
            '        oRow("acctgdesc") = GetStrData("SELECT acctgdesc FROM QL_mstacctg WHERE cmpcode='" & Session("CompnyCode") & "' AND acctgoid=" & iCoGS)
            '        oRow("debet") = 0
            '        oRow("credit") = dTotHPP
            '        oRow("refno") = tbRet.Rows(R2)("trnretjualno").ToString
            '        oRow("note") = "HPP - SALES RETURN NO. : " & tbRet.Rows(R2)("trnretjualno").ToString
            '        tbPostGL.Rows.Add(oRow)
            '        tbPostGL.AcceptChanges()
            '    End If
            '    dvSIGL.RowFilter = ""
            'Next

            gvJournal.DataSource = tbPostGL
            gvJournal.DataBind()
            lblJournal.Visible = True

            Session("tbPostGL") = tbPostGL

            If sMsg <> "" Then
                lblError.Text = sMsg
                SetControlPosting(0) : Exit Sub
            Else
                SetControlPosting(2)
            End If

            sSql = "SELECT COUNT(-1) FROM QL_crdhpp WHERE cmpcode='" & Session("CompnyCode") & "' " & _
                "AND hppstatus='CLOSE' AND periodacctg='" & GetDateToPeriodAcctg(CDate(txtDate.Text)) & "'"
            If cKoneksi.ambilscalar(sSql) > 0 Then
                lblError.Text = "HPP Closing already executed for this period."
                SetControlPosting(0) : Exit Sub
            Else
                SetControlPosting(2)
            End If

            sSql = "SELECT COUNT(-1) FROM QL_crdstock WHERE cmpcode='" & Session("CompnyCode") & "' AND closingdate='1/1/1900' " & _
                "AND periodacctg='" & GetDateToPeriodAcctg(CDate(txtDate.Text)) & "'"
            If cKoneksi.ambilscalar(sSql) > 0 Then
                lblError.Text = "Please do Closing Stock for period " & GetDateToPeriodAcctg(CDate(txtDate.Text)) & " first."
                SetControlPosting(0) : Exit Sub
            Else
                SetControlPosting(2)
            End If
        Catch ex As Exception
            lblError.Text = ex.ToString
        End Try
    End Sub

    Protected Sub gvItemHPP_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItemHPP.PageIndexChanging
        gvItemHPP.PageIndex = e.NewPageIndex
        Dim dt As DataTable : dt = Session("tbItemHPP")
        gvItemHPP.DataSource = dt : gvItemHPP.DataBind()
    End Sub

    Protected Sub gvItemHPP_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvItemHPP.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 2)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 2)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 2)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 2)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 2)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 2)
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim sMsg As String = ""
        Dim sErrTemp As String = ""
        If Not IsValidDate(txtDate.Text, "MM/dd/yyyy", sErrTemp) Then
            sMsg &= "- Invalid date format: " & sErrTemp & " !<BR>"
        Else
            If Format(CDate(txtDate.Text), "MMMM yyyy") <> CDate(nextclosing.Text) Then
                sMsg &= "- Next HPP Closing date is " & nextclosing.Text & " !<BR>"
            End If
        End If

        If Session("tbItemHPP") Is Nothing Then
            sMsg &= "- No HPP Closing detail !<BR>"
        Else
            Dim tbCek As DataTable : tbCek = Session("tbItemHPP")
            If tbCek.Rows.Count < 1 Then sMsg &= "- No HPP Closing detail !<BR>"
        End If

        If sMsg <> "" Then
            lblError.Text = sMsg : Exit Sub
        Else
            lblError.Text = ""
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Dim seq As Decimal = 1
        Session("HPPID") = GenerateID("QL_crdhpp", Session("CompnyCode"))
        Session("HistID") = GenerateID("QL_acctgclosinghistory", Session("CompnyCode"))
        Dim vIDGLMst As Integer = GenerateID("QL_trnglmst", Session("CompnyCode"))
        Dim vIDGLDtl As Integer = GenerateID("QL_trngldtl", Session("CompnyCode"))

        Try
            Dim sqlex As String = ""
            ' Next Month Period
            Dim dNextMonth As Date = DateAdd(DateInterval.Month, 1, CDate(nextclosing.Text))

            If (Not Session("tbItemHPP") Is Nothing) And (Not Session("tbPurch") Is Nothing) Then
                Dim dt As DataTable = Session("tbItemHPP")

                ' SAVE DETAIL ITEM HPP
                For R1 As Decimal = 0 To dt.Rows.Count - 1
                    sSql = "UPDATE QL_crdhpp SET refqtyakhir=" & ToDouble(dt.Rows(R1)("refqtyakhir").ToString) & ",usagecost=" & ToDouble(dt.Rows(R1)("usagecost").ToString) & "," & _
                        "laborcost=" & ToDouble(dt.Rows(R1)("laborcost").ToString) & ",ohdcost=" & ToDouble(dt.Rows(R1)("ohdcost").ToString) & ",refhppakhir=" & ToDouble(dt.Rows(R1)("refhppakhir").ToString) & "," & _
                        "reftotalakhir=" & ToDouble(dt.Rows(R1)("reftotalakhir").ToString) & ",hppstatus='CLOSE',upduser='" & Session("UserID") & "',updtime=current_timestamp " & _
                        "WHERE cmpcode='" & Session("CompnyCode") & "' AND periodacctg='" & GetDateToPeriodAcctg(CDate(nextclosing.Text)) & "' " & _
                        "AND reftype='" & dt.Rows(R1)("reftype").ToString & "' AND refoid=" & dt.Rows(R1)("refoid").ToString
                    xCmd.CommandText = sSql
                    If xCmd.ExecuteNonQuery() <= 0 Then
                        sSql = "INSERT INTO QL_crdhpp (cmpcode,hppoid,periodacctg,reftype,refoid,refqty,refunit,usagecost,laborcost,ohdcost,refhpp,reftotal,refqtyakhir,refhppakhir,reftotalakhir,hppstatus,crtuser,crttime,upduser,updtime) VALUES " & _
                            "('" & Session("CompnyCode") & "','" & Session("HPPID") & "','" & GetDateToPeriodAcctg(CDate(nextclosing.Text)) & "'," & _
                            "'" & dt.Rows(R1)("reftype").ToString & "'," & dt.Rows(R1)("refoid").ToString & "," & ToDouble(dt.Rows(R1)("refqty").ToString) & "," & _
                            "" & dt.Rows(R1)("refunit").ToString & "," & ToDouble(dt.Rows(R1)("usagecost").ToString) & "," & ToDouble(dt.Rows(R1)("laborcost").ToString) & "," & _
                            "" & ToDouble(dt.Rows(R1)("ohdcost").ToString) & "," & ToDouble(dt.Rows(R1)("refhpp").ToString) & "," & ToDouble(dt.Rows(R1)("reftotal").ToString) & "," & _
                            "" & ToDouble(dt.Rows(R1)("refqtyakhir").ToString) & "," & ToDouble(dt.Rows(R1)("refhppakhir").ToString) & "," & ToDouble(dt.Rows(R1)("reftotalakhir").ToString) & "," & _
                            "'CLOSE','" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp)"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Session("HPPID") += 1 : seq += 1
                    End If

                    ' Next Month
                    sSql = "INSERT INTO QL_crdhpp (cmpcode,hppoid,periodacctg,reftype,refoid,refqty,refunit,usagecost,laborcost,ohdcost,refhpp,reftotal,refqtyakhir,refhppakhir,reftotalakhir,hppstatus,crtuser,crttime,upduser,updtime) VALUES " & _
                        "('" & Session("CompnyCode") & "','" & Session("HPPID") & "','" & GetDateToPeriodAcctg(dNextMonth) & "'," & _
                        "'" & dt.Rows(R1)("reftype").ToString & "'," & dt.Rows(R1)("refoid").ToString & "," & ToDouble(dt.Rows(R1)("refqtyakhir").ToString) & "," & _
                        "" & dt.Rows(R1)("refunit").ToString & ",0,0,0," & ToDouble(dt.Rows(R1)("refhppakhir").ToString) & "," & ToDouble(dt.Rows(R1)("reftotalakhir").ToString) & "," & _
                        "" & ToDouble(dt.Rows(R1)("refqtyakhir").ToString) & "," & ToDouble(dt.Rows(R1)("refhppakhir").ToString) & "," & ToDouble(dt.Rows(R1)("reftotalakhir").ToString) & "," & _
                        "'OPEN','" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Session("HPPID") += 1 : seq += 1
                Next

                Dim dtMat As DataTable
                dtMat = Session("tbPurch")

                ' SAVE DETAIL MATERIAL HPP
                For R2 As Decimal = 0 To dtMat.Rows.Count - 1
                    sSql = "UPDATE QL_crdhpp SET refqtyakhir=" & ToDouble(dtMat.Rows(R2)("akhirqty").ToString) & ",usagecost=0,laborcost=0,ohdcost=0,refhppakhir=" & ToDouble(dtMat.Rows(R2)("akhirprice").ToString) & "," & _
                        "reftotalakhir=" & ToDouble(dtMat.Rows(R2)("akhirprice").ToString) * ToDouble(dtMat.Rows(R2)("akhirqty").ToString) & ",hppstatus='CLOSE',upduser='" & Session("UserID") & "',updtime=current_timestamp " & _
                        "WHERE cmpcode='" & Session("CompnyCode") & "' AND periodacctg='" & GetDateToPeriodAcctg(CDate(nextclosing.Text)) & "' " & _
                        "AND reftype='MATERIAL' AND refoid=" & dtMat.Rows(R2)("matoid").ToString
                    xCmd.CommandText = sSql
                    If xCmd.ExecuteNonQuery() <= 0 Then
                        sSql = "INSERT INTO QL_crdhpp (cmpcode,hppoid,periodacctg,reftype,refoid,refqty,refunit,usagecost,laborcost,ohdcost,refhpp,reftotal,refqtyakhir,refhppakhir,reftotalakhir,hppstatus,crtuser,crttime,upduser,updtime) VALUES " & _
                            "('" & Session("CompnyCode") & "','" & Session("HPPID") & "','" & GetDateToPeriodAcctg(CDate(nextclosing.Text)) & "'," & _
                            "'MATERIAL'," & dtMat.Rows(R2)("matoid").ToString & "," & ToDouble(dtMat.Rows(R2)("awalqty").ToString) & "," & _
                            "" & dtMat.Rows(R2)("matunit").ToString & ",0,0,0," & ToDouble(dtMat.Rows(R2)("awalprice").ToString) & "," & _
                            "" & ToDouble(dtMat.Rows(R2)("awalprice").ToString) * ToDouble(dtMat.Rows(R2)("awalqty").ToString) & "," & _
                            "" & ToDouble(dtMat.Rows(R2)("akhirqty").ToString) & "," & ToDouble(dtMat.Rows(R2)("akhirprice").ToString) & "," & _
                            "" & ToDouble(dtMat.Rows(R2)("akhirprice").ToString) * ToDouble(dtMat.Rows(R2)("akhirqty").ToString) & "," & _
                            "'CLOSE','" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp)"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Session("HPPID") += 1 : seq += 1
                    End If

                    ' Next Month
                    sSql = "INSERT INTO QL_crdhpp (cmpcode,hppoid,periodacctg,reftype,refoid,refqty,refunit,usagecost,laborcost,ohdcost,refhpp,reftotal,refqtyakhir,refhppakhir,reftotalakhir,hppstatus,crtuser,crttime,upduser,updtime) VALUES " & _
                        "('" & Session("CompnyCode") & "','" & Session("HPPID") & "','" & GetDateToPeriodAcctg(dNextMonth) & "'," & _
                        "'MATERIAL'," & dtMat.Rows(R2)("matoid").ToString & "," & ToDouble(dtMat.Rows(R2)("akhirqty").ToString) & "," & _
                        "" & dtMat.Rows(R2)("matunit").ToString & ",0,0,0," & ToDouble(dtMat.Rows(R2)("akhirprice").ToString) & "," & _
                        "" & ToDouble(dtMat.Rows(R2)("akhirqty").ToString) * ToDouble(dtMat.Rows(R2)("akhirprice").ToString) & "," & _
                        "" & ToDouble(dtMat.Rows(R2)("akhirqty").ToString) & "," & ToDouble(dtMat.Rows(R2)("akhirprice").ToString) & "," & _
                        "" & ToDouble(dtMat.Rows(R2)("akhirqty").ToString) * ToDouble(dtMat.Rows(R2)("akhirprice").ToString) & "," & _
                        "'OPEN','" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Session("HPPID") += 1 : seq += 1
                Next

               
                ' UPDATE OTHER TO BE CLOSE
                sSql = "UPDATE QL_crdhpp SET hppstatus='CLOSE',upduser='" & Session("UserID") & "',updtime=current_timestamp " & _
                    "WHERE cmpcode='" & Session("CompnyCode") & "' AND periodacctg='" & GetDateToPeriodAcctg(CDate(nextclosing.Text)) & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "INSERT INTO QL_acctgclosinghistory (cmpcode,historyclosingoid,historyopendate,historyopenuserid,historyopentime,historyclosingdate,historyclosinguserid,historyclosingtime," & _
                    "closingperiod,closingstatus,closingnote,closinggroup) VALUES " & _
                    "('" & Session("CompnyCode") & "','" & Session("HistID") & "','" & CDate(txtDate.Text) & "','" & Session("UserID") & "','" & CDate(txtDate.Text) & "'," & _
                    "'" & CDate(txtDate.Text) & "','" & Session("UserID") & "','" & CDate(txtDate.Text) & "','" & GetDateToPeriodAcctg(CDate(txtDate.Text)) & "','CLOSE','HPP CLOSING - " & CDate(txtDate.Text) & "','HPP')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Else
                objTrans.Rollback() : xCmd.Connection.Close()
                lblError.Text = "No data saved. Empty Material or Item detail." : Exit Sub
            End If

            sSql = "update ql_mstOid set lastoid='" & Session("HPPID") - 1 & "' where tablename='QL_crdhpp' and cmpcode='" & Session("CompnyCode") & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "update ql_mstOid set lastoid='" & Session("HistID") & "' where tablename='QL_acctgclosinghistory' and cmpcode='" & Session("CompnyCode") & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Dim tbPostGL As DataTable
            tbPostGL = Session("tbPostGL")

            ' ============= INSERT GL DTL 
            If tbPostGL.Rows.Count > 0 Then
                ' INSERT INTO QL_trnglmst
                sSql = "INSERT into QL_trnglmst (cmpcode,glmstoid,gldate,periodacctg,refname,refoid,glnote,glstatus,postdate,crtuser,crttime,upduser,updtime) VALUES " & _
                    "('" & Session("CompnyCode") & "'," & vIDGLMst & ",'" & CDate(txtDate.Text) & "','" & GetDateToPeriodAcctg(CDate(txtDate.Text)) & "'," & _
                    "'QL_crdhpp',0,'CLOSING HPP - PERIODE " & GetDateToPeriodAcctg(CDate(txtDate.Text)) & "','POST',current_timestamp," & _
                    "'" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid = " & vIDGLMst & " WHERE tablename = 'QL_TRNGLMST' AND cmpcode = '" & Session("CompnyCode") & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                For j As Integer = 0 To tbPostGL.Rows.Count - 1
                    Dim sDbCr As String = ""
                    Dim dAmt As Decimal = 0
                    If ToDouble(tbPostGL.Rows(j)("debet").ToString) <> 0 Then
                        sDbCr = "D"
                        dAmt = ToDouble(tbPostGL.Rows(j)("debet").ToString)
                    ElseIf ToDouble(tbPostGL.Rows(j)("credit").ToString) <> 0 Then
                        sDbCr = "C"
                        dAmt = ToDouble(tbPostGL.Rows(j)("credit").ToString)
                    End If

                    sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,glmstoid,gldtlseq,acctgoid,gldbcr,glamt,noref,glnote,glres1,glres2,crtuser,crttime,upduser,updtime) VALUES " & _
                        "('" & Session("CompnyCode") & "', " & vIDGLDtl & ", " & vIDGLMst & ", " & j + 1 & ", " & tbPostGL.Rows(j)("acctgoid") & ", '" & sDbCr & "', " & dAmt & "," & _
                        "'', '" & tbPostGL.Rows(j)("note") & "', '', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    vIDGLDtl += 1
                Next

                sSql = "UPDATE QL_mstoid SET lastoid = " & (vIDGLDtl - 1) & " WHERE tablename = 'QL_TRNGLDTL' AND cmpcode = '" & Session("CompnyCode") & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                'Else
                '    lblError.Text = "Missing Auto Jurnal Detail !<br />Data cannot be saved !"
                '    objTrans.Rollback()
                '    If conn.State = ConnectionState.Open Then
                '        conn.Close()
                '    End If
                '    Exit Sub
            End If
            ' ============= INSERT GL DTL END

            objTrans.Commit() : xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback() : xCmd.Connection.Close()
            lblError.Text = ex.ToString : Exit Sub
        End Try
        Response.Redirect("trnHPPClosing.aspx?awal=true")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("trnHPPClosing.aspx?awal=true")
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        Dim dtMst As DataTable : dtMst = Session("TbDtlPost")
        gvMst.DataSource = dtMst
        gvMst.DataBind()
    End Sub

    Protected Sub gvMst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvMst.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If DataBinder.Eval(e.Row.DataItem, "refno") = "1" Or DataBinder.Eval(e.Row.DataItem, "refno") = "2" Then
                e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 2)
                e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 2)
                If DataBinder.Eval(e.Row.DataItem, "refno") = "1" Then
                    e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 2)
                Else
                    e.Row.Cells(5).Text = ""
                End If
            Else
                e.Row.Cells(3).Text = ""
                e.Row.Cells(4).Text = ""
                e.Row.Cells(5).Text = ""
            End If
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 2)
        End If
    End Sub

    Protected Sub imbUnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cProc.SetModalPopUpExtender(beUnPOST, pnlUnPOST, mpeUnPOST, True)
    End Sub

    Protected Sub imbOKUnPOST_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblUnPOSTMsg.Text = ""
        sSql = "SELECT COUNT(-1) FROM QL_mstprof WHERE cmpcode='" & Session("CompnyCode") & "' AND userid='" & DDLUser.SelectedValue & "' AND profpass='" & Tchar(tbPwd.Text) & "'"
        If cKoneksi.ambilscalar(sSql) > 0 Then
            If ToDouble(lblGLIdUnPost.Text) <= 0 Then
                lblUnPOSTMsg.Text = "Data Daily Posting tidak valid !!"
                mpeUnPOST.Show() : Exit Sub
            End If

            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans

            Try
                ' DELETE Dtl
                sSql = "DELETE FROM QL_trngldtl WHERE cmpcode='" & Session("CompnyCode") & "' AND glmstoid='" & lblGLIdUnPost.Text & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                ' DELETE Mst
                sSql = "DELETE FROM QL_trnglmst WHERE cmpcode='" & Session("CompnyCode") & "' AND glmstoid='" & lblGLIdUnPost.Text & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                objTrans.Commit() : xCmd.Connection.Close()
            Catch ex As Exception
                objTrans.Rollback() : xCmd.Connection.Close()
                lblUnPOSTMsg.Text = ex.Message : mpeUnPOST.Show() : Exit Sub
            End Try
            Response.Redirect("trnHPPClosing.aspx?awal=true")
        Else
            lblUnPOSTMsg.Text = "User / Password salah !!"
            mpeUnPOST.Show()
        End If
    End Sub

    Protected Sub imbCancelUnPOST_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cProc.SetModalPopUpExtender(beUnPOST, pnlUnPOST, mpeUnPOST, False)
    End Sub

    Protected Sub DDLMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""

        gvPurchase.DataSource = Nothing : gvPurchase.DataBind() : gvPurchase.PageIndex = 0 : lbPurch.Text = "" : lbPurch.Visible = False
        gvUsage.DataSource = Nothing : gvUsage.DataBind() : gvUsage.PageIndex = 0 : lbProd.Text = "" : lbProd.Visible = False
        gvItemHPP.DataSource = Nothing : gvItemHPP.DataBind() : gvItemHPP.PageIndex = 0 : lbhpp.Visible = False
        gvNewAvg.DataSource = Nothing : gvNewAvg.DataBind() : gvNewAvg.PageIndex = 0 : lbNewAvg.Visible = False
        gvCOGS.DataSource = Nothing : gvCOGS.DataBind() : gvCOGS.PageIndex = 0 : lblCOGS.Visible = False
        gvJournal.DataSource = Nothing : gvJournal.DataBind() : gvJournal.PageIndex = 0 : lblJournal.Visible = False

        Session("tbPurch") = Nothing
        Session("tbUsage") = Nothing
        Session("tbItemHPP") = Nothing
        Session("tbCOGS") = Nothing
        Session("tbPostGL") = Nothing

        txtDate.Text = Format(New Date(DDLYear.SelectedValue, DDLMonth.SelectedValue, Date.DaysInMonth(DDLYear.SelectedValue, DDLMonth.SelectedValue)), "MM/dd/yyyy")
    End Sub

    Protected Sub DDLYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""

        gvPurchase.DataSource = Nothing : gvPurchase.DataBind() : gvPurchase.PageIndex = 0 : lbPurch.Text = "" : lbPurch.Visible = False
        gvUsage.DataSource = Nothing : gvUsage.DataBind() : gvUsage.PageIndex = 0 : lbProd.Text = "" : lbProd.Visible = False
        gvItemHPP.DataSource = Nothing : gvItemHPP.DataBind() : gvItemHPP.PageIndex = 0 : lbhpp.Visible = False
        gvNewAvg.DataSource = Nothing : gvNewAvg.DataBind() : gvNewAvg.PageIndex = 0 : lbNewAvg.Visible = False
        gvCOGS.DataSource = Nothing : gvCOGS.DataBind() : gvCOGS.PageIndex = 0 : lblCOGS.Visible = False
        gvJournal.DataSource = Nothing : gvJournal.DataBind() : gvJournal.PageIndex = 0 : lblJournal.Visible = False

        Session("tbPurch") = Nothing
        Session("tbUsage") = Nothing
        Session("tbItemHPP") = Nothing
        Session("tbCOGS") = Nothing
        Session("tbPostGL") = Nothing

        txtDate.Text = Format(New Date(DDLYear.SelectedValue, DDLMonth.SelectedValue, Date.DaysInMonth(DDLYear.SelectedValue, DDLMonth.SelectedValue)), "MM/dd/yyyy")
    End Sub

    Protected Sub gvPurchase_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPurchase.PageIndexChanging
        gvPurchase.PageIndex = e.NewPageIndex
        Dim dt As DataTable : dt = Session("tbPurch")
        gvPurchase.DataSource = dt : gvPurchase.DataBind()
    End Sub

    Protected Sub gvPurchase_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPurchase.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 2)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 2)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 2)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 2)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 2)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 2)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 2)
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 2)
            e.Row.Cells(10).Text = ToMaskEdit(ToDouble(e.Row.Cells(10).Text), 2)
        End If
    End Sub

    Protected Sub gvUsage_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvUsage.PageIndexChanging
        gvUsage.PageIndex = e.NewPageIndex
        Dim dt As DataTable : dt = Session("tbUsage")
        gvUsage.DataSource = dt : gvUsage.DataBind()
    End Sub

    Protected Sub gvUsage_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvUsage.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim sTempCode As String = ""
            sTempCode = Session("CodeB4")
            Session("CodeB4") = e.Row.Cells(0).Text
            If sTempCode = e.Row.Cells(0).Text Then
                e.Row.Cells(0).Text = "" : e.Row.Cells(1).Text = "" : e.Row.Cells(6).Text = ""
            Else
                e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 2)
            End If
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 2)
            e.Row.Cells(10).Text = ToMaskEdit(ToDouble(e.Row.Cells(10).Text), 2)
        End If
    End Sub

    Protected Sub gvNewAvg_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvNewAvg.PageIndexChanging
        gvNewAvg.PageIndex = e.NewPageIndex
        Dim dt As DataTable : dt = Session("tbItemHPP")
        gvNewAvg.DataSource = dt : gvNewAvg.DataBind()
    End Sub

    Protected Sub gvNewAvg_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvNewAvg.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 2)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 2)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 2)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 2)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 2)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 2)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 2)
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 2)
            e.Row.Cells(10).Text = ToMaskEdit(ToDouble(e.Row.Cells(10).Text), 2)
            e.Row.Cells(11).Text = ToMaskEdit(ToDouble(e.Row.Cells(11).Text), 2)
            e.Row.Cells(12).Text = ToMaskEdit(ToDouble(e.Row.Cells(12).Text), 2)
            e.Row.Cells(13).Text = ToMaskEdit(ToDouble(e.Row.Cells(13).Text), 2)
        End If
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        Dim dt As DataTable : dt = Session("tbPostGL")
        gvJournal.DataSource = dt : gvJournal.DataBind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 2)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 2)
        End If
    End Sub
#End Region

End Class
