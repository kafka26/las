Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Accounting_trnPayAR
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
    Const iMask = 4, iRoundDigit = 4
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsDiffDetailInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If ToInteger(diffacctgoid.SelectedValue) <= 0 Then
            sError &= "- Please select DIFF. ACCOUNT field!<BR>"
        End If
        If diffamount.Text = "" Then
            sError &= "- Please fill A/R DIFF. AMOUNT field!<BR>"
        Else
            If ToDouble(diffamount.Text) <= 0 Then
                sError &= "- A/R DIFF. AMOUNT must be more than 0!<BR>"
            Else
                'If ToDouble(diffamount.Text) > ToDouble(arbalance.Text) Then
                '    sError &= "- A/R DIFF. AMOUNT must be less or equal than A/R BALANCE!<BR>"
                'End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If aritemmstoid.Text = "" Then
            sError &= "- Please select A/R NO. field!<BR>"
        End If
        If payaramt.Text = "" Then
            sError &= "- Please fill A/R PAYMENT AMT. field!<BR>"
        Else
            If ToDouble(payaramt.Text) <= 0 Then
                sError &= "- A/R PAYMENT AMT. must be more than 0!<BR>"
            Else
                If ToDouble(payaramt.Text) < ToDouble(arbalance.Text) Then
                    If ToDouble(subtotalpayment.Text) > ToDouble(arbalance.Text) Then
                        sError &= "- TOTAL DIFFERENCE must be less or equal than " & ToMaskEdit(ToDouble(arbalance.Text) - ToDouble(payaramt.Text), iMask) & " (A/R BALANCE - A/R PAYMENT AMT.)!<BR>"
                    End If
                ElseIf ToDouble(payaramt.Text) > ToDouble(arbalance.Text) Then
                    If ToDouble(subtotalpayment.Text) - ToDouble(totaldiff.Text) <> ToDouble(arbalance.Text) Then
                        sError &= "- TOTAL DIFFERENCE must be " & ToMaskEdit(ToDouble(subtotalpayment.Text) - ToDouble(arbalance.Text), iMask) & " (SUB TOTAL PAYMENT - A/R BALANCE)!<BR>"
                    End If
                End If
            End If
        End If
        If arcurroid.SelectedValue <> "" And curroid.SelectedValue <> "" Then
            If arcurroid.SelectedValue <> curroid.SelectedValue Then
                If payaramtother.Text = "" Then
                    sError &= "- Please fill PAYMENT AMT. field!<BR>"
                Else
                    If ToDouble(payaramtother.Text) <= 0 Then
                        sError &= "- PAYMENT AMT. must be more than 0!<BR>"
                    End If
                End If
            End If
        End If
        'If CDate(cashbankdate.Text) < CDate(ardate.Text) Then
        '    sError &= "- PAYMENT DATE < INVOICE DATE..!<BR>"
        'End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If cashbankdate.Text = "" Then
            sErr = "None"
            sError &= "- Please fill PAYMENT DATE field!<BR>"
        Else
            If Not IsValidDate(cashbankdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- PAYMENT DATE is invalid. " & sErr & "<BR>"
            Else
                If CDate(cashbankdate.Text) > Today Then
                    sError &= "- PAYMENT Date must less or equal than TODAY!<BR>"
                End If
            End If
        End If
        If custoid.Text = "" Then
            sError &= "- Please select SUPPLIER field!<BR>"
        End If
        If curroid.SelectedValue = "" Then
            sError &= "- Please select CURRENCY field!<BR>"
        End If
        If ToDouble(payapnett.Text) <= 0 Then
            sError &= "- TOTAL PAYMENT must be more than 0!<BR>"
        Else
            If cashbanktype.SelectedValue = "BLM" Then
                If ToDouble(payapnett.Text) > ToDouble(dparamt.Text) Then
                    sError &= "- TOTAL PAYMENT must be less than DP AMOUNT!<BR>"
                End If
            End If
        End If
        If acctgoid.SelectedValue = "" Then
            sError &= "- Please select PAYMENT ACCOUNT field!<BR>"
        End If
        Dim sErr2 As String = ""
        If cashbanktype.SelectedValue = "BBM" Or cashbanktype.SelectedValue = "BGM" Then
            If cashbankrefno.Text = "" Then
                sError &= "- Please fill REF. NO. field!<BR>"
            End If
            If cashbankduedate.Text = "" Then
                sErr2 = "None"
                sError &= "- Please fill DUE DATE field!<BR>"
            Else
                If Not IsValidDate(cashbankduedate.Text, "MM/dd/yyyy", sErr2) Then
                    sError &= "- DUE DATE is invalid. " & sErr & "<BR>"
                Else
                    If sErr = "" Then
                        If CDate(cashbankdate.Text) > CDate(cashbankduedate.Text) Then
                            sError &= "- DUE DATE must be more or equal than PAYMENT DATE!<BR>"
                        End If
                    End If
                End If
            End If
            If cashbanktype.SelectedValue = "BGM" Then
                If cashbanktakegiro.Text = "" Then
                    sError &= "- Please fill DATE TAKE GIRO field!<BR>"
                Else
                    Dim sErrGiro As String = ""
                    If Not IsValidDate(cashbanktakegiro.Text, "MM/dd/yyyy", sErrGiro) Then
                        sError &= "- DATE TAKE GIRO is invalid. " & sErrGiro & "<BR>"
                    End If
                End If
            End If
        ElseIf cashbanktype.SelectedValue = "BLM" Then
            If giroacctgoid.Text = "" Then
                sError &= "- Please select DP NO. field!<BR>"
            Else
                Dim sDPDate As String = GetStrData("SELECT CONVERT(VARCHAR(10), dpardate, 101) dpardate FROM QL_trndpar WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dparoid=" & giroacctgoid.Text)
                If CDate(cashbankdate.Text) < CDate(sDPDate) Then
                    sError &= "- PAYMENT DATE must be more or equal than DP DATE (DP DATE is " & sDPDate & ")!<BR>"
                End If
            End If
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill Detail Data!<BR>"
        Else
            Dim dtTbl As DataTable = Session("TblDtl")
            If dtTbl.Rows.Count <= 0 Then
                sError &= "- Please fill Detail Data!<BR>"
            Else
                Dim sCode As String = "" : Dim sRefType As String = "" : Dim sRefColOid As String = "" : Dim sRefDate As String = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    If Not sCode.Contains(dtTbl.Rows(C1)("arcurrcode").ToString) Then
                        sCode &= dtTbl.Rows(C1)("arcurrcode").ToString & ","
                    End If
                    If dtTbl.Rows(C1)("payarflag").ToString = "" Then
                        Dim dr() As DataRow = dtTbl.Select("dtlseq=" & dtTbl.Rows(C1)("dtlseq") & " AND payarflag='Kurang Bayar'")
                        If dr.Length > 0 Then
                            If ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) > ToDouble(dtTbl.Rows(C1)("arbalance").ToString) Then
                                sError &= "- TOTAL DIFFERENCE for A/R NO. " & dtTbl.Rows(C1)("aritemno").ToString & " must be less or equal than " & ToMaskEdit(ToDouble(dtTbl.Rows(C1)("arbalance").ToString) - ToDouble(dtTbl.Rows(C1)("payaramt").ToString), iMask) & " (A/R BALANCE - A/R PAYMENT AMT.)!<BR>"
                            End If
                        Else
                            dr = dtTbl.Select("dtlseq=" & dtTbl.Rows(C1)("dtlseq") & " AND payarflag='Lebih Bayar'")
                            If dr.Length > 0 Then
                                If ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString) <> ToDouble(dtTbl.Rows(C1)("arbalance").ToString) Then
                                    sError &= "- TOTAL DIFFERENCE for A/R NO. " & dtTbl.Rows(C1)("aritemno").ToString & " must be " & ToMaskEdit(ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("arbalance").ToString), iMask) & " (SUB TOTAL PAYMENT - A/R BALANCE)!<BR>"
                                End If
                            End If
                        End If
                    End If
                    sRefType = dtTbl.Rows(C1)("reftype").ToString
                    If sRefType.ToUpper = "QL_TRNJUALMST" Then
                        sRefColOid = "trnjualmstoid" : sRefDate = "trnjualdate"
                    ElseIf sRefType.ToUpper = "QL_TRNTRANSITEMMST" Then
                        sRefColOid = "transitemmstoid" : sRefDate = "transitemdate"
                    End If
                    sSql = "SELECT CONVERT(VARCHAR(10), " & sRefDate & ",101) date FROM " & sRefType & " WHERE cmpcode='" & CompnyCode & "' AND " & sRefColOid & "=" & dtTbl.Rows(C1)("aritemmstoid").ToString
                    If CDate(cashbankdate.Text) < CDate(GetStrData(sSql)) Then
                        sError &= "- PAYMENT DATE must more or equal than INVOICE DATE! # " & dtTbl.Rows(C1)("aritemno").ToString & " #|(" & GetStrData(sSql) & ")<BR>"
                    End If
                Next
                If sCode <> "" Then
                    sCode = Left(sCode, sCode.Length - 1)
                    Dim sSplit() As String = sCode.Split(",")
                    If sSplit.Length > 1 Then
                        sError &= "- Every detail data must have same Currency!<BR>"
                    End If
                End If
            End If
        End If
        'If addacctgoid1.SelectedValue <> 0 Then
        '    If ToDouble(addacctgamt1.Text) <= 0 Then
        '        sError &= "- Additional Cost Amount 1 must be greater than 0!<BR>"
        '    End If
        'End If
        If addacctgoid2.SelectedValue <> 0 Then
            'If ToDouble(addacctgamt2.Text) <= 0 Then
            '    sError &= "- Additional Cost Amount 2 must be greater than 0!<BR>"
            'End If
            If addacctgoid1.SelectedValue <> 0 Then
                If addacctgoid1.SelectedValue = addacctgoid2.SelectedValue Then
                    sError &= "- Additional Cost 1 and Additional Cost 2 must be in different account!<BR>"
                End If
            End If
        End If
        If addacctgoid3.SelectedValue <> 0 Then
            'If ToDouble(addacctgamt3.Text) <= 0 Then
            '    sError &= "- Additional Cost Amount 3 must be greater than 0!<BR>"
            'End If
            If addacctgoid1.SelectedValue <> 0 Then
                If addacctgoid1.SelectedValue = addacctgoid3.SelectedValue Then
                    sError &= "- Additional Cost 1 and Additional Cost 3 must be in different account!<BR>"
                End If
            End If
            If addacctgoid2.SelectedValue <> 0 Then
                If addacctgoid2.SelectedValue = addacctgoid3.SelectedValue Then
                    sError &= "- Additional Cost 2 and Additional Cost 3 must be in different account!<BR>"
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            cashbankstatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function GetSumDetail() As DataTable
        Dim dtRet As DataTable = New DataTable()
        dtRet.Columns.Add("acctgoid", Type.GetType("System.Int32"))
        dtRet.Columns.Add("acctgamt", Type.GetType("System.Double"))
        dtRet.Columns.Add("acctgflag", Type.GetType("System.String"))
        dtRet.Columns.Add("acctgamtidr", Type.GetType("System.Double"))
        dtRet.Columns.Add("acctgamtusd", Type.GetType("System.Double"))
        dtRet.Columns.Add("acctgamtother", Type.GetType("System.Double"))
        If Session("TblDtl") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            'dt.Columns.Add("payaramtpostidr", Type.GetType("System.Double"))
            'dt.Columns.Add("payaramtpostusd", Type.GetType("System.Double"))
            Dim sOid As String = ""
            For C1 As Integer = 0 To dt.Rows.Count - 1
                Dim sPlusMinus As String = "(#)"
                If dt.Rows(C1)("payarflag").ToString.ToUpper = "KURANG BAYAR" Then
                    sPlusMinus = "(-)"
                ElseIf dt.Rows(C1)("payarflag").ToString.ToUpper = "LEBIH BAYAR" Then
                    sPlusMinus = "(+)"
                End If
                If Not sOid.Contains(sPlusMinus & "[" & dt.Rows(C1)("aracctgoid").ToString & "]") Then
                    sOid &= sPlusMinus & "[" & dt.Rows(C1)("aracctgoid").ToString & "],"
                End If
                'dt.Rows(C1)("payaramtpostidr") = ToDouble(dt.Rows(C1)("payaramtpost").ToString) * ToDouble(dt.Rows(C1)("arratetoidr").ToString)
                'dt.Rows(C1)("payaramtpostusd") = ToDouble(dt.Rows(C1)("payaramtpost").ToString) * ToDouble(dt.Rows(C1)("arratetousd").ToString)
            Next
            If sOid <> "" Then
                sOid = Left(sOid, sOid.Length - 1).Replace("[", "").Replace("]", "")
                Dim sSplit() As String = sOid.Split(",")
                For C1 As Integer = 0 To sSplit.Length - 1
                    Dim dr As DataRow = dtRet.NewRow
                    Dim iOid As Integer = CInt(Right(sSplit(C1), sSplit(C1).Length - 3))
                    dr("acctgoid") = iOid
                    dr("acctgamt") = Math.Abs(ToDouble(dt.Compute("SUM(payaramtpost)", "aracctgoid=" & iOid & " AND payarflag='" & IIf(Left(sSplit(C1), 3) = "(#)", "", IIf(Left(sSplit(C1), 3) = "(+)", "Lebih Bayar", "Kurang Bayar")) & "'").ToString))
                    dr("acctgflag") = Left(sSplit(C1), 3)
                    dr("acctgamtidr") = Math.Abs(ToDouble(dt.Compute("SUM(payaramtpost)", "aracctgoid=" & iOid & " AND payarflag='" & IIf(Left(sSplit(C1), 3) = "(#)", "", IIf(Left(sSplit(C1), 3) = "(+)", "Lebih Bayar", "Kurang Bayar")) & "'").ToString))
                    dr("acctgamtusd") = Math.Abs(ToDouble(dt.Compute("SUM(payaramtpost)", "aracctgoid=" & iOid & " AND payarflag='" & IIf(Left(sSplit(C1), 3) = "(#)", "", IIf(Left(sSplit(C1), 3) = "(+)", "Lebih Bayar", "Kurang Bayar")) & "'").ToString))
                    dr("acctgamtother") = Math.Abs(ToDouble(dt.Compute("SUM(payaramtother)", "aracctgoid=" & iOid & " AND payarflag='" & IIf(Left(sSplit(C1), 3) = "(#)", "", IIf(Left(sSplit(C1), 3) = "(+)", "Lebih Bayar", "Kurang Bayar")) & "'").ToString))
                    dtRet.Rows.Add(dr)
                Next
            End If
        End If
        Return dtRet
    End Function

    Private Function GetRateAR() As Integer
        GetRateAR = 0
        If Session("TblDtl") IsNot Nothing Then
            If Session("TblDtl").Rows.Count > 0 Then
                GetRateAR = CInt(Session("TblDtl").Rows(0)("arcurroid").ToString)
            End If
        End If
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trncashbankmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND cashbankstatus='In Process' AND cashbankgroup='AR'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Accounting\trnPayAR.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If GetStrData(sSql) > 0 Then
            lbInProcess.Visible = True
            lbInProcess.Text = "You have " & GetStrData(sSql) & " In Process A/R Payment data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitAllDDL()
        ' Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If FillDDL(DDLBusUnit, sSql) Then
            InitDDLCOARayment()
        End If
        ' Fill DDL Currency
        sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        If FillDDL(curroid, sSql) Then
            ShowCurrSymbol(curroid.SelectedValue, 0)
        End If
        sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        If FillDDL(arcurroid, sSql) Then
            ShowCurrSymbol(arcurroid.SelectedValue, 1)
        End If

        InitDDLAdd()
    End Sub

    Private Sub InitDDLAdd()
        ' COA ADDITIONAL 1,2,3
        FillDDLAcctg(addacctgoid1, "VAR_ADD_COST", DDLBusUnit.SelectedValue, "0", "NONE")
        If addacctgoid1.Items.Count < 1 Then FillDDL(addacctgoid1, "SELECT 0 AS value,'NONE' AS text ")
        addacctgoid1_SelectedIndexChanged(addacctgoid1, Nothing)
        FillDDLAcctg(addacctgoid2, "VAR_ADD_COST", DDLBusUnit.SelectedValue, "0", "NONE")
        If addacctgoid2.Items.Count < 1 Then FillDDL(addacctgoid2, "SELECT 0 AS value,'NONE' AS text ")
        addacctgoid2_SelectedIndexChanged(addacctgoid2, Nothing)
        FillDDLAcctg(addacctgoid3, "VAR_ADD_COST", DDLBusUnit.SelectedValue, "0", "NONE")
        If addacctgoid3.Items.Count < 1 Then FillDDL(addacctgoid3, "SELECT 0 AS value,'NONE' AS text ")
        addacctgoid3_SelectedIndexChanged(addacctgoid3, Nothing)
    End Sub

    Private Sub ShowCurrSymbol(ByVal sCurr As String, ByVal iType As Int16)
        sSql = "SELECT (CASE WHEN currsymbol IS NULL THEN currcode WHEN currsymbol='' THEN currcode ELSE currsymbol END) AS currsymbol FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND curroid=" & sCurr
        If iType = 0 Then
            lblPayment.Text = GetStrData(sSql)
        Else
            lblARPayment.Text = GetStrData(sSql)
            lblDiffPayment.Text = lblARPayment.Text
        End If
        If curroid.SelectedValue.ToString <> arcurroid.SelectedValue.ToString Then
            payaramtother.Enabled = True : payaramtother.CssClass = "inpText"
        Else
            payaramtother.Enabled = False : payaramtother.CssClass = "inpTextDisabled"
        End If
    End Sub

    Private Sub InitDDLCOARayment()
        Dim sVar As String = "VAR_CASH"
        Dim bVal As Boolean = False
        Dim bVal2 As Boolean = True
        Dim bVal3 As Boolean = False
        Dim sCss As String = "inpText"
        Dim sText As String = "Ref. No."
        Dim sText2 As String = "Due Date"
        giroacctgoid.Text = "" : cashbankrefno.Text = "" : cashbankduedate.Text = "" : dparamt.Text = ""
        If cashbanktype.SelectedValue = "BBM" Then
            sVar = "VAR_BANK" : bVal = True : bVal3 = True
        ElseIf cashbanktype.SelectedValue = "BGM" Then
            sVar = "VAR_BANK" : bVal = True : bVal3 = True
        ElseIf cashbanktype.SelectedValue = "BLM" Then
            sVar = "VAR_DP_AR" : bVal2 = False : sCss = "inpTextDisabled" : sText = "DP No." : sText2 = "DP Amount" : bVal3 = True
        End If
        ' Fill DDL COA Payment
        FillDDLAcctg(acctgoid, sVar, DDLBusUnit.SelectedValue)
        lblWarnDueDate.Visible = bVal : cashbankduedate.Visible = bVal : imbDueDate.Visible = bVal : lblInfoDueDate.Visible = bVal : cashbankduedate.Text = ""
        lblDueDate.Visible = bVal3 : lblSeptDueDate.Visible = bVal3 : lblWarnRefNo.Visible = bVal3
        acctgoid.Enabled = bVal2 : acctgoid.CssClass = sCss : btnSearchDP.Visible = Not bVal2 : btnClearDP.Visible = Not bVal2 : cashbankrefno.Enabled = bVal2 : cashbankrefno.CssClass = sCss : dparamt.Visible = Not bVal2
        lblRefNo.Text = sText : lblDueDate.Text = sText2
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT cashbankoid, cashbankno, CONVERT(VARCHAR(10), cashbankdate, 101) AS cashbankdate, custname, (CASE cashbanktype WHEN 'BKM' THEN 'CASH' WHEN 'BBM' THEN 'TRANSFER' WHEN 'BGM' THEN 'GIRO/CHEQUE' WHEN 'BLM' THEN 'DOWN PAYMENT' ELSE '' END) AS cashbanktype, cashbankstatus, cashbanknote, divname, 'False' AS checkvalue FROM QL_trncashbankmst cb INNER JOIN QL_mstcust s ON custoid=personoid INNER JOIN QL_mstdivision div ON div.cmpcode=cb.cmpcode WHERE cashbankgroup='AR' AND cashbanktype<>'BOM'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cb.cmpcode='" & Session("CompnyCode") & "'"
        End If
        sSql &= sSqlPlus & " ORDER BY cb.cashbankoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trncashbankmst")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "cashbankoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub BindCustomerData()
        sSql = "SELECT DISTINCT ar.custoid,c.custcode,c.custname,c.custaddr,c.custtype FROM QL_conar ar INNER JOIN QL_mstcust c ON c.custoid=ar.custoid WHERE ar.cmpcode='" & DDLBusUnit.SelectedValue & "' AND ISNULL((SELECT par.payarres1 FROM QL_trnpayar par WHERE par.cmpcode=ar.cmpcode AND par.payaroid=ar.payrefoid),'')<>'Lebih Bayar' AND " & FilterDDLListCust.SelectedValue & " LIKE '%" & Tchar(FilterTextListCust.Text) & "%' GROUP BY ar.custoid,c.custcode,c.custname,c.custaddr,c.custtype HAVING SUM(ar.amttrans)>SUM(ar.amtbayar) ORDER BY custcode"
        FillGV(gvListCust, sSql, "QL_mstcust")
    End Sub

    Private Sub InitDDLDiffAcctg()
        Dim sVar As String = ""
        If diffflag.SelectedIndex = 0 Then
            sVar = "VAR_LEBIH_BAYAR_AR"
        Else
            sVar = "VAR_KURANG_BAYAR_AR"
        End If
        FillDDLAcctg(diffacctgoid, sVar, DDLBusUnit.SelectedValue, "0", "-")
    End Sub

    Private Sub CreateTblDtlDiff()
        Dim dtlTable As DataTable = New DataTable("TabelARPaymentDiff")
        dtlTable.Columns.Add("diffseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("diffflag", Type.GetType("System.String"))
        dtlTable.Columns.Add("diffacctgoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("diffaccount", Type.GetType("System.String"))
        dtlTable.Columns.Add("diffamount", Type.GetType("System.Double"))
        dtlTable.Columns.Add("diffamountother", Type.GetType("System.Double"))
        dtlTable.Columns.Add("diffnote", Type.GetType("System.String"))
        Session("TblDtlDiff") = dtlTable
    End Sub

    Private Sub ClearDetailDiff()
        diffseq.Text = "1"
        If Session("TblDtlDiff") Is Nothing = False Then
            Dim objTable As DataTable = Session("TblDtlDiff")
            diffseq.Text = objTable.Rows.Count + 1
        End If
        i_u3.Text = "New Detail"
        diffacctgoid.SelectedIndex = -1
        diffamount.Text = ""
        diffamountother.Text = ""
        diffnote.Text = ""
        gvDtlDiff.Columns(0).Visible = True
        gvDtlDiff.Columns(gvDtlDiff.Columns.Count - 1).Visible = True
        gvDtlDiff.SelectedIndex = -1
    End Sub

    Private Sub CountTotalDiff()
        Dim dVal As Double = 0
        If Session("TblDtlDiff") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtlDiff")
            For C1 As Integer = 0 To dt.Rows.Count - 1
                dVal += ToDouble(dt.Rows(C1)("diffamount").ToString)
            Next
        End If
        totaldiff.Text = ToMaskEdit(dVal, iMask)
        If diffflag.SelectedIndex = 0 Then
            subtotalpayment.Text = ToMaskEdit(ToDouble(payaramt.Text), iMask)
        Else
            subtotalpayment.Text = ToMaskEdit(ToDouble(payaramt.Text) + ToDouble(totaldiff.Text), iMask)
        End If
    End Sub

    Private Sub BindDPData()
        sSql = "SELECT dp.dparoid, dp.dparno, (CONVERT(VARCHAR(10), dp.dpardate, 101)) AS dpardate, dp.acctgoid AS dparacctgoid, a.acctgdesc, dp.dparnote, (dp.dparamt - ISNULL(dp.dparaccumamt, 0.0)) AS dparamt FROM QL_trndpar dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid /*LEFT JOIN QL_trncashbankmst cb ON cb.cmpcode=dp.cmpcode AND cb.cashbankoid=dp.cashbankoid*/ WHERE dp.cmpcode='" & DDLBusUnit.SelectedValue & "' AND " & FilterDDLListDP.SelectedValue & " LIKE '%" & Tchar(FilterTextListDP.Text) & "%' AND dp.dparstatus='Post' AND dp.custoid=" & custoid.Text & " /*AND dp.curroid=" & curroid.SelectedValue & "*/ AND dp.dparamt > dp.dparaccumamt ORDER BY dp.dparoid"
        FillGV(gvListDP, sSql, "QL_trndpar")
    End Sub

    Private Sub BindARData()
        Dim sAdd As String = ""
        If Session("oid") IsNot Nothing And Session("oid") <> "" Then
            sAdd = " AND pay.cashbankoid<>" & Session("oid")
        End If
        sSql = "SELECT * FROM ("
        sSql &= "SELECT con.cmpcode, con.refoid aroid,con.reftype, CASE reftype WHEN 'QL_trnjualmst' THEN (SELECT trnjualno FROM QL_trnjualmst m WHERE m.trnjualmstoid=refoid) WHEN 'QL_trntransitemmst' THEN (SELECT transitemno FROM QL_trntransitemmst m WHERE m.transitemmstoid=refoid) END arno,(CONVERT(VARCHAR(10), con.trnardate,101)) AS ardate, con.amttrans argrandtotal, '' arnote, '' armstres1,(ISNULL((SELECT SUM(ar.amtbayar) FROM QL_conar ar LEFT JOIN QL_trnpayar pay ON ar.cmpcode=pay.cmpcode AND ar.payrefoid=pay.payaroid AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' WHERE ar.payrefoid<>0 AND ar.cmpcode=con.cmpcode AND ar.reftype=con.reftype AND ar.refoid=con.refoid),0.0)) AS arpaidamt,con.acctgoid AS aracctgoid,(a.acctgcode+' - '+a.acctgdesc) AS araccount,(CONVERT(VARCHAR(10), con.trnardate,101)) AS ardatetakegiro, 1 curroid,1 AS arratetoidr, 1 AS arratetousd, con.custoid FROM QL_conar con INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE ISNULL(con.payrefoid,0)=0 AND con.cmpcode='" & DDLBusUnit.SelectedValue & "' AND con.custoid=" & custoid.Text & " "
        If cashbanktakegiro.Text <> "" Then
            'sSql &= " AND CONVERT(DATETIME, arm.datetakegiro)<=CONVERT(DATETIME, '" & cashbanktakegiro.Text & " 23:59:59')"
        End If
        sSql &= ") dt WHERE " & FilterDDLListAR.SelectedValue & " LIKE '%" & Tchar(FilterTextListAR.Text) & "%' ORDER BY aroid DESC"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_conar")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = "arpaidamt<argrandtotal"
        gvListAR.DataSource = dv.ToTable
        gvListAR.DataBind()
    End Sub

    Private Sub CreateTblDtl()
        Dim dtlTable As DataTable = New DataTable("TabelARPayment")
        dtlTable.Columns.Add("dtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("aritemmstoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("aritemno", Type.GetType("System.String"))
        dtlTable.Columns.Add("aracctgoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("araccount", Type.GetType("System.String"))
        dtlTable.Columns.Add("aritemgrandtotal", Type.GetType("System.Double"))
        dtlTable.Columns.Add("arpaidamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("arbalance", Type.GetType("System.Double"))
        dtlTable.Columns.Add("payaramt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("arcurroid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("arcurrcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("payaramtother", Type.GetType("System.Double"))
        dtlTable.Columns.Add("payarnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("subtotalpayment", Type.GetType("System.Double"))
        dtlTable.Columns.Add("payarflag", Type.GetType("System.String"))
        dtlTable.Columns.Add("totaldiff", Type.GetType("System.Double"))
        dtlTable.Columns.Add("payaramtpost", Type.GetType("System.Double"))
        dtlTable.Columns.Add("arratetoidr", Type.GetType("System.Double"))
        dtlTable.Columns.Add("arratetousd", Type.GetType("System.Double"))
        dtlTable.Columns.Add("reftype", Type.GetType("System.String"))
        dtlTable.Columns.Add("salescode", Type.GetType("System.String"))
        dtlTable.Columns.Add("komisi", Type.GetType("System.Double"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub ClearDetail()
        dtlseq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                dtlseq.Text = objTable.Rows.Count + 1
                EnableHeader(False)
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        i_u2.Text = "New Detail"
        aritemmstoid.Text = ""
        aritemno.Text = ""
        aracctgoid.Text = ""
        araccount.Text = ""
        aritemgrandtotal.Text = ""
        arpaidamt.Text = ""
        arbalance.Text = ""
        payaramt.Text = ""
        arcurroid.SelectedIndex = -1
        ShowCurrSymbol(arcurroid.SelectedValue, 1)
        arratetoidr.Text = ""
        arratetousd.Text = ""
        payaramtother.Text = ""
        payaramt_TextChanged(Nothing, Nothing)
        payarnote.Text = ""
        subtotalpayment.Text = ""
        totaldiff.Text = ""
        gvDtl.Columns(0).Visible = True
        gvDtl.Columns(gvDtl.Columns.Count - 1).Visible = True
        gvDtl.SelectedIndex = -1
        Session("TblDtlDiff") = Nothing
        gvDtlDiff.DataSource = Nothing : gvDtlDiff.DataBind()
        ClearDetailDiff()
        Session("LastFlagIndex") = Nothing
        ardate.Text = ""
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal : DDLBusUnit.CssClass = sCss
        btnSearchCust.Visible = bVal : btnClearCust.Visible = bVal
        'cashbanktakegiro.Enabled = bVal : cashbanktakegiro.CssClass = sCss : imbDTG.Visible = bVal
    End Sub

    Private Sub CountTotalPayment()
        Dim dVal As Double = 0, dValOther As Double = 0
        If Session("TblDtl") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            For C1 As Integer = 0 To dt.Rows.Count - 1
                If ToDouble(dt.Rows(C1)("payaramtother").ToString) > 0 Then
                    dValOther += ToDouble(dt.Rows(C1)("payaramtother").ToString)
                Else
                    dVal += ToDouble(dt.Rows(C1)("payaramtpost").ToString)
                End If
            Next
        End If
        If dValOther > 0 Then
            cashbankamt.Text = ToMaskEdit(dValOther, iMask)
        Else
            cashbankamt.Text = ToMaskEdit(dVal, iMask)
        End If
    End Sub

    Private Sub ReAmountCost()
        payapnett.Text = ToMaskEdit(ToDouble(cashbankamt.Text) + ToDouble(addacctgamt1.Text) + ToDouble(addacctgamt2.Text) + ToDouble(addacctgamt3.Text), iMask)
    End Sub

    Private Sub generateNo()
        If DDLBusUnit.SelectedValue <> "" Then
            If cashbankdate.Text <> "" Then
                Dim sErr As String = ""
                If IsValidDate(cashbankdate.Text, "MM/dd/yyyy", sErr) Then
                    If acctgoid.SelectedValue <> "" Then
                        Dim sNo As String = cashbanktype.SelectedValue & "-" & Format(CDate(cashbankdate.Text), "yyMM") & "-"
                        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, " & DefaultFormatCounter & ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankno LIKE '" & sNo & "%' --AND acctgoid=" & acctgoid.SelectedValue
                        If GetStrData(sSql) = "" Then
                            cashbankno.Text = GenNumberString(sNo, "", 1, DefaultFormatCounter)
                        Else
                            cashbankno.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT cb.cmpcode, cashbankoid, cb.periodacctg, cashbankdate, cashbankno, personoid, custname, custtype, cb.curroid, cashbankamt, cashbanktype, cb.acctgoid, cashbankrefno, cashbankduedate, cashbanknote, cashbankstatus, cb.createuser, cb.createtime, cb.upduser, cb.updtime, cashbanktakegiro, ISNULL(giroacctgoid, 0) AS giroacctgoid, (CASE cashbanktype WHEN 'BLM' THEN (ISNULL((SELECT dparamt - ISNULL(dp.dparaccumamt, 0.0) FROM QL_trndpar dp WHERE dp.cmpcode=cb.cmpcode AND dp.dparoid=ISNULL(cb.giroacctgoid, 0)), 0.0) + cashbankamt) ELSE 0.0 END) AS cashbankdpp, cb.addacctgoid1, cb.addacctgamt1, cb.addacctgoid2, cb.addacctgamt2, cb.addacctgoid3, cb.addacctgamt3 FROM QL_trncashbankmst cb INNER JOIN QL_mstcust s ON custoid=personoid WHERE cb.cashbankoid=" & sOid
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = xreader("cmpcode").ToString
                cashbankoid.Text = xreader("cashbankoid").ToString
                periodacctg.Text = xreader("periodacctg").ToString
                cashbankdate.Text = Format(xreader("cashbankdate"), "MM/dd/yyyy")
                cashbankno.Text = xreader("cashbankno").ToString
                custoid.Text = xreader("personoid").ToString
                custname.Text = xreader("custname").ToString
                custtype.Text = xreader("custtype").ToString
                curroid.SelectedValue = xreader("curroid").ToString
                ShowCurrSymbol(curroid.SelectedValue, 0)
                cashbanktype.SelectedValue = xreader("cashbanktype").ToString
                cashbanktype_SelectedIndexChanged(Nothing, Nothing)
                acctgoid.SelectedValue = xreader("acctgoid").ToString
                cashbankrefno.Text = xreader("cashbankrefno").ToString
                cashbankduedate.Text = Format(xreader("cashbankduedate"), "MM/dd/yyyy")
                cashbanknote.Text = xreader("cashbanknote").ToString
                cashbankstatus.Text = xreader("cashbankstatus").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
                cashbanktakegiro.Text = Format(xreader("cashbanktakegiro"), "MM/dd/yyyy")
                If cashbanktakegiro.Text = "01/01/1900" Then
                    cashbanktakegiro.Text = ""
                End If
                giroacctgoid.Text = xreader("giroacctgoid").ToString
                Session("lastdparoid") = giroacctgoid.Text
                dparamt.Text = ToMaskEdit(ToDouble(xreader("cashbankdpp").ToString), iMask)
                Session("lastcashbanktype") = cashbanktype.SelectedValue
                addacctgoid1.SelectedValue = xreader("addacctgoid1").ToString
                addacctgoid1_SelectedIndexChanged(addacctgoid1, Nothing)
                addacctgamt1.Text = ToMaskEdit(ToDouble(Trim(xreader("addacctgamt1").ToString)), iMask)
                addacctgoid2.SelectedValue = xreader("addacctgoid2").ToString
                addacctgoid2_SelectedIndexChanged(addacctgoid2, Nothing)
                addacctgamt2.Text = ToMaskEdit(ToDouble(Trim(xreader("addacctgamt2").ToString)), iMask)
                addacctgoid3.SelectedValue = xreader("addacctgoid3").ToString
                addacctgoid3_SelectedIndexChanged(addacctgoid3, Nothing)
                addacctgamt3.Text = ToMaskEdit(ToDouble(Trim(xreader("addacctgamt3").ToString)), iMask)
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            xreader.Close()
            conn.Close()
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False : btnPost.Visible = False : btnShowCOA.Visible = False : EnableHeader(False)
            Exit Sub
        End Try
        curroid.Enabled = False : curroid.CssClass = "inpTextDisabled"
        cashbanktype.Enabled = False : cashbanktype.CssClass = "inpTextDisabled"
        acctgoid.Enabled = False : acctgoid.CssClass = "inpTextDisabled"
        cashbankdate.Enabled = False : cashbankdate.CssClass = "inpTextDisabled" : imbPayDate.Visible = False : lblPayDate.Visible = False
        If cashbankstatus.Text = "Post" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnPost.Visible = False
            btnAddToList.Visible = False
            gvDtl.Columns(0).Visible = False
            gvDtl.Columns(gvDtl.Columns.Count - 1).Visible = False
            btnShowCOA.Visible = True
        End If
        sSql = "SELECT pay.refoid aritemmstoid, CASE con.reftype WHEN 'QL_trnjualmst' THEN (SELECT trnjualno FROM QL_trnjualmst m WHERE m.trnjualmstoid=con.refoid) WHEN 'QL_trntransitemmst' THEN (SELECT transitemno FROM QL_trntransitemmst m WHERE m.transitemmstoid=con.refoid) END aritemno, pay.acctgoid AS aracctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS araccount, con.amttrans aritemgrandtotal,(ISNULL((SELECT SUM(ar.amtbayar) FROM QL_conar ar INNER JOIN QL_trnpayar pay2 ON ar.cmpcode=pay2.cmpcode AND ar.payrefoid=pay2.payaroid AND pay2.cashbankoid<>pay.cashbankoid WHERE ar.payrefoid<>0 AND ar.cmpcode=pay.cmpcode AND ar.reftype=pay.reftype AND ar.refoid=pay.refoid AND ISNULL(pay2.payarres1, '')<>'Lebih Bayar' AND ar.trnartype LIKE 'PAYAR%'), 0.0)+ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 WHERE ar2.payrefoid<>0 AND pay.cmpcode=ar2.cmpcode AND pay.reftype=ar2.reftype AND pay.refoid=ar2.refoid AND ar2.trnartype IN ('DNAR','CNAR', 'SRET')),0.0)) AS arpaidamt, pay.payaramt, 1 AS arcurroid, 'IDR' AS arcurrcode, pay.payaramtother, pay.payarnote, 0.0 AS arbalance, 0.0 AS subtotalpayment, 0.0 AS totaldiff,0.0 AS payaramtpost, 1 AS arratetoidr, 1 AS arratetousd,pay.reftype FROM QL_trnpayar pay INNER JOIN QL_conar con ON con.refoid=pay.refoid AND con.reftype=pay.reftype AND con.payrefoid=0 INNER JOIN QL_mstacctg a ON a.acctgoid=pay.acctgoid WHERE pay.cashbankoid=" & sOid & " AND ISNULL(pay.payarres1, '')='' "
        Dim dt1 As DataTable = cKon.ambiltabel(sSql, "QL_trnpayar1")

        ' Old Query Kurang Lebih Bayar
        sSql = "SELECT pay.refoid aritemmstoid, pay.reftype, pay.acctgoid AS aracctgoid, (a.acctgcode + ' - ' + a.acctgdesc) AS araccount,pay.payaramt, pay.payaramtother, pay.payarnote, ISNULL(pay.payarres1, '') AS payarflag FROM QL_trnpayar pay INNER JOIN QL_mstacctg a ON a.acctgoid=pay.acctgoid WHERE pay.cashbankoid=" & sOid & " AND ISNULL(pay.payarres1, '')<>''"
        Dim dt2 As DataTable = cKon.ambiltabel(sSql, "QL_trnpayar2")
        Dim dv As DataView = dt2.DefaultView

        CreateTblDtl()
        Dim objTbl As DataTable = Session("TblDtl")
        For C1 As Integer = 0 To dt1.Rows.Count - 1
            Dim dr As DataRow = objTbl.NewRow
            dr("dtlseq") = C1 + 1
            dr("aritemmstoid") = dt1.Rows(C1)("aritemmstoid")
            dr("aritemno") = dt1.Rows(C1)("aritemno").ToString
            dr("aracctgoid") = dt1.Rows(C1)("aracctgoid")
            dr("araccount") = dt1.Rows(C1)("araccount").ToString
            dr("aritemgrandtotal") = ToDouble(dt1.Rows(C1)("aritemgrandtotal").ToString)
            dr("arpaidamt") = ToDouble(dt1.Rows(C1)("arpaidamt").ToString)
            dr("arbalance") = ToDouble(dt1.Rows(C1)("aritemgrandtotal").ToString) - ToDouble(dt1.Rows(C1)("arpaidamt").ToString)
            Dim dDiff As Double = 0
            dv.RowFilter = "aritemmstoid=" & dt1.Rows(C1)("aritemmstoid").ToString
            For C2 As Integer = 0 To dv.Count - 1
                If dv(C2)("payarflag").ToString = "Kurang Bayar" Then
                    dDiff += -ToDouble(dv(C2)("payaramt").ToString)
                Else
                    dDiff += ToDouble(dv(C2)("payaramt").ToString)
                End If
            Next
            dv.RowFilter = ""
            If dDiff < 0 Then
                dr("payaramt") = ToDouble(dt1.Rows(C1)("payaramt").ToString)
            Else
                dr("payaramt") = ToDouble(dt1.Rows(C1)("payaramt").ToString) + dDiff
            End If
            dr("arcurroid") = dt1.Rows(C1)("arcurroid")
            dr("arcurrcode") = dt1.Rows(C1)("arcurrcode").ToString
            dr("payaramtother") = ToDouble(dt1.Rows(C1)("payaramtother").ToString)
            dr("payarnote") = dt1.Rows(C1)("payarnote").ToString
            dr("subtotalpayment") = ToDouble(dt1.Rows(C1)("payaramt").ToString) + Math.Abs(dDiff)
            dr("payarflag") = ""
            dr("totaldiff") = Math.Abs(dDiff)
            If dDiff < 0 Then
                dr("payaramtpost") = ToDouble(dt1.Rows(C1)("payaramt").ToString) + Math.Abs(dDiff)
            Else
                dr("payaramtpost") = ToDouble(dt1.Rows(C1)("payaramt").ToString)
            End If
            dr("arratetoidr") = ToDouble(dt1.Rows(C1)("arratetoidr").ToString)
            dr("arratetousd") = ToDouble(dt1.Rows(C1)("arratetousd").ToString)
            dr("reftype") = dt1.Rows(C1)("reftype").ToString
            objTbl.Rows.Add(dr)

            dv.RowFilter = "aritemmstoid=" & dt1.Rows(C1)("aritemmstoid").ToString & " AND reftype='" & dt1.Rows(C1)("reftype").ToString & "'"
            For C2 As Integer = 0 To dv.Count - 1
                dr = objTbl.NewRow
                dr("dtlseq") = C1 + 1
                dr("aritemmstoid") = dv(C2)("aritemmstoid")
                dr("aritemno") = dt1.Rows(C1)("aritemno").ToString
                dr("aracctgoid") = dv(C2)("aracctgoid")
                dr("araccount") = dv(C2)("araccount").ToString
                dr("aritemgrandtotal") = ToDouble(dt1.Rows(C1)("aritemgrandtotal").ToString)
                dr("arpaidamt") = ToDouble(dt1.Rows(C1)("arpaidamt").ToString)
                dr("arbalance") = ToDouble(dt1.Rows(C1)("aritemgrandtotal").ToString) - ToDouble(dt1.Rows(C1)("arpaidamt").ToString)
                dr("payaramt") = Math.Abs(ToDouble(dv(C2)("payaramt").ToString))
                dr("arcurroid") = dt1.Rows(C1)("arcurroid")
                dr("arcurrcode") = dt1.Rows(C1)("arcurrcode").ToString
                dr("payaramtother") = ToDouble(dv(C2)("payaramtother").ToString)
                dr("payarnote") = dv(C2)("payarnote").ToString
                dr("subtotalpayment") = Math.Abs(ToDouble(dv(C2)("payaramt").ToString))
                dr("payarflag") = dv(C2)("payarflag").ToString
                dr("totaldiff") = 0
                If dv(C2)("payarflag").ToString = "Kurang Bayar" Then
                    dr("payaramtpost") = -ToDouble(dv(C2)("payaramt").ToString)
                Else
                    dr("payaramtpost") = ToDouble(dv(C2)("payaramt").ToString)
                End If
                dr("arratetoidr") = ToDouble(dt1.Rows(C1)("arratetoidr").ToString)
                dr("arratetousd") = ToDouble(dt1.Rows(C1)("arratetousd").ToString)
                dr("reftype") = dt1.Rows(C1)("reftype").ToString
                objTbl.Rows.Add(dr)
            Next
            dv.RowFilter = ""
        Next
        objTbl.AcceptChanges()
        Session("TblDtl") = objTbl
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        ClearDetail()
        CountTotalPayment()
        ReAmountCost()
        Session("lastdparamt") = ToDouble(payapnett.Text)
    End Sub

    Private Sub ShowReport()
        Dim UserName As String
        sSql = "SELECT profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"
        UserName = GetStrData(sSql)
        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("cashbankoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            report.Load(Server.MapPath(folderReport & "rptPaymentARAll.rpt"))
            Dim sWhere As String = "WHERE cashbankgroup='AR'"
            If Session("CompnyCode") <> CompnyCode Then
                sWhere = " AND cb.cmpcode='" & Session("CompnyCode") & "'"
            End If
            If sOid = "" Then
                sWhere &= " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND cashbankdate>='" & FilterPeriod1.Text & " 00:00:00' AND cashbankdate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbType.Checked Then
                    sWhere &= " AND cashbanktype='" & FilterDDLType.SelectedValue & "'"
                End If
                If cbStatus.Checked Then
                    sWhere &= " AND cashbankstatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Accounting\trnPayAR.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND cb.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sWhere &= " AND cb.cashbankoid IN (" & sOid & ")"
            End If
            report.SetParameterValue("userID", Session("UserID"))
            report.SetParameterValue("userName", UserName)
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "ARPaymentAllPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            report.Close()
            report.Dispose()
        End Try
        Response.Redirect("~\Accounting\trnPayAR.aspx?awal=true")
    End Sub

    Private Sub GenerateDPARNo()
        Dim sNo As String = "DPAR-" & Format(CDate(cashbankdate.Text), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(dparno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trndpar WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dparno LIKE '%" & sNo & "%'"
        dparno.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
    End Sub

    Private Sub GenerateCBNo2()
        If DDLBusUnit.SelectedValue <> "" Then
            If cashbankdate.Text <> "" Then
                Dim sErr As String = ""
                If IsValidDate(cashbankdate.Text, "MM/dd/yyyy", sErr) Then
                    If acctgoid.SelectedValue <> "" Then
                        Dim sNo As String = cashbanktype.SelectedValue & "-" & Format(CDate(cashbankdate.Text), "yyMM") & "-"
                        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankno LIKE '" & sNo & "%' --AND acctgoid=" & acctgoid.SelectedValue
                        If GetStrData(sSql) = "" Then
                            cashbankno2.Text = GenNumberString(sNo, "", 1, DefaultFormatCounter)
                        Else
                            cashbankno2.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
                        End If
                    End If
                End If
            End If
        End If
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Accounting\trnPayAR.aspx")
        End If
        If checkPagePermission("~\Accounting\trnPayAR.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - A/R Payment"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPost.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            CheckStatus()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitAllDDL()
            btnShowCOA.Visible = False
            'Check Rate Pajak & Standart
            Dim cRate As New ClassRate()
            cRate.SetRateValue(1, Format(GetServerTime(), "MM/dd/yyyy"))
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, 2) : Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, 2) : Exit Sub
            End If
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                cashbankoid.Text = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
                cashbankdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                cashbankstatus.Text = "In Process"
                periodacctg.Text = GetDateToPeriodAcctg(CDate(cashbankdate.Text))
                generateNo()
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        Dim dt As New DataTable
        If Not Session("TblDtl") Is Nothing Then
            dt = Session("TblDtl")
            gvDtl.DataSource = dt
            gvDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Accounting\trnPayAR.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lbInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = 0
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbType.Checked = False
        FilterDDLType.SelectedIndex = -1
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, cb.updtime, GETDATE()) > " & nDays & " AND cashbankstatus='In Process'"
        If checkPagePermission("~\Accounting\trnPayAR.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND cb.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND cashbankdate>='" & FilterPeriod1.Text & " 00:00:00' AND cashbankdate<='" & FilterPeriod2.Text & "'"
            Else
                Exit Sub
            End If
        End If
        If cbType.Checked Then
            sSqlPlus &= " AND cashbanktype='" & FilterDDLType.SelectedValue & "'"
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND cashbankstatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Accounting\trnPayAR.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND cb.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        cbStatus.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbType.Checked = False
        FilterDDLType.SelectedIndex = -1
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Accounting\trnPayAR.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND cb.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        btnClearCust_Click(Nothing, Nothing)
        cashbanktype.SelectedIndex = -1
        cashbanktype_SelectedIndexChanged(Nothing, Nothing)
        InitDDLAdd()
    End Sub

    Protected Sub cashbankdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cashbankdate.TextChanged
        If Session("oid") Is Nothing Or Session("oid") = "" Then
            generateNo()
        End If
    End Sub

    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchCust.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        FilterDDLListCust.SelectedIndex = -1 : FilterTextListCust.Text = "" : gvListCust.SelectedIndex = -1
        BindCustomerData()
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
    End Sub

    Protected Sub btnClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearCust.Click
        custoid.Text = "" : custname.Text = "" : custtype.Text = "" : btnClearAR_Click(Nothing, Nothing)
        If cashbanktype.SelectedValue = "BLM" Then
            btnClearDP_Click(Nothing, Nothing)
        End If
    End Sub

    Protected Sub btnFindListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListCust.Click
        BindCustomerData()
        mpeListCust.Show()
    End Sub

    Protected Sub btnAllListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListCust.Click
        FilterDDLListCust.SelectedIndex = -1 : FilterTextListCust.Text = "" : gvListCust.SelectedIndex = -1
        BindCustomerData()
        mpeListCust.Show()
    End Sub

    Protected Sub gvListCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListCust.PageIndexChanging
        gvListCust.PageIndex = e.NewPageIndex
        BindCustomerData()
        mpeListCust.Show()
    End Sub

    Protected Sub gvListCust_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListCust.SelectedIndexChanged
        If custoid.Text <> gvListCust.SelectedDataKey.Item("custoid").ToString Then
            btnClearCust_Click(Nothing, Nothing)
        End If
        custoid.Text = gvListCust.SelectedDataKey.Item("custoid").ToString
        custname.Text = gvListCust.SelectedDataKey.Item("custname").ToString
        custtype.Text = gvListCust.SelectedDataKey.Item("custtype").ToString
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub

    Protected Sub lkbCloseListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListCust.Click
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub

    Protected Sub curroid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles curroid.SelectedIndexChanged
        If cashbanktype.SelectedValue = "BLM" Then
            btnClearDP_Click(Nothing, Nothing)
        End If
        ShowCurrSymbol(curroid.SelectedValue, 0)
    End Sub

    Protected Sub cashbanktype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cashbanktype.SelectedIndexChanged
        InitDDLCOARayment()
        If Session("oid") Is Nothing Or Session("oid") = "" Then
            generateNo()
        End If
    End Sub

    Protected Sub acctgoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles acctgoid.SelectedIndexChanged
        If Session("oid") Is Nothing Or Session("oid") = "" Then
            generateNo()
        End If
    End Sub

    Protected Sub addacctgoid1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles addacctgoid1.SelectedIndexChanged
        addacctgamt1.Text = ""
        addacctgamt1.Enabled = IIf(sender.SelectedValue = 0, False, True)
        addacctgamt1.CssClass = IIf(sender.SelectedValue = 0, "inpTextDisabled", "inpText")
        ReAmountCost()
        If sender.SelectedValue <> 0 Then cProc.SetFocusToControl(Me.Page, addacctgamt1)
    End Sub

    Protected Sub addacctgoid2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles addacctgoid2.SelectedIndexChanged
        addacctgamt2.Text = ""
        addacctgamt2.Enabled = IIf(sender.SelectedValue = 0, False, True)
        addacctgamt2.CssClass = IIf(sender.SelectedValue = 0, "inpTextDisabled", "inpText")
        ReAmountCost()
        If sender.SelectedValue <> 0 Then cProc.SetFocusToControl(Me.Page, addacctgamt2)
    End Sub

    Protected Sub addacctgoid3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles addacctgoid3.SelectedIndexChanged
        addacctgamt3.Text = ""
        addacctgamt3.Enabled = IIf(sender.SelectedValue = 0, False, True)
        addacctgamt3.CssClass = IIf(sender.SelectedValue = 0, "inpTextDisabled", "inpText")
        ReAmountCost()
        If sender.SelectedValue <> 0 Then cProc.SetFocusToControl(Me.Page, addacctgamt3)
    End Sub

    Protected Sub addacctgamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _
    addacctgamt1.TextChanged, addacctgamt2.TextChanged, addacctgamt3.TextChanged
        sender.Text = ToMaskEdit(ToDouble(sender.Text), iMask)
        ReAmountCost()
    End Sub

    Protected Sub btnSearchDP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchDP.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        If curroid.SelectedValue = "" Then
            showMessage("Please select Currency first!", 2)
            Exit Sub
        End If
        If custoid.Text = "" Then
            showMessage("Please select Customer first!", 2)
            Exit Sub
        End If
        FilterDDLListDP.SelectedIndex = -1 : FilterTextListDP.Text = "" : gvListDP.SelectedIndex = -1
        BindDPData()
        cProc.SetModalPopUpExtender(btnHideListDP, pnlListDP, mpeListDP, True)
    End Sub

    Protected Sub btnClearDP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearDP.Click
        giroacctgoid.Text = "" : cashbankrefno.Text = "" : acctgoid.SelectedIndex = -1 : dparamt.Text = ""
    End Sub

    Protected Sub btnFindListDP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListDP.Click
        BindDPData()
        mpeListDP.Show()
    End Sub

    Protected Sub btnAllListDP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListDP.Click
        FilterDDLListDP.SelectedIndex = -1 : FilterTextListDP.Text = "" : gvListDP.SelectedIndex = -1
        BindDPData()
        mpeListDP.Show()
    End Sub

    Protected Sub gvListDP_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListDP.PageIndexChanging
        gvListDP.PageIndex = e.NewPageIndex
        BindDPData()
        mpeListDP.Show()
    End Sub

    Protected Sub gvListDP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListDP.SelectedIndexChanged
        If giroacctgoid.Text <> gvListDP.SelectedDataKey.Item("dparoid").ToString Then
            btnClearDP_Click(Nothing, Nothing)
        End If
        giroacctgoid.Text = gvListDP.SelectedDataKey.Item("dparoid").ToString
        cashbankrefno.Text = gvListDP.SelectedDataKey.Item("dparno").ToString
        acctgoid.SelectedValue = gvListDP.SelectedDataKey.Item("dparacctgoid").ToString
        dparamt.Text = ToMaskEdit(ToDouble(gvListDP.SelectedDataKey.Item("dparamt").ToString), iMask)
        cProc.SetModalPopUpExtender(btnHideListDP, pnlListDP, mpeListDP, False)
    End Sub

    Protected Sub lkbCloseListDP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListDP.Click
        cProc.SetModalPopUpExtender(btnHideListDP, pnlListDP, mpeListDP, False)
    End Sub

    Protected Sub btnSearchAR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchAR.Click
        If custoid.Text = "" Then
            showMessage("Please select Customer first!", 2)
            Exit Sub
        End If
        If cashbanktakegiro.Text <> "" Then
            Dim sErr As String = ""
            If Not IsValidDate(cashbanktakegiro.Text, "MM/dd/yyyy", sErr) Then
                showMessage("Date Take Giro is invalid. " & sErr, 2)
                Exit Sub
            End If
        End If
        FilterDDLListAR.SelectedIndex = -1 : FilterTextListAR.Text = "" : gvListAR.SelectedIndex = -1
        BindARData()
        cProc.SetModalPopUpExtender(btnHideListAR, pnlListAR, mpeListAR, True)
    End Sub

    Protected Sub btnClearAR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearAR.Click
        aritemmstoid.Text = "" : aritemno.Text = "" : aracctgoid.Text = "" : araccount.Text = "" : aritemgrandtotal.Text = "" : arpaidamt.Text = "" : arbalance.Text = ""
        payaramt.Text = "" : arcurroid.SelectedIndex = -1 : payaramtother.Text = "" : payaramt_TextChanged(Nothing, Nothing) : arratetoidr.Text = "" : arratetousd.Text = ""
        ShowCurrSymbol(arcurroid.SelectedValue, 1)
    End Sub

    Protected Sub btnFindListAR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListAR.Click
        BindARData()
        mpeListAR.Show()
    End Sub

    Protected Sub btnAllListAR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListAR.Click
        FilterDDLListAR.SelectedIndex = -1 : FilterTextListAR.Text = "" : gvListAR.SelectedIndex = -1
        BindARData()
        mpeListAR.Show()
    End Sub

    Protected Sub gvListAR_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListAR.PageIndexChanging
        gvListAR.PageIndex = e.NewPageIndex
        BindARData()
        mpeListAR.Show()
    End Sub

    Protected Sub gvListAR_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListAR.SelectedIndexChanged
        If aritemmstoid.Text <> gvListAR.SelectedDataKey.Item("aroid").ToString Then
            btnClearAR_Click(Nothing, Nothing)
        End If
        aritemmstoid.Text = gvListAR.SelectedDataKey.Item("aroid").ToString
        aritemno.Text = gvListAR.SelectedDataKey.Item("arno").ToString
        ardate.Text = gvListAR.SelectedDataKey.Item("ardate").ToString
        reftype.Text = gvListAR.SelectedDataKey.Item("reftype").ToString
        aracctgoid.Text = gvListAR.SelectedDataKey.Item("aracctgoid").ToString
        araccount.Text = gvListAR.SelectedDataKey.Item("araccount").ToString
        aritemgrandtotal.Text = ToMaskEdit(ToDouble(gvListAR.SelectedDataKey.Item("argrandtotal").ToString), iMask)
        arpaidamt.Text = ToMaskEdit(ToDouble(gvListAR.SelectedDataKey.Item("arpaidamt").ToString), iMask)
        arbalance.Text = ToMaskEdit(ToDouble(gvListAR.SelectedDataKey.Item("argrandtotal").ToString) - ToDouble(gvListAR.SelectedDataKey.Item("arpaidamt").ToString), iMask)
        payaramt.Text = ToMaskEdit(ToDouble(arbalance.Text), iMask)
        subtotalpayment.Text = ToMaskEdit(ToDouble(arbalance.Text), iMask)
        arcurroid.SelectedValue = gvListAR.SelectedDataKey.Item("curroid").ToString
        ShowCurrSymbol(arcurroid.SelectedValue, 1)
        arratetoidr.Text = gvListAR.SelectedDataKey.Item("arratetoidr").ToString
        arratetousd.Text = gvListAR.SelectedDataKey.Item("arratetousd").ToString
        cProc.SetModalPopUpExtender(btnHideListAR, pnlListAR, mpeListAR, False)
    End Sub

    Protected Sub lkbCloseListAR_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListAR.Click
        cProc.SetModalPopUpExtender(btnHideListAR, pnlListAR, mpeListAR, False)
    End Sub

    Protected Sub payaramt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles payaramt.TextChanged
        payaramt.Text = ToMaskEdit(ToDouble(payaramt.Text), iMask)
        If ToDouble(arbalance.Text) > 0 Then
            If ToDouble(payaramt.Text) = ToDouble(arbalance.Text) Then
                pnlOverPayment.Visible = False
                Session("TblDtlDiff") = Nothing
                gvDtlDiff.DataSource = Session("TblDtlDiff") : gvDtlDiff.DataBind()
                ClearDetailDiff()
                Session("LastFlagIndex") = Nothing
            Else
                pnlOverPayment.Visible = True
                If ToDouble(payaramt.Text) > ToDouble(arbalance.Text) Then
                    diffflag.SelectedIndex = 0 : diffamount.Text = ToMaskEdit(ToDouble(payaramt.Text) - ToDouble(arbalance.Text), 2)
                Else
                    diffflag.SelectedIndex = 1 : diffamount.Text = ToMaskEdit(ToDouble(arbalance.Text) - ToDouble(payaramt.Text), 2)
                End If
                If Session("LastFlagIndex") Is Nothing Then
                    Session("LastFlagIndex") = diffflag.SelectedIndex
                Else
                    If CInt(Session("LastFlagIndex")) <> diffflag.SelectedIndex Then
                        Session("TblDtlDiff") = Nothing
                        gvDtlDiff.DataSource = Session("TblDtlDiff") : gvDtlDiff.DataBind()
                        ClearDetailDiff()
                    End If
                End If
                InitDDLDiffAcctg()
            End If
        Else
            pnlOverPayment.Visible = False
            Session("TblDtlDiff") = Nothing
            gvDtlDiff.DataSource = Session("TblDtlDiff") : gvDtlDiff.DataBind()
            ClearDetailDiff()
            Session("LastFlagIndex") = Nothing
        End If
        CountTotalDiff()
    End Sub

    Protected Sub btnAddToListDiff_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToListDiff.Click
        If IsDiffDetailInputValid() Then
            If Session("TblDtlDiff") Is Nothing Then
                CreateTblDtlDiff()
            End If
            Dim objTable As DataTable = Session("TblDtlDiff")
            Dim dv As DataView = objTable.DefaultView
            If i_u3.Text = "New Detail" Then
                dv.RowFilter = "diffacctgoid=" & diffacctgoid.SelectedValue
            Else
                dv.RowFilter = "diffacctgoid=" & diffacctgoid.SelectedValue & " AND diffseq<>" & diffseq.Text
            End If
            If dv.Count > 0 Then
                dv.RowFilter = ""
                showMessage("This Data has been added before, please check!", 2)
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u3.Text = "New Detail" Then
                objRow = objTable.NewRow()
                diffseq.Text = objTable.Rows.Count + 1
                objRow("diffseq") = diffseq.Text
            Else
                objRow = objTable.Rows(diffseq.Text - 1)
                objRow.BeginEdit()
            End If
            objRow("diffflag") = diffflag.SelectedValue
            objRow("diffacctgoid") = diffacctgoid.SelectedValue
            objRow("diffaccount") = diffacctgoid.SelectedItem.Text
            objRow("diffamount") = ToDouble(diffamount.Text)
            objRow("diffamountother") = ToDouble(diffamountother.Text)
            objRow("diffnote") = diffnote.Text
            If i_u3.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            Session("TblDtlDiff") = objTable
            gvDtlDiff.DataSource = Session("TblDtlDiff")
            gvDtlDiff.DataBind()
            ClearDetailDiff()
            CountTotalDiff()
        End If
    End Sub

    Protected Sub btnClearDiff_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearDiff.Click
        ClearDetailDiff()
    End Sub

    Protected Sub gvDtlDiff_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtlDiff.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), iMask)
        End If
    End Sub

    Protected Sub gvDtlDiff_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtlDiff.RowDeleting
        Dim objTable As DataTable = Session("TblDtlDiff")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("diffseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtlDiff") = objTable
        gvDtlDiff.DataSource = Session("TblDtlDiff")
        gvDtlDiff.DataBind()
        ClearDetailDiff()
        CountTotalDiff()
    End Sub

    Protected Sub gvDtlDiff_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDtlDiff.SelectedIndexChanged
        Try
            diffseq.Text = gvDtlDiff.SelectedDataKey.Item("diffseq").ToString().Trim
            If Session("TblDtlDiff") Is Nothing = False Then
                i_u3.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtlDiff")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "diffseq=" & diffseq.Text
                diffacctgoid.SelectedValue = dv.Item(0).Item("diffacctgoid").ToString
                diffamount.Text = ToMaskEdit(dv.Item(0).Item("diffamount").ToString, iMask)
                diffamountother.Text = ToMaskEdit(dv.Item(0).Item("diffamountother").ToString, iMask)
                diffnote.Text = dv.Item(0).Item("diffnote").ToString
                gvDtlDiff.Columns(0).Visible = False
                gvDtlDiff.Columns(gvDtlDiff.Columns.Count - 1).Visible = False
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            If Session("TblDtl") Is Nothing Then
                CreateTblDtl()
            End If
            Dim objTable As DataTable = Session("TblDtl")
            Dim dv As DataView = objTable.DefaultView
            If i_u2.Text = "New Detail" Then
                dv.RowFilter = "aritemmstoid=" & aritemmstoid.Text & " AND reftype='" & reftype.Text & "' AND payarflag=''"
            Else
                dv.RowFilter = "aritemmstoid=" & aritemmstoid.Text & " AND reftype='" & reftype.Text & "' AND payarflag='' AND dtlseq<>" & dtlseq.Text
            End If
            If dv.Count > 0 Then
                dv.RowFilter = ""
                showMessage("This Data has been added before, please check!", 2)
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim dRate As Double = ToDouble(payaramtother.Text) / ToDouble(payaramt.Text)
            Dim dOther As Double = Math.Round(ToDouble(payaramtother.Text), iRoundDigit, MidpointRounding.AwayFromZero)
            Dim dOtherPlus As Double = 0
            If i_u2.Text = "New Detail" Then
                Dim objRow As DataRow = objTable.NewRow()
                dtlseq.Text = objTable.Rows.Count + 1
                objRow("dtlseq") = dtlseq.Text
                objRow("aritemmstoid") = aritemmstoid.Text
                objRow("aritemno") = aritemno.Text
                objRow("aracctgoid") = aracctgoid.Text
                objRow("araccount") = araccount.Text
                objRow("aritemgrandtotal") = ToDouble(aritemgrandtotal.Text)
                objRow("arpaidamt") = ToDouble(arpaidamt.Text)
                objRow("arbalance") = ToDouble(arbalance.Text)
                objRow("payaramt") = ToDouble(payaramt.Text)
                objRow("arcurroid") = arcurroid.SelectedValue
                objRow("arcurrcode") = arcurroid.SelectedItem.Text
                objRow("payarnote") = payarnote.Text
                objRow("subtotalpayment") = ToDouble(subtotalpayment.Text)
                objRow("payarflag") = ""
                objRow("totaldiff") = ToDouble(totaldiff.Text)
                objRow("arratetoidr") = ToDouble(arratetoidr.Text)
                objRow("arratetousd") = ToDouble(arratetousd.Text)
                If ToDouble(payaramt.Text) >= ToDouble(arbalance.Text) Then
                    objRow("payaramtpost") = ToDouble(arbalance.Text)
                    objRow("payaramtother") = Math.Round(ToDouble(arbalance.Text) * dRate, iRoundDigit, MidpointRounding.AwayFromZero)
                    dOtherPlus = Math.Round(ToDouble(arbalance.Text) * dRate, iRoundDigit, MidpointRounding.AwayFromZero)
                Else
                    objRow("payaramtpost") = ToDouble(subtotalpayment.Text)
                    objRow("payaramtother") = Math.Round(ToDouble(subtotalpayment.Text) * dRate, iRoundDigit, MidpointRounding.AwayFromZero)
                    dOtherPlus = Math.Round(ToDouble(subtotalpayment.Text) * dRate, iRoundDigit, MidpointRounding.AwayFromZero)
                End If
                objRow("reftype") = reftype.Text
                objTable.Rows.Add(objRow)
            Else
                dv.RowFilter = "payarflag='' AND dtlseq=" & dtlseq.Text
                If dv.Count = 1 Then
                    dv(0)("aritemmstoid") = aritemmstoid.Text
                    dv(0)("aritemno") = aritemno.Text
                    dv(0)("aracctgoid") = aracctgoid.Text
                    dv(0)("araccount") = araccount.Text
                    dv(0)("aritemgrandtotal") = ToDouble(aritemgrandtotal.Text)
                    dv(0)("arpaidamt") = ToDouble(arpaidamt.Text)
                    dv(0)("arbalance") = ToDouble(arbalance.Text)
                    dv(0)("payaramt") = ToDouble(payaramt.Text)
                    dv(0)("arcurroid") = arcurroid.SelectedValue
                    dv(0)("arcurrcode") = arcurroid.SelectedItem.Text
                    dv(0)("payarnote") = payarnote.Text
                    dv(0)("subtotalpayment") = ToDouble(subtotalpayment.Text)
                    dv(0)("totaldiff") = ToDouble(totaldiff.Text)
                    dv(0)("arratetoidr") = ToDouble(arratetoidr.Text)
                    dv(0)("arratetousd") = ToDouble(arratetousd.Text)
                    If ToDouble(payaramt.Text) >= ToDouble(arbalance.Text) Then
                        dv(0)("payaramtpost") = ToDouble(arbalance.Text)
                        dv(0)("payaramtother") = Math.Round(ToDouble(arbalance.Text) * dRate, iRoundDigit, MidpointRounding.AwayFromZero)
                        dOtherPlus = Math.Round(ToDouble(arbalance.Text) * dRate, iRoundDigit, MidpointRounding.AwayFromZero)
                    Else
                        dv(0)("payaramtpost") = ToDouble(subtotalpayment.Text)
                        dv(0)("payaramtother") = Math.Round(ToDouble(subtotalpayment.Text) * dRate, iRoundDigit, MidpointRounding.AwayFromZero)
                        dOtherPlus = Math.Round(ToDouble(subtotalpayment.Text) * dRate, iRoundDigit, MidpointRounding.AwayFromZero)
                    End If
                    dv(0)("reftype") = reftype.Text
                End If
                dv.RowFilter = ""
                dv.RowFilter = "payarflag<>'' AND dtlseq=" & dtlseq.Text
                For C1 As Integer = 0 To dv.Count - 1
                    dv.Delete(0)
                Next
                dv.RowFilter = ""
            End If
            objTable.AcceptChanges()
            If Session("TblDtlDiff") IsNot Nothing Then
                Dim dtDiff As DataTable = Session("TblDtlDiff")
                For C1 As Integer = 0 To dtDiff.Rows.Count - 1
                    Dim dr As DataRow = objTable.NewRow
                    dr("dtlseq") = dtlseq.Text
                    dr("aritemmstoid") = aritemmstoid.Text
                    dr("aritemno") = aritemno.Text
                    dr("aracctgoid") = dtDiff.Rows(C1)("diffacctgoid").ToString
                    dr("araccount") = dtDiff.Rows(C1)("diffaccount").ToString
                    dr("aritemgrandtotal") = ToDouble(aritemgrandtotal.Text)
                    dr("arpaidamt") = ToDouble(arpaidamt.Text)
                    dr("arbalance") = ToDouble(arbalance.Text)
                    dr("payaramt") = ToDouble(dtDiff.Rows(C1)("diffamount").ToString)
                    dr("arcurroid") = arcurroid.SelectedValue
                    dr("arcurrcode") = arcurroid.SelectedItem.Text
                    dOtherPlus = Math.Round(dOtherPlus + ToDouble(dtDiff.Rows(C1)("diffamount").ToString) * dRate, iRoundDigit, MidpointRounding.AwayFromZero)
                    If C1 = dtDiff.Rows.Count - 1 Then
                        dr("payaramtother") = Math.Round(ToDouble(dtDiff.Rows(C1)("diffamount").ToString) * dRate, iRoundDigit, MidpointRounding.AwayFromZero)
                        If dOtherPlus <> dOther Then
                            dr("payaramtother") = Math.Round(ToDouble(dr("payaramtother").ToString) + (dOther - dOtherPlus), iRoundDigit, MidpointRounding.AwayFromZero)
                        End If
                    Else
                        dr("payaramtother") = Math.Round(ToDouble(dtDiff.Rows(C1)("diffamount").ToString) * dRate, iRoundDigit, MidpointRounding.AwayFromZero)
                    End If
                    dr("payarnote") = dtDiff.Rows(C1)("diffnote").ToString
                    dr("subtotalpayment") = ToDouble(dtDiff.Rows(C1)("diffamount").ToString)
                    dr("payarflag") = dtDiff.Rows(C1)("diffflag").ToString
                    dr("totaldiff") = 0
                    dr("arratetoidr") = ToDouble(arratetoidr.Text)
                    dr("arratetousd") = ToDouble(arratetousd.Text)
                    If dtDiff.Rows(C1)("diffflag").ToString = "Lebih Bayar" Then
                        dr("payaramtpost") = ToDouble(dtDiff.Rows(C1)("diffamount").ToString)
                    Else
                        dr("payaramtpost") = -ToDouble(dtDiff.Rows(C1)("diffamount").ToString)
                    End If
                    dr("reftype") = reftype.Text
                    objTable.Rows.Add(dr)
                Next
            End If
            objTable.AcceptChanges()
            dv = objTable.DefaultView
            dv.Sort = "dtlseq, payarflag"
            objTable.AcceptChanges()
            Session("TblDtl") = objTable
            gvDtl.DataSource = Session("TblDtl")
            gvDtl.DataBind()
            ClearDetail()
            CountTotalPayment()
            ReAmountCost()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), iMask)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), iMask)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), iMask)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), iMask)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), iMask)
            If e.Row.Cells(10).Text = "Kurang Bayar" Or e.Row.Cells(10).Text = "Lebih Bayar" Then
                e.Row.Cells(0).Enabled = False
                e.Row.Cells(gvDtl.Columns.Count - 1).Enabled = False
            End If
        End If
    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDtl.SelectedIndexChanged
        Try
            dtlseq.Text = gvDtl.SelectedDataKey.Item("dtlseq").ToString().Trim
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "dtlseq=" & dtlseq.Text & " AND payarflag=''"
                aritemmstoid.Text = dv.Item(0).Item("aritemmstoid").ToString
                aritemno.Text = dv.Item(0).Item("aritemno").ToString
                aracctgoid.Text = dv.Item(0).Item("aracctgoid").ToString
                araccount.Text = dv.Item(0).Item("araccount").ToString
                aritemgrandtotal.Text = ToMaskEdit(dv.Item(0).Item("aritemgrandtotal").ToString, iMask)
                arpaidamt.Text = ToMaskEdit(dv.Item(0).Item("arpaidamt").ToString, iMask)
                arbalance.Text = ToMaskEdit(dv.Item(0).Item("arbalance").ToString, iMask)
                payaramt.Text = ToMaskEdit(dv.Item(0).Item("payaramt").ToString, iMask)
                arcurroid.SelectedValue = dv.Item(0).Item("arcurroid").ToString
                ShowCurrSymbol(arcurroid.SelectedValue, 1)
                payaramtother.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("payaramtother").ToString), iMask)
                payaramt_TextChanged(Nothing, Nothing)
                payarnote.Text = dv.Item(0).Item("payarnote").ToString
                subtotalpayment.Text = ToMaskEdit(dv.Item(0).Item("subtotalpayment").ToString, iMask)
                totaldiff.Text = ToMaskEdit(dv.Item(0).Item("totaldiff").ToString, iMask)
                arratetoidr.Text = dv.Item(0).Item("arratetoidr").ToString
                arratetousd.Text = dv.Item(0).Item("arratetousd").ToString
                reftype.Text = dv.Item(0).Item("reftype").ToString
                'ardate.Text = gvListAR.SelectedDataKey.Item("ardate").ToString

                dv.RowFilter = ""
                If pnlOverPayment.Visible = True Then
                    If CInt(Session("LastFlagIndex")) = 0 Then
                        dv.RowFilter = "dtlseq=" & dtlseq.Text & " AND payarflag='Lebih Bayar'"
                    Else
                        dv.RowFilter = "dtlseq=" & dtlseq.Text & " AND payarflag='Kurang Bayar'"
                    End If
                    If dv.Count > 0 Then
                        CreateTblDtlDiff()
                        Dim dt As DataTable = Session("TblDtlDiff")
                        Dim dOther As Double = ToDouble(payaramtother.Text)
                        For C1 As Integer = 0 To dv.Count - 1
                            Dim dr As DataRow = dt.NewRow
                            dr("diffseq") = C1 + 1
                            dr("diffflag") = dv(C1)("payarflag").ToString
                            dr("diffacctgoid") = dv(C1)("aracctgoid").ToString
                            dr("diffaccount") = dv(C1)("araccount").ToString
                            dr("diffamount") = Math.Abs(ToDouble(dv(C1)("subtotalpayment").ToString))
                            dr("diffamountother") = ToDouble(dv(C1)("payaramtother").ToString)
                            dOther += ToDouble(dv(C1)("payaramtother").ToString)
                            dr("diffnote") = dv(C1)("payarnote").ToString
                            dt.Rows.Add(dr)
                        Next
                        dt.AcceptChanges()
                        Session("TblDtlDiff") = dt
                        gvDtlDiff.DataSource = Session("TblDtlDiff")
                        gvDtlDiff.DataBind()
                        payaramtother.Text = ToMaskEdit(dOther, iMask)
                    End If
                    dv.RowFilter = ""
                    CountTotalDiff()
                End If
            End If
            gvDtl.Columns(0).Visible = False
            gvDtl.Columns(gvDtl.Columns.Count - 1).Visible = False
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub gvDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        Dim sOid As String = objRow(e.RowIndex)("dtlseq").ToString
        Dim dv As DataView = objTable.DefaultView
        dv.RowFilter = "dtlseq=" & sOid
        For C1 As Int16 = 0 To dv.Count - 1
            dv.Delete(0)
        Next
        dv.RowFilter = ""
        objTable.AcceptChanges()
        Session("TblDtl") = objTable
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        ClearDetail()
        CountTotalPayment()
        ReAmountCost()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            Dim iCBMstOid2 As Integer = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_trncashbankmst WHERE cashbankoid=" & cashbankoid.Text
                If CheckDataExists(sSql) Then
                    cashbankoid.Text = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
                End If
                sSql = "SELECT COUNT(*) FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND acctgoid=" & acctgoid.SelectedValue & " AND cashbankno='" & cashbankno.Text & "'"
                If CheckDataExists(sSql) Then
                    generateNo()
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_trncashbankmst", "cashbankoid", cashbankoid.Text, "cashbankstatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    cashbankstatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            payaroid.Text = GenerateID("QL_TRNPAYAR", CompnyCode)
            conaroid.Text = GenerateID("QL_CONAR", CompnyCode)
            Dim sDueDate As String = ""
            If cashbanktype.SelectedValue = "BKM" Or cashbanktype.SelectedValue = "BLM" Then
                sDueDate = cashbankdate.Text
            Else
                sDueDate = cashbankduedate.Text
            End If
            Dim sDTG As String = "1/1/1900"
            If cashbanktakegiro.Text <> "" Then
                sDTG = cashbanktakegiro.Text
            End If
            Dim glmstoid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
            Dim gldtloid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
            Dim iDPAROid As Integer = GenerateID("QL_TRNDPAR", CompnyCode)
            Dim dtSum As DataTable = Nothing
            Dim cRate As New ClassRate()
            Dim cRate2 As New ClassRate()
            Dim iDiffIDRAcctgOid, iDiffUSDAcctgOid As Integer
            Dim iARGiroAcctgOid As Integer = ToInteger(giroacctgoid.Text)
            Dim iARCurrOid As Integer = GetRateAR()
            Dim sARCurrCode As String = GetStrData("SELECT currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND curroid=" & iARCurrOid)
            Dim sOidDP As Integer = 0
            Dim iCustGroupOid As Integer = 0
            Dim dtCurrentPayar As New DataTable
            If Session("oid") = Nothing Or Session("oid") = "" Then
            Else
                ' Select Current Payment detail on DB for later reverse
                sSql = "SELECT refoid,reftype FROM QL_trnpayar WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text & " AND ISNULL(payarres1,'')='' "
                dtCurrentPayar = cKon.ambiltabel(sSql, "QL_trnpayar")
            End If

            If cashbankstatus.Text = "Post" Then
                If cashbanktype.SelectedValue = "BLM" Then
                    'cRate.SetRateValue(ToInteger(GetStrData("SELECT rateoid FROM QL_trndpar WHERE dparoid=" & giroacctgoid.Text)), ToInteger(GetStrData("SELECT rate2oid FROM QL_trndpar WHERE dparoid=" & giroacctgoid.Text)))
                    cRate.SetRateValue(CInt(curroid.SelectedValue), Format(GetServerTime(), "MM/dd/yyyy"))
                Else
                    cRate.SetRateValue(CInt(curroid.SelectedValue), Format(GetServerTime(), "MM/dd/yyyy"))
                End If
                If cRate.GetRateDailyLastError <> "" Then
                    showMessage(cRate.GetRateDailyLastError, 2)
                    Exit Sub
                End If
                If cRate.GetRateMonthlyLastError <> "" Then
                    showMessage(cRate.GetRateMonthlyLastError, 2)
                    Exit Sub
                End If
                cRate2.SetRateValue(iARCurrOid, Format(GetServerTime(), "MM/dd/yyyy"))
                If cRate2.GetRateDailyLastError <> "" Then
                    showMessage(cRate2.GetRateDailyLastError, 2)
                    Exit Sub
                End If
                If cRate2.GetRateMonthlyLastError <> "" Then
                    showMessage(cRate2.GetRateMonthlyLastError, 2)
                    Exit Sub
                End If
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
                dtSum = GetSumDetail()
                Dim sVarErr As String = ""
                If Not IsInterfaceExists("VAR_DIFF_CURR_IDR", DDLBusUnit.SelectedValue) Then
                    sVarErr = "VAR_DIFF_CURR_IDR"
                End If
                If Not IsInterfaceExists("VAR_DIFF_CURR_USD", DDLBusUnit.SelectedValue) Then
                    sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_DIFF_CURR_USD"
                End If
                If cashbanktype.SelectedValue = "BGM" Then
                    If Not IsInterfaceExists("VAR_GIRO_IN", DDLBusUnit.SelectedValue) Then
                        sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_GIRO_IN"
                    End If
                End If
                If sVarErr <> "" Then
                    showMessage(GetInterfaceWarning(sVarErr, "approved"), 2)
                    Exit Sub
                End If
                iDiffIDRAcctgOid = GetAcctgOID(GetVarInterface("VAR_DIFF_CURR_IDR", DDLBusUnit.SelectedValue), CompnyCode)
                iDiffUSDAcctgOid = GetAcctgOID(GetVarInterface("VAR_DIFF_CURR_USD", DDLBusUnit.SelectedValue), CompnyCode)
                If cashbanktype.SelectedValue = "BGM" Then
                    iARGiroAcctgOid = GetAcctgOID(GetVarInterface("VAR_GIRO_IN", DDLBusUnit.SelectedValue), CompnyCode)
                End If
                ' Update Credit limit Customer
                sSql = "SELECT custgroupoid from QL_mstcust WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND custoid=" & custoid.Text & ""
                iCustGroupOid = cKon.ambilscalar(sSql)

                'Get acctgoid from VAR_DPAR
                sOidDP = GetAcctgOID(GetVarInterface("VAR_DPAR_LEBIH_BAYAR", DDLBusUnit.SelectedValue), CompnyCode)
                GenerateDPARNo()
                GenerateCBNo2()
            End If

            'isi salescode dan komisi nya
            Dim objTable As DataTable = Session("TblDtl")
            For C1 As Int16 = 0 To objTable.Rows.Count - 1
                If objTable.Rows(C1)("reftype").ToString.ToUpper = "QL_TRNJUALMST" Then
                    sSql = "SELECT ISNULL(arm.salescode,'') FROM QL_trnjualmst arm INNER JOIN QL_mstperson c ON c.cmpcode=arm.cmpcode AND c.salescode=arm.salescode WHERE(arm.trnjualmstoid = " & objTable.Rows(C1)("aritemmstoid").ToString & ")"
                    objTable.Rows(C1)("salescode") = GetStrData(sSql)
                    sSql = "SELECT ISNULL(c.komisi,0) FROM QL_trnjualmst arm LEFT JOIN QL_mstperson c ON c.cmpcode=arm.cmpcode AND c.salescode=arm.salescode WHERE(arm.trnjualmstoid = " & objTable.Rows(C1)("aritemmstoid").ToString & ")"
                    objTable.Rows(C1)("komisi") = cKon.ambilscalar(sSql)
                End If
            Next
            objTable.AcceptChanges()

            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trncashbankmst (cmpcode, cashbankoid, periodacctg, cashbankno, cashbankdate, cashbanktype, cashbankgroup, acctgoid, curroid, cashbankamt, cashbankamtidr, cashbankamtusd, personoid, cashbankduedate, cashbankrefno, cashbanknote, cashbankstatus, createuser, createtime, upduser, updtime, cashbanktakegiro, giroacctgoid, cashbankdpp, addacctgoid1, addacctgamt1, addacctgoid2, addacctgamt2, addacctgoid3, addacctgamt3) VALUES ('" & DDLBusUnit.SelectedValue & "', " & cashbankoid.Text & ", '" & periodacctg.Text & "', '" & cashbankno.Text & "', '" & cashbankdate.Text & "', '" & cashbanktype.SelectedValue & "', 'AR', " & acctgoid.SelectedValue & ", " & curroid.SelectedValue & ", " & ToDouble(payapnett.Text) & ", " & ToDouble(payapnett.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(payapnett.Text) * cRate.GetRateMonthlyUSDValue & ", " & custoid.Text & ", '" & sDueDate & "', '" & Tchar(cashbankrefno.Text) & "', '" & Tchar(cashbanknote.Text) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & sDTG & "', " & iARGiroAcctgOid & ", " & ToDouble(dparamt.Text) & ", " & addacctgoid1.SelectedValue & "," & ToDouble(addacctgamt1.Text) & "," & addacctgoid2.SelectedValue & "," & ToDouble(addacctgamt2.Text) & "," & addacctgoid3.SelectedValue & "," & ToDouble(addacctgamt3.Text) & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & cashbankoid.Text & " WHERE tablename='QL_TRNCASHBANKMST' and cmpcode='" & DDLBusUnit.SelectedValue & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    If cashbanktype.SelectedValue = "BLM" Then
                        sSql = "UPDATE QL_trndpar SET dparaccumamt = dparaccumamt + " & ToDouble(payapnett.Text) & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dparoid=" & giroacctgoid.Text
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                Else
                    sSql = "UPDATE QL_trncashbankmst SET periodacctg='" & periodacctg.Text & "', cashbankno='" & cashbankno.Text & "', cashbankdate='" & cashbankdate.Text & "', cashbanktype='" & cashbanktype.SelectedValue & "', acctgoid=" & acctgoid.SelectedValue & ", curroid=" & curroid.SelectedValue & ", cashbankamt=" & ToDouble(payapnett.Text) & ", cashbankamtidr=" & ToDouble(payapnett.Text) * cRate.GetRateMonthlyIDRValue & ", cashbankamtusd=" & ToDouble(payapnett.Text) * cRate.GetRateMonthlyUSDValue & ", personoid=" & custoid.Text & ", cashbankduedate='" & sDueDate & "', cashbankrefno='" & Tchar(cashbankrefno.Text) & "', cashbanknote='" & Tchar(cashbanknote.Text) & "', cashbankstatus='" & cashbankstatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, cashbanktakegiro='" & sDTG & "', giroacctgoid=" & iARGiroAcctgOid & ", cashbankdpp=" & ToDouble(dparamt.Text) & ", addacctgoid1=" & addacctgoid1.SelectedValue & ",addacctgamt1=" & ToDouble(addacctgamt1.Text) & ",addacctgoid2=" & addacctgoid2.SelectedValue & ",addacctgamt2=" & ToDouble(addacctgamt2.Text) & ",addacctgoid3=" & addacctgoid3.SelectedValue & ",addacctgamt3=" & ToDouble(addacctgamt3.Text) & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    If Session("lastcashbanktype") IsNot Nothing And Session("lastcashbanktype") = "BLM" Then
                        sSql = "UPDATE QL_trndpar SET dparaccumamt = dparaccumamt - " & ToDouble(Session("lastdparamt").ToString) & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dparoid=" & Session("lastdparoid")
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                    If cashbanktype.SelectedValue = "BLM" Then
                        sSql = "UPDATE QL_trndpar SET dparaccumamt = dparaccumamt + " & ToDouble(payapnett.Text) & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dparoid=" & giroacctgoid.Text
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If

                    ' Reversing each detail A/R
                    For R1 As Integer = 0 To dtCurrentPayar.Rows.Count - 1
                        Dim sTableName As String = "" : Dim sColStatusName As String = "" : Dim sIDColName As String = ""
                        If dtCurrentPayar.Rows(R1)("reftype").ToString.ToUpper = "QL_TRNJUALMST" Then
                            sTableName = "QL_trnjualmst" : sColStatusName = "trnjualstatus" : sIDColName = "trnjualmstoid"
                        ElseIf dtCurrentPayar.Rows(R1)("reftype").ToString.ToUpper = "QL_TRNTRANSITEMMST" Then
                            sTableName = "QL_trntransitemmst" : sColStatusName = "transitemmststatus" : sIDColName = "transitemmstoid"
                        End If
                        sSql = "UPDATE " & sTableName & " SET " & sColStatusName & "='Post', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP " & _
                            "WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND " & sIDColName & "=" & dtCurrentPayar.Rows(R1)("refoid").ToString & " "
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next

                    sSql = "DELETE FROM QL_conar WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnartype='PAYAR' AND payrefoid IN (SELECT payaroid FROM QL_trnpayar WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trnpayar WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    Dim dtTbl As DataTable = Session("TblDtl")
                    Dim dPayARIDR As Double = 0, dPayARUSD As Double = 0
                    For C1 As Int16 = 0 To dtTbl.Rows.Count - 1
                        Dim sTableName As String = "" : Dim sColStatusName As String = "" : Dim sIDColName As String = ""
                        If dtTbl.Rows(C1)("reftype").ToString.ToUpper = "QL_TRNJUALMST" Then
                            sTableName = "QL_trnjualmst" : sColStatusName = "trnjualstatus" : sIDColName = "trnjualmstoid"
                        ElseIf dtTbl.Rows(C1)("reftype").ToString.ToUpper = "QL_TRNTRANSITEMMST" Then
                            sTableName = "QL_trntransitemmst" : sColStatusName = "transitemmststatus" : sIDColName = "transitemmstoid"
                        End If
                        If CInt(curroid.SelectedValue) <> iARCurrOid Then
                            If curroid.SelectedItem.Text = "IDR" Then
                                dPayARIDR = (ToDouble(dtTbl.Rows(C1)("payaramtother").ToString) / ToDouble(dtTbl.Rows(C1)("payaramtpost").ToString)) * (ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString))
                                If sARCurrCode = "USD" Then
                                    dPayARUSD = ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)
                                Else
                                    dPayARUSD = (ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)) * cRate2.GetRateMonthlyUSDValue
                                End If
                            ElseIf curroid.SelectedItem.Text = "USD" Then
                                If sARCurrCode = "IDR" Then
                                    dPayARIDR = ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)
                                Else
                                    dPayARIDR = (ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)) * cRate2.GetRateMonthlyUSDValue
                                End If
                                dPayARUSD = (ToDouble(dtTbl.Rows(C1)("payaramtother").ToString) / ToDouble(dtTbl.Rows(C1)("payaramtpost").ToString)) * (ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString))
                            Else
                                If sARCurrCode = "IDR" Then
                                    dPayARIDR = ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)
                                    dPayARUSD = (ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)) * cRate2.GetRateMonthlyUSDValue
                                ElseIf sARCurrCode = "USD" Then
                                    dPayARIDR = (ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)) * cRate2.GetRateMonthlyIDRValue
                                    dPayARUSD = ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)
                                Else
                                    dPayARIDR = (ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)) * cRate2.GetRateMonthlyIDRValue
                                    dPayARUSD = (ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)) * cRate2.GetRateMonthlyUSDValue
                                End If
                            End If
                        Else
                            dPayARIDR = (ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)) * cRate2.GetRateMonthlyIDRValue
                            dPayARUSD = (ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)) * cRate2.GetRateMonthlyUSDValue
                        End If
                        ' Insert To QL_trnpayar
                        sSql = "INSERT INTO QL_trnpayar (cmpcode, payaroid, cashbankoid, custoid, reftype, refoid, acctgoid, payarrefno, payarduedate, payaramt, payaramtidr, payaramtusd, payarnote, payarstatus, createuser, createtime, upduser, updtime, payarres1, payaramtother, salescode, komisipersen, komisiamtidr, komisiamtusd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & CInt(payaroid.Text) + C1 & ", " & cashbankoid.Text & ", " & custoid.Text & ", '" & dtTbl.Rows(C1)("reftype").ToString & "', " & dtTbl.Rows(C1)("aritemmstoid").ToString & ", " & dtTbl.Rows(C1)("aracctgoid").ToString & ", '" & Tchar(cashbankrefno.Text) & "', '" & sDueDate & "', " & ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString) & ", " & dPayARIDR & ", " & dPayARUSD & ", '" & Tchar(dtTbl.Rows(C1)("payarnote").ToString) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & dtTbl.Rows(C1)("payarflag").ToString & "', " & ToDouble(dtTbl.Rows(C1)("payaramtother").ToString) & ", '" & dtTbl.Rows(C1)("salescode").ToString & "', " & ToDouble(dtTbl.Rows(C1)("komisi").ToString) & ", " & dPayARIDR * (ToDouble(dtTbl.Rows(C1)("komisi").ToString)) / 100 & ",  " & dPayARUSD * (ToDouble(dtTbl.Rows(C1)("komisi").ToString)) / 100 & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        ' Insert To QL_conar
                        sSql = "INSERT INTO QL_conar (cmpcode, conaroid, reftype, refoid, payrefoid, custoid, acctgoid, trnarstatus, trnartype, trnardate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans, amtbayar, trnarnote, createuser, createtime, upduser, updtime, amttransidr, amtbayaridr, amttransusd, amtbayarusd, amtbayarother) VALUES ('" & DDLBusUnit.SelectedValue & "', " & CInt(conaroid.Text) + C1 & ", '" & dtTbl.Rows(C1)("reftype").ToString & "', " & dtTbl.Rows(C1)("aritemmstoid").ToString & ", " & CInt(payaroid.Text) + C1 & ", " & custoid.Text & ", " & dtTbl.Rows(C1)("aracctgoid").ToString & ", '" & cashbankstatus.Text & "', 'PAYAR', '1/1/1900', '" & periodacctg.Text & "', " & IIf(cashbanktype.SelectedValue = "BGM", iARGiroAcctgOid, acctgoid.SelectedValue) & ", '" & cashbankdate.Text & "', '" & Tchar(cashbankrefno.Text) & "', " & IIf(cashbanktype.SelectedValue = "BKM" Or cashbanktype.SelectedValue = "BLM", 0, acctgoid.SelectedValue) & ", '" & sDueDate & "', 0, " & ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString) & ", 'Payment A/R No. " & dtTbl.Rows(C1)("aritemno").ToString & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, " & (ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)) * ToDouble(dtTbl.Rows(C1)("arratetoidr").ToString) & ", 0, " & (ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)) * ToDouble(dtTbl.Rows(C1)("arratetousd").ToString) & ", " & ToDouble(dtTbl.Rows(C1)("payaramtother").ToString) & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        If dtTbl.Rows(C1)("payarflag").ToString = "" Then
                            If ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) >= ToDouble(dtTbl.Rows(C1)("arbalance").ToString) Then
                                sSql = "UPDATE " & sTableName & " SET " & sColStatusName & "='Closed', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND " & sIDColName & "=" & dtTbl.Rows(C1)("aritemmstoid").ToString
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                            End If
                        End If

                        sSql = "UPDATE QL_mstcustgroup SET custcreditlimitusagerupiah = custcreditlimitusagerupiah - " & ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString) & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND custgroupoid=" & iCustGroupOid & ""
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (dtTbl.Rows.Count - 1 + CInt(payaroid.Text)) & " WHERE tablename='QL_TRNPAYAR' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (dtTbl.Rows.Count - 1 + CInt(conaroid.Text)) & " WHERE tablename='QL_CONAR' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    If cashbankstatus.Text = "Post" Then
                        Dim sGLDate As String = cashbankdate.Text
                        If cashbanktype.SelectedValue = "BBM" Then
                            sGLDate = cashbankduedate.Text
                        End If
                        ' Insert GL MST
                        sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & glmstoid & ", '" & sGLDate & "', '" & GetDateToPeriodAcctg(CDate(sGLDate)) & "', 'Payment A/R|No. " & cashbankno.Text & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        ' Insert GL DTL
                        Dim iSeq As Integer = 1
                        Dim dVal As Double = ToDouble(cashbankamt.Text)
                        If CInt(curroid.SelectedValue) <> iARCurrOid Then
                            dVal = ToDouble(dtSum.Compute("SUM(acctgamt)", "acctgflag IN ('(#)', '(+)')").ToString) - ToDouble(dtSum.Compute("SUM(acctgamt)", "acctgflag='(-)'").ToString)
                        End If
                        Dim dValPlus As Double = ToDouble(dtSum.Compute("SUM(acctgamt)", "acctgflag='(+)'").ToString)
                        Dim dValMin As Double = ToDouble(dtSum.Compute("SUM(acctgamt)", "acctgflag='(-)'").ToString)
                        Dim dValPlusOther As Double = ToDouble(dtSum.Compute("SUM(acctgamtother)", "acctgflag='(+)'").ToString)
                        Dim dValMinOther As Double = ToDouble(dtSum.Compute("SUM(acctgamtother)", "acctgflag='(-)'").ToString)
                        Dim dValIDR As Double = Math.Round(ToDouble(dtSum.Compute("SUM(acctgamtidr)", "acctgflag='(#)'").ToString), iRoundDigit, MidpointRounding.AwayFromZero)
                        Dim dValUSD As Double = Math.Round(ToDouble(dtSum.Compute("SUM(acctgamtusd)", "acctgflag='(#)'").ToString), iRoundDigit, MidpointRounding.AwayFromZero)
                        Dim dValPayIDR As Double = Math.Round(ToDouble(cashbankamt.Text) * cRate.GetRateMonthlyIDRValue, iRoundDigit, MidpointRounding.AwayFromZero)
                        Dim dValPayUSD As Double = Math.Round(ToDouble(cashbankamt.Text) * cRate.GetRateMonthlyUSDValue, iRoundDigit, MidpointRounding.AwayFromZero)
                        Dim dValPlusPayIDR As Double = Math.Round(dValPlus * cRate2.GetRateMonthlyIDRValue, iRoundDigit, MidpointRounding.AwayFromZero)
                        Dim dValPlusPayUSD As Double = Math.Round(dValPlus * cRate2.GetRateMonthlyUSDValue, iRoundDigit, MidpointRounding.AwayFromZero)
                        Dim dValMinPayIDR As Double = Math.Round(dValMin * cRate2.GetRateMonthlyIDRValue, iRoundDigit, MidpointRounding.AwayFromZero)
                        Dim dValMinPayUSD As Double = Math.Round(dValMin * cRate2.GetRateMonthlyUSDValue, iRoundDigit, MidpointRounding.AwayFromZero)

                        Dim dAddCost1IDR As Decimal = Math.Round(ToDouble(addacctgamt1.Text) * cRate.GetRateMonthlyIDRValue, iRoundDigit, MidpointRounding.AwayFromZero)
                        Dim dAddCost2IDR As Decimal = Math.Round(ToDouble(addacctgamt2.Text) * cRate.GetRateMonthlyIDRValue, iRoundDigit, MidpointRounding.AwayFromZero)
                        Dim dAddCost3IDR As Decimal = Math.Round(ToDouble(addacctgamt3.Text) * cRate.GetRateMonthlyIDRValue, iRoundDigit, MidpointRounding.AwayFromZero)
                        Dim dAddCost1USD As Decimal = Math.Round(ToDouble(addacctgamt1.Text) * cRate.GetRateMonthlyUSDValue, iRoundDigit, MidpointRounding.AwayFromZero)
                        Dim dAddCost2USD As Decimal = Math.Round(ToDouble(addacctgamt2.Text) * cRate.GetRateMonthlyUSDValue, iRoundDigit, MidpointRounding.AwayFromZero)
                        Dim dAddCost3USD As Decimal = Math.Round(ToDouble(addacctgamt3.Text) * cRate.GetRateMonthlyUSDValue, iRoundDigit, MidpointRounding.AwayFromZero)
                        Dim dAddCost1 As Decimal = dAddCost1IDR / cRate2.GetRateMonthlyIDRValue
                        Dim dAddCost2 As Decimal = dAddCost2IDR / cRate2.GetRateMonthlyIDRValue
                        Dim dAddCost3 As Decimal = dAddCost3IDR / cRate2.GetRateMonthlyIDRValue

                        If CInt(curroid.SelectedValue) <> iARCurrOid Then
                            If curroid.SelectedItem.Text = "IDR" Then
                                dValPayIDR = ToDouble(cashbankamt.Text)
                                dValPlusPayIDR = dValPlusOther
                                dValMinPayIDR = dValMinOther
                                If sARCurrCode = "USD" Then
                                    dValPayUSD = dVal
                                    dValPlusPayUSD = dValPlus
                                    dValMinPayUSD = dValMin
                                End If
                            ElseIf curroid.SelectedItem.Text = "USD" Then
                                If sARCurrCode = "IDR" Then
                                    dValPayIDR = dVal
                                    dValPlusPayIDR = dValPlus
                                    dValMinPayIDR = dValMin
                                End If
                                dValPayUSD = ToDouble(cashbankamt.Text)
                                dValPlusPayUSD = dValPlusOther
                                dValMinPayUSD = dValMinOther
                            Else
                                If sARCurrCode = "IDR" Then
                                    dValPayIDR = dVal
                                    dValPayUSD = dVal * cRate2.GetRateMonthlyUSDValue
                                    dValPlusPayIDR = dValPlus
                                    dValMinPayIDR = dValMin
                                ElseIf sARCurrCode = "USD" Then
                                    dValPayIDR = dVal * cRate2.GetRateMonthlyIDRValue
                                    dValPayUSD = dVal
                                    dValPlusPayUSD = dValPlus
                                    dValMinPayUSD = dValMin
                                Else
                                    dValPayIDR = dVal * cRate2.GetRateMonthlyIDRValue
                                    dValPayUSD = dVal * cRate2.GetRateMonthlyUSDValue
                                End If
                            End If
                        End If

                        Dim dvSum As DataView = dtSum.DefaultView
                        ' === DEBET
                        If cashbanktype.SelectedValue = "BGM" Then
                            ' Piutang Giro
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & iARGiroAcctgOid & ", 'D', " & dVal + dAddCost1 + dAddCost2 + dAddCost3 & ", '" & cashbankno.Text & "', 'Payment A/R|No. " & cashbankno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dValPayIDR + dAddCost1IDR + dAddCost2IDR + dAddCost3IDR & ", " & dValPayUSD + dAddCost1USD + dAddCost2USD + dAddCost3USD & ", 'QL_trncashbankmst " & cashbankoid.Text & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            gldtloid += 1
                            iSeq += 1
                        Else
                            ' Kas/Bank
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & acctgoid.SelectedValue & ", 'D', " & dVal + dAddCost1 + dAddCost2 + dAddCost3 & ", '" & cashbankno.Text & "', 'Payment A/R|No. " & cashbankno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dValPayIDR + dAddCost1IDR + dAddCost2IDR + dAddCost3IDR & ", " & dValPayUSD + dAddCost1USD + dAddCost2USD + dAddCost3USD & ", 'QL_trncashbankmst " & cashbankoid.Text & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            gldtloid += 1
                            iSeq += 1
                        End If
                        ' Kekurangan Bayar
                        dvSum.RowFilter = "acctgflag='(-)'"
                        For C1 As Integer = 0 To dvSum.Count - 1
                            Dim dIDR As Double = ToDouble(dvSum(C1)("acctgamt").ToString) * cRate2.GetRateMonthlyIDRValue
                            Dim dUSD As Double = ToDouble(dvSum(C1)("acctgamt").ToString) * cRate2.GetRateMonthlyUSDValue
                            If CInt(curroid.SelectedValue) <> iARCurrOid Then
                                If curroid.SelectedItem.Text = "IDR" Then
                                    dIDR = ToDouble(dvSum(C1)("acctgamtother").ToString)
                                    If sARCurrCode = "USD" Then
                                        dUSD = ToDouble(dvSum(C1)("acctgamt").ToString)
                                    End If
                                ElseIf curroid.SelectedItem.Text = "USD" Then
                                    If sARCurrCode = "IDR" Then
                                        dIDR = ToDouble(dvSum(C1)("acctgamt").ToString)
                                    End If
                                    dUSD = ToDouble(dvSum(C1)("acctgamtother").ToString)
                                Else
                                    If sARCurrCode = "IDR" Then
                                        dIDR = ToDouble(dvSum(C1)("acctgamt").ToString)
                                    ElseIf sARCurrCode = "USD" Then
                                        dUSD = ToDouble(dvSum(C1)("acctgamt").ToString)
                                    End If
                                End If
                            End If
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & dvSum(C1)("acctgoid").ToString & ", 'D', " & ToDouble(dvSum(C1)("acctgamt").ToString) & ", '" & cashbankno.Text & "', 'Payment A/R|No. " & cashbankno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dIDR & ", " & dUSD & ", 'QL_trncashbankmst " & cashbankoid.Text & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            gldtloid += 1
                            iSeq += 1
                        Next
                        dvSum.RowFilter = ""

                        ' Additional Cost 1+
                        If ToDouble(addacctgamt1.Text) < 0 Then
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & addacctgoid1.SelectedValue & ", 'D', " & -1 * dAddCost1 & ", '" & cashbankno.Text & "', 'Payment A/R|No. " & cashbankno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & -1 * dAddCost1IDR & ", " & -1 * dAddCost1USD & ", 'QL_trncashbankmst " & cashbankoid.Text & "', 'Additional Cost 1 - Payment A/R|No. " & cashbankno.Text & "', 'K')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            gldtloid += 1 : iSeq += 1
                        End If
                        ' Additional Cost 2+
                        If ToDouble(addacctgamt2.Text) < 0 Then
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & addacctgoid2.SelectedValue & ", 'D', " & -1 * dAddCost2 & ", '" & cashbankno.Text & "', 'Payment A/R|No. " & cashbankno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & -1 * dAddCost2IDR & ", " & -1 * dAddCost2USD & ", 'QL_trncashbankmst " & cashbankoid.Text & "', 'Additional Cost 2 - Payment A/R|No. " & cashbankno.Text & "', 'K')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            gldtloid += 1 : iSeq += 1
                        End If
                        ' Additional Cost 3+
                        If ToDouble(addacctgamt3.Text) < 0 Then
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & addacctgoid3.SelectedValue & ", 'D', " & -1 * dAddCost3 & ", '" & cashbankno.Text & "', 'Payment A/R|No. " & cashbankno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & -1 * dAddCost3IDR & ", " & -1 * dAddCost3USD & ", 'QL_trncashbankmst " & cashbankoid.Text & "', 'Additional Cost 3 - Payment A/R|No. " & cashbankno.Text & "', 'K')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            gldtloid += 1 : iSeq += 1
                        End If
                        ' === CREDIT
                        ' Piutang Usaha
                        dvSum.RowFilter = "acctgflag='(#)'"
                        For C1 As Integer = 0 To dvSum.Count - 1
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & dvSum(C1)("acctgoid").ToString & ", 'C', " & ToDouble(dvSum(C1)("acctgamt").ToString) & ", '" & cashbankno.Text & "', 'Payment A/R|No. " & cashbankno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dvSum(C1)("acctgamtidr").ToString) & ", " & ToDouble(dvSum(C1)("acctgamtusd").ToString) & ", 'QL_trncashbankmst " & cashbankoid.Text & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            gldtloid += 1
                            iSeq += 1
                        Next
                        dvSum.RowFilter = ""

                        ' Additional Cost 1+
                        If ToDouble(addacctgamt1.Text) > 0 Then
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & addacctgoid1.SelectedValue & ", 'C', " & dAddCost1 & ", '" & cashbankno.Text & "', 'Payment A/R|No. " & cashbankno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dAddCost1IDR & ", " & dAddCost1USD & ", 'QL_trncashbankmst " & cashbankoid.Text & "','Additional Cost 1 - Payment A/R|No. " & cashbankno.Text & "','M')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            gldtloid += 1 : iSeq += 1
                        End If
                        ' Additional Cost 2+
                        If ToDouble(addacctgamt2.Text) > 0 Then
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & addacctgoid2.SelectedValue & ", 'C', " & dAddCost2 & ", '" & cashbankno.Text & "', 'Payment A/R|No. " & cashbankno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dAddCost2IDR & ", " & dAddCost2USD & ", 'QL_trncashbankmst " & cashbankoid.Text & "','Additional Cost 2 - Payment A/R|No. " & cashbankno.Text & "','M')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            gldtloid += 1 : iSeq += 1
                        End If
                        ' Additional Cost 3+
                        If ToDouble(addacctgamt3.Text) > 0 Then
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & addacctgoid3.SelectedValue & ", 'C', " & dAddCost3 & ", '" & cashbankno.Text & "', 'Payment A/R|No. " & cashbankno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dAddCost3IDR & ", " & dAddCost3USD & ", 'QL_trncashbankmst " & cashbankoid.Text & "','Additional Cost 3 - Payment A/R|No. " & cashbankno.Text & "','M')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            gldtloid += 1 : iSeq += 1
                        End If

                        ' Kelebihan Bayar
                        dvSum.RowFilter = "acctgflag='(+)'"
                        For C1 As Integer = 0 To dvSum.Count - 1
                            Dim dIDR As Double = ToDouble(dvSum(C1)("acctgamt").ToString) * cRate2.GetRateMonthlyIDRValue
                            Dim dUSD As Double = ToDouble(dvSum(C1)("acctgamt").ToString) * cRate2.GetRateMonthlyUSDValue
                            If CInt(curroid.SelectedValue) <> iARCurrOid Then
                                If curroid.SelectedItem.Text = "IDR" Then
                                    dIDR = ToDouble(dvSum(C1)("acctgamtother").ToString)
                                    If sARCurrCode = "USD" Then
                                        dUSD = ToDouble(dvSum(C1)("acctgamt").ToString)
                                    End If
                                ElseIf curroid.SelectedItem.Text = "USD" Then
                                    If sARCurrCode = "IDR" Then
                                        dIDR = ToDouble(dvSum(C1)("acctgamt").ToString)
                                    End If
                                    dUSD = ToDouble(dvSum(C1)("acctgamtother").ToString)
                                Else
                                    If sARCurrCode = "IDR" Then
                                        dIDR = ToDouble(dvSum(C1)("acctgamt").ToString)
                                    ElseIf sARCurrCode = "USD" Then
                                        dUSD = ToDouble(dvSum(C1)("acctgamt").ToString)
                                    End If
                                End If
                            End If
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & dvSum(C1)("acctgoid").ToString & ", 'C', " & ToDouble(dvSum(C1)("acctgamt").ToString) & ", '" & cashbankno.Text & "', 'Payment A/R|No. " & cashbankno.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dIDR & ", " & dUSD & ", 'QL_trncashbankmst " & cashbankoid.Text & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            gldtloid += 1
                            iSeq += 1
                        Next
                        dvSum.RowFilter = ""
                        Dim IsDP As Boolean = False
                        If cashbanktype.SelectedValue <> "BLM" Then
                            ' Kelebihan Bayar (DP)
                            dvSum.RowFilter = "acctgflag='(+)' AND acctgoid=" & sOidDP
                            For C1 As Integer = 0 To dvSum.Count - 1
                                IsDP = True
                                Dim dIDR As Double = ToDouble(dvSum(C1)("acctgamt").ToString) * cRate2.GetRateMonthlyIDRValue
                                Dim dUSD As Double = ToDouble(dvSum(C1)("acctgamt").ToString) * cRate2.GetRateMonthlyUSDValue
                                If CInt(curroid.SelectedValue) <> iARCurrOid Then
                                    If curroid.SelectedItem.Text = "IDR" Then
                                        dIDR = ToDouble(dvSum(C1)("acctgamtother").ToString)
                                        If sARCurrCode = "USD" Then
                                            dUSD = ToDouble(dvSum(C1)("acctgamt").ToString)
                                        End If
                                    ElseIf curroid.SelectedItem.Text = "USD" Then
                                        If sARCurrCode = "IDR" Then
                                            dIDR = ToDouble(dvSum(C1)("acctgamt").ToString)
                                        End If
                                        dUSD = ToDouble(dvSum(C1)("acctgamtother").ToString)
                                    Else
                                        If sARCurrCode = "IDR" Then
                                            dIDR = ToDouble(dvSum(C1)("acctgamt").ToString)
                                        ElseIf sARCurrCode = "USD" Then
                                            dUSD = ToDouble(dvSum(C1)("acctgamt").ToString)
                                        End If
                                    End If
                                End If

                                'Insert ke DPAR jika lebih bayar akun DPAR
                                sSql = "INSERT INTO QL_trncashbankmst (cmpcode, cashbankoid, periodacctg, cashbankno, cashbankdate, cashbanktype, cashbankgroup, acctgoid, curroid, cashbankamt, cashbankamtidr, cashbankamtusd, personoid, cashbankduedate, cashbankrefno, cashbanknote, cashbankstatus, createuser, createtime, upduser, updtime, cashbanktakegiro, giroacctgoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iCBMstoid2 & ", '" & periodacctg.Text & "', '" & cashbankno2.Text & "', '" & cashbankdate.Text & "', '" & cashbanktype.SelectedValue & "', 'DPAR', " & acctgoid.SelectedValue & ", " & curroid.SelectedValue & ", " & ToDouble(dvSum(C1)("acctgamt").ToString) & ", " & dIDR & ", " & dUSD & ", " & custoid.Text & ", '" & IIf(cashbanktype.SelectedValue <> "BKM", cashbankduedate.Text, "1/1/1900") & "', '" & Tchar(cashbankrefno.Text) & "', 'DPAR Lebih Bayar From " & Tchar(cashbankno.Text) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & IIf(cashbanktype.SelectedValue = "BGM", cashbanktakegiro.Text, "1/1/1900") & "', " & iARGiroAcctgOid & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                sSql = "INSERT INTO QL_trndpar (cmpcode, dparoid, periodacctg, dparno, dpardate, custoid, acctgoid, cashbankoid, dparpaytype, dparpayacctgoid, dparpayrefno, dparduedate, curroid, rateoid, rate2oid, dparamt, dparaccumamt, dparnote, dparstatus, createuser, createtime, upduser, updtime, dpartakegiro, giroacctgoid, dparamtidr, dparamtusd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iDPAROid & ", '" & periodacctg.Text & "', '" & dparno.Text & "', '" & cashbankdate.Text & "', " & custoid.Text & ", " & dvSum(C1)("acctgoid").ToString & ", " & iCBMstoid2 & ", '" & cashbanktype.SelectedValue & "', " & acctgoid.SelectedValue & ", '" & Tchar(cashbankrefno.Text) & "', '" & IIf(cashbanktype.SelectedValue <> "BKM", cashbankduedate.Text, "1/1/1900") & "', " & curroid.SelectedValue & ", " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & ToDouble(dvSum(C1)("acctgamt").ToString) & ", 0, 'DPAR Lebih Bayar Dari " & Tchar(cashbankno.Text) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & IIf(cashbanktype.SelectedValue = "BGM", cashbanktakegiro.Text, "1/1/1900") & "', " & iARGiroAcctgOid & ", " & dIDR & ", " & dUSD & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()

                                Dim sDate2 As String = cashbankdate.Text
                                If cashbanktype.SelectedValue = "BBK" Then
                                    sDate2 = cashbankduedate.Text
                                End If
                                Dim sPeriod2 As String = GetDateToPeriodAcctg(CDate(sDate2))
                                glmstoid += 1
                                '' Insert Into GL Mst
                                'sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & glmstoid & ", '" & sDate2 & "', '" & sPeriod2 & "', 'DP A/R Lebih Bayar|No=" & dparno.Text & "', '" & cashbankstatus.Text & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                                'xCmd.CommandText = sSql
                                'xCmd.ExecuteNonQuery()
                                'iSeq = 1
                                '' Insert Into GL Dtl
                                '' Cash/Bank/Giro
                                'sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & IIf(cashbanktype.SelectedValue = "BGM", iARGiroAcctgOid, acctgoid.SelectedValue) & ", 'D', " & ToDouble(dvSum(C1)("acctgamt").ToString) & ", '" & dparno.Text & "', 'DPAR Lebih Bayar Dari " & Tchar(cashbankno.Text) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dIDR & ", " & dUSD & ", 'QL_trndpar " & iDPAROid & "')"
                                'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                'iSeq += 1 : gldtloid += 1
                                '' Uang Muka
                                'sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & dvSum(C1)("acctgoid").ToString & ", 'C', " & ToDouble(dvSum(C1)("acctgamt").ToString) & ", '" & dparno.Text & "', 'DPAR Lebih Bayar Dari " & Tchar(cashbankno.Text) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dIDR & ", " & dUSD & ", 'QL_trndpar " & iDPAROid & "')"
                                'xCmd.CommandText = sSql
                                'xCmd.ExecuteNonQuery()
                                'gldtloid += 1 : iSeq += 1
                                'iCBMstoid2 += 1 : iDPAROid += 1
                            Next
                            If IsDP Then
                                sSql = "UPDATE QL_mstoid SET lastoid=" & iCBMstOid2 & " WHERE tablename='QL_TRNCASHBANKMST' and cmpcode='" & CompnyCode & "'"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()

                                sSql = "UPDATE QL_mstoid SET lastoid=" & iDPAROid & " WHERE tablename='QL_TRNDPAR' and cmpcode='" & CompnyCode & "'"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                            End If
                        End If
                        dvSum.RowFilter = ""
                        sSql = "UPDATE QL_mstoid SET lastoid=" & glmstoid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        sSql = "UPDATE QL_mstoid SET lastoid=" & gldtloid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                    objTrans.Commit()
                    conn.Close()
                End If
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        cashbankstatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    cashbankstatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 1)
                conn.Close()
                cashbankstatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Cash/Bank No. have been regenerated because being used by another data. Your new Cash/Bank No. is " & cashbankno.Text & ".<BR>"
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Accounting\trnPayAR.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Accounting\trnPayAR.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If cashbankoid.Text = "" Then
            showMessage("Please select A/R Payment data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_trncashbankmst", "cashbankoid", cashbankoid.Text, "cashbankstatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                cashbankstatus.Text = "In Process"
                Exit Sub
            End If
        End If

        Dim dtCurrentPayar As New DataTable
        If Session("oid") = Nothing Or Session("oid") = "" Then
        Else
            ' Select Current Payment detail on DB for later reverse
            sSql = "SELECT refoid,reftype FROM QL_trnpayar WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text & " AND ISNULL(payarres1,'')='' "
            dtCurrentPayar = cKon.ambiltabel(sSql, "QL_trnpayar")
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Reversing each detail A/P
            For R1 As Integer = 0 To dtCurrentPayar.Rows.Count - 1
                Dim sTableName As String = "" : Dim sColStatusName As String = "" : Dim sIDColName As String = ""
                Select Case dtCurrentPayar.Rows(R1)("reftype").ToString.ToUpper
                    Case "QL_TRNJUALMST"
                        sTableName = "QL_trnjualmst" : sColStatusName = "trnjualstatus" : sIDColName = "trnjualmstoid"
                    Case "QL_TRNTRANSITEMMST"
                        sTableName = "QL_trntransitemmst" : sColStatusName = "transitemmststatus" : sIDColName = "transitemmstoid"
                End Select
                sSql = "UPDATE " & sTableName & " SET " & sColStatusName & "='Post', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP " & _
                    "WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND " & sIDColName & "=" & dtCurrentPayar.Rows(R1)("refoid").ToString & " "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            Next

            sSql = "DELETE FROM QL_conar WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnartype='PAYAR' AND payrefoid IN (SELECT payaroid FROM QL_trnpayar WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnpayar WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If Session("lastcashbanktype") IsNot Nothing And Session("lastcashbanktype") = "BLM" Then
                sSql = "UPDATE QL_trndpar SET dparaccumamt = dparaccumamt - " & ToDouble(Session("lastdparamt").ToString) & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dparoid=" & Session("lastdparoid")
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Accounting\trnPayAR.aspx?awal=true")
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPost.Click
        periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
        If isPeriodClosed(DDLBusUnit.SelectedValue, periodacctg.Text) Then
            showMessage("Cannot continue this transaction anymore because the period : " & MonthName(CInt(periodacctg.Text.Substring(4, 2))) & " " & periodacctg.Text.Substring(0, 4) & " have been closed.", 3)
            Exit Sub
        End If
        cashbankstatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnShowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnShowCOA.Click
        DDLRateType.SelectedIndex = -1
        ShowCOAPosting(cashbankno.Text, DDLBusUnit.SelectedValue, gvCOAPosting, DDLRateType.SelectedValue, "QL_trncashbankmst " & cashbankoid.Text)
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, True)
    End Sub

    Protected Sub lkbCloseCOAPosting_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseCOAPosting.Click
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, False)
    End Sub

    Protected Sub gvCOAPosting_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCOAPosting.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), iMask)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), iMask)
        End If
    End Sub

    Protected Sub DDLRateType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLRateType.SelectedIndexChanged
        ShowCOAPosting(cashbankno.Text, DDLBusUnit.SelectedValue, gvCOAPosting, DDLRateType.SelectedValue, "QL_trncashbankmst " & cashbankoid.Text)
        mpeCOAPosting.Show()
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        UpdateCheckedValue()
        ShowReport()
    End Sub

    Protected Sub diffamount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Text = ToMaskEdit(ToDouble(diffamount.Text), 2)
    End Sub
#End Region

    Protected Sub gvListAR_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), iMask)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), iMask)
        End If
    End Sub
End Class
