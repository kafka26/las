<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnHPPClosing.aspx.vb" Inherits="Accounting_trnHPPClosing" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Menu.gif" />
                <asp:Label ID="Label1" runat="server" CssClass="Title" Font-Bold="False"
                    Text="Calculate HPP :."></asp:Label></th>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td colspan="3">
            </td>
        </tr>
        <tr>
            <td colspan="3" align="left" valign="top">
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD align=left></TD><TD align=left></TD></TR><TR><TD align=left><asp:Label id="Label2" runat="server" Text="Period :"></asp:Label>&nbsp;<asp:DropDownList id="DDLMonth" runat="server" CssClass="inpText" OnSelectedIndexChanged="DDLMonth_SelectedIndexChanged" AutoPostBack="True">
    </asp:DropDownList> <asp:DropDownList id="DDLYear" runat="server" CssClass="inpText" OnSelectedIndexChanged="DDLYear_SelectedIndexChanged" AutoPostBack="True">
    </asp:DropDownList> <asp:TextBox id="txtDate" runat="server" CssClass="inpText" Width="75px" Visible="False"></asp:TextBox></TD><TD align=right><asp:Label id="Label4" runat="server" Font-Bold="True" Text="# Last HPP Closing is :"></asp:Label> <asp:Label id="lastclosing" runat="server" Font-Bold="True" CssClass="Important"></asp:Label> <asp:Label id="Label5" runat="server" Font-Bold="True" Text="# Next HPP Closing is :"></asp:Label> <asp:Label id="nextclosing" runat="server" Font-Bold="True" CssClass="Important"></asp:Label></TD></TR><TR><TD align=left></TD><TD align=right></TD></TR><TR><TD align=left><asp:UpdatePanel id="upFind" runat="server"><ContentTemplate>
<TABLE><TBODY><TR><TD align=left><asp:Button id="btnFind" runat="server" Font-Bold="True" Text="CALCULATE" CssClass="green" Width="100px" EnableTheming="False"></asp:Button></TD><TD align=left><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upFind"><ProgressTemplate>
<asp:Image id="Image1" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle"></asp:Image>&nbsp;Please wait ... 
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE>
</ContentTemplate>
</asp:UpdatePanel></TD><TD align=right><asp:UpdatePanel id="upSave" runat="server"><ContentTemplate>
<TABLE><TBODY><TR><TD align=right><asp:UpdateProgress id="UpdateProgress2" runat="server" AssociatedUpdatePanelID="upSave"><ProgressTemplate>
<asp:Image id="Image3" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle"></asp:Image>&nbsp;Please wait ... 
</ProgressTemplate>
</asp:UpdateProgress></TD><TD align=right><asp:ImageButton id="imbUnPost" onclick="imbUnPost_Click" runat="server" ImageUrl="~/Images/unpost.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:Button id="btnSave" runat="server" Font-Bold="True" Text="POSTING" CssClass="orange" Width="75px"></asp:Button> <asp:Button id="btnCancel" runat="server" Font-Bold="True" Text="CANCEL" CssClass="gray" Width="75px"></asp:Button></TD></TR></TBODY></TABLE>
</ContentTemplate>
</asp:UpdatePanel></TD></TR><TR><TD align=left colSpan=2><asp:Label id="lblError" runat="server" CssClass="Important"></asp:Label></TD></TR><TR><TD align=left colSpan=2><asp:Label id="lbPurch" runat="server" Font-Bold="True" Text="Total Purchase:" Visible="False" Font-Underline="False"></asp:Label></TD></TR><TR><TD align=left colSpan=2><asp:GridView id="gvPurchase" runat="server" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<PagerSettings Mode="NumericFirstLast"></PagerSettings>

<RowStyle CssClass="gvrow"></RowStyle>
<Columns>
<asp:BoundField DataField="matcode" HeaderText="Code">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="matlongdesc" HeaderText="Description">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="awalqty" HeaderText="Qty Open">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="awalprice" HeaderText="Avg Open">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="awaltotal" HeaderText="Total Open">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="totalqty" HeaderText="Qty Purchase">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="totalprice" HeaderText="Total Purchase">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="returqty" HeaderText="Qty Return">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="totalretur" HeaderText="Total Return">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="akhirqty" HeaderText="Qty Final">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="akhirprice" HeaderText="Avg Final">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#F25407"></FooterStyle>

<PagerStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="Yellow" Font-Bold="True"></SelectedRowStyle>

<HeaderStyle CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left colSpan=2><asp:Label id="lbProd" runat="server" Font-Bold="True" Text="Total Production:" Visible="False" Font-Underline="False"></asp:Label></TD></TR><TR><TD align=left colSpan=2><asp:GridView id="gvUsage" runat="server" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<PagerSettings Mode="NumericFirstLast"></PagerSettings>

<RowStyle CssClass="gvrow"></RowStyle>
<Columns>
<asp:BoundField DataField="itemcode" HeaderText="Item Code">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdesc" HeaderText="Description">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="refqty" HeaderText="Qty Open" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="refhpp" HeaderText="Avg Open" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="reftotal" HeaderText="Total Open" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="saldoakhir" HeaderText="Qty Final" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="qtyitem" HeaderText="Result Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matcode" HeaderText="Mat Code">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="matlongdesc" HeaderText="Description">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="qtyusage" HeaderText="Qty Usage">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="totalusage" HeaderText="Total Usage">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#F25407"></FooterStyle>

<PagerStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="Yellow" Font-Bold="True"></SelectedRowStyle>

<HeaderStyle CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=left colSpan=2><asp:Label id="lbhpp" runat="server" Font-Bold="True" Text="Cost of Good Manufactured (COGM) Calculation:" Visible="False" Font-Underline="True"></asp:Label></TD></TR><TR><TD align=left colSpan=2><asp:GridView id="gvItemHPP" runat="server" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<PagerSettings Mode="NumericFirstLast"></PagerSettings>

<RowStyle CssClass="gvrow"></RowStyle>
<Columns>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Description">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="refqtyprod" HeaderText="Result Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="usagecost" HeaderText="Material Cost">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="laborcost" HeaderText="DL Cost">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ohdcost" HeaderText="OHD Cost">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reftotalprod" HeaderText="Amount Prod">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="refhppprod" HeaderText="Avg Prod">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#F25407"></FooterStyle>

<PagerStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="Yellow" Font-Bold="True"></SelectedRowStyle>

<HeaderStyle BackColor="#124E81" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=left colSpan=2><asp:Label id="lbNewAvg" runat="server" Font-Bold="True" Text="New Average Price :" Visible="False" Font-Underline="True"></asp:Label></TD></TR><TR><TD align=left colSpan=2><asp:GridView id="gvNewAvg" runat="server" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<PagerSettings Mode="NumericFirstLast"></PagerSettings>

<RowStyle CssClass="gvrow"></RowStyle>
<Columns>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Description">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="refqty" HeaderText="Qty Open">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="refhpp" HeaderText="HPP Open">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reftotal" HeaderText="Amount Open">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="refqtyprod" HeaderText="Qty Prod.">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="refhppprod" HeaderText="Price Prod">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reftotalprod" HeaderText="Amount Prod">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="refqtyadjin" HeaderText="Adj In Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="refhppadjin" HeaderText="Adj In Price">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reftotaladjin" HeaderText="Adj In Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="refhppakhir" HeaderText="New Avg">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="refqtyakhir" HeaderText="Qty Final" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reftotalakhir" HeaderText="Total Final" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#F25407"></FooterStyle>

<PagerStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="Yellow" Font-Bold="True"></SelectedRowStyle>

<HeaderStyle BackColor="#124E81" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left colSpan=2><asp:Label id="lblCOGS" runat="server" Font-Bold="True" Text="Cost of Good Sold (COGS) Calculation:" Visible="False" Font-Underline="True"></asp:Label></TD></TR><TR><TD align=left colSpan=2><asp:GridView id="gvCOGS" runat="server" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
                <RowStyle CssClass="gvrow" />
                <Columns>
                    <asp:BoundField DataField="item" HeaderText="Item">
                        <HeaderStyle CssClass="gvhdr" />
                    </asp:BoundField>
                    <asp:BoundField DataField="value" HeaderText="Value">
                        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Right" />
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                </Columns>
                <FooterStyle BackColor="#F25407" />
                <SelectedRowStyle BackColor="Yellow" Font-Bold="True" />
                <HeaderStyle CssClass="gvhdr" Font-Bold="True" />
                <AlternatingRowStyle CssClass="gvrowalternate" />
            </asp:GridView> </TD></TR><TR><TD align=left colSpan=2><asp:Label id="lblJournal" runat="server" Font-Bold="True" Text="HPP Journal:" Visible="False" Font-Underline="True"></asp:Label></TD></TR><TR><TD align=left colSpan=2><asp:GridView id="gvJournal" runat="server" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" PageSize="20">
<PagerSettings Mode="NumericFirstLast"></PagerSettings>

<RowStyle CssClass="gvrow"></RowStyle>
<Columns>
<asp:BoundField DataField="tipe" HeaderText="Tipe">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="refno" HeaderText="Ref No">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgcode" HeaderText="Acc No">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Acc Name">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="credit" HeaderText="Credit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Note" HeaderText="Note">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="tipejurnal" HeaderText="Flag">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#F25407"></FooterStyle>

<PagerStyle CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="Yellow" Font-Bold="True"></SelectedRowStyle>

<HeaderStyle CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=left colSpan=2><asp:Label id="lblPostingState" runat="server" Font-Bold="True" Text="* Already done Daily Posting for this date." CssClass="Important"></asp:Label></TD></TR><TR><TD align=left colSpan=2><asp:GridView id="gvPostList" runat="server" Width="50%" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
<RowStyle CssClass="gvrow"></RowStyle>
<Columns>
<asp:CommandField SelectText="Show" ShowSelectButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="tipe" HeaderText="Transaction">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="jml" HeaderText="Count">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteImageUrl="~/Images/del.jpeg" ShowDeleteButton="True" ButtonType="Image" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#F25407"></FooterStyle>

<SelectedRowStyle BackColor="Yellow" Font-Bold="True"></SelectedRowStyle>

<HeaderStyle BackColor="#124E81" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 5px" align=left colSpan=2></TD></TR><TR><TD align=left colSpan=2><asp:GridView id="gvMst" runat="server" Width="100%" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<PagerSettings Mode="NumericFirstLast"></PagerSettings>

<RowStyle CssClass="gvrow"></RowStyle>
<Columns>
<asp:BoundField DataField="reftype" HeaderText="Type">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="refcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="refdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="price" HeaderText="Price">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="disc" HeaderText="Disc.">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="subtotal" HeaderText="Sub Total">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" ForeColor="White"></FooterStyle>

<PagerStyle CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<SPAN style="COLOR: #ff0000"><asp:Label id="lblEmpty" runat="server" Text="Data tidak ditemukan !!"></asp:Label></SPAN>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left colSpan=2><asp:UpdatePanel id="upUnPOST" runat="server"><ContentTemplate>
<asp:Panel id="pnlUnPOST" runat="server" CssClass="modalMsgBox" Width="400px" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 100%"><TBODY><TR><TD align=center><asp:Label id="lblCaptUnPOST" runat="server" Font-Size="Medium" Font-Bold="True" Text="UNPOST CONFIRMATION"></asp:Label></TD></TR><TR><TD align=center><asp:Label id="lblGLIdUnPost" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" align=center>Silakan kontak&nbsp;Supervisor / Manager<BR />untuk UNPOST Daily Posting tanggal <asp:Label id="lblDateUnPOST" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD align=center><TABLE><TBODY><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: small" align=right>User Name :</TD><TD align=left><asp:DropDownList id="DDLUser" runat="server" CssClass="inpText" Width="180px"></asp:DropDownList></TD></TR><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: small" align=right>Password :</TD><TD align=left><asp:TextBox id="tbPwd" runat="server" Font-Size="Medium" CssClass="inpText" Width="175px" TextMode="Password"></asp:TextBox></TD></TR><TR><TD align=center colSpan=2><asp:Label id="lblUnPOSTMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD align=center colSpan=2><asp:ImageButton id="imbOKUnPOST" onclick="imbOKUnPOST_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbCancelUnPOST" onclick="imbCancelUnPOST_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></asp:Panel><asp:Button id="beUnPOST" runat="server" Visible="False"></asp:Button><ajaxToolkit:ModalPopupExtender id="mpeUnPOST" runat="server" TargetControlID="beUnPOST" PopupDragHandleControlID="lblCaptUnPOST" PopupControlID="pnlUnPOST" DropShadow="True" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
</asp:UpdatePanel></TD></TR></TBODY></TABLE>
</contenttemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>

