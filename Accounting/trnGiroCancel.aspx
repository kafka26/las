<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnGiroCancel.aspx.vb" Inherits="Transaction_TransferConfirmation" title="" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Giro Cancelation" CssClass="Title" ForeColor="SaddleBrown"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Giro Cancelation :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                <asp:UpdatePanel id="upPOClosing" runat="server">
                    <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left colSpan=3></TD><TD class="Label" align=left colSpan=1></TD><TD style="HEIGHT: 5px" class="Label" align=left colSpan=1></TD><TD class="Label" align=left colSpan=1></TD></TR><TR><TD style="WIDTH: 10%" class="Label" align=left><asp:Label id="Label2" runat="server" Text="Giro No."></asp:Label></TD><TD style="WIDTH: 2%" class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="mtno" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="imbFindMT" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbEraseMT" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton><asp:Label id="mtmstoid" runat="server" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label4" runat="server" Text="Cancel Date"></asp:Label>&nbsp;<asp:Label id="Label5" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="confirmdate" runat="server" CssClass="inpText" Width="75px" OnTextChanged="confirmdate_TextChanged" AutoPostBack="True"></asp:TextBox>&nbsp;<asp:ImageButton id="imbconfirmdate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton>&nbsp;<asp:Label id="Label6" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label> <asp:Label id="cashbankoid" runat="server" Visible="False"></asp:Label> <asp:Label id="cashbankgroup" runat="server" Visible="False"></asp:Label> <asp:Label id="curroid" runat="server" Visible="False" __designer:wfdid="w1"></asp:Label></TD></TR><TR><TD style="WIDTH: 10%" class="Label" align=left></TD><TD style="WIDTH: 2%" class="Label" align=center></TD><TD class="Label" align=left colSpan=4><asp:Panel id="pnlGiro" runat="server" Width="100%" Visible="False"><asp:GridView id="gvDtl" runat="server" ForeColor="#333333" Width="98%" DataKeyNames="cashbankoid,cashbankno,cashbankgroup,curroid" PageSize="5" OnSelectedIndexChanged="gvDtl_SelectedIndexChanged" OnRowDataBound="gvDtl_RowDataBound" OnPageIndexChanging="gvDtl_PageIndexChanging" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<RowStyle BackColor="#EFF3FB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="nmr" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankno" HeaderText="Cash/Bank No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankrefno" HeaderText="Giro No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankdate" HeaderText="Payment Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbanktakegiro" HeaderText="Date Take Giro">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankduedate" HeaderText="Due Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankamt" HeaderText="Total">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="createuser" HeaderText="Create User">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#2461BF" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                        <asp:Label ID="Label7" runat="server" ForeColor="Red" Text="No detail data !!"></asp:Label>
                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </asp:Panel> </TD></TR><TR><TD style="WIDTH: 10%" class="Label" align=left><asp:Label id="Label3" runat="server" Text="To Warehouse" Visible="False"></asp:Label></TD><TD style="WIDTH: 2%" class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="mtrwhoid" runat="server" CssClass="inpTextDisabled" Width="155px" Visible="False" Enabled="False"></asp:DropDownList></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left><ajaxToolkit:CalendarExtender id="ceeconfirmdate" runat="server" Format="MM/dd/yyyy" PopupButtonID="imbconfirmdate" TargetControlID="confirmdate"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeconfirmdate" runat="server" TargetControlID="confirmdate" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3></TD><TD class="Label" align=left colSpan=1></TD><TD style="HEIGHT: 5px" class="Label" align=left colSpan=1></TD><TD style="HEIGHT: 5px" class="Label" align=left colSpan=1><asp:Label id="periodacctg" runat="server"></asp:Label> <asp:DropDownList id="mtrwhfrom" runat="server" CssClass="inpText" Visible="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Label id="Label8" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Detail Payment :" Font-Underline="False"></asp:Label></TD><TD class="Label" align=left colSpan=1></TD><TD class="Label" align=left colSpan=1></TD><TD class="Label" align=left colSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Panel id="pnlData" runat="server" CssClass="inpText" Width="100%" ScrollBars="Vertical" Height="200px"><asp:GridView id="gvPODtl" runat="server" ForeColor="#333333" Width="98%" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#EFF3FB"></RowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbOK" runat="server" ForeColor="Transparent" Text='<%# eval("oid") %>' ToolTip='<%# eval("oid") %>' Checked="True" Enabled="False"></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="5%"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankno" HeaderText="BGK/BGM No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankrefno" HeaderText="Ref No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankdate" HeaderText="Payment Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankduedate" HeaderText="Due Date">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="invoiceno" HeaderText="Invoice No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payamtidr" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                <asp:Label ID="lblInfoInGV" runat="server" CssClass="Important" Text="Data can't be found!"></asp:Label>
            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </asp:Panel> </TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=3></TD><TD class="Label" align=left colSpan=1></TD><TD style="HEIGHT: 10px" class="Label" align=left colSpan=1></TD><TD style="HEIGHT: 10px" class="Label" align=left colSpan=1></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left colSpan=3>Processed By <asp:Label id="upduser" runat="server"></asp:Label>&nbsp;On <asp:Label id="updtime" runat="server"></asp:Label></TD><TD style="COLOR: #585858" class="Label" align=left colSpan=1></TD><TD style="COLOR: #585858" class="Label" align=left colSpan=1></TD><TD style="COLOR: #585858" class="Label" align=left colSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:ImageButton id="imbConfirm" runat="server" ImageUrl="~/Images/confirm.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD class="Label" align=left colSpan=1></TD><TD class="Label" align=left colSpan=1></TD><TD class="Label" align=left colSpan=1></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upPOClosing">
                                <ProgressTemplate>
                                    <span></span><span style="font-weight: bold; font-size: 10pt; color: purple">
                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />
                                        Please Wait .....</span><br />
                                </ProgressTemplate>
                            </asp:UpdateProgress> </DIV>
</contenttemplate>
                </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upListPO" runat="server">
        <contenttemplate>
<asp:Panel id="PanelListPO" runat="server" CssClass="modalBox" Width="800px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListPO" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Material Transfer"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListPR" runat="server" Width="100%" DefaultButton="btnFindListPO">
                <asp:Label id="Label24" runat="server" Text="Filter :"></asp:Label> <asp:DropDownList id="FilterDDLListPO" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="mtm.mtno">MT No.</asp:ListItem>
    <asp:ListItem Value="g.gendesc">Warehouse</asp:ListItem>
    <asp:ListItem Value="mtm.mtmstnote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListPO" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListPO" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewAllListPO" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; BORDER-BOTTOM-STYLE: none" id="Fieldset1"><DIV id="Div1"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="gvListPO" runat="server" BorderColor="Black" ForeColor="#333333" Width="98%" AutoGenerateColumns="False" CellPadding="4" GridLines="None" DataKeyNames="mtmstoid,mtno,mtrwhoid" BorderStyle="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Width="3%"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="3%"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="mtno" HeaderText="MT No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Width="20%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="mtdate" HeaderText="MT Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="warehouse" HeaderText="Warehouse">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="mtmstnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                            <asp:Label ID="lblInfoOnListGV" runat="server" CssClass="Important" Text="There is no data !!"></asp:Label>
                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> &nbsp;</DIV>&nbsp;</FIELDSET> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbListPO" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListPO" runat="server" TargetControlID="btnHiddenListPO" PopupDragHandleControlID="lblListPO" BackgroundCssClass="modalBackground" PopupControlID="PanelListPO"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListPO" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

