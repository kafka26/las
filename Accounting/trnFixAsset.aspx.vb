
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Data.OleDb
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Accounting_trnFixAsset
    Inherits System.Web.UI.Page

#Region "Variabel"
    Public DefCounter As String = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompnyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_CONN")
    Public objConn As New SqlConnection(ConnStr)
    Dim xCmd As New SqlCommand("", objConn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cProc As New ClassProcedure
    Private connExcel As New OleDbConnection
    Private adapt As New OleDbDataAdapter
    Dim dsData As New DataSet
    Dim cRate As New ClassRate
    Private ws As DataTable
    Dim ckon As New Koneksi
    Public sql_temp As String
    Private Report As New ReportDocument
    Public folderReport As String = "~/report/"
    'Public PrinterLX As String = ConfigurationSettings.AppSettings("PrintLX")
    Dim tempPayFlag As String = ""
#End Region

#Region "Prosedure"

    Private Sub ReAmountAccum()
        If fixfirstvalue.Text.Trim = "" Then
            fixfirstvalue.Text = 0
        End If
        If fixPresentValue.Text.Trim = "" Then
            fixPresentValue.Text = 0
        End If
    End Sub

    Private Sub ReAmountDep()
        If fixPresentValue.Text <> "" And fixdepmonth.Text <> "" Then
            If ToDecimal(fixPresentValue.Text <= 0) Then
                showMessage("Nilai buku harus lebih besar dari 0 !!", 2)
                fixdepval.Text = ""
                Exit Sub
            End If

            If ToDecimal(fixPresentValue.Text) > 0 Then
                accumDV.Text = ToMaskEdit(ToDecimal(fixfirstvalue.Text) - ToDecimal(fixPresentValue.Text), 2)
            End If

            If ToDecimal(fixdepmonth.Text) = "-1" Then
                fixdepval.Text = "0.00"
            Else
                fixdepval.Text = ToMaskEdit((ToDecimal(fixPresentValue.Text)) / Val(ToDecimal(fixdepmonth.Text)), 2)
                AccumVal.Text = ToMaskEdit((ToDecimal(fixPresentValue.Text)) / Val(ToDecimal(fixdepmonth.Text)), 2)
            End If

            If Session("oid") <> Nothing And Session("oid") <> "" Then
            Else
                Session("tbldtl") = Nothing
                GVFixedAssetdtl.DataSource = Nothing
                GVFixedAssetdtl.DataBind()
            End If
        End If
    End Sub

    Public Sub binddata(ByVal sFilter As String)
        Try
            Dim tgle As Date = CDate(txtPeriode1.Text)
            tgle = CDate(txtPeriode1.Text)
        Catch ex As Exception
            showMessage("Invalid format date !!", 2)
            Exit Sub
        End Try
        If CDate(txtPeriode1.Text) > CDate(txtPeriode2.Text) Then
            showMessage("Second/End Period must be greater than First Period !!", 2)
            Exit Sub
        End If

        sSql = "SELECT f.cmpcode, f.fixoid, f.fixcode, f.fixdesc, f.fixgroup, f.fixdate, f.fixfirstvalue, f.fixdepmonth, f.fixdepval, f.fixflag, f.upduser, f.updtime, f.fixpresentvalue, f.fixother, g.genoid, g.gendesc FROM QL_trnfixmst f INNER JOIN ql_mstacctg a ON a.acctgoid=f.acctgoid INNER JOIN QL_mstgen g ON g.genoid = f.fixgroup WHERE f.fixflag NOT IN ('DELETE') AND f.cmpcode='" & Session("CompnyCode") & "' AND f.fixoid > 0 " & sFilter & ""

        'If FilterDDL.SelectedValue <> "ALL" Then
        '    sSql += " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(txtFilter.Text) & "%'"
        'End If

        If cbPeriode.Checked Then
            sSql += "AND fixdate BETWEEN '" & CDate(txtPeriode1.Text) & "' AND '" & CDate(txtPeriode2.Text) & "' "
            'If cbDesc.Checked Then
            '    sSql += "AND fixdesc LIKE '%" & Tchar(txtFilter.Text) & "%' "
            '    If cbBlmPosting.Checked Then
            '        sSql += "AND fixflag<>'Post' "
            '    End If
            'ElseIf cbBlmPosting.Checked Then
            '    sSql += "AND fixflag<>'Post' "
            'End If
        ElseIf cbDesc.Checked Then
            'sSql += "AND fixdesc LIKE '%" & Tchar(txtFilter.Text) & "%' "
            If cbBlmPosting.Checked Then
                sSql += "AND fixflag <> 'Post' "
            End If
        ElseIf cbBlmPosting.Checked Then
            sSql += "AND fixflag <> 'Post' "
        End If
        sSql += "ORDER BY fixoid desc"

        Dim objTable As DataTable = ckon.ambiltabel(sSql, "QL_trnfixmst")
        Session("tbldata") = objTable
        GVFixedAsset.DataSource = objTable
        GVFixedAsset.DataBind()
    End Sub

    Private Sub BindDataFAP()
        sSql = "SELECT Mstoid,cmpcode,BeliNo,suppname,PayType FROM ( SELECT DISTINCT (fa.trnbelifamstoid) AS Mstoid,fa.cmpcode,fa.trnbelifano AS BeliNo,s.suppname,fa.trnbelifatype AS PayType FROM QL_trnbelimst_fa fa INNER JOIN QL_mstsupp s ON s.suppoid=fa.suppoid AND s.cmpcode=fa.cmpcode INNER JOIN ql_trnbelidtl_fa fad ON fad.cmpcode=fa.cmpcode AND fad.trnbelifamstoid=fa.trnbelifamstoid AND fad.trnbelifadtlusage < fad.trnbelifadtlqty WHERE fa.trnbelifamststatus<>'In Process' ) TblBeli WHERE cmpcode='" & Session("CompnyCode") & "' AND " & FilterFAPDDL.SelectedValue & " LIKE '%" & Tchar(FilterFAP.Text) & "%'"

        '" UNION " & _
        '" SELECT DISTINCT (p.trnbelimstoid) AS Mstoid ,p.cmpcode,p.trnbelino AS BeliNo,s.suppname,p.trnbelipaytype AS PayType FROM QL_trnbelimst p INNER JOIN QL_mstsupp s ON s.suppoid=p.suppoid INNER JOIN ql_trnbelidtl pd ON pd.cmpcode=p.cmpcode AND pd.trnbelimstoid=p.trnbelimstoid INNER JOIN QL_mstitem i ON i.itemoid=pd.itemoid INNER JOIN QL_mstgen gn ON gn.genoid=i.itemgroupoid INNER JOIN QL_mstacctg a ON a.acctgoid=s.apacctgoid WHERE p.trnbelimststatus = 'Post' AND p.trnbelimstoid > 0 ) TblBeli WHERE cmpcode='" & Session("CompnyCode") & "' AND " & FilterFAPDDL.SelectedValue & " LIKE '%" & Tchar(FilterFAP.Text) & "%'"

        Dim objTable As DataTable = ckon.ambiltabel(sSql, "QL_trnbelimst_fa")
        Session("tblFAP") = objTable
        GvFAPmst.DataSource = objTable
        GvFAPmst.DataBind()
    End Sub

    Private Sub BindDataFAD()

        sSql = "SELECT cmpcode,DtlOid,MstOid,BeliNo,ItemCode,ItemDesc,DtlAmtNetto,DtlQty FROM ( SELECT fad.cmpcode,fad.trnbelifadtloid AS DtlOid,fad.trnbelifamstoid AS MstOid,fam.trnbelifano AS BeliNo,fad.itemfacode AS ItemCode,fad.itemfadesc AS ItemDesc,fad.trnbelifadtlamtnetto AS DtlAmtNetto,fad.trnbelifadtlqty AS DtlQty,fad.trnbelifadtlusage AS UseQty  FROM QL_trnbelidtl_fa fad INNER JOIN QL_trnbelimst_fa fam ON fam.cmpcode=fad.cmpcode AND fam.trnbelifamstoid=fad.trnbelifamstoid WHERE fad.trnbelifadtlusage < fad.trnbelifadtlqty) tblFAP WHERE cmpcode='" & Session("CompnyCode") & "' AND MstOid=" & FAPmstoid.Text & " AND BeliNo='" & Tchar(FAPMst.Text) & "' AND " & FilterDDLListReg.SelectedValue & " LIKE '%" & Tchar(FilterTextListReg.Text) & "%'"

        '" UNION SELECT tb.cmpcode,tb.trnbelidtloid AS DtlOid,tb.trnbelimstoid AS MstOid,tbm.trnbelino AS BeliNo,it.itemcode AS ItemCode,it.itemLongDescription AS ItemDesc,tb.trnbelidtlamtnetto AS DtlAmtNetto,tb.trnbelidtlqty AS DtlQty,0.0 AS AS UseQty FROM QL_trnbelidtl tb INNER JOIN QL_mstitem it ON it.itemoid=tb.itemoid INNER JOIN ql_trnbelimst tbm ON tbm.cmpcode=tb.cmpcode AND tbm.trnbelimstoid=tb.trnbelimstoid 

        Dim obj As DataTable = ckon.ambiltabel(sSql, "QL_trnbelidtl_fa")
        Session("dtlFAP") = obj
        GvFAPdtl.DataSource = obj
        GvFAPdtl.DataBind()
        GvFAPdtl.Visible = True
    End Sub

    Protected Sub initAllDDL()
        sSql = "SELECT genoid,gendesc from QL_mstgen WHERE gengroup = 'ASSETTYPE'"
        FillDDL(fixgroup, sSql)
        sSql = "SELECT cmpcode,divcode from QL_mstdivision Where divcode='" & Session("CompnyCode") & "'"
        FillDDL(DDLoutlet, sSql)
        'BIND DATA YEAR
        Dim yearrange As Integer = 10
        Dim nowyear As String = CStr(GetServerTime().Year)
        ddlYear.Items.Clear()
        For i As Integer = 0 To yearrange - 1
            'ddlYear.Items.Insert(i, nowyear + i)
            ddlYear.Items.Insert(i, nowyear + i - (yearrange - 3))
            ddlYear.Items(i).Value = nowyear + i - (yearrange - 3)
        Next
        ddlYear.SelectedValue = CStr(GetServerTime().Year)
        fixDate.Text = Format(GetServerTime, "MM/dd/yyyy")
        sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & Session("CompnyCode") & "' AND activeflag='ACTIVE'"
        FillDDL(CurrDDL, sSql)
        cRate.SetRateValue(CInt(CurrDDL.SelectedValue), fixDate.Text)

    End Sub

    Private Sub fillTextBox(ByVal sCmpcode As String, ByVal vjurnaloid As String)
        Dim mySqlConn As New SqlConnection(ConnStr)
        Dim sqlSelect As String = "SELECT cmpcode, fixoid, fixdate, acctgoid, payacctgoid, fixdesc, fixlocation, fixperson, fixother, LEFT(convert(varchar,fixfirstvalue), LEN(convert(varchar,fixfirstvalue))-2) fixfirstvalue, fixdepmonth, LEFT(convert(varchar,fixdepval), LEN(convert(varchar,fixdepval))-2) fixdepval, fixflag, createuser, createtime, upduser, updtime, fixcode, fixpresentvalue, fixgroup, accumdepacctgoid  ,curroid ,rateoid ,rate2oid, accumdepexpacctgoid, fixlastasset, refname,fapurchasemstoid, fapurchasedtloid from QL_trnfixmst WHERE fixoid = " & vjurnaloid & " AND cmpcode = '" & sCmpcode & "'"

        Dim mySqlDA As New SqlClient.SqlDataAdapter(sqlSelect, ConnStr)
        Dim objDs As New DataSet
        Dim objTable As DataTable
        Dim objRow() As DataRow

        mySqlDA.Fill(objDs)
        objTable = objDs.Tables(0)
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

        If objRow.Length > 0 Then

            'DDLoutlet.SelectedValue = Trim(objRow(0)("cmpcode").ToString)
            fixmstoid.Text = Trim(objRow(0)("fixoid").ToString)
            fixDate.Text = Format(objRow(0)("fixdate"), "MM/dd/yyyy")
            DDLassets.SelectedValue = Trim(objRow(0)("acctgoid").ToString)
            DDLaccum.SelectedValue = Trim(objRow(0)("accumdepacctgoid").ToString)
            DDLadExpense.SelectedValue = Trim(objRow(0)("accumdepexpacctgoid").ToString)
            fixdesc.Text = Trim(objRow(0)("fixdesc").ToString)
            fixlocation.Text = Trim(objRow(0)("fixlocation").ToString)
            fixperson.Text = Trim(objRow(0)("fixperson").ToString)
            FAnote.Text = Trim(objRow(0)("fixother").ToString)
            fixfirstvalue.Text = ToMaskEdit(Trim(objRow(0)("fixfirstvalue").ToString), 2)
            'fixdepmonth.Text = CInt(Trim(objRow(0)("fixdepmonth").ToString))
            fixdepval.Text = ToMaskEdit(Trim(objRow(0)("fixdepval").ToString), 2)
            fixLastAsset.Text = ToMaskEdit(Trim(objRow(0)("fixLastAsset").ToString), 2)
            AccumVal.Text = ToMaskEdit(Trim(objRow(0)("fixdepval").ToString), 2)
            FAPmstoid.Text = Trim(objRow(0)("fapurchasemstoid").ToString)
            FAPDtl.Text = Trim(objRow(0)("fapurchasedtloid").ToString)
            lblUser.Text = Trim(objRow(0)("createuser").ToString)
            lblTime.Text = Trim(objRow(0)("createtime").ToString)
            upduser.Text = Trim(objRow(0)("upduser").ToString)
            updtime.Text = Trim(objRow(0)("updtime").ToString)
            FixCode.Text = Trim(objRow(0)("fixcode").ToString)
            fixgroup.SelectedValue = Trim(objRow(0)("fixgroup").ToString)
            fixPresentValue.Text = ToMaskEdit(Trim(objRow(0)("fixpresentvalue").ToString), 2)
            accumDV.Text = ToMaskEdit(ToDecimal(fixfirstvalue.Text) - ToDecimal(fixPresentValue.Text), 2)
            CurrDDL.SelectedValue = Trim(objRow(0)("curroid").ToString)
            RateOid.Text = Trim(objRow(0)("rateoid").ToString)
            Rate2Oid.Text = Trim(objRow(0)("rate2oid").ToString)

            RateToIDR.Text = ToMaskEdit(cRate.GetRateDailyIDRValue, GetRoundValue(cRate.GetRateDailyIDRValue.ToString))
            RateToUSD.Text = ToMaskEdit(cRate.GetRateDailyUSDValue, GetRoundValue(cRate.GetRateDailyUSDValue.ToString))
            Rate2ToIDR.Text = ToMaskEdit(cRate.GetRateMonthlyIDRValue, GetRoundValue(cRate.GetRateMonthlyIDRValue.ToString))
            Rate2ToUsd.Text = ToMaskEdit(cRate.GetRateMonthlyUSDValue, GetRoundValue(cRate.GetRateMonthlyUSDValue.ToString))

            Dim RN As String = Trim(objRow(0)("refname").ToString)
            If RN = "QL_trnbelimst_fa" Then
                sSql = "SELECT trnbelifano from QL_trnbelimst_fa WHERE trnbelifamstoid = " & FAPmstoid.Text & " AND cmpcode='" & Session("CompnyCode") & "'"
                FAPMst.Text = GetStrData(sSql)
            ElseIf RN = "QL_trnbelimst" Then
                sSql = "SELECT trnbelino from QL_trnbelimst WHERE trnbelimstoid = " & FAPmstoid.Text & " AND cmpcode='" & Session("CompnyCode") & "'"
                FAPMst.Text = GetStrData(sSql)
            End If

            Dim CUTOFFDATE As Date
            sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup ='CUTOFDATE' AND cmpcode='" & Session("CompnyCode") & "'"
            If Not IsDate(CDate(GetStrData(sSql))) Then
                showMessage("Invalid Cut Off Date setup on General group 'CUTOFDATE' !", 2)
                Exit Sub
            Else
                CUTOFFDATE = CDate(CDate(GetStrData(sSql)))
            End If
            CutofDate.Text = Format(CUTOFFDATE, "MM/dd/yyyy")

            sSql = "SELECT COUNT(LEFT(trnbelifano,4)) from QL_trnbelimst_fa WHERE trnbelifano = '" & FAPMst.Text & "'"
            If GetStrData(sSql) > 0 Then
                fixgroup.CssClass = "inpTextDisabled"
                fixgroup.Enabled = False
            Else
                fixgroup.CssClass = "inpText"
                fixgroup.Enabled = True
            End If

            If Trim(objRow(0)("fixflag").ToString) <> "Post" And Trim(objRow(0)("fixflag").ToString) <> "DISPOSED" Then
                btnSave.Visible = True
                btnDelete.Visible = True
                btnPosting.Visible = True
            Else
                btnSave.Visible = False
                btnDelete.Visible = False
                btnPosting.Visible = False
                btnGenerate.Visible = False
                fixfirstvalue.Enabled = False
                fixPresentValue.Enabled = False
                fixdepmonth.Enabled = False
                fixLastAsset.Enabled = False
            End If

            sSql = "SELECT genother5 FROM QL_mstgen WHERE gengroup = 'ASSETTYPE' AND genoid = '" & fixgroup.SelectedValue & "'"
            fixdepmonth.Text = GetStrData(sSql)
            If fixdepmonth.Text = "-1" Then
                fixLastAsset.Visible = False
                Label42.Visible = False
                Label16.Visible = True
            Else
                fixLastAsset.Visible = True
                Label42.Visible = True
                Label16.Visible = True
            End If

            'data detail
            sqlSelect = "SELECT cmpcode, fixdtloid, fixoid, fixdtlseq, fixperiod, fixperiodvalue, fixperioddepvalue, fixperioddepaccum, depcostacctgoid, accdepacctgoid, fixnote, fixpostdate, fixflag, crtuser, crttime, upduser, updtime from QL_trnfixdtl WHERE fixoid=" & vjurnaloid & " AND cmpcode='" & sCmpcode & "'"

            Dim mySqlDAdtl As New SqlClient.SqlDataAdapter(sqlSelect, ConnStr)
            Dim objDsDtl As New DataSet
            Dim objTableDtl As DataTable
            Dim objRowDtl() As DataRow

            mySqlDAdtl.Fill(objDsDtl)
            objTableDtl = objDsDtl.Tables(0)
            objRowDtl = objTableDtl.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            Session("tbldtl") = objDsDtl.Tables(0)

            GVFixedAssetdtl.DataSource = objDsDtl.Tables(0)
            GVFixedAssetdtl.DataBind()
            GVFixedAssetdtl.Visible = True
        End If
        mySqlConn.Close()
        DDLoutlet.Enabled = False
        DDLoutlet.CssClass = "inpTextDisabled"
    End Sub

    Private Sub GenerateMstOid()
        fixmstoid.Text = GenerateID("QL_trnfixmst", DDLoutlet.SelectedValue)
    End Sub

    Private Sub GenerateDtlOid()
        Session("dtloid") = GenerateID("QL_trnfixdtl", DDLoutlet.SelectedValue)
    End Sub

#End Region

#Region "Function"
    Private Function SetTableDetail() As DataTable
        Dim dtlTable As DataTable = New DataTable("TblDtl")
        dtlTable.Columns.Add("cmpcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("fixdtloid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("fixoid", Type.GetType("System.String"))
        dtlTable.Columns.Add("fixdtlseq", Type.GetType("System.String"))
        dtlTable.Columns.Add("fixperiod", Type.GetType("System.String"))
        dtlTable.Columns.Add("fixperiodvalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("fixperioddepvalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("fixperioddepaccum", Type.GetType("System.Double"))
        dtlTable.Columns.Add("depcostacctgoid", Type.GetType("System.String"))
        dtlTable.Columns.Add("accdepacctgoid", Type.GetType("System.String"))
        dtlTable.Columns.Add("fixnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("fixflag", Type.GetType("System.String"))
        dtlTable.Columns.Add("fixpostdate", Type.GetType("System.DateTime"))
        dtlTable.Columns.Add("upduser", Type.GetType("System.String"))
        dtlTable.Columns.Add("updtime", Type.GetType("System.DateTime"))
        Return dtlTable
    End Function

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub
#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim CompnyCode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = CompnyCode
            Session("sCmpcode") = CompnyCode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Accounting\trnFixAsset.aspx")
        End If
        If checkPagePermission("~\Accounting\trnFixAsset.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        Page.Title = CompnyName & " - Fix Asset "
        Session("oid") = Request.QueryString("oid")

        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        btnPostingDtl.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST all this data?');")

        If Not IsPostBack Then
            'binddata()
            Dim CUTOFFDATE As Date
            sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup ='CUTOFDATE' AND cmpcode='" & Session("CompnyCode") & "'"
            If Not IsDate(CDate(GetStrData(sSql))) Then
                showMessage("Invalid Cut Off Date setup on General group 'CUTOFDATE' !", 2)
                Exit Sub
            Else
                CUTOFFDATE = CDate(GetStrData(sSql))
            End If
            txtPeriode1.Text = Format(GetServerTime.AddDays(-2), "MM/dd/yyyy")
            txtPeriode2.Text = Format(GetServerTime, "MM/dd/yyyy")
            lblViewInfo.Visible = True
            fixgroup_SelectedIndexChanged(Nothing, Nothing)
            initAllDDL()
            'InitDDLCurr()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                lblCekPage.Text = "UPDATE"
                fillTextBox(Session("sCmpcode"), Session("oid"))
                lblPOST.Text = "In Process"
                upduser.Text = Session("UserID")
                updtime.Text = Now
                TabContainer1.ActiveTabIndex = 1
            Else
                GenerateMstOid()
                lblCekPage.Text = "NEW"
                CutofDate.Text = Format(CUTOFFDATE, "MM/dd/yyyy")
                btnDelete.Visible = False
                btnPosting.Visible = False
                fixLastAsset.Visible = False
                Label42.Visible = False
                Label16.Visible = False
                fixdepmonth.Text = "-1"
                fixgroup_SelectedIndexChanged(Nothing, Nothing)
                CurrDDL_SelectedIndexChanged(Nothing, Nothing)
                fixDate.Text = Format(Now, "MM/dd/yyyy")
                fixdepval.Text = "0.00"
                accumDV.Text = "0.00"
                upduser.Text = "-"
                updtime.Text = "-"
                lblUser.Text = Session("UserID")
                lblTime.Text = GetServerTime()
                lblPOST.Text = "In Process"
                TabContainer1.ActiveTabIndex = 0
            End If
            Dim dt As DataTable
            dt = Session("tbldtl")
            GVFixedAssetdtl.DataSource = dt
            GVFixedAssetdtl.DataBind()
        End If
    End Sub

    Protected Sub GVFixedAsset_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("trnFixAsset.aspx?cmpcode=" & GVFixedAsset.SelectedDataKey("cmpcode").ToString & "&oid=" & GVFixedAsset.SelectedDataKey("fixoid").ToString & "")
    End Sub

    Protected Sub GVFixedAsset_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GVFixedAsset.PageIndex = e.NewPageIndex
        binddata("")
    End Sub

    Protected Sub GVFixedAsset_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVFixedAsset.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = Format(CDate(e.Row.Cells(4).Text), "MM/dd/yyyy")
            e.Row.Cells(6).Text = ToMaskEdit(e.Row.Cells(6).Text, 2)
            e.Row.Cells(7).Text = ToMaskEdit(e.Row.Cells(7).Text, 2)
        End If
    End Sub

    Protected Sub GVFixedAssetdtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVFixedAssetdtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 2)
            e.Row.Cells(3).Text = ToMaskEdit(e.Row.Cells(3).Text, 2)
            e.Row.Cells(2).Text = ToMaskEdit(e.Row.Cells(2).Text, 2)
        End If
    End Sub

    Protected Sub GVFixedAssetdtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objTable As DataTable
        objTable = Session("tbldtl")
        Dim dv As DataView = objTable.DefaultView
        dv.RowFilter = "fixperiod='" & GVFixedAssetdtl.SelectedDataKey("fixperiod").ToString & "'"
        dv.RowFilter = "fixflag = 'In Process'"
        If dv.Count > 0 Then
            AccumVal.Text = ToMaskEdit(ToDecimal(dv(0)("fixperioddepaccum").ToString), 2)
        Else
            showMessage(" Tidak dapat mengeluarkan data detail !!", 2)
        End If
        dv.RowFilter = ""
    End Sub

    Protected Sub GVFixedAssetdtl_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GVFixedAssetdtl.PageIndex = e.NewPageIndex
        GVFixedAssetdtl.DataSource = Session("tbldtl")
        GVFixedAssetdtl.DataBind()
    End Sub

    Protected Sub GVDtlMonth_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GVDtlMonth.PageIndex = e.NewPageIndex
        GVDtlMonth.DataSource = Session("TblFixPeriodPosting")
        GVDtlMonth.DataBind()
    End Sub

    Protected Sub GVDtlMonth_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 2)
            e.Row.Cells(5).Text = ToMaskEdit(e.Row.Cells(5).Text, 2)
        End If
    End Sub

    Protected Sub GvFAPmst_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FAPMst.Text = GvFAPmst.SelectedDataKey(2).ToString().Trim
        FAPmstoid.Text = GvFAPmst.SelectedDataKey(1).ToString().Trim
        FAPcmpcode.Text = GvFAPmst.SelectedDataKey(0).ToString().Trim
        AssetType.Text = GvFAPmst.SelectedDataKey(4).ToString().Trim

        sSql = "SELECT COUNT(LEFT(trnbelifano,4)) from QL_trnbelimst_fa WHERE trnbelifano = '" & Tchar(FAPMst.Text) & "'"
        If GetStrData(sSql) > 0 Then
            fixgroup.SelectedValue = AssetType.Text
            fixgroup.CssClass = "inpTextDisabled"
            fixgroup.Enabled = False
            fixgroup_SelectedIndexChanged(sender, e)
        Else
            fixgroup.SelectedValue = Nothing
            fixgroup.CssClass = "inpText"
            fixgroup.Enabled = True
            fixdepmonth.Text = "-1"
        End If
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
        'GvFAPmst.Visible = False
    End Sub

    Protected Sub GvFAPmst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GvFAPmst.PageIndexChanging
        GvFAPmst.PageIndex = e.NewPageIndex
        BindDataFAP()
        mpeListSupp.Show()
        'GvFAPmst.PageIndex = e.NewPageIndex
        'GvFAPmst.DataSource = Session("tblFAP")
        'GvFAPmst.DataBind()
    End Sub

    Protected Sub GvFAPdtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GvFAPdtl.SelectedIndexChanged
        FAPDtl.Text = GvFAPdtl.SelectedDataKey(1).ToString().Trim
        FixCode.Text = GvFAPdtl.SelectedDataKey(4).ToString().Trim
        fixdesc.Text = GvFAPdtl.SelectedDataKey(5).ToString().Trim
        fixfirstvalue.Text = ToMaskEdit(GvFAPdtl.SelectedDataKey(6).ToString, 2)
        fixPresentValue.Text = ToMaskEdit(GvFAPdtl.SelectedDataKey(6).ToString, 2)
        ReAmountDep()
        cProc.SetModalPopUpExtender(btnHideListReg, pnlListReg, mpeListReg, False)
    End Sub

    Protected Sub fixgroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FillDDLAcctg(DDLassets, "VAR_ASSET", Session("CompnyCode"))
        sSql = "SELECT genother1 from QL_mstgen WHERE gengroup = 'ASSETTYPE' and genoid = '" & fixgroup.SelectedValue & "'"
        DDLassets.SelectedValue = GetStrData(sSql)

        FillDDLAcctg(DDLaccum, "VAR_ASSET_ACCUM", Session("CompnyCode"))
        sSql = "SELECT genother2 from QL_mstgen WHERE gengroup = 'ASSETTYPE' and genoid = '" & fixgroup.SelectedValue & "'"
        DDLaccum.SelectedValue = GetStrData(sSql)

        FillDDLAcctg(DDLadExpense, "VAR_ACCUM_DEP_EXPENSE", Session("CompnyCode"))
        sSql = "SELECT genother3 from QL_mstgen WHERE gengroup = 'ASSETTYPE' and genoid = '" & fixgroup.SelectedValue & "'"
        DDLadExpense.SelectedValue = GetStrData(sSql)

        sSql = "SELECT genother5 from QL_mstgen WHERE gengroup = 'ASSETTYPE' and genoid = '" & fixgroup.SelectedValue & "'"
        fixdepmonth.Text = GetStrData(sSql)
        If fixdepmonth.Text = "-1" Then
            fixLastAsset.Visible = False
            Label42.Visible = False
            Label16.Visible = False
        Else
            fixLastAsset.Visible = True
            Label42.Visible = True
            Label16.Visible = True
        End If
        GVFixedAssetdtl.Visible = False

        If ToDecimal(fixdepmonth.Text) = "-1" Or ToDecimal(fixdepmonth.Text) = "0" Then
            fixdepval.Text = "0.00"
        Else
            fixdepval.Text = ToMaskEdit((ToDecimal(fixPresentValue.Text)) / Val(ToDecimal(fixdepmonth.Text)), 2)
            AccumVal.Text = ToMaskEdit((ToDecimal(fixPresentValue.Text)) / Val(ToDecimal(fixdepmonth.Text)), 2)
        End If
    End Sub

    Protected Sub fixdepmonth_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles fixdepmonth.TextChanged
        ReAmountDep()
    End Sub

    Protected Sub fixPresentValue_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles fixPresentValue.TextChanged
        fixPresentValue.Text = ToMaskEdit(ToDecimal(fixPresentValue.Text), 2)
        ReAmountDep()
    End Sub

    Protected Sub fixLastAsset_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles fixLastAsset.TextChanged
        fixLastAsset.Text = ToMaskEdit(ToDecimal(fixLastAsset.Text), 2)
        If ToDecimal(fixLastAsset.Text) > ToDecimal(fixPresentValue.Text) Then
            showMessage("Asset terakhir tidak dapat lebih dari asset saat ini ! ", 2)
            Exit Sub
        End If
        Session("tbldtl") = Nothing
        GVFixedAssetdtl.DataSource = Nothing
        GVFixedAssetdtl.DataBind()
        ReAmountDep()
    End Sub

    Protected Sub fixfirstvalue_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles fixfirstvalue.TextChanged
        fixfirstvalue.Text = ToMaskEdit(ToDecimal(fixfirstvalue.Text), 2)
        fixPresentValue.Text = ToMaskEdit(ToDecimal(fixfirstvalue.Text), 2)
        ReAmountDep()
    End Sub

    Protected Sub accumDV_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        accumDV.Text = ToMaskEdit(ToDecimal(accumDV.Text), 2)
        ReAmountDep()
    End Sub

    Protected Sub DDLoutlet_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GenerateMstOid()
        GenerateDtlOid()
    End Sub

    Protected Sub lbkHeader_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub lbkPostMoreInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView2.ActiveViewIndex = 1
    End Sub

    Protected Sub lbkPostInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView2.ActiveViewIndex = 0
    End Sub

    Protected Sub lbkDetil_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbkDetil.Click
        MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("tbldata") = Nothing
        GVFixedAsset.PageIndex = 0
        Session("SearchFixAsset") = sql_temp
        lblViewInfo.Visible = False
        Dim sFilter As String = ""
        If FilterDDL.SelectedValue <> "ALL" Then
            sFilter &= " AND " & FilterDDL.SelectedValue & " = '" & Tchar(txtFilter.Text) & "'"
        Else
            sFilter &= ""
        End If
        binddata(sFilter)
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        txtFilter.Text = ""
        FilterDDL.SelectedValue = "ALL"
        cbPeriode.Checked = False
        cbDesc.Checked = False
        cbBlmPosting.Checked = False
        txtPeriode1.Text = Format(GetServerTime.AddDays(-2), "MM/dd/yyyy")
        txtPeriode2.Text = Format(GetServerTime, "MM/dd/yyyy")
        lblViewInfo.Visible = False
        binddata("")
    End Sub

    Protected Sub btnGenerate_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        Dim dtlTable As DataTable = SetTableDetail()
        Session("tbldtl") = dtlTable

        ' Validation
        sSql = "SELECT genother4 from QL_mstgen WHERE gengroup = 'ASSETTYPE' and genoid = '" & fixgroup.SelectedValue & "'"
        If CDbl(fixdepmonth.Text) = -1 Or GetStrData(sSql) = "NO" Then
            showMessage("Type Asset dan Depreciation tidak di perlukan untuk asset ini !!", 2)
            Exit Sub
        End If
        Dim sMsg As String = ""
        If Not IsValidDate(fixDate.Text, "MM/dd/yyyy", "") Then
            showMessage("- Format tanggal aset salah !!<BR>", 2)
            Exit Sub
        End If
        If CDate(fixDate.Text) <= CDate(CutofDate.Text) Then
            showMessage("- Tanggal fix asset purchase Tidak boleh Kurang dari CutoffDate (" & CutofDate.Text & ") !!<BR>", 2)
            Exit Sub
        End If
        If ToDecimal(fixPresentValue.Text) <= 0 Then
            sMsg &= "- Nilai buku harus lebih besar dari 0!!<BR>"
        End If
        If ToDecimal(fixPresentValue.Text) > ToDecimal(fixfirstvalue.Text) Then
            sMsg &= "- Nilai buku tidak boleh lebih besar Harga Perolehan!!<BR>"
        End If
        If fixdepmonth.Text = "" Then
            sMsg &= "- Depreciation harus lebih besar dari 0 !!<BR>"
        ElseIf CDbl(fixdepmonth.Text) <> -1 And CDbl(fixdepmonth.Text) <= 0 Then
            sMsg &= "- Depreciation harus lebih besar dari 0 !!<BR>"
        End If
        If ToDecimal(fixLastAsset.Text) >= ToDecimal(fixdepval.Text) Then
            sMsg &= "- Value terakhir tidak boleh >= Depreciation Value !!<BR>"
        End If
        If sMsg <> "" Then
            showMessage(sMsg, 2)
            Exit Sub
        End If
        ' Generate
        Dim FAdtlID As String = "1"
        'Generate FA detail ID
        Dim objTableFAdtl As DataTable
        Dim objRowDAdt() As DataRow
        objTableFAdtl = Session("tbldtl")
        objRowDAdt = objTableFAdtl.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        FAdtlID = objRowDAdt.Length + 1
        Dim objTable As DataTable
        Dim objRow As DataRow
        objTable = Session("tbldtl")

        Dim iYear As Integer = CDate(fixDate.Text).Year
        Dim iMonth As Integer = CDate(fixDate.Text).Month
        'If CDate(fixDate.Text).Day <= 15 Then
        'iMonth = CDate(toDate(fixDate.Text)).Month
        'Else
        '    iMonth = CDate(fixDate.Text).Month + 1
        'End If
        Dim sMonth As String = ""
        Dim iValue As Decimal = ToDecimal(fixPresentValue.Text)
        Dim Accum As Decimal = ToDecimal(AccumVal.Text)
        Dim Sisa As Decimal = ToDecimal(fixdepval.Text)
        Dim timeServer = GetServerTime()

        For C1 As Int16 = 0 To CInt(fixdepmonth.Text) - 1
            If CStr(iMonth).Length = 1 Then
                sMonth = "0" & CStr(iMonth)
            Else
                sMonth = CStr(iMonth)
            End If

            iValue = iValue - Math.Round(ToDecimal(Sisa), 2, MidpointRounding.ToEven)
            If Math.Round(ToDecimal(iValue), 2, MidpointRounding.ToEven) < 1 Then
                Accum += Math.Round(ToDecimal(iValue), 2, MidpointRounding.ToEven)
                iValue = ToDecimal(fixLastAsset.Text)
                Sisa = Math.Round(ToDecimal(Sisa), 2, MidpointRounding.ToEven) - Math.Round(ToDecimal(fixLastAsset.Text), 2, MidpointRounding.ToEven)
            End If
            If C1 = 0 Then
                Accum += ToDecimal(accumDV.Text)
            End If

            If Session("tbldtl") IsNot Nothing Then
                objRow = objTable.NewRow()
                objRow("cmpcode") = DDLoutlet.SelectedValue
                objRow("fixdtloid") = FAdtlID + C1
                objRow("fixoid") = 1
                objRow("fixdtlseq") = 1
                objRow("fixperiod") = CStr(iYear) & "" & sMonth
                objRow("fixperiodvalue") = iValue
                objRow("fixperioddepvalue") = Sisa
                objRow("fixperioddepaccum") = Accum
                objRow("depcostacctgoid") = DDLaccum.SelectedValue
                objRow("accdepacctgoid") = DDLadExpense.SelectedValue
                objRow("fixnote") = ""
                objRow("fixflag") = "In Process"
                objRow("fixpostdate") = timeServer
                objRow("updtime") = timeServer
                objRow("upduser") = Session("UserID")
                objTable.Rows.Add(objRow)
            End If

            Accum += ToDecimal(Sisa)
            iMonth += 1
            If iMonth = 13 Then
                iMonth = 1
                iYear += 1
            End If
        Next

        Session("tbldtl") = objTable
        GVFixedAssetdtl.DataSource = Session("tbldtl")
        GVFixedAssetdtl.DataBind()
        GVFixedAssetdtl.Visible = True

        'btnSave.Visible = True
        'MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim C As Integer = GVFixedAssetdtl.Rows.Count
        Dim sMsg As String = ""
        If C = 0 And fixdepmonth.Text > 0 Then
            showMessage("Silahkan Click Auto Generate, Untuk Mengisi Data Detail !!!", 2)
            Exit Sub
        End If
        ' Validasi
        If Not IsValidDate(fixDate.Text, "MM/dd/yyyy", "") Then
            showMessage("- Format tanggal aset salah !!<BR>", 2)
            Exit Sub
        End If
        If CDate(fixDate.Text) <= CDate(CutofDate.Text) Then
            showMessage("Tanggal fix asset Tidak boleh <= Tanggal CutofDate (" & CutofDate.Text & ") !!", 2)
            Exit Sub
        End If
        If FAPMst.Text.Trim = "" Then
            sMsg &= "- Silahkan isi Akun FA Purchase !!<BR>"
        End If
    
        If FixCode.Text.Trim = "" Then
            sMsg &= "- Silahkan isi Kode aset !!<BR>"
        End If
        If fixdesc.Text.Trim = "" Then
            sMsg &= "- Silahkan isi deskripsi aset !!<BR>"
        End If
        If FAnote.Text.Trim = "" Then
            sMsg &= "- Silahkan isi Aset Note !!<BR>"
        End If
        If fixfirstvalue.Text = "" Or ToDecimal(fixfirstvalue.Text) <= 0 Then
            sMsg &= "- Nilai awal harus lebih besar dari 0!!<BR>"
        End If
        If ToDecimal(fixPresentValue.Text) > ToDecimal(fixfirstvalue.Text) Then
            sMsg &= "- Nilai buku tidak boleh lebih besar Harga Perolehan!!<BR>"
        End If
        If ToDecimal(fixPresentValue.Text) <= 0 Then
            sMsg &= "- Nilai buku harus lebih besar dari 0!!<BR>" : End If
        If fixdepmonth.Text = "" Or fixdepmonth.Text = 0 Then
            sMsg &= "- Depreciation harus lebih besar dari 0 bulan!!<BR>"
        End If
        If FAPmstoid.Text = 0 Then
            FAPDtl.Text = 0
            sSql = "SELECT COUNT(-1) FROM ql_trnfixmst WHERE cmpcode='" & Session("CompnyCode") & "' AND fixcode='" & Tchar(FixCode.Text) & "' AND fixoid > 0"
            If Session("oid") <> Nothing Or Session("oid") <> "" Then
                sSql &= " AND fixoid<>" & Session("oid")
            End If
            Dim hitungCode As Integer = Integer.Parse(GetStrData(sSql))
            If hitungCode > 0 Then
                sMsg &= "- Kode ini sudah digunakan oleh fixed aset yang lain!!<BR>"
            End If
        End If
        If sMsg <> "" Then
            showMessage(sMsg, 2)
            lblPOST.Text = "In Process"
            Exit Sub
        End If

        ' Generate oid
        Dim MstOid As String = GenerateID("QL_trnfixmst", Session("CompnyCode"))
        Dim DtlOid As String = GenerateID("QL_trnfixdtl", Session("CompnyCode"))
        sSql = "SELECT COUNT(LEFT(trnbelifano,4)) from QL_trnbelimst_fa WHERE trnbelifano = '" & FAPMst.Text & "'"
        If GetStrData(sSql) > 0 Then
            Session("st") = "QL_trnbelimst_fa"
        Else
            sSql = "SELECT COUNT(LEFT(trnbelino,6)) from QL_trnbelimst WHERE trnbelino = '" & FAPMst.Text & "' and cmpcode='" & Session("Compnycode") & "'"
            If GetStrData(sSql) > 0 Then
                Session("st") = "QL_trnbelimst"
            Else
                Session("st") = ""
            End If
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If objConn.State = ConnectionState.Closed Then
            objConn.Open()
        End If
        objTrans = objConn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "INSERT into QL_trnfixmst (cmpcode, fixoid, fixcode, fixdesc, fixgroup, fixdate, fixfirstvalue, fixdepmonth, fixdepval, fixlocation, fixpresentvalue, fixdepakum, fixperson, fixother, fixlastasset, acctgoid, payacctgoid, accumdepacctgoid, accumdepexpacctgoid, refname, fapurchasemstoid, fapurchasedtloid, fixflag,curroid,rateoid,rate2oid,createuser, createtime, upduser, updtime ) VALUES ('" & Session("CompnyCode") & "', " & MstOid & ", '" & Tchar(FixCode.Text) & "', '" & Tchar(fixdesc.Text) & "', '" & fixgroup.SelectedValue & "','" & CDate(fixDate.Text) & "'," & ToDecimal(fixfirstvalue.Text) & "," & ToDecimal(fixdepmonth.Text) & "," & ToDecimal(fixdepval.Text) & ",'" & Tchar(fixlocation.Text) & "'," & ToDecimal(fixPresentValue.Text) & "," & ToDecimal(accumDV.Text) & ",'" & Tchar(fixperson.Text) & "','" & Tchar(FAnote.Text) & "'," & ToDecimal(fixLastAsset.Text) & "," & DDLassets.SelectedValue & ",0," & DDLaccum.SelectedValue & "," & DDLadExpense.SelectedValue & ", '" & Session("st") & "', " & FAPmstoid.Text & ", " & FAPDtl.Text & ", '" & lblPOST.Text & "','" & CurrDDL.SelectedValue & "','" & RateOid.Text & "','" & Rate2Oid.Text & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                'Update mst lastoid
                sSql = "UPDATE QL_mstoid SET lastoid=" & MstOid & " WHERE tablename='QL_trnfixmst' AND cmpcode='" & Session("CompnyCode") & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

            Else
                ' Update
                sSql = "UPDATE QL_trnfixmst SET fixcode='" & Tchar(FixCode.Text) & "', fixdesc='" & Tchar(fixdesc.Text.Trim) & "', fixfirstvalue=" & ToDecimal(fixfirstvalue.Text) & ", fixdepmonth=" & ToDecimal(fixdepmonth.Text) & ", fixdepval=" & ToDecimal(fixdepval.Text) & ", fixpresentvalue=" & ToDecimal(fixPresentValue.Text) & ", fixdepakum=" & ToDecimal(accumDV.Text) & ", fixlastasset=" & ToDecimal(fixLastAsset.Text) & ", fixother='" & Tchar(FAnote.Text) & "', acctgoid=" & DDLassets.SelectedValue & ", fixdate='" & CDate(fixDate.Text) & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, fixgroup='" & fixgroup.SelectedValue & "', fixflag='" & lblPOST.Text & "',curroid = '" & CurrDDL.SelectedValue & "', rateoid = '" & RateOid.Text & "', rate2oid = '" & Rate2Oid.Text & "', accumdepacctgoid=" & DDLaccum.SelectedValue & ", accumdepexpacctgoid=" & DDLadExpense.SelectedValue & " WHERE fixoid = " & fixmstoid.Text & " AND cmpcode='" & Session("CompnyCode") & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "Delete from QL_trnfixdtl WHERE cmpcode='" & Session("CompnyCode") & "' AND fixoid='" & fixmstoid.Text & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If

            If Not Session("tbldtl") Is Nothing Then
                Dim i As Integer
                Dim objTable As DataTable
                Dim objRow() As DataRow
                ' Insert
                objTable = Session("tbldtl")
                objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

                For i = 0 To objRow.Length - 1
                    ' Insert new dtl
                    sSql = "INSERT into QL_trnfixdtl (cmpcode, fixdtloid, fixoid, fixdtlseq, fixperiod, fixperiodvalue, fixperioddepvalue, fixperioddepaccum, depcostacctgoid, accdepacctgoid, fixnote, fixpostdate, fixflag, crtuser, crttime, upduser, updtime) VALUES ('" & Session("CompnyCode") & "'," & DtlOid & "," & fixmstoid.Text & "," & (i + 1) & ",'" & objRow(i)("fixperiod").ToString.Trim & "'," & objRow(i)("fixperiodvalue").ToString.Trim & "," & objRow(i)("fixperioddepvalue").ToString.Trim & ", " & objRow(i)("fixperioddepaccum").ToString.Trim & "," & objRow(i)("depcostacctgoid").ToString.Trim & "," & objRow(i)("accdepacctgoid").ToString.Trim & ",'',CURRENT_TIMESTAMP,'" & objRow(i)("fixflag").ToString.Trim & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    DtlOid += 1
                Next
                ' Update dtl lastoid
                sSql = "UPDATE QL_mstoid SET lastoid=" & (DtlOid + objRow.Length - 1) & " WHERE cmpcode='" & Session("CompnyCode") & "' AND tablename='QL_trnfixdtl'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                'Update Usage
                If lblCekPage.Text = "NEW" Then
                    If Session("st") = "QL_trnbelimst_fa" Then
                        sSql = "SELECT isnull(trnbelifadtlusage,0) from QL_trnbelidtl_fa WHERE trnbelifadtloid = " & FAPDtl.Text & " AND cmpcode='" & Session("CompnyCode") & "'"
                        Dim updUsg As Integer = GetStrData(sSql)
                        updUsg += 1
                        sSql = "UPDATE QL_trnbelidtl_fa SET trnbelifadtlusage = " & updUsg & " WHERE trnbelifadtloid = " & FAPDtl.Text & " AND cmpcode='" & Session("CompnyCode") & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()

                    ElseIf Session("st") = "QL_trnbelimst" Then
                        sSql = "SELECT trnbelidtlqtyusage from QL_trnbelidtl WHERE trnbelidtloid = " & FAPDtl.Text & " AND cmpcode='" & DDLoutlet.SelectedValue & "'"
                        Dim updUsg As Integer = GetStrData(sSql)
                        updUsg += 1
                        sSql = "UPDATE QL_trnbelidtl SET trnbelidtlqtyusage = " & updUsg & " WHERE trnbelidtloid = " & FAPDtl.Text & " AND cmpcode='" & Session("CompnyCode") & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                End If
            End If

            objTrans.Commit()
            objConn.Close()
            Session("tbldtl") = Nothing
        Catch ex As Exception
            objTrans.Rollback()
            objConn.Close()
            lblPOST.Text = "In Process"
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try

        If lblPOST.Text = "Post" Then
            Session("SavedInfo") &= "Data telah diposting !!!<BR>Fix Asset Code = " & FixCode.Text
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            showMessage(Session("SavedInfo"), 3)
        Else
            Response.Redirect("~\Accounting\trnFixAsset.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnDelete_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim strSQL As String
        Dim objTrans As SqlClient.SqlTransaction
        If objConn.State = ConnectionState.Closed Then
            objConn.Open()
        End If
        objTrans = objConn.BeginTransaction()
        xCmd.Connection = objConn
        xCmd.Transaction = objTrans
        Try
            strSQL = "UPDATE QL_trnbelidtl_fa SET trnbelifadtlusage = 0 WHERE trnbelifadtloid = " & FAPDtl.Text & " AND cmpcode='" & Session("CompnyCode") & "'"
            xCmd.CommandText = strSQL
            xCmd.ExecuteNonQuery()
            'delete dtl
            strSQL = "Delete from QL_trnfixdtl WHERE fixoid=" & fixmstoid.Text & " AND cmpcode='" & Session("Compnycode") & "'"
            xCmd.CommandText = strSQL
            xCmd.ExecuteNonQuery()
            ' delete mst
            strSQL = "Delete from QL_trnfixmst WHERE fixoid=" & fixmstoid.Text & " AND cmpcode='" & Session("Compnycode") & "'"
            xCmd.CommandText = strSQL
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            Session("tbldtl") = Nothing
            objConn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            objConn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        If lblPOST.Text <> "" Then
            Session("SavedInfo") &= "Data telah dihapus !!!<BR>Fix Asset Code = " & FixCode.Text
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            showMessage(Session("SavedInfo"), 3)
        Else
            Response.Redirect("~\Accounting\trnFixAsset.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnCancel_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        btnSave.Enabled = True
        Session("oid") = Nothing
        Session("tbldtl") = Nothing
        Response.Redirect("trnFixAsset.aspx?awal=true")
    End Sub

    Protected Sub btnCancelDtl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("oid") = Nothing
        Session("tbldtl") = Nothing
        Response.Redirect("trnFixAsset.aspx?awal=true")
    End Sub

    Protected Sub btnPosting_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblPOST.Text = "Post"
        btnSave_Click(sender, e)
    End Sub

    Protected Sub btnViewDtl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'fixDate.Text = "25/" & ddlMonth.SelectedValue & "/" & ddlYear.SelectedValue & ""
        'Dim mySqlConn As New SqlConnection(ConnStr)
        sSql = "SELECT d.cmpcode,d.fixdtloid,d.fixperiod,m.fixdesc,d.fixperioddepvalue,d.fixoid,d.fixpostdate,d.fixperiodvalue,m.fixcode,m.fixflag,d.fixflag FROM QL_trnfixdtl d INNER JOIN QL_trnfixmst m ON m.cmpcode=d.cmpcode AND m.fixoid=d.fixoid AND m.fixflag='Post'INNER JOIN QL_mstacctg a ON a.acctgoid=m.acctgoid WHERE d.fixperiod='" & ddlYear.SelectedValue & "" & ddlMonth.SelectedValue & "' AND d.fixflag <> 'Post' AND d.cmpcode='" & Session("CompnyCode") & "' order by d.cmpcode ASC"
        Dim objTable As DataTable = ckon.ambiltabel(sSql, "QL_trnfixmst")
        Session("TblFixPeriodPosting") = objTable
        GVDtlMonth.DataSource = Session("TblFixPeriodPosting")
        GVDtlMonth.DataBind()
    End Sub

    Protected Sub btnPostingDtl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not Session("TblFixPeriodPosting") Is Nothing Then
            Dim objTable As DataTable = Session("TblFixPeriodPosting")
            Dim C As Integer = objTable.Rows.Count
            If C = 0 Then
                showMessage("Data Kosong, Find Data Lagi !!!", 2)
                Exit Sub
            End If
            Dim pilihPeriod As String = ddlYear.SelectedValue & "" & ddlMonth.SelectedValue
            Dim periodToday As String = Format(GetServerTime, "yyyyMM")
            If pilihPeriod > periodToday Then
                showMessage("Periode Tidak Boleh > " & periodToday & " !!!", 2)
                Exit Sub
            End If
            Dim iGlDtlOid As Integer = GenerateID("QL_trngldtl", Session("CompnyCode"))
            Dim iGlMstOid As Integer = GenerateID("QL_trnglmst", Session("CompnyCode"))

            Dim objTrans As SqlClient.SqlTransaction
            objConn.Open()
            objTrans = objConn.BeginTransaction()
            xCmd.Connection = objConn
            xCmd.Transaction = objTrans

            Try
                '//////POSTING TO GL FROM FIXED ASSET
                For C1 As Int16 = 0 To objTable.Rows.Count - 1
                    Dim cmpCode As String = objTable.Rows(C1).Item("cmpcode").ToString
                    'If C1 > 0 Then
                    '    Session("cek") = GVDtlMonth.DataKeys(C1 - 1).Item("cmpcode").ToString
                    'End If
                    'If cmpCode = Session("cek") Then
                    '    Session("DtlGLOid") += 1
                    '    Session("MstGLOid") += 1
                    'Else

                    'End If
                    Dim depcostacctgoid As Integer = 0
                    Dim accdepacctgoid As Integer = 0
                    sSql = "UPDATE QL_trnfixdtl SET fixflag='Post' WHERE fixdtloid=" & objTable.Rows(C1).Item("fixdtloid").ToString & " AND cmpcode='" & Session("Compnycode") & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    sSql = "SELECT depcostacctgoid from QL_trnfixdtl WHERE fixdtloid=" & objTable.Rows(C1).Item("fixdtloid").ToString & " AND cmpcode='" & Session("Compnycode") & "'"
                    xCmd.CommandText = sSql
                    If Not IsDBNull(xCmd.ExecuteScalar) Then
                        depcostacctgoid = xCmd.ExecuteScalar
                    End If

                    sSql = "SELECT accdepacctgoid from QL_trnfixdtl WHERE fixdtloid=" & objTable.Rows(C1).Item("fixdtloid").ToString & " AND cmpcode='" & Session("Compnycode") & "'"
                    xCmd.CommandText = sSql
                    If Not IsDBNull(xCmd.ExecuteScalar) Then
                        accdepacctgoid = xCmd.ExecuteScalar
                    End If
                    Dim dPostingDate As Date = New Date(ddlYear.SelectedValue, ddlMonth.SelectedValue, Date.DaysInMonth(ddlYear.SelectedValue, ddlMonth.SelectedValue))
                    'Dim lll As Date = CDate(fixDate.Text)
                    'Dim iYear As Integer = CDate(fixDate.Text).Year
                    'Dim iMonth As Integer = CDate(fixDate.Text).Month
                    'Dim iDay As Integer = Date.DaysInMonth(iYear, iMonth)
                    'Dim tglFA As Date = CDate(iMonth & "/" & iDay & "/" & iYear)

                    '//////INSERT INTO TRN GL MST
                    Dim iSeq As Integer = 1
                    sSql = "INSERT INTO QL_trnglmst (cmpcode,glmstoid,gldate,periodacctg,glnote,glflag,postdate,createuser,createtime,upduser,updtime,type,glother1,glother2,glother3,rateoid,rate2oid,glrateidr,glrate2idr,glrateusd,glrate2usd) " & _
                   " VALUES " & _
                   "( '" & Session("Compnycode") & "'," & iGlMstOid & ",'" & dPostingDate & "','" & objTable.Rows(C1).Item("fixperiod").ToString & "','FIXED ASSET-MONTHLY POSTING-" & Tchar(objTable.Rows(C1).Item("fixcode").ToString) & "','Post',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'ASSET','NULL','NULL','NULL','" & RateOid.Text & "','" & Rate2Oid.Text & "','" & ToDecimal(RateToIDR.Text) & "','" & ToDecimal(Rate2ToIDR.Text) & "','" & ToDecimal(Rate2ToUsd.Text) & "','" & ToDecimal(Rate2ToUsd.Text) & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    'insert data detail
                    'VAR BEBAN AKUM PENYUSUTAN
                    sSql = "INSERT INTO QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,noref,glnote,glflag,upduser,updtime,glamtidr,glamtusd,glother1) " & _
                    " VALUES " & _
                    "( '" & Session("Compnycode") & "'," & iGlDtlOid & ", " & iSeq & "," & iGlMstOid & "," & depcostacctgoid & ",'D','" & ToDecimal(objTable.Rows(C1)("fixperioddepvalue")) & "','" & Tchar(objTable.Rows(C1)("fixcode").ToString) & "','FIXED ASSET-MONTHLY POSTING-" & Tchar(objTable.Rows(C1).Item("fixcode").ToString) & "','Post','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & ToDecimal(objTable.Rows(C1)("fixperioddepvalue")) * ToDecimal(Rate2ToIDR.Text) & "','" & ToDecimal(objTable.Rows(C1)("fixperioddepvalue")) * ToDecimal(Rate2ToUsd.Text) & "','QL_trnfixdtl " & objTable.Rows(C1).Item("fixdtloid").ToString & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1 : iSeq += 1
                    'VAR PENYUSUTAN
                    sSql = "INSERT INTO QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,noref,glnote,glflag,upduser,updtime,glamtidr,glamtusd,glother1) " & _
                    " VALUES " & _
                    "( '" & Session("Compnycode") & "'," & iGlDtlOid & ", " & iSeq & "," & iGlMstOid & "," & accdepacctgoid & ",'C','" & ToDecimal(objTable.Rows(C1)("fixperioddepvalue")) & "','" & Tchar(objTable.Rows(C1).Item("fixcode").ToString) & "','FIXED ASSET-MONTHLY POSTING-" & Tchar(objTable.Rows(C1).Item("fixcode").ToString) & "','Post','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & ToDecimal(objTable.Rows(C1)("fixperioddepvalue")) * ToDecimal(Rate2ToIDR.Text) & "','" & ToDecimal(objTable.Rows(C1)("fixperioddepvalue")) * ToDecimal(Rate2ToUsd.Text) & "','QL_trnfixdtl " & objTable.Rows(C1).Item("fixdtloid").ToString & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1 : iGlMstOid += 1

                    'Update fixmst book value
                    sSql = "UPDATE QL_trnfixmst SET fixpresentvalue=" & ToDecimal(objTable.Rows(C1).Item("fixperiodvalue").ToString) & "  WHERE cmpcode='" & Session("Compnycode") & "' AND fixoid=" & objTable.Rows(C1).Item("fixoid").ToString
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Next

                sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid & " WHERE cmpcode='" & Session("Compnycode") & "' AND tablename='QL_trnglmst'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid - 1 & " WHERE cmpcode='" & Session("Compnycode") & "' AND tablename='QL_trngldtl'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                objTrans.Commit()
                objConn.Close()
                btnViewDtl_Click(sender, e)
            Catch ex As Exception
                objTrans.Rollback()
                objConn.Close()
                showMessage(ex.Message, 1)
                Exit Sub
            End Try
            Session("vJurnalMst") = Nothing
            Session("vJurnalDtl") = Nothing
            If lblPOST.Text <> "" Then
                Session("SavedInfo") &= "Semua Data telah diposting !!!"
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Accounting\trnFixAsset.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub BtnCari1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'FilterDDLListSupp.SelectedIndex = -1
        FilterFAP.Text = ""
        GvFAPmst.SelectedIndex = -1
        BindDataFAP()
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
    End Sub

    Protected Sub btnHapus1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FAPMst.Text = ""
        FAPmstoid.Text = 0
        fixgroup.SelectedValue = Nothing
        fixgroup.CssClass = "inpText"
        fixgroup.Enabled = True
        fixdepmonth.Text = "-1"
        'GvFAPmst.Visible = False
        btnHapus2_Click(sender, e)
    End Sub

    Protected Sub btnCari2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If FAPMst.Text.Trim = "" Then
            showMessage("Pilih FA Purchase dulu", 2)
            Exit Sub
        End If
        FilterTextListReg.Text = ""
        GvFAPdtl.SelectedIndex = -1
        BindDataFAD()
        cProc.SetModalPopUpExtender(btnHideListReg, pnlListReg, mpeListReg, True)
    End Sub

    Protected Sub btnHapus2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FAPDtl.Text = 0
        FixCode.Text = ""
        fixdesc.Text = ""
        fixfirstvalue.Text = "0.00"
        fixPresentValue.Text = "0.00"
        fixdepval.Text = "0.00"
        GvFAPdtl.Visible = False
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Accounting\trnFixAsset.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub CurrDDL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sErr As String = ""
        If fixDate.Text <> "" Then
            If IsValidDate(fixDate.Text, "MM/dd/yyyy", sErr) Then
                If CurrDDL.SelectedValue <> "" Then
                    cRate.SetRateValue(CInt(CurrDDL.SelectedValue), fixDate.Text)
                    If cRate.GetRateDailyLastError <> "" Then
                        showMessage(cRate.GetRateDailyLastError, 2)
                        RateOid.Text = ""
                        RateToIDR.Text = ""
                        RateToUSD.Text = ""
                        Exit Sub
                    End If
                    If cRate.GetRateMonthlyLastError <> "" Then
                        showMessage(cRate.GetRateMonthlyLastError, 2)
                        Rate2Oid.Text = ""
                        Rate2ToIDR.Text = ""
                        Rate2ToUsd.Text = ""
                        Exit Sub
                    End If
                    RateOid.Text = cRate.GetRateDailyOid
                    RateToIDR.Text = ToMaskEdit(cRate.GetRateDailyIDRValue, GetRoundValue(cRate.GetRateDailyIDRValue.ToString))
                    RateToUSD.Text = ToMaskEdit(cRate.GetRateDailyUSDValue, GetRoundValue(cRate.GetRateDailyUSDValue.ToString))
                    Rate2Oid.Text = cRate.GetRateMonthlyOid
                    Rate2ToIDR.Text = ToMaskEdit(cRate.GetRateMonthlyIDRValue, GetRoundValue(cRate.GetRateMonthlyIDRValue.ToString))
                    Rate2ToUsd.Text = ToMaskEdit(cRate.GetRateMonthlyUSDValue, GetRoundValue(cRate.GetRateMonthlyUSDValue.ToString))
                End If
            End If
        End If
    End Sub

    Protected Sub btnFindListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSupp.Click
        BindDataFAP()
        mpeListSupp.Show()
    End Sub

    Protected Sub btnAllListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListSupp.Click
        'FilterDDLListSupp.SelectedIndex = -1
        FilterFAP.Text = ""
        GvFAPmst.SelectedIndex = -1
        BindDataFAP()
        mpeListSupp.Show()

    End Sub

    Protected Sub lkbCloseListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub GvFAPdtl_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GvFAPdtl.PageIndexChanging
        GvFAPdtl.PageIndex = e.NewPageIndex
        BindDataFAD()
        mpeListReg.Show()
    End Sub

    Protected Sub btnFindListReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListReg.Click
        BindDataFAD()
        mpeListReg.Show()
    End Sub

    Protected Sub btnAllListReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListReg.Click
        FilterTextListReg.Text = ""
        GvFAPdtl.SelectedIndex = -1
        BindDataFAD()
        mpeListReg.Show()
    End Sub

    Protected Sub lkbCloseListReg_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListReg, pnlListReg, mpeListReg, False)
    End Sub
#End Region
End Class
