'Prgmr:4n7JuK ; Last Update On 13.12.10
' CREDIT NOTE : -A/R : Max Amount = A/R Balance
'               +A/P : No Max Amount
Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports ClassProcedure

Partial Class Accounting_trnCreditNote
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cProc As New ClassProcedure
    Dim cKon As New Koneksi
    Dim cRate As New ClassRate()
#End Region

#Region "Functions"
    Private Function IsValidPeriod(ByVal sDateAwal As String, ByVal sDateAkhir As String) As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(sDateAwal, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2, "") : Return False
        End If
        If Not IsValidDate(sDateAkhir, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2, "") : Return False
        End If
        If CDate(sDateAwal) > CDate(sDateAkhir) Then
            showMessage("Period 2 must be more than Period 1 !", 2, "") : Return False
        End If
        Return True
    End Function

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
        Return ""
    End Function
#End Region

#Region "Procedures"
    Sub GenerateCNNo()
        Dim sNo As String = "CN-" & Format(CDate(cndate.Text), "yyMM") & "-"
        sSql = "SELECT isnull(max(abs(replace(cnno,'" & sNo & "',''))),0)+1  FROM ql_trncreditnote WHERE cmpcode='" & bus_unit.SelectedValue & "' AND cnno LIKE '" & sNo & "%'"
        cnno.Text = GenNumberString(sNo, "", GetStrData(sSql), 4)
    End Sub

    Private Sub CheckCNStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM ql_trncreditnote WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND cnstatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Accounting\trnCreditNote.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "' "
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lkbCNInProcess.Visible = True
            lkbCNInProcess.Text = "You have " & GetStrData(sSql) & " In Process Credit Note data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Public Sub BindData(ByVal sSqlPlus As String)
        sSql = "SELECT * FROM ( "
        sSql &= "SELECT cn.cmpcode,dv.divname,cn.cnoid,cn.cnno,cn.cndate,'A/P' AS tipe,s.suppname AS suppcustname,bm.trnbelino AS aparno,cn.cnamt,cn.cnstatus,cn.createuser,cn.reftype,cn.updtime FROM QL_trncreditnote cn INNER JOIN QL_mstdivision dv ON dv.divcode=cn.cmpcode INNER JOIN QL_trnbelimst bm ON bm.cmpcode=cn.cmpcode AND bm.trnbelimstoid=cn.refoid INNER JOIN QL_mstsupp s ON s.suppoid=cn.suppcustoid WHERE cn.reftype='Ql_trnbelimst' UNION ALL SELECT cn.cmpcode,dv.divname,cn.cnoid,cn.cnno,cn.cndate,'A/R' AS tipe,c.custname AS suppcustname,'' AS aparno,cn.cnamt,cn.cnstatus,cn.createuser,cn.reftype,cn.updtime FROM QL_trncreditnote cn INNER JOIN QL_mstdivision dv ON dv.divcode=cn.cmpcode INNER JOIN QL_mstcust c ON c.custoid=cn.suppcustoid WHERE cn.reftype='Ql_trnjualmst' UNION ALL SELECT cn.cmpcode,dv.divname,cn.cnoid,cn.cnno,cn.cndate,'A/P' AS tipe,s.suppname AS suppcustname,bm.approductno AS aparno,cn.cnamt,cn.cnstatus,cn.createuser,cn.reftype,cn.updtime FROM QL_trncreditnote cn INNER JOIN QL_mstdivision dv ON dv.divcode=cn.cmpcode INNER JOIN QL_trnapproductmst bm ON bm.cmpcode=cn.cmpcode AND bm.approductmstoid=cn.refoid INNER JOIN QL_mstsupp s ON s.suppoid=cn.suppcustoid WHERE cn.reftype='QL_trnapproductmst' UNION ALL SELECT cn.cmpcode,dv.divname,cn.cnoid,cn.cnno,cn.cndate,'A/R' AS tipe,s.custname AS suppcustname,bm.transitemno AS aparno,cn.cnamt,cn.cnstatus,cn.createuser,cn.reftype,cn.updtime FROM QL_trncreditnote cn INNER JOIN QL_mstdivision dv ON dv.divcode=cn.cmpcode INNER JOIN QL_trntransitemmst bm ON bm.cmpcode=cn.cmpcode AND bm.transitemmstoid=cn.refoid INNER JOIN QL_mstcust s ON s.custoid=cn.suppcustoid WHERE cn.reftype='QL_trntransitemmst' "
        sSql &= ") AS a "
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " WHERE a.cmpcode='" & Session("CompnyCode") & "'"
        Else
            sSql &= " WHERE a.cmpcode LIKE '%'"
        End If

        sSql &= sSqlPlus & " ORDER BY CONVERT(DATETIME, a.cndate) DESC, a.cnoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trncreditnote")
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
        lblViewInfo.Visible = False : lkbCNInProcess.Visible = False
    End Sub

    Private Sub BindDataSuppCust()
        If reftype.SelectedValue.ToUpper = "AP" Then
            sSql = "SELECT DISTINCT a.suppoid AS oid,a.suppcode AS code,a.suppname AS name FROM ( "
            sSql &= "SELECT ap.cmpcode,ap.reftype,ap.refoid,ap.suppoid,s.suppcode,s.suppname,ap.acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS apaccount,ap.amttrans apamt, ap.trnapdate,(ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 INNER JOIN QL_trnpayap pay ON pay.cmpcode=ap2.cmpcode AND pay.payapoid=ap2.payrefoid WHERE ap2.payrefoid<>0 And ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid = ap2.refoid AND ISNULL(pay.payapres1,'')<>'Lebih Bayar' AND ap2.trnaptype LIKE 'PAYAP%'),0.0)) AS appaidamt, (ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode And ap.reftype=ap2.reftype And ap.refoid=ap2.refoid AND ap2.trnaptype IN ('DNAP','CNAP', 'RETPI')),0.0)) AS apdncnamt FROM QL_conap ap INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid INNER JOIN QL_mstacctg a ON a.acctgoid=ap.acctgoid WHERE ap.cmpcode='" & bus_unit.SelectedValue & "' AND ISNULL(ap.payrefoid,0)=0 "
            sSql &= ") AS a "
            sSql &= "WHERE a.apamt>a.appaidamt+a.apdncnamt AND (a.suppcode LIKE '%" & Tchar(FilterTextListSupp.Text.Trim) & "%' OR a.suppname LIKE '%" & Tchar(FilterTextListSupp.Text.Trim) & "%') ORDER BY a.suppcode "
        Else
            sSql = "SELECT DISTINCT a.custoid AS oid,a.custcode AS code,a.custname AS name FROM ( "
            sSql &= "SELECT ar.cmpcode,ar.reftype,ar.refoid,ar.custoid,c.custcode,c.custname,ar.acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS araccount,ar.amttrans aramt,ar.trnardate,(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid WHERE ar2.payrefoid<>0 And ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0)) AS arpaidamt, (ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2  WHERE ar2.payrefoid <> 0 And ar.cmpcode = ar2.cmpcode And ar.reftype = ar2.reftype And ar.refoid = ar2.refoid AND ar2.trnartype IN ('DNAR','CNAR', 'SRET')),0.0)) AS ardncnamt FROM QL_conar ar INNER JOIN QL_mstcust c ON ar.custoid=c.custoid INNER JOIN QL_mstacctg a ON a.acctgoid=ar.acctgoid WHERE ar.cmpcode='" & bus_unit.SelectedValue & "' AND ISNULL(ar.payrefoid,0)=0 "
            sSql &= ") AS a "
            sSql &= " WHERE a.aramt>a.arpaidamt+a.ardncnamt AND (a.custcode LIKE '%" & Tchar(FilterTextListSupp.Text.Trim) & "%' OR a.custname LIKE '%" & Tchar(FilterTextListSupp.Text.Trim) & "%') ORDER BY a.custcode "
        End If
        Dim dtSuppCust As DataTable = cKon.ambiltabel(sSql, "suppcust")
        If dtSuppCust.Rows.Count Then
            Session("suppcust") = dtSuppCust
            gvSuppCust.DataSource = dtSuppCust
            gvSuppCust.DataBind()
            gvSuppCust.Visible = True
            cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
        Else
            showMessage("No data Found..!!", 2, "")
        End If
    End Sub

    Public Sub FillTextBox(ByVal iOid As String)
        Try
            sSql = "SELECT * FROM ( "
            sSql &= "SELECT cn.cmpcode,dv.divname,cn.cnoid,cn.cnno,cn.cndate,cn.suppcustoid,s.suppname AS suppcustname,cn.reftype tipe,cn.refoid,Tbl_Trans.transno AS aparno,cn.curroid,cn.rateoid,cn.rate2oid,ap.trnapdate apardate,ap.amttrans aparamt,(ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 INNER JOIN QL_trnpayap pay ON pay.cmpcode=ap2.cmpcode AND pay.payapoid=ap2.payrefoid WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid AND ISNULL(pay.payapres1,'') <> 'Lebih Bayar' AND ap2.trnaptype LIKE 'PAYAP%'),0.0)) AS amtbayar,ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid AND ap2.trnaptype IN ('DNAP', 'RETPI') AND ap2.payrefoid<>0),0.0)+ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 INNER JOIN QL_trncreditnote cn1 ON cn1.cmpcode=ap2.cmpcode AND cn1.cnoid=ap2.payrefoid AND ap2.trnaptype='CNAP' AND ap2.payrefoid<>0 AND ap2.payrefoid<>cn.cnoid WHERE ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid),0.0) AS amtdncn,cn.cnamt,cn.dbacctgoid,cn.cracctgoid,(a.acctgcode + ' - ' + a.acctgdesc) cracctgdesc,cn.cnnote,cn.cnstatus,cn.createuser,cn.createtime,cn.upduser,cn.updtime FROM QL_trncreditnote cn INNER JOIN QL_mstdivision dv ON dv.divcode=cn.cmpcode INNER JOIN (SELECT bm.cmpcode, bm.trnbelimstoid oid, bm.trnbelino transno, 'QL_trnbelimst' AS reftype FROM QL_trnbelimst bm UNION ALL SELECT apm.cmpcode, apm.approductmstoid oid, apm.approductno transno, 'QL_trnapproductmst' AS reftype FROM QL_trnapproductmst apm) AS Tbl_Trans ON Tbl_Trans.cmpcode=cn.cmpcode AND Tbl_Trans.oid=cn.refoid AND Tbl_Trans.reftype=cn.reftype INNER JOIN QL_mstsupp s ON s.suppoid=cn.suppcustoid INNER JOIN QL_mstacctg a ON a.acctgoid=cn.cracctgoid INNER JOIN QL_conap ap ON ap.cmpcode=cn.cmpcode AND ap.reftype=cn.reftype AND ap.refoid=cn.refoid AND ap.payrefoid=0 UNION ALL "
            sSql &= " SELECT cn.cmpcode,dv.divname,cn.cnoid,cn.cnno,cn.cndate,cn.suppcustoid,c.custname AS suppcustname,cn.reftype AS tipe,cn.refoid, (CASE cn.reftype WHEN 'QL_trnjualmst' THEN (SELECT trnjualno FROM QL_trnjualmst jm WHERE jm.trnjualmstoid=cn.refoid) WHEN 'QL_trntransitemmst' THEN (SELECT transitemno FROM QL_trntransitemmst jm WHERE transitemmstoid=cn.refoid) ELSE '' END) AS aparno,cn.curroid,cn.rateoid,cn.rate2oid,ar.trnardate apardate,ar.amttrans aparamt,(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0)) AS amtbayar,ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 WHERE ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ar2.trnartype IN ('DNAR', 'SRET') AND ar2.payrefoid<>0),0.0)+ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trncreditnote cn1 ON cn1.cmpcode=ar2.cmpcode AND cn1.cnoid=ar2.payrefoid AND ar2.trnartype='CNAR' AND ar2.payrefoid<>0 AND ar2.payrefoid<>cn.cnoid WHERE ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid),0.0) AS amtdncn,cn.cnamt,cn.dbacctgoid,cn.cracctgoid,(a.acctgcode + ' - ' + a.acctgdesc) cracctgdesc,cn.cnnote,cn.cnstatus,cn.createuser,cn.createtime,cn.upduser,cn.updtime FROM QL_trncreditnote cn INNER JOIN QL_mstdivision dv ON dv.divcode=cn.cmpcode INNER JOIN QL_mstcust c ON c.custoid=cn.suppcustoid INNER JOIN QL_mstacctg a ON a.acctgoid=cn.cracctgoid INNER JOIN QL_conar ar ON ar.cmpcode=cn.cmpcode AND ar.reftype=cn.reftype AND ar.refoid=cn.refoid AND ar.payrefoid=0 WHERE cn.reftype IN ('QL_trnjualmst', 'QL_trntransitemmst') "
            sSql &= ") AS a WHERE a.cnoid = " & iOid
            Dim dtData As DataTable = cKon.ambiltabel(sSql, "QL_trncreditnote")
            If dtData.Rows.Count < 1 Then
                showMessage("Failed to load transaction data.", 1, "")
            Else
                bus_unit.SelectedValue = dtData.Rows(0)("cmpcode").ToString
                cnoid.Text = dtData.Rows(0)("cnoid").ToString
                cnno.Text = dtData.Rows(0)("cnno").ToString
                cndate.Text = Format(CDate(dtData.Rows(0)("cndate").ToString), "MM/dd/yyyy")
                If dtData.Rows(0)("tipe").ToString = "QL_trnbelimst" Then
                    reftype.SelectedValue = "ap"
                Else
                    reftype.SelectedValue = "ar"
                End If
                reftype_SelectedIndexChanged(Nothing, Nothing)
                supp_cust_oid.Text = dtData.Rows(0)("suppcustoid").ToString
                supp_cust_name.Text = dtData.Rows(0)("suppcustname").ToString
                aparreftype.Text = dtData.Rows(0)("tipe").ToString
                refoid.Text = dtData.Rows(0)("refoid").ToString
                aparno.Text = dtData.Rows(0)("aparno").ToString
                apardate.Text = Format(CDate(dtData.Rows(0)("apardate").ToString), "MM/dd/yyyy")
                curroid.SelectedValue = dtData.Rows(0)("curroid").ToString
                aparrateoid.Text = dtData.Rows(0)("rateoid").ToString
                aparrate2oid.Text = dtData.Rows(0)("rate2oid").ToString
                aparamt.Text = ToMaskEdit(ToDouble(dtData.Rows(0)("aparamt").ToString), 4)
                aparpaidamt.Text = ToMaskEdit(ToDouble(dtData.Rows(0)("amtbayar").ToString) + ToDouble(dtData.Rows(0)("amtdncn").ToString), 4)
                aparbalance.Text = ToMaskEdit(ToDouble(dtData.Rows(0)("aparamt").ToString) - (ToDouble(dtData.Rows(0)("amtbayar").ToString) + ToDouble(dtData.Rows(0)("amtdncn").ToString)), 4)
                maxamount.Text = ToMaskEdit(ToDouble(aparbalance.Text), 4)
                cnamt.Text = ToMaskEdit(ToDouble(dtData.Rows(0)("cnamt").ToString), 4)
                cnamt_TextChanged(Nothing, Nothing)
                coadebet.SelectedValue = dtData.Rows(0)("dbacctgoid").ToString
                InitCOACredit(dtData.Rows(0)("cracctgoid").ToString, dtData.Rows(0)("cracctgdesc").ToString)
                coacredit.SelectedValue = dtData.Rows(0)("cracctgoid").ToString
                cnnote.Text = dtData.Rows(0)("cnnote").ToString
                cnstatus.Text = dtData.Rows(0)("cnstatus").ToString
                create.Text = "Created By <B>" & dtData.Rows(0)("createuser").ToString & "</B> On <B>" & dtData.Rows(0)("createtime").ToString & "</B> "
                upduser.Text = dtData.Rows(0)("upduser").ToString
                updtime.Text = dtData.Rows(0)("updtime").ToString
                If cnstatus.Text = "In Process" Then
                    btnSave.Visible = True
                    btnPost.Visible = True
                    btnDelete.Visible = True
                    lblcnno.Text = "Draft No"
                    cnoid.Visible = True
                    cnno.Visible = False
                Else
                    btnSave.Visible = False
                    btnPost.Visible = False
                    btnDelete.Visible = False
                    lblcnno.Text = "CN No"
                    cnoid.Visible = False
                    cnno.Visible = True
                End If
            End If

        Catch ex As Exception
            showMessage("Error loading transaction data:<BR>" & ex.Message, 1, "")
            Exit Sub
        End Try
    End Sub

    Private Sub ResetAPARData()
        lblaparno.Text = IIf(reftype.SelectedValue.ToUpper = "AP", "A/P No", "A/R No")
        refoid.Text = 0 : aparno.Text = ""
        aparreftype.Text = "" : apardate.Text = ""
        cProc.DisposeGridView(gvAPAR) : Session("apar") = Nothing : gvAPAR.Visible = False
        lblaparcurr.Text = IIf(reftype.SelectedValue.ToUpper = "AP", "A/P Currency", "A/R Currency")
        curroid.SelectedIndex = 0 : aparrateoid.Text = "" : aparrate2oid.Text = ""
        lblaparamt.Text = IIf(reftype.SelectedValue.ToUpper = "AP", "A/P Amount", "A/R Amount")
        aparamt.Text = ""
        lblaparpaidamt.Text = IIf(reftype.SelectedValue.ToUpper = "AP", "A/P Paid Amt", "A/R Paid Amt")
        aparpaidamt.Text = ""
        lblaparbalance.Text = IIf(reftype.SelectedValue.ToUpper = "AP", "A/P Balance", "A/R Balance")
        aparbalance.Text = "" : cnamt.Text = ""
        lblaparafter.Text = IIf(reftype.SelectedValue.ToUpper = "AP", "A/P Balance After CN", "A/R Balance After CN")
        aparbalanceafter.Text = ""
        coadebet.SelectedIndex = IIf(coadebet.Items.Count > 0, 0, -1)
        coacredit.Items.Clear()
        maxamount.Text = "0"
        lblmaxamt.Visible = (reftype.SelectedValue.ToUpper = "AR")
        maxamount.Visible = (reftype.SelectedValue.ToUpper = "AR")
    End Sub

    Private Sub InitCOADebet(ByVal sCmpcode As String)
        ' COA Debet
        Dim sVarErr As String = "" : coadebet.Items.Clear()
        If reftype.SelectedValue.ToUpper = "AP" Then
            If IsInterfaceExists("VAR_CREDIT_NOTE_AP", sCmpcode) Then
                FillDDLAcctg(coadebet, "VAR_CREDIT_NOTE_AP", sCmpcode)
            Else
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_CREDIT_NOTE_AP"
            End If
            If sVarErr <> "" Then
                showMessage(GetInterfaceWarning(sVarErr, ""), 2, "")
            End If
        Else
            If IsInterfaceExists("VAR_CREDIT_NOTE_AR", sCmpcode) Then
                FillDDLAcctg(coadebet, "VAR_CREDIT_NOTE_AR", sCmpcode)
            Else
                sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_CREDIT_NOTE_AR"
            End If
            If sVarErr <> "" Then
                showMessage(GetInterfaceWarning(sVarErr, ""), 2, "")
            End If
        End If
    End Sub

    Private Sub InitCOACredit(ByVal iAcctgOid As Integer, ByVal sAcctgCodeDesc As String)
        coacredit.Items.Clear()
        coacredit.Items.Add(New ListItem(sAcctgCodeDesc, iAcctgOid))
    End Sub

    Public Sub InitAllDDL()
        ' Bussiness Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE' "
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & CompnyCode & "'"
        End If
        FillDDL(bus_unit, sSql)
        InitCOADebet(bus_unit.SelectedValue)

        ' Fill DDL Currency
        sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        FillDDL(curroid, sSql)
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer, ByVal sState As String)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        Dim sAsliTemp As String = sMessage.Replace("<br />", vbCrLf).Replace("<BR>", vbCrLf)
        Dim sTemp() As String = sAsliTemp.Split(vbCrLf)

        If sTemp.Length > 25 Then
            lblMessage.Text = "<textarea class='inpText' readonly='true' style='height:250px;width:99%;'>" & sAsliTemp & "</textarea>"
        Else
            lblMessage.Text = sMessage
        End If
        lblCaption.Text = strCaption : lblState.Text = sState
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then Response.Redirect("~\Other\login.aspx")
        If Not (checkPagePermission("~\accounting\trnCreditNote.aspx", Session("Role"))) Then
            Response.Redirect("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("trnCreditNote.aspx")
        End If

        Session("oid") = Request.QueryString("oid")
        Me.btnPost.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POSTING this data ?');")
        Me.btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data ?');")
        Page.Title = CompnyName & " - Credit Note"

        If Not IsPostBack Then
            CheckCNStatus()
            InitAllDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                Date1.Text = Format(GetServerTime, "MM/01/yyyy")
                Date2.Text = Format(GetServerTime, "MM/dd/yyyy")
                cndate.Text = Format(GetServerTime, "MM/dd/yyyy")
                cnoid.Text = GenerateID("QL_TRNCREDITNOTE", CompnyCode)
                cnstatus.Text = "In Process"
                btnDelete.Visible = False : btnShowCOA.Visible = False
                create.Text = "Created By <B>" & Session("UserID") & "</B> On <B>" & GetServerTime() & "</B>"
                upduser.Text = "-" : updtime.Text = "-"
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
    End Sub

    Protected Sub btnOKPopUp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOKPopUp.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        Select Case lblState.Text
            Case "REDIR"
                Response.Redirect("~\accounting\trnCreditNote.aspx?awal=true")
        End Select
    End Sub

    Protected Sub bus_unit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles bus_unit.SelectedIndexChanged
        reftype_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub reftype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles reftype.SelectedIndexChanged
        lblsupp_cust.Text = IIf(reftype.SelectedValue.ToUpper = "AP", "Supplier", "Customer")
        supp_cust_oid.Text = "" : supp_cust_name.Text = ""
        cProc.DisposeGridView(gvSuppCust) : gvSuppCust.Visible = False
        ResetAPARData()
        InitCOADebet(bus_unit.SelectedValue)
    End Sub

    Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSC.Click
        FilterDDLListSupp.SelectedIndex = -1
        FilterTextListSupp.Text = ""
        gvSuppCust.SelectedIndex = -1
        BindDataSuppCust()
    End Sub

    Protected Sub btnClearSC_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSC.Click
        gvSuppCust.Visible = False
        cProc.DisposeGridView(gvSuppCust)
        Session("suppcust") = Nothing
        supp_cust_oid.Text = ""
        supp_cust_name.Text = ""
        ResetAPARData()
    End Sub

    Protected Sub gvSuppCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSuppCust.PageIndexChanging
        gvSuppCust.PageIndex = e.NewPageIndex
        Dim dtSuppCust As DataTable = Session("suppcust")
        gvSuppCust.DataSource = dtSuppCust
        gvSuppCust.DataBind()
        mpeListSupp.Show()
    End Sub

    Protected Sub gvSuppCust_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        supp_cust_oid.Text = gvSuppCust.SelectedDataKey("oid").ToString
        supp_cust_name.Text = gvSuppCust.SelectedDataKey("name").ToString
        gvSuppCust.Visible = False
        cProc.DisposeGridView(gvSuppCust)
        Session("suppcust") = Nothing
        ResetAPARData()
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub btnFindAPAR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindAPAR.Click
        If supp_cust_oid.Text = "" Then
            showMessage("Please select " & lblsupp_cust.Text & " first.", 2, "") : Exit Sub
        End If

        If reftype.SelectedValue.ToUpper = "AP" Then
            sSql = "SELECT a.cmpcode,a.reftype,a.refoid,a.acctgoid,a.acctgaccount,a.transno,a.transdate,a.curroid,a.currcode,a.amttrans,a.amtbayar+a.amtdncn AS amtbayar,a.amttrans-(a.amtbayar+a.amtdncn) AS amtbalance FROM ( "
            sSql &= "SELECT ap.cmpcode,ap.reftype,ap.refoid,ap.suppoid,Tbl_Trans.transno AS transno,s.suppcode,s.suppname,ap.acctgoid ,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgaccount,ap.amttrans,c.curroid,c.currcode,CONVERT(VARCHAR(10),ap.trnapdate,101) AS transdate,(ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 INNER JOIN QL_trnpayap pay ON pay.cmpcode=ap2.cmpcode AND pay.payapoid=ap2.payrefoid WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid AND ISNULL(pay.payapres1,'')<>'Lebih Bayar' AND ap2.trnaptype LIKE 'PAYAP%'),0.0)) AS amtbayar,(ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2  WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid AND ap2.trnaptype IN ('DNAP','CNAP', 'RETPI')),0.0)) AS amtdncn  FROM QL_conap ap INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid INNER JOIN (SELECT bm.cmpcode, bm.trnbelimstoid oid, bm.trnbelino transno, bm.curroid, 'QL_trnbelimst' AS reftype FROM QL_trnbelimst bm UNION ALL SELECT apm.cmpcode, apm.approductmstoid oid, apm.approductno transno, apm.curroid, 'QL_trnapproductmst' AS reftype FROM QL_trnapproductmst apm) AS Tbl_Trans ON Tbl_Trans.cmpcode=ap.cmpcode AND Tbl_Trans.oid=ap.refoid AND Tbl_Trans.reftype=ap.reftype INNER JOIN QL_mstacctg a ON a.acctgoid=ap.acctgoid LEFT JOIN QL_mstcurr c ON c.curroid=Tbl_Trans.curroid AND c.cmpcode=Tbl_Trans.cmpcode WHERE ap.cmpcode='" & CompnyCode & "' AND ISNULL(ap.payrefoid,0)=0 AND ap.suppoid=" & supp_cust_oid.Text & " "
            sSql &= ") AS a "
            sSql &= " WHERE (a.amttrans - (a.amtbayar + a.amtdncn))<>0 AND a.transno LIKE '%" & Tchar(aparno.Text.Trim) & "%' ORDER BY a.transno "
        Else
            sSql = "SELECT a.cmpcode,a.reftype,a.refoid,a.acctgoid,a.acctgaccount,a.transno,a.transdate,a.curroid,(SELECT cr.currcode FROM QL_mstcurr cr WHERE cr.curroid=a.curroid) currcode,a.amttrans,a.amtbayar+a.amtdncn AS amtbayar,a.amttrans-(a.amtbayar+a.amtdncn) AS amtbalance FROM (SELECT ar.cmpcode,ar.reftype,ar.refoid,ar.custoid,c.custcode,c.custname,ar.acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgaccount,ar.amttrans, (CASE ar.reftype WHEN 'QL_trnjualmst' THEN (SELECT trnjualno FROM QL_trnjualmst jm WHERE jm.trnjualmstoid=ar.refoid) WHEN 'QL_trntransitemmst' THEN (SELECT transitemno FROM QL_trntransitemmst jm WHERE transitemmstoid=ar.refoid) ELSE '' END) AS transno, ar.trnardate AS transdate, 1 AS curroid,(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0)) AS amtbayar,(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ar2.trnartype IN ('DNAR','CNAR', 'SRET')),0.0)) AS amtdncn FROM QL_conar ar INNER JOIN QL_mstcust c ON ar.custoid=c.custoid INNER JOIN QL_mstacctg a ON a.acctgoid=ar.acctgoid WHERE ar.cmpcode='" & CompnyCode & "' AND ISNULL(ar.payrefoid,0)=0 AND ar.custoid=" & supp_cust_oid.Text & " "
            sSql &= ") AS a "
            sSql &= " WHERE (a.amttrans - (a.amtbayar + a.amtdncn))<>0 AND a.transno LIKE '%" & Tchar(aparno.Text.Trim) & "%' ORDER BY a.transno"
        End If
        Dim dtAPAR As DataTable = cKon.ambiltabel(sSql, "apar")
        Session("apar") = dtAPAR
        gvAPAR.DataSource = dtAPAR
        gvAPAR.DataBind()
        gvAPAR.Visible = True
    End Sub

    Protected Sub btnClearAPAR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearAPAR.Click
        ResetAPARData()
    End Sub

    Protected Sub gvAPAR_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAPAR.PageIndexChanging
        gvAPAR.PageIndex = e.NewPageIndex
        Dim dtAPAR As DataTable = Session("apar")
        gvAPAR.DataSource = dtAPAR
        gvAPAR.DataBind()
    End Sub

    Protected Sub gvAPAR_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvAPAR.RowDataBound
		If e.Row.RowType = DataControlRowType.DataRow Then
			e.Row.Cells(2).Text = CDate(e.Row.Cells(2).Text)
			e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
			e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
			e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
		ElseIf e.Row.RowType = DataControlRowType.Header Then
			e.Row.Cells(1).Text = IIf(reftype.SelectedValue.ToUpper = "AP", "A/P No", "A/R No")
			e.Row.Cells(2).Text = IIf(reftype.SelectedValue.ToUpper = "AP", "A/P Date", "A/R Date")
			e.Row.Cells(4).Text = IIf(reftype.SelectedValue.ToUpper = "AP", "A/P Amount", "A/R Amount")
			e.Row.Cells(6).Text = IIf(reftype.SelectedValue.ToUpper = "AP", "A/P Balance", "A/R Balance")
		End If
    End Sub

    Protected Sub gvAPAR_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvAPAR.SelectedIndexChanged
        Dim sRef As String = gvAPAR.SelectedDataKey("reftype").ToString
        Dim iRef As Integer = gvAPAR.SelectedDataKey("refoid").ToString
        Dim dtAPAR As DataTable = Session("apar")
        Dim dvSelectAPAR As DataView = dtAPAR.DefaultView
        dvSelectAPAR.RowFilter = "reftype='" & sRef & "' AND refoid=" & iRef
        If dvSelectAPAR.Count < 1 Then
            showMessage("Invalid " & reftype.SelectedItem.Text & " selection.", 2, "")
        Else
            refoid.Text = iRef
            aparreftype.Text = sRef
            aparno.Text = dvSelectAPAR(0)("transno").ToString
            apardate.Text = Format(CDate(dvSelectAPAR(0)("transdate").ToString), "MM/dd/yyyy")
            curroid.SelectedValue = dvSelectAPAR(0)("curroid").ToString
            cRate.SetRateValue(CInt(curroid.SelectedValue), apardate.Text)
            aparrateoid.Text = cRate.GetRateDailyOid
            aparrate2oid.Text = cRate.GetRateMonthlyOid
            aparamt.Text = ToMaskEdit(ToDouble(dvSelectAPAR(0)("amttrans").ToString), 4)
            aparpaidamt.Text = ToMaskEdit(ToDouble(dvSelectAPAR(0)("amtbayar").ToString), 4)
            aparbalance.Text = ToMaskEdit(ToDouble(dvSelectAPAR(0)("amtbalance").ToString), 4)

            coadebet.SelectedIndex = 0
            InitCOACredit(dvSelectAPAR(0)("acctgoid").ToString, dvSelectAPAR(0)("acctgaccount").ToString)
            lblmaxamt.Visible = (reftype.SelectedValue.ToUpper = "AR")
            maxamount.Visible = (reftype.SelectedValue.ToUpper = "AR")
            If maxamount.Visible Then
                maxamount.Text = ToMaskEdit(ToDouble(dvSelectAPAR(0)("amtbalance").ToString), 4)
            Else
                maxamount.Text = "0"
            End If
            cnamt.Text = ""
            aparbalanceafter.Text = ToMaskEdit(ToDouble(dvSelectAPAR(0)("amtbalance").ToString), 4)
        End If
        dvSelectAPAR.RowFilter = ""
        gvAPAR.Visible = False
        cProc.DisposeGridView(gvAPAR)
        Session("apar") = Nothing
    End Sub

    Protected Sub cnamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnamt.TextChanged
        cnamt.Text = ToMaskEdit(ToDouble(cnamt.Text), 4)
        aparbalanceafter.Text = ToMaskEdit(ToDouble(aparbalance.Text) + (ToDouble(cnamt.Text) * IIf(reftype.SelectedValue.ToUpper = "AR", -1, 1)), 4)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\accounting\trnCreditNote.aspx?awal=true")
    End Sub

    Protected Sub btnshowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnShowCOA.Click
        'showTableCOA(no.Text, cmpcode, gvakun)
        'pnlPosting2.Visible = True : btnHidePosting2.Visible = True : mpePosting2.Show()
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPost.Click
        cnstatus.Text = "Post" : btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim sMsg As String = ""

        If bus_unit.Items.Count < 1 Then sMsg &= "- Please create Bussines Unit data first.<BR>"
        If cndate.Text = "" Then sMsg &= "- Please fill CN Date first.<BR>"
        Dim sRefe As String = ""
        If cndate.Text <> "" Then If Not IsValidDate(cndate.Text, "MM/dd/yyyy", sRefe) Then sMsg &= "- CN Date is invalid. " & sRefe & "<BR>"
        If ToDouble(supp_cust_oid.Text) = 0 Then sMsg &= "- Please select " & lblsupp_cust.Text & " first.<BR>"
        If refoid.Text = "" Then
            sMsg &= "- Please select " & reftype.SelectedItem.Text & " first.<BR>"
        Else
            If ToDouble(cnamt.Text) <= 0 Then
                sMsg &= "- Credit Note amount must be greater than 0.<BR>"
            Else
                If reftype.SelectedValue.ToUpper = "AR" Then
                    If ToDouble(cnamt.Text) > ToDouble(maxamount.Text) Then
                        sMsg &= "- Maximum Credit Note amount is " & ToMaskEdit(ToDouble(maxamount.Text), 4) & "<BR>"
                    End If
                End If
            End If
            If cnstatus.Text = "Post" Then
                cRate.SetRateValue(CInt(curroid.SelectedValue), apardate.Text)
                If cRate.GetRateDailyLastError <> "" Then sMsg &= "- " & cRate.GetRateDailyLastError & ".<BR>"
                If cRate.GetRateMonthlyLastError <> "" Then sMsg &= "- " & cRate.GetRateMonthlyLastError & ".<BR>"
            End If
        End If
        If reftype.SelectedValue.ToUpper = "AP" Then
            If coadebet.Items.Count < 1 Then sMsg &= "- " & GetInterfaceWarning("VAR_CREDIT_NOTE_AP", "") & ".<BR>"
        Else
            If coadebet.Items.Count < 1 Then sMsg &= "- " & GetInterfaceWarning("VAR_CREDIT_NOTE_AR", "") & ".<BR>"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, 2, "")
            cnstatus.Text = "In Process" : Exit Sub
        End If

        Dim sDraftNo As String = cnoid.Text : Dim sPostNo As String = cnno.Text
        If Session("oid") = Nothing Or Session("oid") = "" Then
            sSql = "SELECT COUNT(*) FROM QL_trncreditnote WHERE cnoid=" & cnoid.Text
            If CheckDataExists(sSql) Then
                cnoid.Text = GenerateID("QL_TRNCREDITNOTE", CompnyCode)
            End If
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_trncreditnote", "cnoid", cnoid.Text, "cnstatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2, "")
                cnstatus.Text = "In Process" : Exit Sub
            End If
        End If

        If cnstatus.Text = "Post" Then
            GenerateCNNo()
            If isPeriodAcctgClosed(bus_unit.SelectedValue, cndate.Text) Then
                showMessage("Cannot posting accounting data to period " & MonthName(Month(CDate(cndate.Text))).ToUpper & " " & Year(CDate(cndate.Text)).ToString & " anymore because the period has been closed. Please select another period!", 2, "") : cnstatus.Text = "In Process" : Exit Sub
            End If
        End If
        Dim sDateNow As Date = GetServerTime.Date
        Dim sPeriodAcctgNow As String = GetDateToPeriodAcctg(GetServerTime)

        Dim iConap As Integer = GenerateID("QL_CONAP", CompnyCode)
        Dim iConar As Integer = GenerateID("QL_CONAR", CompnyCode)
        Dim iGLMstOid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
        Dim iGLDtlOid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then conn.Open()
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            If Session("oid") = Nothing Or Session("oid") = "" Then
				sSql = "INSERT INTO QL_trncreditnote (cmpcode,cnoid,cnno,cndate,reftype,refoid,suppcustoid,dbacctgoid,cracctgoid,curroid,rateoid,rate2oid,cnamt,cnamtidr,cnamtusd," & _
					"cntaxtype,cntaxamt,cntaxamtidr,cntaxamtusd,cntotal,cntotalidr,cntotalusd,cnnote,cnstatus,createuser,createtime,upduser,updtime) VALUES " & _
					"('" & CompnyCode & "'," & cnoid.Text & ",'" & cnno.Text & "','" & CDate(cndate.Text) & "','" & aparreftype.Text & "'," & refoid.Text & "," & _
					"" & supp_cust_oid.Text & "," & coadebet.SelectedValue & "," & coacredit.SelectedValue & "," & curroid.SelectedValue & "," & aparrateoid.Text & "," & aparrate2oid.Text & "," & _
					"" & ToDouble(cnamt.Text) & "," & ToDouble(cnamt.Text) * cRate.GetRateMonthlyIDRValue & "," & ToDouble(cnamt.Text) * cRate.GetRateMonthlyUSDValue & "," & _
					"'" & cntaxtype.SelectedValue & "'," & ToDouble(cntaxamt.Text) & "," & ToDouble(cntaxamt.Text) * cRate.GetRateMonthlyIDRValue & "," & ToDouble(cntaxamt.Text) * cRate.GetRateMonthlyUSDValue & "," & _
					"" & (ToDouble(cnamt.Text) + ToDouble(cntaxamt.Text)) & "," & (ToDouble(cnamt.Text) + ToDouble(cntaxamt.Text)) * cRate.GetRateMonthlyIDRValue & "," & _
					"" & (ToDouble(cnamt.Text) + ToDouble(cntaxamt.Text)) * cRate.GetRateMonthlyUSDValue & ",'" & Tchar(cnnote.Text) & "','" & cnstatus.Text & "'," & _
					"'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & cnoid.Text & " WHERE tablename='QL_TRNCREDITNOTE' and cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Else
                ' SELECT OLD DATA 
				sSql = "SELECT reftype FROM QL_trncreditnote WHERE cmpcode='" & CompnyCode & "' AND cnoid=" & cnoid.Text
                xCmd.CommandText = sSql
                Dim sOldType As String = xCmd.ExecuteScalar
				sSql = "SELECT refoid FROM QL_trncreditnote WHERE cmpcode='" & CompnyCode & "' AND cnoid=" & cnoid.Text
                xCmd.CommandText = sSql
                Dim sOldOid As String = xCmd.ExecuteScalar

                If reftype.SelectedValue.ToUpper = "AP" Then
					sSql = "DELETE FROM QL_conap WHERE cmpcode='" & CompnyCode & "' AND trnaptype='CNAP' AND payrefoid=" & cnoid.Text
                Else
					sSql = "DELETE FROM QL_conar WHERE cmpcode='" & CompnyCode & "' AND trnartype='CNAR' AND payrefoid=" & cnoid.Text
                End If
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If reftype.SelectedValue.ToUpper = "AP" Then
					sSql = " UPDATE QL_TRNBELIMST SET trnbelimststatus='Approved', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & CompnyCode & "' AND trnbelimstoid=" & sOldOid
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else
                    If sOldType.ToUpper = "QL_TRNJUALMST" Then
                        sSql = " UPDATE QL_trnjualmst SET trnjualstatus='Post', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & CompnyCode & "' AND trnjualmstoid=" & sOldOid
                    ElseIf sOldType.ToUpper = "QL_TRNTRANSITEMMST" Then
                        sSql = " UPDATE QL_trntransitemmst SET transitemmststatus='Post', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & CompnyCode & "' AND transitemmstoid=" & sOldOid
                    End If
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

				sSql = "UPDATE QL_trncreditnote SET cnno='" & cnno.Text & "',cndate='" & CDate(cndate.Text) & "',reftype='" & aparreftype.Text & "',refoid=" & refoid.Text & "," & _
					"suppcustoid=" & supp_cust_oid.Text & ",dbacctgoid=" & coadebet.SelectedValue & ",cracctgoid=" & coacredit.SelectedValue & ",curroid=" & curroid.SelectedValue & "," & _
					"rateoid=" & aparrateoid.Text & ",rate2oid=" & aparrate2oid.Text & ",cnamt=" & ToDouble(cnamt.Text) & ",cnamtidr=" & ToDouble(cnamt.Text) * cRate.GetRateMonthlyIDRValue & "," & _
					"cnamtusd=" & ToDouble(cnamt.Text) * cRate.GetRateMonthlyUSDValue & ",cntaxtype='" & cntaxtype.SelectedValue & "',cntaxamt=" & ToDouble(cntaxamt.Text) & "," & _
					"cntaxamtidr=" & ToDouble(cntaxamt.Text) * cRate.GetRateMonthlyIDRValue & ",cntaxamtusd=" & ToDouble(cntaxamt.Text) * cRate.GetRateMonthlyUSDValue & "," & _
					"cntotal=" & (ToDouble(cnamt.Text) + ToDouble(cntaxamt.Text)) & ",cntotalidr=" & (ToDouble(cnamt.Text) + ToDouble(cntaxamt.Text)) * cRate.GetRateMonthlyIDRValue & "," & _
					"cntotalusd=" & (ToDouble(cnamt.Text) + ToDouble(cntaxamt.Text)) * cRate.GetRateMonthlyUSDValue & ",cnnote='" & Tchar(cnnote.Text) & "',cnstatus='" & cnstatus.Text & "'," & _
					"upduser='" & Session("UserID") & "',updtime=CURRENT_TIMESTAMP " & _
					"WHERE cmpcode='" & CompnyCode & "' AND cnoid=" & cnoid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            If reftype.SelectedValue.ToUpper = "AP" Then
				sSql = "INSERT INTO QL_conap (cmpcode,conapoid,reftype,refoid,payrefoid,suppoid,acctgoid,trnapstatus,trnaptype,trnapdate,periodacctg,paymentacctgoid,paymentdate,payrefno," & _
					"paybankoid,payduedate,amttrans,amtbayar,trnapnote,createuser,createtime,upduser,updtime,amttransidr,amtbayaridr,amttransusd,amtbayarusd,amtbayarother) VALUES " & _
					"('" & CompnyCode & "'," & iConap & ",'" & aparreftype.Text & "'," & refoid.Text & "," & cnoid.Text & "," & supp_cust_oid.Text & "," & coacredit.SelectedValue & "," & _
					"'" & cnstatus.Text & "','CN" & reftype.SelectedValue.ToUpper & "','1/1/1900','" & GetDateToPeriodAcctg(CDate(cndate.Text)) & "'," & coadebet.SelectedValue & ",'" & CDate(cndate.Text) & "'," & _
					"'" & cnno.Text & "',0,'" & CDate(cndate.Text) & "',0," & (ToDouble(cnamt.Text) + ToDouble(cntaxamt.Text)) * -1 & ",'CN No. " & cnno.Text & " for A/P No. " & aparno.Text & "'," & _
					"'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,0," & (ToDouble(cnamt.Text) + ToDouble(cntaxamt.Text)) * cRate.GetRateMonthlyIDRValue * -1 & "," & _
					"0," & (ToDouble(cnamt.Text) + ToDouble(cntaxamt.Text)) * cRate.GetRateMonthlyUSDValue * -1 & ",0)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & iConap & " WHERE tablename='QL_CONAP' and cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Else
				sSql = "INSERT INTO QL_conar (cmpcode,conaroid,reftype,refoid,payrefoid,custoid,acctgoid,trnarstatus,trnartype,trnardate,periodacctg,paymentacctgoid,paymentdate,payrefno," & _
					"paybankoid,payduedate,amttrans,amtbayar,trnarnote,createuser,createtime,upduser,updtime,amttransidr,amtbayaridr,amttransusd,amtbayarusd,amtbayarother) VALUES " & _
					"('" & CompnyCode & "'," & iConar & ",'" & aparreftype.Text & "'," & refoid.Text & "," & cnoid.Text & "," & supp_cust_oid.Text & "," & coacredit.SelectedValue & "," & _
					"'" & cnstatus.Text & "','CN" & reftype.SelectedValue.ToUpper & "','1/1/1900','" & GetDateToPeriodAcctg(CDate(cndate.Text)) & "'," & coadebet.SelectedValue & ",'" & CDate(cndate.Text) & "'," & _
					"'" & cnno.Text & "',0,'" & CDate(cndate.Text) & "',0," & (ToDouble(cnamt.Text) + ToDouble(cntaxamt.Text)) & ",'CN No. " & cnno.Text & " for A/R No. " & aparno.Text & "'," & _
					"'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,0," & (ToDouble(cnamt.Text) + ToDouble(cntaxamt.Text)) * cRate.GetRateMonthlyIDRValue & "," & _
					"0," & (ToDouble(cnamt.Text) + ToDouble(cntaxamt.Text)) * cRate.GetRateMonthlyUSDValue & ",0)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & iConar & " WHERE tablename='QL_CONAR' and cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If ToDouble(aparbalanceafter.Text) <= 0 Then
                    Dim sRef As String = aparreftype.Text.ToUpper
                    If sRef = "QL_TRNJUALMST" Then
                        sSql = " UPDATE QL_trnjualmst SET trnjualstatus='Closed', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & CompnyCode & "' AND trnjualmstoid=" & refoid.Text
                    ElseIf sRef = "QL_TRNTRANSITEMMST" Then
                        sSql = " UPDATE QL_trntransitemmst SET transitemmststatus='Closed', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & CompnyCode & "' AND transitemmstoid=" & refoid.Text
                    End If
					xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            End If

            If cnstatus.Text = "Post" Then
                ' Insert GL MST
				sSql = "INSERT INTO QL_trnglmst (cmpcode,glmstoid,gldate,periodacctg,glnote,glflag,postdate,createuser,createtime,upduser,updtime,type,rateoid,rate2oid,glrateidr,glrate2idr,glrateusd,glrate2usd) VALUES " & _
					"('" & CompnyCode & "'," & iGLMstOid & ",'" & (CDate(cndate.Text)) & "','" & GetDateToPeriodAcctg(CDate(cndate.Text)) & "','CN No. " & cnno.Text & " for " & reftype.SelectedItem.Text & " No. " & aparno.Text & "','Post',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP," & _
					"'" & Session("UserID") & "',CURRENT_TIMESTAMP,''," & cRate.GetRateDailyOid & "," & cRate.GetRateMonthlyOid & "," & cRate.GetRateDailyIDRValue & "," & cRate.GetRateMonthlyIDRValue & "," & cRate.GetRateDailyUSDValue & "," & cRate.GetRateMonthlyUSDValue & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

				sSql = "INSERT INTO QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,noref,glnote,glflag,upduser,updtime,glamtidr,glamtusd,glother1,glother2) VALUES " & _
					"('" & CompnyCode & "'," & iGLDtlOid & ",1," & iGLMstOid & "," & coadebet.SelectedValue & ",'D'," & ToDouble(cnamt.Text) + ToDouble(cntaxamt.Text) & "," & _
					"'" & cnno.Text & "','CN No. " & cnno.Text & " for " & reftype.SelectedItem.Text & " No. " & aparno.Text & "','Post','" & Session("UserID") & "',CURRENT_TIMESTAMP," & _
					"" & (ToDouble(cnamt.Text) + ToDouble(cntaxamt.Text)) * cRate.GetRateMonthlyIDRValue & "," & (ToDouble(cnamt.Text) + ToDouble(cntaxamt.Text)) * cRate.GetRateMonthlyUSDValue & ",'QL_trncreditnote','" & cnoid.Text & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                iGLDtlOid += 1

				sSql = "INSERT INTO QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,noref,glnote,glflag,upduser,updtime,glamtidr,glamtusd,glother1,glother2) VALUES " & _
					"('" & CompnyCode & "'," & iGLDtlOid & ",2," & iGLMstOid & "," & coacredit.SelectedValue & ",'C'," & ToDouble(cnamt.Text) + ToDouble(cntaxamt.Text) & "," & _
					"'" & cnno.Text & "','CN No. " & cnno.Text & " for " & reftype.SelectedItem.Text & " No. " & aparno.Text & "','Post','" & Session("UserID") & "',CURRENT_TIMESTAMP," & _
					"" & (ToDouble(cnamt.Text) + ToDouble(cntaxamt.Text)) * cRate.GetRateMonthlyIDRValue & "," & (ToDouble(cnamt.Text) + ToDouble(cntaxamt.Text)) * cRate.GetRateMonthlyUSDValue & ",'QL_trncreditnote','" & cnoid.Text & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & iGLMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & iGLDtlOid & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            objTrans.Commit() : conn.Close()
        Catch exSql As SqlException
            objTrans.Rollback() : conn.Close()
            If ToDouble(Session("ErrorCounter")) < 5 Then
                If exSql.Number = 2627 Then
                    Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                    btnSave_Click(sender, e)
                    Exit Sub
                Else
                    showMessage(exSql.ToString, 1, "")
                    cnstatus.Text = "In Process"
                    Exit Sub
                End If
            Else
                showMessage(exSql.ToString, 1, "")
                cnstatus.Text = "In Process"
                Exit Sub
            End If
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            cnstatus.Text = "In Process"
            showMessage(ex.ToString, 1, "") : Exit Sub
        End Try

        If sDraftNo <> cnoid.Text Then
            showMessage("Draft No. have been regenerated because have been used by another data. Your new Draft No. is " & cnoid.Text & ".<BR>", 3, "REDIR")
        Else
            If sPostNo <> cnno.Text Then
                showMessage("Data have been posted with CN No. " & cnno.Text & ".", 3, "REDIR")
            Else
                Response.Redirect("~\Accounting\trnCreditNote.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If cnoid.Text.Trim = "" Then
            showMessage("Please select Credit Note data first!", 2, "") : Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_trncreditnote", "cnoid", cnoid.Text, "cnstatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2, "")
                cnstatus.Text = "In Process" : Exit Sub
            End If
        End If
        Dim sColName As String = "" : Dim sStatus As String = "" : Dim sColStatus As String = ""
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then conn.Open()
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            ' SELECT OLD DATA 
            sSql = "SELECT reftype FROM QL_trncreditnote WHERE cmpcode='" & bus_unit.SelectedValue & "' AND cnoid=" & cnoid.Text
            xCmd.CommandText = sSql
            Dim sOldType As String = xCmd.ExecuteScalar
            sSql = "SELECT refoid FROM QL_trncreditnote WHERE cmpcode='" & bus_unit.SelectedValue & "' AND cnoid=" & cnoid.Text
            xCmd.CommandText = sSql
            Dim sOldOid As String = xCmd.ExecuteScalar
            If sOldType.ToUpper = "QL_TRNJUALMST" Then
                sColName = "trnjualmstoid" : sStatus = "Post" : sColStatus = "trnjualstatus"
            ElseIf sOldType.ToUpper = "QL_TRNTRANSITEMMST" Then
                sColName = "transitemmstoid" : sStatus = "Post" : sColStatus = "transitemmststatus"
            End If
            If sOldType.ToUpper = "QL_TRNBELIMST" Or sOldType.ToUpper = "QL_TRNAPPRODUCTMST" Then
                sSql = "DELETE FROM QL_conap WHERE cmpcode='" & bus_unit.SelectedValue & "' AND trnaptype='CNAP' AND payrefoid=" & cnoid.Text
            Else
                sSql = "DELETE FROM QL_conar WHERE cmpcode='" & bus_unit.SelectedValue & "' AND trnartype='CNAR' AND payrefoid=" & cnoid.Text
            End If
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            If sOldType.ToUpper = "QL_TRNJUALMST" Then
                sSql = "UPDATE " & sOldType.ToUpper & " SET " & sColStatus & "='" & sStatus & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & bus_unit.SelectedValue & "' AND " & sColName & "=" & sOldOid
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            ElseIf sOldType.ToUpper = "QL_TRNTRANSITEMMST" Then
                sSql = "UPDATE " & sOldType.ToUpper & " SET " & sColStatus & "='" & sStatus & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & bus_unit.SelectedValue & "' AND " & sColName & "=" & sOldOid
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            sSql = "DELETE FROM QL_trncreditnote WHERE cmpcode='" & bus_unit.SelectedValue & "' AND cnoid=" & cnoid.Text
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, 1, "") : Exit Sub
        End Try
        Response.Redirect("~\accounting\trnCreditNote.aspx?awal=true")
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
    End Sub

    Protected Sub gvMst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvMst.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "MM/dd/yyyy")
            e.Row.Cells(6).Text = ToMaskEdit(e.Row.Cells(6).Text, 4)
        End If
    End Sub

    Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvMst.DataSource = Session("TblMst")
            gvMst.DataBind()
        End If
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFind.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If chkPeriod.Checked Then
            If IsValidPeriod(Date1.Text, Date2.Text) Then
                sSqlPlus &= " AND a.cndate>='" & Date1.Text & " 00:00:00' AND a.cndate<='" & Date2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        sSqlPlus &= " AND a.reftype LIKE '" & FilterType.SelectedValue & "' AND a.cnstatus LIKE '" & FilterStatus.SelectedValue & "'"
        If checkPagePermission("~\Accounting\trnCreditNote.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND a.createuser='" & Session("UserID") & "' "
        End If
        BindData(sSqlPlus)
    End Sub

    Protected Sub btnViewAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAll.Click
        Dim sSqlPlus As String = ""
        FilterDDL.SelectedIndex = -1 : FilterText.Text = ""
        chkPeriod.Checked = False
        Date1.Text = Format(GetServerTime(), "MM/01/yyyy")
        Date2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        FilterType.SelectedIndex = 0
        FilterStatus.SelectedIndex = 0
        If checkPagePermission("~\Accounting\trnCreditNote.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND a.createuser='" & Session("UserID") & "' "
        End If
        BindData(sSqlPlus)
    End Sub

    Protected Sub lkbCNInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCNInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = "" : FilterDDL.SelectedIndex = -1
        chkPeriod.Checked = False
        Date1.Text = Format(GetServerTime(), "MM/01/yyyy")
        Date2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        FilterType.SelectedIndex = 0
        FilterStatus.SelectedIndex = 0
        Dim sSqlPlus As String = ""
        If checkPagePermission("~\Accounting\trnCreditNote.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND a.createuser='" & Session("UserID") & "' "
        End If
        sSqlPlus &= " AND DATEDIFF(DAY, a.updtime, GETDATE()) > " & nDays & " AND a.cnstatus='In Process' "
        BindData(sSqlPlus)
    End Sub

    Protected Sub btnFindListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSupp.Click
        BindDataSuppCust()
        mpeListSupp.Show()
    End Sub

    Protected Sub btnAllListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListSupp.Click
        FilterDDLListSupp.SelectedIndex = -1
        FilterTextListSupp.Text = ""
        gvSuppCust.SelectedIndex = -1
        BindDataSuppCust()
        mpeListSupp.Show()
    End Sub

    Protected Sub lkbCloseListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

#End Region
End Class