<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnExpense.aspx.vb" Inherits="Accounting_CashBankExpense" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" ForeColor="SaddleBrown" Text=".: Cash/Bank Expense" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Cash/Bank Expense :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" __designer:wfdid="w575" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=5></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="Label19" runat="server" Text="Filter" __designer:wfdid="w576"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="105px" __designer:wfdid="w577"><asp:ListItem Value="cb.cashbankno">Cash/Bank No.</asp:ListItem>
<asp:ListItem Value="a.acctgdesc">Payment Account</asp:ListItem>
<asp:ListItem Value="ISNULL(suppname, '')">Supplier</asp:ListItem>
<asp:ListItem Value="p.personname">PIC</asp:ListItem>
<asp:ListItem Value="cb.cashbanknote">Note</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="200px" __designer:wfdid="w578"></asp:TextBox></TD><TD align=left></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:CheckBox id="cbType" runat="server" Text="Type" __designer:wfdid="w579"></asp:CheckBox></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="ddlType" runat="server" CssClass="inpText" Width="105px" __designer:wfdid="w580"><asp:ListItem Value="BKK">CASH</asp:ListItem>
<asp:ListItem Value="BBK">TRANSFER</asp:ListItem>
<asp:ListItem Value="BGK">GIRO/CHEQUE</asp:ListItem>
<asp:ListItem Value="BLK">DOWN PAYMENT</asp:ListItem>
</asp:DropDownList> </TD><TD align=left></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text=" Period" __designer:wfdid="w581"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="60px" __designer:wfdid="w582"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w583"></asp:ImageButton>&nbsp;<asp:Label id="Label5" runat="server" Text="to" __designer:wfdid="w584"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="60px" __designer:wfdid="w585"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w586"></asp:ImageButton>&nbsp;<asp:Label id="lbldate1" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w587"></asp:Label></TD><TD align=left colSpan=1></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text=" Status" __designer:wfdid="w588"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="105px" __designer:wfdid="w589">
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
</asp:DropDownList></TD><TD align=left colSpan=1></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w590" TargetControlID="FilterPeriod1" PopupButtonID="imbDate1" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender3" runat="server" __designer:wfdid="w591" TargetControlID="FilterPeriod1" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w592" TargetControlID="FilterPeriod2" PopupButtonID="imbDate2" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender4" runat="server" __designer:wfdid="w593" TargetControlID="FilterPeriod2" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> </TD><TD align=left colSpan=1></TD></TR><TR><TD style="HEIGHT: 5pt" class="Label" align=left colSpan=4></TD><TD align=left colSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w594"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w595"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w596"></asp:ImageButton></TD><TD align=left colSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:LinkButton id="lkbExpenseInProcess" runat="server" __designer:wfdid="w597"></asp:LinkButton></TD><TD align=left colSpan=1></TD></TR><TR><TD style="HEIGHT: 5pt" class="Label" align=left colSpan=4></TD><TD align=left colSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvTRN" runat="server" ForeColor="#333333" Width="100%" __designer:wfdid="w571" DataKeyNames="cashbankoid" PageSize="8" CellPadding="4" AutoGenerateColumns="False" GridLines="None" AllowPaging="True" AllowSorting="True" OnRowDataBound="gvTRN_RowDataBound">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="cashbankoid" DataNavigateUrlFormatString="~\Accounting\trnExpense.aspx?oid={0}" DataTextField="cashbankno" HeaderText="Cash/Bank No." SortExpression="cashbankno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="cashbankdate" HeaderText="Expense Date" SortExpression="cashbankdate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbanktype" HeaderText="Type" SortExpression="cashbanktype">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Payment Account" SortExpression="acctgdesc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier" SortExpression="suppname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankamt" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankstatus" HeaderText="Status" SortExpression="cashbankstatus">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbanknote" HeaderText="Note" SortExpression="cashbanknote">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
            <asp:CheckBox ID="cbGVMst" runat="server" Checked='<%# eval("checkvalue") %>' />
            <asp:Label ID="lblOidGVMst" runat="server" Text='<%# eval("cashbankoid") %>' Visible="False"></asp:Label>
        
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w599"></asp:Label></TD><TD align=left colSpan=1></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label17" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Cash/Bank Expense Header" Font-Underline="False" __designer:wfdid="w706"></asp:Label> </TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="i_u" runat="server" CssClass="Important" Text="New Data" __designer:wfdid="w707"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="periodacctg" runat="server" __designer:wfdid="w708" Visible="False"></asp:Label> <asp:Label id="cashbankoid" runat="server" __designer:wfdid="w709" Visible="False"></asp:Label><asp:RadioButtonList id="RblExpense" runat="server" __designer:wfdid="w1" RepeatDirection="Horizontal" OnSelectedIndexChanged="RblExpense_SelectedIndexChanged" AutoPostBack="True"><asp:ListItem Selected="True">Expense</asp:ListItem>
<asp:ListItem>PPH</asp:ListItem>
</asp:RadioButtonList></TD><TD style="WIDTH: 59px" class="Label" align=left><asp:Label id="Label2" runat="server" Text="Business Unit" __designer:wfdid="w712" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w713" Visible="False" AutoPostBack="True" Enabled="False"></asp:DropDownList> <asp:Label id="suppoid" runat="server" __designer:wfdid="w710" Visible="False"></asp:Label> <asp:Label id="giroacctgoid" runat="server" __designer:wfdid="w711" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblInfoNo" runat="server" Text="Cash/Bank No." Width="96px" __designer:wfdid="w714"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbankno" runat="server" CssClass="inpTextDisabled" Width="160px" __designer:wfdid="w715" Enabled="False"></asp:TextBox></TD><TD style="WIDTH: 59px" class="Label" align=left><asp:Label id="Label6" runat="server" Text="Expense Date" Width="96px" __designer:wfdid="w716"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbankdate" runat="server" CssClass="inpText" Width="80px" __designer:wfdid="w717" AutoPostBack="True" EnableTheming="True" ToolTip="MM/DD/YYYY"></asp:TextBox> <asp:ImageButton id="imbDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Width="16px" __designer:wfdid="w718"></asp:ImageButton> <asp:Label id="lblInfoDate" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w719"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label10" runat="server" Text="Peyment Type" Width="88px" __designer:wfdid="w720"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="cashbanktype" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w721" AutoPostBack="True"><asp:ListItem Value="BKK">CASH</asp:ListItem>
<asp:ListItem Value="BBK">TRANSFER</asp:ListItem>
<asp:ListItem Value="BGK">GIRO/CHEQUE</asp:ListItem>
<asp:ListItem Value="BLK">DOWN PAYMENT</asp:ListItem>
</asp:DropDownList></TD><TD style="WIDTH: 59px" class="Label" align=left><asp:Label id="Label3" runat="server" Text="Payment Account" Width="104px" __designer:wfdid="w722"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="acctgoid" runat="server" CssClass="inpText" __designer:wfdid="w723" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label15" runat="server" Text="Currency" __designer:wfdid="w724"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="curroid" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w725" Enabled="False"></asp:DropDownList></TD><TD style="WIDTH: 59px" class="Label" align=left><asp:Label id="Label25" runat="server" Text="Total Amount" Width="88px" __designer:wfdid="w726"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbankamt" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w727" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblRefNo" runat="server" Text="Ref. No." __designer:wfdid="w728"></asp:Label> <asp:Label id="lblWarnRefNo" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w729" Visible="False"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbankrefno" runat="server" CssClass="inpText" Width="160px" __designer:wfdid="w730" MaxLength="30"></asp:TextBox> <asp:ImageButton id="btnSearchDP" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w731"></asp:ImageButton> <asp:ImageButton id="btnClearDP" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w732"></asp:ImageButton></TD><TD style="WIDTH: 59px" class="Label" align=left><asp:Label id="lblDueDate" runat="server" Text="Due Date" __designer:wfdid="w733"></asp:Label> <asp:Label id="lblWarnDueDate" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w734" Visible="False"></asp:Label></TD><TD class="Label" align=center><asp:Label id="lblSeptDueDate" runat="server" Text=":" __designer:wfdid="w735"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="dpapamt" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w736" Enabled="False"></asp:TextBox><asp:TextBox id="cashbankduedate" runat="server" CssClass="inpText" Width="80px" __designer:wfdid="w737" ToolTip="MM/DD/YYYY"></asp:TextBox> <asp:ImageButton id="imbDueDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Width="16px" __designer:wfdid="w738"></asp:ImageButton> <asp:Label id="lblInfoDueDate" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w739"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblDTG" runat="server" Text="Date Take Giro" Width="96px" __designer:wfdid="w636"></asp:Label> <asp:Label id="lblWarnDTG" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w741"></asp:Label></TD><TD class="Label" align=center><asp:Label id="lblSeptDTG" runat="server" Text=":" __designer:wfdid="w742"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="cashbanktakegiro" runat="server" CssClass="inpText" Width="80px" __designer:wfdid="w743" ToolTip="MM/DD/YYYY"></asp:TextBox> <asp:ImageButton id="imbDTG" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Width="16px" __designer:wfdid="w744"></asp:ImageButton> <asp:Label id="lblInfoDTG" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w745"></asp:Label></TD><TD style="WIDTH: 59px" class="Label" align=left><asp:Label id="lblDeduction" runat="server" Text="Total Deduction" Width="96px" __designer:wfdid="w746" Visible="False"></asp:Label></TD><TD class="Label" align=center><asp:Label id="lblDeductionSign" runat="server" Text=":" __designer:wfdid="w747" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="totaldeduction" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w748" Visible="False" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblSuppCust" runat="server" Text="Supplier" __designer:wfdid="w749"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="suppname" runat="server" CssClass="inpTextDisabled" Width="160px" __designer:wfdid="w750" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSearchSupp" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w751"></asp:ImageButton> <asp:ImageButton id="btnClearSupp" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w752"></asp:ImageButton></TD><TD style="WIDTH: 59px" class="Label" align=left><asp:Label id="Label22" runat="server" Text="Tax" __designer:wfdid="w753"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="cashbanktaxtype" runat="server" CssClass="inpText" Width="85px" __designer:wfdid="w754" OnSelectedIndexChanged="cashbanktaxtype_SelectedIndexChanged" AutoPostBack="True"><asp:ListItem>NON TAX</asp:ListItem>
<asp:ListItem>TAX</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="cashbanktaxpct" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w755" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label23" runat="server" Text="Tax Amount" __designer:wfdid="w756"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbanktaxamt" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w757" Enabled="False"></asp:TextBox></TD><TD style="WIDTH: 59px" class="Label" align=left><asp:Label id="Label24" runat="server" Text="Other Tax Amt." Width="96px" __designer:wfdid="w758"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbankothertaxamt" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w759" AutoPostBack="True" MaxLength="18"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label26" runat="server" Text="Grand Total" __designer:wfdid="w760"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbankgrandtotal" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w761" Enabled="False"></asp:TextBox></TD><TD style="WIDTH: 59px" id="TD6" class="Label" align=left runat="server" Visible="true"><asp:Label id="Label12" runat="server" Text="PIC" __designer:wfdid="w762"></asp:Label></TD><TD id="TD4" class="Label" align=center runat="server" Visible="true">:</TD><TD id="TD5" class="Label" align=left runat="server" Visible="true"><asp:DropDownList id="personoid" runat="server" CssClass="inpText" Width="205px" __designer:wfdid="w763"><asp:ListItem>0</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label8" runat="server" Text="Header Note" __designer:wfdid="w764"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbanknote" runat="server" CssClass="inpText" Width="200px" __designer:wfdid="w765" MaxLength="100"></asp:TextBox></TD><TD style="WIDTH: 59px" class="Label" align=left><asp:Label id="Label9" runat="server" Text="Status" __designer:wfdid="w766"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbankstatus" runat="server" CssClass="inpTextDisabled" Width="80px" __designer:wfdid="w767" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><ajaxToolkit:CalendarExtender id="ceDate" runat="server" __designer:wfdid="w768" TargetControlID="cashbankdate" PopupButtonID="imbDate" Format="MM/dd/yyyy">
                </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeDate" runat="server" __designer:wfdid="w769" TargetControlID="cashbankdate" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999">
                </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="ceDueDate" runat="server" __designer:wfdid="w770" TargetControlID="cashbankduedate" PopupButtonID="imbDueDate" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeDueDate" runat="server" __designer:wfdid="w771" TargetControlID="cashbankduedate" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="ceDTG" runat="server" __designer:wfdid="w772" TargetControlID="cashbanktakegiro" PopupButtonID="imbDTG" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeDTG" runat="server" __designer:wfdid="w773" TargetControlID="cashbanktakegiro" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbOtherTaxAmt" runat="server" __designer:wfdid="w774" TargetControlID="cashbankothertaxamt" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6 rowSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><asp:Label id="Label18" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Cash/Bank Expense Detail" Font-Underline="False" __designer:wfdid="w775"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="i_u2" runat="server" Text="New Detail" __designer:wfdid="w776" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="cashbankglseq" runat="server" __designer:wfdid="w777" Visible="False">1</asp:Label> <asp:Label id="cashbankgloid" runat="server" __designer:wfdid="w778" Visible="False"></asp:Label><asp:Label id="paycommoid" runat="server" __designer:wfdid="w29" Visible="False"></asp:Label><asp:Label id="reftype" runat="server" __designer:wfdid="w30" Visible="False"></asp:Label></TD><TD style="WIDTH: 59px" class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="acctgoid2" runat="server" __designer:wfdid="w779" Visible="False"></asp:Label> <asp:Label id="acctgcode" runat="server" __designer:wfdid="w780" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=4><asp:RadioButtonList id="RblExpenseType" runat="server" __designer:wfdid="w10" RepeatDirection="Horizontal"><asp:ListItem Selected="True">BIAYA</asp:ListItem>
<asp:ListItem Value="QL_trnpaycommmst">KOMISI SALES</asp:ListItem>
<asp:ListItem Value="QL_trnpayspvmst">KOMISI SPV</asp:ListItem>
<asp:ListItem Value="QL_r_salary">PENGGAJIAN</asp:ListItem>
</asp:RadioButtonList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label11" runat="server" Text="Expense Account" __designer:wfdid="w781"></asp:Label> <asp:Label id="Label21" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w782"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="acctgdesc" runat="server" CssClass="inpTextDisabled" Width="160px" __designer:wfdid="w783" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchCOA" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w784"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearCOA" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w785"></asp:ImageButton></TD><TD style="WIDTH: 59px" class="Label" align=left><asp:Label id="Label4" runat="server" Text="Amount" __designer:wfdid="w786"></asp:Label> <asp:Label id="Label20" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w787"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbankglamt" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w788" MaxLength="16"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label28" runat="server" Text="Ref. Trans" __designer:wfdid="w786"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="paycommno" runat="server" CssClass="inpTextDisabled" Width="160px" __designer:wfdid="w11" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchComm" onclick="btnSearchComm_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w12"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseComm" onclick="btnEraseComm_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w13"></asp:ImageButton></TD><TD style="WIDTH: 59px" class="Label" align=left><asp:Label id="Label16" runat="server" Text="Ref. Amount" __designer:wfdid="w786"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="paycommamt" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w14" Enabled="False" MaxLength="16"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label13" runat="server" Text="Detail Note" __designer:wfdid="w789"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbankglnote" runat="server" CssClass="inpText" Width="200px" __designer:wfdid="w790" MaxLength="100"></asp:TextBox></TD><TD style="WIDTH: 59px" id="TD1" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label27" runat="server" Text="Division" __designer:wfdid="w791" Visible="False"></asp:Label></TD><TD id="TD3" class="Label" align=center runat="server" Visible="false"></TD><TD id="TD2" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="groupoid" runat="server" CssClass="inpText" Width="205px" __designer:wfdid="w792" Visible="False"><asp:ListItem>0</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="ftbAmount" runat="server" __designer:wfdid="w793" TargetControlID="cashbankglamt" ValidChars="1234567890,.-">
                </ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w794"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w795"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:GridView id="gvDtl" runat="server" ForeColor="#333333" Width="100%" __designer:wfdid="w796" DataKeyNames="cashbankglseq" PageSize="5" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#EFF3FB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="acctgcode" HeaderText="Account No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Account Desc.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankglamt" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="refno" HeaderText="Ref. Trans">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="refamt" HeaderText="Ref. Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankglnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label7" runat="server" ForeColor="Red" Text="No detail data !!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=6></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w797"></asp:Label>&nbsp;On <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w798"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update&nbsp;By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w799"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w800"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w801" AlternateText="Save"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w802" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w803" AlternateText="Delete"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnposting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w804"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnShowCOA" runat="server" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsMiddle" __designer:wfdid="w805" AlternateText="Show COA"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:dtid="2251799813685272" __designer:wfdid="w2" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate __designer:dtid="2251799813685273">
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV style="WIDTH: 87%; HEIGHT: 1px" id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w3"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress>&nbsp;</DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                            &nbsp;
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Cash/Bank Expense :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upListSupp" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlListSupp" runat="server" CssClass="modalBox" Width="650px" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSupp" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Supplier/Customer"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListSupp" runat="server" Width="100%" DefaultButton="btnFindListSupp">
                                Filter :
                                <asp:DropDownList ID="FilterDDLListSupp" runat="server" CssClass="inpText" Width="100px">
                                    <asp:ListItem Value="suppcode">Code</asp:ListItem>
                                    <asp:ListItem Value="suppname">Name</asp:ListItem>
                                    <asp:ListItem Value="suppaddr">Address</asp:ListItem>
                                </asp:DropDownList>
                                <asp:TextBox ID="FilterTextListSupp" runat="server" CssClass="inpText" Width="150px"></asp:TextBox>
                                <asp:ImageButton ID="btnFindListSupp" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/btnfind.bmp" />
                                <asp:ImageButton ID="btnAllListSupp" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewall.png" /></asp:Panel> </TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListSupp" runat="server" ForeColor="#333333" Width="99%" AllowPaging="True" GridLines="None" AutoGenerateColumns="False" CellPadding="4" PageSize="5" DataKeyNames="suppoid,suppname,supptaxable">
                                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True">
                                        <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:CommandField>
                                    <asp:BoundField DataField="suppcode" HeaderText="Code">
                                        <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="suppname" HeaderText="Name">
                                        <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="supptaxable" HeaderText="Taxable">
                                        <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="suppaddr" HeaderText="Address">
                                        <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                </Columns>
                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <PagerStyle CssClass="gvpopup" Font-Bold="True" ForeColor="White" HorizontalAlign="Right" />
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="White" />
                            </asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListSupp" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSupp" runat="server" TargetControlID="btnHideListSupp" PopupDragHandleControlID="lblListSupp" PopupControlID="pnlListSupp" Drag="True" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListSupp" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upListCOA" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListCOA" runat="server" CssClass="modalBox" Width="550px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListCOA" runat="server" Font-Size="Medium" Font-Bold="True" Font-Underline="False">List Of Expense Account</asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListCOA" runat="server" CssClass="Label" Width="100%" DefaultButton="btnFindListCOA">Filter : <asp:DropDownList id="FilterDDLListCOA" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="a.acctgcode">Account No.</asp:ListItem>
<asp:ListItem Value="a.acctgdesc">Account Desc.</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListCOA" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListCOA" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListCOA" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel></TD></TR>
    <tr>
        <td align="center" class="Label" colspan="3" valign="top">
            <asp:GridView id="gvListCOA" runat="server" ForeColor="#333333" Width="99%" DataKeyNames="acctgoid,acctgcode,acctgdesc" CellPadding="4" AutoGenerateColumns="False" GridLines="None" AllowPaging="True" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="acctgcode" HeaderText="Account No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Account Desc.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" ForeColor="White" CssClass="gvpopup" Font-Bold="True"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label14" runat="server" Text="No data found !!" CssClass="Important"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
</asp:GridView>
        </td>
    </tr>
    <TR><TD vAlign=top align=center colSpan=3><asp:LinkButton id="lkbCloseListCOA" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHideListCOA" runat="server" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="mpeListCOA" runat="server" TargetControlID="btnHideListCOA" PopupControlID="pnlListCOA" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListCOA" Drag="True"></ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upListDP" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlListDP" runat="server" CssClass="modalBox" Width="750px" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListDP" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Down Payment"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListDP" runat="server" Width="100%" DefaultButton="btnFindListDP">Filter : <asp:DropDownList id="FilterDDLListDP" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="dpapno">DP No.</asp:ListItem>
<asp:ListItem Value="acctgdesc">DP Account</asp:ListItem>
<asp:ListItem Value="dpapnote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListDP" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListDP" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListDP" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListDP" runat="server" ForeColor="#333333" Width="99%" AllowPaging="True" GridLines="None" AutoGenerateColumns="False" CellPadding="4" PageSize="5" DataKeyNames="dpapoid,dpapno,dpapacctgoid,dpapamt">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="dpapno" HeaderText="Transaction No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dpapdate" HeaderText="Trans. Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Account">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dpapnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListDP" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListDP" runat="server" TargetControlID="btnHideListDP" PopupDragHandleControlID="lblListDP" PopupControlID="pnlListDP" Drag="True" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListDP" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upCOAPosting" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlCOAPosting" runat="server" CssClass="modalBox" Visible="False"
                Width="600px">
                <table style="width: 100%">
                    <tr>
                        <td align="center" class="Label" colspan="3">
                            <asp:Label ID="lblCOAPosting" runat="server" Font-Bold="True" Font-Size="Medium"
                                Text="COA - Posting Rate :"></asp:Label>
                            <asp:DropDownList ID="DDLRateType" runat="server" AutoPostBack="True" CssClass="inpText"
                                Font-Bold="True" Font-Size="Medium" Width="125px">
                                <asp:ListItem Value="Default">Rate Default</asp:ListItem>
                                <asp:ListItem Value="IDR">Rate To IDR</asp:ListItem>
                                <asp:ListItem Value="USD">Rate To USD</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="center" class="Label" colspan="3" style="height: 5px">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="Label" colspan="3">
                            <asp:GridView ID="gvCOAPosting" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                ForeColor="#333333" GridLines="None" Width="99%">
                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                <Columns>
                                    <asp:BoundField DataField="acctgcode" HeaderText="Account No.">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="acctgdesc" HeaderText="Account Desc.">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="acctgdebet" HeaderText="Debet">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Right" Width="15%" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="acctgcredit" HeaderText="Credit">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Right" Width="15%" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                </Columns>
                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label13" runat="server" ForeColor="Red" Text="No Data Found"></asp:Label>
                                </EmptyDataTemplate>
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="White" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="Label" colspan="3" style="height: 5px">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="3">
                            <asp:LinkButton ID="lkbCloseCOAPosting" runat="server">[ CLOSE ]</asp:LinkButton></td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Button ID="btnHideCOAPosting" runat="server" Visible="False" />
            <ajaxToolkit:ModalPopupExtender ID="mpeCOAPosting" runat="server" BackgroundCssClass="modalBackground"
                Drag="True" PopupControlID="pnlCOAPosting" PopupDragHandleControlID="lblCOAPosting"
                TargetControlID="btnHideCOAPosting">
            </ajaxToolkit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="UpListComm" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListComm" runat="server" CssClass="modalBox" Width="750px" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListComm" runat="server" Font-Size="Medium" Font-Bold="True" Text="List Of Sales Commision"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListComm" runat="server" Width="100%" DefaultButton="btnFindListComm">Filter : <asp:DropDownList id="FilterDDLListComm" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="personname">Sales</asp:ListItem>
<asp:ListItem Value="paycommno">Ref No.</asp:ListItem>
<asp:ListItem Value="paycommnote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListComm" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListComm" onclick="btnFindListComm_Click" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListComm" onclick="btnAllListComm_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListComm" runat="server" ForeColor="#333333" Width="99%" OnSelectedIndexChanged="gvListComm_SelectedIndexChanged" GridLines="None" AutoGenerateColumns="False" CellPadding="4" PageSize="5" DataKeyNames="paycommoid,paycommno,paycommdate,paycommamt,paycommnote,reftype" AllowPaging="True" OnPageIndexChanging="gvListComm_PageIndexChanging" OnRowDataBound="gvListComm_RowDataBound">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="personname" HeaderText="Ref Name.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="paycommno" HeaderText="Ref No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="paycommdate" HeaderText="Ref. Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="paycommamt" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="paycommnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListCom" onclick="lkbCloseListCom_Click" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListComm" runat="server" TargetControlID="btnHideListComm" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlListComm" PopupDragHandleControlID="lblListComm"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListComm" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

