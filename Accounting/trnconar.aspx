<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnconar.aspx.vb" Inherits="Accounting_trnConAR" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="12pt"
                    ForeColor="Maroon" Text=".: A/R Initial Balance"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 15px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" 
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                                        <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                            <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" __designer:wfdid="w20" DefaultButton="btnSearch"><TABLE><TBODY><TR><TD align=left><asp:Label id="Label1" runat="server" Text="Filter " __designer:wfdid="w21"></asp:Label></TD><TD align=left>: </TD><TD align=left colSpan=2><asp:DropDownList id="ddlFilter" runat="server" Width="100px" __designer:wfdid="w22" CssClass="inpText" AutoPostBack="True" OnSelectedIndexChanged="ddlFilter_SelectedIndexChanged"><asp:ListItem Value="s.custname">Customer</asp:ListItem>
<asp:ListItem Value="bm.trnjualno">Invoice No</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterConap" runat="server" Width="123px" __designer:wfdid="w23" CssClass="inpText"></asp:TextBox></TD><TD align=left>&nbsp;&nbsp; </TD></TR><TR><TD align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status " Width="65px" __designer:wfdid="w26"></asp:CheckBox></TD><TD align=left>: </TD><TD align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" Width="100px" __designer:wfdid="w21" CssClass="inpText"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="IN PROCESS">In Process</asp:ListItem>
<asp:ListItem Value="POST">Post</asp:ListItem>
</asp:DropDownList> <asp:Label id="multiplepost" runat="server" Text="false" __designer:wfdid="w32" Visible="False"></asp:Label></TD><TD align=left></TD></TR><TR><TD align=left></TD><TD align=left>&nbsp;</TD><TD align=left colSpan=2><asp:ImageButton id="ImageButton1" onclick="imgFind_Click" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w19"></asp:ImageButton> <asp:ImageButton id="imgViewAll" onclick="imgViewAll_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w20"></asp:ImageButton> <asp:ImageButton id="ibselectall" onclick="ibselectall_Click" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" __designer:wfdid="w28" Visible="False" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton id="ibuncheckall" onclick="ibuncheckall_Click" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" __designer:wfdid="w29" Visible="False" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton id="btnPost" onclick="btnPost_Click" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w24" Visible="False" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton id="btnExcel" onclick="btnExcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w25" Visible="False"></asp:ImageButton></TD><TD align=left></TD></TR></TBODY></TABLE><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="WIDTH: 324px" align=left><asp:GridView id="gvTrnConap" runat="server" Width="100%" __designer:wfdid="w15" OnSelectedIndexChanged="gvTrnConap_SelectedIndexChanged" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="trnjualmstoid,cmpcode" AllowPaging="True" PageSize="8" GridLines="None">
<PagerSettings Mode="NumericFirstLast" PageButtonCount="15"></PagerSettings>

<RowStyle CssClass="gvrow"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectText="Show" ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnjualno" HeaderText="Invoice No.">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="outlet" HeaderText="Outlet" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="typejual" HeaderText="Type" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualdate" HeaderText="Invoice Date">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="payduedate" HeaderText="Due Date" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="amttrans" HeaderText="Invoice Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualstatus" HeaderText="Status">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:CheckBox id="cbGVMst" runat="server" __designer:wfdid="w12" Checked='<%# eval("checkvalue") %>'></asp:CheckBox><asp:Label id="lblOidGVMst" runat="server" Text='<%# eval("trnjualmstoid") %>' __designer:wfdid="w11" Visible="False"></asp:Label> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
</Columns>

<FooterStyle Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label15" runat="server" ForeColor="Red" Text="No data found!!" __designer:wfdid="w6"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#124E81" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w34"></asp:Label></TD></TR></TBODY></TABLE>&nbsp; </asp:Panel> <asp:Button id="btnAll" onclick="btnAll_Click" runat="server" Font-Bold="True" Text="VIEW ALL" Width="75px" __designer:wfdid="w35" Visible="False" CssClass="gray"></asp:Button> <asp:Button id="btnSearch" onclick="btnSearch_Click" runat="server" Font-Bold="True" Text="FIND" Width="75px" __designer:wfdid="w36" Visible="False" CssClass="orange"></asp:Button> 
</contenttemplate>
                                            <triggers>
<asp:PostBackTrigger ControlID="btnExcel"></asp:PostBackTrigger>
</triggers>
                                        </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            &nbsp;<img align="absMiddle" alt="" height="16" src="../Images/List.gif" />
                            <strong><span style="font-size: 9pt">
                                :: List Of A/R Initial Balance</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<DIV style="WIDTH: 100%"><TABLE width="100%"><TBODY><TR><TD style="WIDTH: 131px" align=left><asp:Label id="suppoid" runat="server" __designer:wfdid="w128" Visible="False">suppoid</asp:Label></TD><TD align=center></TD><TD align=left><asp:TextBox id="conapoid" runat="server" Width="10px" __designer:wfdid="w129" Visible="False" CssClass="inpText" MaxLength="10" Enabled="False"></asp:TextBox> <asp:TextBox id="trnbelimstoid" runat="server" Width="10px" __designer:wfdid="w130" Visible="False" CssClass="inpText" MaxLength="10" Enabled="False"></asp:TextBox> <asp:TextBox id="paymentoid" runat="server" Width="10px" __designer:wfdid="w131" Visible="False" CssClass="inpText" MaxLength="10" Enabled="False"></asp:TextBox> <asp:TextBox id="cashbankoid" runat="server" Width="10px" __designer:wfdid="w132" Visible="False" CssClass="inpText" MaxLength="10" Enabled="False"></asp:TextBox> <asp:TextBox id="trnglmstoid" runat="server" Width="10px" __designer:wfdid="w133" Visible="False" CssClass="inpText" MaxLength="10" Enabled="False"></asp:TextBox> <asp:TextBox id="trngldtloid" runat="server" Width="10px" __designer:wfdid="w134" Visible="False" CssClass="inpText" MaxLength="10" Enabled="False"></asp:TextBox> <asp:Label id="LblFakturPajak" runat="server" __designer:wfdid="w135" Visible="False"></asp:Label> <ajaxToolkit:MaskedEditExtender id="mee6" runat="server" __designer:wfdid="w136" TargetControlID="paymentdate" MaskType="Date" Mask="99/99/9999" CultureName="id-ID" ClearMaskOnLostFocus="true" UserDateFormat="DayMonthYear"></ajaxToolkit:MaskedEditExtender> <asp:TextBox id="rate" runat="server" Width="103px" __designer:wfdid="w143" Visible="False" CssClass="inpText" MaxLength="12" Enabled="true" OnTextChanged="amtbeli_TextChanged">1</asp:TextBox> <asp:DropDownList id="DDLOutlet" runat="server" __designer:wfdid="w142" Visible="False" CssClass="inpTextDisabled" Enabled="False"></asp:DropDownList>&nbsp; <asp:TextBox id="acctgCode" runat="server" Width="325px" __designer:wfdid="w145" Visible="False" CssClass="inpTextDisabled" ReadOnly="True"></asp:TextBox></TD><TD align=left><ajaxToolkit:CalendarExtender id="ce1" runat="server" __designer:wfdid="w140" TargetControlID="invoicedate" Format="MM/dd/yyyy" PopupButtonID="imbInvoiceDate"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="ce3" runat="server" __designer:wfdid="w141" TargetControlID="paymentdate" Format="MM/dd/yyyy" PopupButtonID="imbPaymentDate"></ajaxToolkit:CalendarExtender> <asp:DropDownList id="DDLType" runat="server" Width="96px" __designer:wfdid="w165" Visible="False" CssClass="inpText"><asp:ListItem Value="Raw">Raw Material</asp:ListItem>
<asp:ListItem Value="Gen">Gen Material</asp:ListItem>
<asp:ListItem>Finish Good</asp:ListItem>
<asp:ListItem>WIP</asp:ListItem>
</asp:DropDownList></TD><TD align=center></TD><TD align=left>&nbsp;<asp:Label id="namanpwp" runat="server" __designer:wfdid="w138" Visible="False"></asp:Label> <asp:Label id="alamatnpwp" runat="server" __designer:wfdid="w139" Visible="False"></asp:Label> <asp:Label id="Label14" runat="server" ForeColor="Red" Text="(*)" __designer:wfdid="w144" Visible="False"></asp:Label> <asp:ImageButton id="btnSearcAcctg" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Width="16px" __designer:wfdid="w146" Visible="False" Height="16px"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 131px" align=left>Invoice No&nbsp;<asp:Label id="Label18" runat="server" ForeColor="Red" Font-Size="X-Small" Text="*" __designer:wfdid="w151"></asp:Label></TD><TD align=center>:</TD><TD style="FONT-SIZE: 8pt; COLOR: #000099" align=left><asp:TextBox id="trnbelino" runat="server" Width="150px" __designer:wfdid="w152" CssClass="inpText" MaxLength="20" Enabled="true" AutoPostBack="True"></asp:TextBox></TD><TD align=left></TD><TD align=center></TD><TD align=left></TD></TR><TR><TD style="WIDTH: 131px" align=left><asp:Label id="Label4" runat="server" Text="Invoice Date" __designer:wfdid="w155"></asp:Label> <asp:Label id="Label10" runat="server" ForeColor="Red" Font-Size="X-Small" Text="*" __designer:wfdid="w156"></asp:Label></TD><TD align=center>:</TD><TD style="FONT-SIZE: 8pt; COLOR: #000099" align=left><asp:TextBox id="invoicedate" runat="server" Width="75px" __designer:wfdid="w157" CssClass="inpTextDisabled" MaxLength="10" Enabled="true" AutoPostBack="True"></asp:TextBox> <asp:ImageButton id="imbInvoiceDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w158" Visible="False"></asp:ImageButton> <asp:Label id="Label2" runat="server" Text="(MM/dd/yyyy)" Width="88px" __designer:wfdid="w159" CssClass="Important"></asp:Label></TD><TD align=left><asp:Label id="Label5" runat="server" Text="Due Date" __designer:wfdid="w160"></asp:Label>&nbsp;<asp:Label id="Label11" runat="server" ForeColor="Red" Font-Size="X-Small" Text="*" __designer:wfdid="w161"></asp:Label></TD><TD align=center>:</TD><TD align=left><asp:TextBox id="payduedate" runat="server" Width="75px" __designer:wfdid="w162" CssClass="inpTextDisabled" MaxLength="10" Enabled="False"></asp:TextBox> <asp:Label id="Label6" runat="server" Text="(MM/dd/yyyy)" Width="88px" __designer:wfdid="w163" CssClass="Important"></asp:Label></TD></TR><TR><TD style="WIDTH: 131px" align=left><asp:Label id="npwp" runat="server" __designer:wfdid="w137">Customer</asp:Label>&nbsp;<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="*" __designer:wfdid="w147"></asp:Label></TD><TD align=center>:</TD><TD style="FONT-SIZE: 8pt; COLOR: #000099" align=left><asp:TextBox id="suppcode" runat="server" Width="150px" __designer:wfdid="w148" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnCariSupp" onclick="btnCariSupp_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Width="16px" __designer:wfdid="w149"></asp:ImageButton> <asp:ImageButton id="ImbEraseSupp" onclick="ImbEraseSupp_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Width="16px" __designer:wfdid="w150"></asp:ImageButton></TD><TD align=left><asp:Label id="Label12" runat="server" Text="A/R Account" Width="96px" __designer:wfdid="w153" ToolTip="VAR_AR"></asp:Label></TD><TD align=center>:</TD><TD align=left><asp:DropDownList id="DDLacctgCode" runat="server" __designer:wfdid="w154" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 131px" align=left><asp:Label id="Label22" runat="server" Text="Payment Term" Width="112px" __designer:wfdid="w166"></asp:Label></TD><TD align=center>:</TD><TD style="FONT-SIZE: 8pt; COLOR: #000099" align=left><asp:DropDownList id="trnpaytype" runat="server" Width="105px" __designer:wfdid="w167" CssClass="inpText" AutoPostBack="True" OnSelectedIndexChanged="trnpaytype_SelectedIndexChanged"></asp:DropDownList></TD><TD align=left><asp:Label id="Label3" runat="server" Text="Total Inv. (DPP)" __designer:wfdid="w171"></asp:Label> <asp:Label id="Label25" runat="server" ForeColor="Red" Font-Size="X-Small" Text="*" __designer:wfdid="w2"></asp:Label></TD><TD align=center>:</TD><TD align=left><asp:TextBox id="dpp" runat="server" Width="110px" __designer:wfdid="w172" CssClass="inpText" MaxLength="12" Enabled="true" OnTextChanged="amtbeli_TextChanged" AutoPostBack="True">0</asp:TextBox></TD></TR><TR><TD style="WIDTH: 131px" align=left><asp:Label id="Label24" runat="server" Text="Tax (%)" __designer:wfdid="w168"></asp:Label></TD><TD align=center>:</TD><TD style="FONT-SIZE: 8pt; COLOR: #000099" align=left><asp:TextBox id="trntaxpct" runat="server" Width="50px" __designer:wfdid="w169" CssClass="inpText" MaxLength="2" Enabled="true" OnTextChanged="trntaxpct_TextChanged" AutoPostBack="True">0</asp:TextBox></TD><TD align=left><asp:Label id="Label19" runat="server" Text="Tax Amt" Width="96px" __designer:wfdid="w164"></asp:Label></TD><TD align=center>:</TD><TD align=left><asp:TextBox id="amttax" runat="server" Width="110px" __designer:wfdid="w170" CssClass="inpTextDisabled" MaxLength="12" Enabled="true" ReadOnly="True">0</asp:TextBox></TD></TR><TR><TD style="WIDTH: 131px" align=left><asp:Label id="Label27" runat="server" Text="Sales" __designer:wfdid="w179"></asp:Label> <asp:Label id="Label9" runat="server" ForeColor="Red" Font-Size="X-Small" Text="*" __designer:wfdid="w1"></asp:Label></TD><TD align=center>:</TD><TD style="FONT-SIZE: 8pt; COLOR: #000099" align=left><asp:DropDownList id="salescode" runat="server" Width="150px" __designer:wfdid="w18" CssClass="inpText" AutoPostBack="True"></asp:DropDownList></TD><TD align=left><asp:Label id="Label23" runat="server" Text="Total Inv. (Nett)" Width="120px" __designer:wfdid="w177"></asp:Label></TD><TD align=center>:</TD><TD align=left><asp:TextBox id="amtbeli" runat="server" Width="110px" __designer:wfdid="w178" CssClass="inpTextDisabled" MaxLength="12" Enabled="true" OnTextChanged="amtbeli_TextChanged">0</asp:TextBox></TD></TR><TR><TD style="WIDTH: 131px" align=left><asp:Label id="Label7" runat="server" Text="Note" Width="80px" __designer:wfdid="w173"></asp:Label></TD><TD align=center>:</TD><TD style="FONT-SIZE: 8pt; COLOR: #000099" align=left><asp:TextBox id="trnapnote" runat="server" Width="250px" __designer:wfdid="w174" CssClass="inpText" MaxLength="100" Enabled="true"></asp:TextBox></TD><TD id="TD2" align=left Visible="false"><asp:Label id="Label21" runat="server" Text="Status" Width="120px" __designer:wfdid="w175"></asp:Label></TD><TD align=center Visible="false">:</TD><TD style="FONT-SIZE: 8pt; COLOR: #000099" id="TD1" align=left Visible="false"><asp:Label id="lblPosting" runat="server" __designer:wfdid="w176" CssClass="Important"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 131px" align=left><asp:TextBox id="amtbayar" runat="server" Width="103px" __designer:wfdid="w180" Visible="False" CssClass="inpText" MaxLength="12" Enabled="true" OnTextChanged="amtbeli_TextChanged" AutoPostBack="True">0</asp:TextBox> <asp:TextBox id="paymentdate" runat="server" Width="100px" __designer:wfdid="w188" Visible="False" CssClass="inpText" MaxLength="10" Enabled="true"></asp:TextBox> <asp:TextBox id="payrefno" runat="server" Width="150px" __designer:wfdid="w192" Visible="False" CssClass="inpText" MaxLength="15" Enabled="true"></asp:TextBox> <asp:Label id="Label16" runat="server" Text="Payment Date" Width="88px" __designer:wfdid="w187" Visible="False"></asp:Label> <asp:Label id="paymentacctgoid" runat="server" Font-Size="X-Small" __designer:wfdid="w189" Visible="False"></asp:Label> <asp:Label id="Label17" runat="server" Text="Payment No" Width="94px" __designer:wfdid="w191" Visible="False"></asp:Label> <asp:Label id="Label26" runat="server" Text="A/P Payment Account" Width="136px" __designer:wfdid="w183" Visible="False"></asp:Label> <asp:Label id="Label20" runat="server" Text="Total A/R" Width="120px" __designer:wfdid="w181" Visible="False"></asp:Label></TD><TD align=center></TD><TD align=left colSpan=4><asp:GridView id="gvAcctg" runat="server" Width="100%" __designer:wfdid="w3" OnSelectedIndexChanged="gvAcctg_SelectedIndexChanged1" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="acctgoid,acctgcode,acctgdesc" AllowPaging="True" PageSize="1" GridLines="None" OnPageIndexChanging="gvAcctg_PageIndexChanging">
<RowStyle CssClass="gvrow"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="40px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="40px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="acctgoid" HeaderText="SuppOid" Visible="False">
<HeaderStyle CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgcode" HeaderText="Code">
<HeaderStyle CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Akun">
<HeaderStyle CssClass="gvhdr" Width="300px"></HeaderStyle>

<ItemStyle Width="300px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvfooter" Font-Bold="True"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label13" runat="server" ForeColor="Red" Text="No data found!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#124E81" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="acctgoid" runat="server" Font-Size="X-Small" __designer:wfdid="w4" Visible="False"></asp:Label> <asp:Label id="postdate" runat="server" Font-Size="X-Small" Text="1/1/1900" __designer:wfdid="w5" Visible="False"></asp:Label> <asp:TextBox id="amtbelinetto" runat="server" Width="50px" __designer:wfdid="w6" Visible="False" CssClass="inpTextDisabled" MaxLength="30" Enabled="true" ReadOnly="True">0.00</asp:TextBox> <asp:TextBox id="currate" runat="server" Width="100px" __designer:wfdid="w7" Visible="False" CssClass="inpTextDisabled" MaxLength="30" Enabled="False">1</asp:TextBox> <asp:Label id="purchAcctgoid" runat="server" Font-Size="X-Small" Width="99px" __designer:wfdid="w8" Visible="False"></asp:Label> <asp:TextBox id="purchAcctg" runat="server" Width="50px" __designer:wfdid="w9" Visible="False" CssClass="inpTextDisabled" ReadOnly="True"></asp:TextBox> <asp:ImageButton id="imbPurch" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Width="16px" __designer:wfdid="w10" Visible="False" Height="16px"></asp:ImageButton> <asp:Label id="statusRePost" runat="server" Text="POST" __designer:wfdid="w11" Visible="False"></asp:Label> <asp:DropDownList id="curroid" runat="server" Width="180px" __designer:wfdid="w12" Visible="False" CssClass="inpText" Enabled="False" AutoPostBack="True" OnSelectedIndexChanged="curroid_SelectedIndexChanged"></asp:DropDownList> <asp:ImageButton id="imbPaymentDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w13" Visible="False"></asp:ImageButton> <asp:TextBox id="payacctg" runat="server" Width="250px" __designer:wfdid="w14" Visible="False" CssClass="inpText" MaxLength="99"></asp:TextBox> <asp:ImageButton id="imbPayAcctg" onclick="imbPayAcctg_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Width="16px" __designer:wfdid="w15" Visible="False"></asp:ImageButton> <asp:ImageButton id="ImbErasePayAcc" onclick="ImbErasePayAcc_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Width="16px" __designer:wfdid="w16" Visible="False"></asp:ImageButton> <asp:TextBox id="totalAP" runat="server" Width="123px" __designer:wfdid="w17" Visible="False" CssClass="inpTextDisabled" MaxLength="12" Enabled="true" OnTextChanged="amtbeli_TextChanged" ReadOnly="True">0</asp:TextBox></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="FONT-SIZE: x-small; COLOR: #585858" align=left><BR />Last Update On <asp:Label id="UpdTime" runat="server" ForeColor="#585858" Font-Size="X-Small" Font-Bold="True" Text='<%# Eval("UPDTIME", "{0:G}") %>' __designer:wfdid="w203"></asp:Label> by <asp:Label id="UpdUser" runat="server" ForeColor="#585858" Font-Size="X-Small" Font-Bold="True" Text='<%# Eval("UPDUSER", "{0}") %>' __designer:wfdid="w204"></asp:Label></TD><TD style="FONT-SIZE: x-small; COLOR: #585858" align=left></TD><TD style="FONT-SIZE: x-small; COLOR: #585858" align=left rowSpan=2></TD></TR><TR><TD align=left>&nbsp;&nbsp;&nbsp;&nbsp;<BR /><asp:ImageButton id="btnSave1" onclick="btnSave1_Click" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w205" AlternateText="Save"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancel1" onclick="btnCancel1_Click1" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w206" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete1" onclick="btnDelete1_Click1" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w207" AlternateText="Delete"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPost1" onclick="btnPost1_Click" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w208"></asp:ImageButton> <asp:ImageButton id="ibRePost" runat="server" ImageUrl="~/Images/repost.png" ImageAlign="AbsMiddle" __designer:wfdid="w209" Visible="False"></asp:ImageButton></TD><TD align=left><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w210" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<asp:Image id="Image1" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w211"></asp:Image> Please Wait ..... 
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE><ajaxToolkit:MaskedEditExtender id="mee3" runat="server" __designer:wfdid="w212" TargetControlID="invoicedate" MaskType="Date" Mask="99/99/9999" CultureName="id-ID" ClearMaskOnLostFocus="true" UserDateFormat="DayMonthYear"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="mee1" runat="server" __designer:wfdid="w213" TargetControlID="payduedate" MaskType="Date" Mask="99/99/9999" CultureName="id-ID" UserDateFormat="DayMonthYear"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender1" runat="server" __designer:wfdid="w214" TargetControlID="dpp" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender2" runat="server" __designer:wfdid="w215" TargetControlID="trntaxpct" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender3" runat="server" __designer:wfdid="w216" TargetControlID="amtbayar" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender>&nbsp;&nbsp; </DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/Form.gif" />&nbsp; <strong><span>
                                Form A/R
                                Initial Balance :.</span></strong>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <table style="width: 100%">
        <tr>
            <td style="width: 100px">
                &nbsp;</td>
        </tr>
    </table>
    <asp:UpdatePanel id="upListCust" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListCust" runat="server" Width="650px" CssClass="modalBox" Visible="False"><TABLE width="100%"><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListCust" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Customer"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListCust" runat="server" Width="100%" DefaultButton="btnFindListCust">Filter : <asp:DropDownList id="FilterDDLListCust" runat="server" Width="100px" CssClass="inpText">
                                        <asp:ListItem Value="custcode">Code</asp:ListItem>
                                        <asp:ListItem Value="custname">Name</asp:ListItem>
                                        <asp:ListItem Value="custaddr">Address</asp:ListItem>
                                    </asp:DropDownList> <asp:TextBox id="FilterTextListCust" runat="server" Width="150px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListCust" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListCust" onclick="btnAllListCust_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListCust" runat="server" ForeColor="#333333" Width="98%" OnSelectedIndexChanged="gvListCust_SelectedIndexChanged" GridLines="None" PageSize="5" AllowPaging="True" DataKeyNames="custoid,custname" CellPadding="4" AutoGenerateColumns="False" OnPageIndexChanging="gvListCust_PageIndexChanging">
                                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="True">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:CommandField>
                                        <asp:BoundField DataField="custcode" HeaderText="Code">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="custname" HeaderText="Name">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="custaddr" HeaderText="Address">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" Width="40%" />
                                            <ItemStyle HorizontalAlign="Left" Width="40%" />
                                        </asp:BoundField>
                                    </Columns>
                                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle CssClass="gvpopup" Font-Bold="True" ForeColor="White" HorizontalAlign="Right" />
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListCust" onclick="lkbCloseListCust_Click" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListCust" runat="server" PopupDragHandleControlID="lblListCust" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlListCust" TargetControlID="btnHideListCust">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListCust" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="UpdatePanel6" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPosting2" runat="server" Width="760px" Visible="False" CssClass="modalBox" BorderWidth="2px" BorderStyle="Solid"><TABLE style="WIDTH: 560px"><TBODY><TR><TD style="HEIGHT: 25px" align=center colSpan=3><asp:Label id="lblPosting2" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting"></asp:Label></TD></TR><TR><TD colSpan=3><asp:GridView id="gvakun" runat="server" Width="750px" AutoGenerateColumns="False"><Columns>
<asp:BoundField DataField="acctgcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Account">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kredit" HeaderText="Credit">
<HeaderStyle HorizontalAlign="Right"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCancel2" onclick="lkbCancel2_Click" runat="server" Font-Size="Small" Font-Bold="True">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePosting2" runat="server" TargetControlID="btnHidePosting2" PopupControlID="pnlPosting2" BackgroundCssClass="modalBackground" Drag="True" PopupDragHandleControlID="lblPosting2"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting2" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" onclick="imbOKPopUpMsg_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" PopupDragHandleControlID="lblCaption" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" TargetControlID="bePopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

