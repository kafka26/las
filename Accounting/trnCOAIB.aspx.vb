Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class Accounting_COAInitialBalance
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Dim xCmd As New SqlCommand("", conn)
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Functions"
    Private Function IsFilterValid()
        Dim sError As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        Dim sErr As String = ""
        If gldate.Text = "" Then
            sError &= "- Please fill INIT DATE field!<BR>"
        Else
            If Not IsValidDate(gldate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- INIT DATE is invalid! " & sErr & "<BR>"
            End If
        End If
        If DDLCurrency.SelectedValue = "" Then
            sError &= "- Please select CURRENCY field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetTextBoxValue(ByVal nRowNum As Integer, ByVal nCol As Integer) As String
        Dim sReturn As String = ""
        Dim row As System.Web.UI.WebControls.GridViewRow = gvTblDtl.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                End If
            Next
        End If
        Return sReturn
    End Function

    Private Function GetDropDownListValue(ByVal nRowNum As Integer, ByVal nCol As Integer) As String
        Dim sReturn As String = ""
        Dim row As System.Web.UI.WebControls.GridViewRow = gvTblDtl.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
                End If
            Next
        End If
        Return sReturn
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitAllDDL()
        'Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)
        'Fill DDL Currency
        sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        FillDDL(DDLCurrency, sSql)
    End Sub

    Private Sub UpdateDetailData()
        If Not Session("TblDtl") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDtl")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                For C1 As Integer = 0 To gvTblDtl.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvTblDtl.Rows(C1)
                    dtView.RowFilter = "seq=" & row.Cells(0).Text
                    If dtView.Count > 0 Then
                        dtView(0)("acctgdbcr") = GetDropDownListValue(C1, 3)
                        dtView(0)("amount") = ToDouble(GetTextBoxValue(C1, 4))
                    End If
                    dtView.RowFilter = ""
                Next
                Session("TblDtl") = dtView.ToTable
            End If
        End If
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Accounting\trnCOAIB.aspx")
        End If
        If checkPagePermission("~\Accounting\trnCOAIB.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        upduser.Text = Session("UserID") : updtime.Text = GetServerTime().ToString
        Page.Title = CompnyName & " - COA Initial Balance"
        btnPost.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
			InitAllDDL()
			Dim CUTOFFDATE As Date
			sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup ='CUTOFDATE' AND cmpcode='" & Session("CompnyCode") & "'"
			If Not IsDate(CDate(GetStrData(sSql))) Then
				showMessage("Invalid Cut Off Date setup on General group 'CUTOFDATE' !", 2)
				Exit Sub
			Else
				CUTOFFDATE = CDate(GetStrData(sSql))
			End If
			gldate.Text = Format(CUTOFFDATE.AddDays(-1), "MM/dd/yyyy")
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnShowData_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnShowData.Click
        If IsFilterValid() Then
			Dim sPeriod As String = GetDateToPeriodAcctg(CDate(gldate.Text))
			sSql = "SELECT 0 seq, acctgoid, acctgcode, acctgdesc, acctgdbcr, 0.0 amount FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgoid NOT IN (SELECT DISTINCT acctggrp3 FROM QL_mstacctg WHERE acctggrp3 IS NOT NULL AND cmpcode='" & CompnyCode & "') AND acctgoid NOT IN (SELECT crd.acctgoid FROM QL_crdgl crd WHERE crd.cmpcode='" & CompnyCode & "' AND crd.acctgoid=acctgoid AND (crd.crdgloid<=0 OR CONVERT(INT, crd.periodacctg) < " & CInt(sPeriod) & ")) AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' ORDER BY acctgcode"

			'sSql = "SELECT 0 seq, acctgoid, acctgcode, acctgdesc, acctgdbcr, 0.0 amount FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgoid NOT IN (SELECT DISTINCT acctggrp3 FROM QL_mstacctg WHERE acctggrp3 IS NOT NULL AND cmpcode='" & CompnyCode & "') AND acctgoid NOT IN (SELECT DISTINCT acctgoid FROM QL_trngldtl gld INNER JOIN QL_trnglmst glm ON glm.cmpcode=gld.cmpcode AND glm.glmstoid=gld.glmstoid WHERE gld.cmpcode='" & DDLBusUnit.SelectedValue & "' AND (gld.gldtloid<=0 OR CONVERT(INT, glm.periodacctg)<" & CInt(sPeriod) & ")) /*AND acctg_incexc='01'*/ AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' ORDER BY acctgcode"
            Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstacctg")
            For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                dtTbl.Rows.Item(C1)("seq") = C1 + 1
            Next
            Session("TblDtl") = dtTbl
            gvTblDtl.DataSource = Session("TblDtl")
            gvTblDtl.DataBind()
            DDLBusUnit.Enabled = False
            DDLBusUnit.CssClass = "inpTextDisabled"
            gldate.Enabled = False
            gldate.CssClass = "inpTextDisabled"
            btnDate.Visible = False
            DDLCurrency.Enabled = False
            DDLCurrency.CssClass = "inpTextDisabled"
        End If
    End Sub

    Protected Sub gvTblDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTblDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(4).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPost.Click
        UpdateDetailData()
        If Session("TblDtl") Is Nothing Then
            showMessage("Please view COA data first!", 2)
            Exit Sub
        End If
        Dim objTable As DataTable = Session("TblDtl")
        If objTable.Rows.Count <= 0 Then
            showMessage("Please view COA data first!", 2)
            Exit Sub
        End If
        Dim cRate As New ClassRate()
        cRate.SetRateValue(CInt(DDLCurrency.SelectedValue), gldate.Text)
        If cRate.GetRateDailyLastError <> "" Then
            showMessage(cRate.GetRateDailyLastError, 2)
            Exit Sub
        End If
        If cRate.GetRateMonthlyLastError <> "" Then
            showMessage(cRate.GetRateMonthlyLastError, 2)
            Exit Sub
		End If
		sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup ='CUTOFDATE' AND cmpcode='" & Session("CompnyCode") & "'"
		Dim CutofDate As Date = CDate(GetStrData(sSql))
		If CDate(gldate.Text) >= CutofDate Then
			showMessage("Tanggal asset Tidak boleh >= CutoffDate (" & CutofDate & ") !!", 2)
			Exit Sub
		End If
        Dim objView As DataView = objTable.DefaultView
        objView.RowFilter = "amount > 0"
        If objView.Count <= 0 Then
            objView.RowFilter = ""
            showMessage("Please fill Init Amount for some COA to be initiated!", 2)
            Exit Sub
        End If
        Dim periodacctg As String = GetDateToPeriodAcctg(CDate(gldate.Text))
        Dim glmstoid As Integer = CInt(GetStrData("SELECT ((CASE WHEN MIN(glmstoid) IS NULL THEN 0 WHEN MIN(glmstoid)>=1 THEN 0 ELSE MIN(glmstoid) END) - 1) AS Oid FROM QL_trnglmst"))
        Dim gldtloid As Integer = CInt(GetStrData("SELECT ((CASE WHEN MIN(gldtloid) IS NULL THEN 0 WHEN MIN(gldtloid)>=1 THEN 0 ELSE MIN(gldtloid) END) - 1) AS Oid FROM QL_trngldtl"))
        Dim crdgloid As Integer = CInt(GetStrData("SELECT ((CASE WHEN MIN(crdgloid) IS NULL THEN 0 WHEN MIN(crdgloid)>=1 THEN 0 ELSE MIN(crdgloid) END) - 1) AS Oid FROM QL_crdgl"))
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & glmstoid & ", '" & gldate.Text & "', '" & periodacctg & "', 'COA Initial Balance', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            For C1 As Integer = 0 To objView.Count - 1
                sSql = "INSERT into QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & C1 + 1 & ", " & glmstoid & ", " & objView(C1).Item("acctgoid") & ", '" & objView(C1).Item("acctgdbcr").ToString & "', " & ToDouble(objView(C1).Item("amount").ToString) & ", 'COA Initial Balance', 'COA Initial Balance', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objView(C1).Item("amount").ToString) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(objView(C1).Item("amount").ToString) * cRate.GetRateMonthlyUSDValue & ")"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                gldtloid -= 1
                ' Insert QL_crdgl
                sSql = "INSERT INTO QL_crdgl (cmpcode, crdgloid, periodacctg, crdgldate, acctgoid, amtopen, amtdebit, amtcredit, amtbalance, amtopenusd, amtdebitusd, amtcreditusd, amtbalanceusd, postdate, crdglflag, crdglnote, createuser, createtime, upduser, updtime) VALUES ('" & DDLBusUnit.SelectedValue & "', " & crdgloid & ", '" & periodacctg & "', '" & gldate.Text & "', " & objView(C1).Item("acctgoid") & ", " & IIf(objView(C1).Item("acctgdbcr").ToString.ToUpper = "C", -ToDouble(objView(C1).Item("amount").ToString), ToDouble(objView(C1).Item("amount").ToString)) * cRate.GetRateMonthlyIDRValue & ", 0, 0, " & IIf(objView(C1).Item("acctgdbcr").ToString.ToUpper = "C", -ToDouble(objView(C1).Item("amount").ToString), ToDouble(objView(C1).Item("amount").ToString)) * cRate.GetRateMonthlyIDRValue & ", " & IIf(objView(C1).Item("acctgdbcr").ToString.ToUpper = "C", -ToDouble(objView(C1).Item("amount").ToString), ToDouble(objView(C1).Item("amount").ToString)) * cRate.GetRateMonthlyUSDValue & ", 0, 0, " & IIf(objView(C1).Item("acctgdbcr").ToString.ToUpper = "C", -ToDouble(objView(C1).Item("amount").ToString), ToDouble(objView(C1).Item("amount").ToString)) * cRate.GetRateMonthlyUSDValue & ", CURRENT_TIMESTAMP, 'Open', 'COA Initial Balance', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                crdgloid -= 1
            Next
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            showMessage(ex.Message, 1)
            conn.Close()
            objView.RowFilter = ""
            Exit Sub
        End Try
        objView.RowFilter = ""
        Response.Redirect("~\Accounting\trnCOAIB.aspx?awal=true")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Accounting\trnCOAIB.aspx?awal=true")
    End Sub
#End Region

End Class